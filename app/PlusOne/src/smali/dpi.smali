.class public Ldpi;
.super Lhny;
.source "PG"


# instance fields
.field public final a:I

.field public final b:Ljava/lang/String;

.field public c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private e:Landroid/content/Context;

.field private f:I

.field private h:Ljava/lang/String;

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:Lhxb;


# direct methods
.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ILjava/util/ArrayList;Ljava/util/ArrayList;ZZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;ZZZ",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 155
    invoke-direct {p0, p1, p2}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 156
    iput-object p1, p0, Ldpi;->e:Landroid/content/Context;

    .line 157
    iput p3, p0, Ldpi;->a:I

    .line 158
    iput p6, p0, Ldpi;->f:I

    .line 160
    iput-object p4, p0, Ldpi;->b:Ljava/lang/String;

    .line 161
    iput-object p5, p0, Ldpi;->h:Ljava/lang/String;

    .line 163
    iput-object p7, p0, Ldpi;->c:Ljava/util/ArrayList;

    .line 164
    iput-object p8, p0, Ldpi;->d:Ljava/util/ArrayList;

    .line 165
    iput-boolean p9, p0, Ldpi;->i:Z

    .line 166
    iput-boolean p10, p0, Ldpi;->j:Z

    .line 167
    iput-boolean p11, p0, Ldpi;->k:Z

    .line 169
    iput-object p12, p0, Ldpi;->l:Ljava/lang/String;

    .line 170
    iput-object p13, p0, Ldpi;->m:Ljava/lang/String;

    .line 171
    iput-object p14, p0, Ldpi;->n:Ljava/lang/String;

    .line 173
    const-class v0, Lhxb;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhxb;

    iput-object v0, p0, Ldpi;->p:Lhxb;

    .line 174
    return-void
.end method

.method private a(Ljava/util/ArrayList;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 395
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 397
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 398
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 399
    const-string v3, "f."

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x2

    .line 400
    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 401
    :cond_0
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 397
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 404
    :cond_1
    return-object v2
.end method

.method private a(ILjava/lang/String;)V
    .locals 4

    .prologue
    const v3, 0x7f0a09da

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 363
    iget-object v0, p0, Ldpi;->e:Landroid/content/Context;

    invoke-static {v0}, Ldsm;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 364
    const/16 v0, 0x65

    if-ne p1, v0, :cond_0

    .line 365
    invoke-virtual {p0}, Ldpi;->f()Landroid/content/Context;

    move-result-object v0

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p2, v1, v2

    invoke-virtual {v0, v3, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldpi;->o:Ljava/lang/String;

    .line 374
    :cond_0
    :goto_0
    return-void

    .line 369
    :cond_1
    const/16 v0, 0x193

    if-ne p1, v0, :cond_0

    .line 370
    invoke-virtual {p0}, Ldpi;->f()Landroid/content/Context;

    move-result-object v0

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p2, v1, v2

    invoke-virtual {v0, v3, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldpi;->o:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public V_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 346
    iget-boolean v0, p0, Ldpi;->k:Z

    if-eqz v0, :cond_0

    .line 347
    const/4 v0, 0x0

    .line 349
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Ldpi;->n:Ljava/lang/String;

    goto :goto_0
.end method

.method protected a()Lhoz;
    .locals 14

    .prologue
    const-wide/16 v12, 0x8

    const/4 v10, 0x3

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 178
    iget-object v0, p0, Ldpi;->e:Landroid/content/Context;

    invoke-static {v0}, Ldsm;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 179
    iget-boolean v0, p0, Ldpi;->k:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldpi;->p:Lhxb;

    invoke-virtual {p0}, Ldpi;->f()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Ldpi;->a:I

    iget-object v3, p0, Ldpi;->b:Ljava/lang/String;

    iget-boolean v4, p0, Ldpi;->k:Z

    invoke-interface {v0, v1, v2, v3, v4}, Lhxb;->a(Landroid/content/Context;ILjava/lang/String;Z)V

    :cond_0
    new-instance v0, Ljqn;

    iget-object v1, p0, Ldpi;->e:Landroid/content/Context;

    iget v2, p0, Ldpi;->a:I

    iget-object v3, p0, Ldpi;->b:Ljava/lang/String;

    iget-object v4, p0, Ldpi;->c:Ljava/util/ArrayList;

    invoke-direct {p0, v4}, Ldpi;->a(Ljava/util/ArrayList;)Ljava/util/List;

    move-result-object v4

    iget-object v5, p0, Ldpi;->d:Ljava/util/ArrayList;

    invoke-direct {p0, v5}, Ldpi;->a(Ljava/util/ArrayList;)Ljava/util/List;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Ljqn;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/util/List;Ljava/util/List;)V

    invoke-virtual {p0}, Ldpi;->f()Landroid/content/Context;

    move-result-object v1

    const-class v2, Llfn;

    invoke-static {v1, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Llfn;

    invoke-virtual {p0}, Ldpi;->d()V

    iget v2, p0, Ldpi;->a:I

    const-string v3, "[GMS Call start] updatePersonCirclesTask."

    invoke-interface {v1, v2, v12, v13, v3}, Llfn;->a(IJLjava/lang/String;)V

    invoke-static {v0}, Lhoc;->a(Lhny;)Lhoz;

    move-result-object v6

    iget v0, p0, Ldpi;->a:I

    const-string v2, "[GMS Call end] updatePersonCirclesTask."

    invoke-interface {v1, v0, v12, v13, v2}, Llfn;->a(IJLjava/lang/String;)V

    invoke-virtual {v6}, Lhoz;->f()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "ModifyCircleMemberships"

    invoke-static {v0, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "ModifyCircleMembershipsTask failed with "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6}, Lhoz;->a()I

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xb

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_1
    iget-boolean v0, p0, Ldpi;->k:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Ldpi;->p:Lhxb;

    invoke-virtual {p0}, Ldpi;->f()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Ldpi;->a:I

    iget-object v3, p0, Ldpi;->b:Ljava/lang/String;

    iget-object v4, p0, Ldpi;->h:Ljava/lang/String;

    move v5, v8

    invoke-interface/range {v0 .. v5}, Lhxb;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)V

    :cond_2
    invoke-virtual {v6}, Lhoz;->a()I

    move-result v0

    iget-object v1, p0, Ldpi;->h:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Ldpi;->a(ILjava/lang/String;)V

    invoke-virtual {p0}, Ldpi;->e()V

    :goto_0
    new-instance v0, Lhoz;

    invoke-virtual {v6}, Lhoz;->a()I

    move-result v1

    invoke-virtual {v6}, Lhoz;->b()Ljava/lang/Exception;

    move-result-object v2

    invoke-virtual {v6}, Lhoz;->f()Z

    move-result v3

    invoke-virtual {p0, v3}, Ldpi;->a(Z)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Ldpi;->a(Lhoz;)V

    :goto_1
    return-object v0

    :cond_3
    iget-object v0, p0, Ldpi;->e:Landroid/content/Context;

    iget v1, p0, Ldpi;->a:I

    invoke-static {v0, v1}, Ldsm;->b(Landroid/content/Context;I)Z

    iget-boolean v0, p0, Ldpi;->k:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Ldpi;->p:Lhxb;

    invoke-virtual {p0}, Ldpi;->f()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Ldpi;->a:I

    iget-object v3, p0, Ldpi;->b:Ljava/lang/String;

    iget-object v4, p0, Ldpi;->h:Ljava/lang/String;

    move v5, v9

    invoke-interface/range {v0 .. v5}, Lhxb;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)V

    :cond_4
    invoke-virtual {p0}, Ldpi;->f()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lieh;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    sget-object v1, Ldxd;->i:Lief;

    iget v2, p0, Ldpi;->a:I

    invoke-interface {v0, v1, v2}, Lieh;->b(Lief;I)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {p0}, Ldpi;->f()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Ldpi;->a:I

    invoke-static {v0, v1}, Lffl;->a(Landroid/content/Context;I)V

    goto :goto_0

    :cond_5
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    :cond_6
    invoke-virtual {p0}, Ldpi;->f()Landroid/content/Context;

    move-result-object v1

    new-instance v0, Ldlh;

    new-instance v2, Lkfo;

    iget v3, p0, Ldpi;->a:I

    invoke-direct {v2, v1, v3}, Lkfo;-><init>(Landroid/content/Context;I)V

    iget v3, p0, Ldpi;->a:I

    iget-object v3, p0, Ldpi;->b:Ljava/lang/String;

    iget-object v4, p0, Ldpi;->h:Ljava/lang/String;

    iget-object v5, p0, Ldpi;->c:Ljava/util/ArrayList;

    iget-object v6, p0, Ldpi;->d:Ljava/util/ArrayList;

    iget-boolean v7, p0, Ldpi;->k:Z

    iget v7, p0, Ldpi;->f:I

    invoke-direct/range {v0 .. v7}, Ldlh;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;I)V

    iget-boolean v1, p0, Ldpi;->k:Z

    if-eqz v1, :cond_7

    iget-object v1, p0, Ldpi;->p:Lhxb;

    invoke-virtual {p0}, Ldpi;->f()Landroid/content/Context;

    move-result-object v2

    iget v3, p0, Ldpi;->a:I

    iget-object v4, p0, Ldpi;->b:Ljava/lang/String;

    iget-boolean v5, p0, Ldpi;->k:Z

    invoke-interface {v1, v2, v3, v4, v5}, Lhxb;->a(Landroid/content/Context;ILjava/lang/String;Z)V

    :cond_7
    invoke-virtual {p0}, Ldpi;->d()V

    invoke-virtual {v0}, Ldlh;->l()V

    invoke-virtual {v0}, Ldlh;->t()Z

    move-result v1

    if-eqz v1, :cond_b

    const-string v1, "ModifyCircleMemberships"

    invoke-static {v1, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_8

    const-string v1, "ModifyCircleMembershipsTask failed with "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget v2, v0, Lkff;->i:I

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0xb

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_8
    iget-boolean v1, p0, Ldpi;->k:Z

    if-eqz v1, :cond_9

    iget-object v1, p0, Ldpi;->p:Lhxb;

    invoke-virtual {p0}, Ldpi;->f()Landroid/content/Context;

    move-result-object v2

    iget v3, p0, Ldpi;->a:I

    iget-object v4, p0, Ldpi;->b:Ljava/lang/String;

    iget-object v5, p0, Ldpi;->h:Ljava/lang/String;

    move v6, v8

    invoke-interface/range {v1 .. v6}, Lhxb;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)V

    :cond_9
    iget v1, v0, Lkff;->i:I

    iget-object v2, p0, Ldpi;->h:Ljava/lang/String;

    invoke-direct {p0, v1, v2}, Ldpi;->a(ILjava/lang/String;)V

    invoke-virtual {p0}, Ldpi;->e()V

    :cond_a
    :goto_2
    invoke-virtual {p0, v0}, Ldpi;->a(Ldlh;)Lhoz;

    move-result-object v0

    goto/16 :goto_1

    :cond_b
    invoke-virtual {v0}, Ldlh;->D()Loxu;

    move-result-object v1

    check-cast v1, Lmff;

    iget-object v2, p0, Ldpi;->b:Ljava/lang/String;

    invoke-virtual {p0, v1, v2}, Ldpi;->a(Lmff;Ljava/lang/String;)V

    iget-boolean v1, p0, Ldpi;->k:Z

    if-eqz v1, :cond_a

    iget-object v1, p0, Ldpi;->p:Lhxb;

    invoke-virtual {p0}, Ldpi;->f()Landroid/content/Context;

    move-result-object v2

    iget v3, p0, Ldpi;->a:I

    iget-object v4, p0, Ldpi;->b:Ljava/lang/String;

    iget-object v5, p0, Ldpi;->h:Ljava/lang/String;

    move v6, v9

    invoke-interface/range {v1 .. v6}, Lhxb;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)V

    goto :goto_2
.end method

.method protected a(Ldlh;)Lhoz;
    .locals 4

    .prologue
    .line 321
    new-instance v0, Lhoz;

    iget v1, p1, Lkff;->i:I

    iget-object v2, p1, Lkff;->k:Ljava/lang/Exception;

    .line 322
    invoke-virtual {p1}, Ldlh;->t()Z

    move-result v3

    invoke-virtual {p0, v3}, Ldpi;->a(Z)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    .line 323
    invoke-virtual {p0, v0}, Ldpi;->a(Lhoz;)V

    .line 324
    return-object v0
.end method

.method public a(Z)Ljava/lang/String;
    .locals 2

    .prologue
    .line 354
    if-eqz p1, :cond_1

    .line 355
    iget-object v0, p0, Ldpi;->o:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 356
    invoke-virtual {p0}, Ldpi;->f()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a0592

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 358
    :goto_0
    return-object v0

    .line 356
    :cond_0
    iget-object v0, p0, Ldpi;->o:Ljava/lang/String;

    goto :goto_0

    .line 358
    :cond_1
    iget-object v0, p0, Ldpi;->l:Ljava/lang/String;

    goto :goto_0
.end method

.method protected a(Lhoz;)V
    .locals 0

    .prologue
    .line 389
    return-void
.end method

.method protected a(Lmff;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 275
    iget-object v0, p1, Lmff;->a:Loju;

    .line 276
    const/4 v3, 0x0

    .line 277
    iget-object v1, v0, Loju;->a:[Lohv;

    if-eqz v1, :cond_0

    iget-object v1, v0, Loju;->a:[Lohv;

    array-length v1, v1

    if-lez v1, :cond_0

    .line 278
    iget-object v0, v0, Loju;->a:[Lohv;

    aget-object v3, v0, v2

    .line 285
    if-eqz v3, :cond_0

    iget-object v0, v3, Lohv;->d:[Loij;

    if-nez v0, :cond_0

    .line 286
    iget-object v0, p0, Ldpi;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Loij;

    iput-object v0, v3, Lohv;->d:[Loij;

    move v1, v2

    .line 287
    :goto_0
    iget-object v0, p0, Ldpi;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 288
    new-instance v4, Loij;

    invoke-direct {v4}, Loij;-><init>()V

    .line 289
    iget-object v0, p0, Ldpi;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lhxe;->b(Ljava/lang/String;)Lohn;

    move-result-object v0

    iput-object v0, v4, Loij;->b:Lohn;

    .line 290
    iget-object v0, v3, Lohv;->d:[Loij;

    aput-object v4, v0, v1

    .line 287
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 304
    :cond_0
    iget-object v0, p0, Ldpi;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldpi;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    const/4 v2, 0x1

    .line 305
    :cond_2
    if-nez v3, :cond_3

    if-eqz v2, :cond_4

    .line 306
    :cond_3
    invoke-virtual {p0}, Ldpi;->f()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Ldpi;->a:I

    iget-object v4, p0, Ldpi;->c:Ljava/util/ArrayList;

    iget-object v5, p0, Ldpi;->d:Ljava/util/ArrayList;

    move-object v2, p2

    invoke-static/range {v0 .. v5}, Ldsm;->a(Landroid/content/Context;ILjava/lang/String;Lohv;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 310
    :cond_4
    invoke-virtual {p0}, Ldpi;->f()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lieh;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    .line 313
    sget-object v1, Ldxd;->i:Lief;

    iget v2, p0, Ldpi;->a:I

    invoke-interface {v0, v1, v2}, Lieh;->b(Lief;I)Z

    move-result v0

    if-nez v0, :cond_5

    .line 314
    invoke-virtual {p0}, Ldpi;->f()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Ldpi;->a:I

    invoke-static {v0, v1}, Lffl;->a(Landroid/content/Context;I)V

    return-void

    .line 316
    :cond_5
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 329
    iget-boolean v0, p0, Ldpi;->k:Z

    if-eqz v0, :cond_0

    .line 330
    const/4 v0, 0x0

    .line 340
    :goto_0
    return-object v0

    .line 333
    :cond_0
    iget-object v0, p0, Ldpi;->m:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 334
    iget-object v0, p0, Ldpi;->m:Ljava/lang/String;

    goto :goto_0

    .line 335
    :cond_1
    iget-boolean v0, p0, Ldpi;->i:Z

    if-eqz v0, :cond_2

    .line 336
    invoke-virtual {p0}, Ldpi;->f()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a09d3

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 337
    :cond_2
    iget-boolean v0, p0, Ldpi;->j:Z

    if-eqz v0, :cond_3

    .line 338
    invoke-virtual {p0}, Ldpi;->f()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a09d6

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 340
    :cond_3
    invoke-virtual {p0}, Ldpi;->f()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a09d7

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected d()V
    .locals 0

    .prologue
    .line 379
    return-void
.end method

.method protected e()V
    .locals 0

    .prologue
    .line 384
    return-void
.end method
