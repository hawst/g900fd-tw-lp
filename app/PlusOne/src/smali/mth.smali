.class public final Lmth;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmth;


# instance fields
.field public b:Lmwi;

.field public c:Lmsj;

.field public d:Lmtg;

.field public e:Lmtg;

.field public f:Ljava/lang/Long;

.field private g:Lmtk;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2280
    const/4 v0, 0x0

    new-array v0, v0, [Lmth;

    sput-object v0, Lmth;->a:[Lmth;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2281
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2284
    iput-object v0, p0, Lmth;->b:Lmwi;

    .line 2287
    iput-object v0, p0, Lmth;->c:Lmsj;

    .line 2290
    iput-object v0, p0, Lmth;->d:Lmtg;

    .line 2293
    iput-object v0, p0, Lmth;->e:Lmtg;

    .line 2296
    iput-object v0, p0, Lmth;->g:Lmtk;

    .line 2281
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 2327
    const/4 v0, 0x0

    .line 2328
    iget-object v1, p0, Lmth;->b:Lmwi;

    if-eqz v1, :cond_0

    .line 2329
    const/4 v0, 0x1

    iget-object v1, p0, Lmth;->b:Lmwi;

    .line 2330
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2332
    :cond_0
    iget-object v1, p0, Lmth;->c:Lmsj;

    if-eqz v1, :cond_1

    .line 2333
    const/4 v1, 0x2

    iget-object v2, p0, Lmth;->c:Lmsj;

    .line 2334
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2336
    :cond_1
    iget-object v1, p0, Lmth;->d:Lmtg;

    if-eqz v1, :cond_2

    .line 2337
    const/4 v1, 0x3

    iget-object v2, p0, Lmth;->d:Lmtg;

    .line 2338
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2340
    :cond_2
    iget-object v1, p0, Lmth;->e:Lmtg;

    if-eqz v1, :cond_3

    .line 2341
    const/4 v1, 0x4

    iget-object v2, p0, Lmth;->e:Lmtg;

    .line 2342
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2344
    :cond_3
    iget-object v1, p0, Lmth;->g:Lmtk;

    if-eqz v1, :cond_4

    .line 2345
    const/4 v1, 0x5

    iget-object v2, p0, Lmth;->g:Lmtk;

    .line 2346
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2348
    :cond_4
    iget-object v1, p0, Lmth;->f:Ljava/lang/Long;

    if-eqz v1, :cond_5

    .line 2349
    const/4 v1, 0x6

    iget-object v2, p0, Lmth;->f:Ljava/lang/Long;

    .line 2350
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2352
    :cond_5
    iget-object v1, p0, Lmth;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2353
    iput v0, p0, Lmth;->ai:I

    .line 2354
    return v0
.end method

.method public a(Loxn;)Lmth;
    .locals 2

    .prologue
    .line 2362
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2363
    sparse-switch v0, :sswitch_data_0

    .line 2367
    iget-object v1, p0, Lmth;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 2368
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmth;->ah:Ljava/util/List;

    .line 2371
    :cond_1
    iget-object v1, p0, Lmth;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2373
    :sswitch_0
    return-object p0

    .line 2378
    :sswitch_1
    iget-object v0, p0, Lmth;->b:Lmwi;

    if-nez v0, :cond_2

    .line 2379
    new-instance v0, Lmwi;

    invoke-direct {v0}, Lmwi;-><init>()V

    iput-object v0, p0, Lmth;->b:Lmwi;

    .line 2381
    :cond_2
    iget-object v0, p0, Lmth;->b:Lmwi;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2385
    :sswitch_2
    iget-object v0, p0, Lmth;->c:Lmsj;

    if-nez v0, :cond_3

    .line 2386
    new-instance v0, Lmsj;

    invoke-direct {v0}, Lmsj;-><init>()V

    iput-object v0, p0, Lmth;->c:Lmsj;

    .line 2388
    :cond_3
    iget-object v0, p0, Lmth;->c:Lmsj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2392
    :sswitch_3
    iget-object v0, p0, Lmth;->d:Lmtg;

    if-nez v0, :cond_4

    .line 2393
    new-instance v0, Lmtg;

    invoke-direct {v0}, Lmtg;-><init>()V

    iput-object v0, p0, Lmth;->d:Lmtg;

    .line 2395
    :cond_4
    iget-object v0, p0, Lmth;->d:Lmtg;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2399
    :sswitch_4
    iget-object v0, p0, Lmth;->e:Lmtg;

    if-nez v0, :cond_5

    .line 2400
    new-instance v0, Lmtg;

    invoke-direct {v0}, Lmtg;-><init>()V

    iput-object v0, p0, Lmth;->e:Lmtg;

    .line 2402
    :cond_5
    iget-object v0, p0, Lmth;->e:Lmtg;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2406
    :sswitch_5
    iget-object v0, p0, Lmth;->g:Lmtk;

    if-nez v0, :cond_6

    .line 2407
    new-instance v0, Lmtk;

    invoke-direct {v0}, Lmtk;-><init>()V

    iput-object v0, p0, Lmth;->g:Lmtk;

    .line 2409
    :cond_6
    iget-object v0, p0, Lmth;->g:Lmtk;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2413
    :sswitch_6
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lmth;->f:Ljava/lang/Long;

    goto :goto_0

    .line 2363
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 2303
    iget-object v0, p0, Lmth;->b:Lmwi;

    if-eqz v0, :cond_0

    .line 2304
    const/4 v0, 0x1

    iget-object v1, p0, Lmth;->b:Lmwi;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2306
    :cond_0
    iget-object v0, p0, Lmth;->c:Lmsj;

    if-eqz v0, :cond_1

    .line 2307
    const/4 v0, 0x2

    iget-object v1, p0, Lmth;->c:Lmsj;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2309
    :cond_1
    iget-object v0, p0, Lmth;->d:Lmtg;

    if-eqz v0, :cond_2

    .line 2310
    const/4 v0, 0x3

    iget-object v1, p0, Lmth;->d:Lmtg;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2312
    :cond_2
    iget-object v0, p0, Lmth;->e:Lmtg;

    if-eqz v0, :cond_3

    .line 2313
    const/4 v0, 0x4

    iget-object v1, p0, Lmth;->e:Lmtg;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2315
    :cond_3
    iget-object v0, p0, Lmth;->g:Lmtk;

    if-eqz v0, :cond_4

    .line 2316
    const/4 v0, 0x5

    iget-object v1, p0, Lmth;->g:Lmtk;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2318
    :cond_4
    iget-object v0, p0, Lmth;->f:Ljava/lang/Long;

    if-eqz v0, :cond_5

    .line 2319
    const/4 v0, 0x6

    iget-object v1, p0, Lmth;->f:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 2321
    :cond_5
    iget-object v0, p0, Lmth;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2323
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2277
    invoke-virtual {p0, p1}, Lmth;->a(Loxn;)Lmth;

    move-result-object v0

    return-object v0
.end method
