.class public final Lmap;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmqb;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 19114
    invoke-direct {p0}, Loxq;-><init>()V

    .line 19117
    iput-object v0, p0, Lmap;->apiHeader:Llyr;

    .line 19120
    iput-object v0, p0, Lmap;->a:Lmqb;

    .line 19114
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 19137
    const/4 v0, 0x0

    .line 19138
    iget-object v1, p0, Lmap;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 19139
    const/4 v0, 0x1

    iget-object v1, p0, Lmap;->apiHeader:Llyr;

    .line 19140
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 19142
    :cond_0
    iget-object v1, p0, Lmap;->a:Lmqb;

    if-eqz v1, :cond_1

    .line 19143
    const/4 v1, 0x2

    iget-object v2, p0, Lmap;->a:Lmqb;

    .line 19144
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 19146
    :cond_1
    iget-object v1, p0, Lmap;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 19147
    iput v0, p0, Lmap;->ai:I

    .line 19148
    return v0
.end method

.method public a(Loxn;)Lmap;
    .locals 2

    .prologue
    .line 19156
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 19157
    sparse-switch v0, :sswitch_data_0

    .line 19161
    iget-object v1, p0, Lmap;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 19162
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmap;->ah:Ljava/util/List;

    .line 19165
    :cond_1
    iget-object v1, p0, Lmap;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 19167
    :sswitch_0
    return-object p0

    .line 19172
    :sswitch_1
    iget-object v0, p0, Lmap;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 19173
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmap;->apiHeader:Llyr;

    .line 19175
    :cond_2
    iget-object v0, p0, Lmap;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 19179
    :sswitch_2
    iget-object v0, p0, Lmap;->a:Lmqb;

    if-nez v0, :cond_3

    .line 19180
    new-instance v0, Lmqb;

    invoke-direct {v0}, Lmqb;-><init>()V

    iput-object v0, p0, Lmap;->a:Lmqb;

    .line 19182
    :cond_3
    iget-object v0, p0, Lmap;->a:Lmqb;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 19157
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 19125
    iget-object v0, p0, Lmap;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 19126
    const/4 v0, 0x1

    iget-object v1, p0, Lmap;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 19128
    :cond_0
    iget-object v0, p0, Lmap;->a:Lmqb;

    if-eqz v0, :cond_1

    .line 19129
    const/4 v0, 0x2

    iget-object v1, p0, Lmap;->a:Lmqb;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 19131
    :cond_1
    iget-object v0, p0, Lmap;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 19133
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 19110
    invoke-virtual {p0, p1}, Lmap;->a(Loxn;)Lmap;

    move-result-object v0

    return-object v0
.end method
