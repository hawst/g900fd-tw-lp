.class public final Lcsj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcsh;
.implements Lcsm;
.implements Lhob;
.implements Llgq;
.implements Llnx;
.implements Llra;
.implements Llrb;
.implements Llrc;
.implements Llrg;


# instance fields
.field private final a:Lz;

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcsn;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lhoc;

.field private d:Lcsd;

.field private e:Lcsi;

.field private f:Lhee;

.field private g:Llgl;

.field private h:Z


# direct methods
.method public constructor <init>(Lz;Llqr;)V
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcsj;->b:Ljava/util/Set;

    .line 65
    iput-object p1, p0, Lcsj;->a:Lz;

    .line 67
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 68
    return-void
.end method

.method static synthetic a(Lcsj;)Z
    .locals 1

    .prologue
    .line 45
    iget-boolean v0, p0, Lcsj;->h:Z

    return v0
.end method

.method static synthetic b(Lcsj;)Lz;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcsj;->a:Lz;

    return-object v0
.end method

.method static synthetic c(Lcsj;)Llgl;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcsj;->g:Llgl;

    return-object v0
.end method

.method static synthetic d(Lcsj;)V
    .locals 5

    .prologue
    .line 45
    iget-object v0, p0, Lcsj;->d:Lcsd;

    invoke-virtual {v0}, Lcsd;->c()V

    iget-object v0, p0, Lcsj;->a:Lz;

    const-class v1, Lhms;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Lcsj;->a:Lz;

    invoke-direct {v1, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->eJ:Lhmv;

    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    new-instance v0, Ldpy;

    iget-object v1, p0, Lcsj;->a:Lz;

    iget-object v2, p0, Lcsj;->f:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    iget-object v3, p0, Lcsj;->e:Lcsi;

    invoke-virtual {v3}, Lcsi;->c()Lduo;

    move-result-object v3

    iget-object v4, p0, Lcsj;->e:Lcsi;

    invoke-virtual {v4}, Lcsi;->b()[Ljuy;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Ldpy;-><init>(Landroid/content/Context;ILduo;[Ljuy;)V

    new-instance v1, Lhpd;

    iget-object v2, p0, Lcsj;->a:Lz;

    iget-object v3, p0, Lcsj;->a:Lz;

    invoke-virtual {v3}, Lz;->f()Lae;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lhpd;-><init>(Landroid/content/Context;Lae;)V

    iget-object v2, p0, Lcsj;->a:Lz;

    invoke-virtual {v2}, Lz;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0872

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Ldpy;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lhos;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcsj;->c:Lhoc;

    invoke-virtual {v1, v0}, Lhoc;->b(Lhny;)V

    return-void
.end method

.method static synthetic e(Lcsj;)Lcsi;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcsj;->e:Lcsi;

    return-object v0
.end method


# virtual methods
.method public E_()V
    .locals 1

    .prologue
    .line 112
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcsj;->h:Z

    .line 113
    return-void
.end method

.method public a()V
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcsj;->g:Llgl;

    invoke-virtual {v0}, Llgl;->a()V

    .line 72
    iget-object v0, p0, Lcsj;->e:Lcsi;

    invoke-virtual {v0}, Lcsi;->a()V

    .line 73
    return-void
.end method

.method public a(Landroid/content/Context;Llnh;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 85
    const-class v0, Lcsd;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcsd;

    iput-object v0, p0, Lcsj;->d:Lcsd;

    .line 86
    const-class v0, Lcsi;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcsi;

    iput-object v0, p0, Lcsj;->e:Lcsi;

    .line 87
    const-class v0, Lhee;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Lcsj;->f:Lhee;

    .line 88
    const-class v0, Llgl;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llgl;

    iput-object v0, p0, Lcsj;->g:Llgl;

    .line 89
    const-class v0, Lhoc;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    iput-object v0, p0, Lcsj;->c:Lhoc;

    .line 90
    const-class v0, Lcsg;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcsg;

    .line 91
    invoke-virtual {v0, p0}, Lcsg;->a(Lcsh;)V

    .line 92
    return-void
.end method

.method public a(Lcsn;)V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcsj;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 77
    return-void
.end method

.method public a(Lduo;[Ljuy;)V
    .locals 0

    .prologue
    .line 128
    invoke-virtual {p0, p1, p2}, Lcsj;->b(Lduo;[Ljuy;)V

    .line 129
    return-void
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 4

    .prologue
    .line 119
    const-string v0, "RemovePhotosFromTrashTask"

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 120
    invoke-virtual {p3, p1}, Lhos;->a(Ljava/lang/String;)V

    .line 121
    iget-object v0, p0, Lcsj;->d:Lcsd;

    invoke-virtual {v0}, Lcsd;->b()V

    .line 122
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "restore"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "restored_uris"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "resolver"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 124
    :cond_0
    return-void

    .line 122
    :cond_1
    const-string v1, "restored_uris"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    const-string v1, "resolver"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lduo;

    iget-object v1, p0, Lcsj;->b:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcsn;

    invoke-interface {v1, v2, v0}, Lcsn;->a(Ljava/util/List;Lduo;)V

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 96
    iget-object v0, p0, Lcsj;->e:Lcsi;

    invoke-virtual {v0}, Lcsi;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcsj;->e:Lcsi;

    invoke-virtual {v0}, Lcsi;->c()Lduo;

    move-result-object v0

    iget-object v1, p0, Lcsj;->e:Lcsi;

    invoke-virtual {v1}, Lcsi;->b()[Ljuy;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcsj;->b(Lduo;[Ljuy;)V

    .line 99
    :cond_0
    iget-object v0, p0, Lcsj;->g:Llgl;

    invoke-virtual {v0, p0}, Llgl;->a(Llgq;)V

    .line 100
    iget-object v0, p0, Lcsj;->c:Lhoc;

    invoke-virtual {v0, p0}, Lhoc;->a(Lhob;)Lhoc;

    .line 101
    return-void
.end method

.method public b(Lcsn;)V
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcsj;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 81
    return-void
.end method

.method protected b(Lduo;[Ljuy;)V
    .locals 2

    .prologue
    .line 156
    iget-object v0, p0, Lcsj;->a:Lz;

    const v1, 0x1020002

    invoke-virtual {v0, v1}, Lz;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcsk;

    invoke-direct {v1, p0, p1, p2}, Lcsk;-><init>(Lcsj;Lduo;[Ljuy;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 180
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lcsj;->g:Llgl;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Llgl;->a(Z)V

    .line 106
    iget-object v0, p0, Lcsj;->g:Llgl;

    invoke-virtual {v0, p0}, Llgl;->b(Llgq;)V

    .line 107
    iget-object v0, p0, Lcsj;->c:Lhoc;

    invoke-virtual {v0, p0}, Lhoc;->b(Lhob;)Lhoc;

    .line 108
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcsj;->e:Lcsi;

    invoke-virtual {v0}, Lcsi;->a()V

    .line 201
    return-void
.end method
