.class public final Lnsv;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Integer;

.field private b:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2001
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 2022
    const/4 v0, 0x0

    .line 2023
    iget-object v1, p0, Lnsv;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 2024
    const/4 v0, 0x1

    iget-object v1, p0, Lnsv;->a:Ljava/lang/Integer;

    .line 2025
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2027
    :cond_0
    iget-object v1, p0, Lnsv;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 2028
    const/4 v1, 0x2

    iget-object v2, p0, Lnsv;->b:Ljava/lang/Integer;

    .line 2029
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2031
    :cond_1
    iget-object v1, p0, Lnsv;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2032
    iput v0, p0, Lnsv;->ai:I

    .line 2033
    return v0
.end method

.method public a(Loxn;)Lnsv;
    .locals 2

    .prologue
    .line 2041
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2042
    sparse-switch v0, :sswitch_data_0

    .line 2046
    iget-object v1, p0, Lnsv;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 2047
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnsv;->ah:Ljava/util/List;

    .line 2050
    :cond_1
    iget-object v1, p0, Lnsv;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2052
    :sswitch_0
    return-object p0

    .line 2057
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnsv;->a:Ljava/lang/Integer;

    goto :goto_0

    .line 2061
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnsv;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 2042
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 2010
    iget-object v0, p0, Lnsv;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 2011
    const/4 v0, 0x1

    iget-object v1, p0, Lnsv;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2013
    :cond_0
    iget-object v0, p0, Lnsv;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 2014
    const/4 v0, 0x2

    iget-object v1, p0, Lnsv;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2016
    :cond_1
    iget-object v0, p0, Lnsv;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2018
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1997
    invoke-virtual {p0, p1}, Lnsv;->a(Loxn;)Lnsv;

    move-result-object v0

    return-object v0
.end method
