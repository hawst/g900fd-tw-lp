.class public Lfew;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lheo;
.implements Lhry;


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcuv;

.field private d:Lhei;

.field private final e:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 88
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "local_media_inserted"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "initial_sync_complete"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "current_metadata_limit"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "pressed_load_more"

    aput-object v2, v0, v1

    sput-object v0, Lfew;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lfew;->e:Landroid/util/SparseArray;

    .line 103
    iput-object p1, p0, Lfew;->b:Landroid/content/Context;

    .line 104
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Lfew;->d:Lhei;

    .line 105
    new-instance v0, Lcuv;

    invoke-direct {v0, p1}, Lcuv;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lfew;->c:Lcuv;

    .line 106
    iget-object v0, p0, Lfew;->b:Landroid/content/Context;

    invoke-static {v0}, Lhrx;->a(Landroid/content/Context;)Lhrx;

    move-result-object v0

    invoke-virtual {v0, p0}, Lhrx;->a(Lhry;)V

    .line 107
    iget-object v0, p0, Lfew;->b:Landroid/content/Context;

    invoke-static {v0}, Lhpx;->a(Landroid/content/Context;)Lhpx;

    move-result-object v0

    new-instance v1, Lfex;

    invoke-direct {v1, p0}, Lfex;-><init>(Lfew;)V

    invoke-virtual {v0, v1}, Lhpx;->a(Lhpy;)V

    .line 115
    return-void
.end method

.method static synthetic a(Lfew;)Lhei;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lfew;->d:Lhei;

    return-object v0
.end method

.method static synthetic a(Lfew;IZ)V
    .locals 2

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lfew;->f(I)Lhek;

    move-result-object v0

    const-string v1, "local_media_inserted"

    invoke-interface {v0, v1, p2}, Lhek;->b(Ljava/lang/String;Z)Lhek;

    move-result-object v0

    invoke-interface {v0}, Lhek;->c()I

    return-void
.end method

.method static synthetic b(Lfew;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lfew;->b:Landroid/content/Context;

    return-object v0
.end method

.method private e(I)Lhej;
    .locals 2

    .prologue
    .line 118
    iget-object v0, p0, Lfew;->d:Lhei;

    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "AllPhotosSyncManager"

    invoke-interface {v0, v1}, Lhej;->d(Ljava/lang/String;)Lhej;

    move-result-object v0

    return-object v0
.end method

.method private f(I)Lhek;
    .locals 2

    .prologue
    .line 122
    iget-object v0, p0, Lfew;->d:Lhei;

    invoke-interface {v0, p1}, Lhei;->b(I)Lhek;

    move-result-object v0

    const-string v1, "AllPhotosSyncManager"

    invoke-interface {v0, v1}, Lhek;->f(Ljava/lang/String;)Lhek;

    move-result-object v0

    return-object v0
.end method

.method private g(I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 126
    iget-object v1, p0, Lfew;->e:Landroid/util/SparseArray;

    monitor-enter v1

    .line 127
    :try_start_0
    iget-object v0, p0, Lfew;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 128
    if-nez v0, :cond_0

    .line 129
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    .line 130
    iget-object v2, p0, Lfew;->e:Landroid/util/SparseArray;

    invoke-virtual {v2, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 132
    :cond_0
    monitor-exit v1

    return-object v0

    .line 133
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private h(I)I
    .locals 3

    .prologue
    .line 137
    invoke-direct {p0, p1}, Lfew;->e(I)Lhej;

    move-result-object v0

    const-string v1, "current_metadata_limit"

    const/16 v2, 0x5dc0

    invoke-interface {v0, v1, v2}, Lhej;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method


# virtual methods
.method public a(I)Ljava/lang/Long;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 164
    const/4 v1, -0x1

    if-ne p1, v1, :cond_1

    .line 169
    :cond_0
    :goto_0
    return-object v0

    .line 168
    :cond_1
    invoke-direct {p0, p1}, Lfew;->e(I)Lhej;

    move-result-object v1

    .line 169
    const-string v2, "all_photos_count"

    invoke-interface {v1, v2}, Lhej;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v0, "all_photos_count"

    const-wide/16 v2, 0x0

    invoke-interface {v1, v0, v2, v3}, Lhej;->a(Ljava/lang/String;J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0
.end method

.method public a(IJ)V
    .locals 10

    .prologue
    .line 309
    invoke-direct {p0, p1}, Lfew;->f(I)Lhek;

    move-result-object v0

    const-string v1, "pressed_load_more"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lhek;->b(Ljava/lang/String;Z)Lhek;

    move-result-object v0

    invoke-interface {v0}, Lhek;->c()I

    .line 312
    iget-object v0, p0, Lfew;->b:Landroid/content/Context;

    invoke-static {v0, p1}, Ljvd;->a(Landroid/content/Context;I)J

    move-result-wide v0

    .line 313
    cmp-long v0, p2, v0

    if-gez v0, :cond_0

    .line 346
    :goto_0
    return-void

    .line 320
    :cond_0
    invoke-direct {p0, p1}, Lfew;->g(I)Ljava/lang/Object;

    move-result-object v2

    .line 321
    monitor-enter v2

    .line 322
    :try_start_0
    iget-object v0, p0, Lfew;->b:Landroid/content/Context;

    invoke-static {v0, p1}, Ljvd;->a(Landroid/content/Context;I)J

    move-result-wide v0

    .line 323
    const-string v3, "AllPhotosSyncManager"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 324
    const-string v3, "loadMoreMedia accountId: "

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 328
    invoke-direct {p0, p1}, Lfew;->h(I)I

    move-result v4

    iget-object v5, p0, Lfew;->b:Landroid/content/Context;

    .line 330
    invoke-static {v5, p1}, Ljvd;->b(Landroid/content/Context;I)J

    move-result-wide v6

    iget-object v5, p0, Lfew;->b:Landroid/content/Context;

    .line 331
    invoke-static {v5, p1}, Ldtd;->c(Landroid/content/Context;I)Z

    move-result v5

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit16 v9, v9, 0xbf

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v8, " target item count: "

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v8, " current item count: "

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v8, " current metadata cap: "

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " current metadata count: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " syncComplete: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 324
    :cond_1
    :goto_1
    cmp-long v0, p2, v0

    if-lez v0, :cond_2

    iget-object v0, p0, Lfew;->b:Landroid/content/Context;

    .line 340
    invoke-static {v0, p1}, Ldtd;->c(Landroid/content/Context;I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 341
    invoke-direct {p0, p1}, Lfew;->h(I)I

    move-result v0

    invoke-direct {p0, p1}, Lfew;->f(I)Lhek;

    move-result-object v1

    const-string v3, "current_metadata_limit"

    add-int/lit16 v0, v0, 0x7d0

    invoke-interface {v1, v3, v0}, Lhek;->b(Ljava/lang/String;I)Lhek;

    move-result-object v0

    invoke-interface {v0}, Lhek;->c()I

    .line 342
    const/4 v0, 0x0

    sget-object v1, Ldts;->d:Ldts;

    invoke-virtual {p0, p1, v0, v1}, Lfew;->a(ILdvz;Ldts;)V

    .line 344
    iget-object v0, p0, Lfew;->b:Landroid/content/Context;

    invoke-static {v0, p1}, Ljvd;->a(Landroid/content/Context;I)J

    move-result-wide v0

    goto :goto_1

    .line 346
    :cond_2
    monitor-exit v2

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(ILdts;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 297
    sget-object v1, Ldts;->c:Ldts;

    if-ne p2, v1, :cond_0

    .line 298
    invoke-virtual {p0, p1}, Lfew;->d(I)V

    invoke-virtual {p0, v0}, Lfew;->a(Z)V

    sget-object v0, Ldts;->c:Ldts;

    invoke-virtual {p0, p1, v2, v0}, Lfew;->a(ILdvz;Ldts;)V

    .line 302
    :goto_0
    return-void

    .line 300
    :cond_0
    sget-object v1, Ldts;->c:Ldts;

    if-eq p2, v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    invoke-virtual {p0, v0}, Lfew;->a(Z)V

    invoke-virtual {p0, p1, v2, p2}, Lfew;->a(ILdvz;Ldts;)V

    goto :goto_0
.end method

.method public a(ILdvz;Ldts;)V
    .locals 10

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 356
    invoke-direct {p0, p1}, Lfew;->g(I)Ljava/lang/Object;

    move-result-object v2

    .line 357
    monitor-enter v2

    .line 359
    :try_start_0
    iget-boolean v3, p3, Ldts;->g:Z

    if-nez v3, :cond_0

    move v1, v0

    .line 361
    :cond_0
    iget-object v0, p0, Lfew;->b:Landroid/content/Context;

    .line 362
    invoke-static {v0, p1}, Lcuj;->a(Landroid/content/Context;I)Lnyc;

    move-result-object v3

    .line 364
    if-nez v3, :cond_1

    .line 365
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Unable to retrieve all photos sync settings"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 403
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 368
    :cond_1
    :try_start_1
    iget-object v0, v3, Lnyc;->a:Ljava/lang/Integer;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v4

    invoke-direct {p0, p1}, Lfew;->h(I)I

    move-result v0

    invoke-direct {p0, p1}, Lfew;->e(I)Lhej;

    move-result-object v5

    const-string v6, "pressed_load_more"

    const/4 v7, 0x0

    invoke-interface {v5, v6, v7}, Lhej;->a(Ljava/lang/String;Z)Z

    move-result v5

    if-nez v5, :cond_2

    invoke-static {v4, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 373
    :cond_2
    invoke-direct {p0, p1}, Lfew;->f(I)Lhek;

    move-result-object v4

    const-string v5, "current_metadata_limit"

    invoke-interface {v4, v5, v0}, Lhek;->b(Ljava/lang/String;I)Lhek;

    move-result-object v4

    invoke-interface {v4}, Lhek;->c()I

    .line 375
    const-string v4, "AllPhotosSyncManager"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 376
    const-string v4, "About to begin all photos sync, max to sync: "

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 380
    invoke-direct {p0, p1}, Lfew;->h(I)I

    move-result v6

    iget-object v3, v3, Lnyc;->a:Ljava/lang/Integer;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit16 v8, v8, 0x80

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, ", syncType: "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", from foreground: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", local metadata cap: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", max photos in settings: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", account: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 376
    :cond_3
    iget-object v1, p0, Lfew;->b:Landroid/content/Context;

    invoke-static {v1, p1, p2, p3, v0}, Ldtd;->a(Landroid/content/Context;ILdvz;Ldts;I)Ldtp;

    move-result-object v0

    .line 388
    iget-object v1, v0, Ldtp;->b:Ljava/lang/Long;

    if-eqz v1, :cond_4

    .line 389
    iget-object v1, v0, Ldtp;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct {p0, p1}, Lfew;->f(I)Lhek;

    move-result-object v1

    const-string v3, "all_photos_count"

    invoke-interface {v1, v3, v4, v5}, Lhek;->b(Ljava/lang/String;J)Lhek;

    move-result-object v1

    invoke-interface {v1}, Lhek;->c()I

    .line 392
    :cond_4
    iget-object v1, p0, Lfew;->b:Landroid/content/Context;

    invoke-static {v1, p1}, Ljvd;->b(Landroid/content/Context;I)J

    move-result-wide v4

    .line 395
    const-string v1, "AllPhotosSyncManager"

    const/4 v3, 0x3

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 396
    const-string v1, "Finished all photos sync, reason: "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, v0, Ldtp;->a:Ldtr;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x41

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", total photos synced: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", account: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 402
    :cond_5
    invoke-direct {p0, p1}, Lfew;->f(I)Lhek;

    move-result-object v0

    const-string v1, "initial_sync_complete"

    const/4 v3, 0x1

    invoke-interface {v0, v1, v3}, Lhek;->b(Ljava/lang/String;Z)Lhek;

    move-result-object v0

    invoke-interface {v0}, Lhek;->c()I

    .line 403
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 405
    iget-object v0, p0, Lfew;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {p1}, Ljvd;->a(I)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 407
    return-void
.end method

.method public a(Landroid/content/Context;Lhem;)V
    .locals 0

    .prologue
    .line 564
    return-void
.end method

.method public a(Landroid/content/Context;Z)V
    .locals 2

    .prologue
    .line 179
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 180
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "local_data_dirty"

    .line 181
    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 182
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 183
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 526
    invoke-virtual {p0}, Lfew;->a()Z

    move-result v2

    .line 529
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 530
    const-wide/16 v0, -0x1

    .line 532
    :try_start_0
    invoke-static {v3}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-wide v0

    .line 538
    :goto_0
    const-wide/16 v4, 0x0

    cmp-long v3, v0, v4

    if-gez v3, :cond_1

    .line 539
    const-string v2, "AllPhotosSyncManager"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 540
    const-string v2, "Ignoreing fingerprint generated for invalid media store id, uri: "

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x19

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " id: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 560
    :cond_0
    return-void

    .line 547
    :cond_1
    iget-object v0, p0, Lfew;->d:Lhei;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "logged_in"

    aput-object v4, v1, v3

    invoke-interface {v0, v1}, Lhei;->a([Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 548
    iget-object v3, p0, Lfew;->b:Landroid/content/Context;

    invoke-static {v3, v0, p1, p2}, Ljvd;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V

    .line 555
    if-eqz v2, :cond_2

    .line 556
    iget-object v3, p0, Lfew;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-static {v0}, Ljvd;->a(I)Landroid/net/Uri;

    move-result-object v0

    const/4 v4, 0x0

    invoke-virtual {v3, v0, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_1

    .line 537
    :catch_0
    move-exception v3

    goto :goto_0

    :catch_1
    move-exception v3

    goto :goto_0
.end method

.method public a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lheq;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 569
    new-instance v0, Lffa;

    invoke-direct {v0, p0}, Lffa;-><init>(Lfew;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 604
    new-instance v0, Lffb;

    invoke-direct {v0}, Lffb;-><init>()V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 619
    return-void
.end method

.method public a(Z)V
    .locals 3

    .prologue
    .line 481
    iget-object v1, p0, Lfew;->c:Lcuv;

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    new-instance v2, Lfez;

    invoke-direct {v2, p0}, Lfez;-><init>(Lfew;)V

    invoke-virtual {v1, v0, v2}, Lcuv;->a(ZLcus;)V

    .line 516
    return-void

    .line 481
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()Z
    .locals 3

    .prologue
    .line 240
    iget-object v0, p0, Lfew;->b:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 241
    const-string v1, "have_fingerprints_been_generated"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public a(Landroid/content/Context;)Z
    .locals 3

    .prologue
    .line 186
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 187
    const-string v1, "local_data_dirty"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public b()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 249
    iget-object v0, p0, Lfew;->b:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 250
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "have_fingerprints_been_generated"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 252
    iget-object v0, p0, Lfew;->d:Lhei;

    new-array v1, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "logged_in"

    aput-object v3, v1, v2

    invoke-interface {v0, v1}, Lhei;->a([Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 253
    iget-object v2, p0, Lfew;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {v0}, Ljvd;->a(I)Landroid/net/Uri;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_0

    .line 256
    :cond_0
    return-void
.end method

.method public b(I)Z
    .locals 6

    .prologue
    .line 207
    invoke-virtual {p0, p1}, Lfew;->c(I)Z

    move-result v0

    .line 208
    invoke-direct {p0, p1}, Lfew;->e(I)Lhej;

    move-result-object v1

    const-string v2, "initial_sync_complete"

    invoke-interface {v1, v2}, Lhej;->c(Ljava/lang/String;)Z

    move-result v1

    .line 209
    invoke-virtual {p0}, Lfew;->a()Z

    move-result v2

    .line 210
    const-string v3, "AllPhotosSyncManager"

    const/4 v4, 0x4

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 211
    const-string v3, "isAllPhotosTableInitialized accountId: "

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x5f

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " local media added: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " initial sync complete: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " fingerprints generated: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 218
    :cond_0
    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    if-eqz v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()V
    .locals 4

    .prologue
    .line 264
    iget-object v0, p0, Lfew;->d:Lhei;

    invoke-interface {v0}, Lhei;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 265
    iget-object v2, p0, Lfew;->d:Lhei;

    invoke-interface {v2, v0}, Lhei;->a(I)Lhej;

    move-result-object v2

    .line 266
    invoke-interface {v2}, Lhej;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "is_google_plus"

    invoke-interface {v2, v3}, Lhej;->c(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 267
    iget-object v2, p0, Lfew;->b:Landroid/content/Context;

    invoke-static {v2, v0}, Ldtd;->b(Landroid/content/Context;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 268
    new-instance v2, Ljava/lang/Thread;

    new-instance v3, Lfey;

    invoke-direct {v3, p0, v0}, Lfey;-><init>(Lfew;I)V

    const-string v0, "resume_in_progress_sync"

    invoke-direct {v2, v3, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 284
    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    goto :goto_0

    .line 288
    :cond_1
    return-void
.end method

.method public c(I)Z
    .locals 2

    .prologue
    .line 226
    invoke-direct {p0, p1}, Lfew;->e(I)Lhej;

    move-result-object v0

    const-string v1, "local_media_inserted"

    invoke-interface {v0, v1}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public d(I)V
    .locals 6

    .prologue
    .line 439
    iget-object v0, p0, Lfew;->d:Lhei;

    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 440
    new-instance v1, Landroid/accounts/Account;

    const-string v2, "com.google"

    invoke-direct {v1, v0, v2}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lfew;->b:Landroid/content/Context;

    .line 441
    invoke-static {}, Lcom/google/android/apps/photos/content/GooglePhotoDownsyncProvider;->a()Ljava/lang/String;

    move-result-object v0

    .line 440
    invoke-static {v1, v0}, Landroid/content/ContentResolver;->cancelSync(Landroid/accounts/Account;Ljava/lang/String;)V

    .line 442
    invoke-direct {p0, p1}, Lfew;->g(I)Ljava/lang/Object;

    move-result-object v1

    .line 443
    monitor-enter v1

    .line 444
    :try_start_0
    iget-object v2, p0, Lfew;->c:Lcuv;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 445
    :try_start_1
    iget-object v0, p0, Lfew;->c:Lcuv;

    invoke-virtual {v0}, Lcuv;->a()V

    .line 447
    iget-object v0, p0, Lfew;->b:Landroid/content/Context;

    invoke-static {v0, p1}, Ldtd;->a(Landroid/content/Context;I)V

    .line 448
    iget-object v0, p0, Lfew;->b:Landroid/content/Context;

    invoke-static {v0, p1}, Ljvd;->c(Landroid/content/Context;I)V

    .line 450
    invoke-direct {p0, p1}, Lfew;->f(I)Lhek;

    move-result-object v3

    .line 451
    sget-object v4, Lfew;->a:[Ljava/lang/String;

    const/4 v0, 0x0

    :goto_0
    const/4 v5, 0x4

    if-ge v0, v5, :cond_0

    aget-object v5, v4, v0

    .line 452
    invoke-interface {v3, v5}, Lhek;->e(Ljava/lang/String;)Lhek;

    .line 451
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 454
    :cond_0
    invoke-interface {v3}, Lhek;->c()I

    .line 456
    iget-object v0, p0, Lfew;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {p1}, Ljvd;->a(I)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 458
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 459
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    return-void

    .line 458
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0

    .line 459
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0
.end method
