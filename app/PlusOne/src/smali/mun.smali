.class public final Lmun;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Integer;

.field private b:Lmuq;

.field private c:[Lmup;

.field private d:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14295
    invoke-direct {p0}, Loxq;-><init>()V

    .line 14604
    const/4 v0, 0x0

    iput-object v0, p0, Lmun;->b:Lmuq;

    .line 14607
    sget-object v0, Lmup;->a:[Lmup;

    iput-object v0, p0, Lmun;->c:[Lmup;

    .line 14295
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 14636
    .line 14637
    iget-object v0, p0, Lmun;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 14638
    const/4 v0, 0x1

    iget-object v2, p0, Lmun;->a:Ljava/lang/Integer;

    .line 14639
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 14641
    :goto_0
    iget-object v2, p0, Lmun;->b:Lmuq;

    if-eqz v2, :cond_0

    .line 14642
    const/4 v2, 0x2

    iget-object v3, p0, Lmun;->b:Lmuq;

    .line 14643
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 14645
    :cond_0
    iget-object v2, p0, Lmun;->c:[Lmup;

    if-eqz v2, :cond_2

    .line 14646
    iget-object v2, p0, Lmun;->c:[Lmup;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 14647
    if-eqz v4, :cond_1

    .line 14648
    const/4 v5, 0x3

    .line 14649
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 14646
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 14653
    :cond_2
    iget-object v1, p0, Lmun;->d:Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 14654
    const/4 v1, 0x4

    iget-object v2, p0, Lmun;->d:Ljava/lang/Long;

    .line 14655
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 14657
    :cond_3
    iget-object v1, p0, Lmun;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 14658
    iput v0, p0, Lmun;->ai:I

    .line 14659
    return v0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lmun;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 14667
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 14668
    sparse-switch v0, :sswitch_data_0

    .line 14672
    iget-object v2, p0, Lmun;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 14673
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmun;->ah:Ljava/util/List;

    .line 14676
    :cond_1
    iget-object v2, p0, Lmun;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 14678
    :sswitch_0
    return-object p0

    .line 14683
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmun;->a:Ljava/lang/Integer;

    goto :goto_0

    .line 14687
    :sswitch_2
    iget-object v0, p0, Lmun;->b:Lmuq;

    if-nez v0, :cond_2

    .line 14688
    new-instance v0, Lmuq;

    invoke-direct {v0}, Lmuq;-><init>()V

    iput-object v0, p0, Lmun;->b:Lmuq;

    .line 14690
    :cond_2
    iget-object v0, p0, Lmun;->b:Lmuq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 14694
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 14695
    iget-object v0, p0, Lmun;->c:[Lmup;

    if-nez v0, :cond_4

    move v0, v1

    .line 14696
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lmup;

    .line 14697
    iget-object v3, p0, Lmun;->c:[Lmup;

    if-eqz v3, :cond_3

    .line 14698
    iget-object v3, p0, Lmun;->c:[Lmup;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 14700
    :cond_3
    iput-object v2, p0, Lmun;->c:[Lmup;

    .line 14701
    :goto_2
    iget-object v2, p0, Lmun;->c:[Lmup;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 14702
    iget-object v2, p0, Lmun;->c:[Lmup;

    new-instance v3, Lmup;

    invoke-direct {v3}, Lmup;-><init>()V

    aput-object v3, v2, v0

    .line 14703
    iget-object v2, p0, Lmun;->c:[Lmup;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 14704
    invoke-virtual {p1}, Loxn;->a()I

    .line 14701
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 14695
    :cond_4
    iget-object v0, p0, Lmun;->c:[Lmup;

    array-length v0, v0

    goto :goto_1

    .line 14707
    :cond_5
    iget-object v2, p0, Lmun;->c:[Lmup;

    new-instance v3, Lmup;

    invoke-direct {v3}, Lmup;-><init>()V

    aput-object v3, v2, v0

    .line 14708
    iget-object v2, p0, Lmun;->c:[Lmup;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 14712
    :sswitch_4
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lmun;->d:Ljava/lang/Long;

    goto/16 :goto_0

    .line 14668
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 14614
    iget-object v0, p0, Lmun;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 14615
    const/4 v0, 0x1

    iget-object v1, p0, Lmun;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 14617
    :cond_0
    iget-object v0, p0, Lmun;->b:Lmuq;

    if-eqz v0, :cond_1

    .line 14618
    const/4 v0, 0x2

    iget-object v1, p0, Lmun;->b:Lmuq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 14620
    :cond_1
    iget-object v0, p0, Lmun;->c:[Lmup;

    if-eqz v0, :cond_3

    .line 14621
    iget-object v1, p0, Lmun;->c:[Lmup;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 14622
    if-eqz v3, :cond_2

    .line 14623
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 14621
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 14627
    :cond_3
    iget-object v0, p0, Lmun;->d:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 14628
    const/4 v0, 0x4

    iget-object v1, p0, Lmun;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 14630
    :cond_4
    iget-object v0, p0, Lmun;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 14632
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 14291
    invoke-virtual {p0, p1}, Lmun;->a(Loxn;)Lmun;

    move-result-object v0

    return-object v0
.end method
