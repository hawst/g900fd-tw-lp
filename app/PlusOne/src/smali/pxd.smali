.class public final Lpxd;
.super Loxq;
.source "PG"


# static fields
.field public static final a:Loxr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Loxr",
            "<",
            "Lpxd;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:Lpxc;

.field private c:Lpxc;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 138
    const v0, 0x2e575b2

    new-instance v1, Lpxe;

    invoke-direct {v1}, Lpxe;-><init>()V

    .line 143
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    move-result-object v0

    sput-object v0, Lpxd;->a:Loxr;

    .line 142
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 166
    const/4 v0, 0x0

    .line 167
    iget-object v1, p0, Lpxd;->b:Lpxc;

    if-eqz v1, :cond_0

    .line 168
    const/4 v0, 0x1

    iget-object v1, p0, Lpxd;->b:Lpxc;

    .line 169
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 171
    :cond_0
    iget-object v1, p0, Lpxd;->c:Lpxc;

    if-eqz v1, :cond_1

    .line 172
    const/4 v1, 0x2

    iget-object v2, p0, Lpxd;->c:Lpxc;

    .line 173
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 175
    :cond_1
    iget-object v1, p0, Lpxd;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 176
    iput v0, p0, Lpxd;->ai:I

    .line 177
    return v0
.end method

.method public a(Loxn;)Lpxd;
    .locals 2

    .prologue
    .line 185
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 186
    sparse-switch v0, :sswitch_data_0

    .line 190
    iget-object v1, p0, Lpxd;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 191
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpxd;->ah:Ljava/util/List;

    .line 194
    :cond_1
    iget-object v1, p0, Lpxd;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 196
    :sswitch_0
    return-object p0

    .line 201
    :sswitch_1
    iget-object v0, p0, Lpxd;->b:Lpxc;

    if-nez v0, :cond_2

    .line 202
    new-instance v0, Lpxc;

    invoke-direct {v0}, Lpxc;-><init>()V

    iput-object v0, p0, Lpxd;->b:Lpxc;

    .line 204
    :cond_2
    iget-object v0, p0, Lpxd;->b:Lpxc;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 208
    :sswitch_2
    iget-object v0, p0, Lpxd;->c:Lpxc;

    if-nez v0, :cond_3

    .line 209
    new-instance v0, Lpxc;

    invoke-direct {v0}, Lpxc;-><init>()V

    iput-object v0, p0, Lpxd;->c:Lpxc;

    .line 211
    :cond_3
    iget-object v0, p0, Lpxd;->c:Lpxc;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 186
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 154
    iget-object v0, p0, Lpxd;->b:Lpxc;

    if-eqz v0, :cond_0

    .line 155
    const/4 v0, 0x1

    iget-object v1, p0, Lpxd;->b:Lpxc;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 157
    :cond_0
    iget-object v0, p0, Lpxd;->c:Lpxc;

    if-eqz v0, :cond_1

    .line 158
    const/4 v0, 0x2

    iget-object v1, p0, Lpxd;->c:Lpxc;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 160
    :cond_1
    iget-object v0, p0, Lpxd;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 162
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 135
    invoke-virtual {p0, p1}, Lpxd;->a(Loxn;)Lpxd;

    move-result-object v0

    return-object v0
.end method
