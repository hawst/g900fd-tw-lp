.class public final Lfpx;
.super Lt;
.source "PG"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "NewApi"
    }
.end annotation


# instance fields
.field private N:Landroid/widget/EditText;

.field private O:Landroid/os/Bundle;

.field private P:Landroid/app/AlertDialog;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lt;-><init>()V

    .line 131
    return-void
.end method

.method static synthetic a(Lfpx;)V
    .locals 4

    .prologue
    .line 33
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "text"

    iget-object v2, p0, Lfpx;->N:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "passthrough_data"

    iget-object v2, p0, Lfpx;->O:Landroid/os/Bundle;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    invoke-virtual {p0}, Lfpx;->u_()Lu;

    move-result-object v1

    invoke-virtual {p0}, Lfpx;->v_()I

    move-result v2

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3, v0}, Lu;->a(IILandroid/content/Intent;)V

    invoke-virtual {p0}, Lfpx;->a()V

    return-void
.end method

.method static synthetic b(Lfpx;)Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lfpx;->P:Landroid/app/AlertDialog;

    return-object v0
.end method


# virtual methods
.method public c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 52
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lfpx;->n()Lz;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 53
    invoke-virtual {p0}, Lfpx;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 54
    const v2, 0x7f040209

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 55
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0a0579

    .line 56
    invoke-virtual {p0, v3}, Lfpx;->e_(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lfpz;

    invoke-direct {v4, p0}, Lfpz;-><init>(Lfpx;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0a0597

    .line 62
    invoke-virtual {p0, v3}, Lfpx;->e_(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lfpy;

    invoke-direct {v4, p0}, Lfpy;-><init>(Lfpx;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 71
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lfpx;->P:Landroid/app/AlertDialog;

    .line 72
    invoke-virtual {p0}, Lfpx;->k()Landroid/os/Bundle;

    move-result-object v1

    .line 73
    iget-object v2, p0, Lfpx;->P:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 75
    const-string v2, "passthrough_data"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    iput-object v2, p0, Lfpx;->O:Landroid/os/Bundle;

    .line 76
    const v2, 0x7f1005e0

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lfpx;->N:Landroid/widget/EditText;

    .line 77
    const-string v0, "text"

    const-string v2, ""

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 78
    iget-object v2, p0, Lfpx;->N:Landroid/widget/EditText;

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 79
    iget-object v2, p0, Lfpx;->N:Landroid/widget/EditText;

    invoke-virtual {p0}, Lfpx;->o()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c0027

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setLines(I)V

    .line 80
    iget-object v2, p0, Lfpx;->N:Landroid/widget/EditText;

    invoke-virtual {v2, v6}, Landroid/widget/EditText;->setHorizontallyScrolling(Z)V

    .line 81
    iget-object v2, p0, Lfpx;->N:Landroid/widget/EditText;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setSelection(I)V

    .line 82
    iget-object v0, p0, Lfpx;->N:Landroid/widget/EditText;

    const-string v2, "placeholder"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 83
    iget-object v0, p0, Lfpx;->N:Landroid/widget/EditText;

    const/4 v2, 0x2

    new-array v2, v2, [Landroid/text/InputFilter;

    new-instance v3, Landroid/text/InputFilter$LengthFilter;

    const-string v4, "max_length"

    const/16 v5, 0x1f4

    .line 84
    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    invoke-direct {v3, v4}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v3, v2, v6

    new-instance v3, Lfqc;

    invoke-direct {v3}, Lfqc;-><init>()V

    aput-object v3, v2, v7

    .line 83
    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 88
    const-string v0, "allow_empty"

    invoke-virtual {v1, v0, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 89
    iget-object v1, p0, Lfpx;->N:Landroid/widget/EditText;

    new-instance v2, Lfqa;

    invoke-direct {v2, p0, v0}, Lfqa;-><init>(Lfpx;Z)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 106
    iget-object v0, p0, Lfpx;->N:Landroid/widget/EditText;

    new-instance v1, Lfqb;

    invoke-direct {v1, p0}, Lfqb;-><init>(Lfpx;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 116
    iget-object v0, p0, Lfpx;->P:Landroid/app/AlertDialog;

    return-object v0
.end method
