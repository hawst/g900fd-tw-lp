.class public Lhgy;
.super Landroid/widget/FrameLayout;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lbc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/FrameLayout;",
        "Landroid/view/View$OnClickListener;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# static fields
.field private static synthetic h:Z


# instance fields
.field private a:Z

.field public final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lhgw;",
            ">;"
        }
    .end annotation
.end field

.field public c:Landroid/view/ViewGroup;

.field public d:I

.field public e:Ljava/lang/Runnable;

.field public f:Z

.field public g:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-class v0, Lhgy;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lhgy;->h:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 136
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lhgy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 137
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 146
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lhgy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 147
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 157
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lhgy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IZ)V

    .line 158
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IZ)V
    .locals 1

    .prologue
    .line 169
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 59
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lhgy;->b:Ljava/util/ArrayList;

    .line 61
    const/4 v0, -0x1

    iput v0, p0, Lhgy;->d:I

    .line 174
    invoke-virtual {p0}, Lhgy;->a()V

    .line 170
    iput-boolean p4, p0, Lhgy;->a:Z

    .line 171
    return-void
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 555
    const v0, 0x7f10005f

    if-ne p1, v0, :cond_0

    .line 556
    new-instance v0, Lhxg;

    invoke-virtual {p0}, Lhgy;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lhgy;->d:I

    const/4 v3, 0x5

    sget-object v4, Lhgz;->a:[Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Lhxg;-><init>(Landroid/content/Context;II[Ljava/lang/String;)V

    return-object v0

    .line 559
    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 181
    const v0, 0x7f04004c

    invoke-virtual {p0, v0}, Lhgy;->d(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lhgy;->addView(Landroid/view/View;)V

    .line 183
    const v0, 0x7f1001a4

    invoke-virtual {p0, v0}, Lhgy;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lhgy;->c:Landroid/view/ViewGroup;

    .line 184
    return-void
.end method

.method public a(IIILjava/lang/String;Ljava/lang/Object;Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 464
    invoke-virtual {p0}, Lhgy;->g()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-le p1, v0, :cond_0

    .line 465
    invoke-virtual {p0, p1}, Lhgy;->e(I)V

    .line 468
    :cond_0
    iget-object v0, p0, Lhgy;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 469
    invoke-virtual {v0, p2, v4, p3, v4}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 470
    invoke-virtual {v0, p4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 471
    instance-of v2, p5, Lhxc;

    .line 472
    if-eqz p6, :cond_3

    const v1, 0x7f02003c

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 475
    iget-boolean v1, p0, Lhgy;->a:Z

    if-eqz v1, :cond_1

    .line 476
    if-eqz v2, :cond_5

    const v1, 0x7f0a054a

    .line 479
    :goto_1
    invoke-virtual {p0}, Lhgy;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p4, v3, v4

    invoke-virtual {v2, v1, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 482
    :cond_1
    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_2

    .line 483
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 486
    :cond_2
    invoke-virtual {v0, p5}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 487
    return-void

    .line 472
    :cond_3
    if-eqz v2, :cond_4

    move-object v1, p5

    check-cast v1, Lhxc;

    .line 473
    :goto_2
    invoke-virtual {p0, v1}, Lhgy;->b(Lhxc;)I

    move-result v1

    goto :goto_0

    .line 472
    :cond_4
    const/4 v1, 0x0

    goto :goto_2

    .line 476
    :cond_5
    const v1, 0x7f0a054b

    goto :goto_1
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 608
    return-void
.end method

.method public a(Ldo;Landroid/database/Cursor;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    const/4 v1, 0x0

    .line 565
    invoke-virtual {p1}, Ldo;->o()I

    move-result v0

    .line 566
    const v2, 0x7f10005f

    if-ne v0, v2, :cond_8

    .line 567
    if-eqz p2, :cond_7

    .line 568
    sget-boolean v0, Lhgy;->h:Z

    if-nez v0, :cond_0

    sget-object v0, Lhgz;->a:[Ljava/lang/String;

    invoke-interface {p2}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lhgy;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v3}, Ljava/util/ArrayList;-><init>(I)V

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_5

    iget-object v0, p0, Lhgy;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhgw;

    invoke-virtual {v0}, Lhgw;->h()I

    move-result v5

    if-ne v5, v9, :cond_3

    invoke-virtual {v0, v1}, Lhgw;->b(I)Lhxc;

    move-result-object v5

    move v0, v1

    :goto_1
    invoke-interface {p2, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v5}, Lhxc;->a()Ljava/lang/String;

    move-result-object v6

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    new-instance v0, Lhxc;

    invoke-virtual {v5}, Lhxc;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5}, Lhxc;->c()I

    move-result v7

    invoke-interface {p2, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5}, Lhxc;->d()I

    move-result v5

    invoke-direct {v0, v6, v7, v8, v5}, Lhxc;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    new-instance v5, Lhgw;

    invoke-direct {v5, v0}, Lhgw;-><init>(Lhxc;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    invoke-virtual {v0}, Lhgw;->g()I

    move-result v5

    if-eq v5, v9, :cond_4

    invoke-virtual {v0}, Lhgw;->i()I

    move-result v5

    if-eq v5, v9, :cond_4

    invoke-virtual {v0}, Lhgw;->j()I

    move-result v5

    if-ne v5, v9, :cond_1

    :cond_4
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lhgy;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :goto_3
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    iget-object v0, p0, Lhgy;->b:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_6
    invoke-virtual {p0}, Lhgy;->b()V

    .line 573
    :cond_7
    return-void

    .line 571
    :cond_8
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 44
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lhgy;->a(Ldo;Landroid/database/Cursor;)V

    return-void
.end method

.method public a(Lhgw;)V
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 247
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhgy;->f:Z

    .line 248
    new-instance v3, Ljava/util/ArrayList;

    iget-object v0, p0, Lhgy;->b:Ljava/util/ArrayList;

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p0}, Lhgy;->h()Lhgw;

    move-result-object v0

    invoke-virtual {v0}, Lhgw;->b()[Lhxc;

    move-result-object v4

    invoke-virtual {v0}, Lhgw;->a()[Ljqs;

    move-result-object v5

    invoke-virtual {v0}, Lhgw;->c()[Lkxr;

    move-result-object v6

    invoke-virtual {v0}, Lhgw;->d()[Lhxs;

    move-result-object v7

    iget-object v0, p0, Lhgy;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    if-eqz p1, :cond_9

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v8

    move v2, v1

    :goto_0
    if-ge v2, v8, :cond_1

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhgw;

    invoke-virtual {p1, v0}, Lhgw;->b(Lhgw;)Z

    move-result v9

    if-eqz v9, :cond_0

    iget-object v9, p0, Lhgy;->b:Ljava/util/ArrayList;

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lhgw;->b()[Lhxc;

    move-result-object v2

    array-length v3, v2

    move v0, v1

    :goto_1
    if-ge v0, v3, :cond_3

    aget-object v8, v2, v0

    invoke-virtual {v8, v4}, Lhxc;->a([Lhxc;)Z

    move-result v9

    if-nez v9, :cond_2

    iget-object v9, p0, Lhgy;->b:Ljava/util/ArrayList;

    new-instance v10, Lhgw;

    invoke-direct {v10, v8}, Lhgw;-><init>(Lhxc;)V

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lhgw;->a()[Ljqs;

    move-result-object v2

    array-length v3, v2

    move v0, v1

    :goto_2
    if-ge v0, v3, :cond_5

    aget-object v4, v2, v0

    invoke-virtual {v4, v5}, Ljqs;->a([Ljqs;)Z

    move-result v8

    if-nez v8, :cond_4

    iget-object v8, p0, Lhgy;->b:Ljava/util/ArrayList;

    new-instance v9, Lhgw;

    invoke-direct {v9, v4}, Lhgw;-><init>(Ljqs;)V

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    invoke-virtual {p1}, Lhgw;->c()[Lkxr;

    move-result-object v2

    array-length v3, v2

    move v0, v1

    :goto_3
    if-ge v0, v3, :cond_7

    aget-object v4, v2, v0

    invoke-virtual {v4, v6}, Lkxr;->a([Lkxr;)Z

    move-result v5

    if-nez v5, :cond_6

    iget-object v5, p0, Lhgy;->b:Ljava/util/ArrayList;

    new-instance v8, Lhgw;

    invoke-direct {v8, v4}, Lhgw;-><init>(Lkxr;)V

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_7
    invoke-virtual {p1}, Lhgw;->d()[Lhxs;

    move-result-object v2

    array-length v3, v2

    move v0, v1

    :goto_4
    if-ge v0, v3, :cond_9

    aget-object v1, v2, v0

    invoke-virtual {v1, v7}, Lhxs;->a([Lhxs;)Z

    move-result v4

    if-nez v4, :cond_8

    iget-object v4, p0, Lhgy;->b:Ljava/util/ArrayList;

    new-instance v5, Lhgw;

    invoke-direct {v5, v1}, Lhgw;-><init>(Lhxs;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_9
    invoke-virtual {p0}, Lhgy;->b()V

    .line 249
    return-void
.end method

.method public a(Lhxc;)V
    .locals 4

    .prologue
    .line 324
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhgy;->f:Z

    .line 325
    invoke-virtual {p0}, Lhgy;->h()Lhgw;

    move-result-object v0

    invoke-virtual {v0}, Lhgw;->b()[Lhxc;

    move-result-object v0

    .line 326
    invoke-virtual {p1, v0}, Lhxc;->a([Lhxc;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 339
    :goto_0
    return-void

    .line 330
    :cond_0
    invoke-virtual {p0}, Lhgy;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 331
    iget v2, p0, Lhgy;->d:I

    .line 332
    const-class v0, Lhms;

    invoke-static {v1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v3, Lhmr;

    invoke-direct {v3, v1, v2}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v1, Lhmv;->dE:Lhmv;

    .line 334
    invoke-virtual {v3, v1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 332
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 336
    iget-object v0, p0, Lhgy;->b:Ljava/util/ArrayList;

    new-instance v1, Lhgw;

    invoke-direct {v1, p1}, Lhgw;-><init>(Lhxc;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 338
    invoke-virtual {p0}, Lhgy;->b()V

    goto :goto_0
.end method

.method public a(Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 222
    iput-object p1, p0, Lhgy;->e:Ljava/lang/Runnable;

    .line 223
    return-void
.end method

.method public a(Ljqs;)V
    .locals 4

    .prologue
    .line 347
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhgy;->f:Z

    .line 348
    invoke-virtual {p0}, Lhgy;->h()Lhgw;

    move-result-object v0

    invoke-virtual {v0}, Lhgw;->a()[Ljqs;

    move-result-object v0

    .line 349
    invoke-virtual {p1, v0}, Ljqs;->a([Ljqs;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 361
    :goto_0
    return-void

    .line 353
    :cond_0
    invoke-virtual {p0}, Lhgy;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 354
    iget v2, p0, Lhgy;->d:I

    .line 355
    const-class v0, Lhms;

    invoke-static {v1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v3, Lhmr;

    invoke-direct {v3, v1, v2}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v1, Lhmv;->B:Lhmv;

    .line 357
    invoke-virtual {v3, v1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 355
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 359
    iget-object v0, p0, Lhgy;->b:Ljava/util/ArrayList;

    new-instance v1, Lhgw;

    invoke-direct {v1, p1}, Lhgw;-><init>(Ljqs;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 360
    invoke-virtual {p0}, Lhgy;->b()V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 637
    iput-boolean p1, p0, Lhgy;->g:Z

    .line 638
    invoke-virtual {p0}, Lhgy;->b()V

    .line 639
    return-void
.end method

.method protected b(Lhxc;)I
    .locals 2

    .prologue
    const v0, 0x7f020035

    .line 519
    if-eqz p1, :cond_0

    .line 520
    invoke-virtual {p1}, Lhxc;->c()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 529
    :cond_0
    :goto_0
    return v0

    .line 524
    :pswitch_0
    const v0, 0x7f020036

    goto :goto_0

    .line 520
    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public b()V
    .locals 13

    .prologue
    .line 389
    const/4 v1, 0x0

    .line 391
    iget-object v0, p0, Lhgy;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lhgw;

    .line 392
    sget-boolean v0, Lhgy;->h:Z

    if-nez v0, :cond_1

    invoke-virtual {v7}, Lhgw;->h()I

    move-result v0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_1

    invoke-virtual {v7}, Lhgw;->g()I

    move-result v0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_1

    .line 393
    invoke-virtual {v7}, Lhgw;->i()I

    move-result v0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_1

    invoke-virtual {v7}, Lhgw;->j()I

    move-result v0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_1

    .line 392
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 395
    :cond_1
    invoke-virtual {v7}, Lhgw;->b()[Lhxc;

    move-result-object v11

    array-length v12, v11

    const/4 v0, 0x0

    move v8, v0

    :goto_0
    if-ge v8, v12, :cond_3

    aget-object v5, v11, v8

    .line 397
    invoke-virtual {p0}, Lhgy;->getContext()Landroid/content/Context;

    move-result-object v0

    iget v2, p0, Lhgy;->d:I

    .line 399
    invoke-virtual {v5}, Lhxc;->c()I

    move-result v3

    .line 396
    invoke-static {v0, v2, v3}, Lhxm;->a(Landroid/content/Context;II)Z

    move-result v6

    .line 400
    invoke-virtual {v5}, Lhxc;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {v5}, Lhxc;->b()Ljava/lang/String;

    move-result-object v4

    .line 402
    :goto_1
    add-int/lit8 v9, v1, 0x1

    invoke-virtual {v5}, Lhxc;->c()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const v2, 0x7f020185

    :goto_2
    invoke-virtual {p0}, Lhgy;->i()I

    move-result v3

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lhgy;->a(IIILjava/lang/String;Ljava/lang/Object;Z)V

    .line 395
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    move v1, v9

    goto :goto_0

    .line 401
    :cond_2
    invoke-virtual {p0}, Lhgy;->getContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0a057b

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 402
    :pswitch_0
    const v2, 0x7f020315

    goto :goto_2

    :pswitch_1
    const v2, 0x7f0201ce

    goto :goto_2

    :pswitch_2
    const v2, 0x7f0201ac

    goto :goto_2

    .line 406
    :cond_3
    invoke-virtual {v7}, Lhgw;->a()[Ljqs;

    move-result-object v9

    array-length v11, v9

    const/4 v0, 0x0

    move v7, v0

    :goto_3
    if-ge v7, v11, :cond_0

    aget-object v5, v9, v7

    .line 407
    invoke-virtual {v5}, Ljqs;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {v5}, Ljqs;->b()Ljava/lang/String;

    move-result-object v4

    .line 410
    :goto_4
    add-int/lit8 v8, v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lhgy;->i()I

    move-result v3

    const/4 v6, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lhgy;->a(IIILjava/lang/String;Ljava/lang/Object;Z)V

    .line 406
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    move v1, v8

    goto :goto_3

    .line 408
    :cond_4
    invoke-virtual {v5}, Ljqs;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {v5}, Ljqs;->c()Ljava/lang/String;

    move-result-object v4

    goto :goto_4

    .line 409
    :cond_5
    invoke-virtual {p0}, Lhgy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x104000e

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_4

    .line 414
    :cond_6
    invoke-virtual {p0}, Lhgy;->g()I

    move-result v0

    .line 415
    :goto_5
    if-ge v1, v0, :cond_7

    .line 416
    iget-object v2, p0, Lhgy;->c:Landroid/view/ViewGroup;

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 417
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 421
    :cond_7
    iget-object v0, p0, Lhgy;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_6
    if-ltz v0, :cond_9

    .line 422
    iget-object v1, p0, Lhgy;->c:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 423
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-ne v2, v3, :cond_8

    .line 424
    iget-object v2, p0, Lhgy;->c:Landroid/view/ViewGroup;

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 421
    :cond_8
    add-int/lit8 v0, v0, -0x1

    goto :goto_6

    .line 428
    :cond_9
    iget-object v0, p0, Lhgy;->e:Ljava/lang/Runnable;

    if-eqz v0, :cond_a

    .line 429
    iget-object v0, p0, Lhgy;->e:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 431
    :cond_a
    return-void

    .line 402
    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public b(Ljqs;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 369
    iput-boolean v4, p0, Lhgy;->f:Z

    .line 370
    const/4 v1, 0x0

    .line 371
    iget-object v0, p0, Lhgy;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhgw;

    .line 372
    invoke-virtual {v0}, Lhgw;->g()I

    move-result v3

    if-ne v3, v4, :cond_0

    invoke-virtual {v0}, Lhgw;->h()I

    move-result v3

    if-nez v3, :cond_0

    const/4 v3, 0x0

    .line 373
    invoke-virtual {v0, v3}, Lhgw;->a(I)Ljqs;

    move-result-object v3

    invoke-static {v3, p1}, Ljqs;->a(Ljqs;Ljqs;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 379
    :goto_0
    if-eqz v0, :cond_1

    .line 380
    iget-object v1, p0, Lhgy;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 381
    invoke-virtual {p0}, Lhgy;->b()V

    .line 383
    :cond_1
    return-void

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method public c(I)V
    .locals 0

    .prologue
    .line 187
    iput p1, p0, Lhgy;->d:I

    .line 188
    return-void
.end method

.method public d(I)Landroid/view/View;
    .locals 2

    .prologue
    .line 510
    invoke-virtual {p0}, Lhgy;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public e(I)V
    .locals 2

    .prologue
    .line 538
    const v0, 0x7f040160

    invoke-virtual {p0, v0}, Lhgy;->d(I)Landroid/view/View;

    move-result-object v0

    .line 539
    iget-boolean v1, p0, Lhgy;->a:Z

    if-eqz v1, :cond_0

    .line 540
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 542
    :cond_0
    iget-object v1, p0, Lhgy;->c:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 543
    return-void
.end method

.method public g()I
    .locals 1

    .prologue
    .line 449
    iget-object v0, p0, Lhgy;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    return v0
.end method

.method public h()Lhgw;
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Lhgy;->b:Ljava/util/ArrayList;

    invoke-static {v0}, Lhgw;->a(Ljava/lang/Iterable;)Lhgw;

    move-result-object v0

    return-object v0
.end method

.method protected i()I
    .locals 1

    .prologue
    .line 438
    iget-boolean v0, p0, Lhgy;->a:Z

    if-eqz v0, :cond_0

    .line 439
    const v0, 0x7f020147

    .line 441
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public j()V
    .locals 2

    .prologue
    .line 493
    iget-object v0, p0, Lhgy;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 500
    :goto_0
    return-void

    .line 496
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhgy;->f:Z

    .line 497
    iget-object v0, p0, Lhgy;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 498
    iget-object v1, p0, Lhgy;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 499
    invoke-virtual {p0}, Lhgy;->b()V

    goto :goto_0
.end method

.method public k()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 646
    iput-boolean v5, p0, Lhgy;->f:Z

    .line 647
    const/4 v1, 0x0

    .line 648
    iget-object v0, p0, Lhgy;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhgw;

    .line 649
    invoke-virtual {v0}, Lhgw;->g()I

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v0}, Lhgw;->h()I

    move-result v3

    if-ne v3, v5, :cond_0

    const/4 v3, 0x0

    .line 650
    invoke-virtual {v0, v3}, Lhgw;->b(I)Lhxc;

    move-result-object v3

    invoke-virtual {v3}, Lhxc;->c()I

    move-result v3

    const/16 v4, 0x9

    if-ne v3, v4, :cond_0

    .line 656
    :goto_0
    if-eqz v0, :cond_1

    .line 657
    iget-object v1, p0, Lhgy;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 658
    invoke-virtual {p0}, Lhgy;->b()V

    .line 660
    :cond_1
    return-void

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 619
    iget-boolean v0, p0, Lhgy;->a:Z

    if-nez v0, :cond_1

    .line 634
    :cond_0
    :goto_0
    return-void

    .line 622
    :cond_1
    invoke-virtual {p0}, Lhgy;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 623
    iget v2, p0, Lhgy;->d:I

    .line 624
    const-class v0, Lhms;

    invoke-static {v1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v3, Lhmr;

    invoke-direct {v3, v1, v2}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v1, Lhmv;->dH:Lhmv;

    .line 626
    invoke-virtual {v3, v1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 624
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 628
    iget-object v0, p0, Lhgy;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v0

    .line 629
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 630
    const/4 v1, 0x1

    iput-boolean v1, p0, Lhgy;->f:Z

    .line 631
    iget-object v1, p0, Lhgy;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 632
    invoke-virtual {p0}, Lhgy;->b()V

    goto :goto_0
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 204
    check-cast p1, Lhha;

    .line 205
    invoke-virtual {p1}, Lhha;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 207
    iget-object v0, p0, Lhgy;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 208
    iget-object v0, p0, Lhgy;->b:Ljava/util/ArrayList;

    iget-object v1, p1, Lhha;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 210
    iget-boolean v0, p1, Lhha;->b:Z

    iput-boolean v0, p0, Lhgy;->f:Z

    .line 211
    iget-boolean v0, p1, Lhha;->c:Z

    iput-boolean v0, p0, Lhgy;->g:Z

    .line 213
    invoke-virtual {p0}, Lhgy;->b()V

    .line 214
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 192
    invoke-super {p0}, Landroid/widget/FrameLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 193
    new-instance v1, Lhha;

    invoke-direct {v1, v0}, Lhha;-><init>(Landroid/os/Parcelable;)V

    .line 195
    iget-object v0, p0, Lhgy;->b:Ljava/util/ArrayList;

    iput-object v0, v1, Lhha;->a:Ljava/util/ArrayList;

    .line 196
    iget-boolean v0, p0, Lhgy;->f:Z

    iput-boolean v0, v1, Lhha;->b:Z

    .line 197
    iget-boolean v0, p0, Lhgy;->g:Z

    iput-boolean v0, v1, Lhha;->c:Z

    .line 199
    return-object v1
.end method
