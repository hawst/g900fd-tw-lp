.class public final Lncn;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnym;

.field public b:Lnyt;

.field private c:Ljava/lang/Boolean;

.field private d:Lncp;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 836
    invoke-direct {p0}, Loxq;-><init>()V

    .line 839
    iput-object v0, p0, Lncn;->a:Lnym;

    .line 844
    iput-object v0, p0, Lncn;->d:Lncp;

    .line 847
    iput-object v0, p0, Lncn;->b:Lnyt;

    .line 836
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 870
    const/4 v0, 0x0

    .line 871
    iget-object v1, p0, Lncn;->a:Lnym;

    if-eqz v1, :cond_0

    .line 872
    const/4 v0, 0x1

    iget-object v1, p0, Lncn;->a:Lnym;

    .line 873
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 875
    :cond_0
    iget-object v1, p0, Lncn;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 876
    const/4 v1, 0x2

    iget-object v2, p0, Lncn;->c:Ljava/lang/Boolean;

    .line 877
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 879
    :cond_1
    iget-object v1, p0, Lncn;->d:Lncp;

    if-eqz v1, :cond_2

    .line 880
    const/4 v1, 0x3

    iget-object v2, p0, Lncn;->d:Lncp;

    .line 881
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 883
    :cond_2
    iget-object v1, p0, Lncn;->b:Lnyt;

    if-eqz v1, :cond_3

    .line 884
    const/4 v1, 0x4

    iget-object v2, p0, Lncn;->b:Lnyt;

    .line 885
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 887
    :cond_3
    iget-object v1, p0, Lncn;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 888
    iput v0, p0, Lncn;->ai:I

    .line 889
    return v0
.end method

.method public a(Loxn;)Lncn;
    .locals 2

    .prologue
    .line 897
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 898
    sparse-switch v0, :sswitch_data_0

    .line 902
    iget-object v1, p0, Lncn;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 903
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lncn;->ah:Ljava/util/List;

    .line 906
    :cond_1
    iget-object v1, p0, Lncn;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 908
    :sswitch_0
    return-object p0

    .line 913
    :sswitch_1
    iget-object v0, p0, Lncn;->a:Lnym;

    if-nez v0, :cond_2

    .line 914
    new-instance v0, Lnym;

    invoke-direct {v0}, Lnym;-><init>()V

    iput-object v0, p0, Lncn;->a:Lnym;

    .line 916
    :cond_2
    iget-object v0, p0, Lncn;->a:Lnym;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 920
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lncn;->c:Ljava/lang/Boolean;

    goto :goto_0

    .line 924
    :sswitch_3
    iget-object v0, p0, Lncn;->d:Lncp;

    if-nez v0, :cond_3

    .line 925
    new-instance v0, Lncp;

    invoke-direct {v0}, Lncp;-><init>()V

    iput-object v0, p0, Lncn;->d:Lncp;

    .line 927
    :cond_3
    iget-object v0, p0, Lncn;->d:Lncp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 931
    :sswitch_4
    iget-object v0, p0, Lncn;->b:Lnyt;

    if-nez v0, :cond_4

    .line 932
    new-instance v0, Lnyt;

    invoke-direct {v0}, Lnyt;-><init>()V

    iput-object v0, p0, Lncn;->b:Lnyt;

    .line 934
    :cond_4
    iget-object v0, p0, Lncn;->b:Lnyt;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 898
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 852
    iget-object v0, p0, Lncn;->a:Lnym;

    if-eqz v0, :cond_0

    .line 853
    const/4 v0, 0x1

    iget-object v1, p0, Lncn;->a:Lnym;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 855
    :cond_0
    iget-object v0, p0, Lncn;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 856
    const/4 v0, 0x2

    iget-object v1, p0, Lncn;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 858
    :cond_1
    iget-object v0, p0, Lncn;->d:Lncp;

    if-eqz v0, :cond_2

    .line 859
    const/4 v0, 0x3

    iget-object v1, p0, Lncn;->d:Lncp;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 861
    :cond_2
    iget-object v0, p0, Lncn;->b:Lnyt;

    if-eqz v0, :cond_3

    .line 862
    const/4 v0, 0x4

    iget-object v1, p0, Lncn;->b:Lnyt;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 864
    :cond_3
    iget-object v0, p0, Lncn;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 866
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 832
    invoke-virtual {p0, p1}, Lncn;->a(Loxn;)Lncn;

    move-result-object v0

    return-object v0
.end method
