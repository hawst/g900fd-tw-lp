.class final Ldas;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private synthetic a:Ldaq;


# direct methods
.method constructor <init>(Ldaq;)V
    .locals 0

    .prologue
    .line 78
    iput-object p1, p0, Ldas;->a:Ldaq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 80
    const-string v0, "account_id"

    const/4 v1, -0x1

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 82
    const-string v1, "view_id"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 83
    new-instance v2, Lemo;

    iget-object v3, p0, Ldas;->a:Ldaq;

    invoke-virtual {v3}, Ldaq;->n()Lz;

    move-result-object v3

    iget-object v4, p0, Ldas;->a:Ldaq;

    invoke-static {v4}, Ldaq;->a(Ldaq;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v0, v1, v4}, Lemo;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V

    return-object v2
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v13, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 88
    iget-object v0, p0, Ldas;->a:Ldaq;

    invoke-static {v0}, Ldaq;->b(Ldaq;)Ldeb;

    move-result-object v0

    invoke-virtual {v0}, Ldeb;->c()V

    .line 90
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    .line 91
    :cond_0
    iget-object v0, p0, Ldas;->a:Ldaq;

    invoke-static {v0}, Ldaq;->c(Ldaq;)V

    .line 129
    :goto_0
    return-void

    .line 96
    :cond_1
    invoke-interface {p1, v13}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 97
    iget-object v0, p0, Ldas;->a:Ldaq;

    iget-object v1, p0, Ldas;->a:Ldaq;

    invoke-static {v1}, Ldaq;->b(Ldaq;)Ldeb;

    move-result-object v1

    invoke-static {v0, v1}, Ldaq;->a(Ldaq;Ljava/lang/Object;)V

    goto :goto_0

    .line 102
    :cond_2
    const/4 v0, 0x3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 103
    const/4 v0, 0x4

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 104
    const/4 v0, 0x5

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 105
    invoke-interface {p1, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 106
    const/4 v0, 0x7

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 107
    const/16 v0, 0xa

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-lez v0, :cond_4

    move v0, v1

    .line 108
    :goto_1
    const/4 v3, 0x6

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 109
    const/16 v3, 0x9

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-eqz v3, :cond_5

    move v3, v1

    .line 110
    :goto_2
    const/16 v9, 0x8

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    .line 112
    new-instance v12, Ldea;

    invoke-direct {v12}, Ldea;-><init>()V

    .line 113
    invoke-virtual {v12, v4}, Ldea;->a(Ljava/lang/String;)V

    .line 114
    invoke-virtual {v12, v5}, Ldea;->b(Ljava/lang/String;)V

    .line 115
    invoke-virtual {v12, v6}, Ldea;->c(Ljava/lang/String;)V

    .line 116
    invoke-virtual {v12, v7}, Ldea;->d(Ljava/lang/String;)V

    .line 117
    invoke-virtual {v12, v8}, Ldea;->e(Ljava/lang/String;)V

    .line 118
    invoke-virtual {v12, v0}, Ldea;->a(Z)V

    .line 119
    invoke-virtual {v12, v10, v11}, Ldea;->a(J)V

    .line 120
    invoke-virtual {v12, v3}, Ldea;->b(Z)V

    .line 121
    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v12, v0}, Ldea;->a(I)V

    .line 123
    iget-object v0, p0, Ldas;->a:Ldaq;

    invoke-static {v0}, Ldaq;->b(Ldaq;)Ldeb;

    move-result-object v0

    invoke-virtual {v0, v12}, Ldeb;->a(Ldea;)V

    .line 124
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 125
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eq v0, v1, :cond_2

    .line 128
    :cond_3
    iget-object v0, p0, Ldas;->a:Ldaq;

    iget-object v1, p0, Ldas;->a:Ldaq;

    invoke-static {v1}, Ldaq;->b(Ldaq;)Ldeb;

    move-result-object v1

    invoke-static {v0, v1}, Ldaq;->b(Ldaq;Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_4
    move v0, v2

    .line 107
    goto :goto_1

    :cond_5
    move v3, v2

    .line 109
    goto :goto_2
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 134
    return-void
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 78
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p2}, Ldas;->a(Landroid/database/Cursor;)V

    return-void
.end method
