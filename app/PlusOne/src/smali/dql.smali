.class public final Ldql;
.super Lhny;
.source "PG"


# instance fields
.field private final a:I

.field private final b:Lkfo;

.field private final c:Ljava/lang/String;

.field private d:[B

.field private e:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;I[B)V
    .locals 2

    .prologue
    .line 40
    const-string v0, "UploadProfilePhotoTask"

    invoke-direct {p0, p1, v0}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 42
    iput p2, p0, Ldql;->a:I

    .line 43
    new-instance v0, Lkfo;

    iget v1, p0, Ldql;->a:I

    invoke-direct {v0, p1, v1}, Lkfo;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Ldql;->b:Lkfo;

    .line 44
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 45
    invoke-interface {v0, p2}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "gaia_id"

    .line 46
    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldql;->c:Ljava/lang/String;

    .line 47
    iput-object p3, p0, Ldql;->d:[B

    .line 48
    iput-object p1, p0, Ldql;->e:Landroid/content/Context;

    .line 49
    return-void
.end method

.method private a(Z)Ljava/lang/String;
    .locals 2

    .prologue
    .line 128
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ldql;->f()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a058e

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 129
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Ldql;->f()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a08f0

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/16 v9, 0xc8

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x3

    .line 53
    :try_start_0
    new-instance v0, Ljava/io/ByteArrayInputStream;

    iget-object v1, p0, Ldql;->d:[B

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 56
    invoke-static {v0}, Ljbh;->a(Ljava/io/InputStream;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 64
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 65
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 66
    new-instance v2, Ljvu;

    iget-object v3, p0, Ldql;->e:Landroid/content/Context;

    iget v4, p0, Ldql;->a:I

    iget-object v5, p0, Ldql;->c:Ljava/lang/String;

    invoke-direct {v2, v3, v4, v5, v1}, Ljvu;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/util/List;)V

    .line 68
    invoke-virtual {v2}, Ljvu;->l()V

    .line 70
    invoke-virtual {v2}, Ljvu;->t()Z

    move-result v1

    if-nez v1, :cond_0

    .line 71
    invoke-virtual {v2, v0}, Ljvu;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 72
    :cond_0
    const-string v0, "UploadProfilePhoto"

    invoke-static {v0, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 74
    iget v0, v2, Lkff;->i:I

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x35

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "CheckPhotosExistenceOperation failed with "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 73
    :cond_1
    const-wide/16 v0, 0x0

    .line 84
    :goto_0
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-eqz v2, :cond_6

    .line 85
    new-instance v2, Ldnj;

    iget-object v3, p0, Ldql;->e:Landroid/content/Context;

    iget v4, p0, Ldql;->a:I

    iget-object v5, p0, Ldql;->c:Ljava/lang/String;

    .line 86
    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v3, v4, v5, v0}, Ldnj;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V

    .line 87
    invoke-virtual {v2}, Ldnj;->l()V

    .line 89
    invoke-virtual {v2}, Ldnj;->t()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 90
    const-string v0, "UploadProfilePhoto"

    invoke-static {v0, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 91
    iget v0, v2, Lkff;->i:I

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v3, 0x2a

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "SetProfilePhotoOperation error "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 93
    :cond_2
    new-instance v0, Lhoz;

    iget v1, v2, Lkff;->i:I

    iget-object v2, v2, Lkff;->k:Ljava/lang/Exception;

    invoke-direct {p0, v8}, Ldql;->a(Z)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    .line 115
    :goto_1
    return-object v0

    .line 57
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 58
    const-string v0, "UploadProfilePhoto"

    invoke-static {v0, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 59
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x23

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Generating fingerprint failed with "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 61
    :cond_3
    new-instance v0, Lhoz;

    invoke-direct {p0, v8}, Ldql;->a(Z)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v7, v1, v2}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    goto :goto_1

    .line 78
    :cond_4
    invoke-virtual {v2, v0}, Ljvu;->c(Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_0

    .line 95
    :cond_5
    new-instance v0, Lhoz;

    invoke-direct {p0, v7}, Ldql;->a(Z)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v9, v10, v1}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    goto :goto_1

    .line 101
    :cond_6
    new-instance v0, Ldog;

    .line 105
    invoke-virtual {p0}, Ldql;->f()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Ldql;->b:Lkfo;

    iget-object v3, p0, Ldql;->c:Ljava/lang/String;

    const-string v4, "profile"

    iget-object v5, p0, Ldql;->d:[B

    invoke-direct/range {v0 .. v5}, Ldog;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Ljava/lang/String;[B)V

    .line 107
    invoke-virtual {v0}, Ldog;->l()V

    .line 109
    invoke-virtual {v0}, Ldog;->t()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 110
    const-string v1, "UploadProfilePhoto"

    invoke-static {v1, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 111
    iget v1, v0, Lkff;->i:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x26

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "UploadMediaOperation error "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 113
    :cond_7
    new-instance v1, Lhoz;

    iget v2, v0, Lkff;->i:I

    iget-object v0, v0, Lkff;->k:Ljava/lang/Exception;

    invoke-direct {p0, v8}, Ldql;->a(Z)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v0, v3}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    move-object v0, v1

    goto/16 :goto_1

    .line 115
    :cond_8
    new-instance v0, Lhoz;

    invoke-direct {p0, v7}, Ldql;->a(Z)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v9, v10, v1}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method public b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 124
    invoke-virtual {p0}, Ldql;->f()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a08f5

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
