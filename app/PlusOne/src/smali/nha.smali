.class public final Lnha;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnha;


# instance fields
.field public b:Ljava/lang/String;

.field public c:[Lofv;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 415
    const/4 v0, 0x0

    new-array v0, v0, [Lnha;

    sput-object v0, Lnha;->a:[Lnha;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 416
    invoke-direct {p0}, Loxq;-><init>()V

    .line 421
    sget-object v0, Lofv;->a:[Lofv;

    iput-object v0, p0, Lnha;->c:[Lofv;

    .line 416
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 442
    .line 443
    iget-object v0, p0, Lnha;->b:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 444
    const/4 v0, 0x1

    iget-object v2, p0, Lnha;->b:Ljava/lang/String;

    .line 445
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 447
    :goto_0
    iget-object v2, p0, Lnha;->c:[Lofv;

    if-eqz v2, :cond_1

    .line 448
    iget-object v2, p0, Lnha;->c:[Lofv;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 449
    if-eqz v4, :cond_0

    .line 450
    const/4 v5, 0x2

    .line 451
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 448
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 455
    :cond_1
    iget-object v1, p0, Lnha;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 456
    iput v0, p0, Lnha;->ai:I

    .line 457
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lnha;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 465
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 466
    sparse-switch v0, :sswitch_data_0

    .line 470
    iget-object v2, p0, Lnha;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 471
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnha;->ah:Ljava/util/List;

    .line 474
    :cond_1
    iget-object v2, p0, Lnha;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 476
    :sswitch_0
    return-object p0

    .line 481
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnha;->b:Ljava/lang/String;

    goto :goto_0

    .line 485
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 486
    iget-object v0, p0, Lnha;->c:[Lofv;

    if-nez v0, :cond_3

    move v0, v1

    .line 487
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lofv;

    .line 488
    iget-object v3, p0, Lnha;->c:[Lofv;

    if-eqz v3, :cond_2

    .line 489
    iget-object v3, p0, Lnha;->c:[Lofv;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 491
    :cond_2
    iput-object v2, p0, Lnha;->c:[Lofv;

    .line 492
    :goto_2
    iget-object v2, p0, Lnha;->c:[Lofv;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 493
    iget-object v2, p0, Lnha;->c:[Lofv;

    new-instance v3, Lofv;

    invoke-direct {v3}, Lofv;-><init>()V

    aput-object v3, v2, v0

    .line 494
    iget-object v2, p0, Lnha;->c:[Lofv;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 495
    invoke-virtual {p1}, Loxn;->a()I

    .line 492
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 486
    :cond_3
    iget-object v0, p0, Lnha;->c:[Lofv;

    array-length v0, v0

    goto :goto_1

    .line 498
    :cond_4
    iget-object v2, p0, Lnha;->c:[Lofv;

    new-instance v3, Lofv;

    invoke-direct {v3}, Lofv;-><init>()V

    aput-object v3, v2, v0

    .line 499
    iget-object v2, p0, Lnha;->c:[Lofv;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 466
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 426
    iget-object v0, p0, Lnha;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 427
    const/4 v0, 0x1

    iget-object v1, p0, Lnha;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 429
    :cond_0
    iget-object v0, p0, Lnha;->c:[Lofv;

    if-eqz v0, :cond_2

    .line 430
    iget-object v1, p0, Lnha;->c:[Lofv;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 431
    if-eqz v3, :cond_1

    .line 432
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 430
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 436
    :cond_2
    iget-object v0, p0, Lnha;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 438
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 412
    invoke-virtual {p0, p1}, Lnha;->a(Loxn;)Lnha;

    move-result-object v0

    return-object v0
.end method
