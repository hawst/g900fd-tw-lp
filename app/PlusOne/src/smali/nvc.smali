.class public final Lnvc;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Ljava/lang/String;

.field public b:I

.field public c:Lnvd;

.field public d:Loxz;

.field public e:Logy;

.field private f:Ljava/lang/Long;

.field private g:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 247
    invoke-direct {p0}, Loxq;-><init>()V

    .line 250
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lnvc;->a:[Ljava/lang/String;

    .line 255
    const/high16 v0, -0x80000000

    iput v0, p0, Lnvc;->b:I

    .line 258
    iput-object v1, p0, Lnvc;->c:Lnvd;

    .line 261
    iput-object v1, p0, Lnvc;->d:Loxz;

    .line 264
    iput-object v1, p0, Lnvc;->e:Logy;

    .line 247
    return-void
.end method


# virtual methods
.method public a()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 300
    .line 301
    iget-object v1, p0, Lnvc;->a:[Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lnvc;->a:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_1

    .line 303
    iget-object v2, p0, Lnvc;->a:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 305
    invoke-static {v4}, Loxo;->b(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v1, v4

    .line 303
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 308
    :cond_0
    iget-object v0, p0, Lnvc;->a:[Ljava/lang/String;

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v1

    .line 310
    :cond_1
    iget-object v1, p0, Lnvc;->f:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 311
    const/4 v1, 0x2

    iget-object v2, p0, Lnvc;->f:Ljava/lang/Long;

    .line 312
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 314
    :cond_2
    iget v1, p0, Lnvc;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_3

    .line 315
    const/4 v1, 0x3

    iget v2, p0, Lnvc;->b:I

    .line 316
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 318
    :cond_3
    iget-object v1, p0, Lnvc;->c:Lnvd;

    if-eqz v1, :cond_4

    .line 319
    const/4 v1, 0x4

    iget-object v2, p0, Lnvc;->c:Lnvd;

    .line 320
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 322
    :cond_4
    iget-object v1, p0, Lnvc;->d:Loxz;

    if-eqz v1, :cond_5

    .line 323
    const/4 v1, 0x5

    iget-object v2, p0, Lnvc;->d:Loxz;

    .line 324
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 326
    :cond_5
    iget-object v1, p0, Lnvc;->e:Logy;

    if-eqz v1, :cond_6

    .line 327
    const/4 v1, 0x6

    iget-object v2, p0, Lnvc;->e:Logy;

    .line 328
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 330
    :cond_6
    iget-object v1, p0, Lnvc;->g:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    .line 331
    const/4 v1, 0x7

    iget-object v2, p0, Lnvc;->g:Ljava/lang/Boolean;

    .line 332
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 334
    :cond_7
    iget-object v1, p0, Lnvc;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 335
    iput v0, p0, Lnvc;->ai:I

    .line 336
    return v0
.end method

.method public a(Loxn;)Lnvc;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 344
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 345
    sparse-switch v0, :sswitch_data_0

    .line 349
    iget-object v1, p0, Lnvc;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 350
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnvc;->ah:Ljava/util/List;

    .line 353
    :cond_1
    iget-object v1, p0, Lnvc;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 355
    :sswitch_0
    return-object p0

    .line 360
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 361
    iget-object v0, p0, Lnvc;->a:[Ljava/lang/String;

    array-length v0, v0

    .line 362
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    .line 363
    iget-object v2, p0, Lnvc;->a:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 364
    iput-object v1, p0, Lnvc;->a:[Ljava/lang/String;

    .line 365
    :goto_1
    iget-object v1, p0, Lnvc;->a:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 366
    iget-object v1, p0, Lnvc;->a:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 367
    invoke-virtual {p1}, Loxn;->a()I

    .line 365
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 370
    :cond_2
    iget-object v1, p0, Lnvc;->a:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    .line 374
    :sswitch_2
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lnvc;->f:Ljava/lang/Long;

    goto :goto_0

    .line 378
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 379
    if-eqz v0, :cond_3

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    .line 382
    :cond_3
    iput v0, p0, Lnvc;->b:I

    goto :goto_0

    .line 384
    :cond_4
    iput v3, p0, Lnvc;->b:I

    goto :goto_0

    .line 389
    :sswitch_4
    iget-object v0, p0, Lnvc;->c:Lnvd;

    if-nez v0, :cond_5

    .line 390
    new-instance v0, Lnvd;

    invoke-direct {v0}, Lnvd;-><init>()V

    iput-object v0, p0, Lnvc;->c:Lnvd;

    .line 392
    :cond_5
    iget-object v0, p0, Lnvc;->c:Lnvd;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 396
    :sswitch_5
    iget-object v0, p0, Lnvc;->d:Loxz;

    if-nez v0, :cond_6

    .line 397
    new-instance v0, Loxz;

    invoke-direct {v0}, Loxz;-><init>()V

    iput-object v0, p0, Lnvc;->d:Loxz;

    .line 399
    :cond_6
    iget-object v0, p0, Lnvc;->d:Loxz;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 403
    :sswitch_6
    iget-object v0, p0, Lnvc;->e:Logy;

    if-nez v0, :cond_7

    .line 404
    new-instance v0, Logy;

    invoke-direct {v0}, Logy;-><init>()V

    iput-object v0, p0, Lnvc;->e:Logy;

    .line 406
    :cond_7
    iget-object v0, p0, Lnvc;->e:Logy;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 410
    :sswitch_7
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnvc;->g:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 345
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 271
    iget-object v0, p0, Lnvc;->a:[Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 272
    iget-object v1, p0, Lnvc;->a:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 273
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->a(ILjava/lang/String;)V

    .line 272
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 276
    :cond_0
    iget-object v0, p0, Lnvc;->f:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 277
    const/4 v0, 0x2

    iget-object v1, p0, Lnvc;->f:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 279
    :cond_1
    iget v0, p0, Lnvc;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_2

    .line 280
    const/4 v0, 0x3

    iget v1, p0, Lnvc;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 282
    :cond_2
    iget-object v0, p0, Lnvc;->c:Lnvd;

    if-eqz v0, :cond_3

    .line 283
    const/4 v0, 0x4

    iget-object v1, p0, Lnvc;->c:Lnvd;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 285
    :cond_3
    iget-object v0, p0, Lnvc;->d:Loxz;

    if-eqz v0, :cond_4

    .line 286
    const/4 v0, 0x5

    iget-object v1, p0, Lnvc;->d:Loxz;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 288
    :cond_4
    iget-object v0, p0, Lnvc;->e:Logy;

    if-eqz v0, :cond_5

    .line 289
    const/4 v0, 0x6

    iget-object v1, p0, Lnvc;->e:Logy;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 291
    :cond_5
    iget-object v0, p0, Lnvc;->g:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    .line 292
    const/4 v0, 0x7

    iget-object v1, p0, Lnvc;->g:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 294
    :cond_6
    iget-object v0, p0, Lnvc;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 296
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 243
    invoke-virtual {p0, p1}, Lnvc;->a(Loxn;)Lnvc;

    move-result-object v0

    return-object v0
.end method
