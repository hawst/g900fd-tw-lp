.class public final Lozl;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lozl;


# instance fields
.field public b:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Lozl;

    sput-object v0, Lozl;->a:[Lozl;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 13
    const/high16 v0, -0x80000000

    iput v0, p0, Lozl;->b:I

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 27
    const/4 v0, 0x0

    .line 28
    iget v1, p0, Lozl;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 29
    const/4 v0, 0x1

    iget v1, p0, Lozl;->b:I

    .line 30
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 32
    :cond_0
    iget-object v1, p0, Lozl;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 33
    iput v0, p0, Lozl;->ai:I

    .line 34
    return v0
.end method

.method public a(Loxn;)Lozl;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 42
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 43
    sparse-switch v0, :sswitch_data_0

    .line 47
    iget-object v1, p0, Lozl;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 48
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lozl;->ah:Ljava/util/List;

    .line 51
    :cond_1
    iget-object v1, p0, Lozl;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 53
    :sswitch_0
    return-object p0

    .line 58
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 59
    if-eq v0, v2, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-ne v0, v1, :cond_3

    .line 64
    :cond_2
    iput v0, p0, Lozl;->b:I

    goto :goto_0

    .line 66
    :cond_3
    iput v2, p0, Lozl;->b:I

    goto :goto_0

    .line 43
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 18
    iget v0, p0, Lozl;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 19
    const/4 v0, 0x1

    iget v1, p0, Lozl;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 21
    :cond_0
    iget-object v0, p0, Lozl;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 23
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lozl;->a(Loxn;)Lozl;

    move-result-object v0

    return-object v0
.end method
