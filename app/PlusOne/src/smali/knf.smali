.class public final Lknf;
.super Lkmz;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lkmz;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Loft;)V
    .locals 3

    .prologue
    .line 24
    invoke-super {p0, p1}, Lkmz;->a(Loft;)V

    .line 26
    iget-object v0, p1, Loft;->f:[Lofs;

    .line 31
    if-eqz v0, :cond_0

    array-length v1, v0

    if-nez v1, :cond_1

    .line 32
    :cond_0
    new-instance v0, Ljava/io/IOException;

    const-string v1, "empty media item"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 35
    :cond_1
    const/4 v1, 0x0

    aget-object v0, v0, v1

    iget-object v1, v0, Lofs;->b:Ljava/lang/String;

    if-nez v1, :cond_2

    new-instance v0, Ljava/io/IOException;

    const-string v1, "missing image object"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    const-string v1, "https:"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, v0, Lofs;->b:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 37
    :goto_0
    iget-object v0, p1, Loft;->d:Ljava/lang/String;

    invoke-static {v0}, Llsu;->a(Ljava/lang/String;)Ljava/lang/String;

    .line 39
    return-void

    .line 35
    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method
