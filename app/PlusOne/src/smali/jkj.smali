.class public final Ljkj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljjm;
.implements Ljjp;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Landroid/content/Context;Lkfo;Ljin;)V
    .locals 4

    .prologue
    .line 66
    .line 67
    invoke-static {p1}, Ljke;->a(Landroid/content/Context;)Ljke;

    move-result-object v0

    new-instance v1, Lpeu;

    invoke-direct {v1}, Lpeu;-><init>()V

    invoke-virtual {v0}, Ljke;->b()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lpeu;->a:Ljava/lang/String;

    new-instance v2, Ljkb;

    invoke-direct {v2, p1}, Ljkb;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2}, Ljkb;->a()Llug;

    move-result-object v2

    iput-object v2, v1, Lpeu;->b:Llug;

    const/16 v2, 0x32

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lpeu;->c:Ljava/lang/Integer;

    invoke-virtual {v0, p3}, Ljke;->a(Ljin;)[I

    move-result-object v0

    iput-object v0, v1, Lpeu;->d:[I

    .line 68
    new-instance v2, Ljjg;

    invoke-direct {v2, p1, p2, v1}, Ljjg;-><init>(Landroid/content/Context;Lkfo;Lpeu;)V

    .line 69
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 70
    const-string v0, "fetch_category"

    invoke-virtual {p3}, Ljin;->ordinal()I

    move-result v3

    invoke-virtual {v1, v0, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 71
    const-string v0, "is_initial_fetch"

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 72
    const-class v0, Ljjo;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljjo;

    .line 73
    invoke-virtual {v0, v2, p0, p1, v1}, Ljjo;->a(Lkgg;Ljjp;Landroid/content/Context;Landroid/os/Bundle;)V

    .line 74
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    const-string v0, "com.google.android.libraries.social.notifications.SYNC_FETCH_NOTIFICATIONS"

    return-object v0
.end method

.method public a(Landroid/content/Intent;Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 50
    invoke-static {p1}, Lcom/google/android/libraries/social/notifications/impl/GunsIntentService;->a(Landroid/content/Intent;)Lkfo;

    move-result-object v0

    .line 51
    if-nez v0, :cond_0

    .line 62
    :goto_0
    return-void

    .line 54
    :cond_0
    invoke-static {p1}, Lcom/google/android/libraries/social/notifications/impl/GunsIntentService;->c(Landroid/content/Intent;)Ljin;

    move-result-object v1

    .line 55
    invoke-static {p2, v0, v1}, Ljju;->b(Landroid/content/Context;Lkfo;Ljin;)[B

    move-result-object v2

    .line 57
    if-eqz v2, :cond_1

    array-length v3, v2

    if-nez v3, :cond_2

    .line 58
    :cond_1
    invoke-direct {p0, p2, v0, v1}, Ljkj;->a(Landroid/content/Context;Lkfo;Ljin;)V

    goto :goto_0

    .line 60
    :cond_2
    invoke-static {p2}, Ljke;->a(Landroid/content/Context;)Ljke;

    move-result-object v3

    new-instance v4, Lpfk;

    invoke-direct {v4}, Lpfk;-><init>()V

    invoke-virtual {v3}, Ljke;->b()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lpfk;->a:Ljava/lang/String;

    new-instance v5, Ljkb;

    invoke-direct {v5, p2}, Ljkb;-><init>(Landroid/content/Context;)V

    invoke-virtual {v5}, Ljkb;->a()Llug;

    move-result-object v5

    iput-object v5, v4, Lpfk;->b:Llug;

    const/16 v5, 0x32

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iput-object v5, v4, Lpfk;->c:Ljava/lang/Integer;

    invoke-virtual {v3, v1}, Ljke;->a(Ljin;)[I

    move-result-object v3

    iput-object v3, v4, Lpfk;->d:[I

    iput-object v2, v4, Lpfk;->e:[B

    new-instance v2, Ljkk;

    invoke-direct {v2, p2, v0, v4}, Ljkk;-><init>(Landroid/content/Context;Lkfo;Lpfk;)V

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    const-string v0, "fetch_category"

    invoke-virtual {v1}, Ljin;->ordinal()I

    move-result v1

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "is_initial_fetch"

    const/4 v1, 0x0

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-class v0, Ljjo;

    invoke-static {p2, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljjo;

    invoke-virtual {v0, v2, p0, p2, v3}, Ljjo;->a(Lkgg;Ljjp;Landroid/content/Context;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public a(Lkgg;Landroid/content/Context;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x3

    .line 37
    const-string v0, "is_initial_fetch"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 38
    check-cast p1, Ljjg;

    .line 39
    const-string v0, "fetch_category"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljin;->a(I)Ljin;

    move-result-object v0

    .line 40
    iget-object v1, p1, Lkff;->g:Lkfo;

    invoke-virtual {p1}, Ljjg;->t()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p1}, Ljjg;->i()Lpew;

    move-result-object v2

    const-string v3, "SyncFetchHandler"

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lpew;->toString()Ljava/lang/String;

    :cond_0
    invoke-static {p2, v1}, Ljju;->a(Landroid/content/Context;Lkfo;)V

    invoke-static {p2, v1, v0, v2}, Ljju;->a(Landroid/content/Context;Lkfo;Ljin;Lpew;)V

    .line 46
    :goto_0
    return-void

    .line 40
    :cond_1
    const-string v0, "SyncFetchHandler"

    const-string v1, "Failed to fetch notifications."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 42
    :cond_2
    check-cast p1, Ljkk;

    .line 43
    const-string v0, "fetch_category"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljin;->a(I)Ljin;

    move-result-object v0

    .line 44
    iget-object v1, p1, Lkff;->g:Lkfo;

    invoke-virtual {p1}, Ljkk;->t()Z

    move-result v2

    if-nez v2, :cond_5

    invoke-virtual {p1}, Ljkk;->i()Lpfm;

    move-result-object v2

    const-string v3, "SyncFetchHandler"

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v2}, Lpfm;->toString()Ljava/lang/String;

    :cond_3
    iget-object v3, v2, Lpfm;->d:[B

    if-nez v3, :cond_4

    invoke-static {p2, v1, v0, v2}, Ljju;->a(Landroid/content/Context;Lkfo;Ljin;Lpfm;)V

    goto :goto_0

    :cond_4
    invoke-direct {p0, p2, v1, v0}, Ljkj;->a(Landroid/content/Context;Lkfo;Ljin;)V

    goto :goto_0

    :cond_5
    const-string v0, "SyncFetchHandler"

    const-string v1, "Failed to sync notifications."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20
    invoke-virtual {p0}, Ljkj;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
