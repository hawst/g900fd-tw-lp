.class public Ldmh;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmgy;",
        "Lmgz;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Z

.field private p:I

.field private q:Z

.field private r:[B


# direct methods
.method public constructor <init>(Landroid/content/Context;IILjava/lang/String;ZZ[B)V
    .locals 6

    .prologue
    .line 37
    const-string v3, "plusone"

    new-instance v4, Lmgy;

    invoke-direct {v4}, Lmgy;-><init>()V

    new-instance v5, Lmgz;

    invoke-direct {v5}, Lmgz;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 39
    iput p3, p0, Ldmh;->p:I

    .line 40
    iput-object p4, p0, Ldmh;->a:Ljava/lang/String;

    .line 41
    iput-boolean p5, p0, Ldmh;->b:Z

    .line 42
    iput-boolean p6, p0, Ldmh;->q:Z

    .line 43
    iput-object p7, p0, Ldmh;->r:[B

    .line 44
    return-void
.end method


# virtual methods
.method public a(ILjava/lang/String;Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 80
    const/16 v0, 0xc8

    if-ne p1, v0, :cond_0

    if-eqz p3, :cond_1

    .line 81
    :cond_0
    invoke-virtual {p0}, Ldmh;->b()V

    .line 83
    :cond_1
    invoke-super {p0, p1, p2, p3}, Leuh;->a(ILjava/lang/String;Ljava/lang/Exception;)V

    .line 84
    return-void
.end method

.method protected a(Lmgy;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 60
    new-instance v0, Lnga;

    invoke-direct {v0}, Lnga;-><init>()V

    iput-object v0, p1, Lmgy;->a:Lnga;

    .line 61
    iget-object v0, p1, Lmgy;->a:Lnga;

    .line 62
    iget v1, p0, Ldmh;->p:I

    iput v1, v0, Lnga;->b:I

    .line 63
    iget-object v1, p0, Ldmh;->a:Ljava/lang/String;

    iput-object v1, v0, Lnga;->a:Ljava/lang/String;

    .line 64
    iget-boolean v1, p0, Ldmh;->b:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lnga;->c:Ljava/lang/Boolean;

    .line 65
    iget-boolean v1, p0, Ldmh;->q:Z

    if-eqz v1, :cond_0

    .line 66
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lnga;->d:Ljava/lang/Boolean;

    .line 67
    iget-object v1, p0, Ldmh;->f:Landroid/content/Context;

    iget v2, p0, Ldmh;->c:I

    invoke-static {v1, v2, v3}, Ldhv;->a(Landroid/content/Context;IZ)V

    .line 69
    :cond_0
    iget-object v1, p0, Ldmh;->r:[B

    invoke-static {v1}, Llap;->a([B)Lnwr;

    move-result-object v1

    .line 70
    if-eqz v1, :cond_1

    .line 71
    new-instance v2, Logw;

    invoke-direct {v2}, Logw;-><init>()V

    iput-object v2, v0, Lnga;->e:Logw;

    .line 72
    iget-object v0, v0, Lnga;->e:Logw;

    iput-object v1, v0, Logw;->a:Lnwr;

    .line 75
    :cond_1
    iget-object v0, p1, Lmgy;->apiHeader:Llyq;

    iget-object v0, v0, Llyq;->b:Lpyu;

    const-string v1, "mobile:android_app"

    iput-object v1, v0, Lpyu;->c:Ljava/lang/String;

    .line 76
    return-void
.end method

.method protected a(Lmgz;)V
    .locals 3

    .prologue
    .line 88
    iget-object v0, p1, Lmgz;->a:Lngc;

    .line 89
    iget-object v1, v0, Lngc;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 90
    iget-object v0, v0, Lngc;->a:Loae;

    invoke-virtual {p0, v0}, Ldmh;->a(Loae;)V

    .line 97
    :cond_0
    :goto_0
    return-void

    .line 92
    :cond_1
    invoke-virtual {p0}, Ldmh;->b()V

    .line 93
    iget-boolean v0, p0, Ldmh;->q:Z

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Ldmh;->f:Landroid/content/Context;

    iget v1, p0, Ldmh;->c:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Ldhv;->a(Landroid/content/Context;IZ)V

    goto :goto_0
.end method

.method protected a(Loae;)V
    .locals 0

    .prologue
    .line 50
    return-void
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 21
    check-cast p1, Lmgy;

    invoke-virtual {p0, p1}, Ldmh;->a(Lmgy;)V

    return-void
.end method

.method protected b()V
    .locals 0

    .prologue
    .line 56
    return-void
.end method

.method protected synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 21
    check-cast p1, Lmgz;

    invoke-virtual {p0, p1}, Ldmh;->a(Lmgz;)V

    return-void
.end method
