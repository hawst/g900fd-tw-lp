.class public final Lmrt;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmrt;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 473
    const/4 v0, 0x0

    new-array v0, v0, [Lmrt;

    sput-object v0, Lmrt;->a:[Lmrt;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 474
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 495
    const/4 v0, 0x0

    .line 496
    iget-object v1, p0, Lmrt;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 497
    const/4 v0, 0x1

    iget-object v1, p0, Lmrt;->b:Ljava/lang/String;

    .line 498
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 500
    :cond_0
    iget-object v1, p0, Lmrt;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 501
    const/4 v1, 0x2

    iget-object v2, p0, Lmrt;->c:Ljava/lang/String;

    .line 502
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 504
    :cond_1
    iget-object v1, p0, Lmrt;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 505
    iput v0, p0, Lmrt;->ai:I

    .line 506
    return v0
.end method

.method public a(Loxn;)Lmrt;
    .locals 2

    .prologue
    .line 514
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 515
    sparse-switch v0, :sswitch_data_0

    .line 519
    iget-object v1, p0, Lmrt;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 520
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmrt;->ah:Ljava/util/List;

    .line 523
    :cond_1
    iget-object v1, p0, Lmrt;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 525
    :sswitch_0
    return-object p0

    .line 530
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmrt;->b:Ljava/lang/String;

    goto :goto_0

    .line 534
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmrt;->c:Ljava/lang/String;

    goto :goto_0

    .line 515
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 483
    iget-object v0, p0, Lmrt;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 484
    const/4 v0, 0x1

    iget-object v1, p0, Lmrt;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 486
    :cond_0
    iget-object v0, p0, Lmrt;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 487
    const/4 v0, 0x2

    iget-object v1, p0, Lmrt;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 489
    :cond_1
    iget-object v0, p0, Lmrt;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 491
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 470
    invoke-virtual {p0, p1}, Lmrt;->a(Loxn;)Lmrt;

    move-result-object v0

    return-object v0
.end method
