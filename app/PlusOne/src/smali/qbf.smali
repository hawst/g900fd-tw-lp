.class public final Lqbf;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Long;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 527
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 558
    const/4 v0, 0x0

    .line 559
    iget-object v1, p0, Lqbf;->a:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 560
    const/4 v0, 0x1

    iget-object v1, p0, Lqbf;->a:Ljava/lang/Long;

    .line 561
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Loxo;->f(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 563
    :cond_0
    iget-object v1, p0, Lqbf;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 564
    const/4 v1, 0x2

    iget-object v2, p0, Lqbf;->b:Ljava/lang/String;

    .line 565
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 567
    :cond_1
    iget-object v1, p0, Lqbf;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 568
    const/4 v1, 0x3

    iget-object v2, p0, Lqbf;->c:Ljava/lang/String;

    .line 569
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 571
    :cond_2
    iget-object v1, p0, Lqbf;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 572
    const/4 v1, 0x4

    iget-object v2, p0, Lqbf;->d:Ljava/lang/String;

    .line 573
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 575
    :cond_3
    iget-object v1, p0, Lqbf;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 576
    iput v0, p0, Lqbf;->ai:I

    .line 577
    return v0
.end method

.method public a(Loxn;)Lqbf;
    .locals 2

    .prologue
    .line 585
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 586
    sparse-switch v0, :sswitch_data_0

    .line 590
    iget-object v1, p0, Lqbf;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 591
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lqbf;->ah:Ljava/util/List;

    .line 594
    :cond_1
    iget-object v1, p0, Lqbf;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 596
    :sswitch_0
    return-object p0

    .line 601
    :sswitch_1
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lqbf;->a:Ljava/lang/Long;

    goto :goto_0

    .line 605
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lqbf;->b:Ljava/lang/String;

    goto :goto_0

    .line 609
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lqbf;->c:Ljava/lang/String;

    goto :goto_0

    .line 613
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lqbf;->d:Ljava/lang/String;

    goto :goto_0

    .line 586
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 540
    iget-object v0, p0, Lqbf;->a:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 541
    const/4 v0, 0x1

    iget-object v1, p0, Lqbf;->a:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 543
    :cond_0
    iget-object v0, p0, Lqbf;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 544
    const/4 v0, 0x2

    iget-object v1, p0, Lqbf;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 546
    :cond_1
    iget-object v0, p0, Lqbf;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 547
    const/4 v0, 0x3

    iget-object v1, p0, Lqbf;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 549
    :cond_2
    iget-object v0, p0, Lqbf;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 550
    const/4 v0, 0x4

    iget-object v1, p0, Lqbf;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 552
    :cond_3
    iget-object v0, p0, Lqbf;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 554
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 523
    invoke-virtual {p0, p1}, Lqbf;->a(Loxn;)Lqbf;

    move-result-object v0

    return-object v0
.end method
