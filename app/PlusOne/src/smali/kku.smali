.class public final Lkku;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private synthetic a:Landroid/content/res/Resources;

.field private synthetic b:Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;Landroid/content/res/Resources;)V
    .locals 0

    .prologue
    .line 106
    iput-object p1, p0, Lkku;->b:Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;

    iput-object p2, p0, Lkku;->a:Landroid/content/res/Resources;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 109
    iget-object v0, p0, Lkku;->b:Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;

    invoke-static {v0}, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;->b(Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 130
    :goto_0
    return-void

    .line 112
    :cond_0
    iget-object v0, p0, Lkku;->b:Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;->a(Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;Z)Z

    move v1, v2

    .line 114
    :goto_1
    iget-object v0, p0, Lkku;->b:Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;

    invoke-static {v0}, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;->c(Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 115
    iget-object v0, p0, Lkku;->b:Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;

    invoke-static {v0}, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;->c(Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkkv;

    invoke-interface {v0}, Lkkv;->a()V

    .line 114
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 118
    :cond_1
    iget-object v0, p0, Lkku;->b:Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;->n()Lz;

    move-result-object v0

    invoke-static {v0}, Liuo;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 119
    const/4 v0, 0x0

    iget-object v1, p0, Lkku;->a:Landroid/content/res/Resources;

    const v3, 0x7f0a03f3

    .line 121
    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lkku;->a:Landroid/content/res/Resources;

    const v4, 0x7f0a02c4

    .line 122
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lkku;->a:Landroid/content/res/Resources;

    const v5, 0x7f0a02c5

    .line 123
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 119
    invoke-static {v0, v1, v3, v4}, Llgr;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)Llgr;

    move-result-object v0

    .line 125
    iget-object v1, p0, Lkku;->b:Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;

    invoke-virtual {v0, v1, v2}, Llgr;->a(Lu;I)V

    .line 126
    iget-object v1, p0, Lkku;->b:Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;->p()Lae;

    move-result-object v1

    const-string v2, "dialog-loc-settings"

    invoke-virtual {v0, v1, v2}, Llgr;->a(Lae;Ljava/lang/String;)V

    goto :goto_0

    .line 128
    :cond_2
    iget-object v0, p0, Lkku;->b:Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;

    invoke-static {v0}, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;->d(Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;)V

    goto :goto_0
.end method
