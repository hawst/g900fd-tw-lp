.class public final Lket;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:J

.field private static final b:J

.field private static final c:J


# instance fields
.field private d:Lkeu;

.field private e:Lkeq;

.field private f:Lkeq;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 58
    sget-object v0, Llso;->b:Llso;

    const-wide/16 v2, 0x5

    invoke-virtual {v0, v2, v3}, Llso;->a(J)J

    move-result-wide v0

    sput-wide v0, Lket;->a:J

    .line 60
    sget-object v0, Llso;->b:Llso;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v2, v3}, Llso;->a(J)J

    move-result-wide v0

    sput-wide v0, Lket;->b:J

    .line 66
    sget-object v0, Llso;->b:Llso;

    const-wide/16 v2, 0x200

    invoke-virtual {v0, v2, v3}, Llso;->a(J)J

    move-result-wide v0

    sput-wide v0, Lket;->c:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    new-instance v0, Lkew;

    invoke-direct {v0}, Lkew;-><init>()V

    invoke-virtual {v0, p1}, Lkew;->a(Landroid/content/Context;)Lkeu;

    move-result-object v0

    iput-object v0, p0, Lket;->d:Lkeu;

    .line 77
    new-instance v0, Lker;

    invoke-direct {v0}, Lker;-><init>()V

    const-string v1, "media"

    .line 78
    invoke-virtual {v0, v1}, Lker;->a(Ljava/lang/String;)Lker;

    move-result-object v0

    sget-wide v2, Lket;->a:J

    .line 79
    invoke-virtual {v0, v2, v3}, Lker;->a(J)Lker;

    move-result-object v0

    sget-wide v2, Lket;->b:J

    .line 80
    invoke-virtual {v0, v2, v3}, Lker;->b(J)Lker;

    move-result-object v0

    const v1, 0x3dcccccd    # 0.1f

    .line 81
    invoke-virtual {v0, v1}, Lker;->a(F)Lker;

    move-result-object v0

    .line 82
    invoke-virtual {v0}, Lker;->a()Lkeq;

    move-result-object v0

    iput-object v0, p0, Lket;->e:Lkeq;

    .line 84
    new-instance v0, Lker;

    invoke-direct {v0}, Lker;-><init>()V

    const-string v1, "media_sync"

    .line 85
    invoke-virtual {v0, v1}, Lker;->a(Ljava/lang/String;)Lker;

    move-result-object v0

    const-wide/16 v2, 0x0

    .line 86
    invoke-virtual {v0, v2, v3}, Lker;->a(J)Lker;

    move-result-object v0

    sget-wide v2, Lket;->c:J

    .line 87
    invoke-virtual {v0, v2, v3}, Lker;->b(J)Lker;

    move-result-object v0

    const/high16 v1, 0x3e800000    # 0.25f

    .line 88
    invoke-virtual {v0, v1}, Lker;->a(F)Lker;

    move-result-object v0

    .line 89
    invoke-virtual {v0}, Lker;->a()Lkeq;

    move-result-object v0

    iput-object v0, p0, Lket;->f:Lkeq;

    .line 90
    return-void
.end method


# virtual methods
.method public a()Lkes;
    .locals 4

    .prologue
    .line 120
    new-instance v0, Lkes;

    iget-object v1, p0, Lket;->d:Lkeu;

    iget-object v2, p0, Lket;->e:Lkeq;

    iget-object v3, p0, Lket;->f:Lkeq;

    invoke-direct {v0, v1, v2, v3}, Lkes;-><init>(Lkeu;Lkeq;Lkeq;)V

    return-object v0
.end method
