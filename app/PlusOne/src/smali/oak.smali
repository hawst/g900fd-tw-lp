.class public final Loak;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Loak;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 766
    const/4 v0, 0x0

    new-array v0, v0, [Loak;

    sput-object v0, Loak;->a:[Loak;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 767
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 789
    const/4 v0, 0x1

    iget-object v1, p0, Loak;->b:Ljava/lang/String;

    .line 791
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 792
    const/4 v1, 0x2

    iget-object v2, p0, Loak;->c:Ljava/lang/String;

    .line 793
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 794
    iget-object v1, p0, Loak;->d:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 795
    const/4 v1, 0x3

    iget-object v2, p0, Loak;->d:Ljava/lang/String;

    .line 796
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 798
    :cond_0
    iget-object v1, p0, Loak;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 799
    iput v0, p0, Loak;->ai:I

    .line 800
    return v0
.end method

.method public a(Loxn;)Loak;
    .locals 2

    .prologue
    .line 808
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 809
    sparse-switch v0, :sswitch_data_0

    .line 813
    iget-object v1, p0, Loak;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 814
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Loak;->ah:Ljava/util/List;

    .line 817
    :cond_1
    iget-object v1, p0, Loak;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 819
    :sswitch_0
    return-object p0

    .line 824
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loak;->b:Ljava/lang/String;

    goto :goto_0

    .line 828
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loak;->c:Ljava/lang/String;

    goto :goto_0

    .line 832
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loak;->d:Ljava/lang/String;

    goto :goto_0

    .line 809
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 778
    const/4 v0, 0x1

    iget-object v1, p0, Loak;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 779
    const/4 v0, 0x2

    iget-object v1, p0, Loak;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 780
    iget-object v0, p0, Loak;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 781
    const/4 v0, 0x3

    iget-object v1, p0, Loak;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 783
    :cond_0
    iget-object v0, p0, Loak;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 785
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 763
    invoke-virtual {p0, p1}, Loak;->a(Loxn;)Loak;

    move-result-object v0

    return-object v0
.end method
