.class final Ldtj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ldtg;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:I

.field private final c:Ldts;

.field private final d:I

.field private final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILdts;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1556
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1557
    iput-object p1, p0, Ldtj;->a:Landroid/content/Context;

    .line 1558
    iput p2, p0, Ldtj;->b:I

    .line 1559
    iput-object p3, p0, Ldtj;->c:Ldts;

    .line 1560
    iput-object p4, p0, Ldtj;->e:Ljava/lang/String;

    .line 1561
    sget-object v0, Ldtf;->a:[I

    invoke-virtual {p3}, Ldts;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1574
    :pswitch_0
    const/4 v0, 0x0

    iput v0, p0, Ldtj;->d:I

    .line 1576
    :goto_0
    return-void

    .line 1563
    :pswitch_1
    const/4 v0, 0x2

    iput v0, p0, Ldtj;->d:I

    goto :goto_0

    .line 1566
    :pswitch_2
    const/4 v0, 0x1

    iput v0, p0, Ldtj;->d:I

    goto :goto_0

    .line 1569
    :pswitch_3
    const/4 v0, 0x3

    iput v0, p0, Ldtj;->d:I

    goto :goto_0

    .line 1561
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ldth;
    .locals 7

    .prologue
    .line 1580
    new-instance v0, Ldkq;

    iget-object v1, p0, Ldtj;->a:Landroid/content/Context;

    iget v2, p0, Ldtj;->b:I

    iget-object v4, p0, Ldtj;->e:Ljava/lang/String;

    iget v5, p0, Ldtj;->d:I

    iget-object v3, p0, Ldtj;->c:Ldts;

    iget-boolean v6, v3, Ldts;->g:Z

    move-object v3, p1

    invoke-direct/range {v0 .. v6}, Ldkq;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;IZ)V

    .line 1582
    invoke-virtual {v0}, Ldkq;->l()V

    .line 1583
    const-string v1, "EsTileSync"

    invoke-virtual {v0, v1}, Ldkq;->e(Ljava/lang/String;)V

    .line 1585
    new-instance v1, Ldth;

    invoke-direct {v1, v0}, Ldth;-><init>(Ldkq;)V

    return-object v1
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 1590
    const-string v0, "DeltaAllPhotosPageFetcher, requestReason: "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Ldtj;->d:I

    iget-object v2, p0, Ldtj;->e:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x18

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", syncToken: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
