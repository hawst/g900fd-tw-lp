.class public final Lode;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/Boolean;

.field public c:Ljava/lang/Boolean;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/Boolean;

.field public g:Loxz;

.field public h:Ljava/lang/String;

.field private i:Ljava/lang/Boolean;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:I

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1603
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1624
    const/4 v0, 0x0

    iput-object v0, p0, Lode;->g:Loxz;

    .line 1627
    const/high16 v0, -0x80000000

    iput v0, p0, Lode;->l:I

    .line 1603
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1686
    const/4 v0, 0x0

    .line 1687
    iget-object v1, p0, Lode;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1688
    const/4 v0, 0x1

    iget-object v1, p0, Lode;->a:Ljava/lang/String;

    .line 1689
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1691
    :cond_0
    iget-object v1, p0, Lode;->i:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 1692
    const/4 v1, 0x2

    iget-object v2, p0, Lode;->i:Ljava/lang/Boolean;

    .line 1693
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1695
    :cond_1
    iget-object v1, p0, Lode;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 1696
    const/4 v1, 0x3

    iget-object v2, p0, Lode;->b:Ljava/lang/Boolean;

    .line 1697
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1699
    :cond_2
    iget-object v1, p0, Lode;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 1700
    const/4 v1, 0x4

    iget-object v2, p0, Lode;->c:Ljava/lang/Boolean;

    .line 1701
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1703
    :cond_3
    iget-object v1, p0, Lode;->j:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 1704
    const/4 v1, 0x5

    iget-object v2, p0, Lode;->j:Ljava/lang/String;

    .line 1705
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1707
    :cond_4
    iget-object v1, p0, Lode;->k:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 1708
    const/4 v1, 0x6

    iget-object v2, p0, Lode;->k:Ljava/lang/String;

    .line 1709
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1711
    :cond_5
    iget-object v1, p0, Lode;->d:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 1712
    const/4 v1, 0x7

    iget-object v2, p0, Lode;->d:Ljava/lang/String;

    .line 1713
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1715
    :cond_6
    iget-object v1, p0, Lode;->e:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 1716
    const/16 v1, 0x8

    iget-object v2, p0, Lode;->e:Ljava/lang/String;

    .line 1717
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1719
    :cond_7
    iget-object v1, p0, Lode;->f:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    .line 1720
    const/16 v1, 0x9

    iget-object v2, p0, Lode;->f:Ljava/lang/Boolean;

    .line 1721
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1723
    :cond_8
    iget-object v1, p0, Lode;->g:Loxz;

    if-eqz v1, :cond_9

    .line 1724
    const/16 v1, 0xa

    iget-object v2, p0, Lode;->g:Loxz;

    .line 1725
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1727
    :cond_9
    iget v1, p0, Lode;->l:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_a

    .line 1728
    const/16 v1, 0xb

    iget v2, p0, Lode;->l:I

    .line 1729
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1731
    :cond_a
    iget-object v1, p0, Lode;->m:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 1732
    const/16 v1, 0xc

    iget-object v2, p0, Lode;->m:Ljava/lang/String;

    .line 1733
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1735
    :cond_b
    iget-object v1, p0, Lode;->n:Ljava/lang/Boolean;

    if-eqz v1, :cond_c

    .line 1736
    const/16 v1, 0xd

    iget-object v2, p0, Lode;->n:Ljava/lang/Boolean;

    .line 1737
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1739
    :cond_c
    iget-object v1, p0, Lode;->h:Ljava/lang/String;

    if-eqz v1, :cond_d

    .line 1740
    const/16 v1, 0xe

    iget-object v2, p0, Lode;->h:Ljava/lang/String;

    .line 1741
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1743
    :cond_d
    iget-object v1, p0, Lode;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1744
    iput v0, p0, Lode;->ai:I

    .line 1745
    return v0
.end method

.method public a(Loxn;)Lode;
    .locals 2

    .prologue
    .line 1753
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1754
    sparse-switch v0, :sswitch_data_0

    .line 1758
    iget-object v1, p0, Lode;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1759
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lode;->ah:Ljava/util/List;

    .line 1762
    :cond_1
    iget-object v1, p0, Lode;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1764
    :sswitch_0
    return-object p0

    .line 1769
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lode;->a:Ljava/lang/String;

    goto :goto_0

    .line 1773
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lode;->i:Ljava/lang/Boolean;

    goto :goto_0

    .line 1777
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lode;->b:Ljava/lang/Boolean;

    goto :goto_0

    .line 1781
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lode;->c:Ljava/lang/Boolean;

    goto :goto_0

    .line 1785
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lode;->j:Ljava/lang/String;

    goto :goto_0

    .line 1789
    :sswitch_6
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lode;->k:Ljava/lang/String;

    goto :goto_0

    .line 1793
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lode;->d:Ljava/lang/String;

    goto :goto_0

    .line 1797
    :sswitch_8
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lode;->e:Ljava/lang/String;

    goto :goto_0

    .line 1801
    :sswitch_9
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lode;->f:Ljava/lang/Boolean;

    goto :goto_0

    .line 1805
    :sswitch_a
    iget-object v0, p0, Lode;->g:Loxz;

    if-nez v0, :cond_2

    .line 1806
    new-instance v0, Loxz;

    invoke-direct {v0}, Loxz;-><init>()V

    iput-object v0, p0, Lode;->g:Loxz;

    .line 1808
    :cond_2
    iget-object v0, p0, Lode;->g:Loxz;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1812
    :sswitch_b
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1813
    if-eqz v0, :cond_3

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    const/16 v1, 0x6d

    if-eq v0, v1, :cond_3

    const/16 v1, 0x6e

    if-eq v0, v1, :cond_3

    const/16 v1, 0x67

    if-eq v0, v1, :cond_3

    const/4 v1, 0x4

    if-eq v0, v1, :cond_3

    const/16 v1, 0x6f

    if-eq v0, v1, :cond_3

    const/4 v1, 0x5

    if-eq v0, v1, :cond_3

    const/4 v1, 0x6

    if-eq v0, v1, :cond_3

    const/4 v1, 0x7

    if-eq v0, v1, :cond_3

    const/16 v1, 0x8

    if-eq v0, v1, :cond_3

    const/16 v1, 0x9

    if-eq v0, v1, :cond_3

    const/16 v1, 0xa

    if-eq v0, v1, :cond_3

    const/16 v1, 0xb

    if-eq v0, v1, :cond_3

    const/16 v1, 0xc

    if-eq v0, v1, :cond_3

    const/16 v1, 0xd

    if-eq v0, v1, :cond_3

    const/16 v1, 0xe

    if-eq v0, v1, :cond_3

    const/16 v1, 0xf

    if-eq v0, v1, :cond_3

    const/16 v1, 0x10

    if-eq v0, v1, :cond_3

    const/16 v1, 0x11

    if-eq v0, v1, :cond_3

    const/16 v1, 0x12

    if-eq v0, v1, :cond_3

    const/16 v1, 0x13

    if-eq v0, v1, :cond_3

    const/16 v1, 0x14

    if-eq v0, v1, :cond_3

    const/16 v1, 0x70

    if-eq v0, v1, :cond_3

    const/16 v1, 0x15

    if-eq v0, v1, :cond_3

    const/16 v1, 0x16

    if-eq v0, v1, :cond_3

    const/16 v1, 0x17

    if-eq v0, v1, :cond_3

    const/16 v1, 0x18

    if-eq v0, v1, :cond_3

    const/16 v1, 0x19

    if-eq v0, v1, :cond_3

    const/16 v1, 0x1a

    if-eq v0, v1, :cond_3

    const/16 v1, 0x1b

    if-eq v0, v1, :cond_3

    const/16 v1, 0x1c

    if-eq v0, v1, :cond_3

    const/16 v1, 0x1d

    if-eq v0, v1, :cond_3

    const/16 v1, 0x1e

    if-eq v0, v1, :cond_3

    const/16 v1, 0x1f

    if-eq v0, v1, :cond_3

    const/16 v1, 0x20

    if-eq v0, v1, :cond_3

    const/16 v1, 0x21

    if-eq v0, v1, :cond_3

    const/16 v1, 0x22

    if-eq v0, v1, :cond_3

    const/16 v1, 0x23

    if-eq v0, v1, :cond_3

    const/16 v1, 0x24

    if-eq v0, v1, :cond_3

    const/16 v1, 0x25

    if-eq v0, v1, :cond_3

    const/16 v1, 0x26

    if-eq v0, v1, :cond_3

    const/16 v1, 0x27

    if-eq v0, v1, :cond_3

    const/16 v1, 0x28

    if-eq v0, v1, :cond_3

    const/16 v1, 0x29

    if-eq v0, v1, :cond_3

    const/16 v1, 0x71

    if-eq v0, v1, :cond_3

    const/16 v1, 0x68

    if-eq v0, v1, :cond_3

    const/16 v1, 0x2a

    if-eq v0, v1, :cond_3

    const/16 v1, 0x2b

    if-eq v0, v1, :cond_3

    const/16 v1, 0x2c

    if-eq v0, v1, :cond_3

    const/16 v1, 0x2d

    if-eq v0, v1, :cond_3

    const/16 v1, 0x2e

    if-eq v0, v1, :cond_3

    const/16 v1, 0x2f

    if-eq v0, v1, :cond_3

    const/16 v1, 0x30

    if-eq v0, v1, :cond_3

    const/16 v1, 0x31

    if-eq v0, v1, :cond_3

    const/16 v1, 0x32

    if-eq v0, v1, :cond_3

    const/16 v1, 0x33

    if-eq v0, v1, :cond_3

    const/16 v1, 0x34

    if-eq v0, v1, :cond_3

    const/16 v1, 0x35

    if-eq v0, v1, :cond_3

    const/16 v1, 0x36

    if-eq v0, v1, :cond_3

    const/16 v1, 0x37

    if-eq v0, v1, :cond_3

    const/16 v1, 0x38

    if-eq v0, v1, :cond_3

    const/16 v1, 0x39

    if-eq v0, v1, :cond_3

    const/16 v1, 0x3a

    if-eq v0, v1, :cond_3

    const/16 v1, 0x3b

    if-eq v0, v1, :cond_3

    const/16 v1, 0x72

    if-eq v0, v1, :cond_3

    const/16 v1, 0x3c

    if-eq v0, v1, :cond_3

    const/16 v1, 0x3d

    if-eq v0, v1, :cond_3

    const/16 v1, 0x3e

    if-eq v0, v1, :cond_3

    const/16 v1, 0x3f

    if-eq v0, v1, :cond_3

    const/16 v1, 0x40

    if-eq v0, v1, :cond_3

    const/16 v1, 0x73

    if-eq v0, v1, :cond_3

    const/16 v1, 0x41

    if-eq v0, v1, :cond_3

    const/16 v1, 0x42

    if-eq v0, v1, :cond_3

    const/16 v1, 0x43

    if-eq v0, v1, :cond_3

    const/16 v1, 0x44

    if-eq v0, v1, :cond_3

    const/16 v1, 0x45

    if-eq v0, v1, :cond_3

    const/16 v1, 0x46

    if-eq v0, v1, :cond_3

    const/16 v1, 0x47

    if-eq v0, v1, :cond_3

    const/16 v1, 0x48

    if-eq v0, v1, :cond_3

    const/16 v1, 0x49

    if-eq v0, v1, :cond_3

    const/16 v1, 0x4a

    if-eq v0, v1, :cond_3

    const/16 v1, 0x4b

    if-eq v0, v1, :cond_3

    const/16 v1, 0x4c

    if-eq v0, v1, :cond_3

    const/16 v1, 0x4d

    if-eq v0, v1, :cond_3

    const/16 v1, 0x4e

    if-eq v0, v1, :cond_3

    const/16 v1, 0x4f

    if-eq v0, v1, :cond_3

    const/16 v1, 0x50

    if-eq v0, v1, :cond_3

    const/16 v1, 0x51

    if-eq v0, v1, :cond_3

    const/16 v1, 0x52

    if-eq v0, v1, :cond_3

    const/16 v1, 0x53

    if-eq v0, v1, :cond_3

    const/16 v1, 0x54

    if-eq v0, v1, :cond_3

    const/16 v1, 0x55

    if-eq v0, v1, :cond_3

    const/16 v1, 0x56

    if-eq v0, v1, :cond_3

    const/16 v1, 0x57

    if-eq v0, v1, :cond_3

    const/16 v1, 0x58

    if-eq v0, v1, :cond_3

    const/16 v1, 0x59

    if-eq v0, v1, :cond_3

    const/16 v1, 0x74

    if-eq v0, v1, :cond_3

    const/16 v1, 0x5a

    if-eq v0, v1, :cond_3

    const/16 v1, 0x5b

    if-eq v0, v1, :cond_3

    const/16 v1, 0x5c

    if-eq v0, v1, :cond_3

    const/16 v1, 0x5d

    if-eq v0, v1, :cond_3

    const/16 v1, 0x5e

    if-eq v0, v1, :cond_3

    const/16 v1, 0x5f

    if-eq v0, v1, :cond_3

    const/16 v1, 0x69

    if-eq v0, v1, :cond_3

    const/16 v1, 0x60

    if-eq v0, v1, :cond_3

    const/16 v1, 0x61

    if-eq v0, v1, :cond_3

    const/16 v1, 0x62

    if-eq v0, v1, :cond_3

    const/16 v1, 0x63

    if-eq v0, v1, :cond_3

    const/16 v1, 0x6a

    if-eq v0, v1, :cond_3

    const/16 v1, 0x6b

    if-eq v0, v1, :cond_3

    const/16 v1, 0x64

    if-eq v0, v1, :cond_3

    const/16 v1, 0x6c

    if-eq v0, v1, :cond_3

    const/16 v1, 0x65

    if-eq v0, v1, :cond_3

    const/16 v1, 0x66

    if-ne v0, v1, :cond_4

    .line 1930
    :cond_3
    iput v0, p0, Lode;->l:I

    goto/16 :goto_0

    .line 1932
    :cond_4
    const/4 v0, 0x0

    iput v0, p0, Lode;->l:I

    goto/16 :goto_0

    .line 1937
    :sswitch_c
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lode;->m:Ljava/lang/String;

    goto/16 :goto_0

    .line 1941
    :sswitch_d
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lode;->n:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 1945
    :sswitch_e
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lode;->h:Ljava/lang/String;

    goto/16 :goto_0

    .line 1754
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x58 -> :sswitch_b
        0x62 -> :sswitch_c
        0x68 -> :sswitch_d
        0x72 -> :sswitch_e
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1638
    iget-object v0, p0, Lode;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1639
    const/4 v0, 0x1

    iget-object v1, p0, Lode;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1641
    :cond_0
    iget-object v0, p0, Lode;->i:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 1642
    const/4 v0, 0x2

    iget-object v1, p0, Lode;->i:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 1644
    :cond_1
    iget-object v0, p0, Lode;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 1645
    const/4 v0, 0x3

    iget-object v1, p0, Lode;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 1647
    :cond_2
    iget-object v0, p0, Lode;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 1648
    const/4 v0, 0x4

    iget-object v1, p0, Lode;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 1650
    :cond_3
    iget-object v0, p0, Lode;->j:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 1651
    const/4 v0, 0x5

    iget-object v1, p0, Lode;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1653
    :cond_4
    iget-object v0, p0, Lode;->k:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 1654
    const/4 v0, 0x6

    iget-object v1, p0, Lode;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1656
    :cond_5
    iget-object v0, p0, Lode;->d:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 1657
    const/4 v0, 0x7

    iget-object v1, p0, Lode;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1659
    :cond_6
    iget-object v0, p0, Lode;->e:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 1660
    const/16 v0, 0x8

    iget-object v1, p0, Lode;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1662
    :cond_7
    iget-object v0, p0, Lode;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    .line 1663
    const/16 v0, 0x9

    iget-object v1, p0, Lode;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 1665
    :cond_8
    iget-object v0, p0, Lode;->g:Loxz;

    if-eqz v0, :cond_9

    .line 1666
    const/16 v0, 0xa

    iget-object v1, p0, Lode;->g:Loxz;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1668
    :cond_9
    iget v0, p0, Lode;->l:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_a

    .line 1669
    const/16 v0, 0xb

    iget v1, p0, Lode;->l:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1671
    :cond_a
    iget-object v0, p0, Lode;->m:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 1672
    const/16 v0, 0xc

    iget-object v1, p0, Lode;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1674
    :cond_b
    iget-object v0, p0, Lode;->n:Ljava/lang/Boolean;

    if-eqz v0, :cond_c

    .line 1675
    const/16 v0, 0xd

    iget-object v1, p0, Lode;->n:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 1677
    :cond_c
    iget-object v0, p0, Lode;->h:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 1678
    const/16 v0, 0xe

    iget-object v1, p0, Lode;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1680
    :cond_d
    iget-object v0, p0, Lode;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1682
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1599
    invoke-virtual {p0, p1}, Lode;->a(Loxn;)Lode;

    move-result-object v0

    return-object v0
.end method
