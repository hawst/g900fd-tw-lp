.class final Lhvx;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static w:Lhvx;


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:I

.field public final f:I

.field public final g:Landroid/graphics/Paint;

.field public final h:Landroid/graphics/Paint;

.field public final i:I

.field public final j:I

.field public final k:I

.field public final l:Landroid/graphics/drawable/Drawable;

.field public final m:I

.field public final n:I

.field public final o:Landroid/graphics/drawable/NinePatchDrawable;

.field public final p:I

.field public final q:I

.field public final r:Landroid/graphics/Paint;

.field public final s:I

.field public final t:Landroid/graphics/Paint;

.field public final u:I

.field public final v:I


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    const v4, 0x7f0b0101

    const v3, 0x7f0d019c

    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 103
    const v0, 0x7f0b0286

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    .line 104
    const v0, 0x7f0d0192

    .line 105
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lhvx;->a:I

    .line 106
    const v0, 0x7f0d0191

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lhvx;->b:I

    .line 108
    const v0, 0x7f0d0187

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lhvx;->c:I

    .line 109
    const v0, 0x7f0d019b

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lhvx;->d:I

    .line 111
    const v0, 0x7f0b0197

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lhvx;->e:I

    .line 112
    const v0, 0x7f0b0149

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lhvx;->f:I

    .line 114
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lhvx;->g:Landroid/graphics/Paint;

    .line 115
    iget-object v0, p0, Lhvx;->g:Landroid/graphics/Paint;

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 116
    iget-object v0, p0, Lhvx;->g:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 117
    iget-object v0, p0, Lhvx;->g:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 119
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lhvx;->h:Landroid/graphics/Paint;

    .line 120
    iget-object v0, p0, Lhvx;->h:Landroid/graphics/Paint;

    const v2, 0x7f0b010a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 121
    iget-object v0, p0, Lhvx;->h:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 122
    const v0, 0x7f0d0193

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lhvx;->i:I

    .line 125
    const v0, 0x7f0d0197

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lhvx;->j:I

    .line 126
    const v0, 0x7f0d0198

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lhvx;->k:I

    .line 128
    const v0, 0x7f0c004b

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lhvx;->m:I

    .line 129
    const v0, 0x7f0c004c

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lhvx;->n:I

    .line 131
    const v0, 0x7f020433

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lhvx;->l:Landroid/graphics/drawable/Drawable;

    .line 133
    const v0, 0x7f020475

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/NinePatchDrawable;

    iput-object v0, p0, Lhvx;->o:Landroid/graphics/drawable/NinePatchDrawable;

    .line 136
    const v0, 0x7f0d0190

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lhvx;->p:I

    .line 138
    const v0, 0x7f0c0049

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lhvx;->q:I

    .line 140
    new-instance v0, Landroid/graphics/Paint;

    const/4 v2, 0x2

    invoke-direct {v0, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lhvx;->r:Landroid/graphics/Paint;

    .line 142
    const v0, 0x7f0c004a

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lhvx;->s:I

    .line 144
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lhvx;->t:Landroid/graphics/Paint;

    .line 145
    iget-object v0, p0, Lhvx;->t:Landroid/graphics/Paint;

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 146
    iget-object v0, p0, Lhvx;->t:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 147
    iget-object v0, p0, Lhvx;->t:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 148
    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lhvx;->u:I

    .line 150
    const v0, 0x7f0d0194

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lhvx;->v:I

    .line 151
    return-void
.end method

.method public static a(Landroid/content/Context;)Lhvx;
    .locals 1

    .prologue
    .line 91
    sget-object v0, Lhvx;->w:Lhvx;

    if-nez v0, :cond_0

    .line 92
    new-instance v0, Lhvx;

    invoke-direct {v0, p0}, Lhvx;-><init>(Landroid/content/Context;)V

    sput-object v0, Lhvx;->w:Lhvx;

    .line 94
    :cond_0
    sget-object v0, Lhvx;->w:Lhvx;

    return-object v0
.end method
