.class public final Lnfq;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lnym;

.field public b:Ljava/lang/String;

.field private c:[Lnym;

.field private d:Ljava/lang/Boolean;

.field private e:Ljava/lang/Boolean;

.field private f:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 695
    invoke-direct {p0}, Loxq;-><init>()V

    .line 698
    sget-object v0, Lnym;->a:[Lnym;

    iput-object v0, p0, Lnfq;->c:[Lnym;

    .line 703
    sget-object v0, Lnym;->a:[Lnym;

    iput-object v0, p0, Lnfq;->a:[Lnym;

    .line 695
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 746
    .line 747
    iget-object v0, p0, Lnfq;->c:[Lnym;

    if-eqz v0, :cond_1

    .line 748
    iget-object v3, p0, Lnfq;->c:[Lnym;

    array-length v4, v3

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 749
    if-eqz v5, :cond_0

    .line 750
    const/4 v6, 0x1

    .line 751
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 748
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 755
    :cond_2
    iget-object v2, p0, Lnfq;->d:Ljava/lang/Boolean;

    if-eqz v2, :cond_3

    .line 756
    const/4 v2, 0x2

    iget-object v3, p0, Lnfq;->d:Ljava/lang/Boolean;

    .line 757
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 759
    :cond_3
    iget-object v2, p0, Lnfq;->a:[Lnym;

    if-eqz v2, :cond_5

    .line 760
    iget-object v2, p0, Lnfq;->a:[Lnym;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    .line 761
    if-eqz v4, :cond_4

    .line 762
    const/4 v5, 0x3

    .line 763
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 760
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 767
    :cond_5
    iget-object v1, p0, Lnfq;->b:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 768
    const/4 v1, 0x4

    iget-object v2, p0, Lnfq;->b:Ljava/lang/String;

    .line 769
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 771
    :cond_6
    iget-object v1, p0, Lnfq;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    .line 772
    const/4 v1, 0x5

    iget-object v2, p0, Lnfq;->e:Ljava/lang/Boolean;

    .line 773
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 775
    :cond_7
    iget-object v1, p0, Lnfq;->f:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    .line 776
    const/4 v1, 0x6

    iget-object v2, p0, Lnfq;->f:Ljava/lang/Boolean;

    .line 777
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 779
    :cond_8
    iget-object v1, p0, Lnfq;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 780
    iput v0, p0, Lnfq;->ai:I

    .line 781
    return v0
.end method

.method public a(Loxn;)Lnfq;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 789
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 790
    sparse-switch v0, :sswitch_data_0

    .line 794
    iget-object v2, p0, Lnfq;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 795
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnfq;->ah:Ljava/util/List;

    .line 798
    :cond_1
    iget-object v2, p0, Lnfq;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 800
    :sswitch_0
    return-object p0

    .line 805
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 806
    iget-object v0, p0, Lnfq;->c:[Lnym;

    if-nez v0, :cond_3

    move v0, v1

    .line 807
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnym;

    .line 808
    iget-object v3, p0, Lnfq;->c:[Lnym;

    if-eqz v3, :cond_2

    .line 809
    iget-object v3, p0, Lnfq;->c:[Lnym;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 811
    :cond_2
    iput-object v2, p0, Lnfq;->c:[Lnym;

    .line 812
    :goto_2
    iget-object v2, p0, Lnfq;->c:[Lnym;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 813
    iget-object v2, p0, Lnfq;->c:[Lnym;

    new-instance v3, Lnym;

    invoke-direct {v3}, Lnym;-><init>()V

    aput-object v3, v2, v0

    .line 814
    iget-object v2, p0, Lnfq;->c:[Lnym;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 815
    invoke-virtual {p1}, Loxn;->a()I

    .line 812
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 806
    :cond_3
    iget-object v0, p0, Lnfq;->c:[Lnym;

    array-length v0, v0

    goto :goto_1

    .line 818
    :cond_4
    iget-object v2, p0, Lnfq;->c:[Lnym;

    new-instance v3, Lnym;

    invoke-direct {v3}, Lnym;-><init>()V

    aput-object v3, v2, v0

    .line 819
    iget-object v2, p0, Lnfq;->c:[Lnym;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 823
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnfq;->d:Ljava/lang/Boolean;

    goto :goto_0

    .line 827
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 828
    iget-object v0, p0, Lnfq;->a:[Lnym;

    if-nez v0, :cond_6

    move v0, v1

    .line 829
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lnym;

    .line 830
    iget-object v3, p0, Lnfq;->a:[Lnym;

    if-eqz v3, :cond_5

    .line 831
    iget-object v3, p0, Lnfq;->a:[Lnym;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 833
    :cond_5
    iput-object v2, p0, Lnfq;->a:[Lnym;

    .line 834
    :goto_4
    iget-object v2, p0, Lnfq;->a:[Lnym;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    .line 835
    iget-object v2, p0, Lnfq;->a:[Lnym;

    new-instance v3, Lnym;

    invoke-direct {v3}, Lnym;-><init>()V

    aput-object v3, v2, v0

    .line 836
    iget-object v2, p0, Lnfq;->a:[Lnym;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 837
    invoke-virtual {p1}, Loxn;->a()I

    .line 834
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 828
    :cond_6
    iget-object v0, p0, Lnfq;->a:[Lnym;

    array-length v0, v0

    goto :goto_3

    .line 840
    :cond_7
    iget-object v2, p0, Lnfq;->a:[Lnym;

    new-instance v3, Lnym;

    invoke-direct {v3}, Lnym;-><init>()V

    aput-object v3, v2, v0

    .line 841
    iget-object v2, p0, Lnfq;->a:[Lnym;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 845
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnfq;->b:Ljava/lang/String;

    goto/16 :goto_0

    .line 849
    :sswitch_5
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnfq;->e:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 853
    :sswitch_6
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnfq;->f:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 790
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 714
    iget-object v1, p0, Lnfq;->c:[Lnym;

    if-eqz v1, :cond_1

    .line 715
    iget-object v2, p0, Lnfq;->c:[Lnym;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 716
    if-eqz v4, :cond_0

    .line 717
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 715
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 721
    :cond_1
    iget-object v1, p0, Lnfq;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 722
    const/4 v1, 0x2

    iget-object v2, p0, Lnfq;->d:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IZ)V

    .line 724
    :cond_2
    iget-object v1, p0, Lnfq;->a:[Lnym;

    if-eqz v1, :cond_4

    .line 725
    iget-object v1, p0, Lnfq;->a:[Lnym;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 726
    if-eqz v3, :cond_3

    .line 727
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 725
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 731
    :cond_4
    iget-object v0, p0, Lnfq;->b:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 732
    const/4 v0, 0x4

    iget-object v1, p0, Lnfq;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 734
    :cond_5
    iget-object v0, p0, Lnfq;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    .line 735
    const/4 v0, 0x5

    iget-object v1, p0, Lnfq;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 737
    :cond_6
    iget-object v0, p0, Lnfq;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    .line 738
    const/4 v0, 0x6

    iget-object v1, p0, Lnfq;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 740
    :cond_7
    iget-object v0, p0, Lnfq;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 742
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 691
    invoke-virtual {p0, p1}, Lnfq;->a(Loxn;)Lnfq;

    move-result-object v0

    return-object v0
.end method
