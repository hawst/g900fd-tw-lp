.class public final Llta;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:[Lltb;

.field private c:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Loxq;-><init>()V

    .line 16
    sget-object v0, Lltb;->a:[Lltb;

    iput-object v0, p0, Llta;->b:[Lltb;

    .line 9
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 40
    .line 41
    iget-object v0, p0, Llta;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 42
    const/4 v0, 0x1

    iget-object v2, p0, Llta;->c:Ljava/lang/Integer;

    .line 43
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 45
    :goto_0
    iget-object v2, p0, Llta;->a:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 46
    const/4 v2, 0x2

    iget-object v3, p0, Llta;->a:Ljava/lang/String;

    .line 47
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 49
    :cond_0
    iget-object v2, p0, Llta;->b:[Lltb;

    if-eqz v2, :cond_2

    .line 50
    iget-object v2, p0, Llta;->b:[Lltb;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 51
    if-eqz v4, :cond_1

    .line 52
    const/4 v5, 0x3

    .line 53
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 50
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 57
    :cond_2
    iget-object v1, p0, Llta;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 58
    iput v0, p0, Llta;->ai:I

    .line 59
    return v0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Llta;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 67
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 68
    sparse-switch v0, :sswitch_data_0

    .line 72
    iget-object v2, p0, Llta;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 73
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Llta;->ah:Ljava/util/List;

    .line 76
    :cond_1
    iget-object v2, p0, Llta;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 78
    :sswitch_0
    return-object p0

    .line 83
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Llta;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 87
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llta;->a:Ljava/lang/String;

    goto :goto_0

    .line 91
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 92
    iget-object v0, p0, Llta;->b:[Lltb;

    if-nez v0, :cond_3

    move v0, v1

    .line 93
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lltb;

    .line 94
    iget-object v3, p0, Llta;->b:[Lltb;

    if-eqz v3, :cond_2

    .line 95
    iget-object v3, p0, Llta;->b:[Lltb;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 97
    :cond_2
    iput-object v2, p0, Llta;->b:[Lltb;

    .line 98
    :goto_2
    iget-object v2, p0, Llta;->b:[Lltb;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 99
    iget-object v2, p0, Llta;->b:[Lltb;

    new-instance v3, Lltb;

    invoke-direct {v3}, Lltb;-><init>()V

    aput-object v3, v2, v0

    .line 100
    iget-object v2, p0, Llta;->b:[Lltb;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 101
    invoke-virtual {p1}, Loxn;->a()I

    .line 98
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 92
    :cond_3
    iget-object v0, p0, Llta;->b:[Lltb;

    array-length v0, v0

    goto :goto_1

    .line 104
    :cond_4
    iget-object v2, p0, Llta;->b:[Lltb;

    new-instance v3, Lltb;

    invoke-direct {v3}, Lltb;-><init>()V

    aput-object v3, v2, v0

    .line 105
    iget-object v2, p0, Llta;->b:[Lltb;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 68
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 21
    iget-object v0, p0, Llta;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 22
    const/4 v0, 0x1

    iget-object v1, p0, Llta;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 24
    :cond_0
    iget-object v0, p0, Llta;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 25
    const/4 v0, 0x2

    iget-object v1, p0, Llta;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 27
    :cond_1
    iget-object v0, p0, Llta;->b:[Lltb;

    if-eqz v0, :cond_3

    .line 28
    iget-object v1, p0, Llta;->b:[Lltb;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 29
    if-eqz v3, :cond_2

    .line 30
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 28
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 34
    :cond_3
    iget-object v0, p0, Llta;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 36
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Llta;->a(Loxn;)Llta;

    move-result-object v0

    return-object v0
.end method
