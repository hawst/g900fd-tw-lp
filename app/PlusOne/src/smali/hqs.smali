.class final Lhqs;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final f:Lhqt;

.field private static final g:[Ljava/lang/String;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Landroid/net/Uri;

.field private final d:[Ljava/lang/String;

.field private final e:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v4, 0x0

    .line 819
    new-instance v1, Lhqt;

    const-wide/16 v2, -0x1

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v1 .. v7}, Lhqt;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;B)V

    sput-object v1, Lhqs;->f:Lhqt;

    .line 826
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "MAX(_id)"

    aput-object v1, v0, v7

    sput-object v0, Lhqs;->g:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 838
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 815
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lhqs;->d:[Ljava/lang/String;

    .line 839
    iput-object p1, p0, Lhqs;->a:Ljava/lang/String;

    .line 840
    iput-object p2, p0, Lhqs;->b:Ljava/lang/String;

    .line 841
    const-string v0, "photo"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 842
    const-string v0, "external"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 843
    sget-object v0, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iput-object v0, p0, Lhqs;->c:Landroid/net/Uri;

    .line 850
    :goto_0
    invoke-static {}, Lhqp;->e()[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhqs;->e:[Ljava/lang/String;

    .line 860
    :goto_1
    return-void

    .line 844
    :cond_0
    const-string v0, "phoneStorage"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 845
    sget-object v0, Lhsf;->a:Landroid/net/Uri;

    iput-object v0, p0, Lhqs;->c:Landroid/net/Uri;

    goto :goto_0

    .line 847
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid volume name; must be one of the defined volumes"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 851
    :cond_2
    const-string v0, "video"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 852
    const-string v0, "external"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 853
    sget-object v0, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iput-object v0, p0, Lhqs;->c:Landroid/net/Uri;

    .line 860
    :goto_2
    invoke-static {}, Lhqp;->f()[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhqs;->e:[Ljava/lang/String;

    goto :goto_1

    .line 854
    :cond_3
    const-string v0, "phoneStorage"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 855
    sget-object v0, Lhsf;->b:Landroid/net/Uri;

    iput-object v0, p0, Lhqs;->c:Landroid/net/Uri;

    goto :goto_2

    .line 857
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid volume name; must be one of the defined volumes"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 862
    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid media type; must be one of the defined types"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private a(Landroid/database/Cursor;)Lhqt;
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 914
    if-eqz p1, :cond_2

    :try_start_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 915
    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "/DCIM/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    .line 916
    if-eqz v0, :cond_0

    move-object v4, v5

    .line 918
    :goto_0
    if-eqz v0, :cond_1

    .line 920
    :goto_1
    const/4 v0, 0x4

    .line 921
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x5

    .line 922
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    const/4 v2, 0x3

    .line 923
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 920
    invoke-static {v0, v1, v2, v3}, Lhsf;->a(Ljava/lang/String;IJ)Ljava/lang/String;

    move-result-object v6

    .line 924
    new-instance v1, Lhqt;

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    const/4 v7, 0x0

    invoke-direct/range {v1 .. v7}, Lhqt;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 927
    invoke-static {p1}, Lifu;->a(Landroid/database/Cursor;)V

    .line 929
    :goto_2
    return-object v1

    .line 916
    :cond_0
    const/4 v1, 0x1

    .line 917
    :try_start_1
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 918
    :cond_1
    const/4 v0, 0x6

    .line 919
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v5

    goto :goto_1

    .line 927
    :cond_2
    invoke-static {p1}, Lifu;->a(Landroid/database/Cursor;)V

    .line 929
    sget-object v1, Lhqs;->f:Lhqt;

    goto :goto_2

    .line 927
    :catchall_0
    move-exception v0

    invoke-static {p1}, Lifu;->a(Landroid/database/Cursor;)V

    throw v0
.end method

.method static synthetic a(Lhqs;Landroid/content/ContentResolver;)Lhqt;
    .locals 6

    .prologue
    .line 807
    iget-object v0, p0, Lhqs;->d:[Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "-1"

    aput-object v2, v0, v1

    iget-object v1, p0, Lhqs;->c:Landroid/net/Uri;

    iget-object v2, p0, Lhqs;->e:[Ljava/lang/String;

    const-string v3, "_id > ? AND _data IS NOT NULL"

    iget-object v4, p0, Lhqs;->d:[Ljava/lang/String;

    const-string v5, "_id DESC LIMIT 1"

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-direct {p0, v0}, Lhqs;->a(Landroid/database/Cursor;)Lhqt;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lhqs;Landroid/content/ContentResolver;J)Lhqt;
    .locals 6

    .prologue
    .line 807
    iget-object v0, p0, Lhqs;->d:[Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    iget-object v1, p0, Lhqs;->c:Landroid/net/Uri;

    iget-object v2, p0, Lhqs;->e:[Ljava/lang/String;

    const-string v3, "_id > ? AND _data IS NOT NULL"

    iget-object v4, p0, Lhqs;->d:[Ljava/lang/String;

    const-string v5, "_id LIMIT 1"

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-direct {p0, v0}, Lhqs;->a(Landroid/database/Cursor;)Lhqt;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Lhrj;J)V
    .locals 8

    .prologue
    .line 936
    invoke-virtual {p1}, Lhrj;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 937
    new-instance v1, Landroid/content/ContentValues;

    const/4 v2, 0x3

    invoke-direct {v1, v2}, Landroid/content/ContentValues;-><init>(I)V

    .line 938
    const-string v2, "volume_name"

    iget-object v3, p0, Lhqs;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 939
    const-string v2, "media_type"

    iget-object v3, p0, Lhqs;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 940
    const-string v2, "last_media_id"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 943
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lhqs;->a:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lhqs;->b:Ljava/lang/String;

    aput-object v4, v2, v3

    .line 944
    const-string v3, "SELECT count(*) FROM media_tracker WHERE media_type = ? AND volume_name = ?"

    invoke-static {v0, v3, v2}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-nez v3, :cond_0

    .line 945
    const-string v2, "media_tracker"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 949
    :goto_0
    return-void

    .line 947
    :cond_0
    const-string v3, "media_tracker"

    const-string v4, "media_type = ? AND volume_name = ?"

    invoke-virtual {v0, v3, v1, v4, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0
.end method

.method public a(Landroid/content/ContentResolver;)Z
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 872
    .line 874
    :try_start_0
    iget-object v1, p0, Lhqs;->c:Landroid/net/Uri;

    sget-object v2, Lhqs;->g:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 876
    if-eqz v1, :cond_2

    .line 877
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    .line 884
    :goto_0
    invoke-static {v1}, Lifu;->a(Landroid/database/Cursor;)V

    .line 887
    :goto_1
    return v0

    :cond_0
    move v0, v6

    .line 877
    goto :goto_0

    :cond_1
    move v0, v6

    goto :goto_0

    .line 884
    :cond_2
    invoke-static {v1}, Lifu;->a(Landroid/database/Cursor;)V

    :goto_2
    move v0, v6

    .line 887
    goto :goto_1

    .line 880
    :catch_0
    move-exception v0

    move-object v0, v7

    :goto_3
    :try_start_2
    const-string v1, "iu.UploadsManager"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 881
    const-string v1, "exception loading config: "

    invoke-virtual {p0}, Lhqs;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 884
    :cond_3
    :goto_4
    invoke-static {v0}, Lifu;->a(Landroid/database/Cursor;)V

    goto :goto_2

    .line 881
    :cond_4
    :try_start_3
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_4

    .line 884
    :catchall_0
    move-exception v1

    move-object v7, v0

    move-object v0, v1

    :goto_5
    invoke-static {v7}, Lifu;->a(Landroid/database/Cursor;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_5

    :catchall_2
    move-exception v0

    move-object v7, v1

    goto :goto_5

    .line 880
    :catch_1
    move-exception v0

    move-object v0, v1

    goto :goto_3
.end method

.method public a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 952
    iget-object v0, p0, Lhqs;->a:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 957
    instance-of v1, p1, Lhqs;

    if-nez v1, :cond_1

    .line 962
    :cond_0
    :goto_0
    return v0

    .line 960
    :cond_1
    check-cast p1, Lhqs;

    .line 961
    iget-object v1, p1, Lhqs;->a:Ljava/lang/String;

    iget-object v2, p0, Lhqs;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p1, Lhqs;->b:Ljava/lang/String;

    iget-object v2, p0, Lhqs;->b:Ljava/lang/String;

    .line 962
    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 967
    iget-object v0, p0, Lhqs;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 969
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lhqs;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 970
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 975
    iget-object v0, p0, Lhqs;->a:Ljava/lang/String;

    iget-object v1, p0, Lhqs;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "-"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
