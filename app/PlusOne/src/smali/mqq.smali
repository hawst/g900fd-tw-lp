.class public final Lmqq;
.super Loxq;
.source "PG"


# instance fields
.field public a:I

.field private b:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 242
    invoke-direct {p0}, Loxq;-><init>()V

    .line 253
    const/high16 v0, -0x80000000

    iput v0, p0, Lmqq;->a:I

    .line 242
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 270
    const/4 v0, 0x0

    .line 271
    iget-object v1, p0, Lmqq;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 272
    const/4 v0, 0x1

    iget-object v1, p0, Lmqq;->b:Ljava/lang/Boolean;

    .line 273
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 275
    :cond_0
    iget v1, p0, Lmqq;->a:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_1

    .line 276
    const/4 v1, 0x2

    iget v2, p0, Lmqq;->a:I

    .line 277
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 279
    :cond_1
    iget-object v1, p0, Lmqq;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 280
    iput v0, p0, Lmqq;->ai:I

    .line 281
    return v0
.end method

.method public a(Loxn;)Lmqq;
    .locals 2

    .prologue
    .line 289
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 290
    sparse-switch v0, :sswitch_data_0

    .line 294
    iget-object v1, p0, Lmqq;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 295
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmqq;->ah:Ljava/util/List;

    .line 298
    :cond_1
    iget-object v1, p0, Lmqq;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 300
    :sswitch_0
    return-object p0

    .line 305
    :sswitch_1
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmqq;->b:Ljava/lang/Boolean;

    goto :goto_0

    .line 309
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 310
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 313
    :cond_2
    iput v0, p0, Lmqq;->a:I

    goto :goto_0

    .line 315
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lmqq;->a:I

    goto :goto_0

    .line 290
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 258
    iget-object v0, p0, Lmqq;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 259
    const/4 v0, 0x1

    iget-object v1, p0, Lmqq;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 261
    :cond_0
    iget v0, p0, Lmqq;->a:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 262
    const/4 v0, 0x2

    iget v1, p0, Lmqq;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 264
    :cond_1
    iget-object v0, p0, Lmqq;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 266
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 238
    invoke-virtual {p0, p1}, Lmqq;->a(Loxn;)Lmqq;

    move-result-object v0

    return-object v0
.end method
