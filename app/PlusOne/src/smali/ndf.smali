.class public final Lndf;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lndc;

.field public b:[Lndc;

.field public c:[Lndc;

.field public d:Ljava/lang/Long;

.field public e:Lndg;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 575
    invoke-direct {p0}, Loxq;-><init>()V

    .line 578
    sget-object v0, Lndc;->a:[Lndc;

    iput-object v0, p0, Lndf;->a:[Lndc;

    .line 581
    sget-object v0, Lndc;->a:[Lndc;

    iput-object v0, p0, Lndf;->b:[Lndc;

    .line 584
    sget-object v0, Lndc;->a:[Lndc;

    iput-object v0, p0, Lndf;->c:[Lndc;

    .line 589
    const/4 v0, 0x0

    iput-object v0, p0, Lndf;->e:Lndg;

    .line 575
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 627
    .line 628
    iget-object v0, p0, Lndf;->a:[Lndc;

    if-eqz v0, :cond_1

    .line 629
    iget-object v3, p0, Lndf;->a:[Lndc;

    array-length v4, v3

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 630
    if-eqz v5, :cond_0

    .line 631
    const/4 v6, 0x1

    .line 632
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 629
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 636
    :cond_2
    iget-object v2, p0, Lndf;->b:[Lndc;

    if-eqz v2, :cond_4

    .line 637
    iget-object v3, p0, Lndf;->b:[Lndc;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_4

    aget-object v5, v3, v2

    .line 638
    if-eqz v5, :cond_3

    .line 639
    const/4 v6, 0x2

    .line 640
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 637
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 644
    :cond_4
    iget-object v2, p0, Lndf;->c:[Lndc;

    if-eqz v2, :cond_6

    .line 645
    iget-object v2, p0, Lndf;->c:[Lndc;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    .line 646
    if-eqz v4, :cond_5

    .line 647
    const/4 v5, 0x3

    .line 648
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 645
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 652
    :cond_6
    iget-object v1, p0, Lndf;->d:Ljava/lang/Long;

    if-eqz v1, :cond_7

    .line 653
    const/4 v1, 0x4

    iget-object v2, p0, Lndf;->d:Ljava/lang/Long;

    .line 654
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 656
    :cond_7
    iget-object v1, p0, Lndf;->e:Lndg;

    if-eqz v1, :cond_8

    .line 657
    const/4 v1, 0x5

    iget-object v2, p0, Lndf;->e:Lndg;

    .line 658
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 660
    :cond_8
    iget-object v1, p0, Lndf;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 661
    iput v0, p0, Lndf;->ai:I

    .line 662
    return v0
.end method

.method public a(Loxn;)Lndf;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 670
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 671
    sparse-switch v0, :sswitch_data_0

    .line 675
    iget-object v2, p0, Lndf;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 676
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lndf;->ah:Ljava/util/List;

    .line 679
    :cond_1
    iget-object v2, p0, Lndf;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 681
    :sswitch_0
    return-object p0

    .line 686
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 687
    iget-object v0, p0, Lndf;->a:[Lndc;

    if-nez v0, :cond_3

    move v0, v1

    .line 688
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lndc;

    .line 689
    iget-object v3, p0, Lndf;->a:[Lndc;

    if-eqz v3, :cond_2

    .line 690
    iget-object v3, p0, Lndf;->a:[Lndc;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 692
    :cond_2
    iput-object v2, p0, Lndf;->a:[Lndc;

    .line 693
    :goto_2
    iget-object v2, p0, Lndf;->a:[Lndc;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 694
    iget-object v2, p0, Lndf;->a:[Lndc;

    new-instance v3, Lndc;

    invoke-direct {v3}, Lndc;-><init>()V

    aput-object v3, v2, v0

    .line 695
    iget-object v2, p0, Lndf;->a:[Lndc;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 696
    invoke-virtual {p1}, Loxn;->a()I

    .line 693
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 687
    :cond_3
    iget-object v0, p0, Lndf;->a:[Lndc;

    array-length v0, v0

    goto :goto_1

    .line 699
    :cond_4
    iget-object v2, p0, Lndf;->a:[Lndc;

    new-instance v3, Lndc;

    invoke-direct {v3}, Lndc;-><init>()V

    aput-object v3, v2, v0

    .line 700
    iget-object v2, p0, Lndf;->a:[Lndc;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 704
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 705
    iget-object v0, p0, Lndf;->b:[Lndc;

    if-nez v0, :cond_6

    move v0, v1

    .line 706
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lndc;

    .line 707
    iget-object v3, p0, Lndf;->b:[Lndc;

    if-eqz v3, :cond_5

    .line 708
    iget-object v3, p0, Lndf;->b:[Lndc;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 710
    :cond_5
    iput-object v2, p0, Lndf;->b:[Lndc;

    .line 711
    :goto_4
    iget-object v2, p0, Lndf;->b:[Lndc;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    .line 712
    iget-object v2, p0, Lndf;->b:[Lndc;

    new-instance v3, Lndc;

    invoke-direct {v3}, Lndc;-><init>()V

    aput-object v3, v2, v0

    .line 713
    iget-object v2, p0, Lndf;->b:[Lndc;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 714
    invoke-virtual {p1}, Loxn;->a()I

    .line 711
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 705
    :cond_6
    iget-object v0, p0, Lndf;->b:[Lndc;

    array-length v0, v0

    goto :goto_3

    .line 717
    :cond_7
    iget-object v2, p0, Lndf;->b:[Lndc;

    new-instance v3, Lndc;

    invoke-direct {v3}, Lndc;-><init>()V

    aput-object v3, v2, v0

    .line 718
    iget-object v2, p0, Lndf;->b:[Lndc;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 722
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 723
    iget-object v0, p0, Lndf;->c:[Lndc;

    if-nez v0, :cond_9

    move v0, v1

    .line 724
    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lndc;

    .line 725
    iget-object v3, p0, Lndf;->c:[Lndc;

    if-eqz v3, :cond_8

    .line 726
    iget-object v3, p0, Lndf;->c:[Lndc;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 728
    :cond_8
    iput-object v2, p0, Lndf;->c:[Lndc;

    .line 729
    :goto_6
    iget-object v2, p0, Lndf;->c:[Lndc;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_a

    .line 730
    iget-object v2, p0, Lndf;->c:[Lndc;

    new-instance v3, Lndc;

    invoke-direct {v3}, Lndc;-><init>()V

    aput-object v3, v2, v0

    .line 731
    iget-object v2, p0, Lndf;->c:[Lndc;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 732
    invoke-virtual {p1}, Loxn;->a()I

    .line 729
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 723
    :cond_9
    iget-object v0, p0, Lndf;->c:[Lndc;

    array-length v0, v0

    goto :goto_5

    .line 735
    :cond_a
    iget-object v2, p0, Lndf;->c:[Lndc;

    new-instance v3, Lndc;

    invoke-direct {v3}, Lndc;-><init>()V

    aput-object v3, v2, v0

    .line 736
    iget-object v2, p0, Lndf;->c:[Lndc;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 740
    :sswitch_4
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lndf;->d:Ljava/lang/Long;

    goto/16 :goto_0

    .line 744
    :sswitch_5
    iget-object v0, p0, Lndf;->e:Lndg;

    if-nez v0, :cond_b

    .line 745
    new-instance v0, Lndg;

    invoke-direct {v0}, Lndg;-><init>()V

    iput-object v0, p0, Lndf;->e:Lndg;

    .line 747
    :cond_b
    iget-object v0, p0, Lndf;->e:Lndg;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 671
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 594
    iget-object v1, p0, Lndf;->a:[Lndc;

    if-eqz v1, :cond_1

    .line 595
    iget-object v2, p0, Lndf;->a:[Lndc;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 596
    if-eqz v4, :cond_0

    .line 597
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 595
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 601
    :cond_1
    iget-object v1, p0, Lndf;->b:[Lndc;

    if-eqz v1, :cond_3

    .line 602
    iget-object v2, p0, Lndf;->b:[Lndc;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 603
    if-eqz v4, :cond_2

    .line 604
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 602
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 608
    :cond_3
    iget-object v1, p0, Lndf;->c:[Lndc;

    if-eqz v1, :cond_5

    .line 609
    iget-object v1, p0, Lndf;->c:[Lndc;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    .line 610
    if-eqz v3, :cond_4

    .line 611
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 609
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 615
    :cond_5
    iget-object v0, p0, Lndf;->d:Ljava/lang/Long;

    if-eqz v0, :cond_6

    .line 616
    const/4 v0, 0x4

    iget-object v1, p0, Lndf;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 618
    :cond_6
    iget-object v0, p0, Lndf;->e:Lndg;

    if-eqz v0, :cond_7

    .line 619
    const/4 v0, 0x5

    iget-object v1, p0, Lndf;->e:Lndg;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 621
    :cond_7
    iget-object v0, p0, Lndf;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 623
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 571
    invoke-virtual {p0, p1}, Lndf;->a(Loxn;)Lndf;

    move-result-object v0

    return-object v0
.end method
