.class public final Loio;
.super Loxq;
.source "PG"


# instance fields
.field private a:I

.field private b:[Ljava/lang/String;

.field private c:Loiq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 457
    invoke-direct {p0}, Loxq;-><init>()V

    .line 757
    const/high16 v0, -0x80000000

    iput v0, p0, Loio;->a:I

    .line 760
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Loio;->b:[Ljava/lang/String;

    .line 763
    const/4 v0, 0x0

    iput-object v0, p0, Loio;->c:Loiq;

    .line 457
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 785
    .line 786
    iget v0, p0, Loio;->a:I

    const/high16 v2, -0x80000000

    if-eq v0, v2, :cond_3

    .line 787
    const/4 v0, 0x1

    iget v2, p0, Loio;->a:I

    .line 788
    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 790
    :goto_0
    iget-object v2, p0, Loio;->b:[Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Loio;->b:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 792
    iget-object v3, p0, Loio;->b:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_0

    aget-object v5, v3, v1

    .line 794
    invoke-static {v5}, Loxo;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 792
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 796
    :cond_0
    add-int/2addr v0, v2

    .line 797
    iget-object v1, p0, Loio;->b:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 799
    :cond_1
    iget-object v1, p0, Loio;->c:Loiq;

    if-eqz v1, :cond_2

    .line 800
    const/4 v1, 0x3

    iget-object v2, p0, Loio;->c:Loiq;

    .line 801
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 803
    :cond_2
    iget-object v1, p0, Loio;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 804
    iput v0, p0, Loio;->ai:I

    .line 805
    return v0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Loio;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 813
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 814
    sparse-switch v0, :sswitch_data_0

    .line 818
    iget-object v1, p0, Loio;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 819
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Loio;->ah:Ljava/util/List;

    .line 822
    :cond_1
    iget-object v1, p0, Loio;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 824
    :sswitch_0
    return-object p0

    .line 829
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 830
    if-eq v0, v4, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 833
    :cond_2
    iput v0, p0, Loio;->a:I

    goto :goto_0

    .line 835
    :cond_3
    iput v4, p0, Loio;->a:I

    goto :goto_0

    .line 840
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 841
    iget-object v0, p0, Loio;->b:[Ljava/lang/String;

    array-length v0, v0

    .line 842
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    .line 843
    iget-object v2, p0, Loio;->b:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 844
    iput-object v1, p0, Loio;->b:[Ljava/lang/String;

    .line 845
    :goto_1
    iget-object v1, p0, Loio;->b:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_4

    .line 846
    iget-object v1, p0, Loio;->b:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 847
    invoke-virtual {p1}, Loxn;->a()I

    .line 845
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 850
    :cond_4
    iget-object v1, p0, Loio;->b:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    .line 854
    :sswitch_3
    iget-object v0, p0, Loio;->c:Loiq;

    if-nez v0, :cond_5

    .line 855
    new-instance v0, Loiq;

    invoke-direct {v0}, Loiq;-><init>()V

    iput-object v0, p0, Loio;->c:Loiq;

    .line 857
    :cond_5
    iget-object v0, p0, Loio;->c:Loiq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 814
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 768
    iget v0, p0, Loio;->a:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 769
    const/4 v0, 0x1

    iget v1, p0, Loio;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 771
    :cond_0
    iget-object v0, p0, Loio;->b:[Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 772
    iget-object v1, p0, Loio;->b:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 773
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->a(ILjava/lang/String;)V

    .line 772
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 776
    :cond_1
    iget-object v0, p0, Loio;->c:Loiq;

    if-eqz v0, :cond_2

    .line 777
    const/4 v0, 0x3

    iget-object v1, p0, Loio;->c:Loiq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 779
    :cond_2
    iget-object v0, p0, Loio;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 781
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 453
    invoke-virtual {p0, p1}, Loio;->a(Loxn;)Loio;

    move-result-object v0

    return-object v0
.end method
