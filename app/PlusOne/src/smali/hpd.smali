.class public Lhpd;
.super Lhos;
.source "PG"

# interfaces
.implements Licu;


# instance fields
.field private d:Z

.field private e:Lu;

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lae;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Lhos;-><init>(Landroid/content/Context;Lae;)V

    .line 54
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 86
    iget-object v0, p0, Lhpd;->b:Lae;

    const-string v1, "bg_task_progress_dialog"

    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lt;

    .line 87
    if-eqz v0, :cond_0

    .line 88
    invoke-virtual {v0}, Lt;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "arg_task_tag"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 89
    invoke-virtual {v0}, Lt;->a()V

    .line 91
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InlinedApi"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 157
    iput-boolean v6, p0, Lhpd;->c:Z

    .line 158
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 178
    :goto_0
    return-void

    .line 162
    :cond_0
    iget-object v0, p0, Lhpd;->a:Landroid/content/Context;

    const v1, 0x104000a

    .line 164
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const v4, 0x1080027

    const v5, 0x1010355

    move-object v0, p1

    move-object v1, p2

    .line 162
    invoke-static/range {v0 .. v5}, Llgr;->a(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;II)Llgr;

    move-result-object v0

    .line 169
    iget-object v1, p0, Lhpd;->e:Lu;

    if-eqz v1, :cond_1

    .line 170
    iget-object v1, p0, Lhpd;->e:Lu;

    invoke-virtual {v0, v1, v6}, Llgr;->a(Lu;I)V

    .line 172
    :cond_1
    iget-boolean v1, p0, Lhpd;->d:Z

    invoke-virtual {v0, v1}, Llgr;->b(Z)V

    .line 174
    :try_start_0
    iget-object v1, p0, Lhpd;->b:Lae;

    iget-object v2, p0, Lhpd;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Llgr;->a(Lae;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 175
    :catch_0
    move-exception v0

    .line 176
    const-string v1, "BackgroundTaskUiHelper"

    const-string v2, "AlertFragmentDialog.show threw exception"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 3

    .prologue
    .line 58
    iget-object v0, p0, Lhpd;->b:Lae;

    const-string v1, "bg_task_progress_dialog"

    .line 59
    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lt;

    .line 60
    if-nez v0, :cond_0

    .line 61
    invoke-virtual {p0, p1, p2}, Lhpd;->c(Ljava/lang/String;Ljava/lang/String;)Lt;

    move-result-object v0

    .line 62
    invoke-virtual {v0}, Lt;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "arg_task_tag"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    invoke-virtual {v0, p4}, Lt;->b(Z)V

    .line 64
    iget-object v1, p0, Lhpd;->b:Lae;

    const-string v2, "bg_task_progress_dialog"

    invoke-virtual {v0, v1, v2}, Lt;->a(Lae;Ljava/lang/String;)V

    .line 66
    :cond_0
    return-void
.end method

.method public a(Lu;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 128
    iput-object p1, p0, Lhpd;->e:Lu;

    .line 129
    iput-object p2, p0, Lhpd;->f:Ljava/lang/String;

    .line 130
    iput-boolean p3, p0, Lhpd;->d:Z

    .line 131
    return-void
.end method

.method public a(Landroid/content/Context;Lhoz;I)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 118
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lhoz;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 119
    iput-boolean v0, p0, Lhpd;->c:Z

    .line 120
    invoke-virtual {p2}, Lhoz;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0, p3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 121
    const/4 v0, 0x1

    .line 123
    :cond_0
    return v0
.end method

.method public a(Lhoz;)Z
    .locals 2

    .prologue
    .line 104
    invoke-static {p1}, Lhoz;->a(Lhoz;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lhoz;->b()Ljava/lang/Exception;

    move-result-object v0

    invoke-virtual {p0, v0}, Lhpd;->a(Ljava/lang/Exception;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105
    const/4 v0, 0x1

    .line 107
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lhpd;->a:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Lhpd;->a(Landroid/content/Context;Lhoz;I)Z

    move-result v0

    goto :goto_0
.end method

.method public a(Ljava/lang/Exception;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 143
    iget-object v0, p0, Lhpd;->a:Landroid/content/Context;

    const-class v2, Licv;

    .line 144
    invoke-static {v0, v2}, Llnh;->c(Landroid/content/Context;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    .line 145
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Licv;

    .line 146
    invoke-interface {v0, p1, p0}, Licv;->a(Ljava/lang/Exception;Licu;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 147
    iput-boolean v1, p0, Lhpd;->c:Z

    .line 148
    const/4 v0, 0x1

    .line 151
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public b()Landroid/content/Context;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lhpd;->a:Landroid/content/Context;

    return-object v0
.end method

.method public c()Lae;
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lhpd;->b:Lae;

    return-object v0
.end method

.method public c(Ljava/lang/String;Ljava/lang/String;)Lt;
    .locals 6

    .prologue
    .line 74
    const/4 v0, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    iget-object v5, p0, Lhpd;->e:Lu;

    move-object v1, p1

    move-object v2, p2

    invoke-static/range {v0 .. v5}, Llgt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLu;)Llgt;

    move-result-object v0

    return-object v0
.end method
