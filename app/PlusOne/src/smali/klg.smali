.class final Lklg;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhle;
.implements Ljhl;


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:I

.field private final d:Lkno;

.field private final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljac;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lhei;

.field private h:Lhlf;

.field private i:J

.field private j:J

.field private k:I

.field private l:I

.field private m:I

.field private n:I

.field private o:Z

.field private p:I

.field private q:J

.field private r:J

.field private s:Z

.field private t:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 75
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "media_url"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "bytes_uploaded"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "bytes_total"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "upload_state"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "upload_finish_time"

    aput-object v2, v0, v1

    sput-object v0, Lklg;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILkno;)V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    const/4 v1, 0x0

    .line 135
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    iput-wide v2, p0, Lklg;->i:J

    .line 106
    iput-wide v2, p0, Lklg;->j:J

    .line 136
    iput-object p1, p0, Lklg;->b:Landroid/content/Context;

    .line 137
    iput p2, p0, Lklg;->c:I

    .line 138
    iput-object p3, p0, Lklg;->d:Lkno;

    .line 140
    iget-object v0, p3, Lkno;->e:[Lknp;

    array-length v0, v0

    .line 141
    if-lez v0, :cond_1

    .line 142
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(I)V

    iput-object v1, p0, Lklg;->e:Ljava/util/Set;

    .line 143
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1, v0}, Ljava/util/HashMap;-><init>(I)V

    iput-object v1, p0, Lklg;->f:Ljava/util/Map;

    .line 144
    invoke-direct {p0}, Lklg;->h()V

    .line 149
    :goto_0
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Lklg;->g:Lhei;

    .line 150
    const-class v0, Lieh;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    .line 151
    sget-object v1, Lkmx;->f:Lief;

    invoke-interface {v0, v1, p2}, Lieh;->b(Lief;I)Z

    move-result v1

    iput-boolean v1, p0, Lklg;->t:Z

    .line 153
    sget-object v1, Lkmx;->g:Lief;

    invoke-interface {v0, v1, p2}, Lieh;->b(Lief;I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lklg;->t:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lklg;->s:Z

    .line 156
    iget-boolean v0, p0, Lklg;->s:Z

    if-eqz v0, :cond_0

    .line 157
    const-class v0, Lhlf;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhlf;

    iput-object v0, p0, Lklg;->h:Lhlf;

    .line 159
    :cond_0
    return-void

    .line 146
    :cond_1
    iput-object v1, p0, Lklg;->e:Ljava/util/Set;

    .line 147
    iput-object v1, p0, Lklg;->f:Ljava/util/Map;

    goto :goto_0

    .line 153
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public constructor <init>(Landroid/content/Context;ILmhi;Ljava/util/Date;Ljava/lang/String;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Lmhi;",
            "Ljava/util/Date;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lizu;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 172
    invoke-static {p3, p4, p5, p6}, Lklg;->a(Lmhi;Ljava/util/Date;Ljava/lang/String;Ljava/util/List;)Lkno;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lklg;-><init>(Landroid/content/Context;ILkno;)V

    .line 174
    return-void
.end method

.method private static a(Lmhi;Ljava/util/Date;Ljava/lang/String;Ljava/util/List;)Lkno;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmhi;",
            "Ljava/util/Date;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lizu;",
            ">;)",
            "Lkno;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 186
    new-instance v3, Lkno;

    invoke-direct {v3}, Lkno;-><init>()V

    .line 187
    iput-object p0, v3, Lkno;->a:Lmhi;

    .line 188
    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v3, Lkno;->b:Ljava/lang/Long;

    .line 189
    iput-object p2, v3, Lkno;->c:Ljava/lang/String;

    .line 191
    if-eqz p3, :cond_1

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    move v1, v0

    .line 192
    :goto_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 194
    :goto_1
    if-ge v2, v1, :cond_2

    .line 195
    invoke-interface {p3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizu;

    .line 196
    invoke-virtual {v0}, Lizu;->j()Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {v0}, Lizu;->i()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 197
    new-instance v5, Lknp;

    invoke-direct {v5}, Lknp;-><init>()V

    .line 200
    invoke-virtual {v0}, Lizu;->g()Ljac;

    move-result-object v6

    iget v6, v6, Ljac;->e:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    iput-object v6, v5, Lknp;->b:Ljava/lang/Integer;

    .line 201
    invoke-virtual {v0}, Lizu;->e()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v5, Lknp;->c:Ljava/lang/String;

    .line 202
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 194
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    move v1, v2

    .line 191
    goto :goto_0

    .line 205
    :cond_2
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lknp;

    invoke-interface {v4, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lknp;

    iput-object v0, v3, Lkno;->e:[Lknp;

    .line 207
    return-object v3
.end method

.method private g()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 350
    iget-object v0, p0, Lklg;->d:Lkno;

    iget-object v0, v0, Lkno;->e:[Lknp;

    array-length v1, v0

    .line 351
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 352
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 353
    iget-object v3, p0, Lklg;->d:Lkno;

    iget-object v3, v3, Lkno;->e:[Lknp;

    aget-object v3, v3, v0

    .line 354
    iget-object v3, v3, Lknp;->c:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 352
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 356
    :cond_0
    return-object v2
.end method

.method private h()V
    .locals 6

    .prologue
    .line 386
    const/4 v0, 0x0

    iget-object v1, p0, Lklg;->d:Lkno;

    iget-object v1, v1, Lkno;->e:[Lknp;

    array-length v1, v1

    :goto_0
    if-ge v0, v1, :cond_0

    .line 387
    iget-object v2, p0, Lklg;->d:Lkno;

    iget-object v2, v2, Lkno;->e:[Lknp;

    aget-object v2, v2, v0

    .line 388
    iget-object v3, v2, Lknp;->b:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v3}, Ljac;->a(I)Ljac;

    move-result-object v3

    .line 389
    iget-object v4, p0, Lklg;->f:Ljava/util/Map;

    iget-object v2, v2, Lknp;->c:Ljava/lang/String;

    invoke-interface {v4, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 390
    sget-object v2, Lklh;->a:[I

    invoke-virtual {v3}, Ljac;->ordinal()I

    move-result v4

    aget v2, v2, v4

    packed-switch v2, :pswitch_data_0

    .line 400
    const-string v2, "NetworkQueueRequest"

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x14

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Unknown media type: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 386
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 394
    :pswitch_0
    iget v2, p0, Lklg;->k:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lklg;->k:I

    goto :goto_1

    .line 397
    :pswitch_1
    iget v2, p0, Lklg;->m:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lklg;->m:I

    goto :goto_1

    .line 403
    :cond_0
    return-void

    .line 390
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private i()V
    .locals 7

    .prologue
    const-wide/16 v2, -0x1

    .line 487
    iget-wide v0, p0, Lklg;->i:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-wide v0, p0, Lklg;->j:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 536
    :cond_0
    :goto_0
    return-void

    .line 493
    :cond_1
    iget-object v0, p0, Lklg;->d:Lkno;

    iget-object v0, v0, Lkno;->e:[Lknp;

    array-length v0, v0

    .line 494
    if-eqz v0, :cond_0

    .line 500
    iget-object v0, p0, Lklg;->d:Lkno;

    iget-object v0, v0, Lkno;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lklg;->d:Lkno;

    iget-object v0, v0, Lkno;->d:Ljava/lang/String;

    .line 501
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 505
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 506
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 507
    const/4 v0, 0x0

    iget-object v3, p0, Lklg;->d:Lkno;

    iget-object v3, v3, Lkno;->e:[Lknp;

    array-length v3, v3

    :goto_1
    if-ge v0, v3, :cond_2

    .line 508
    iget-object v4, p0, Lklg;->d:Lkno;

    iget-object v4, v4, Lkno;->e:[Lknp;

    aget-object v4, v4, v0

    .line 509
    iget-object v5, v4, Lknp;->b:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-static {v5}, Ljac;->a(I)Ljac;

    move-result-object v5

    .line 510
    sget-object v6, Lklh;->a:[I

    invoke-virtual {v5}, Ljac;->ordinal()I

    move-result v5

    aget v5, v6, v5

    packed-switch v5, :pswitch_data_0

    .line 507
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 514
    :pswitch_0
    iget-object v4, v4, Lknp;->c:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 517
    :pswitch_1
    iget-object v4, v4, Lknp;->c:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 523
    :cond_2
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 525
    iget-object v0, p0, Lklg;->h:Lhlf;

    invoke-virtual {p0}, Lklg;->f()I

    move-result v3

    iget-object v4, p0, Lklg;->d:Lkno;

    iget-object v4, v4, Lkno;->d:Ljava/lang/String;

    invoke-interface {v0, v3, v4, v1, p0}, Lhlf;->a(ILjava/lang/String;Ljava/util/Collection;Lhle;)J

    move-result-wide v0

    iput-wide v0, p0, Lklg;->i:J

    .line 530
    :cond_3
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 532
    iget-object v0, p0, Lklg;->h:Lhlf;

    invoke-virtual {p0}, Lklg;->f()I

    move-result v1

    iget-object v3, p0, Lklg;->d:Lkno;

    iget-object v3, v3, Lkno;->d:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-interface {v0, v1, v3, v2, v4}, Lhlf;->a(ILjava/lang/String;Ljava/util/Collection;Lhle;)J

    move-result-wide v0

    iput-wide v0, p0, Lklg;->j:J

    .line 534
    iget v0, p0, Lklg;->k:I

    iput v0, p0, Lklg;->l:I

    goto/16 :goto_0

    .line 510
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public a(Ljhn;Ljhm;)I
    .locals 10

    .prologue
    .line 242
    const/4 v7, 0x0

    .line 243
    const/4 v6, 0x0

    .line 244
    iget-object v0, p0, Lklg;->d:Lkno;

    iget-object v0, v0, Lkno;->e:[Lknp;

    array-length v0, v0

    if-nez v0, :cond_0

    const/4 v0, 0x2

    iput v0, p0, Lklg;->p:I

    .line 246
    :goto_0
    iget v0, p0, Lklg;->p:I

    packed-switch v0, :pswitch_data_0

    .line 294
    new-instance v0, Ljava/lang/RuntimeException;

    iget v1, p0, Lklg;->p:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x37

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Invalid upload status associated with item: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 244
    :cond_0
    iget-object v0, p0, Lklg;->g:Lhei;

    invoke-virtual {p0}, Lklg;->f()I

    move-result v1

    invoke-interface {v0, v1}, Lhei;->c(I)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x3

    iput v0, p0, Lklg;->p:I

    goto :goto_0

    :cond_1
    iget-boolean v0, p0, Lklg;->s:Z

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lklg;->i()V

    :cond_2
    :goto_1
    iget v0, p0, Lklg;->m:I

    iget v1, p0, Lklg;->n:I

    if-ne v0, v1, :cond_c

    const/4 v0, 0x1

    :goto_2
    if-eqz v0, :cond_10

    iget-boolean v0, p0, Lklg;->t:Z

    if-eqz v0, :cond_d

    const/4 v0, 0x2

    iput v0, p0, Lklg;->p:I

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lklg;->d:Lkno;

    iget-object v0, v0, Lkno;->e:[Lknp;

    array-length v1, v0

    if-nez v1, :cond_4

    const/4 v3, 0x0

    :goto_3
    if-nez v3, :cond_6

    const/4 v0, 0x2

    iput v0, p0, Lklg;->p:I

    goto :goto_1

    :cond_4
    invoke-static {}, Llsu;->a()Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v0, "media_url IN ( "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v0, 0x0

    :goto_4
    if-ge v0, v1, :cond_5

    iget-object v3, p0, Lklg;->d:Lkno;

    iget-object v3, v3, Lkno;->e:[Lknp;

    aget-object v3, v3, v0

    const-string v4, "\'"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v3, v3, Lknp;->c:Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\', "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    const-string v0, " ) AND upload_account_id = "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lklg;->f()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-static {v2}, Llsu;->a(Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v3

    goto :goto_3

    :cond_6
    iget-object v0, p0, Lklg;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lklg;->b:Landroid/content/Context;

    invoke-static {v1}, Lhqv;->e(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lklg;->a:[Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    const/4 v0, 0x0

    if-nez v2, :cond_1c

    :try_start_0
    const-string v0, "NetworkQueueRequest"

    const-string v1, "No media was found for query. Marking item upload status as done."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x2

    iput v0, p0, Lklg;->p:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_2

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1

    :pswitch_0
    :try_start_1
    iget v0, p0, Lklg;->l:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lklg;->l:I

    :cond_7
    :goto_5
    :sswitch_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_9

    const/4 v0, 0x3

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x21

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Unknown upload state: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    if-eqz v2, :cond_8

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_8
    throw v0

    :sswitch_1
    const/4 v0, 0x0

    :try_start_2
    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lklg;->e:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    const/4 v3, 0x4

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iget-wide v8, p0, Lklg;->q:J

    invoke-static {v8, v9, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    iput-wide v4, p0, Lklg;->q:J

    iget-object v3, p0, Lklg;->f:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljac;

    sget-object v3, Lklh;->a:[I

    invoke-virtual {v0}, Ljac;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_1

    const-string v3, "NetworkQueueRequest"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x14

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Unknown media type: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    :pswitch_1
    iget v0, p0, Lklg;->n:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lklg;->n:I

    goto/16 :goto_5

    :sswitch_2
    add-int/lit8 v0, v1, 0x1

    const-string v1, "NetworkQueueRequest"

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x39

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Upload failed for media. mediaUploadFailCount="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v0

    goto/16 :goto_5

    :sswitch_3
    const-string v0, "NetworkQueueRequest"

    const-string v3, "Media upload state is STATE_DONT_UPLOAD. This should never happen."

    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_5

    :cond_9
    if-eqz v2, :cond_a

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_a
    if-lez v1, :cond_b

    const/4 v0, 0x1

    :goto_6
    iput-boolean v0, p0, Lklg;->o:Z

    goto/16 :goto_1

    :cond_b
    const/4 v0, 0x0

    goto :goto_6

    :cond_c
    const/4 v0, 0x0

    goto/16 :goto_2

    :cond_d
    iget v0, p0, Lklg;->m:I

    if-lez v0, :cond_e

    iget-wide v0, p0, Lklg;->q:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_e

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lklg;->q:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0xdbba0

    sub-long v0, v2, v0

    iput-wide v0, p0, Lklg;->r:J

    :goto_7
    iget-wide v0, p0, Lklg;->r:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_f

    const/4 v0, 0x1

    :goto_8
    iput v0, p0, Lklg;->p:I

    goto/16 :goto_0

    :cond_e
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lklg;->r:J

    goto :goto_7

    :cond_f
    const/4 v0, 0x2

    goto :goto_8

    :cond_10
    iget-boolean v0, p0, Lklg;->o:Z

    if-eqz v0, :cond_11

    const/4 v0, 0x3

    iput v0, p0, Lklg;->p:I

    goto/16 :goto_0

    :cond_11
    iget-object v0, p0, Lklg;->d:Lkno;

    iget-object v0, v0, Lkno;->d:Ljava/lang/String;

    if-nez v0, :cond_12

    const/4 v0, 0x4

    iput v0, p0, Lklg;->p:I

    goto/16 :goto_0

    :cond_12
    const/4 v0, 0x0

    iput v0, p0, Lklg;->p:I

    goto/16 :goto_0

    .line 249
    :pswitch_2
    const/4 v0, 0x3

    move v1, v7

    move v2, v0

    move-object v0, v6

    .line 298
    :goto_9
    iget v3, p0, Lklg;->k:I

    iput v3, p1, Ljhn;->c:I

    iget v3, p0, Lklg;->l:I

    iput v3, p1, Ljhn;->d:I

    iget v3, p0, Lklg;->m:I

    iput v3, p1, Ljhn;->a:I

    iget v3, p0, Lklg;->n:I

    iput v3, p1, Ljhn;->b:I

    .line 299
    invoke-virtual {p2, v0}, Ljhm;->a(Ljava/lang/Exception;)V

    invoke-virtual {p2, v1}, Ljhm;->a(Z)V

    .line 300
    return v2

    .line 254
    :pswitch_3
    :try_start_3
    new-instance v0, Lklm;

    iget-object v1, p0, Lklg;->b:Landroid/content/Context;

    invoke-virtual {p0}, Lklg;->f()I

    move-result v2

    iget-object v3, p0, Lklg;->d:Lkno;

    iget-object v3, v3, Lkno;->a:Lmhi;

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lklm;-><init>(Landroid/content/Context;ILmhi;Lkey;)V

    invoke-virtual {v0}, Lklm;->l()V

    const-string v1, "NetworkQueueRequest"

    invoke-virtual {v0, v1}, Lklm;->e(Ljava/lang/String;)V

    invoke-virtual {v0}, Lklm;->D()Loxu;

    move-result-object v0

    check-cast v0, Lmhj;

    iget-object v1, v0, Lmhj;->a:Loda;

    iget-object v0, v1, Loda;->a:Logi;

    iget-object v2, v0, Logi;->a:[Logr;

    iget-object v0, p0, Lklg;->d:Lkno;

    iget-object v0, v0, Lkno;->e:[Lknp;

    array-length v0, v0

    if-lez v0, :cond_17

    iget-boolean v0, p0, Lklg;->s:Z

    if-eqz v0, :cond_16

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    iget-object v4, p0, Lklg;->d:Lkno;

    iget-object v4, v4, Lkno;->e:[Lknp;

    array-length v4, v4

    :goto_a
    if-ge v0, v4, :cond_13

    iget-object v5, p0, Lklg;->d:Lkno;

    iget-object v5, v5, Lkno;->e:[Lknp;

    aget-object v5, v5, v0

    iget-object v5, v5, Lknp;->c:Ljava/lang/String;

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    :cond_13
    const/4 v0, 0x0

    :goto_b
    array-length v4, v2

    if-ge v0, v4, :cond_17

    aget-object v4, v2, v0

    iget-object v5, v4, Logr;->r:Ljava/lang/String;

    if-eqz v5, :cond_14

    iget-object v5, v4, Logr;->r:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_15

    :cond_14
    const-string v4, "NetworkQueueRequest"

    const-string v5, "Empty update.albumId"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_c
    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    :cond_15
    iget-object v5, p0, Lklg;->h:Lhlf;

    invoke-virtual {p0}, Lklg;->f()I

    move-result v8

    iget-object v4, v4, Logr;->r:Ljava/lang/String;

    const/4 v9, 0x0

    invoke-interface {v5, v8, v4, v3, v9}, Lhlf;->a(ILjava/lang/String;Ljava/util/Collection;Lhle;)J
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_c

    .line 256
    :catch_0
    move-exception v0

    .line 257
    invoke-virtual {v0}, Ljava/io/IOException;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    instance-of v1, v1, Lkgf;

    if-eqz v1, :cond_1a

    .line 258
    const-string v1, "ACTIVITY_ALREADY_EXISTS"

    invoke-static {v0, v1}, Lkgf;->a(Ljava/lang/Exception;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_19

    .line 264
    const/4 v0, 0x1

    move v1, v7

    move v2, v0

    move-object v0, v6

    goto/16 :goto_9

    .line 254
    :cond_16
    :try_start_4
    invoke-direct {p0}, Lklg;->g()Ljava/util/List;

    move-result-object v3

    const/4 v0, 0x0

    :goto_d
    array-length v4, v2

    if-ge v0, v4, :cond_17

    aget-object v4, v2, v0

    iget-object v5, p0, Lklg;->b:Landroid/content/Context;

    invoke-virtual {p0}, Lklg;->f()I

    move-result v8

    iget-object v4, v4, Logr;->r:Ljava/lang/String;

    const/4 v9, 0x0

    invoke-static {v5, v8, v3, v4, v9}, Lhqd;->a(Landroid/content/Context;ILjava/util/List;Ljava/lang/String;Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_d

    :cond_17
    iget-object v0, p0, Lklg;->b:Landroid/content/Context;

    const-class v2, Lkmv;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkmv;

    iget-object v2, v1, Loda;->b:Loge;

    if-eqz v2, :cond_18

    invoke-virtual {p0}, Lklg;->f()I

    move-result v2

    iget-object v1, v1, Loda;->b:Loge;

    invoke-virtual {v0, v2, v1}, Lkmv;->a(ILoge;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 255
    :cond_18
    const/4 v0, 0x1

    move v1, v7

    move v2, v0

    move-object v0, v6

    .line 276
    goto/16 :goto_9

    .line 267
    :cond_19
    const-string v1, "NetworkQueueRequest"

    const-string v2, "Server error encountered when sending post."

    invoke-virtual {v0}, Ljava/io/IOException;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    invoke-static {v1, v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 268
    const/4 v1, 0x5

    move v2, v1

    move v1, v7

    .line 269
    goto/16 :goto_9

    .line 273
    :cond_1a
    const/4 v0, 0x1

    .line 274
    const/4 v1, 0x4

    move v2, v1

    move v1, v0

    move-object v0, v6

    .line 277
    goto/16 :goto_9

    .line 280
    :pswitch_4
    const/4 v0, 0x5

    move v1, v7

    move v2, v0

    move-object v0, v6

    .line 281
    goto/16 :goto_9

    .line 285
    :pswitch_5
    :try_start_5
    new-instance v0, Lkli;

    iget-object v1, p0, Lklg;->b:Landroid/content/Context;

    invoke-virtual {p0}, Lklg;->f()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lkli;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v0}, Lkli;->l()V

    const-string v1, "NetworkQueueRequest"

    invoke-virtual {v0, v1}, Lkli;->e(Ljava/lang/String;)V

    invoke-virtual {v0}, Lkli;->D()Loxu;

    move-result-object v0

    check-cast v0, Llzh;

    iget-object v0, v0, Llzh;->a:Lnff;

    iget-object v1, p0, Lklg;->d:Lkno;

    iget-object v0, v0, Lnff;->a:Lnyb;

    iget-object v0, v0, Lnyb;->d:Ljava/lang/String;

    iput-object v0, v1, Lkno;->d:Ljava/lang/String;

    iget-boolean v0, p0, Lklg;->s:Z

    if-eqz v0, :cond_1b

    invoke-direct {p0}, Lklg;->i()V

    .line 286
    :goto_e
    const/4 v0, 0x3

    move v1, v7

    move v2, v0

    move-object v0, v6

    .line 290
    goto/16 :goto_9

    .line 285
    :cond_1b
    iget-object v0, p0, Lklg;->b:Landroid/content/Context;

    invoke-virtual {p0}, Lklg;->f()I

    move-result v1

    invoke-direct {p0}, Lklg;->g()Ljava/util/List;

    move-result-object v2

    iget-object v3, p0, Lklg;->d:Lkno;

    iget-object v3, v3, Lkno;->d:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Lhqd;->a(Landroid/content/Context;ILjava/util/List;Ljava/lang/String;Z)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_e

    .line 288
    :catch_1
    move-exception v0

    const/4 v0, 0x1

    .line 289
    const/4 v1, 0x4

    move v2, v1

    move v1, v0

    move-object v0, v6

    .line 291
    goto/16 :goto_9

    :cond_1c
    move v1, v0

    goto/16 :goto_5

    .line 246
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch

    .line 244
    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_0
        0x12c -> :sswitch_2
        0x190 -> :sswitch_1
        0x1f4 -> :sswitch_3
        0x258 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 212
    const-string v0, "network_post_request_handle"

    return-object v0
.end method

.method public a(Lhky;)V
    .locals 6

    .prologue
    .line 540
    sget-object v0, Lklh;->b:[I

    invoke-virtual {p1}, Lhky;->b()Lhld;

    move-result-object v1

    invoke-virtual {v1}, Lhld;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 556
    :goto_0
    return-void

    .line 542
    :pswitch_0
    iget-object v0, p0, Lklg;->h:Lhlf;

    .line 543
    invoke-virtual {p1}, Lhky;->a()Lhkv;

    move-result-object v1

    invoke-interface {v0, v1}, Lhlf;->a(Lhkv;)Ljava/util/List;

    move-result-object v0

    .line 544
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhla;

    .line 545
    iget-wide v2, p0, Lklg;->q:J

    .line 546
    invoke-virtual {v0}, Lhla;->d()J

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    iput-wide v2, p0, Lklg;->q:J

    goto :goto_1

    .line 548
    :cond_0
    iget v0, p0, Lklg;->m:I

    iput v0, p0, Lklg;->n:I

    goto :goto_0

    .line 551
    :pswitch_1
    iget v0, p0, Lklg;->m:I

    invoke-virtual {p1}, Lhky;->c()I

    move-result v1

    sub-int/2addr v0, v1

    iput v0, p0, Lklg;->n:I

    .line 552
    const/4 v0, 0x1

    iput-boolean v0, p0, Lklg;->o:Z

    .line 553
    const-string v0, "NetworkQueueRequest"

    const-string v1, "Album upload service reported an error"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 540
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public b()I
    .locals 1

    .prologue
    .line 366
    const/4 v0, 0x0

    return v0
.end method

.method public c()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 372
    iget-object v0, p0, Lklg;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f11000d

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v5, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 377
    iget-object v0, p0, Lklg;->d:Lkno;

    iget-object v0, v0, Lkno;->c:Ljava/lang/String;

    return-object v0
.end method

.method public e()Lkno;
    .locals 1

    .prologue
    .line 361
    iget-object v0, p0, Lklg;->d:Lkno;

    return-object v0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 381
    iget v0, p0, Lklg;->c:I

    return v0
.end method
