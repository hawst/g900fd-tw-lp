.class public final Ldmn;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmhw;",
        "Lmhx;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final p:Z

.field private final q:Ljava/lang/Integer;

.field private r:Lndr;

.field private s:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lnzx;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lkfo;ILjava/lang/Iterable;ZLjava/lang/Integer;Ljava/lang/String;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lkfo;",
            "I",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;Z",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 67
    const-string v4, "readcollectionsbyid"

    new-instance v5, Lmhw;

    invoke-direct {v5}, Lmhw;-><init>()V

    new-instance v6, Lmhx;

    invoke-direct {v6}, Lmhx;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v6}, Leuh;-><init>(Landroid/content/Context;Lkfo;ILjava/lang/String;Loxu;Loxu;)V

    .line 51
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ldmn;->s:Ljava/util/HashMap;

    .line 70
    iput-object p4, p0, Ldmn;->b:Ljava/lang/Iterable;

    .line 71
    iput-boolean p5, p0, Ldmn;->p:Z

    .line 72
    iput-object p6, p0, Ldmn;->q:Ljava/lang/Integer;

    .line 73
    iput-object p7, p0, Ldmn;->a:Ljava/lang/String;

    .line 74
    return-void
.end method


# virtual methods
.method protected a(Lmhw;)V
    .locals 6

    .prologue
    .line 78
    new-instance v0, Lndq;

    invoke-direct {v0}, Lndq;-><init>()V

    iput-object v0, p1, Lmhw;->a:Lndq;

    .line 79
    iget-object v1, p1, Lmhw;->a:Lndq;

    .line 80
    iget-object v0, p0, Ldmn;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 81
    new-instance v0, Lndj;

    invoke-direct {v0}, Lndj;-><init>()V

    iput-object v0, v1, Lndq;->b:Lndj;

    .line 82
    iget-object v0, v1, Lndq;->b:Lndj;

    iget-object v2, p0, Ldmn;->a:Ljava/lang/String;

    iput-object v2, v0, Lndj;->a:Ljava/lang/String;

    .line 84
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 85
    iget-object v0, p0, Ldmn;->b:Ljava/lang/Iterable;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 86
    new-instance v4, Lndo;

    invoke-direct {v4}, Lndo;-><init>()V

    .line 87
    new-instance v5, Lotc;

    invoke-direct {v5}, Lotc;-><init>()V

    iput-object v5, v4, Lndo;->b:Lotc;

    .line 88
    iget-object v5, v4, Lndo;->b:Lotc;

    iput-object v0, v5, Lotc;->a:Ljava/lang/String;

    .line 89
    iget-boolean v0, p0, Ldmn;->p:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x2

    :goto_1
    iput v0, v4, Lndo;->c:I

    .line 90
    iget-object v0, p0, Ldmn;->q:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 91
    iget-object v0, p0, Ldmn;->q:Ljava/lang/Integer;

    iput-object v0, v4, Lndo;->d:Ljava/lang/Integer;

    .line 93
    :cond_1
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 89
    :cond_2
    const/4 v0, 0x1

    goto :goto_1

    .line 95
    :cond_3
    const/4 v0, 0x0

    new-array v0, v0, [Lndo;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lndo;

    iput-object v0, v1, Lndq;->a:[Lndo;

    .line 96
    return-void
.end method

.method protected a(Lmhx;)V
    .locals 13

    .prologue
    const/4 v12, 0x5

    const/4 v3, 0x0

    const/4 v5, 0x0

    .line 100
    iget-object v0, p1, Lmhx;->a:Lndr;

    iput-object v0, p0, Ldmn;->r:Lndr;

    .line 108
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 109
    iget-object v0, p0, Ldmn;->r:Lndr;

    iget-object v1, v0, Lndr;->d:[Lnyz;

    array-length v2, v1

    move v0, v5

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v4, v1, v0

    .line 110
    iget-object v7, v4, Lnyz;->b:Ljava/lang/String;

    invoke-virtual {v6, v7, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 112
    :cond_0
    iget-object v0, p0, Ldmn;->r:Lndr;

    iget-object v7, v0, Lndr;->b:[Lnzx;

    array-length v8, v7

    move v4, v5

    :goto_1
    if-ge v4, v8, :cond_6

    aget-object v9, v7, v4

    .line 114
    sget-object v0, Lnzr;->a:Loxr;

    .line 115
    invoke-virtual {v9, v0}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnzr;

    .line 116
    iget-object v10, v0, Lnzr;->b:Lnxr;

    .line 117
    iget-object v1, p0, Ldmn;->s:Ljava/util/HashMap;

    iget-object v2, v10, Lnxr;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    iget-object v1, v10, Lnxr;->g:Lnzm;

    if-eqz v1, :cond_3

    iget-object v1, v10, Lnxr;->g:Lnzm;

    iget-object v1, v1, Lnzm;->b:Ljava/lang/String;

    move-object v2, v1

    .line 121
    :goto_2
    invoke-virtual {v6, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnyz;

    .line 122
    if-eqz v1, :cond_4

    .line 123
    iget-object v1, v1, Lnyz;->c:Ljava/lang/String;

    iput-object v1, v10, Lnxr;->f:Ljava/lang/String;

    .line 124
    sget-object v1, Lnzr;->a:Loxr;

    invoke-virtual {v9, v1, v0}, Lnzx;->a(Loxr;Ljava/lang/Object;)V

    .line 130
    :cond_1
    :goto_3
    iget-object v0, v9, Lnzx;->i:Lnzm;

    if-eqz v0, :cond_5

    iget-object v0, v9, Lnzx;->i:Lnzm;

    iget-object v0, v0, Lnzm;->b:Ljava/lang/String;

    .line 132
    :goto_4
    invoke-virtual {v6, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnyz;

    .line 133
    if-eqz v0, :cond_2

    .line 134
    iput-object v0, v9, Lnzx;->h:Lnyz;

    .line 112
    :cond_2
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    :cond_3
    move-object v2, v3

    .line 120
    goto :goto_2

    .line 125
    :cond_4
    if-eqz v2, :cond_1

    const-string v0, "ReadCollectionsById"

    invoke-static {v0, v12}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 126
    iget-object v0, v10, Lnxr;->b:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, 0x2c

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/2addr v10, v11

    invoke-direct {v1, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v10, "Could not find owner ref "

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for collection ID "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    :cond_5
    move-object v0, v3

    .line 130
    goto :goto_4

    .line 137
    :cond_6
    iget-object v0, p0, Ldmn;->r:Lndr;

    iget-object v7, v0, Lndr;->c:[Lnzx;

    array-length v8, v7

    move v4, v5

    :goto_5
    if-ge v4, v8, :cond_c

    aget-object v9, v7, v4

    .line 138
    sget-object v0, Lnzu;->a:Loxr;

    invoke-virtual {v9, v0}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnzu;

    .line 139
    iget-object v10, v0, Lnzu;->b:Lnym;

    .line 140
    iget-object v1, v10, Lnym;->i:Lnzm;

    if-eqz v1, :cond_8

    iget-object v1, v10, Lnym;->i:Lnzm;

    iget-object v1, v1, Lnzm;->b:Ljava/lang/String;

    move-object v2, v1

    .line 141
    :goto_6
    invoke-virtual {v6, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnyz;

    .line 142
    if-eqz v1, :cond_9

    .line 143
    iput-object v1, v10, Lnym;->h:Lnyz;

    .line 144
    sget-object v1, Lnzu;->a:Loxr;

    invoke-virtual {v9, v1, v0}, Lnzx;->a(Loxr;Ljava/lang/Object;)V

    .line 137
    :cond_7
    :goto_7
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_5

    :cond_8
    move-object v2, v3

    .line 140
    goto :goto_6

    .line 145
    :cond_9
    const-string v0, "ReadCollectionsById"

    invoke-static {v0, v12}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 146
    if-eqz v2, :cond_a

    .line 147
    iget-object v0, v10, Lnym;->f:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, 0x27

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v9, v10

    invoke-direct {v1, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v9, "Could not find owner ref "

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for photo ID "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_7

    .line 150
    :cond_a
    iget-object v0, v10, Lnym;->f:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, v10, Lnym;->z:[Lnxt;

    array-length v0, v0

    if-lez v0, :cond_b

    iget-object v0, v10, Lnym;->z:[Lnxt;

    aget-object v0, v0, v5

    iget-object v0, v0, Lnxt;->b:Ljava/lang/String;

    :goto_8
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, 0x2e

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v9, v10

    invoke-direct {v2, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v9, "Photo with ID "

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " in collection "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " has no owner ref"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_7

    :cond_b
    const-string v0, "unknown"

    goto :goto_8

    .line 157
    :cond_c
    return-void
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 30
    check-cast p1, Lmhw;

    invoke-virtual {p0, p1}, Ldmn;->a(Lmhw;)V

    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Ldmn;->r:Lndr;

    iget-object v0, v0, Lndr;->a:Ljava/lang/String;

    return-object v0
.end method

.method protected synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 30
    check-cast p1, Lmhx;

    invoke-virtual {p0, p1}, Ldmn;->a(Lmhx;)V

    return-void
.end method

.method public c()[Lnzx;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Ldmn;->r:Lndr;

    iget-object v0, v0, Lndr;->c:[Lnzx;

    return-object v0
.end method

.method public d()[Lndp;
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Ldmn;->r:Lndr;

    iget-object v0, v0, Lndr;->e:[Lndp;

    return-object v0
.end method

.method public e()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lnzx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 185
    iget-object v0, p0, Ldmn;->s:Ljava/util/HashMap;

    return-object v0
.end method
