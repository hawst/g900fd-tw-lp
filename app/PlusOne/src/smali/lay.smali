.class public final Llay;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Llay;


# instance fields
.field private b:Ljava/lang/String;

.field private c:I

.field private d:Llaz;

.field private e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    new-instance v0, Llay;

    invoke-direct {v0}, Llay;-><init>()V

    sput-object v0, Llay;->a:Llay;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    const/4 v0, -0x1

    iput v0, p0, Llay;->c:I

    .line 42
    return-void
.end method

.method public static a()Llay;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Llay;->a:Llay;

    return-object v0
.end method


# virtual methods
.method public declared-synchronized a(ILjava/lang/String;)Llaz;
    .locals 1

    .prologue
    .line 57
    monitor-enter p0

    :try_start_0
    iget v0, p0, Llay;->c:I

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Llay;->b:Ljava/lang/String;

    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Llay;->d:Llaz;

    .line 59
    invoke-virtual {p0}, Llay;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 62
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 57
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(ILjava/lang/String;Llaz;)V
    .locals 1

    .prologue
    .line 49
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Llay;->e:Z

    if-nez v0, :cond_0

    .line 50
    iput p1, p0, Llay;->c:I

    .line 51
    iput-object p2, p0, Llay;->b:Ljava/lang/String;

    .line 52
    iput-object p3, p0, Llay;->d:Llaz;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 54
    :cond_0
    monitor-exit p0

    return-void

    .line 49
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Z)V
    .locals 1

    .prologue
    .line 77
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Llay;->e:Z

    .line 78
    iget-boolean v0, p0, Llay;->e:Z

    if-eqz v0, :cond_0

    .line 79
    invoke-virtual {p0}, Llay;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 81
    :cond_0
    monitor-exit p0

    return-void

    .line 77
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b()V
    .locals 1

    .prologue
    .line 67
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Llay;->d:Llaz;

    .line 68
    const/4 v0, 0x0

    iput-object v0, p0, Llay;->b:Ljava/lang/String;

    .line 69
    const/4 v0, -0x1

    iput v0, p0, Llay;->c:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 70
    monitor-exit p0

    return-void

    .line 67
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 73
    iget-boolean v0, p0, Llay;->e:Z

    return v0
.end method

.method public declared-synchronized d()J
    .locals 2

    .prologue
    .line 84
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Llay;->d:Llaz;

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Llay;->d:Llaz;

    invoke-virtual {v0}, Llaz;->a()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 87
    :goto_0
    monitor-exit p0

    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0

    .line 84
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
