.class public final Llxy;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:[Llwv;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 4616
    invoke-direct {p0}, Loxq;-><init>()V

    .line 4623
    sget-object v0, Llwv;->a:[Llwv;

    iput-object v0, p0, Llxy;->c:[Llwv;

    .line 4616
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 4647
    .line 4648
    iget-object v0, p0, Llxy;->a:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 4649
    const/4 v0, 0x1

    iget-object v2, p0, Llxy;->a:Ljava/lang/String;

    .line 4650
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 4652
    :goto_0
    iget-object v2, p0, Llxy;->b:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 4653
    const/4 v2, 0x2

    iget-object v3, p0, Llxy;->b:Ljava/lang/String;

    .line 4654
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 4656
    :cond_0
    iget-object v2, p0, Llxy;->c:[Llwv;

    if-eqz v2, :cond_2

    .line 4657
    iget-object v2, p0, Llxy;->c:[Llwv;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 4658
    if-eqz v4, :cond_1

    .line 4659
    const/4 v5, 0x3

    .line 4660
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 4657
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 4664
    :cond_2
    iget-object v1, p0, Llxy;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4665
    iput v0, p0, Llxy;->ai:I

    .line 4666
    return v0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Llxy;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 4674
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 4675
    sparse-switch v0, :sswitch_data_0

    .line 4679
    iget-object v2, p0, Llxy;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 4680
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Llxy;->ah:Ljava/util/List;

    .line 4683
    :cond_1
    iget-object v2, p0, Llxy;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4685
    :sswitch_0
    return-object p0

    .line 4690
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llxy;->a:Ljava/lang/String;

    goto :goto_0

    .line 4694
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llxy;->b:Ljava/lang/String;

    goto :goto_0

    .line 4698
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 4699
    iget-object v0, p0, Llxy;->c:[Llwv;

    if-nez v0, :cond_3

    move v0, v1

    .line 4700
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Llwv;

    .line 4701
    iget-object v3, p0, Llxy;->c:[Llwv;

    if-eqz v3, :cond_2

    .line 4702
    iget-object v3, p0, Llxy;->c:[Llwv;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4704
    :cond_2
    iput-object v2, p0, Llxy;->c:[Llwv;

    .line 4705
    :goto_2
    iget-object v2, p0, Llxy;->c:[Llwv;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 4706
    iget-object v2, p0, Llxy;->c:[Llwv;

    new-instance v3, Llwv;

    invoke-direct {v3}, Llwv;-><init>()V

    aput-object v3, v2, v0

    .line 4707
    iget-object v2, p0, Llxy;->c:[Llwv;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 4708
    invoke-virtual {p1}, Loxn;->a()I

    .line 4705
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 4699
    :cond_3
    iget-object v0, p0, Llxy;->c:[Llwv;

    array-length v0, v0

    goto :goto_1

    .line 4711
    :cond_4
    iget-object v2, p0, Llxy;->c:[Llwv;

    new-instance v3, Llwv;

    invoke-direct {v3}, Llwv;-><init>()V

    aput-object v3, v2, v0

    .line 4712
    iget-object v2, p0, Llxy;->c:[Llwv;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 4675
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 4628
    iget-object v0, p0, Llxy;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 4629
    const/4 v0, 0x1

    iget-object v1, p0, Llxy;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 4631
    :cond_0
    iget-object v0, p0, Llxy;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 4632
    const/4 v0, 0x2

    iget-object v1, p0, Llxy;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 4634
    :cond_1
    iget-object v0, p0, Llxy;->c:[Llwv;

    if-eqz v0, :cond_3

    .line 4635
    iget-object v1, p0, Llxy;->c:[Llwv;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 4636
    if-eqz v3, :cond_2

    .line 4637
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 4635
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 4641
    :cond_3
    iget-object v0, p0, Llxy;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 4643
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 4612
    invoke-virtual {p0, p1}, Llxy;->a(Loxn;)Llxy;

    move-result-object v0

    return-object v0
.end method
