.class public Lbsu;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private A:Lboi;

.field private B:Lboi;

.field private C:Landroid/graphics/Bitmap;

.field private final b:Landroid/content/Context;

.field private final c:Lbig;

.field private final d:Lbjp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbjp",
            "<",
            "Lbhk;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lbjp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbjp",
            "<",
            "Lbhl;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lbgf;

.field private final g:Lbte;

.field private final h:Lasy;

.field private final i:Lbjf;

.field private final j:Z

.field private final k:Ljava/util/concurrent/Executor;

.field private final l:Ljava/util/concurrent/Executor;

.field private final m:Lbtq;

.field private final n:Lbtc;

.field private o:J

.field private final p:Landroid/graphics/RectF;

.field private final q:Ljava/lang/Runnable;

.field private final r:Ljava/util/concurrent/LinkedBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/LinkedBlockingQueue",
            "<",
            "Lbtb;",
            ">;"
        }
    .end annotation
.end field

.field private final s:Lbtt;

.field private final t:Ljava/lang/Object;

.field private final u:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private v:Lbta;

.field private w:I

.field private x:Z

.field private y:Lbrx;

.field private volatile z:Lbtw;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    const-class v0, Lbsu;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbsu;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lbig;Lbjp;Lbjp;Lbgf;Lbte;Lasy;Lbjf;Lboi;ZLjava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lbtq;Lbtc;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lbig;",
            "Lbjp",
            "<",
            "Lbhk;",
            ">;",
            "Lbjp",
            "<",
            "Lbhl;",
            ">;",
            "Lbgf;",
            "Lbte;",
            "Lasy;",
            "Lbjf;",
            "Lboi;",
            "Z",
            "Ljava/util/concurrent/Executor;",
            "Ljava/util/concurrent/Executor;",
            "Lbtq;",
            "Lbtc;",
            ")V"
        }
    .end annotation

    .prologue
    .line 240
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 121
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iput-object v1, p0, Lbsu;->p:Landroid/graphics/RectF;

    .line 132
    new-instance v1, Lbsv;

    invoke-direct {v1, p0}, Lbsv;-><init>(Lbsu;)V

    iput-object v1, p0, Lbsu;->s:Lbtt;

    .line 152
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lbsu;->t:Ljava/lang/Object;

    .line 158
    new-instance v1, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v1, p0, Lbsu;->u:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 241
    const-string v1, "context"

    const/4 v2, 0x0

    invoke-static {p1, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    iput-object v1, p0, Lbsu;->b:Landroid/content/Context;

    .line 242
    const-string v1, "mediaExtractorFactory"

    .line 243
    const/4 v2, 0x0

    invoke-static {p2, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbig;

    iput-object v1, p0, Lbsu;->c:Lbig;

    .line 244
    const-string v1, "audioDecoderPool"

    const/4 v2, 0x0

    invoke-static {p3, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbjp;

    iput-object v1, p0, Lbsu;->d:Lbjp;

    .line 245
    const-string v1, "videoDecoderPool"

    const/4 v2, 0x0

    invoke-static {p4, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbjp;

    iput-object v1, p0, Lbsu;->e:Lbjp;

    .line 246
    const-string v1, "renderContext"

    const/4 v2, 0x0

    invoke-static {p5, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbgf;

    iput-object v1, p0, Lbsu;->f:Lbgf;

    .line 247
    const-string v1, "renderer"

    const/4 v2, 0x0

    invoke-static {p6, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbte;

    iput-object v1, p0, Lbsu;->g:Lbte;

    .line 248
    const-string v1, "stateTracker"

    const/4 v2, 0x0

    invoke-static {p7, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lasy;

    iput-object v1, p0, Lbsu;->h:Lasy;

    .line 249
    const-string v1, "bitmapFactory"

    const/4 v2, 0x0

    invoke-static {p8, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbjf;

    iput-object v1, p0, Lbsu;->i:Lbjf;

    .line 250
    const-string v1, "storyboard"

    const/4 v2, 0x0

    invoke-static {p9, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lboi;

    iput-object v1, p0, Lbsu;->A:Lboi;

    .line 251
    iput-boolean p10, p0, Lbsu;->j:Z

    .line 252
    const-string v1, "decoderExecutor"

    const/4 v2, 0x0

    invoke-static {p11, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Executor;

    iput-object v1, p0, Lbsu;->k:Ljava/util/concurrent/Executor;

    .line 253
    iput-object p12, p0, Lbsu;->l:Ljava/util/concurrent/Executor;

    .line 254
    move-object/from16 v0, p13

    iput-object v0, p0, Lbsu;->m:Lbtq;

    .line 255
    move-object/from16 v0, p14

    iput-object v0, p0, Lbsu;->n:Lbtc;

    .line 257
    new-instance v1, Lbsw;

    invoke-direct {v1, p0}, Lbsw;-><init>(Lbsu;)V

    iput-object v1, p0, Lbsu;->q:Ljava/lang/Runnable;

    .line 269
    iget-object v1, p0, Lbsu;->m:Lbtq;

    if-eqz v1, :cond_0

    .line 270
    const-string v1, "listenerExecutor"

    const-string v2, "cannot be null if mPlaybackStateListener or mProgressListener are non-null"

    invoke-static {p12, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 274
    :cond_0
    new-instance v1, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v1}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v1, p0, Lbsu;->r:Ljava/util/concurrent/LinkedBlockingQueue;

    .line 275
    sget-object v1, Lbta;->c:Lbta;

    iput-object v1, p0, Lbsu;->v:Lbta;

    .line 276
    return-void
.end method

.method private a(I)V
    .locals 2

    .prologue
    .line 802
    iget-object v1, p0, Lbsu;->t:Ljava/lang/Object;

    monitor-enter v1

    .line 803
    :try_start_0
    iput p1, p0, Lbsu;->w:I

    .line 804
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic a(Lbsu;)V
    .locals 13

    .prologue
    const/4 v12, 0x2

    const/4 v7, 0x0

    const-wide/16 v10, 0x0

    const-wide/16 v8, 0x3e8

    const/4 v6, 0x1

    .line 57
    iget-object v0, p0, Lbsu;->y:Lbrx;

    invoke-virtual {v0}, Lbrx;->g()J

    move-result-wide v0

    iget-object v2, p0, Lbsu;->z:Lbtw;

    invoke-virtual {v2}, Lbtw;->c()J

    move-result-wide v2

    iget-boolean v4, p0, Lbsu;->j:Z

    if-eqz v4, :cond_1

    cmp-long v0, v0, v10

    if-gtz v0, :cond_0

    cmp-long v0, v2, v10

    if-gtz v0, :cond_0

    invoke-direct {p0, v6}, Lbsu;->a(Z)V

    invoke-direct {p0}, Lbsu;->h()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    cmp-long v4, v0, v10

    if-gtz v4, :cond_3

    const-wide/32 v4, 0xc350

    cmp-long v4, v2, v4

    if-lez v4, :cond_2

    sget-object v4, Lbsu;->a:Ljava/lang/String;

    const-string v4, "Audio stopped %d ms ago. Video has %d ms remaining."

    new-array v5, v12, [Ljava/lang/Object;

    neg-long v0, v0

    div-long/2addr v0, v8

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v5, v7

    div-long v0, v2, v8

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    :cond_2
    invoke-direct {p0, v6}, Lbsu;->a(Z)V

    invoke-direct {p0}, Lbsu;->h()V

    goto :goto_0

    :cond_3
    const-wide/32 v4, 0xc350

    cmp-long v4, v0, v4

    if-lez v4, :cond_0

    sget-object v4, Lbsu;->a:Ljava/lang/String;

    const-string v4, "Video stopped %d ms ago. Audio has %d ms remaining."

    new-array v5, v12, [Ljava/lang/Object;

    neg-long v2, v2

    div-long/2addr v2, v8

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v5, v7

    div-long/2addr v0, v8

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    goto :goto_0
.end method

.method private a(Lbta;J)V
    .locals 8

    .prologue
    .line 824
    iget-object v1, p0, Lbsu;->t:Ljava/lang/Object;

    monitor-enter v1

    .line 828
    :try_start_0
    iput-object p1, p0, Lbsu;->v:Lbta;

    .line 832
    iget-object v0, p0, Lbsu;->v:Lbta;

    sget-object v2, Lbta;->c:Lbta;

    if-eq v0, v2, :cond_0

    .line 833
    const/4 v0, 0x0

    iput v0, p0, Lbsu;->w:I

    .line 834
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbsu;->x:Z

    .line 836
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 837
    iget-object v0, p0, Lbsu;->m:Lbtq;

    if-eqz v0, :cond_1

    .line 838
    iget-object v3, p0, Lbsu;->C:Landroid/graphics/Bitmap;

    .line 839
    iget-object v6, p0, Lbsu;->l:Ljava/util/concurrent/Executor;

    new-instance v0, Lbsx;

    move-object v1, p0

    move-object v2, p1

    move-wide v4, p2

    invoke-direct/range {v0 .. v5}, Lbsx;-><init>(Lbsu;Lbta;Landroid/graphics/Bitmap;J)V

    invoke-interface {v6, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 872
    :cond_1
    return-void

    .line 836
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 808
    iget-object v1, p0, Lbsu;->t:Ljava/lang/Object;

    monitor-enter v1

    .line 809
    :try_start_0
    iput-boolean p1, p0, Lbsu;->x:Z

    .line 810
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private a(JLbsq;)Z
    .locals 5

    .prologue
    .line 555
    invoke-interface {p3}, Lbsq;->e()V

    .line 556
    iget-object v0, p0, Lbsu;->y:Lbrx;

    invoke-virtual {v0, p1, p2}, Lbrx;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbsu;->z:Lbtw;

    .line 557
    invoke-interface {p3}, Lbsq;->d()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lbtw;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(JLbsq;Lboi;)Z
    .locals 11

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 667
    iget-object v1, p0, Lbsu;->A:Lboi;

    invoke-static {v1, p4}, Lboi;->a(Lbop;Lbop;)I

    move-result v3

    .line 678
    cmp-long v1, p1, v4

    if-nez v1, :cond_1

    move-wide p1, v4

    .line 687
    :cond_0
    :goto_0
    if-eqz p4, :cond_6

    .line 688
    iget-object v1, p0, Lbsu;->A:Lboi;

    invoke-virtual {v1}, Lboi;->a()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p4}, Lboi;->a()Ljava/util/List;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 692
    :goto_1
    if-eqz v1, :cond_3

    .line 693
    iget-object v1, p0, Lbsu;->y:Lbrx;

    iget-object v4, p0, Lbsu;->A:Lboi;

    invoke-virtual {v1, v4}, Lbrx;->a(Lbmc;)V

    move v1, v2

    .line 709
    :goto_2
    if-nez v1, :cond_4

    .line 746
    :goto_3
    return v0

    .line 680
    :cond_1
    if-eq v3, v2, :cond_2

    .line 681
    iget-object v1, p0, Lbsu;->A:Lboi;

    invoke-virtual {v1, p4, p1, p2}, Lboi;->a(Lboi;J)J

    move-result-wide p1

    goto :goto_0

    .line 682
    :cond_2
    if-eqz p4, :cond_0

    move-wide p1, v4

    .line 683
    goto :goto_0

    .line 695
    :cond_3
    iget-object v1, p0, Lbsu;->y:Lbrx;

    invoke-static {v1}, Lcgr;->a(Lcgq;)V

    .line 696
    :try_start_0
    iget-object v1, p0, Lbsu;->A:Lboi;

    iget-object v4, p0, Lbsu;->d:Lbjp;

    iget-object v5, p0, Lbsu;->c:Lbig;

    invoke-static {v1, v4, v5}, Lbhn;->a(Lbmc;Lbjp;Lbig;)Lbhx;
    :try_end_0
    .catch Lbhp; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 705
    iget-object v4, p0, Lbsu;->y:Lbrx;

    iget-object v5, p0, Lbsu;->A:Lboi;

    invoke-virtual {v4, v5, v1}, Lbrx;->a(Lbmc;Lbhx;)V

    .line 706
    iget-object v1, p0, Lbsu;->y:Lbrx;

    invoke-virtual {v1, p1, p2}, Lbrx;->a(J)Z

    move-result v1

    and-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 717
    :cond_4
    if-eq v3, v2, :cond_5

    .line 718
    iget-object v0, p0, Lbsu;->z:Lbtw;

    iget-object v2, p0, Lbsu;->A:Lboi;

    .line 719
    invoke-interface {p3}, Lbsq;->d()J

    move-result-wide v4

    .line 718
    invoke-virtual {v0, v2, v3, v4, v5}, Lbtw;->a(Lbop;IJ)Z

    move-result v0

    and-int/2addr v0, v1

    goto :goto_3

    .line 721
    :cond_5
    iget-object v2, p0, Lbsu;->z:Lbtw;

    invoke-static {v2}, Lcgr;->a(Lcgq;)V

    .line 722
    new-instance v2, Lbhv;

    iget-object v3, p0, Lbsu;->A:Lboi;

    new-instance v4, Lbho;

    iget-object v5, p0, Lbsu;->f:Lbgf;

    invoke-direct {v4, v5}, Lbho;-><init>(Lbgf;)V

    invoke-direct {v2, v3, v4}, Lbhv;-><init>(Lbop;Lbho;)V

    .line 726
    :try_start_1
    iget-object v3, p0, Lbsu;->A:Lboi;

    iget-object v4, p0, Lbsu;->e:Lbjp;

    new-instance v5, Lbhq;

    iget-object v6, p0, Lbsu;->f:Lbgf;

    iget-object v7, p0, Lbsu;->k:Ljava/util/concurrent/Executor;

    iget-object v8, p0, Lbsu;->i:Lbjf;

    iget-object v9, p0, Lbsu;->b:Landroid/content/Context;

    invoke-direct {v5, v6, v7, v8, v9}, Lbhq;-><init>(Lbgf;Ljava/util/concurrent/Executor;Lbjf;Landroid/content/Context;)V

    iget-object v6, p0, Lbsu;->c:Lbig;

    invoke-static {v3, v4, v5, v6, v2}, Lbhn;->a(Lbop;Lbjp;Lbhq;Lbig;Lbhv;)Ljava/util/List;
    :try_end_1
    .catch Lbhp; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    .line 736
    iget-object v3, p0, Lbsu;->z:Lbtw;

    iget-object v4, p0, Lbsu;->A:Lboi;

    iget-object v5, p0, Lbsu;->g:Lbte;

    invoke-virtual {v3, v4, v2, v0, v5}, Lbtw;->a(Lbop;Lbhv;Ljava/util/List;Lbte;)V

    .line 738
    iget-object v0, p0, Lbsu;->z:Lbtw;

    invoke-interface {p3}, Lbsq;->d()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lbtw;->a(J)Z

    move-result v0

    and-int/2addr v0, v1

    goto :goto_3

    .line 734
    :catch_0
    move-exception v1

    goto :goto_3

    .line 703
    :catch_1
    move-exception v1

    goto :goto_3

    :cond_6
    move v1, v0

    goto/16 :goto_1
.end method

.method static synthetic b(Lbsu;)Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lbsu;->p:Landroid/graphics/RectF;

    return-object v0
.end method

.method static synthetic c(Lbsu;)J
    .locals 2

    .prologue
    .line 57
    iget-wide v0, p0, Lbsu;->o:J

    return-wide v0
.end method

.method static synthetic d(Lbsu;)Lbtq;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lbsu;->m:Lbtq;

    return-object v0
.end method

.method static synthetic e(Lbsu;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lbsu;->t:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic f(Lbsu;)I
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Lbsu;->w:I

    return v0
.end method

.method static synthetic g(Lbsu;)Z
    .locals 1

    .prologue
    .line 57
    iget-boolean v0, p0, Lbsu;->x:Z

    return v0
.end method

.method private h()V
    .locals 3

    .prologue
    .line 755
    iget-object v1, p0, Lbsu;->t:Ljava/lang/Object;

    monitor-enter v1

    .line 756
    :try_start_0
    iget-object v0, p0, Lbsu;->v:Lbta;

    sget-object v2, Lbta;->c:Lbta;

    if-eq v0, v2, :cond_0

    .line 757
    iget-object v0, p0, Lbsu;->r:Ljava/util/concurrent/LinkedBlockingQueue;

    sget-object v2, Lbta;->c:Lbta;

    invoke-static {v2}, Lbtb;->a(Lbta;)Lbtb;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/concurrent/LinkedBlockingQueue;->add(Ljava/lang/Object;)Z

    .line 759
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private i()V
    .locals 4

    .prologue
    .line 818
    iget-object v0, p0, Lbsu;->z:Lbtw;

    iget-object v1, p0, Lbsu;->f:Lbgf;

    invoke-virtual {v0, v1}, Lbtw;->a(Lbgf;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lbsu;->C:Landroid/graphics/Bitmap;

    .line 819
    sget-object v0, Lbta;->a:Lbta;

    const-wide/16 v2, 0x0

    invoke-direct {p0, v0, v2, v3}, Lbsu;->a(Lbta;J)V

    .line 820
    const/4 v0, 0x0

    iput-object v0, p0, Lbsu;->C:Landroid/graphics/Bitmap;

    .line 821
    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 561
    iget-object v1, p0, Lbsu;->t:Ljava/lang/Object;

    monitor-enter v1

    .line 562
    :try_start_0
    iget-object v0, p0, Lbsu;->v:Lbta;

    sget-object v2, Lbta;->a:Lbta;

    if-eq v0, v2, :cond_0

    .line 563
    iget-object v0, p0, Lbsu;->r:Ljava/util/concurrent/LinkedBlockingQueue;

    sget-object v2, Lbta;->a:Lbta;

    invoke-static {v2}, Lbtb;->a(Lbta;)Lbtb;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/concurrent/LinkedBlockingQueue;->add(Ljava/lang/Object;)Z

    .line 564
    iget-object v0, p0, Lbsu;->v:Lbta;

    sget-object v2, Lbta;->d:Lbta;

    if-ne v0, v2, :cond_0

    .line 565
    iget-object v0, p0, Lbsu;->z:Lbtw;

    invoke-virtual {v0}, Lbtw;->a()V

    .line 568
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(J)V
    .locals 5

    .prologue
    .line 583
    iget-object v1, p0, Lbsu;->t:Ljava/lang/Object;

    monitor-enter v1

    .line 584
    :try_start_0
    iget-object v0, p0, Lbsu;->r:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingQueue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 585
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 586
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbtb;

    .line 587
    iget-object v3, v0, Lbtb;->a:Lbta;

    sget-object v4, Lbta;->d:Lbta;

    if-ne v3, v4, :cond_0

    .line 589
    iget-object v3, p0, Lbsu;->r:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v3, v0}, Ljava/util/concurrent/LinkedBlockingQueue;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 596
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 592
    :cond_1
    :try_start_1
    iget-object v0, p0, Lbsu;->r:Ljava/util/concurrent/LinkedBlockingQueue;

    new-instance v2, Lbtb;

    sget-object v3, Lbta;->d:Lbta;

    invoke-direct {v2, v3, p1, p2}, Lbtb;-><init>(Lbta;J)V

    invoke-virtual {v0, v2}, Ljava/util/concurrent/LinkedBlockingQueue;->add(Ljava/lang/Object;)Z

    .line 593
    iget-object v0, p0, Lbsu;->v:Lbta;

    sget-object v2, Lbta;->d:Lbta;

    if-ne v0, v2, :cond_2

    .line 594
    iget-object v0, p0, Lbsu;->z:Lbtw;

    invoke-virtual {v0}, Lbtw;->a()V

    .line 596
    :cond_2
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public a(Lboi;)V
    .locals 5

    .prologue
    .line 644
    iget-object v1, p0, Lbsu;->t:Ljava/lang/Object;

    monitor-enter v1

    .line 645
    :try_start_0
    const-string v0, "storyboard"

    const/4 v2, 0x0

    invoke-static {p1, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lboi;

    iput-object v0, p0, Lbsu;->B:Lboi;

    .line 646
    iget-object v0, p0, Lbsu;->r:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingQueue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 647
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 648
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbtb;

    .line 649
    iget-object v3, v0, Lbtb;->a:Lbta;

    sget-object v4, Lbta;->f:Lbta;

    if-ne v3, v4, :cond_0

    .line 651
    iget-object v3, p0, Lbsu;->r:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v3, v0}, Ljava/util/concurrent/LinkedBlockingQueue;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 655
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 654
    :cond_1
    :try_start_1
    iget-object v0, p0, Lbsu;->r:Ljava/util/concurrent/LinkedBlockingQueue;

    sget-object v2, Lbta;->f:Lbta;

    invoke-static {v2}, Lbtb;->a(Lbta;)Lbtb;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/concurrent/LinkedBlockingQueue;->add(Ljava/lang/Object;)Z

    .line 655
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public a(Lbss;Lbrw;Lbge;)V
    .locals 22

    .prologue
    .line 298
    new-instance v10, Lcgw;

    sget-object v4, Lbsu;->a:Ljava/lang/String;

    invoke-direct {v10, v4}, Lcgw;-><init>(Ljava/lang/String;)V

    .line 301
    invoke-virtual/range {p1 .. p1}, Lbss;->c()J

    move-result-wide v4

    .line 302
    move-object/from16 v0, p0

    iget-object v6, v0, Lbsu;->A:Lboi;

    invoke-virtual {v6}, Lboi;->c()J

    move-result-wide v6

    cmp-long v6, v4, v6

    if-gez v6, :cond_0

    move-object/from16 v0, p0

    iget-object v6, v0, Lbsu;->A:Lboi;

    .line 303
    invoke-virtual {v6}, Lboi;->n()J

    move-result-wide v6

    cmp-long v6, v4, v6

    if-ltz v6, :cond_1

    .line 304
    :cond_0
    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lbsu;->a(Z)V

    .line 541
    :goto_0
    return-void

    .line 309
    :cond_1
    :try_start_0
    new-instance v6, Lbrx;

    move-object/from16 v0, p0

    iget-object v7, v0, Lbsu;->s:Lbtt;

    move-object/from16 v0, p2

    invoke-direct {v6, v7, v0}, Lbrx;-><init>(Lbtt;Lbrw;)V

    move-object/from16 v0, p0

    iput-object v6, v0, Lbsu;->y:Lbrx;

    .line 310
    new-instance v6, Lbtw;

    move-object/from16 v0, p0

    iget-object v7, v0, Lbsu;->s:Lbtt;

    move-object/from16 v0, p0

    iget-object v8, v0, Lbsu;->n:Lbtc;

    move-object/from16 v0, p3

    invoke-direct {v6, v7, v0, v8}, Lbtw;-><init>(Lbtt;Lbge;Lbtc;)V

    move-object/from16 v0, p0

    iput-object v6, v0, Lbsu;->z:Lbtw;

    .line 312
    sget-object v6, Lbta;->d:Lbta;

    const-wide/16 v8, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v8, v9}, Lbsu;->a(Lbta;J)V

    .line 314
    move-object/from16 v0, p0

    iget-object v11, v0, Lbsu;->y:Lbrx;

    .line 315
    const/4 v6, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v11, v6}, Lbsu;->a(JLbsq;Lboi;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 316
    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lbsu;->a(Z)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lbgo; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lbpr; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 522
    sget-object v4, Lbta;->c:Lbta;

    const-wide/16 v6, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v6, v7}, Lbsu;->a(Lbta;J)V

    .line 524
    move-object/from16 v0, p0

    iget-object v4, v0, Lbsu;->y:Lbrx;

    invoke-static {v4}, Lcgr;->a(Lcgq;)V

    .line 525
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lbsu;->y:Lbrx;

    .line 526
    move-object/from16 v0, p0

    iget-object v4, v0, Lbsu;->z:Lbtw;

    invoke-static {v4}, Lcgr;->a(Lcgq;)V

    .line 527
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lbsu;->z:Lbtw;

    .line 528
    move-object/from16 v0, p0

    iget-object v4, v0, Lbsu;->r:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v4}, Ljava/util/concurrent/LinkedBlockingQueue;->clear()V

    .line 529
    invoke-virtual {v10}, Lcgw;->b()V

    .line 533
    move-object/from16 v0, p0

    iget-object v5, v0, Lbsu;->t:Ljava/lang/Object;

    monitor-enter v5

    .line 534
    :try_start_1
    move-object/from16 v0, p0

    iget v4, v0, Lbsu;->w:I

    if-eqz v4, :cond_3

    const/4 v4, 0x1

    .line 535
    :goto_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 536
    if-eqz v4, :cond_2

    .line 537
    move-object/from16 v0, p0

    iget-object v4, v0, Lbsu;->h:Lasy;

    invoke-virtual {v4}, Lasy;->b()Livo;

    move-result-object v4

    sget-object v5, Lbsu;->a:Ljava/lang/String;

    invoke-static {v4, v5}, Livl;->a(Livo;Ljava/lang/String;)V

    .line 540
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lbsu;->h:Lasy;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lasy;->b(Livo;)V

    goto/16 :goto_0

    .line 534
    :cond_3
    const/4 v4, 0x0

    goto :goto_1

    .line 535
    :catchall_0
    move-exception v4

    :try_start_2
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v4

    .line 319
    :cond_4
    :try_start_3
    move-object/from16 v0, p0

    iget-object v6, v0, Lbsu;->z:Lbtw;

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lbsu;->j:Z

    invoke-virtual {v6, v7}, Lbtw;->a(Z)V

    .line 322
    new-instance v12, Lbsz;

    invoke-direct {v12}, Lbsz;-><init>()V

    .line 323
    iput-wide v4, v12, Lbsz;->a:J

    .line 324
    iput-wide v4, v12, Lbsz;->b:J

    .line 325
    move-object/from16 v0, p0

    iget-object v4, v0, Lbsu;->z:Lbtw;

    iput-object v4, v12, Lbsz;->c:Lbtw;

    .line 326
    move-object/from16 v0, p0

    iget-object v4, v0, Lbsu;->h:Lasy;

    invoke-virtual {v4, v12}, Lasy;->b(Livo;)V

    .line 328
    invoke-virtual/range {p1 .. p1}, Lbss;->b()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 329
    move-object/from16 v0, p0

    iget-object v4, v0, Lbsu;->g:Lbte;

    invoke-interface/range {p3 .. p3}, Lbge;->d()I

    move-result v5

    invoke-interface/range {p3 .. p3}, Lbge;->e()I

    move-result v6

    invoke-interface {v4, v5, v6}, Lbte;->a(II)V

    .line 330
    invoke-direct/range {p0 .. p0}, Lbsu;->i()V

    .line 337
    :goto_2
    const/4 v7, 0x0

    .line 345
    const/4 v6, 0x1

    .line 346
    const-wide/16 v4, 0x0

    .line 347
    new-instance v13, Landroid/graphics/RectF;

    invoke-direct {v13}, Landroid/graphics/RectF;-><init>()V

    move-wide/from16 v20, v4

    move v4, v6

    move v5, v7

    move-wide/from16 v6, v20

    .line 348
    :goto_3
    if-nez v5, :cond_19

    .line 349
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v8

    if-eqz v8, :cond_7

    .line 350
    new-instance v4, Ljava/lang/InterruptedException;

    invoke-direct {v4}, Ljava/lang/InterruptedException;-><init>()V

    throw v4
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lbgo; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lbpr; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 502
    :catch_0
    move-exception v4

    :try_start_4
    throw v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 522
    :catchall_1
    move-exception v4

    sget-object v5, Lbta;->c:Lbta;

    const-wide/16 v6, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v6, v7}, Lbsu;->a(Lbta;J)V

    .line 524
    move-object/from16 v0, p0

    iget-object v5, v0, Lbsu;->y:Lbrx;

    invoke-static {v5}, Lcgr;->a(Lcgq;)V

    .line 525
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-object v5, v0, Lbsu;->y:Lbrx;

    .line 526
    move-object/from16 v0, p0

    iget-object v5, v0, Lbsu;->z:Lbtw;

    invoke-static {v5}, Lcgr;->a(Lcgq;)V

    .line 527
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-object v5, v0, Lbsu;->z:Lbtw;

    .line 528
    move-object/from16 v0, p0

    iget-object v5, v0, Lbsu;->r:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v5}, Ljava/util/concurrent/LinkedBlockingQueue;->clear()V

    .line 529
    invoke-virtual {v10}, Lcgw;->b()V

    .line 533
    move-object/from16 v0, p0

    iget-object v6, v0, Lbsu;->t:Ljava/lang/Object;

    monitor-enter v6

    .line 534
    :try_start_5
    move-object/from16 v0, p0

    iget v5, v0, Lbsu;->w:I

    if-eqz v5, :cond_1d

    const/4 v5, 0x1

    .line 535
    :goto_4
    monitor-exit v6
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_5

    .line 536
    if-eqz v5, :cond_5

    .line 537
    move-object/from16 v0, p0

    iget-object v5, v0, Lbsu;->h:Lasy;

    invoke-virtual {v5}, Lasy;->b()Livo;

    move-result-object v5

    sget-object v6, Lbsu;->a:Ljava/lang/String;

    invoke-static {v5, v6}, Livl;->a(Livo;Ljava/lang/String;)V

    .line 540
    :cond_5
    move-object/from16 v0, p0

    iget-object v5, v0, Lbsu;->h:Lasy;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lasy;->b(Livo;)V

    .line 541
    throw v4

    .line 332
    :cond_6
    :try_start_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lbsu;->y:Lbrx;

    invoke-virtual {v4}, Lbrx;->a()V

    .line 333
    sget-object v4, Lbta;->b:Lbta;

    const-wide/16 v6, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v6, v7}, Lbsu;->a(Lbta;J)V
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Lbgo; {:try_start_6 .. :try_end_6} :catch_1
    .catch Lbpr; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_6 .. :try_end_6} :catch_3
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_4
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_2

    .line 505
    :catch_1
    move-exception v4

    :try_start_7
    new-instance v4, Ljava/lang/InterruptedException;

    const-string v5, "sink destroyed"

    invoke-direct {v4, v5}, Ljava/lang/InterruptedException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 352
    :cond_7
    :try_start_8
    invoke-static {}, Ljava/lang/Thread;->yield()V

    .line 353
    invoke-interface {v11}, Lbsq;->d()J

    move-result-wide v8

    .line 354
    iput-wide v8, v12, Lbsz;->b:J

    .line 355
    const-string v14, "playLoop"

    invoke-virtual {v10, v14}, Lcgw;->a(Ljava/lang/String;)V

    .line 356
    move-object/from16 v0, p0

    iget-object v14, v0, Lbsu;->u:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 360
    move-object/from16 v0, p0

    iget-object v14, v0, Lbsu;->g:Lbte;

    invoke-interface/range {p3 .. p3}, Lbge;->d()I

    move-result v15

    invoke-interface/range {p3 .. p3}, Lbge;->e()I

    move-result v16

    invoke-interface/range {v14 .. v16}, Lbte;->a(II)V

    .line 364
    :cond_8
    invoke-interface/range {p2 .. p2}, Lbrw;->f()J

    move-result-wide v14

    const-wide/32 v16, 0x11170

    cmp-long v14, v14, v16

    if-gez v14, :cond_9

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lbsu;->j:Z

    if-nez v14, :cond_9

    .line 365
    invoke-virtual/range {p0 .. p0}, Lbsu;->f()Z

    move-result v14

    if-eqz v14, :cond_b

    .line 366
    :cond_9
    move-object/from16 v0, p0

    iget-object v14, v0, Lbsu;->z:Lbtw;

    invoke-virtual {v14, v8, v9}, Lbtw;->b(J)Z

    move-result v14

    .line 367
    if-eqz v14, :cond_a

    invoke-virtual/range {p0 .. p0}, Lbsu;->f()Z

    move-result v14

    if-eqz v14, :cond_a

    .line 369
    move-object/from16 v0, p0

    iget-object v14, v0, Lbsu;->z:Lbtw;

    move-object/from16 v0, p0

    iget-boolean v15, v0, Lbsu;->j:Z

    invoke-virtual {v14, v15}, Lbtw;->a(Z)V

    .line 371
    :cond_a
    const-string v14, "Video step"

    invoke-virtual {v10, v14}, Lcgw;->b(Ljava/lang/String;)V

    .line 374
    :cond_b
    move-object/from16 v0, p0

    iget-object v14, v0, Lbsu;->y:Lbrx;

    invoke-virtual {v14}, Lbrx;->c()V

    .line 375
    const-string v14, "Audio step"

    invoke-virtual {v10, v14}, Lcgw;->b(Ljava/lang/String;)V

    .line 376
    invoke-virtual {v10}, Lcgw;->a()V

    .line 380
    sub-long v14, v8, v6

    invoke-static {v14, v15}, Ljava/lang/Math;->abs(J)J

    move-result-wide v14

    const-wide/32 v16, 0x186a0

    cmp-long v14, v14, v16

    if-lez v14, :cond_c

    .line 381
    const/4 v4, 0x1

    .line 383
    :cond_c
    move-object/from16 v0, p0

    iget-object v14, v0, Lbsu;->m:Lbtq;

    if-eqz v14, :cond_1e

    if-eqz v4, :cond_1e

    .line 384
    move-object/from16 v0, p0

    iget-object v4, v0, Lbsu;->g:Lbte;

    invoke-interface {v4, v13}, Lbte;->a(Landroid/graphics/RectF;)V

    .line 385
    move-object/from16 v0, p0

    iget-object v4, v0, Lbsu;->m:Lbtq;

    const-string v6, "mListener"

    const/4 v7, 0x0

    invoke-static {v4, v6, v7}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v6, v0, Lbsu;->p:Landroid/graphics/RectF;

    monitor-enter v6
    :try_end_8
    .catch Ljava/lang/InterruptedException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Lbgo; {:try_start_8 .. :try_end_8} :catch_1
    .catch Lbpr; {:try_start_8 .. :try_end_8} :catch_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_8 .. :try_end_8} :catch_3
    .catch Ljava/lang/RuntimeException; {:try_start_8 .. :try_end_8} :catch_4
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :try_start_9
    move-object/from16 v0, p0

    iput-wide v8, v0, Lbsu;->o:J

    move-object/from16 v0, p0

    iget-object v4, v0, Lbsu;->p:Landroid/graphics/RectF;

    invoke-virtual {v4, v13}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    monitor-exit v6
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    :try_start_a
    move-object/from16 v0, p0

    iget-object v4, v0, Lbsu;->l:Ljava/util/concurrent/Executor;

    move-object/from16 v0, p0

    iget-object v6, v0, Lbsu;->q:Ljava/lang/Runnable;

    invoke-interface {v4, v6}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 387
    const/4 v4, 0x0

    move-wide v6, v8

    move v8, v5

    move v5, v4

    .line 395
    :goto_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lbsu;->r:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v4}, Ljava/util/concurrent/LinkedBlockingQueue;->poll()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lbtb;

    .line 396
    :cond_d
    :goto_6
    if-nez v8, :cond_18

    if-eqz v4, :cond_18

    .line 397
    invoke-virtual/range {p0 .. p0}, Lbsu;->e()Z

    move-result v9

    .line 398
    sget-object v14, Lbsy;->a:[I

    iget-object v15, v4, Lbtb;->a:Lbta;

    invoke-virtual {v15}, Lbta;->ordinal()I

    move-result v15

    aget v14, v14, v15

    packed-switch v14, :pswitch_data_0

    .line 499
    :goto_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lbsu;->r:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v4}, Ljava/util/concurrent/LinkedBlockingQueue;->poll()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lbtb;
    :try_end_a
    .catch Ljava/lang/InterruptedException; {:try_start_a .. :try_end_a} :catch_0
    .catch Lbgo; {:try_start_a .. :try_end_a} :catch_1
    .catch Lbpr; {:try_start_a .. :try_end_a} :catch_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_a .. :try_end_a} :catch_3
    .catch Ljava/lang/RuntimeException; {:try_start_a .. :try_end_a} :catch_4
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    goto :goto_6

    .line 385
    :catchall_2
    move-exception v4

    :try_start_b
    monitor-exit v6
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    :try_start_c
    throw v4
    :try_end_c
    .catch Ljava/lang/InterruptedException; {:try_start_c .. :try_end_c} :catch_0
    .catch Lbgo; {:try_start_c .. :try_end_c} :catch_1
    .catch Lbpr; {:try_start_c .. :try_end_c} :catch_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_c .. :try_end_c} :catch_3
    .catch Ljava/lang/RuntimeException; {:try_start_c .. :try_end_c} :catch_4
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    .line 506
    :catch_2
    move-exception v4

    .line 507
    const/4 v5, 0x2

    :try_start_d
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lbsu;->a(I)V

    .line 508
    throw v4
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    .line 400
    :pswitch_0
    :try_start_e
    invoke-interface {v11}, Lbsq;->e()V

    .line 401
    invoke-direct/range {p0 .. p0}, Lbsu;->i()V
    :try_end_e
    .catch Ljava/lang/InterruptedException; {:try_start_e .. :try_end_e} :catch_0
    .catch Lbgo; {:try_start_e .. :try_end_e} :catch_1
    .catch Lbpr; {:try_start_e .. :try_end_e} :catch_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_e .. :try_end_e} :catch_3
    .catch Ljava/lang/RuntimeException; {:try_start_e .. :try_end_e} :catch_4
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    goto :goto_7

    .line 509
    :catch_3
    move-exception v4

    .line 510
    const/4 v5, 0x1

    :try_start_f
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lbsu;->a(I)V

    .line 511
    throw v4
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    .line 404
    :pswitch_1
    :try_start_10
    invoke-interface {v11}, Lbsq;->f()V

    .line 405
    sget-object v4, Lbta;->b:Lbta;

    const-wide/16 v14, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v14, v15}, Lbsu;->a(Lbta;J)V
    :try_end_10
    .catch Ljava/lang/InterruptedException; {:try_start_10 .. :try_end_10} :catch_0
    .catch Lbgo; {:try_start_10 .. :try_end_10} :catch_1
    .catch Lbpr; {:try_start_10 .. :try_end_10} :catch_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_10 .. :try_end_10} :catch_3
    .catch Ljava/lang/RuntimeException; {:try_start_10 .. :try_end_10} :catch_4
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    goto :goto_7

    .line 512
    :catch_4
    move-exception v4

    .line 514
    :try_start_11
    invoke-virtual {v4}, Ljava/lang/RuntimeException;->getCause()Ljava/lang/Throwable;

    move-result-object v5

    instance-of v5, v5, Llmd;

    if-eqz v5, :cond_1c

    .line 515
    const/4 v5, 0x3

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lbsu;->a(I)V

    .line 519
    :goto_8
    throw v4
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_1

    .line 410
    :pswitch_2
    const/4 v8, 0x1

    .line 411
    goto :goto_7

    .line 413
    :pswitch_3
    :try_start_12
    sget-object v14, Lbta;->d:Lbta;

    iget-wide v0, v4, Lbtb;->b:J

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v16

    invoke-direct {v0, v14, v1, v2}, Lbsu;->a(Lbta;J)V

    .line 414
    iget-wide v14, v4, Lbtb;->b:J

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15, v11}, Lbsu;->a(JLbsq;)Z

    move-result v14

    if-eqz v14, :cond_f

    .line 415
    if-eqz v9, :cond_e

    .line 416
    sget-object v9, Lbta;->b:Lbta;

    const-wide/16 v14, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v9, v14, v15}, Lbsu;->a(Lbta;J)V

    .line 417
    invoke-interface {v11}, Lbsq;->f()V

    .line 422
    :goto_9
    iget-wide v14, v4, Lbtb;->b:J

    iput-wide v14, v12, Lbsz;->a:J

    goto :goto_7

    .line 419
    :cond_e
    invoke-direct/range {p0 .. p0}, Lbsu;->i()V

    .line 420
    const/4 v5, 0x1

    goto :goto_9

    .line 423
    :cond_f
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v4

    if-eqz v4, :cond_12

    .line 425
    move-object/from16 v0, p0

    iget-object v4, v0, Lbsu;->r:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v4}, Ljava/util/concurrent/LinkedBlockingQueue;->poll()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lbtb;

    .line 426
    if-eqz v4, :cond_11

    .line 427
    iget-object v9, v4, Lbtb;->a:Lbta;

    sget-object v14, Lbta;->a:Lbta;

    if-eq v9, v14, :cond_10

    iget-object v9, v4, Lbtb;->a:Lbta;

    sget-object v14, Lbta;->b:Lbta;

    if-ne v9, v14, :cond_d

    .line 430
    :cond_10
    const-wide/16 v14, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15}, Lbsu;->a(J)V

    goto/16 :goto_5

    .line 436
    :cond_11
    const/4 v8, 0x1

    goto/16 :goto_7

    .line 440
    :cond_12
    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lbsu;->a(Z)V

    .line 441
    const/4 v8, 0x1

    .line 443
    goto/16 :goto_7

    .line 445
    :pswitch_4
    sget-object v14, Lbta;->e:Lbta;

    const-wide/16 v16, 0x0

    move-object/from16 v0, p0

    move-wide/from16 v1, v16

    invoke-direct {v0, v14, v1, v2}, Lbsu;->a(Lbta;J)V

    .line 446
    invoke-interface {v11}, Lbsq;->d()J

    move-result-wide v14

    .line 447
    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15, v11}, Lbsu;->a(JLbsq;)Z

    move-result v14

    if-eqz v14, :cond_14

    .line 448
    if-eqz v9, :cond_13

    .line 449
    sget-object v9, Lbta;->b:Lbta;

    const-wide/16 v14, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v9, v14, v15}, Lbsu;->a(Lbta;J)V

    .line 450
    invoke-interface {v11}, Lbsq;->f()V

    .line 454
    :goto_a
    iget-wide v14, v4, Lbtb;->b:J

    iput-wide v14, v12, Lbsz;->a:J

    goto/16 :goto_7

    .line 452
    :cond_13
    invoke-direct/range {p0 .. p0}, Lbsu;->i()V

    goto :goto_a

    .line 457
    :cond_14
    const/4 v8, 0x1

    .line 461
    goto/16 :goto_7

    .line 463
    :pswitch_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lbsu;->A:Lboi;

    .line 464
    move-object/from16 v0, p0

    iget-object v14, v0, Lbsu;->t:Ljava/lang/Object;

    monitor-enter v14
    :try_end_12
    .catch Ljava/lang/InterruptedException; {:try_start_12 .. :try_end_12} :catch_0
    .catch Lbgo; {:try_start_12 .. :try_end_12} :catch_1
    .catch Lbpr; {:try_start_12 .. :try_end_12} :catch_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_12 .. :try_end_12} :catch_3
    .catch Ljava/lang/RuntimeException; {:try_start_12 .. :try_end_12} :catch_4
    .catchall {:try_start_12 .. :try_end_12} :catchall_1

    .line 465
    :try_start_13
    move-object/from16 v0, p0

    iget-object v15, v0, Lbsu;->B:Lboi;

    move-object/from16 v0, p0

    iput-object v15, v0, Lbsu;->A:Lboi;

    .line 466
    monitor-exit v14
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_3

    .line 468
    :try_start_14
    invoke-interface {v11}, Lbsq;->d()J

    move-result-wide v14

    .line 469
    invoke-interface {v11}, Lbsq;->e()V

    .line 470
    if-eqz v9, :cond_15

    .line 473
    move-object/from16 v0, p0

    iget-object v0, v0, Lbsu;->z:Lbtw;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lbsu;->f:Lbgf;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Lbtw;->a(Lbgf;)Landroid/graphics/Bitmap;

    move-result-object v16

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lbsu;->C:Landroid/graphics/Bitmap;

    .line 477
    :goto_b
    sget-object v16, Lbta;->f:Lbta;

    const-wide/16 v18, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    move-wide/from16 v2, v18

    invoke-direct {v0, v1, v2, v3}, Lbsu;->a(Lbta;J)V

    .line 478
    invoke-virtual {v10}, Lcgw;->b()V

    .line 480
    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15, v11, v4}, Lbsu;->a(JLbsq;Lboi;)Z

    move-result v4

    if-eqz v4, :cond_17

    .line 481
    if-eqz v9, :cond_16

    .line 482
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lbsu;->C:Landroid/graphics/Bitmap;

    .line 483
    sget-object v4, Lbta;->b:Lbta;

    const-wide/16 v14, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v14, v15}, Lbsu;->a(Lbta;J)V

    .line 484
    invoke-interface {v11}, Lbsq;->f()V
    :try_end_14
    .catch Ljava/lang/InterruptedException; {:try_start_14 .. :try_end_14} :catch_0
    .catch Lbgo; {:try_start_14 .. :try_end_14} :catch_1
    .catch Lbpr; {:try_start_14 .. :try_end_14} :catch_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_14 .. :try_end_14} :catch_3
    .catch Ljava/lang/RuntimeException; {:try_start_14 .. :try_end_14} :catch_4
    .catchall {:try_start_14 .. :try_end_14} :catchall_1

    goto/16 :goto_7

    .line 466
    :catchall_3
    move-exception v4

    :try_start_15
    monitor-exit v14
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_3

    :try_start_16
    throw v4

    .line 475
    :cond_15
    const/16 v16, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lbsu;->C:Landroid/graphics/Bitmap;

    goto :goto_b

    .line 486
    :cond_16
    invoke-direct/range {p0 .. p0}, Lbsu;->i()V
    :try_end_16
    .catch Ljava/lang/InterruptedException; {:try_start_16 .. :try_end_16} :catch_0
    .catch Lbgo; {:try_start_16 .. :try_end_16} :catch_1
    .catch Lbpr; {:try_start_16 .. :try_end_16} :catch_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_16 .. :try_end_16} :catch_3
    .catch Ljava/lang/RuntimeException; {:try_start_16 .. :try_end_16} :catch_4
    .catchall {:try_start_16 .. :try_end_16} :catchall_1

    goto/16 :goto_7

    .line 490
    :cond_17
    const/4 v8, 0x1

    goto/16 :goto_7

    :cond_18
    move v4, v5

    move v5, v8

    .line 501
    goto/16 :goto_3

    .line 522
    :cond_19
    sget-object v4, Lbta;->c:Lbta;

    const-wide/16 v6, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v6, v7}, Lbsu;->a(Lbta;J)V

    .line 524
    move-object/from16 v0, p0

    iget-object v4, v0, Lbsu;->y:Lbrx;

    invoke-static {v4}, Lcgr;->a(Lcgq;)V

    .line 525
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lbsu;->y:Lbrx;

    .line 526
    move-object/from16 v0, p0

    iget-object v4, v0, Lbsu;->z:Lbtw;

    invoke-static {v4}, Lcgr;->a(Lcgq;)V

    .line 527
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lbsu;->z:Lbtw;

    .line 528
    move-object/from16 v0, p0

    iget-object v4, v0, Lbsu;->r:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v4}, Ljava/util/concurrent/LinkedBlockingQueue;->clear()V

    .line 529
    invoke-virtual {v10}, Lcgw;->b()V

    .line 533
    move-object/from16 v0, p0

    iget-object v5, v0, Lbsu;->t:Ljava/lang/Object;

    monitor-enter v5

    .line 534
    :try_start_17
    move-object/from16 v0, p0

    iget v4, v0, Lbsu;->w:I

    if-eqz v4, :cond_1b

    const/4 v4, 0x1

    .line 535
    :goto_c
    monitor-exit v5
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_4

    .line 536
    if-eqz v4, :cond_1a

    .line 537
    move-object/from16 v0, p0

    iget-object v4, v0, Lbsu;->h:Lasy;

    invoke-virtual {v4}, Lasy;->b()Livo;

    move-result-object v4

    sget-object v5, Lbsu;->a:Ljava/lang/String;

    invoke-static {v4, v5}, Livl;->a(Livo;Ljava/lang/String;)V

    .line 540
    :cond_1a
    move-object/from16 v0, p0

    iget-object v4, v0, Lbsu;->h:Lasy;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lasy;->b(Livo;)V

    goto/16 :goto_0

    .line 534
    :cond_1b
    const/4 v4, 0x0

    goto :goto_c

    .line 535
    :catchall_4
    move-exception v4

    :try_start_18
    monitor-exit v5
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_4

    throw v4

    .line 517
    :cond_1c
    const/4 v5, 0x1

    :try_start_19
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lbsu;->a(I)V
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_1

    goto/16 :goto_8

    .line 534
    :cond_1d
    const/4 v5, 0x0

    goto/16 :goto_4

    .line 535
    :catchall_5
    move-exception v4

    :try_start_1a
    monitor-exit v6
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_5

    throw v4

    :cond_1e
    move v8, v5

    move v5, v4

    goto/16 :goto_5

    .line 398
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public b()V
    .locals 3

    .prologue
    .line 572
    iget-object v1, p0, Lbsu;->t:Ljava/lang/Object;

    monitor-enter v1

    .line 573
    :try_start_0
    iget-object v0, p0, Lbsu;->v:Lbta;

    sget-object v2, Lbta;->b:Lbta;

    if-eq v0, v2, :cond_0

    .line 574
    iget-object v0, p0, Lbsu;->r:Ljava/util/concurrent/LinkedBlockingQueue;

    sget-object v2, Lbta;->b:Lbta;

    invoke-static {v2}, Lbtb;->a(Lbta;)Lbtb;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/concurrent/LinkedBlockingQueue;->add(Ljava/lang/Object;)Z

    .line 575
    iget-object v0, p0, Lbsu;->v:Lbta;

    sget-object v2, Lbta;->d:Lbta;

    if-ne v0, v2, :cond_0

    .line 576
    iget-object v0, p0, Lbsu;->z:Lbtw;

    invoke-virtual {v0}, Lbtw;->a()V

    .line 579
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public c()V
    .locals 5

    .prologue
    .line 600
    iget-object v1, p0, Lbsu;->t:Ljava/lang/Object;

    monitor-enter v1

    .line 601
    :try_start_0
    iget-object v0, p0, Lbsu;->r:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingQueue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 602
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 603
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbtb;

    .line 606
    iget-object v3, v0, Lbtb;->a:Lbta;

    sget-object v4, Lbta;->d:Lbta;

    if-eq v3, v4, :cond_1

    iget-object v0, v0, Lbtb;->a:Lbta;

    sget-object v3, Lbta;->e:Lbta;

    if-ne v0, v3, :cond_0

    .line 608
    :cond_1
    monitor-exit v1

    .line 612
    :goto_0
    return-void

    .line 611
    :cond_2
    iget-object v0, p0, Lbsu;->r:Ljava/util/concurrent/LinkedBlockingQueue;

    sget-object v2, Lbta;->e:Lbta;

    invoke-static {v2}, Lbtb;->a(Lbta;)Lbtb;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/concurrent/LinkedBlockingQueue;->add(Ljava/lang/Object;)Z

    .line 612
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public d()V
    .locals 2

    .prologue
    .line 616
    iget-object v0, p0, Lbsu;->u:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 617
    return-void
.end method

.method public e()Z
    .locals 3

    .prologue
    .line 626
    iget-object v1, p0, Lbsu;->t:Ljava/lang/Object;

    monitor-enter v1

    .line 627
    :try_start_0
    iget-object v0, p0, Lbsu;->v:Lbta;

    sget-object v2, Lbta;->b:Lbta;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 628
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public f()Z
    .locals 3

    .prologue
    .line 632
    iget-object v1, p0, Lbsu;->t:Ljava/lang/Object;

    monitor-enter v1

    .line 633
    :try_start_0
    iget-object v0, p0, Lbsu;->v:Lbta;

    sget-object v2, Lbta;->a:Lbta;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 634
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public g()Z
    .locals 3

    .prologue
    .line 638
    iget-object v1, p0, Lbsu;->t:Ljava/lang/Object;

    monitor-enter v1

    .line 639
    :try_start_0
    iget-object v0, p0, Lbsu;->v:Lbta;

    sget-object v2, Lbta;->f:Lbta;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 640
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
