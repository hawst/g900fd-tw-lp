.class public final Leyu;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lkvh;


# instance fields
.field private a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 3909
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3910
    iput-object p1, p0, Leyu;->a:Landroid/content/Context;

    .line 3911
    return-void
.end method


# virtual methods
.method public a(ILjava/lang/String;II)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 3931
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Leyu;->a:Landroid/content/Context;

    const-class v2, Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareMemberSearchActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 3932
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3933
    const-string v1, "square_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3934
    const-string v1, "square_membership"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3935
    const-string v1, "square_joinability"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3936
    return-object v0
.end method

.method public a(ILjava/lang/String;IILjava/lang/Integer;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 3916
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Leyu;->a:Landroid/content/Context;

    const-class v2, Lcom/google/android/apps/plus/squares/legacyimpl/SquareMemberListActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 3917
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3918
    const-string v1, "square_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3919
    const-string v1, "square_membership"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3920
    const-string v1, "square_joinability"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3921
    if-eqz p5, :cond_0

    .line 3922
    const-string v1, "square_member_list_type"

    .line 3923
    invoke-virtual {p5}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 3922
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3925
    :cond_0
    return-object v0
.end method
