.class public final Lpgr;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lppf;

.field private c:[B

.field private d:[B

.field private e:Ljava/lang/Float;

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 119
    invoke-direct {p0}, Loxq;-><init>()V

    .line 130
    const/4 v0, 0x0

    iput-object v0, p0, Lpgr;->b:Lppf;

    .line 119
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 161
    const/4 v0, 0x0

    .line 162
    iget-object v1, p0, Lpgr;->c:[B

    if-eqz v1, :cond_0

    .line 163
    const/4 v0, 0x1

    iget-object v1, p0, Lpgr;->c:[B

    .line 164
    invoke-static {v0, v1}, Loxo;->b(I[B)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 166
    :cond_0
    iget-object v1, p0, Lpgr;->a:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 167
    const/4 v1, 0x2

    iget-object v2, p0, Lpgr;->a:Ljava/lang/String;

    .line 168
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 170
    :cond_1
    iget-object v1, p0, Lpgr;->d:[B

    if-eqz v1, :cond_2

    .line 171
    const/4 v1, 0x3

    iget-object v2, p0, Lpgr;->d:[B

    .line 172
    invoke-static {v1, v2}, Loxo;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 174
    :cond_2
    iget-object v1, p0, Lpgr;->e:Ljava/lang/Float;

    if-eqz v1, :cond_3

    .line 175
    const/4 v1, 0x4

    iget-object v2, p0, Lpgr;->e:Ljava/lang/Float;

    .line 176
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 178
    :cond_3
    iget-object v1, p0, Lpgr;->b:Lppf;

    if-eqz v1, :cond_4

    .line 179
    const/4 v1, 0x5

    iget-object v2, p0, Lpgr;->b:Lppf;

    .line 180
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 182
    :cond_4
    iget-object v1, p0, Lpgr;->f:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 183
    const/4 v1, 0x6

    iget-object v2, p0, Lpgr;->f:Ljava/lang/String;

    .line 184
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 186
    :cond_5
    iget-object v1, p0, Lpgr;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 187
    iput v0, p0, Lpgr;->ai:I

    .line 188
    return v0
.end method

.method public a(Loxn;)Lpgr;
    .locals 2

    .prologue
    .line 196
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 197
    sparse-switch v0, :sswitch_data_0

    .line 201
    iget-object v1, p0, Lpgr;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 202
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpgr;->ah:Ljava/util/List;

    .line 205
    :cond_1
    iget-object v1, p0, Lpgr;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 207
    :sswitch_0
    return-object p0

    .line 212
    :sswitch_1
    invoke-virtual {p1}, Loxn;->l()[B

    move-result-object v0

    iput-object v0, p0, Lpgr;->c:[B

    goto :goto_0

    .line 216
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpgr;->a:Ljava/lang/String;

    goto :goto_0

    .line 220
    :sswitch_3
    invoke-virtual {p1}, Loxn;->l()[B

    move-result-object v0

    iput-object v0, p0, Lpgr;->d:[B

    goto :goto_0

    .line 224
    :sswitch_4
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpgr;->e:Ljava/lang/Float;

    goto :goto_0

    .line 228
    :sswitch_5
    iget-object v0, p0, Lpgr;->b:Lppf;

    if-nez v0, :cond_2

    .line 229
    new-instance v0, Lppf;

    invoke-direct {v0}, Lppf;-><init>()V

    iput-object v0, p0, Lpgr;->b:Lppf;

    .line 231
    :cond_2
    iget-object v0, p0, Lpgr;->b:Lppf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 235
    :sswitch_6
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpgr;->f:Ljava/lang/String;

    goto :goto_0

    .line 197
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x25 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 137
    iget-object v0, p0, Lpgr;->c:[B

    if-eqz v0, :cond_0

    .line 138
    const/4 v0, 0x1

    iget-object v1, p0, Lpgr;->c:[B

    invoke-virtual {p1, v0, v1}, Loxo;->a(I[B)V

    .line 140
    :cond_0
    iget-object v0, p0, Lpgr;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 141
    const/4 v0, 0x2

    iget-object v1, p0, Lpgr;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 143
    :cond_1
    iget-object v0, p0, Lpgr;->d:[B

    if-eqz v0, :cond_2

    .line 144
    const/4 v0, 0x3

    iget-object v1, p0, Lpgr;->d:[B

    invoke-virtual {p1, v0, v1}, Loxo;->a(I[B)V

    .line 146
    :cond_2
    iget-object v0, p0, Lpgr;->e:Ljava/lang/Float;

    if-eqz v0, :cond_3

    .line 147
    const/4 v0, 0x4

    iget-object v1, p0, Lpgr;->e:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 149
    :cond_3
    iget-object v0, p0, Lpgr;->b:Lppf;

    if-eqz v0, :cond_4

    .line 150
    const/4 v0, 0x5

    iget-object v1, p0, Lpgr;->b:Lppf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 152
    :cond_4
    iget-object v0, p0, Lpgr;->f:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 153
    const/4 v0, 0x6

    iget-object v1, p0, Lpgr;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 155
    :cond_5
    iget-object v0, p0, Lpgr;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 157
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 115
    invoke-virtual {p0, p1}, Lpgr;->a(Loxn;)Lpgr;

    move-result-object v0

    return-object v0
.end method
