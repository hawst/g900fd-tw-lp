.class public final Lmny;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmny;


# instance fields
.field public b:[Lmnz;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6279
    const/4 v0, 0x0

    new-array v0, v0, [Lmny;

    sput-object v0, Lmny;->a:[Lmny;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 6280
    invoke-direct {p0}, Loxq;-><init>()V

    .line 6360
    sget-object v0, Lmnz;->a:[Lmnz;

    iput-object v0, p0, Lmny;->b:[Lmnz;

    .line 6280
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 6378
    .line 6379
    iget-object v1, p0, Lmny;->b:[Lmnz;

    if-eqz v1, :cond_1

    .line 6380
    iget-object v2, p0, Lmny;->b:[Lmnz;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 6381
    if-eqz v4, :cond_0

    .line 6382
    const/4 v5, 0x1

    .line 6383
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 6380
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 6387
    :cond_1
    iget-object v1, p0, Lmny;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6388
    iput v0, p0, Lmny;->ai:I

    .line 6389
    return v0
.end method

.method public a(Loxn;)Lmny;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 6397
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 6398
    sparse-switch v0, :sswitch_data_0

    .line 6402
    iget-object v2, p0, Lmny;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 6403
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmny;->ah:Ljava/util/List;

    .line 6406
    :cond_1
    iget-object v2, p0, Lmny;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6408
    :sswitch_0
    return-object p0

    .line 6413
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 6414
    iget-object v0, p0, Lmny;->b:[Lmnz;

    if-nez v0, :cond_3

    move v0, v1

    .line 6415
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lmnz;

    .line 6416
    iget-object v3, p0, Lmny;->b:[Lmnz;

    if-eqz v3, :cond_2

    .line 6417
    iget-object v3, p0, Lmny;->b:[Lmnz;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 6419
    :cond_2
    iput-object v2, p0, Lmny;->b:[Lmnz;

    .line 6420
    :goto_2
    iget-object v2, p0, Lmny;->b:[Lmnz;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 6421
    iget-object v2, p0, Lmny;->b:[Lmnz;

    new-instance v3, Lmnz;

    invoke-direct {v3}, Lmnz;-><init>()V

    aput-object v3, v2, v0

    .line 6422
    iget-object v2, p0, Lmny;->b:[Lmnz;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 6423
    invoke-virtual {p1}, Loxn;->a()I

    .line 6420
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 6414
    :cond_3
    iget-object v0, p0, Lmny;->b:[Lmnz;

    array-length v0, v0

    goto :goto_1

    .line 6426
    :cond_4
    iget-object v2, p0, Lmny;->b:[Lmnz;

    new-instance v3, Lmnz;

    invoke-direct {v3}, Lmnz;-><init>()V

    aput-object v3, v2, v0

    .line 6427
    iget-object v2, p0, Lmny;->b:[Lmnz;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 6398
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 6365
    iget-object v0, p0, Lmny;->b:[Lmnz;

    if-eqz v0, :cond_1

    .line 6366
    iget-object v1, p0, Lmny;->b:[Lmnz;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 6367
    if-eqz v3, :cond_0

    .line 6368
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 6366
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 6372
    :cond_1
    iget-object v0, p0, Lmny;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 6374
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6276
    invoke-virtual {p0, p1}, Lmny;->a(Loxn;)Lmny;

    move-result-object v0

    return-object v0
.end method
