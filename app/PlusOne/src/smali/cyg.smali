.class public Lcyg;
.super Llol;
.source "PG"


# static fields
.field private static final N:Ldgq;


# instance fields
.field private O:Ldgr;

.field private P:Ldeo;

.field private Q:Ljava/lang/Integer;

.field private final R:Lcyj;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 32
    new-instance v0, Ldgq;

    const v1, 0x7f10069d

    invoke-direct {v0, v1}, Ldgq;-><init>(I)V

    sput-object v0, Lcyg;->N:Ldgq;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    .line 30
    invoke-direct {p0}, Llol;-><init>()V

    .line 40
    new-instance v0, Lcyj;

    invoke-direct {v0, p0}, Lcyj;-><init>(Lcyg;)V

    iput-object v0, p0, Lcyg;->R:Lcyj;

    .line 43
    new-instance v0, Ldgn;

    iget-object v1, p0, Lcyg;->av:Llqm;

    sget-object v2, Lcyg;->N:Ldgq;

    new-instance v3, Lcyi;

    invoke-direct {v3, p0}, Lcyi;-><init>(Lcyg;)V

    invoke-direct {v0, v1, v2, v3}, Ldgn;-><init>(Llqr;Ldgu;Ldgv;)V

    .line 44
    new-instance v0, Ldep;

    iget-object v1, p0, Lcyg;->av:Llqm;

    new-instance v2, Lcyh;

    invoke-direct {v2, p0}, Lcyh;-><init>(Lcyg;)V

    invoke-direct {v0, v1, v2}, Ldep;-><init>(Llqr;Ldeq;)V

    .line 116
    return-void
.end method

.method static synthetic a(Lcyg;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 0

    .prologue
    .line 30
    iput-object p1, p0, Lcyg;->Q:Ljava/lang/Integer;

    return-object p1
.end method

.method private a()V
    .locals 3

    .prologue
    .line 82
    iget-object v0, p0, Lcyg;->O:Ldgr;

    sget-object v1, Lcyg;->N:Ldgq;

    iget-object v2, p0, Lcyg;->P:Ldeo;

    invoke-virtual {v2}, Ldeo;->a()Lddl;

    move-result-object v2

    invoke-interface {v2}, Lddl;->v()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Ldgr;->a(Ldgu;Z)V

    .line 83
    return-void
.end method

.method static synthetic a(Lcyg;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcyg;->a()V

    return-void
.end method

.method static synthetic b(Lcyg;)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcyg;->Q:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic c(Lcyg;)Ldeo;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcyg;->P:Ldeo;

    return-object v0
.end method


# virtual methods
.method public aO_()V
    .locals 3

    .prologue
    .line 62
    invoke-super {p0}, Llol;->aO_()V

    .line 64
    iget-object v0, p0, Lcyg;->R:Lcyj;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(Lfhh;)V

    .line 65
    iget-object v0, p0, Lcyg;->Q:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 66
    iget-object v0, p0, Lcyg;->Q:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 67
    iget-object v0, p0, Lcyg;->Q:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(I)Lfib;

    move-result-object v0

    .line 68
    iget-object v1, p0, Lcyg;->R:Lcyj;

    iget-object v2, p0, Lcyg;->Q:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2, v0}, Lcyj;->a(ILfib;)Z

    .line 72
    :cond_0
    invoke-direct {p0}, Lcyg;->a()V

    .line 73
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 54
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 55
    iget-object v0, p0, Lcyg;->au:Llnh;

    const-class v1, Ldgr;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldgr;

    iput-object v0, p0, Lcyg;->O:Ldgr;

    .line 56
    iget-object v0, p0, Lcyg;->au:Llnh;

    const-class v1, Ldeo;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldeo;

    iput-object v0, p0, Lcyg;->P:Ldeo;

    .line 57
    iget-object v0, p0, Lcyg;->O:Ldgr;

    sget-object v1, Lcyg;->N:Ldgq;

    invoke-virtual {v0, v1}, Ldgr;->a(Ldgu;)V

    .line 58
    return-void
.end method

.method public z()V
    .locals 1

    .prologue
    .line 77
    invoke-super {p0}, Llol;->z()V

    .line 78
    iget-object v0, p0, Lcyg;->R:Lcyj;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(Lfhh;)V

    .line 79
    return-void
.end method
