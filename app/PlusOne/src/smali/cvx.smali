.class public Lcvx;
.super Llol;
.source "PG"


# static fields
.field private static final N:Ldgq;

.field private static final O:Ldgq;


# instance fields
.field private final P:Lhoc;

.field private Q:Ldgr;

.field private R:Ldeo;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 34
    new-instance v0, Ldgq;

    const v1, 0x7f10069f

    invoke-direct {v0, v1}, Ldgq;-><init>(I)V

    sput-object v0, Lcvx;->N:Ldgq;

    .line 36
    new-instance v0, Ldgq;

    const v1, 0x7f1006a0

    invoke-direct {v0, v1}, Ldgq;-><init>(I)V

    sput-object v0, Lcvx;->O:Ldgq;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    .line 33
    invoke-direct {p0}, Llol;-><init>()V

    .line 39
    new-instance v0, Lhoc;

    iget-object v1, p0, Lcvx;->av:Llqm;

    invoke-direct {v0, p0, v1}, Lhoc;-><init>(Lu;Llqr;)V

    iput-object v0, p0, Lcvx;->P:Lhoc;

    .line 46
    new-instance v0, Ldgn;

    iget-object v1, p0, Lcvx;->av:Llqm;

    sget-object v2, Lcvx;->N:Ldgq;

    new-instance v3, Lcvy;

    invoke-direct {v3, p0}, Lcvy;-><init>(Lcvx;)V

    invoke-direct {v0, v1, v2, v3}, Ldgn;-><init>(Llqr;Ldgu;Ldgv;)V

    .line 52
    new-instance v0, Ldgn;

    iget-object v1, p0, Lcvx;->av:Llqm;

    sget-object v2, Lcvx;->O:Ldgq;

    new-instance v3, Lcvz;

    invoke-direct {v3, p0}, Lcvz;-><init>(Lcvx;)V

    invoke-direct {v0, v1, v2, v3}, Ldgn;-><init>(Llqr;Ldgu;Ldgv;)V

    .line 58
    new-instance v0, Ldep;

    iget-object v1, p0, Lcvx;->av:Llqm;

    new-instance v2, Lcwa;

    invoke-direct {v2, p0}, Lcwa;-><init>(Lcvx;)V

    invoke-direct {v0, v1, v2}, Ldep;-><init>(Llqr;Ldeq;)V

    .line 64
    new-instance v0, Ldep;

    iget-object v1, p0, Lcvx;->av:Llqm;

    new-instance v2, Lcwb;

    invoke-direct {v2, p0}, Lcwb;-><init>(Lcvx;)V

    invoke-direct {v0, v1, v2}, Ldep;-><init>(Llqr;Ldeq;)V

    .line 70
    return-void
.end method

.method static synthetic a(Lcvx;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 33
    iget-object v0, p0, Lcvx;->R:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcvx;->R:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    invoke-interface {v0}, Lddl;->Q()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcvx;->R:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    invoke-interface {v0}, Lddl;->F()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcvx;->R:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    invoke-interface {v0}, Lddl;->g()Lnym;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcvx;->R:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    invoke-interface {v0}, Lddl;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    move v4, v1

    :goto_0
    if-eqz v4, :cond_1

    iget-object v0, p0, Lcvx;->R:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    invoke-interface {v0}, Lddl;->g()Lnym;

    move-result-object v0

    iget-object v0, v0, Lnym;->H:Ljava/lang/Boolean;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iget-object v5, p0, Lcvx;->Q:Ldgr;

    sget-object v6, Lcvx;->N:Ldgq;

    if-eqz v4, :cond_2

    if-nez v0, :cond_2

    move v3, v1

    :goto_2
    invoke-virtual {v5, v6, v3}, Ldgr;->a(Ldgu;Z)V

    iget-object v3, p0, Lcvx;->Q:Ldgr;

    sget-object v5, Lcvx;->O:Ldgq;

    if-eqz v4, :cond_3

    if-eqz v0, :cond_3

    :goto_3
    invoke-virtual {v3, v5, v1}, Ldgr;->a(Ldgu;Z)V

    return-void

    :cond_0
    move v4, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v3, v2

    goto :goto_2

    :cond_3
    move v1, v2

    goto :goto_3
.end method

.method static synthetic a(Lcvx;Z)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 33
    invoke-virtual {p0}, Lcvx;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "account_id"

    const/4 v3, -0x1

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    iget-object v0, p0, Lcvx;->R:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    invoke-interface {v0}, Lddl;->g()Lnym;

    move-result-object v2

    new-instance v5, Lnwy;

    invoke-direct {v5}, Lnwy;-><init>()V

    iget-object v0, v2, Lnym;->h:Lnyz;

    if-eqz v0, :cond_1

    iget-object v0, v2, Lnym;->h:Lnyz;

    iget-object v0, v0, Lnyz;->c:Ljava/lang/String;

    :goto_0
    iput-object v0, v5, Lnwy;->a:Ljava/lang/String;

    iget-object v0, v2, Lnym;->e:Ljava/lang/String;

    iput-object v0, v5, Lnwy;->b:Ljava/lang/String;

    iget-object v0, v2, Lnym;->f:Ljava/lang/String;

    iput-object v0, v5, Lnwy;->c:Ljava/lang/String;

    iget-object v0, v2, Lnym;->m:Lnzb;

    if-eqz v0, :cond_0

    iget-object v0, v2, Lnym;->m:Lnzb;

    iget-object v1, v0, Lnzb;->a:Ljava/lang/String;

    :cond_0
    iput-object v1, v5, Lnwy;->d:Ljava/lang/String;

    new-instance v0, Lcwc;

    iget-object v2, p0, Lcvx;->at:Llnl;

    const-class v1, Ldii;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    move-object v1, p0

    move v6, p1

    invoke-direct/range {v0 .. v6}, Lcwc;-><init>(Lcvx;Landroid/content/Context;Ljava/lang/String;ILnwy;Z)V

    iget-object v1, p0, Lcvx;->P:Lhoc;

    invoke-virtual {v1, v0}, Lhoc;->c(Lhny;)V

    return-void

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method static synthetic b(Lcvx;)Llnl;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcvx;->at:Llnl;

    return-object v0
.end method

.method static synthetic c(Lcvx;)Llnl;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcvx;->at:Llnl;

    return-object v0
.end method

.method static synthetic d(Lcvx;)Llnl;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcvx;->at:Llnl;

    return-object v0
.end method

.method static synthetic e(Lcvx;)Llnl;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcvx;->at:Llnl;

    return-object v0
.end method


# virtual methods
.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 74
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 76
    iget-object v0, p0, Lcvx;->au:Llnh;

    const-class v1, Ldgr;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldgr;

    iput-object v0, p0, Lcvx;->Q:Ldgr;

    .line 77
    iget-object v0, p0, Lcvx;->au:Llnh;

    const-class v1, Ldeo;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldeo;

    iput-object v0, p0, Lcvx;->R:Ldeo;

    .line 78
    return-void
.end method
