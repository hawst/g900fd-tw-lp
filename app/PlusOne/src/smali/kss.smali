.class public final Lkss;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhec;
.implements Lhob;
.implements Llnx;
.implements Llrg;


# instance fields
.field private final a:Lu;

.field private final b:Lktc;

.field private final c:Lktb;

.field private d:Lhoc;

.field private e:Landroid/content/Context;

.field private f:Lhee;


# direct methods
.method public constructor <init>(Lu;Llqr;Lktc;Lktb;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lkss;->a:Lu;

    .line 42
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 43
    iput-object p3, p0, Lkss;->b:Lktc;

    .line 44
    iput-object p4, p0, Lkss;->c:Lktb;

    .line 45
    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 59
    new-instance v0, Lksq;

    invoke-direct {v0}, Lksq;-><init>()V

    .line 60
    iget-object v1, p0, Lkss;->a:Lu;

    invoke-virtual {v1}, Lu;->q()Lae;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lksq;->a(Lae;Ljava/lang/String;)V

    .line 61
    return-void
.end method

.method public a(Landroid/content/Context;Llnh;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 49
    iput-object p1, p0, Lkss;->e:Landroid/content/Context;

    .line 50
    const-class v0, Lhee;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Lkss;->f:Lhee;

    .line 51
    const-class v0, Lhoc;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    iput-object v0, p0, Lkss;->d:Lhoc;

    .line 52
    iget-object v0, p0, Lkss;->d:Lhoc;

    invoke-virtual {v0, p0}, Lhoc;->a(Lhob;)Lhoc;

    .line 53
    return-void
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 5

    .prologue
    .line 77
    const-string v0, "ReportSquareTask"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lkss;->c:Lktb;

    invoke-interface {v0}, Lktb;->c()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 79
    iget-object v0, p0, Lkss;->d:Lhoc;

    new-instance v1, Lkti;

    iget-object v2, p0, Lkss;->e:Landroid/content/Context;

    iget-object v3, p0, Lkss;->f:Lhee;

    .line 80
    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    iget-object v4, p0, Lkss;->b:Lktc;

    invoke-interface {v4}, Lktc;->d()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lkti;-><init>(Landroid/content/Context;ILjava/lang/String;)V

    .line 79
    invoke-virtual {v0, v1}, Lhoc;->b(Lhny;)V

    .line 83
    :cond_0
    return-void
.end method

.method public c(I)V
    .locals 5

    .prologue
    .line 70
    iget-object v0, p0, Lkss;->d:Lhoc;

    new-instance v1, Lkst;

    iget-object v2, p0, Lkss;->e:Landroid/content/Context;

    iget-object v3, p0, Lkss;->f:Lhee;

    .line 71
    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    iget-object v4, p0, Lkss;->b:Lktc;

    invoke-interface {v4}, Lktc;->d()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4, p1}, Lkst;-><init>(Landroid/content/Context;ILjava/lang/String;I)V

    .line 70
    invoke-virtual {v0, v1}, Lhoc;->c(Lhny;)V

    .line 72
    return-void
.end method
