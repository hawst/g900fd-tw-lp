.class public final Lldt;
.super Landroid/view/View;
.source "PG"


# instance fields
.field private a:I

.field private b:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lldt;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lldt;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    return-void
.end method


# virtual methods
.method public a(II)V
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Lldt;->a(III)V

    .line 34
    return-void
.end method

.method public a(III)V
    .locals 1

    .prologue
    .line 37
    iput p1, p0, Lldt;->a:I

    .line 38
    iput p2, p0, Lldt;->b:I

    .line 39
    new-instance v0, Lldx;

    invoke-direct {v0, p2}, Lldx;-><init>(I)V

    .line 40
    iput p3, v0, Lldx;->a:I

    .line 41
    invoke-virtual {p0, v0}, Lldt;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 42
    return-void
.end method

.method protected onMeasure(II)V
    .locals 2

    .prologue
    .line 46
    iget v0, p0, Lldt;->a:I

    iget v1, p0, Lldt;->b:I

    invoke-virtual {p0, v0, v1}, Lldt;->setMeasuredDimension(II)V

    .line 47
    return-void
.end method
