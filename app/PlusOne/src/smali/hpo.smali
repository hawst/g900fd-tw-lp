.class public Lhpo;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static a:Lhpo;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Landroid/os/Handler;

.field private final e:Lhrj;

.field private final f:Lhqz;

.field private final g:Lhei;

.field private volatile h:Lhpt;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lhpo;->c:Ljava/util/HashSet;

    .line 69
    iput-object p1, p0, Lhpo;->b:Landroid/content/Context;

    .line 70
    const-class v0, Lhrj;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhrj;

    iput-object v0, p0, Lhpo;->e:Lhrj;

    .line 71
    iget-object v0, p0, Lhpo;->b:Landroid/content/Context;

    invoke-static {v0}, Lhqz;->a(Landroid/content/Context;)Lhqz;

    move-result-object v0

    iput-object v0, p0, Lhpo;->f:Lhqz;

    .line 72
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Lhpo;->g:Lhei;

    .line 74
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "iu-sync-manager"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    .line 76
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 77
    new-instance v1, Lhpq;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v1, p0, v0}, Lhpq;-><init>(Lhpo;Landroid/os/Looper;)V

    iput-object v1, p0, Lhpo;->d:Landroid/os/Handler;

    .line 78
    iget-object v0, p0, Lhpo;->d:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 79
    new-instance v0, Lhpp;

    invoke-direct {v0, p0}, Lhpp;-><init>(Lhpo;)V

    .line 88
    iget-object v1, p0, Lhpo;->b:Landroid/content/Context;

    invoke-static {v1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Landroid/accounts/AccountManager;->addOnAccountsUpdatedListener(Landroid/accounts/OnAccountsUpdateListener;Landroid/os/Handler;Z)V

    .line 90
    return-void
.end method

.method static synthetic a(Lhpo;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lhpo;->d:Landroid/os/Handler;

    return-object v0
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lhpo;
    .locals 3

    .prologue
    .line 62
    const-class v1, Lhpo;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lhpo;->a:Lhpo;

    if-nez v0, :cond_0

    .line 63
    new-instance v0, Lhpo;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lhpo;-><init>(Landroid/content/Context;)V

    sput-object v0, Lhpo;->a:Lhpo;

    .line 65
    :cond_0
    sget-object v0, Lhpo;->a:Lhpo;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 62
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(IZ)Lhra;
    .locals 7

    .prologue
    const/4 v6, -0x1

    const/4 v0, 0x0

    const/4 v5, 0x4

    .line 301
    :goto_0
    iget-object v1, p0, Lhpo;->e:Lhrj;

    .line 302
    invoke-static {v1, p1}, Lhqn;->b(Lhrj;I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    move-result-object v1

    .line 303
    if-nez v1, :cond_b

    .line 304
    iget-object v1, p0, Lhpo;->b:Landroid/content/Context;

    invoke-static {v1, p1, v6}, Lhqn;->a(Landroid/content/Context;II)Z

    .line 305
    iget-object v1, p0, Lhpo;->e:Lhrj;

    .line 306
    invoke-static {v1, p1}, Lhqn;->b(Lhrj;I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    move-result-object v1

    move-object v2, v1

    .line 308
    :goto_1
    if-nez v2, :cond_1

    .line 354
    :cond_0
    :goto_2
    return-object v0

    .line 315
    :cond_1
    invoke-virtual {v2}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->m()I

    move-result v1

    .line 316
    iget-object v3, p0, Lhpo;->b:Landroid/content/Context;

    invoke-static {v3, v1}, Lhpo;->a(Landroid/content/Context;I)Z

    move-result v3

    if-nez v3, :cond_4

    .line 319
    const-string v2, "iu.SyncManager"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 320
    const-string v2, "invalid account, remove all uploads in DB: "

    .line 321
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v3}, Lifu;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 320
    :cond_2
    :goto_3
    iget-object v2, p0, Lhpo;->e:Lhrj;

    invoke-static {v2, v1}, Lhqn;->a(Lhrj;I)V

    goto :goto_0

    .line 321
    :cond_3
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3

    .line 327
    :cond_4
    new-instance v1, Lhra;

    iget-object v3, p0, Lhpo;->b:Landroid/content/Context;

    invoke-direct {v1, v3, v2}, Lhra;-><init>(Landroid/content/Context;Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;)V

    .line 328
    invoke-static {}, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->e()Z

    move-result v2

    if-nez v2, :cond_8

    .line 329
    const-string v2, "iu.SyncManager"

    invoke-static {v2, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 330
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x21

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "--- NEW; skip: no storage; task: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 332
    :cond_5
    const/16 v2, 0xb

    invoke-virtual {v1, v2}, Lhra;->a(I)V

    .line 341
    :cond_6
    :goto_4
    invoke-virtual {v1}, Lhra;->a()I

    move-result v2

    .line 342
    if-eqz p2, :cond_9

    invoke-direct {p0, v2}, Lhpo;->e(I)Z

    move-result v3

    if-nez v3, :cond_7

    invoke-virtual {v1}, Lhra;->e()Z

    move-result v3

    if-nez v3, :cond_9

    .line 343
    :cond_7
    const-string v2, "iu.SyncManager"

    invoke-static {v2, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 344
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x16

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "NEXT; rejected; task: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    .line 334
    :cond_8
    const-string v2, "iu.SyncManager"

    invoke-static {v2, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 335
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x17

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "--- NEW; upload; task: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 348
    :cond_9
    if-eq p1, v6, :cond_a

    if-eq v2, p1, :cond_a

    .line 349
    const-string v2, "iu.SyncManager"

    invoke-static {v2, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 350
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1b

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "NEXT; wrong account; task: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    :cond_a
    move-object v0, v1

    .line 354
    goto/16 :goto_2

    :cond_b
    move-object v2, v1

    goto/16 :goto_1
.end method

.method static synthetic a(Lhpo;IZ)Lhra;
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Lhpo;->a(IZ)Lhra;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lhpo;I)V
    .locals 3

    .prologue
    const/16 v2, 0x28

    .line 40
    iget-object v0, p0, Lhpo;->e:Lhrj;

    invoke-static {v0, p1, v2}, Lhqn;->a(Lhrj;II)V

    iget-object v0, p0, Lhpo;->h:Lhpt;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lhpt;->c()Lhra;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lhra;->b()I

    move-result v1

    if-ne v1, v2, :cond_0

    invoke-virtual {v0}, Lhra;->f()V

    :cond_0
    return-void
.end method

.method static synthetic a(Lhpo;J)V
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lhpo;->h:Lhpt;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lhpt;->c()Lhra;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1, p2}, Lhra;->a(J)Z

    :cond_0
    return-void
.end method

.method private static a(Landroid/content/Context;I)Z
    .locals 7

    .prologue
    const/4 v6, -0x1

    const/4 v1, 0x0

    .line 537
    if-ne p1, v6, :cond_0

    move v0, v1

    .line 549
    :goto_0
    return v0

    .line 541
    :cond_0
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 542
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    .line 543
    const-string v3, "com.google"

    invoke-virtual {v2, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v3

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 544
    iget-object v5, v5, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v0, v5}, Lhei;->a(Ljava/lang/String;)I

    move-result v5

    .line 545
    if-eq v5, v6, :cond_1

    if-ne v5, p1, :cond_1

    .line 546
    const/4 v0, 0x1

    goto :goto_0

    .line 543
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    move v0, v1

    .line 549
    goto :goto_0
.end method

.method static synthetic b(Lhpo;)V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x1

    .line 40
    iget-object v0, p0, Lhpo;->h:Lhpt;

    :goto_0
    if-nez v0, :cond_4

    iget-object v0, p0, Lhpo;->d:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    const/4 v0, -0x1

    invoke-direct {p0, v0, v4}, Lhpo;->a(IZ)Lhra;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lhra;->a()I

    move-result v1

    iget-object v2, p0, Lhpo;->b:Landroid/content/Context;

    invoke-static {v2, v1}, Lhpo;->a(Landroid/content/Context;I)Z

    move-result v2

    if-eqz v2, :cond_2

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v3, "ignore_settings"

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v3, "iu.SyncManager"

    invoke-static {v3, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x11

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "REQUEST sync for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v0, p0, Lhpo;->g:Lhei;

    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/accounts/Account;

    const-string v3, "com.google"

    invoke-direct {v1, v0, v3}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lhpo;->b:Landroid/content/Context;

    invoke-static {v0}, Lhqv;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0, v2}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    const-string v0, "iu.SyncManager"

    const/4 v2, 0x5

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v2, 0x28

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "account: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " has been removed ?!"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    iget-object v2, p0, Lhpo;->c:Ljava/util/HashSet;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lhpo;->c:Ljava/util/HashSet;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lhpo;->h:Lhpt;

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_4
    invoke-virtual {v0}, Lhpt;->c()Lhra;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lhra;->a()I

    move-result v2

    invoke-direct {p0, v2}, Lhpo;->e(I)Z

    move-result v2

    if-nez v2, :cond_5

    invoke-virtual {v1}, Lhra;->e()Z

    move-result v2

    if-nez v2, :cond_7

    :cond_5
    const-string v0, "iu.SyncManager"

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1a

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "STOP task: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "; task rejected"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_6
    invoke-virtual {v1}, Lhra;->g()V

    goto :goto_1

    :cond_7
    iget v0, v0, Lhpt;->a:I

    invoke-direct {p0, v0, v4}, Lhpo;->a(IZ)Lhra;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lhra;->c()I

    move-result v2

    invoke-virtual {v1}, Lhra;->c()I

    move-result v3

    if-ge v2, v3, :cond_1

    const-string v2, "iu.SyncManager"

    invoke-static {v2, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x23

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "STOP task: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "; higher priority task: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_8
    invoke-virtual {v1}, Lhra;->g()V

    goto/16 :goto_1
.end method

.method static synthetic b(Lhpo;I)V
    .locals 3

    .prologue
    const/16 v2, 0x1e

    .line 40
    iget-object v0, p0, Lhpo;->e:Lhrj;

    invoke-static {v0, p1, v2}, Lhqn;->a(Lhrj;II)V

    iget-object v0, p0, Lhpo;->h:Lhpt;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lhpt;->c()Lhra;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lhra;->b()I

    move-result v1

    if-ne v1, v2, :cond_0

    invoke-virtual {v0}, Lhra;->f()V

    :cond_0
    return-void
.end method

.method static synthetic c(Lhpo;)V
    .locals 12

    .prologue
    const/4 v5, 0x2

    const/4 v8, 0x4

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v3, 0x0

    .line 40
    iget-object v0, p0, Lhpo;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lhpo;->b:Landroid/content/Context;

    invoke-static {v1}, Lhqv;->b(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const-string v4, "auto_upload_enabled"

    aput-object v4, v2, v7

    const-string v4, "auto_upload_account_id"

    aput-object v4, v2, v6

    const-string v4, "instant_share_eventid"

    aput-object v4, v2, v5

    const/4 v4, 0x3

    const-string v5, "instant_share_starttime"

    aput-object v5, v2, v4

    const-string v4, "instant_share_endtime"

    aput-object v4, v2, v8

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    if-nez v2, :cond_1

    const-string v0, "iu.SyncManager"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "iu.SyncManager"

    const-string v1, "failed to query system settings"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "iu.SyncManager"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "iu.SyncManager"

    const-string v1, "no system settings found"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_7

    move v1, v6

    :goto_1
    const/4 v0, 0x1

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    const/4 v0, 0x2

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v4, 0x3

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const/4 v8, 0x4

    invoke-interface {v2, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    if-eqz v0, :cond_8

    cmp-long v0, v10, v4

    if-ltz v0, :cond_8

    cmp-long v0, v10, v8

    if-gtz v0, :cond_8

    move v0, v6

    :goto_2
    if-nez v1, :cond_4

    if-eqz v0, :cond_6

    :cond_4
    const/4 v0, -0x1

    if-eq v3, v0, :cond_6

    iget-object v0, p0, Lhpo;->b:Landroid/content/Context;

    invoke-static {v0, v3}, Lhpo;->a(Landroid/content/Context;I)Z

    move-result v0

    if-nez v0, :cond_6

    const-string v0, "iu.SyncManager"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x20

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "REMOVE sync account: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_5
    iget-object v0, p0, Lhpo;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lhpo;->b:Landroid/content/Context;

    invoke-static {v1}, Lhqv;->b(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_6
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    :cond_7
    move v1, v7

    goto :goto_1

    :cond_8
    move v0, v7

    goto :goto_2

    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private d(I)Lhra;
    .locals 3

    .prologue
    .line 274
    const-string v0, "AutoBackupSyncManager.getNextSyncTask"

    invoke-static {v0}, Ljdk;->a(Ljava/lang/String;)I

    move-result v1

    .line 275
    new-instance v0, Ljava/util/concurrent/FutureTask;

    new-instance v2, Lhpr;

    invoke-direct {v2, p0, p1}, Lhpr;-><init>(Lhpo;I)V

    invoke-direct {v0, v2}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 278
    iget-object v2, p0, Lhpo;->d:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 280
    :try_start_0
    invoke-virtual {v0}, Ljava/util/concurrent/FutureTask;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhra;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 286
    invoke-static {v1}, Ljdk;->a(I)V

    .line 288
    :goto_0
    return-object v0

    .line 282
    :catch_0
    move-exception v0

    invoke-static {v1}, Ljdk;->a(I)V

    .line 288
    const/4 v0, 0x0

    goto :goto_0

    .line 286
    :catchall_0
    move-exception v0

    invoke-static {v1}, Ljdk;->a(I)V

    throw v0
.end method

.method private e(I)Z
    .locals 3

    .prologue
    .line 530
    iget-object v1, p0, Lhpo;->c:Ljava/util/HashSet;

    monitor-enter v1

    .line 531
    :try_start_0
    iget-object v0, p0, Lhpo;->c:Ljava/util/HashSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 532
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public declared-synchronized a(I)V
    .locals 6

    .prologue
    .line 93
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lhpo;->b:Landroid/content/Context;

    const-class v1, Lhrj;

    .line 94
    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhrj;

    invoke-virtual {v0}, Lhrj;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 95
    sget-object v1, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a:Lifm;

    invoke-virtual {v1}, Lifm;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "upload_account_id == ?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    .line 97
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 95
    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 98
    iget-object v1, p0, Lhpo;->g:Lhei;

    invoke-interface {v1, p1}, Lhei;->c(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 99
    iget-object v1, p0, Lhpo;->g:Lhei;

    invoke-interface {v1, p1}, Lhei;->a(I)Lhej;

    move-result-object v1

    .line 100
    iget-object v2, p0, Lhpo;->f:Lhqz;

    const-string v3, "gaia_id"

    invoke-interface {v1, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lhqz;->b(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 102
    :cond_0
    monitor-exit p0

    return-void

    .line 93
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(J)V
    .locals 3

    .prologue
    .line 188
    iget-object v0, p0, Lhpo;->d:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 189
    return-void
.end method

.method public a(Lhpt;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v6, 0x0

    .line 113
    invoke-static {p1}, Llsk;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhpt;

    iput-object v0, p0, Lhpo;->h:Lhpt;

    .line 115
    iget-object v1, p0, Lhpo;->c:Ljava/util/HashSet;

    monitor-enter v1

    .line 116
    :try_start_0
    iget-object v0, p0, Lhpo;->c:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 117
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 120
    :try_start_1
    iget v1, p1, Lhpt;->a:I

    .line 123
    :cond_0
    invoke-direct {p0, v1}, Lhpo;->d(I)Lhra;

    move-result-object v2

    .line 124
    if-nez v2, :cond_4

    const/4 v0, -0x1

    .line 127
    :goto_0
    if-eqz v2, :cond_6

    invoke-direct {p0, v0}, Lhpo;->e(I)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v2}, Lhra;->e()Z

    move-result v3

    if-nez v3, :cond_6

    .line 128
    :cond_1
    const-string v0, "iu.SyncManager"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 129
    const-string v0, "SYNC; not accepted; task: "

    invoke-virtual {v2}, Lhra;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 172
    :cond_2
    :goto_1
    const/4 v0, 0x0

    iput-object v0, p0, Lhpo;->h:Lhpt;

    .line 175
    invoke-virtual {p1}, Lhpt;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 179
    const-wide/16 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lhpo;->b(J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 182
    :cond_3
    iput-object v6, p0, Lhpo;->h:Lhpt;

    .line 183
    return-void

    .line 117
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 125
    :cond_4
    :try_start_3
    invoke-virtual {v2}, Lhra;->a()I

    move-result v0

    goto :goto_0

    .line 129
    :cond_5
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    .line 182
    :catchall_1
    move-exception v0

    iput-object v6, p0, Lhpo;->h:Lhpt;

    throw v0

    .line 134
    :cond_6
    :try_start_4
    invoke-virtual {p1, v2}, Lhpt;->a(Lhra;)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 135
    if-eqz v2, :cond_2

    if-ne v1, v0, :cond_2

    .line 136
    :try_start_5
    const-string v0, "iu.SyncManager"

    const/4 v3, 0x4

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 141
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x12

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "SYNC; start task: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 144
    :cond_7
    iget-object v0, p1, Lhpt;->b:Landroid/content/SyncResult;

    invoke-virtual {v2, v0}, Lhra;->a(Landroid/content/SyncResult;)V

    .line 146
    const-string v0, "iu.SyncManager"

    const/4 v2, 0x4

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 147
    iget-object v0, p1, Lhpt;->b:Landroid/content/SyncResult;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x18

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "SYNC; complete; result: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 155
    :cond_8
    const/4 v0, 0x0

    :try_start_6
    invoke-virtual {p1, v0}, Lhpt;->a(Lhra;)Z

    .line 158
    :goto_2
    iget-object v0, p1, Lhpt;->b:Landroid/content/SyncResult;

    iget-object v0, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v0, Landroid/content/SyncStats;->numIoExceptions:J

    cmp-long v0, v2, v8

    if-gtz v0, :cond_9

    iget-object v0, p1, Lhpt;->b:Landroid/content/SyncResult;

    iget-object v0, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v0, Landroid/content/SyncStats;->numAuthExceptions:J

    cmp-long v0, v2, v8

    if-gtz v0, :cond_9

    iget-object v0, p1, Lhpt;->b:Landroid/content/SyncResult;

    iget-wide v2, v0, Landroid/content/SyncResult;->delayUntil:J

    cmp-long v0, v2, v8

    if-lez v0, :cond_0

    .line 161
    :cond_9
    invoke-virtual {p1}, Lhpt;->a()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto/16 :goto_1

    .line 150
    :catch_0
    move-exception v0

    :try_start_7
    iget-object v0, p1, Lhpt;->b:Landroid/content/SyncResult;

    iget-object v0, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v0, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v0, Landroid/content/SyncStats;->numIoExceptions:J
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 155
    const/4 v0, 0x0

    :try_start_8
    invoke-virtual {p1, v0}, Lhpt;->a(Lhra;)Z

    goto :goto_2

    :catchall_2
    move-exception v0

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lhpt;->a(Lhra;)Z

    throw v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1
.end method

.method public b(I)V
    .locals 3

    .prologue
    .line 193
    iget-object v0, p0, Lhpo;->d:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 194
    return-void
.end method

.method public b(J)V
    .locals 3

    .prologue
    .line 207
    iget-object v0, p0, Lhpo;->d:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 208
    return-void
.end method

.method public c(I)V
    .locals 3

    .prologue
    .line 197
    iget-object v0, p0, Lhpo;->d:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 198
    return-void
.end method
