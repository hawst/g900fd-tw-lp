.class public final Lnvf;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field private c:Ljava/lang/Long;

.field private d:Ljava/lang/Boolean;

.field private e:I

.field private f:Lnvd;

.field private g:Loxz;

.field private h:Logy;

.field private i:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/high16 v1, -0x80000000

    const/4 v0, 0x0

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 21
    iput v1, p0, Lnvf;->e:I

    .line 24
    iput-object v0, p0, Lnvf;->f:Lnvd;

    .line 27
    iput-object v0, p0, Lnvf;->g:Loxz;

    .line 30
    iput-object v0, p0, Lnvf;->h:Logy;

    .line 33
    iput v1, p0, Lnvf;->i:I

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 5

    .prologue
    const/high16 v4, -0x80000000

    .line 69
    const/4 v0, 0x1

    iget-object v1, p0, Lnvf;->a:Ljava/lang/String;

    .line 71
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 72
    iget-object v1, p0, Lnvf;->c:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 73
    const/4 v1, 0x2

    iget-object v2, p0, Lnvf;->c:Ljava/lang/Long;

    .line 74
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 76
    :cond_0
    iget-object v1, p0, Lnvf;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 77
    const/4 v1, 0x3

    iget-object v2, p0, Lnvf;->b:Ljava/lang/String;

    .line 78
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 80
    :cond_1
    iget-object v1, p0, Lnvf;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 81
    const/4 v1, 0x4

    iget-object v2, p0, Lnvf;->d:Ljava/lang/Boolean;

    .line 82
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 84
    :cond_2
    iget v1, p0, Lnvf;->e:I

    if-eq v1, v4, :cond_3

    .line 85
    const/4 v1, 0x5

    iget v2, p0, Lnvf;->e:I

    .line 86
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 88
    :cond_3
    iget-object v1, p0, Lnvf;->f:Lnvd;

    if-eqz v1, :cond_4

    .line 89
    const/4 v1, 0x6

    iget-object v2, p0, Lnvf;->f:Lnvd;

    .line 90
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 92
    :cond_4
    iget-object v1, p0, Lnvf;->g:Loxz;

    if-eqz v1, :cond_5

    .line 93
    const/4 v1, 0x7

    iget-object v2, p0, Lnvf;->g:Loxz;

    .line 94
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 96
    :cond_5
    iget-object v1, p0, Lnvf;->h:Logy;

    if-eqz v1, :cond_6

    .line 97
    const/16 v1, 0x8

    iget-object v2, p0, Lnvf;->h:Logy;

    .line 98
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 100
    :cond_6
    iget v1, p0, Lnvf;->i:I

    if-eq v1, v4, :cond_7

    .line 101
    const/16 v1, 0x9

    iget v2, p0, Lnvf;->i:I

    .line 102
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 104
    :cond_7
    iget-object v1, p0, Lnvf;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 105
    iput v0, p0, Lnvf;->ai:I

    .line 106
    return v0
.end method

.method public a(Loxn;)Lnvf;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 114
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 115
    sparse-switch v0, :sswitch_data_0

    .line 119
    iget-object v1, p0, Lnvf;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 120
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnvf;->ah:Ljava/util/List;

    .line 123
    :cond_1
    iget-object v1, p0, Lnvf;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 125
    :sswitch_0
    return-object p0

    .line 130
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnvf;->a:Ljava/lang/String;

    goto :goto_0

    .line 134
    :sswitch_2
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lnvf;->c:Ljava/lang/Long;

    goto :goto_0

    .line 138
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnvf;->b:Ljava/lang/String;

    goto :goto_0

    .line 142
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnvf;->d:Ljava/lang/Boolean;

    goto :goto_0

    .line 146
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 147
    if-eqz v0, :cond_2

    if-eq v0, v3, :cond_2

    if-eq v0, v4, :cond_2

    const/16 v1, 0xe

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa

    if-eq v0, v1, :cond_2

    const/16 v1, 0xb

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9

    if-eq v0, v1, :cond_2

    const/16 v1, 0xc

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd

    if-eq v0, v1, :cond_2

    const/16 v1, 0xf

    if-eq v0, v1, :cond_2

    const/16 v1, 0x10

    if-eq v0, v1, :cond_2

    const/16 v1, 0x27

    if-eq v0, v1, :cond_2

    const/16 v1, 0x16

    if-eq v0, v1, :cond_2

    const/16 v1, 0x11

    if-eq v0, v1, :cond_2

    const/16 v1, 0x12

    if-eq v0, v1, :cond_2

    const/16 v1, 0x13

    if-eq v0, v1, :cond_2

    const/16 v1, 0x14

    if-eq v0, v1, :cond_2

    const/16 v1, 0x15

    if-eq v0, v1, :cond_2

    const/16 v1, 0x17

    if-eq v0, v1, :cond_2

    const/16 v1, 0x18

    if-eq v0, v1, :cond_2

    const/16 v1, 0x19

    if-eq v0, v1, :cond_2

    const/16 v1, 0x1a

    if-eq v0, v1, :cond_2

    const/16 v1, 0x1b

    if-eq v0, v1, :cond_2

    const/16 v1, 0x1c

    if-eq v0, v1, :cond_2

    const/16 v1, 0x1d

    if-eq v0, v1, :cond_2

    const/16 v1, 0x1e

    if-eq v0, v1, :cond_2

    const/16 v1, 0x1f

    if-eq v0, v1, :cond_2

    const/16 v1, 0x2c

    if-eq v0, v1, :cond_2

    const/16 v1, 0x2d

    if-eq v0, v1, :cond_2

    const/16 v1, 0x31

    if-eq v0, v1, :cond_2

    const/16 v1, 0x32

    if-eq v0, v1, :cond_2

    const/16 v1, 0x22

    if-eq v0, v1, :cond_2

    const/16 v1, 0x2e

    if-eq v0, v1, :cond_2

    const/16 v1, 0x20

    if-eq v0, v1, :cond_2

    const/16 v1, 0x21

    if-eq v0, v1, :cond_2

    const/16 v1, 0x23

    if-eq v0, v1, :cond_2

    const/16 v1, 0x24

    if-eq v0, v1, :cond_2

    const/16 v1, 0x25

    if-eq v0, v1, :cond_2

    const/16 v1, 0x26

    if-eq v0, v1, :cond_2

    const/16 v1, 0x28

    if-eq v0, v1, :cond_2

    const/16 v1, 0x29

    if-eq v0, v1, :cond_2

    const/16 v1, 0x2a

    if-eq v0, v1, :cond_2

    const/16 v1, 0x2b

    if-eq v0, v1, :cond_2

    const/16 v1, 0x2f

    if-eq v0, v1, :cond_2

    const/16 v1, 0x30

    if-eq v0, v1, :cond_2

    const/16 v1, 0x33

    if-eq v0, v1, :cond_2

    const/16 v1, 0x34

    if-ne v0, v1, :cond_3

    .line 199
    :cond_2
    iput v0, p0, Lnvf;->e:I

    goto/16 :goto_0

    .line 201
    :cond_3
    iput v2, p0, Lnvf;->e:I

    goto/16 :goto_0

    .line 206
    :sswitch_6
    iget-object v0, p0, Lnvf;->f:Lnvd;

    if-nez v0, :cond_4

    .line 207
    new-instance v0, Lnvd;

    invoke-direct {v0}, Lnvd;-><init>()V

    iput-object v0, p0, Lnvf;->f:Lnvd;

    .line 209
    :cond_4
    iget-object v0, p0, Lnvf;->f:Lnvd;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 213
    :sswitch_7
    iget-object v0, p0, Lnvf;->g:Loxz;

    if-nez v0, :cond_5

    .line 214
    new-instance v0, Loxz;

    invoke-direct {v0}, Loxz;-><init>()V

    iput-object v0, p0, Lnvf;->g:Loxz;

    .line 216
    :cond_5
    iget-object v0, p0, Lnvf;->g:Loxz;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 220
    :sswitch_8
    iget-object v0, p0, Lnvf;->h:Logy;

    if-nez v0, :cond_6

    .line 221
    new-instance v0, Logy;

    invoke-direct {v0}, Logy;-><init>()V

    iput-object v0, p0, Lnvf;->h:Logy;

    .line 223
    :cond_6
    iget-object v0, p0, Lnvf;->h:Logy;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 227
    :sswitch_9
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 228
    if-eqz v0, :cond_7

    if-eq v0, v3, :cond_7

    if-ne v0, v4, :cond_8

    .line 231
    :cond_7
    iput v0, p0, Lnvf;->i:I

    goto/16 :goto_0

    .line 233
    :cond_8
    iput v2, p0, Lnvf;->i:I

    goto/16 :goto_0

    .line 115
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    const/high16 v4, -0x80000000

    .line 38
    const/4 v0, 0x1

    iget-object v1, p0, Lnvf;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 39
    iget-object v0, p0, Lnvf;->c:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 40
    const/4 v0, 0x2

    iget-object v1, p0, Lnvf;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 42
    :cond_0
    iget-object v0, p0, Lnvf;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 43
    const/4 v0, 0x3

    iget-object v1, p0, Lnvf;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 45
    :cond_1
    iget-object v0, p0, Lnvf;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 46
    const/4 v0, 0x4

    iget-object v1, p0, Lnvf;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 48
    :cond_2
    iget v0, p0, Lnvf;->e:I

    if-eq v0, v4, :cond_3

    .line 49
    const/4 v0, 0x5

    iget v1, p0, Lnvf;->e:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 51
    :cond_3
    iget-object v0, p0, Lnvf;->f:Lnvd;

    if-eqz v0, :cond_4

    .line 52
    const/4 v0, 0x6

    iget-object v1, p0, Lnvf;->f:Lnvd;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 54
    :cond_4
    iget-object v0, p0, Lnvf;->g:Loxz;

    if-eqz v0, :cond_5

    .line 55
    const/4 v0, 0x7

    iget-object v1, p0, Lnvf;->g:Loxz;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 57
    :cond_5
    iget-object v0, p0, Lnvf;->h:Logy;

    if-eqz v0, :cond_6

    .line 58
    const/16 v0, 0x8

    iget-object v1, p0, Lnvf;->h:Logy;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 60
    :cond_6
    iget v0, p0, Lnvf;->i:I

    if-eq v0, v4, :cond_7

    .line 61
    const/16 v0, 0x9

    iget v1, p0, Lnvf;->i:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 63
    :cond_7
    iget-object v0, p0, Lnvf;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 65
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lnvf;->a(Loxn;)Lnvf;

    move-result-object v0

    return-object v0
.end method
