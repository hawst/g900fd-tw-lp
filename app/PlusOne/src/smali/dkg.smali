.class public final Ldkg;
.super Lkfu;
.source "PG"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:I

.field private final c:Lnei;

.field private final p:[Lnfj;

.field private final q:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;J)V
    .locals 2

    .prologue
    .line 83
    const/4 v0, 0x1

    new-array v0, v0, [J

    const/4 v1, 0x0

    aput-wide p4, v0, v1

    invoke-direct {p0, p1, p2, p3, v0}, Ldkg;-><init>(Landroid/content/Context;ILjava/lang/String;[J)V

    .line 84
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;[J)V
    .locals 7

    .prologue
    .line 91
    new-instance v0, Lkfo;

    invoke-direct {v0, p1, p2}, Lkfo;-><init>(Landroid/content/Context;I)V

    const-string v1, "plusi"

    const-string v2, "oauth2:https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/plus.stream.read https://www.googleapis.com/auth/plus.stream.write https://www.googleapis.com/auth/plus.circles.write https://www.googleapis.com/auth/plus.circles.read https://www.googleapis.com/auth/plus.photos.readwrite https://www.googleapis.com/auth/plus.native"

    invoke-direct {p0, p1, v0, v1, v2}, Lkfu;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    invoke-static {}, Ldkg;->b()Lnei;

    move-result-object v0

    iput-object v0, p0, Ldkg;->c:Lnei;

    .line 94
    iput p2, p0, Ldkg;->b:I

    .line 95
    iput-object p3, p0, Ldkg;->a:Ljava/lang/String;

    .line 96
    array-length v0, p4

    new-array v0, v0, [Lnfj;

    iput-object v0, p0, Ldkg;->p:[Lnfj;

    .line 97
    array-length v0, p4

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Ldkg;->q:[Ljava/lang/String;

    .line 99
    const/4 v6, 0x0

    :goto_0
    array-length v0, p4

    if-ge v6, v0, :cond_0

    .line 100
    new-instance v0, Ldki;

    aget-wide v4, p4, v6

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    invoke-direct/range {v0 .. v6}, Ldki;-><init>(Ldkg;Landroid/content/Context;IJI)V

    invoke-virtual {p0, v0}, Ldkg;->a(Lkff;)V

    .line 101
    new-instance v0, Ldkh;

    aget-wide v4, p4, v6

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    invoke-direct/range {v0 .. v6}, Ldkh;-><init>(Ldkg;Landroid/content/Context;IJI)V

    invoke-virtual {p0, v0}, Ldkg;->a(Lkff;)V

    .line 99
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 103
    :cond_0
    return-void
.end method

.method static synthetic a(Ldkg;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Ldkg;->a:Ljava/lang/String;

    return-object v0
.end method

.method public static b()Lnei;
    .locals 3

    .prologue
    .line 55
    new-instance v0, Lnei;

    invoke-direct {v0}, Lnei;-><init>()V

    .line 56
    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    iput-object v1, v0, Lnei;->m:Ljava/lang/Boolean;

    .line 57
    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    iput-object v1, v0, Lnei;->g:Ljava/lang/Boolean;

    .line 58
    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    iput-object v1, v0, Lnei;->i:Ljava/lang/Boolean;

    .line 59
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v1, v0, Lnei;->a:Ljava/lang/Boolean;

    .line 60
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v1, v0, Lnei;->l:Ljava/lang/Boolean;

    .line 61
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v1, v0, Lnei;->h:Ljava/lang/Boolean;

    .line 62
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v1, v0, Lnei;->k:Ljava/lang/Boolean;

    .line 63
    sget-object v1, Ldba;->N:Lloz;

    .line 64
    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    iput-object v1, v0, Lnei;->e:Ljava/lang/Boolean;

    .line 67
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v1, v0, Lnei;->f:Ljava/lang/Boolean;

    .line 68
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v1, v0, Lnei;->d:Ljava/lang/Boolean;

    .line 69
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v1, v0, Lnei;->b:Ljava/lang/Boolean;

    .line 70
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v1, v0, Lnei;->c:Ljava/lang/Boolean;

    .line 71
    const/4 v1, 0x2

    iput v1, v0, Lnei;->j:I

    .line 74
    new-instance v1, Lnea;

    invoke-direct {v1}, Lnea;-><init>()V

    iput-object v1, v0, Lnei;->n:Lnea;

    .line 75
    iget-object v1, v0, Lnei;->n:Lnea;

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v2, v1, Lnea;->a:Ljava/lang/Boolean;

    .line 76
    return-object v0
.end method

.method static synthetic b(Ldkg;)Lnei;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Ldkg;->c:Lnei;

    return-object v0
.end method

.method static synthetic c(Ldkg;)[Lnfj;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Ldkg;->p:[Lnfj;

    return-object v0
.end method

.method static synthetic d(Ldkg;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Ldkg;->q:[Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected a(Ljava/util/List;Z)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lkff;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 107
    invoke-super {p0, p1, p2}, Lkfu;->a(Ljava/util/List;Z)V

    .line 109
    iget-object v0, p0, Ldkg;->p:[Lnfj;

    array-length v5, v0

    .line 110
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    mul-int/lit8 v1, v5, 0x2

    if-ne v0, v1, :cond_4

    move v0, v2

    :goto_0
    invoke-static {v0}, Llsk;->a(Z)V

    move v4, v3

    .line 112
    :goto_1
    if-ge v4, v5, :cond_5

    .line 114
    mul-int/lit8 v0, v4, 0x2

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkff;

    .line 115
    mul-int/lit8 v1, v4, 0x2

    add-int/lit8 v1, v1, 0x1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkff;

    .line 116
    invoke-virtual {v0}, Lkff;->t()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {v1}, Lkff;->t()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Ldkg;->p:[Lnfj;

    aget-object v0, v0, v4

    if-eqz v0, :cond_3

    .line 118
    iget-object v0, p0, Ldkg;->p:[Lnfj;

    aget-object v0, v0, v4

    iget-object v1, v0, Lnfj;->a:Lnzx;

    .line 122
    sget-object v0, Lnzu;->a:Loxr;

    invoke-virtual {v1, v0}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnzu;

    .line 123
    if-eqz v0, :cond_2

    iget-object v6, v0, Lnzu;->b:Lnym;

    if-eqz v6, :cond_2

    .line 124
    iget-object v6, v0, Lnzu;->b:Lnym;

    .line 125
    iget-object v7, p0, Ldkg;->q:[Ljava/lang/String;

    aget-object v7, v7, v4

    if-eqz v7, :cond_0

    .line 126
    new-instance v7, Lnxd;

    invoke-direct {v7}, Lnxd;-><init>()V

    .line 127
    iget-object v8, p0, Ldkg;->q:[Ljava/lang/String;

    aget-object v8, v8, v4

    iput-object v8, v7, Lnxd;->b:Ljava/lang/String;

    .line 128
    new-array v8, v2, [Lnxd;

    aput-object v7, v8, v3

    iput-object v8, v6, Lnym;->N:[Lnxd;

    .line 131
    :cond_0
    iget-object v7, v6, Lnym;->b:Lnyl;

    if-eqz v7, :cond_1

    iget-object v7, v6, Lnym;->b:Lnyl;

    iget-object v7, v7, Lnyl;->b:Ljava/lang/String;

    if-eqz v7, :cond_1

    .line 132
    iget-object v6, v6, Lnym;->b:Lnyl;

    iput-object v6, v1, Lnzx;->f:Lnyl;

    .line 134
    :cond_1
    sget-object v6, Lnzu;->a:Loxr;

    invoke-virtual {v1, v6, v0}, Lnzx;->a(Loxr;Ljava/lang/Object;)V

    .line 137
    :cond_2
    iget-object v0, p0, Ldkg;->f:Landroid/content/Context;

    iget v6, p0, Ldkg;->b:I

    invoke-static {v1, v0, v6}, Ljvj;->a(Lnzx;Landroid/content/Context;I)V

    .line 112
    :cond_3
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    :cond_4
    move v0, v3

    .line 110
    goto :goto_0

    .line 139
    :cond_5
    return-void
.end method
