.class public final Lobr;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Boolean;

.field private b:Lodo;

.field private c:Lodo;

.field private d:Lodo;

.field private e:Lobl;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 475
    invoke-direct {p0}, Loxq;-><init>()V

    .line 480
    iput-object v0, p0, Lobr;->b:Lodo;

    .line 483
    iput-object v0, p0, Lobr;->c:Lodo;

    .line 486
    iput-object v0, p0, Lobr;->d:Lodo;

    .line 489
    iput-object v0, p0, Lobr;->e:Lobl;

    .line 475
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 515
    const/4 v0, 0x0

    .line 516
    iget-object v1, p0, Lobr;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 517
    const/4 v0, 0x5

    iget-object v1, p0, Lobr;->a:Ljava/lang/Boolean;

    .line 518
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 520
    :cond_0
    iget-object v1, p0, Lobr;->b:Lodo;

    if-eqz v1, :cond_1

    .line 521
    const/4 v1, 0x6

    iget-object v2, p0, Lobr;->b:Lodo;

    .line 522
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 524
    :cond_1
    iget-object v1, p0, Lobr;->c:Lodo;

    if-eqz v1, :cond_2

    .line 525
    const/4 v1, 0x7

    iget-object v2, p0, Lobr;->c:Lodo;

    .line 526
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 528
    :cond_2
    iget-object v1, p0, Lobr;->d:Lodo;

    if-eqz v1, :cond_3

    .line 529
    const/16 v1, 0x8

    iget-object v2, p0, Lobr;->d:Lodo;

    .line 530
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 532
    :cond_3
    iget-object v1, p0, Lobr;->e:Lobl;

    if-eqz v1, :cond_4

    .line 533
    const/16 v1, 0x9

    iget-object v2, p0, Lobr;->e:Lobl;

    .line 534
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 536
    :cond_4
    iget-object v1, p0, Lobr;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 537
    iput v0, p0, Lobr;->ai:I

    .line 538
    return v0
.end method

.method public a(Loxn;)Lobr;
    .locals 2

    .prologue
    .line 546
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 547
    sparse-switch v0, :sswitch_data_0

    .line 551
    iget-object v1, p0, Lobr;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 552
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lobr;->ah:Ljava/util/List;

    .line 555
    :cond_1
    iget-object v1, p0, Lobr;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 557
    :sswitch_0
    return-object p0

    .line 562
    :sswitch_1
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lobr;->a:Ljava/lang/Boolean;

    goto :goto_0

    .line 566
    :sswitch_2
    iget-object v0, p0, Lobr;->b:Lodo;

    if-nez v0, :cond_2

    .line 567
    new-instance v0, Lodo;

    invoke-direct {v0}, Lodo;-><init>()V

    iput-object v0, p0, Lobr;->b:Lodo;

    .line 569
    :cond_2
    iget-object v0, p0, Lobr;->b:Lodo;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 573
    :sswitch_3
    iget-object v0, p0, Lobr;->c:Lodo;

    if-nez v0, :cond_3

    .line 574
    new-instance v0, Lodo;

    invoke-direct {v0}, Lodo;-><init>()V

    iput-object v0, p0, Lobr;->c:Lodo;

    .line 576
    :cond_3
    iget-object v0, p0, Lobr;->c:Lodo;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 580
    :sswitch_4
    iget-object v0, p0, Lobr;->d:Lodo;

    if-nez v0, :cond_4

    .line 581
    new-instance v0, Lodo;

    invoke-direct {v0}, Lodo;-><init>()V

    iput-object v0, p0, Lobr;->d:Lodo;

    .line 583
    :cond_4
    iget-object v0, p0, Lobr;->d:Lodo;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 587
    :sswitch_5
    iget-object v0, p0, Lobr;->e:Lobl;

    if-nez v0, :cond_5

    .line 588
    new-instance v0, Lobl;

    invoke-direct {v0}, Lobl;-><init>()V

    iput-object v0, p0, Lobr;->e:Lobl;

    .line 590
    :cond_5
    iget-object v0, p0, Lobr;->e:Lobl;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 547
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x28 -> :sswitch_1
        0x32 -> :sswitch_2
        0x3a -> :sswitch_3
        0x42 -> :sswitch_4
        0x4a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 494
    iget-object v0, p0, Lobr;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 495
    const/4 v0, 0x5

    iget-object v1, p0, Lobr;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 497
    :cond_0
    iget-object v0, p0, Lobr;->b:Lodo;

    if-eqz v0, :cond_1

    .line 498
    const/4 v0, 0x6

    iget-object v1, p0, Lobr;->b:Lodo;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 500
    :cond_1
    iget-object v0, p0, Lobr;->c:Lodo;

    if-eqz v0, :cond_2

    .line 501
    const/4 v0, 0x7

    iget-object v1, p0, Lobr;->c:Lodo;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 503
    :cond_2
    iget-object v0, p0, Lobr;->d:Lodo;

    if-eqz v0, :cond_3

    .line 504
    const/16 v0, 0x8

    iget-object v1, p0, Lobr;->d:Lodo;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 506
    :cond_3
    iget-object v0, p0, Lobr;->e:Lobl;

    if-eqz v0, :cond_4

    .line 507
    const/16 v0, 0x9

    iget-object v1, p0, Lobr;->e:Lobl;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 509
    :cond_4
    iget-object v0, p0, Lobr;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 511
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 471
    invoke-virtual {p0, p1}, Lobr;->a(Loxn;)Lobr;

    move-result-object v0

    return-object v0
.end method
