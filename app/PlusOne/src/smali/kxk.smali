.class public abstract Lkxk;
.super Lhxz;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lhxz",
        "<",
        "Lkxl;",
        ">;"
    }
.end annotation


# static fields
.field public static final b:Lkxl;


# instance fields
.field public final c:I

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public volatile f:Lkff;

.field private g:Lkxl;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lkxl;

    invoke-direct {v0}, Lkxl;-><init>()V

    sput-object v0, Lkxk;->b:Lkxl;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lhxz;-><init>(Landroid/content/Context;)V

    .line 35
    iput p2, p0, Lkxk;->c:I

    .line 36
    iput-object p3, p0, Lkxk;->d:Ljava/lang/String;

    .line 37
    iput-object p4, p0, Lkxk;->e:Ljava/lang/String;

    .line 38
    return-void
.end method


# virtual methods
.method public a(Lkxl;)V
    .locals 1

    .prologue
    .line 49
    invoke-virtual {p0}, Lkxk;->r()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 58
    :cond_0
    :goto_0
    return-void

    .line 53
    :cond_1
    iput-object p1, p0, Lkxk;->g:Lkxl;

    .line 55
    invoke-virtual {p0}, Lkxk;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    invoke-super {p0, p1}, Lhxz;->b(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public synthetic b(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 17
    check-cast p1, Lkxl;

    invoke-virtual {p0, p1}, Lkxk;->a(Lkxl;)V

    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lkxk;->f:Lkff;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lkff;->m()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lkxk;->f:Lkff;

    .line 67
    invoke-super {p0}, Lhxz;->b()Z

    move-result v0

    return v0
.end method

.method protected g()V
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lkxk;->g:Lkxl;

    if-nez v0, :cond_0

    .line 43
    invoke-virtual {p0}, Lkxk;->t()V

    .line 45
    :cond_0
    return-void
.end method

.method public m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lkxk;->e:Ljava/lang/String;

    return-object v0
.end method
