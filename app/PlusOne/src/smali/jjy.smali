.class public final Ljjy;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljjm;
.implements Ljjp;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    const-string v0, "com.google.android.libraries.social.notifications.PAGE_FETCH_NOTIFICATIONS"

    return-object v0
.end method

.method public a(Landroid/content/Intent;Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 29
    invoke-static {p1}, Lcom/google/android/libraries/social/notifications/impl/GunsIntentService;->a(Landroid/content/Intent;)Lkfo;

    move-result-object v0

    .line 30
    if-nez v0, :cond_1

    .line 46
    :cond_0
    :goto_0
    return-void

    .line 34
    :cond_1
    invoke-static {p1}, Lcom/google/android/libraries/social/notifications/impl/GunsIntentService;->c(Landroid/content/Intent;)Ljin;

    move-result-object v1

    .line 35
    invoke-static {p2, v0, v1}, Ljju;->a(Landroid/content/Context;Lkfo;Ljin;)[B

    move-result-object v2

    .line 36
    if-eqz v2, :cond_0

    .line 40
    invoke-static {p2}, Ljke;->a(Landroid/content/Context;)Ljke;

    move-result-object v3

    new-instance v4, Lpeu;

    invoke-direct {v4}, Lpeu;-><init>()V

    invoke-virtual {v3}, Ljke;->b()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lpeu;->a:Ljava/lang/String;

    new-instance v5, Ljkb;

    invoke-direct {v5, p2}, Ljkb;-><init>(Landroid/content/Context;)V

    invoke-virtual {v5}, Ljkb;->a()Llug;

    move-result-object v5

    iput-object v5, v4, Lpeu;->b:Llug;

    const/16 v5, 0x32

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iput-object v5, v4, Lpeu;->c:Ljava/lang/Integer;

    invoke-virtual {v3, v1}, Ljke;->a(Ljin;)[I

    move-result-object v3

    iput-object v3, v4, Lpeu;->d:[I

    iput-object v2, v4, Lpeu;->e:[B

    .line 41
    new-instance v2, Ljjg;

    invoke-direct {v2, p2, v0, v4}, Ljjg;-><init>(Landroid/content/Context;Lkfo;Lpeu;)V

    .line 42
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 43
    const-string v0, "fetch_category"

    invoke-virtual {v1}, Ljin;->ordinal()I

    move-result v1

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 44
    const-class v0, Ljjo;

    invoke-static {p2, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljjo;

    .line 45
    invoke-virtual {v0, v2, p0, p2, v3}, Ljjo;->a(Lkgg;Ljjp;Landroid/content/Context;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public a(Lkgg;Landroid/content/Context;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 55
    check-cast p1, Ljjg;

    .line 56
    const-string v0, "fetch_category"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljin;->a(I)Ljin;

    move-result-object v0

    .line 57
    invoke-virtual {p1}, Ljjg;->t()Z

    move-result v1

    if-nez v1, :cond_0

    .line 58
    invoke-virtual {p1}, Ljjg;->i()Lpew;

    move-result-object v1

    .line 60
    iget-object v2, p1, Lkff;->g:Lkfo;

    .line 59
    invoke-static {p2, v2, v0, v1}, Ljju;->a(Landroid/content/Context;Lkfo;Ljin;Lpew;)V

    .line 64
    :goto_0
    return-void

    .line 62
    :cond_0
    const-string v0, "PageFetchHandler"

    const-string v1, "Failed to fetch notifications."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0}, Ljjy;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
