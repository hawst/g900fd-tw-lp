.class public final Lmcd;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnhs;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 29806
    invoke-direct {p0}, Loxq;-><init>()V

    .line 29809
    iput-object v0, p0, Lmcd;->apiHeader:Llyr;

    .line 29812
    iput-object v0, p0, Lmcd;->a:Lnhs;

    .line 29806
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 29829
    const/4 v0, 0x0

    .line 29830
    iget-object v1, p0, Lmcd;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 29831
    const/4 v0, 0x1

    iget-object v1, p0, Lmcd;->apiHeader:Llyr;

    .line 29832
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 29834
    :cond_0
    iget-object v1, p0, Lmcd;->a:Lnhs;

    if-eqz v1, :cond_1

    .line 29835
    const/4 v1, 0x2

    iget-object v2, p0, Lmcd;->a:Lnhs;

    .line 29836
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 29838
    :cond_1
    iget-object v1, p0, Lmcd;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 29839
    iput v0, p0, Lmcd;->ai:I

    .line 29840
    return v0
.end method

.method public a(Loxn;)Lmcd;
    .locals 2

    .prologue
    .line 29848
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 29849
    sparse-switch v0, :sswitch_data_0

    .line 29853
    iget-object v1, p0, Lmcd;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 29854
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmcd;->ah:Ljava/util/List;

    .line 29857
    :cond_1
    iget-object v1, p0, Lmcd;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 29859
    :sswitch_0
    return-object p0

    .line 29864
    :sswitch_1
    iget-object v0, p0, Lmcd;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 29865
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmcd;->apiHeader:Llyr;

    .line 29867
    :cond_2
    iget-object v0, p0, Lmcd;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 29871
    :sswitch_2
    iget-object v0, p0, Lmcd;->a:Lnhs;

    if-nez v0, :cond_3

    .line 29872
    new-instance v0, Lnhs;

    invoke-direct {v0}, Lnhs;-><init>()V

    iput-object v0, p0, Lmcd;->a:Lnhs;

    .line 29874
    :cond_3
    iget-object v0, p0, Lmcd;->a:Lnhs;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 29849
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 29817
    iget-object v0, p0, Lmcd;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 29818
    const/4 v0, 0x1

    iget-object v1, p0, Lmcd;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 29820
    :cond_0
    iget-object v0, p0, Lmcd;->a:Lnhs;

    if-eqz v0, :cond_1

    .line 29821
    const/4 v0, 0x2

    iget-object v1, p0, Lmcd;->a:Lnhs;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 29823
    :cond_1
    iget-object v0, p0, Lmcd;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 29825
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 29802
    invoke-virtual {p0, p1}, Lmcd;->a(Loxn;)Lmcd;

    move-result-object v0

    return-object v0
.end method
