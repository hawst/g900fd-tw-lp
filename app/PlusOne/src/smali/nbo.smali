.class public final Lnbo;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lnbs;

.field private c:Lnbh;

.field private d:[Lnbm;

.field private e:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 336
    invoke-direct {p0}, Loxq;-><init>()V

    .line 341
    iput-object v1, p0, Lnbo;->c:Lnbh;

    .line 344
    sget-object v0, Lnbm;->a:[Lnbm;

    iput-object v0, p0, Lnbo;->d:[Lnbm;

    .line 349
    iput-object v1, p0, Lnbo;->b:Lnbs;

    .line 336
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 379
    .line 380
    iget-object v0, p0, Lnbo;->a:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 381
    const/4 v0, 0x1

    iget-object v2, p0, Lnbo;->a:Ljava/lang/String;

    .line 382
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 384
    :goto_0
    iget-object v2, p0, Lnbo;->c:Lnbh;

    if-eqz v2, :cond_0

    .line 385
    const/4 v2, 0x2

    iget-object v3, p0, Lnbo;->c:Lnbh;

    .line 386
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 388
    :cond_0
    iget-object v2, p0, Lnbo;->d:[Lnbm;

    if-eqz v2, :cond_2

    .line 389
    iget-object v2, p0, Lnbo;->d:[Lnbm;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 390
    if-eqz v4, :cond_1

    .line 391
    const/4 v5, 0x3

    .line 392
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 389
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 396
    :cond_2
    iget-object v1, p0, Lnbo;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 397
    const/4 v1, 0x4

    iget-object v2, p0, Lnbo;->e:Ljava/lang/Boolean;

    .line 398
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 400
    :cond_3
    iget-object v1, p0, Lnbo;->b:Lnbs;

    if-eqz v1, :cond_4

    .line 401
    const/4 v1, 0x5

    iget-object v2, p0, Lnbo;->b:Lnbs;

    .line 402
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 404
    :cond_4
    iget-object v1, p0, Lnbo;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 405
    iput v0, p0, Lnbo;->ai:I

    .line 406
    return v0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lnbo;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 414
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 415
    sparse-switch v0, :sswitch_data_0

    .line 419
    iget-object v2, p0, Lnbo;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 420
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnbo;->ah:Ljava/util/List;

    .line 423
    :cond_1
    iget-object v2, p0, Lnbo;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 425
    :sswitch_0
    return-object p0

    .line 430
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnbo;->a:Ljava/lang/String;

    goto :goto_0

    .line 434
    :sswitch_2
    iget-object v0, p0, Lnbo;->c:Lnbh;

    if-nez v0, :cond_2

    .line 435
    new-instance v0, Lnbh;

    invoke-direct {v0}, Lnbh;-><init>()V

    iput-object v0, p0, Lnbo;->c:Lnbh;

    .line 437
    :cond_2
    iget-object v0, p0, Lnbo;->c:Lnbh;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 441
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 442
    iget-object v0, p0, Lnbo;->d:[Lnbm;

    if-nez v0, :cond_4

    move v0, v1

    .line 443
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnbm;

    .line 444
    iget-object v3, p0, Lnbo;->d:[Lnbm;

    if-eqz v3, :cond_3

    .line 445
    iget-object v3, p0, Lnbo;->d:[Lnbm;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 447
    :cond_3
    iput-object v2, p0, Lnbo;->d:[Lnbm;

    .line 448
    :goto_2
    iget-object v2, p0, Lnbo;->d:[Lnbm;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 449
    iget-object v2, p0, Lnbo;->d:[Lnbm;

    new-instance v3, Lnbm;

    invoke-direct {v3}, Lnbm;-><init>()V

    aput-object v3, v2, v0

    .line 450
    iget-object v2, p0, Lnbo;->d:[Lnbm;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 451
    invoke-virtual {p1}, Loxn;->a()I

    .line 448
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 442
    :cond_4
    iget-object v0, p0, Lnbo;->d:[Lnbm;

    array-length v0, v0

    goto :goto_1

    .line 454
    :cond_5
    iget-object v2, p0, Lnbo;->d:[Lnbm;

    new-instance v3, Lnbm;

    invoke-direct {v3}, Lnbm;-><init>()V

    aput-object v3, v2, v0

    .line 455
    iget-object v2, p0, Lnbo;->d:[Lnbm;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 459
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnbo;->e:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 463
    :sswitch_5
    iget-object v0, p0, Lnbo;->b:Lnbs;

    if-nez v0, :cond_6

    .line 464
    new-instance v0, Lnbs;

    invoke-direct {v0}, Lnbs;-><init>()V

    iput-object v0, p0, Lnbo;->b:Lnbs;

    .line 466
    :cond_6
    iget-object v0, p0, Lnbo;->b:Lnbs;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 415
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 354
    iget-object v0, p0, Lnbo;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 355
    const/4 v0, 0x1

    iget-object v1, p0, Lnbo;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 357
    :cond_0
    iget-object v0, p0, Lnbo;->c:Lnbh;

    if-eqz v0, :cond_1

    .line 358
    const/4 v0, 0x2

    iget-object v1, p0, Lnbo;->c:Lnbh;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 360
    :cond_1
    iget-object v0, p0, Lnbo;->d:[Lnbm;

    if-eqz v0, :cond_3

    .line 361
    iget-object v1, p0, Lnbo;->d:[Lnbm;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 362
    if-eqz v3, :cond_2

    .line 363
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 361
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 367
    :cond_3
    iget-object v0, p0, Lnbo;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 368
    const/4 v0, 0x4

    iget-object v1, p0, Lnbo;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 370
    :cond_4
    iget-object v0, p0, Lnbo;->b:Lnbs;

    if-eqz v0, :cond_5

    .line 371
    const/4 v0, 0x5

    iget-object v1, p0, Lnbo;->b:Lnbs;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 373
    :cond_5
    iget-object v0, p0, Lnbo;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 375
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 332
    invoke-virtual {p0, p1}, Lnbo;->a(Loxn;)Lnbo;

    move-result-object v0

    return-object v0
.end method
