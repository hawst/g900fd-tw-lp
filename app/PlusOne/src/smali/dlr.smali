.class public final Ldlr;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmfo;",
        "Lmfp;",
        ">;"
    }
.end annotation


# static fields
.field private static a:Ldls;


# instance fields
.field private final b:Ldvt;

.field private p:Ljava/lang/String;

.field private q:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    new-instance v0, Ldls;

    invoke-direct {v0}, Ldls;-><init>()V

    sput-object v0, Ldlr;->a:Ldls;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;ILdvt;)V
    .locals 6

    .prologue
    .line 42
    const-string v3, "muteuser"

    new-instance v4, Lmfo;

    invoke-direct {v4}, Lmfo;-><init>()V

    new-instance v5, Lmfp;

    invoke-direct {v5}, Lmfp;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 43
    iput-object p3, p0, Ldlr;->b:Ldvt;

    .line 44
    return-void
.end method

.method public static b()Ldls;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Ldlr;->a:Ldls;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Ldlr;->p:Ljava/lang/String;

    .line 54
    iput-boolean p2, p0, Ldlr;->q:Z

    .line 55
    return-void
.end method

.method protected a(Lmfo;)V
    .locals 2

    .prologue
    .line 59
    new-instance v0, Lnli;

    invoke-direct {v0}, Lnli;-><init>()V

    iput-object v0, p1, Lmfo;->a:Lnli;

    .line 60
    iget-object v0, p1, Lmfo;->a:Lnli;

    .line 61
    iget-object v1, p0, Ldlr;->p:Ljava/lang/String;

    iput-object v1, v0, Lnli;->a:Ljava/lang/String;

    .line 62
    iget-boolean v1, p0, Ldlr;->q:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lnli;->b:Ljava/lang/Boolean;

    .line 63
    return-void
.end method

.method protected a(Lmfp;)V
    .locals 3

    .prologue
    .line 67
    iget-object v0, p1, Lmfp;->a:Lnlu;

    .line 68
    iget-object v1, p0, Ldlr;->b:Ldvt;

    iget-object v2, p0, Ldlr;->p:Ljava/lang/String;

    iget-object v0, v0, Lnlu;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v1, v2, v0}, Ldvt;->a(Ljava/lang/String;Z)Z

    .line 69
    return-void
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 15
    check-cast p1, Lmfo;

    invoke-virtual {p0, p1}, Ldlr;->a(Lmfo;)V

    return-void
.end method

.method protected synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 15
    check-cast p1, Lmfp;

    invoke-virtual {p0, p1}, Ldlr;->a(Lmfp;)V

    return-void
.end method
