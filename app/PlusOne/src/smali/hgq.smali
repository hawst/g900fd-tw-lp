.class final Lhgq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhfv;


# instance fields
.field private synthetic a:Lhgo;


# direct methods
.method constructor <init>(Lhgo;)V
    .locals 0

    .prologue
    .line 480
    iput-object p1, p0, Lhgq;->a:Lhgo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)I
    .locals 1

    .prologue
    .line 532
    iget-object v0, p0, Lhgq;->a:Lhgo;

    invoke-static {v0}, Lhgo;->a(Lhgo;)[Landroid/database/Cursor;

    move-result-object v0

    aget-object v0, v0, p1

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    return v0
.end method

.method public a(Lhfw;)I
    .locals 1

    .prologue
    .line 552
    iget-object v0, p0, Lhgq;->a:Lhgo;

    iget-object v0, v0, Lhgo;->c:Lhfv;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhgq;->a:Lhgo;

    iget-object v0, v0, Lhgo;->c:Lhfv;

    .line 553
    invoke-interface {v0, p1}, Lhfv;->a(Lhfw;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 560
    iget-object v0, p0, Lhgq;->a:Lhgo;

    iget-object v0, v0, Lhgo;->c:Lhfv;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhgq;->a:Lhgo;

    iget-object v0, v0, Lhgo;->c:Lhfv;

    invoke-interface {v0}, Lhfv;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "You shouldn\'t see this!"

    goto :goto_0
.end method

.method public a(Landroid/content/Context;Lhfw;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 527
    iget-object v0, p0, Lhgq;->a:Lhgo;

    iget-object v0, v0, Lhgo;->c:Lhfv;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhgq;->a:Lhgo;

    iget-object v0, v0, Lhgo;->c:Lhfv;

    invoke-interface {v0, p1, p2}, Lhfv;->a(Landroid/content/Context;Lhfw;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public a(IILhfw;)V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v6, 0x0

    const/4 v8, 0x3

    const/4 v2, 0x2

    const/4 v5, 0x1

    .line 486
    iget-object v0, p0, Lhgq;->a:Lhgo;

    invoke-static {v0}, Lhgo;->a(Lhgo;)[Landroid/database/Cursor;

    move-result-object v0

    aget-object v7, v0, p1

    .line 487
    invoke-interface {v7, p2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 489
    packed-switch p1, :pswitch_data_0

    .line 519
    :goto_0
    :pswitch_0
    return-void

    .line 491
    :pswitch_1
    new-instance v0, Lhxc;

    .line 492
    invoke-interface {v7, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 493
    invoke-interface {v7, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 494
    invoke-interface {v7, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 495
    invoke-interface {v7, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lhxc;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    .line 497
    invoke-virtual {p3, v0, v5}, Lhfw;->a(Lhxc;I)Lhfw;

    goto :goto_0

    .line 501
    :pswitch_2
    new-instance v0, Lkxr;

    .line 502
    invoke-interface {v7, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 503
    invoke-interface {v7, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 506
    invoke-interface {v7, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    :goto_1
    invoke-direct/range {v0 .. v5}, Lkxr;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 507
    invoke-virtual {p3, v0}, Lhfw;->a(Lkxr;)Lhfw;

    goto :goto_0

    :cond_0
    move v5, v6

    .line 506
    goto :goto_1

    .line 511
    :pswitch_3
    new-instance v0, Ljqs;

    .line 512
    invoke-interface {v7, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 513
    invoke-interface {v7, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x8

    .line 514
    invoke-interface {v7, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x5

    .line 515
    invoke-interface {v7, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/16 v8, 0xc

    .line 516
    invoke-interface {v7, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    if-eqz v7, :cond_1

    :goto_2
    invoke-direct/range {v0 .. v5}, Ljqs;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 518
    invoke-virtual {p3, v0, v9}, Lhfw;->a(Ljqs;I)Lhfw;

    goto :goto_0

    :cond_1
    move v5, v6

    .line 516
    goto :goto_2

    .line 489
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public a(Landroid/database/Cursor;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 482
    return-void
.end method

.method public a(Lu;ILhxc;Ljava/lang/String;Llgs;)Z
    .locals 6

    .prologue
    .line 571
    iget-object v0, p0, Lhgq;->a:Lhgo;

    iget-object v0, v0, Lhgo;->c:Lhfv;

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, Lhfv;->a(Lu;ILhxc;Ljava/lang/String;Llgs;)Z

    move-result v0

    return v0
.end method

.method public b(Lhfw;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lhfw;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 538
    iget-object v0, p0, Lhgq;->a:Lhgo;

    iget-object v0, v0, Lhgo;->c:Lhfv;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhgq;->a:Lhgo;

    iget-object v0, v0, Lhgo;->c:Lhfv;

    .line 539
    invoke-interface {v0, p1}, Lhfv;->b(Lhfw;)Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 565
    iget-object v0, p0, Lhgq;->a:Lhgo;

    iget-object v0, v0, Lhgo;->c:Lhfv;

    invoke-interface {v0}, Lhfv;->b()Z

    move-result v0

    return v0
.end method

.method public c(Lhfw;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 546
    iget-object v0, p0, Lhgq;->a:Lhgo;

    iget-object v0, v0, Lhgo;->c:Lhfv;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhgq;->a:Lhgo;

    iget-object v0, v0, Lhgo;->c:Lhfv;

    invoke-interface {v0, p1}, Lhfv;->c(Lhfw;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method
