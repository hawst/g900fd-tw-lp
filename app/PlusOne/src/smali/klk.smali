.class public final Lklk;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lkkm;


# instance fields
.field private final a:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 55
    const/16 v0, 0x1388

    invoke-direct {p0, v0}, Lklk;-><init>(I)V

    .line 56
    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput p1, p0, Lklk;->a:I

    .line 48
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Lodg;Ljava/util/List;Lkjc;)Lhoz;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lodg;",
            "Ljava/util/List",
            "<",
            "Lizu;",
            ">;",
            "Lkjc;",
            ")",
            "Lhoz;"
        }
    .end annotation

    .prologue
    .line 60
    new-instance v3, Lmhi;

    invoke-direct {v3}, Lmhi;-><init>()V

    .line 61
    iput-object p2, v3, Lmhi;->a:Lodg;

    .line 62
    new-instance v0, Lklg;

    .line 63
    invoke-virtual {p4}, Lkjc;->f()I

    move-result v2

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    .line 66
    invoke-virtual {p4}, Lkjc;->p()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_2

    const/4 v5, 0x0

    :cond_0
    :goto_0
    move-object v1, p1

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lklg;-><init>(Landroid/content/Context;ILmhi;Ljava/util/Date;Ljava/lang/String;Ljava/util/List;)V

    .line 69
    const-class v1, Ljhi;

    invoke-static {p1, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljhi;

    .line 71
    invoke-virtual {p4}, Lkjc;->f()I

    move-result v2

    .line 70
    invoke-interface {v1, v2}, Ljhi;->a(I)Ljhh;

    move-result-object v3

    .line 73
    const/4 v1, 0x0

    .line 74
    const/4 v2, 0x0

    .line 76
    if-eqz v3, :cond_8

    .line 77
    const-class v1, Ljgn;

    invoke-static {p1, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljgn;

    invoke-interface {v1}, Ljgn;->a()Z

    move-result v4

    .line 78
    invoke-interface {v3}, Ljhh;->a()I

    move-result v1

    if-lez v1, :cond_3

    const/4 v1, 0x1

    .line 79
    :goto_1
    invoke-interface {v3, v0}, Ljhh;->a(Ljhl;)J

    move-result-wide v6

    .line 85
    if-eqz v4, :cond_7

    if-nez v1, :cond_7

    .line 86
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 91
    :goto_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 92
    sub-long/2addr v4, v0

    .line 94
    invoke-interface {v3, v6, v7}, Ljhh;->b(J)Ljhn;

    move-result-object v8

    .line 95
    if-nez v8, :cond_4

    .line 96
    const-string v0, "CreatePostQueueTask"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 97
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x35

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "The item cleared the queue in "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ms."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 99
    :cond_1
    const/4 v0, 0x0

    move v3, v0

    .line 141
    :goto_3
    if-eqz v2, :cond_9

    const/4 v0, 0x1

    .line 142
    :goto_4
    new-instance v4, Lhoz;

    if-eqz v0, :cond_a

    const/4 v1, 0x0

    .line 143
    :goto_5
    invoke-static {p1, p4, v0, v3}, Lkkn;->a(Landroid/content/Context;Lkjc;ZI)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v1, v2, v0}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    .line 145
    invoke-virtual {p4}, Lkjc;->f()I

    move-result v0

    invoke-virtual {p4}, Lkjc;->k()Lhgw;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lhft;->a(Landroid/content/Context;ILhgw;)V

    .line 147
    invoke-virtual {v4}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "queued"

    const/4 v0, 0x2

    if-ne v3, v0, :cond_b

    const/4 v0, 0x1

    :goto_6
    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 150
    return-object v4

    .line 66
    :cond_2
    const/16 v1, 0xa

    invoke-virtual {v5, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    if-ltz v1, :cond_0

    const/4 v6, 0x0

    invoke-virtual {v5, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_0

    .line 78
    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    .line 103
    :cond_4
    invoke-interface {v3, v6, v7}, Ljhh;->c(J)Ljhm;

    move-result-object v8

    .line 104
    invoke-virtual {v8}, Ljhm;->b()Z

    move-result v9

    if-eqz v9, :cond_5

    .line 107
    invoke-virtual {v8}, Ljhm;->a()Ljava/lang/Exception;

    move-result-object v0

    .line 108
    const/4 v1, -0x1

    .line 109
    invoke-interface {v3, v6, v7}, Ljhh;->a(J)Z

    move-object v2, v0

    move v3, v1

    .line 110
    goto :goto_3

    .line 113
    :cond_5
    iget v8, p0, Lklk;->a:I

    int-to-long v8, v8

    cmp-long v4, v4, v8

    if-lez v4, :cond_6

    .line 114
    const/4 v0, 0x2

    move v3, v0

    .line 118
    goto :goto_3

    .line 122
    :cond_6
    const-wide/16 v4, 0x64

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_2

    :catch_0
    move-exception v0

    .line 124
    :cond_7
    const/4 v0, 0x2

    move v3, v0

    .line 131
    goto :goto_3

    .line 136
    :cond_8
    new-instance v3, Ljhn;

    invoke-direct {v3}, Ljhn;-><init>()V

    .line 137
    new-instance v4, Ljhm;

    invoke-direct {v4}, Ljhm;-><init>()V

    .line 138
    invoke-virtual {v0, v3, v4}, Lklg;->a(Ljhn;Ljhm;)I

    move v3, v1

    goto :goto_3

    .line 141
    :cond_9
    const/4 v0, 0x0

    goto :goto_4

    .line 142
    :cond_a
    const/16 v1, 0xc8

    goto :goto_5

    .line 147
    :cond_b
    const/4 v0, 0x0

    goto :goto_6
.end method
