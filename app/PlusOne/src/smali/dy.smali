.class final Ldy;
.super Ljava/util/concurrent/FutureTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/concurrent/FutureTask",
        "<TResult;>;"
    }
.end annotation


# instance fields
.field private synthetic a:Ldv;


# direct methods
.method constructor <init>(Ldv;Ljava/util/concurrent/Callable;)V
    .locals 0

    .prologue
    .line 127
    iput-object p1, p0, Ldy;->a:Ldv;

    invoke-direct {p0, p2}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    return-void
.end method


# virtual methods
.method protected done()V
    .locals 3

    .prologue
    .line 131
    :try_start_0
    invoke-virtual {p0}, Ldy;->get()Ljava/lang/Object;

    move-result-object v0

    .line 133
    iget-object v1, p0, Ldy;->a:Ldv;

    invoke-static {v1, v0}, Ldv;->b(Ldv;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2

    .line 144
    :goto_0
    return-void

    .line 136
    :catch_0
    move-exception v0

    .line 137
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "An error occured while executing doInBackground()"

    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 140
    :catch_1
    move-exception v0

    iget-object v0, p0, Ldy;->a:Ldv;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ldv;->b(Ldv;Ljava/lang/Object;)V

    goto :goto_0

    .line 141
    :catch_2
    move-exception v0

    .line 142
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "An error occured while executing doInBackground()"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 135
    :catch_3
    move-exception v0

    goto :goto_0
.end method
