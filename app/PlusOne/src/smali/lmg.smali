.class public final Llmg;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lllx;


# static fields
.field private static final a:Ljava/io/FilenameFilter;

.field private static final b:Ljava/io/FilenameFilter;


# instance fields
.field private final c:Ljava/io/File;

.field private final d:I

.field private final e:Llmm;

.field private final f:Llml;

.field private final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Llmn;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ljava/lang/Object;

.field private i:J

.field private j:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 92
    new-instance v0, Llmj;

    invoke-direct {v0}, Llmj;-><init>()V

    sput-object v0, Llmg;->a:Ljava/io/FilenameFilter;

    .line 99
    new-instance v0, Llmk;

    invoke-direct {v0}, Llmk;-><init>()V

    sput-object v0, Llmg;->b:Ljava/io/FilenameFilter;

    return-void
.end method

.method public constructor <init>(Ljava/io/File;IJLlmm;Llml;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Llmg;->g:Ljava/util/Map;

    .line 120
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Llmg;->h:Ljava/lang/Object;

    .line 138
    const-string v0, "cacheDir"

    invoke-static {p1, v0}, Llsk;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    iput-object v0, p0, Llmg;->c:Ljava/io/File;

    .line 139
    const-string v0, "policy"

    invoke-static {p5, v0}, Llsk;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llmm;

    iput-object v0, p0, Llmg;->e:Llmm;

    .line 140
    const-string v0, "clock"

    invoke-static {p6, v0}, Llsk;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llml;

    iput-object v0, p0, Llmg;->f:Llml;

    .line 142
    if-lez p2, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "chunkSizeBytes must be positive"

    invoke-static {v0, v3}, Llsk;->a(ZLjava/lang/Object;)V

    .line 143
    iput p2, p0, Llmg;->d:I

    .line 145
    const-wide/16 v4, 0x0

    cmp-long v0, p3, v4

    if-ltz v0, :cond_1

    :goto_1
    const-string v0, "quotaBytes must be non-negative"

    invoke-static {v1, v0}, Llsk;->a(ZLjava/lang/Object;)V

    .line 146
    int-to-long v0, p2

    div-long v0, p3, v0

    iput-wide v0, p0, Llmg;->i:J

    .line 147
    return-void

    :cond_0
    move v0, v2

    .line 142
    goto :goto_0

    :cond_1
    move v1, v2

    .line 145
    goto :goto_1
.end method

.method private b(Ljava/lang/String;I[BJ)V
    .locals 10

    .prologue
    .line 355
    iget-wide v0, p0, Llmg;->i:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 400
    :goto_0
    return-void

    .line 359
    :cond_0
    iget-object v0, p0, Llmg;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llmn;

    .line 360
    if-nez v0, :cond_5

    .line 361
    iget-object v2, p0, Llmg;->c:Ljava/io/File;

    iget-object v0, p0, Llmg;->f:Llml;

    new-instance v1, Llmn;

    invoke-static {}, Lllr;->a()Lllr;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v0}, Llml;->a()J

    move-result-wide v6

    const-wide/16 v8, -0x1

    move-object v3, p1

    invoke-direct/range {v1 .. v9}, Llmn;-><init>(Ljava/io/File;Ljava/lang/String;Lllr;ZJJ)V

    .line 362
    iget-object v0, p0, Llmg;->g:Ljava/util/Map;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v3, v1

    .line 366
    :goto_1
    const-wide/16 v0, -0x1

    cmp-long v0, p4, v0

    if-nez v0, :cond_1

    .line 367
    invoke-virtual {v3}, Llmn;->g()J

    move-result-wide p4

    .line 371
    :cond_1
    const-wide/16 v0, -0x1

    cmp-long v0, p4, v0

    if-eqz v0, :cond_2

    .line 372
    iget v0, p0, Llmg;->d:I

    mul-int/2addr v0, p2

    int-to-long v0, v0

    cmp-long v0, v0, p4

    if-gtz v0, :cond_3

    const/4 v0, 0x1

    :goto_2
    const-string v1, "chunk beyond EOF"

    invoke-static {v0, v1}, Llsk;->a(ZLjava/lang/Object;)V

    .line 376
    :cond_2
    invoke-virtual {v3, p2, p4, p5}, Llmn;->a(IJ)V

    .line 379
    new-instance v4, Ljava/io/File;

    iget-object v1, p0, Llmg;->c:Ljava/io/File;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v0, "~c"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_4

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-direct {v4, v1, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 380
    const/4 v2, 0x0

    .line 382
    :try_start_0
    new-instance v1, Ljava/io/RandomAccessFile;

    const-string v0, "rw"

    invoke-direct {v1, v4, v0}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 383
    :try_start_1
    iget v0, p0, Llmg;->d:I

    mul-int/2addr v0, p2

    int-to-long v6, v0

    invoke-virtual {v1, v6, v7}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 384
    const/4 v0, 0x0

    iget v2, p0, Llmg;->d:I

    invoke-virtual {v1, p3, v0, v2}, Ljava/io/RandomAccessFile;->write([BII)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 392
    invoke-static {v1}, Llrz;->a(Ljava/io/Closeable;)V

    .line 396
    iget-object v0, p0, Llmg;->f:Llml;

    invoke-interface {v0}, Llml;->a()J

    move-result-wide v0

    invoke-virtual {v3, v0, v1}, Llmn;->a(J)V

    .line 397
    invoke-virtual {v3}, Llmn;->d()V

    .line 399
    invoke-direct {p0}, Llmg;->i()V

    goto/16 :goto_0

    .line 372
    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    .line 379
    :cond_4
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3

    .line 385
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 386
    :goto_4
    :try_start_2
    const-string v2, "PersistentChunkStore"

    const-string v5, "Error writing chunk"

    invoke-static {v2, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 387
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    .line 388
    invoke-virtual {v3}, Llmn;->f()V

    .line 389
    iget-object v0, p0, Llmg;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 392
    invoke-static {v1}, Llrz;->a(Ljava/io/Closeable;)V

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_5
    invoke-static {v1}, Llrz;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_5

    .line 385
    :catch_1
    move-exception v0

    goto :goto_4

    :cond_5
    move-object v3, v0

    goto/16 :goto_1
.end method

.method public static c()Llml;
    .locals 1

    .prologue
    .line 61
    new-instance v0, Llmh;

    invoke-direct {v0}, Llmh;-><init>()V

    return-object v0
.end method

.method public static d()Llmm;
    .locals 1

    .prologue
    .line 73
    new-instance v0, Llmi;

    invoke-direct {v0}, Llmi;-><init>()V

    return-object v0
.end method

.method private i()V
    .locals 10

    .prologue
    .line 407
    :cond_0
    :goto_0
    invoke-virtual {p0}, Llmg;->h()I

    move-result v0

    int-to-long v0, v0

    iget-wide v2, p0, Llmg;->i:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    .line 408
    const/4 v1, 0x0

    const-wide v4, 0x7fffffffffffffffL

    iget-object v0, p0, Llmg;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llmn;

    iget-object v2, p0, Llmg;->e:Llmm;

    invoke-interface {v2, v0}, Llmm;->a(Llmn;)J

    move-result-wide v2

    cmp-long v7, v2, v4

    if-gez v7, :cond_3

    move-wide v8, v2

    move-object v2, v0

    move-wide v0, v8

    :goto_2
    move-wide v4, v0

    move-object v1, v2

    goto :goto_1

    :cond_1
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Llmn;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Llmg;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 410
    :cond_2
    return-void

    :cond_3
    move-object v2, v1

    move-wide v0, v4

    goto :goto_2
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 314
    iget v0, p0, Llmg;->d:I

    return v0
.end method

.method public a(Ljava/lang/String;)J
    .locals 3

    .prologue
    .line 289
    iget-object v2, p0, Llmg;->h:Ljava/lang/Object;

    monitor-enter v2

    .line 290
    :try_start_0
    iget-object v0, p0, Llmg;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llmn;

    .line 291
    if-nez v0, :cond_0

    .line 292
    const-wide/16 v0, -0x1

    monitor-exit v2

    .line 294
    :goto_0
    return-wide v0

    :cond_0
    invoke-virtual {v0}, Llmn;->g()J

    move-result-wide v0

    monitor-exit v2

    goto :goto_0

    .line 296
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(J)V
    .locals 5

    .prologue
    .line 169
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "quotaBytes must be non-negative"

    invoke-static {v0, v1}, Llsk;->a(ZLjava/lang/Object;)V

    .line 170
    iget-object v1, p0, Llmg;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 171
    :try_start_0
    iget-boolean v0, p0, Llmg;->j:Z

    const-string v2, "ChunkStore not initialized"

    invoke-static {v0, v2}, Llsk;->b(ZLjava/lang/Object;)V

    .line 172
    iget v0, p0, Llmg;->d:I

    int-to-long v2, v0

    div-long v2, p1, v2

    iput-wide v2, p0, Llmg;->i:J

    .line 173
    invoke-direct {p0}, Llmg;->i()V

    .line 174
    monitor-exit v1

    return-void

    .line 169
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 174
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Ljava/lang/String;I[B)V
    .locals 7

    .prologue
    .line 233
    iget-object v6, p0, Llmg;->h:Ljava/lang/Object;

    monitor-enter v6

    .line 234
    :try_start_0
    iget-boolean v0, p0, Llmg;->j:Z

    const-string v1, "ChunkStore not initialized"

    invoke-static {v0, v1}, Llsk;->b(ZLjava/lang/Object;)V

    .line 236
    const-wide/16 v4, -0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Llmg;->b(Ljava/lang/String;I[BJ)V

    .line 237
    monitor-exit v6

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Ljava/lang/String;I[BJ)V
    .locals 4

    .prologue
    .line 242
    iget-object v1, p0, Llmg;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 243
    :try_start_0
    iget-boolean v0, p0, Llmg;->j:Z

    const-string v2, "ChunkStore not initialized"

    invoke-static {v0, v2}, Llsk;->b(ZLjava/lang/Object;)V

    .line 245
    invoke-direct/range {p0 .. p5}, Llmg;->b(Ljava/lang/String;I[BJ)V

    .line 246
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Ljava/lang/String;I)Z
    .locals 3

    .prologue
    .line 301
    iget-object v1, p0, Llmg;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 302
    :try_start_0
    iget-boolean v0, p0, Llmg;->j:Z

    const-string v2, "ChunkStore not initialized"

    invoke-static {v0, v2}, Llsk;->b(ZLjava/lang/Object;)V

    .line 304
    iget-object v0, p0, Llmg;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llmn;

    .line 305
    if-nez v0, :cond_0

    .line 306
    const/4 v0, 0x0

    monitor-exit v1

    .line 308
    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0, p2}, Llmn;->a(I)Z

    move-result v0

    monitor-exit v1

    goto :goto_0

    .line 309
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b(Ljava/lang/String;I[B)I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 251
    iget-object v4, p0, Llmg;->h:Ljava/lang/Object;

    monitor-enter v4

    .line 252
    :try_start_0
    iget-boolean v0, p0, Llmg;->j:Z

    const-string v2, "ChunkStore not initialized"

    invoke-static {v0, v2}, Llsk;->b(ZLjava/lang/Object;)V

    .line 254
    iget-object v0, p0, Llmg;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llmn;

    .line 255
    if-nez v0, :cond_0

    .line 256
    monitor-exit v4

    move v0, v1

    .line 274
    :goto_0
    return v0

    .line 258
    :cond_0
    invoke-virtual {v0}, Llmn;->g()J

    move-result-wide v2

    const-wide/16 v6, -0x1

    cmp-long v2, v2, v6

    if-eqz v2, :cond_1

    iget v2, p0, Llmg;->d:I

    mul-int/2addr v2, p2

    int-to-long v2, v2

    .line 259
    invoke-virtual {v0}, Llmn;->g()J

    move-result-wide v6

    cmp-long v2, v2, v6

    if-ltz v2, :cond_1

    .line 260
    const/4 v0, -0x1

    monitor-exit v4

    goto :goto_0

    .line 284
    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 262
    :cond_1
    :try_start_1
    invoke-virtual {v0, p2}, Llmn;->a(I)Z

    move-result v2

    if-nez v2, :cond_2

    .line 263
    monitor-exit v4

    move v0, v1

    goto :goto_0

    .line 266
    :cond_2
    iget v1, p0, Llmg;->d:I

    invoke-virtual {v0, p2, v1}, Llmn;->a(II)I

    move-result v1

    .line 267
    iget-object v2, p0, Llmg;->f:Llml;

    invoke-interface {v2}, Llml;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Llmn;->a(J)V

    .line 268
    new-instance v5, Ljava/io/File;

    iget-object v2, p0, Llmg;->c:Ljava/io/File;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v0, "~c"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_3

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-direct {v5, v2, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 269
    const/4 v3, 0x0

    .line 271
    :try_start_2
    new-instance v2, Ljava/io/RandomAccessFile;

    const-string v0, "r"

    invoke-direct {v2, v5, v0}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 272
    :try_start_3
    iget v0, p0, Llmg;->d:I

    mul-int/2addr v0, p2

    int-to-long v6, v0

    invoke-virtual {v2, v6, v7}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 273
    const/4 v0, 0x0

    iget v3, p0, Llmg;->d:I

    invoke-virtual {v2, p3, v0, v3}, Ljava/io/RandomAccessFile;->readFully([BII)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 274
    :try_start_4
    invoke-static {v2}, Llrz;->a(Ljava/io/Closeable;)V

    monitor-exit v4

    move v0, v1

    goto :goto_0

    .line 268
    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 275
    :catch_0
    move-exception v0

    move-object v1, v3

    .line 278
    :goto_2
    :try_start_5
    const-string v2, "Failed to read cache file "

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_4

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 279
    :goto_3
    invoke-virtual {p0, p1}, Llmg;->b(Ljava/lang/String;)V

    .line 280
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 282
    :catchall_1
    move-exception v0

    :goto_4
    :try_start_6
    invoke-static {v1}, Llrz;->a(Ljava/io/Closeable;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 278
    :cond_4
    :try_start_7
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_3

    .line 282
    :catchall_2
    move-exception v0

    move-object v1, v3

    goto :goto_4

    :catchall_3
    move-exception v0

    move-object v1, v2

    goto :goto_4

    .line 275
    :catch_1
    move-exception v0

    move-object v1, v2

    goto :goto_2
.end method

.method public b()V
    .locals 3

    .prologue
    .line 343
    iget-object v1, p0, Llmg;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 344
    :try_start_0
    iget-boolean v0, p0, Llmg;->j:Z

    const-string v2, "ChunkStore not initialized"

    invoke-static {v0, v2}, Llsk;->b(ZLjava/lang/Object;)V

    .line 346
    iget-object v0, p0, Llmg;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llmn;

    .line 347
    invoke-virtual {v0}, Llmn;->e()Z

    goto :goto_0

    .line 349
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 191
    iget-object v1, p0, Llmg;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 192
    :try_start_0
    iget-boolean v0, p0, Llmg;->j:Z

    const-string v2, "ChunkStore not initialized"

    invoke-static {v0, v2}, Llsk;->b(ZLjava/lang/Object;)V

    .line 195
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Llmg;->c:Ljava/io/File;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v0, "~m"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v4, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v2, v3, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 197
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 200
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Llmg;->c:Ljava/io/File;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v0, "~c"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v4, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-direct {v2, v3, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 201
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 204
    iget-object v0, p0, Llmg;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 205
    monitor-exit v1

    return-void

    .line 195
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 205
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 200
    :cond_1
    :try_start_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public e()V
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 153
    iget-object v3, p0, Llmg;->h:Ljava/lang/Object;

    monitor-enter v3

    .line 154
    :try_start_0
    iget-object v1, p0, Llmg;->c:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 155
    iget-object v1, p0, Llmg;->c:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->mkdir()Z

    .line 158
    :cond_0
    iget-boolean v1, p0, Llmg;->j:Z

    if-nez v1, :cond_6

    .line 159
    iget-object v1, p0, Llmg;->c:Ljava/io/File;

    sget-object v2, Llmg;->a:Ljava/io/FilenameFilter;

    invoke-virtual {v1, v2}, Ljava/io/File;->list(Ljava/io/FilenameFilter;)[Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Llmg;->c:Ljava/io/File;

    sget-object v4, Llmg;->b:Ljava/io/FilenameFilter;

    invoke-virtual {v2, v4}, Ljava/io/File;->list(Ljava/io/FilenameFilter;)[Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/util/HashSet;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v5, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    array-length v6, v4

    move v2, v0

    :goto_0
    if-ge v2, v6, :cond_4

    aget-object v1, v4, v2

    const/4 v0, 0x0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, -0x2

    invoke-virtual {v1, v0, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    const-string v0, "~c"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v9

    if-eqz v9, :cond_2

    invoke-virtual {v8, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v5, v0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_3

    const/4 v1, 0x0

    :try_start_1
    iget-object v0, p0, Llmg;->c:Ljava/io/File;

    invoke-static {v0, v7}, Llmn;->a(Ljava/io/File;Ljava/lang/String;)Llmn;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    :goto_2
    if-eqz v0, :cond_1

    :try_start_2
    iget-object v1, p0, Llmg;->g:Ljava/util/Map;

    invoke-interface {v1, v7, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    :goto_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v8}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 162
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 159
    :catch_0
    move-exception v0

    :try_start_3
    const-string v8, "PersistentChunkStore"

    const-string v9, "Error loading metadata"

    invoke-static {v8, v9, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    goto :goto_2

    :cond_3
    new-instance v0, Ljava/io/File;

    iget-object v7, p0, Llmg;->c:Ljava/io/File;

    invoke-direct {v0, v7, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto :goto_3

    :cond_4
    invoke-virtual {v5}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-instance v2, Ljava/io/File;

    iget-object v4, p0, Llmg;->c:Ljava/io/File;

    invoke-direct {v2, v4, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    goto :goto_4

    .line 160
    :cond_5
    const/4 v0, 0x1

    iput-boolean v0, p0, Llmg;->j:Z

    .line 162
    :cond_6
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-void
.end method

.method public f()J
    .locals 6

    .prologue
    .line 181
    iget-object v1, p0, Llmg;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 182
    :try_start_0
    iget-boolean v0, p0, Llmg;->j:Z

    const-string v2, "ChunkStore not initialized"

    invoke-static {v0, v2}, Llsk;->b(ZLjava/lang/Object;)V

    .line 183
    iget-wide v2, p0, Llmg;->i:J

    iget v0, p0, Llmg;->d:I

    int-to-long v4, v0

    mul-long/2addr v2, v4

    monitor-exit v1

    return-wide v2

    .line 184
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public g()V
    .locals 7

    .prologue
    .line 212
    iget-object v1, p0, Llmg;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 213
    :try_start_0
    iget-boolean v0, p0, Llmg;->j:Z

    const-string v2, "ChunkStore not initialized"

    invoke-static {v0, v2}, Llsk;->b(ZLjava/lang/Object;)V

    .line 216
    iget-object v0, p0, Llmg;->c:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v2

    .line 219
    if-eqz v2, :cond_0

    .line 220
    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 221
    new-instance v5, Ljava/io/File;

    iget-object v6, p0, Llmg;->c:Ljava/io/File;

    invoke-direct {v5, v6, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 222
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    .line 220
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 227
    :cond_0
    iget-object v0, p0, Llmg;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 228
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public h()I
    .locals 4

    .prologue
    .line 330
    iget-object v2, p0, Llmg;->h:Ljava/lang/Object;

    monitor-enter v2

    .line 331
    :try_start_0
    iget-boolean v0, p0, Llmg;->j:Z

    const-string v1, "ChunkStore not initialized"

    invoke-static {v0, v1}, Llsk;->b(ZLjava/lang/Object;)V

    .line 333
    const/4 v0, 0x0

    .line 334
    iget-object v1, p0, Llmg;->g:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llmn;

    .line 335
    invoke-virtual {v0}, Llmn;->c()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    .line 336
    goto :goto_0

    .line 337
    :cond_0
    monitor-exit v2

    return v1

    .line 338
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
