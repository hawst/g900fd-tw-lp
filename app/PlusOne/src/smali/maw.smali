.class public final Lmaw;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmyd;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2347
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2350
    iput-object v0, p0, Lmaw;->apiHeader:Llyq;

    .line 2353
    iput-object v0, p0, Lmaw;->a:Lmyd;

    .line 2347
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 2370
    const/4 v0, 0x0

    .line 2371
    iget-object v1, p0, Lmaw;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 2372
    const/4 v0, 0x1

    iget-object v1, p0, Lmaw;->apiHeader:Llyq;

    .line 2373
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2375
    :cond_0
    iget-object v1, p0, Lmaw;->a:Lmyd;

    if-eqz v1, :cond_1

    .line 2376
    const/4 v1, 0x2

    iget-object v2, p0, Lmaw;->a:Lmyd;

    .line 2377
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2379
    :cond_1
    iget-object v1, p0, Lmaw;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2380
    iput v0, p0, Lmaw;->ai:I

    .line 2381
    return v0
.end method

.method public a(Loxn;)Lmaw;
    .locals 2

    .prologue
    .line 2389
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2390
    sparse-switch v0, :sswitch_data_0

    .line 2394
    iget-object v1, p0, Lmaw;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 2395
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmaw;->ah:Ljava/util/List;

    .line 2398
    :cond_1
    iget-object v1, p0, Lmaw;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2400
    :sswitch_0
    return-object p0

    .line 2405
    :sswitch_1
    iget-object v0, p0, Lmaw;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 2406
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmaw;->apiHeader:Llyq;

    .line 2408
    :cond_2
    iget-object v0, p0, Lmaw;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2412
    :sswitch_2
    iget-object v0, p0, Lmaw;->a:Lmyd;

    if-nez v0, :cond_3

    .line 2413
    new-instance v0, Lmyd;

    invoke-direct {v0}, Lmyd;-><init>()V

    iput-object v0, p0, Lmaw;->a:Lmyd;

    .line 2415
    :cond_3
    iget-object v0, p0, Lmaw;->a:Lmyd;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2390
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 2358
    iget-object v0, p0, Lmaw;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 2359
    const/4 v0, 0x1

    iget-object v1, p0, Lmaw;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2361
    :cond_0
    iget-object v0, p0, Lmaw;->a:Lmyd;

    if-eqz v0, :cond_1

    .line 2362
    const/4 v0, 0x2

    iget-object v1, p0, Lmaw;->a:Lmyd;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2364
    :cond_1
    iget-object v0, p0, Lmaw;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2366
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2343
    invoke-virtual {p0, p1}, Lmaw;->a(Loxn;)Lmaw;

    move-result-object v0

    return-object v0
.end method
