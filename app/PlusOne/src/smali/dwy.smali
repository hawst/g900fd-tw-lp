.class public final Ldwy;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Landroid/content/Context;II)[Ldwz;
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 44
    invoke-static {p0, p1}, Ldhv;->t(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 46
    if-eqz v0, :cond_1

    .line 47
    const/16 v1, 0x2c

    invoke-static {v0, v1}, Llsu;->a(Ljava/lang/String;C)Ljava/util/ArrayList;

    move-result-object v5

    .line 48
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    rem-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_1

    .line 49
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    invoke-static {v0, p2}, Ljava/lang/Math;->min(II)I

    move-result v0

    new-array v2, v0, [Ldwz;

    move v3, v4

    .line 51
    :goto_0
    :try_start_0
    array-length v0, v2

    if-ge v3, v0, :cond_0

    .line 52
    new-instance v6, Ldwz;

    shl-int/lit8 v0, v3, 0x1

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    shl-int/lit8 v1, v3, 0x1

    add-int/lit8 v1, v1, 0x1

    .line 53
    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-direct {v6, v0, v1}, Ldwz;-><init>(Ljava/lang/String;I)V

    aput-object v6, v2, v3
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 51
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_0
    move-object v0, v2

    .line 63
    :goto_1
    return-object v0

    :catch_0
    move-exception v0

    :cond_1
    new-array v0, v4, [Ldwz;

    goto :goto_1
.end method
