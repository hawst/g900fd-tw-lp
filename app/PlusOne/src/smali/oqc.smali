.class public final Loqc;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Loqc;


# instance fields
.field private b:Ljava/lang/Integer;

.field private c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 150
    const/4 v0, 0x0

    new-array v0, v0, [Loqc;

    sput-object v0, Loqc;->a:[Loqc;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 151
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 172
    const/4 v0, 0x0

    .line 173
    iget-object v1, p0, Loqc;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 174
    const/4 v0, 0x2

    iget-object v1, p0, Loqc;->c:Ljava/lang/String;

    .line 175
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 177
    :cond_0
    iget-object v1, p0, Loqc;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 178
    const/4 v1, 0x3

    iget-object v2, p0, Loqc;->b:Ljava/lang/Integer;

    .line 179
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 181
    :cond_1
    iget-object v1, p0, Loqc;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 182
    iput v0, p0, Loqc;->ai:I

    .line 183
    return v0
.end method

.method public a(Loxn;)Loqc;
    .locals 2

    .prologue
    .line 191
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 192
    sparse-switch v0, :sswitch_data_0

    .line 196
    iget-object v1, p0, Loqc;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 197
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Loqc;->ah:Ljava/util/List;

    .line 200
    :cond_1
    iget-object v1, p0, Loqc;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 202
    :sswitch_0
    return-object p0

    .line 207
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loqc;->c:Ljava/lang/String;

    goto :goto_0

    .line 211
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Loqc;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 192
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x18 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 160
    iget-object v0, p0, Loqc;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 161
    const/4 v0, 0x2

    iget-object v1, p0, Loqc;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 163
    :cond_0
    iget-object v0, p0, Loqc;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 164
    const/4 v0, 0x3

    iget-object v1, p0, Loqc;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 166
    :cond_1
    iget-object v0, p0, Loqc;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 168
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 147
    invoke-virtual {p0, p1}, Loqc;->a(Loxn;)Loqc;

    move-result-object v0

    return-object v0
.end method
