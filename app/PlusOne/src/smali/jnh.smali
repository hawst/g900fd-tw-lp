.class public final Ljnh;
.super Landroid/view/ViewGroup;
.source "PG"


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/widget/TextView;

.field private c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 55
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 0

    .prologue
    .line 81
    iput p1, p0, Ljnh;->c:I

    .line 82
    return-void
.end method

.method public a(Landroid/widget/TextView;Landroid/widget/TextView;)V
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Ljnh;->a:Landroid/widget/TextView;

    .line 72
    invoke-virtual {p0, p1}, Ljnh;->addView(Landroid/view/View;)V

    .line 74
    iput-object p2, p0, Ljnh;->b:Landroid/widget/TextView;

    .line 75
    if-eqz p2, :cond_0

    .line 76
    invoke-virtual {p0, p2}, Ljnh;->addView(Landroid/view/View;)V

    .line 78
    :cond_0
    return-void
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    .prologue
    const/4 v1, -0x2

    .line 67
    new-instance v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    return-object v0
.end method

.method protected onLayout(ZIIII)V
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 128
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-lt v1, v3, :cond_1

    invoke-virtual {p0}, Ljnh;->getLayoutDirection()I

    move-result v1

    if-ne v1, v0, :cond_1

    move v1, v0

    .line 129
    :goto_0
    sub-int v5, p5, p3

    .line 130
    sub-int v3, p4, p2

    .line 132
    iget-object v0, p0, Ljnh;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v4

    .line 133
    iget-object v0, p0, Ljnh;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 137
    if-eqz v1, :cond_2

    .line 139
    sub-int v2, v3, v4

    move v0, v3

    .line 144
    :goto_1
    iget-object v3, p0, Ljnh;->a:Landroid/widget/TextView;

    add-int v7, v6, v5

    invoke-virtual {v3, v2, v6, v0, v7}, Landroid/widget/TextView;->layout(IIII)V

    .line 146
    iget-object v0, p0, Ljnh;->b:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 149
    if-eqz v1, :cond_3

    .line 150
    iget v0, p0, Ljnh;->c:I

    sub-int v0, v2, v0

    .line 151
    iget-object v1, p0, Ljnh;->b:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v1

    sub-int v1, v0, v1

    move v2, v1

    move v1, v0

    .line 158
    :goto_2
    iget-object v0, p0, Ljnh;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 159
    iget-object v3, p0, Ljnh;->b:Landroid/widget/TextView;

    add-int v4, v0, v5

    invoke-virtual {v3, v2, v0, v1, v4}, Landroid/widget/TextView;->layout(IIII)V

    .line 161
    :cond_0
    return-void

    :cond_1
    move v1, v2

    .line 128
    goto :goto_0

    :cond_2
    move v0, v4

    .line 142
    goto :goto_1

    .line 154
    :cond_3
    iget v0, p0, Ljnh;->c:I

    add-int v1, v4, v0

    .line 155
    iget-object v0, p0, Ljnh;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v0

    add-int/2addr v0, v1

    move v2, v1

    move v1, v0

    goto :goto_2
.end method

.method protected onMeasure(II)V
    .locals 5

    .prologue
    const/high16 v4, -0x80000000

    const/4 v2, 0x0

    .line 87
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 94
    const v0, 0x7fffffff

    .line 100
    :goto_0
    iget-object v1, p0, Ljnh;->b:Landroid/widget/TextView;

    if-eqz v1, :cond_1

    .line 101
    iget v1, p0, Ljnh;->c:I

    sub-int v1, v0, v1

    .line 102
    iget-object v3, p0, Ljnh;->b:Landroid/widget/TextView;

    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {v3, v1, p2}, Landroid/widget/TextView;->measure(II)V

    .line 104
    iget-object v1, p0, Ljnh;->b:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v1

    iget v3, p0, Ljnh;->c:I

    add-int/2addr v1, v3

    .line 109
    :goto_1
    sub-int/2addr v0, v1

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 110
    iget-object v3, p0, Ljnh;->a:Landroid/widget/TextView;

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {v3, v0, p2}, Landroid/widget/TextView;->measure(II)V

    .line 113
    iget-object v0, p0, Ljnh;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 114
    iget-object v0, p0, Ljnh;->b:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ljnh;->b:Landroid/widget/TextView;

    .line 115
    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 116
    :goto_2
    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 118
    iget-object v3, p0, Ljnh;->b:Landroid/widget/TextView;

    if-eqz v3, :cond_0

    iget-object v2, p0, Ljnh;->b:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v2

    .line 120
    :cond_0
    iget-object v3, p0, Ljnh;->a:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v1, v3

    .line 121
    iget-object v3, p0, Ljnh;->a:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v3

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 123
    invoke-virtual {p0, v1, v0}, Ljnh;->setMeasuredDimension(II)V

    .line 124
    return-void

    .line 90
    :sswitch_0
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    goto :goto_0

    :cond_1
    move v1, v2

    .line 106
    goto :goto_1

    :cond_2
    move v0, v2

    .line 115
    goto :goto_2

    .line 87
    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_0
        0x40000000 -> :sswitch_0
    .end sparse-switch
.end method
