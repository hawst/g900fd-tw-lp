.class public final Lobu;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:[Lodo;

.field private c:Lpdz;

.field private d:Ljava/lang/Long;

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 15
    sget-object v0, Lodo;->a:[Lodo;

    iput-object v0, p0, Lobu;->b:[Lodo;

    .line 18
    const/4 v0, 0x0

    iput-object v0, p0, Lobu;->c:Lpdz;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 52
    .line 53
    iget-object v0, p0, Lobu;->a:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 54
    const/4 v0, 0x1

    iget-object v2, p0, Lobu;->a:Ljava/lang/String;

    .line 55
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 57
    :goto_0
    iget-object v2, p0, Lobu;->b:[Lodo;

    if-eqz v2, :cond_1

    .line 58
    iget-object v2, p0, Lobu;->b:[Lodo;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 59
    if-eqz v4, :cond_0

    .line 60
    const/4 v5, 0x2

    .line 61
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 58
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 65
    :cond_1
    iget-object v1, p0, Lobu;->c:Lpdz;

    if-eqz v1, :cond_2

    .line 66
    const/4 v1, 0x3

    iget-object v2, p0, Lobu;->c:Lpdz;

    .line 67
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 69
    :cond_2
    iget-object v1, p0, Lobu;->d:Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 70
    const/4 v1, 0x4

    iget-object v2, p0, Lobu;->d:Ljava/lang/Long;

    .line 71
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 73
    :cond_3
    iget-object v1, p0, Lobu;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 74
    const/4 v1, 0x5

    iget-object v2, p0, Lobu;->e:Ljava/lang/String;

    .line 75
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 77
    :cond_4
    iget-object v1, p0, Lobu;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 78
    iput v0, p0, Lobu;->ai:I

    .line 79
    return v0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lobu;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 87
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 88
    sparse-switch v0, :sswitch_data_0

    .line 92
    iget-object v2, p0, Lobu;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 93
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lobu;->ah:Ljava/util/List;

    .line 96
    :cond_1
    iget-object v2, p0, Lobu;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 98
    :sswitch_0
    return-object p0

    .line 103
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lobu;->a:Ljava/lang/String;

    goto :goto_0

    .line 107
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 108
    iget-object v0, p0, Lobu;->b:[Lodo;

    if-nez v0, :cond_3

    move v0, v1

    .line 109
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lodo;

    .line 110
    iget-object v3, p0, Lobu;->b:[Lodo;

    if-eqz v3, :cond_2

    .line 111
    iget-object v3, p0, Lobu;->b:[Lodo;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 113
    :cond_2
    iput-object v2, p0, Lobu;->b:[Lodo;

    .line 114
    :goto_2
    iget-object v2, p0, Lobu;->b:[Lodo;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 115
    iget-object v2, p0, Lobu;->b:[Lodo;

    new-instance v3, Lodo;

    invoke-direct {v3}, Lodo;-><init>()V

    aput-object v3, v2, v0

    .line 116
    iget-object v2, p0, Lobu;->b:[Lodo;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 117
    invoke-virtual {p1}, Loxn;->a()I

    .line 114
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 108
    :cond_3
    iget-object v0, p0, Lobu;->b:[Lodo;

    array-length v0, v0

    goto :goto_1

    .line 120
    :cond_4
    iget-object v2, p0, Lobu;->b:[Lodo;

    new-instance v3, Lodo;

    invoke-direct {v3}, Lodo;-><init>()V

    aput-object v3, v2, v0

    .line 121
    iget-object v2, p0, Lobu;->b:[Lodo;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 125
    :sswitch_3
    iget-object v0, p0, Lobu;->c:Lpdz;

    if-nez v0, :cond_5

    .line 126
    new-instance v0, Lpdz;

    invoke-direct {v0}, Lpdz;-><init>()V

    iput-object v0, p0, Lobu;->c:Lpdz;

    .line 128
    :cond_5
    iget-object v0, p0, Lobu;->c:Lpdz;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 132
    :sswitch_4
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lobu;->d:Ljava/lang/Long;

    goto/16 :goto_0

    .line 136
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lobu;->e:Ljava/lang/String;

    goto/16 :goto_0

    .line 88
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 27
    iget-object v0, p0, Lobu;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 28
    const/4 v0, 0x1

    iget-object v1, p0, Lobu;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 30
    :cond_0
    iget-object v0, p0, Lobu;->b:[Lodo;

    if-eqz v0, :cond_2

    .line 31
    iget-object v1, p0, Lobu;->b:[Lodo;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 32
    if-eqz v3, :cond_1

    .line 33
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 31
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 37
    :cond_2
    iget-object v0, p0, Lobu;->c:Lpdz;

    if-eqz v0, :cond_3

    .line 38
    const/4 v0, 0x3

    iget-object v1, p0, Lobu;->c:Lpdz;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 40
    :cond_3
    iget-object v0, p0, Lobu;->d:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 41
    const/4 v0, 0x4

    iget-object v1, p0, Lobu;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 43
    :cond_4
    iget-object v0, p0, Lobu;->e:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 44
    const/4 v0, 0x5

    iget-object v1, p0, Lobu;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 46
    :cond_5
    iget-object v0, p0, Lobu;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 48
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lobu;->a(Loxn;)Lobu;

    move-result-object v0

    return-object v0
.end method
