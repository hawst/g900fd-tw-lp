.class public final Lfdf;
.super Lhxz;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lhxz",
        "<",
        "Lfdg;",
        ">;"
    }
.end annotation


# static fields
.field private static final b:[Ljava/lang/String;


# instance fields
.field private final c:Ldp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldo",
            "<",
            "Lfdg;",
            ">.dp;"
        }
    .end annotation
.end field

.field private final d:I

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private g:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 29
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "data"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "comment_count"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "media_attr"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "user_actions"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "last_refresh_time"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "acl"

    aput-object v2, v0, v1

    sput-object v0, Lfdf;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lhxz;-><init>(Landroid/content/Context;)V

    .line 49
    new-instance v0, Ldp;

    invoke-direct {v0, p0}, Ldp;-><init>(Ldo;)V

    iput-object v0, p0, Lfdf;->c:Ldp;

    .line 58
    iput p4, p0, Lfdf;->d:I

    .line 59
    iput-object p2, p0, Lfdf;->e:Ljava/lang/String;

    .line 60
    iput-object p3, p0, Lfdf;->f:Ljava/lang/String;

    .line 61
    return-void
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Z
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/4 v8, 0x0

    .line 141
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 154
    :cond_0
    :goto_0
    return v0

    .line 147
    :cond_1
    :try_start_0
    const-string v1, "shared_collections"

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "_id"

    aput-object v3, v2, v0

    const-string v3, "_id = ?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p2, v4, v0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 151
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    .line 153
    if-eqz v1, :cond_0

    .line 154
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 153
    :catchall_0
    move-exception v0

    move-object v1, v8

    :goto_1
    if-eqz v1, :cond_2

    .line 154
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    .line 153
    :catchall_1
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method protected D_()Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 65
    invoke-virtual {p0}, Lfdf;->n()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lfdf;->f:Ljava/lang/String;

    .line 66
    invoke-static {v1}, Ljvj;->p(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lfdf;->c:Ldp;

    .line 65
    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 69
    iget-object v0, p0, Lfdf;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 70
    invoke-virtual {p0}, Lfdf;->n()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lfdf;->g:Ljava/lang/String;

    .line 71
    invoke-static {v1}, Ljvd;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lfdf;->c:Ldp;

    .line 70
    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 75
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic j()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 27
    invoke-virtual {p0}, Lfdf;->l()Lfdg;

    move-result-object v0

    return-object v0
.end method

.method protected k()Z
    .locals 2

    .prologue
    .line 80
    invoke-virtual {p0}, Lfdf;->n()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lfdf;->c:Ldp;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 81
    const/4 v0, 0x1

    return v0
.end method

.method public l()Lfdg;
    .locals 12

    .prologue
    .line 86
    const/4 v0, 0x0

    iput-object v0, p0, Lfdf;->g:Ljava/lang/String;

    .line 87
    invoke-virtual {p0}, Lfdf;->n()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lfdf;->d:I

    invoke-static {v0, v1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    .line 88
    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 90
    const/4 v8, 0x0

    .line 92
    const/4 v1, 0x2

    :try_start_0
    new-array v4, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lfdf;->e:Ljava/lang/String;

    aput-object v2, v4, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lfdf;->f:Ljava/lang/String;

    aput-object v2, v4, v1

    .line 96
    const-string v1, "all_tiles"

    sget-object v2, Lfdf;->b:[Ljava/lang/String;

    const-string v3, "view_id = ? AND tile_id = ?"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v9

    .line 99
    :try_start_1
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 100
    new-instance v1, Lnym;

    invoke-direct {v1}, Lnym;-><init>()V

    const/4 v2, 0x0

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    invoke-static {v1, v2}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v1

    check-cast v1, Lnym;

    .line 102
    const/4 v2, 0x1

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lnym;->k:Ljava/lang/Integer;

    .line 103
    const/4 v2, 0x2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 104
    const/4 v4, 0x3

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 105
    const/4 v6, 0x4

    invoke-interface {v9, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 106
    const/4 v8, 0x5

    invoke-interface {v9, v8}, Landroid/database/Cursor;->isNull(I)Z

    move-result v8

    if-eqz v8, :cond_5

    const/4 v8, -0x1

    .line 109
    :goto_0
    iget-object v10, v1, Lnym;->y:[Lnxr;

    if-eqz v10, :cond_1

    iget-object v10, v1, Lnym;->y:[Lnxr;

    array-length v10, v10

    if-eqz v10, :cond_1

    iget-object v10, v1, Lnym;->y:[Lnxr;

    const/4 v11, 0x0

    aget-object v10, v10, v11

    iget-object v10, v10, Lnxr;->c:Ljava/lang/String;

    if-eqz v10, :cond_1

    const/4 v10, 0x2

    if-eq v8, v10, :cond_0

    const/4 v10, -0x1

    if-ne v8, v10, :cond_1

    .line 114
    :cond_0
    iget-object v10, v1, Lnym;->y:[Lnxr;

    const/4 v11, 0x0

    aget-object v10, v10, v11

    iget-object v10, v10, Lnxr;->c:Ljava/lang/String;

    iput-object v10, p0, Lfdf;->g:Ljava/lang/String;

    .line 117
    :cond_1
    invoke-virtual {p0}, Lfdf;->D()V

    .line 118
    invoke-virtual {p0}, Lfdf;->C()V

    .line 120
    const/4 v10, 0x2

    if-eq v8, v10, :cond_2

    const/4 v10, -0x1

    if-ne v8, v10, :cond_3

    :cond_2
    iget-object v8, p0, Lfdf;->g:Ljava/lang/String;

    .line 122
    invoke-direct {p0, v0, v8}, Lfdf;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_3
    const/4 v8, 0x1

    .line 124
    :goto_1
    new-instance v0, Lfdg;

    invoke-direct/range {v0 .. v8}, Lfdg;-><init>(Lnym;JJJZ)V
    :try_end_1
    .catch Loxt; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 130
    if-eqz v9, :cond_4

    .line 131
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_4
    :goto_2
    return-object v0

    .line 106
    :cond_5
    const/4 v8, 0x5

    .line 107
    :try_start_2
    invoke-interface {v9, v8}, Landroid/database/Cursor;->getInt(I)I
    :try_end_2
    .catch Loxt; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v8

    goto :goto_0

    .line 122
    :cond_6
    const/4 v8, 0x0

    goto :goto_1

    .line 126
    :cond_7
    if-eqz v9, :cond_8

    .line 131
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_8
    const/4 v0, 0x0

    goto :goto_2

    .line 128
    :catch_0
    move-exception v0

    move-object v0, v8

    :goto_3
    if-eqz v0, :cond_9

    .line 131
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_9
    const/4 v0, 0x0

    goto :goto_2

    .line 130
    :catchall_0
    move-exception v0

    move-object v9, v8

    :goto_4
    if-eqz v9, :cond_a

    .line 131
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_a
    throw v0

    .line 130
    :catchall_1
    move-exception v0

    goto :goto_4

    .line 128
    :catch_1
    move-exception v0

    move-object v0, v9

    goto :goto_3
.end method
