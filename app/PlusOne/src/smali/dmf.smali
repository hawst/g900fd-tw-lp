.class public final Ldmf;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmgq;",
        "Lmgr;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final p:Ljava/lang/String;

.field private final q:Ljava/lang/String;

.field private final r:Z

.field private final s:I


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)V
    .locals 6

    .prologue
    .line 44
    const-string v3, "photosofuser"

    new-instance v4, Lmgq;

    invoke-direct {v4}, Lmgq;-><init>()V

    new-instance v5, Lmgr;

    invoke-direct {v5}, Lmgr;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 47
    iput-object p3, p0, Ldmf;->a:Ljava/lang/String;

    .line 48
    iput-object p5, p0, Ldmf;->b:Ljava/lang/String;

    .line 49
    iput-object p6, p0, Ldmf;->q:Ljava/lang/String;

    .line 50
    iput-boolean p8, p0, Ldmf;->r:Z

    .line 51
    iput p7, p0, Ldmf;->s:I

    .line 52
    iput-object p4, p0, Ldmf;->p:Ljava/lang/String;

    .line 53
    return-void
.end method


# virtual methods
.method protected a(Lmgq;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 57
    new-instance v0, Lnel;

    invoke-direct {v0}, Lnel;-><init>()V

    iput-object v0, p1, Lmgq;->a:Lnel;

    .line 58
    iget-object v1, p1, Lmgq;->a:Lnel;

    .line 59
    iget-object v0, p0, Ldmf;->a:Ljava/lang/String;

    iput-object v0, v1, Lnel;->a:Ljava/lang/String;

    .line 60
    new-instance v0, Lnei;

    invoke-direct {v0}, Lnei;-><init>()V

    .line 61
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v0, Lnei;->g:Ljava/lang/Boolean;

    .line 62
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v0, Lnei;->i:Ljava/lang/Boolean;

    .line 63
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v0, Lnei;->a:Ljava/lang/Boolean;

    .line 64
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v0, Lnei;->l:Ljava/lang/Boolean;

    .line 65
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v0, Lnei;->h:Ljava/lang/Boolean;

    .line 66
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v0, Lnei;->k:Ljava/lang/Boolean;

    .line 67
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v0, Lnei;->e:Ljava/lang/Boolean;

    .line 68
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v0, Lnei;->f:Ljava/lang/Boolean;

    .line 69
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v0, Lnei;->d:Ljava/lang/Boolean;

    .line 70
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v0, Lnei;->b:Ljava/lang/Boolean;

    .line 71
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v0, Lnei;->c:Ljava/lang/Boolean;

    .line 72
    const/4 v2, 0x2

    iput v2, v0, Lnei;->j:I

    .line 74
    iput-object v0, v1, Lnel;->e:Lnei;

    .line 76
    iget v0, p0, Ldmf;->s:I

    if-lez v0, :cond_0

    iget v0, p0, Ldmf;->s:I

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Lnel;->b:Ljava/lang/Integer;

    .line 78
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v1, Lnel;->g:Ljava/lang/Boolean;

    .line 79
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v1, Lnel;->h:Ljava/lang/Boolean;

    .line 80
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v1, Lnel;->f:Ljava/lang/Boolean;

    .line 82
    iget-object v0, p0, Ldmf;->b:Ljava/lang/String;

    const-string v2, "~pending_photos_of_user"

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 83
    iget-object v0, p0, Ldmf;->q:Ljava/lang/String;

    iput-object v0, v1, Lnel;->d:Ljava/lang/String;

    .line 84
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v1, Lnel;->g:Ljava/lang/Boolean;

    .line 89
    :goto_1
    return-void

    .line 76
    :cond_0
    const/16 v0, 0x64

    goto :goto_0

    .line 86
    :cond_1
    iget-object v0, p0, Ldmf;->q:Ljava/lang/String;

    iput-object v0, v1, Lnel;->c:Ljava/lang/String;

    .line 87
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v1, Lnel;->f:Ljava/lang/Boolean;

    goto :goto_1
.end method

.method protected a(Lmgr;)V
    .locals 11

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 93
    iget-object v0, p1, Lmgr;->a:Lnfp;

    .line 94
    iget-object v1, p0, Ldmf;->b:Ljava/lang/String;

    const-string v2, "~pending_photos_of_user"

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 97
    iget-object v1, v0, Lnfp;->d:[Lnzx;

    .line 98
    iget-object v0, v0, Lnfp;->b:Ljava/lang/String;

    .line 105
    :goto_0
    iget-object v2, p0, Ldmf;->q:Ljava/lang/String;

    if-nez v2, :cond_0

    if-eqz v1, :cond_0

    array-length v2, v1

    if-nez v2, :cond_2

    :cond_0
    move-object v3, v6

    .line 107
    :goto_1
    iget-object v1, p0, Ldmf;->f:Landroid/content/Context;

    iget v2, p0, Ldmf;->c:I

    iget-object v4, p0, Ldmf;->p:Ljava/lang/String;

    iget-boolean v8, p0, Ldmf;->r:Z

    invoke-static {v1, v2, v4, v0, v8}, Ljvj;->b(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)V

    .line 109
    iget-object v0, p0, Ldmf;->f:Landroid/content/Context;

    iget v1, p0, Ldmf;->c:I

    iget-object v2, p0, Ldmf;->p:Ljava/lang/String;

    iget-boolean v4, p0, Ldmf;->r:Z

    invoke-static/range {v0 .. v7}, Ljvj;->a(Landroid/content/Context;ILjava/lang/String;[Lnzx;ZZLjava/lang/String;Z)V

    .line 111
    return-void

    .line 100
    :cond_1
    iget-object v1, v0, Lnfp;->c:[Lnzx;

    .line 101
    iget-object v0, v0, Lnfp;->a:Ljava/lang/String;

    goto :goto_0

    .line 105
    :cond_2
    iget-object v2, p0, Ldmf;->b:Ljava/lang/String;

    const-string v3, "~pending_photos_of_user"

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Ldmf;->f:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0aaa

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    :goto_2
    new-instance v4, Lnzx;

    invoke-direct {v4}, Lnzx;-><init>()V

    const/4 v3, 0x2

    iput v3, v4, Lnzx;->k:I

    iput-object v2, v4, Lnzx;->c:Ljava/lang/String;

    new-instance v3, Lnzo;

    invoke-direct {v3}, Lnzo;-><init>()V

    new-instance v8, Lnyb;

    invoke-direct {v8}, Lnyb;-><init>()V

    iget-object v9, p0, Ldmf;->b:Ljava/lang/String;

    iput-object v9, v8, Lnyb;->d:Ljava/lang/String;

    iput-object v2, v8, Lnyb;->b:Ljava/lang/String;

    if-eqz v1, :cond_3

    array-length v2, v1

    if-eqz v2, :cond_3

    aget-object v2, v1, v5

    iget-object v2, v2, Lnzx;->f:Lnyl;

    iput-object v2, v4, Lnzx;->f:Lnyl;

    :cond_3
    new-instance v2, Lnyz;

    invoke-direct {v2}, Lnyz;-><init>()V

    iget-object v9, p0, Ldmf;->a:Ljava/lang/String;

    iput-object v9, v2, Lnyz;->c:Ljava/lang/String;

    iput-object v2, v8, Lnyb;->f:Lnyz;

    iput-object v8, v3, Lnzo;->b:Lnyb;

    sget-object v9, Lnzo;->a:Loxr;

    invoke-virtual {v4, v9, v3}, Lnzx;->a(Loxr;Ljava/lang/Object;)V

    const/4 v3, 0x3

    new-array v9, v7, [Ljava/lang/String;

    iget-object v2, v2, Lnyz;->c:Ljava/lang/String;

    iget-object v8, v8, Lnyb;->d:Ljava/lang/String;

    const-string v10, "ALBUM"

    invoke-static {v6, v2, v8, v10}, Ljvj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v9, v5

    invoke-static {v3, v9}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v4, Lnzx;->b:Ljava/lang/String;

    iput-object v1, v4, Lnzx;->j:[Lnzx;

    new-array v3, v7, [Lnzx;

    aput-object v4, v3, v5

    goto/16 :goto_1

    :cond_4
    iget-object v2, p0, Ldmf;->f:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0aab

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_2
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 25
    check-cast p1, Lmgq;

    invoke-virtual {p0, p1}, Ldmf;->a(Lmgq;)V

    return-void
.end method

.method protected synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 25
    check-cast p1, Lmgr;

    invoke-virtual {p0, p1}, Ldmf;->a(Lmgr;)V

    return-void
.end method
