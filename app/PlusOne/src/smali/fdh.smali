.class public final Lfdh;
.super Ljfh;
.source "PG"


# static fields
.field private static final c:[Ljava/lang/String;


# instance fields
.field private d:Z

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 46
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "square_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "square_name"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "unread_count"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "membership_status"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "joinability"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "photo_url"

    aput-object v2, v0, v1

    sput-object v0, Lfdh;->c:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 76
    invoke-direct {p0}, Ljfh;-><init>()V

    .line 72
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfdh;->d:Z

    .line 74
    const/4 v0, 0x0

    iput v0, p0, Lfdh;->e:I

    .line 77
    const-string v0, "squares"

    invoke-virtual {p0, v0}, Lfdh;->b(Ljava/lang/String;)V

    .line 78
    return-void
.end method

.method private k()Z
    .locals 2

    .prologue
    .line 371
    iget-object v0, p0, Lfdh;->a:Landroid/content/Context;

    const-class v1, Lhei;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iget v1, p0, Lfdh;->b:I

    .line 372
    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "is_google_plus"

    invoke-interface {v0, v1}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method protected a(Landroid/database/Cursor;)I
    .locals 2

    .prologue
    .line 185
    const-string v0, "invited"

    const/4 v1, 0x1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 186
    const/4 v0, 0x5

    .line 188
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/database/ContentObserver;)V
    .locals 3

    .prologue
    .line 327
    iget-object v0, p0, Lfdh;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lktp;->a:Landroid/net/Uri;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, p1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 329
    return-void
.end method

.method protected a(Landroid/database/Cursor;Ljgb;)V
    .locals 12

    .prologue
    const/4 v7, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 193
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 194
    const-string v3, "invited"

    invoke-virtual {v3, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 195
    iget-object v3, p0, Lfdh;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f11001c

    iget v5, p0, Lfdh;->e:I

    new-array v0, v0, [Ljava/lang/Object;

    iget v6, p0, Lfdh;->e:I

    .line 197
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v0, v1

    .line 196
    invoke-virtual {v3, v4, v5, v0}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    move-object v0, p2

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move v7, v1

    .line 199
    invoke-interface/range {v0 .. v7}, Ljgb;->a(ILizu;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 226
    :goto_0
    return-void

    .line 206
    :cond_0
    invoke-interface {p1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 207
    const/4 v3, 0x3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iget-object v4, p0, Lfdh;->a:Landroid/content/Context;

    const/16 v5, 0x63

    if-le v3, v5, :cond_1

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0593

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 208
    :goto_1
    const/4 v3, 0x6

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 211
    const/4 v4, 0x4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 212
    invoke-static {v4}, Lkto;->b(I)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 213
    iget-object v0, p0, Lfdh;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f0a0467

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 218
    :goto_2
    iget-object v0, p0, Lfdh;->a:Landroid/content/Context;

    sget-object v4, Ljac;->a:Ljac;

    .line 219
    invoke-static {v0, v3, v4}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v5

    move-object v3, p2

    move v4, v1

    move-object v8, v2

    move v10, v1

    .line 218
    invoke-interface/range {v3 .. v10}, Ljgb;->a(ILizu;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 222
    new-instance v0, Lkqw;

    sget-object v1, Lomy;->f:Lhmn;

    invoke-direct {v0, v1, v11}, Lkqw;-><init>(Lhmn;Ljava/lang/String;)V

    invoke-interface {p2, v0}, Ljgb;->a(Lhmk;)Lhmk;

    goto :goto_0

    .line 207
    :cond_1
    if-lez v3, :cond_2

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    goto :goto_1

    :cond_2
    move-object v9, v2

    goto :goto_1

    .line 214
    :cond_3
    if-ne v4, v7, :cond_4

    :goto_3
    if-eqz v0, :cond_5

    .line 215
    iget-object v0, p0, Lfdh;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f0a0468

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    goto :goto_2

    :cond_4
    move v0, v1

    .line 214
    goto :goto_3

    :cond_5
    move-object v7, v2

    goto :goto_2
.end method

.method public a(Ljfz;J)V
    .locals 6

    .prologue
    .line 338
    invoke-super {p0, p1, p2, p3}, Ljfh;->a(Ljfz;J)V

    .line 340
    iget-object v0, p1, Ljfz;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 341
    iget-object v0, p0, Lfdh;->a:Landroid/content/Context;

    const v1, 0x7f0a062a

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f020544

    iget-object v2, p0, Lfdh;->a:Landroid/content/Context;

    const v3, 0x7f0a04e7

    .line 343
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lfdh;->a:Landroid/content/Context;

    iget v4, p0, Lfdh;->b:I

    .line 344
    invoke-static {v3, v4}, Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareSearchActivity;->a(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v3

    .line 341
    invoke-virtual {p1, v0, v1, v2, v3}, Ljfz;->a(Ljava/lang/String;ILjava/lang/String;Landroid/content/Intent;)V

    .line 346
    :cond_0
    return-void
.end method

.method protected a(Ljfz;Landroid/database/Cursor;)V
    .locals 9

    .prologue
    .line 350
    const/4 v0, 0x2

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const v7, 0x7f0202f2

    iget-object v0, p0, Lfdh;->a:Landroid/content/Context;

    const v1, 0x7f0a0238

    .line 352
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    iget-object v0, p0, Lfdh;->a:Landroid/content/Context;

    const-class v1, Lkvh;

    .line 353
    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkvh;

    iget v1, p0, Lfdh;->b:I

    const/4 v2, 0x1

    .line 354
    invoke-interface {p2, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x4

    .line 355
    invoke-interface {p2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    const/4 v4, 0x5

    .line 356
    invoke-interface {p2, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    const/4 v5, 0x0

    .line 353
    invoke-interface/range {v0 .. v5}, Lkvh;->a(ILjava/lang/String;IILjava/lang/Integer;)Landroid/content/Intent;

    move-result-object v0

    .line 350
    invoke-virtual {p1, v6, v7, v8, v0}, Ljfz;->a(Ljava/lang/String;ILjava/lang/String;Landroid/content/Intent;)V

    .line 358
    return-void
.end method

.method protected a(Ljgb;)V
    .locals 8

    .prologue
    const v1, 0x7f0204f7

    const/4 v7, 0x0

    const/4 v2, 0x0

    .line 103
    invoke-virtual {p0}, Lfdh;->t()Z

    move-result v0

    if-nez v0, :cond_1

    .line 104
    iget v0, p0, Lfdh;->e:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lfdh;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0a080d

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    :goto_0
    iget-object v0, p0, Lfdh;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f0a080e

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object v0, p1

    move-object v5, v2

    move-object v6, v2

    invoke-interface/range {v0 .. v7}, Ljgb;->a(ILizu;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    new-instance v0, Lhmk;

    sget-object v1, Lomy;->c:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    invoke-interface {p1, v0}, Ljgb;->a(Lhmk;)Lhmk;

    .line 108
    :goto_1
    return-void

    .line 104
    :cond_0
    iget-object v0, p0, Lfdh;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f11001c

    iget v4, p0, Lfdh;->e:I

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    iget v6, p0, Lfdh;->e:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v0, v3, v4, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 106
    :cond_1
    iget-object v0, p0, Lfdh;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0a080f

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lfdh;->r()Z

    move-result v7

    move-object v0, p1

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    invoke-interface/range {v0 .. v7}, Ljgb;->a(ILizu;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    new-instance v0, Lhmk;

    sget-object v1, Lomy;->d:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    invoke-interface {p1, v0}, Ljgb;->a(Lhmk;)Lhmk;

    goto :goto_1
.end method

.method protected aq_()Z
    .locals 1

    .prologue
    .line 82
    invoke-direct {p0}, Lfdh;->k()Z

    move-result v0

    return v0
.end method

.method protected ar_()Z
    .locals 1

    .prologue
    .line 149
    invoke-virtual {p0}, Lfdh;->u()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lfdh;->m()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected b(Landroid/database/Cursor;)Landroid/content/Intent;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 230
    const-string v0, "invited"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 231
    invoke-virtual {p0}, Lfdh;->i()Landroid/content/Intent;

    move-result-object v0

    .line 238
    :goto_0
    return-object v0

    .line 234
    :cond_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lfdh;->a:Landroid/content/Context;

    const-class v2, Lcom/google/android/apps/plus/phone/HomeActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 235
    const-string v1, "destination"

    const/16 v2, 0x9

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 236
    const-string v1, "square_id"

    .line 237
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 236
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public b(Landroid/database/ContentObserver;)V
    .locals 1

    .prologue
    .line 333
    iget-object v0, p0, Lfdh;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 334
    return-void
.end method

.method protected b(Ljgb;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 164
    iget-object v0, p0, Lfdh;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0a0810

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object v0, p1

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move v7, v1

    .line 166
    invoke-interface/range {v0 .. v7}, Ljgb;->a(ILizu;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 169
    new-instance v0, Lhmk;

    sget-object v1, Lomy;->c:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    invoke-interface {p1, v0}, Ljgb;->a(Lhmk;)Lhmk;

    .line 171
    return-void
.end method

.method protected b()Z
    .locals 1

    .prologue
    .line 87
    invoke-direct {p0}, Lfdh;->k()Z

    move-result v0

    return v0
.end method

.method public b(I)Z
    .locals 1

    .prologue
    .line 367
    invoke-virtual {p0}, Lfdh;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Ljfh;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected c()I
    .locals 1

    .prologue
    .line 92
    invoke-virtual {p0}, Lfdh;->t()Z

    move-result v0

    if-nez v0, :cond_0

    .line 93
    const/4 v0, 0x2

    .line 96
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lfdh;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x3

    goto :goto_0

    :cond_1
    const/4 v0, 0x4

    goto :goto_0
.end method

.method protected c(Landroid/database/Cursor;)J
    .locals 2

    .prologue
    .line 243
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method protected e()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 144
    iget-object v0, p0, Lfdh;->a:Landroid/content/Context;

    iget v1, p0, Lfdh;->b:I

    invoke-static {v0, v1}, Leyq;->o(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method protected g()Z
    .locals 1

    .prologue
    .line 154
    invoke-virtual {p0}, Lfdh;->ar_()Z

    move-result v0

    return v0
.end method

.method protected h()I
    .locals 1

    .prologue
    .line 159
    const/4 v0, 0x5

    return v0
.end method

.method protected i()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 175
    iget-object v0, p0, Lfdh;->a:Landroid/content/Context;

    iget v1, p0, Lfdh;->b:I

    invoke-static {v0, v1}, Leyq;->o(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method protected j()Z
    .locals 1

    .prologue
    .line 180
    const/4 v0, 0x1

    return v0
.end method

.method protected l()Landroid/database/Cursor;
    .locals 12

    .prologue
    const/4 v11, 0x3

    const/4 v10, -0x1

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 252
    invoke-direct {p0}, Lfdh;->k()Z

    move-result v0

    if-nez v0, :cond_0

    .line 253
    new-instance v0, Lhym;

    sget-object v1, Lfdh;->c:[Ljava/lang/String;

    invoke-direct {v0, v1}, Lhym;-><init>([Ljava/lang/String;)V

    .line 292
    :goto_0
    return-object v0

    .line 255
    :cond_0
    iget-object v0, p0, Lfdh;->a:Landroid/content/Context;

    const-class v1, Lktq;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lktq;

    .line 257
    iget-object v1, p0, Lfdh;->a:Landroid/content/Context;

    const-class v2, Lktw;

    invoke-static {v1, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lktw;

    iget v2, p0, Lfdh;->b:I

    invoke-virtual {v1, v2}, Lktw;->a(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 258
    iget-object v1, p0, Lfdh;->a:Landroid/content/Context;

    iget v2, p0, Lfdh;->b:I

    invoke-static {v1, v2}, Lktk;->a(Landroid/content/Context;I)Lktk;

    move-result-object v1

    .line 259
    iget-object v2, p0, Lfdh;->a:Landroid/content/Context;

    invoke-static {v2, v1}, Lhoc;->a(Landroid/content/Context;Lhny;)V

    .line 262
    :cond_1
    iget-object v1, p0, Lfdh;->a:Landroid/content/Context;

    iget v2, p0, Lfdh;->b:I

    .line 263
    invoke-static {v1, v2}, Lhzt;->b(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const-string v2, "SELECT COUNT(*) FROM squares WHERE membership_status=? AND invitation_dismissed=? AND list_category=?"

    new-array v3, v11, [Ljava/lang/String;

    const-string v4, "5"

    aput-object v4, v3, v7

    const-string v4, "0"

    aput-object v4, v3, v8

    const-string v4, "1"

    aput-object v4, v3, v9

    .line 262
    invoke-static {v1, v2, v3}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v2

    long-to-int v1, v2

    iput v1, p0, Lfdh;->e:I

    .line 272
    iget v1, p0, Lfdh;->b:I

    sget-object v2, Lfdh;->c:[Ljava/lang/String;

    const-string v3, "square_name COLLATE NOCASE"

    invoke-interface {v0, v1, v2, v3, v7}, Lktq;->a(I[Ljava/lang/String;Ljava/lang/String;Z)Landroid/database/Cursor;

    move-result-object v1

    .line 275
    iget v0, p0, Lfdh;->e:I

    if-lez v0, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 276
    new-instance v2, Lhym;

    sget-object v0, Lfdh;->c:[Ljava/lang/String;

    invoke-direct {v2, v0, v8}, Lhym;-><init>([Ljava/lang/String;I)V

    .line 277
    iget-object v0, p0, Lfdh;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f11001c

    iget v4, p0, Lfdh;->e:I

    new-array v5, v8, [Ljava/lang/Object;

    iget v6, p0, Lfdh;->e:I

    .line 279
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    .line 278
    invoke-virtual {v0, v3, v4, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 280
    const/4 v3, 0x7

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "-1"

    aput-object v4, v3, v7

    const-string v4, "invited"

    aput-object v4, v3, v8

    aput-object v0, v3, v9

    .line 284
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v11

    const/4 v0, 0x4

    .line 285
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x5

    .line 286
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x6

    const/4 v4, 0x0

    aput-object v4, v3, v0

    .line 280
    invoke-virtual {v2, v3}, Lhym;->a([Ljava/lang/Object;)V

    .line 289
    new-instance v0, Landroid/database/MergeCursor;

    new-array v3, v9, [Landroid/database/Cursor;

    aput-object v2, v3, v7

    aput-object v1, v3, v8

    invoke-direct {v0, v3}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    goto/16 :goto_0

    :cond_2
    move-object v0, v1

    goto/16 :goto_0
.end method

.method protected m()Z
    .locals 1

    .prologue
    .line 297
    invoke-virtual {p0}, Lfdh;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lfdh;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected n()I
    .locals 1

    .prologue
    .line 302
    const/4 v0, 0x0

    return v0
.end method

.method protected o()V
    .locals 1

    .prologue
    .line 307
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfdh;->d:Z

    .line 308
    return-void
.end method

.method protected p()V
    .locals 1

    .prologue
    .line 312
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfdh;->d:Z

    .line 313
    return-void
.end method

.method public q()Z
    .locals 1

    .prologue
    .line 317
    const/4 v0, 0x1

    return v0
.end method

.method protected r()Z
    .locals 1

    .prologue
    .line 362
    invoke-virtual {p0}, Lfdh;->u()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
