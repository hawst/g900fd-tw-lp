.class public final Lons;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lons;


# instance fields
.field public b:Ljava/lang/Long;

.field public c:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 932
    const/4 v0, 0x0

    new-array v0, v0, [Lons;

    sput-object v0, Lons;->a:[Lons;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 933
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 954
    const/4 v0, 0x0

    .line 955
    iget-object v1, p0, Lons;->b:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 956
    const/4 v0, 0x1

    iget-object v1, p0, Lons;->b:Ljava/lang/Long;

    .line 957
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Loxo;->f(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 959
    :cond_0
    iget-object v1, p0, Lons;->c:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 960
    const/4 v1, 0x2

    iget-object v2, p0, Lons;->c:Ljava/lang/Long;

    .line 961
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 963
    :cond_1
    iget-object v1, p0, Lons;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 964
    iput v0, p0, Lons;->ai:I

    .line 965
    return v0
.end method

.method public a(Loxn;)Lons;
    .locals 2

    .prologue
    .line 973
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 974
    sparse-switch v0, :sswitch_data_0

    .line 978
    iget-object v1, p0, Lons;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 979
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lons;->ah:Ljava/util/List;

    .line 982
    :cond_1
    iget-object v1, p0, Lons;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 984
    :sswitch_0
    return-object p0

    .line 989
    :sswitch_1
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lons;->b:Ljava/lang/Long;

    goto :goto_0

    .line 993
    :sswitch_2
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lons;->c:Ljava/lang/Long;

    goto :goto_0

    .line 974
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 942
    iget-object v0, p0, Lons;->b:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 943
    const/4 v0, 0x1

    iget-object v1, p0, Lons;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 945
    :cond_0
    iget-object v0, p0, Lons;->c:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 946
    const/4 v0, 0x2

    iget-object v1, p0, Lons;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 948
    :cond_1
    iget-object v0, p0, Lons;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 950
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 929
    invoke-virtual {p0, p1}, Lons;->a(Loxn;)Lons;

    move-result-object v0

    return-object v0
.end method
