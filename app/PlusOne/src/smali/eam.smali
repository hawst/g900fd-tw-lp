.class public abstract Leam;
.super Leak;
.source "PG"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<",
        "ListViewType:Landroid/widget/AbsListView;",
        "AdapterType:",
        "Lhyd;",
        ">",
        "Leak;",
        "Landroid/widget/AbsListView$OnScrollListener;"
    }
.end annotation


# instance fields
.field public N:Landroid/widget/AbsListView;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "T",
            "ListViewType;"
        }
    .end annotation
.end field

.field public O:Lhyd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TAdapterType;"
        }
    .end annotation
.end field

.field private P:I

.field private Q:I

.field private R:I

.field private S:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 19
    invoke-direct {p0}, Leak;-><init>()V

    .line 31
    iput v0, p0, Leam;->R:I

    .line 32
    iput v0, p0, Leam;->S:I

    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;
    .locals 2

    .prologue
    .line 63
    invoke-super {p0, p1, p2, p3, p4}, Leak;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;

    move-result-object v1

    .line 66
    const v0, 0x102000a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AbsListView;

    iput-object v0, p0, Leam;->N:Landroid/widget/AbsListView;

    .line 67
    iget-object v0, p0, Leam;->N:Landroid/widget/AbsListView;

    invoke-virtual {v0, p0}, Landroid/widget/AbsListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 69
    return-object v1
.end method

.method protected a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 160
    iget-object v0, p0, Leam;->N:Landroid/widget/AbsListView;

    if-nez v0, :cond_0

    .line 179
    :goto_0
    return-void

    .line 168
    :cond_0
    iget-object v0, p0, Leam;->N:Landroid/widget/AbsListView;

    invoke-virtual {v0}, Landroid/widget/AbsListView;->getFirstVisiblePosition()I

    move-result v0

    iput v0, p0, Leam;->P:I

    .line 169
    iget-object v0, p0, Leam;->O:Lhyd;

    if-eqz v0, :cond_2

    .line 170
    iget-object v0, p0, Leam;->N:Landroid/widget/AbsListView;

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 171
    if-eqz v0, :cond_1

    .line 172
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    iput v0, p0, Leam;->Q:I

    goto :goto_0

    .line 174
    :cond_1
    iput v1, p0, Leam;->Q:I

    goto :goto_0

    .line 177
    :cond_2
    iput v1, p0, Leam;->Q:I

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 39
    invoke-super {p0, p1}, Leak;->a(Landroid/os/Bundle;)V

    .line 41
    if-eqz p1, :cond_0

    .line 42
    const-string v0, "scroll_pos"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Leam;->P:I

    .line 43
    const-string v0, "scroll_off"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Leam;->Q:I

    .line 48
    :goto_0
    return-void

    .line 45
    :cond_0
    iput v0, p0, Leam;->P:I

    .line 46
    iput v0, p0, Leam;->Q:I

    goto :goto_0
.end method

.method public aO_()V
    .locals 1

    .prologue
    .line 101
    invoke-super {p0}, Leak;->aO_()V

    .line 103
    iget-object v0, p0, Leam;->O:Lhyd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Leam;->O:Lhyd;

    invoke-virtual {v0}, Lhyd;->a()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Leam;->O:Lhyd;

    .line 106
    :cond_0
    return-void
.end method

.method public ae_()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 77
    invoke-super {p0}, Leak;->ae_()V

    .line 78
    iget-object v0, p0, Leam;->N:Landroid/widget/AbsListView;

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Leam;->N:Landroid/widget/AbsListView;

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 80
    iput-object v1, p0, Leam;->N:Landroid/widget/AbsListView;

    .line 82
    :cond_0
    return-void
.end method

.method public c()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 186
    iget-object v0, p0, Leam;->N:Landroid/widget/AbsListView;

    if-nez v0, :cond_1

    .line 197
    :cond_0
    :goto_0
    return-void

    .line 190
    :cond_1
    iget-object v0, p0, Leam;->N:Landroid/widget/AbsListView;

    instance-of v0, v0, Landroid/widget/ListView;

    if-eqz v0, :cond_0

    iget v0, p0, Leam;->Q:I

    if-nez v0, :cond_2

    iget v0, p0, Leam;->P:I

    if-eqz v0, :cond_0

    .line 191
    :cond_2
    iget-object v0, p0, Leam;->N:Landroid/widget/AbsListView;

    check-cast v0, Landroid/widget/ListView;

    iget v1, p0, Leam;->P:I

    iget v2, p0, Leam;->Q:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    .line 194
    iput v3, p0, Leam;->P:I

    .line 195
    iput v3, p0, Leam;->Q:I

    goto :goto_0
.end method

.method protected d()Z
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Leam;->O:Lhyd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Leam;->O:Lhyd;

    invoke-virtual {v0}, Lhyd;->a()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Leam;->O:Lhyd;

    invoke-virtual {v0}, Lhyd;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 113
    invoke-super {p0, p1}, Leak;->e(Landroid/os/Bundle;)V

    .line 115
    invoke-virtual {p0}, Leam;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Leam;->N:Landroid/widget/AbsListView;

    if-eqz v0, :cond_0

    .line 116
    invoke-virtual {p0}, Leam;->a()V

    .line 117
    const-string v0, "scroll_pos"

    iget v1, p0, Leam;->P:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 118
    const-string v0, "scroll_off"

    iget v1, p0, Leam;->Q:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 120
    :cond_0
    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 2

    .prologue
    .line 127
    if-lez p4, :cond_1

    .line 128
    add-int v0, p2, p3

    .line 129
    if-lt v0, p4, :cond_0

    iget v1, p0, Leam;->R:I

    if-ne v0, v1, :cond_0

    iget v1, p0, Leam;->S:I

    .line 131
    :cond_0
    iput v0, p0, Leam;->R:I

    .line 134
    iput p4, p0, Leam;->S:I

    .line 136
    :cond_1
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 143
    return-void
.end method

.method public z()V
    .locals 1

    .prologue
    .line 89
    invoke-super {p0}, Leak;->z()V

    .line 91
    iget-object v0, p0, Leam;->O:Lhyd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Leam;->O:Lhyd;

    invoke-virtual {v0}, Lhyd;->a()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Leam;->O:Lhyd;

    invoke-virtual {v0}, Lhyd;->e()V

    .line 94
    :cond_0
    return-void
.end method
