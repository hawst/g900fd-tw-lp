.class public final Loga;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lofv;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:[Lofd;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 4903
    invoke-direct {p0}, Loxq;-><init>()V

    .line 4906
    const/4 v0, 0x0

    iput-object v0, p0, Loga;->a:Lofv;

    .line 4913
    sget-object v0, Lofd;->a:[Lofd;

    iput-object v0, p0, Loga;->d:[Lofd;

    .line 4903
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 4940
    .line 4941
    iget-object v0, p0, Loga;->a:Lofv;

    if-eqz v0, :cond_4

    .line 4942
    const/4 v0, 0x1

    iget-object v2, p0, Loga;->a:Lofv;

    .line 4943
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 4945
    :goto_0
    iget-object v2, p0, Loga;->b:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 4946
    const/4 v2, 0x2

    iget-object v3, p0, Loga;->b:Ljava/lang/String;

    .line 4947
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 4949
    :cond_0
    iget-object v2, p0, Loga;->c:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 4950
    const/4 v2, 0x3

    iget-object v3, p0, Loga;->c:Ljava/lang/String;

    .line 4951
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 4953
    :cond_1
    iget-object v2, p0, Loga;->d:[Lofd;

    if-eqz v2, :cond_3

    .line 4954
    iget-object v2, p0, Loga;->d:[Lofd;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 4955
    if-eqz v4, :cond_2

    .line 4956
    const/4 v5, 0x4

    .line 4957
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 4954
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 4961
    :cond_3
    iget-object v1, p0, Loga;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4962
    iput v0, p0, Loga;->ai:I

    .line 4963
    return v0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Loga;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 4971
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 4972
    sparse-switch v0, :sswitch_data_0

    .line 4976
    iget-object v2, p0, Loga;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 4977
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Loga;->ah:Ljava/util/List;

    .line 4980
    :cond_1
    iget-object v2, p0, Loga;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4982
    :sswitch_0
    return-object p0

    .line 4987
    :sswitch_1
    iget-object v0, p0, Loga;->a:Lofv;

    if-nez v0, :cond_2

    .line 4988
    new-instance v0, Lofv;

    invoke-direct {v0}, Lofv;-><init>()V

    iput-object v0, p0, Loga;->a:Lofv;

    .line 4990
    :cond_2
    iget-object v0, p0, Loga;->a:Lofv;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 4994
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loga;->b:Ljava/lang/String;

    goto :goto_0

    .line 4998
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loga;->c:Ljava/lang/String;

    goto :goto_0

    .line 5002
    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 5003
    iget-object v0, p0, Loga;->d:[Lofd;

    if-nez v0, :cond_4

    move v0, v1

    .line 5004
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lofd;

    .line 5005
    iget-object v3, p0, Loga;->d:[Lofd;

    if-eqz v3, :cond_3

    .line 5006
    iget-object v3, p0, Loga;->d:[Lofd;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 5008
    :cond_3
    iput-object v2, p0, Loga;->d:[Lofd;

    .line 5009
    :goto_2
    iget-object v2, p0, Loga;->d:[Lofd;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 5010
    iget-object v2, p0, Loga;->d:[Lofd;

    new-instance v3, Lofd;

    invoke-direct {v3}, Lofd;-><init>()V

    aput-object v3, v2, v0

    .line 5011
    iget-object v2, p0, Loga;->d:[Lofd;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 5012
    invoke-virtual {p1}, Loxn;->a()I

    .line 5009
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 5003
    :cond_4
    iget-object v0, p0, Loga;->d:[Lofd;

    array-length v0, v0

    goto :goto_1

    .line 5015
    :cond_5
    iget-object v2, p0, Loga;->d:[Lofd;

    new-instance v3, Lofd;

    invoke-direct {v3}, Lofd;-><init>()V

    aput-object v3, v2, v0

    .line 5016
    iget-object v2, p0, Loga;->d:[Lofd;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 4972
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 4918
    iget-object v0, p0, Loga;->a:Lofv;

    if-eqz v0, :cond_0

    .line 4919
    const/4 v0, 0x1

    iget-object v1, p0, Loga;->a:Lofv;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 4921
    :cond_0
    iget-object v0, p0, Loga;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 4922
    const/4 v0, 0x2

    iget-object v1, p0, Loga;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 4924
    :cond_1
    iget-object v0, p0, Loga;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 4925
    const/4 v0, 0x3

    iget-object v1, p0, Loga;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 4927
    :cond_2
    iget-object v0, p0, Loga;->d:[Lofd;

    if-eqz v0, :cond_4

    .line 4928
    iget-object v1, p0, Loga;->d:[Lofd;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 4929
    if-eqz v3, :cond_3

    .line 4930
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 4928
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 4934
    :cond_4
    iget-object v0, p0, Loga;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 4936
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 4899
    invoke-virtual {p0, p1}, Loga;->a(Loxn;)Loga;

    move-result-object v0

    return-object v0
.end method
