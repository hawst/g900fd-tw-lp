.class public final Ldmb;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmge;",
        "Lmgf;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:J

.field private final b:Ljava/lang/String;

.field private final p:Ljava/lang/String;

.field private final q:Ljava/lang/String;

.field private final r:Ljava/lang/String;

.field private final s:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;IJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 29
    const-string v4, "photoscreatecomment"

    new-instance v5, Lmge;

    invoke-direct {v5}, Lmge;-><init>()V

    new-instance v6, Lmgf;

    invoke-direct {v6}, Lmgf;-><init>()V

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    invoke-direct/range {v1 .. v6}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 32
    iput-wide p3, p0, Ldmb;->a:J

    .line 33
    iput-object p5, p0, Ldmb;->b:Ljava/lang/String;

    .line 34
    iput-object p6, p0, Ldmb;->s:Ljava/lang/String;

    .line 35
    move-object/from16 v0, p7

    iput-object v0, p0, Ldmb;->p:Ljava/lang/String;

    .line 36
    move-object/from16 v0, p8

    iput-object v0, p0, Ldmb;->q:Ljava/lang/String;

    .line 37
    move-object/from16 v0, p9

    iput-object v0, p0, Ldmb;->r:Ljava/lang/String;

    .line 38
    return-void
.end method


# virtual methods
.method protected a(Lmge;)V
    .locals 4

    .prologue
    .line 42
    new-instance v0, Lnau;

    invoke-direct {v0}, Lnau;-><init>()V

    iput-object v0, p1, Lmge;->a:Lnau;

    .line 43
    iget-object v0, p1, Lmge;->a:Lnau;

    .line 44
    iget-object v1, p0, Ldmb;->p:Ljava/lang/String;

    iput-object v1, v0, Lnau;->a:Ljava/lang/String;

    .line 45
    iget-wide v2, p0, Ldmb;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lnau;->b:Ljava/lang/Long;

    .line 46
    iget-object v1, p0, Ldmb;->q:Ljava/lang/String;

    iput-object v1, v0, Lnau;->c:Ljava/lang/String;

    .line 47
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lnau;->d:Ljava/lang/Boolean;

    .line 48
    iget-object v1, p0, Ldmb;->r:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 49
    iget-object v1, p0, Ldmb;->r:Ljava/lang/String;

    iput-object v1, v0, Lnau;->e:Ljava/lang/String;

    .line 51
    :cond_0
    return-void
.end method

.method protected a(Lmgf;)V
    .locals 6

    .prologue
    .line 55
    iget-object v4, p1, Lmgf;->a:Lnav;

    .line 56
    iget-object v0, p0, Ldmb;->f:Landroid/content/Context;

    iget v1, p0, Ldmb;->c:I

    iget-object v2, p0, Ldmb;->b:Ljava/lang/String;

    iget-object v3, p0, Ldmb;->s:Ljava/lang/String;

    iget-object v4, v4, Lnav;->a:[Lnyf;

    const/4 v5, 0x1

    invoke-static/range {v0 .. v5}, Ljvd;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;[Lnyf;Z)V

    .line 58
    return-void
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 15
    check-cast p1, Lmge;

    invoke-virtual {p0, p1}, Ldmb;->a(Lmge;)V

    return-void
.end method

.method protected synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 15
    check-cast p1, Lmgf;

    invoke-virtual {p0, p1}, Ldmb;->a(Lmgf;)V

    return-void
.end method
