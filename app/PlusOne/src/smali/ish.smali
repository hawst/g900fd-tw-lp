.class public final Lish;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xc
.end annotation


# instance fields
.field private a:Lisi;

.field private b:Ljava/lang/String;

.field private c:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lisj;",
            ">;"
        }
    .end annotation
.end field

.field private d:Landroid/mtp/MtpDevice;

.field private e:Landroid/os/PowerManager$WakeLock;


# direct methods
.method public constructor <init>(Landroid/mtp/MtpDevice;Ljava/util/Collection;Ljava/lang/String;Landroid/content/Context;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/mtp/MtpDevice;",
            "Ljava/util/Collection",
            "<",
            "Lisj;",
            ">;",
            "Ljava/lang/String;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput-object p3, p0, Lish;->b:Ljava/lang/String;

    .line 64
    iput-object p2, p0, Lish;->c:Ljava/util/Collection;

    .line 65
    iput-object p1, p0, Lish;->d:Landroid/mtp/MtpDevice;

    .line 66
    const-string v0, "power"

    invoke-virtual {p4, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 67
    const/4 v1, 0x6

    const-string v2, "Google Photos MTP Import Task"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lish;->e:Landroid/os/PowerManager$WakeLock;

    .line 68
    return-void
.end method

.method private static a(J)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 121
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v1

    .line 122
    const-string v2, "mounted"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 131
    :cond_0
    :goto_0
    return v0

    .line 126
    :cond_1
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 128
    :try_start_0
    new-instance v2, Landroid/os/StatFs;

    invoke-direct {v2, v1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 129
    invoke-virtual {v2}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v1

    int-to-long v4, v1

    invoke-virtual {v2}, Landroid/os/StatFs;->getBlockSize()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    int-to-long v2, v1

    mul-long/2addr v2, v4

    cmp-long v1, v2, p0

    if-lez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    .line 131
    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method public a(Lisi;)V
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Lish;->a:Lisi;

    .line 72
    return-void
.end method

.method public run()V
    .locals 10

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 76
    iget-object v1, p0, Lish;->e:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 78
    :try_start_0
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    iget-object v1, p0, Lish;->c:Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v5

    iget-object v1, p0, Lish;->a:Lisi;

    const/4 v3, 0x0

    const/4 v6, 0x0

    invoke-interface {v1, v3, v5, v6}, Lisi;->a(IILjava/lang/String;)V

    new-instance v6, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    iget-object v3, p0, Lish;->b:Ljava/lang/String;

    invoke-direct {v6, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->mkdirs()Z

    iget-object v1, p0, Lish;->c:Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v1, v0

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lisj;

    add-int/lit8 v3, v1, 0x1

    invoke-virtual {v0}, Lisj;->a()I

    move-result v1

    int-to-long v8, v1

    invoke-static {v8, v9}, Lish;->a(J)Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lish;->d:Landroid/mtp/MtpDevice;

    invoke-virtual {v0, v1}, Lisj;->a(Landroid/mtp/MtpDevice;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v0, Ljava/io/FileNotFoundException;

    const-string v1, "Failure in determining destination file"

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 80
    :catch_0
    move-exception v0

    :try_start_1
    iget-object v0, p0, Lish;->a:Lisi;

    invoke-interface {v0}, Lisi;->d()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 82
    iput-object v2, p0, Lish;->a:Lisi;

    .line 83
    iget-object v0, p0, Lish;->e:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 84
    :goto_1
    return-void

    .line 78
    :cond_0
    :try_start_2
    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v6, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    iget-object v8, p0, Lish;->d:Landroid/mtp/MtpDevice;

    invoke-virtual {v0}, Lisj;->d()I

    move-result v9

    invoke-virtual {v8, v9, v1}, Landroid/mtp/MtpDevice;->importFile(ILjava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_1

    move-object v1, v2

    :cond_1
    :goto_2
    if-nez v1, :cond_2

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    iget-object v0, p0, Lish;->a:Lisi;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lish;->a:Lisi;

    invoke-interface {v0, v3, v5, v1}, Lisi;->a(IILjava/lang/String;)V

    :cond_3
    move v1, v3

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lish;->a:Lisi;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lish;->a:Lisi;

    invoke-interface {v0, v4, v1}, Lisi;->a(Ljava/util/Collection;I)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 82
    :cond_5
    iput-object v2, p0, Lish;->a:Lisi;

    .line 83
    iget-object v0, p0, Lish;->e:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    goto :goto_1

    .line 82
    :catchall_0
    move-exception v0

    iput-object v2, p0, Lish;->a:Lisi;

    .line 83
    iget-object v1, p0, Lish;->e:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    throw v0

    :cond_6
    move-object v1, v2

    goto :goto_2
.end method
