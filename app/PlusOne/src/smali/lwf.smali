.class public final Llwf;
.super Loxq;
.source "PG"


# static fields
.field public static final a:Loxr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Loxr",
            "<",
            "Llwf;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:Llwh;

.field private c:Llwn;

.field private d:[Llwj;

.field private e:Llwi;

.field private f:[Llwi;

.field private g:Llwl;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 9
    const v0, 0x44274c1

    new-instance v1, Llwg;

    invoke-direct {v1}, Llwg;-><init>()V

    .line 14
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    move-result-object v0

    sput-object v0, Llwf;->a:Loxr;

    .line 13
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 652
    iput-object v1, p0, Llwf;->c:Llwn;

    .line 655
    sget-object v0, Llwj;->a:[Llwj;

    iput-object v0, p0, Llwf;->d:[Llwj;

    .line 658
    iput-object v1, p0, Llwf;->e:Llwi;

    .line 661
    sget-object v0, Llwi;->a:[Llwi;

    iput-object v0, p0, Llwf;->f:[Llwi;

    .line 664
    iput-object v1, p0, Llwf;->g:Llwl;

    .line 667
    iput-object v1, p0, Llwf;->b:Llwh;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 704
    .line 705
    iget-object v0, p0, Llwf;->c:Llwn;

    if-eqz v0, :cond_7

    .line 706
    const/4 v0, 0x1

    iget-object v2, p0, Llwf;->c:Llwn;

    .line 707
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 709
    :goto_0
    iget-object v2, p0, Llwf;->d:[Llwj;

    if-eqz v2, :cond_1

    .line 710
    iget-object v3, p0, Llwf;->d:[Llwj;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 711
    if-eqz v5, :cond_0

    .line 712
    const/4 v6, 0x2

    .line 713
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 710
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 717
    :cond_1
    iget-object v2, p0, Llwf;->e:Llwi;

    if-eqz v2, :cond_2

    .line 718
    const/4 v2, 0x3

    iget-object v3, p0, Llwf;->e:Llwi;

    .line 719
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 721
    :cond_2
    iget-object v2, p0, Llwf;->f:[Llwi;

    if-eqz v2, :cond_4

    .line 722
    iget-object v2, p0, Llwf;->f:[Llwi;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 723
    if-eqz v4, :cond_3

    .line 724
    const/4 v5, 0x4

    .line 725
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 722
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 729
    :cond_4
    iget-object v1, p0, Llwf;->g:Llwl;

    if-eqz v1, :cond_5

    .line 730
    const/4 v1, 0x5

    iget-object v2, p0, Llwf;->g:Llwl;

    .line 731
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 733
    :cond_5
    iget-object v1, p0, Llwf;->b:Llwh;

    if-eqz v1, :cond_6

    .line 734
    const/4 v1, 0x6

    iget-object v2, p0, Llwf;->b:Llwh;

    .line 735
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 737
    :cond_6
    iget-object v1, p0, Llwf;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 738
    iput v0, p0, Llwf;->ai:I

    .line 739
    return v0

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Llwf;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 747
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 748
    sparse-switch v0, :sswitch_data_0

    .line 752
    iget-object v2, p0, Llwf;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 753
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Llwf;->ah:Ljava/util/List;

    .line 756
    :cond_1
    iget-object v2, p0, Llwf;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 758
    :sswitch_0
    return-object p0

    .line 763
    :sswitch_1
    iget-object v0, p0, Llwf;->c:Llwn;

    if-nez v0, :cond_2

    .line 764
    new-instance v0, Llwn;

    invoke-direct {v0}, Llwn;-><init>()V

    iput-object v0, p0, Llwf;->c:Llwn;

    .line 766
    :cond_2
    iget-object v0, p0, Llwf;->c:Llwn;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 770
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 771
    iget-object v0, p0, Llwf;->d:[Llwj;

    if-nez v0, :cond_4

    move v0, v1

    .line 772
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Llwj;

    .line 773
    iget-object v3, p0, Llwf;->d:[Llwj;

    if-eqz v3, :cond_3

    .line 774
    iget-object v3, p0, Llwf;->d:[Llwj;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 776
    :cond_3
    iput-object v2, p0, Llwf;->d:[Llwj;

    .line 777
    :goto_2
    iget-object v2, p0, Llwf;->d:[Llwj;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 778
    iget-object v2, p0, Llwf;->d:[Llwj;

    new-instance v3, Llwj;

    invoke-direct {v3}, Llwj;-><init>()V

    aput-object v3, v2, v0

    .line 779
    iget-object v2, p0, Llwf;->d:[Llwj;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 780
    invoke-virtual {p1}, Loxn;->a()I

    .line 777
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 771
    :cond_4
    iget-object v0, p0, Llwf;->d:[Llwj;

    array-length v0, v0

    goto :goto_1

    .line 783
    :cond_5
    iget-object v2, p0, Llwf;->d:[Llwj;

    new-instance v3, Llwj;

    invoke-direct {v3}, Llwj;-><init>()V

    aput-object v3, v2, v0

    .line 784
    iget-object v2, p0, Llwf;->d:[Llwj;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 788
    :sswitch_3
    iget-object v0, p0, Llwf;->e:Llwi;

    if-nez v0, :cond_6

    .line 789
    new-instance v0, Llwi;

    invoke-direct {v0}, Llwi;-><init>()V

    iput-object v0, p0, Llwf;->e:Llwi;

    .line 791
    :cond_6
    iget-object v0, p0, Llwf;->e:Llwi;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 795
    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 796
    iget-object v0, p0, Llwf;->f:[Llwi;

    if-nez v0, :cond_8

    move v0, v1

    .line 797
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Llwi;

    .line 798
    iget-object v3, p0, Llwf;->f:[Llwi;

    if-eqz v3, :cond_7

    .line 799
    iget-object v3, p0, Llwf;->f:[Llwi;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 801
    :cond_7
    iput-object v2, p0, Llwf;->f:[Llwi;

    .line 802
    :goto_4
    iget-object v2, p0, Llwf;->f:[Llwi;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    .line 803
    iget-object v2, p0, Llwf;->f:[Llwi;

    new-instance v3, Llwi;

    invoke-direct {v3}, Llwi;-><init>()V

    aput-object v3, v2, v0

    .line 804
    iget-object v2, p0, Llwf;->f:[Llwi;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 805
    invoke-virtual {p1}, Loxn;->a()I

    .line 802
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 796
    :cond_8
    iget-object v0, p0, Llwf;->f:[Llwi;

    array-length v0, v0

    goto :goto_3

    .line 808
    :cond_9
    iget-object v2, p0, Llwf;->f:[Llwi;

    new-instance v3, Llwi;

    invoke-direct {v3}, Llwi;-><init>()V

    aput-object v3, v2, v0

    .line 809
    iget-object v2, p0, Llwf;->f:[Llwi;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 813
    :sswitch_5
    iget-object v0, p0, Llwf;->g:Llwl;

    if-nez v0, :cond_a

    .line 814
    new-instance v0, Llwl;

    invoke-direct {v0}, Llwl;-><init>()V

    iput-object v0, p0, Llwf;->g:Llwl;

    .line 816
    :cond_a
    iget-object v0, p0, Llwf;->g:Llwl;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 820
    :sswitch_6
    iget-object v0, p0, Llwf;->b:Llwh;

    if-nez v0, :cond_b

    .line 821
    new-instance v0, Llwh;

    invoke-direct {v0}, Llwh;-><init>()V

    iput-object v0, p0, Llwf;->b:Llwh;

    .line 823
    :cond_b
    iget-object v0, p0, Llwf;->b:Llwh;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 748
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 672
    iget-object v1, p0, Llwf;->c:Llwn;

    if-eqz v1, :cond_0

    .line 673
    const/4 v1, 0x1

    iget-object v2, p0, Llwf;->c:Llwn;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 675
    :cond_0
    iget-object v1, p0, Llwf;->d:[Llwj;

    if-eqz v1, :cond_2

    .line 676
    iget-object v2, p0, Llwf;->d:[Llwj;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 677
    if-eqz v4, :cond_1

    .line 678
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 676
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 682
    :cond_2
    iget-object v1, p0, Llwf;->e:Llwi;

    if-eqz v1, :cond_3

    .line 683
    const/4 v1, 0x3

    iget-object v2, p0, Llwf;->e:Llwi;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 685
    :cond_3
    iget-object v1, p0, Llwf;->f:[Llwi;

    if-eqz v1, :cond_5

    .line 686
    iget-object v1, p0, Llwf;->f:[Llwi;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    .line 687
    if-eqz v3, :cond_4

    .line 688
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 686
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 692
    :cond_5
    iget-object v0, p0, Llwf;->g:Llwl;

    if-eqz v0, :cond_6

    .line 693
    const/4 v0, 0x5

    iget-object v1, p0, Llwf;->g:Llwl;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 695
    :cond_6
    iget-object v0, p0, Llwf;->b:Llwh;

    if-eqz v0, :cond_7

    .line 696
    const/4 v0, 0x6

    iget-object v1, p0, Llwf;->b:Llwh;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 698
    :cond_7
    iget-object v0, p0, Llwf;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 700
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Llwf;->a(Loxn;)Llwf;

    move-result-object v0

    return-object v0
.end method
