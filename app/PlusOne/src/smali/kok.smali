.class public Lkok;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhlv;


# instance fields
.field private final a:I

.field private final b:F

.field private final c:Ljava/lang/String;

.field private final d:[Lqbk;


# direct methods
.method public constructor <init>(Ljava/lang/String;IF[Lqbk;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lkok;->c:Ljava/lang/String;

    .line 38
    iput p2, p0, Lkok;->a:I

    .line 39
    iput p3, p0, Lkok;->b:F

    .line 40
    iput-object p4, p0, Lkok;->d:[Lqbk;

    .line 41
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 56
    iget v0, p0, Lkok;->a:I

    return v0
.end method

.method public a(Landroid/content/Context;Lhlx;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lkok;->c:Ljava/lang/String;

    return-object v0
.end method

.method public a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 47
    const-class v0, Lhlw;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhlw;

    invoke-interface {v0, p1, p0}, Lhlw;->a(Landroid/content/Context;Lhlv;)V

    .line 48
    return-void
.end method

.method public a(Lhmp;)V
    .locals 0

    .prologue
    .line 69
    return-void
.end method

.method public b()F
    .locals 1

    .prologue
    .line 60
    iget v0, p0, Lkok;->b:F

    return v0
.end method

.method public c()[Lqbk;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lkok;->d:[Lqbk;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 73
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "RpcFailureEvent networkType=%d, networkSpeedMbps=%f, requestPath=%s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lkok;->a:I

    .line 75
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, p0, Lkok;->b:F

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lkok;->d:[Lqbk;

    aput-object v4, v2, v3

    .line 73
    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
