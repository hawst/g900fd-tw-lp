.class final Lelq;
.super Landroid/widget/Filter;
.source "PG"


# instance fields
.field final synthetic a:Lelj;


# direct methods
.method constructor <init>(Lelj;)V
    .locals 0

    .prologue
    .line 1541
    iput-object p1, p0, Lelq;->a:Lelj;

    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    return-void
.end method


# virtual methods
.method public convertResultToString(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 1593
    check-cast p1, Landroid/database/Cursor;

    .line 1594
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1595
    :cond_0
    const-string v0, ""

    .line 1612
    :goto_0
    return-object v0

    .line 1598
    :cond_1
    const-string v0, "circle_name"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 1599
    if-eq v0, v1, :cond_2

    .line 1600
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1602
    :cond_2
    const-string v0, "name"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 1603
    if-eq v0, v1, :cond_3

    .line 1604
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1606
    :cond_3
    const-string v0, "address"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 1607
    if-eq v0, v1, :cond_4

    .line 1608
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1612
    :cond_4
    const-string v0, ""

    goto :goto_0
.end method

.method protected performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .locals 3

    .prologue
    .line 1551
    iget-object v0, p0, Lelq;->a:Lelj;

    invoke-static {v0}, Lelj;->f(Lelj;)V

    .line 1553
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    .line 1554
    iget-object v1, p0, Lelq;->a:Lelj;

    invoke-static {v1, v0}, Lelj;->a(Lelj;Ljava/util/concurrent/CountDownLatch;)Ljava/util/concurrent/CountDownLatch;

    .line 1555
    iget-object v1, p0, Lelq;->a:Lelj;

    invoke-static {v1}, Lelj;->h(Lelj;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lelr;

    invoke-direct {v2, p0, p1}, Lelr;-><init>(Lelq;Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1577
    :try_start_0
    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1581
    :goto_0
    iget-object v0, p0, Lelq;->a:Lelj;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lelj;->a(Lelj;Ljava/util/concurrent/CountDownLatch;)Ljava/util/concurrent/CountDownLatch;

    .line 1583
    new-instance v0, Landroid/widget/Filter$FilterResults;

    invoke-direct {v0}, Landroid/widget/Filter$FilterResults;-><init>()V

    return-object v0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .locals 1

    .prologue
    .line 1588
    iget-object v0, p0, Lelq;->a:Lelj;

    invoke-virtual {v0}, Lelj;->getCount()I

    move-result v0

    iput v0, p2, Landroid/widget/Filter$FilterResults;->count:I

    .line 1589
    return-void
.end method
