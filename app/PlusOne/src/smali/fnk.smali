.class public final Lfnk;
.super Lkgg;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkgg",
        "<",
        "Lmam;",
        "Lman;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Z

.field private final c:Z

.field private final p:Lmol;

.field private final q:[Lmmr;

.field private final r:[Landroid/graphics/Point;

.field private s:Lman;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lkfo;Ljava/lang/String;ZZLmol;[Lmmr;[Landroid/graphics/Point;)V
    .locals 6

    .prologue
    .line 34
    const-string v3, "editstory"

    new-instance v4, Lmam;

    invoke-direct {v4}, Lmam;-><init>()V

    new-instance v5, Lman;

    invoke-direct {v5}, Lman;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lkgg;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Loxu;Loxu;)V

    .line 35
    iput-object p3, p0, Lfnk;->a:Ljava/lang/String;

    .line 36
    iput-boolean p4, p0, Lfnk;->b:Z

    .line 37
    iput-boolean p5, p0, Lfnk;->c:Z

    .line 38
    iput-object p6, p0, Lfnk;->p:Lmol;

    .line 39
    iput-object p7, p0, Lfnk;->q:[Lmmr;

    .line 40
    iput-object p8, p0, Lfnk;->r:[Landroid/graphics/Point;

    .line 41
    return-void
.end method


# virtual methods
.method protected a(Lmam;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 49
    new-instance v1, Lmlm;

    invoke-direct {v1}, Lmlm;-><init>()V

    iput-object v1, p1, Lmam;->a:Lmlm;

    .line 50
    iget-object v1, p1, Lmam;->a:Lmlm;

    new-instance v2, Lmlw;

    invoke-direct {v2}, Lmlw;-><init>()V

    iput-object v2, v1, Lmlm;->a:Lmlw;

    .line 51
    iget-object v1, p1, Lmam;->a:Lmlm;

    iget-object v1, v1, Lmlm;->a:Lmlw;

    new-instance v2, Lmlh;

    invoke-direct {v2}, Lmlh;-><init>()V

    iput-object v2, v1, Lmlw;->a:Lmlh;

    .line 52
    iget-object v1, p1, Lmam;->a:Lmlm;

    iget-object v1, v1, Lmlm;->a:Lmlw;

    iget-object v1, v1, Lmlw;->a:Lmlh;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lmlh;->b:Ljava/lang/Boolean;

    .line 53
    iget-object v1, p1, Lmam;->a:Lmlm;

    iget-object v1, v1, Lmlm;->a:Lmlw;

    iget-object v1, v1, Lmlw;->a:Lmlh;

    iget-object v2, p0, Lfnk;->r:[Landroid/graphics/Point;

    array-length v2, v2

    new-array v2, v2, [Lmli;

    iput-object v2, v1, Lmlh;->a:[Lmli;

    .line 56
    iget-object v3, p0, Lfnk;->r:[Landroid/graphics/Point;

    array-length v4, v3

    move v1, v0

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v2, v3, v0

    .line 57
    new-instance v5, Lmli;

    invoke-direct {v5}, Lmli;-><init>()V

    .line 59
    iget v6, v2, Landroid/graphics/Point;->x:I

    int-to-float v6, v6

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    iput-object v6, v5, Lmli;->b:Ljava/lang/Float;

    .line 60
    iget v2, v2, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    iput-object v2, v5, Lmli;->c:Ljava/lang/Float;

    .line 61
    iget-object v2, p0, Lfnk;->f:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    iput-object v2, v5, Lmli;->d:Ljava/lang/Float;

    .line 62
    iget-object v2, p1, Lmam;->a:Lmlm;

    iget-object v2, v2, Lmlm;->a:Lmlw;

    iget-object v2, v2, Lmlw;->a:Lmlh;

    iget-object v6, v2, Lmlh;->a:[Lmli;

    add-int/lit8 v2, v1, 0x1

    aput-object v5, v6, v1

    .line 56
    add-int/lit8 v0, v0, 0x1

    move v1, v2

    goto :goto_0

    .line 64
    :cond_0
    iget-object v0, p1, Lmam;->a:Lmlm;

    new-instance v1, Lmok;

    invoke-direct {v1}, Lmok;-><init>()V

    iput-object v1, v0, Lmlm;->b:Lmok;

    .line 65
    iget-object v0, p1, Lmam;->a:Lmlm;

    iget-object v0, v0, Lmlm;->b:Lmok;

    iget-object v1, p0, Lfnk;->a:Ljava/lang/String;

    iput-object v1, v0, Lmok;->a:Ljava/lang/String;

    .line 66
    iget-object v0, p1, Lmam;->a:Lmlm;

    iget-object v0, v0, Lmlm;->b:Lmok;

    iget-object v1, p0, Lfnk;->p:Lmol;

    iput-object v1, v0, Lmok;->b:Lmol;

    .line 67
    iget-object v0, p1, Lmam;->a:Lmlm;

    iget-object v1, p0, Lfnk;->q:[Lmmr;

    iput-object v1, v0, Lmlm;->c:[Lmmr;

    .line 68
    iget-object v0, p1, Lmam;->a:Lmlm;

    new-instance v1, Lmlx;

    invoke-direct {v1}, Lmlx;-><init>()V

    iput-object v1, v0, Lmlm;->d:Lmlx;

    .line 69
    iget-object v0, p1, Lmam;->a:Lmlm;

    iget-object v0, v0, Lmlm;->d:Lmlx;

    iget-boolean v1, p0, Lfnk;->b:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lmlx;->a:Ljava/lang/Boolean;

    .line 70
    iget-object v0, p1, Lmam;->a:Lmlm;

    iget-object v0, v0, Lmlm;->d:Lmlx;

    iget-boolean v1, p0, Lfnk;->c:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lmlx;->b:Ljava/lang/Boolean;

    .line 71
    return-void
.end method

.method protected a(Lman;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lfnk;->s:Lman;

    .line 76
    return-void
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 19
    check-cast p1, Lmam;

    invoke-virtual {p0, p1}, Lfnk;->a(Lmam;)V

    return-void
.end method

.method protected synthetic a_(Loxu;)V
    .locals 0

    .prologue
    .line 19
    check-cast p1, Lman;

    invoke-virtual {p0, p1}, Lfnk;->a(Lman;)V

    return-void
.end method

.method public i()Lman;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lfnk;->s:Lman;

    return-object v0
.end method
