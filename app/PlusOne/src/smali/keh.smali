.class public final Lkeh;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "DefaultLocale"
    }
.end annotation


# static fields
.field private static final a:I


# instance fields
.field private final b:Lked;

.field private final c:Lkek;

.field private final d:Lkeo;

.field private final e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 76
    const-wide/high16 v0, 0x4030000000000000L    # 16.0

    .line 77
    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lkeh;->a:I

    .line 76
    return-void
.end method

.method public constructor <init>(I)V
    .locals 2

    .prologue
    .line 120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    new-instance v0, Lkek;

    invoke-direct {v0}, Lkek;-><init>()V

    iput-object v0, p0, Lkeh;->c:Lkek;

    .line 86
    new-instance v0, Lkeo;

    invoke-direct {v0}, Lkeo;-><init>()V

    iput-object v0, p0, Lkeh;->d:Lkeo;

    .line 121
    iput p1, p0, Lkeh;->e:I

    .line 122
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    .line 123
    new-instance v0, Lkem;

    invoke-direct {v0}, Lkem;-><init>()V

    iput-object v0, p0, Lkeh;->b:Lked;

    .line 127
    :goto_0
    return-void

    .line 125
    :cond_0
    new-instance v0, Lkei;

    invoke-direct {v0}, Lkei;-><init>()V

    iput-object v0, p0, Lkeh;->b:Lked;

    goto :goto_0
.end method

.method private declared-synchronized a(IILkeg;)Landroid/graphics/Bitmap;
    .locals 8

    .prologue
    .line 202
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lkeh;->b:Lked;

    invoke-interface {v0, p1, p2}, Lked;->a(II)Lkee;

    move-result-object v2

    .line 204
    iget-object v0, p0, Lkeh;->c:Lkek;

    invoke-virtual {v0, v2}, Lkek;->a(Lkee;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 206
    if-nez v0, :cond_3

    .line 208
    iget-object v1, p0, Lkeh;->b:Lked;

    sget v3, Lkeh;->a:I

    mul-int/2addr v3, p1

    sget v4, Lkeh;->a:I

    mul-int/2addr v4, p2

    invoke-interface {v1, v3, v4}, Lked;->a(II)Lkee;

    move-result-object v1

    .line 214
    iget-object v3, p0, Lkeh;->d:Lkeo;

    invoke-virtual {v3, v2, v1}, Lkeo;->a(Lkee;Lkee;)Ljava/util/SortedSet;

    move-result-object v1

    .line 215
    iget-object v3, p0, Lkeh;->b:Lked;

    invoke-interface {v3, v2, v1, p3}, Lked;->a(Lkee;Ljava/util/SortedSet;Lkeg;)Lkee;

    move-result-object v1

    .line 216
    if-eqz v1, :cond_0

    .line 217
    iget-object v0, p0, Lkeh;->c:Lkek;

    invoke-virtual {v0, v1}, Lkek;->a(Lkee;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 221
    :cond_0
    :goto_0
    if-eqz v0, :cond_2

    .line 222
    iget-object v3, p0, Lkeh;->b:Lked;

    invoke-interface {v3, v2, v0}, Lked;->a(Lkee;Landroid/graphics/Bitmap;)V

    .line 224
    iget v3, p0, Lkeh;->f:I

    int-to-long v4, v3

    iget-wide v6, v1, Lkee;->c:J

    sub-long/2addr v4, v6

    long-to-int v3, v4

    iput v3, p0, Lkeh;->f:I

    .line 225
    iget-object v3, p0, Lkeh;->d:Lkeo;

    invoke-virtual {v3, v1}, Lkeo;->b(Lkee;)V

    .line 227
    iget v3, p0, Lkeh;->g:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lkeh;->g:I

    .line 228
    const-string v3, "BitmapPoolLru"

    const/4 v4, 0x2

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 229
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x12

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Got bitmap: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " for: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 237
    :cond_1
    :goto_1
    monitor-exit p0

    return-object v0

    .line 232
    :cond_2
    :try_start_1
    iget v1, p0, Lkeh;->h:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lkeh;->h:I

    .line 233
    const-string v1, "BitmapPoolLru"

    const/4 v3, 0x3

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 234
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1d

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Missing bitmap: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " match type: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 202
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    move-object v1, v2

    goto/16 :goto_0
.end method

.method private declared-synchronized a(I)V
    .locals 6

    .prologue
    .line 271
    monitor-enter p0

    if-gez p1, :cond_1

    .line 272
    :try_start_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Target byte size must be >= 0, got: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 273
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 271
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 286
    :cond_0
    :try_start_1
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 288
    iget v0, p0, Lkeh;->i:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkeh;->i:I

    .line 289
    const-string v0, "BitmapPoolLru"

    const/4 v2, 0x2

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 290
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x10

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Evicted bitmap: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 275
    :cond_1
    iget v0, p0, Lkeh;->f:I

    if-le v0, p1, :cond_2

    .line 276
    iget-object v0, p0, Lkeh;->c:Lkek;

    invoke-virtual {v0}, Lkek;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 277
    iget-object v1, p0, Lkeh;->b:Lked;

    invoke-interface {v1, v0}, Lked;->a(Landroid/graphics/Bitmap;)Lkee;

    move-result-object v1

    .line 279
    iget-object v2, p0, Lkeh;->d:Lkeo;

    invoke-virtual {v2, v1}, Lkeo;->b(Lkee;)V

    .line 280
    iget v2, p0, Lkeh;->f:I

    int-to-long v2, v2

    iget-wide v4, v1, Lkee;->c:J

    sub-long/2addr v2, v4

    long-to-int v2, v2

    iput v2, p0, Lkeh;->f:I

    .line 282
    if-nez v0, :cond_0

    .line 283
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Pool is larger than its max size, but has no more bitmaps to evict."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 293
    :cond_2
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public a(II)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 176
    sget-object v0, Lkeg;->a:Lkeg;

    invoke-direct {p0, p1, p2, v0}, Lkeh;->a(IILkeg;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized a()V
    .locals 2

    .prologue
    .line 242
    monitor-enter p0

    :try_start_0
    const-string v0, "BitmapPoolLru"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 243
    invoke-virtual {p0}, Lkeh;->toString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 245
    :cond_0
    monitor-exit p0

    return-void

    .line 242
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Landroid/graphics/Bitmap;)V
    .locals 6

    .prologue
    .line 150
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lkeh;->b:Lked;

    invoke-interface {v0, p1}, Lked;->a(Landroid/graphics/Bitmap;)Lkee;

    move-result-object v0

    .line 151
    iget-wide v2, v0, Lkee;->c:J

    iget v1, p0, Lkeh;->e:I

    int-to-long v4, v1

    cmp-long v1, v2, v4

    if-gtz v1, :cond_0

    .line 152
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    if-ne v1, v2, :cond_0

    .line 153
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isMutable()Z

    move-result v1

    if-nez v1, :cond_1

    .line 154
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 168
    :goto_0
    monitor-exit p0

    return-void

    .line 158
    :cond_1
    :try_start_1
    iget-object v1, p0, Lkeh;->c:Lkek;

    invoke-virtual {v1, v0, p1}, Lkek;->a(Lkee;Landroid/graphics/Bitmap;)V

    .line 159
    iget-object v1, p0, Lkeh;->d:Lkeo;

    invoke-virtual {v1, v0}, Lkeo;->a(Lkee;)V

    .line 161
    const-string v1, "BitmapPoolLru"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 162
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x18

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Putting bitmap in pool: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 164
    :cond_2
    iget v1, p0, Lkeh;->f:I

    int-to-long v2, v1

    iget-wide v0, v0, Lkee;->c:J

    add-long/2addr v0, v2

    long-to-int v0, v0

    iput v0, p0, Lkeh;->f:I

    .line 165
    iget v0, p0, Lkeh;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkeh;->j:I

    .line 167
    iget v0, p0, Lkeh;->e:I

    invoke-direct {p0, v0}, Lkeh;->a(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 150
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(II)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 185
    sget-object v0, Lkeg;->b:Lkeg;

    invoke-direct {p0, p1, p2, v0}, Lkeh;->a(IILkeg;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized b()V
    .locals 1

    .prologue
    .line 256
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0, v0}, Lkeh;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 257
    monitor-exit p0

    return-void

    .line 256
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c()F
    .locals 2

    .prologue
    .line 261
    iget v0, p0, Lkeh;->f:I

    int-to-float v0, v0

    iget v1, p0, Lkeh;->e:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 249
    const-string v0, "BitmapPoolLru { Size: %d, Hits: %d, Misses: %d, Puts: %d, Evictions: %d\nMap: %s\nSizes: %s }"

    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, Lkeh;->f:I

    .line 250
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lkeh;->g:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget v3, p0, Lkeh;->h:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget v3, p0, Lkeh;->j:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget v3, p0, Lkeh;->i:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget-object v3, p0, Lkeh;->c:Lkek;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    iget-object v3, p0, Lkeh;->d:Lkeo;

    aput-object v3, v1, v2

    .line 249
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
