.class public Llrl;
.super Los;
.source "PG"


# instance fields
.field private e:I

.field public final y:Llqc;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Los;-><init>()V

    .line 20
    new-instance v0, Llqc;

    invoke-direct {v0}, Llqc;-><init>()V

    iput-object v0, p0, Llrl;->y:Llqc;

    return-void
.end method

.method private c(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 155
    iget v0, p0, Llrl;->e:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Llrl;->e:I

    if-nez v0, :cond_0

    .line 156
    iget-object v0, p0, Llrl;->y:Llqc;

    invoke-virtual {v0, p1}, Llqc;->b(Landroid/content/Intent;)V

    .line 158
    :cond_0
    return-void
.end method

.method private l()V
    .locals 1

    .prologue
    .line 161
    iget v0, p0, Llrl;->e:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Llrl;->e:I

    .line 162
    return-void
.end method


# virtual methods
.method public O()Llqr;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Llrl;->y:Llqc;

    return-object v0
.end method

.method public a(Lu;Landroid/content/Intent;I)V
    .locals 0

    .prologue
    .line 123
    invoke-direct {p0, p2}, Llrl;->c(Landroid/content/Intent;)V

    .line 124
    invoke-super {p0, p1, p2, p3}, Los;->a(Lu;Landroid/content/Intent;I)V

    .line 125
    invoke-direct {p0}, Llrl;->l()V

    .line 126
    return-void
.end method

.method public a(Lxn;)V
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Llrl;->y:Llqc;

    invoke-virtual {v0}, Llqc;->f()V

    .line 185
    invoke-super {p0, p1}, Los;->a(Lxn;)V

    .line 186
    return-void
.end method

.method public b(Lxn;)V
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Llrl;->y:Llqc;

    invoke-virtual {v0}, Llqc;->g()V

    .line 191
    invoke-super {p0, p1}, Los;->b(Lxn;)V

    .line 192
    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Llrl;->y:Llqc;

    invoke-virtual {v0, p1}, Llqc;->a(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 197
    const/4 v0, 0x1

    .line 199
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Los;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public finish()V
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Llrl;->y:Llqc;

    invoke-virtual {v0}, Llqc;->e()V

    .line 179
    invoke-super {p0}, Los;->finish()V

    .line 180
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Llrl;->y:Llqc;

    invoke-virtual {v0, p1, p2, p3}, Llqc;->a(IILandroid/content/Intent;)V

    .line 98
    invoke-super {p0, p1, p2, p3}, Los;->onActivityResult(IILandroid/content/Intent;)V

    .line 99
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Llrl;->y:Llqc;

    invoke-virtual {v0}, Llqc;->i()Z

    move-result v0

    if-nez v0, :cond_0

    .line 229
    invoke-super {p0}, Los;->onBackPressed()V

    .line 231
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Llrl;->y:Llqc;

    invoke-virtual {v0, p1}, Llqc;->c(Landroid/os/Bundle;)V

    .line 30
    invoke-super {p0, p1}, Los;->onCreate(Landroid/os/Bundle;)V

    .line 31
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Llrl;->y:Llqc;

    invoke-virtual {v0, p1}, Llqc;->a(Landroid/view/Menu;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 205
    const/4 v0, 0x1

    .line 207
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Los;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Llrl;->y:Llqc;

    invoke-virtual {v0}, Llqc;->c()V

    .line 86
    invoke-super {p0}, Los;->onDestroy()V

    .line 87
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Llrl;->y:Llqc;

    invoke-virtual {v0, p1}, Llqc;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 236
    const/4 v0, 0x1

    .line 238
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Los;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Llrl;->y:Llqc;

    invoke-virtual {v0, p1}, Llqc;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 244
    const/4 v0, 0x1

    .line 246
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Los;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onLowMemory()V
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Llrl;->y:Llqc;

    invoke-virtual {v0}, Llqc;->m()V

    .line 104
    invoke-super {p0}, Los;->onLowMemory()V

    .line 105
    return-void
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Llrl;->y:Llqc;

    invoke-virtual {v0, p1}, Llqc;->a(Landroid/content/Intent;)V

    .line 167
    invoke-super {p0, p1}, Los;->onNewIntent(Landroid/content/Intent;)V

    .line 168
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Llrl;->y:Llqc;

    invoke-virtual {v0, p1}, Llqc;->a(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 221
    const/4 v0, 0x1

    .line 223
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Los;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Llrl;->y:Llqc;

    invoke-virtual {v0}, Llqc;->b()V

    .line 68
    invoke-super {p0}, Los;->onPause()V

    .line 69
    return-void
.end method

.method public onPostCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Llrl;->y:Llqc;

    invoke-virtual {v0, p1}, Llqc;->a(Landroid/os/Bundle;)V

    .line 36
    invoke-super {p0, p1}, Los;->onPostCreate(Landroid/os/Bundle;)V

    .line 37
    return-void
.end method

.method protected onPostResume()V
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Llrl;->y:Llqc;

    invoke-virtual {v0}, Llqc;->a()V

    .line 62
    invoke-super {p0}, Los;->onPostResume()V

    .line 63
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Llrl;->y:Llqc;

    invoke-virtual {v0}, Llqc;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 213
    const/4 v0, 0x1

    .line 215
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Los;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    goto :goto_0
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Llrl;->y:Llqc;

    invoke-virtual {v0, p1}, Llqc;->b(Landroid/os/Bundle;)V

    .line 49
    invoke-super {p0, p1}, Los;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 50
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 54
    invoke-virtual {p0}, Llrl;->f()Lae;

    move-result-object v0

    invoke-static {v0}, Llrp;->a(Lae;)V

    .line 55
    iget-object v0, p0, Llrl;->y:Llqc;

    invoke-virtual {v0}, Llqc;->k()V

    .line 56
    invoke-super {p0}, Los;->onResume()V

    .line 57
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Llrl;->y:Llqc;

    invoke-virtual {v0, p1}, Llqc;->d(Landroid/os/Bundle;)V

    .line 80
    invoke-super {p0, p1}, Los;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 81
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p0}, Llrl;->f()Lae;

    move-result-object v0

    invoke-static {v0}, Llrp;->a(Lae;)V

    .line 42
    iget-object v0, p0, Llrl;->y:Llqc;

    invoke-virtual {v0}, Llqc;->j()V

    .line 43
    invoke-super {p0}, Los;->onStart()V

    .line 44
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Llrl;->y:Llqc;

    invoke-virtual {v0}, Llqc;->l()V

    .line 74
    invoke-super {p0}, Los;->onStop()V

    .line 75
    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Llrl;->y:Llqc;

    invoke-virtual {v0}, Llqc;->d()V

    .line 173
    invoke-super {p0}, Los;->onUserLeaveHint()V

    .line 174
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Llrl;->y:Llqc;

    invoke-virtual {v0, p1}, Llqc;->a(Z)V

    .line 92
    invoke-super {p0, p1}, Los;->onWindowFocusChanged(Z)V

    .line 93
    return-void
.end method

.method public startActivity(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 109
    invoke-direct {p0, p1}, Llrl;->c(Landroid/content/Intent;)V

    .line 110
    invoke-super {p0, p1}, Los;->startActivity(Landroid/content/Intent;)V

    .line 111
    invoke-direct {p0}, Llrl;->l()V

    .line 112
    return-void
.end method

.method public startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 116
    invoke-direct {p0, p1}, Llrl;->c(Landroid/content/Intent;)V

    .line 117
    invoke-super {p0, p1, p2}, Los;->startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V

    .line 118
    invoke-direct {p0}, Llrl;->l()V

    .line 119
    return-void
.end method

.method public startActivityForResult(Landroid/content/Intent;I)V
    .locals 0

    .prologue
    .line 138
    invoke-direct {p0, p1}, Llrl;->c(Landroid/content/Intent;)V

    .line 139
    invoke-super {p0, p1, p2}, Los;->startActivityForResult(Landroid/content/Intent;I)V

    .line 140
    invoke-direct {p0}, Llrl;->l()V

    .line 141
    return-void
.end method

.method public startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 145
    invoke-direct {p0, p1}, Llrl;->c(Landroid/content/Intent;)V

    .line 146
    invoke-super {p0, p1, p2, p3}, Los;->startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V

    .line 147
    invoke-direct {p0}, Llrl;->l()V

    .line 148
    return-void
.end method

.method public startActivityFromFragment(Landroid/app/Fragment;Landroid/content/Intent;ILandroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 131
    invoke-direct {p0, p2}, Llrl;->c(Landroid/content/Intent;)V

    .line 132
    invoke-super {p0, p1, p2, p3, p4}, Los;->startActivityFromFragment(Landroid/app/Fragment;Landroid/content/Intent;ILandroid/os/Bundle;)V

    .line 133
    invoke-direct {p0}, Llrl;->l()V

    .line 134
    return-void
.end method
