.class public final Lmab;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnwm;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 6316
    invoke-direct {p0}, Loxq;-><init>()V

    .line 6319
    iput-object v0, p0, Lmab;->apiHeader:Llyr;

    .line 6322
    iput-object v0, p0, Lmab;->a:Lnwm;

    .line 6316
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 6339
    const/4 v0, 0x0

    .line 6340
    iget-object v1, p0, Lmab;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 6341
    const/4 v0, 0x1

    iget-object v1, p0, Lmab;->apiHeader:Llyr;

    .line 6342
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 6344
    :cond_0
    iget-object v1, p0, Lmab;->a:Lnwm;

    if-eqz v1, :cond_1

    .line 6345
    const/4 v1, 0x2

    iget-object v2, p0, Lmab;->a:Lnwm;

    .line 6346
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6348
    :cond_1
    iget-object v1, p0, Lmab;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6349
    iput v0, p0, Lmab;->ai:I

    .line 6350
    return v0
.end method

.method public a(Loxn;)Lmab;
    .locals 2

    .prologue
    .line 6358
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 6359
    sparse-switch v0, :sswitch_data_0

    .line 6363
    iget-object v1, p0, Lmab;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 6364
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmab;->ah:Ljava/util/List;

    .line 6367
    :cond_1
    iget-object v1, p0, Lmab;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6369
    :sswitch_0
    return-object p0

    .line 6374
    :sswitch_1
    iget-object v0, p0, Lmab;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 6375
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmab;->apiHeader:Llyr;

    .line 6377
    :cond_2
    iget-object v0, p0, Lmab;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 6381
    :sswitch_2
    iget-object v0, p0, Lmab;->a:Lnwm;

    if-nez v0, :cond_3

    .line 6382
    new-instance v0, Lnwm;

    invoke-direct {v0}, Lnwm;-><init>()V

    iput-object v0, p0, Lmab;->a:Lnwm;

    .line 6384
    :cond_3
    iget-object v0, p0, Lmab;->a:Lnwm;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 6359
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 6327
    iget-object v0, p0, Lmab;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 6328
    const/4 v0, 0x1

    iget-object v1, p0, Lmab;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 6330
    :cond_0
    iget-object v0, p0, Lmab;->a:Lnwm;

    if-eqz v0, :cond_1

    .line 6331
    const/4 v0, 0x2

    iget-object v1, p0, Lmab;->a:Lnwm;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 6333
    :cond_1
    iget-object v0, p0, Lmab;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 6335
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6312
    invoke-virtual {p0, p1}, Lmab;->a(Loxn;)Lmab;

    move-result-object v0

    return-object v0
.end method
