.class public final Lnie;
.super Loxq;
.source "PG"


# instance fields
.field public a:I

.field public b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 6688
    invoke-direct {p0}, Loxq;-><init>()V

    .line 6698
    const/high16 v0, -0x80000000

    iput v0, p0, Lnie;->a:I

    .line 6688
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 6717
    const/4 v0, 0x0

    .line 6718
    iget v1, p0, Lnie;->a:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 6719
    const/4 v0, 0x1

    iget v1, p0, Lnie;->a:I

    .line 6720
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 6722
    :cond_0
    iget-object v1, p0, Lnie;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 6723
    const/4 v1, 0x2

    iget-object v2, p0, Lnie;->b:Ljava/lang/String;

    .line 6724
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6726
    :cond_1
    iget-object v1, p0, Lnie;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6727
    iput v0, p0, Lnie;->ai:I

    .line 6728
    return v0
.end method

.method public a(Loxn;)Lnie;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 6736
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 6737
    sparse-switch v0, :sswitch_data_0

    .line 6741
    iget-object v1, p0, Lnie;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 6742
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnie;->ah:Ljava/util/List;

    .line 6745
    :cond_1
    iget-object v1, p0, Lnie;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6747
    :sswitch_0
    return-object p0

    .line 6752
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 6753
    if-eq v0, v2, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    .line 6757
    :cond_2
    iput v0, p0, Lnie;->a:I

    goto :goto_0

    .line 6759
    :cond_3
    iput v2, p0, Lnie;->a:I

    goto :goto_0

    .line 6764
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnie;->b:Ljava/lang/String;

    goto :goto_0

    .line 6737
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 6705
    iget v0, p0, Lnie;->a:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 6706
    const/4 v0, 0x1

    iget v1, p0, Lnie;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 6708
    :cond_0
    iget-object v0, p0, Lnie;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 6709
    const/4 v0, 0x2

    iget-object v1, p0, Lnie;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 6711
    :cond_1
    iget-object v0, p0, Lnie;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 6713
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6684
    invoke-virtual {p0, p1}, Lnie;->a(Loxn;)Lnie;

    move-result-object v0

    return-object v0
.end method
