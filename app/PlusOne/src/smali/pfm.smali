.class public final Lpfm;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Long;

.field public b:[Llub;

.field public c:[B

.field public d:[B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 191
    const v0, 0x4902a31

    new-instance v1, Lpfn;

    invoke-direct {v1}, Lpfn;-><init>()V

    .line 196
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    .line 195
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 192
    invoke-direct {p0}, Loxq;-><init>()V

    .line 201
    sget-object v0, Llub;->a:[Llub;

    iput-object v0, p0, Lpfm;->b:[Llub;

    .line 192
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 232
    .line 233
    iget-object v0, p0, Lpfm;->a:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 234
    const/4 v0, 0x1

    iget-object v2, p0, Lpfm;->a:Ljava/lang/Long;

    .line 235
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Loxo;->f(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 237
    :goto_0
    iget-object v2, p0, Lpfm;->b:[Llub;

    if-eqz v2, :cond_1

    .line 238
    iget-object v2, p0, Lpfm;->b:[Llub;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 239
    if-eqz v4, :cond_0

    .line 240
    const/4 v5, 0x2

    .line 241
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 238
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 245
    :cond_1
    iget-object v1, p0, Lpfm;->c:[B

    if-eqz v1, :cond_2

    .line 246
    const/4 v1, 0x3

    iget-object v2, p0, Lpfm;->c:[B

    .line 247
    invoke-static {v1, v2}, Loxo;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 249
    :cond_2
    iget-object v1, p0, Lpfm;->d:[B

    if-eqz v1, :cond_3

    .line 250
    const/4 v1, 0x4

    iget-object v2, p0, Lpfm;->d:[B

    .line 251
    invoke-static {v1, v2}, Loxo;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 253
    :cond_3
    iget-object v1, p0, Lpfm;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 254
    iput v0, p0, Lpfm;->ai:I

    .line 255
    return v0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lpfm;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 263
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 264
    sparse-switch v0, :sswitch_data_0

    .line 268
    iget-object v2, p0, Lpfm;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 269
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lpfm;->ah:Ljava/util/List;

    .line 272
    :cond_1
    iget-object v2, p0, Lpfm;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 274
    :sswitch_0
    return-object p0

    .line 279
    :sswitch_1
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lpfm;->a:Ljava/lang/Long;

    goto :goto_0

    .line 283
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 284
    iget-object v0, p0, Lpfm;->b:[Llub;

    if-nez v0, :cond_3

    move v0, v1

    .line 285
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Llub;

    .line 286
    iget-object v3, p0, Lpfm;->b:[Llub;

    if-eqz v3, :cond_2

    .line 287
    iget-object v3, p0, Lpfm;->b:[Llub;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 289
    :cond_2
    iput-object v2, p0, Lpfm;->b:[Llub;

    .line 290
    :goto_2
    iget-object v2, p0, Lpfm;->b:[Llub;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 291
    iget-object v2, p0, Lpfm;->b:[Llub;

    new-instance v3, Llub;

    invoke-direct {v3}, Llub;-><init>()V

    aput-object v3, v2, v0

    .line 292
    iget-object v2, p0, Lpfm;->b:[Llub;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 293
    invoke-virtual {p1}, Loxn;->a()I

    .line 290
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 284
    :cond_3
    iget-object v0, p0, Lpfm;->b:[Llub;

    array-length v0, v0

    goto :goto_1

    .line 296
    :cond_4
    iget-object v2, p0, Lpfm;->b:[Llub;

    new-instance v3, Llub;

    invoke-direct {v3}, Llub;-><init>()V

    aput-object v3, v2, v0

    .line 297
    iget-object v2, p0, Lpfm;->b:[Llub;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 301
    :sswitch_3
    invoke-virtual {p1}, Loxn;->l()[B

    move-result-object v0

    iput-object v0, p0, Lpfm;->c:[B

    goto :goto_0

    .line 305
    :sswitch_4
    invoke-virtual {p1}, Loxn;->l()[B

    move-result-object v0

    iput-object v0, p0, Lpfm;->d:[B

    goto :goto_0

    .line 264
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 210
    iget-object v0, p0, Lpfm;->a:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 211
    const/4 v0, 0x1

    iget-object v1, p0, Lpfm;->a:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 213
    :cond_0
    iget-object v0, p0, Lpfm;->b:[Llub;

    if-eqz v0, :cond_2

    .line 214
    iget-object v1, p0, Lpfm;->b:[Llub;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 215
    if-eqz v3, :cond_1

    .line 216
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 214
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 220
    :cond_2
    iget-object v0, p0, Lpfm;->c:[B

    if-eqz v0, :cond_3

    .line 221
    const/4 v0, 0x3

    iget-object v1, p0, Lpfm;->c:[B

    invoke-virtual {p1, v0, v1}, Loxo;->a(I[B)V

    .line 223
    :cond_3
    iget-object v0, p0, Lpfm;->d:[B

    if-eqz v0, :cond_4

    .line 224
    const/4 v0, 0x4

    iget-object v1, p0, Lpfm;->d:[B

    invoke-virtual {p1, v0, v1}, Loxo;->a(I[B)V

    .line 226
    :cond_4
    iget-object v0, p0, Lpfm;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 228
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 188
    invoke-virtual {p0, p1}, Lpfm;->a(Loxn;)Lpfm;

    move-result-object v0

    return-object v0
.end method
