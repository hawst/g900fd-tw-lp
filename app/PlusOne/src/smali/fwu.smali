.class public final Lfwu;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llip;


# static fields
.field private static b:Z

.field private static c:I

.field private static d:I

.field private static e:I


# instance fields
.field private f:Z

.field private g:Landroid/graphics/Bitmap;

.field private h:Landroid/graphics/drawable/NinePatchDrawable;

.field private i:Landroid/graphics/drawable/NinePatchDrawable;

.field private j:Lfwv;

.field private k:I

.field private l:Landroid/text/StaticLayout;

.field private m:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/graphics/Bitmap;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lfwv;IILjava/lang/CharSequence;I)V
    .locals 13

    .prologue
    .line 198
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v11, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move/from16 v8, p6

    move/from16 v9, p7

    move-object/from16 v10, p8

    move/from16 v12, p9

    invoke-direct/range {v0 .. v12}, Lfwu;-><init>(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lfwv;IILjava/lang/CharSequence;ZI)V

    .line 201
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/graphics/Bitmap;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lfwv;IILjava/lang/CharSequence;Z)V
    .locals 15

    .prologue
    .line 331
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v11, 0x0

    const/4 v12, -0x1

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move/from16 v8, p6

    move/from16 v9, p7

    move/from16 v10, p9

    invoke-direct/range {v0 .. v14}, Lfwu;-><init>(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lfwv;IIZZIII)V

    .line 336
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lfwv;II)V
    .locals 13

    .prologue
    .line 238
    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    move-object/from16 v10, p3

    invoke-direct/range {v0 .. v12}, Lfwu;-><init>(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lfwv;IILjava/lang/CharSequence;ZI)V

    .line 240
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lfwv;IILjava/lang/CharSequence;ZI)V
    .locals 15

    .prologue
    .line 309
    const/4 v10, 0x1

    const/4 v12, -0x1

    const/4 v14, 0x0

    move-object v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    move/from16 v11, p11

    move/from16 v13, p12

    invoke-direct/range {v0 .. v14}, Lfwu;-><init>(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lfwv;IIZZIII)V

    .line 313
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lfwv;IILjava/lang/CharSequence;ZII)V
    .locals 15

    .prologue
    .line 284
    const/4 v10, 0x1

    const/4 v14, 0x0

    move-object v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    move/from16 v11, p11

    move/from16 v12, p12

    move/from16 v13, p13

    invoke-direct/range {v0 .. v14}, Lfwu;-><init>(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lfwv;IIZZIII)V

    .line 287
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lfwv;IIZZIII)V
    .locals 11

    .prologue
    .line 363
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 364
    sget-boolean v2, Lfwu;->b:Z

    if-nez v2, :cond_0

    const/4 v2, 0x1

    sput-boolean v2, Lfwu;->b:Z

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0288

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    sput v3, Lfwu;->c:I

    const v3, 0x7f0d0289

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    sput v3, Lfwu;->d:I

    const v3, 0x7f0d028a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    sput v2, Lfwu;->e:I

    .line 366
    :cond_0
    if-ltz p12, :cond_1

    :goto_0
    move/from16 v0, p12

    iput v0, p0, Lfwu;->k:I

    .line 369
    iput-object p2, p0, Lfwu;->g:Landroid/graphics/Bitmap;

    .line 371
    move-object/from16 v0, p5

    iput-object v0, p0, Lfwu;->h:Landroid/graphics/drawable/NinePatchDrawable;

    .line 372
    move-object/from16 v0, p6

    iput-object v0, p0, Lfwu;->i:Landroid/graphics/drawable/NinePatchDrawable;

    .line 373
    move-object/from16 v0, p7

    iput-object v0, p0, Lfwu;->j:Lfwv;

    .line 374
    if-eqz p5, :cond_2

    invoke-virtual/range {p5 .. p5}, Landroid/graphics/drawable/NinePatchDrawable;->getMinimumWidth()I

    move-result v2

    move v9, v2

    .line 377
    :goto_1
    if-eqz p5, :cond_3

    invoke-virtual/range {p5 .. p5}, Landroid/graphics/drawable/NinePatchDrawable;->getMinimumHeight()I

    move-result v2

    .line 378
    :goto_2
    iget-object v3, p0, Lfwu;->g:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_4

    if-eqz p3, :cond_4

    iget v3, p0, Lfwu;->k:I

    move v8, v3

    .line 381
    :goto_3
    if-nez p3, :cond_5

    .line 382
    const/4 v3, 0x0

    move v6, v3

    move v7, v3

    .line 391
    :goto_4
    if-nez p2, :cond_8

    const/4 v3, 0x0

    move v5, v3

    .line 392
    :goto_5
    if-nez p2, :cond_9

    const/4 v3, 0x0

    move v4, v3

    .line 393
    :goto_6
    if-eqz p10, :cond_b

    if-eqz p11, :cond_a

    sget v3, Lfwu;->d:I

    :goto_7
    mul-int/lit8 v3, v3, 0x2

    .line 395
    :goto_8
    new-instance v10, Landroid/graphics/Rect;

    add-int/2addr v5, v7

    add-int/2addr v5, v8

    invoke-static {v9, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    add-int v5, v5, p8

    add-int/2addr v3, v5

    .line 396
    invoke-static {v6, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    invoke-static {v2, v4}, Ljava/lang/Math;->max(II)I

    move-result v2

    add-int v2, v2, p9

    move/from16 v0, p8

    move/from16 v1, p9

    invoke-direct {v10, v0, v1, v3, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v10, p0, Lfwu;->m:Landroid/graphics/Rect;

    .line 397
    return-void

    .line 366
    :cond_1
    sget p12, Lfwu;->e:I

    goto :goto_0

    .line 374
    :cond_2
    const/4 v2, 0x0

    move v9, v2

    goto :goto_1

    .line 377
    :cond_3
    const/4 v2, 0x0

    goto :goto_2

    .line 378
    :cond_4
    const/4 v3, 0x0

    move v8, v3

    goto :goto_3

    .line 384
    :cond_5
    invoke-static {p4, p3}, Llib;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;)I

    move-result v4

    .line 385
    if-nez p13, :cond_7

    .line 386
    :goto_9
    if-gtz p14, :cond_6

    const p14, 0x7fffffff

    :cond_6
    move/from16 v0, p14

    invoke-static {p4, p3, v4, v0}, Llib;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v3

    iput-object v3, p0, Lfwu;->l:Landroid/text/StaticLayout;

    .line 388
    iget-object v3, p0, Lfwu;->l:Landroid/text/StaticLayout;

    invoke-virtual {v3}, Landroid/text/StaticLayout;->getHeight()I

    move-result v3

    move v6, v3

    move v7, v4

    goto :goto_4

    .line 385
    :cond_7
    move/from16 v0, p13

    invoke-static {v0, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    goto :goto_9

    .line 391
    :cond_8
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    move v5, v3

    goto :goto_5

    .line 392
    :cond_9
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    move v4, v3

    goto :goto_6

    .line 393
    :cond_a
    sget v3, Lfwu;->c:I

    goto :goto_7

    :cond_b
    const/4 v3, 0x0

    goto :goto_8
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lfwv;II)V
    .locals 13

    .prologue
    .line 218
    const/4 v2, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    move-object v10, p2

    invoke-direct/range {v0 .. v12}, Lfwu;-><init>(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lfwv;IILjava/lang/CharSequence;ZI)V

    .line 220
    return-void
.end method


# virtual methods
.method public a(Llip;Llip;)I
    .locals 1

    .prologue
    .line 603
    sget-object v0, Llip;->a_:Lliq;

    invoke-virtual {v0, p1, p2}, Lliq;->a(Llip;Llip;)I

    move-result v0

    return v0
.end method

.method public a()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 485
    iget-object v0, p0, Lfwu;->m:Landroid/graphics/Rect;

    return-object v0
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 502
    iget-object v0, p0, Lfwu;->m:Landroid/graphics/Rect;

    if-eqz v0, :cond_0

    .line 503
    iget-object v0, p0, Lfwu;->m:Landroid/graphics/Rect;

    iget-object v1, p0, Lfwu;->m:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    add-int/2addr v1, p1

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 505
    :cond_0
    return-void
.end method

.method public a(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 453
    iget-boolean v0, p0, Lfwu;->f:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lfwu;->i:Landroid/graphics/drawable/NinePatchDrawable;

    .line 454
    :goto_0
    if-eqz v0, :cond_0

    .line 455
    iget-object v2, p0, Lfwu;->m:Landroid/graphics/Rect;

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 456
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 459
    :cond_0
    iget-object v0, p0, Lfwu;->g:Landroid/graphics/Bitmap;

    if-nez v0, :cond_4

    move v0, v1

    .line 460
    :goto_1
    iget-object v2, p0, Lfwu;->g:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lfwu;->l:Landroid/text/StaticLayout;

    if-nez v2, :cond_5

    :cond_1
    move v2, v1

    .line 462
    :goto_2
    iget-object v3, p0, Lfwu;->l:Landroid/text/StaticLayout;

    if-nez v3, :cond_6

    .line 464
    :goto_3
    iget-object v3, p0, Lfwu;->m:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget-object v4, p0, Lfwu;->m:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    sub-int/2addr v4, v0

    sub-int/2addr v4, v2

    sub-int v1, v4, v1

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v3

    .line 466
    iget-object v3, p0, Lfwu;->g:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_7

    .line 467
    iget-object v3, p0, Lfwu;->m:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget-object v4, p0, Lfwu;->m:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    iget-object v5, p0, Lfwu;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    sub-int/2addr v4, v5

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    .line 468
    iget-object v4, p0, Lfwu;->g:Landroid/graphics/Bitmap;

    int-to-float v5, v1

    int-to-float v3, v3

    const/4 v6, 0x0

    invoke-virtual {p1, v4, v5, v3, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 469
    add-int/2addr v0, v2

    add-int/2addr v0, v1

    .line 472
    :goto_4
    iget-object v1, p0, Lfwu;->l:Landroid/text/StaticLayout;

    if-eqz v1, :cond_2

    .line 473
    iget-object v1, p0, Lfwu;->m:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, Lfwu;->m:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    iget-object v3, p0, Lfwu;->l:Landroid/text/StaticLayout;

    invoke-virtual {v3}, Landroid/text/StaticLayout;->getHeight()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    .line 474
    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 475
    iget-object v2, p0, Lfwu;->l:Landroid/text/StaticLayout;

    invoke-virtual {v2, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 476
    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 478
    :cond_2
    return-void

    .line 453
    :cond_3
    iget-object v0, p0, Lfwu;->h:Landroid/graphics/drawable/NinePatchDrawable;

    goto :goto_0

    .line 459
    :cond_4
    iget-object v0, p0, Lfwu;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    goto :goto_1

    .line 460
    :cond_5
    iget v2, p0, Lfwu;->k:I

    goto :goto_2

    .line 462
    :cond_6
    iget-object v1, p0, Lfwu;->l:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getWidth()I

    move-result v1

    goto :goto_3

    :cond_7
    move v0, v1

    goto :goto_4
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 588
    iput-boolean p1, p0, Lfwu;->f:Z

    .line 589
    return-void
.end method

.method public a(III)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 523
    iget-object v2, p0, Lfwu;->j:Lfwv;

    if-nez v2, :cond_1

    .line 550
    :cond_0
    :goto_0
    return v0

    .line 527
    :cond_1
    const/4 v2, 0x3

    if-ne p3, v2, :cond_2

    .line 528
    iput-boolean v0, p0, Lfwu;->f:Z

    move v0, v1

    .line 529
    goto :goto_0

    .line 532
    :cond_2
    iget-object v2, p0, Lfwu;->m:Landroid/graphics/Rect;

    invoke-virtual {v2, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-nez v2, :cond_3

    .line 533
    if-ne p3, v1, :cond_0

    .line 534
    iput-boolean v0, p0, Lfwu;->f:Z

    goto :goto_0

    .line 539
    :cond_3
    packed-switch p3, :pswitch_data_0

    :goto_1
    move v0, v1

    .line 550
    goto :goto_0

    .line 541
    :pswitch_0
    iput-boolean v1, p0, Lfwu;->f:Z

    goto :goto_1

    .line 546
    :pswitch_1
    iget-boolean v2, p0, Lfwu;->f:Z

    if-eqz v2, :cond_4

    .line 547
    iget-object v2, p0, Lfwu;->j:Lfwv;

    invoke-interface {v2, p0}, Lfwv;->a(Lfwu;)V

    .line 549
    :cond_4
    iput-boolean v0, p0, Lfwu;->f:Z

    goto :goto_1

    .line 539
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public b(I)V
    .locals 2

    .prologue
    .line 508
    iget-object v0, p0, Lfwu;->m:Landroid/graphics/Rect;

    if-eqz v0, :cond_0

    .line 509
    iget-object v0, p0, Lfwu;->m:Landroid/graphics/Rect;

    iget-object v1, p0, Lfwu;->m:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, p1

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 511
    :cond_0
    return-void
.end method

.method public synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 23
    check-cast p1, Llip;

    check-cast p2, Llip;

    invoke-virtual {p0, p1, p2}, Lfwu;->a(Llip;Llip;)I

    move-result v0

    return v0
.end method
