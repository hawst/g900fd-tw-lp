.class public final Llli;
.super Lhxz;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lhxz",
        "<[",
        "Lnmv;",
        ">;"
    }
.end annotation


# static fields
.field private static final b:[Lnmv;


# instance fields
.field private c:I

.field private d:Ljava/lang/String;

.field private e:[Lnmv;

.field private volatile f:Lkff;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const/4 v0, 0x0

    new-array v0, v0, [Lnmv;

    sput-object v0, Llli;->b:[Lnmv;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lhxz;-><init>(Landroid/content/Context;)V

    .line 31
    iput p2, p0, Llli;->c:I

    .line 32
    iput-object p3, p0, Llli;->d:Ljava/lang/String;

    .line 33
    return-void
.end method


# virtual methods
.method public a([Lnmv;)V
    .locals 1

    .prologue
    .line 44
    invoke-virtual {p0}, Llli;->r()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 53
    :cond_0
    :goto_0
    return-void

    .line 48
    :cond_1
    iput-object p1, p0, Llli;->e:[Lnmv;

    .line 50
    invoke-virtual {p0}, Llli;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 51
    invoke-super {p0, p1}, Lhxz;->b(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public synthetic b(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 16
    check-cast p1, [Lnmv;

    invoke-virtual {p0, p1}, Llli;->a([Lnmv;)V

    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Llli;->f:Lkff;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lkff;->m()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Llli;->f:Lkff;

    .line 58
    invoke-super {p0}, Lhxz;->b()Z

    move-result v0

    return v0
.end method

.method protected g()V
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Llli;->e:[Lnmv;

    if-nez v0, :cond_0

    iget-object v0, p0, Llli;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 38
    invoke-virtual {p0}, Llli;->t()V

    .line 40
    :cond_0
    return-void
.end method

.method public synthetic j()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 16
    invoke-virtual {p0}, Llli;->l()[Lnmv;

    move-result-object v0

    return-object v0
.end method

.method public l()[Lnmv;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 71
    iget-object v0, p0, Llli;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    sget-object v0, Llli;->b:[Lnmv;

    .line 104
    :goto_0
    return-object v0

    .line 75
    :cond_0
    new-instance v0, Lllj;

    .line 76
    invoke-virtual {p0}, Llli;->n()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lkfo;

    invoke-virtual {p0}, Llli;->n()Landroid/content/Context;

    move-result-object v3

    iget v4, p0, Llli;->c:I

    invoke-direct {v2, v3, v4}, Lkfo;-><init>(Landroid/content/Context;I)V

    iget-object v3, p0, Llli;->d:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lllj;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;)V

    .line 77
    iput-object v0, p0, Llli;->f:Lkff;

    .line 79
    :try_start_0
    invoke-virtual {v0}, Lllj;->l()V

    .line 80
    invoke-virtual {v0}, Lllj;->n()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 81
    sget-object v0, Llli;->b:[Lnmv;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 84
    iput-object v5, p0, Llli;->f:Lkff;

    goto :goto_0

    :cond_1
    iput-object v5, p0, Llli;->f:Lkff;

    .line 87
    invoke-virtual {v0}, Lllj;->t()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 88
    const-string v1, "OneboxSearchLoader"

    invoke-virtual {v0, v1}, Lllj;->d(Ljava/lang/String;)V

    .line 89
    iget-object v1, v0, Lkff;->k:Ljava/lang/Exception;

    if-eqz v1, :cond_2

    .line 90
    const-string v1, "OneboxSearchLoader"

    const-string v2, "Failed to fetch onebox data."

    iget-object v0, v0, Lkff;->k:Ljava/lang/Exception;

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 91
    sget-object v0, Llli;->b:[Lnmv;

    goto :goto_0

    .line 84
    :catchall_0
    move-exception v0

    iput-object v5, p0, Llli;->f:Lkff;

    throw v0

    .line 93
    :cond_2
    const-string v0, "OneboxSearchLoader"

    const-string v1, "Failed to fetch onebox data."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    sget-object v0, Llli;->b:[Lnmv;

    goto :goto_0

    .line 97
    :cond_3
    invoke-virtual {v0}, Lllj;->i()Lnna;

    move-result-object v0

    .line 98
    if-eqz v0, :cond_4

    .line 99
    iget-object v1, v0, Lnna;->d:[Lnmv;

    if-eqz v1, :cond_4

    .line 100
    iget-object v0, v0, Lnna;->d:[Lnmv;

    goto :goto_0

    .line 104
    :cond_4
    sget-object v0, Llli;->b:[Lnmv;

    goto :goto_0
.end method
