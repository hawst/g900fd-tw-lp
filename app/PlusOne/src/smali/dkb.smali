.class public final Ldkb;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmaq;",
        "Lmar;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final p:Lhrt;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 59
    const-string v3, "eventread"

    new-instance v4, Lmaq;

    invoke-direct {v4}, Lmaq;-><init>()V

    new-instance v5, Lmar;

    invoke-direct {v5}, Lmar;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 60
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Event ID must not be empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 63
    :cond_0
    iput-object p3, p0, Ldkb;->a:Ljava/lang/String;

    .line 64
    iput-object p4, p0, Ldkb;->b:Ljava/lang/String;

    .line 65
    const-class v0, Lhrt;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhrt;

    iput-object v0, p0, Ldkb;->p:Lhrt;

    .line 66
    return-void
.end method


# virtual methods
.method public a(ILjava/lang/String;Ljava/lang/Exception;)V
    .locals 3

    .prologue
    .line 70
    const/16 v0, 0x194

    if-ne p1, v0, :cond_1

    .line 71
    iget-object v0, p0, Ldkb;->f:Landroid/content/Context;

    iget v1, p0, Ldkb;->c:I

    iget-object v2, p0, Ldkb;->a:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Ldrm;->e(Landroid/content/Context;ILjava/lang/String;)V

    .line 81
    :cond_0
    :goto_0
    return-void

    .line 72
    :cond_1
    const/16 v0, 0x190

    if-lt p1, v0, :cond_0

    .line 74
    iget-object v0, p0, Ldkb;->a:Ljava/lang/String;

    iget-object v1, p0, Ldkb;->f:Landroid/content/Context;

    invoke-static {v1}, Lhqd;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75
    const-string v0, "HttpOperation"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 76
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x33

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "[GET_EVENT] received error: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; disable IS"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    :cond_2
    iget-object v0, p0, Ldkb;->p:Lhrt;

    iget-object v1, p0, Ldkb;->f:Landroid/content/Context;

    iget v2, p0, Ldkb;->c:I

    invoke-virtual {v0, v1, v2}, Lhrt;->a(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method protected a(Lmaq;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 86
    new-instance v0, Lmqf;

    invoke-direct {v0}, Lmqf;-><init>()V

    .line 87
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lmqf;->a:Ljava/lang/Integer;

    .line 88
    new-instance v1, Lmqe;

    invoke-direct {v1}, Lmqe;-><init>()V

    .line 89
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lmqe;->a:Ljava/lang/Integer;

    .line 91
    new-instance v2, Lmqh;

    invoke-direct {v2}, Lmqh;-><init>()V

    .line 93
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v2, Lmqh;->b:Ljava/lang/Integer;

    .line 94
    new-array v3, v6, [Lmqh;

    aput-object v2, v3, v7

    .line 97
    new-instance v2, Lmqi;

    invoke-direct {v2}, Lmqi;-><init>()V

    .line 98
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iput-object v4, v2, Lmqi;->b:Ljava/lang/Boolean;

    .line 99
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iput-object v4, v2, Lmqi;->a:Ljava/lang/Boolean;

    .line 101
    new-instance v4, Lmpm;

    invoke-direct {v4}, Lmpm;-><init>()V

    .line 102
    iget-object v5, p0, Ldkb;->a:Ljava/lang/String;

    iput-object v5, v4, Lmpm;->a:Ljava/lang/String;

    .line 103
    iget-object v5, p0, Ldkb;->b:Ljava/lang/String;

    iput-object v5, v4, Lmpm;->b:Ljava/lang/String;

    .line 105
    new-instance v5, Lmqd;

    invoke-direct {v5}, Lmqd;-><init>()V

    .line 106
    iput-object v3, v5, Lmqd;->b:[Lmqh;

    .line 107
    iput-object v0, v5, Lmqd;->c:Lmqf;

    .line 108
    iput-object v1, v5, Lmqd;->d:Lmqe;

    .line 109
    iput v6, v5, Lmqd;->h:I

    .line 110
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v5, Lmqd;->f:Ljava/lang/Boolean;

    .line 111
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v5, Lmqd;->g:Ljava/lang/Boolean;

    .line 112
    iput-object v2, v5, Lmqd;->e:Lmqi;

    .line 113
    new-array v0, v6, [Lmqd;

    aput-object v5, v0, v7

    .line 115
    new-instance v1, Lmqj;

    invoke-direct {v1}, Lmqj;-><init>()V

    .line 116
    iput-object v1, p1, Lmaq;->a:Lmqj;

    .line 117
    iput-object v0, v1, Lmqj;->d:[Lmqd;

    .line 118
    iput-object v4, v1, Lmqj;->a:Lmpm;

    .line 119
    new-instance v0, Loxz;

    invoke-direct {v0}, Loxz;-><init>()V

    iput-object v0, v1, Lmqj;->f:Loxz;

    .line 120
    iget-object v0, v1, Lmqj;->f:Loxz;

    invoke-static {}, Ldrm;->a()[I

    move-result-object v1

    iput-object v1, v0, Loxz;->a:[I

    .line 121
    return-void
.end method

.method protected a(Lmar;)V
    .locals 13

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 125
    iget-object v0, p1, Lmar;->a:Lmqk;

    .line 126
    iget-object v3, v0, Lmqk;->c:Ljava/lang/String;

    .line 127
    iget-object v1, v0, Lmqk;->a:Loya;

    .line 128
    invoke-static {v1}, Ldrm;->a(Loya;)Lidh;

    move-result-object v4

    .line 132
    const-wide/16 v10, 0x0

    .line 135
    iget-object v1, p0, Ldkb;->f:Landroid/content/Context;

    iget v2, p0, Ldkb;->c:I

    .line 136
    invoke-virtual {v4}, Lidh;->c()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Ldkc;->a:[Ljava/lang/String;

    invoke-static {v1, v2, v5, v6}, Ldrm;->a(Landroid/content/Context;ILjava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 139
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 140
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 141
    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 142
    const/4 v2, 0x2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v10

    .line 145
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 147
    iget-object v1, p0, Ldkb;->f:Landroid/content/Context;

    iget v2, p0, Ldkb;->c:I

    iget-object v5, v0, Lmqk;->b:Logr;

    move-object v12, v8

    invoke-static/range {v1 .. v12}, Ldrm;->a(Landroid/content/Context;ILjava/lang/String;Lidh;Logr;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;ZJLjava/util/List;)V

    .line 150
    return-void

    .line 145
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    move-object v7, v8

    move-object v6, v8

    goto :goto_0
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 30
    check-cast p1, Lmaq;

    invoke-virtual {p0, p1}, Ldkb;->a(Lmaq;)V

    return-void
.end method

.method protected synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 30
    check-cast p1, Lmar;

    invoke-virtual {p0, p1}, Ldkb;->a(Lmar;)V

    return-void
.end method
