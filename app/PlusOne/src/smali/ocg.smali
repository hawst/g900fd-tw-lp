.class public final Locg;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Locg;


# instance fields
.field public b:Locj;

.field public c:Lock;

.field private d:[Loch;

.field private e:[Loci;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1175
    const/4 v0, 0x0

    new-array v0, v0, [Locg;

    sput-object v0, Locg;->a:[Locg;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1176
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1179
    iput-object v0, p0, Locg;->b:Locj;

    .line 1182
    iput-object v0, p0, Locg;->c:Lock;

    .line 1185
    sget-object v0, Loch;->a:[Loch;

    iput-object v0, p0, Locg;->d:[Loch;

    .line 1188
    sget-object v0, Loci;->a:[Loci;

    iput-object v0, p0, Locg;->e:[Loci;

    .line 1176
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1219
    .line 1220
    iget-object v0, p0, Locg;->b:Locj;

    if-eqz v0, :cond_5

    .line 1221
    const/4 v0, 0x1

    iget-object v2, p0, Locg;->b:Locj;

    .line 1222
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1224
    :goto_0
    iget-object v2, p0, Locg;->c:Lock;

    if-eqz v2, :cond_0

    .line 1225
    const/4 v2, 0x2

    iget-object v3, p0, Locg;->c:Lock;

    .line 1226
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1228
    :cond_0
    iget-object v2, p0, Locg;->d:[Loch;

    if-eqz v2, :cond_2

    .line 1229
    iget-object v3, p0, Locg;->d:[Loch;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 1230
    if-eqz v5, :cond_1

    .line 1231
    const/4 v6, 0x3

    .line 1232
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 1229
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1236
    :cond_2
    iget-object v2, p0, Locg;->e:[Loci;

    if-eqz v2, :cond_4

    .line 1237
    iget-object v2, p0, Locg;->e:[Loci;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 1238
    if-eqz v4, :cond_3

    .line 1239
    const/4 v5, 0x4

    .line 1240
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 1237
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1244
    :cond_4
    iget-object v1, p0, Locg;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1245
    iput v0, p0, Locg;->ai:I

    .line 1246
    return v0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Locg;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1254
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1255
    sparse-switch v0, :sswitch_data_0

    .line 1259
    iget-object v2, p0, Locg;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 1260
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Locg;->ah:Ljava/util/List;

    .line 1263
    :cond_1
    iget-object v2, p0, Locg;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1265
    :sswitch_0
    return-object p0

    .line 1270
    :sswitch_1
    iget-object v0, p0, Locg;->b:Locj;

    if-nez v0, :cond_2

    .line 1271
    new-instance v0, Locj;

    invoke-direct {v0}, Locj;-><init>()V

    iput-object v0, p0, Locg;->b:Locj;

    .line 1273
    :cond_2
    iget-object v0, p0, Locg;->b:Locj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1277
    :sswitch_2
    iget-object v0, p0, Locg;->c:Lock;

    if-nez v0, :cond_3

    .line 1278
    new-instance v0, Lock;

    invoke-direct {v0}, Lock;-><init>()V

    iput-object v0, p0, Locg;->c:Lock;

    .line 1280
    :cond_3
    iget-object v0, p0, Locg;->c:Lock;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1284
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1285
    iget-object v0, p0, Locg;->d:[Loch;

    if-nez v0, :cond_5

    move v0, v1

    .line 1286
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Loch;

    .line 1287
    iget-object v3, p0, Locg;->d:[Loch;

    if-eqz v3, :cond_4

    .line 1288
    iget-object v3, p0, Locg;->d:[Loch;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1290
    :cond_4
    iput-object v2, p0, Locg;->d:[Loch;

    .line 1291
    :goto_2
    iget-object v2, p0, Locg;->d:[Loch;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 1292
    iget-object v2, p0, Locg;->d:[Loch;

    new-instance v3, Loch;

    invoke-direct {v3}, Loch;-><init>()V

    aput-object v3, v2, v0

    .line 1293
    iget-object v2, p0, Locg;->d:[Loch;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1294
    invoke-virtual {p1}, Loxn;->a()I

    .line 1291
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1285
    :cond_5
    iget-object v0, p0, Locg;->d:[Loch;

    array-length v0, v0

    goto :goto_1

    .line 1297
    :cond_6
    iget-object v2, p0, Locg;->d:[Loch;

    new-instance v3, Loch;

    invoke-direct {v3}, Loch;-><init>()V

    aput-object v3, v2, v0

    .line 1298
    iget-object v2, p0, Locg;->d:[Loch;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1302
    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1303
    iget-object v0, p0, Locg;->e:[Loci;

    if-nez v0, :cond_8

    move v0, v1

    .line 1304
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Loci;

    .line 1305
    iget-object v3, p0, Locg;->e:[Loci;

    if-eqz v3, :cond_7

    .line 1306
    iget-object v3, p0, Locg;->e:[Loci;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1308
    :cond_7
    iput-object v2, p0, Locg;->e:[Loci;

    .line 1309
    :goto_4
    iget-object v2, p0, Locg;->e:[Loci;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    .line 1310
    iget-object v2, p0, Locg;->e:[Loci;

    new-instance v3, Loci;

    invoke-direct {v3}, Loci;-><init>()V

    aput-object v3, v2, v0

    .line 1311
    iget-object v2, p0, Locg;->e:[Loci;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1312
    invoke-virtual {p1}, Loxn;->a()I

    .line 1309
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 1303
    :cond_8
    iget-object v0, p0, Locg;->e:[Loci;

    array-length v0, v0

    goto :goto_3

    .line 1315
    :cond_9
    iget-object v2, p0, Locg;->e:[Loci;

    new-instance v3, Loci;

    invoke-direct {v3}, Loci;-><init>()V

    aput-object v3, v2, v0

    .line 1316
    iget-object v2, p0, Locg;->e:[Loci;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1255
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1193
    iget-object v1, p0, Locg;->b:Locj;

    if-eqz v1, :cond_0

    .line 1194
    const/4 v1, 0x1

    iget-object v2, p0, Locg;->b:Locj;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 1196
    :cond_0
    iget-object v1, p0, Locg;->c:Lock;

    if-eqz v1, :cond_1

    .line 1197
    const/4 v1, 0x2

    iget-object v2, p0, Locg;->c:Lock;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 1199
    :cond_1
    iget-object v1, p0, Locg;->d:[Loch;

    if-eqz v1, :cond_3

    .line 1200
    iget-object v2, p0, Locg;->d:[Loch;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 1201
    if-eqz v4, :cond_2

    .line 1202
    const/4 v5, 0x3

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 1200
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1206
    :cond_3
    iget-object v1, p0, Locg;->e:[Loci;

    if-eqz v1, :cond_5

    .line 1207
    iget-object v1, p0, Locg;->e:[Loci;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    .line 1208
    if-eqz v3, :cond_4

    .line 1209
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 1207
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1213
    :cond_5
    iget-object v0, p0, Locg;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1215
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1172
    invoke-virtual {p0, p1}, Locg;->a(Loxn;)Locg;

    move-result-object v0

    return-object v0
.end method
