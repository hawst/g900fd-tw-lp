.class public final Lppk;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lpul;

.field private b:[Lppj;

.field private c:[Lppf;

.field private d:Lppf;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 325
    invoke-direct {p0}, Loxq;-><init>()V

    .line 328
    iput-object v1, p0, Lppk;->a:Lpul;

    .line 331
    sget-object v0, Lppj;->a:[Lppj;

    iput-object v0, p0, Lppk;->b:[Lppj;

    .line 334
    sget-object v0, Lppf;->a:[Lppf;

    iput-object v0, p0, Lppk;->c:[Lppf;

    .line 337
    iput-object v1, p0, Lppk;->d:Lppf;

    .line 325
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 368
    .line 369
    iget-object v0, p0, Lppk;->a:Lpul;

    if-eqz v0, :cond_5

    .line 370
    const/4 v0, 0x1

    iget-object v2, p0, Lppk;->a:Lpul;

    .line 371
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 373
    :goto_0
    iget-object v2, p0, Lppk;->b:[Lppj;

    if-eqz v2, :cond_1

    .line 374
    iget-object v3, p0, Lppk;->b:[Lppj;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 375
    if-eqz v5, :cond_0

    .line 376
    const/4 v6, 0x2

    .line 377
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 374
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 381
    :cond_1
    iget-object v2, p0, Lppk;->d:Lppf;

    if-eqz v2, :cond_2

    .line 382
    const/4 v2, 0x3

    iget-object v3, p0, Lppk;->d:Lppf;

    .line 383
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 385
    :cond_2
    iget-object v2, p0, Lppk;->c:[Lppf;

    if-eqz v2, :cond_4

    .line 386
    iget-object v2, p0, Lppk;->c:[Lppf;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 387
    if-eqz v4, :cond_3

    .line 388
    const/4 v5, 0x4

    .line 389
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 386
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 393
    :cond_4
    iget-object v1, p0, Lppk;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 394
    iput v0, p0, Lppk;->ai:I

    .line 395
    return v0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lppk;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 403
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 404
    sparse-switch v0, :sswitch_data_0

    .line 408
    iget-object v2, p0, Lppk;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 409
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lppk;->ah:Ljava/util/List;

    .line 412
    :cond_1
    iget-object v2, p0, Lppk;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 414
    :sswitch_0
    return-object p0

    .line 419
    :sswitch_1
    iget-object v0, p0, Lppk;->a:Lpul;

    if-nez v0, :cond_2

    .line 420
    new-instance v0, Lpul;

    invoke-direct {v0}, Lpul;-><init>()V

    iput-object v0, p0, Lppk;->a:Lpul;

    .line 422
    :cond_2
    iget-object v0, p0, Lppk;->a:Lpul;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 426
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 427
    iget-object v0, p0, Lppk;->b:[Lppj;

    if-nez v0, :cond_4

    move v0, v1

    .line 428
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lppj;

    .line 429
    iget-object v3, p0, Lppk;->b:[Lppj;

    if-eqz v3, :cond_3

    .line 430
    iget-object v3, p0, Lppk;->b:[Lppj;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 432
    :cond_3
    iput-object v2, p0, Lppk;->b:[Lppj;

    .line 433
    :goto_2
    iget-object v2, p0, Lppk;->b:[Lppj;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 434
    iget-object v2, p0, Lppk;->b:[Lppj;

    new-instance v3, Lppj;

    invoke-direct {v3}, Lppj;-><init>()V

    aput-object v3, v2, v0

    .line 435
    iget-object v2, p0, Lppk;->b:[Lppj;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 436
    invoke-virtual {p1}, Loxn;->a()I

    .line 433
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 427
    :cond_4
    iget-object v0, p0, Lppk;->b:[Lppj;

    array-length v0, v0

    goto :goto_1

    .line 439
    :cond_5
    iget-object v2, p0, Lppk;->b:[Lppj;

    new-instance v3, Lppj;

    invoke-direct {v3}, Lppj;-><init>()V

    aput-object v3, v2, v0

    .line 440
    iget-object v2, p0, Lppk;->b:[Lppj;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 444
    :sswitch_3
    iget-object v0, p0, Lppk;->d:Lppf;

    if-nez v0, :cond_6

    .line 445
    new-instance v0, Lppf;

    invoke-direct {v0}, Lppf;-><init>()V

    iput-object v0, p0, Lppk;->d:Lppf;

    .line 447
    :cond_6
    iget-object v0, p0, Lppk;->d:Lppf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 451
    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 452
    iget-object v0, p0, Lppk;->c:[Lppf;

    if-nez v0, :cond_8

    move v0, v1

    .line 453
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lppf;

    .line 454
    iget-object v3, p0, Lppk;->c:[Lppf;

    if-eqz v3, :cond_7

    .line 455
    iget-object v3, p0, Lppk;->c:[Lppf;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 457
    :cond_7
    iput-object v2, p0, Lppk;->c:[Lppf;

    .line 458
    :goto_4
    iget-object v2, p0, Lppk;->c:[Lppf;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    .line 459
    iget-object v2, p0, Lppk;->c:[Lppf;

    new-instance v3, Lppf;

    invoke-direct {v3}, Lppf;-><init>()V

    aput-object v3, v2, v0

    .line 460
    iget-object v2, p0, Lppk;->c:[Lppf;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 461
    invoke-virtual {p1}, Loxn;->a()I

    .line 458
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 452
    :cond_8
    iget-object v0, p0, Lppk;->c:[Lppf;

    array-length v0, v0

    goto :goto_3

    .line 464
    :cond_9
    iget-object v2, p0, Lppk;->c:[Lppf;

    new-instance v3, Lppf;

    invoke-direct {v3}, Lppf;-><init>()V

    aput-object v3, v2, v0

    .line 465
    iget-object v2, p0, Lppk;->c:[Lppf;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 404
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 342
    iget-object v1, p0, Lppk;->a:Lpul;

    if-eqz v1, :cond_0

    .line 343
    const/4 v1, 0x1

    iget-object v2, p0, Lppk;->a:Lpul;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 345
    :cond_0
    iget-object v1, p0, Lppk;->b:[Lppj;

    if-eqz v1, :cond_2

    .line 346
    iget-object v2, p0, Lppk;->b:[Lppj;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 347
    if-eqz v4, :cond_1

    .line 348
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 346
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 352
    :cond_2
    iget-object v1, p0, Lppk;->d:Lppf;

    if-eqz v1, :cond_3

    .line 353
    const/4 v1, 0x3

    iget-object v2, p0, Lppk;->d:Lppf;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 355
    :cond_3
    iget-object v1, p0, Lppk;->c:[Lppf;

    if-eqz v1, :cond_5

    .line 356
    iget-object v1, p0, Lppk;->c:[Lppf;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    .line 357
    if-eqz v3, :cond_4

    .line 358
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 356
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 362
    :cond_5
    iget-object v0, p0, Lppk;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 364
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 321
    invoke-virtual {p0, p1}, Lppk;->a(Loxn;)Lppk;

    move-result-object v0

    return-object v0
.end method
