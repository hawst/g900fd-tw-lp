.class public final Lkzw;
.super Lllq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:J

.field private f:Z

.field private g:Lkzv;

.field private h:[Lkzx;

.field private i:I

.field private j:I

.field private k:I

.field private l:I

.field private m:I


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lllq;-><init>()V

    .line 38
    return-void
.end method

.method public constructor <init>(Lpcd;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 45
    invoke-direct {p0}, Lllq;-><init>()V

    .line 46
    iget-object v0, p1, Lpcd;->d:Ljava/lang/String;

    iput-object v0, p0, Lkzw;->a:Ljava/lang/String;

    .line 47
    iget-object v0, p1, Lpcd;->b:Ljava/lang/String;

    iput-object v0, p0, Lkzw;->b:Ljava/lang/String;

    .line 48
    iget-object v0, p1, Lpcd;->c:Ljava/lang/String;

    iput-object v0, p0, Lkzw;->c:Ljava/lang/String;

    .line 49
    iget-object v0, p1, Lpcd;->f:Ljava/lang/Long;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Long;)J

    move-result-wide v2

    iput-wide v2, p0, Lkzw;->e:J

    .line 50
    iget-object v0, p1, Lpcd;->g:Ljava/lang/Boolean;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v0

    iput-boolean v0, p0, Lkzw;->f:Z

    .line 51
    iget v0, p1, Lpcd;->j:I

    iput v0, p0, Lkzw;->l:I

    .line 52
    iget-object v0, p1, Lpcd;->i:Loya;

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p1, Lpcd;->i:Loya;

    sget-object v2, Lpbu;->a:Loxr;

    invoke-virtual {v0, v2}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpbu;

    .line 54
    new-instance v2, Lkzv;

    invoke-direct {v2, v0}, Lkzv;-><init>(Lpbu;)V

    iput-object v2, p0, Lkzw;->g:Lkzv;

    .line 55
    iget-object v2, p0, Lkzw;->g:Lkzv;

    iget-object v3, v0, Lpbu;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lkzv;->b(Ljava/lang/String;)V

    .line 56
    iget-object v0, v0, Lpbu;->b:Ljava/lang/String;

    invoke-direct {p0, v0}, Lkzw;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkzw;->d:Ljava/lang/String;

    .line 58
    :cond_0
    const/4 v0, -0x1

    iput v0, p0, Lkzw;->k:I

    .line 59
    iget-object v0, p1, Lpcd;->e:[Loya;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lpcd;->e:[Loya;

    array-length v0, v0

    :goto_0
    iput v0, p0, Lkzw;->i:I

    .line 60
    iget v0, p0, Lkzw;->i:I

    if-lez v0, :cond_4

    .line 61
    iget v0, p0, Lkzw;->i:I

    new-array v0, v0, [Lkzx;

    iput-object v0, p0, Lkzw;->h:[Lkzx;

    .line 62
    :goto_1
    iget v0, p0, Lkzw;->i:I

    if-ge v1, v0, :cond_4

    .line 63
    iget-object v0, p1, Lpcd;->e:[Loya;

    aget-object v0, v0, v1

    sget-object v2, Lpca;->a:Loxr;

    invoke-virtual {v0, v2}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpca;

    .line 64
    iget-object v2, p0, Lkzw;->h:[Lkzx;

    new-instance v3, Lkzx;

    invoke-direct {v3, v0}, Lkzx;-><init>(Lpca;)V

    aput-object v3, v2, v1

    .line 65
    iget-object v2, p0, Lkzw;->h:[Lkzx;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Lkzx;->d()Lkzv;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 66
    iget v2, p0, Lkzw;->j:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lkzw;->j:I

    .line 67
    iget-object v2, p0, Lkzw;->d:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 68
    iget-object v0, v0, Lpca;->e:Loya;

    sget-object v2, Lpbu;->a:Loxr;

    .line 69
    invoke-virtual {v0, v2}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpbu;

    .line 70
    iget-object v0, v0, Lpbu;->b:Ljava/lang/String;

    invoke-direct {p0, v0}, Lkzw;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkzw;->d:Ljava/lang/String;

    .line 73
    :cond_1
    iget-object v0, p0, Lkzw;->h:[Lkzx;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lkzx;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p1, Lpcd;->h:Ljava/lang/String;

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 74
    iput v1, p0, Lkzw;->k:I

    .line 62
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    move v0, v1

    .line 59
    goto :goto_0

    .line 78
    :cond_4
    return-void
.end method

.method public constructor <init>(Lpcd;I)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lkzw;-><init>(Lpcd;)V

    .line 42
    iput p2, p0, Lkzw;->m:I

    .line 43
    return-void
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 81
    if-nez p1, :cond_0

    .line 82
    const/4 v0, 0x0

    .line 84
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-string v1, "authkey"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a([B)Lkzw;
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 127
    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    if-eqz v4, :cond_0

    new-instance v3, Lkzw;

    invoke-direct {v3}, Lkzw;-><init>()V

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    iput v0, v3, Lkzw;->i:I

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    iput v0, v3, Lkzw;->j:I

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->get()B

    move-result v0

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_1
    invoke-static {v4}, Lkzw;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lkzw;->a:Ljava/lang/String;

    invoke-static {v4}, Lkzw;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lkzw;->b:Ljava/lang/String;

    invoke-static {v4}, Lkzw;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lkzw;->c:Ljava/lang/String;

    invoke-static {v4}, Lkzw;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lkzw;->d:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v5

    iput v5, v3, Lkzw;->k:I

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v6

    iput-wide v6, v3, Lkzw;->e:J

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->get()B

    move-result v5

    if-ne v5, v1, :cond_4

    :goto_2
    iput-boolean v1, v3, Lkzw;->f:Z

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v1

    iput v1, v3, Lkzw;->l:I

    if-eqz v0, :cond_2

    new-instance v0, Lkzv;

    invoke-direct {v0}, Lkzv;-><init>()V

    iput-object v0, v3, Lkzw;->g:Lkzv;

    iget-object v0, v3, Lkzw;->g:Lkzv;

    invoke-virtual {v0, v4}, Lkzv;->a(Ljava/nio/ByteBuffer;)V

    :cond_2
    iget v0, v3, Lkzw;->i:I

    new-array v0, v0, [Lkzx;

    iput-object v0, v3, Lkzw;->h:[Lkzx;

    :goto_3
    iget v0, v3, Lkzw;->i:I

    if-ge v2, v0, :cond_5

    iget-object v0, v3, Lkzw;->h:[Lkzx;

    new-instance v1, Lkzx;

    invoke-direct {v1}, Lkzx;-><init>()V

    aput-object v1, v0, v2

    iget-object v0, v3, Lkzw;->h:[Lkzx;

    aget-object v0, v0, v2

    invoke-virtual {v0, v4}, Lkzx;->a(Ljava/nio/ByteBuffer;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    move v1, v2

    goto :goto_2

    :cond_5
    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    iput v0, v3, Lkzw;->m:I

    move-object v0, v3

    goto/16 :goto_0
.end method

.method private a(Ljava/io/DataOutputStream;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 102
    iget v0, p0, Lkzw;->i:I

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 103
    iget v0, p0, Lkzw;->j:I

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 104
    iget-object v0, p0, Lkzw;->g:Lkzv;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 105
    :goto_0
    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeBoolean(Z)V

    .line 106
    iget-object v2, p0, Lkzw;->a:Ljava/lang/String;

    invoke-static {p1, v2}, Lkzw;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 107
    iget-object v2, p0, Lkzw;->b:Ljava/lang/String;

    invoke-static {p1, v2}, Lkzw;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 108
    iget-object v2, p0, Lkzw;->c:Ljava/lang/String;

    invoke-static {p1, v2}, Lkzw;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 109
    iget-object v2, p0, Lkzw;->d:Ljava/lang/String;

    invoke-static {p1, v2}, Lkzw;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 110
    iget v2, p0, Lkzw;->k:I

    invoke-virtual {p1, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 111
    iget-wide v2, p0, Lkzw;->e:J

    invoke-virtual {p1, v2, v3}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 112
    iget-boolean v2, p0, Lkzw;->f:Z

    invoke-virtual {p1, v2}, Ljava/io/DataOutputStream;->writeBoolean(Z)V

    .line 113
    iget v2, p0, Lkzw;->l:I

    invoke-virtual {p1, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 114
    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lkzw;->g:Lkzv;

    invoke-virtual {v0, p1}, Lkzv;->a(Ljava/io/DataOutputStream;)V

    .line 117
    :cond_0
    :goto_1
    iget v0, p0, Lkzw;->i:I

    if-ge v1, v0, :cond_2

    .line 118
    iget-object v0, p0, Lkzw;->h:[Lkzx;

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, Lkzx;->a(Ljava/io/DataOutputStream;)V

    .line 117
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    move v0, v1

    .line 104
    goto :goto_0

    .line 120
    :cond_2
    iget v0, p0, Lkzw;->m:I

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 121
    return-void
.end method

.method public static a(Lkzw;)[B
    .locals 2

    .prologue
    .line 92
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    const/16 v1, 0x200

    invoke-direct {v0, v1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 93
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 95
    invoke-direct {p0, v1}, Lkzw;->a(Ljava/io/DataOutputStream;)V

    .line 96
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 97
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V

    .line 98
    return-object v0
.end method

.method private s()Z
    .locals 2

    .prologue
    .line 206
    iget v0, p0, Lkzw;->k:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lkzw;->k:I

    if-ltz v0, :cond_1

    iget v0, p0, Lkzw;->k:I

    iget-object v1, p0, Lkzw;->h:[Lkzx;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 168
    iget v0, p0, Lkzw;->j:I

    return v0
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 196
    invoke-direct {p0}, Lkzw;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 197
    iput p1, p0, Lkzw;->k:I

    .line 199
    :cond_0
    return-void
.end method

.method public a(J)V
    .locals 3

    .prologue
    const-wide/16 v0, 0x0

    .line 219
    cmp-long v2, p1, v0

    if-gez v2, :cond_0

    move-wide p1, v0

    :cond_0
    iput-wide p1, p0, Lkzw;->e:J

    .line 220
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 172
    iget v0, p0, Lkzw;->i:I

    return v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lkzw;->a:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lkzw;->c:Ljava/lang/String;

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lkzw;->d:Ljava/lang/String;

    return-object v0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 192
    iget v0, p0, Lkzw;->k:I

    return v0
.end method

.method public g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 202
    invoke-direct {p0}, Lkzw;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lkzw;->h:[Lkzx;

    iget v1, p0, Lkzw;->k:I

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lkzx;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()J
    .locals 2

    .prologue
    .line 211
    iget-wide v0, p0, Lkzw;->e:J

    return-wide v0
.end method

.method public i()Lkzv;
    .locals 2

    .prologue
    .line 231
    iget-object v0, p0, Lkzw;->h:[Lkzx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lkzw;->h:[Lkzx;

    array-length v0, v0

    if-nez v0, :cond_1

    .line 232
    :cond_0
    const/4 v0, 0x0

    .line 234
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lkzw;->h:[Lkzx;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lkzx;->d()Lkzv;

    move-result-object v0

    goto :goto_0
.end method

.method public j()[Lkzx;
    .locals 1

    .prologue
    .line 238
    iget-object v0, p0, Lkzw;->h:[Lkzx;

    return-object v0
.end method

.method public k()Z
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 246
    iget v0, p0, Lkzw;->j:I

    if-le v0, v1, :cond_0

    iget v0, p0, Lkzw;->i:I

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l()Z
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 250
    iget v0, p0, Lkzw;->j:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lkzw;->i:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public m()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 254
    iget v1, p0, Lkzw;->j:I

    if-ne v1, v0, :cond_0

    iget v1, p0, Lkzw;->i:I

    const/4 v2, 0x2

    if-le v1, v2, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public n()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 258
    iget v1, p0, Lkzw;->j:I

    if-ne v1, v0, :cond_0

    iget v1, p0, Lkzw;->i:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public o()Z
    .locals 2

    .prologue
    .line 262
    iget v0, p0, Lkzw;->j:I

    if-nez v0, :cond_0

    iget v0, p0, Lkzw;->i:I

    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public p()Z
    .locals 2

    .prologue
    .line 266
    iget v0, p0, Lkzw;->i:I

    const/4 v1, 0x2

    if-lt v0, v1, :cond_1

    invoke-virtual {p0}, Lkzw;->k()Z

    move-result v0

    if-nez v0, :cond_0

    .line 267
    invoke-virtual {p0}, Lkzw;->l()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lkzw;->m()Z

    move-result v0

    if-nez v0, :cond_0

    .line 268
    invoke-virtual {p0}, Lkzw;->n()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lkzw;->o()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public q()I
    .locals 1

    .prologue
    .line 272
    iget v0, p0, Lkzw;->l:I

    return v0
.end method

.method public r()I
    .locals 1

    .prologue
    .line 276
    iget v0, p0, Lkzw;->m:I

    return v0
.end method
