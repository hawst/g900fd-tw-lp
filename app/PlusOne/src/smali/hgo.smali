.class public Lhgo;
.super Lhgl;
.source "PG"

# interfaces
.implements Lbc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lhgl;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# static fields
.field private static final e:[Ljava/lang/String;

.field private static final f:[Ljava/lang/String;

.field private static final g:[Ljava/lang/String;

.field private static final h:[Ljava/lang/String;

.field private static final i:[Ljava/lang/String;

.field private static final j:[Ljava/lang/String;

.field private static l:[Ljava/lang/String;

.field private static m:Ljava/util/regex/Pattern;


# instance fields
.field private A:Z

.field private B:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final C:I

.field private final D:I

.field private final E:I

.field private final F:I

.field private final G:I

.field private final H:I

.field private final I:I

.field private J:Lhgs;

.field private K:[Landroid/database/Cursor;

.field private L:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final M:Ljava/util/HashMap;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private final N:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final O:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private P:Ljpl;

.field private Q:Z

.field private R:Landroid/database/Cursor;

.field private S:Landroid/database/Cursor;

.field private T:Z

.field private final U:Landroid/database/DataSetObserver;

.field private V:Lhgq;

.field private n:Landroid/database/Cursor;

.field private o:Lbb;

.field private p:Lhei;

.field private q:Lhxh;

.field private r:I

.field private s:I

.field private t:Z

.field private u:Z

.field private v:Z

.field private w:Ljava/lang/String;

.field private x:Ljava/lang/String;

.field private y:Z

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 68
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "circle_id"

    aput-object v1, v0, v4

    const-string v1, "type"

    aput-object v1, v0, v5

    const-string v1, "circle_name"

    aput-object v1, v0, v6

    const-string v1, "contact_count"

    aput-object v1, v0, v7

    sput-object v0, Lhgo;->e:[Ljava/lang/String;

    .line 81
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "square_id"

    aput-object v1, v0, v4

    const-string v1, "square_name"

    aput-object v1, v0, v5

    const-string v1, "restricted_domain"

    aput-object v1, v0, v6

    sput-object v0, Lhgo;->f:[Ljava/lang/String;

    .line 92
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "person_id"

    aput-object v1, v0, v4

    const-string v1, "gaia_id"

    aput-object v1, v0, v5

    const-string v1, "name"

    aput-object v1, v0, v6

    const-string v1, "avatar"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "packed_circle_ids"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "email"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "profile_type"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "in_same_visibility_group"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "verified"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "interaction_sort_key"

    aput-object v2, v0, v1

    sput-object v0, Lhgo;->g:[Ljava/lang/String;

    .line 116
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "person_id"

    aput-object v1, v0, v3

    const-string v1, "lookup_key"

    aput-object v1, v0, v4

    const-string v1, "name"

    aput-object v1, v0, v5

    const-string v1, "email"

    aput-object v1, v0, v6

    sput-object v0, Lhgo;->h:[Ljava/lang/String;

    .line 123
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "person_id"

    aput-object v1, v0, v3

    const-string v1, "lookup_key"

    aput-object v1, v0, v4

    const-string v1, "name"

    aput-object v1, v0, v5

    const-string v1, "email"

    aput-object v1, v0, v6

    const-string v1, "phone"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "phone_type"

    aput-object v2, v0, v1

    .line 139
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "gaia_id"

    aput-object v1, v0, v3

    const-string v1, "packed_circle_ids"

    aput-object v1, v0, v4

    sput-object v0, Lhgo;->i:[Ljava/lang/String;

    .line 147
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "gaia_id"

    aput-object v1, v0, v4

    const-string v1, "person_id"

    aput-object v1, v0, v5

    const-string v1, "name"

    aput-object v1, v0, v6

    const-string v1, "avatar"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "profile_type"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "snippet"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "in_same_visibility_group"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "verified"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "auto_complete_index"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "auto_complete_suggestion"

    aput-object v2, v0, v1

    sput-object v0, Lhgo;->j:[Ljava/lang/String;

    .line 177
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "gaia_id"

    aput-object v1, v0, v4

    sput-object v0, Lhgo;->l:[Ljava/lang/String;

    .line 186
    const-string v0, "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}\\@([a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}(\\.[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25})*)?"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lhgo;->m:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILhgt;Lhgm;Lae;Lbb;I)V
    .locals 9

    .prologue
    .line 345
    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lhgo;-><init>(Landroid/content/Context;ILhgt;Lhgm;Lae;Lbb;II)V

    .line 347
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILhgt;Lhgm;Lae;Lbb;II)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 358
    invoke-direct {p0, p1, p2, p3, p4}, Lhgl;-><init>(Landroid/content/Context;ILhgt;Lhgm;)V

    .line 224
    const/4 v0, -0x1

    iput v0, p0, Lhgo;->s:I

    .line 246
    iput-boolean v2, p0, Lhgo;->z:Z

    .line 248
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lhgo;->B:Ljava/util/Set;

    .line 266
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lhgo;->M:Ljava/util/HashMap;

    .line 272
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lhgo;->N:Ljava/util/HashSet;

    .line 277
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lhgo;->O:Ljava/util/HashSet;

    .line 279
    new-instance v0, Ljpl;

    invoke-direct {v0}, Ljpl;-><init>()V

    iput-object v0, p0, Lhgo;->P:Ljpl;

    .line 335
    new-instance v0, Lhgp;

    invoke-direct {v0, p0}, Lhgp;-><init>(Lhgo;)V

    iput-object v0, p0, Lhgo;->U:Landroid/database/DataSetObserver;

    .line 576
    new-instance v0, Lhgq;

    invoke-direct {v0, p0}, Lhgq;-><init>(Lhgo;)V

    iput-object v0, p0, Lhgo;->V:Lhgq;

    .line 360
    const-class v0, Lhgs;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhgs;

    iput-object v0, p0, Lhgo;->J:Lhgs;

    .line 361
    iget-object v0, p0, Lhgo;->J:Lhgs;

    invoke-interface {v0}, Lhgs;->a()Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Lhgo;->n:Landroid/database/Cursor;

    .line 363
    const/4 v0, 0x5

    new-array v0, v0, [Landroid/database/Cursor;

    iput-object v0, p0, Lhgo;->K:[Landroid/database/Cursor;

    .line 365
    iput-boolean v2, p0, Lhgo;->A:Z

    .line 366
    mul-int/lit8 v0, p8, 0xa

    add-int/lit16 v0, v0, 0x400

    .line 367
    add-int/lit8 v1, v0, 0x1

    iput v0, p0, Lhgo;->C:I

    .line 368
    add-int/lit8 v0, v1, 0x1

    iput v1, p0, Lhgo;->D:I

    .line 369
    add-int/lit8 v1, v0, 0x1

    iput v0, p0, Lhgo;->E:I

    .line 370
    add-int/lit8 v0, v1, 0x1

    iput v1, p0, Lhgo;->F:I

    .line 371
    add-int/lit8 v1, v0, 0x1

    iput v0, p0, Lhgo;->G:I

    .line 372
    add-int/lit8 v0, v1, 0x1

    iput v1, p0, Lhgo;->H:I

    .line 373
    iput v0, p0, Lhgo;->I:I

    .line 375
    const-string v0, "people_search_results"

    .line 376
    invoke-virtual {p5, v0}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lhgr;

    .line 377
    if-nez v0, :cond_1

    .line 378
    new-instance v0, Lhgr;

    invoke-direct {v0}, Lhgr;-><init>()V

    .line 379
    invoke-virtual {p5}, Lae;->a()Lat;

    move-result-object v1

    const-string v2, "people_search_results"

    invoke-virtual {v1, v0, v2}, Lat;->a(Lu;Ljava/lang/String;)Lat;

    move-result-object v1

    .line 380
    invoke-virtual {v1}, Lat;->c()I

    .line 390
    :cond_0
    :goto_0
    iget-object v1, p0, Lhgo;->P:Ljpl;

    invoke-virtual {v0, v1}, Lhgr;->a(Ljpl;)V

    .line 392
    iput-object p6, p0, Lhgo;->o:Lbb;

    .line 394
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Lhgo;->p:Lhei;

    .line 395
    iput p7, p0, Lhgo;->r:I

    .line 396
    iget-object v0, p0, Lhgo;->P:Ljpl;

    iget-boolean v1, p0, Lhgo;->z:Z

    invoke-virtual {v0, v1}, Ljpl;->a(Z)V

    .line 397
    new-instance v0, Lhxh;

    iget v1, p0, Lhgo;->r:I

    invoke-direct {v0, p1, p6, v1, p8}, Lhxh;-><init>(Landroid/content/Context;Lbb;II)V

    iput-object v0, p0, Lhgo;->q:Lhxh;

    .line 398
    iget-object v0, p0, Lhgo;->q:Lhxh;

    iget-object v1, p0, Lhgo;->U:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lhxh;->a(Landroid/database/DataSetObserver;)V

    .line 399
    return-void

    .line 382
    :cond_1
    invoke-virtual {v0}, Lhgr;->a()Ljpl;

    move-result-object v1

    .line 383
    if-eqz v1, :cond_0

    .line 384
    iput-object v1, p0, Lhgo;->P:Ljpl;

    .line 385
    iget-object v1, p0, Lhgo;->P:Ljpl;

    invoke-virtual {v1}, Ljpl;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lhgo;->w:Ljava/lang/String;

    .line 386
    iput-boolean v2, p0, Lhgo;->Q:Z

    goto :goto_0
.end method

.method private a(Landroid/database/Cursor;Z)V
    .locals 12

    .prologue
    .line 879
    iget-object v0, p0, Lhgo;->P:Ljpl;

    invoke-virtual {v0}, Ljpl;->h()V

    .line 880
    iget-object v0, p0, Lhgo;->p:Lhei;

    iget v1, p0, Lhgo;->r:I

    invoke-interface {v0, v1}, Lhei;->c(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 881
    iget-object v0, p0, Lhgo;->p:Lhei;

    iget v1, p0, Lhgo;->r:I

    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 882
    const-string v1, "gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 883
    invoke-direct {p0, v2}, Lhgo;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 884
    const-string v3, "display_name"

    invoke-interface {v0, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 885
    const-string v4, "profile_photo_url"

    invoke-interface {v0, v4}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 886
    const-string v4, "is_dasher_account"

    invoke-interface {v0, v4}, Lhej;->c(Ljava/lang/String;)Z

    move-result v10

    .line 888
    if-eqz p2, :cond_0

    if-eqz v3, :cond_0

    iget-object v0, p0, Lhgo;->w:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 890
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lhgo;->w:Ljava/lang/String;

    .line 891
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 892
    iget-object v0, p0, Lhgo;->P:Ljpl;

    const/4 v4, 0x1

    .line 894
    invoke-static {v5}, Lhst;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v11, 0x0

    .line 892
    invoke-virtual/range {v0 .. v11}, Ljpl;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 900
    :cond_0
    if-eqz p1, :cond_3

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 902
    :cond_1
    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 903
    iget-object v0, p0, Lhgo;->B:Ljava/util/Set;

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 904
    iget-object v0, p0, Lhgo;->P:Ljpl;

    const/4 v1, 0x1

    .line 905
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x3

    .line 907
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x7

    .line 908
    invoke-interface {p1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    const/4 v5, 0x4

    .line 909
    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x5

    .line 910
    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x6

    .line 911
    invoke-interface {p1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x8

    .line 914
    invoke-interface {p1, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    if-eqz v10, :cond_4

    const/4 v10, 0x1

    :goto_0
    const/16 v11, 0x9

    .line 915
    invoke-interface {p1, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    if-eqz v11, :cond_5

    const/4 v11, 0x1

    .line 904
    :goto_1
    invoke-virtual/range {v0 .. v11}, Ljpl;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 917
    :cond_2
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 920
    :cond_3
    iget-object v0, p0, Lhgo;->P:Ljpl;

    invoke-virtual {v0}, Ljpl;->i()V

    .line 922
    invoke-direct {p0}, Lhgo;->e()V

    .line 923
    return-void

    .line 914
    :cond_4
    const/4 v10, 0x0

    goto :goto_0

    .line 915
    :cond_5
    const/4 v11, 0x0

    goto :goto_1
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 3

    .prologue
    .line 608
    if-nez p2, :cond_1

    iget-object v0, p0, Lhgo;->w:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 656
    :cond_0
    :goto_0
    return-void

    .line 612
    :cond_1
    iget-object v0, p0, Lhgo;->P:Ljpl;

    invoke-virtual {v0, p1, p2}, Ljpl;->a(Ljava/lang/String;Z)V

    .line 614
    iput-object p1, p0, Lhgo;->w:Ljava/lang/String;

    .line 615
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 616
    iget-object v0, p0, Lhgo;->o:Lbb;

    iget v1, p0, Lhgo;->C:I

    invoke-virtual {v0, v1}, Lbb;->a(I)V

    .line 617
    iget-object v0, p0, Lhgo;->o:Lbb;

    iget v1, p0, Lhgo;->D:I

    invoke-virtual {v0, v1}, Lbb;->a(I)V

    .line 618
    iget-object v0, p0, Lhgo;->o:Lbb;

    iget v1, p0, Lhgo;->F:I

    invoke-virtual {v0, v1}, Lbb;->a(I)V

    .line 619
    iget-object v0, p0, Lhgo;->o:Lbb;

    iget v1, p0, Lhgo;->G:I

    invoke-virtual {v0, v1}, Lbb;->a(I)V

    .line 620
    iget-object v0, p0, Lhgo;->o:Lbb;

    iget v1, p0, Lhgo;->H:I

    invoke-virtual {v0, v1}, Lbb;->a(I)V

    .line 621
    iget-object v0, p0, Lhgo;->N:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 622
    invoke-virtual {p0}, Lhgo;->at()V

    goto :goto_0

    .line 624
    :cond_2
    invoke-direct {p0}, Lhgo;->d()Landroid/os/Bundle;

    move-result-object v0

    .line 629
    iget v1, p0, Lhgo;->s:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_3

    .line 630
    iget-object v1, p0, Lhgo;->o:Lbb;

    iget v2, p0, Lhgo;->C:I

    invoke-virtual {v1, v2, v0, p0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 633
    :cond_3
    iget-object v1, p0, Lhgo;->o:Lbb;

    iget v2, p0, Lhgo;->D:I

    invoke-virtual {v1, v2, v0, p0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 634
    iget-object v1, p0, Lhgo;->o:Lbb;

    iget v2, p0, Lhgo;->F:I

    invoke-virtual {v1, v2, v0, p0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 636
    iget-object v1, p0, Lhgo;->p:Lhei;

    iget v2, p0, Lhgo;->r:I

    invoke-interface {v1, v2}, Lhei;->a(I)Lhej;

    move-result-object v1

    .line 637
    const-string v2, "is_dasher_account"

    invoke-interface {v1, v2}, Lhej;->c(Ljava/lang/String;)Z

    move-result v1

    .line 639
    iget-boolean v2, p0, Lhgo;->T:Z

    if-nez v2, :cond_4

    if-nez v1, :cond_4

    .line 640
    iget-object v1, p0, Lhgo;->o:Lbb;

    iget v2, p0, Lhgo;->G:I

    invoke-virtual {v1, v2, v0, p0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 643
    :cond_4
    iget-boolean v1, p0, Lhgo;->v:Z

    if-eqz v1, :cond_0

    .line 644
    iget-object v1, p0, Lhgo;->O:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->clear()V

    .line 645
    iget-object v1, p0, Lhgo;->N:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->clear()V

    .line 650
    iget-object v1, p0, Lhgo;->o:Lbb;

    iget v2, p0, Lhgo;->H:I

    invoke-virtual {v1, v2}, Lbb;->a(I)V

    .line 652
    iget-object v1, p0, Lhgo;->N:Ljava/util/HashSet;

    iget v2, p0, Lhgo;->H:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 653
    iget-object v1, p0, Lhgo;->o:Lbb;

    iget v2, p0, Lhgo;->H:I

    invoke-virtual {v1, v2, v0, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    goto/16 :goto_0
.end method

.method static synthetic a(Lhgo;)[Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lhgo;->K:[Landroid/database/Cursor;

    return-object v0
.end method

.method private b(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 712
    const-string v1, "g:"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private b(ILandroid/database/Cursor;)V
    .locals 3

    .prologue
    .line 843
    iget-object v0, p0, Lhgo;->M:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 844
    if-eqz v0, :cond_0

    if-eq v0, p2, :cond_0

    .line 845
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 846
    iget-object v1, p0, Lhgo;->M:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 848
    :cond_0
    if-eqz p2, :cond_1

    if-eq v0, p2, :cond_1

    .line 849
    iget-object v0, p0, Lhgo;->M:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 851
    :cond_1
    return-void
.end method

.method private c(ILandroid/database/Cursor;)I
    .locals 12

    .prologue
    .line 967
    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    .line 968
    :cond_0
    iget-object v0, p0, Lhgo;->O:Ljava/util/HashSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 969
    iget-object v0, p0, Lhgo;->N:Ljava/util/HashSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 970
    const/4 v11, 0x0

    .line 1015
    :goto_0
    return v11

    .line 972
    :cond_1
    iget-object v0, p0, Lhgo;->O:Ljava/util/HashSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 974
    const/4 v0, 0x0

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 977
    iget-object v1, p0, Lhgo;->P:Ljpl;

    invoke-virtual {v1}, Ljpl;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 978
    const/4 v11, 0x0

    goto :goto_0

    .line 981
    :cond_2
    iget-object v0, p0, Lhgo;->N:Ljava/util/HashSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 983
    const/4 v0, 0x0

    .line 984
    const/4 v1, 0x1

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 985
    iget-object v2, p0, Lhgo;->P:Ljpl;

    invoke-virtual {v2, v1}, Ljpl;->a(Ljava/lang/String;)V

    move v11, v0

    .line 987
    :cond_3
    :goto_1
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 988
    const/4 v0, 0x2

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 989
    const/4 v0, 0x1

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 990
    const/4 v0, 0x3

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 991
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 992
    const-string v0, "AclPickerSearchAdapter"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 993
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x3f

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v1, v4

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "empty personId for gaiaId/name "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", from AutocompleteMergedPeople"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 998
    :cond_4
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lhgo;->B:Ljava/util/Set;

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 999
    :cond_5
    iget-object v0, p0, Lhgo;->P:Ljpl;

    const/4 v4, 0x5

    .line 1002
    invoke-interface {p2, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    const/4 v5, 0x4

    .line 1003
    invoke-interface {p2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x6

    .line 1004
    invoke-interface {p2, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x7

    .line 1005
    invoke-interface {p2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    if-eqz v7, :cond_6

    const/4 v7, 0x1

    :goto_2
    const/16 v8, 0x8

    .line 1006
    invoke-interface {p2, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    if-eqz v8, :cond_7

    const/4 v8, 0x1

    :goto_3
    const/16 v9, 0x9

    .line 1007
    invoke-interface {p2, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    const/16 v10, 0xa

    .line 1008
    invoke-interface {p2, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 1001
    invoke-virtual/range {v0 .. v10}, Ljpl;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZZILjava/lang/String;)Z

    move-result v0

    .line 1009
    if-eqz v0, :cond_9

    .line 1010
    add-int/lit8 v0, v11, 0x1

    :goto_4
    move v11, v0

    .line 1012
    goto/16 :goto_1

    .line 1005
    :cond_6
    const/4 v7, 0x0

    goto :goto_2

    .line 1006
    :cond_7
    const/4 v8, 0x0

    goto :goto_3

    .line 1014
    :cond_8
    invoke-direct {p0}, Lhgo;->e()V

    goto/16 :goto_0

    :cond_9
    move v0, v11

    goto :goto_4
.end method

.method private d()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 706
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 707
    const-string v1, "query"

    iget-object v2, p0, Lhgo;->w:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 708
    return-object v0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 1019
    iget-object v0, p0, Lhgo;->P:Ljpl;

    invoke-virtual {v0}, Ljpl;->m()Landroid/database/Cursor;

    move-result-object v0

    .line 1020
    const/4 v1, 0x4

    invoke-virtual {p0, v1, v0}, Lhgo;->a(ILandroid/database/Cursor;)V

    .line 1021
    return-void
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 752
    invoke-virtual {p0}, Lhgo;->as()Landroid/content/Context;

    move-result-object v1

    .line 753
    iget v0, p0, Lhgo;->C:I

    if-ne p1, v0, :cond_1

    .line 754
    new-instance v0, Lhxg;

    iget v2, p0, Lhgo;->r:I

    iget v3, p0, Lhgo;->s:I

    sget-object v4, Lhgo;->e:[Ljava/lang/String;

    iget-object v5, p0, Lhgo;->w:Ljava/lang/String;

    .line 755
    if-eqz v5, :cond_0

    const-string v6, "(circle_name LIKE "

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "%"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    :goto_0
    const/16 v6, 0xa

    invoke-direct/range {v0 .. v6}, Lhxg;-><init>(Landroid/content/Context;II[Ljava/lang/String;Ljava/lang/String;I)V

    .line 788
    :goto_1
    return-object v0

    .line 755
    :cond_0
    const/4 v5, 0x0

    goto :goto_0

    .line 757
    :cond_1
    iget v0, p0, Lhgo;->D:I

    if-ne p1, v0, :cond_3

    .line 758
    new-instance v0, Lktu;

    iget v2, p0, Lhgo;->r:I

    sget-object v3, Lhgo;->f:[Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lktu;-><init>(Landroid/content/Context;I[Ljava/lang/String;)V

    iget-object v1, p0, Lhgo;->w:Ljava/lang/String;

    .line 759
    invoke-virtual {v0, v1}, Lktu;->c(Ljava/lang/String;)Lktu;

    move-result-object v0

    const/4 v1, 0x1

    .line 760
    invoke-virtual {v0, v1}, Lktu;->a(I)Lktu;

    move-result-object v1

    iget-object v0, p0, Lhgo;->x:Ljava/lang/String;

    .line 761
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_2
    invoke-virtual {v1, v0}, Lktu;->a(Z)Lktu;

    move-result-object v0

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    .line 763
    :cond_3
    iget v0, p0, Lhgo;->E:I

    if-ne p1, v0, :cond_4

    .line 764
    iget-object v0, p0, Lhgo;->J:Lhgs;

    iget v2, p0, Lhgo;->r:I

    sget-object v3, Lhgo;->i:[Ljava/lang/String;

    iget-boolean v4, p0, Lhgo;->y:Z

    iget-boolean v5, p0, Lhgo;->T:Z

    invoke-interface/range {v0 .. v5}, Lhgs;->a(Landroid/content/Context;I[Ljava/lang/String;ZZ)Ldo;

    move-result-object v0

    goto :goto_1

    .line 767
    :cond_4
    iget v0, p0, Lhgo;->G:I

    if-ne p1, v0, :cond_5

    .line 768
    iget-object v0, p0, Lhgo;->J:Lhgs;

    iget v2, p0, Lhgo;->r:I

    sget-object v2, Lhgo;->h:[Ljava/lang/String;

    iget-object v3, p0, Lhgo;->w:Ljava/lang/String;

    const/4 v4, 0x2

    const/4 v5, 0x0

    invoke-interface/range {v0 .. v5}, Lhgs;->a(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;IZ)Ldo;

    move-result-object v0

    goto :goto_1

    .line 774
    :cond_5
    iget v0, p0, Lhgo;->F:I

    if-ne p1, v0, :cond_6

    .line 775
    iget-object v0, p0, Lhgo;->J:Lhgs;

    iget v2, p0, Lhgo;->r:I

    sget-object v3, Lhgo;->g:[Ljava/lang/String;

    iget-object v4, p0, Lhgo;->w:Ljava/lang/String;

    iget-boolean v5, p0, Lhgo;->y:Z

    iget-boolean v6, p0, Lhgo;->z:Z

    iget-boolean v7, p0, Lhgo;->T:Z

    const/4 v8, 0x0

    const/16 v9, 0xa

    invoke-interface/range {v0 .. v9}, Lhgs;->a(Landroid/content/Context;I[Ljava/lang/String;Ljava/lang/String;ZZZLjava/lang/String;I)Ldo;

    move-result-object v0

    goto :goto_1

    .line 779
    :cond_6
    iget v0, p0, Lhgo;->H:I

    if-ne p1, v0, :cond_7

    .line 780
    iget-object v0, p0, Lhgo;->J:Lhgs;

    iget v2, p0, Lhgo;->r:I

    sget-object v3, Lhgo;->j:[Ljava/lang/String;

    iget-object v4, p0, Lhgo;->w:Ljava/lang/String;

    const/4 v5, 0x2

    iget-boolean v6, p0, Lhgo;->T:Z

    const/4 v7, 0x0

    invoke-interface/range {v0 .. v7}, Lhgs;->a(Landroid/content/Context;I[Ljava/lang/String;Ljava/lang/String;IZLjava/lang/String;)Ldo;

    move-result-object v0

    goto :goto_1

    .line 784
    :cond_7
    iget v0, p0, Lhgo;->I:I

    if-ne p1, v0, :cond_8

    .line 785
    iget-object v0, p0, Lhgo;->J:Lhgs;

    iget v2, p0, Lhgo;->r:I

    sget-object v3, Lhgo;->l:[Ljava/lang/String;

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Lhgs;->a(Landroid/content/Context;I[Ljava/lang/String;Ljava/util/ArrayList;)Ldo;

    move-result-object v0

    goto/16 :goto_1

    .line 788
    :cond_8
    const/4 v0, 0x0

    goto/16 :goto_1
.end method

.method public a(ILandroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 472
    iget-object v0, p0, Lhgo;->L:Ljava/util/Set;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhgo;->L:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 478
    :cond_0
    :goto_0
    return-void

    .line 475
    :cond_1
    iget-object v0, p0, Lhgo;->K:[Landroid/database/Cursor;

    aput-object p2, v0, p1

    .line 476
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-virtual {p0, v0}, Lhgo;->b(I)Landroid/database/Cursor;

    move-result-object v0

    .line 477
    invoke-super {p0, p1, v0}, Lhgl;->a(ILandroid/database/Cursor;)V

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 686
    if-eqz p1, :cond_0

    .line 687
    const-class v0, Lhgo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 688
    const-string v0, "search_list_adapter.query"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhgo;->w:Ljava/lang/String;

    .line 689
    const-string v0, "search_list_adapter.results"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lhgo;->Q:Z

    if-nez v0, :cond_0

    .line 690
    const-string v0, "search_list_adapter.results"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Ljpl;

    iput-object v0, p0, Lhgo;->P:Ljpl;

    .line 693
    :cond_0
    return-void
.end method

.method protected a(Landroid/view/View;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)V
    .locals 9

    .prologue
    .line 580
    check-cast p1, Lcom/google/android/libraries/social/acl/AclPickerListRow;

    .line 582
    iget-object v0, p0, Lhgo;->K:[Landroid/database/Cursor;

    aget-object v4, v0, p2

    .line 583
    invoke-interface {v4}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 584
    iget v1, p0, Lhgo;->d:I

    mul-int v3, p4, v1

    .line 585
    iget v1, p0, Lhgo;->d:I

    add-int/2addr v1, v3

    .line 586
    if-le v1, v0, :cond_0

    :goto_0
    move v2, v3

    .line 589
    :goto_1
    if-ge v2, v0, :cond_2

    .line 590
    invoke-interface {v4, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 592
    new-instance v5, Lhfw;

    invoke-direct {v5}, Lhfw;-><init>()V

    .line 593
    iget-object v6, p0, Lhgo;->V:Lhgq;

    invoke-virtual {v6, p2, v2, v5}, Lhgq;->a(IILhfw;)V

    .line 594
    sub-int v6, v2, v3

    iget-object v7, p0, Lhgo;->V:Lhgq;

    iget-object v8, p0, Lhgo;->b:Lhgt;

    .line 595
    invoke-virtual {v8, v5}, Lhgt;->b(Lhfw;)Z

    move-result v8

    .line 594
    invoke-virtual {p1, v6, v7, v5, v8}, Lcom/google/android/libraries/social/acl/AclPickerListRow;->a(ILhfv;Lhfw;Z)V

    .line 589
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_0
    move v0, v1

    .line 586
    goto :goto_0

    .line 598
    :goto_2
    if-ge v0, v1, :cond_1

    .line 599
    sub-int v2, v0, v3

    const/4 v4, 0x4

    invoke-virtual {p1, v2, v4}, Lcom/google/android/libraries/social/acl/AclPickerListRow;->a(II)V

    .line 598
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 601
    :cond_1
    return-void

    :cond_2
    move v0, v2

    goto :goto_2
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1051
    return-void
.end method

.method public a(Ldo;Landroid/database/Cursor;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v12, 0x2

    const/4 v11, 0x0

    const/4 v2, 0x0

    const/4 v10, 0x1

    .line 793
    invoke-virtual {p1}, Ldo;->o()I

    move-result v0

    .line 794
    iget v1, p0, Lhgo;->C:I

    if-ne v0, v1, :cond_2

    .line 795
    if-nez p2, :cond_1

    move v0, v10

    :goto_0
    iput-boolean v0, p0, Lhgo;->t:Z

    .line 796
    iget-boolean v0, p0, Lhgo;->t:Z

    if-nez v0, :cond_0

    .line 798
    invoke-virtual {p0, v10, p2}, Lhgo;->a(ILandroid/database/Cursor;)V

    .line 832
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v11

    .line 795
    goto :goto_0

    .line 800
    :cond_2
    iget v1, p0, Lhgo;->D:I

    if-ne v0, v1, :cond_4

    .line 801
    if-nez p2, :cond_3

    :goto_2
    iput-boolean v10, p0, Lhgo;->u:Z

    .line 802
    iget-boolean v0, p0, Lhgo;->u:Z

    if-nez v0, :cond_0

    .line 804
    invoke-virtual {p0, v12, p2}, Lhgo;->a(ILandroid/database/Cursor;)V

    goto :goto_1

    :cond_3
    move v10, v11

    .line 801
    goto :goto_2

    .line 806
    :cond_4
    iget v1, p0, Lhgo;->E:I

    if-ne v0, v1, :cond_9

    .line 807
    iget-object v0, p0, Lhgo;->R:Landroid/database/Cursor;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lhgo;->R:Landroid/database/Cursor;

    if-eq v0, p2, :cond_5

    iget-object v0, p0, Lhgo;->R:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_5
    iput-object p2, p0, Lhgo;->R:Landroid/database/Cursor;

    iget-object v0, p0, Lhgo;->P:Ljpl;

    invoke-virtual {v0}, Ljpl;->f()V

    if-eqz p2, :cond_8

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_8

    :cond_6
    invoke-interface {p2, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lhgo;->B:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    iget-object v1, p0, Lhgo;->P:Ljpl;

    invoke-interface {p2, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljpl;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_6

    :cond_8
    iget-object v0, p0, Lhgo;->P:Ljpl;

    invoke-virtual {v0}, Ljpl;->g()V

    invoke-direct {p0}, Lhgo;->e()V

    goto :goto_1

    .line 808
    :cond_9
    iget v1, p0, Lhgo;->G:I

    if-ne v0, v1, :cond_10

    .line 809
    iget-object v0, p0, Lhgo;->w:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_e

    iget-object v0, p0, Lhgo;->w:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/util/Rfc822Tokenizer;->tokenize(Ljava/lang/CharSequence;)[Landroid/text/util/Rfc822Token;

    move-result-object v0

    if-eqz v0, :cond_e

    array-length v1, v0

    if-lez v1, :cond_e

    aget-object v0, v0, v11

    invoke-virtual {v0}, Landroid/text/util/Rfc822Token;->getAddress()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_e

    sget-object v0, Lhgo;->m:Ljava/util/regex/Pattern;

    invoke-virtual {v0, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_e

    :goto_3
    iget-object v0, p0, Lhgo;->S:Landroid/database/Cursor;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lhgo;->S:Landroid/database/Cursor;

    if-eq v0, p2, :cond_a

    iget-object v0, p0, Lhgo;->S:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_a
    iput-object p2, p0, Lhgo;->S:Landroid/database/Cursor;

    iget-object v0, p0, Lhgo;->P:Ljpl;

    invoke-virtual {v0}, Ljpl;->j()V

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_b

    iget-object v0, p0, Lhgo;->P:Ljpl;

    const-string v4, "e:"

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_f

    invoke-virtual {v4, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_4
    move-object v4, v3

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v6}, Ljpl;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_b
    if-eqz p2, :cond_d

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_d

    :cond_c
    iget-object v3, p0, Lhgo;->P:Ljpl;

    invoke-interface {p2, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {p2, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {p2, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v0, 0x3

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    move-object v8, v2

    move-object v9, v2

    invoke-virtual/range {v3 .. v9}, Ljpl;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_c

    :cond_d
    iget-object v0, p0, Lhgo;->P:Ljpl;

    invoke-virtual {v0}, Ljpl;->k()V

    invoke-direct {p0}, Lhgo;->e()V

    goto/16 :goto_1

    :cond_e
    move-object v3, v2

    goto :goto_3

    :cond_f
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_4

    .line 812
    :cond_10
    iget v1, p0, Lhgo;->F:I

    if-ne v0, v1, :cond_11

    .line 813
    invoke-direct {p0, v0, p2}, Lhgo;->b(ILandroid/database/Cursor;)V

    .line 816
    iget-boolean v0, p0, Lhgo;->A:Z

    invoke-direct {p0, p2, v0}, Lhgo;->a(Landroid/database/Cursor;Z)V

    goto/16 :goto_1

    .line 817
    :cond_11
    iget v1, p0, Lhgo;->H:I

    if-ne v0, v1, :cond_13

    .line 818
    iget-object v1, p0, Lhgo;->n:Landroid/database/Cursor;

    if-eq p2, v1, :cond_12

    .line 819
    invoke-direct {p0, v0, p2}, Lhgo;->b(ILandroid/database/Cursor;)V

    .line 820
    invoke-direct {p0, v0, p2}, Lhgo;->c(ILandroid/database/Cursor;)I

    .line 822
    :cond_12
    iget-object v0, p0, Lhgo;->N:Ljava/util/HashSet;

    iget v1, p0, Lhgo;->H:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 823
    :cond_13
    iget v1, p0, Lhgo;->I:I

    if-ne v0, v1, :cond_0

    .line 824
    iget-object v0, p0, Lhgo;->B:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 825
    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 827
    :cond_14
    iget-object v0, p0, Lhgo;->B:Ljava/util/Set;

    invoke-interface {p2, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 828
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_14

    goto/16 :goto_1
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 54
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lhgo;->a(Ldo;Landroid/database/Cursor;)V

    return-void
.end method

.method public a(Lhfv;[I)V
    .locals 4

    .prologue
    .line 466
    iput-object p1, p0, Lhgo;->c:Lhfv;

    .line 467
    new-instance v1, Ljava/util/TreeSet;

    invoke-direct {v1}, Ljava/util/TreeSet;-><init>()V

    const/4 v0, 0x0

    :goto_0
    array-length v2, p2

    if-ge v0, v2, :cond_0

    sget-object v2, Lhgo;->a:[I

    aget v3, p2, v0

    aget v2, v2, v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iput-object v1, p0, Lhgo;->L:Ljava/util/Set;

    .line 468
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 604
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lhgo;->a(Ljava/lang/String;Z)V

    .line 605
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 405
    iput-boolean p1, p0, Lhgo;->A:Z

    .line 406
    return-void
.end method

.method public b()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 716
    iget-object v0, p0, Lhgo;->p:Lhei;

    iget v1, p0, Lhgo;->r:I

    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 718
    iget-boolean v1, p0, Lhgo;->A:Z

    if-nez v1, :cond_0

    .line 719
    const-string v1, "gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 720
    invoke-direct {p0, v1}, Lhgo;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 721
    iget-object v2, p0, Lhgo;->P:Ljpl;

    invoke-virtual {v2, v1}, Ljpl;->b(Ljava/lang/String;)V

    .line 723
    :cond_0
    iget-object v1, p0, Lhgo;->q:Lhxh;

    invoke-virtual {v1}, Lhxh;->b()V

    .line 724
    iget-object v1, p0, Lhgo;->o:Lbb;

    iget v2, p0, Lhgo;->E:I

    invoke-virtual {v1, v2, v3, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 725
    iget-object v1, p0, Lhgo;->o:Lbb;

    iget v2, p0, Lhgo;->I:I

    invoke-virtual {v1, v2, v3, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 726
    const-string v1, "domain_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lhgo;->x:Ljava/lang/String;

    .line 727
    invoke-direct {p0}, Lhgo;->d()Landroid/os/Bundle;

    move-result-object v1

    .line 728
    iget v2, p0, Lhgo;->s:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    iget-object v2, p0, Lhgo;->w:Ljava/lang/String;

    .line 729
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 730
    iget-object v2, p0, Lhgo;->o:Lbb;

    iget v3, p0, Lhgo;->C:I

    invoke-virtual {v2, v3, v1, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 732
    :cond_1
    iget-object v2, p0, Lhgo;->w:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 733
    iget-object v2, p0, Lhgo;->o:Lbb;

    iget v3, p0, Lhgo;->D:I

    invoke-virtual {v2, v3, v1, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 735
    :cond_2
    iget-object v2, p0, Lhgo;->w:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 736
    iget-object v2, p0, Lhgo;->o:Lbb;

    iget v3, p0, Lhgo;->F:I

    invoke-virtual {v2, v3, v1, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 738
    :cond_3
    iget-boolean v2, p0, Lhgo;->T:Z

    if-nez v2, :cond_4

    const-string v2, "is_dasher_account"

    invoke-interface {v0, v2}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 739
    iget-object v0, p0, Lhgo;->o:Lbb;

    iget v2, p0, Lhgo;->G:I

    invoke-virtual {v0, v2, v1, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 741
    :cond_4
    iget-boolean v0, p0, Lhgo;->v:Z

    if-eqz v0, :cond_5

    .line 742
    iget-object v0, p0, Lhgo;->N:Ljava/util/HashSet;

    iget v2, p0, Lhgo;->H:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 743
    iget-object v0, p0, Lhgo;->o:Lbb;

    iget v2, p0, Lhgo;->H:I

    invoke-virtual {v0, v2, v1, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 745
    :cond_5
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 699
    const-string v0, "search_list_adapter.query"

    iget-object v1, p0, Lhgo;->w:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 700
    iget-object v0, p0, Lhgo;->P:Ljpl;

    invoke-virtual {v0}, Ljpl;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 701
    const-string v0, "search_list_adapter.results"

    iget-object v1, p0, Lhgo;->P:Ljpl;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 703
    :cond_0
    return-void
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 423
    iput-boolean p1, p0, Lhgo;->T:Z

    .line 424
    return-void
.end method

.method public c()V
    .locals 0

    .prologue
    .line 748
    return-void
.end method

.method public c(I)V
    .locals 2

    .prologue
    .line 412
    iget v0, p0, Lhgo;->s:I

    if-eq v0, p1, :cond_0

    .line 413
    iput p1, p0, Lhgo;->s:I

    .line 415
    iget-object v0, p0, Lhgo;->w:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lhgo;->a(Ljava/lang/String;Z)V

    .line 417
    :cond_0
    return-void
.end method

.method public c(Z)V
    .locals 0

    .prologue
    .line 431
    iput-boolean p1, p0, Lhgo;->v:Z

    .line 432
    return-void
.end method

.method public d(Z)V
    .locals 0

    .prologue
    .line 446
    iput-boolean p1, p0, Lhgo;->y:Z

    .line 447
    return-void
.end method

.method public e(Z)V
    .locals 2

    .prologue
    .line 453
    iput-boolean p1, p0, Lhgo;->z:Z

    .line 454
    iget-object v0, p0, Lhgo;->P:Ljpl;

    iget-boolean v1, p0, Lhgo;->z:Z

    invoke-virtual {v0, v1}, Ljpl;->a(Z)V

    .line 455
    return-void
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 666
    iget-object v0, p0, Lhgo;->w:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lhgo;->q:Lhxh;

    invoke-virtual {v0}, Lhxh;->a()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic onClick(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 54
    invoke-super {p0, p1}, Lhgl;->onClick(Landroid/view/View;)V

    return-void
.end method
