.class public final Lpjk;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lpjk;


# instance fields
.field private b:Ljava/lang/Long;

.field private c:I

.field private d:Ljava/lang/String;

.field private e:[B

.field private f:[B

.field private g:Lpjt;

.field private h:Lpjj;

.field private i:[B

.field private j:Ljava/lang/Integer;

.field private k:[B

.field private l:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 584
    const/4 v0, 0x0

    new-array v0, v0, [Lpjk;

    sput-object v0, Lpjk;->a:[Lpjk;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 585
    invoke-direct {p0}, Loxq;-><init>()V

    .line 598
    const/high16 v0, -0x80000000

    iput v0, p0, Lpjk;->c:I

    .line 607
    iput-object v1, p0, Lpjk;->g:Lpjt;

    .line 610
    iput-object v1, p0, Lpjk;->h:Lpjj;

    .line 585
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 662
    const/4 v0, 0x0

    .line 663
    iget-object v1, p0, Lpjk;->b:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 664
    const/4 v0, 0x1

    iget-object v1, p0, Lpjk;->b:Ljava/lang/Long;

    .line 665
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Loxo;->f(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 667
    :cond_0
    iget v1, p0, Lpjk;->c:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_1

    .line 668
    const/4 v1, 0x2

    iget v2, p0, Lpjk;->c:I

    .line 669
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 671
    :cond_1
    iget-object v1, p0, Lpjk;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 672
    const/4 v1, 0x3

    iget-object v2, p0, Lpjk;->d:Ljava/lang/String;

    .line 673
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 675
    :cond_2
    iget-object v1, p0, Lpjk;->e:[B

    if-eqz v1, :cond_3

    .line 676
    const/4 v1, 0x4

    iget-object v2, p0, Lpjk;->e:[B

    .line 677
    invoke-static {v1, v2}, Loxo;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 679
    :cond_3
    iget-object v1, p0, Lpjk;->f:[B

    if-eqz v1, :cond_4

    .line 680
    const/4 v1, 0x5

    iget-object v2, p0, Lpjk;->f:[B

    .line 681
    invoke-static {v1, v2}, Loxo;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 683
    :cond_4
    iget-object v1, p0, Lpjk;->g:Lpjt;

    if-eqz v1, :cond_5

    .line 684
    const/4 v1, 0x6

    iget-object v2, p0, Lpjk;->g:Lpjt;

    .line 685
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 687
    :cond_5
    iget-object v1, p0, Lpjk;->h:Lpjj;

    if-eqz v1, :cond_6

    .line 688
    const/4 v1, 0x7

    iget-object v2, p0, Lpjk;->h:Lpjj;

    .line 689
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 691
    :cond_6
    iget-object v1, p0, Lpjk;->i:[B

    if-eqz v1, :cond_7

    .line 692
    const/16 v1, 0x8

    iget-object v2, p0, Lpjk;->i:[B

    .line 693
    invoke-static {v1, v2}, Loxo;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 695
    :cond_7
    iget-object v1, p0, Lpjk;->j:Ljava/lang/Integer;

    if-eqz v1, :cond_8

    .line 696
    const/16 v1, 0x9

    iget-object v2, p0, Lpjk;->j:Ljava/lang/Integer;

    .line 697
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 699
    :cond_8
    iget-object v1, p0, Lpjk;->k:[B

    if-eqz v1, :cond_9

    .line 700
    const/16 v1, 0xa

    iget-object v2, p0, Lpjk;->k:[B

    .line 701
    invoke-static {v1, v2}, Loxo;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 703
    :cond_9
    iget-object v1, p0, Lpjk;->l:[B

    if-eqz v1, :cond_a

    .line 704
    const/16 v1, 0xb

    iget-object v2, p0, Lpjk;->l:[B

    .line 705
    invoke-static {v1, v2}, Loxo;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 707
    :cond_a
    iget-object v1, p0, Lpjk;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 708
    iput v0, p0, Lpjk;->ai:I

    .line 709
    return v0
.end method

.method public a(Loxn;)Lpjk;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 717
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 718
    sparse-switch v0, :sswitch_data_0

    .line 722
    iget-object v1, p0, Lpjk;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 723
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpjk;->ah:Ljava/util/List;

    .line 726
    :cond_1
    iget-object v1, p0, Lpjk;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 728
    :sswitch_0
    return-object p0

    .line 733
    :sswitch_1
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lpjk;->b:Ljava/lang/Long;

    goto :goto_0

    .line 737
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 738
    if-eq v0, v2, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-ne v0, v1, :cond_3

    .line 743
    :cond_2
    iput v0, p0, Lpjk;->c:I

    goto :goto_0

    .line 745
    :cond_3
    iput v2, p0, Lpjk;->c:I

    goto :goto_0

    .line 750
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpjk;->d:Ljava/lang/String;

    goto :goto_0

    .line 754
    :sswitch_4
    invoke-virtual {p1}, Loxn;->l()[B

    move-result-object v0

    iput-object v0, p0, Lpjk;->e:[B

    goto :goto_0

    .line 758
    :sswitch_5
    invoke-virtual {p1}, Loxn;->l()[B

    move-result-object v0

    iput-object v0, p0, Lpjk;->f:[B

    goto :goto_0

    .line 762
    :sswitch_6
    iget-object v0, p0, Lpjk;->g:Lpjt;

    if-nez v0, :cond_4

    .line 763
    new-instance v0, Lpjt;

    invoke-direct {v0}, Lpjt;-><init>()V

    iput-object v0, p0, Lpjk;->g:Lpjt;

    .line 765
    :cond_4
    iget-object v0, p0, Lpjk;->g:Lpjt;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 769
    :sswitch_7
    iget-object v0, p0, Lpjk;->h:Lpjj;

    if-nez v0, :cond_5

    .line 770
    new-instance v0, Lpjj;

    invoke-direct {v0}, Lpjj;-><init>()V

    iput-object v0, p0, Lpjk;->h:Lpjj;

    .line 772
    :cond_5
    iget-object v0, p0, Lpjk;->h:Lpjj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 776
    :sswitch_8
    invoke-virtual {p1}, Loxn;->l()[B

    move-result-object v0

    iput-object v0, p0, Lpjk;->i:[B

    goto :goto_0

    .line 780
    :sswitch_9
    invoke-virtual {p1}, Loxn;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lpjk;->j:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 784
    :sswitch_a
    invoke-virtual {p1}, Loxn;->l()[B

    move-result-object v0

    iput-object v0, p0, Lpjk;->k:[B

    goto/16 :goto_0

    .line 788
    :sswitch_b
    invoke-virtual {p1}, Loxn;->l()[B

    move-result-object v0

    iput-object v0, p0, Lpjk;->l:[B

    goto/16 :goto_0

    .line 718
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4d -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 623
    iget-object v0, p0, Lpjk;->b:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 624
    const/4 v0, 0x1

    iget-object v1, p0, Lpjk;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 626
    :cond_0
    iget v0, p0, Lpjk;->c:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 627
    const/4 v0, 0x2

    iget v1, p0, Lpjk;->c:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 629
    :cond_1
    iget-object v0, p0, Lpjk;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 630
    const/4 v0, 0x3

    iget-object v1, p0, Lpjk;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 632
    :cond_2
    iget-object v0, p0, Lpjk;->e:[B

    if-eqz v0, :cond_3

    .line 633
    const/4 v0, 0x4

    iget-object v1, p0, Lpjk;->e:[B

    invoke-virtual {p1, v0, v1}, Loxo;->a(I[B)V

    .line 635
    :cond_3
    iget-object v0, p0, Lpjk;->f:[B

    if-eqz v0, :cond_4

    .line 636
    const/4 v0, 0x5

    iget-object v1, p0, Lpjk;->f:[B

    invoke-virtual {p1, v0, v1}, Loxo;->a(I[B)V

    .line 638
    :cond_4
    iget-object v0, p0, Lpjk;->g:Lpjt;

    if-eqz v0, :cond_5

    .line 639
    const/4 v0, 0x6

    iget-object v1, p0, Lpjk;->g:Lpjt;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 641
    :cond_5
    iget-object v0, p0, Lpjk;->h:Lpjj;

    if-eqz v0, :cond_6

    .line 642
    const/4 v0, 0x7

    iget-object v1, p0, Lpjk;->h:Lpjj;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 644
    :cond_6
    iget-object v0, p0, Lpjk;->i:[B

    if-eqz v0, :cond_7

    .line 645
    const/16 v0, 0x8

    iget-object v1, p0, Lpjk;->i:[B

    invoke-virtual {p1, v0, v1}, Loxo;->a(I[B)V

    .line 647
    :cond_7
    iget-object v0, p0, Lpjk;->j:Ljava/lang/Integer;

    if-eqz v0, :cond_8

    .line 648
    const/16 v0, 0x9

    iget-object v1, p0, Lpjk;->j:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->b(II)V

    .line 650
    :cond_8
    iget-object v0, p0, Lpjk;->k:[B

    if-eqz v0, :cond_9

    .line 651
    const/16 v0, 0xa

    iget-object v1, p0, Lpjk;->k:[B

    invoke-virtual {p1, v0, v1}, Loxo;->a(I[B)V

    .line 653
    :cond_9
    iget-object v0, p0, Lpjk;->l:[B

    if-eqz v0, :cond_a

    .line 654
    const/16 v0, 0xb

    iget-object v1, p0, Lpjk;->l:[B

    invoke-virtual {p1, v0, v1}, Loxo;->a(I[B)V

    .line 656
    :cond_a
    iget-object v0, p0, Lpjk;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 658
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 581
    invoke-virtual {p0, p1}, Lpjk;->a(Loxn;)Lpjk;

    move-result-object v0

    return-object v0
.end method
