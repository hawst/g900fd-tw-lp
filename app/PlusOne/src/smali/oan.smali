.class public final Loan;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lltr;

.field private b:Ljava/lang/String;

.field private c:Lltu;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 13
    iput-object v0, p0, Loan;->a:Lltr;

    .line 18
    iput-object v0, p0, Loan;->c:Lltu;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 38
    const/4 v0, 0x0

    .line 39
    iget-object v1, p0, Loan;->a:Lltr;

    if-eqz v1, :cond_0

    .line 40
    const/4 v0, 0x1

    iget-object v1, p0, Loan;->a:Lltr;

    .line 41
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 43
    :cond_0
    iget-object v1, p0, Loan;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 44
    const/4 v1, 0x2

    iget-object v2, p0, Loan;->b:Ljava/lang/String;

    .line 45
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 47
    :cond_1
    iget-object v1, p0, Loan;->c:Lltu;

    if-eqz v1, :cond_2

    .line 48
    const/4 v1, 0x3

    iget-object v2, p0, Loan;->c:Lltu;

    .line 49
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 51
    :cond_2
    iget-object v1, p0, Loan;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 52
    iput v0, p0, Loan;->ai:I

    .line 53
    return v0
.end method

.method public a(Loxn;)Loan;
    .locals 2

    .prologue
    .line 61
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 62
    sparse-switch v0, :sswitch_data_0

    .line 66
    iget-object v1, p0, Loan;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 67
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Loan;->ah:Ljava/util/List;

    .line 70
    :cond_1
    iget-object v1, p0, Loan;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 72
    :sswitch_0
    return-object p0

    .line 77
    :sswitch_1
    iget-object v0, p0, Loan;->a:Lltr;

    if-nez v0, :cond_2

    .line 78
    new-instance v0, Lltr;

    invoke-direct {v0}, Lltr;-><init>()V

    iput-object v0, p0, Loan;->a:Lltr;

    .line 80
    :cond_2
    iget-object v0, p0, Loan;->a:Lltr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 84
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loan;->b:Ljava/lang/String;

    goto :goto_0

    .line 88
    :sswitch_3
    iget-object v0, p0, Loan;->c:Lltu;

    if-nez v0, :cond_3

    .line 89
    new-instance v0, Lltu;

    invoke-direct {v0}, Lltu;-><init>()V

    iput-object v0, p0, Loan;->c:Lltu;

    .line 91
    :cond_3
    iget-object v0, p0, Loan;->c:Lltu;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 62
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 23
    iget-object v0, p0, Loan;->a:Lltr;

    if-eqz v0, :cond_0

    .line 24
    const/4 v0, 0x1

    iget-object v1, p0, Loan;->a:Lltr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 26
    :cond_0
    iget-object v0, p0, Loan;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 27
    const/4 v0, 0x2

    iget-object v1, p0, Loan;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 29
    :cond_1
    iget-object v0, p0, Loan;->c:Lltu;

    if-eqz v0, :cond_2

    .line 30
    const/4 v0, 0x3

    iget-object v1, p0, Loan;->c:Lltu;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 32
    :cond_2
    iget-object v0, p0, Loan;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 34
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Loan;->a(Loxn;)Loan;

    move-result-object v0

    return-object v0
.end method
