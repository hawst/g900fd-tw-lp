.class final Ldre;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private synthetic c:Ldqx;


# direct methods
.method public constructor <init>(Ldqx;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 602
    iput-object p1, p0, Ldre;->c:Ldqx;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 603
    iput-object p2, p0, Ldre;->a:Ljava/lang/String;

    .line 604
    iput-object p3, p0, Ldre;->b:Ljava/lang/String;

    .line 605
    return-void
.end method


# virtual methods
.method protected varargs a()Ljava/lang/Void;
    .locals 6

    .prologue
    const/16 v5, 0x5a

    const/4 v4, 0x0

    .line 609
    iget-object v0, p0, Ldre;->c:Ldqx;

    iget-object v1, p0, Ldre;->c:Ldqx;

    iget-object v1, v1, Ldqx;->b:Ldxb;

    invoke-interface {v1}, Ldxb;->j()Ldxa;

    move-result-object v1

    invoke-virtual {v1}, Ldxa;->a()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldqx;->a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 610
    if-eqz v1, :cond_1

    .line 611
    iget-object v0, p0, Ldre;->c:Ldqx;

    iget-object v0, v0, Ldqx;->a:Landroid/content/Context;

    const-class v2, Lkdv;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkdv;

    invoke-interface {v0}, Lkdv;->d()Lcom/google/android/libraries/social/filecache/FileCache;

    move-result-object v0

    .line 612
    invoke-static {v1, v5, v4}, Llrw;->a(Landroid/graphics/Bitmap;IZ)[B

    move-result-object v2

    .line 614
    if-eqz v2, :cond_0

    .line 615
    iget-object v3, p0, Ldre;->a:Ljava/lang/String;

    invoke-virtual {v0, v3, v2}, Lcom/google/android/libraries/social/filecache/FileCache;->write(Ljava/lang/String;[B)V

    .line 618
    :cond_0
    iget-object v2, p0, Ldre;->c:Ldqx;

    iget-object v2, v2, Ldqx;->a:Landroid/content/Context;

    sget v2, Ljvv;->b:I

    int-to-float v2, v2

    sget v3, Ljvv;->b:I

    int-to-float v3, v3

    invoke-static {v1, v2, v3}, Lfus;->a(Landroid/graphics/Bitmap;FF)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 620
    if-eqz v1, :cond_1

    .line 621
    invoke-static {v1, v5, v4}, Llrw;->a(Landroid/graphics/Bitmap;IZ)[B

    move-result-object v1

    .line 623
    if-eqz v1, :cond_1

    .line 624
    iget-object v2, p0, Ldre;->b:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/libraries/social/filecache/FileCache;->write(Ljava/lang/String;[B)V

    .line 629
    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method protected b()V
    .locals 4

    .prologue
    .line 634
    iget-object v0, p0, Ldre;->c:Ldqx;

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iget-object v2, v0, Ldqx;->e:Lnzi;

    invoke-static {v2}, Loxu;->a(Loxu;)[B

    move-result-object v2

    const-string v3, "edit_info"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    const/4 v2, -0x1

    invoke-virtual {v0, v2, v1}, Ldqx;->a(ILandroid/content/Intent;)V

    .line 635
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 598
    invoke-virtual {p0}, Ldre;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 598
    invoke-virtual {p0}, Ldre;->b()V

    return-void
.end method
