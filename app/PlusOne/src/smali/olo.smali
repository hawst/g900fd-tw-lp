.class public final Lolo;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lolo;


# instance fields
.field private b:I

.field private c:[Lomf;

.field private d:Ljava/lang/Integer;

.field private e:Lolu;

.field private f:Lols;

.field private g:Lolz;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Lolo;

    sput-object v0, Lolo;->a:[Lolo;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 24
    const/high16 v0, -0x80000000

    iput v0, p0, Lolo;->b:I

    .line 27
    sget-object v0, Lomf;->a:[Lomf;

    iput-object v0, p0, Lolo;->c:[Lomf;

    .line 32
    iput-object v1, p0, Lolo;->e:Lolu;

    .line 35
    iput-object v1, p0, Lolo;->f:Lols;

    .line 38
    iput-object v1, p0, Lolo;->g:Lolz;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 71
    .line 72
    iget v0, p0, Lolo;->b:I

    const/high16 v2, -0x80000000

    if-eq v0, v2, :cond_6

    .line 73
    const/4 v0, 0x1

    iget v2, p0, Lolo;->b:I

    .line 74
    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 76
    :goto_0
    iget-object v2, p0, Lolo;->c:[Lomf;

    if-eqz v2, :cond_1

    .line 77
    iget-object v2, p0, Lolo;->c:[Lomf;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 78
    if-eqz v4, :cond_0

    .line 79
    const/4 v5, 0x3

    .line 80
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 77
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 84
    :cond_1
    iget-object v1, p0, Lolo;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 85
    const/4 v1, 0x4

    iget-object v2, p0, Lolo;->d:Ljava/lang/Integer;

    .line 86
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 88
    :cond_2
    iget-object v1, p0, Lolo;->e:Lolu;

    if-eqz v1, :cond_3

    .line 89
    const/4 v1, 0x5

    iget-object v2, p0, Lolo;->e:Lolu;

    .line 90
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 92
    :cond_3
    iget-object v1, p0, Lolo;->f:Lols;

    if-eqz v1, :cond_4

    .line 93
    const/4 v1, 0x6

    iget-object v2, p0, Lolo;->f:Lols;

    .line 94
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 96
    :cond_4
    iget-object v1, p0, Lolo;->g:Lolz;

    if-eqz v1, :cond_5

    .line 97
    const/16 v1, 0xe

    iget-object v2, p0, Lolo;->g:Lolz;

    .line 98
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 100
    :cond_5
    iget-object v1, p0, Lolo;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 101
    iput v0, p0, Lolo;->ai:I

    .line 102
    return v0

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lolo;
    .locals 6

    .prologue
    const/16 v5, 0x1a

    const/16 v4, 0x11

    const/4 v1, 0x0

    .line 110
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 111
    sparse-switch v0, :sswitch_data_0

    .line 115
    iget-object v2, p0, Lolo;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 116
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lolo;->ah:Ljava/util/List;

    .line 119
    :cond_1
    iget-object v2, p0, Lolo;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 121
    :sswitch_0
    return-object p0

    .line 126
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 127
    if-eq v0, v4, :cond_2

    const/16 v2, 0x12

    if-eq v0, v2, :cond_2

    const/16 v2, 0x15

    if-eq v0, v2, :cond_2

    const/16 v2, 0x16

    if-eq v0, v2, :cond_2

    const/16 v2, 0x17

    if-eq v0, v2, :cond_2

    const/16 v2, 0x18

    if-eq v0, v2, :cond_2

    const/16 v2, 0x19

    if-eq v0, v2, :cond_2

    if-ne v0, v5, :cond_3

    .line 135
    :cond_2
    iput v0, p0, Lolo;->b:I

    goto :goto_0

    .line 137
    :cond_3
    iput v4, p0, Lolo;->b:I

    goto :goto_0

    .line 142
    :sswitch_2
    invoke-static {p1, v5}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 143
    iget-object v0, p0, Lolo;->c:[Lomf;

    if-nez v0, :cond_5

    move v0, v1

    .line 144
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lomf;

    .line 145
    iget-object v3, p0, Lolo;->c:[Lomf;

    if-eqz v3, :cond_4

    .line 146
    iget-object v3, p0, Lolo;->c:[Lomf;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 148
    :cond_4
    iput-object v2, p0, Lolo;->c:[Lomf;

    .line 149
    :goto_2
    iget-object v2, p0, Lolo;->c:[Lomf;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 150
    iget-object v2, p0, Lolo;->c:[Lomf;

    new-instance v3, Lomf;

    invoke-direct {v3}, Lomf;-><init>()V

    aput-object v3, v2, v0

    .line 151
    iget-object v2, p0, Lolo;->c:[Lomf;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 152
    invoke-virtual {p1}, Loxn;->a()I

    .line 149
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 143
    :cond_5
    iget-object v0, p0, Lolo;->c:[Lomf;

    array-length v0, v0

    goto :goto_1

    .line 155
    :cond_6
    iget-object v2, p0, Lolo;->c:[Lomf;

    new-instance v3, Lomf;

    invoke-direct {v3}, Lomf;-><init>()V

    aput-object v3, v2, v0

    .line 156
    iget-object v2, p0, Lolo;->c:[Lomf;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 160
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lolo;->d:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 164
    :sswitch_4
    iget-object v0, p0, Lolo;->e:Lolu;

    if-nez v0, :cond_7

    .line 165
    new-instance v0, Lolu;

    invoke-direct {v0}, Lolu;-><init>()V

    iput-object v0, p0, Lolo;->e:Lolu;

    .line 167
    :cond_7
    iget-object v0, p0, Lolo;->e:Lolu;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 171
    :sswitch_5
    iget-object v0, p0, Lolo;->f:Lols;

    if-nez v0, :cond_8

    .line 172
    new-instance v0, Lols;

    invoke-direct {v0}, Lols;-><init>()V

    iput-object v0, p0, Lolo;->f:Lols;

    .line 174
    :cond_8
    iget-object v0, p0, Lolo;->f:Lols;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 178
    :sswitch_6
    iget-object v0, p0, Lolo;->g:Lolz;

    if-nez v0, :cond_9

    .line 179
    new-instance v0, Lolz;

    invoke-direct {v0}, Lolz;-><init>()V

    iput-object v0, p0, Lolo;->g:Lolz;

    .line 181
    :cond_9
    iget-object v0, p0, Lolo;->g:Lolz;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 111
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x1a -> :sswitch_2
        0x20 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
        0x72 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 43
    iget v0, p0, Lolo;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 44
    const/4 v0, 0x1

    iget v1, p0, Lolo;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 46
    :cond_0
    iget-object v0, p0, Lolo;->c:[Lomf;

    if-eqz v0, :cond_2

    .line 47
    iget-object v1, p0, Lolo;->c:[Lomf;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 48
    if-eqz v3, :cond_1

    .line 49
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 47
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 53
    :cond_2
    iget-object v0, p0, Lolo;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 54
    const/4 v0, 0x4

    iget-object v1, p0, Lolo;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 56
    :cond_3
    iget-object v0, p0, Lolo;->e:Lolu;

    if-eqz v0, :cond_4

    .line 57
    const/4 v0, 0x5

    iget-object v1, p0, Lolo;->e:Lolu;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 59
    :cond_4
    iget-object v0, p0, Lolo;->f:Lols;

    if-eqz v0, :cond_5

    .line 60
    const/4 v0, 0x6

    iget-object v1, p0, Lolo;->f:Lols;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 62
    :cond_5
    iget-object v0, p0, Lolo;->g:Lolz;

    if-eqz v0, :cond_6

    .line 63
    const/16 v0, 0xe

    iget-object v1, p0, Lolo;->g:Lolz;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 65
    :cond_6
    iget-object v0, p0, Lolo;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 67
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lolo;->a(Loxn;)Lolo;

    move-result-object v0

    return-object v0
.end method
