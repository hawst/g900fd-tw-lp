.class public final Lmkm;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmzg;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2023
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2026
    iput-object v0, p0, Lmkm;->apiHeader:Llyq;

    .line 2029
    iput-object v0, p0, Lmkm;->a:Lmzg;

    .line 2023
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 2046
    const/4 v0, 0x0

    .line 2047
    iget-object v1, p0, Lmkm;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 2048
    const/4 v0, 0x1

    iget-object v1, p0, Lmkm;->apiHeader:Llyq;

    .line 2049
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2051
    :cond_0
    iget-object v1, p0, Lmkm;->a:Lmzg;

    if-eqz v1, :cond_1

    .line 2052
    const/4 v1, 0x2

    iget-object v2, p0, Lmkm;->a:Lmzg;

    .line 2053
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2055
    :cond_1
    iget-object v1, p0, Lmkm;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2056
    iput v0, p0, Lmkm;->ai:I

    .line 2057
    return v0
.end method

.method public a(Loxn;)Lmkm;
    .locals 2

    .prologue
    .line 2065
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2066
    sparse-switch v0, :sswitch_data_0

    .line 2070
    iget-object v1, p0, Lmkm;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 2071
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmkm;->ah:Ljava/util/List;

    .line 2074
    :cond_1
    iget-object v1, p0, Lmkm;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2076
    :sswitch_0
    return-object p0

    .line 2081
    :sswitch_1
    iget-object v0, p0, Lmkm;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 2082
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmkm;->apiHeader:Llyq;

    .line 2084
    :cond_2
    iget-object v0, p0, Lmkm;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2088
    :sswitch_2
    iget-object v0, p0, Lmkm;->a:Lmzg;

    if-nez v0, :cond_3

    .line 2089
    new-instance v0, Lmzg;

    invoke-direct {v0}, Lmzg;-><init>()V

    iput-object v0, p0, Lmkm;->a:Lmzg;

    .line 2091
    :cond_3
    iget-object v0, p0, Lmkm;->a:Lmzg;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2066
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 2034
    iget-object v0, p0, Lmkm;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 2035
    const/4 v0, 0x1

    iget-object v1, p0, Lmkm;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2037
    :cond_0
    iget-object v0, p0, Lmkm;->a:Lmzg;

    if-eqz v0, :cond_1

    .line 2038
    const/4 v0, 0x2

    iget-object v1, p0, Lmkm;->a:Lmzg;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2040
    :cond_1
    iget-object v0, p0, Lmkm;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2042
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2019
    invoke-virtual {p0, p1}, Lmkm;->a(Loxn;)Lmkm;

    move-result-object v0

    return-object v0
.end method
