.class final Lhvc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic a:Lhvb;


# direct methods
.method constructor <init>(Lhvb;)V
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, Lhvc;->a:Lhvb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    const/16 v1, 0x190

    const/4 v3, 0x0

    const/4 v0, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    .line 80
    iget-object v4, p0, Lhvc;->a:Lhvb;

    iget-object v4, v4, Lhvb;->a:Lhts;

    if-nez v4, :cond_1

    .line 161
    :cond_0
    :goto_0
    return-void

    .line 83
    :cond_1
    iget-object v4, p0, Lhvc;->a:Lhvb;

    invoke-virtual {v4}, Lhvb;->invalidate()V

    .line 85
    iget-object v4, p0, Lhvc;->a:Lhvb;

    iget v5, v4, Lhvb;->f:I

    add-int/lit8 v5, v5, 0x1

    iput v5, v4, Lhvb;->f:I

    .line 86
    iget-object v4, p0, Lhvc;->a:Lhvb;

    iget v4, v4, Lhvb;->f:I

    const/4 v5, 0x2

    if-le v4, v5, :cond_2

    .line 87
    iget-object v4, p0, Lhvc;->a:Lhvb;

    iput v0, v4, Lhvb;->f:I

    .line 94
    :cond_2
    iget-object v4, p0, Lhvc;->a:Lhvb;

    iget v4, v4, Lhvb;->f:I

    packed-switch v4, :pswitch_data_0

    move v1, v2

    .line 145
    :goto_1
    iget-object v3, p0, Lhvc;->a:Lhvb;

    invoke-virtual {v3}, Lhvb;->getAlpha()F

    move-result v3

    .line 146
    cmpl-float v3, v3, v2

    if-eqz v3, :cond_3

    .line 147
    iget-object v3, p0, Lhvc;->a:Lhvb;

    invoke-virtual {v3, v2}, Lhvb;->setAlpha(F)V

    .line 150
    :cond_3
    cmpl-float v2, v2, v1

    if-eqz v2, :cond_4

    .line 151
    iget-object v2, p0, Lhvc;->a:Lhvb;

    invoke-virtual {v2}, Lhvb;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    .line 152
    invoke-virtual {v2, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    int-to-long v2, v0

    .line 153
    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 154
    invoke-static {}, Lhvb;->d()Landroid/view/animation/Interpolator;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 155
    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 158
    :cond_4
    iget-object v1, p0, Lhvc;->a:Lhvb;

    iget-boolean v1, v1, Lhvb;->e:Z

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 159
    invoke-static {}, Llsx;->d()Landroid/os/Handler;

    move-result-object v1

    int-to-long v2, v0

    invoke-virtual {v1, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 96
    :pswitch_0
    iget-object v4, p0, Lhvc;->a:Lhvb;

    iget v5, v4, Lhvb;->d:I

    add-int/lit8 v5, v5, 0x1

    iput v5, v4, Lhvb;->d:I

    .line 97
    iget-object v4, p0, Lhvc;->a:Lhvb;

    iget-object v4, v4, Lhvb;->a:Lhts;

    invoke-interface {v4}, Lhts;->a()I

    move-result v4

    .line 98
    iget-object v5, p0, Lhvc;->a:Lhvb;

    iget v5, v5, Lhvb;->d:I

    if-lt v5, v4, :cond_5

    .line 99
    iget-object v5, p0, Lhvc;->a:Lhvb;

    iput v0, v5, Lhvb;->d:I

    .line 101
    :cond_5
    iget-object v0, p0, Lhvc;->a:Lhvb;

    invoke-static {v0}, Lhvb;->a(Lhvb;)V

    .line 103
    iget-object v0, p0, Lhvc;->a:Lhvb;

    iget-object v0, v0, Lhvb;->c:Lhvd;

    if-eqz v0, :cond_6

    .line 104
    iget-object v0, p0, Lhvc;->a:Lhvb;

    iget-object v0, v0, Lhvb;->c:Lhvd;

    iget-object v5, p0, Lhvc;->a:Lhvb;

    iget v5, v5, Lhvb;->d:I

    invoke-interface {v0, v5, v4}, Lhvd;->a(II)V

    :cond_6
    move v0, v1

    move v1, v2

    move v2, v3

    .line 110
    goto :goto_1

    .line 115
    :pswitch_1
    iget-object v1, p0, Lhvc;->a:Lhvb;

    iget-object v1, v1, Lhvb;->b:Landroid/text/StaticLayout;

    if-eqz v1, :cond_7

    .line 116
    iget-object v1, p0, Lhvc;->a:Lhvb;

    iget-object v1, v1, Lhvb;->b:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    .line 117
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 121
    :cond_7
    :goto_2
    mul-int/lit8 v0, v0, 0x46

    .line 125
    const/16 v1, 0xbb8

    const/16 v3, 0x1f40

    .line 126
    invoke-static {v3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 125
    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    move v1, v2

    .line 127
    goto/16 :goto_1

    .line 117
    :cond_8
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    goto :goto_2

    :pswitch_2
    move v0, v1

    move v1, v3

    .line 134
    goto/16 :goto_1

    .line 94
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
