.class public final Lnbq;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lnwv;

.field public c:Lnbr;

.field private d:Lnym;

.field private e:Lnzx;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 208
    invoke-direct {p0}, Loxq;-><init>()V

    .line 213
    iput-object v0, p0, Lnbq;->b:Lnwv;

    .line 216
    iput-object v0, p0, Lnbq;->d:Lnym;

    .line 219
    iput-object v0, p0, Lnbq;->e:Lnzx;

    .line 222
    iput-object v0, p0, Lnbq;->c:Lnbr;

    .line 208
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 248
    const/4 v0, 0x0

    .line 249
    iget-object v1, p0, Lnbq;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 250
    const/4 v0, 0x1

    iget-object v1, p0, Lnbq;->a:Ljava/lang/String;

    .line 251
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 253
    :cond_0
    iget-object v1, p0, Lnbq;->b:Lnwv;

    if-eqz v1, :cond_1

    .line 254
    const/4 v1, 0x2

    iget-object v2, p0, Lnbq;->b:Lnwv;

    .line 255
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 257
    :cond_1
    iget-object v1, p0, Lnbq;->d:Lnym;

    if-eqz v1, :cond_2

    .line 258
    const/4 v1, 0x3

    iget-object v2, p0, Lnbq;->d:Lnym;

    .line 259
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 261
    :cond_2
    iget-object v1, p0, Lnbq;->e:Lnzx;

    if-eqz v1, :cond_3

    .line 262
    const/4 v1, 0x4

    iget-object v2, p0, Lnbq;->e:Lnzx;

    .line 263
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 265
    :cond_3
    iget-object v1, p0, Lnbq;->c:Lnbr;

    if-eqz v1, :cond_4

    .line 266
    const/4 v1, 0x5

    iget-object v2, p0, Lnbq;->c:Lnbr;

    .line 267
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 269
    :cond_4
    iget-object v1, p0, Lnbq;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 270
    iput v0, p0, Lnbq;->ai:I

    .line 271
    return v0
.end method

.method public a(Loxn;)Lnbq;
    .locals 2

    .prologue
    .line 279
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 280
    sparse-switch v0, :sswitch_data_0

    .line 284
    iget-object v1, p0, Lnbq;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 285
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnbq;->ah:Ljava/util/List;

    .line 288
    :cond_1
    iget-object v1, p0, Lnbq;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 290
    :sswitch_0
    return-object p0

    .line 295
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnbq;->a:Ljava/lang/String;

    goto :goto_0

    .line 299
    :sswitch_2
    iget-object v0, p0, Lnbq;->b:Lnwv;

    if-nez v0, :cond_2

    .line 300
    new-instance v0, Lnwv;

    invoke-direct {v0}, Lnwv;-><init>()V

    iput-object v0, p0, Lnbq;->b:Lnwv;

    .line 302
    :cond_2
    iget-object v0, p0, Lnbq;->b:Lnwv;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 306
    :sswitch_3
    iget-object v0, p0, Lnbq;->d:Lnym;

    if-nez v0, :cond_3

    .line 307
    new-instance v0, Lnym;

    invoke-direct {v0}, Lnym;-><init>()V

    iput-object v0, p0, Lnbq;->d:Lnym;

    .line 309
    :cond_3
    iget-object v0, p0, Lnbq;->d:Lnym;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 313
    :sswitch_4
    iget-object v0, p0, Lnbq;->e:Lnzx;

    if-nez v0, :cond_4

    .line 314
    new-instance v0, Lnzx;

    invoke-direct {v0}, Lnzx;-><init>()V

    iput-object v0, p0, Lnbq;->e:Lnzx;

    .line 316
    :cond_4
    iget-object v0, p0, Lnbq;->e:Lnzx;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 320
    :sswitch_5
    iget-object v0, p0, Lnbq;->c:Lnbr;

    if-nez v0, :cond_5

    .line 321
    new-instance v0, Lnbr;

    invoke-direct {v0}, Lnbr;-><init>()V

    iput-object v0, p0, Lnbq;->c:Lnbr;

    .line 323
    :cond_5
    iget-object v0, p0, Lnbq;->c:Lnbr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 280
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 227
    iget-object v0, p0, Lnbq;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 228
    const/4 v0, 0x1

    iget-object v1, p0, Lnbq;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 230
    :cond_0
    iget-object v0, p0, Lnbq;->b:Lnwv;

    if-eqz v0, :cond_1

    .line 231
    const/4 v0, 0x2

    iget-object v1, p0, Lnbq;->b:Lnwv;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 233
    :cond_1
    iget-object v0, p0, Lnbq;->d:Lnym;

    if-eqz v0, :cond_2

    .line 234
    const/4 v0, 0x3

    iget-object v1, p0, Lnbq;->d:Lnym;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 236
    :cond_2
    iget-object v0, p0, Lnbq;->e:Lnzx;

    if-eqz v0, :cond_3

    .line 237
    const/4 v0, 0x4

    iget-object v1, p0, Lnbq;->e:Lnzx;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 239
    :cond_3
    iget-object v0, p0, Lnbq;->c:Lnbr;

    if-eqz v0, :cond_4

    .line 240
    const/4 v0, 0x5

    iget-object v1, p0, Lnbq;->c:Lnbr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 242
    :cond_4
    iget-object v0, p0, Lnbq;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 244
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 204
    invoke-virtual {p0, p1}, Lnbq;->a(Loxn;)Lnbq;

    move-result-object v0

    return-object v0
.end method
