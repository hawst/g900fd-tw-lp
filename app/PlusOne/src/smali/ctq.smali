.class public Lctq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljlw;
.implements Ljma;
.implements Llnx;
.implements Llqz;
.implements Llra;
.implements Llrd;
.implements Llrg;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljlw",
        "<",
        "Lctq;",
        ">;",
        "Ljma",
        "<",
        "Lctz;",
        ">;",
        "Llnx;",
        "Llqz;",
        "Llra;",
        "Llrd;",
        "Llrg;"
    }
.end annotation


# static fields
.field private static final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:Lctz;

.field private final c:Ljlv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljlv",
            "<",
            "Lctq;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Landroid/os/Bundle;

.field private e:Landroid/app/Activity;

.field private f:I

.field private g:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    new-instance v0, Lctr;

    invoke-direct {v0}, Lctr;-><init>()V

    sput-object v0, Lctq;->a:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Llqr;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    new-instance v1, Ljlv;

    invoke-direct {v1, p0}, Ljlv;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, Lctq;->c:Ljlv;

    .line 84
    iput-object p1, p0, Lctq;->e:Landroid/app/Activity;

    .line 85
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 87
    sget-object v3, Lctq;->a:Ljava/util/Set;

    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 88
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "android.intent.extra.ALLOW_MULTIPLE"

    invoke-virtual {v3, v4, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "android.intent.action.SEND_MULTIPLE"

    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    move v2, v0

    :cond_1
    if-eqz v2, :cond_2

    const/4 v0, 0x2

    move v2, v0

    .line 89
    :goto_0
    if-eqz v1, :cond_3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    .line 90
    :goto_1
    const-string v1, "photo_picker_mode"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 93
    :goto_2
    iput-object v0, p0, Lctq;->d:Landroid/os/Bundle;

    .line 94
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 95
    return-void

    :cond_2
    move v2, v0

    .line 88
    goto :goto_0

    .line 89
    :cond_3
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    goto :goto_1

    :cond_4
    move-object v0, v1

    goto :goto_2
.end method


# virtual methods
.method public E_()V
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lctq;->b:Lctz;

    invoke-virtual {v0}, Lctz;->b()Ljlx;

    move-result-object v0

    invoke-interface {v0, p0}, Ljlx;->a(Ljma;)V

    .line 193
    return-void
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 172
    iput p1, p0, Lctq;->f:I

    .line 173
    invoke-virtual {p0}, Lctq;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 174
    const/4 v0, 0x0

    iput-boolean v0, p0, Lctq;->g:Z

    .line 177
    :cond_0
    iget-object v0, p0, Lctq;->e:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 178
    iget-object v0, p0, Lctq;->c:Ljlv;

    invoke-virtual {v0}, Ljlv;->a()V

    .line 179
    return-void
.end method

.method public a(Landroid/content/Context;Llnh;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 99
    const-class v0, Lctz;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctz;

    iput-object v0, p0, Lctq;->b:Lctz;

    .line 100
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 104
    if-eqz p1, :cond_0

    .line 105
    const-string v0, "com.google.android.apps.photos.selection.PickerModeModel.PickerMode"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lctq;->f:I

    .line 106
    const-string v0, "com.google.android.apps.photos.selection.PickerModeModel.StartedInMultiSelect"

    .line 107
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lctq;->g:Z

    .line 119
    :goto_0
    iget-object v0, p0, Lctq;->b:Lctz;

    invoke-virtual {v0}, Lctz;->b()Ljlx;

    move-result-object v0

    invoke-interface {v0, p0, v1}, Ljlx;->a(Ljma;Z)V

    .line 120
    return-void

    .line 108
    :cond_0
    iget-object v0, p0, Lctq;->d:Landroid/os/Bundle;

    if-eqz v0, :cond_3

    .line 109
    iget-object v0, p0, Lctq;->d:Landroid/os/Bundle;

    const-string v2, "photo_picker_mode"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lctq;->f:I

    .line 111
    iget v0, p0, Lctq;->f:I

    const/4 v2, 0x4

    if-eq v0, v2, :cond_1

    iget v0, p0, Lctq;->f:I

    const/4 v2, 0x2

    if-eq v0, v2, :cond_1

    iget v0, p0, Lctq;->f:I

    const/4 v2, 0x3

    if-ne v0, v2, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lctq;->g:Z

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    .line 117
    :cond_3
    iput v1, p0, Lctq;->f:I

    goto :goto_0
.end method

.method public a(Lctz;)V
    .locals 2

    .prologue
    .line 183
    invoke-virtual {p1}, Lctz;->a()Ljcn;

    move-result-object v0

    invoke-virtual {v0}, Ljcn;->k()I

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lctq;->f:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget v0, p0, Lctq;->f:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 186
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lctq;->a(I)V

    .line 188
    :cond_0
    return-void
.end method

.method public synthetic a_(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 30
    check-cast p1, Lctz;

    invoke-virtual {p0, p1}, Lctq;->a(Lctz;)V

    return-void
.end method

.method public b()Ljlx;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljlx",
            "<",
            "Lctq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 130
    iget-object v0, p0, Lctq;->c:Ljlv;

    return-object v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 124
    const-string v0, "com.google.android.apps.photos.selection.PickerModeModel.PickerMode"

    iget v1, p0, Lctq;->f:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 125
    const-string v0, "com.google.android.apps.photos.selection.PickerModeModel.StartedInMultiSelect"

    iget-boolean v1, p0, Lctq;->g:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 126
    return-void
.end method

.method public c()I
    .locals 1

    .prologue
    .line 137
    iget v0, p0, Lctq;->f:I

    return v0
.end method

.method public d()Z
    .locals 2

    .prologue
    .line 145
    iget v0, p0, Lctq;->f:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    iget v0, p0, Lctq;->f:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 153
    iget-boolean v0, p0, Lctq;->g:Z

    return v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 160
    iget v0, p0, Lctq;->f:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Z
    .locals 2

    .prologue
    .line 167
    iget v0, p0, Lctq;->f:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    iget v0, p0, Lctq;->f:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
