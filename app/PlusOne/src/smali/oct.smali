.class public final Loct;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Loct;


# instance fields
.field private b:[Ljava/lang/String;

.field private c:[Locu;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 154
    const/4 v0, 0x0

    new-array v0, v0, [Loct;

    sput-object v0, Loct;->a:[Loct;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 155
    invoke-direct {p0}, Loxq;-><init>()V

    .line 158
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Loct;->b:[Ljava/lang/String;

    .line 161
    sget-object v0, Locu;->a:[Locu;

    iput-object v0, p0, Loct;->c:[Locu;

    .line 155
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 184
    .line 185
    iget-object v0, p0, Loct;->b:[Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Loct;->b:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 187
    iget-object v3, p0, Loct;->b:[Ljava/lang/String;

    array-length v4, v3

    move v0, v1

    move v2, v1

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, v3, v0

    .line 189
    invoke-static {v5}, Loxo;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 187
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 192
    :cond_0
    iget-object v0, p0, Loct;->b:[Ljava/lang/String;

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v2

    .line 194
    :goto_1
    iget-object v2, p0, Loct;->c:[Locu;

    if-eqz v2, :cond_2

    .line 195
    iget-object v2, p0, Loct;->c:[Locu;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 196
    if-eqz v4, :cond_1

    .line 197
    const/4 v5, 0x2

    .line 198
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 195
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 202
    :cond_2
    iget-object v1, p0, Loct;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 203
    iput v0, p0, Loct;->ai:I

    .line 204
    return v0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public a(Loxn;)Loct;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 212
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 213
    sparse-switch v0, :sswitch_data_0

    .line 217
    iget-object v2, p0, Loct;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 218
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Loct;->ah:Ljava/util/List;

    .line 221
    :cond_1
    iget-object v2, p0, Loct;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 223
    :sswitch_0
    return-object p0

    .line 228
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 229
    iget-object v0, p0, Loct;->b:[Ljava/lang/String;

    array-length v0, v0

    .line 230
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 231
    iget-object v3, p0, Loct;->b:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 232
    iput-object v2, p0, Loct;->b:[Ljava/lang/String;

    .line 233
    :goto_1
    iget-object v2, p0, Loct;->b:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_2

    .line 234
    iget-object v2, p0, Loct;->b:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 235
    invoke-virtual {p1}, Loxn;->a()I

    .line 233
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 238
    :cond_2
    iget-object v2, p0, Loct;->b:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto :goto_0

    .line 242
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 243
    iget-object v0, p0, Loct;->c:[Locu;

    if-nez v0, :cond_4

    move v0, v1

    .line 244
    :goto_2
    add-int/2addr v2, v0

    new-array v2, v2, [Locu;

    .line 245
    iget-object v3, p0, Loct;->c:[Locu;

    if-eqz v3, :cond_3

    .line 246
    iget-object v3, p0, Loct;->c:[Locu;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 248
    :cond_3
    iput-object v2, p0, Loct;->c:[Locu;

    .line 249
    :goto_3
    iget-object v2, p0, Loct;->c:[Locu;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 250
    iget-object v2, p0, Loct;->c:[Locu;

    new-instance v3, Locu;

    invoke-direct {v3}, Locu;-><init>()V

    aput-object v3, v2, v0

    .line 251
    iget-object v2, p0, Loct;->c:[Locu;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 252
    invoke-virtual {p1}, Loxn;->a()I

    .line 249
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 243
    :cond_4
    iget-object v0, p0, Loct;->c:[Locu;

    array-length v0, v0

    goto :goto_2

    .line 255
    :cond_5
    iget-object v2, p0, Loct;->c:[Locu;

    new-instance v3, Locu;

    invoke-direct {v3}, Locu;-><init>()V

    aput-object v3, v2, v0

    .line 256
    iget-object v2, p0, Loct;->c:[Locu;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 213
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 166
    iget-object v1, p0, Loct;->b:[Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 167
    iget-object v2, p0, Loct;->b:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 168
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Loxo;->a(ILjava/lang/String;)V

    .line 167
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 171
    :cond_0
    iget-object v1, p0, Loct;->c:[Locu;

    if-eqz v1, :cond_2

    .line 172
    iget-object v1, p0, Loct;->c:[Locu;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 173
    if-eqz v3, :cond_1

    .line 174
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 172
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 178
    :cond_2
    iget-object v0, p0, Loct;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 180
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 151
    invoke-virtual {p0, p1}, Loct;->a(Loxn;)Loct;

    move-result-object v0

    return-object v0
.end method
