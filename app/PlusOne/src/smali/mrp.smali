.class public final Lmrp;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmrp;


# instance fields
.field private b:I

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/Boolean;

.field private g:[Lmrp;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Lmrp;

    sput-object v0, Lmrp;->a:[Lmrp;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 27
    const/high16 v0, -0x80000000

    iput v0, p0, Lmrp;->b:I

    .line 39
    sget-object v0, Lmrp;->a:[Lmrp;

    iput-object v0, p0, Lmrp;->g:[Lmrp;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 87
    .line 88
    iget v0, p0, Lmrp;->b:I

    const/high16 v2, -0x80000000

    if-eq v0, v2, :cond_9

    .line 89
    const/4 v0, 0x1

    iget v2, p0, Lmrp;->b:I

    .line 90
    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 92
    :goto_0
    iget-object v2, p0, Lmrp;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 93
    const/4 v2, 0x2

    iget-object v3, p0, Lmrp;->c:Ljava/lang/String;

    .line 94
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 96
    :cond_0
    iget-object v2, p0, Lmrp;->d:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 97
    const/4 v2, 0x3

    iget-object v3, p0, Lmrp;->d:Ljava/lang/String;

    .line 98
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 100
    :cond_1
    iget-object v2, p0, Lmrp;->e:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 101
    const/4 v2, 0x4

    iget-object v3, p0, Lmrp;->e:Ljava/lang/String;

    .line 102
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 104
    :cond_2
    iget-object v2, p0, Lmrp;->f:Ljava/lang/Boolean;

    if-eqz v2, :cond_3

    .line 105
    const/4 v2, 0x5

    iget-object v3, p0, Lmrp;->f:Ljava/lang/Boolean;

    .line 106
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 108
    :cond_3
    iget-object v2, p0, Lmrp;->g:[Lmrp;

    if-eqz v2, :cond_5

    .line 109
    iget-object v2, p0, Lmrp;->g:[Lmrp;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    .line 110
    if-eqz v4, :cond_4

    .line 111
    const/4 v5, 0x6

    .line 112
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 109
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 116
    :cond_5
    iget-object v1, p0, Lmrp;->h:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 117
    const/4 v1, 0x7

    iget-object v2, p0, Lmrp;->h:Ljava/lang/String;

    .line 118
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 120
    :cond_6
    iget-object v1, p0, Lmrp;->i:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 121
    const/16 v1, 0x8

    iget-object v2, p0, Lmrp;->i:Ljava/lang/String;

    .line 122
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 124
    :cond_7
    iget-object v1, p0, Lmrp;->j:Ljava/lang/Integer;

    if-eqz v1, :cond_8

    .line 125
    const/16 v1, 0x9

    iget-object v2, p0, Lmrp;->j:Ljava/lang/Integer;

    .line 126
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 128
    :cond_8
    iget-object v1, p0, Lmrp;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 129
    iput v0, p0, Lmrp;->ai:I

    .line 130
    return v0

    :cond_9
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lmrp;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 138
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 139
    sparse-switch v0, :sswitch_data_0

    .line 143
    iget-object v2, p0, Lmrp;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 144
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmrp;->ah:Ljava/util/List;

    .line 147
    :cond_1
    iget-object v2, p0, Lmrp;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 149
    :sswitch_0
    return-object p0

    .line 154
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 155
    if-eq v0, v4, :cond_2

    const/4 v2, 0x2

    if-eq v0, v2, :cond_2

    const/4 v2, 0x3

    if-eq v0, v2, :cond_2

    const/4 v2, 0x4

    if-eq v0, v2, :cond_2

    const/4 v2, 0x5

    if-eq v0, v2, :cond_2

    const/4 v2, 0x6

    if-eq v0, v2, :cond_2

    const/4 v2, 0x7

    if-eq v0, v2, :cond_2

    const/16 v2, 0x8

    if-eq v0, v2, :cond_2

    const/16 v2, 0x9

    if-eq v0, v2, :cond_2

    const/16 v2, 0xa

    if-eq v0, v2, :cond_2

    const/16 v2, 0xb

    if-ne v0, v2, :cond_3

    .line 166
    :cond_2
    iput v0, p0, Lmrp;->b:I

    goto :goto_0

    .line 168
    :cond_3
    iput v4, p0, Lmrp;->b:I

    goto :goto_0

    .line 173
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmrp;->c:Ljava/lang/String;

    goto :goto_0

    .line 177
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmrp;->d:Ljava/lang/String;

    goto :goto_0

    .line 181
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmrp;->e:Ljava/lang/String;

    goto :goto_0

    .line 185
    :sswitch_5
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmrp;->f:Ljava/lang/Boolean;

    goto :goto_0

    .line 189
    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 190
    iget-object v0, p0, Lmrp;->g:[Lmrp;

    if-nez v0, :cond_5

    move v0, v1

    .line 191
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lmrp;

    .line 192
    iget-object v3, p0, Lmrp;->g:[Lmrp;

    if-eqz v3, :cond_4

    .line 193
    iget-object v3, p0, Lmrp;->g:[Lmrp;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 195
    :cond_4
    iput-object v2, p0, Lmrp;->g:[Lmrp;

    .line 196
    :goto_2
    iget-object v2, p0, Lmrp;->g:[Lmrp;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 197
    iget-object v2, p0, Lmrp;->g:[Lmrp;

    new-instance v3, Lmrp;

    invoke-direct {v3}, Lmrp;-><init>()V

    aput-object v3, v2, v0

    .line 198
    iget-object v2, p0, Lmrp;->g:[Lmrp;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 199
    invoke-virtual {p1}, Loxn;->a()I

    .line 196
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 190
    :cond_5
    iget-object v0, p0, Lmrp;->g:[Lmrp;

    array-length v0, v0

    goto :goto_1

    .line 202
    :cond_6
    iget-object v2, p0, Lmrp;->g:[Lmrp;

    new-instance v3, Lmrp;

    invoke-direct {v3}, Lmrp;-><init>()V

    aput-object v3, v2, v0

    .line 203
    iget-object v2, p0, Lmrp;->g:[Lmrp;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 207
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmrp;->h:Ljava/lang/String;

    goto/16 :goto_0

    .line 211
    :sswitch_8
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmrp;->i:Ljava/lang/String;

    goto/16 :goto_0

    .line 215
    :sswitch_9
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmrp;->j:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 139
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 50
    iget v0, p0, Lmrp;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 51
    const/4 v0, 0x1

    iget v1, p0, Lmrp;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 53
    :cond_0
    iget-object v0, p0, Lmrp;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 54
    const/4 v0, 0x2

    iget-object v1, p0, Lmrp;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 56
    :cond_1
    iget-object v0, p0, Lmrp;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 57
    const/4 v0, 0x3

    iget-object v1, p0, Lmrp;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 59
    :cond_2
    iget-object v0, p0, Lmrp;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 60
    const/4 v0, 0x4

    iget-object v1, p0, Lmrp;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 62
    :cond_3
    iget-object v0, p0, Lmrp;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 63
    const/4 v0, 0x5

    iget-object v1, p0, Lmrp;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 65
    :cond_4
    iget-object v0, p0, Lmrp;->g:[Lmrp;

    if-eqz v0, :cond_6

    .line 66
    iget-object v1, p0, Lmrp;->g:[Lmrp;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_6

    aget-object v3, v1, v0

    .line 67
    if-eqz v3, :cond_5

    .line 68
    const/4 v4, 0x6

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 66
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 72
    :cond_6
    iget-object v0, p0, Lmrp;->h:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 73
    const/4 v0, 0x7

    iget-object v1, p0, Lmrp;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 75
    :cond_7
    iget-object v0, p0, Lmrp;->i:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 76
    const/16 v0, 0x8

    iget-object v1, p0, Lmrp;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 78
    :cond_8
    iget-object v0, p0, Lmrp;->j:Ljava/lang/Integer;

    if-eqz v0, :cond_9

    .line 79
    const/16 v0, 0x9

    iget-object v1, p0, Lmrp;->j:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 81
    :cond_9
    iget-object v0, p0, Lmrp;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 83
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lmrp;->a(Loxn;)Lmrp;

    move-result-object v0

    return-object v0
.end method
