.class public final Lese;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhpg;


# instance fields
.field private final a:Lhms;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const-class v0, Lhms;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    iput-object v0, p0, Lese;->a:Lhms;

    .line 28
    return-void
.end method

.method static synthetic a(Lese;)Lhms;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lese;->a:Lhms;

    return-object v0
.end method

.method private a(Lhmr;)V
    .locals 1

    .prologue
    .line 59
    new-instance v0, Lesf;

    invoke-direct {v0, p0, p1}, Lesf;-><init>(Lese;Lhmr;)V

    invoke-static {v0}, Llsx;->a(Ljava/lang/Runnable;)V

    .line 65
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 53
    const/4 v0, -0x1

    new-instance v1, Lhml;

    invoke-direct {v1}, Lhml;-><init>()V

    new-instance v2, Lhmk;

    sget-object v3, Lomt;->h:Lhmn;

    invoke-direct {v2, v3}, Lhmk;-><init>(Lhmn;)V

    .line 54
    invoke-virtual {v1, v2}, Lhml;->a(Lhmk;)Lhml;

    move-result-object v1

    new-instance v2, Lhmk;

    sget-object v3, Lomz;->b:Lhmn;

    invoke-direct {v2, v3}, Lhmk;-><init>(Lhmn;)V

    .line 55
    invoke-virtual {v1, v2}, Lhml;->a(Lhmk;)Lhml;

    move-result-object v1

    .line 53
    invoke-static {p1, v0, v1}, Lhly;->a(Landroid/content/Context;ILhml;)V

    .line 56
    return-void
.end method

.method public a(Landroid/content/Context;I)V
    .locals 2

    .prologue
    .line 32
    new-instance v0, Lhmr;

    invoke-direct {v0, p1, p2}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v1, Lhmv;->eo:Lhmv;

    .line 33
    invoke-virtual {v0, v1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v0

    sget-object v1, Lhmw;->Q:Lhmw;

    .line 34
    invoke-virtual {v0, v1}, Lhmr;->a(Lhmw;)Lhmr;

    move-result-object v0

    .line 32
    invoke-direct {p0, v0}, Lese;->a(Lhmr;)V

    .line 35
    return-void
.end method

.method public b(Landroid/content/Context;I)V
    .locals 2

    .prologue
    .line 39
    new-instance v0, Lhmr;

    invoke-direct {v0, p1, p2}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v1, Lhmv;->ep:Lhmv;

    .line 40
    invoke-virtual {v0, v1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v0

    sget-object v1, Lhmw;->Q:Lhmw;

    .line 41
    invoke-virtual {v0, v1}, Lhmr;->a(Lhmw;)Lhmr;

    move-result-object v0

    .line 39
    invoke-direct {p0, v0}, Lese;->a(Lhmr;)V

    .line 42
    return-void
.end method

.method public c(Landroid/content/Context;I)V
    .locals 2

    .prologue
    .line 46
    new-instance v0, Lhmr;

    invoke-direct {v0, p1, p2}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v1, Lhmv;->eq:Lhmv;

    .line 47
    invoke-virtual {v0, v1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v0

    sget-object v1, Lhmw;->Q:Lhmw;

    .line 48
    invoke-virtual {v0, v1}, Lhmr;->a(Lhmw;)Lhmr;

    move-result-object v0

    .line 46
    invoke-direct {p0, v0}, Lese;->a(Lhmr;)V

    .line 49
    return-void
.end method
