.class public interface abstract Lcuk;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Lief;

.field public static final b:Lief;

.field public static final c:Lief;

.field public static final d:Lief;

.field public static final e:Lief;

.field public static final f:Lief;

.field public static final g:Lief;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    .line 111
    new-instance v0, Lief;

    const-string v1, "debug.photosync.max"

    const-string v2, "10000"

    const-string v3, "c5a8f830"

    invoke-direct {v0, v1, v2, v3, v6}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lcuk;->a:Lief;

    .line 120
    new-instance v0, Lief;

    const-string v1, "debug.photosync.period"

    sget-object v2, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x6

    .line 122
    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    const-string v3, "6c952e8a"

    invoke-direct {v0, v1, v2, v3, v6}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lcuk;->b:Lief;

    .line 131
    new-instance v0, Lief;

    const-string v1, "debug.photosync.thumbs_wifi"

    const-string v2, "0"

    const-string v3, "8178c27e"

    invoke-direct {v0, v1, v2, v3, v6}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lcuk;->c:Lief;

    .line 141
    new-instance v0, Lief;

    const-string v1, "debug.photosync.thumbs_cell"

    const-string v2, "0"

    const-string v3, "d3a51053"

    invoke-direct {v0, v1, v2, v3, v6}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lcuk;->d:Lief;

    .line 151
    new-instance v0, Lief;

    const-string v1, "debug.photosync.screens_wifi"

    const-string v2, "0"

    const-string v3, "15aff012"

    invoke-direct {v0, v1, v2, v3, v6}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lcuk;->e:Lief;

    .line 161
    new-instance v0, Lief;

    const-string v1, "debug.photosync.screens_cell"

    const-string v2, "0"

    const-string v3, "f6a353e7"

    invoke-direct {v0, v1, v2, v3, v6}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lcuk;->f:Lief;

    .line 171
    new-instance v0, Lief;

    const-string v1, "debug.photosync.initial"

    const-string v2, "10000"

    const-string v3, "9d536325"

    invoke-direct {v0, v1, v2, v3, v6}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lcuk;->g:Lief;

    return-void
.end method
