.class public final Lmia;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnth;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 15955
    invoke-direct {p0}, Loxq;-><init>()V

    .line 15958
    iput-object v0, p0, Lmia;->apiHeader:Llyq;

    .line 15961
    iput-object v0, p0, Lmia;->a:Lnth;

    .line 15955
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 15978
    const/4 v0, 0x0

    .line 15979
    iget-object v1, p0, Lmia;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 15980
    const/4 v0, 0x1

    iget-object v1, p0, Lmia;->apiHeader:Llyq;

    .line 15981
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 15983
    :cond_0
    iget-object v1, p0, Lmia;->a:Lnth;

    if-eqz v1, :cond_1

    .line 15984
    const/4 v1, 0x2

    iget-object v2, p0, Lmia;->a:Lnth;

    .line 15985
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15987
    :cond_1
    iget-object v1, p0, Lmia;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15988
    iput v0, p0, Lmia;->ai:I

    .line 15989
    return v0
.end method

.method public a(Loxn;)Lmia;
    .locals 2

    .prologue
    .line 15997
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 15998
    sparse-switch v0, :sswitch_data_0

    .line 16002
    iget-object v1, p0, Lmia;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 16003
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmia;->ah:Ljava/util/List;

    .line 16006
    :cond_1
    iget-object v1, p0, Lmia;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 16008
    :sswitch_0
    return-object p0

    .line 16013
    :sswitch_1
    iget-object v0, p0, Lmia;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 16014
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmia;->apiHeader:Llyq;

    .line 16016
    :cond_2
    iget-object v0, p0, Lmia;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 16020
    :sswitch_2
    iget-object v0, p0, Lmia;->a:Lnth;

    if-nez v0, :cond_3

    .line 16021
    new-instance v0, Lnth;

    invoke-direct {v0}, Lnth;-><init>()V

    iput-object v0, p0, Lmia;->a:Lnth;

    .line 16023
    :cond_3
    iget-object v0, p0, Lmia;->a:Lnth;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 15998
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 15966
    iget-object v0, p0, Lmia;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 15967
    const/4 v0, 0x1

    iget-object v1, p0, Lmia;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 15969
    :cond_0
    iget-object v0, p0, Lmia;->a:Lnth;

    if-eqz v0, :cond_1

    .line 15970
    const/4 v0, 0x2

    iget-object v1, p0, Lmia;->a:Lnth;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 15972
    :cond_1
    iget-object v0, p0, Lmia;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 15974
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 15951
    invoke-virtual {p0, p1}, Lmia;->a(Loxn;)Lmia;

    move-result-object v0

    return-object v0
.end method
