.class public final Lojh;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:[I

.field private c:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 317
    invoke-direct {p0}, Loxq;-><init>()V

    .line 322
    sget-object v0, Loxx;->a:[I

    iput-object v0, p0, Lojh;->b:[I

    .line 317
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 346
    .line 347
    iget-object v0, p0, Lojh;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 348
    const/4 v0, 0x1

    iget-object v2, p0, Lojh;->a:Ljava/lang/Integer;

    .line 349
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 351
    :goto_0
    iget-object v2, p0, Lojh;->b:[I

    if-eqz v2, :cond_1

    iget-object v2, p0, Lojh;->b:[I

    array-length v2, v2

    if-lez v2, :cond_1

    .line 353
    iget-object v3, p0, Lojh;->b:[I

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_0

    aget v5, v3, v1

    .line 355
    invoke-static {v5}, Loxo;->i(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 353
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 357
    :cond_0
    add-int/2addr v0, v2

    .line 358
    iget-object v1, p0, Lojh;->b:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 360
    :cond_1
    iget-object v1, p0, Lojh;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 361
    const/4 v1, 0x3

    iget-object v2, p0, Lojh;->c:Ljava/lang/Integer;

    .line 362
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 364
    :cond_2
    iget-object v1, p0, Lojh;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 365
    iput v0, p0, Lojh;->ai:I

    .line 366
    return v0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lojh;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 374
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 375
    sparse-switch v0, :sswitch_data_0

    .line 379
    iget-object v1, p0, Lojh;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 380
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lojh;->ah:Ljava/util/List;

    .line 383
    :cond_1
    iget-object v1, p0, Lojh;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 385
    :sswitch_0
    return-object p0

    .line 390
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lojh;->a:Ljava/lang/Integer;

    goto :goto_0

    .line 394
    :sswitch_2
    const/16 v0, 0x10

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 395
    iget-object v0, p0, Lojh;->b:[I

    array-length v0, v0

    .line 396
    add-int/2addr v1, v0

    new-array v1, v1, [I

    .line 397
    iget-object v2, p0, Lojh;->b:[I

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 398
    iput-object v1, p0, Lojh;->b:[I

    .line 399
    :goto_1
    iget-object v1, p0, Lojh;->b:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 400
    iget-object v1, p0, Lojh;->b:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    .line 401
    invoke-virtual {p1}, Loxn;->a()I

    .line 399
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 404
    :cond_2
    iget-object v1, p0, Lojh;->b:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    goto :goto_0

    .line 408
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lojh;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 375
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 329
    iget-object v0, p0, Lojh;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 330
    const/4 v0, 0x1

    iget-object v1, p0, Lojh;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 332
    :cond_0
    iget-object v0, p0, Lojh;->b:[I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lojh;->b:[I

    array-length v0, v0

    if-lez v0, :cond_1

    .line 333
    iget-object v1, p0, Lojh;->b:[I

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget v3, v1, v0

    .line 334
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->a(II)V

    .line 333
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 337
    :cond_1
    iget-object v0, p0, Lojh;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 338
    const/4 v0, 0x3

    iget-object v1, p0, Lojh;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 340
    :cond_2
    iget-object v0, p0, Lojh;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 342
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 313
    invoke-virtual {p0, p1}, Lojh;->a(Loxn;)Lojh;

    move-result-object v0

    return-object v0
.end method
