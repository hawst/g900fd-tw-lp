.class public final Llxz;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Llxz;


# instance fields
.field private b:I

.field private c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 5561
    const/4 v0, 0x0

    new-array v0, v0, [Llxz;

    sput-object v0, Llxz;->a:[Llxz;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5562
    invoke-direct {p0}, Loxq;-><init>()V

    .line 5565
    const/high16 v0, -0x80000000

    iput v0, p0, Llxz;->b:I

    .line 5562
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 5584
    const/4 v0, 0x0

    .line 5585
    iget v1, p0, Llxz;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 5586
    const/4 v0, 0x1

    iget v1, p0, Llxz;->b:I

    .line 5587
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 5589
    :cond_0
    iget-object v1, p0, Llxz;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 5590
    const/4 v1, 0x2

    iget-object v2, p0, Llxz;->c:Ljava/lang/String;

    .line 5591
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5593
    :cond_1
    iget-object v1, p0, Llxz;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5594
    iput v0, p0, Llxz;->ai:I

    .line 5595
    return v0
.end method

.method public a(Loxn;)Llxz;
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 5603
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 5604
    sparse-switch v0, :sswitch_data_0

    .line 5608
    iget-object v1, p0, Llxz;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 5609
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llxz;->ah:Ljava/util/List;

    .line 5612
    :cond_1
    iget-object v1, p0, Llxz;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5614
    :sswitch_0
    return-object p0

    .line 5619
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 5620
    if-eq v0, v2, :cond_2

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-ne v0, v1, :cond_3

    .line 5627
    :cond_2
    iput v0, p0, Llxz;->b:I

    goto :goto_0

    .line 5629
    :cond_3
    iput v2, p0, Llxz;->b:I

    goto :goto_0

    .line 5634
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llxz;->c:Ljava/lang/String;

    goto :goto_0

    .line 5604
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 5572
    iget v0, p0, Llxz;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 5573
    const/4 v0, 0x1

    iget v1, p0, Llxz;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 5575
    :cond_0
    iget-object v0, p0, Llxz;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 5576
    const/4 v0, 0x2

    iget-object v1, p0, Llxz;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 5578
    :cond_1
    iget-object v0, p0, Llxz;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 5580
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5558
    invoke-virtual {p0, p1}, Llxz;->a(Loxn;)Llxz;

    move-result-object v0

    return-object v0
.end method
