.class public final Loen;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Loen;


# instance fields
.field private b:Ljava/lang/String;

.field private c:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 327
    const/4 v0, 0x0

    new-array v0, v0, [Loen;

    sput-object v0, Loen;->a:[Loen;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 328
    invoke-direct {p0}, Loxq;-><init>()V

    .line 333
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Loen;->c:[Ljava/lang/String;

    .line 328
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 352
    .line 353
    iget-object v0, p0, Loen;->b:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 354
    const/4 v0, 0x1

    iget-object v2, p0, Loen;->b:Ljava/lang/String;

    .line 355
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 357
    :goto_0
    iget-object v2, p0, Loen;->c:[Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Loen;->c:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 359
    iget-object v3, p0, Loen;->c:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_0

    aget-object v5, v3, v1

    .line 361
    invoke-static {v5}, Loxo;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 359
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 363
    :cond_0
    add-int/2addr v0, v2

    .line 364
    iget-object v1, p0, Loen;->c:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 366
    :cond_1
    iget-object v1, p0, Loen;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 367
    iput v0, p0, Loen;->ai:I

    .line 368
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Loen;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 376
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 377
    sparse-switch v0, :sswitch_data_0

    .line 381
    iget-object v1, p0, Loen;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 382
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Loen;->ah:Ljava/util/List;

    .line 385
    :cond_1
    iget-object v1, p0, Loen;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 387
    :sswitch_0
    return-object p0

    .line 392
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loen;->b:Ljava/lang/String;

    goto :goto_0

    .line 396
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 397
    iget-object v0, p0, Loen;->c:[Ljava/lang/String;

    array-length v0, v0

    .line 398
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    .line 399
    iget-object v2, p0, Loen;->c:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 400
    iput-object v1, p0, Loen;->c:[Ljava/lang/String;

    .line 401
    :goto_1
    iget-object v1, p0, Loen;->c:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 402
    iget-object v1, p0, Loen;->c:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 403
    invoke-virtual {p1}, Loxn;->a()I

    .line 401
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 406
    :cond_2
    iget-object v1, p0, Loen;->c:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    .line 377
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 338
    iget-object v0, p0, Loen;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 339
    const/4 v0, 0x1

    iget-object v1, p0, Loen;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 341
    :cond_0
    iget-object v0, p0, Loen;->c:[Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 342
    iget-object v1, p0, Loen;->c:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 343
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->a(ILjava/lang/String;)V

    .line 342
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 346
    :cond_1
    iget-object v0, p0, Loen;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 348
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 324
    invoke-virtual {p0, p1}, Loen;->a(Loxn;)Loen;

    move-result-object v0

    return-object v0
.end method
