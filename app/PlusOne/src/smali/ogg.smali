.class public final Logg;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Logg;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8466
    const/4 v0, 0x0

    new-array v0, v0, [Logg;

    sput-object v0, Logg;->a:[Logg;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8467
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 8488
    const/4 v0, 0x0

    .line 8489
    iget-object v1, p0, Logg;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 8490
    const/4 v0, 0x1

    iget-object v1, p0, Logg;->b:Ljava/lang/String;

    .line 8491
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 8493
    :cond_0
    iget-object v1, p0, Logg;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 8494
    const/4 v1, 0x2

    iget-object v2, p0, Logg;->c:Ljava/lang/String;

    .line 8495
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8497
    :cond_1
    iget-object v1, p0, Logg;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8498
    iput v0, p0, Logg;->ai:I

    .line 8499
    return v0
.end method

.method public a(Loxn;)Logg;
    .locals 2

    .prologue
    .line 8507
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 8508
    sparse-switch v0, :sswitch_data_0

    .line 8512
    iget-object v1, p0, Logg;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 8513
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Logg;->ah:Ljava/util/List;

    .line 8516
    :cond_1
    iget-object v1, p0, Logg;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 8518
    :sswitch_0
    return-object p0

    .line 8523
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Logg;->b:Ljava/lang/String;

    goto :goto_0

    .line 8527
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Logg;->c:Ljava/lang/String;

    goto :goto_0

    .line 8508
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 8476
    iget-object v0, p0, Logg;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 8477
    const/4 v0, 0x1

    iget-object v1, p0, Logg;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 8479
    :cond_0
    iget-object v0, p0, Logg;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 8480
    const/4 v0, 0x2

    iget-object v1, p0, Logg;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 8482
    :cond_1
    iget-object v0, p0, Logg;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 8484
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 8463
    invoke-virtual {p0, p1}, Logg;->a(Loxn;)Logg;

    move-result-object v0

    return-object v0
.end method
