.class public final Lnwl;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnwl;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Lpeo;

.field private d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 982
    const/4 v0, 0x0

    new-array v0, v0, [Lnwl;

    sput-object v0, Lnwl;->a:[Lnwl;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 983
    invoke-direct {p0}, Loxq;-><init>()V

    .line 990
    const/4 v0, 0x0

    iput-object v0, p0, Lnwl;->c:Lpeo;

    .line 983
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1010
    const/4 v0, 0x0

    .line 1011
    iget-object v1, p0, Lnwl;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1012
    const/4 v0, 0x1

    iget-object v1, p0, Lnwl;->b:Ljava/lang/String;

    .line 1013
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1015
    :cond_0
    iget-object v1, p0, Lnwl;->d:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1016
    const/4 v1, 0x2

    iget-object v2, p0, Lnwl;->d:Ljava/lang/String;

    .line 1017
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1019
    :cond_1
    iget-object v1, p0, Lnwl;->c:Lpeo;

    if-eqz v1, :cond_2

    .line 1020
    const/4 v1, 0x3

    iget-object v2, p0, Lnwl;->c:Lpeo;

    .line 1021
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1023
    :cond_2
    iget-object v1, p0, Lnwl;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1024
    iput v0, p0, Lnwl;->ai:I

    .line 1025
    return v0
.end method

.method public a(Loxn;)Lnwl;
    .locals 2

    .prologue
    .line 1033
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1034
    sparse-switch v0, :sswitch_data_0

    .line 1038
    iget-object v1, p0, Lnwl;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1039
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnwl;->ah:Ljava/util/List;

    .line 1042
    :cond_1
    iget-object v1, p0, Lnwl;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1044
    :sswitch_0
    return-object p0

    .line 1049
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnwl;->b:Ljava/lang/String;

    goto :goto_0

    .line 1053
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnwl;->d:Ljava/lang/String;

    goto :goto_0

    .line 1057
    :sswitch_3
    iget-object v0, p0, Lnwl;->c:Lpeo;

    if-nez v0, :cond_2

    .line 1058
    new-instance v0, Lpeo;

    invoke-direct {v0}, Lpeo;-><init>()V

    iput-object v0, p0, Lnwl;->c:Lpeo;

    .line 1060
    :cond_2
    iget-object v0, p0, Lnwl;->c:Lpeo;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1034
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 995
    iget-object v0, p0, Lnwl;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 996
    const/4 v0, 0x1

    iget-object v1, p0, Lnwl;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 998
    :cond_0
    iget-object v0, p0, Lnwl;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 999
    const/4 v0, 0x2

    iget-object v1, p0, Lnwl;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1001
    :cond_1
    iget-object v0, p0, Lnwl;->c:Lpeo;

    if-eqz v0, :cond_2

    .line 1002
    const/4 v0, 0x3

    iget-object v1, p0, Lnwl;->c:Lpeo;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1004
    :cond_2
    iget-object v0, p0, Lnwl;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1006
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 979
    invoke-virtual {p0, p1}, Lnwl;->a(Loxn;)Lnwl;

    move-result-object v0

    return-object v0
.end method
