.class public final Lmzg;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmza;

.field public b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 168
    invoke-direct {p0}, Loxq;-><init>()V

    .line 171
    const/4 v0, 0x0

    iput-object v0, p0, Lmzg;->a:Lmza;

    .line 168
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 190
    const/4 v0, 0x0

    .line 191
    iget-object v1, p0, Lmzg;->a:Lmza;

    if-eqz v1, :cond_0

    .line 192
    const/4 v0, 0x1

    iget-object v1, p0, Lmzg;->a:Lmza;

    .line 193
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 195
    :cond_0
    iget-object v1, p0, Lmzg;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 196
    const/4 v1, 0x2

    iget-object v2, p0, Lmzg;->b:Ljava/lang/String;

    .line 197
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 199
    :cond_1
    iget-object v1, p0, Lmzg;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 200
    iput v0, p0, Lmzg;->ai:I

    .line 201
    return v0
.end method

.method public a(Loxn;)Lmzg;
    .locals 2

    .prologue
    .line 209
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 210
    sparse-switch v0, :sswitch_data_0

    .line 214
    iget-object v1, p0, Lmzg;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 215
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmzg;->ah:Ljava/util/List;

    .line 218
    :cond_1
    iget-object v1, p0, Lmzg;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 220
    :sswitch_0
    return-object p0

    .line 225
    :sswitch_1
    iget-object v0, p0, Lmzg;->a:Lmza;

    if-nez v0, :cond_2

    .line 226
    new-instance v0, Lmza;

    invoke-direct {v0}, Lmza;-><init>()V

    iput-object v0, p0, Lmzg;->a:Lmza;

    .line 228
    :cond_2
    iget-object v0, p0, Lmzg;->a:Lmza;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 232
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmzg;->b:Ljava/lang/String;

    goto :goto_0

    .line 210
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 178
    iget-object v0, p0, Lmzg;->a:Lmza;

    if-eqz v0, :cond_0

    .line 179
    const/4 v0, 0x1

    iget-object v1, p0, Lmzg;->a:Lmza;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 181
    :cond_0
    iget-object v0, p0, Lmzg;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 182
    const/4 v0, 0x2

    iget-object v1, p0, Lmzg;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 184
    :cond_1
    iget-object v0, p0, Lmzg;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 186
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 164
    invoke-virtual {p0, p1}, Lmzg;->a(Loxn;)Lmzg;

    move-result-object v0

    return-object v0
.end method
