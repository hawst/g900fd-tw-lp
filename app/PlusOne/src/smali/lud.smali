.class public final Llud;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Llud;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Lluc;

.field private e:Llwa;

.field private f:Ljava/lang/Long;

.field private g:Lluf;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const/4 v0, 0x0

    new-array v0, v0, [Llud;

    sput-object v0, Llud;->a:[Llud;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 9
    invoke-direct {p0}, Loxq;-><init>()V

    .line 16
    iput-object v0, p0, Llud;->d:Lluc;

    .line 19
    iput-object v0, p0, Llud;->e:Llwa;

    .line 24
    iput-object v0, p0, Llud;->g:Lluf;

    .line 9
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 53
    const/4 v0, 0x0

    .line 54
    iget-object v1, p0, Llud;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 55
    const/4 v0, 0x1

    iget-object v1, p0, Llud;->b:Ljava/lang/String;

    .line 56
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 58
    :cond_0
    iget-object v1, p0, Llud;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 59
    const/4 v1, 0x2

    iget-object v2, p0, Llud;->c:Ljava/lang/String;

    .line 60
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 62
    :cond_1
    iget-object v1, p0, Llud;->d:Lluc;

    if-eqz v1, :cond_2

    .line 63
    const/4 v1, 0x3

    iget-object v2, p0, Llud;->d:Lluc;

    .line 64
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 66
    :cond_2
    iget-object v1, p0, Llud;->e:Llwa;

    if-eqz v1, :cond_3

    .line 67
    const/4 v1, 0x4

    iget-object v2, p0, Llud;->e:Llwa;

    .line 68
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 70
    :cond_3
    iget-object v1, p0, Llud;->f:Ljava/lang/Long;

    if-eqz v1, :cond_4

    .line 71
    const/4 v1, 0x5

    iget-object v2, p0, Llud;->f:Ljava/lang/Long;

    .line 72
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 74
    :cond_4
    iget-object v1, p0, Llud;->g:Lluf;

    if-eqz v1, :cond_5

    .line 75
    const/4 v1, 0x7

    iget-object v2, p0, Llud;->g:Lluf;

    .line 76
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 78
    :cond_5
    iget-object v1, p0, Llud;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 79
    iput v0, p0, Llud;->ai:I

    .line 80
    return v0
.end method

.method public a(Loxn;)Llud;
    .locals 2

    .prologue
    .line 88
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 89
    sparse-switch v0, :sswitch_data_0

    .line 93
    iget-object v1, p0, Llud;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 94
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llud;->ah:Ljava/util/List;

    .line 97
    :cond_1
    iget-object v1, p0, Llud;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 99
    :sswitch_0
    return-object p0

    .line 104
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llud;->b:Ljava/lang/String;

    goto :goto_0

    .line 108
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llud;->c:Ljava/lang/String;

    goto :goto_0

    .line 112
    :sswitch_3
    iget-object v0, p0, Llud;->d:Lluc;

    if-nez v0, :cond_2

    .line 113
    new-instance v0, Lluc;

    invoke-direct {v0}, Lluc;-><init>()V

    iput-object v0, p0, Llud;->d:Lluc;

    .line 115
    :cond_2
    iget-object v0, p0, Llud;->d:Lluc;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 119
    :sswitch_4
    iget-object v0, p0, Llud;->e:Llwa;

    if-nez v0, :cond_3

    .line 120
    new-instance v0, Llwa;

    invoke-direct {v0}, Llwa;-><init>()V

    iput-object v0, p0, Llud;->e:Llwa;

    .line 122
    :cond_3
    iget-object v0, p0, Llud;->e:Llwa;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 126
    :sswitch_5
    invoke-virtual {p1}, Loxn;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Llud;->f:Ljava/lang/Long;

    goto :goto_0

    .line 130
    :sswitch_6
    iget-object v0, p0, Llud;->g:Lluf;

    if-nez v0, :cond_4

    .line 131
    new-instance v0, Lluf;

    invoke-direct {v0}, Lluf;-><init>()V

    iput-object v0, p0, Llud;->g:Lluf;

    .line 133
    :cond_4
    iget-object v0, p0, Llud;->g:Lluf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 89
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x3a -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 29
    iget-object v0, p0, Llud;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 30
    const/4 v0, 0x1

    iget-object v1, p0, Llud;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 32
    :cond_0
    iget-object v0, p0, Llud;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 33
    const/4 v0, 0x2

    iget-object v1, p0, Llud;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 35
    :cond_1
    iget-object v0, p0, Llud;->d:Lluc;

    if-eqz v0, :cond_2

    .line 36
    const/4 v0, 0x3

    iget-object v1, p0, Llud;->d:Lluc;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 38
    :cond_2
    iget-object v0, p0, Llud;->e:Llwa;

    if-eqz v0, :cond_3

    .line 39
    const/4 v0, 0x4

    iget-object v1, p0, Llud;->e:Llwa;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 41
    :cond_3
    iget-object v0, p0, Llud;->f:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 42
    const/4 v0, 0x5

    iget-object v1, p0, Llud;->f:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(IJ)V

    .line 44
    :cond_4
    iget-object v0, p0, Llud;->g:Lluf;

    if-eqz v0, :cond_5

    .line 45
    const/4 v0, 0x7

    iget-object v1, p0, Llud;->g:Lluf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 47
    :cond_5
    iget-object v0, p0, Llud;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 49
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Llud;->a(Loxn;)Llud;

    move-result-object v0

    return-object v0
.end method
