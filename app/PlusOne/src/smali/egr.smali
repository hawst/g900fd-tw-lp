.class public final Legr;
.super Legi;
.source "PG"

# interfaces
.implements Lcqg;
.implements Lkc;


# instance fields
.field private N:Landroid/support/v4/view/ViewPager;

.field private ah:Lcom/google/android/apps/plus/views/PhotosHomeTabContainer;

.field private ai:Z

.field private aj:I

.field private ak:I

.field private al:Landroid/net/Uri;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Legi;-><init>()V

    .line 331
    return-void
.end method

.method static synthetic a(Legr;I)I
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0, p1}, Legr;->d(I)I

    move-result v0

    return v0
.end method

.method static synthetic a(Legr;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Legr;->al:Landroid/net/Uri;

    return-object v0
.end method

.method private ac()Lu;
    .locals 3

    .prologue
    .line 210
    iget-object v0, p0, Legr;->N:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->c()I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x28

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "android:switcher:2131755808"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Legr;->q()Lae;

    move-result-object v1

    invoke-virtual {v1, v0}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    return-object v0
.end method

.method private ad()Z
    .locals 2

    .prologue
    .line 278
    invoke-direct {p0}, Legr;->ac()Lu;

    move-result-object v0

    .line 280
    instance-of v1, v0, Legi;

    if-eqz v1, :cond_0

    check-cast v0, Legi;

    .line 281
    invoke-virtual {v0}, Legi;->Z()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Legr;)I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Legr;->ak:I

    return v0
.end method

.method private d(I)I
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 254
    const/4 v0, 0x0

    :goto_0
    shl-int v1, v3, v0

    iget v2, p0, Legr;->aj:I

    if-gt v1, v2, :cond_2

    .line 255
    if-nez p1, :cond_0

    iget v1, p0, Legr;->aj:I

    shr-int/2addr v1, v0

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 256
    shl-int v0, v3, v0

    .line 264
    :goto_1
    return v0

    .line 259
    :cond_0
    iget v1, p0, Legr;->aj:I

    shr-int/2addr v1, v0

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 260
    add-int/lit8 p1, p1, -0x1

    .line 254
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 264
    :cond_2
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private f(I)Lhmw;
    .locals 1

    .prologue
    .line 500
    const/4 v0, 0x0

    .line 501
    sparse-switch p1, :sswitch_data_0

    .line 515
    :goto_0
    return-object v0

    .line 503
    :sswitch_0
    sget-object v0, Lhmw;->ad:Lhmw;

    goto :goto_0

    .line 507
    :sswitch_1
    sget-object v0, Lhmw;->aj:Lhmw;

    goto :goto_0

    .line 511
    :sswitch_2
    sget-object v0, Lhmw;->az:Lhmw;

    goto :goto_0

    .line 501
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_1
        0x4 -> :sswitch_0
        0x40 -> :sswitch_2
    .end sparse-switch
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Legr;->N:Landroid/support/v4/view/ViewPager;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 65
    :goto_0
    invoke-direct {p0, v0}, Legr;->d(I)I

    move-result v0

    .line 66
    sparse-switch v0, :sswitch_data_0

    .line 80
    sget-object v0, Lhmw;->au:Lhmw;

    :goto_1
    return-object v0

    .line 64
    :cond_0
    iget-object v0, p0, Legr;->N:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->c()I

    move-result v0

    goto :goto_0

    .line 68
    :sswitch_0
    sget-object v0, Lhmw;->au:Lhmw;

    goto :goto_1

    .line 70
    :sswitch_1
    sget-object v0, Lhmw;->av:Lhmw;

    goto :goto_1

    .line 72
    :sswitch_2
    sget-object v0, Lhmw;->aw:Lhmw;

    goto :goto_1

    .line 74
    :sswitch_3
    sget-object v0, Lhmw;->ax:Lhmw;

    goto :goto_1

    .line 76
    :sswitch_4
    sget-object v0, Lhmw;->ay:Lhmw;

    goto :goto_1

    .line 78
    :sswitch_5
    sget-object v0, Lhmw;->az:Lhmw;

    goto :goto_1

    .line 66
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_4
        0x2 -> :sswitch_0
        0x4 -> :sswitch_1
        0x8 -> :sswitch_3
        0x10 -> :sswitch_2
        0x40 -> :sswitch_5
    .end sparse-switch
.end method

.method public J_()Z
    .locals 2

    .prologue
    .line 306
    invoke-direct {p0}, Legr;->ac()Lu;

    move-result-object v0

    .line 307
    instance-of v1, v0, Legi;

    if-eqz v1, :cond_0

    check-cast v0, Legi;

    .line 308
    invoke-virtual {v0}, Legi;->J_()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public L_()V
    .locals 2

    .prologue
    .line 220
    invoke-direct {p0}, Legr;->ac()Lu;

    move-result-object v0

    .line 222
    instance-of v1, v0, Legi;

    if-eqz v1, :cond_0

    .line 223
    check-cast v0, Legi;

    invoke-virtual {v0}, Legi;->L_()V

    .line 225
    :cond_0
    return-void
.end method

.method protected U()Z
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x0

    return v0
.end method

.method public V()Z
    .locals 1

    .prologue
    .line 320
    iget-boolean v0, p0, Legr;->ac:Z

    if-eqz v0, :cond_0

    .line 321
    invoke-super {p0}, Legi;->a()Z

    move-result v0

    .line 323
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Legi;->V()Z

    move-result v0

    goto :goto_0
.end method

.method protected Y_()Z
    .locals 1

    .prologue
    .line 313
    iget-boolean v0, p0, Legr;->ac:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Legr;->am()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Legr;->X:Lctz;

    .line 314
    invoke-virtual {v0}, Lctz;->a()Ljcn;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Legr;->X:Lctz;

    .line 315
    invoke-virtual {v0}, Lctz;->a()Ljcn;

    move-result-object v0

    invoke-virtual {v0}, Ljcn;->k()I

    move-result v0

    if-gtz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public Z()Z
    .locals 1

    .prologue
    .line 201
    invoke-super {p0}, Legi;->Z()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Legr;->ad()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    .prologue
    .line 99
    const v0, 0x7f0400df

    invoke-super {p0, p1, p2, p3, v0}, Legi;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a(IFI)V
    .locals 1

    .prologue
    .line 471
    iget-object v0, p0, Legr;->ah:Lcom/google/android/apps/plus/views/PhotosHomeTabContainer;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/plus/views/PhotosHomeTabContainer;->a(IFI)V

    .line 472
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 92
    invoke-super {p0, p1}, Legi;->a(Landroid/os/Bundle;)V

    .line 93
    invoke-virtual {p0}, Legr;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "disable_up_button"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Legr;->ai:Z

    .line 94
    return-void
.end method

.method public a(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x0

    .line 105
    invoke-super {p0, p1, p2}, Legi;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 107
    invoke-virtual {p0}, Legr;->k()Landroid/os/Bundle;

    move-result-object v0

    .line 109
    const-string v3, "tabs"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 110
    const-string v3, "tabs"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Legr;->aj:I

    .line 113
    iget-object v0, p0, Legr;->P:Lhee;

    invoke-interface {v0}, Lhee;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 114
    iget v0, p0, Legr;->aj:I

    and-int/lit8 v0, v0, 0x6

    iput v0, p0, Legr;->aj:I

    .line 118
    :cond_0
    iget v0, p0, Legr;->aj:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 119
    iget v0, p0, Legr;->aj:I

    xor-int/lit8 v0, v0, 0x2

    iput v0, p0, Legr;->aj:I

    .line 120
    iget v0, p0, Legr;->aj:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Legr;->aj:I

    .line 123
    :cond_1
    iget v0, p0, Legr;->aa:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    iget v0, p0, Legr;->aj:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_2

    .line 125
    iget v0, p0, Legr;->aj:I

    xor-int/lit8 v0, v0, 0x8

    iput v0, p0, Legr;->aj:I

    .line 129
    :cond_2
    iget v0, p0, Legr;->aj:I

    move v3, v0

    move v0, v1

    :goto_0
    if-eqz v3, :cond_4

    .line 130
    and-int/lit8 v4, v3, 0x1

    if-eqz v4, :cond_3

    .line 131
    add-int/lit8 v0, v0, 0x1

    .line 129
    :cond_3
    shr-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 135
    :cond_4
    iput v0, p0, Legr;->ak:I

    .line 139
    invoke-virtual {p0}, Legr;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "tab_bundles"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 141
    const v3, 0x7f10031f

    .line 142
    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 144
    new-instance v3, Legt;

    invoke-virtual {p0}, Legr;->q()Lae;

    move-result-object v4

    invoke-direct {v3, p0, v4, v0}, Legt;-><init>(Legr;Lae;Ljava/util/HashMap;)V

    .line 145
    const v0, 0x7f100320

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Legr;->N:Landroid/support/v4/view/ViewPager;

    .line 146
    const v0, 0x7f10031e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/PhotosHomeTabContainer;

    iput-object v0, p0, Legr;->ah:Lcom/google/android/apps/plus/views/PhotosHomeTabContainer;

    .line 147
    iget-object v4, p0, Legr;->ah:Lcom/google/android/apps/plus/views/PhotosHomeTabContainer;

    iget v0, p0, Legr;->ak:I

    if-le v0, v2, :cond_a

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v4, v0}, Lcom/google/android/apps/plus/views/PhotosHomeTabContainer;->a(Z)V

    .line 148
    iget-object v0, p0, Legr;->N:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v3}, Landroid/support/v4/view/ViewPager;->a(Lip;)V

    .line 149
    iget-object v0, p0, Legr;->ah:Lcom/google/android/apps/plus/views/PhotosHomeTabContainer;

    iget-object v3, p0, Legr;->N:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/PhotosHomeTabContainer;->a(Landroid/support/v4/view/ViewPager;)V

    .line 150
    iget-object v0, p0, Legr;->ah:Lcom/google/android/apps/plus/views/PhotosHomeTabContainer;

    invoke-virtual {p0}, Legr;->o()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0327

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/PhotosHomeTabContainer;->setBackgroundColor(I)V

    .line 152
    iget-object v0, p0, Legr;->ah:Lcom/google/android/apps/plus/views/PhotosHomeTabContainer;

    .line 153
    invoke-virtual {p0}, Legr;->o()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0324

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    .line 152
    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/PhotosHomeTabContainer;->c(I)V

    .line 154
    iget-object v0, p0, Legr;->N:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/ViewPager;->a(Lkc;)V

    .line 155
    invoke-virtual {p0}, Legr;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "scroll_to_uri"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Legr;->al:Landroid/net/Uri;

    .line 157
    if-nez p2, :cond_9

    .line 158
    iget-object v0, p0, Legr;->at:Llnl;

    const-string v3, "photos_home"

    invoke-virtual {v0, v3, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v3, "photos_home_tab"

    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 159
    invoke-virtual {p0}, Legr;->k()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "starting_tab_index"

    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 161
    iget v3, p0, Legr;->aj:I

    and-int/2addr v3, v0

    if-eqz v3, :cond_5

    iget-object v3, p0, Legr;->P:Lhee;

    invoke-interface {v3}, Lhee;->f()Z

    move-result v3

    if-nez v3, :cond_6

    .line 162
    :cond_5
    iget v0, p0, Legr;->aj:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_b

    move v0, v2

    .line 169
    :cond_6
    :goto_2
    iget v2, p0, Legr;->aj:I

    and-int/2addr v2, v0

    if-nez v2, :cond_c

    const/4 v1, -0x1

    .line 170
    :cond_7
    iget-object v2, p0, Legr;->N:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2, v1}, Landroid/support/v4/view/ViewPager;->a(I)V

    .line 172
    if-nez v1, :cond_8

    .line 173
    iget-object v1, p0, Legr;->N:Landroid/support/v4/view/ViewPager;

    new-instance v2, Legs;

    invoke-direct {v2, p0}, Legs;-><init>(Legr;)V

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->post(Ljava/lang/Runnable;)Z

    .line 181
    :cond_8
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 182
    invoke-virtual {p0, v1}, Legr;->b(Landroid/os/Bundle;)V

    .line 183
    invoke-direct {p0, v0}, Legr;->f(I)Lhmw;

    move-result-object v2

    .line 184
    if-eqz v2, :cond_9

    .line 185
    iget-object v0, p0, Legr;->au:Llnh;

    const-class v3, Lhms;

    invoke-virtual {v0, v3}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v3, Lhmr;

    iget-object v4, p0, Legr;->at:Llnl;

    invoke-direct {v3, v4}, Lhmr;-><init>(Landroid/content/Context;)V

    .line 187
    invoke-virtual {v3, v2}, Lhmr;->b(Lhmw;)Lhmr;

    move-result-object v2

    .line 188
    invoke-virtual {v2, v1}, Lhmr;->c(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    .line 185
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 192
    :cond_9
    return-void

    :cond_a
    move v0, v1

    .line 147
    goto/16 :goto_1

    .line 165
    :cond_b
    invoke-direct {p0, v1}, Legr;->d(I)I

    move-result v0

    goto :goto_2

    :cond_c
    move v2, v1

    .line 169
    :goto_3
    shr-int v3, v0, v2

    and-int/lit8 v3, v3, 0x1

    if-nez v3, :cond_7

    iget v3, p0, Legr;->aj:I

    shr-int/2addr v3, v2

    and-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_d

    add-int/lit8 v1, v1, 0x1

    :cond_d
    add-int/lit8 v2, v2, 0x1

    goto :goto_3
.end method

.method public a(Loo;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 269
    invoke-super {p0, p1}, Legi;->a(Loo;)V

    .line 271
    iget-boolean v0, p0, Legr;->ai:Z

    if-nez v0, :cond_0

    .line 272
    invoke-static {p1, v1}, Lley;->a(Loo;Z)V

    .line 273
    invoke-virtual {p1, v1}, Loo;->c(Z)V

    .line 275
    :cond_0
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 196
    invoke-super {p0, p1}, Legi;->a(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 466
    iget-object v0, p0, Legr;->ah:Lcom/google/android/apps/plus/views/PhotosHomeTabContainer;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/PhotosHomeTabContainer;->b(I)V

    .line 467
    return-void
.end method

.method protected b(Lhjk;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 286
    invoke-super {p0, p1}, Legi;->b(Lhjk;)V

    .line 288
    invoke-virtual {p0, p1, v2}, Legr;->a(Lhjk;I)V

    .line 289
    const v0, 0x7f0a0a77

    invoke-interface {p1, v0}, Lhjk;->d(I)V

    .line 291
    invoke-virtual {p0}, Legr;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "photo_picker_mode"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 293
    if-nez v0, :cond_0

    .line 294
    const v0, 0x7f100696

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    .line 297
    :cond_0
    invoke-direct {p0}, Legr;->ad()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 298
    const v0, 0x7f10067b

    .line 299
    invoke-interface {p1, v0}, Lhjk;->a(I)Lhjc;

    move-result-object v0

    check-cast v0, Lhjv;

    .line 300
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lhjv;->a(I)V

    .line 302
    :cond_1
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 86
    invoke-super {p0, p1}, Legi;->c(Landroid/os/Bundle;)V

    .line 87
    iget-object v0, p0, Legr;->au:Llnh;

    const-class v1, Lcqg;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 88
    return-void
.end method

.method protected c(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 461
    const/4 v0, 0x0

    return v0
.end method

.method public g_(I)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 476
    iget-object v0, p0, Legr;->N:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->c()I

    move-result v0

    invoke-direct {p0, v0}, Legr;->d(I)I

    move-result v1

    .line 477
    invoke-direct {p0, p1}, Legr;->d(I)I

    move-result v0

    .line 479
    iget-object v2, p0, Legr;->ah:Lcom/google/android/apps/plus/views/PhotosHomeTabContainer;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/PhotosHomeTabContainer;->g_(I)V

    .line 481
    invoke-direct {p0, p1}, Legr;->d(I)I

    move-result v2

    invoke-virtual {p0}, Legr;->k()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "photo_picker_mode"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Legr;->at:Llnl;

    const-string v4, "photos_home"

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "photos_home_tab"

    invoke-interface {v3, v4, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 483
    :cond_0
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 484
    invoke-virtual {p0, v2}, Legr;->b(Landroid/os/Bundle;)V

    .line 485
    invoke-direct {p0, v0}, Legr;->f(I)Lhmw;

    move-result-object v3

    .line 486
    if-eqz v3, :cond_1

    .line 487
    iget-object v0, p0, Legr;->au:Llnh;

    const-class v4, Lhms;

    invoke-virtual {v0, v4}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v4, Lhmr;

    iget-object v5, p0, Legr;->at:Llnl;

    invoke-direct {v4, v5}, Lhmr;-><init>(Landroid/content/Context;)V

    .line 489
    invoke-direct {p0, v1}, Legr;->f(I)Lhmw;

    move-result-object v1

    invoke-virtual {v4, v1}, Lhmr;->a(Lhmw;)Lhmr;

    move-result-object v1

    .line 490
    invoke-virtual {v1, v3}, Lhmr;->b(Lhmw;)Lhmr;

    move-result-object v1

    .line 491
    invoke-virtual {v1, v2}, Lhmr;->c(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    .line 487
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 496
    :cond_1
    iget-object v0, p0, Legr;->O:Lhje;

    invoke-virtual {v0}, Lhje;->a()V

    .line 497
    return-void
.end method
