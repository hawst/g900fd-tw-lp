.class public final Lmrb;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmrb;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 200
    const/4 v0, 0x0

    new-array v0, v0, [Lmrb;

    sput-object v0, Lmrb;->a:[Lmrb;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 201
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 242
    const/4 v0, 0x0

    .line 243
    iget-object v1, p0, Lmrb;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 244
    const/4 v0, 0x1

    iget-object v1, p0, Lmrb;->b:Ljava/lang/String;

    .line 245
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 247
    :cond_0
    iget-object v1, p0, Lmrb;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 248
    const/4 v1, 0x2

    iget-object v2, p0, Lmrb;->c:Ljava/lang/String;

    .line 249
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 251
    :cond_1
    iget-object v1, p0, Lmrb;->e:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 252
    const/4 v1, 0x3

    iget-object v2, p0, Lmrb;->e:Ljava/lang/String;

    .line 253
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 255
    :cond_2
    iget-object v1, p0, Lmrb;->f:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 256
    const/4 v1, 0x4

    iget-object v2, p0, Lmrb;->f:Ljava/lang/String;

    .line 257
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 259
    :cond_3
    iget-object v1, p0, Lmrb;->g:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 260
    const/4 v1, 0x5

    iget-object v2, p0, Lmrb;->g:Ljava/lang/Integer;

    .line 261
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 263
    :cond_4
    iget-object v1, p0, Lmrb;->d:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 264
    const/4 v1, 0x6

    iget-object v2, p0, Lmrb;->d:Ljava/lang/String;

    .line 265
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 267
    :cond_5
    iget-object v1, p0, Lmrb;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 268
    iput v0, p0, Lmrb;->ai:I

    .line 269
    return v0
.end method

.method public a(Loxn;)Lmrb;
    .locals 2

    .prologue
    .line 277
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 278
    sparse-switch v0, :sswitch_data_0

    .line 282
    iget-object v1, p0, Lmrb;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 283
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmrb;->ah:Ljava/util/List;

    .line 286
    :cond_1
    iget-object v1, p0, Lmrb;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 288
    :sswitch_0
    return-object p0

    .line 293
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmrb;->b:Ljava/lang/String;

    goto :goto_0

    .line 297
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmrb;->c:Ljava/lang/String;

    goto :goto_0

    .line 301
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmrb;->e:Ljava/lang/String;

    goto :goto_0

    .line 305
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmrb;->f:Ljava/lang/String;

    goto :goto_0

    .line 309
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmrb;->g:Ljava/lang/Integer;

    goto :goto_0

    .line 313
    :sswitch_6
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmrb;->d:Ljava/lang/String;

    goto :goto_0

    .line 278
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 218
    iget-object v0, p0, Lmrb;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 219
    const/4 v0, 0x1

    iget-object v1, p0, Lmrb;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 221
    :cond_0
    iget-object v0, p0, Lmrb;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 222
    const/4 v0, 0x2

    iget-object v1, p0, Lmrb;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 224
    :cond_1
    iget-object v0, p0, Lmrb;->e:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 225
    const/4 v0, 0x3

    iget-object v1, p0, Lmrb;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 227
    :cond_2
    iget-object v0, p0, Lmrb;->f:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 228
    const/4 v0, 0x4

    iget-object v1, p0, Lmrb;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 230
    :cond_3
    iget-object v0, p0, Lmrb;->g:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 231
    const/4 v0, 0x5

    iget-object v1, p0, Lmrb;->g:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 233
    :cond_4
    iget-object v0, p0, Lmrb;->d:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 234
    const/4 v0, 0x6

    iget-object v1, p0, Lmrb;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 236
    :cond_5
    iget-object v0, p0, Lmrb;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 238
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 197
    invoke-virtual {p0, p1}, Lmrb;->a(Loxn;)Lmrb;

    move-result-object v0

    return-object v0
.end method
