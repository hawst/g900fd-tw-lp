.class public final Ldqh;
.super Lhny;
.source "PG"


# instance fields
.field private final a:I

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 29
    const-string v0, "UpdateCollectionShareLinkTask"

    invoke-direct {p0, p1, v0}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 31
    iput p2, p0, Ldqh;->a:I

    .line 32
    iput-object p3, p0, Ldqh;->b:Ljava/lang/String;

    .line 33
    iput-object p4, p0, Ldqh;->c:Ljava/lang/String;

    .line 35
    iput-boolean p5, p0, Ldqh;->d:Z

    .line 36
    return-void
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 40
    invoke-virtual {p0}, Ldqh;->f()Landroid/content/Context;

    move-result-object v1

    .line 42
    const-class v0, Lkfd;

    invoke-static {v1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkfd;

    .line 44
    new-instance v2, Ldoa;

    iget v3, p0, Ldqh;->a:I

    iget-object v4, p0, Ldqh;->b:Ljava/lang/String;

    iget-object v5, p0, Ldqh;->c:Ljava/lang/String;

    invoke-direct {v2, v1, v3, v4, v5}, Ldoa;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V

    .line 46
    iget-boolean v1, p0, Ldqh;->d:Z

    invoke-virtual {v2, v1}, Ldoa;->b(Z)V

    .line 48
    invoke-interface {v0, v2}, Lkfd;->a(Lkff;)V

    .line 50
    new-instance v0, Lhoz;

    iget v1, v2, Lkff;->i:I

    invoke-direct {v0, v1, v6, v6}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    .line 51
    invoke-virtual {v0}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v1

    .line 52
    const-string v3, "allow_share_via_link"

    iget-boolean v4, p0, Ldqh;->d:Z

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 53
    invoke-virtual {v2}, Ldoa;->t()Z

    move-result v3

    if-nez v3, :cond_0

    .line 54
    const-string v3, "album_link_url"

    invoke-virtual {v2}, Ldoa;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    :cond_0
    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 61
    invoke-virtual {p0}, Ldqh;->f()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a0aef

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
