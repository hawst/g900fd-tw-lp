.class public final Lnne;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnne;


# instance fields
.field private b:Lnnd;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/Boolean;

.field private h:Ljava/lang/Long;

.field private i:Ljava/lang/Long;

.field private j:Ljava/lang/Boolean;

.field private k:[Lnnf;

.field private l:[Lnnf;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 923
    const/4 v0, 0x0

    new-array v0, v0, [Lnne;

    sput-object v0, Lnne;->a:[Lnne;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 924
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1013
    const/4 v0, 0x0

    iput-object v0, p0, Lnne;->b:Lnnd;

    .line 1032
    sget-object v0, Lnnf;->a:[Lnnf;

    iput-object v0, p0, Lnne;->k:[Lnnf;

    .line 1035
    sget-object v0, Lnnf;->a:[Lnnf;

    iput-object v0, p0, Lnne;->l:[Lnnf;

    .line 924
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1097
    .line 1098
    iget-object v0, p0, Lnne;->b:Lnnd;

    if-eqz v0, :cond_e

    .line 1099
    const/4 v0, 0x1

    iget-object v2, p0, Lnne;->b:Lnnd;

    .line 1100
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1102
    :goto_0
    iget-object v2, p0, Lnne;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 1103
    const/4 v2, 0x2

    iget-object v3, p0, Lnne;->c:Ljava/lang/String;

    .line 1104
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1106
    :cond_0
    iget-object v2, p0, Lnne;->e:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 1107
    const/4 v2, 0x3

    iget-object v3, p0, Lnne;->e:Ljava/lang/String;

    .line 1108
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1110
    :cond_1
    iget-object v2, p0, Lnne;->f:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 1111
    const/4 v2, 0x4

    iget-object v3, p0, Lnne;->f:Ljava/lang/String;

    .line 1112
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1114
    :cond_2
    iget-object v2, p0, Lnne;->g:Ljava/lang/Boolean;

    if-eqz v2, :cond_3

    .line 1115
    const/4 v2, 0x5

    iget-object v3, p0, Lnne;->g:Ljava/lang/Boolean;

    .line 1116
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1118
    :cond_3
    iget-object v2, p0, Lnne;->h:Ljava/lang/Long;

    if-eqz v2, :cond_4

    .line 1119
    const/4 v2, 0x6

    iget-object v3, p0, Lnne;->h:Ljava/lang/Long;

    .line 1120
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Loxo;->f(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 1122
    :cond_4
    iget-object v2, p0, Lnne;->i:Ljava/lang/Long;

    if-eqz v2, :cond_5

    .line 1123
    const/4 v2, 0x7

    iget-object v3, p0, Lnne;->i:Ljava/lang/Long;

    .line 1124
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Loxo;->f(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 1126
    :cond_5
    iget-object v2, p0, Lnne;->d:Ljava/lang/String;

    if-eqz v2, :cond_6

    .line 1127
    const/16 v2, 0x8

    iget-object v3, p0, Lnne;->d:Ljava/lang/String;

    .line 1128
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1130
    :cond_6
    iget-object v2, p0, Lnne;->j:Ljava/lang/Boolean;

    if-eqz v2, :cond_7

    .line 1131
    const/16 v2, 0x9

    iget-object v3, p0, Lnne;->j:Ljava/lang/Boolean;

    .line 1132
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1134
    :cond_7
    iget-object v2, p0, Lnne;->k:[Lnnf;

    if-eqz v2, :cond_9

    .line 1135
    iget-object v3, p0, Lnne;->k:[Lnnf;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_9

    aget-object v5, v3, v2

    .line 1136
    if-eqz v5, :cond_8

    .line 1137
    const/16 v6, 0xa

    .line 1138
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 1135
    :cond_8
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1142
    :cond_9
    iget-object v2, p0, Lnne;->l:[Lnnf;

    if-eqz v2, :cond_b

    .line 1143
    iget-object v2, p0, Lnne;->l:[Lnnf;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_b

    aget-object v4, v2, v1

    .line 1144
    if-eqz v4, :cond_a

    .line 1145
    const/16 v5, 0xb

    .line 1146
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 1143
    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1150
    :cond_b
    iget-object v1, p0, Lnne;->m:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 1151
    const/16 v1, 0xc

    iget-object v2, p0, Lnne;->m:Ljava/lang/String;

    .line 1152
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1154
    :cond_c
    iget-object v1, p0, Lnne;->n:Ljava/lang/String;

    if-eqz v1, :cond_d

    .line 1155
    const/16 v1, 0xd

    iget-object v2, p0, Lnne;->n:Ljava/lang/String;

    .line 1156
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1158
    :cond_d
    iget-object v1, p0, Lnne;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1159
    iput v0, p0, Lnne;->ai:I

    .line 1160
    return v0

    :cond_e
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lnne;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1168
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1169
    sparse-switch v0, :sswitch_data_0

    .line 1173
    iget-object v2, p0, Lnne;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 1174
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnne;->ah:Ljava/util/List;

    .line 1177
    :cond_1
    iget-object v2, p0, Lnne;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1179
    :sswitch_0
    return-object p0

    .line 1184
    :sswitch_1
    iget-object v0, p0, Lnne;->b:Lnnd;

    if-nez v0, :cond_2

    .line 1185
    new-instance v0, Lnnd;

    invoke-direct {v0}, Lnnd;-><init>()V

    iput-object v0, p0, Lnne;->b:Lnnd;

    .line 1187
    :cond_2
    iget-object v0, p0, Lnne;->b:Lnnd;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1191
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnne;->c:Ljava/lang/String;

    goto :goto_0

    .line 1195
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnne;->e:Ljava/lang/String;

    goto :goto_0

    .line 1199
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnne;->f:Ljava/lang/String;

    goto :goto_0

    .line 1203
    :sswitch_5
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnne;->g:Ljava/lang/Boolean;

    goto :goto_0

    .line 1207
    :sswitch_6
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lnne;->h:Ljava/lang/Long;

    goto :goto_0

    .line 1211
    :sswitch_7
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lnne;->i:Ljava/lang/Long;

    goto :goto_0

    .line 1215
    :sswitch_8
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnne;->d:Ljava/lang/String;

    goto :goto_0

    .line 1219
    :sswitch_9
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnne;->j:Ljava/lang/Boolean;

    goto :goto_0

    .line 1223
    :sswitch_a
    const/16 v0, 0x52

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1224
    iget-object v0, p0, Lnne;->k:[Lnnf;

    if-nez v0, :cond_4

    move v0, v1

    .line 1225
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnnf;

    .line 1226
    iget-object v3, p0, Lnne;->k:[Lnnf;

    if-eqz v3, :cond_3

    .line 1227
    iget-object v3, p0, Lnne;->k:[Lnnf;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1229
    :cond_3
    iput-object v2, p0, Lnne;->k:[Lnnf;

    .line 1230
    :goto_2
    iget-object v2, p0, Lnne;->k:[Lnnf;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 1231
    iget-object v2, p0, Lnne;->k:[Lnnf;

    new-instance v3, Lnnf;

    invoke-direct {v3}, Lnnf;-><init>()V

    aput-object v3, v2, v0

    .line 1232
    iget-object v2, p0, Lnne;->k:[Lnnf;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1233
    invoke-virtual {p1}, Loxn;->a()I

    .line 1230
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1224
    :cond_4
    iget-object v0, p0, Lnne;->k:[Lnnf;

    array-length v0, v0

    goto :goto_1

    .line 1236
    :cond_5
    iget-object v2, p0, Lnne;->k:[Lnnf;

    new-instance v3, Lnnf;

    invoke-direct {v3}, Lnnf;-><init>()V

    aput-object v3, v2, v0

    .line 1237
    iget-object v2, p0, Lnne;->k:[Lnnf;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1241
    :sswitch_b
    const/16 v0, 0x5a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1242
    iget-object v0, p0, Lnne;->l:[Lnnf;

    if-nez v0, :cond_7

    move v0, v1

    .line 1243
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lnnf;

    .line 1244
    iget-object v3, p0, Lnne;->l:[Lnnf;

    if-eqz v3, :cond_6

    .line 1245
    iget-object v3, p0, Lnne;->l:[Lnnf;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1247
    :cond_6
    iput-object v2, p0, Lnne;->l:[Lnnf;

    .line 1248
    :goto_4
    iget-object v2, p0, Lnne;->l:[Lnnf;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_8

    .line 1249
    iget-object v2, p0, Lnne;->l:[Lnnf;

    new-instance v3, Lnnf;

    invoke-direct {v3}, Lnnf;-><init>()V

    aput-object v3, v2, v0

    .line 1250
    iget-object v2, p0, Lnne;->l:[Lnnf;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1251
    invoke-virtual {p1}, Loxn;->a()I

    .line 1248
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 1242
    :cond_7
    iget-object v0, p0, Lnne;->l:[Lnnf;

    array-length v0, v0

    goto :goto_3

    .line 1254
    :cond_8
    iget-object v2, p0, Lnne;->l:[Lnnf;

    new-instance v3, Lnnf;

    invoke-direct {v3}, Lnnf;-><init>()V

    aput-object v3, v2, v0

    .line 1255
    iget-object v2, p0, Lnne;->l:[Lnnf;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1259
    :sswitch_c
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnne;->m:Ljava/lang/String;

    goto/16 :goto_0

    .line 1263
    :sswitch_d
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnne;->n:Ljava/lang/String;

    goto/16 :goto_0

    .line 1169
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1044
    iget-object v1, p0, Lnne;->b:Lnnd;

    if-eqz v1, :cond_0

    .line 1045
    const/4 v1, 0x1

    iget-object v2, p0, Lnne;->b:Lnnd;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 1047
    :cond_0
    iget-object v1, p0, Lnne;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1048
    const/4 v1, 0x2

    iget-object v2, p0, Lnne;->c:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 1050
    :cond_1
    iget-object v1, p0, Lnne;->e:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1051
    const/4 v1, 0x3

    iget-object v2, p0, Lnne;->e:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 1053
    :cond_2
    iget-object v1, p0, Lnne;->f:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 1054
    const/4 v1, 0x4

    iget-object v2, p0, Lnne;->f:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 1056
    :cond_3
    iget-object v1, p0, Lnne;->g:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 1057
    const/4 v1, 0x5

    iget-object v2, p0, Lnne;->g:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IZ)V

    .line 1059
    :cond_4
    iget-object v1, p0, Lnne;->h:Ljava/lang/Long;

    if-eqz v1, :cond_5

    .line 1060
    const/4 v1, 0x6

    iget-object v2, p0, Lnne;->h:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Loxo;->b(IJ)V

    .line 1062
    :cond_5
    iget-object v1, p0, Lnne;->i:Ljava/lang/Long;

    if-eqz v1, :cond_6

    .line 1063
    const/4 v1, 0x7

    iget-object v2, p0, Lnne;->i:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Loxo;->b(IJ)V

    .line 1065
    :cond_6
    iget-object v1, p0, Lnne;->d:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 1066
    const/16 v1, 0x8

    iget-object v2, p0, Lnne;->d:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 1068
    :cond_7
    iget-object v1, p0, Lnne;->j:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    .line 1069
    const/16 v1, 0x9

    iget-object v2, p0, Lnne;->j:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IZ)V

    .line 1071
    :cond_8
    iget-object v1, p0, Lnne;->k:[Lnnf;

    if-eqz v1, :cond_a

    .line 1072
    iget-object v2, p0, Lnne;->k:[Lnnf;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_a

    aget-object v4, v2, v1

    .line 1073
    if-eqz v4, :cond_9

    .line 1074
    const/16 v5, 0xa

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 1072
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1078
    :cond_a
    iget-object v1, p0, Lnne;->l:[Lnnf;

    if-eqz v1, :cond_c

    .line 1079
    iget-object v1, p0, Lnne;->l:[Lnnf;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_c

    aget-object v3, v1, v0

    .line 1080
    if-eqz v3, :cond_b

    .line 1081
    const/16 v4, 0xb

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 1079
    :cond_b
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1085
    :cond_c
    iget-object v0, p0, Lnne;->m:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 1086
    const/16 v0, 0xc

    iget-object v1, p0, Lnne;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1088
    :cond_d
    iget-object v0, p0, Lnne;->n:Ljava/lang/String;

    if-eqz v0, :cond_e

    .line 1089
    const/16 v0, 0xd

    iget-object v1, p0, Lnne;->n:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1091
    :cond_e
    iget-object v0, p0, Lnne;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1093
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 920
    invoke-virtual {p0, p1}, Lnne;->a(Loxn;)Lnne;

    move-result-object v0

    return-object v0
.end method
