.class public abstract Lhos;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lae;

.field public c:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lae;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lhos;->a:Landroid/content/Context;

    .line 36
    iput-object p2, p0, Lhos;->b:Lae;

    .line 37
    return-void
.end method


# virtual methods
.method public a(II)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 151
    if-nez p1, :cond_0

    move-object v1, v0

    .line 152
    :goto_0
    if-nez p2, :cond_1

    .line 153
    :goto_1
    invoke-virtual {p0, v1, v0}, Lhos;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    return-void

    .line 151
    :cond_0
    iget-object v1, p0, Lhos;->a:Landroid/content/Context;

    invoke-virtual {v1, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 152
    :cond_1
    iget-object v0, p0, Lhos;->a:Landroid/content/Context;

    invoke-virtual {v0, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public a(Lhny;)V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lhos;->a(Lhny;Z)V

    .line 46
    return-void
.end method

.method public a(Lhny;Z)V
    .locals 3

    .prologue
    .line 55
    invoke-virtual {p1}, Lhny;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lhny;->V_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lhny;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2, p2}, Lhos;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 57
    return-void
.end method

.method public abstract a(Ljava/lang/String;)V
.end method

.method public abstract a(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
.end method

.method public abstract a(Lu;Ljava/lang/String;Z)V
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 130
    iput-boolean p1, p0, Lhos;->c:Z

    .line 131
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 119
    iget-boolean v0, p0, Lhos;->c:Z

    return v0
.end method

.method public abstract a(Lhoz;)Z
.end method

.method public abstract a(Ljava/lang/Exception;)Z
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 69
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, p2, v1}, Lhos;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 70
    return-void
.end method
