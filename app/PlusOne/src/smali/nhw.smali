.class public final Lnhw;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lnkh;

.field private b:Lnja;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 962
    invoke-direct {p0}, Loxq;-><init>()V

    .line 965
    const/4 v0, 0x0

    iput-object v0, p0, Lnhw;->b:Lnja;

    .line 968
    sget-object v0, Lnkh;->a:[Lnkh;

    iput-object v0, p0, Lnhw;->a:[Lnkh;

    .line 962
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 989
    .line 990
    iget-object v0, p0, Lnhw;->b:Lnja;

    if-eqz v0, :cond_2

    .line 991
    const/4 v0, 0x1

    iget-object v2, p0, Lnhw;->b:Lnja;

    .line 992
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 994
    :goto_0
    iget-object v2, p0, Lnhw;->a:[Lnkh;

    if-eqz v2, :cond_1

    .line 995
    iget-object v2, p0, Lnhw;->a:[Lnkh;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 996
    if-eqz v4, :cond_0

    .line 997
    const/4 v5, 0x2

    .line 998
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 995
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1002
    :cond_1
    iget-object v1, p0, Lnhw;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1003
    iput v0, p0, Lnhw;->ai:I

    .line 1004
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lnhw;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1012
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1013
    sparse-switch v0, :sswitch_data_0

    .line 1017
    iget-object v2, p0, Lnhw;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 1018
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnhw;->ah:Ljava/util/List;

    .line 1021
    :cond_1
    iget-object v2, p0, Lnhw;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1023
    :sswitch_0
    return-object p0

    .line 1028
    :sswitch_1
    iget-object v0, p0, Lnhw;->b:Lnja;

    if-nez v0, :cond_2

    .line 1029
    new-instance v0, Lnja;

    invoke-direct {v0}, Lnja;-><init>()V

    iput-object v0, p0, Lnhw;->b:Lnja;

    .line 1031
    :cond_2
    iget-object v0, p0, Lnhw;->b:Lnja;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1035
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1036
    iget-object v0, p0, Lnhw;->a:[Lnkh;

    if-nez v0, :cond_4

    move v0, v1

    .line 1037
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnkh;

    .line 1038
    iget-object v3, p0, Lnhw;->a:[Lnkh;

    if-eqz v3, :cond_3

    .line 1039
    iget-object v3, p0, Lnhw;->a:[Lnkh;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1041
    :cond_3
    iput-object v2, p0, Lnhw;->a:[Lnkh;

    .line 1042
    :goto_2
    iget-object v2, p0, Lnhw;->a:[Lnkh;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 1043
    iget-object v2, p0, Lnhw;->a:[Lnkh;

    new-instance v3, Lnkh;

    invoke-direct {v3}, Lnkh;-><init>()V

    aput-object v3, v2, v0

    .line 1044
    iget-object v2, p0, Lnhw;->a:[Lnkh;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1045
    invoke-virtual {p1}, Loxn;->a()I

    .line 1042
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1036
    :cond_4
    iget-object v0, p0, Lnhw;->a:[Lnkh;

    array-length v0, v0

    goto :goto_1

    .line 1048
    :cond_5
    iget-object v2, p0, Lnhw;->a:[Lnkh;

    new-instance v3, Lnkh;

    invoke-direct {v3}, Lnkh;-><init>()V

    aput-object v3, v2, v0

    .line 1049
    iget-object v2, p0, Lnhw;->a:[Lnkh;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1013
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 973
    iget-object v0, p0, Lnhw;->b:Lnja;

    if-eqz v0, :cond_0

    .line 974
    const/4 v0, 0x1

    iget-object v1, p0, Lnhw;->b:Lnja;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 976
    :cond_0
    iget-object v0, p0, Lnhw;->a:[Lnkh;

    if-eqz v0, :cond_2

    .line 977
    iget-object v1, p0, Lnhw;->a:[Lnkh;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 978
    if-eqz v3, :cond_1

    .line 979
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 977
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 983
    :cond_2
    iget-object v0, p0, Lnhw;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 985
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 958
    invoke-virtual {p0, p1}, Lnhw;->a(Loxn;)Lnhw;

    move-result-object v0

    return-object v0
.end method
