.class public final Lkmy;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lkjj;
.implements Llqz;
.implements Llrd;
.implements Llrg;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lkji;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lkji;

.field private final d:Lkjf;

.field private final e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lkjk;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/app/Activity;Llqr;)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lkmy;->b:Ljava/util/ArrayList;

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lkmy;->c:Lkji;

    .line 29
    new-instance v0, Lkjf;

    invoke-direct {v0}, Lkjf;-><init>()V

    iput-object v0, p0, Lkmy;->d:Lkjf;

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lkmy;->e:Ljava/util/ArrayList;

    .line 38
    iput-object p1, p0, Lkmy;->a:Landroid/content/Context;

    .line 39
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 40
    return-void
.end method


# virtual methods
.method public a()Lkji;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lkmy;->c:Lkji;

    return-object v0
.end method

.method public a(I)Lkji;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lkmy;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkji;

    return-object v0
.end method

.method public a(Ljava/lang/Class;)Lkji;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lkji;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 81
    iget-object v0, p0, Lkmy;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkji;

    .line 82
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-ne v2, p1, :cond_0

    .line 86
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 44
    if-nez p1, :cond_1

    move-object v1, v2

    .line 45
    :goto_0
    iget-object v0, p0, Lkmy;->a:Landroid/content/Context;

    const-class v3, Lkjl;

    invoke-static {v0, v3}, Llnh;->c(Landroid/content/Context;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkjl;

    .line 46
    iget-object v4, p0, Lkmy;->a:Landroid/content/Context;

    invoke-interface {v0, v4}, Lkjl;->a(Landroid/content/Context;)Lkji;

    move-result-object v4

    .line 47
    if-eqz v4, :cond_0

    .line 48
    iget-object v0, p0, Lkmy;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 49
    if-nez p1, :cond_2

    move-object v0, v2

    :goto_2
    invoke-interface {v4, v0}, Lkji;->a(Landroid/os/Bundle;)V

    .line 50
    invoke-interface {v4}, Lkji;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 51
    iput-object v4, p0, Lkmy;->c:Lkji;

    goto :goto_1

    .line 44
    :cond_1
    const-string v0, "SELECTED_SHARELET_NAME"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 49
    :cond_2
    invoke-interface {v4}, Lkji;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    goto :goto_2

    .line 55
    :cond_3
    return-void
.end method

.method public a(Lkji;)V
    .locals 2

    .prologue
    .line 102
    iget-object v0, p0, Lkmy;->c:Lkji;

    if-ne v0, p1, :cond_1

    .line 115
    :cond_0
    return-void

    .line 105
    :cond_1
    iget-object v0, p0, Lkmy;->c:Lkji;

    if-eqz v0, :cond_2

    .line 106
    iget-object v0, p0, Lkmy;->c:Lkji;

    invoke-interface {v0}, Lkji;->b()V

    .line 108
    :cond_2
    iput-object p1, p0, Lkmy;->c:Lkji;

    .line 109
    iget-object v0, p0, Lkmy;->c:Lkji;

    if-eqz v0, :cond_3

    .line 110
    iget-object v0, p0, Lkmy;->c:Lkji;

    invoke-interface {v0}, Lkji;->a()V

    .line 112
    :cond_3
    iget-object v0, p0, Lkmy;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkjk;

    .line 113
    invoke-interface {v0}, Lkjk;->d()V

    goto :goto_0
.end method

.method public a(Lkjk;)V
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lkmy;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 120
    return-void
.end method

.method public b()Lkjf;
    .locals 1

    .prologue
    .line 96
    invoke-virtual {p0}, Lkmy;->a()Lkji;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lkmy;->a()Lkji;

    move-result-object v0

    invoke-interface {v0}, Lkji;->e()Lkjf;

    move-result-object v0

    .line 97
    :goto_0
    if-nez v0, :cond_0

    iget-object v0, p0, Lkmy;->d:Lkjf;

    :cond_0
    return-object v0

    .line 96
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 59
    const-string v1, "SELECTED_SHARELET_NAME"

    .line 60
    invoke-virtual {p0}, Lkmy;->a()Lkji;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 59
    :goto_0
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    iget-object v0, p0, Lkmy;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkji;

    .line 62
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 63
    invoke-interface {v0, v2}, Lkji;->b(Landroid/os/Bundle;)V

    .line 64
    invoke-interface {v0}, Lkji;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_1

    .line 60
    :cond_0
    invoke-virtual {p0}, Lkmy;->a()Lkji;

    move-result-object v0

    invoke-interface {v0}, Lkji;->l()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 66
    :cond_1
    return-void
.end method

.method public c()I
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lkmy;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method
