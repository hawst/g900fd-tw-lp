.class public final Lkuf;
.super Llol;
.source "PG"

# interfaces
.implements Lbc;
.implements Lhjj;
.implements Lhmq;
.implements Lhob;
.implements Lkch;
.implements Lkut;
.implements Lkvs;
.implements Llgs;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Llol;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lhjj;",
        "Lhmq;",
        "Lhob;",
        "Lkch;",
        "Lkut;",
        "Lkvs;",
        "Llgs;"
    }
.end annotation


# instance fields
.field private final N:Lhje;

.field private O:Lhoc;

.field private P:Lhee;

.field private Q:Llhd;

.field private R:Lkci;

.field private S:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

.field private T:Lkuq;

.field private U:Z

.field private V:Lkvq;

.field private W:Z

.field private X:Z

.field private Y:Ljava/lang/String;

.field private Z:Liax;

.field private final aa:Licq;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 88
    invoke-direct {p0}, Llol;-><init>()V

    .line 114
    new-instance v0, Lhje;

    iget-object v1, p0, Lkuf;->av:Llqm;

    invoke-direct {v0, p0, v1, p0}, Lhje;-><init>(Lu;Llqr;Lhjj;)V

    iput-object v0, p0, Lkuf;->N:Lhje;

    .line 117
    new-instance v0, Lhoc;

    iget-object v1, p0, Lkuf;->av:Llqm;

    invoke-direct {v0, p0, v1}, Lhoc;-><init>(Lu;Llqr;)V

    iget-object v1, p0, Lkuf;->au:Llnh;

    .line 118
    invoke-virtual {v0, v1}, Lhoc;->a(Llnh;)Lhoc;

    move-result-object v0

    iput-object v0, p0, Lkuf;->O:Lhoc;

    .line 122
    new-instance v0, Llhd;

    invoke-direct {v0}, Llhd;-><init>()V

    iput-object v0, p0, Lkuf;->Q:Llhd;

    .line 124
    new-instance v0, Lkci;

    iget-object v1, p0, Lkuf;->av:Llqm;

    const v2, 0x7f10005b

    invoke-direct {v0, p0, v1, v2, p0}, Lkci;-><init>(Lu;Llqr;ILkch;)V

    iput-object v0, p0, Lkuf;->R:Lkci;

    .line 129
    const/4 v0, 0x0

    iput-boolean v0, p0, Lkuf;->U:Z

    .line 138
    new-instance v0, Licq;

    iget-object v1, p0, Lkuf;->av:Llqm;

    invoke-direct {v0, v1}, Licq;-><init>(Llqr;)V

    const v1, 0x7f0a057b

    .line 139
    invoke-virtual {v0, v1}, Licq;->a(I)Licq;

    move-result-object v0

    new-instance v1, Lkug;

    invoke-direct {v1, p0}, Lkug;-><init>(Lkuf;)V

    .line 140
    invoke-virtual {v0, v1}, Licq;->a(Lico;)Licq;

    move-result-object v0

    iput-object v0, p0, Lkuf;->aa:Licq;

    .line 138
    return-void
.end method

.method private U()Z
    .locals 1

    .prologue
    .line 346
    iget-object v0, p0, Lkuf;->Y:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private V()V
    .locals 4

    .prologue
    .line 366
    iget-object v0, p0, Lkuf;->O:Lhoc;

    const-string v1, "fetch_newer"

    invoke-virtual {v0, v1}, Lhoc;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lkuf;->n()Lz;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 367
    iget-boolean v0, p0, Lkuf;->X:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lkuf;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 368
    iget-object v0, p0, Lkuf;->aa:Licq;

    sget-object v1, Lict;->a:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    .line 371
    :cond_0
    new-instance v0, Lktk;

    iget-object v1, p0, Lkuf;->at:Llnl;

    iget-object v2, p0, Lkuf;->P:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lktk;-><init>(Landroid/content/Context;IZ)V

    .line 372
    const-string v1, "fetch_newer"

    invoke-virtual {v0, v1}, Lhny;->b(Ljava/lang/String;)Lhny;

    .line 373
    iget-object v1, p0, Lkuf;->O:Lhoc;

    invoke-virtual {v1, v0}, Lhoc;->b(Lhny;)V

    .line 376
    :cond_1
    invoke-direct {p0}, Lkuf;->W()V

    .line 377
    return-void
.end method

.method private W()V
    .locals 1

    .prologue
    .line 534
    invoke-virtual {p0}, Lkuf;->n()Lz;

    move-result-object v0

    check-cast v0, Los;

    invoke-virtual {v0}, Los;->h()Loo;

    move-result-object v0

    invoke-virtual {v0}, Loo;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 535
    iget-object v0, p0, Lkuf;->R:Lkci;

    invoke-virtual {v0}, Lkci;->e()V

    .line 537
    :cond_0
    iget-object v0, p0, Lkuf;->N:Lhje;

    invoke-virtual {v0}, Lhje;->a()V

    .line 538
    return-void
.end method

.method static synthetic a(Lkuf;)Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lkuf;->S:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    return-object v0
.end method

.method static synthetic b(Lkuf;)Llnl;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lkuf;->at:Llnl;

    return-object v0
.end method

.method static synthetic c(Lkuf;)Lhee;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lkuf;->P:Lhee;

    return-object v0
.end method

.method private e()Z
    .locals 1

    .prologue
    .line 312
    iget-object v0, p0, Lkuf;->T:Lkuq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lkuf;->T:Lkuq;

    invoke-virtual {v0}, Lkuq;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 491
    sget-object v0, Lhmw;->H:Lhmw;

    return-object v0
.end method

.method public K_()V
    .locals 1

    .prologue
    .line 542
    iget-object v0, p0, Lkuf;->R:Lkci;

    invoke-virtual {v0}, Lkci;->c()V

    .line 543
    invoke-direct {p0}, Lkuf;->V()V

    .line 544
    return-void
.end method

.method public Y()Z
    .locals 1

    .prologue
    .line 308
    iget-object v0, p0, Lkuf;->S:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->l()Z

    move-result v0

    return v0
.end method

.method public Z()Z
    .locals 1

    .prologue
    .line 548
    const/4 v0, 0x1

    return v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9

    .prologue
    .line 201
    iget-object v0, p0, Lkuf;->at:Llnl;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 202
    const v1, 0x7f0400e2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v8

    .line 203
    const v0, 0x7f100304

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    iput-object v0, p0, Lkuf;->S:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    .line 204
    new-instance v0, Lkuq;

    iget-object v1, p0, Lkuf;->at:Llnl;

    iget-object v2, p0, Lkuf;->P:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    iget-object v4, p0, Lkuf;->S:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    iget-object v5, p0, Lkuf;->V:Lkvq;

    new-instance v6, Llkd;

    iget-object v3, p0, Lkuf;->Q:Llhd;

    invoke-direct {v6, v3}, Llkd;-><init>(Llhc;)V

    .line 206
    invoke-virtual {p0}, Lkuf;->n()Lz;

    move-result-object v3

    instance-of v7, v3, Lcom/google/android/apps/plus/phone/HomeActivity;

    move-object v3, p0

    invoke-direct/range {v0 .. v7}, Lkuq;-><init>(Landroid/content/Context;ILkut;Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;Lkvq;Llkc;Z)V

    iput-object v0, p0, Lkuf;->T:Lkuq;

    .line 207
    iget-object v0, p0, Lkuf;->S:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    iget-object v1, p0, Lkuf;->T:Lkuq;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(Landroid/widget/ListAdapter;)V

    .line 208
    iget-object v0, p0, Lkuf;->S:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    const v1, 0x7f020415

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->g(I)V

    .line 209
    return-object v8
.end method

.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 273
    packed-switch p1, :pswitch_data_0

    .line 282
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 275
    :pswitch_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lkuf;->U:Z

    .line 276
    new-instance v0, Lkup;

    invoke-virtual {p0}, Lkuf;->n()Lz;

    move-result-object v1

    iget-object v2, p0, Lkuf;->P:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    const/4 v3, 0x2

    new-array v3, v3, [I

    fill-array-data v3, :array_0

    invoke-direct {v0, v1, v2, v3}, Lkup;-><init>(Landroid/content/Context;I[I)V

    goto :goto_0

    .line 273
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch

    .line 276
    :array_0
    .array-data 4
        0x1
        0x3
    .end array-data
.end method

.method public a(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 524
    return-void
.end method

.method public a(IZLandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 528
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 165
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 167
    if-eqz p1, :cond_0

    .line 168
    const-string v0, "squares_refresh"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lkuf;->W:Z

    .line 169
    const-string v0, "squares_datapresent"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lkuf;->X:Z

    .line 175
    :goto_0
    new-instance v0, Lkvq;

    iget-object v1, p0, Lkuf;->at:Llnl;

    iget-object v2, p0, Lkuf;->P:Lhee;

    .line 176
    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    invoke-direct {v0, v1, p0, v2}, Lkvq;-><init>(Landroid/content/Context;Lu;I)V

    iput-object v0, p0, Lkuf;->V:Lkvq;

    .line 177
    iget-object v0, p0, Lkuf;->V:Lkvq;

    invoke-virtual {v0, p0}, Lkvq;->a(Lkvs;)V

    .line 178
    return-void

    .line 171
    :cond_0
    invoke-virtual {p0}, Lkuf;->k()Landroid/os/Bundle;

    move-result-object v0

    .line 172
    const-string v1, "refresh"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lkuf;->W:Z

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 496
    const-string v0, "dismiss_invitation"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 497
    invoke-virtual {p0}, Lkuf;->n()Lz;

    move-result-object v1

    .line 498
    const-string v0, "square_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 500
    iget-object v0, p0, Lkuf;->au:Llnh;

    const-class v3, Lhoc;

    invoke-virtual {v0, v3}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    new-instance v3, Lkvn;

    iget-object v4, p0, Lkuf;->P:Lhee;

    .line 501
    invoke-interface {v4}, Lhee;->d()I

    move-result v4

    invoke-direct {v3, v1, v4, v2}, Lkvn;-><init>(Landroid/content/Context;ILjava/lang/String;)V

    .line 500
    invoke-virtual {v0, v3}, Lhoc;->b(Lhny;)V

    .line 503
    const-string v0, "extra_square_id"

    .line 504
    invoke-static {v0, v2}, Lhmt;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    .line 505
    iget-object v0, p0, Lkuf;->P:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v3

    .line 506
    iget-object v0, p0, Lkuf;->au:Llnh;

    const-class v4, Lhms;

    invoke-virtual {v0, v4}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v4, Lhmr;

    invoke-direct {v4, v1, v3}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v1, Lhmv;->cr:Lhmv;

    .line 508
    invoke-virtual {v4, v1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 509
    invoke-virtual {v1, v2}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    .line 506
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 512
    :cond_0
    return-void
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 304
    return-void
.end method

.method public a(Ldo;Landroid/database/Cursor;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 287
    invoke-virtual {p1}, Ldo;->o()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 301
    :cond_0
    :goto_0
    return-void

    .line 289
    :pswitch_0
    iput-boolean v0, p0, Lkuf;->U:Z

    .line 290
    if-eqz p2, :cond_1

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    iput-boolean v0, p0, Lkuf;->X:Z

    .line 291
    iget-object v0, p0, Lkuf;->T:Lkuq;

    invoke-virtual {v0, p2}, Lkuq;->a(Landroid/database/Cursor;)V

    .line 292
    invoke-direct {p0}, Lkuf;->U()Z

    move-result v0

    if-nez v0, :cond_2

    .line 293
    iget-boolean v0, p0, Lkuf;->W:Z

    check-cast p1, Lkup;

    invoke-virtual {p1}, Lkup;->D()Z

    move-result v1

    or-int/2addr v0, v1

    iput-boolean v0, p0, Lkuf;->W:Z

    .line 294
    const/4 v0, 0x0

    iput-object v0, p0, Lkuf;->Y:Ljava/lang/String;

    .line 297
    :cond_2
    invoke-direct {p0}, Lkuf;->W()V

    .line 298
    iget-boolean v0, p0, Lkuf;->X:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lkuf;->aa:Licq;

    sget-object v1, Lict;->b:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lkuf;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lkuf;->W:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lkuf;->aa:Licq;

    sget-object v1, Lict;->a:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lkuf;->aa:Licq;

    invoke-direct {p0}, Lkuf;->U()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lkuf;->Y:Ljava/lang/String;

    :goto_1
    invoke-virtual {v1, v0}, Licq;->a(Ljava/lang/CharSequence;)Licq;

    iget-object v0, p0, Lkuf;->aa:Licq;

    sget-object v1, Lict;->c:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    goto :goto_0

    :cond_5
    const v0, 0x7f0a06f8

    invoke-virtual {p0, v0}, Lkuf;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 287
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 88
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lkuf;->a(Ldo;Landroid/database/Cursor;)V

    return-void
.end method

.method public a(Lhjk;)V
    .locals 4

    .prologue
    .line 238
    const v0, 0x7f10067b

    .line 239
    invoke-interface {p1, v0}, Lhjk;->a(I)Lhjc;

    move-result-object v0

    check-cast v0, Lhjv;

    .line 240
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lhjv;->a(I)V

    .line 242
    const v0, 0x7f0a062a

    invoke-interface {p1, v0}, Lhjk;->d(I)V

    .line 243
    const v0, 0x7f10053c

    new-instance v1, Litl;

    const-string v2, "plus_communities"

    invoke-direct {v1, v2}, Litl;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 246
    iget-object v0, p0, Lkuf;->au:Llnh;

    const-class v1, Lktx;

    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lktx;

    .line 247
    if-eqz v0, :cond_0

    .line 248
    iget-object v0, p0, Lkuf;->au:Llnh;

    const-class v1, Lkty;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkty;

    .line 249
    const v1, 0x7f100693

    const v2, 0x7f10005c

    const/4 v3, 0x0

    .line 251
    invoke-interface {v0}, Lkty;->a()I

    move-result v0

    .line 249
    invoke-interface {p1, v1, v2, v3, v0}, Lhjk;->a(IIII)Landroid/view/MenuItem;

    .line 253
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;I)V
    .locals 7

    .prologue
    .line 408
    iget-object v0, p0, Lkuf;->P:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v2

    .line 409
    iget-object v0, p0, Lkuf;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v3, p0, Lkuf;->at:Llnl;

    invoke-direct {v1, v3, v2}, Lhmr;-><init>(Landroid/content/Context;I)V

    .line 411
    invoke-static {p2}, Lkvu;->b(I)Lhmv;

    move-result-object v3

    invoke-virtual {v1, v3}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    const-string v3, "extra_square_id"

    .line 412
    invoke-static {v3, p1}, Lhmt;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v1, v3}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    .line 409
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 415
    invoke-static {p2}, Lkvu;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 416
    iget-object v0, p0, Lkuf;->V:Lkvq;

    invoke-virtual {v0, p1}, Lkvq;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 417
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 418
    iget-object v0, p0, Lkuf;->Z:Liax;

    iget-object v1, p0, Lkuf;->at:Llnl;

    const-string v5, "g:"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v5, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :goto_0
    const/16 v5, 0x1b

    const/16 v6, 0x6f

    invoke-interface/range {v0 .. v6}, Liax;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;II)V

    .line 423
    :cond_0
    return-void

    .line 418
    :cond_1
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;ILhoz;)V
    .locals 4

    .prologue
    .line 427
    invoke-virtual {p3}, Lhoz;->f()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x3

    if-eq p2, v0, :cond_0

    const/4 v0, 0x2

    if-ne p2, v0, :cond_1

    .line 428
    :cond_0
    invoke-virtual {p0}, Lkuf;->x()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f100326

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/ui/views/ClickableToast;

    .line 429
    const v1, 0x7f0a04b9

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/ClickableToast;->a(I)V

    .line 430
    new-instance v1, Lkui;

    invoke-direct {v1, p0, p1}, Lkui;-><init>(Lkuf;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/ClickableToast;->a(Lliu;)V

    .line 438
    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/social/ui/views/ClickableToast;->a(J)V

    .line 440
    :cond_1
    return-void
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 382
    const-string v0, "fetch_newer"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 383
    iput-boolean v2, p0, Lkuf;->W:Z

    .line 385
    invoke-static {p2}, Lhoz;->a(Lhoz;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 386
    const v0, 0x7f0a09e6

    invoke-virtual {p0, v0}, Lkuf;->e_(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkuf;->Y:Ljava/lang/String;

    iget-boolean v0, p0, Lkuf;->X:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lkuf;->Y:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lkuf;->n()Lz;

    move-result-object v0

    iget-object v1, p0, Lkuf;->Y:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 391
    :cond_0
    :goto_0
    invoke-direct {p0}, Lkuf;->W()V

    .line 393
    new-instance v0, Lkuh;

    invoke-direct {v0, p0}, Lkuh;-><init>(Lkuf;)V

    invoke-static {v0}, Llsx;->a(Ljava/lang/Runnable;)V

    .line 402
    invoke-virtual {p3, v2}, Lhos;->a(Z)V

    .line 404
    :cond_1
    return-void

    .line 388
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lkuf;->Y:Ljava/lang/String;

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v4, 0x0

    const/16 v8, 0x1b

    .line 444
    invoke-virtual {p0}, Lkuf;->n()Lz;

    move-result-object v1

    .line 445
    iget-object v0, p0, Lkuf;->P:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v2

    .line 446
    const-wide/16 v6, 0x0

    move-object v3, p1

    move-object v5, v4

    invoke-static/range {v1 .. v7}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v7

    .line 449
    const-string v0, "suggestion_id"

    invoke-virtual {v7, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 450
    const-string v0, "suggestion_ui"

    invoke-virtual {v7, v0, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 452
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 453
    iget-object v0, p0, Lkuf;->Z:Liax;

    const-string v4, "g:"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v4, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :goto_0
    const/16 v6, 0x6f

    move-object v4, p2

    move v5, v8

    invoke-interface/range {v0 .. v6}, Liax;->b(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;II)V

    .line 457
    :cond_0
    invoke-virtual {p0, v7}, Lkuf;->a(Landroid/content/Intent;)V

    .line 458
    return-void

    .line 453
    :cond_1
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Loo;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 230
    invoke-virtual {p0}, Lkuf;->n()Lz;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/apps/plus/phone/HomeActivity;

    if-nez v0, :cond_0

    .line 231
    invoke-static {p1, v1}, Lley;->a(Loo;Z)V

    .line 232
    invoke-virtual {p1, v1}, Loo;->c(Z)V

    .line 234
    :cond_0
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 257
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 258
    const v2, 0x7f10067b

    if-ne v0, v2, :cond_0

    .line 259
    invoke-direct {p0}, Lkuf;->V()V

    move v0, v1

    .line 268
    :goto_0
    return v0

    .line 261
    :cond_0
    const v2, 0x7f10005c

    if-ne v0, v2, :cond_1

    .line 262
    iget-object v0, p0, Lkuf;->au:Llnh;

    const-class v2, Lktx;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lktx;

    iget-object v2, p0, Lkuf;->P:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    invoke-interface {v0}, Lktx;->a()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkuf;->a(Landroid/content/Intent;)V

    .line 263
    iget-object v0, p0, Lkuf;->at:Llnl;

    const/4 v2, 0x4

    new-instance v3, Lhml;

    invoke-direct {v3}, Lhml;-><init>()V

    new-instance v4, Lhmk;

    sget-object v5, Lomv;->E:Lhmn;

    invoke-direct {v4, v5}, Lhmk;-><init>(Lhmn;)V

    .line 264
    invoke-virtual {v3, v4}, Lhml;->a(Lhmk;)Lhml;

    move-result-object v3

    iget-object v4, p0, Lkuf;->at:Llnl;

    .line 265
    invoke-virtual {v3, v4}, Lhml;->a(Landroid/content/Context;)Lhml;

    move-result-object v3

    .line 263
    invoke-static {v0, v2, v3}, Lhly;->a(Landroid/content/Context;ILhml;)V

    move v0, v1

    .line 266
    goto :goto_0

    .line 268
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aO_()V
    .locals 0

    .prologue
    .line 214
    invoke-super {p0}, Llol;->aO_()V

    .line 216
    invoke-direct {p0}, Lkuf;->W()V

    .line 217
    return-void
.end method

.method public aQ_()Z
    .locals 2

    .prologue
    .line 553
    iget-object v0, p0, Lkuf;->O:Lhoc;

    const-string v1, "fetch_newer"

    invoke-virtual {v0, v1}, Lhoc;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lkuf;->U:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 558
    return-void
.end method

.method public b(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 516
    return-void
.end method

.method public b(Loo;)V
    .locals 0

    .prologue
    .line 566
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 152
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 153
    iget-object v0, p0, Lkuf;->au:Llnh;

    const-class v1, Lhmq;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 154
    iget-object v0, p0, Lkuf;->au:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Lkuf;->P:Lhee;

    .line 155
    iget-object v0, p0, Lkuf;->au:Llnh;

    const-class v1, Llhe;

    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llhe;

    .line 156
    if-eqz v0, :cond_0

    .line 157
    iget-object v1, p0, Lkuf;->Q:Llhd;

    invoke-virtual {v0}, Llhe;->a()Llhc;

    move-result-object v0

    invoke-virtual {v1, v0}, Llhd;->a(Llhc;)V

    .line 159
    :cond_0
    iget-object v0, p0, Lkuf;->au:Llnh;

    const-class v1, Liax;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liax;

    iput-object v0, p0, Lkuf;->Z:Liax;

    .line 160
    iget-object v0, p0, Lkuf;->au:Llnh;

    const-class v1, Lhoc;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    invoke-virtual {v0, p0}, Lhoc;->a(Lhob;)Lhoc;

    .line 161
    return-void
.end method

.method public c(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 520
    return-void
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 182
    invoke-super {p0, p1}, Llol;->d(Landroid/os/Bundle;)V

    .line 184
    iget-boolean v0, p0, Lkuf;->W:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lkuf;->X:Z

    if-nez v0, :cond_1

    .line 185
    :cond_0
    invoke-direct {p0}, Lkuf;->V()V

    .line 189
    :goto_0
    return-void

    .line 187
    :cond_1
    invoke-virtual {p0}, Lkuf;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    goto :goto_0
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 193
    const-string v0, "squares_refresh"

    iget-boolean v1, p0, Lkuf;->W:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 194
    const-string v0, "squares_datapresent"

    iget-boolean v1, p0, Lkuf;->X:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 195
    invoke-super {p0, p1}, Llol;->e(Landroid/os/Bundle;)V

    .line 196
    return-void
.end method

.method public g(Ljava/lang/String;)V
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 463
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 464
    const-string v1, "g:"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 465
    :goto_0
    iget-object v1, p0, Lkuf;->P:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    .line 466
    invoke-virtual {p0}, Lkuf;->n()Lz;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v2, v1, v0, v3, v4}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    .line 468
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-ge v0, v2, :cond_2

    .line 469
    invoke-virtual {p0, v1}, Lkuf;->a(Landroid/content/Intent;)V

    .line 476
    :cond_0
    :goto_1
    return-void

    .line 464
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 472
    :cond_2
    invoke-virtual {p0}, Lkuf;->n()Lz;

    move-result-object v0

    const-class v2, Lhkr;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhkr;

    invoke-virtual {v0}, Lhkr;->a()Landroid/os/Bundle;

    move-result-object v0

    .line 473
    invoke-virtual {p0}, Lkuf;->n()Lz;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Lz;->startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V

    goto :goto_1
.end method

.method public h(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 480
    const/4 v0, 0x0

    const v1, 0x7f0a045b

    .line 482
    invoke-virtual {p0, v1}, Lkuf;->e_(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0a045c

    .line 483
    invoke-virtual {p0, v2}, Lkuf;->e_(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0a0597

    invoke-virtual {p0, v3}, Lkuf;->e_(I)Ljava/lang/String;

    move-result-object v3

    .line 480
    invoke-static {v0, v1, v2, v3}, Llgr;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)Llgr;

    move-result-object v0

    .line 484
    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Llgr;->a(Lu;I)V

    .line 485
    invoke-virtual {v0}, Llgr;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "square_id"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 486
    invoke-virtual {p0}, Lkuf;->p()Lae;

    move-result-object v1

    const-string v2, "dismiss_invitation"

    invoke-virtual {v0, v1, v2}, Llgr;->a(Lae;Ljava/lang/String;)V

    .line 487
    return-void
.end method

.method public z()V
    .locals 1

    .prologue
    .line 221
    invoke-super {p0}, Llol;->z()V

    .line 223
    iget-object v0, p0, Lkuf;->T:Lkuq;

    if-eqz v0, :cond_0

    .line 224
    iget-object v0, p0, Lkuf;->T:Lkuq;

    invoke-virtual {v0}, Lkuq;->c()V

    .line 226
    :cond_0
    return-void
.end method
