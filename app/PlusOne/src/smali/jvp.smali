.class final Ljvp;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Linr;


# instance fields
.field private synthetic a:Landroid/net/Uri;

.field private synthetic b:Ljava/lang/String;

.field private synthetic c:Ljvt;

.field private synthetic d:Ljvo;


# direct methods
.method constructor <init>(Ljvo;Landroid/net/Uri;Ljava/lang/String;Ljvt;)V
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Ljvp;->d:Ljvo;

    iput-object p2, p0, Ljvp;->a:Landroid/net/Uri;

    iput-object p3, p0, Ljvp;->b:Ljava/lang/String;

    iput-object p4, p0, Ljvp;->c:Ljvt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILandroid/content/Intent;)V
    .locals 4

    .prologue
    .line 74
    iget-object v0, p0, Ljvp;->d:Ljvo;

    iget-object v1, v0, Ljvo;->b:Ljava/util/Map;

    iget-object v0, p0, Ljvp;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Ljvp;->b:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    .line 76
    iget-object v0, p0, Ljvp;->d:Ljvo;

    iget-object v0, v0, Ljvo;->a:Landroid/content/Context;

    sget-object v1, Ljvs;->b:Ljvs;

    invoke-static {v0, v1}, Ljvq;->a(Landroid/content/Context;Ljvs;)Ljvq;

    move-result-object v0

    iget-object v1, p0, Ljvp;->a:Landroid/net/Uri;

    .line 77
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljvq;->a(Ljava/lang/String;)V

    .line 82
    :cond_0
    :goto_1
    iget-object v0, p0, Ljvp;->c:Ljvt;

    invoke-interface {v0, p1}, Ljvt;->onPanoramaTypeDetected(I)V

    .line 83
    return-void

    .line 74
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 78
    :cond_2
    if-eqz p1, :cond_0

    .line 79
    iget-object v0, p0, Ljvp;->d:Ljvo;

    iget-object v0, v0, Ljvo;->a:Landroid/content/Context;

    sget-object v1, Ljvs;->a:Ljvs;

    invoke-static {v0, v1}, Ljvq;->a(Landroid/content/Context;Ljvs;)Ljvq;

    move-result-object v0

    iget-object v1, p0, Ljvp;->a:Landroid/net/Uri;

    .line 80
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljvq;->a(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public m()V
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Ljvp;->c:Ljvt;

    invoke-interface {v0}, Ljvt;->onPanoramaTypeDetectionFailure()V

    .line 88
    return-void
.end method
