.class public Lgbz;
.super Lldq;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;
.implements Lhtm;
.implements Lhtx;
.implements Lhvl;
.implements Lljv;


# instance fields
.field private A:I

.field private B:I

.field private C:I

.field private D:I

.field private E:I

.field private F:Llae;

.field private G:Landroid/text/Spannable;

.field private H:Llir;

.field private I:Landroid/graphics/Bitmap;

.field private J:Landroid/text/Spanned;

.field private K:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

.field private L:Landroid/text/StaticLayout;

.field private M:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

.field private N:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

.field private O:Landroid/text/SpannableStringBuilder;

.field private P:Llir;

.field private Q:Landroid/text/Spanned;

.field private R:Llir;

.field private S:Ljava/lang/String;

.field private T:Landroid/text/SpannableStringBuilder;

.field private U:Llir;

.field private V:Landroid/text/SpannableStringBuilder;

.field private W:Llir;

.field public a:Ljava/lang/String;

.field private aA:Lgbh;

.field private aB:Lldr;

.field private aC:Lhvp;

.field private aD:Llal;

.field private aE:Lkzp;

.field private aF:Llad;

.field private aG:Llee;

.field private aH:Landroid/widget/Button;

.field private aI:Lhua;

.field private aJ:Lgap;

.field private aK:Lldb;

.field private aL:Lkzm;

.field private aM:Landroid/graphics/Point;

.field private aN:Lldl;

.field private aO:Liwk;

.field private aa:Ljava/lang/String;

.field private ab:Landroid/text/StaticLayout;

.field private ac:Ljava/lang/String;

.field private ad:Landroid/text/StaticLayout;

.field private ae:I

.field private af:I

.field private ag:Llah;

.field private ah:Llam;

.field private ai:Lgca;

.field private aj:J

.field private ak:Z

.field private al:Z

.field private am:Z

.field private an:Z

.field private ao:Z

.field private ap:Z

.field private aq:Landroid/widget/TextView;

.field private ar:Landroid/graphics/Point;

.field private as:Ljava/lang/String;

.field private at:Ljava/lang/String;

.field private au:Z

.field private av:Z

.field private aw:Z

.field private ax:Landroid/graphics/Bitmap;

.field private ay:Lhvk;

.field private az:Lhvg;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:J

.field public g:Z

.field public h:Ljava/lang/String;

.field public i:Lfdp;

.field public j:Z

.field public k:Z

.field public l:[B

.field private y:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 237
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lgbz;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 238
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 241
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lgbz;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 242
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 245
    invoke-direct {p0, p1, p2, p3}, Lldq;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 246
    new-instance v0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lgbz;->K:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    .line 247
    iget-object v0, p0, Lgbz;->K:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(I)V

    .line 248
    iget-object v0, p0, Lgbz;->K:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->b(I)V

    .line 249
    iget-object v0, p0, Lgbz;->K:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 251
    new-instance v0, Lhvk;

    invoke-direct {v0, p1, p2, p3}, Lhvk;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lgbz;->ay:Lhvk;

    .line 252
    new-instance v0, Lldr;

    invoke-direct {v0, p1, p2, p3}, Lldr;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lgbz;->aB:Lldr;

    .line 254
    new-instance v0, Lhvp;

    invoke-direct {v0, p1, p2, p3}, Lhvp;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lgbz;->aC:Lhvp;

    .line 255
    const-class v0, Lkzl;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkzl;

    .line 257
    invoke-virtual {p0}, Lgbz;->q()I

    move-result v1

    invoke-interface {v0, v1}, Lkzl;->f(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 258
    sget-object v0, Lgbz;->z:Llct;

    iget v4, v0, Llct;->k:I

    const/4 v5, 0x0

    move-object v0, p1

    move-object v1, p2

    move v2, p3

    invoke-static/range {v0 .. v5}, Lkcr;->a(Landroid/content/Context;Landroid/util/AttributeSet;IIII)Landroid/widget/Button;

    move-result-object v0

    iput-object v0, p0, Lgbz;->aH:Landroid/widget/Button;

    .line 261
    iget-object v0, p0, Lgbz;->aH:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setSingleLine(Z)V

    .line 262
    iget-object v0, p0, Lgbz;->aH:Landroid/widget/Button;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 263
    iget-object v0, p0, Lgbz;->aH:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 265
    iget-object v0, p0, Lgbz;->aH:Landroid/widget/Button;

    invoke-virtual {p0}, Lgbz;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 270
    :goto_0
    invoke-virtual {p0}, Lgbz;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 272
    new-instance v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lgbz;->M:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    .line 273
    invoke-virtual {p0}, Lgbz;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v2, Lieh;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    sget-object v2, Ldxd;->z:Lief;

    invoke-virtual {p0}, Lgbz;->q()I

    move-result v4

    invoke-interface {v0, v2, v4}, Lieh;->b(Lief;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 274
    iget-object v0, p0, Lgbz;->M:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->a(I)V

    .line 275
    iget-object v0, p0, Lgbz;->M:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    const v2, 0x7f0a0519

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->a(Ljava/lang/String;)V

    .line 280
    :goto_1
    iget-object v0, p0, Lgbz;->M:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 281
    iget-object v0, p0, Lgbz;->M:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 283
    new-instance v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lgbz;->N:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    .line 284
    iget-object v0, p0, Lgbz;->N:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->a(I)V

    .line 285
    iget-object v0, p0, Lgbz;->N:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 286
    iget-object v0, p0, Lgbz;->N:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 287
    new-instance v0, Liwk;

    invoke-virtual {p0}, Lgbz;->q()I

    move-result v1

    invoke-direct {v0, p1, v1}, Liwk;-><init>(Landroid/content/Context;I)V

    const-class v1, Lixj;

    .line 288
    invoke-virtual {v0, v1}, Liwk;->a(Ljava/lang/Class;)Liwk;

    move-result-object v0

    iput-object v0, p0, Lgbz;->aO:Liwk;

    .line 289
    return-void

    .line 267
    :cond_0
    new-instance v0, Llee;

    invoke-direct {v0, p1, p2, p3}, Llee;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lgbz;->aG:Llee;

    goto :goto_0

    .line 277
    :cond_1
    iget-object v0, p0, Lgbz;->M:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->a(I)V

    .line 278
    iget-object v0, p0, Lgbz;->M:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->a(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private D()Z
    .locals 1

    .prologue
    .line 883
    invoke-direct {p0}, Lgbz;->F()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lgbz;->E()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private E()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1096
    iget v1, p0, Lgbz;->w:I

    if-eq v1, v0, :cond_0

    iget-object v1, p0, Lgbz;->aE:Lkzp;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lgbz;->aE:Lkzp;

    .line 1097
    invoke-virtual {v1}, Lkzp;->n()Llac;

    move-result-object v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private F()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1772
    iget-boolean v0, p0, Lgbz;->k:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgbz;->aE:Lkzp;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbz;->aE:Lkzp;

    .line 1773
    invoke-virtual {v0}, Lkzp;->f()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 1807
    :goto_0
    return v0

    .line 1777
    :cond_1
    iget-object v0, p0, Lgbz;->aE:Lkzp;

    if-nez v0, :cond_2

    move v0, v2

    .line 1778
    goto :goto_0

    .line 1782
    :cond_2
    invoke-virtual {p0}, Lgbz;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v3, Lgcs;

    invoke-static {v0, v3}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgcs;

    .line 1785
    if-eqz v0, :cond_3

    .line 1786
    invoke-virtual {p0}, Lgbz;->q()I

    invoke-interface {v0}, Lgcs;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1787
    iget-object v0, p0, Lgbz;->aE:Lkzp;

    invoke-virtual {v0}, Lkzp;->i()Lkzq;

    move-result-object v0

    .line 1789
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lkzq;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lgbz;->aE:Lkzp;

    .line 1790
    invoke-virtual {v0}, Lkzp;->g()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 1791
    goto :goto_0

    .line 1795
    :cond_3
    iget-object v0, p0, Lgbz;->aE:Lkzp;

    invoke-virtual {v0}, Lkzp;->j()Lkzo;

    move-result-object v0

    if-eqz v0, :cond_4

    move v0, v1

    .line 1796
    goto :goto_0

    .line 1800
    :cond_4
    iget-object v0, p0, Lgbz;->aF:Llad;

    if-eqz v0, :cond_5

    move v0, v1

    .line 1801
    goto :goto_0

    .line 1804
    :cond_5
    iget-object v0, p0, Lgbz;->aE:Lkzp;

    invoke-virtual {v0}, Lkzp;->n()Llac;

    move-result-object v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lgbz;->aE:Lkzp;

    .line 1805
    invoke-virtual {v0}, Lkzp;->k()Llak;

    move-result-object v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lgbz;->aE:Lkzp;

    .line 1806
    invoke-virtual {v0}, Lkzp;->l()Llao;

    move-result-object v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lgbz;->aE:Lkzp;

    .line 1807
    invoke-virtual {v0}, Lkzp;->h()Llab;

    move-result-object v0

    if-eqz v0, :cond_7

    :cond_6
    move v0, v1

    goto :goto_0

    :cond_7
    move v0, v2

    goto :goto_0
.end method

.method private G()Z
    .locals 1

    .prologue
    .line 1811
    iget-object v0, p0, Lgbz;->aE:Lkzp;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lgbz;->aE:Lkzp;

    .line 1812
    invoke-virtual {v0}, Lkzp;->k()Llak;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lgbz;->am:Z

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lgbz;->aE:Lkzp;

    .line 1813
    invoke-virtual {v0}, Lkzp;->n()Llac;

    move-result-object v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)Landroid/text/Layout$Alignment;
    .locals 1

    .prologue
    .line 1368
    sget-object v0, Lgbz;->z:Llct;

    iget-object v0, v0, Llct;->a:Lfo;

    invoke-virtual {v0, p1}, Lfo;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_OPPOSITE:Landroid/text/Layout$Alignment;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    goto :goto_0
.end method


# virtual methods
.method protected a(I)I
    .locals 11

    .prologue
    .line 1031
    invoke-direct {p0}, Lgbz;->F()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1054
    :goto_0
    return p1

    .line 1036
    :cond_0
    invoke-virtual {p0}, Lgbz;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lhee;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v1

    .line 1037
    invoke-virtual {p0}, Lgbz;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v2, Lkzl;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkzl;

    .line 1038
    invoke-interface {v0, v1}, Lkzl;->e(I)Z

    move-result v10

    .line 1040
    iget-object v0, p0, Lgbz;->aC:Lhvp;

    invoke-virtual {p0, v0}, Lgbz;->removeView(Landroid/view/View;)V

    .line 1042
    iget-object v0, p0, Lgbz;->aF:Llad;

    if-eqz v0, :cond_1

    .line 1043
    invoke-virtual {p0}, Lgbz;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lita;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lita;

    .line 1044
    iget-object v1, p0, Lgbz;->aF:Llad;

    .line 1046
    invoke-virtual {p0}, Lgbz;->getContext()Landroid/content/Context;

    iget-object v2, p0, Lgbz;->aF:Llad;

    invoke-virtual {v2}, Llad;->b()Ljava/lang/String;

    .line 1045
    invoke-interface {v0}, Lita;->a()I

    move-result v0

    .line 1044
    invoke-virtual {v1, v0}, Llad;->a(I)V

    .line 1049
    :cond_1
    iget-object v0, p0, Lgbz;->aC:Lhvp;

    iget-object v1, p0, Lgbz;->i:Lfdp;

    iget-object v2, p0, Lgbz;->aE:Lkzp;

    iget-object v3, p0, Lgbz;->aF:Llad;

    iget-boolean v4, p0, Lgbz;->k:Z

    iget-object v5, p0, Lgbz;->aD:Llal;

    iget-object v7, p0, Lgbz;->c:Ljava/lang/String;

    iget-object v8, p0, Lgbz;->r:Ljava/lang/String;

    iget-object v9, p0, Lgbz;->b:Ljava/lang/String;

    move v6, p1

    invoke-virtual/range {v0 .. v10}, Lhvp;->a(Lhvr;Lhtt;Lhue;ZLhuc;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)I

    move-result p1

    .line 1052
    iget-object v0, p0, Lgbz;->aC:Lhvp;

    invoke-virtual {p0, v0}, Lgbz;->addView(Landroid/view/View;)V

    goto :goto_0
.end method

.method protected a(II)I
    .locals 12

    .prologue
    const/4 v1, 0x1

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 1161
    iget-object v0, p0, Lgbz;->ax:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    move v0, v7

    :goto_0
    sub-int v3, p2, v0

    .line 1162
    iget-object v0, p0, Lgbz;->aa:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    move v8, v1

    .line 1163
    :goto_1
    iget-object v0, p0, Lgbz;->ac:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    move v9, v1

    .line 1164
    :goto_2
    invoke-virtual {p0}, Lgbz;->getContext()Landroid/content/Context;

    move-result-object v11

    .line 1166
    if-nez v8, :cond_3

    if-nez v9, :cond_3

    .line 1199
    :goto_3
    return p1

    .line 1161
    :cond_0
    iget-object v0, p0, Lgbz;->ax:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-static {v7, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_0

    :cond_1
    move v8, v7

    .line 1162
    goto :goto_1

    :cond_2
    move v9, v7

    .line 1163
    goto :goto_2

    .line 1170
    :cond_3
    if-nez v8, :cond_4

    if-eqz v9, :cond_a

    .line 1171
    :cond_4
    sget-object v0, Lgbz;->z:Llct;

    iget v0, v0, Llct;->m:I

    add-int v10, p1, v0

    .line 1174
    :goto_4
    if-eqz v8, :cond_5

    .line 1175
    const/16 v0, 0x9

    invoke-static {v11, v0}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v2

    .line 1178
    new-instance v0, Landroid/text/StaticLayout;

    iget-object v1, p0, Lgbz;->aa:Ljava/lang/String;

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    iput-object v0, p0, Lgbz;->ab:Landroid/text/StaticLayout;

    .line 1180
    iget-object v0, p0, Lgbz;->ab:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    add-int/2addr v10, v0

    .line 1183
    :cond_5
    if-eqz v8, :cond_6

    if-eqz v9, :cond_6

    .line 1184
    sget-object v0, Lgbz;->z:Llct;

    iget v0, v0, Llct;->o:I

    add-int/2addr v10, v0

    .line 1187
    :cond_6
    if-eqz v9, :cond_9

    .line 1188
    const/16 v0, 0xa

    .line 1189
    invoke-static {v11, v0}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v2

    .line 1190
    new-instance v0, Landroid/text/StaticLayout;

    iget-object v1, p0, Lgbz;->ac:Ljava/lang/String;

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    iput-object v0, p0, Lgbz;->ad:Landroid/text/StaticLayout;

    .line 1192
    iget-object v0, p0, Lgbz;->ad:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    add-int/2addr v0, v10

    .line 1195
    :goto_5
    if-nez v8, :cond_7

    if-eqz v9, :cond_8

    .line 1196
    :cond_7
    sget-object v1, Lgbz;->z:Llct;

    iget v1, v1, Llct;->m:I

    mul-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    :cond_8
    move p1, v0

    .line 1199
    goto :goto_3

    :cond_9
    move v0, v10

    goto :goto_5

    :cond_a
    move v10, p1

    goto :goto_4
.end method

.method public a(III)I
    .locals 0

    .prologue
    .line 1532
    return p2
.end method

.method public a(IIII)I
    .locals 3

    .prologue
    .line 643
    iget-object v0, p0, Lgbz;->q:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget v1, p0, Lgbz;->t:I

    invoke-virtual {p0, v0, p2, v1}, Lgbz;->d(III)I

    move-result v0

    .line 646
    invoke-direct {p0}, Lgbz;->D()Z

    move-result v1

    if-nez v1, :cond_0

    .line 647
    sget-object v1, Lgbz;->z:Llct;

    iget v1, v1, Llct;->m:I

    add-int/2addr v0, v1

    .line 649
    :cond_0
    iput v0, p0, Lgbz;->y:I

    .line 650
    iget-object v1, p0, Lgbz;->q:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget v1, p0, Lgbz;->t:I

    invoke-virtual {p0, v0}, Lgbz;->a(I)I

    move-result v0

    .line 652
    iget-object v1, p0, Lgbz;->q:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget v1, p0, Lgbz;->t:I

    invoke-virtual {p0, v0}, Lgbz;->c(I)I

    move-result v0

    .line 655
    invoke-virtual {p0, v0, p4}, Lgbz;->a(II)I

    move-result v0

    .line 656
    iput v0, p0, Lgbz;->A:I

    .line 657
    iget-object v1, p0, Lgbz;->q:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget v2, p0, Lgbz;->t:I

    invoke-virtual {p0, v1, v0, v2}, Lgbz;->e(III)I

    move-result v0

    .line 659
    invoke-virtual {p0, p3, v0, p4}, Lgbz;->f(III)I

    move-result v0

    .line 660
    invoke-virtual {p0, p3, v0, p4}, Lgbz;->g(III)I

    move-result v0

    .line 661
    invoke-virtual {p0}, Lgbz;->aD_()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 663
    iget-object v1, p0, Lgbz;->q:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget v2, p0, Lgbz;->t:I

    invoke-virtual {p0, v1, v0, v2}, Lgbz;->c(III)I

    move-result v0

    .line 665
    iget-object v1, p0, Lgbz;->q:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget v2, p0, Lgbz;->t:I

    invoke-virtual {p0, v1, v0, v2}, Lgbz;->a(III)I

    move-result v0

    .line 671
    :goto_0
    iput v0, p0, Lgbz;->B:I

    .line 672
    iget v1, p0, Lgbz;->t:I

    invoke-virtual {p0, v0, v1}, Lgbz;->b(II)I

    move-result v0

    .line 674
    iput v0, p0, Lgbz;->C:I

    .line 675
    iget v1, p0, Lgbz;->t:I

    invoke-virtual {p0, v0, v1}, Lgbz;->c(II)I

    move-result v0

    .line 676
    iput v0, p0, Lgbz;->D:I

    .line 677
    invoke-virtual {p0, p3, v0, p4}, Lgbz;->b(III)I

    move-result v0

    .line 679
    return v0

    .line 668
    :cond_1
    invoke-virtual {p0, p3, v0, p4}, Lgbz;->c(III)I

    move-result v0

    goto :goto_0
.end method

.method public a(Landroid/graphics/Canvas;I)I
    .locals 0

    .prologue
    .line 1536
    return p2
.end method

.method protected a(Landroid/graphics/Canvas;II)I
    .locals 9

    .prologue
    .line 1373
    invoke-virtual {p0}, Lgbz;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lhss;->c(Landroid/content/Context;)I

    move-result v2

    .line 1375
    sget-object v0, Lgbz;->z:Llct;

    iget v0, v0, Llct;->m:I

    add-int v1, p2, v0

    .line 1376
    sget-object v0, Lgbz;->z:Llct;

    iget v0, v0, Llct;->m:I

    .line 1378
    invoke-direct {p0}, Lgbz;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgbz;->M:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    .line 1379
    invoke-virtual {v0}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->getMeasuredHeight()I

    move-result v0

    .line 1381
    :goto_0
    sget-object v3, Lgbz;->z:Llct;

    iget v3, v3, Llct;->m:I

    add-int/2addr v3, v2

    add-int/2addr v1, v3

    .line 1383
    iget v3, p0, Lgbz;->E:I

    add-int/2addr v3, p3

    .line 1385
    int-to-float v4, v1

    int-to-float v5, v3

    invoke-virtual {p1, v4, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1386
    iget-object v4, p0, Lgbz;->L:Landroid/text/StaticLayout;

    invoke-virtual {v4, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 1387
    neg-int v4, v1

    int-to-float v4, v4

    neg-int v5, v3

    int-to-float v5, v5

    invoke-virtual {p1, v4, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1388
    iget-object v4, p0, Lgbz;->L:Landroid/text/StaticLayout;

    iget-object v5, p0, Lgbz;->L:Landroid/text/StaticLayout;

    invoke-virtual {v5}, Landroid/text/StaticLayout;->getLineCount()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v4, v5}, Landroid/text/StaticLayout;->getLineBaseline(I)I

    move-result v4

    sget-object v5, Lgbz;->z:Llct;

    iget v5, v5, Llct;->l:I

    add-int/2addr v4, v5

    .line 1391
    iget-boolean v5, p0, Lgbz;->g:Z

    if-eqz v5, :cond_0

    .line 1393
    add-int v5, v3, v4

    .line 1394
    sget-object v6, Lgbz;->z:Llct;

    iget-object v6, v6, Llct;->z:Landroid/graphics/Bitmap;

    int-to-float v7, v1

    int-to-float v5, v5

    const/4 v8, 0x0

    invoke-virtual {p1, v6, v7, v5, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1395
    sget-object v5, Lgbz;->z:Llct;

    iget-object v5, v5, Llct;->z:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    sget-object v6, Lgbz;->z:Llct;

    iget v6, v6, Llct;->k:I

    add-int/2addr v5, v6

    add-int/2addr v1, v5

    .line 1398
    :cond_0
    int-to-float v5, v1

    add-int v6, v3, v4

    int-to-float v6, v6

    invoke-virtual {p1, v5, v6}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1399
    iget-object v5, p0, Lgbz;->P:Llir;

    invoke-virtual {v5, p1}, Llir;->draw(Landroid/graphics/Canvas;)V

    .line 1400
    neg-int v1, v1

    int-to-float v1, v1

    neg-int v5, v3

    sub-int/2addr v5, v4

    int-to-float v5, v5

    invoke-virtual {p1, v1, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1401
    iget-object v1, p0, Lgbz;->P:Llir;

    invoke-virtual {v1}, Llir;->getHeight()I

    move-result v1

    add-int/2addr v1, v4

    .line 1403
    iget v4, p0, Lgbz;->E:I

    sub-int/2addr v3, v4

    .line 1405
    iget v4, p0, Lgbz;->E:I

    add-int/2addr v0, v4

    iget v4, p0, Lgbz;->E:I

    add-int/2addr v1, v4

    .line 1406
    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 1405
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/2addr v0, v3

    .line 1407
    sget-object v1, Lgbz;->z:Llct;

    iget v1, v1, Llct;->m:I

    add-int/2addr v0, v1

    .line 1409
    return v0

    .line 1379
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Landroid/graphics/Canvas;III)I
    .locals 9

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1203
    iget-object v2, p0, Lgbz;->ab:Landroid/text/StaticLayout;

    if-eqz v2, :cond_0

    move v6, v0

    .line 1204
    :goto_0
    iget-object v2, p0, Lgbz;->ad:Landroid/text/StaticLayout;

    if-eqz v2, :cond_1

    move v8, v0

    .line 1207
    :goto_1
    if-nez v6, :cond_2

    if-nez v8, :cond_2

    .line 1251
    :goto_2
    return p3

    :cond_0
    move v6, v1

    .line 1203
    goto :goto_0

    :cond_1
    move v8, v1

    .line 1204
    goto :goto_1

    .line 1211
    :cond_2
    sget-object v0, Lgbz;->z:Llct;

    iget v0, v0, Llct;->m:I

    add-int/2addr v0, p3

    .line 1215
    iget-object v1, p0, Lgbz;->ab:Landroid/text/StaticLayout;

    if-eqz v1, :cond_3

    .line 1216
    int-to-float v1, p2

    int-to-float v2, v0

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1217
    iget-object v1, p0, Lgbz;->ab:Landroid/text/StaticLayout;

    invoke-virtual {v1, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 1218
    neg-int v1, p2

    int-to-float v1, v1

    neg-int v2, v0

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1219
    iget-object v1, p0, Lgbz;->ab:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 1222
    :cond_3
    iget-object v1, p0, Lgbz;->ax:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_4

    .line 1223
    iget-object v1, p0, Lgbz;->ax:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    sub-int v1, p4, v1

    add-int/2addr v1, p2

    .line 1224
    iget-object v2, p0, Lgbz;->ab:Landroid/text/StaticLayout;

    .line 1225
    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int v2, v0, v2

    iget-object v3, p0, Lgbz;->ax:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    .line 1224
    invoke-static {p3, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 1227
    iget-object v3, p0, Lgbz;->ax:Landroid/graphics/Bitmap;

    int-to-float v1, v1

    int-to-float v2, v2

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v1, v2, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1230
    :cond_4
    if-eqz v6, :cond_5

    if-eqz v8, :cond_5

    .line 1231
    sget-object v1, Lgbz;->z:Llct;

    iget v1, v1, Llct;->o:I

    add-int/2addr v0, v1

    .line 1234
    :cond_5
    iget-object v1, p0, Lgbz;->ad:Landroid/text/StaticLayout;

    if-eqz v1, :cond_6

    .line 1235
    int-to-float v1, p2

    int-to-float v2, v0

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1236
    iget-object v1, p0, Lgbz;->ad:Landroid/text/StaticLayout;

    invoke-virtual {v1, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 1237
    neg-int v1, p2

    int-to-float v1, v1

    neg-int v2, v0

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1238
    iget-object v1, p0, Lgbz;->ad:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 1241
    :cond_6
    if-nez v6, :cond_7

    if-eqz v8, :cond_a

    .line 1242
    :cond_7
    sget-object v1, Lgbz;->z:Llct;

    iget v1, v1, Llct;->m:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    move v7, v0

    .line 1245
    :goto_3
    int-to-float v1, p2

    int-to-float v2, v7

    add-int v0, p2, p4

    int-to-float v3, v0

    int-to-float v4, v7

    sget-object v0, Lgbz;->z:Llct;

    iget-object v5, v0, Llct;->w:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1247
    if-nez v6, :cond_8

    if-eqz v8, :cond_9

    .line 1248
    :cond_8
    sget-object v0, Lgbz;->z:Llct;

    iget v0, v0, Llct;->m:I

    mul-int/lit8 v0, v0, 0x2

    add-int/2addr v7, v0

    :cond_9
    move p3, v7

    .line 1251
    goto/16 :goto_2

    :cond_a
    move v7, v0

    goto :goto_3
.end method

.method protected a(Landroid/graphics/Canvas;IIIII)I
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v3, 0x0

    .line 819
    iget-object v0, p0, Lgbz;->ai:Lgca;

    if-eqz v0, :cond_2

    .line 822
    iget-object v0, p0, Lgbz;->aE:Lkzp;

    if-eqz v0, :cond_6

    move v4, v7

    .line 823
    :goto_0
    if-eqz v4, :cond_0

    .line 824
    iget-object v0, p0, Lgbz;->aE:Lkzp;

    invoke-virtual {v0}, Lkzp;->k()Llak;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 826
    const/16 v3, 0xca

    .line 835
    :cond_0
    :goto_1
    iget-object v0, p0, Lgbz;->ai:Lgca;

    iget-object v1, p0, Lgbz;->r:Ljava/lang/String;

    iget-object v2, p0, Lgbz;->b:Ljava/lang/String;

    if-eqz v4, :cond_9

    iget-object v4, p0, Lgbz;->aE:Lkzp;

    .line 837
    invoke-virtual {v4}, Lkzp;->m()Ljava/lang/String;

    move-result-object v4

    :goto_2
    iget-object v5, p0, Lgbz;->l:[B

    .line 836
    invoke-interface/range {v0 .. v5}, Lgca;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;[B)Z

    move-result v0

    .line 841
    if-eqz v0, :cond_1

    .line 842
    const/4 v0, -0x1

    invoke-static {p0, v0}, Lhly;->a(Landroid/view/View;I)V

    .line 844
    :cond_1
    iput-object v6, p0, Lgbz;->ai:Lgca;

    .line 847
    :cond_2
    iget-object v0, p0, Lgbz;->aq:Landroid/widget/TextView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lgbz;->aq:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-ne v0, p0, :cond_3

    .line 849
    iget-object v0, p0, Lgbz;->aq:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v0

    add-int/2addr p6, v0

    .line 852
    :cond_3
    invoke-direct {p0}, Lgbz;->D()Z

    move-result v0

    if-nez v0, :cond_4

    .line 853
    sget-object v0, Lgbz;->z:Llct;

    iget v0, v0, Llct;->m:I

    add-int/2addr p6, v0

    .line 855
    :cond_4
    invoke-virtual {p0, p6}, Lgbz;->b(I)I

    move-result v0

    .line 857
    invoke-virtual {p0, v0}, Lgbz;->d(I)I

    move-result v0

    .line 858
    invoke-virtual {p0, p1, p4, v0, p5}, Lgbz;->a(Landroid/graphics/Canvas;III)I

    move-result v0

    .line 859
    invoke-virtual {p0, p1, p2, v0}, Lgbz;->a(Landroid/graphics/Canvas;II)I

    move-result v0

    .line 860
    invoke-virtual {p0, p1, p4, v0}, Lgbz;->b(Landroid/graphics/Canvas;II)I

    move-result v0

    .line 861
    invoke-virtual {p0, p1, p4, v0, p5}, Lgbz;->b(Landroid/graphics/Canvas;III)I

    move-result v0

    .line 863
    invoke-virtual {p0}, Lgbz;->aD_()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 865
    invoke-virtual {p0, v0}, Lgbz;->e(I)I

    move-result v0

    .line 866
    invoke-virtual {p0, p1, v0}, Lgbz;->a(Landroid/graphics/Canvas;I)I

    move-result v0

    .line 872
    :goto_3
    iget-object v1, p0, Lgbz;->ay:Lhvk;

    invoke-virtual {p0, v1, v0}, Lgbz;->a(Landroid/view/View;I)I

    move-result v0

    .line 873
    iget v1, p0, Lgbz;->w:I

    if-ne v1, v7, :cond_5

    .line 874
    iget-object v1, p0, Lgbz;->az:Lhvg;

    iget-object v2, p0, Lgbz;->q:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget v2, p0, Lgbz;->t:I

    invoke-virtual {p0, v1, v0}, Lgbz;->a(Landroid/view/View;I)I

    move-result v0

    .line 877
    :cond_5
    invoke-virtual {p0, p1, p4, v0, p5}, Lgbz;->c(Landroid/graphics/Canvas;III)I

    move-result v0

    .line 879
    return v0

    :cond_6
    move v4, v3

    .line 822
    goto/16 :goto_0

    .line 827
    :cond_7
    iget-object v0, p0, Lgbz;->aE:Lkzp;

    invoke-virtual {v0}, Lkzp;->n()Llac;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 829
    const/16 v3, 0xcf

    goto/16 :goto_1

    .line 830
    :cond_8
    iget-object v0, p0, Lgbz;->aE:Lkzp;

    invoke-virtual {v0}, Lkzp;->l()Llao;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 832
    const/16 v3, 0x12c

    goto/16 :goto_1

    :cond_9
    move-object v4, v6

    .line 837
    goto/16 :goto_2

    .line 868
    :cond_a
    invoke-virtual {p0, v0}, Lgbz;->e(I)I

    move-result v0

    goto :goto_3
.end method

.method protected a(Landroid/view/View;I)I
    .locals 1

    .prologue
    .line 1591
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eq v0, p0, :cond_1

    .line 1596
    :cond_0
    :goto_0
    return p2

    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v0

    add-int/2addr p2, v0

    goto :goto_0
.end method

.method protected a(Landroid/database/Cursor;J)Landroid/text/SpannableStringBuilder;
    .locals 4

    .prologue
    .line 589
    const-wide/16 v0, 0x1

    and-long/2addr v0, p2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 590
    const/16 v0, 0xd

    .line 592
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 591
    invoke-static {v0}, Llht;->a([B)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 590
    invoke-static {v0}, Llir;->a(Landroid/text/SpannableStringBuilder;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 594
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lfdp;)Lgbz;
    .locals 0

    .prologue
    .line 691
    iput-object p1, p0, Lgbz;->i:Lfdp;

    .line 692
    return-object p0
.end method

.method public a(Lgca;)Lgbz;
    .locals 1

    .prologue
    .line 696
    iget-boolean v0, p0, Lgbz;->ao:Z

    if-nez v0, :cond_0

    .line 697
    iput-object p1, p0, Lgbz;->ai:Lgca;

    .line 699
    :cond_0
    return-object p0
.end method

.method public a()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 888
    invoke-super {p0}, Lldq;->a()V

    .line 890
    iput v2, p0, Lgbz;->y:I

    .line 891
    iput v2, p0, Lgbz;->A:I

    .line 892
    iput v2, p0, Lgbz;->B:I

    .line 893
    iput v2, p0, Lgbz;->C:I

    .line 894
    iput v2, p0, Lgbz;->D:I

    .line 896
    iput-object v1, p0, Lgbz;->r:Ljava/lang/String;

    .line 897
    iput-object v1, p0, Lgbz;->a:Ljava/lang/String;

    .line 898
    iput-object v1, p0, Lgbz;->b:Ljava/lang/String;

    .line 899
    iput-object v1, p0, Lgbz;->c:Ljava/lang/String;

    .line 900
    iput-object v1, p0, Lgbz;->d:Ljava/lang/String;

    .line 901
    iput-object v1, p0, Lgbz;->J:Landroid/text/Spanned;

    .line 902
    iput-object v1, p0, Lgbz;->e:Ljava/lang/String;

    .line 903
    iput-object v1, p0, Lgbz;->L:Landroid/text/StaticLayout;

    .line 905
    iput-object v1, p0, Lgbz;->O:Landroid/text/SpannableStringBuilder;

    .line 906
    iput-wide v4, p0, Lgbz;->f:J

    .line 907
    iput-object v1, p0, Lgbz;->at:Ljava/lang/String;

    .line 908
    iput-object v1, p0, Lgbz;->h:Ljava/lang/String;

    .line 909
    iput-object v1, p0, Lgbz;->P:Llir;

    .line 911
    iput-object v1, p0, Lgbz;->Q:Landroid/text/Spanned;

    .line 912
    iput-object v1, p0, Lgbz;->R:Llir;

    .line 913
    iput-object v1, p0, Lgbz;->T:Landroid/text/SpannableStringBuilder;

    .line 914
    iput-object v1, p0, Lgbz;->U:Llir;

    .line 915
    iput-object v1, p0, Lgbz;->V:Landroid/text/SpannableStringBuilder;

    .line 916
    iput-object v1, p0, Lgbz;->W:Llir;

    .line 917
    iput-object v1, p0, Lgbz;->ac:Ljava/lang/String;

    .line 918
    iput-object v1, p0, Lgbz;->ad:Landroid/text/StaticLayout;

    .line 919
    iput-object v1, p0, Lgbz;->aa:Ljava/lang/String;

    .line 920
    iput-object v1, p0, Lgbz;->ab:Landroid/text/StaticLayout;

    .line 921
    iput-object v1, p0, Lgbz;->ax:Landroid/graphics/Bitmap;

    .line 923
    iput-object v1, p0, Lgbz;->F:Llae;

    .line 924
    iput-object v1, p0, Lgbz;->G:Landroid/text/Spannable;

    .line 925
    iput-object v1, p0, Lgbz;->I:Landroid/graphics/Bitmap;

    .line 926
    iput-object v1, p0, Lgbz;->H:Llir;

    .line 928
    iput v2, p0, Lgbz;->ae:I

    .line 929
    iput v2, p0, Lgbz;->af:I

    .line 930
    iput-object v1, p0, Lgbz;->ag:Llah;

    .line 931
    iput-object v1, p0, Lgbz;->ah:Llam;

    .line 933
    iput-object v1, p0, Lgbz;->i:Lfdp;

    .line 934
    iput-object v1, p0, Lgbz;->ai:Lgca;

    .line 935
    iput-object v1, p0, Lgbz;->x:Landroid/view/View$OnClickListener;

    .line 937
    iput-wide v4, p0, Lgbz;->aj:J

    .line 939
    iput-boolean v2, p0, Lgbz;->ak:Z

    .line 940
    iput-boolean v2, p0, Lgbz;->al:Z

    .line 941
    iput-boolean v2, p0, Lgbz;->am:Z

    .line 942
    iput-boolean v2, p0, Lgbz;->an:Z

    .line 943
    iput-boolean v2, p0, Lgbz;->j:Z

    .line 944
    iput-boolean v2, p0, Lgbz;->ao:Z

    .line 946
    iput-boolean v2, p0, Lgbz;->k:Z

    .line 947
    iput-object v1, p0, Lgbz;->as:Ljava/lang/String;

    .line 948
    iput-boolean v2, p0, Lgbz;->au:Z

    .line 949
    iput-boolean v2, p0, Lgbz;->aw:Z

    .line 950
    iput-boolean v2, p0, Lgbz;->g:Z

    .line 952
    iget-object v0, p0, Lgbz;->aC:Lhvp;

    if-eqz v0, :cond_0

    .line 953
    iget-object v0, p0, Lgbz;->aC:Lhvp;

    invoke-virtual {v0}, Lhvp;->a()V

    .line 955
    :cond_0
    iget-object v0, p0, Lgbz;->aN:Lldl;

    if-eqz v0, :cond_1

    .line 956
    iget-object v0, p0, Lgbz;->aN:Lldl;

    invoke-virtual {v0}, Lldl;->a()V

    .line 958
    :cond_1
    iput-object v1, p0, Lgbz;->aD:Llal;

    .line 959
    iput-object v1, p0, Lgbz;->aE:Lkzp;

    .line 961
    iget-object v0, p0, Lgbz;->aH:Landroid/widget/Button;

    if-eqz v0, :cond_2

    .line 962
    iget-object v0, p0, Lgbz;->aH:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 964
    :cond_2
    iget-object v0, p0, Lgbz;->aG:Llee;

    if-eqz v0, :cond_3

    .line 965
    iget-object v0, p0, Lgbz;->aG:Llee;

    invoke-virtual {v0}, Llee;->a()V

    .line 967
    :cond_3
    iput-object v1, p0, Lgbz;->aI:Lhua;

    .line 969
    iget-object v0, p0, Lgbz;->aK:Lldb;

    if-eqz v0, :cond_4

    .line 970
    iget-object v0, p0, Lgbz;->aK:Lldb;

    invoke-virtual {v0}, Lldb;->a()V

    .line 972
    :cond_4
    iput-object v1, p0, Lgbz;->aK:Lldb;

    .line 973
    iput-object v1, p0, Lgbz;->aM:Landroid/graphics/Point;

    .line 974
    iput-object v1, p0, Lgbz;->aL:Lkzm;

    .line 976
    iget-object v0, p0, Lgbz;->aB:Lldr;

    if-eqz v0, :cond_5

    .line 977
    iget-object v0, p0, Lgbz;->aB:Lldr;

    invoke-virtual {v0}, Lldr;->a()V

    .line 980
    :cond_5
    iget-object v0, p0, Lgbz;->aJ:Lgap;

    if-eqz v0, :cond_6

    .line 981
    iget-object v0, p0, Lgbz;->aJ:Lgap;

    invoke-virtual {v0}, Lgap;->a()V

    .line 984
    :cond_6
    iget-object v0, p0, Lgbz;->ay:Lhvk;

    invoke-virtual {v0}, Lhvk;->a()V

    .line 986
    iget-object v0, p0, Lgbz;->az:Lhvg;

    if-eqz v0, :cond_7

    .line 987
    iget-object v0, p0, Lgbz;->az:Lhvg;

    invoke-virtual {v0}, Lhvg;->a()V

    .line 989
    :cond_7
    iput-object v1, p0, Lgbz;->aA:Lgbh;

    .line 991
    iget-object v0, p0, Lgbz;->aq:Landroid/widget/TextView;

    if-eqz v0, :cond_8

    .line 992
    iget-object v0, p0, Lgbz;->ar:Landroid/graphics/Point;

    invoke-virtual {v0, v2, v2}, Landroid/graphics/Point;->set(II)V

    .line 993
    iget-object v0, p0, Lgbz;->aq:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 996
    :cond_8
    iput-object v1, p0, Lgbz;->aF:Llad;

    .line 997
    return-void
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 579
    return-void
.end method

.method protected a(Landroid/database/Cursor;Llcr;I)V
    .locals 10

    .prologue
    .line 327
    invoke-virtual {p0, p0}, Lgbz;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 328
    invoke-virtual {p0, p1}, Lgbz;->a(Landroid/database/Cursor;)V

    .line 329
    invoke-virtual {p0}, Lgbz;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 330
    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbz;->r:Ljava/lang/String;

    .line 331
    const/16 v0, 0x13

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbz;->a:Ljava/lang/String;

    .line 332
    const/16 v0, 0x1a

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbz;->b:Ljava/lang/String;

    .line 333
    const/4 v0, 0x3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbz;->c:Ljava/lang/String;

    .line 334
    const/4 v0, 0x4

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbz;->d:Ljava/lang/String;

    .line 335
    iget-object v0, p0, Lgbz;->d:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 336
    const-string v0, ""

    iput-object v0, p0, Lgbz;->d:Ljava/lang/String;

    .line 338
    :cond_0
    const/16 v0, 0x1e

    .line 339
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 338
    invoke-static {v0}, Llht;->a([B)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    iput-object v0, p0, Lgbz;->J:Landroid/text/Spanned;

    .line 340
    const/4 v0, 0x5

    .line 341
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 340
    invoke-static {v0}, Lhst;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbz;->e:Ljava/lang/String;

    .line 343
    const/16 v0, 0x9

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lgbz;->f:J

    .line 345
    const/16 v0, 0x1c

    .line 346
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_f

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lgbz;->g:Z

    .line 348
    const/16 v0, 0xa

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 349
    const-wide/16 v0, 0x2

    and-long/2addr v0, v4

    const-wide/16 v6, 0x0

    cmp-long v0, v0, v6

    if-eqz v0, :cond_1

    .line 350
    const/16 v0, 0xc

    .line 352
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 351
    invoke-static {v0}, Llht;->a([B)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 350
    invoke-static {v0}, Llir;->a(Landroid/text/SpannableStringBuilder;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    iput-object v0, p0, Lgbz;->Q:Landroid/text/Spanned;

    .line 355
    :cond_1
    const-wide/16 v0, 0x8

    and-long/2addr v0, v4

    const-wide/16 v6, 0x0

    cmp-long v0, v0, v6

    if-eqz v0, :cond_2

    .line 356
    const/16 v0, 0x8

    .line 357
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 358
    if-eqz v0, :cond_2

    .line 359
    invoke-static {v0}, Llae;->a([B)Llae;

    move-result-object v0

    iput-object v0, p0, Lgbz;->F:Llae;

    .line 362
    :cond_2
    invoke-virtual {p0}, Lgbz;->f()Ljava/lang/String;

    move-result-object v0

    .line 363
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 364
    new-instance v1, Llju;

    const-string v3, "ucvg-location"

    const/4 v6, 0x0

    invoke-direct {v1, v3, v6}, Llju;-><init>(Ljava/lang/String;Z)V

    .line 366
    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-direct {v3, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    iput-object v3, p0, Lgbz;->G:Landroid/text/Spannable;

    .line 367
    iget-object v3, p0, Lgbz;->G:Landroid/text/Spannable;

    const/4 v6, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v7, 0x21

    invoke-interface {v3, v1, v6, v0, v7}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 371
    :cond_3
    const/16 v0, 0x15

    .line 372
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 373
    if-eqz v0, :cond_10

    .line 375
    invoke-static {v0}, Llal;->a([B)Llal;

    move-result-object v0

    .line 376
    invoke-virtual {v0}, Llal;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbz;->at:Ljava/lang/String;

    .line 381
    :goto_1
    const/16 v0, 0xe

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbz;->h:Ljava/lang/String;

    .line 382
    const/16 v0, 0xf

    .line 383
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 384
    iget-object v0, p0, Lgbz;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_13

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_13

    .line 385
    const/16 v0, 0x21

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbz;->S:Ljava/lang/String;

    .line 386
    iget-object v0, p0, Lgbz;->at:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 387
    new-instance v0, Landroid/text/SpannableStringBuilder;

    const v3, 0x7f0a0723

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    sget-object v8, Lgbz;->z:Llct;

    iget-object v8, v8, Llct;->a:Lfo;

    .line 389
    invoke-virtual {v8, v1}, Lfo;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    .line 388
    invoke-virtual {v2, v3, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    iput-object v0, p0, Lgbz;->T:Landroid/text/SpannableStringBuilder;

    .line 396
    :goto_2
    iget-object v0, p0, Lgbz;->T:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 397
    invoke-virtual {v3, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    .line 398
    iget-object v0, p0, Lgbz;->S:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_12

    const/4 v0, 0x1

    .line 399
    :goto_3
    invoke-virtual {v3, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    if-ne v6, v3, :cond_4

    .line 400
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v1, v6

    .line 401
    iget-object v3, p0, Lgbz;->T:Landroid/text/SpannableStringBuilder;

    sget-object v7, Lgbz;->z:Llct;

    iget-object v7, v7, Llct;->af:Landroid/text/style/StyleSpan;

    const/16 v8, 0x21

    invoke-virtual {v3, v7, v6, v1, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 403
    if-nez v0, :cond_4

    iget v3, p0, Lgbz;->w:I

    const/4 v7, 0x1

    if-ne v3, v7, :cond_4

    .line 404
    iget-object v3, p0, Lgbz;->T:Landroid/text/SpannableStringBuilder;

    new-instance v7, Llju;

    const-string v8, "ucvg-attribution"

    const/4 v9, 0x0

    invoke-direct {v7, v8, v9}, Llju;-><init>(Ljava/lang/String;Z)V

    const/16 v8, 0x21

    invoke-virtual {v3, v7, v6, v1, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 409
    :cond_4
    if-eqz v0, :cond_5

    iget v0, p0, Lgbz;->w:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_5

    .line 410
    iget-object v0, p0, Lgbz;->T:Landroid/text/SpannableStringBuilder;

    new-instance v1, Llju;

    const-string v3, "ucvg-originalactivity"

    const/4 v6, 0x0

    invoke-direct {v1, v3, v6}, Llju;-><init>(Ljava/lang/String;Z)V

    const/4 v3, 0x0

    iget-object v6, p0, Lgbz;->T:Landroid/text/SpannableStringBuilder;

    .line 411
    invoke-virtual {v6}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v6

    const/16 v7, 0x21

    .line 410
    invoke-virtual {v0, v1, v3, v6, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 418
    :cond_5
    :goto_4
    invoke-virtual {p0, p1, v4, v5}, Lgbz;->a(Landroid/database/Cursor;J)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    iput-object v0, p0, Lgbz;->V:Landroid/text/SpannableStringBuilder;

    .line 420
    const/16 v0, 0x20

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lgbz;->ae:I

    .line 421
    const/4 v0, 0x7

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lgbz;->af:I

    .line 423
    const/16 v0, 0x11

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 425
    if-eqz v0, :cond_14

    .line 426
    invoke-static {v0}, Llam;->a([B)Llam;

    move-result-object v0

    iput-object v0, p0, Lgbz;->ah:Llam;

    .line 431
    :goto_5
    const/4 v0, 0x6

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 433
    if-eqz v0, :cond_15

    .line 434
    invoke-static {v0}, Llah;->a([B)Llah;

    move-result-object v0

    iput-object v0, p0, Lgbz;->ag:Llah;

    .line 439
    :goto_6
    const/16 v0, 0xb

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lgbz;->aj:J

    .line 440
    iget-wide v0, p0, Lgbz;->aj:J

    const-wide/16 v6, 0x8

    and-long/2addr v0, v6

    const-wide/16 v6, 0x0

    cmp-long v0, v0, v6

    if-eqz v0, :cond_16

    const/4 v0, 0x1

    :goto_7
    iput-boolean v0, p0, Lgbz;->ak:Z

    .line 441
    iget-wide v0, p0, Lgbz;->aj:J

    const-wide/16 v6, 0x10

    and-long/2addr v0, v6

    const-wide/16 v6, 0x0

    cmp-long v0, v0, v6

    if-eqz v0, :cond_17

    const/4 v0, 0x1

    :goto_8
    iput-boolean v0, p0, Lgbz;->al:Z

    .line 442
    iget-wide v0, p0, Lgbz;->aj:J

    const-wide/16 v6, 0x20

    and-long/2addr v0, v6

    const-wide/16 v6, 0x0

    cmp-long v0, v0, v6

    if-eqz v0, :cond_18

    const/4 v0, 0x1

    :goto_9
    iput-boolean v0, p0, Lgbz;->am:Z

    .line 443
    iget-wide v0, p0, Lgbz;->aj:J

    const-wide/16 v6, 0x4

    and-long/2addr v0, v6

    const-wide/16 v6, 0x0

    cmp-long v0, v0, v6

    if-eqz v0, :cond_19

    const/4 v0, 0x1

    :goto_a
    iput-boolean v0, p0, Lgbz;->an:Z

    .line 444
    iget-wide v0, p0, Lgbz;->aj:J

    const-wide/16 v6, 0x1

    and-long/2addr v0, v6

    const-wide/16 v6, 0x0

    cmp-long v0, v0, v6

    if-nez v0, :cond_1a

    iget-wide v0, p0, Lgbz;->aj:J

    const-wide/16 v6, 0x400

    and-long/2addr v0, v6

    const-wide/16 v6, 0x0

    cmp-long v0, v0, v6

    if-nez v0, :cond_1a

    const/4 v0, 0x1

    :goto_b
    iput-boolean v0, p0, Lgbz;->j:Z

    .line 446
    iget-wide v0, p0, Lgbz;->aj:J

    const-wide/16 v6, 0x80

    and-long/2addr v0, v6

    const-wide/16 v6, 0x0

    cmp-long v0, v0, v6

    if-eqz v0, :cond_1b

    const/4 v0, 0x1

    :goto_c
    iput-boolean v0, p0, Lgbz;->ao:Z

    .line 447
    iget-wide v0, p0, Lgbz;->aj:J

    const-wide/16 v6, 0x800

    and-long/2addr v0, v6

    const-wide/16 v6, 0x0

    cmp-long v0, v0, v6

    if-eqz v0, :cond_1c

    const/4 v0, 0x1

    :goto_d
    iput-boolean v0, p0, Lgbz;->ap:Z

    .line 449
    const-wide/16 v0, 0x4000

    and-long/2addr v0, v4

    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1d

    const/4 v0, 0x1

    :goto_e
    iput-boolean v0, p0, Lgbz;->k:Z

    .line 451
    iget-boolean v0, p0, Lgbz;->aw:Z

    if-eqz v0, :cond_1e

    iget-boolean v0, p0, Lgbz;->k:Z

    if-eqz v0, :cond_1e

    iget-wide v0, p0, Lgbz;->aj:J

    const-wide/16 v4, 0x2

    and-long/2addr v0, v4

    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1e

    const/4 v0, 0x1

    :goto_f
    iput-boolean v0, p0, Lgbz;->au:Z

    .line 454
    const/16 v0, 0x14

    .line 455
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    .line 457
    const/4 v0, 0x0

    .line 458
    if-eqz v1, :cond_6

    .line 459
    invoke-static {v1}, Llal;->a([B)Llal;

    move-result-object v1

    iput-object v1, p0, Lgbz;->aD:Llal;

    .line 460
    iget-object v1, p0, Lgbz;->aD:Llal;

    invoke-virtual {v1}, Llal;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lgbz;->as:Ljava/lang/String;

    .line 461
    iget-object v1, p0, Lgbz;->aD:Llal;

    invoke-virtual {v1}, Llal;->f()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 473
    :cond_6
    :goto_10
    const/16 v1, 0x22

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    .line 475
    if-eqz v1, :cond_8

    .line 476
    invoke-static {v1}, Lkzp;->a([B)Lkzp;

    move-result-object v1

    iput-object v1, p0, Lgbz;->aE:Lkzp;

    .line 477
    iget-object v1, p0, Lgbz;->aE:Lkzp;

    if-eqz v1, :cond_8

    .line 478
    iget-object v1, p0, Lgbz;->aE:Lkzp;

    invoke-virtual {v1}, Lkzp;->l()Llao;

    move-result-object v1

    .line 479
    if-eqz v1, :cond_7

    .line 480
    invoke-virtual {v1}, Llao;->b()Ljava/lang/String;

    move-result-object v1

    .line 482
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 483
    const v3, 0x7f0a0529

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lgbz;->aa:Ljava/lang/String;

    .line 484
    iput-object v1, p0, Lgbz;->ac:Ljava/lang/String;

    .line 485
    sget-object v1, Lgbz;->z:Llct;

    iget-object v1, v1, Llct;->h:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lgbz;->ax:Landroid/graphics/Bitmap;

    .line 489
    :cond_7
    iget-object v1, p0, Lgbz;->aE:Lkzp;

    if-eqz v1, :cond_1f

    iget-object v1, p0, Lgbz;->aE:Lkzp;

    .line 490
    invoke-virtual {v1}, Lkzp;->n()Llac;

    move-result-object v1

    if-eqz v1, :cond_1f

    const/4 v1, 0x1

    .line 491
    :goto_11
    if-eqz v1, :cond_20

    iget-object v1, p0, Lgbz;->aE:Lkzp;

    .line 492
    invoke-virtual {v1}, Lkzp;->n()Llac;

    move-result-object v1

    invoke-virtual {v1}, Llac;->a()Z

    move-result v1

    if-eqz v1, :cond_20

    const/4 v1, 0x1

    .line 493
    :goto_12
    if-eqz v1, :cond_8

    .line 494
    invoke-virtual {p0}, Lgbz;->i()V

    .line 500
    :cond_8
    const/16 v1, 0x26

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 501
    const/16 v3, 0x27

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 502
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_9

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_9

    .line 503
    new-instance v4, Llad;

    invoke-direct {v4, v3, v1}, Llad;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v4, p0, Lgbz;->aF:Llad;

    .line 507
    :cond_9
    iget-boolean v1, p0, Lgbz;->ao:Z

    if-eqz v1, :cond_a

    .line 508
    const/4 v1, 0x0

    iput-object v1, p0, Lgbz;->ai:Lgca;

    .line 511
    :cond_a
    invoke-virtual {p0}, Lgbz;->aD_()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 512
    invoke-virtual {p0, p1, p2, p3}, Lgbz;->a_(Landroid/database/Cursor;Llcr;I)V

    .line 515
    :cond_b
    const/16 v1, 0x16

    invoke-interface {p1, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_21

    .line 516
    const/16 v1, 0x16

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    invoke-static {v1}, Llai;->a([B)Llai;

    move-result-object v1

    iput-object v1, p0, Lgbz;->aI:Lhua;

    .line 522
    :goto_13
    const/16 v1, 0x1d

    .line 523
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    .line 522
    invoke-static {v1}, Lkzm;->a([B)Lkzm;

    move-result-object v1

    iput-object v1, p0, Lgbz;->aL:Lkzm;

    .line 525
    invoke-static {p0}, Llhn;->a(Landroid/view/View;)V

    .line 528
    invoke-virtual {p0}, Lgbz;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-wide v4, p0, Lgbz;->f:J

    invoke-static {v1, v4, v5}, Llhu;->a(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v1

    .line 529
    if-nez v0, :cond_23

    .line 530
    const/16 v0, 0x1f

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 531
    if-nez v0, :cond_c

    .line 532
    const-string v0, ""

    .line 535
    :cond_c
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_22

    .line 537
    invoke-static {}, Llsu;->a()Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 538
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 536
    invoke-static {v1}, Llsu;->a(Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v1

    .line 544
    :goto_14
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    iput-object v2, p0, Lgbz;->O:Landroid/text/SpannableStringBuilder;

    .line 545
    iget v1, p0, Lgbz;->w:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_d

    .line 546
    new-instance v1, Llju;

    const-string v2, "ucvg-acl"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Llju;-><init>(Ljava/lang/String;Z)V

    .line 548
    iget-object v2, p0, Lgbz;->O:Landroid/text/SpannableStringBuilder;

    const/4 v3, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v4, 0x21

    invoke-virtual {v2, v1, v3, v0, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 563
    :cond_d
    :goto_15
    const/16 v0, 0x23

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    iput-object v0, p0, Lgbz;->l:[B

    .line 565
    invoke-static {}, Llsj;->a()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 566
    iget-object v1, p0, Lgbz;->K:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    const-string v2, "update_avatar_"

    iget-object v0, p0, Lgbz;->r:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_24

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_16
    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setTransitionName(Ljava/lang/String;)V

    .line 568
    :cond_e
    return-void

    .line 346
    :cond_f
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 378
    :cond_10
    const/4 v0, 0x0

    iput-object v0, p0, Lgbz;->at:Ljava/lang/String;

    goto/16 :goto_1

    .line 391
    :cond_11
    new-instance v0, Landroid/text/SpannableStringBuilder;

    const v3, 0x7f0a0724

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    sget-object v8, Lgbz;->z:Llct;

    iget-object v8, v8, Llct;->a:Lfo;

    .line 393
    invoke-virtual {v8, v1}, Lfo;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    sget-object v8, Lgbz;->z:Llct;

    iget-object v8, v8, Llct;->a:Lfo;

    iget-object v9, p0, Lgbz;->at:Ljava/lang/String;

    .line 394
    invoke-virtual {v8, v9}, Lfo;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    .line 391
    invoke-virtual {v2, v3, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    iput-object v0, p0, Lgbz;->T:Landroid/text/SpannableStringBuilder;

    goto/16 :goto_2

    .line 398
    :cond_12
    const/4 v0, 0x0

    goto/16 :goto_3

    .line 415
    :cond_13
    const/4 v0, 0x0

    iput-object v0, p0, Lgbz;->T:Landroid/text/SpannableStringBuilder;

    goto/16 :goto_4

    .line 428
    :cond_14
    const/4 v0, 0x0

    iput-object v0, p0, Lgbz;->ah:Llam;

    goto/16 :goto_5

    .line 436
    :cond_15
    const/4 v0, 0x0

    iput-object v0, p0, Lgbz;->ag:Llah;

    goto/16 :goto_6

    .line 440
    :cond_16
    const/4 v0, 0x0

    goto/16 :goto_7

    .line 441
    :cond_17
    const/4 v0, 0x0

    goto/16 :goto_8

    .line 442
    :cond_18
    const/4 v0, 0x0

    goto/16 :goto_9

    .line 443
    :cond_19
    const/4 v0, 0x0

    goto/16 :goto_a

    .line 444
    :cond_1a
    const/4 v0, 0x0

    goto/16 :goto_b

    .line 446
    :cond_1b
    const/4 v0, 0x0

    goto/16 :goto_c

    .line 447
    :cond_1c
    const/4 v0, 0x0

    goto/16 :goto_d

    .line 449
    :cond_1d
    const/4 v0, 0x0

    goto/16 :goto_e

    .line 451
    :cond_1e
    const/4 v0, 0x0

    goto/16 :goto_f

    .line 463
    :pswitch_0
    const v0, 0x7f0a0478

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_10

    .line 467
    :pswitch_1
    const v0, 0x7f0a0479

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_10

    .line 490
    :cond_1f
    const/4 v1, 0x0

    goto/16 :goto_11

    .line 492
    :cond_20
    const/4 v1, 0x0

    goto/16 :goto_12

    .line 519
    :cond_21
    const/4 v1, 0x0

    iput-object v1, p0, Lgbz;->aI:Lhua;

    goto/16 :goto_13

    .line 540
    :cond_22
    const-string v2, "UpdateCardViewGroup"

    const-string v3, "empty acl display"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 542
    invoke-static {}, Llsu;->a()Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 541
    invoke-static {v1}, Llsu;->a(Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_14

    .line 554
    :cond_23
    invoke-static {}, Llsu;->a()Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 555
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 553
    invoke-static {v1}, Llsu;->a(Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v1

    .line 556
    new-instance v3, Landroid/text/style/ForegroundColorSpan;

    const v4, 0x7f0b0103

    .line 557
    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v3, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    .line 558
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    iput-object v2, p0, Lgbz;->O:Landroid/text/SpannableStringBuilder;

    .line 559
    iget-object v1, p0, Lgbz;->O:Landroid/text/SpannableStringBuilder;

    const/4 v2, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v4, 0x21

    invoke-virtual {v1, v3, v2, v0, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_15

    .line 566
    :cond_24
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_16

    .line 461
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 1871
    iget-object v0, p0, Lgbz;->aO:Liwk;

    invoke-virtual {v0}, Liwk;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1872
    invoke-virtual {p0}, Lgbz;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lgbz;->aO:Liwk;

    invoke-virtual {v1}, Liwk;->b()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1878
    :goto_0
    return-void

    .line 1875
    :cond_0
    invoke-virtual {p0}, Lgbz;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lhtp;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhtp;

    .line 1876
    iget-object v1, p0, Lgbz;->r:Ljava/lang/String;

    iget-object v2, p0, Lgbz;->c:Ljava/lang/String;

    iget-boolean v3, p0, Lgbz;->j:Z

    iget-boolean v4, p0, Lgbz;->g:Z

    iget-object v5, p0, Lgbz;->l:[B

    move-object v6, p1

    invoke-interface/range {v0 .. v6}, Lhtp;->a(Ljava/lang/String;Ljava/lang/String;ZZ[BLandroid/os/Bundle;)V

    goto :goto_0
.end method

.method public a(Landroid/text/Spanned;)V
    .locals 1

    .prologue
    .line 683
    invoke-static {p1}, Llir;->a(Landroid/text/Spanned;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    iput-object v0, p0, Lgbz;->V:Landroid/text/SpannableStringBuilder;

    .line 684
    return-void
.end method

.method public a(Landroid/text/style/URLSpan;)V
    .locals 6

    .prologue
    .line 1891
    invoke-virtual {p1}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v4

    .line 1892
    iget v0, p0, Lgbz;->w:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lgbz;->i:Lfdp;

    if-eqz v0, :cond_1

    const-string v0, "ucvg-acl"

    .line 1893
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1894
    iget-object v0, p0, Lgbz;->i:Lfdp;

    invoke-virtual {v0}, Lfdp;->a()V

    .line 1908
    :cond_0
    :goto_0
    return-void

    .line 1895
    :cond_1
    iget-object v0, p0, Lgbz;->i:Lfdp;

    if-eqz v0, :cond_2

    const-string v0, "ucvg-attribution"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1896
    iget-object v0, p0, Lgbz;->i:Lfdp;

    iget-object v1, p0, Lgbz;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lfdp;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 1897
    :cond_2
    iget-object v0, p0, Lgbz;->i:Lfdp;

    if-eqz v0, :cond_3

    const-string v0, "ucvg-originalactivity"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1898
    iget-object v0, p0, Lgbz;->i:Lfdp;

    iget-object v1, p0, Lgbz;->S:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lfdp;->f(Ljava/lang/String;)V

    goto :goto_0

    .line 1899
    :cond_3
    iget-object v0, p0, Lgbz;->i:Lfdp;

    if-eqz v0, :cond_0

    .line 1900
    const-string v0, "ucvg-location"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1901
    invoke-virtual {p0}, Lgbz;->g()V

    goto :goto_0

    .line 1903
    :cond_4
    invoke-virtual {p0}, Lgbz;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lkzh;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkzh;

    .line 1904
    iget-object v1, p0, Lgbz;->r:Ljava/lang/String;

    iget-object v2, p0, Lgbz;->b:Ljava/lang/String;

    iget-object v3, p0, Lgbz;->c:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-interface/range {v0 .. v5}, Lkzh;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lkzs;)V

    goto :goto_0
.end method

.method public a(Landroid/widget/Button;)V
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 1838
    iget-object v0, p0, Lgbz;->aO:Liwk;

    invoke-virtual {v0}, Liwk;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1839
    invoke-virtual {p0}, Lgbz;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lgbz;->aO:Liwk;

    invoke-virtual {v1}, Liwk;->b()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1859
    :cond_0
    :goto_0
    return-void

    .line 1842
    :cond_1
    invoke-virtual {p0}, Lgbz;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lhtl;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhtl;

    .line 1843
    iget-object v1, p0, Lgbz;->r:Ljava/lang/String;

    iget-object v2, p0, Lgbz;->c:Ljava/lang/String;

    iget-object v3, p0, Lgbz;->d:Ljava/lang/String;

    iget-boolean v4, p0, Lgbz;->j:Z

    if-nez v4, :cond_3

    move v4, v5

    :goto_1
    iget-object v7, p0, Lgbz;->ag:Llah;

    if-eqz v7, :cond_4

    iget-object v7, p0, Lgbz;->ag:Llah;

    .line 1844
    invoke-virtual {v7}, Llah;->c()Z

    move-result v7

    if-eqz v7, :cond_4

    :goto_2
    iget-object v7, p0, Lgbz;->l:[B

    move-object v6, p0

    .line 1843
    invoke-interface/range {v0 .. v7}, Lhtl;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLhtm;[B)V

    .line 1847
    iget-object v0, p0, Lgbz;->ay:Lhvk;

    invoke-virtual {v0}, Lhvk;->h()V

    .line 1848
    invoke-static {}, Llsj;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbz;->ag:Llah;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lgbz;->ag:Llah;

    .line 1849
    invoke-virtual {v0}, Llah;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1851
    :cond_2
    iget-object v0, p0, Lgbz;->aJ:Lgap;

    if-eqz v0, :cond_5

    .line 1852
    iget-object v0, p0, Lgbz;->aJ:Lgap;

    invoke-virtual {v0}, Lgap;->a()V

    .line 1856
    :goto_3
    iget-object v0, p0, Lgbz;->aJ:Lgap;

    invoke-virtual {v0, p1, p0}, Lgap;->a(Landroid/widget/Button;Lgbz;)V

    .line 1857
    iget-object v0, p0, Lgbz;->aJ:Lgap;

    invoke-virtual {v0}, Lgap;->b()V

    goto :goto_0

    :cond_3
    move v4, v6

    .line 1843
    goto :goto_1

    :cond_4
    move v5, v6

    .line 1844
    goto :goto_2

    .line 1854
    :cond_5
    new-instance v0, Lgaq;

    invoke-direct {v0}, Lgaq;-><init>()V

    iput-object v0, p0, Lgbz;->aJ:Lgap;

    goto :goto_3
.end method

.method protected a(Ljava/lang/StringBuilder;)V
    .locals 0

    .prologue
    .line 637
    return-void
.end method

.method public a(Lkzp;)V
    .locals 0

    .prologue
    .line 1927
    iput-object p1, p0, Lgbz;->aE:Lkzp;

    .line 1928
    return-void
.end method

.method protected a(Z)V
    .locals 1

    .prologue
    .line 1918
    iget-object v0, p0, Lgbz;->r:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1919
    iget-object v0, p0, Lgbz;->aG:Llee;

    if-eqz v0, :cond_0

    .line 1920
    iget-object v0, p0, Lgbz;->aG:Llee;

    invoke-virtual {v0, p1}, Llee;->a(Z)V

    .line 1922
    :cond_0
    iget-object v0, p0, Lgbz;->aB:Lldr;

    invoke-virtual {v0, p1}, Lldr;->a(Z)V

    .line 1924
    :cond_1
    return-void
.end method

.method protected a(ZIIII)V
    .locals 12

    .prologue
    .line 704
    iget-object v0, p0, Lgbz;->q:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    .line 705
    iget v1, p0, Lgbz;->t:I

    add-int/2addr v1, v0

    .line 706
    iget v2, p0, Lgbz;->t:I

    const/high16 v3, 0x40000000    # 2.0f

    .line 707
    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 708
    iget-object v3, p0, Lgbz;->q:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    sget-object v4, Lgbz;->z:Llct;

    iget v4, v4, Llct;->m:I

    add-int/2addr v3, v4

    .line 709
    iget v4, p0, Lgbz;->s:I

    add-int/2addr v4, v3

    .line 710
    iget v5, p0, Lgbz;->s:I

    const/high16 v6, 0x40000000    # 2.0f

    .line 711
    invoke-static {v5, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 712
    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v6, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    .line 714
    iget-object v7, p0, Lgbz;->aB:Lldr;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lgbz;->ah:Llam;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lgbz;->aB:Lldr;

    invoke-virtual {v7}, Lldr;->getParent()Landroid/view/ViewParent;

    move-result-object v7

    if-ne v7, p0, :cond_0

    iget-object v7, p0, Lgbz;->ah:Llam;

    .line 715
    invoke-virtual {v7}, Llam;->a()I

    move-result v7

    if-lez v7, :cond_0

    .line 716
    iget-object v7, p0, Lgbz;->aB:Lldr;

    invoke-virtual {v7, v5, v6}, Lldr;->measure(II)V

    .line 717
    iget-object v7, p0, Lgbz;->aB:Lldr;

    iget v8, p0, Lgbz;->D:I

    iget v9, p0, Lgbz;->D:I

    iget-object v10, p0, Lgbz;->aB:Lldr;

    .line 718
    invoke-virtual {v10}, Lldr;->getMeasuredHeight()I

    move-result v10

    add-int/2addr v9, v10

    .line 717
    invoke-virtual {v7, v3, v8, v4, v9}, Lldr;->layout(IIII)V

    .line 721
    :cond_0
    iget-object v7, p0, Lgbz;->K:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    if-eqz v7, :cond_1

    .line 722
    iget-object v7, p0, Lgbz;->K:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    .line 723
    invoke-virtual {p0}, Lgbz;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lhss;->c(Landroid/content/Context;)I

    move-result v8

    const/high16 v9, 0x40000000    # 2.0f

    .line 722
    invoke-static {v8, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    invoke-virtual {v7, v8, v6}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->measure(II)V

    .line 724
    iget-object v7, p0, Lgbz;->K:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    iget v8, p0, Lgbz;->A:I

    iget-object v9, p0, Lgbz;->K:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    .line 725
    invoke-virtual {v9}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->getMeasuredWidth()I

    move-result v9

    add-int/2addr v9, v3

    iget v10, p0, Lgbz;->A:I

    iget-object v11, p0, Lgbz;->K:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    .line 726
    invoke-virtual {v11}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->getMeasuredHeight()I

    move-result v11

    add-int/2addr v10, v11

    .line 724
    invoke-virtual {v7, v3, v8, v9, v10}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->layout(IIII)V

    .line 729
    :cond_1
    iget-object v7, p0, Lgbz;->ay:Lhvk;

    invoke-virtual {v7}, Lhvk;->getParent()Landroid/view/ViewParent;

    move-result-object v7

    if-ne v7, p0, :cond_2

    .line 730
    iget-object v7, p0, Lgbz;->ay:Lhvk;

    invoke-virtual {v7, v2, v6}, Lhvk;->measure(II)V

    .line 731
    iget-object v7, p0, Lgbz;->ay:Lhvk;

    iget v8, p0, Lgbz;->B:I

    iget v9, p0, Lgbz;->B:I

    iget-object v10, p0, Lgbz;->ay:Lhvk;

    .line 732
    invoke-virtual {v10}, Lhvk;->getMeasuredHeight()I

    move-result v10

    add-int/2addr v9, v10

    .line 731
    invoke-virtual {v7, v0, v8, v1, v9}, Lhvk;->layout(IIII)V

    .line 735
    :cond_2
    iget-object v7, p0, Lgbz;->az:Lhvg;

    if-eqz v7, :cond_3

    iget-object v7, p0, Lgbz;->az:Lhvg;

    invoke-virtual {v7}, Lhvg;->getParent()Landroid/view/ViewParent;

    move-result-object v7

    if-ne v7, p0, :cond_3

    .line 736
    iget-object v7, p0, Lgbz;->az:Lhvg;

    iget v8, p0, Lgbz;->C:I

    iget v9, p0, Lgbz;->C:I

    iget-object v10, p0, Lgbz;->az:Lhvg;

    .line 737
    invoke-virtual {v10}, Lhvg;->getMeasuredHeight()I

    move-result v10

    add-int/2addr v9, v10

    .line 736
    invoke-virtual {v7, v0, v8, v1, v9}, Lhvg;->layout(IIII)V

    .line 740
    :cond_3
    iget-object v1, p0, Lgbz;->aC:Lhvp;

    if-eqz v1, :cond_4

    invoke-direct {p0}, Lgbz;->F()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 741
    iget-object v1, p0, Lgbz;->aC:Lhvp;

    invoke-virtual {v1, v5, v6}, Lhvp;->measure(II)V

    .line 742
    iget-object v1, p0, Lgbz;->aC:Lhvp;

    iget v7, p0, Lgbz;->y:I

    iget v8, p0, Lgbz;->y:I

    iget-object v9, p0, Lgbz;->aC:Lhvp;

    .line 743
    invoke-virtual {v9}, Lhvp;->getMeasuredHeight()I

    move-result v9

    add-int/2addr v8, v9

    .line 742
    invoke-virtual {v1, v3, v7, v4, v8}, Lhvp;->layout(IIII)V

    .line 746
    :cond_4
    iget-object v1, p0, Lgbz;->aN:Lldl;

    if-eqz v1, :cond_5

    invoke-direct {p0}, Lgbz;->E()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 747
    iget-object v1, p0, Lgbz;->aN:Lldl;

    invoke-virtual {v1, v2, v6}, Lldl;->measure(II)V

    .line 748
    iget-object v1, p0, Lgbz;->aN:Lldl;

    iget v2, p0, Lgbz;->y:I

    iget-object v3, p0, Lgbz;->aN:Lldl;

    .line 749
    invoke-virtual {v3}, Lldl;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, v0

    iget v7, p0, Lgbz;->y:I

    iget-object v8, p0, Lgbz;->aN:Lldl;

    .line 750
    invoke-virtual {v8}, Lldl;->getMeasuredHeight()I

    move-result v8

    add-int/2addr v7, v8

    .line 748
    invoke-virtual {v1, v0, v2, v3, v7}, Lldl;->layout(IIII)V

    .line 753
    :cond_5
    iget-object v0, p0, Lgbz;->aH:Landroid/widget/Button;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lgbz;->aH:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-ne v0, p0, :cond_6

    .line 755
    iget-object v0, p0, Lgbz;->K:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->getTop()I

    move-result v0

    .line 756
    iget-object v1, p0, Lgbz;->aH:Landroid/widget/Button;

    iget-object v2, p0, Lgbz;->aH:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/Button;->getMeasuredWidth()I

    move-result v2

    sub-int v2, v4, v2

    iget-object v3, p0, Lgbz;->aH:Landroid/widget/Button;

    .line 757
    invoke-virtual {v3}, Landroid/widget/Button;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v3, v0

    .line 756
    invoke-virtual {v1, v2, v0, v4, v3}, Landroid/widget/Button;->layout(IIII)V

    .line 760
    :cond_6
    iget-object v0, p0, Lgbz;->aG:Llee;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lgbz;->aG:Llee;

    invoke-virtual {v0}, Llee;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-ne v0, p0, :cond_7

    .line 761
    sget-object v0, Lgbz;->z:Llct;

    iget v0, v0, Llct;->m:I

    add-int/2addr v0, v4

    .line 762
    iget-object v1, p0, Lgbz;->K:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->getTop()I

    move-result v1

    .line 763
    iget-object v2, p0, Lgbz;->aG:Llee;

    iget-object v3, p0, Lgbz;->aG:Llee;

    invoke-virtual {v3}, Llee;->getMeasuredWidth()I

    move-result v3

    sub-int v3, v0, v3

    iget-object v7, p0, Lgbz;->aG:Llee;

    .line 764
    invoke-virtual {v7}, Llee;->getMeasuredHeight()I

    move-result v7

    add-int/2addr v7, v1

    .line 763
    invoke-virtual {v2, v3, v1, v0, v7}, Llee;->layout(IIII)V

    .line 767
    :cond_7
    iget-object v0, p0, Lgbz;->M:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    if-eqz v0, :cond_8

    invoke-direct {p0}, Lgbz;->G()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lgbz;->M:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    .line 768
    invoke-virtual {v0}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-ne v0, p0, :cond_8

    .line 777
    iget-object v0, p0, Lgbz;->M:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->getMeasuredWidth()I

    move-result v0

    iget-object v1, p0, Lgbz;->N:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    .line 778
    invoke-virtual {v1}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->getMeasuredWidth()I

    move-result v1

    .line 777
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    sub-int v1, v4, v0

    .line 779
    iget v0, p0, Lgbz;->A:I

    iget v2, p0, Lgbz;->E:I

    add-int/2addr v2, v0

    .line 780
    iget-object v0, p0, Lgbz;->M:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->getMeasuredHeight()I

    move-result v0

    iget-object v3, p0, Lgbz;->N:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    .line 781
    invoke-virtual {v3}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->getMeasuredHeight()I

    move-result v3

    .line 780
    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int v3, v2, v0

    .line 783
    iget-object v0, p0, Lgbz;->N:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    invoke-virtual {v0, v1, v2, v4, v3}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->layout(IIII)V

    .line 784
    iget-object v7, p0, Lgbz;->N:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    iget-boolean v0, p0, Lgbz;->am:Z

    if-eqz v0, :cond_c

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v7, v0}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->setVisibility(I)V

    .line 785
    iget-object v0, p0, Lgbz;->M:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    invoke-virtual {v0, v1, v2, v4, v3}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->layout(IIII)V

    .line 786
    iget-object v1, p0, Lgbz;->M:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    iget-boolean v0, p0, Lgbz;->am:Z

    if-eqz v0, :cond_d

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->setVisibility(I)V

    .line 789
    :cond_8
    iget-object v0, p0, Lgbz;->aA:Lgbh;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lgbz;->aA:Lgbh;

    invoke-virtual {v0}, Lgbh;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-ne v0, p0, :cond_9

    .line 790
    iget-object v0, p0, Lgbz;->aA:Lgbh;

    invoke-virtual {v0, v5, v6}, Lgbh;->measure(II)V

    .line 791
    sget-object v0, Lgbz;->z:Llct;

    iget v0, v0, Llct;->m:I

    .line 793
    iget-object v1, p0, Lgbz;->ay:Lhvk;

    invoke-virtual {v1}, Lhvk;->getTop()I

    move-result v1

    iget-object v2, p0, Lgbz;->aA:Lgbh;

    invoke-virtual {v2}, Lgbh;->getMeasuredHeight()I

    move-result v2

    sub-int/2addr v1, v2

    .line 794
    iget-object v2, p0, Lgbz;->aA:Lgbh;

    iget-object v3, p0, Lgbz;->aA:Lgbh;

    invoke-virtual {v3}, Lgbh;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, v0

    iget-object v4, p0, Lgbz;->aA:Lgbh;

    .line 795
    invoke-virtual {v4}, Lgbh;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v1

    .line 794
    invoke-virtual {v2, v0, v1, v3, v4}, Lgbh;->layout(IIII)V

    .line 798
    :cond_9
    iget-object v0, p0, Lgbz;->aK:Lldb;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lgbz;->aK:Lldb;

    invoke-virtual {v0}, Lldb;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-ne v0, p0, :cond_a

    .line 799
    iget-object v0, p0, Lgbz;->aK:Lldb;

    iget-object v1, p0, Lgbz;->aM:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    iget-object v2, p0, Lgbz;->aM:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    iget-object v3, p0, Lgbz;->aM:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    iget-object v4, p0, Lgbz;->aK:Lldb;

    .line 800
    invoke-virtual {v4}, Lldb;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v3, v4

    iget-object v4, p0, Lgbz;->aM:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    iget-object v5, p0, Lgbz;->aK:Lldb;

    .line 801
    invoke-virtual {v5}, Lldb;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v4, v5

    .line 799
    invoke-virtual {v0, v1, v2, v3, v4}, Lldb;->layout(IIII)V

    .line 804
    :cond_a
    iget-object v0, p0, Lgbz;->aq:Landroid/widget/TextView;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lgbz;->aq:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-ne v0, p0, :cond_b

    .line 805
    iget-object v0, p0, Lgbz;->aq:Landroid/widget/TextView;

    iget-object v1, p0, Lgbz;->ar:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    iget-object v2, p0, Lgbz;->ar:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    iget-object v3, p0, Lgbz;->ar:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    iget-object v4, p0, Lgbz;->aq:Landroid/widget/TextView;

    .line 806
    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v3, v4

    iget-object v4, p0, Lgbz;->ar:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    iget-object v5, p0, Lgbz;->aq:Landroid/widget/TextView;

    .line 807
    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v4, v5

    .line 805
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->layout(IIII)V

    .line 809
    :cond_b
    return-void

    .line 784
    :cond_c
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 786
    :cond_d
    const/16 v0, 0x8

    goto/16 :goto_1
.end method

.method public aD_()Z
    .locals 1

    .prologue
    .line 1524
    const/4 v0, 0x0

    return v0
.end method

.method public a_(Landroid/database/Cursor;Llcr;I)V
    .locals 0

    .prologue
    .line 1529
    return-void
.end method

.method protected b(I)I
    .locals 1

    .prologue
    .line 1058
    invoke-direct {p0}, Lgbz;->F()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1062
    :goto_0
    return p1

    :cond_0
    iget-object v0, p0, Lgbz;->aC:Lhvp;

    invoke-virtual {v0}, Lhvp;->getHeight()I

    move-result v0

    add-int/2addr p1, v0

    goto :goto_0
.end method

.method protected b(II)I
    .locals 11

    .prologue
    const/4 v1, 0x1

    const/4 v9, 0x0

    .line 1578
    iget-object v0, p0, Lgbz;->ay:Lhvk;

    invoke-virtual {p0, v0}, Lgbz;->removeView(Landroid/view/View;)V

    .line 1581
    invoke-virtual {p0}, Lgbz;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v2, Lgcs;

    invoke-static {v0, v2}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgcs;

    .line 1582
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lgbz;->q()I

    invoke-interface {v0}, Lgcs;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    move v6, v1

    .line 1583
    :goto_0
    iget-object v0, p0, Lgbz;->ay:Lhvk;

    iget-object v2, p0, Lgbz;->ag:Llah;

    iget-boolean v3, p0, Lgbz;->al:Z

    iget-boolean v4, p0, Lgbz;->ak:Z

    iget-boolean v5, p0, Lgbz;->an:Z

    iget v7, p0, Lgbz;->af:I

    iget-object v8, p0, Lgbz;->ah:Llam;

    iget v10, p0, Lgbz;->w:I

    if-eqz v10, :cond_1

    move v10, v1

    :goto_1
    move v1, p2

    move-object v9, p0

    invoke-virtual/range {v0 .. v10}, Lhvk;->a(ILhtz;ZZZZILhts;Lhvl;Z)I

    move-result v0

    .line 1585
    iget-object v1, p0, Lgbz;->ay:Lhvk;

    invoke-virtual {p0, v1}, Lgbz;->addView(Landroid/view/View;)V

    .line 1587
    add-int/2addr v0, p1

    return v0

    :cond_0
    move v6, v9

    .line 1582
    goto :goto_0

    :cond_1
    move v10, v9

    .line 1583
    goto :goto_1
.end method

.method protected b(III)I
    .locals 4

    .prologue
    .line 1632
    iget-object v0, p0, Lgbz;->aB:Lldr;

    invoke-virtual {p0, v0}, Lgbz;->removeView(Landroid/view/View;)V

    .line 1634
    iget v0, p0, Lgbz;->w:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lgbz;->ah:Llam;

    if-eqz v0, :cond_0

    .line 1635
    invoke-virtual {p0}, Lgbz;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Llhn;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1647
    :cond_0
    :goto_0
    return p2

    .line 1639
    :cond_1
    iget-object v0, p0, Lgbz;->ah:Llam;

    invoke-virtual {v0}, Llam;->a()I

    move-result v0

    .line 1640
    if-eqz v0, :cond_0

    .line 1644
    iget-object v0, p0, Lgbz;->aB:Lldr;

    iget-object v1, p0, Lgbz;->ah:Llam;

    iget-object v2, p0, Lgbz;->ay:Lhvk;

    invoke-virtual {p0}, Lgbz;->z()Z

    move-result v3

    invoke-virtual {v0, v1, p2, v2, v3}, Lldr;->a(Lhts;ILhvd;Z)I

    move-result p2

    .line 1645
    iget-object v0, p0, Lgbz;->aB:Lldr;

    invoke-virtual {p0, v0}, Lgbz;->addView(Landroid/view/View;)V

    goto :goto_0
.end method

.method protected b(Landroid/graphics/Canvas;II)I
    .locals 7

    .prologue
    .line 1469
    iget-object v0, p0, Lgbz;->R:Llir;

    if-eqz v0, :cond_0

    .line 1470
    int-to-float v0, p2

    int-to-float v1, p3

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1471
    iget-object v0, p0, Lgbz;->R:Llir;

    invoke-virtual {v0, p1}, Llir;->draw(Landroid/graphics/Canvas;)V

    .line 1472
    neg-int v0, p2

    int-to-float v0, v0

    neg-int v1, p3

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1473
    iget-object v0, p0, Lgbz;->R:Llir;

    invoke-virtual {v0}, Llir;->getHeight()I

    move-result v0

    add-int/2addr p3, v0

    .line 1476
    :cond_0
    iget-object v0, p0, Lgbz;->U:Llir;

    if-eqz v0, :cond_4

    .line 1477
    iget-object v0, p0, Lgbz;->R:Llir;

    if-eqz v0, :cond_1

    .line 1478
    sget-object v0, Lgbz;->z:Llct;

    iget v0, v0, Llct;->m:I

    add-int/2addr p3, v0

    .line 1480
    :cond_1
    sget-object v0, Lgbz;->z:Llct;

    iget v0, v0, Llct;->m:I

    add-int v6, p3, v0

    .line 1482
    sget-object v0, Lgbz;->z:Llct;

    iget-object v0, v0, Llct;->H:Landroid/graphics/Rect;

    const/4 v1, 0x0

    sget-object v2, Lgbz;->z:Llct;

    iget v2, v2, Llct;->m:I

    neg-int v2, v2

    iget-object v3, p0, Lgbz;->U:Llir;

    invoke-virtual {v3}, Llir;->getWidth()I

    move-result v3

    iget-object v4, p0, Lgbz;->U:Llir;

    .line 1483
    invoke-virtual {v4}, Llir;->getHeight()I

    move-result v4

    sget-object v5, Lgbz;->z:Llct;

    iget v5, v5, Llct;->m:I

    add-int/2addr v4, v5

    .line 1482
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 1485
    iget-object v0, p0, Lgbz;->W:Llir;

    if-eqz v0, :cond_2

    .line 1486
    sget-object v0, Lgbz;->z:Llct;

    iget-object v0, v0, Llct;->H:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->bottom:I

    sget-object v2, Lgbz;->z:Llct;

    iget v2, v2, Llct;->l:I

    iget-object v3, p0, Lgbz;->W:Llir;

    .line 1487
    invoke-virtual {v3}, Llir;->getHeight()I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 1490
    :cond_2
    int-to-float v0, p2

    int-to-float v1, v6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1491
    sget-object v0, Lgbz;->z:Llct;

    iget-object v0, v0, Llct;->H:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v1, v0

    sget-object v0, Lgbz;->z:Llct;

    iget-object v0, v0, Llct;->H:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v2, v0

    sget-object v0, Lgbz;->z:Llct;

    iget-object v0, v0, Llct;->H:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    int-to-float v3, v0

    sget-object v0, Lgbz;->z:Llct;

    iget-object v0, v0, Llct;->H:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v4, v0

    sget-object v0, Lgbz;->z:Llct;

    iget-object v5, v0, Llct;->w:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1494
    iget-object v0, p0, Lgbz;->U:Llir;

    invoke-virtual {v0, p1}, Llir;->draw(Landroid/graphics/Canvas;)V

    .line 1496
    invoke-virtual {p0}, Lgbz;->aD_()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1497
    sget-object v0, Lgbz;->z:Llct;

    iget-object v0, v0, Llct;->H:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v1, v0

    sget-object v0, Lgbz;->z:Llct;

    iget-object v0, v0, Llct;->H:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v0

    sget-object v0, Lgbz;->z:Llct;

    iget-object v0, v0, Llct;->H:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    int-to-float v3, v0

    sget-object v0, Lgbz;->z:Llct;

    iget-object v0, v0, Llct;->H:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v0

    sget-object v0, Lgbz;->z:Llct;

    iget-object v5, v0, Llct;->w:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1502
    :cond_3
    neg-int v0, p2

    int-to-float v0, v0

    neg-int v1, v6

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1503
    iget-object v0, p0, Lgbz;->U:Llir;

    invoke-virtual {v0}, Llir;->getHeight()I

    move-result v0

    add-int p3, v6, v0

    .line 1506
    :cond_4
    iget-object v0, p0, Lgbz;->W:Llir;

    if-eqz v0, :cond_7

    .line 1507
    iget-object v0, p0, Lgbz;->R:Llir;

    if-nez v0, :cond_5

    iget-object v0, p0, Lgbz;->U:Llir;

    if-eqz v0, :cond_6

    .line 1508
    :cond_5
    sget-object v0, Lgbz;->z:Llct;

    iget v0, v0, Llct;->l:I

    add-int/2addr p3, v0

    .line 1510
    :cond_6
    int-to-float v0, p2

    int-to-float v1, p3

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1511
    iget-object v0, p0, Lgbz;->W:Llir;

    invoke-virtual {v0, p1}, Llir;->draw(Landroid/graphics/Canvas;)V

    .line 1512
    neg-int v0, p2

    int-to-float v0, v0

    neg-int v1, p3

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1513
    iget-object v0, p0, Lgbz;->W:Llir;

    invoke-virtual {v0}, Llir;->getHeight()I

    move-result v0

    add-int/2addr p3, v0

    .line 1516
    :cond_7
    iget-object v0, p0, Lgbz;->R:Llir;

    if-nez v0, :cond_8

    iget-object v0, p0, Lgbz;->U:Llir;

    if-nez v0, :cond_8

    iget-object v0, p0, Lgbz;->W:Llir;

    if-eqz v0, :cond_9

    .line 1517
    :cond_8
    sget-object v0, Lgbz;->z:Llct;

    iget v0, v0, Llct;->m:I

    add-int/2addr p3, v0

    .line 1520
    :cond_9
    return p3
.end method

.method protected b(Landroid/graphics/Canvas;III)I
    .locals 6

    .prologue
    .line 1557
    iget-object v0, p0, Lgbz;->H:Llir;

    if-eqz v0, :cond_2

    .line 1558
    int-to-float v1, p2

    int-to-float v2, p3

    add-int v0, p2, p4

    int-to-float v3, v0

    int-to-float v4, p3

    sget-object v0, Lgbz;->z:Llct;

    iget-object v5, v0, Llct;->w:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1559
    sget-object v0, Lgbz;->z:Llct;

    iget v0, v0, Llct;->m:I

    add-int/2addr v0, p3

    .line 1560
    iget-object v1, p0, Lgbz;->I:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    .line 1561
    sget-object v1, Lgbz;->z:Llct;

    iget v1, v1, Llct;->q:I

    add-int/2addr v1, v0

    .line 1562
    iget-object v2, p0, Lgbz;->I:Landroid/graphics/Bitmap;

    int-to-float v3, p2

    int-to-float v1, v1

    const/4 v4, 0x0

    invoke-virtual {p1, v2, v3, v1, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1563
    iget-object v1, p0, Lgbz;->I:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    sget-object v2, Lgbz;->z:Llct;

    iget v2, v2, Llct;->r:I

    add-int/2addr v1, v2

    add-int/2addr p2, v1

    .line 1565
    :cond_0
    int-to-float v1, p2

    int-to-float v2, v0

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1566
    iget-object v1, p0, Lgbz;->H:Llir;

    invoke-virtual {v1, p1}, Llir;->draw(Landroid/graphics/Canvas;)V

    .line 1567
    neg-int v1, p2

    int-to-float v1, v1

    neg-int v2, v0

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1568
    iget-object v1, p0, Lgbz;->I:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_1

    .line 1569
    iget-object v1, p0, Lgbz;->I:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    sget-object v1, Lgbz;->z:Llct;

    iget v1, v1, Llct;->r:I

    .line 1571
    :cond_1
    iget-object v1, p0, Lgbz;->H:Llir;

    invoke-virtual {v1}, Llir;->getHeight()I

    move-result v1

    sget-object v2, Lgbz;->z:Llct;

    iget v2, v2, Llct;->m:I

    add-int/2addr v1, v2

    add-int p3, v0, v1

    .line 1574
    :cond_2
    return p3
.end method

.method public b(Z)Landroid/content/Intent;
    .locals 5

    .prologue
    .line 1664
    iget-object v0, p0, Lgbz;->r:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1665
    const/4 v0, 0x0

    .line 1686
    :goto_0
    return-object v0

    .line 1667
    :cond_0
    invoke-virtual {p0}, Lgbz;->q()I

    move-result v0

    .line 1668
    invoke-virtual {p0}, Lgbz;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 1670
    iget-object v2, p0, Lgbz;->r:Ljava/lang/String;

    iget-boolean v3, p0, Lgbz;->g:Z

    iget-object v4, p0, Lgbz;->l:[B

    invoke-static {v1, v0, v2, v3, v4}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;Z[B)Landroid/content/Intent;

    move-result-object v0

    .line 1672
    iget-object v1, p0, Lgbz;->as:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1673
    const-string v1, "square_id"

    iget-object v2, p0, Lgbz;->as:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1675
    :cond_1
    iget-object v1, p0, Lgbz;->aE:Lkzp;

    if-eqz v1, :cond_2

    .line 1677
    :try_start_0
    const-string v1, "context_specific_data"

    iget-object v2, p0, Lgbz;->aE:Lkzp;

    .line 1678
    invoke-static {v2}, Lkzp;->a(Lkzp;)[B

    move-result-object v2

    .line 1677
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1683
    :cond_2
    :goto_1
    const-string v1, "show_keyboard"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1684
    const-string v1, "max_span"

    invoke-virtual {p0}, Lgbz;->A()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0

    .line 1680
    :catch_0
    move-exception v1

    const-string v1, "UpdateCardViewGroup"

    iget-object v2, p0, Lgbz;->aE:Lkzp;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x2a

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Unable to serialize DbContextSpecificData "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public b()V
    .locals 1

    .prologue
    .line 1001
    invoke-super {p0}, Lldq;->b()V

    .line 1002
    invoke-static {p0}, Llii;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1003
    iget-object v0, p0, Lgbz;->K:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    if-eqz v0, :cond_0

    .line 1004
    iget-object v0, p0, Lgbz;->K:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->b()V

    .line 1006
    :cond_0
    iget-object v0, p0, Lgbz;->ay:Lhvk;

    invoke-virtual {v0}, Lhvk;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-ne v0, p0, :cond_1

    .line 1007
    iget-object v0, p0, Lgbz;->ay:Lhvk;

    invoke-virtual {v0}, Lhvk;->b()V

    .line 1010
    :cond_1
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 1864
    invoke-virtual {p0}, Lgbz;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lhtp;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhtp;

    .line 1865
    iget-object v1, p0, Lgbz;->r:Ljava/lang/String;

    iget-object v2, p0, Lgbz;->c:Ljava/lang/String;

    iget-boolean v3, p0, Lgbz;->j:Z

    iget-boolean v4, p0, Lgbz;->g:Z

    iget-object v4, p0, Lgbz;->l:[B

    invoke-interface {v0, v1, v2, v3, p1}, Lhtp;->a(Ljava/lang/String;Ljava/lang/String;ZLandroid/os/Bundle;)V

    .line 1867
    return-void
.end method

.method public b(Landroid/text/Spanned;)V
    .locals 1

    .prologue
    .line 687
    invoke-static {p1}, Llir;->a(Landroid/text/Spanned;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    iput-object v0, p0, Lgbz;->Q:Landroid/text/Spanned;

    .line 688
    return-void
.end method

.method protected c(I)I
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1066
    invoke-direct {p0}, Lgbz;->E()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1084
    :goto_0
    return p1

    .line 1070
    :cond_0
    iget-object v0, p0, Lgbz;->aN:Lldl;

    invoke-virtual {p0, v0}, Lgbz;->removeView(Landroid/view/View;)V

    .line 1072
    iget-object v0, p0, Lgbz;->aN:Lldl;

    if-nez v0, :cond_1

    .line 1073
    new-instance v0, Lldl;

    invoke-virtual {p0}, Lgbz;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, Lldl;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lgbz;->aN:Lldl;

    .line 1078
    :cond_1
    iget-object v0, p0, Lgbz;->aE:Lkzp;

    invoke-virtual {v0}, Lkzp;->n()Llac;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lgbz;->aE:Lkzp;

    .line 1079
    invoke-virtual {v0}, Lkzp;->n()Llac;

    move-result-object v0

    invoke-virtual {v0}, Llac;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    .line 1080
    :goto_1
    iget-boolean v3, p0, Lgbz;->am:Z

    if-eqz v3, :cond_2

    if-eqz v0, :cond_3

    :cond_2
    move v2, v1

    .line 1081
    :cond_3
    iget-object v0, p0, Lgbz;->aN:Lldl;

    iget-object v1, p0, Lgbz;->d:Ljava/lang/String;

    invoke-virtual {v0, v2, v1, p1}, Lldl;->a(ZLjava/lang/String;I)I

    move-result p1

    .line 1082
    iget-object v0, p0, Lgbz;->aN:Lldl;

    invoke-virtual {p0, v0}, Lgbz;->addView(Landroid/view/View;)V

    goto :goto_0

    :cond_4
    move v0, v2

    .line 1079
    goto :goto_1
.end method

.method protected c(II)I
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1600
    iget v0, p0, Lgbz;->w:I

    if-nez v0, :cond_1

    .line 1619
    :cond_0
    :goto_0
    return p1

    .line 1603
    :cond_1
    iget-object v0, p0, Lgbz;->az:Lhvg;

    invoke-virtual {p0, v0}, Lgbz;->removeView(Landroid/view/View;)V

    .line 1605
    iget v0, p0, Lgbz;->ae:I

    if-nez v0, :cond_2

    iget-object v0, p0, Lgbz;->ag:Llah;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbz;->ag:Llah;

    invoke-virtual {v0}, Llah;->b()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbz;->ag:Llah;

    .line 1606
    invoke-virtual {v0}, Llah;->b()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lgbz;->ag:Llah;

    invoke-virtual {v0}, Llah;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1609
    :cond_2
    iget-object v0, p0, Lgbz;->az:Lhvg;

    if-nez v0, :cond_3

    .line 1610
    new-instance v0, Lhvg;

    invoke-virtual {p0}, Lgbz;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lhvg;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lgbz;->az:Lhvg;

    .line 1611
    iget-object v0, p0, Lgbz;->az:Lhvg;

    sget-object v1, Lgbz;->z:Llct;

    iget v1, v1, Llct;->m:I

    sget-object v2, Lgbz;->z:Llct;

    iget v2, v2, Llct;->D:I

    sget-object v3, Lgbz;->z:Llct;

    iget v3, v3, Llct;->m:I

    sget-object v4, Lgbz;->z:Llct;

    iget v4, v4, Llct;->D:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lhvg;->setPadding(IIII)V

    .line 1614
    :cond_3
    iget-object v0, p0, Lgbz;->az:Lhvg;

    iget-object v1, p0, Lgbz;->ag:Llah;

    iget v2, p0, Lgbz;->ae:I

    invoke-virtual {v0, v1, v2}, Lhvg;->a(Lhtz;I)V

    .line 1615
    iget-object v0, p0, Lgbz;->az:Lhvg;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {p2, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 1616
    invoke-static {v5, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 1615
    invoke-virtual {v0, v1, v2}, Lhvg;->measure(II)V

    .line 1617
    iget-object v0, p0, Lgbz;->az:Lhvg;

    invoke-virtual {p0, v0}, Lgbz;->addView(Landroid/view/View;)V

    .line 1619
    iget-object v0, p0, Lgbz;->az:Lhvg;

    invoke-virtual {v0}, Lhvg;->getMeasuredHeight()I

    move-result v0

    add-int/2addr p1, v0

    goto :goto_0
.end method

.method protected c(III)I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1101
    iget-object v0, p0, Lgbz;->aL:Lkzm;

    if-nez v0, :cond_0

    .line 1115
    :goto_0
    return p2

    .line 1104
    :cond_0
    iget-object v0, p0, Lgbz;->aK:Lldb;

    invoke-virtual {p0, v0}, Lgbz;->removeView(Landroid/view/View;)V

    .line 1105
    new-instance v0, Lldb;

    invoke-virtual {p0}, Lgbz;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lldb;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lgbz;->aK:Lldb;

    .line 1106
    iget-object v0, p0, Lgbz;->aK:Lldb;

    iget-object v1, p0, Lgbz;->i:Lfdp;

    invoke-virtual {p0}, Lgbz;->q()I

    move-result v2

    iget-object v3, p0, Lgbz;->aL:Lkzm;

    invoke-virtual {v0, v1, v2, v3}, Lldb;->a(Lldc;ILkzm;)V

    .line 1107
    iget-object v0, p0, Lgbz;->aK:Lldb;

    invoke-virtual {p0, v0}, Lgbz;->addView(Landroid/view/View;)V

    .line 1109
    invoke-static {v4, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 1110
    const/high16 v1, 0x40000000    # 2.0f

    .line 1111
    invoke-static {p3, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 1112
    iget-object v2, p0, Lgbz;->aK:Lldb;

    invoke-virtual {v2, v1, v0}, Lldb;->measure(II)V

    .line 1113
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, p1, p2}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lgbz;->aM:Landroid/graphics/Point;

    .line 1115
    iget-object v0, p0, Lgbz;->aM:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    iget-object v1, p0, Lgbz;->aK:Lldb;

    invoke-virtual {v1}, Lldb;->getMeasuredHeight()I

    move-result v1

    add-int p2, v0, v1

    goto :goto_0
.end method

.method protected c(Landroid/graphics/Canvas;III)I
    .locals 6

    .prologue
    .line 1651
    iget v0, p0, Lgbz;->w:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lgbz;->aB:Lldr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbz;->aB:Lldr;

    invoke-virtual {v0}, Lldr;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-ne v0, p0, :cond_0

    iget-object v0, p0, Lgbz;->ah:Llam;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbz;->ah:Llam;

    .line 1652
    invoke-virtual {v0}, Llam;->a()I

    move-result v0

    if-nez v0, :cond_1

    .line 1659
    :cond_0
    :goto_0
    return p3

    .line 1656
    :cond_1
    int-to-float v1, p2

    int-to-float v2, p3

    add-int v0, p2, p4

    int-to-float v3, v0

    int-to-float v4, p3

    sget-object v0, Lgbz;->z:Llct;

    iget-object v5, v0, Llct;->w:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1659
    iget-object v0, p0, Lgbz;->aB:Lldr;

    invoke-virtual {v0}, Lldr;->getHeight()I

    move-result v0

    add-int/2addr p3, v0

    goto :goto_0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 1014
    iget-object v0, p0, Lgbz;->K:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    if-eqz v0, :cond_0

    .line 1015
    iget-object v0, p0, Lgbz;->K:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->c()V

    .line 1017
    :cond_0
    iget-object v0, p0, Lgbz;->ay:Lhvk;

    invoke-virtual {v0}, Lhvk;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-ne v0, p0, :cond_1

    .line 1018
    iget-object v0, p0, Lgbz;->ay:Lhvk;

    invoke-virtual {v0}, Lhvk;->c()V

    .line 1020
    :cond_1
    invoke-super {p0}, Lldq;->c()V

    .line 1021
    return-void
.end method

.method public c(Z)V
    .locals 0

    .prologue
    .line 292
    iput-boolean p1, p0, Lgbz;->av:Z

    .line 293
    return-void
.end method

.method protected d(I)I
    .locals 1

    .prologue
    .line 1088
    invoke-direct {p0}, Lgbz;->E()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1092
    :goto_0
    return p1

    :cond_0
    iget-object v0, p0, Lgbz;->aN:Lldl;

    invoke-virtual {v0}, Lldl;->getHeight()I

    move-result v0

    add-int/2addr p1, v0

    goto :goto_0
.end method

.method protected d(III)I
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1127
    iget-object v0, p0, Lgbz;->aq:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lgbz;->removeView(Landroid/view/View;)V

    .line 1128
    iget-boolean v0, p0, Lgbz;->au:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lgbz;->av:Z

    if-eqz v0, :cond_2

    :cond_0
    if-lez p3, :cond_2

    .line 1129
    iget-object v0, p0, Lgbz;->aq:Landroid/widget/TextView;

    if-nez v0, :cond_1

    .line 1130
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lgbz;->ar:Landroid/graphics/Point;

    .line 1131
    invoke-virtual {p0}, Lgbz;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    const/16 v2, 0x26

    invoke-static {v0, v1, v5, v2}, Llhx;->a(Landroid/content/Context;Landroid/util/AttributeSet;II)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Lgbz;->aq:Landroid/widget/TextView;

    .line 1134
    iget-object v0, p0, Lgbz;->aq:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1135
    iget-object v0, p0, Lgbz;->aq:Landroid/widget/TextView;

    const v1, 0x7f0203e4

    invoke-virtual {v0, v1, v5, v5, v5}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 1137
    iget-object v0, p0, Lgbz;->aq:Landroid/widget/TextView;

    sget-object v1, Lgbz;->z:Llct;

    iget v1, v1, Llct;->F:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    .line 1138
    iget-object v0, p0, Lgbz;->aq:Landroid/widget/TextView;

    sget-object v1, Lgbz;->z:Llct;

    iget v1, v1, Llct;->G:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMinHeight(I)V

    .line 1139
    iget-object v0, p0, Lgbz;->aq:Landroid/widget/TextView;

    sget-object v1, Lgbz;->z:Llct;

    iget v1, v1, Llct;->F:I

    sget-object v2, Lgbz;->z:Llct;

    iget v2, v2, Llct;->m:I

    sget-object v3, Lgbz;->z:Llct;

    iget v3, v3, Llct;->F:I

    sget-object v4, Lgbz;->z:Llct;

    iget v4, v4, Llct;->m:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 1141
    iget-object v0, p0, Lgbz;->aq:Landroid/widget/TextView;

    const v1, 0x7f0b0112

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 1144
    :cond_1
    iget-object v0, p0, Lgbz;->ar:Landroid/graphics/Point;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/Point;->set(II)V

    .line 1145
    iget-boolean v0, p0, Lgbz;->av:Z

    if-nez v0, :cond_3

    .line 1146
    iget-object v0, p0, Lgbz;->aq:Landroid/widget/TextView;

    const v1, 0x7f0a0970

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1151
    :goto_0
    iget-object v0, p0, Lgbz;->aq:Landroid/widget/TextView;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {p3, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 1152
    invoke-static {v5, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 1151
    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->measure(II)V

    .line 1153
    iget-object v0, p0, Lgbz;->aq:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lgbz;->addView(Landroid/view/View;)V

    .line 1155
    iget-object v0, p0, Lgbz;->aq:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v0

    sget-object v1, Lgbz;->z:Llct;

    iget v1, v1, Llct;->l:I

    add-int/2addr v0, v1

    add-int/2addr p2, v0

    .line 1157
    :cond_2
    return p2

    .line 1148
    :cond_3
    iget-object v0, p0, Lgbz;->aq:Landroid/widget/TextView;

    const v1, 0x7f0a0971

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method public d(Z)V
    .locals 0

    .prologue
    .line 1027
    iput-boolean p1, p0, Lgbz;->aw:Z

    .line 1028
    return-void
.end method

.method protected e(I)I
    .locals 1

    .prologue
    .line 1119
    iget-object v0, p0, Lgbz;->aK:Lldb;

    if-nez v0, :cond_0

    .line 1123
    :goto_0
    return p1

    :cond_0
    iget-object v0, p0, Lgbz;->aK:Lldb;

    invoke-virtual {v0}, Lldb;->getHeight()I

    move-result v0

    add-int/2addr p1, v0

    goto :goto_0
.end method

.method protected e(III)I
    .locals 13

    .prologue
    .line 1255
    invoke-virtual {p0}, Lgbz;->getContext()Landroid/content/Context;

    move-result-object v7

    .line 1256
    invoke-static {v7}, Lhss;->c(Landroid/content/Context;)I

    move-result v8

    .line 1258
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 1259
    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    .line 1261
    iget-object v0, p0, Lgbz;->K:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {p0, v0}, Lgbz;->removeView(Landroid/view/View;)V

    .line 1262
    iget-object v0, p0, Lgbz;->K:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    iget-object v1, p0, Lgbz;->c:Ljava/lang/String;

    iget-object v2, p0, Lgbz;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1263
    iget-object v0, p0, Lgbz;->K:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0, v9, v9}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->measure(II)V

    .line 1264
    iget-object v0, p0, Lgbz;->K:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {p0, v0}, Lgbz;->addView(Landroid/view/View;)V

    .line 1267
    sget-object v0, Lgbz;->z:Llct;

    iget v0, v0, Llct;->m:I

    add-int v10, p1, v0

    .line 1268
    sget-object v0, Lgbz;->z:Llct;

    iget v0, v0, Llct;->m:I

    mul-int/lit8 v0, v0, 0x2

    sub-int v0, p3, v0

    .line 1270
    iget-object v1, p0, Lgbz;->M:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    invoke-virtual {p0, v1}, Lgbz;->removeView(Landroid/view/View;)V

    .line 1271
    iget-object v1, p0, Lgbz;->N:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    invoke-virtual {p0, v1}, Lgbz;->removeView(Landroid/view/View;)V

    .line 1272
    invoke-direct {p0}, Lgbz;->G()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1273
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 1274
    iget-object v2, p0, Lgbz;->M:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    invoke-virtual {v2, v1, v9}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->measure(II)V

    .line 1275
    iget-object v2, p0, Lgbz;->N:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    invoke-virtual {v2, v1, v9}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->measure(II)V

    .line 1276
    iget-object v1, p0, Lgbz;->M:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->getMeasuredWidth()I

    move-result v1

    iget-object v2, p0, Lgbz;->N:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    .line 1277
    invoke-virtual {v2}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->getMeasuredWidth()I

    move-result v2

    .line 1276
    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    sget-object v2, Lgbz;->z:Llct;

    iget v2, v2, Llct;->m:I

    add-int/2addr v1, v2

    sub-int/2addr v0, v1

    .line 1278
    iget-object v1, p0, Lgbz;->M:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    invoke-virtual {p0, v1}, Lgbz;->addView(Landroid/view/View;)V

    .line 1279
    iget-object v1, p0, Lgbz;->N:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    invoke-virtual {p0, v1}, Lgbz;->addView(Landroid/view/View;)V

    .line 1282
    :cond_0
    sget-object v1, Lgbz;->z:Llct;

    iget v1, v1, Llct;->m:I

    add-int/2addr v1, v8

    sub-int v11, v0, v1

    .line 1284
    iget-object v0, p0, Lgbz;->J:Landroid/text/Spanned;

    if-eqz v0, :cond_5

    .line 1285
    const/16 v0, 0x1a

    .line 1286
    invoke-static {v7, v0}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v0

    iget-object v1, p0, Lgbz;->J:Landroid/text/Spanned;

    sget-object v2, Lgbz;->z:Llct;

    iget v2, v2, Llct;->aE:I

    .line 1285
    invoke-static {v0, v1, v11, v2}, Llib;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v0

    iput-object v0, p0, Lgbz;->L:Landroid/text/StaticLayout;

    .line 1296
    :goto_0
    iget-object v0, p0, Lgbz;->L:Landroid/text/StaticLayout;

    .line 1297
    invoke-virtual {v0}, Landroid/text/StaticLayout;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    iget-object v1, p0, Lgbz;->L:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getPaint()Landroid/text/TextPaint;

    move-result-object v1

    .line 1296
    invoke-static {v0, v1}, Landroid/text/Layout;->getDesiredWidth(Ljava/lang/CharSequence;Landroid/text/TextPaint;)F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v12, v0

    .line 1299
    const/4 v0, 0x0

    .line 1301
    iget-boolean v1, p0, Lgbz;->g:Z

    if-eqz v1, :cond_b

    .line 1302
    sget-object v0, Lgbz;->z:Llct;

    iget-object v0, v0, Llct;->z:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    sget-object v1, Lgbz;->z:Llct;

    iget v1, v1, Llct;->k:I

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v6, v0

    .line 1305
    :goto_1
    const/16 v0, 0xa

    .line 1306
    invoke-static {v7, v0}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v0

    iget-object v1, p0, Lgbz;->O:Landroid/text/SpannableStringBuilder;

    sub-int v2, v11, v6

    const/4 v3, 0x1

    const/high16 v4, 0x3f800000    # 1.0f

    move-object v5, p0

    .line 1305
    invoke-static/range {v0 .. v5}, Llir;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;IIFLljv;)Llir;

    move-result-object v0

    iput-object v0, p0, Lgbz;->P:Llir;

    .line 1309
    iget-object v0, p0, Lgbz;->P:Llir;

    invoke-virtual {v0}, Llir;->getWidth()I

    move-result v0

    add-int/2addr v0, v6

    .line 1311
    invoke-static {v12, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    sub-int v0, v11, v0

    .line 1313
    iget-object v1, p0, Lgbz;->aH:Landroid/widget/Button;

    invoke-virtual {p0, v1}, Lgbz;->removeView(Landroid/view/View;)V

    .line 1314
    iget-object v1, p0, Lgbz;->aG:Llee;

    invoke-virtual {p0, v1}, Lgbz;->removeView(Landroid/view/View;)V

    .line 1315
    iget-object v1, p0, Lgbz;->aI:Lhua;

    if-eqz v1, :cond_3

    invoke-direct {p0}, Lgbz;->G()Z

    move-result v1

    if-nez v1, :cond_3

    .line 1316
    iget-object v1, p0, Lgbz;->aH:Landroid/widget/Button;

    if-eqz v1, :cond_7

    .line 1317
    sget-object v1, Lgbz;->z:Llct;

    iget v1, v1, Llct;->m:I

    sub-int/2addr v0, v1

    .line 1318
    iget-object v1, p0, Lgbz;->aI:Lhua;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lhua;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 1319
    iget-object v2, p0, Lgbz;->aH:Landroid/widget/Button;

    invoke-virtual {v2, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1320
    iget-object v2, p0, Lgbz;->aH:Landroid/widget/Button;

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setMaxWidth(I)V

    .line 1321
    iget-object v2, p0, Lgbz;->aH:Landroid/widget/Button;

    const/high16 v3, -0x80000000

    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {v2, v0, v9}, Landroid/widget/Button;->measure(II)V

    .line 1323
    iget-object v0, p0, Lgbz;->aH:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    int-to-float v2, v2

    const/high16 v3, 0x3f000000    # 0.5f

    mul-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    add-int/lit8 v2, v2, 0x2

    iget-object v3, p0, Lgbz;->aH:Landroid/widget/Button;

    invoke-virtual {v3}, Landroid/widget/Button;->getPaint()Landroid/text/TextPaint;

    move-result-object v3

    sget-object v4, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    const/4 v5, 0x0

    invoke-static {v1, v3, v0, v4, v5}, Llib;->a(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/TextUtils$TruncateAt;Landroid/text/TextUtils$EllipsizeCallback;)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "\u2026"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lt v0, v2, :cond_6

    :cond_2
    const/4 v0, 0x1

    :goto_2
    if-eqz v0, :cond_3

    .line 1324
    iget-object v0, p0, Lgbz;->aH:Landroid/widget/Button;

    invoke-virtual {p0, v0}, Lgbz;->addView(Landroid/view/View;)V

    .line 1339
    :cond_3
    :goto_3
    invoke-static {v7}, Lhss;->c(Landroid/content/Context;)I

    move-result v0

    .line 1340
    iget-object v1, p0, Lgbz;->L:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getLineCount()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_9

    .line 1341
    iget-object v1, p0, Lgbz;->L:Landroid/text/StaticLayout;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/text/StaticLayout;->getLineBaseline(I)I

    move-result v1

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lgbz;->E:I

    .line 1347
    :goto_4
    invoke-direct {p0}, Lgbz;->G()Z

    move-result v0

    if-eqz v0, :cond_a

    iget v0, p0, Lgbz;->E:I

    iget-object v1, p0, Lgbz;->M:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    .line 1348
    invoke-virtual {v1}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 1350
    :goto_5
    iget v1, p0, Lgbz;->E:I

    iget-object v2, p0, Lgbz;->L:Landroid/text/StaticLayout;

    iget-object v3, p0, Lgbz;->L:Landroid/text/StaticLayout;

    .line 1351
    invoke-virtual {v3}, Landroid/text/StaticLayout;->getLineCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Landroid/text/StaticLayout;->getLineBaseline(I)I

    move-result v2

    add-int/2addr v1, v2

    sget-object v2, Lgbz;->z:Llct;

    iget v2, v2, Llct;->l:I

    add-int/2addr v1, v2

    .line 1353
    iget-object v2, p0, Lgbz;->P:Llir;

    invoke-virtual {v2}, Llir;->getHeight()I

    move-result v2

    add-int/2addr v2, v1

    .line 1354
    invoke-static {v8, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 1355
    add-int v0, v10, v8

    sget-object v3, Lgbz;->z:Llct;

    iget v3, v3, Llct;->m:I

    add-int/2addr v0, v3

    .line 1356
    iget-boolean v3, p0, Lgbz;->g:Z

    if-eqz v3, :cond_4

    .line 1357
    sget-object v3, Lgbz;->z:Llct;

    iget-object v3, v3, Llct;->z:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    sget-object v4, Lgbz;->z:Llct;

    iget v4, v4, Llct;->k:I

    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 1359
    :cond_4
    iget-object v3, p0, Lgbz;->P:Llir;

    add-int/2addr v1, p2

    invoke-virtual {v3, v0, v1}, Llir;->a(II)V

    .line 1360
    iget-object v0, p0, Lgbz;->P:Llir;

    invoke-virtual {p0, v0}, Lgbz;->a(Llip;)V

    .line 1362
    sget-object v0, Lgbz;->z:Llct;

    iget v0, v0, Llct;->m:I

    add-int/2addr v0, v2

    add-int/2addr v0, p2

    .line 1364
    return v0

    .line 1290
    :cond_5
    iget-object v0, p0, Lgbz;->d:Ljava/lang/String;

    invoke-direct {p0, v0}, Lgbz;->a(Ljava/lang/String;)Landroid/text/Layout$Alignment;

    move-result-object v0

    .line 1291
    const/16 v1, 0x10

    .line 1292
    invoke-static {v7, v1}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v1

    iget-object v2, p0, Lgbz;->d:Ljava/lang/String;

    const/4 v3, 0x1

    .line 1291
    invoke-static {v1, v2, v11, v3, v0}, Llib;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;IILandroid/text/Layout$Alignment;)Landroid/text/StaticLayout;

    move-result-object v0

    iput-object v0, p0, Lgbz;->L:Landroid/text/StaticLayout;

    goto/16 :goto_0

    .line 1323
    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 1326
    :cond_7
    iget-object v1, p0, Lgbz;->aG:Llee;

    if-eqz v1, :cond_3

    .line 1327
    iget-object v1, p0, Lgbz;->aG:Llee;

    iget-object v2, p0, Lgbz;->aI:Lhua;

    invoke-virtual {p0}, Lgbz;->z()Z

    move-result v3

    invoke-virtual {v1, v2, v0, p0, v3}, Llee;->a(Lhua;ILandroid/view/View$OnClickListener;Z)V

    .line 1328
    iget-object v0, p0, Lgbz;->aG:Llee;

    invoke-virtual {v0, v9, v9}, Llee;->measure(II)V

    .line 1329
    iget-object v0, p0, Lgbz;->aG:Llee;

    invoke-virtual {v0}, Llee;->b()I

    move-result v0

    if-lez v0, :cond_8

    .line 1330
    iget-object v0, p0, Lgbz;->aG:Llee;

    invoke-virtual {p0, v0}, Lgbz;->addView(Landroid/view/View;)V

    goto/16 :goto_3

    .line 1332
    :cond_8
    iget-object v0, p0, Lgbz;->aG:Llee;

    invoke-virtual {v0}, Llee;->a()V

    goto/16 :goto_3

    .line 1343
    :cond_9
    iget-object v1, p0, Lgbz;->L:Landroid/text/StaticLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/text/StaticLayout;->getLineBaseline(I)I

    move-result v1

    iget-object v2, p0, Lgbz;->P:Llir;

    .line 1344
    invoke-virtual {v2}, Llir;->getHeight()I

    move-result v2

    add-int/2addr v1, v2

    sget-object v2, Lgbz;->z:Llct;

    iget v2, v2, Llct;->l:I

    add-int/2addr v1, v2

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lgbz;->E:I

    goto/16 :goto_4

    .line 1348
    :cond_a
    const/4 v0, 0x0

    goto/16 :goto_5

    :cond_b
    move v6, v0

    goto/16 :goto_1
.end method

.method protected f(III)I
    .locals 12

    .prologue
    const/16 v10, 0x19

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    .line 1413
    invoke-virtual {p0}, Lgbz;->getContext()Landroid/content/Context;

    move-result-object v9

    .line 1414
    iget-object v0, p0, Lgbz;->Q:Landroid/text/Spanned;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1415
    new-instance v0, Llir;

    iget-object v1, p0, Lgbz;->Q:Landroid/text/Spanned;

    .line 1416
    invoke-static {v9, v10}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v2

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    move v3, p3

    move-object v8, p0

    invoke-direct/range {v0 .. v8}, Llir;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLljv;)V

    iput-object v0, p0, Lgbz;->R:Llir;

    .line 1418
    iget-object v0, p0, Lgbz;->R:Llir;

    invoke-virtual {v0, p1, p2}, Llir;->a(II)V

    .line 1419
    iget-object v0, p0, Lgbz;->R:Llir;

    invoke-virtual {p0, v0}, Lgbz;->a(Llip;)V

    .line 1420
    iget-object v0, p0, Lgbz;->R:Llir;

    invoke-virtual {v0}, Llir;->getHeight()I

    move-result v0

    add-int/2addr p2, v0

    .line 1423
    :cond_0
    invoke-static {v9, v10}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v2

    .line 1426
    iget-object v0, p0, Lgbz;->T:Landroid/text/SpannableStringBuilder;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1427
    iget-object v0, p0, Lgbz;->R:Llir;

    if-eqz v0, :cond_1

    .line 1428
    sget-object v0, Lgbz;->z:Llct;

    iget v0, v0, Llct;->m:I

    add-int/2addr p2, v0

    .line 1430
    :cond_1
    sget-object v0, Lgbz;->z:Llct;

    iget v0, v0, Llct;->m:I

    add-int v9, p2, v0

    .line 1431
    new-instance v0, Llir;

    iget-object v1, p0, Lgbz;->T:Landroid/text/SpannableStringBuilder;

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    move v3, p3

    move-object v8, p0

    invoke-direct/range {v0 .. v8}, Llir;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLljv;)V

    iput-object v0, p0, Lgbz;->U:Llir;

    .line 1433
    iget-object v0, p0, Lgbz;->U:Llir;

    invoke-virtual {v0, p1, v9}, Llir;->a(II)V

    .line 1434
    iget-object v0, p0, Lgbz;->U:Llir;

    invoke-virtual {p0, v0}, Lgbz;->a(Llip;)V

    .line 1435
    iget-object v0, p0, Lgbz;->U:Llir;

    invoke-virtual {v0}, Llir;->getHeight()I

    move-result v0

    add-int p2, v9, v0

    .line 1438
    :cond_2
    iget-object v0, p0, Lgbz;->V:Landroid/text/SpannableStringBuilder;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1439
    iget-object v0, p0, Lgbz;->R:Llir;

    if-nez v0, :cond_3

    iget-object v0, p0, Lgbz;->U:Llir;

    if-eqz v0, :cond_4

    .line 1440
    :cond_3
    sget-object v0, Lgbz;->z:Llct;

    iget v0, v0, Llct;->l:I

    add-int/2addr p2, v0

    .line 1443
    :cond_4
    iget v0, p0, Lgbz;->w:I

    if-nez v0, :cond_9

    .line 1444
    invoke-virtual {p0}, Lgbz;->aD_()Z

    move-result v0

    if-eqz v0, :cond_8

    sget-object v0, Lgbz;->z:Llct;

    iget v9, v0, Llct;->T:I

    .line 1447
    :goto_0
    iget-object v0, p0, Lgbz;->V:Landroid/text/SpannableStringBuilder;

    const/16 v1, 0x3e8

    iget-object v3, p0, Lgbz;->V:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-virtual {v0, v7, v1}, Landroid/text/SpannableStringBuilder;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v7

    move-object v6, v2

    move v8, p3

    move v10, v5

    move-object v11, p0

    .line 1449
    invoke-static/range {v6 .. v11}, Llir;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;IIFLljv;)Llir;

    move-result-object v0

    iput-object v0, p0, Lgbz;->W:Llir;

    .line 1456
    :goto_1
    iget-object v0, p0, Lgbz;->W:Llir;

    invoke-virtual {v0, p1, p2}, Llir;->a(II)V

    .line 1457
    iget-object v0, p0, Lgbz;->W:Llir;

    invoke-virtual {p0, v0}, Lgbz;->a(Llip;)V

    .line 1458
    iget-object v0, p0, Lgbz;->W:Llir;

    invoke-virtual {v0}, Llir;->getHeight()I

    move-result v0

    add-int/2addr p2, v0

    .line 1461
    :cond_5
    iget-object v0, p0, Lgbz;->R:Llir;

    if-nez v0, :cond_6

    iget-object v0, p0, Lgbz;->U:Llir;

    if-nez v0, :cond_6

    iget-object v0, p0, Lgbz;->W:Llir;

    if-eqz v0, :cond_7

    .line 1462
    :cond_6
    sget-object v0, Lgbz;->z:Llct;

    iget v0, v0, Llct;->m:I

    add-int/2addr p2, v0

    .line 1465
    :cond_7
    return p2

    .line 1444
    :cond_8
    sget-object v0, Lgbz;->z:Llct;

    iget v9, v0, Llct;->U:I

    goto :goto_0

    .line 1452
    :cond_9
    new-instance v0, Llir;

    iget-object v1, p0, Lgbz;->V:Landroid/text/SpannableStringBuilder;

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    move v3, p3

    move-object v8, p0

    invoke-direct/range {v0 .. v8}, Llir;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLljv;)V

    iput-object v0, p0, Lgbz;->W:Llir;

    goto :goto_1
.end method

.method protected f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 582
    iget-object v0, p0, Lgbz;->F:Llae;

    if-eqz v0, :cond_0

    .line 583
    iget-object v0, p0, Lgbz;->F:Llae;

    invoke-virtual {p0}, Lgbz;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Llae;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 585
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected g(III)I
    .locals 10

    .prologue
    .line 1540
    iget-object v0, p0, Lgbz;->G:Landroid/text/Spannable;

    if-eqz v0, :cond_0

    .line 1541
    sget-object v0, Lgbz;->z:Llct;

    iget v0, v0, Llct;->m:I

    add-int v9, p2, v0

    .line 1542
    sget-object v0, Lgbz;->z:Llct;

    iget-object v0, v0, Llct;->p:[Landroid/graphics/Bitmap;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    iput-object v0, p0, Lgbz;->I:Landroid/graphics/Bitmap;

    .line 1543
    new-instance v0, Llir;

    iget-object v1, p0, Lgbz;->G:Landroid/text/Spannable;

    .line 1544
    invoke-virtual {p0}, Lgbz;->getContext()Landroid/content/Context;

    move-result-object v2

    const/16 v3, 0xa

    invoke-static {v2, v3}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v2

    iget-object v3, p0, Lgbz;->I:Landroid/graphics/Bitmap;

    .line 1546
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    sub-int v3, p3, v3

    iget-object v4, p0, Lgbz;->G:Landroid/text/Spannable;

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lgbz;->a(Ljava/lang/String;)Landroid/text/Layout$Alignment;

    move-result-object v4

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v8, p0

    invoke-direct/range {v0 .. v8}, Llir;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLljv;)V

    iput-object v0, p0, Lgbz;->H:Llir;

    .line 1548
    iget-object v0, p0, Lgbz;->H:Llir;

    invoke-virtual {v0, p1, v9}, Llir;->a(II)V

    .line 1549
    iget-object v0, p0, Lgbz;->H:Llir;

    invoke-virtual {p0, v0}, Lgbz;->a(Llip;)V

    .line 1550
    iget-object v0, p0, Lgbz;->H:Llir;

    invoke-virtual {v0}, Llir;->getHeight()I

    move-result v0

    sget-object v1, Lgbz;->z:Llct;

    iget v1, v1, Llct;->m:I

    add-int/2addr v0, v1

    add-int p2, v9, v0

    .line 1553
    :cond_0
    return p2
.end method

.method protected g()V
    .locals 2

    .prologue
    .line 1911
    iget-object v0, p0, Lgbz;->i:Lfdp;

    if-eqz v0, :cond_0

    .line 1912
    iget-object v0, p0, Lgbz;->i:Lfdp;

    iget-object v1, p0, Lgbz;->r:Ljava/lang/String;

    iget-object v1, p0, Lgbz;->F:Llae;

    invoke-virtual {v0, v1}, Lfdp;->a(Llae;)V

    .line 1914
    :cond_0
    return-void
.end method

.method public getContentDescription()Ljava/lang/CharSequence;
    .locals 8
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "accessibility"
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 600
    invoke-virtual {p0}, Lgbz;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 601
    invoke-static {}, Llsu;->a()Ljava/lang/StringBuilder;

    move-result-object v3

    .line 602
    new-array v0, v7, [Ljava/lang/CharSequence;

    iget-object v4, p0, Lgbz;->aa:Ljava/lang/String;

    aput-object v4, v0, v1

    invoke-static {v3, v0}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 603
    new-array v0, v7, [Ljava/lang/CharSequence;

    iget-object v4, p0, Lgbz;->ac:Ljava/lang/String;

    aput-object v4, v0, v1

    invoke-static {v3, v0}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 604
    iget-object v0, p0, Lgbz;->aC:Lhvp;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbz;->ay:Lhvk;

    invoke-virtual {v0}, Lhvk;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-ne v0, p0, :cond_0

    .line 605
    new-array v0, v7, [Ljava/lang/CharSequence;

    iget-object v4, p0, Lgbz;->aC:Lhvp;

    .line 606
    invoke-virtual {v4}, Lhvp;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v4

    aput-object v4, v0, v1

    .line 605
    invoke-static {v3, v0}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 608
    :cond_0
    new-array v4, v7, [Ljava/lang/CharSequence;

    iget-object v0, p0, Lgbz;->J:Landroid/text/Spanned;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lgbz;->J:Landroid/text/Spanned;

    :goto_0
    aput-object v0, v4, v1

    invoke-static {v3, v4}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 610
    new-array v0, v7, [Ljava/lang/CharSequence;

    iget-object v4, p0, Lgbz;->O:Landroid/text/SpannableStringBuilder;

    aput-object v4, v0, v1

    invoke-static {v3, v0}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 611
    new-array v0, v7, [Ljava/lang/CharSequence;

    iget-object v4, p0, Lgbz;->Q:Landroid/text/Spanned;

    aput-object v4, v0, v1

    invoke-static {v3, v0}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 612
    new-array v0, v7, [Ljava/lang/CharSequence;

    iget-object v4, p0, Lgbz;->T:Landroid/text/SpannableStringBuilder;

    aput-object v4, v0, v1

    invoke-static {v3, v0}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 613
    new-array v0, v7, [Ljava/lang/CharSequence;

    iget-object v4, p0, Lgbz;->V:Landroid/text/SpannableStringBuilder;

    aput-object v4, v0, v1

    invoke-static {v3, v0}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 614
    new-array v0, v7, [Ljava/lang/CharSequence;

    iget-object v4, p0, Lgbz;->G:Landroid/text/Spannable;

    aput-object v4, v0, v1

    invoke-static {v3, v0}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 616
    invoke-virtual {p0, v3}, Lgbz;->a(Ljava/lang/StringBuilder;)V

    .line 618
    invoke-virtual {p0}, Lgbz;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 619
    const v0, 0x7f110023

    iget v4, p0, Lgbz;->af:I

    new-array v5, v7, [Ljava/lang/Object;

    iget v6, p0, Lgbz;->af:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-virtual {v2, v0, v4, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ". "

    .line 620
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 623
    :cond_1
    iget-object v0, p0, Lgbz;->ag:Llah;

    if-nez v0, :cond_4

    move v0, v1

    .line 624
    :goto_1
    if-lez v0, :cond_2

    .line 625
    const v4, 0x7f110022

    new-array v5, v7, [Ljava/lang/Object;

    .line 626
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    .line 625
    invoke-virtual {v2, v4, v0, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 629
    :cond_2
    invoke-static {v3}, Llsu;->a(Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 608
    :cond_3
    iget-object v0, p0, Lgbz;->d:Ljava/lang/String;

    goto :goto_0

    .line 623
    :cond_4
    iget-object v0, p0, Lgbz;->ag:Llah;

    invoke-virtual {v0}, Llah;->b()I

    move-result v0

    goto :goto_1
.end method

.method public h()V
    .locals 4

    .prologue
    .line 296
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgbz;->av:Z

    .line 297
    iget-object v0, p0, Lgbz;->q:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Lgbz;->q:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget v2, p0, Lgbz;->m:I

    iget-object v3, p0, Lgbz;->q:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v3

    iget-object v3, p0, Lgbz;->q:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    sub-int/2addr v2, v3

    invoke-virtual {p0, v0, v1, v2}, Lgbz;->d(III)I

    .line 299
    invoke-virtual {p0}, Lgbz;->invalidate()V

    .line 300
    return-void
.end method

.method public i()V
    .locals 1

    .prologue
    .line 303
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgbz;->am:Z

    .line 304
    return-void
.end method

.method public j()V
    .locals 1

    .prologue
    .line 307
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgbz;->am:Z

    .line 308
    return-void
.end method

.method public k()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 311
    iget-boolean v0, p0, Lgbz;->am:Z

    if-eqz v0, :cond_1

    .line 312
    iget-object v0, p0, Lgbz;->M:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->setVisibility(I)V

    .line 313
    iget-object v0, p0, Lgbz;->N:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->setVisibility(I)V

    .line 319
    :goto_0
    iget-object v0, p0, Lgbz;->aE:Lkzp;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbz;->aE:Lkzp;

    invoke-virtual {v0}, Lkzp;->n()Llac;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 320
    iget-object v0, p0, Lgbz;->q:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget v0, p0, Lgbz;->t:I

    invoke-virtual {p0, v1}, Lgbz;->c(I)I

    .line 321
    invoke-virtual {p0}, Lgbz;->invalidate()V

    .line 323
    :cond_0
    return-void

    .line 315
    :cond_1
    iget-object v0, p0, Lgbz;->M:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->setVisibility(I)V

    .line 316
    iget-object v0, p0, Lgbz;->N:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->setVisibility(I)V

    goto :goto_0
.end method

.method protected l()Z
    .locals 1

    .prologue
    .line 813
    iget-object v0, p0, Lgbz;->r:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public m()V
    .locals 4

    .prologue
    .line 1624
    iget-object v0, p0, Lgbz;->aA:Lgbh;

    if-nez v0, :cond_0

    .line 1625
    new-instance v0, Lgbh;

    invoke-virtual {p0}, Lgbz;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lgbh;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lgbz;->aA:Lgbh;

    .line 1626
    iget-object v0, p0, Lgbz;->aA:Lgbh;

    invoke-virtual {p0}, Lgbz;->q()I

    move-result v1

    iget-object v2, p0, Lgbz;->b:Ljava/lang/String;

    iget-object v3, p0, Lgbz;->r:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lgbh;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 1627
    iget-object v0, p0, Lgbz;->aA:Lgbh;

    invoke-virtual {p0, v0}, Lgbz;->addView(Landroid/view/View;)V

    .line 1629
    :cond_0
    return-void
.end method

.method public n()Z
    .locals 1

    .prologue
    .line 1691
    iget v0, p0, Lgbz;->af:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected o()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 1696
    iget v0, p0, Lgbz;->w:I

    if-nez v0, :cond_0

    sget-object v0, Lgbz;->z:Llct;

    iget-object v0, v0, Llct;->A:Landroid/graphics/Rect;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lgbz;->z:Llct;

    iget-object v0, v0, Llct;->B:Landroid/graphics/Rect;

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v0, -0x2

    const/4 v4, -0x3

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 1710
    iget-object v2, p0, Lgbz;->aG:Llee;

    if-eq p1, v2, :cond_0

    iget-object v2, p0, Lgbz;->aH:Landroid/widget/Button;

    if-ne p1, v2, :cond_3

    .line 1711
    :cond_0
    invoke-virtual {p0}, Lgbz;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v2, Lhtg;

    invoke-static {v0, v2}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhtg;

    .line 1714
    if-eqz v0, :cond_2

    .line 1715
    iget-object v2, p0, Lgbz;->r:Ljava/lang/String;

    iget-object v3, p0, Lgbz;->aI:Lhua;

    iget-object v4, p0, Lgbz;->aG:Llee;

    if-ne p1, v4, :cond_1

    iget-object v1, p0, Lgbz;->aG:Llee;

    .line 1716
    invoke-virtual {v1}, Llee;->c()I

    move-result v1

    .line 1715
    :cond_1
    invoke-interface {v0, v2, v3, v1}, Lhtg;->a(Ljava/lang/String;Lhua;I)V

    .line 1756
    :cond_2
    :goto_0
    return-void

    .line 1718
    :cond_3
    iget-object v2, p0, Lgbz;->K:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    if-ne p1, v2, :cond_4

    .line 1719
    invoke-virtual {p0}, Lgbz;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lhsn;

    invoke-static {v0, v1}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhsn;

    .line 1721
    if-eqz v0, :cond_2

    .line 1722
    invoke-virtual {p0}, Lgbz;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lhkr;

    invoke-static {v1, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhkr;

    iget-object v2, p0, Lgbz;->K:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v1, v2}, Lhkr;->a(Landroid/view/View;)V

    .line 1723
    iget-object v1, p0, Lgbz;->c:Ljava/lang/String;

    invoke-interface {v0, v1}, Lhsn;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 1725
    :cond_4
    iget-object v2, p0, Lgbz;->M:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    if-ne p1, v2, :cond_8

    .line 1726
    iget-object v2, p0, Lgbz;->i:Lfdp;

    if-eqz v2, :cond_2

    .line 1727
    iget-object v2, p0, Lgbz;->aE:Lkzp;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lgbz;->aE:Lkzp;

    .line 1728
    invoke-virtual {v2}, Lkzp;->n()Llac;

    move-result-object v2

    if-eqz v2, :cond_5

    move v2, v6

    .line 1729
    :goto_1
    if-eqz v2, :cond_6

    .line 1732
    :goto_2
    iget-boolean v0, p0, Lgbz;->ap:Z

    if-nez v0, :cond_7

    .line 1733
    :goto_3
    iget-object v0, p0, Lgbz;->i:Lfdp;

    iget-object v1, p0, Lgbz;->c:Ljava/lang/String;

    iget-object v2, p0, Lgbz;->d:Ljava/lang/String;

    iget-object v3, p0, Lgbz;->r:Ljava/lang/String;

    iget-boolean v5, p0, Lgbz;->am:Z

    invoke-virtual/range {v0 .. v6}, Lfdp;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZZ)V

    goto :goto_0

    :cond_5
    move v2, v1

    .line 1728
    goto :goto_1

    :cond_6
    move v4, v0

    .line 1729
    goto :goto_2

    :cond_7
    move v6, v1

    .line 1732
    goto :goto_3

    .line 1736
    :cond_8
    iget-object v2, p0, Lgbz;->N:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    if-ne p1, v2, :cond_b

    .line 1737
    iget-object v2, p0, Lgbz;->i:Lfdp;

    if-eqz v2, :cond_2

    .line 1738
    iget-object v2, p0, Lgbz;->aE:Lkzp;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lgbz;->aE:Lkzp;

    .line 1739
    invoke-virtual {v2}, Lkzp;->n()Llac;

    move-result-object v2

    if-eqz v2, :cond_9

    .line 1740
    :goto_4
    if-eqz v6, :cond_a

    .line 1743
    :goto_5
    iget-object v0, p0, Lgbz;->i:Lfdp;

    iget-object v1, p0, Lgbz;->c:Ljava/lang/String;

    iget-object v2, p0, Lgbz;->d:Ljava/lang/String;

    iget-object v3, p0, Lgbz;->r:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3, v4}, Lfdp;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    :cond_9
    move v6, v1

    .line 1739
    goto :goto_4

    :cond_a
    move v4, v0

    .line 1740
    goto :goto_5

    .line 1746
    :cond_b
    iget-object v0, p0, Lgbz;->aq:Landroid/widget/TextView;

    if-ne p1, v0, :cond_c

    .line 1747
    iget-object v0, p0, Lgbz;->i:Lfdp;

    if-eqz v0, :cond_2

    .line 1748
    iget-object v0, p0, Lgbz;->i:Lfdp;

    iget-object v1, p0, Lgbz;->as:Ljava/lang/String;

    iget-object v2, p0, Lgbz;->r:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lfdp;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1750
    :cond_c
    instance-of v0, p1, Lgbz;

    if-eqz v0, :cond_2

    .line 1751
    invoke-virtual {p0}, Lgbz;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lhtf;

    invoke-static {v0, v1}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhtf;

    .line 1752
    if-eqz v0, :cond_2

    .line 1753
    invoke-interface {v0, p0, p0}, Lhtf;->a(Landroid/view/View;Lhtx;)V

    goto/16 :goto_0
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 5

    .prologue
    .line 1760
    iget-object v0, p0, Lgbz;->M:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lgbz;->N:Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    if-ne p1, v0, :cond_1

    :cond_0
    iget-object v0, p0, Lgbz;->i:Lfdp;

    if-eqz v0, :cond_1

    .line 1762
    iget-object v0, p0, Lgbz;->i:Lfdp;

    iget-object v1, p0, Lgbz;->c:Ljava/lang/String;

    iget-object v2, p0, Lgbz;->d:Ljava/lang/String;

    iget-object v3, p0, Lgbz;->r:Ljava/lang/String;

    const/4 v4, -0x2

    invoke-virtual {v0, v1, v2, v3, v4}, Lfdp;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 1764
    const/4 v0, 0x1

    .line 1766
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1701
    iget-object v0, p0, Lgbz;->c:Ljava/lang/String;

    return-object v0
.end method

.method protected q()I
    .locals 2

    .prologue
    .line 1832
    invoke-virtual {p0}, Lgbz;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lhee;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    .line 1833
    return v0
.end method

.method public r()V
    .locals 2

    .prologue
    .line 1882
    .line 1883
    invoke-virtual {p0}, Lgbz;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lhtr;

    invoke-static {v0, v1}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhtr;

    .line 1884
    if-eqz v0, :cond_0

    .line 1885
    invoke-virtual {p0}, Lgbz;->n()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-interface {v0, p0, p0, v1}, Lhtr;->a(Landroid/view/View;Lhtx;Z)V

    .line 1887
    :cond_0
    return-void

    .line 1885
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public s()Lkzp;
    .locals 1

    .prologue
    .line 1931
    iget-object v0, p0, Lgbz;->aE:Lkzp;

    return-object v0
.end method
