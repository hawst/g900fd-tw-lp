.class public final Lnvs;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Ljava/lang/String;

.field public b:Llwr;

.field public c:Ljava/lang/Boolean;

.field private d:Logy;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2305
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2308
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lnvs;->a:[Ljava/lang/String;

    .line 2311
    iput-object v1, p0, Lnvs;->b:Llwr;

    .line 2316
    iput-object v1, p0, Lnvs;->d:Logy;

    .line 2305
    return-void
.end method


# virtual methods
.method public a()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2341
    .line 2342
    iget-object v1, p0, Lnvs;->a:[Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lnvs;->a:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_1

    .line 2344
    iget-object v2, p0, Lnvs;->a:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 2346
    invoke-static {v4}, Loxo;->b(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v1, v4

    .line 2344
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2349
    :cond_0
    iget-object v0, p0, Lnvs;->a:[Ljava/lang/String;

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v1

    .line 2351
    :cond_1
    iget-object v1, p0, Lnvs;->b:Llwr;

    if-eqz v1, :cond_2

    .line 2352
    const/4 v1, 0x2

    iget-object v2, p0, Lnvs;->b:Llwr;

    .line 2353
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2355
    :cond_2
    iget-object v1, p0, Lnvs;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 2356
    const/4 v1, 0x3

    iget-object v2, p0, Lnvs;->c:Ljava/lang/Boolean;

    .line 2357
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2359
    :cond_3
    iget-object v1, p0, Lnvs;->d:Logy;

    if-eqz v1, :cond_4

    .line 2360
    const/4 v1, 0x4

    iget-object v2, p0, Lnvs;->d:Logy;

    .line 2361
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2363
    :cond_4
    iget-object v1, p0, Lnvs;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2364
    iput v0, p0, Lnvs;->ai:I

    .line 2365
    return v0
.end method

.method public a(Loxn;)Lnvs;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2373
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2374
    sparse-switch v0, :sswitch_data_0

    .line 2378
    iget-object v1, p0, Lnvs;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 2379
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnvs;->ah:Ljava/util/List;

    .line 2382
    :cond_1
    iget-object v1, p0, Lnvs;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2384
    :sswitch_0
    return-object p0

    .line 2389
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 2390
    iget-object v0, p0, Lnvs;->a:[Ljava/lang/String;

    array-length v0, v0

    .line 2391
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    .line 2392
    iget-object v2, p0, Lnvs;->a:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2393
    iput-object v1, p0, Lnvs;->a:[Ljava/lang/String;

    .line 2394
    :goto_1
    iget-object v1, p0, Lnvs;->a:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 2395
    iget-object v1, p0, Lnvs;->a:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 2396
    invoke-virtual {p1}, Loxn;->a()I

    .line 2394
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2399
    :cond_2
    iget-object v1, p0, Lnvs;->a:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    .line 2403
    :sswitch_2
    iget-object v0, p0, Lnvs;->b:Llwr;

    if-nez v0, :cond_3

    .line 2404
    new-instance v0, Llwr;

    invoke-direct {v0}, Llwr;-><init>()V

    iput-object v0, p0, Lnvs;->b:Llwr;

    .line 2406
    :cond_3
    iget-object v0, p0, Lnvs;->b:Llwr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2410
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnvs;->c:Ljava/lang/Boolean;

    goto :goto_0

    .line 2414
    :sswitch_4
    iget-object v0, p0, Lnvs;->d:Logy;

    if-nez v0, :cond_4

    .line 2415
    new-instance v0, Logy;

    invoke-direct {v0}, Logy;-><init>()V

    iput-object v0, p0, Lnvs;->d:Logy;

    .line 2417
    :cond_4
    iget-object v0, p0, Lnvs;->d:Logy;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2374
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 2321
    iget-object v0, p0, Lnvs;->a:[Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2322
    iget-object v1, p0, Lnvs;->a:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 2323
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->a(ILjava/lang/String;)V

    .line 2322
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2326
    :cond_0
    iget-object v0, p0, Lnvs;->b:Llwr;

    if-eqz v0, :cond_1

    .line 2327
    const/4 v0, 0x2

    iget-object v1, p0, Lnvs;->b:Llwr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2329
    :cond_1
    iget-object v0, p0, Lnvs;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 2330
    const/4 v0, 0x3

    iget-object v1, p0, Lnvs;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 2332
    :cond_2
    iget-object v0, p0, Lnvs;->d:Logy;

    if-eqz v0, :cond_3

    .line 2333
    const/4 v0, 0x4

    iget-object v1, p0, Lnvs;->d:Logy;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2335
    :cond_3
    iget-object v0, p0, Lnvs;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2337
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2301
    invoke-virtual {p0, p1}, Lnvs;->a(Loxn;)Lnvs;

    move-result-object v0

    return-object v0
.end method
