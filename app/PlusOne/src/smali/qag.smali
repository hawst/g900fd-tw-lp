.class public final Lqag;
.super Loxq;
.source "PG"


# instance fields
.field private a:[Ljava/lang/Long;

.field private b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 621
    invoke-direct {p0}, Loxq;-><init>()V

    .line 624
    sget-object v0, Loxx;->h:[Ljava/lang/Long;

    iput-object v0, p0, Lqag;->a:[Ljava/lang/Long;

    .line 627
    const/high16 v0, -0x80000000

    iput v0, p0, Lqag;->b:I

    .line 621
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 646
    .line 647
    iget-object v1, p0, Lqag;->a:[Ljava/lang/Long;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lqag;->a:[Ljava/lang/Long;

    array-length v1, v1

    if-lez v1, :cond_1

    .line 649
    iget-object v2, p0, Lqag;->a:[Ljava/lang/Long;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 651
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Loxo;->f(J)I

    move-result v4

    add-int/2addr v1, v4

    .line 649
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 654
    :cond_0
    iget-object v0, p0, Lqag;->a:[Ljava/lang/Long;

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v1

    .line 656
    :cond_1
    iget v1, p0, Lqag;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_2

    .line 657
    const/4 v1, 0x2

    iget v2, p0, Lqag;->b:I

    .line 658
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 660
    :cond_2
    iget-object v1, p0, Lqag;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 661
    iput v0, p0, Lqag;->ai:I

    .line 662
    return v0
.end method

.method public a(Loxn;)Lqag;
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 670
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 671
    sparse-switch v0, :sswitch_data_0

    .line 675
    iget-object v1, p0, Lqag;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 676
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lqag;->ah:Ljava/util/List;

    .line 679
    :cond_1
    iget-object v1, p0, Lqag;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 681
    :sswitch_0
    return-object p0

    .line 686
    :sswitch_1
    invoke-static {p1, v5}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 687
    iget-object v0, p0, Lqag;->a:[Ljava/lang/Long;

    array-length v0, v0

    .line 688
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/Long;

    .line 689
    iget-object v2, p0, Lqag;->a:[Ljava/lang/Long;

    invoke-static {v2, v4, v1, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 690
    iput-object v1, p0, Lqag;->a:[Ljava/lang/Long;

    .line 691
    :goto_1
    iget-object v1, p0, Lqag;->a:[Ljava/lang/Long;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 692
    iget-object v1, p0, Lqag;->a:[Ljava/lang/Long;

    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v0

    .line 693
    invoke-virtual {p1}, Loxn;->a()I

    .line 691
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 696
    :cond_2
    iget-object v1, p0, Lqag;->a:[Ljava/lang/Long;

    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    .line 700
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 701
    if-eqz v0, :cond_3

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/16 v1, 0xa6

    if-eq v0, v1, :cond_3

    const/16 v1, 0xa7

    if-eq v0, v1, :cond_3

    const/16 v1, 0x1b

    if-eq v0, v1, :cond_3

    const/16 v1, 0x1c

    if-eq v0, v1, :cond_3

    const/16 v1, 0x1d

    if-eq v0, v1, :cond_3

    const/16 v1, 0x1e

    if-eq v0, v1, :cond_3

    const/16 v1, 0x1f

    if-eq v0, v1, :cond_3

    const/16 v1, 0x20

    if-eq v0, v1, :cond_3

    const/16 v1, 0x21

    if-eq v0, v1, :cond_3

    const/16 v1, 0x22

    if-eq v0, v1, :cond_3

    const/4 v1, 0x5

    if-eq v0, v1, :cond_3

    const/4 v1, 0x6

    if-eq v0, v1, :cond_3

    const/16 v1, 0x23

    if-eq v0, v1, :cond_3

    const/16 v1, 0x10

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    const/4 v1, 0x4

    if-eq v0, v1, :cond_3

    const/4 v1, 0x7

    if-eq v0, v1, :cond_3

    const/16 v1, 0xd2

    if-eq v0, v1, :cond_3

    if-eq v0, v5, :cond_3

    const/16 v1, 0x9

    if-eq v0, v1, :cond_3

    const/16 v1, 0xa

    if-eq v0, v1, :cond_3

    const/16 v1, 0x24

    if-eq v0, v1, :cond_3

    const/16 v1, 0x25

    if-eq v0, v1, :cond_3

    const/16 v1, 0x26

    if-eq v0, v1, :cond_3

    const/16 v1, 0x27

    if-eq v0, v1, :cond_3

    const/16 v1, 0x28

    if-eq v0, v1, :cond_3

    const/16 v1, 0x29

    if-eq v0, v1, :cond_3

    const/16 v1, 0x2a

    if-eq v0, v1, :cond_3

    const/16 v1, 0x11

    if-eq v0, v1, :cond_3

    const/16 v1, 0xe1

    if-eq v0, v1, :cond_3

    const/16 v1, 0x2b

    if-eq v0, v1, :cond_3

    const/16 v1, 0x2c

    if-eq v0, v1, :cond_3

    const/16 v1, 0x2d

    if-eq v0, v1, :cond_3

    const/16 v1, 0x2e

    if-eq v0, v1, :cond_3

    const/16 v1, 0x2f

    if-eq v0, v1, :cond_3

    const/16 v1, 0x30

    if-eq v0, v1, :cond_3

    const/16 v1, 0x31

    if-eq v0, v1, :cond_3

    const/16 v1, 0x32

    if-eq v0, v1, :cond_3

    const/16 v1, 0x72

    if-eq v0, v1, :cond_3

    const/16 v1, 0x93

    if-eq v0, v1, :cond_3

    const/16 v1, 0x5d

    if-eq v0, v1, :cond_3

    const/16 v1, 0xe8

    if-eq v0, v1, :cond_3

    const/16 v1, 0x95

    if-eq v0, v1, :cond_3

    const/16 v1, 0x9f

    if-eq v0, v1, :cond_3

    const/16 v1, 0xa0

    if-eq v0, v1, :cond_3

    const/16 v1, 0x33

    if-eq v0, v1, :cond_3

    const/16 v1, 0x34

    if-eq v0, v1, :cond_3

    const/16 v1, 0xa9

    if-eq v0, v1, :cond_3

    const/16 v1, 0xaa

    if-eq v0, v1, :cond_3

    const/16 v1, 0xb6

    if-eq v0, v1, :cond_3

    const/16 v1, 0xab

    if-eq v0, v1, :cond_3

    const/16 v1, 0xb7

    if-eq v0, v1, :cond_3

    const/16 v1, 0xd8

    if-eq v0, v1, :cond_3

    const/16 v1, 0xac

    if-eq v0, v1, :cond_3

    const/16 v1, 0xb5

    if-eq v0, v1, :cond_3

    const/16 v1, 0x35

    if-eq v0, v1, :cond_3

    const/16 v1, 0x13

    if-eq v0, v1, :cond_3

    const/16 v1, 0x36

    if-eq v0, v1, :cond_3

    const/16 v1, 0xf

    if-eq v0, v1, :cond_3

    const/16 v1, 0x37

    if-eq v0, v1, :cond_3

    const/16 v1, 0x38

    if-eq v0, v1, :cond_3

    const/16 v1, 0xb8

    if-eq v0, v1, :cond_3

    const/16 v1, 0xb

    if-eq v0, v1, :cond_3

    const/16 v1, 0x39

    if-eq v0, v1, :cond_3

    const/16 v1, 0x3a

    if-eq v0, v1, :cond_3

    const/16 v1, 0xc

    if-eq v0, v1, :cond_3

    const/16 v1, 0x3b

    if-eq v0, v1, :cond_3

    const/16 v1, 0xcf

    if-eq v0, v1, :cond_3

    const/16 v1, 0xb9

    if-eq v0, v1, :cond_3

    const/16 v1, 0x3c

    if-eq v0, v1, :cond_3

    const/16 v1, 0x3d

    if-eq v0, v1, :cond_3

    const/16 v1, 0xad

    if-eq v0, v1, :cond_3

    const/16 v1, 0x3e

    if-eq v0, v1, :cond_3

    const/16 v1, 0xa2

    if-eq v0, v1, :cond_3

    const/16 v1, 0xa3

    if-eq v0, v1, :cond_3

    const/16 v1, 0xa4

    if-eq v0, v1, :cond_3

    const/16 v1, 0xa5

    if-eq v0, v1, :cond_3

    const/16 v1, 0x3f

    if-eq v0, v1, :cond_3

    const/16 v1, 0x40

    if-eq v0, v1, :cond_3

    const/16 v1, 0x41

    if-eq v0, v1, :cond_3

    const/16 v1, 0x42

    if-eq v0, v1, :cond_3

    const/16 v1, 0x43

    if-eq v0, v1, :cond_3

    const/16 v1, 0x44

    if-eq v0, v1, :cond_3

    const/16 v1, 0x45

    if-eq v0, v1, :cond_3

    const/16 v1, 0x46

    if-eq v0, v1, :cond_3

    const/16 v1, 0xae

    if-eq v0, v1, :cond_3

    const/16 v1, 0x47

    if-eq v0, v1, :cond_3

    const/16 v1, 0x48

    if-eq v0, v1, :cond_3

    const/16 v1, 0xc2

    if-eq v0, v1, :cond_3

    const/16 v1, 0xe7

    if-eq v0, v1, :cond_3

    const/16 v1, 0xc3

    if-eq v0, v1, :cond_3

    const/16 v1, 0xd3

    if-eq v0, v1, :cond_3

    const/16 v1, 0xd4

    if-eq v0, v1, :cond_3

    const/16 v1, 0x4e

    if-eq v0, v1, :cond_3

    const/16 v1, 0x4f

    if-eq v0, v1, :cond_3

    const/16 v1, 0x50

    if-eq v0, v1, :cond_3

    const/16 v1, 0x51

    if-eq v0, v1, :cond_3

    const/16 v1, 0x52

    if-eq v0, v1, :cond_3

    const/16 v1, 0x53

    if-eq v0, v1, :cond_3

    const/16 v1, 0x54

    if-eq v0, v1, :cond_3

    const/16 v1, 0x55

    if-eq v0, v1, :cond_3

    const/16 v1, 0x56

    if-eq v0, v1, :cond_3

    const/16 v1, 0x57

    if-eq v0, v1, :cond_3

    const/16 v1, 0x58

    if-eq v0, v1, :cond_3

    const/16 v1, 0xba

    if-eq v0, v1, :cond_3

    const/16 v1, 0xbb

    if-eq v0, v1, :cond_3

    const/16 v1, 0xbc

    if-eq v0, v1, :cond_3

    const/16 v1, 0x59

    if-eq v0, v1, :cond_3

    const/16 v1, 0xaf

    if-eq v0, v1, :cond_3

    const/16 v1, 0x5a

    if-eq v0, v1, :cond_3

    const/16 v1, 0x5b

    if-eq v0, v1, :cond_3

    const/16 v1, 0x5c

    if-eq v0, v1, :cond_3

    const/16 v1, 0x5e

    if-eq v0, v1, :cond_3

    const/16 v1, 0x5f

    if-eq v0, v1, :cond_3

    const/16 v1, 0x60

    if-eq v0, v1, :cond_3

    const/16 v1, 0x61

    if-eq v0, v1, :cond_3

    const/16 v1, 0x62

    if-eq v0, v1, :cond_3

    const/16 v1, 0x12

    if-eq v0, v1, :cond_3

    const/16 v1, 0x15

    if-eq v0, v1, :cond_3

    const/16 v1, 0x63

    if-eq v0, v1, :cond_3

    const/16 v1, 0x64

    if-eq v0, v1, :cond_3

    const/16 v1, 0x65

    if-eq v0, v1, :cond_3

    const/16 v1, 0x66

    if-eq v0, v1, :cond_3

    const/16 v1, 0x67

    if-eq v0, v1, :cond_3

    const/16 v1, 0xbd

    if-eq v0, v1, :cond_3

    const/16 v1, 0xb0

    if-eq v0, v1, :cond_3

    const/16 v1, 0x68

    if-eq v0, v1, :cond_3

    const/16 v1, 0x69

    if-eq v0, v1, :cond_3

    const/16 v1, 0xbf

    if-eq v0, v1, :cond_3

    const/16 v1, 0x6a

    if-eq v0, v1, :cond_3

    const/16 v1, 0x6b

    if-eq v0, v1, :cond_3

    const/16 v1, 0x6c

    if-eq v0, v1, :cond_3

    const/16 v1, 0xb1

    if-eq v0, v1, :cond_3

    const/16 v1, 0x6d

    if-eq v0, v1, :cond_3

    const/16 v1, 0x6e

    if-eq v0, v1, :cond_3

    const/16 v1, 0x6f

    if-eq v0, v1, :cond_3

    const/16 v1, 0x70

    if-eq v0, v1, :cond_3

    const/16 v1, 0x71

    if-eq v0, v1, :cond_3

    const/16 v1, 0x9b

    if-eq v0, v1, :cond_3

    const/16 v1, 0xc0

    if-eq v0, v1, :cond_3

    const/16 v1, 0xe6

    if-eq v0, v1, :cond_3

    const/16 v1, 0x73

    if-eq v0, v1, :cond_3

    const/16 v1, 0x74

    if-eq v0, v1, :cond_3

    const/16 v1, 0x75

    if-eq v0, v1, :cond_3

    const/16 v1, 0x76

    if-eq v0, v1, :cond_3

    const/16 v1, 0x77

    if-eq v0, v1, :cond_3

    const/16 v1, 0x78

    if-eq v0, v1, :cond_3

    const/16 v1, 0x79

    if-eq v0, v1, :cond_3

    const/16 v1, 0x7a

    if-eq v0, v1, :cond_3

    const/16 v1, 0x7b

    if-eq v0, v1, :cond_3

    const/16 v1, 0x7c

    if-eq v0, v1, :cond_3

    const/16 v1, 0x7d

    if-eq v0, v1, :cond_3

    const/16 v1, 0x7e

    if-eq v0, v1, :cond_3

    const/16 v1, 0x49

    if-eq v0, v1, :cond_3

    const/16 v1, 0x4a

    if-eq v0, v1, :cond_3

    const/16 v1, 0x4b

    if-eq v0, v1, :cond_3

    const/16 v1, 0x4c

    if-eq v0, v1, :cond_3

    const/16 v1, 0x4d

    if-eq v0, v1, :cond_3

    const/16 v1, 0x91

    if-eq v0, v1, :cond_3

    const/16 v1, 0xa1

    if-eq v0, v1, :cond_3

    const/16 v1, 0xd

    if-eq v0, v1, :cond_3

    const/16 v1, 0xe

    if-eq v0, v1, :cond_3

    const/16 v1, 0x7f

    if-eq v0, v1, :cond_3

    const/16 v1, 0x14

    if-eq v0, v1, :cond_3

    const/16 v1, 0x80

    if-eq v0, v1, :cond_3

    const/16 v1, 0x81

    if-eq v0, v1, :cond_3

    const/16 v1, 0xd0

    if-eq v0, v1, :cond_3

    const/16 v1, 0x82

    if-eq v0, v1, :cond_3

    const/16 v1, 0x83

    if-eq v0, v1, :cond_3

    const/16 v1, 0x84

    if-eq v0, v1, :cond_3

    const/16 v1, 0x85

    if-eq v0, v1, :cond_3

    const/16 v1, 0x86

    if-eq v0, v1, :cond_3

    const/16 v1, 0x87

    if-eq v0, v1, :cond_3

    const/16 v1, 0x88

    if-eq v0, v1, :cond_3

    const/16 v1, 0x89

    if-eq v0, v1, :cond_3

    const/16 v1, 0x8a

    if-eq v0, v1, :cond_3

    const/16 v1, 0x8b

    if-eq v0, v1, :cond_3

    const/16 v1, 0x8c

    if-eq v0, v1, :cond_3

    const/16 v1, 0x8d

    if-eq v0, v1, :cond_3

    const/16 v1, 0x8e

    if-eq v0, v1, :cond_3

    const/16 v1, 0x8f

    if-eq v0, v1, :cond_3

    const/16 v1, 0x90

    if-eq v0, v1, :cond_3

    const/16 v1, 0xc1

    if-eq v0, v1, :cond_3

    const/16 v1, 0x19

    if-eq v0, v1, :cond_3

    const/16 v1, 0x1a

    if-eq v0, v1, :cond_3

    const/16 v1, 0x92

    if-eq v0, v1, :cond_3

    const/16 v1, 0x94

    if-eq v0, v1, :cond_3

    const/16 v1, 0x97

    if-eq v0, v1, :cond_3

    const/16 v1, 0x98

    if-eq v0, v1, :cond_3

    const/16 v1, 0x99

    if-eq v0, v1, :cond_3

    const/16 v1, 0xb2

    if-eq v0, v1, :cond_3

    const/16 v1, 0xb3

    if-eq v0, v1, :cond_3

    const/16 v1, 0x9a

    if-eq v0, v1, :cond_3

    const/16 v1, 0x9c

    if-eq v0, v1, :cond_3

    const/16 v1, 0x9d

    if-eq v0, v1, :cond_3

    const/16 v1, 0x9e

    if-eq v0, v1, :cond_3

    const/16 v1, 0xa8

    if-eq v0, v1, :cond_3

    const/16 v1, 0xb4

    if-eq v0, v1, :cond_3

    const/16 v1, 0xc4

    if-eq v0, v1, :cond_3

    const/16 v1, 0xc5

    if-eq v0, v1, :cond_3

    const/16 v1, 0xc6

    if-eq v0, v1, :cond_3

    const/16 v1, 0xc7

    if-eq v0, v1, :cond_3

    const/16 v1, 0xc8

    if-eq v0, v1, :cond_3

    const/16 v1, 0xc9

    if-eq v0, v1, :cond_3

    const/16 v1, 0xca

    if-eq v0, v1, :cond_3

    const/16 v1, 0xe4

    if-eq v0, v1, :cond_3

    const/16 v1, 0xcb

    if-eq v0, v1, :cond_3

    const/16 v1, 0xcc

    if-eq v0, v1, :cond_3

    const/16 v1, 0xcd

    if-eq v0, v1, :cond_3

    const/16 v1, 0xce

    if-eq v0, v1, :cond_3

    const/16 v1, 0xd1

    if-eq v0, v1, :cond_3

    const/16 v1, 0xd5

    if-eq v0, v1, :cond_3

    const/16 v1, 0xd6

    if-eq v0, v1, :cond_3

    const/16 v1, 0xd7

    if-eq v0, v1, :cond_3

    const/16 v1, 0xd9

    if-eq v0, v1, :cond_3

    const/16 v1, 0xda

    if-eq v0, v1, :cond_3

    const/16 v1, 0xdb

    if-eq v0, v1, :cond_3

    const/16 v1, 0xdc

    if-eq v0, v1, :cond_3

    const/16 v1, 0xdd

    if-eq v0, v1, :cond_3

    const/16 v1, 0xde

    if-eq v0, v1, :cond_3

    const/16 v1, 0xdf

    if-eq v0, v1, :cond_3

    const/16 v1, 0xe0

    if-eq v0, v1, :cond_3

    const/16 v1, 0xe2

    if-eq v0, v1, :cond_3

    const/16 v1, 0xe3

    if-eq v0, v1, :cond_3

    const/16 v1, 0xe5

    if-eq v0, v1, :cond_3

    const/16 v1, 0xe9

    if-eq v0, v1, :cond_3

    const/16 v1, 0xea

    if-eq v0, v1, :cond_3

    const/16 v1, 0xeb

    if-eq v0, v1, :cond_3

    const/16 v1, 0x16

    if-eq v0, v1, :cond_3

    const/16 v1, 0x17

    if-eq v0, v1, :cond_3

    const/16 v1, 0x18

    if-ne v0, v1, :cond_4

    .line 935
    :cond_3
    iput v0, p0, Lqag;->b:I

    goto/16 :goto_0

    .line 937
    :cond_4
    iput v4, p0, Lqag;->b:I

    goto/16 :goto_0

    .line 671
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 8

    .prologue
    .line 632
    iget-object v0, p0, Lqag;->a:[Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 633
    iget-object v1, p0, Lqag;->a:[Ljava/lang/Long;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 634
    const/4 v4, 0x1

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {p1, v4, v6, v7}, Loxo;->b(IJ)V

    .line 633
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 637
    :cond_0
    iget v0, p0, Lqag;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 638
    const/4 v0, 0x2

    iget v1, p0, Lqag;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 640
    :cond_1
    iget-object v0, p0, Lqag;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 642
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 617
    invoke-virtual {p0, p1}, Lqag;->a(Loxn;)Lqag;

    move-result-object v0

    return-object v0
.end method
