.class public final Lmmg;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lmmf;

.field private b:Lmmf;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 237
    invoke-direct {p0}, Loxq;-><init>()V

    .line 240
    iput-object v0, p0, Lmmg;->a:Lmmf;

    .line 243
    iput-object v0, p0, Lmmg;->b:Lmmf;

    .line 237
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 260
    const/4 v0, 0x0

    .line 261
    iget-object v1, p0, Lmmg;->a:Lmmf;

    if-eqz v1, :cond_0

    .line 262
    const/4 v0, 0x1

    iget-object v1, p0, Lmmg;->a:Lmmf;

    .line 263
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 265
    :cond_0
    iget-object v1, p0, Lmmg;->b:Lmmf;

    if-eqz v1, :cond_1

    .line 266
    const/4 v1, 0x2

    iget-object v2, p0, Lmmg;->b:Lmmf;

    .line 267
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 269
    :cond_1
    iget-object v1, p0, Lmmg;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 270
    iput v0, p0, Lmmg;->ai:I

    .line 271
    return v0
.end method

.method public a(Loxn;)Lmmg;
    .locals 2

    .prologue
    .line 279
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 280
    sparse-switch v0, :sswitch_data_0

    .line 284
    iget-object v1, p0, Lmmg;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 285
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmmg;->ah:Ljava/util/List;

    .line 288
    :cond_1
    iget-object v1, p0, Lmmg;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 290
    :sswitch_0
    return-object p0

    .line 295
    :sswitch_1
    iget-object v0, p0, Lmmg;->a:Lmmf;

    if-nez v0, :cond_2

    .line 296
    new-instance v0, Lmmf;

    invoke-direct {v0}, Lmmf;-><init>()V

    iput-object v0, p0, Lmmg;->a:Lmmf;

    .line 298
    :cond_2
    iget-object v0, p0, Lmmg;->a:Lmmf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 302
    :sswitch_2
    iget-object v0, p0, Lmmg;->b:Lmmf;

    if-nez v0, :cond_3

    .line 303
    new-instance v0, Lmmf;

    invoke-direct {v0}, Lmmf;-><init>()V

    iput-object v0, p0, Lmmg;->b:Lmmf;

    .line 305
    :cond_3
    iget-object v0, p0, Lmmg;->b:Lmmf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 280
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 248
    iget-object v0, p0, Lmmg;->a:Lmmf;

    if-eqz v0, :cond_0

    .line 249
    const/4 v0, 0x1

    iget-object v1, p0, Lmmg;->a:Lmmf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 251
    :cond_0
    iget-object v0, p0, Lmmg;->b:Lmmf;

    if-eqz v0, :cond_1

    .line 252
    const/4 v0, 0x2

    iget-object v1, p0, Lmmg;->b:Lmmf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 254
    :cond_1
    iget-object v0, p0, Lmmg;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 256
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 233
    invoke-virtual {p0, p1}, Lmmg;->a(Loxn;)Lmmg;

    move-result-object v0

    return-object v0
.end method
