.class public final Leoy;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:Landroid/app/Activity;

.field b:Landroid/content/Context;

.field c:Lfba;

.field d:Z

.field e:Z

.field f:Landroid/widget/FrameLayout;

.field g:Landroid/view/View;

.field private h:Llhd;

.field private i:Llhc;

.field private j:Landroid/transition/Transition$TransitionListener;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/content/Context;Lfba;Llhd;Landroid/widget/FrameLayout;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p1, p0, Leoy;->a:Landroid/app/Activity;

    .line 52
    iput-object p2, p0, Leoy;->b:Landroid/content/Context;

    .line 53
    iput-object p3, p0, Leoy;->c:Lfba;

    .line 54
    iput-object p4, p0, Leoy;->h:Llhd;

    .line 55
    iput-object p5, p0, Leoy;->f:Landroid/widget/FrameLayout;

    .line 56
    iput-object p6, p0, Leoy;->g:Landroid/view/View;

    .line 57
    return-void
.end method

.method static synthetic a(Leoy;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 31
    iget-boolean v0, p0, Leoy;->e:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Leoy;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Leoy;->c:Lfba;

    invoke-virtual {v0, v2}, Lfba;->b(Z)V

    iget-object v0, p0, Leoy;->f:Landroid/widget/FrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    iget-object v0, p0, Leoy;->f:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    iget-object v0, p0, Leoy;->c:Lfba;

    invoke-virtual {v0, v3}, Lfba;->a(Lfys;)V

    iget-object v0, p0, Leoy;->h:Llhd;

    iget-object v1, p0, Leoy;->i:Llhc;

    invoke-virtual {v0, v1}, Llhd;->b(Llhc;)V

    invoke-static {}, Llsj;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Leoy;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/Window;->setSharedElementsUseOverlay(Z)V

    iget-object v1, p0, Leoy;->a:Landroid/app/Activity;

    new-instance v2, Lepd;

    invoke-direct {v2, p0}, Lepd;-><init>(Leoy;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->setEnterSharedElementCallback(Landroid/app/SharedElementCallback;)V

    invoke-virtual {v0}, Landroid/view/Window;->getSharedElementEnterTransition()Landroid/transition/Transition;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Leoy;->j:Landroid/transition/Transition$TransitionListener;

    if-eqz v1, :cond_0

    iget-object v1, p0, Leoy;->j:Landroid/transition/Transition$TransitionListener;

    invoke-virtual {v0, v1}, Landroid/transition/Transition;->removeListener(Landroid/transition/Transition$TransitionListener;)Landroid/transition/Transition;

    iput-object v3, p0, Leoy;->j:Landroid/transition/Transition$TransitionListener;

    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 64
    invoke-static {}, Llsj;->a()Z

    move-result v2

    if-nez v2, :cond_0

    .line 202
    :goto_0
    return-void

    .line 68
    :cond_0
    iget-object v2, p0, Leoy;->a:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 69
    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Configuration;->getLayoutDirection()I

    move-result v3

    if-ne v3, v0, :cond_2

    .line 70
    :goto_1
    iget-object v3, p0, Leoy;->a:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v3

    .line 71
    invoke-virtual {v3, v1}, Landroid/view/Window;->setSharedElementsUseOverlay(Z)V

    .line 73
    iget-object v1, p0, Leoy;->a:Landroid/app/Activity;

    new-instance v4, Leoz;

    invoke-direct {v4, p0, v2, v0, v3}, Leoz;-><init>(Leoy;Landroid/content/res/Resources;ZLandroid/view/Window;)V

    invoke-virtual {v1, v4}, Landroid/app/Activity;->setEnterSharedElementCallback(Landroid/app/SharedElementCallback;)V

    .line 153
    invoke-virtual {v3}, Landroid/view/Window;->getSharedElementEnterTransition()Landroid/transition/Transition;

    move-result-object v0

    .line 154
    if-eqz v0, :cond_1

    .line 155
    new-instance v1, Lepa;

    invoke-direct {v1, p0}, Lepa;-><init>(Leoy;)V

    iput-object v1, p0, Leoy;->j:Landroid/transition/Transition$TransitionListener;

    .line 178
    iget-object v1, p0, Leoy;->j:Landroid/transition/Transition$TransitionListener;

    invoke-virtual {v0, v1}, Landroid/transition/Transition;->addListener(Landroid/transition/Transition$TransitionListener;)Landroid/transition/Transition;

    .line 181
    :cond_1
    iget-object v0, p0, Leoy;->c:Lfba;

    new-instance v1, Lepb;

    invoke-direct {v1, p0}, Lepb;-><init>(Leoy;)V

    invoke-virtual {v0, v1}, Lfba;->a(Lfys;)V

    .line 189
    new-instance v0, Lepc;

    invoke-direct {v0, p0}, Lepc;-><init>(Leoy;)V

    iput-object v0, p0, Leoy;->i:Llhc;

    .line 201
    iget-object v0, p0, Leoy;->h:Llhd;

    iget-object v1, p0, Leoy;->i:Llhc;

    invoke-virtual {v0, v1}, Llhd;->a(Llhc;)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 69
    goto :goto_1
.end method
