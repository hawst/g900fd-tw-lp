.class public final Lowz;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4336
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 4352
    const/4 v0, 0x0

    .line 4353
    iget-object v1, p0, Lowz;->a:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 4354
    const/4 v0, 0x1

    iget-object v1, p0, Lowz;->a:Ljava/lang/Long;

    .line 4355
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Loxo;->f(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 4357
    :cond_0
    iget-object v1, p0, Lowz;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4358
    iput v0, p0, Lowz;->ai:I

    .line 4359
    return v0
.end method

.method public a(Loxn;)Lowz;
    .locals 2

    .prologue
    .line 4367
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 4368
    sparse-switch v0, :sswitch_data_0

    .line 4372
    iget-object v1, p0, Lowz;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 4373
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lowz;->ah:Ljava/util/List;

    .line 4376
    :cond_1
    iget-object v1, p0, Lowz;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4378
    :sswitch_0
    return-object p0

    .line 4383
    :sswitch_1
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lowz;->a:Ljava/lang/Long;

    goto :goto_0

    .line 4368
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 4343
    iget-object v0, p0, Lowz;->a:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 4344
    const/4 v0, 0x1

    iget-object v1, p0, Lowz;->a:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 4346
    :cond_0
    iget-object v0, p0, Lowz;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 4348
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 4332
    invoke-virtual {p0, p1}, Lowz;->a(Loxn;)Lowz;

    move-result-object v0

    return-object v0
.end method
