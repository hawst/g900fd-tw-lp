.class public final Ldpt;
.super Lhny;
.source "PG"


# instance fields
.field private final a:Lkfd;

.field private final b:Lkfo;

.field private final c:[I


# direct methods
.method public varargs constructor <init>(Landroid/content/Context;I[I)V
    .locals 1

    .prologue
    .line 27
    const-string v0, "ReadPhotosFeaturesTask"

    invoke-direct {p0, p1, v0}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 28
    const-class v0, Lkfd;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkfd;

    iput-object v0, p0, Ldpt;->a:Lkfd;

    .line 29
    new-instance v0, Lkfo;

    invoke-direct {v0, p1, p2}, Lkfo;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Ldpt;->b:Lkfo;

    .line 30
    iput-object p3, p0, Ldpt;->c:[I

    .line 31
    return-void
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 35
    new-instance v2, Ldmo;

    .line 36
    invoke-virtual {p0}, Ldpt;->f()Landroid/content/Context;

    move-result-object v1

    iget-object v3, p0, Ldpt;->b:Lkfo;

    iget-object v4, p0, Ldpt;->c:[I

    invoke-direct {v2, v1, v3, v4}, Ldmo;-><init>(Landroid/content/Context;Lkfo;[I)V

    .line 37
    iget-object v1, p0, Ldpt;->a:Lkfd;

    invoke-interface {v1, v2}, Lkfd;->a(Lkff;)V

    .line 38
    new-instance v3, Lhoz;

    iget v1, v2, Lkff;->i:I

    iget-object v4, v2, Lkff;->k:Ljava/lang/Exception;

    invoke-direct {v3, v1, v4, v0}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    .line 40
    invoke-virtual {v2}, Ldmo;->t()Z

    move-result v1

    if-nez v1, :cond_0

    .line 41
    iget-object v4, p0, Ldpt;->c:[I

    array-length v5, v4

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v5, :cond_0

    aget v6, v4, v1

    .line 42
    const/4 v7, 0x2

    if-ne v6, v7, :cond_1

    .line 43
    invoke-virtual {v2}, Ldmo;->b()Ljava/util/ArrayList;

    move-result-object v0

    .line 48
    :cond_0
    invoke-virtual {p0}, Ldpt;->f()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lduf;->a(Ljava/util/ArrayList;Landroid/content/Context;)V

    .line 49
    return-object v3

    .line 41
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
