.class public final Lmds;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnvj;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 7855
    invoke-direct {p0}, Loxq;-><init>()V

    .line 7858
    iput-object v0, p0, Lmds;->apiHeader:Llyq;

    .line 7861
    iput-object v0, p0, Lmds;->a:Lnvj;

    .line 7855
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 7878
    const/4 v0, 0x0

    .line 7879
    iget-object v1, p0, Lmds;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 7880
    const/4 v0, 0x1

    iget-object v1, p0, Lmds;->apiHeader:Llyq;

    .line 7881
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 7883
    :cond_0
    iget-object v1, p0, Lmds;->a:Lnvj;

    if-eqz v1, :cond_1

    .line 7884
    const/4 v1, 0x2

    iget-object v2, p0, Lmds;->a:Lnvj;

    .line 7885
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7887
    :cond_1
    iget-object v1, p0, Lmds;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7888
    iput v0, p0, Lmds;->ai:I

    .line 7889
    return v0
.end method

.method public a(Loxn;)Lmds;
    .locals 2

    .prologue
    .line 7897
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 7898
    sparse-switch v0, :sswitch_data_0

    .line 7902
    iget-object v1, p0, Lmds;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 7903
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmds;->ah:Ljava/util/List;

    .line 7906
    :cond_1
    iget-object v1, p0, Lmds;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 7908
    :sswitch_0
    return-object p0

    .line 7913
    :sswitch_1
    iget-object v0, p0, Lmds;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 7914
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmds;->apiHeader:Llyq;

    .line 7916
    :cond_2
    iget-object v0, p0, Lmds;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 7920
    :sswitch_2
    iget-object v0, p0, Lmds;->a:Lnvj;

    if-nez v0, :cond_3

    .line 7921
    new-instance v0, Lnvj;

    invoke-direct {v0}, Lnvj;-><init>()V

    iput-object v0, p0, Lmds;->a:Lnvj;

    .line 7923
    :cond_3
    iget-object v0, p0, Lmds;->a:Lnvj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 7898
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 7866
    iget-object v0, p0, Lmds;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 7867
    const/4 v0, 0x1

    iget-object v1, p0, Lmds;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 7869
    :cond_0
    iget-object v0, p0, Lmds;->a:Lnvj;

    if-eqz v0, :cond_1

    .line 7870
    const/4 v0, 0x2

    iget-object v1, p0, Lmds;->a:Lnvj;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 7872
    :cond_1
    iget-object v0, p0, Lmds;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 7874
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 7851
    invoke-virtual {p0, p1}, Lmds;->a(Loxn;)Lmds;

    move-result-object v0

    return-object v0
.end method
