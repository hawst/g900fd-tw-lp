.class public Lkfv;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lkfj;


# static fields
.field private static a:Lkfr;

.field private static final b:Lloy;

.field private static f:Ljava/lang/String;


# instance fields
.field private final c:Landroid/content/Context;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 27
    new-instance v0, Lkfr;

    invoke-direct {v0}, Lkfr;-><init>()V

    sput-object v0, Lkfv;->a:Lkfr;

    .line 33
    new-instance v0, Lloy;

    const-string v1, "debug.allowBackendOverride"

    invoke-direct {v0, v1}, Lloy;-><init>(Ljava/lang/String;)V

    sput-object v0, Lkfv;->b:Lloy;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lkfv;->c:Landroid/content/Context;

    .line 43
    iput-object p2, p0, Lkfv;->d:Ljava/lang/String;

    .line 44
    iput-object p3, p0, Lkfv;->e:Ljava/lang/String;

    .line 45
    return-void
.end method


# virtual methods
.method protected a(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 108
    sget-object v0, Lkfv;->f:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 109
    invoke-static {p1}, Lorg/chromium/net/UserAgent;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, " (gzip)"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lkfv;->f:Ljava/lang/String;

    .line 111
    :cond_0
    sget-object v0, Lkfv;->f:Ljava/lang/String;

    return-object v0
.end method

.method public a(Ljava/lang/String;)Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 49
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 50
    const-string v0, "Accept-Encoding"

    const-string v2, "gzip"

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    const-string v0, "Accept-Language"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    const-string v0, "User-Agent"

    iget-object v2, p0, Lkfv;->c:Landroid/content/Context;

    invoke-virtual {p0, v2}, Lkfv;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    iget-object v0, p0, Lkfv;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 61
    :try_start_0
    sget-object v0, Lkfv;->a:Lkfr;

    iget-object v2, p0, Lkfv;->e:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lkfr;->a(Ljava/lang/String;)Lkfs;

    move-result-object v0

    .line 62
    iget-object v2, p0, Lkfv;->c:Landroid/content/Context;

    iget-object v3, p0, Lkfv;->d:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Lkfs;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 63
    iget-object v3, p0, Lkfv;->c:Landroid/content/Context;

    invoke-interface {v0, v3, v2}, Lkfs;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 68
    const-string v4, "Authorization"

    const-string v5, "Bearer "

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v5, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    const-string v0, "X-Auth-Time"

    invoke-virtual {v1, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    :cond_0
    sget-object v0, Lkfv;->b:Lloy;

    .line 73
    return-object v1

    .line 64
    :catch_0
    move-exception v0

    .line 65
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Cannot obtain authentication token"

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 68
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a()V
    .locals 3

    .prologue
    .line 88
    iget-object v0, p0, Lkfv;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 92
    :try_start_0
    sget-object v0, Lkfv;->a:Lkfr;

    iget-object v1, p0, Lkfv;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lkfr;->a(Ljava/lang/String;)Lkfs;

    move-result-object v0

    iget-object v1, p0, Lkfv;->c:Landroid/content/Context;

    iget-object v2, p0, Lkfv;->d:Ljava/lang/String;

    .line 93
    invoke-interface {v0, v1, v2}, Lkfs;->b(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 98
    :cond_0
    return-void

    .line 94
    :catch_0
    move-exception v0

    .line 95
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Cannot invalidate authentication token"

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lkfv;->e:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method
