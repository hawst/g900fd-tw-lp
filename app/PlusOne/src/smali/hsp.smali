.class public final Lhsp;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhso;


# instance fields
.field private a:Lkdv;

.field private b:Lhsr;

.field private c:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lhsp;->b:Lhsr;

    .line 26
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lhsp;->c:Landroid/content/Context;

    .line 27
    iget-object v0, p0, Lhsp;->c:Landroid/content/Context;

    const-class v1, Lkdv;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkdv;

    iput-object v0, p0, Lhsp;->a:Lkdv;

    .line 28
    return-void
.end method

.method static synthetic a(Lhsp;Lhsr;Lkdd;)Lcom/google/android/libraries/social/avatars/AvatarResource;
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Lhsp;->a(Lhsr;Lkdd;)Lcom/google/android/libraries/social/avatars/AvatarResource;

    move-result-object v0

    return-object v0
.end method

.method private a(Lhsr;Lkdd;)Lcom/google/android/libraries/social/avatars/AvatarResource;
    .locals 2

    .prologue
    .line 135
    iget-object v0, p0, Lhsp;->a:Lkdv;

    invoke-interface {v0, p1}, Lkdv;->a(Lkdc;)Lkda;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/avatars/AvatarResource;

    .line 136
    if-nez v0, :cond_0

    .line 137
    new-instance v0, Lcom/google/android/libraries/social/avatars/AvatarResource;

    iget-object v1, p0, Lhsp;->a:Lkdv;

    invoke-direct {v0, v1, p1}, Lcom/google/android/libraries/social/avatars/AvatarResource;-><init>(Lkdv;Lhsr;)V

    .line 143
    :goto_0
    iget-object v1, p0, Lhsp;->a:Lkdv;

    invoke-interface {v1, v0, p2}, Lkdv;->a(Lkda;Lkdd;)V

    .line 144
    return-object v0

    .line 140
    :cond_0
    iget-object v1, p0, Lhsp;->b:Lhsr;

    invoke-virtual {p1, v1}, Lhsr;->a(Lhsr;)V

    .line 141
    iput-object p1, p0, Lhsp;->b:Lhsr;

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;III)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 100
    or-int/lit8 v0, p5, 0x10

    invoke-direct {p0, p2, p3, p4, v0}, Lhsp;->b(Ljava/lang/String;III)Lhsr;

    move-result-object v0

    new-instance v1, Lhsq;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2, v0}, Lhsq;-><init>(Lhsp;Lkdf;Lhsr;)V

    invoke-virtual {v1}, Lhsq;->a()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private b(Ljava/lang/String;III)Lhsr;
    .locals 2

    .prologue
    .line 123
    iget-object v0, p0, Lhsp;->b:Lhsr;

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lhsp;->b:Lhsr;

    .line 125
    iget-object v1, p0, Lhsp;->b:Lhsr;

    invoke-virtual {v1}, Lhsr;->b()Lhsr;

    move-result-object v1

    iput-object v1, p0, Lhsp;->b:Lhsr;

    .line 126
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lhsr;->a(Lhsr;)V

    .line 130
    :goto_0
    invoke-virtual {v0, p1, p2, p3, p4}, Lhsr;->a(Ljava/lang/String;III)V

    .line 131
    return-object v0

    .line 128
    :cond_0
    new-instance v0, Lhsr;

    invoke-direct {v0}, Lhsr;-><init>()V

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/String;II)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 85
    const/4 v1, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    invoke-direct/range {v0 .. v5}, Lhsp;->a(Ljava/lang/String;Ljava/lang/String;III)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;III)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 94
    const/4 v1, 0x0

    move-object v0, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lhsp;->a(Ljava/lang/String;Ljava/lang/String;III)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a(IILkdd;)Lkda;
    .locals 2

    .prologue
    .line 54
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, p2, v1}, Lhsp;->b(Ljava/lang/String;III)Lhsr;

    move-result-object v0

    .line 56
    invoke-direct {p0, v0, p3}, Lhsp;->a(Lhsr;Lkdd;)Lcom/google/android/libraries/social/avatars/AvatarResource;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;IILkdd;)Lkda;
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lhsp;->b(Ljava/lang/String;III)Lhsr;

    move-result-object v0

    .line 41
    invoke-direct {p0, v0, p4}, Lhsp;->a(Lhsr;Lkdd;)Lcom/google/android/libraries/social/avatars/AvatarResource;

    move-result-object v0

    return-object v0
.end method

.method public a(II)V
    .locals 4

    .prologue
    .line 74
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 75
    invoke-direct {p0, v0, p1, p2, v1}, Lhsp;->b(Ljava/lang/String;III)Lhsr;

    move-result-object v1

    .line 76
    iget-object v0, p0, Lhsp;->a:Lkdv;

    invoke-interface {v0, v1}, Lkdv;->a(Lkdc;)Lkda;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/avatars/AvatarResource;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhsp;->b:Lhsr;

    invoke-virtual {v1, v0}, Lhsr;->a(Lhsr;)V

    iput-object v1, p0, Lhsp;->b:Lhsr;

    .line 77
    :goto_0
    return-void

    .line 76
    :cond_0
    iget-object v0, p0, Lhsp;->a:Lkdv;

    new-instance v2, Lcom/google/android/libraries/social/avatars/AvatarResource;

    iget-object v3, p0, Lhsp;->a:Lkdv;

    invoke-direct {v2, v3, v1}, Lcom/google/android/libraries/social/avatars/AvatarResource;-><init>(Lkdv;Lhsr;)V

    invoke-interface {v0, v2}, Lkdv;->c(Lkda;)V

    goto :goto_0
.end method
