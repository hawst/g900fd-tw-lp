.class public final Loww;
.super Loxq;
.source "PG"


# instance fields
.field private a:Losx;

.field private b:Ljava/lang/String;

.field private c:Lowu;

.field private d:[Lowu;

.field private e:[Lowu;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 319
    invoke-direct {p0}, Loxq;-><init>()V

    .line 322
    iput-object v0, p0, Loww;->a:Losx;

    .line 327
    iput-object v0, p0, Loww;->c:Lowu;

    .line 330
    sget-object v0, Lowu;->a:[Lowu;

    iput-object v0, p0, Loww;->d:[Lowu;

    .line 333
    sget-object v0, Lowu;->a:[Lowu;

    iput-object v0, p0, Loww;->e:[Lowu;

    .line 319
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 367
    .line 368
    iget-object v0, p0, Loww;->a:Losx;

    if-eqz v0, :cond_6

    .line 369
    const/4 v0, 0x1

    iget-object v2, p0, Loww;->a:Losx;

    .line 370
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 372
    :goto_0
    iget-object v2, p0, Loww;->b:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 373
    const/4 v2, 0x2

    iget-object v3, p0, Loww;->b:Ljava/lang/String;

    .line 374
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 376
    :cond_0
    iget-object v2, p0, Loww;->c:Lowu;

    if-eqz v2, :cond_1

    .line 377
    const/4 v2, 0x3

    iget-object v3, p0, Loww;->c:Lowu;

    .line 378
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 380
    :cond_1
    iget-object v2, p0, Loww;->d:[Lowu;

    if-eqz v2, :cond_3

    .line 381
    iget-object v3, p0, Loww;->d:[Lowu;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_3

    aget-object v5, v3, v2

    .line 382
    if-eqz v5, :cond_2

    .line 383
    const/4 v6, 0x5

    .line 384
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 381
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 388
    :cond_3
    iget-object v2, p0, Loww;->e:[Lowu;

    if-eqz v2, :cond_5

    .line 389
    iget-object v2, p0, Loww;->e:[Lowu;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    .line 390
    if-eqz v4, :cond_4

    .line 391
    const/4 v5, 0x6

    .line 392
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 389
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 396
    :cond_5
    iget-object v1, p0, Loww;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 397
    iput v0, p0, Loww;->ai:I

    .line 398
    return v0

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Loww;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 406
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 407
    sparse-switch v0, :sswitch_data_0

    .line 411
    iget-object v2, p0, Loww;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 412
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Loww;->ah:Ljava/util/List;

    .line 415
    :cond_1
    iget-object v2, p0, Loww;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 417
    :sswitch_0
    return-object p0

    .line 422
    :sswitch_1
    iget-object v0, p0, Loww;->a:Losx;

    if-nez v0, :cond_2

    .line 423
    new-instance v0, Losx;

    invoke-direct {v0}, Losx;-><init>()V

    iput-object v0, p0, Loww;->a:Losx;

    .line 425
    :cond_2
    iget-object v0, p0, Loww;->a:Losx;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 429
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loww;->b:Ljava/lang/String;

    goto :goto_0

    .line 433
    :sswitch_3
    iget-object v0, p0, Loww;->c:Lowu;

    if-nez v0, :cond_3

    .line 434
    new-instance v0, Lowu;

    invoke-direct {v0}, Lowu;-><init>()V

    iput-object v0, p0, Loww;->c:Lowu;

    .line 436
    :cond_3
    iget-object v0, p0, Loww;->c:Lowu;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 440
    :sswitch_4
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 441
    iget-object v0, p0, Loww;->d:[Lowu;

    if-nez v0, :cond_5

    move v0, v1

    .line 442
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lowu;

    .line 443
    iget-object v3, p0, Loww;->d:[Lowu;

    if-eqz v3, :cond_4

    .line 444
    iget-object v3, p0, Loww;->d:[Lowu;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 446
    :cond_4
    iput-object v2, p0, Loww;->d:[Lowu;

    .line 447
    :goto_2
    iget-object v2, p0, Loww;->d:[Lowu;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 448
    iget-object v2, p0, Loww;->d:[Lowu;

    new-instance v3, Lowu;

    invoke-direct {v3}, Lowu;-><init>()V

    aput-object v3, v2, v0

    .line 449
    iget-object v2, p0, Loww;->d:[Lowu;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 450
    invoke-virtual {p1}, Loxn;->a()I

    .line 447
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 441
    :cond_5
    iget-object v0, p0, Loww;->d:[Lowu;

    array-length v0, v0

    goto :goto_1

    .line 453
    :cond_6
    iget-object v2, p0, Loww;->d:[Lowu;

    new-instance v3, Lowu;

    invoke-direct {v3}, Lowu;-><init>()V

    aput-object v3, v2, v0

    .line 454
    iget-object v2, p0, Loww;->d:[Lowu;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 458
    :sswitch_5
    const/16 v0, 0x32

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 459
    iget-object v0, p0, Loww;->e:[Lowu;

    if-nez v0, :cond_8

    move v0, v1

    .line 460
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lowu;

    .line 461
    iget-object v3, p0, Loww;->e:[Lowu;

    if-eqz v3, :cond_7

    .line 462
    iget-object v3, p0, Loww;->e:[Lowu;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 464
    :cond_7
    iput-object v2, p0, Loww;->e:[Lowu;

    .line 465
    :goto_4
    iget-object v2, p0, Loww;->e:[Lowu;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    .line 466
    iget-object v2, p0, Loww;->e:[Lowu;

    new-instance v3, Lowu;

    invoke-direct {v3}, Lowu;-><init>()V

    aput-object v3, v2, v0

    .line 467
    iget-object v2, p0, Loww;->e:[Lowu;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 468
    invoke-virtual {p1}, Loxn;->a()I

    .line 465
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 459
    :cond_8
    iget-object v0, p0, Loww;->e:[Lowu;

    array-length v0, v0

    goto :goto_3

    .line 471
    :cond_9
    iget-object v2, p0, Loww;->e:[Lowu;

    new-instance v3, Lowu;

    invoke-direct {v3}, Lowu;-><init>()V

    aput-object v3, v2, v0

    .line 472
    iget-object v2, p0, Loww;->e:[Lowu;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 407
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 338
    iget-object v1, p0, Loww;->a:Losx;

    if-eqz v1, :cond_0

    .line 339
    const/4 v1, 0x1

    iget-object v2, p0, Loww;->a:Losx;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 341
    :cond_0
    iget-object v1, p0, Loww;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 342
    const/4 v1, 0x2

    iget-object v2, p0, Loww;->b:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 344
    :cond_1
    iget-object v1, p0, Loww;->c:Lowu;

    if-eqz v1, :cond_2

    .line 345
    const/4 v1, 0x3

    iget-object v2, p0, Loww;->c:Lowu;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 347
    :cond_2
    iget-object v1, p0, Loww;->d:[Lowu;

    if-eqz v1, :cond_4

    .line 348
    iget-object v2, p0, Loww;->d:[Lowu;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 349
    if-eqz v4, :cond_3

    .line 350
    const/4 v5, 0x5

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 348
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 354
    :cond_4
    iget-object v1, p0, Loww;->e:[Lowu;

    if-eqz v1, :cond_6

    .line 355
    iget-object v1, p0, Loww;->e:[Lowu;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_6

    aget-object v3, v1, v0

    .line 356
    if-eqz v3, :cond_5

    .line 357
    const/4 v4, 0x6

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 355
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 361
    :cond_6
    iget-object v0, p0, Loww;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 363
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 315
    invoke-virtual {p0, p1}, Loww;->a(Loxn;)Loww;

    move-result-object v0

    return-object v0
.end method
