.class public final Lnsw;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lnsj;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1835
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1838
    sget-object v0, Lnsj;->a:[Lnsj;

    iput-object v0, p0, Lnsw;->a:[Lnsj;

    .line 1835
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1856
    .line 1857
    iget-object v1, p0, Lnsw;->a:[Lnsj;

    if-eqz v1, :cond_1

    .line 1858
    iget-object v2, p0, Lnsw;->a:[Lnsj;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 1859
    if-eqz v4, :cond_0

    .line 1860
    const/4 v5, 0x1

    .line 1861
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 1858
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1865
    :cond_1
    iget-object v1, p0, Lnsw;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1866
    iput v0, p0, Lnsw;->ai:I

    .line 1867
    return v0
.end method

.method public a(Loxn;)Lnsw;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1875
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1876
    sparse-switch v0, :sswitch_data_0

    .line 1880
    iget-object v2, p0, Lnsw;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 1881
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnsw;->ah:Ljava/util/List;

    .line 1884
    :cond_1
    iget-object v2, p0, Lnsw;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1886
    :sswitch_0
    return-object p0

    .line 1891
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1892
    iget-object v0, p0, Lnsw;->a:[Lnsj;

    if-nez v0, :cond_3

    move v0, v1

    .line 1893
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnsj;

    .line 1894
    iget-object v3, p0, Lnsw;->a:[Lnsj;

    if-eqz v3, :cond_2

    .line 1895
    iget-object v3, p0, Lnsw;->a:[Lnsj;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1897
    :cond_2
    iput-object v2, p0, Lnsw;->a:[Lnsj;

    .line 1898
    :goto_2
    iget-object v2, p0, Lnsw;->a:[Lnsj;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 1899
    iget-object v2, p0, Lnsw;->a:[Lnsj;

    new-instance v3, Lnsj;

    invoke-direct {v3}, Lnsj;-><init>()V

    aput-object v3, v2, v0

    .line 1900
    iget-object v2, p0, Lnsw;->a:[Lnsj;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1901
    invoke-virtual {p1}, Loxn;->a()I

    .line 1898
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1892
    :cond_3
    iget-object v0, p0, Lnsw;->a:[Lnsj;

    array-length v0, v0

    goto :goto_1

    .line 1904
    :cond_4
    iget-object v2, p0, Lnsw;->a:[Lnsj;

    new-instance v3, Lnsj;

    invoke-direct {v3}, Lnsj;-><init>()V

    aput-object v3, v2, v0

    .line 1905
    iget-object v2, p0, Lnsw;->a:[Lnsj;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1876
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 1843
    iget-object v0, p0, Lnsw;->a:[Lnsj;

    if-eqz v0, :cond_1

    .line 1844
    iget-object v1, p0, Lnsw;->a:[Lnsj;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 1845
    if-eqz v3, :cond_0

    .line 1846
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 1844
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1850
    :cond_1
    iget-object v0, p0, Lnsw;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1852
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1831
    invoke-virtual {p0, p1}, Lnsw;->a(Loxn;)Lnsw;

    move-result-object v0

    return-object v0
.end method
