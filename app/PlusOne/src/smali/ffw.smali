.class public final Lffw;
.super Lhye;
.source "PG"


# instance fields
.field private final b:I

.field private final c:Ljava/lang/String;

.field private final d:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 451
    invoke-direct {p0, p1}, Lhye;-><init>(Landroid/content/Context;)V

    .line 452
    iput p2, p0, Lffw;->b:I

    .line 453
    iget v0, p0, Lffw;->b:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 454
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 455
    iget v1, p0, Lffw;->b:I

    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lffw;->c:Ljava/lang/String;

    .line 456
    new-array v0, v2, [Ljava/lang/String;

    const/4 v1, 0x2

    new-array v2, v2, [Ljava/lang/String;

    iget-object v3, p0, Lffw;->c:Ljava/lang/String;

    aput-object v3, v2, v4

    .line 457
    invoke-static {v1, v2}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    iput-object v0, p0, Lffw;->d:[Ljava/lang/String;

    .line 463
    :goto_0
    return-void

    .line 460
    :cond_0
    iput-object v3, p0, Lffw;->c:Ljava/lang/String;

    .line 461
    iput-object v3, p0, Lffw;->d:[Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public C()Landroid/database/Cursor;
    .locals 11

    .prologue
    .line 467
    iget v0, p0, Lffw;->b:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 468
    const/4 v0, 0x0

    .line 513
    :goto_0
    return-object v0

    .line 471
    :cond_0
    invoke-virtual {p0}, Lffw;->n()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/DreamSettingsActivity;->b(Landroid/content/Context;)Landroid/database/Cursor;

    move-result-object v8

    .line 473
    invoke-virtual {p0}, Lffw;->n()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lffw;->b:I

    invoke-static {v0, v1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    .line 474
    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v9

    .line 475
    new-instance v10, Landroid/database/MatrixCursor;

    invoke-static {}, Lcom/google/android/apps/plus/service/DreamSettingsActivity;->j()[Ljava/lang/String;

    move-result-object v0

    invoke-direct {v10, v0}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 477
    const-string v0, "SELECT count(*) FROM all_photos WHERE is_primary = 1 AND media_attr & 32 = 0 AND media_attr & 128 = 0 AND media_attr & 64 = 0"

    const/4 v1, 0x0

    invoke-static {v9, v0, v1}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v0

    .line 480
    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 481
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 482
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const/4 v2, 0x0

    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 484
    invoke-virtual {p0}, Lffw;->n()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0a0a83

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const/4 v2, 0x3

    .line 485
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 481
    invoke-virtual {v10, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 488
    :cond_1
    const/4 v0, 0x0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lffw;->c:Ljava/lang/String;

    aput-object v3, v1, v2

    .line 489
    invoke-static {v0, v1}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 490
    const-string v1, "SELECT count(*) FROM all_tiles WHERE view_id = ? AND media_attr & 512 == 0 AND type == 4 AND media_attr & 32 = 0 AND media_attr & 128 = 0 AND media_attr & 64 = 0"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v9, v1, v2}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v2

    .line 493
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_2

    .line 494
    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 495
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object v0, v1, v2

    const/4 v0, 0x2

    .line 497
    invoke-virtual {p0}, Lffw;->n()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0a0a82

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x3

    const/4 v2, 0x2

    .line 498
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    .line 494
    invoke-virtual {v10, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 503
    :cond_2
    const-string v0, "SELECT count(*) FROM all_tiles WHERE view_id = ?"

    iget-object v1, p0, Lffw;->d:[Ljava/lang/String;

    invoke-static {v9, v0, v1}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_3

    .line 504
    new-instance v0, Ldoi;

    invoke-virtual {p0}, Lffw;->n()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lffw;->b:I

    iget-object v3, p0, Lffw;->c:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Ldoi;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)V

    .line 506
    invoke-virtual {v0}, Lkff;->l()V

    .line 509
    :cond_3
    const-string v1, "all_tiles"

    invoke-static {}, Lcom/google/android/apps/plus/service/DreamSettingsActivity;->j()[Ljava/lang/String;

    move-result-object v2

    const-string v3, "view_id = ?"

    iget-object v4, p0, Lffw;->d:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, v9

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 513
    new-instance v0, Landroid/database/MergeCursor;

    const/4 v2, 0x3

    new-array v2, v2, [Landroid/database/Cursor;

    const/4 v3, 0x0

    aput-object v8, v2, v3

    const/4 v3, 0x1

    aput-object v10, v2, v3

    const/4 v3, 0x2

    aput-object v1, v2, v3

    invoke-direct {v0, v2}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    goto/16 :goto_0
.end method
