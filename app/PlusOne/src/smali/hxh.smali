.class public final Lhxh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lbb;

.field private final c:I

.field private final d:Landroid/database/DataSetObservable;

.field private e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private f:Z

.field private final g:Ljava/lang/StringBuilder;

.field private final h:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lbb;I)V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lhxh;-><init>(Landroid/content/Context;Lbb;II)V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lbb;II)V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Landroid/database/DataSetObservable;

    invoke-direct {v0}, Landroid/database/DataSetObservable;-><init>()V

    iput-object v0, p0, Lhxh;->d:Landroid/database/DataSetObservable;

    .line 40
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lhxh;->g:Ljava/lang/StringBuilder;

    .line 50
    iput-object p1, p0, Lhxh;->a:Landroid/content/Context;

    .line 51
    iput-object p2, p0, Lhxh;->b:Lbb;

    .line 52
    iput p3, p0, Lhxh;->c:I

    .line 53
    add-int/lit16 v0, p4, 0x800

    iput v0, p0, Lhxh;->h:I

    .line 54
    return-void
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 137
    new-instance v0, Lhxg;

    iget-object v1, p0, Lhxh;->a:Landroid/content/Context;

    iget v2, p0, Lhxh;->c:I

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const-string v4, "circle_id"

    aput-object v4, v3, v6

    const/4 v4, 0x1

    const-string v5, "circle_name"

    aput-object v5, v3, v4

    invoke-direct {v0, v1, v2, v6, v3}, Lhxg;-><init>(Landroid/content/Context;II[Ljava/lang/String;)V

    return-object v0
.end method

.method public declared-synchronized a(Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 70
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lhxh;->f:Z

    if-eqz v1, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 71
    :cond_0
    const-string v0, ""
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 93
    :goto_0
    monitor-exit p0

    return-object v0

    .line 74
    :cond_1
    :try_start_1
    iget-object v1, p0, Lhxh;->g:Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    move v2, v0

    .line 79
    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v2, v0, :cond_4

    .line 80
    const/16 v0, 0x7c

    invoke-virtual {p1, v0, v2}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 81
    const/4 v1, -0x1

    if-ne v0, v1, :cond_5

    .line 82
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    move v1, v0

    .line 84
    :goto_2
    iget-object v0, p0, Lhxh;->e:Ljava/util/Map;

    invoke-virtual {p1, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 85
    if-eqz v0, :cond_3

    .line 86
    iget-object v2, p0, Lhxh;->g:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-eqz v2, :cond_2

    .line 87
    iget-object v2, p0, Lhxh;->g:Ljava/lang/StringBuilder;

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    :cond_2
    iget-object v2, p0, Lhxh;->g:Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v2, v0

    .line 92
    goto :goto_1

    .line 93
    :cond_4
    iget-object v0, p0, Lhxh;->g:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 70
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_5
    move v1, v0

    goto :goto_2
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 146
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 147
    if-eqz p1, :cond_1

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 149
    :cond_0
    const/4 v1, 0x0

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 152
    :cond_1
    iput-object v0, p0, Lhxh;->e:Ljava/util/Map;

    .line 153
    iput-boolean v3, p0, Lhxh;->f:Z

    .line 154
    iget-object v0, p0, Lhxh;->d:Landroid/database/DataSetObservable;

    invoke-virtual {v0}, Landroid/database/DataSetObservable;->notifyChanged()V

    .line 155
    return-void
.end method

.method public a(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lhxh;->d:Landroid/database/DataSetObservable;

    invoke-virtual {v0, p1}, Landroid/database/DataSetObservable;->registerObserver(Ljava/lang/Object;)V

    .line 167
    return-void
.end method

.method public a(Ldo;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 162
    iget-object v0, p0, Lhxh;->d:Landroid/database/DataSetObservable;

    invoke-virtual {v0}, Landroid/database/DataSetObservable;->notifyInvalidated()V

    .line 163
    return-void
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 23
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p2}, Lhxh;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 60
    iget-boolean v0, p0, Lhxh;->f:Z

    return v0
.end method

.method public declared-synchronized b(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 103
    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 104
    iget-boolean v0, p0, Lhxh;->f:Z

    if-eqz v0, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 122
    :goto_0
    monitor-exit p0

    return-object v0

    .line 110
    :cond_1
    const/4 v0, 0x0

    move v3, v0

    .line 111
    :goto_1
    :try_start_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v3, v0, :cond_3

    .line 112
    const/16 v0, 0x7c

    invoke-virtual {p1, v0, v3}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 113
    const/4 v2, -0x1

    if-ne v0, v2, :cond_4

    .line 114
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    move v2, v0

    .line 116
    :goto_2
    iget-object v0, p0, Lhxh;->e:Ljava/util/Map;

    invoke-virtual {p1, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 117
    if-eqz v0, :cond_2

    .line 118
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 120
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v3, v0

    .line 121
    goto :goto_1

    :cond_3
    move-object v0, v1

    .line 122
    goto :goto_0

    .line 103
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_4
    move v2, v0

    goto :goto_2
.end method

.method public b()V
    .locals 3

    .prologue
    .line 129
    iget-object v0, p0, Lhxh;->b:Lbb;

    iget v1, p0, Lhxh;->h:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 130
    return-void
.end method
