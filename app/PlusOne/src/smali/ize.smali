.class public final Lize;
.super Lkgn;
.source "PG"

# interfaces
.implements Lkig;


# instance fields
.field private final Q:Lkif;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 14
    invoke-direct {p0}, Lkgn;-><init>()V

    .line 28
    new-instance v0, Lkif;

    iget-object v1, p0, Lize;->P:Llqm;

    invoke-direct {v0, p0, v1}, Lkif;-><init>(Lkgn;Llqr;)V

    iput-object v0, p0, Lize;->Q:Lkif;

    return-void
.end method

.method public static b(Landroid/content/Intent;)Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 22
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 23
    const-string v1, "LoginSettingsFragment.account_view_intent"

    const-string v2, "LoginSettingsFragment.account_view_intent"

    .line 24
    invoke-virtual {p0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    .line 23
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 25
    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 39
    new-instance v1, Lizf;

    invoke-direct {v1}, Lizf;-><init>()V

    .line 40
    invoke-virtual {p0}, Lize;->k()Landroid/os/Bundle;

    move-result-object v0

    .line 41
    if-eqz v0, :cond_0

    .line 42
    const-string v2, "LoginSettingsFragment.account_view_intent"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    invoke-virtual {v1, v0}, Lizf;->b(Landroid/content/Intent;)V

    .line 44
    :cond_0
    iget-object v0, p0, Lize;->Q:Lkif;

    invoke-virtual {v0, v1}, Lkif;->a(Lu;)V

    .line 45
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 33
    invoke-super {p0, p1}, Lkgn;->c(Landroid/os/Bundle;)V

    .line 34
    iget-object v0, p0, Lize;->O:Llnh;

    const-class v1, Lkij;

    iget-object v2, p0, Lize;->Q:Lkif;

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 35
    return-void
.end method
