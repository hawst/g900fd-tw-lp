.class public final Lnmr;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lofl;

.field public b:Logu;

.field public c:Ljava/lang/Boolean;

.field private d:Ljava/lang/Integer;

.field private e:Logj;

.field private f:Ljava/lang/Boolean;

.field private g:Locx;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2089
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2094
    iput-object v0, p0, Lnmr;->a:Lofl;

    .line 2097
    iput-object v0, p0, Lnmr;->b:Logu;

    .line 2102
    iput-object v0, p0, Lnmr;->e:Logj;

    .line 2107
    iput-object v0, p0, Lnmr;->g:Locx;

    .line 2089
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 2139
    const/4 v0, 0x0

    .line 2140
    iget-object v1, p0, Lnmr;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 2141
    const/4 v0, 0x1

    iget-object v1, p0, Lnmr;->d:Ljava/lang/Integer;

    .line 2142
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2144
    :cond_0
    iget-object v1, p0, Lnmr;->a:Lofl;

    if-eqz v1, :cond_1

    .line 2145
    const/4 v1, 0x2

    iget-object v2, p0, Lnmr;->a:Lofl;

    .line 2146
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2148
    :cond_1
    iget-object v1, p0, Lnmr;->b:Logu;

    if-eqz v1, :cond_2

    .line 2149
    const/4 v1, 0x3

    iget-object v2, p0, Lnmr;->b:Logu;

    .line 2150
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2152
    :cond_2
    iget-object v1, p0, Lnmr;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 2153
    const/4 v1, 0x4

    iget-object v2, p0, Lnmr;->c:Ljava/lang/Boolean;

    .line 2154
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2156
    :cond_3
    iget-object v1, p0, Lnmr;->e:Logj;

    if-eqz v1, :cond_4

    .line 2157
    const/4 v1, 0x5

    iget-object v2, p0, Lnmr;->e:Logj;

    .line 2158
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2160
    :cond_4
    iget-object v1, p0, Lnmr;->f:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    .line 2161
    const/4 v1, 0x6

    iget-object v2, p0, Lnmr;->f:Ljava/lang/Boolean;

    .line 2162
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2164
    :cond_5
    iget-object v1, p0, Lnmr;->g:Locx;

    if-eqz v1, :cond_6

    .line 2165
    const/4 v1, 0x7

    iget-object v2, p0, Lnmr;->g:Locx;

    .line 2166
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2168
    :cond_6
    iget-object v1, p0, Lnmr;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2169
    iput v0, p0, Lnmr;->ai:I

    .line 2170
    return v0
.end method

.method public a(Loxn;)Lnmr;
    .locals 2

    .prologue
    .line 2178
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2179
    sparse-switch v0, :sswitch_data_0

    .line 2183
    iget-object v1, p0, Lnmr;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 2184
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnmr;->ah:Ljava/util/List;

    .line 2187
    :cond_1
    iget-object v1, p0, Lnmr;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2189
    :sswitch_0
    return-object p0

    .line 2194
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnmr;->d:Ljava/lang/Integer;

    goto :goto_0

    .line 2198
    :sswitch_2
    iget-object v0, p0, Lnmr;->a:Lofl;

    if-nez v0, :cond_2

    .line 2199
    new-instance v0, Lofl;

    invoke-direct {v0}, Lofl;-><init>()V

    iput-object v0, p0, Lnmr;->a:Lofl;

    .line 2201
    :cond_2
    iget-object v0, p0, Lnmr;->a:Lofl;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2205
    :sswitch_3
    iget-object v0, p0, Lnmr;->b:Logu;

    if-nez v0, :cond_3

    .line 2206
    new-instance v0, Logu;

    invoke-direct {v0}, Logu;-><init>()V

    iput-object v0, p0, Lnmr;->b:Logu;

    .line 2208
    :cond_3
    iget-object v0, p0, Lnmr;->b:Logu;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2212
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnmr;->c:Ljava/lang/Boolean;

    goto :goto_0

    .line 2216
    :sswitch_5
    iget-object v0, p0, Lnmr;->e:Logj;

    if-nez v0, :cond_4

    .line 2217
    new-instance v0, Logj;

    invoke-direct {v0}, Logj;-><init>()V

    iput-object v0, p0, Lnmr;->e:Logj;

    .line 2219
    :cond_4
    iget-object v0, p0, Lnmr;->e:Logj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2223
    :sswitch_6
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnmr;->f:Ljava/lang/Boolean;

    goto :goto_0

    .line 2227
    :sswitch_7
    iget-object v0, p0, Lnmr;->g:Locx;

    if-nez v0, :cond_5

    .line 2228
    new-instance v0, Locx;

    invoke-direct {v0}, Locx;-><init>()V

    iput-object v0, p0, Lnmr;->g:Locx;

    .line 2230
    :cond_5
    iget-object v0, p0, Lnmr;->g:Locx;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2179
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 2112
    iget-object v0, p0, Lnmr;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 2113
    const/4 v0, 0x1

    iget-object v1, p0, Lnmr;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2115
    :cond_0
    iget-object v0, p0, Lnmr;->a:Lofl;

    if-eqz v0, :cond_1

    .line 2116
    const/4 v0, 0x2

    iget-object v1, p0, Lnmr;->a:Lofl;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2118
    :cond_1
    iget-object v0, p0, Lnmr;->b:Logu;

    if-eqz v0, :cond_2

    .line 2119
    const/4 v0, 0x3

    iget-object v1, p0, Lnmr;->b:Logu;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2121
    :cond_2
    iget-object v0, p0, Lnmr;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 2122
    const/4 v0, 0x4

    iget-object v1, p0, Lnmr;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 2124
    :cond_3
    iget-object v0, p0, Lnmr;->e:Logj;

    if-eqz v0, :cond_4

    .line 2125
    const/4 v0, 0x5

    iget-object v1, p0, Lnmr;->e:Logj;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2127
    :cond_4
    iget-object v0, p0, Lnmr;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 2128
    const/4 v0, 0x6

    iget-object v1, p0, Lnmr;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 2130
    :cond_5
    iget-object v0, p0, Lnmr;->g:Locx;

    if-eqz v0, :cond_6

    .line 2131
    const/4 v0, 0x7

    iget-object v1, p0, Lnmr;->g:Locx;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2133
    :cond_6
    iget-object v0, p0, Lnmr;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2135
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2085
    invoke-virtual {p0, p1}, Lnmr;->a(Loxn;)Lnmr;

    move-result-object v0

    return-object v0
.end method
