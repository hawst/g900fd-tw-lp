.class public final Lllp;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static a:Ljava/lang/Integer;

.field private static b:Ljava/lang/Integer;

.field private static c:Ljava/lang/Integer;

.field private static d:Ljava/lang/Integer;


# direct methods
.method public static declared-synchronized a(Landroid/content/Context;)I
    .locals 2

    .prologue
    .line 25
    const-class v1, Lllp;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lllp;->a:Ljava/lang/Integer;

    if-nez v0, :cond_0

    .line 26
    invoke-static {p0}, Lllp;->e(Landroid/content/Context;)V

    .line 28
    :cond_0
    sget-object v0, Lllp;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit v1

    return v0

    .line 25
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized b(Landroid/content/Context;)I
    .locals 2

    .prologue
    .line 32
    const-class v1, Lllp;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lllp;->b:Ljava/lang/Integer;

    if-nez v0, :cond_0

    .line 33
    invoke-static {p0}, Lllp;->e(Landroid/content/Context;)V

    .line 35
    :cond_0
    sget-object v0, Lllp;->b:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit v1

    return v0

    .line 32
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized c(Landroid/content/Context;)I
    .locals 2

    .prologue
    .line 39
    const-class v1, Lllp;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lllp;->c:Ljava/lang/Integer;

    if-nez v0, :cond_0

    .line 40
    invoke-static {p0}, Lllp;->e(Landroid/content/Context;)V

    .line 42
    :cond_0
    sget-object v0, Lllp;->c:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit v1

    return v0

    .line 39
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized d(Landroid/content/Context;)I
    .locals 2

    .prologue
    .line 46
    const-class v1, Lllp;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lllp;->d:Ljava/lang/Integer;

    if-nez v0, :cond_0

    .line 47
    invoke-static {p0}, Lllp;->e(Landroid/content/Context;)V

    .line 49
    :cond_0
    sget-object v0, Lllp;->d:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit v1

    return v0

    .line 46
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static e(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 53
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lllp;->a:Ljava/lang/Integer;

    .line 54
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lllp;->b:Ljava/lang/Integer;

    .line 55
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lllp;->c:Ljava/lang/Integer;

    .line 56
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lllp;->d:Ljava/lang/Integer;

    .line 58
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 59
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 60
    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 61
    iget v1, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sput-object v1, Lllp;->a:Ljava/lang/Integer;

    .line 63
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    .line 64
    if-eqz v0, :cond_0

    const-string v1, "DEVELOPMENT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 65
    :cond_0
    const v0, 0x3b9ac9ff

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lllp;->b:Ljava/lang/Integer;

    .line 66
    const v0, 0x3b9ac9ff

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lllp;->c:Ljava/lang/Integer;

    .line 67
    const v0, 0x3b9ac9ff

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lllp;->d:Ljava/lang/Integer;

    .line 85
    :cond_1
    :goto_0
    return-void

    .line 69
    :cond_2
    const-string v1, "\\."

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 70
    array-length v1, v0

    if-lez v1, :cond_3

    .line 71
    const/4 v1, 0x0

    aget-object v1, v0, v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sput-object v1, Lllp;->b:Ljava/lang/Integer;

    .line 73
    :cond_3
    array-length v1, v0

    if-le v1, v3, :cond_4

    .line 74
    const/4 v1, 0x1

    aget-object v1, v0, v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sput-object v1, Lllp;->c:Ljava/lang/Integer;

    .line 76
    :cond_4
    array-length v1, v0

    if-le v1, v4, :cond_1

    .line 77
    const/4 v1, 0x2

    aget-object v0, v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lllp;->d:Ljava/lang/Integer;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 84
    :catch_0
    move-exception v0

    goto :goto_0

    .line 85
    :catch_1
    move-exception v0

    goto :goto_0
.end method
