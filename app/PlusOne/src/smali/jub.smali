.class public final Ljub;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lizo;


# instance fields
.field private final a:Lnzi;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lnzi;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Ljub;->a:Lnzi;

    .line 24
    return-void
.end method

.method public static a(Lnzi;)Z
    .locals 1

    .prologue
    .line 84
    if-eqz p0, :cond_0

    iget-object v0, p0, Lnzi;->b:Lpla;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lnzi;->b:Lpla;

    iget-object v0, v0, Lpla;->a:[Lpme;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lnzi;->b:Lpla;

    iget-object v0, v0, Lpla;->a:[Lpme;

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lpme;I)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 210
    iget-object v1, p0, Lpme;->b:Lpmd;

    if-nez v1, :cond_1

    .line 221
    :cond_0
    :goto_0
    return v0

    .line 213
    :cond_1
    iget-object v1, p0, Lpme;->b:Lpmd;

    iget-object v1, v1, Lpmd;->a:[I

    if-eqz v1, :cond_0

    .line 216
    iget-object v1, p0, Lpme;->b:Lpmd;

    iget-object v2, v1, Lpmd;->a:[I

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_0

    aget v4, v2, v1

    .line 217
    if-ne v4, p1, :cond_2

    .line 218
    const/4 v0, 0x1

    goto :goto_0

    .line 216
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public static b(Lnzi;)Z
    .locals 13

    .prologue
    const/4 v12, 0x3

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 184
    invoke-static {p0}, Ljub;->a(Lnzi;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 185
    invoke-static {p0}, Ljub;->a(Lnzi;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lnzi;->b:Lpla;

    iget-object v7, v0, Lpla;->a:[Lpme;

    array-length v8, v7

    move v6, v2

    :goto_0
    if-ge v6, v8, :cond_5

    aget-object v0, v7, v6

    iget-object v3, v0, Lpme;->b:Lpmd;

    if-eqz v3, :cond_4

    iget-object v3, v0, Lpme;->b:Lpmd;

    iget-object v3, v3, Lpmd;->a:[I

    if-eqz v3, :cond_4

    iget-object v0, v0, Lpme;->b:Lpmd;

    iget-object v9, v0, Lpmd;->a:[I

    array-length v10, v9

    move v3, v2

    move v4, v2

    move v5, v2

    :goto_1
    if-ge v3, v10, :cond_2

    aget v11, v9, v3

    if-ne v11, v1, :cond_0

    move v0, v1

    :goto_2
    or-int/2addr v5, v0

    if-ne v11, v12, :cond_1

    move v0, v1

    :goto_3
    or-int/2addr v4, v0

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_0
    move v0, v2

    goto :goto_2

    :cond_1
    move v0, v2

    goto :goto_3

    :cond_2
    if-eqz v5, :cond_4

    if-nez v4, :cond_4

    move v0, v1

    :goto_4
    if-eqz v0, :cond_6

    .line 203
    :cond_3
    :goto_5
    return v1

    .line 185
    :cond_4
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    :cond_5
    move v0, v2

    goto :goto_4

    .line 188
    :cond_6
    iget-object v0, p0, Lnzi;->b:Lpla;

    iget-object v6, v0, Lpla;->a:[Lpme;

    array-length v7, v6

    move v5, v2

    :goto_6
    if-ge v5, v7, :cond_9

    aget-object v0, v6, v5

    .line 189
    iget-object v3, v0, Lpme;->b:Lpmd;

    if-eqz v3, :cond_3

    iget-object v3, v0, Lpme;->b:Lpmd;

    iget-object v3, v3, Lpmd;->a:[I

    if-eqz v3, :cond_3

    .line 195
    iget-object v0, v0, Lpme;->b:Lpmd;

    iget-object v8, v0, Lpmd;->a:[I

    array-length v9, v8

    move v3, v2

    move v4, v2

    :goto_7
    if-ge v3, v9, :cond_8

    aget v0, v8, v3

    .line 196
    if-ne v0, v12, :cond_7

    move v0, v1

    :goto_8
    or-int/2addr v4, v0

    .line 195
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_7

    :cond_7
    move v0, v2

    .line 196
    goto :goto_8

    .line 198
    :cond_8
    if-eqz v4, :cond_3

    .line 188
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_6

    :cond_9
    move v1, v2

    .line 203
    goto :goto_5
.end method

.method public static c(Lnzi;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 242
    invoke-static {p0}, Ljub;->a(Lnzi;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 254
    :cond_0
    :goto_0
    return v0

    .line 247
    :cond_1
    iget-object v2, p0, Lnzi;->b:Lpla;

    iget-object v3, v2, Lpla;->a:[Lpme;

    array-length v4, v3

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_0

    aget-object v5, v3, v2

    .line 248
    invoke-static {v5, v1}, Ljub;->a(Lpme;I)Z

    move-result v5

    if-eqz v5, :cond_2

    move v0, v1

    .line 249
    goto :goto_0

    .line 247
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 38
    iget-object v0, p0, Ljub;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 39
    iget-object v0, p0, Ljub;->a:Lnzi;

    .line 40
    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    const/4 v1, 0x0

    .line 39
    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljub;->b:Ljava/lang/String;

    .line 43
    :cond_0
    iget-object v0, p0, Ljub;->b:Ljava/lang/String;

    return-object v0
.end method

.method public a(Lizo;)Z
    .locals 2

    .prologue
    .line 32
    instance-of v0, p1, Ljub;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljub;->a:Lnzi;

    check-cast p1, Ljub;

    iget-object v1, p1, Ljub;->a:Lnzi;

    .line 33
    invoke-static {v0, v1}, Loxu;->a(Loxu;Loxu;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Ljub;->a:Lnzi;

    invoke-virtual {v0}, Lnzi;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
