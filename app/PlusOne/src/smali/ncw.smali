.class public final Lncw;
.super Loxq;
.source "PG"


# instance fields
.field public a:I

.field private b:Lncx;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 468
    invoke-direct {p0}, Loxq;-><init>()V

    .line 476
    const/4 v0, 0x0

    iput-object v0, p0, Lncw;->b:Lncx;

    .line 479
    const/high16 v0, -0x80000000

    iput v0, p0, Lncw;->a:I

    .line 468
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 496
    const/4 v0, 0x0

    .line 497
    iget-object v1, p0, Lncw;->b:Lncx;

    if-eqz v1, :cond_0

    .line 498
    const/4 v0, 0x1

    iget-object v1, p0, Lncw;->b:Lncx;

    .line 499
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 501
    :cond_0
    iget v1, p0, Lncw;->a:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_1

    .line 502
    const/4 v1, 0x2

    iget v2, p0, Lncw;->a:I

    .line 503
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 505
    :cond_1
    iget-object v1, p0, Lncw;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 506
    iput v0, p0, Lncw;->ai:I

    .line 507
    return v0
.end method

.method public a(Loxn;)Lncw;
    .locals 2

    .prologue
    .line 515
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 516
    sparse-switch v0, :sswitch_data_0

    .line 520
    iget-object v1, p0, Lncw;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 521
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lncw;->ah:Ljava/util/List;

    .line 524
    :cond_1
    iget-object v1, p0, Lncw;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 526
    :sswitch_0
    return-object p0

    .line 531
    :sswitch_1
    iget-object v0, p0, Lncw;->b:Lncx;

    if-nez v0, :cond_2

    .line 532
    new-instance v0, Lncx;

    invoke-direct {v0}, Lncx;-><init>()V

    iput-object v0, p0, Lncw;->b:Lncx;

    .line 534
    :cond_2
    iget-object v0, p0, Lncw;->b:Lncx;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 538
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 539
    if-eqz v0, :cond_3

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    .line 541
    :cond_3
    iput v0, p0, Lncw;->a:I

    goto :goto_0

    .line 543
    :cond_4
    const/4 v0, 0x0

    iput v0, p0, Lncw;->a:I

    goto :goto_0

    .line 516
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 484
    iget-object v0, p0, Lncw;->b:Lncx;

    if-eqz v0, :cond_0

    .line 485
    const/4 v0, 0x1

    iget-object v1, p0, Lncw;->b:Lncx;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 487
    :cond_0
    iget v0, p0, Lncw;->a:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 488
    const/4 v0, 0x2

    iget v1, p0, Lncw;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 490
    :cond_1
    iget-object v0, p0, Lncw;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 492
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 464
    invoke-virtual {p0, p1}, Lncw;->a(Loxn;)Lncw;

    move-result-object v0

    return-object v0
.end method
