.class public final Looh;
.super Loxq;
.source "PG"


# instance fields
.field public a:I

.field public b:Ljava/lang/Integer;

.field public c:[Looi;

.field public d:[Looj;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 161
    invoke-direct {p0}, Loxq;-><init>()V

    .line 336
    const/high16 v0, -0x80000000

    iput v0, p0, Looh;->a:I

    .line 341
    sget-object v0, Looi;->a:[Looi;

    iput-object v0, p0, Looh;->c:[Looi;

    .line 344
    sget-object v0, Looj;->a:[Looj;

    iput-object v0, p0, Looh;->d:[Looj;

    .line 161
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 375
    .line 376
    iget v0, p0, Looh;->a:I

    const/high16 v2, -0x80000000

    if-eq v0, v2, :cond_5

    .line 377
    const/4 v0, 0x1

    iget v2, p0, Looh;->a:I

    .line 378
    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 380
    :goto_0
    iget-object v2, p0, Looh;->b:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    .line 381
    const/4 v2, 0x2

    iget-object v3, p0, Looh;->b:Ljava/lang/Integer;

    .line 382
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 384
    :cond_0
    iget-object v2, p0, Looh;->c:[Looi;

    if-eqz v2, :cond_2

    .line 385
    iget-object v3, p0, Looh;->c:[Looi;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 386
    if-eqz v5, :cond_1

    .line 387
    const/4 v6, 0x3

    .line 388
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 385
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 392
    :cond_2
    iget-object v2, p0, Looh;->d:[Looj;

    if-eqz v2, :cond_4

    .line 393
    iget-object v2, p0, Looh;->d:[Looj;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 394
    if-eqz v4, :cond_3

    .line 395
    const/4 v5, 0x4

    .line 396
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 393
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 400
    :cond_4
    iget-object v1, p0, Looh;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 401
    iput v0, p0, Looh;->ai:I

    .line 402
    return v0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Looh;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 410
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 411
    sparse-switch v0, :sswitch_data_0

    .line 415
    iget-object v2, p0, Looh;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 416
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Looh;->ah:Ljava/util/List;

    .line 419
    :cond_1
    iget-object v2, p0, Looh;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 421
    :sswitch_0
    return-object p0

    .line 426
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 427
    if-eqz v0, :cond_2

    const/4 v2, 0x1

    if-eq v0, v2, :cond_2

    const/4 v2, 0x2

    if-eq v0, v2, :cond_2

    const/4 v2, 0x3

    if-eq v0, v2, :cond_2

    const/4 v2, 0x4

    if-eq v0, v2, :cond_2

    const/4 v2, 0x5

    if-ne v0, v2, :cond_3

    .line 433
    :cond_2
    iput v0, p0, Looh;->a:I

    goto :goto_0

    .line 435
    :cond_3
    iput v1, p0, Looh;->a:I

    goto :goto_0

    .line 440
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Looh;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 444
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 445
    iget-object v0, p0, Looh;->c:[Looi;

    if-nez v0, :cond_5

    move v0, v1

    .line 446
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Looi;

    .line 447
    iget-object v3, p0, Looh;->c:[Looi;

    if-eqz v3, :cond_4

    .line 448
    iget-object v3, p0, Looh;->c:[Looi;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 450
    :cond_4
    iput-object v2, p0, Looh;->c:[Looi;

    .line 451
    :goto_2
    iget-object v2, p0, Looh;->c:[Looi;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 452
    iget-object v2, p0, Looh;->c:[Looi;

    new-instance v3, Looi;

    invoke-direct {v3}, Looi;-><init>()V

    aput-object v3, v2, v0

    .line 453
    iget-object v2, p0, Looh;->c:[Looi;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 454
    invoke-virtual {p1}, Loxn;->a()I

    .line 451
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 445
    :cond_5
    iget-object v0, p0, Looh;->c:[Looi;

    array-length v0, v0

    goto :goto_1

    .line 457
    :cond_6
    iget-object v2, p0, Looh;->c:[Looi;

    new-instance v3, Looi;

    invoke-direct {v3}, Looi;-><init>()V

    aput-object v3, v2, v0

    .line 458
    iget-object v2, p0, Looh;->c:[Looi;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 462
    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 463
    iget-object v0, p0, Looh;->d:[Looj;

    if-nez v0, :cond_8

    move v0, v1

    .line 464
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Looj;

    .line 465
    iget-object v3, p0, Looh;->d:[Looj;

    if-eqz v3, :cond_7

    .line 466
    iget-object v3, p0, Looh;->d:[Looj;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 468
    :cond_7
    iput-object v2, p0, Looh;->d:[Looj;

    .line 469
    :goto_4
    iget-object v2, p0, Looh;->d:[Looj;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    .line 470
    iget-object v2, p0, Looh;->d:[Looj;

    new-instance v3, Looj;

    invoke-direct {v3}, Looj;-><init>()V

    aput-object v3, v2, v0

    .line 471
    iget-object v2, p0, Looh;->d:[Looj;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 472
    invoke-virtual {p1}, Loxn;->a()I

    .line 469
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 463
    :cond_8
    iget-object v0, p0, Looh;->d:[Looj;

    array-length v0, v0

    goto :goto_3

    .line 475
    :cond_9
    iget-object v2, p0, Looh;->d:[Looj;

    new-instance v3, Looj;

    invoke-direct {v3}, Looj;-><init>()V

    aput-object v3, v2, v0

    .line 476
    iget-object v2, p0, Looh;->d:[Looj;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 411
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 349
    iget v1, p0, Looh;->a:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 350
    const/4 v1, 0x1

    iget v2, p0, Looh;->a:I

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 352
    :cond_0
    iget-object v1, p0, Looh;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 353
    const/4 v1, 0x2

    iget-object v2, p0, Looh;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 355
    :cond_1
    iget-object v1, p0, Looh;->c:[Looi;

    if-eqz v1, :cond_3

    .line 356
    iget-object v2, p0, Looh;->c:[Looi;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 357
    if-eqz v4, :cond_2

    .line 358
    const/4 v5, 0x3

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 356
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 362
    :cond_3
    iget-object v1, p0, Looh;->d:[Looj;

    if-eqz v1, :cond_5

    .line 363
    iget-object v1, p0, Looh;->d:[Looj;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    .line 364
    if-eqz v3, :cond_4

    .line 365
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 363
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 369
    :cond_5
    iget-object v0, p0, Looh;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 371
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 157
    invoke-virtual {p0, p1}, Looh;->a(Loxn;)Looh;

    move-result-object v0

    return-object v0
.end method
