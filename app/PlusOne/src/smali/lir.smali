.class public final Llir;
.super Lljg;
.source "PG"

# interfaces
.implements Llip;


# instance fields
.field private final b:Landroid/text/Spanned;

.field private final c:Lljv;

.field private d:Llju;


# direct methods
.method public constructor <init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLljv;)V
    .locals 1

    .prologue
    .line 96
    invoke-direct/range {p0 .. p7}, Lljg;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    .line 98
    iput-object p8, p0, Llir;->c:Lljv;

    .line 99
    instance-of v0, p1, Landroid/text/Spanned;

    if-eqz v0, :cond_0

    .line 102
    check-cast p1, Landroid/text/Spanned;

    iput-object p1, p0, Llir;->b:Landroid/text/Spanned;

    .line 106
    :goto_0
    return-void

    .line 104
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Llir;->b:Landroid/text/Spanned;

    goto :goto_0
.end method

.method public static a(Landroid/text/SpannableStringBuilder;)Landroid/text/SpannableStringBuilder;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 260
    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    const-class v1, Landroid/text/style/URLSpan;

    invoke-virtual {p0, v2, v0, v1}, Landroid/text/SpannableStringBuilder;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/URLSpan;

    move v1, v2

    .line 261
    :goto_0
    array-length v3, v0

    if-ge v1, v3, :cond_0

    .line 262
    aget-object v3, v0, v1

    .line 263
    new-instance v4, Llju;

    invoke-virtual {v3}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5, v2}, Llju;-><init>(Ljava/lang/String;Z)V

    .line 265
    invoke-virtual {p0, v3}, Landroid/text/SpannableStringBuilder;->getSpanStart(Ljava/lang/Object;)I

    move-result v5

    invoke-virtual {p0, v3}, Landroid/text/SpannableStringBuilder;->getSpanEnd(Ljava/lang/Object;)I

    move-result v6

    .line 266
    invoke-virtual {p0, v3}, Landroid/text/SpannableStringBuilder;->getSpanFlags(Ljava/lang/Object;)I

    move-result v7

    .line 265
    invoke-virtual {p0, v4, v5, v6, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 267
    invoke-virtual {p0, v3}, Landroid/text/SpannableStringBuilder;->removeSpan(Ljava/lang/Object;)V

    .line 261
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 269
    :cond_0
    return-object p0
.end method

.method public static a(Landroid/text/Spanned;)Landroid/text/SpannableStringBuilder;
    .locals 1

    .prologue
    .line 256
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0, p0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    invoke-static {v0}, Llir;->a(Landroid/text/SpannableStringBuilder;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;
    .locals 1

    .prologue
    .line 218
    const/4 v0, 0x0

    invoke-static {p0, v0}, Llir;->a(Ljava/lang/String;Landroid/text/Html$TagHandler;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Landroid/text/Html$TagHandler;)Landroid/text/SpannableStringBuilder;
    .locals 2

    .prologue
    .line 246
    if-nez p0, :cond_0

    .line 248
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 252
    :goto_0
    return-object v0

    .line 251
    :cond_0
    invoke-static {p0, p1}, Llhv;->a(Ljava/lang/String;Landroid/text/Html$TagHandler;)Landroid/text/Spanned;

    move-result-object v0

    .line 252
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    invoke-static {v1}, Llir;->a(Landroid/text/SpannableStringBuilder;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/text/TextPaint;Ljava/lang/CharSequence;IIFLljv;)Llir;
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 48
    if-nez p3, :cond_2

    .line 51
    const-string v1, ""

    move v3, v7

    .line 89
    :cond_0
    :goto_0
    new-instance v0, Llir;

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    move-object v2, p0

    move v5, p4

    move-object v8, p5

    invoke-direct/range {v0 .. v8}, Llir;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLljv;)V

    :cond_1
    return-object v0

    .line 52
    :cond_2
    const/4 v0, 0x1

    if-ne p3, v0, :cond_4

    .line 53
    invoke-static {p0, p1}, Llib;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;)I

    move-result v0

    .line 54
    invoke-static {p2, v7}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 56
    if-ne v3, v0, :cond_3

    move-object v1, p1

    .line 57
    goto :goto_0

    .line 60
    :cond_3
    sget-object v0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {p1, p0, v3, v0, v9}, Llib;->a(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/TextUtils$TruncateAt;Landroid/text/TextUtils$EllipsizeCallback;)Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_0

    .line 64
    :cond_4
    new-instance v0, Llir;

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v5, 0x3f800000    # 1.0f

    move-object v1, p1

    move-object v2, p0

    move v3, p2

    move-object v8, p5

    invoke-direct/range {v0 .. v8}, Llir;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLljv;)V

    .line 66
    invoke-virtual {v0}, Llir;->getLineCount()I

    move-result v1

    if-le v1, p3, :cond_1

    .line 70
    invoke-static {p2, v7}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-virtual {v0}, Llir;->getWidth()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 74
    add-int/lit8 v1, p3, -0x2

    invoke-virtual {v0, v1}, Llir;->getLineEnd(I)I

    move-result v2

    .line 75
    new-instance v1, Landroid/text/SpannableStringBuilder;

    .line 76
    invoke-interface {p1, v7, v2}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 77
    instance-of v4, p1, Landroid/text/Spanned;

    .line 78
    if-eqz v4, :cond_5

    .line 79
    invoke-static {}, Llib;->a()Llie;

    move-result-object v0

    .line 80
    :goto_1
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v5

    invoke-interface {p1, v2, v5}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v5

    sget-object v8, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {v5, p0, p2, v8, v0}, Llib;->a(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/TextUtils$TruncateAt;Landroid/text/TextUtils$EllipsizeCallback;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 82
    if-eqz v4, :cond_0

    .line 83
    check-cast p1, Landroid/text/Spanned;

    invoke-static {p1, v2, v1, v0}, Llib;->a(Landroid/text/Spanned;ILandroid/text/SpannableStringBuilder;Llie;)V

    goto :goto_0

    :cond_5
    move-object v0, v9

    .line 79
    goto :goto_1
.end method

.method public static b(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;
    .locals 2

    .prologue
    .line 227
    if-nez p0, :cond_0

    .line 229
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 234
    :goto_0
    return-object v0

    .line 231
    :cond_0
    new-instance v0, Landroid/text/SpannableStringBuilder;

    const/4 v1, 0x0

    .line 232
    invoke-static {p0, v1}, Llhv;->a(Ljava/lang/String;Landroid/text/Html$TagHandler;)Landroid/text/Spanned;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 233
    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/text/util/Linkify;->addLinks(Landroid/text/Spannable;I)Z

    .line 234
    invoke-static {v0}, Llir;->a(Landroid/text/SpannableStringBuilder;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a(Llip;Llip;)I
    .locals 1

    .prologue
    .line 288
    sget-object v0, Llip;->a_:Lliq;

    invoke-virtual {v0, p1, p2}, Lliq;->a(Llip;Llip;)I

    move-result v0

    return v0
.end method

.method public a()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Llir;->a:Landroid/graphics/Rect;

    return-object v0
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 279
    return-void
.end method

.method public a(III)Z
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 121
    const/4 v0, 0x3

    if-ne p3, v0, :cond_1

    .line 122
    iget-object v0, p0, Llir;->d:Llju;

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Llir;->d:Llju;

    invoke-virtual {v0, v2}, Llju;->a(Z)V

    .line 124
    iput-object v5, p0, Llir;->d:Llju;

    :cond_0
    move v0, v1

    .line 182
    :goto_0
    return v0

    .line 129
    :cond_1
    iget-object v0, p0, Llir;->b:Landroid/text/Spanned;

    if-nez v0, :cond_2

    move v0, v2

    .line 130
    goto :goto_0

    .line 133
    :cond_2
    iget-object v0, p0, Llir;->a:Landroid/graphics/Rect;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-nez v0, :cond_4

    .line 134
    if-ne p3, v1, :cond_3

    .line 135
    iget-object v0, p0, Llir;->d:Llju;

    if-eqz v0, :cond_3

    .line 136
    iget-object v0, p0, Llir;->d:Llju;

    invoke-virtual {v0, v2}, Llju;->a(Z)V

    .line 137
    iput-object v5, p0, Llir;->d:Llju;

    :cond_3
    move v0, v2

    .line 140
    goto :goto_0

    .line 143
    :cond_4
    iget-object v0, p0, Llir;->a:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    sub-int v0, p2, v0

    int-to-float v0, v0

    invoke-static {v4, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-virtual {p0}, Llir;->getHeight()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    int-to-float v3, v3

    invoke-static {v3, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p0, v0}, Llir;->getLineForVertical(I)I

    move-result v0

    .line 144
    iget-object v3, p0, Llir;->a:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    sub-int v3, p1, v3

    int-to-float v3, v3

    invoke-static {v4, v3}, Ljava/lang/Math;->max(FF)F

    move-result v3

    invoke-virtual {p0}, Llir;->getWidth()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    int-to-float v4, v4

    invoke-static {v4, v3}, Ljava/lang/Math;->min(FF)F

    move-result v3

    .line 145
    invoke-virtual {p0, v0, v3}, Llir;->getOffsetForHorizontal(IF)I

    move-result v4

    .line 146
    if-gez v4, :cond_5

    move v0, v2

    .line 147
    goto :goto_0

    .line 148
    :cond_5
    invoke-virtual {p0, v0}, Llir;->getLineWidth(I)F

    move-result v0

    cmpl-float v0, v3, v0

    if-lez v0, :cond_6

    move v0, v2

    .line 150
    goto :goto_0

    .line 153
    :cond_6
    iget-object v0, p0, Llir;->b:Landroid/text/Spanned;

    const-class v3, Llju;

    invoke-interface {v0, v4, v4, v3}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Llju;

    .line 154
    array-length v3, v0

    if-nez v3, :cond_7

    move v0, v2

    .line 155
    goto :goto_0

    .line 158
    :cond_7
    packed-switch p3, :pswitch_data_0

    :cond_8
    :goto_1
    move v0, v1

    .line 182
    goto :goto_0

    .line 160
    :pswitch_0
    aget-object v0, v0, v2

    iput-object v0, p0, Llir;->d:Llju;

    .line 161
    iget-object v0, p0, Llir;->d:Llju;

    invoke-virtual {v0, v1}, Llju;->a(Z)V

    goto :goto_1

    .line 166
    :pswitch_1
    iget-object v3, p0, Llir;->d:Llju;

    aget-object v4, v0, v2

    if-ne v3, v4, :cond_9

    iget-object v3, p0, Llir;->c:Lljv;

    if-eqz v3, :cond_9

    .line 167
    iget-object v3, p0, Llir;->c:Lljv;

    aget-object v0, v0, v2

    invoke-interface {v3, v0}, Lljv;->a(Landroid/text/style/URLSpan;)V

    .line 170
    :cond_9
    iget-object v0, p0, Llir;->d:Llju;

    if-eqz v0, :cond_8

    .line 171
    iget-object v0, p0, Llir;->d:Llju;

    invoke-virtual {v0, v2}, Llju;->a(Z)V

    .line 172
    iput-object v5, p0, Llir;->d:Llju;

    goto :goto_1

    .line 158
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 25
    check-cast p1, Llip;

    check-cast p2, Llip;

    invoke-virtual {p0, p1, p2}, Llir;->a(Llip;Llip;)I

    move-result v0

    return v0
.end method
