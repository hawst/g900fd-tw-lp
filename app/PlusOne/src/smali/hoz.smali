.class public final Lhoz;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static a:J


# instance fields
.field private final b:I

.field private final c:Ljava/lang/Exception;

.field private final d:Ljava/lang/String;

.field private final e:J

.field private f:I

.field private g:Landroid/os/Bundle;

.field private h:[B


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 30
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lhoz;->a:J

    return-void
.end method

.method public constructor <init>(ILjava/lang/Exception;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    const/4 v0, 0x3

    iput v0, p0, Lhoz;->f:I

    .line 63
    iput p1, p0, Lhoz;->b:I

    .line 64
    iput-object p2, p0, Lhoz;->c:Ljava/lang/Exception;

    .line 65
    iput-object p3, p0, Lhoz;->d:Ljava/lang/String;

    .line 66
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lhoz;->e:J

    .line 67
    return-void
.end method

.method constructor <init>(ILjava/lang/Exception;Ljava/lang/String;[BJ)V
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    const/4 v0, 0x3

    iput v0, p0, Lhoz;->f:I

    .line 74
    iput p1, p0, Lhoz;->b:I

    .line 75
    iput-object p2, p0, Lhoz;->c:Ljava/lang/Exception;

    .line 76
    iput-object p3, p0, Lhoz;->d:Ljava/lang/String;

    .line 77
    iput-object p4, p0, Lhoz;->h:[B

    .line 78
    iput-wide p5, p0, Lhoz;->e:J

    .line 79
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 51
    if-eqz p1, :cond_0

    const/16 v0, 0xc8

    :goto_0
    invoke-direct {p0, v0, v1, v1}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    .line 52
    return-void

    .line 51
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lhoz;)Z
    .locals 1

    .prologue
    .line 136
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lhoz;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 85
    iget v0, p0, Lhoz;->b:I

    return v0
.end method

.method a(I)V
    .locals 0

    .prologue
    .line 193
    iput p1, p0, Lhoz;->f:I

    .line 194
    return-void
.end method

.method public b()Ljava/lang/Exception;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lhoz;->c:Ljava/lang/Exception;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lhoz;->d:Ljava/lang/String;

    return-object v0
.end method

.method public d()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 113
    invoke-virtual {p0}, Lhoz;->j()V

    .line 114
    iget-object v0, p0, Lhoz;->g:Landroid/os/Bundle;

    if-nez v0, :cond_0

    .line 115
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lhoz;->g:Landroid/os/Bundle;

    .line 117
    :cond_0
    iget-object v0, p0, Lhoz;->g:Landroid/os/Bundle;

    return-object v0
.end method

.method e()[B
    .locals 1

    .prologue
    .line 121
    invoke-virtual {p0}, Lhoz;->i()V

    .line 122
    iget-object v0, p0, Lhoz;->h:[B

    return-object v0
.end method

.method public f()Z
    .locals 2

    .prologue
    .line 129
    iget v0, p0, Lhoz;->b:I

    const/16 v1, 0xc8

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method g()J
    .locals 2

    .prologue
    .line 140
    iget-wide v0, p0, Lhoz;->e:J

    return-wide v0
.end method

.method h()Z
    .locals 4

    .prologue
    .line 144
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lhoz;->e:J

    sub-long/2addr v0, v2

    sget-wide v2, Lhoz;->a:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method i()V
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lhoz;->g:Landroid/os/Bundle;

    if-nez v0, :cond_0

    .line 177
    :goto_0
    return-void

    .line 175
    :cond_0
    iget-object v0, p0, Lhoz;->g:Landroid/os/Bundle;

    invoke-static {v0}, Llsi;->a(Landroid/os/Parcelable;)[B

    move-result-object v0

    iput-object v0, p0, Lhoz;->h:[B

    .line 176
    const/4 v0, 0x0

    iput-object v0, p0, Lhoz;->g:Landroid/os/Bundle;

    goto :goto_0
.end method

.method j()V
    .locals 2

    .prologue
    .line 183
    iget-object v0, p0, Lhoz;->h:[B

    if-nez v0, :cond_0

    .line 190
    :goto_0
    return-void

    .line 186
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    .line 187
    iget-object v0, p0, Lhoz;->h:[B

    invoke-static {v0, v1}, Llsi;->a([BLjava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    iput-object v0, p0, Lhoz;->g:Landroid/os/Bundle;

    .line 188
    iget-object v0, p0, Lhoz;->g:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 189
    const/4 v0, 0x0

    iput-object v0, p0, Lhoz;->h:[B

    goto :goto_0
.end method

.method k()I
    .locals 1

    .prologue
    .line 197
    iget v0, p0, Lhoz;->f:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 149
    invoke-virtual {p0}, Lhoz;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 150
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    const-string v1, "TaskResult(message=%s, age=%s, errorCode=%d, exception=%s)"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lhoz;->d:Ljava/lang/String;

    aput-object v3, v2, v5

    iget-wide v4, p0, Lhoz;->e:J

    .line 152
    invoke-static {v4, v5}, Llse;->a(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    iget v3, p0, Lhoz;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    iget-object v3, p0, Lhoz;->c:Ljava/lang/Exception;

    aput-object v3, v2, v8

    .line 150
    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 163
    :goto_0
    return-object v0

    .line 156
    :cond_0
    iget-object v0, p0, Lhoz;->g:Landroid/os/Bundle;

    if-eqz v0, :cond_1

    .line 157
    iget-object v0, p0, Lhoz;->g:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->size()I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x13

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Bundle("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 163
    :goto_1
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    const-string v2, "TaskResult(message=%s, age=%s, extras=%s)"

    new-array v3, v8, [Ljava/lang/Object;

    iget-object v4, p0, Lhoz;->d:Ljava/lang/String;

    aput-object v4, v3, v5

    iget-wide v4, p0, Lhoz;->e:J

    .line 165
    invoke-static {v4, v5}, Llse;->a(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    aput-object v0, v3, v7

    .line 163
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 158
    :cond_1
    iget-object v0, p0, Lhoz;->h:[B

    if-eqz v0, :cond_2

    .line 159
    iget-object v0, p0, Lhoz;->h:[B

    array-length v0, v0

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x11

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "byte["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 161
    :cond_2
    const-string v0, "null"

    goto :goto_1
.end method
