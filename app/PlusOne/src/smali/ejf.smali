.class public final Lejf;
.super Lhxz;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lhxz",
        "<",
        "Lokb;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:I

.field private final c:I

.field private final d:Ljava/lang/String;

.field private final e:I

.field private final f:J

.field private g:Lokb;

.field private h:Ldp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldo",
            "<",
            "Lokb;",
            ">.dp;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;IILjava/lang/String;IJ)V
    .locals 2

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lhxz;-><init>(Landroid/content/Context;)V

    .line 38
    new-instance v0, Ldp;

    invoke-direct {v0, p0}, Ldp;-><init>(Ldo;)V

    iput-object v0, p0, Lejf;->h:Ldp;

    .line 52
    iput p2, p0, Lejf;->b:I

    .line 53
    iput p3, p0, Lejf;->c:I

    .line 54
    iput-object p4, p0, Lejf;->d:Ljava/lang/String;

    .line 55
    iput p5, p0, Lejf;->e:I

    .line 56
    iput-wide p6, p0, Lejf;->f:J

    .line 57
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 41
    invoke-static {p0}, Ldwe;->a(Landroid/content/Context;)Ldwe;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Ldwe;->a(ILjava/lang/String;)V

    .line 43
    invoke-static {p0}, Ldwe;->a(Landroid/content/Context;)Ldwe;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p1}, Ldwe;->a(ILjava/lang/String;)V

    .line 45
    invoke-static {p0}, Ldwe;->a(Landroid/content/Context;)Ldwe;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1, p1}, Ldwe;->a(ILjava/lang/String;)V

    .line 47
    return-void
.end method


# virtual methods
.method protected D_()Z
    .locals 4

    .prologue
    .line 178
    invoke-virtual {p0}, Lejf;->n()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->c:Landroid/net/Uri;

    const/4 v2, 0x0

    iget-object v3, p0, Lejf;->h:Ldp;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 180
    const/4 v0, 0x1

    return v0
.end method

.method public a(Lokb;)V
    .locals 1

    .prologue
    .line 165
    invoke-virtual {p0}, Lejf;->r()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 174
    :cond_0
    :goto_0
    return-void

    .line 169
    :cond_1
    iput-object p1, p0, Lejf;->g:Lokb;

    .line 171
    invoke-virtual {p0}, Lejf;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 172
    invoke-super {p0, p1}, Lhxz;->b(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public synthetic b(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 29
    check-cast p1, Lokb;

    invoke-virtual {p0, p1}, Lejf;->a(Lokb;)V

    return-void
.end method

.method protected i()V
    .locals 1

    .prologue
    .line 191
    invoke-super {p0}, Lhxz;->i()V

    .line 192
    const/4 v0, 0x0

    iput-object v0, p0, Lejf;->g:Lokb;

    .line 193
    return-void
.end method

.method public synthetic j()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lejf;->l()Lokb;

    move-result-object v0

    return-object v0
.end method

.method protected k()Z
    .locals 2

    .prologue
    .line 185
    invoke-virtual {p0}, Lejf;->n()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lejf;->h:Ldp;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 186
    const/4 v0, 0x1

    return v0
.end method

.method public l()Lokb;
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 61
    .line 63
    iget v4, p0, Lejf;->b:I

    .line 66
    iget v0, p0, Lejf;->c:I

    packed-switch v0, :pswitch_data_0

    .line 77
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 68
    :pswitch_0
    const/4 v0, 0x1

    move v1, v0

    .line 81
    :goto_0
    new-instance v5, Landroid/os/ConditionVariable;

    invoke-direct {v5}, Landroid/os/ConditionVariable;-><init>()V

    .line 84
    iget-object v0, p0, Lejf;->g:Lokb;

    if-nez v0, :cond_6

    .line 85
    invoke-virtual {p0}, Lejf;->n()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Ldwe;->a(Landroid/content/Context;)Ldwe;

    move-result-object v0

    iget-object v3, p0, Lejf;->d:Ljava/lang/String;

    iget-wide v6, p0, Lejf;->f:J

    invoke-virtual {v0, v1, v3, v6, v7}, Ldwe;->a(ILjava/lang/String;J)[B

    move-result-object v0

    .line 87
    if-eqz v0, :cond_3

    .line 89
    :try_start_0
    new-instance v3, Lokb;

    invoke-direct {v3}, Lokb;-><init>()V

    invoke-static {v3, v0}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lokb;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v3, v0

    .line 95
    :goto_1
    if-nez v3, :cond_5

    .line 96
    iget v0, p0, Lejf;->c:I

    packed-switch v0, :pswitch_data_1

    move-object v0, v2

    .line 110
    :goto_2
    if-eqz v0, :cond_4

    .line 112
    new-instance v6, Ljava/lang/Thread;

    new-instance v7, Lejg;

    invoke-direct {v7, v0, v5}, Lejg;-><init>(Lkff;Landroid/os/ConditionVariable;)V

    const-string v8, "LookupPeopleForProfilesLoader"

    invoke-direct {v6, v7, v8}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 119
    invoke-virtual {v6}, Ljava/lang/Thread;->start()V

    .line 142
    :goto_3
    invoke-virtual {p0}, Lejf;->n()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6, v4}, Ldsm;->e(Landroid/content/Context;I)Ljava/util/HashMap;

    move-result-object v4

    .line 145
    invoke-virtual {v5}, Landroid/os/ConditionVariable;->block()V

    .line 147
    if-nez v3, :cond_7

    .line 148
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ldld;->b()Lokb;

    move-result-object v2

    .line 151
    :cond_0
    :goto_4
    if-eqz v2, :cond_2

    .line 152
    if-eqz v0, :cond_1

    .line 154
    invoke-virtual {p0}, Lejf;->n()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Ldwe;->a(Landroid/content/Context;)Ldwe;

    move-result-object v0

    iget-object v3, p0, Lejf;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v3, v2}, Ldwe;->a(ILjava/lang/String;Loxu;)V

    .line 158
    :cond_1
    iget-object v0, v2, Lokb;->b:[Lohv;

    invoke-static {v0, v4}, Ldsm;->a([Lohv;Ljava/util/HashMap;)V

    .line 160
    :cond_2
    return-object v2

    .line 71
    :pswitch_1
    const/4 v0, 0x2

    move v1, v0

    .line 72
    goto :goto_0

    .line 74
    :pswitch_2
    const/4 v0, 0x3

    move v1, v0

    .line 75
    goto :goto_0

    :catch_0
    move-exception v0

    :cond_3
    move-object v3, v2

    goto :goto_1

    .line 98
    :pswitch_3
    new-instance v0, Ldkw;

    .line 99
    invoke-virtual {p0}, Lejf;->n()Landroid/content/Context;

    move-result-object v6

    iget-object v7, p0, Lejf;->d:Ljava/lang/String;

    iget v8, p0, Lejf;->e:I

    invoke-direct {v0, v6, v4, v7, v8}, Ldkw;-><init>(Landroid/content/Context;ILjava/lang/String;I)V

    goto :goto_2

    .line 102
    :pswitch_4
    new-instance v0, Ldkz;

    .line 103
    invoke-virtual {p0}, Lejf;->n()Landroid/content/Context;

    move-result-object v6

    iget-object v7, p0, Lejf;->d:Ljava/lang/String;

    iget v8, p0, Lejf;->e:I

    invoke-direct {v0, v6, v4, v7, v8}, Ldkz;-><init>(Landroid/content/Context;ILjava/lang/String;I)V

    goto :goto_2

    .line 106
    :pswitch_5
    new-instance v0, Ldlc;

    .line 107
    invoke-virtual {p0}, Lejf;->n()Landroid/content/Context;

    move-result-object v6

    iget-object v7, p0, Lejf;->d:Ljava/lang/String;

    iget v8, p0, Lejf;->e:I

    invoke-direct {v0, v6, v4, v7, v8}, Ldlc;-><init>(Landroid/content/Context;ILjava/lang/String;I)V

    goto :goto_2

    .line 122
    :cond_4
    invoke-virtual {v5}, Landroid/os/ConditionVariable;->open()V

    goto :goto_3

    .line 125
    :cond_5
    invoke-virtual {v5}, Landroid/os/ConditionVariable;->open()V

    move-object v0, v2

    .line 127
    goto :goto_3

    .line 130
    :cond_6
    new-instance v0, Lokb;

    invoke-direct {v0}, Lokb;-><init>()V

    .line 131
    iget-object v3, p0, Lejf;->g:Lokb;

    iget-object v3, v3, Lokb;->b:[Lohv;

    iput-object v3, v0, Lokb;->b:[Lohv;

    .line 132
    iget-object v3, p0, Lejf;->g:Lokb;

    iget-object v3, v3, Lokb;->a:[Lohm;

    iput-object v3, v0, Lokb;->a:[Lohm;

    .line 133
    iget-object v3, p0, Lejf;->g:Lokb;

    iget-object v3, v3, Lokb;->d:Ljava/lang/Integer;

    iput-object v3, v0, Lokb;->d:Ljava/lang/Integer;

    .line 134
    iget-object v3, p0, Lejf;->g:Lokb;

    iget-object v3, v3, Lokb;->e:Ljava/lang/Long;

    iput-object v3, v0, Lokb;->e:Ljava/lang/Long;

    .line 135
    iget-object v3, p0, Lejf;->g:Lokb;

    iget-object v3, v3, Lokb;->c:Loia;

    iput-object v3, v0, Lokb;->c:Loia;

    .line 138
    invoke-virtual {v5}, Landroid/os/ConditionVariable;->open()V

    move-object v3, v0

    move-object v0, v2

    goto/16 :goto_3

    :cond_7
    move-object v2, v3

    goto :goto_4

    .line 66
    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 96
    :pswitch_data_1
    .packed-switch 0x8
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
