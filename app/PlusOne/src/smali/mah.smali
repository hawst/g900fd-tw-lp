.class public final Lmah;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnfi;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 28186
    invoke-direct {p0}, Loxq;-><init>()V

    .line 28189
    iput-object v0, p0, Lmah;->apiHeader:Llyr;

    .line 28192
    iput-object v0, p0, Lmah;->a:Lnfi;

    .line 28186
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 28209
    const/4 v0, 0x0

    .line 28210
    iget-object v1, p0, Lmah;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 28211
    const/4 v0, 0x1

    iget-object v1, p0, Lmah;->apiHeader:Llyr;

    .line 28212
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 28214
    :cond_0
    iget-object v1, p0, Lmah;->a:Lnfi;

    if-eqz v1, :cond_1

    .line 28215
    const/4 v1, 0x2

    iget-object v2, p0, Lmah;->a:Lnfi;

    .line 28216
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 28218
    :cond_1
    iget-object v1, p0, Lmah;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 28219
    iput v0, p0, Lmah;->ai:I

    .line 28220
    return v0
.end method

.method public a(Loxn;)Lmah;
    .locals 2

    .prologue
    .line 28228
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 28229
    sparse-switch v0, :sswitch_data_0

    .line 28233
    iget-object v1, p0, Lmah;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 28234
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmah;->ah:Ljava/util/List;

    .line 28237
    :cond_1
    iget-object v1, p0, Lmah;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 28239
    :sswitch_0
    return-object p0

    .line 28244
    :sswitch_1
    iget-object v0, p0, Lmah;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 28245
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmah;->apiHeader:Llyr;

    .line 28247
    :cond_2
    iget-object v0, p0, Lmah;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 28251
    :sswitch_2
    iget-object v0, p0, Lmah;->a:Lnfi;

    if-nez v0, :cond_3

    .line 28252
    new-instance v0, Lnfi;

    invoke-direct {v0}, Lnfi;-><init>()V

    iput-object v0, p0, Lmah;->a:Lnfi;

    .line 28254
    :cond_3
    iget-object v0, p0, Lmah;->a:Lnfi;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 28229
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 28197
    iget-object v0, p0, Lmah;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 28198
    const/4 v0, 0x1

    iget-object v1, p0, Lmah;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 28200
    :cond_0
    iget-object v0, p0, Lmah;->a:Lnfi;

    if-eqz v0, :cond_1

    .line 28201
    const/4 v0, 0x2

    iget-object v1, p0, Lmah;->a:Lnfi;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 28203
    :cond_1
    iget-object v0, p0, Lmah;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 28205
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 28182
    invoke-virtual {p0, p1}, Lmah;->a(Loxn;)Lmah;

    move-result-object v0

    return-object v0
.end method
