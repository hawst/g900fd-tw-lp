.class public final Lmcq;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmrf;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 31021
    invoke-direct {p0}, Loxq;-><init>()V

    .line 31024
    iput-object v0, p0, Lmcq;->apiHeader:Llyq;

    .line 31027
    iput-object v0, p0, Lmcq;->a:Lmrf;

    .line 31021
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 31044
    const/4 v0, 0x0

    .line 31045
    iget-object v1, p0, Lmcq;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 31046
    const/4 v0, 0x1

    iget-object v1, p0, Lmcq;->apiHeader:Llyq;

    .line 31047
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 31049
    :cond_0
    iget-object v1, p0, Lmcq;->a:Lmrf;

    if-eqz v1, :cond_1

    .line 31050
    const/4 v1, 0x2

    iget-object v2, p0, Lmcq;->a:Lmrf;

    .line 31051
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 31053
    :cond_1
    iget-object v1, p0, Lmcq;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 31054
    iput v0, p0, Lmcq;->ai:I

    .line 31055
    return v0
.end method

.method public a(Loxn;)Lmcq;
    .locals 2

    .prologue
    .line 31063
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 31064
    sparse-switch v0, :sswitch_data_0

    .line 31068
    iget-object v1, p0, Lmcq;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 31069
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmcq;->ah:Ljava/util/List;

    .line 31072
    :cond_1
    iget-object v1, p0, Lmcq;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 31074
    :sswitch_0
    return-object p0

    .line 31079
    :sswitch_1
    iget-object v0, p0, Lmcq;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 31080
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmcq;->apiHeader:Llyq;

    .line 31082
    :cond_2
    iget-object v0, p0, Lmcq;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 31086
    :sswitch_2
    iget-object v0, p0, Lmcq;->a:Lmrf;

    if-nez v0, :cond_3

    .line 31087
    new-instance v0, Lmrf;

    invoke-direct {v0}, Lmrf;-><init>()V

    iput-object v0, p0, Lmcq;->a:Lmrf;

    .line 31089
    :cond_3
    iget-object v0, p0, Lmcq;->a:Lmrf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 31064
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 31032
    iget-object v0, p0, Lmcq;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 31033
    const/4 v0, 0x1

    iget-object v1, p0, Lmcq;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 31035
    :cond_0
    iget-object v0, p0, Lmcq;->a:Lmrf;

    if-eqz v0, :cond_1

    .line 31036
    const/4 v0, 0x2

    iget-object v1, p0, Lmcq;->a:Lmrf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 31038
    :cond_1
    iget-object v0, p0, Lmcq;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 31040
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 31017
    invoke-virtual {p0, p1}, Lmcq;->a(Loxn;)Lmcq;

    move-result-object v0

    return-object v0
.end method
