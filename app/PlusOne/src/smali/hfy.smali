.class public final Lhfy;
.super Llol;
.source "PG"

# interfaces
.implements Lhgm;
.implements Llgs;


# static fields
.field private static final N:[I

.field private static final O:[I

.field private static final P:[I


# instance fields
.field private Q:Lbc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbc",
            "<",
            "Lhfv;",
            ">;"
        }
    .end annotation
.end field

.field private R:Lbc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbc",
            "<[",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private S:Lhgn;

.field private T:Lhgk;

.field private U:Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;

.field private V:Landroid/view/View;

.field private W:Landroid/view/View;

.field private X:Landroid/widget/EditText;

.field private Y:Landroid/view/View;

.field private Z:Landroid/util/DisplayMetrics;

.field private aA:Lhfw;

.field private aa:Landroid/widget/ListView;

.field private ab:Lhgl;

.field private ac:Lhgo;

.field private ad:Z

.field private ae:Lhgt;

.field private af:Lhfv;

.field private ag:Landroid/database/Cursor;

.field private ah:Landroid/database/Cursor;

.field private ai:Landroid/view/View;

.field private aj:Landroid/widget/TextView;

.field private ak:Landroid/widget/TextView;

.field private al:Landroid/widget/CompoundButton;

.field private am:Ljava/lang/String;

.field private an:Landroid/widget/ImageButton;

.field private ao:Z

.field private ap:Z

.field private aq:Z

.field private ar:I

.field private as:Z

.field private aw:[I

.field private ax:I

.field private ay:I

.field private az:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lhfy;->N:[I

    .line 81
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lhfy;->O:[I

    .line 88
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lhfy;->P:[I

    return-void

    .line 72
    nop

    :array_0
    .array-data 4
        0x0
        0x1
        0x2
        0x3
        0x4
    .end array-data

    .line 81
    :array_1
    .array-data 4
        0x3
        0x4
    .end array-data

    .line 88
    :array_2
    .array-data 4
        0x0
        0x1
        0x3
        0x4
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Llol;-><init>()V

    .line 96
    new-instance v0, Lhfz;

    invoke-direct {v0, p0}, Lhfz;-><init>(Lhfy;)V

    iput-object v0, p0, Lhfy;->Q:Lbc;

    .line 119
    new-instance v0, Lhgc;

    invoke-direct {v0, p0}, Lhgc;-><init>(Lhfy;)V

    iput-object v0, p0, Lhfy;->R:Lbc;

    .line 149
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    iput-object v0, p0, Lhfy;->Z:Landroid/util/DisplayMetrics;

    .line 175
    const/16 v0, 0x899

    iput v0, p0, Lhfy;->az:I

    .line 183
    return-void
.end method

.method private V()V
    .locals 1

    .prologue
    .line 432
    iget v0, p0, Lhfy;->az:I

    packed-switch v0, :pswitch_data_0

    .line 443
    :goto_0
    return-void

    .line 434
    :pswitch_0
    invoke-direct {p0}, Lhfy;->W()V

    goto :goto_0

    .line 437
    :pswitch_1
    invoke-direct {p0}, Lhfy;->X()V

    goto :goto_0

    .line 440
    :pswitch_2
    invoke-direct {p0}, Lhfy;->Y()V

    goto :goto_0

    .line 432
    :pswitch_data_0
    .packed-switch 0x899
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private W()V
    .locals 3

    .prologue
    .line 446
    invoke-direct {p0}, Lhfy;->Z()V

    .line 447
    const/16 v0, 0x899

    iput v0, p0, Lhfy;->az:I

    .line 448
    iget-object v0, p0, Lhfy;->af:Lhfv;

    if-eqz v0, :cond_0

    .line 449
    iget-object v0, p0, Lhfy;->ab:Lhgl;

    iget-object v1, p0, Lhfy;->af:Lhfv;

    iget-object v2, p0, Lhfy;->aw:[I

    invoke-virtual {v0, v1, v2}, Lhgl;->a(Lhfv;[I)V

    .line 451
    :cond_0
    return-void
.end method

.method private X()V
    .locals 3

    .prologue
    .line 458
    invoke-direct {p0}, Lhfy;->Z()V

    .line 459
    const/16 v0, 0x89b

    iput v0, p0, Lhfy;->az:I

    .line 460
    iget-object v0, p0, Lhfy;->af:Lhfv;

    if-eqz v0, :cond_0

    .line 461
    iget-object v0, p0, Lhfy;->ab:Lhgl;

    iget-object v1, p0, Lhfy;->ae:Lhgt;

    iget-object v2, p0, Lhfy;->af:Lhfv;

    invoke-virtual {v1, v2}, Lhgt;->a(Lhfv;)Lhfv;

    move-result-object v1

    iget-object v2, p0, Lhfy;->aw:[I

    invoke-virtual {v0, v1, v2}, Lhgl;->a(Lhfv;[I)V

    .line 464
    :cond_0
    return-void
.end method

.method private Y()V
    .locals 2

    .prologue
    .line 467
    iget-object v0, p0, Lhfy;->V:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 468
    const/16 v0, 0x89a

    iput v0, p0, Lhfy;->az:I

    .line 470
    iget-object v0, p0, Lhfy;->W:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 472
    iget-object v0, p0, Lhfy;->X:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 473
    iget-object v0, p0, Lhfy;->X:Landroid/widget/EditText;

    invoke-static {v0}, Llsn;->a(Landroid/view/View;)V

    .line 475
    iget-boolean v0, p0, Lhfy;->ad:Z

    if-nez v0, :cond_0

    .line 476
    iget-object v0, p0, Lhfy;->ac:Lhgo;

    invoke-virtual {v0}, Lhgo;->b()V

    .line 477
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhfy;->ad:Z

    .line 480
    :cond_0
    iget-object v0, p0, Lhfy;->aa:Landroid/widget/ListView;

    iget-object v1, p0, Lhfy;->ac:Lhgo;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 481
    return-void
.end method

.method private Z()V
    .locals 2

    .prologue
    .line 484
    const/16 v0, 0x899

    iput v0, p0, Lhfy;->az:I

    .line 486
    iget-object v0, p0, Lhfy;->V:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 488
    iget-object v0, p0, Lhfy;->X:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 489
    iget-object v0, p0, Lhfy;->W:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 490
    iget-object v0, p0, Lhfy;->X:Landroid/widget/EditText;

    invoke-static {v0}, Llsn;->b(Landroid/view/View;)V

    .line 492
    iget-object v0, p0, Lhfy;->aa:Landroid/widget/ListView;

    iget-object v1, p0, Lhfy;->ab:Lhgl;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 493
    return-void
.end method

.method static synthetic a(Lhfy;)I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lhfy;->ar:I

    return v0
.end method

.method static synthetic a(Lhfy;Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lhfy;->ag:Landroid/database/Cursor;

    return-object p1
.end method

.method static synthetic a(Lhfy;Lhfv;)Lhfv;
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lhfy;->af:Lhfv;

    return-object p1
.end method

.method static synthetic a(Lhfy;Lhgt;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lhfy;->a(Lhgt;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Lhgt;)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 636
    invoke-virtual {p0}, Lhfy;->n()Lz;

    .line 637
    invoke-virtual {p0}, Lhfy;->o()Landroid/content/res/Resources;

    move-result-object v5

    .line 639
    invoke-virtual {p1}, Lhgt;->c()I

    move-result v6

    .line 640
    if-nez v6, :cond_1

    .line 641
    const-string v0, ""

    .line 668
    :cond_0
    :goto_0
    return-object v0

    .line 644
    :cond_1
    invoke-virtual {p1, v3}, Lhgt;->a(I)Lhfw;

    move-result-object v0

    invoke-virtual {v0}, Lhfw;->c()Ljava/lang/String;

    move-result-object v0

    .line 645
    if-eq v6, v2, :cond_0

    .line 649
    invoke-virtual {p1, v2}, Lhgt;->a(I)Lhfw;

    move-result-object v1

    invoke-virtual {v1}, Lhfw;->c()Ljava/lang/String;

    move-result-object v7

    .line 650
    if-ne v6, v4, :cond_2

    .line 651
    const v1, 0x7f0a053e

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v3

    aput-object v7, v4, v2

    invoke-virtual {v5, v1, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    move v1, v4

    .line 656
    :goto_1
    if-ge v1, v6, :cond_5

    .line 657
    invoke-virtual {p1, v1}, Lhgt;->a(I)Lhfw;

    move-result-object v8

    invoke-virtual {v8}, Lhfw;->e()Ljqs;

    move-result-object v8

    if-nez v8, :cond_3

    move v1, v2

    .line 663
    :goto_2
    if-eqz v1, :cond_4

    .line 664
    const v1, 0x7f0a0540

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v3

    aput-object v7, v4, v2

    invoke-virtual {v5, v1, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 656
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 667
    :cond_4
    add-int/lit8 v1, v6, -0x2

    .line 668
    const v6, 0x7f11002e

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    aput-object v0, v8, v3

    aput-object v7, v8, v2

    .line 669
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v8, v4

    .line 668
    invoke-virtual {v5, v6, v1, v8}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_5
    move v1, v3

    goto :goto_2
.end method

.method static synthetic a(Lhfy;Z)Z
    .locals 0

    .prologue
    .line 44
    iput-boolean p1, p0, Lhfy;->aq:Z

    return p1
.end method

.method private aa()I
    .locals 4

    .prologue
    .line 499
    const/4 v0, 0x1

    iget-object v1, p0, Lhfy;->Z:Landroid/util/DisplayMetrics;

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v1, v1

    iget-object v2, p0, Lhfy;->Z:Landroid/util/DisplayMetrics;

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    const/high16 v3, 0x42d00000    # 104.0f

    mul-float/2addr v2, v3

    div-float/2addr v1, v2

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method private ab()V
    .locals 2

    .prologue
    .line 504
    iget-object v0, p0, Lhfy;->U:Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;

    iget-object v1, p0, Lhfy;->ae:Lhgt;

    invoke-virtual {v1}, Lhgt;->d()Lhgw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->a(Lhgw;)V

    .line 505
    return-void
.end method

.method static synthetic b(Lhfy;Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lhfy;->ah:Landroid/database/Cursor;

    return-object p1
.end method

.method static synthetic b(Lhfy;)Z
    .locals 1

    .prologue
    .line 44
    iget-boolean v0, p0, Lhfy;->aq:Z

    return v0
.end method

.method static synthetic c(Lhfy;)I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lhfy;->ax:I

    return v0
.end method

.method static synthetic d(Lhfy;)Lhgn;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lhfy;->S:Lhgn;

    return-object v0
.end method

.method static synthetic e(Lhfy;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lhfy;->ag:Landroid/database/Cursor;

    return-object v0
.end method

.method static synthetic f(Lhfy;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lhfy;->ah:Landroid/database/Cursor;

    return-object v0
.end method

.method static synthetic g(Lhfy;)Lhfv;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lhfy;->af:Lhfv;

    return-object v0
.end method

.method static synthetic h(Lhfy;)[I
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lhfy;->aw:[I

    return-object v0
.end method

.method static synthetic i(Lhfy;)Lhgo;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lhfy;->ac:Lhgo;

    return-object v0
.end method

.method static synthetic j(Lhfy;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lhfy;->V()V

    return-void
.end method

.method static synthetic k(Lhfy;)Lhgl;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lhfy;->ab:Lhgl;

    return-object v0
.end method

.method static synthetic l(Lhfy;)Lhgt;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lhfy;->ae:Lhgt;

    return-object v0
.end method

.method static synthetic m(Lhfy;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lhfy;->ab()V

    return-void
.end method

.method static synthetic n(Lhfy;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lhfy;->an:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic o(Lhfy;)Lbc;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lhfy;->Q:Lbc;

    return-object v0
.end method

.method static synthetic p(Lhfy;)V
    .locals 3

    .prologue
    const/16 v2, 0xa

    .line 44
    invoke-virtual {p0}, Lhfy;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lhfy;->U()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    invoke-virtual {p0, v2}, Lhfy;->c(I)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lhfy;->ac:Lhgo;

    invoke-virtual {p0}, Lhfy;->U()I

    move-result v1

    invoke-virtual {v0, v1}, Lhgo;->c(I)V

    return-void

    :cond_1
    invoke-virtual {p0}, Lhfy;->U()I

    move-result v0

    if-ne v0, v2, :cond_0

    iget v0, p0, Lhfy;->ay:I

    invoke-virtual {p0, v0}, Lhfy;->c(I)V

    goto :goto_0
.end method

.method static synthetic q(Lhfy;)I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lhfy;->az:I

    return v0
.end method

.method static synthetic r(Lhfy;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lhfy;->W()V

    return-void
.end method

.method static synthetic s(Lhfy;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lhfy;->Y()V

    return-void
.end method

.method static synthetic t(Lhfy;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lhfy;->X:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic u(Lhfy;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lhfy;->X()V

    return-void
.end method

.method static synthetic v(Lhfy;)Lhgk;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lhfy;->T:Lhgk;

    return-object v0
.end method


# virtual methods
.method public U()I
    .locals 1

    .prologue
    .line 701
    iget v0, p0, Lhfy;->ax:I

    return v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    const v3, 0x7f100169

    .line 276
    const v0, 0x7f040029

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 277
    const v0, 0x7f10016e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lhfy;->ai:Landroid/view/View;

    .line 278
    const v0, 0x7f100172

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lhfy;->aj:Landroid/widget/TextView;

    .line 279
    const v0, 0x7f100173

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lhfy;->ak:Landroid/widget/TextView;

    .line 280
    const v0, 0x7f100170

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CompoundButton;

    iput-object v0, p0, Lhfy;->al:Landroid/widget/CompoundButton;

    .line 281
    iget-object v0, p0, Lhfy;->al:Landroid/widget/CompoundButton;

    new-instance v2, Lhge;

    invoke-direct {v2, p0}, Lhge;-><init>(Lhfy;)V

    invoke-virtual {v0, v2}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 304
    const v0, 0x7f100166

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 305
    new-instance v2, Lhgf;

    invoke-direct {v2, p0}, Lhgf;-><init>(Lhfy;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 316
    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lhfy;->W:Landroid/view/View;

    .line 317
    const v0, 0x7f10016b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lhfy;->X:Landroid/widget/EditText;

    .line 318
    iget-object v0, p0, Lhfy;->X:Landroid/widget/EditText;

    new-instance v2, Lhgg;

    invoke-direct {v2, p0}, Lhgg;-><init>(Lhfy;)V

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 332
    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lhfy;->Y:Landroid/view/View;

    .line 333
    iget-object v0, p0, Lhfy;->Y:Landroid/view/View;

    new-instance v2, Lhgh;

    invoke-direct {v2, p0}, Lhgh;-><init>(Lhfy;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 345
    const v0, 0x7f10016c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lhfy;->V:Landroid/view/View;

    .line 346
    const v0, 0x7f100148

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;

    iput-object v0, p0, Lhfy;->U:Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;

    .line 347
    iget-object v0, p0, Lhfy;->U:Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;

    iget v2, p0, Lhfy;->ar:I

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->c(I)V

    .line 349
    iget-object v0, p0, Lhfy;->U:Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;

    new-instance v2, Lhgi;

    invoke-direct {v2, p0}, Lhgi;-><init>(Lhfy;)V

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 361
    invoke-direct {p0}, Lhfy;->ab()V

    .line 363
    const v0, 0x7f100168

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lhfy;->an:Landroid/widget/ImageButton;

    .line 364
    iget-object v2, p0, Lhfy;->an:Landroid/widget/ImageButton;

    iget-object v0, p0, Lhfy;->ae:Lhgt;

    invoke-virtual {v0}, Lhgt;->c()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 365
    iget-object v0, p0, Lhfy;->an:Landroid/widget/ImageButton;

    new-instance v2, Lhgj;

    invoke-direct {v2, p0}, Lhgj;-><init>(Lhfy;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 383
    const v0, 0x102000a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lhfy;->aa:Landroid/widget/ListView;

    .line 385
    invoke-direct {p0}, Lhfy;->V()V

    .line 387
    return-object v1

    .line 364
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 608
    return-void
.end method

.method public a(IZLandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 614
    return-void
.end method

.method public a(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 193
    invoke-super {p0, p1}, Llol;->a(Landroid/app/Activity;)V

    .line 194
    invoke-virtual {p0}, Lhfy;->n()Lz;

    move-result-object v0

    const-string v1, "window"

    invoke-virtual {v0, v1}, Lz;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 195
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iget-object v1, p0, Lhfy;->Z:Landroid/util/DisplayMetrics;

    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 196
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 206
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 209
    invoke-virtual {p0}, Lhfy;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 210
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 211
    const-string v2, "restrict_to_domain"

    invoke-virtual {v1, v2, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    invoke-virtual {p0, v2}, Lhfy;->b(Z)V

    .line 212
    const-string v2, "hide_domain_restrict_toggle"

    invoke-virtual {v1, v2, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    invoke-virtual {p0, v2}, Lhfy;->c(Z)V

    .line 213
    const-string v2, "enable_domain_restrict_toggle"

    .line 214
    invoke-virtual {v1, v2, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 213
    invoke-virtual {p0, v2}, Lhfy;->i(Z)V

    .line 215
    const-string v2, "domain_name"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lhfy;->a(Ljava/lang/String;)V

    .line 216
    const-string v2, "is_dasher_account"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {p0, v2}, Lhfy;->a(Z)V

    .line 217
    const-string v2, "circle_usage_type"

    const/4 v3, 0x5

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lhfy;->ay:I

    .line 218
    iget v2, p0, Lhfy;->ay:I

    invoke-virtual {p0, v2}, Lhfy;->c(I)V

    .line 221
    const-string v2, "category_display_mode"

    invoke-virtual {v1, v2, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 222
    packed-switch v1, :pswitch_data_0

    .line 230
    sget-object v1, Lhfy;->N:[I

    iput-object v1, p0, Lhfy;->aw:[I

    .line 235
    :goto_0
    const-string v1, "extra_account_id"

    invoke-virtual {v0, v1, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lhfy;->ar:I

    .line 236
    if-nez p1, :cond_0

    .line 237
    const-string v1, "extra_acl"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lhgw;

    .line 238
    new-instance v1, Lhgt;

    invoke-direct {v1, v0}, Lhgt;-><init>(Lhgw;)V

    iput-object v1, p0, Lhfy;->ae:Lhgt;

    .line 245
    :goto_1
    iget-object v0, p0, Lhfy;->ae:Lhgt;

    new-instance v1, Lhgd;

    invoke-direct {v1, p0}, Lhgd;-><init>(Lhfy;)V

    invoke-virtual {v0, v1}, Lhgt;->a(Lhgv;)V

    .line 255
    new-instance v0, Lhgl;

    invoke-virtual {p0}, Lhfy;->n()Lz;

    move-result-object v1

    invoke-direct {p0}, Lhfy;->aa()I

    move-result v2

    iget-object v3, p0, Lhfy;->ae:Lhgt;

    invoke-direct {v0, v1, v2, v3, p0}, Lhgl;-><init>(Landroid/content/Context;ILhgt;Lhgm;)V

    iput-object v0, p0, Lhfy;->ab:Lhgl;

    .line 257
    new-instance v0, Lhgo;

    invoke-virtual {p0}, Lhfy;->n()Lz;

    move-result-object v1

    invoke-direct {p0}, Lhfy;->aa()I

    move-result v2

    iget-object v3, p0, Lhfy;->ae:Lhgt;

    .line 258
    invoke-virtual {p0}, Lhfy;->p()Lae;

    move-result-object v5

    invoke-virtual {p0}, Lhfy;->w()Lbb;

    move-result-object v6

    iget v7, p0, Lhfy;->ar:I

    move-object v4, p0

    invoke-direct/range {v0 .. v7}, Lhgo;-><init>(Landroid/content/Context;ILhgt;Lhgm;Lae;Lbb;I)V

    iput-object v0, p0, Lhfy;->ac:Lhgo;

    .line 259
    iget-object v0, p0, Lhfy;->ac:Lhgo;

    invoke-virtual {v0, p1}, Lhgo;->a(Landroid/os/Bundle;)V

    .line 260
    iget-object v0, p0, Lhfy;->ac:Lhgo;

    iget v1, p0, Lhfy;->ax:I

    invoke-virtual {v0, v1}, Lhgo;->c(I)V

    .line 261
    iget-object v0, p0, Lhfy;->ac:Lhgo;

    invoke-virtual {v0, v9}, Lhgo;->c(Z)V

    .line 262
    iget-object v0, p0, Lhfy;->ac:Lhgo;

    invoke-virtual {v0, v9}, Lhgo;->e(Z)V

    .line 263
    iget-object v0, p0, Lhfy;->ac:Lhgo;

    invoke-virtual {v0, v9}, Lhgo;->d(Z)V

    .line 264
    iget-object v0, p0, Lhfy;->ac:Lhgo;

    invoke-virtual {v0, v8}, Lhgo;->a(Z)V

    .line 265
    iget-object v0, p0, Lhfy;->ac:Lhgo;

    invoke-virtual {v0, v8}, Lhgo;->b(Z)V

    .line 267
    invoke-virtual {p0}, Lhfy;->w()Lbb;

    move-result-object v0

    iget-object v1, p0, Lhfy;->Q:Lbc;

    invoke-virtual {v0, v9, v10, v1}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 268
    invoke-virtual {p0}, Lhfy;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x2

    iget-object v2, p0, Lhfy;->R:Lbc;

    invoke-virtual {v0, v1, v10, v2}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 270
    return-void

    .line 224
    :pswitch_0
    sget-object v1, Lhfy;->O:[I

    iput-object v1, p0, Lhfy;->aw:[I

    goto/16 :goto_0

    .line 227
    :pswitch_1
    sget-object v1, Lhfy;->P:[I

    iput-object v1, p0, Lhfy;->aw:[I

    goto/16 :goto_0

    .line 240
    :cond_0
    const-string v0, "selection_model"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lhgt;

    iput-object v0, p0, Lhfy;->ae:Lhgt;

    .line 242
    const-string v0, "display_mode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lhfy;->az:I

    .line 243
    const-string v0, "pending_entry"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lhfw;

    iput-object v0, p0, Lhfy;->aA:Lhfw;

    goto/16 :goto_1

    .line 222
    nop

    :pswitch_data_0
    .packed-switch 0xce6
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 592
    iget-object v0, p0, Lhfy;->ae:Lhgt;

    iget-object v1, p0, Lhfy;->aA:Lhfw;

    invoke-virtual {v0, v1}, Lhgt;->a(Lhfw;)V

    .line 593
    return-void
.end method

.method public a(Lhfw;Landroid/view/View;)V
    .locals 9

    .prologue
    const v8, 0x104000a

    const/high16 v7, 0x1040000

    const/4 v6, 0x1

    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 510
    invoke-virtual {p1}, Lhfw;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 589
    :goto_0
    return-void

    .line 515
    :cond_0
    invoke-virtual {p1}, Lhfw;->g()Lhhe;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 516
    invoke-virtual {p1}, Lhfw;->g()Lhhe;

    move-result-object v0

    invoke-virtual {v0}, Lhhe;->c()Lhhf;

    move-result-object v0

    .line 517
    invoke-virtual {p0}, Lhfy;->n()Lz;

    iget v1, p0, Lhfy;->ar:I

    iget-object v1, p0, Lhfy;->ae:Lhgt;

    invoke-virtual {v1}, Lhgt;->d()Lhgw;

    .line 516
    invoke-interface {v0}, Lhhf;->a()Landroid/content/Intent;

    move-result-object v0

    .line 518
    invoke-virtual {p0}, Lhfy;->n()Lz;

    move-result-object v1

    invoke-virtual {v1, v0}, Lz;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 522
    :cond_1
    iget-object v0, p0, Lhfy;->ae:Lhgt;

    invoke-virtual {v0, p1}, Lhgt;->b(Lhfw;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 523
    iget-object v0, p0, Lhfy;->ae:Lhgt;

    invoke-virtual {v0, p1}, Lhgt;->c(Lhfw;)V

    .line 524
    invoke-static {p2}, Lhfu;->a(Landroid/view/View;)V

    .line 588
    :goto_1
    iget-object v0, p0, Lhfy;->ab:Lhgl;

    invoke-virtual {v0}, Lhgl;->notifyDataSetChanged()V

    goto :goto_0

    .line 527
    :cond_2
    iget-object v0, p0, Lhfy;->ae:Lhgt;

    invoke-virtual {v0}, Lhgt;->c()I

    move-result v3

    move v0, v2

    .line 528
    :goto_2
    if-ge v0, v3, :cond_a

    .line 529
    iget-object v4, p0, Lhfy;->ae:Lhgt;

    invoke-virtual {v4, v0}, Lhgt;->a(I)Lhfw;

    move-result-object v4

    invoke-virtual {v4}, Lhfw;->f()Lkxr;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 536
    :goto_3
    if-eq v0, v1, :cond_4

    invoke-virtual {p1}, Lhfw;->f()Lkxr;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 538
    iget-object v1, p0, Lhfy;->ae:Lhgt;

    invoke-virtual {v1, v0}, Lhgt;->a(I)Lhfw;

    move-result-object v0

    invoke-virtual {v0}, Lhfw;->j()Lhfw;

    move-result-object v0

    .line 539
    new-instance v1, Landroid/app/AlertDialog$Builder;

    new-instance v3, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lhfy;->n()Lz;

    move-result-object v4

    const v5, 0x7f0900a9

    invoke-direct {v3, v4, v5}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v1, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0a053a

    new-array v4, v6, [Ljava/lang/Object;

    .line 541
    invoke-virtual {p0}, Lhfy;->n()Lz;

    invoke-virtual {p1}, Lhfw;->c()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    .line 540
    invoke-virtual {p0, v3, v4}, Lhfy;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lhga;

    invoke-direct {v2, p0, v0, p1}, Lhga;-><init>(Lhfy;Lhfw;Lhfw;)V

    .line 542
    invoke-virtual {v1, v8, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    .line 552
    invoke-virtual {v0, v7, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_1

    .line 528
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 553
    :cond_4
    invoke-virtual {p1}, Lhfw;->f()Lkxr;

    move-result-object v3

    if-eqz v3, :cond_5

    iget-object v3, p0, Lhfy;->ae:Lhgt;

    invoke-virtual {v3}, Lhgt;->c()I

    move-result v3

    if-gtz v3, :cond_6

    :cond_5
    if-eq v0, v1, :cond_7

    .line 557
    :cond_6
    new-instance v0, Landroid/app/AlertDialog$Builder;

    new-instance v1, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lhfy;->n()Lz;

    move-result-object v3

    const v4, 0x7f0900a9

    invoke-direct {v1, v3, v4}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0a053b

    new-array v3, v6, [Ljava/lang/Object;

    .line 559
    invoke-virtual {p0}, Lhfy;->n()Lz;

    invoke-virtual {p1}, Lhfw;->c()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v2

    .line 558
    invoke-virtual {p0, v1, v3}, Lhfy;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lhgb;

    invoke-direct {v1, p0, p1}, Lhgb;-><init>(Lhfy;Lhfw;)V

    .line 560
    invoke-virtual {v0, v8, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    .line 570
    invoke-virtual {v0, v7, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_1

    .line 573
    :cond_7
    invoke-virtual {p1}, Lhfw;->d()Lhxc;

    move-result-object v3

    .line 575
    if-eqz v3, :cond_8

    iget-object v0, p0, Lhfy;->af:Lhfv;

    iget v2, p0, Lhfy;->ar:I

    const-string v4, "child_public_warning"

    move-object v1, p0

    move-object v5, p0

    invoke-interface/range {v0 .. v5}, Lhfv;->a(Lu;ILhxc;Ljava/lang/String;Llgs;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 577
    :cond_8
    iget-object v0, p0, Lhfy;->ae:Lhgt;

    invoke-virtual {v0, p1}, Lhgt;->a(Lhfw;)V

    .line 581
    :goto_4
    iget-object v0, p0, Lhfy;->af:Lhfv;

    invoke-static {v0, p1, p2}, Lhfu;->a(Lhfv;Lhfw;Landroid/view/View;)V

    .line 582
    invoke-virtual {p0}, Lhfy;->d()Z

    goto/16 :goto_1

    .line 579
    :cond_9
    iput-object p1, p0, Lhfy;->aA:Lhfw;

    goto :goto_4

    :cond_a
    move v0, v1

    goto/16 :goto_3
.end method

.method public a(Lhgk;)V
    .locals 0

    .prologue
    .line 188
    iput-object p1, p0, Lhfy;->T:Lhgk;

    .line 189
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 681
    iput-object p1, p0, Lhfy;->am:Ljava/lang/String;

    .line 682
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 677
    iput-boolean p1, p0, Lhfy;->as:Z

    .line 678
    return-void
.end method

.method public a()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 420
    iget v1, p0, Lhfy;->az:I

    packed-switch v1, :pswitch_data_0

    .line 428
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 422
    :pswitch_0
    invoke-direct {p0}, Lhfy;->W()V

    goto :goto_0

    .line 425
    :pswitch_1
    invoke-direct {p0}, Lhfy;->W()V

    goto :goto_0

    .line 420
    :pswitch_data_0
    .packed-switch 0x89a
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public aO_()V
    .locals 9

    .prologue
    const/16 v3, 0x8

    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 398
    invoke-super {p0}, Llol;->aO_()V

    .line 400
    iget-boolean v0, p0, Lhfy;->as:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lhfy;->ao:Z

    if-nez v0, :cond_3

    .line 401
    iget-object v0, p0, Lhfy;->ai:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 402
    iget-boolean v0, p0, Lhfy;->ap:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lhfy;->aq:Z

    if-eqz v0, :cond_0

    const v0, 0x7f0a0548

    .line 405
    :goto_0
    iget-boolean v1, p0, Lhfy;->ap:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lhfy;->aq:Z

    if-eqz v1, :cond_1

    const v1, 0x7f0a0549

    .line 408
    :goto_1
    iget-object v4, p0, Lhfy;->aj:Landroid/widget/TextView;

    invoke-virtual {p0}, Lhfy;->o()Landroid/content/res/Resources;

    move-result-object v5

    new-array v6, v8, [Ljava/lang/Object;

    iget-object v7, p0, Lhfy;->am:Ljava/lang/String;

    aput-object v7, v6, v2

    invoke-virtual {v5, v0, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 409
    iget-object v0, p0, Lhfy;->ak:Landroid/widget/TextView;

    invoke-virtual {p0}, Lhfy;->o()Landroid/content/res/Resources;

    move-result-object v4

    new-array v5, v8, [Ljava/lang/Object;

    iget-object v6, p0, Lhfy;->am:Ljava/lang/String;

    aput-object v6, v5, v2

    invoke-virtual {v4, v1, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 410
    iget-object v0, p0, Lhfy;->al:Landroid/widget/CompoundButton;

    iget-boolean v1, p0, Lhfy;->aq:Z

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 411
    iget-object v1, p0, Lhfy;->al:Landroid/widget/CompoundButton;

    iget-boolean v0, p0, Lhfy;->ap:Z

    if-eqz v0, :cond_2

    move v0, v2

    :goto_2
    invoke-virtual {v1, v0}, Landroid/widget/CompoundButton;->setVisibility(I)V

    .line 417
    :goto_3
    return-void

    .line 402
    :cond_0
    const v0, 0x7f0a0546

    goto :goto_0

    .line 405
    :cond_1
    const v1, 0x7f0a0547

    goto :goto_1

    :cond_2
    move v0, v3

    .line 411
    goto :goto_2

    .line 414
    :cond_3
    iget-object v0, p0, Lhfy;->ai:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 415
    iget-object v0, p0, Lhfy;->al:Landroid/widget/CompoundButton;

    invoke-virtual {v0, v2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto :goto_3
.end method

.method public b(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 598
    return-void
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 685
    iput-boolean p1, p0, Lhfy;->aq:Z

    .line 686
    return-void
.end method

.method public c(I)V
    .locals 0

    .prologue
    .line 697
    iput p1, p0, Lhfy;->ax:I

    .line 698
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 200
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 201
    iget-object v0, p0, Lhfy;->au:Llnh;

    const-class v1, Lhgn;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhgn;

    iput-object v0, p0, Lhfy;->S:Lhgn;

    .line 202
    return-void
.end method

.method public c(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 603
    return-void
.end method

.method public c(Z)V
    .locals 0

    .prologue
    .line 689
    iput-boolean p1, p0, Lhfy;->ao:Z

    .line 690
    return-void
.end method

.method d()Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 617
    iget-object v2, p0, Lhfy;->ae:Lhgt;

    invoke-virtual {v2}, Lhgt;->c()I

    move-result v2

    if-ne v2, v0, :cond_1

    .line 618
    iget-object v2, p0, Lhfy;->ae:Lhgt;

    invoke-virtual {v2, v1}, Lhgt;->a(I)Lhfw;

    move-result-object v2

    invoke-virtual {v2}, Lhfw;->f()Lkxr;

    move-result-object v2

    .line 619
    if-eqz v2, :cond_1

    .line 620
    iget-object v1, p0, Lhfy;->T:Lhgk;

    if-eqz v1, :cond_0

    .line 621
    iget-object v1, p0, Lhfy;->T:Lhgk;

    iget-object v2, p0, Lhfy;->ae:Lhgt;

    invoke-virtual {v2}, Lhgt;->d()Lhgw;

    move-result-object v2

    iget-object v3, p0, Lhfy;->ae:Lhgt;

    .line 622
    invoke-direct {p0, v3}, Lhfy;->a(Lhgt;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lhfy;->e()Z

    move-result v4

    .line 621
    invoke-interface {v1, v2, v3, v4}, Lhgk;->a(Lhgw;Ljava/lang/String;Z)V

    .line 627
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 706
    invoke-super {p0, p1}, Llol;->e(Landroid/os/Bundle;)V

    .line 708
    const-string v0, "selection_model"

    iget-object v1, p0, Lhfy;->ae:Lhgt;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 709
    const-string v0, "display_mode"

    iget v1, p0, Lhfy;->az:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 710
    const-string v0, "pending_entry"

    iget-object v1, p0, Lhfy;->aA:Lhfw;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 712
    iget-object v0, p0, Lhfy;->ac:Lhgo;

    invoke-virtual {v0, p1}, Lhgo;->b(Landroid/os/Bundle;)V

    .line 713
    return-void
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 673
    iget-object v0, p0, Lhfy;->al:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    return v0
.end method

.method public h()V
    .locals 1

    .prologue
    .line 392
    invoke-super {p0}, Llol;->g()V

    .line 393
    iget-object v0, p0, Lhfy;->ac:Lhgo;

    invoke-virtual {v0}, Lhgo;->c()V

    .line 394
    return-void
.end method

.method public i(Z)V
    .locals 0

    .prologue
    .line 693
    iput-boolean p1, p0, Lhfy;->ap:Z

    .line 694
    return-void
.end method
