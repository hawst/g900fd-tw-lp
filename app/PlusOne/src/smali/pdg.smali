.class public final Lpdg;
.super Loxq;
.source "PG"


# static fields
.field public static final a:Loxr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Loxr",
            "<",
            "Lpdg;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:[Loya;

.field public g:Loya;

.field private h:Ljava/lang/String;

.field private i:Lpdi;

.field private j:Ljava/lang/String;

.field private k:[Loya;

.field private l:Loya;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 9
    const v0, 0x25e8557

    new-instance v1, Lpdh;

    invoke-direct {v1}, Lpdh;-><init>()V

    .line 14
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    move-result-object v0

    sput-object v0, Lpdg;->a:Loxr;

    .line 13
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 27
    iput-object v1, p0, Lpdg;->i:Lpdi;

    .line 32
    sget-object v0, Loya;->a:[Loya;

    iput-object v0, p0, Lpdg;->k:[Loya;

    .line 35
    iput-object v1, p0, Lpdg;->l:Loya;

    .line 38
    sget-object v0, Loya;->a:[Loya;

    iput-object v0, p0, Lpdg;->f:[Loya;

    .line 45
    iput-object v1, p0, Lpdg;->g:Loya;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 108
    .line 109
    iget-object v0, p0, Lpdg;->b:Ljava/lang/String;

    if-eqz v0, :cond_f

    .line 110
    const/4 v0, 0x1

    iget-object v2, p0, Lpdg;->b:Ljava/lang/String;

    .line 111
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 113
    :goto_0
    iget-object v2, p0, Lpdg;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 114
    const/4 v2, 0x2

    iget-object v3, p0, Lpdg;->c:Ljava/lang/String;

    .line 115
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 117
    :cond_0
    iget-object v2, p0, Lpdg;->d:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 118
    const/4 v2, 0x3

    iget-object v3, p0, Lpdg;->d:Ljava/lang/String;

    .line 119
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 121
    :cond_1
    iget-object v2, p0, Lpdg;->e:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 122
    const/4 v2, 0x4

    iget-object v3, p0, Lpdg;->e:Ljava/lang/String;

    .line 123
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 125
    :cond_2
    iget-object v2, p0, Lpdg;->h:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 126
    const/4 v2, 0x5

    iget-object v3, p0, Lpdg;->h:Ljava/lang/String;

    .line 127
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 129
    :cond_3
    iget-object v2, p0, Lpdg;->i:Lpdi;

    if-eqz v2, :cond_4

    .line 130
    const/4 v2, 0x6

    iget-object v3, p0, Lpdg;->i:Lpdi;

    .line 131
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 133
    :cond_4
    iget-object v2, p0, Lpdg;->j:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 134
    const/4 v2, 0x7

    iget-object v3, p0, Lpdg;->j:Ljava/lang/String;

    .line 135
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 137
    :cond_5
    iget-object v2, p0, Lpdg;->k:[Loya;

    if-eqz v2, :cond_7

    .line 138
    iget-object v3, p0, Lpdg;->k:[Loya;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_7

    aget-object v5, v3, v2

    .line 139
    if-eqz v5, :cond_6

    .line 140
    const/16 v6, 0x8

    .line 141
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 138
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 145
    :cond_7
    iget-object v2, p0, Lpdg;->l:Loya;

    if-eqz v2, :cond_8

    .line 146
    const/16 v2, 0x9

    iget-object v3, p0, Lpdg;->l:Loya;

    .line 147
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 149
    :cond_8
    iget-object v2, p0, Lpdg;->f:[Loya;

    if-eqz v2, :cond_a

    .line 150
    iget-object v2, p0, Lpdg;->f:[Loya;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_a

    aget-object v4, v2, v1

    .line 151
    if-eqz v4, :cond_9

    .line 152
    const/16 v5, 0xb

    .line 153
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 150
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 157
    :cond_a
    iget-object v1, p0, Lpdg;->m:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 158
    const/16 v1, 0xc

    iget-object v2, p0, Lpdg;->m:Ljava/lang/String;

    .line 159
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 161
    :cond_b
    iget-object v1, p0, Lpdg;->n:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 162
    const/16 v1, 0x4b

    iget-object v2, p0, Lpdg;->n:Ljava/lang/String;

    .line 163
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 165
    :cond_c
    iget-object v1, p0, Lpdg;->g:Loya;

    if-eqz v1, :cond_d

    .line 166
    const/16 v1, 0xb9

    iget-object v2, p0, Lpdg;->g:Loya;

    .line 167
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 169
    :cond_d
    iget-object v1, p0, Lpdg;->o:Ljava/lang/String;

    if-eqz v1, :cond_e

    .line 170
    const/16 v1, 0xfe

    iget-object v2, p0, Lpdg;->o:Ljava/lang/String;

    .line 171
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 173
    :cond_e
    iget-object v1, p0, Lpdg;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 174
    iput v0, p0, Lpdg;->ai:I

    .line 175
    return v0

    :cond_f
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lpdg;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 183
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 184
    sparse-switch v0, :sswitch_data_0

    .line 188
    iget-object v2, p0, Lpdg;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 189
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lpdg;->ah:Ljava/util/List;

    .line 192
    :cond_1
    iget-object v2, p0, Lpdg;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 194
    :sswitch_0
    return-object p0

    .line 199
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpdg;->b:Ljava/lang/String;

    goto :goto_0

    .line 203
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpdg;->c:Ljava/lang/String;

    goto :goto_0

    .line 207
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpdg;->d:Ljava/lang/String;

    goto :goto_0

    .line 211
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpdg;->e:Ljava/lang/String;

    goto :goto_0

    .line 215
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpdg;->h:Ljava/lang/String;

    goto :goto_0

    .line 219
    :sswitch_6
    iget-object v0, p0, Lpdg;->i:Lpdi;

    if-nez v0, :cond_2

    .line 220
    new-instance v0, Lpdi;

    invoke-direct {v0}, Lpdi;-><init>()V

    iput-object v0, p0, Lpdg;->i:Lpdi;

    .line 222
    :cond_2
    iget-object v0, p0, Lpdg;->i:Lpdi;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 226
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpdg;->j:Ljava/lang/String;

    goto :goto_0

    .line 230
    :sswitch_8
    const/16 v0, 0x42

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 231
    iget-object v0, p0, Lpdg;->k:[Loya;

    if-nez v0, :cond_4

    move v0, v1

    .line 232
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Loya;

    .line 233
    iget-object v3, p0, Lpdg;->k:[Loya;

    if-eqz v3, :cond_3

    .line 234
    iget-object v3, p0, Lpdg;->k:[Loya;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 236
    :cond_3
    iput-object v2, p0, Lpdg;->k:[Loya;

    .line 237
    :goto_2
    iget-object v2, p0, Lpdg;->k:[Loya;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 238
    iget-object v2, p0, Lpdg;->k:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 239
    iget-object v2, p0, Lpdg;->k:[Loya;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 240
    invoke-virtual {p1}, Loxn;->a()I

    .line 237
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 231
    :cond_4
    iget-object v0, p0, Lpdg;->k:[Loya;

    array-length v0, v0

    goto :goto_1

    .line 243
    :cond_5
    iget-object v2, p0, Lpdg;->k:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 244
    iget-object v2, p0, Lpdg;->k:[Loya;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 248
    :sswitch_9
    iget-object v0, p0, Lpdg;->l:Loya;

    if-nez v0, :cond_6

    .line 249
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    iput-object v0, p0, Lpdg;->l:Loya;

    .line 251
    :cond_6
    iget-object v0, p0, Lpdg;->l:Loya;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 255
    :sswitch_a
    const/16 v0, 0x5a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 256
    iget-object v0, p0, Lpdg;->f:[Loya;

    if-nez v0, :cond_8

    move v0, v1

    .line 257
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Loya;

    .line 258
    iget-object v3, p0, Lpdg;->f:[Loya;

    if-eqz v3, :cond_7

    .line 259
    iget-object v3, p0, Lpdg;->f:[Loya;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 261
    :cond_7
    iput-object v2, p0, Lpdg;->f:[Loya;

    .line 262
    :goto_4
    iget-object v2, p0, Lpdg;->f:[Loya;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    .line 263
    iget-object v2, p0, Lpdg;->f:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 264
    iget-object v2, p0, Lpdg;->f:[Loya;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 265
    invoke-virtual {p1}, Loxn;->a()I

    .line 262
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 256
    :cond_8
    iget-object v0, p0, Lpdg;->f:[Loya;

    array-length v0, v0

    goto :goto_3

    .line 268
    :cond_9
    iget-object v2, p0, Lpdg;->f:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 269
    iget-object v2, p0, Lpdg;->f:[Loya;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 273
    :sswitch_b
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpdg;->m:Ljava/lang/String;

    goto/16 :goto_0

    .line 277
    :sswitch_c
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpdg;->n:Ljava/lang/String;

    goto/16 :goto_0

    .line 281
    :sswitch_d
    iget-object v0, p0, Lpdg;->g:Loya;

    if-nez v0, :cond_a

    .line 282
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    iput-object v0, p0, Lpdg;->g:Loya;

    .line 284
    :cond_a
    iget-object v0, p0, Lpdg;->g:Loya;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 288
    :sswitch_e
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpdg;->o:Ljava/lang/String;

    goto/16 :goto_0

    .line 184
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x5a -> :sswitch_a
        0x62 -> :sswitch_b
        0x25a -> :sswitch_c
        0x5ca -> :sswitch_d
        0x7f2 -> :sswitch_e
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 52
    iget-object v1, p0, Lpdg;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 53
    const/4 v1, 0x1

    iget-object v2, p0, Lpdg;->b:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 55
    :cond_0
    iget-object v1, p0, Lpdg;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 56
    const/4 v1, 0x2

    iget-object v2, p0, Lpdg;->c:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 58
    :cond_1
    iget-object v1, p0, Lpdg;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 59
    const/4 v1, 0x3

    iget-object v2, p0, Lpdg;->d:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 61
    :cond_2
    iget-object v1, p0, Lpdg;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 62
    const/4 v1, 0x4

    iget-object v2, p0, Lpdg;->e:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 64
    :cond_3
    iget-object v1, p0, Lpdg;->h:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 65
    const/4 v1, 0x5

    iget-object v2, p0, Lpdg;->h:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 67
    :cond_4
    iget-object v1, p0, Lpdg;->i:Lpdi;

    if-eqz v1, :cond_5

    .line 68
    const/4 v1, 0x6

    iget-object v2, p0, Lpdg;->i:Lpdi;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 70
    :cond_5
    iget-object v1, p0, Lpdg;->j:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 71
    const/4 v1, 0x7

    iget-object v2, p0, Lpdg;->j:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 73
    :cond_6
    iget-object v1, p0, Lpdg;->k:[Loya;

    if-eqz v1, :cond_8

    .line 74
    iget-object v2, p0, Lpdg;->k:[Loya;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_8

    aget-object v4, v2, v1

    .line 75
    if-eqz v4, :cond_7

    .line 76
    const/16 v5, 0x8

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 74
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 80
    :cond_8
    iget-object v1, p0, Lpdg;->l:Loya;

    if-eqz v1, :cond_9

    .line 81
    const/16 v1, 0x9

    iget-object v2, p0, Lpdg;->l:Loya;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 83
    :cond_9
    iget-object v1, p0, Lpdg;->f:[Loya;

    if-eqz v1, :cond_b

    .line 84
    iget-object v1, p0, Lpdg;->f:[Loya;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_b

    aget-object v3, v1, v0

    .line 85
    if-eqz v3, :cond_a

    .line 86
    const/16 v4, 0xb

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 84
    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 90
    :cond_b
    iget-object v0, p0, Lpdg;->m:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 91
    const/16 v0, 0xc

    iget-object v1, p0, Lpdg;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 93
    :cond_c
    iget-object v0, p0, Lpdg;->n:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 94
    const/16 v0, 0x4b

    iget-object v1, p0, Lpdg;->n:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 96
    :cond_d
    iget-object v0, p0, Lpdg;->g:Loya;

    if-eqz v0, :cond_e

    .line 97
    const/16 v0, 0xb9

    iget-object v1, p0, Lpdg;->g:Loya;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 99
    :cond_e
    iget-object v0, p0, Lpdg;->o:Ljava/lang/String;

    if-eqz v0, :cond_f

    .line 100
    const/16 v0, 0xfe

    iget-object v1, p0, Lpdg;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 102
    :cond_f
    iget-object v0, p0, Lpdg;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 104
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lpdg;->a(Loxn;)Lpdg;

    move-result-object v0

    return-object v0
.end method
