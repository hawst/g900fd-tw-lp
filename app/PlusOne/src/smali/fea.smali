.class final Lfea;
.super Landroid/text/style/URLSpan;
.source "PG"


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 119
    invoke-direct {p0, p1}, Landroid/text/style/URLSpan;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 122
    invoke-virtual {p0}, Lfea;->getURL()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 123
    const-string v1, "tel:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    .line 124
    const-string v2, "http://"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "https://"

    .line 125
    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    const/4 v0, 0x1

    .line 126
    :goto_0
    if-nez v1, :cond_1

    if-eqz v0, :cond_2

    .line 127
    :cond_1
    if-eqz v1, :cond_4

    .line 129
    new-instance v0, Lhmk;

    sget-object v1, Lonh;->b:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    .line 133
    :goto_1
    invoke-static {p1, v0}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    .line 134
    const/4 v0, 0x4

    invoke-static {p1, v0}, Lhly;->a(Landroid/view/View;I)V

    .line 136
    :cond_2
    invoke-super {p0, p1}, Landroid/text/style/URLSpan;->onClick(Landroid/view/View;)V

    .line 137
    return-void

    .line 125
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 131
    :cond_4
    new-instance v0, Lhmk;

    sget-object v1, Lonh;->c:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    goto :goto_1
.end method
