.class public Llro;
.super Lz;
.source "PG"


# instance fields
.field private e:I

.field public final f:Llqc;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Lz;-><init>()V

    .line 20
    new-instance v0, Llqc;

    invoke-direct {v0}, Llqc;-><init>()V

    iput-object v0, p0, Llro;->f:Llqc;

    return-void
.end method

.method private a(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 158
    iget v0, p0, Llro;->e:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Llro;->e:I

    if-nez v0, :cond_0

    .line 159
    iget-object v0, p0, Llro;->f:Llqc;

    invoke-virtual {v0, p1}, Llqc;->b(Landroid/content/Intent;)V

    .line 161
    :cond_0
    return-void
.end method

.method private h()V
    .locals 1

    .prologue
    .line 164
    iget v0, p0, Llro;->e:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Llro;->e:I

    .line 165
    return-void
.end method


# virtual methods
.method public a(Lu;Landroid/content/Intent;I)V
    .locals 0

    .prologue
    .line 124
    invoke-direct {p0, p2}, Llro;->a(Landroid/content/Intent;)V

    .line 125
    invoke-super {p0, p1, p2, p3}, Lz;->a(Lu;Landroid/content/Intent;I)V

    .line 126
    invoke-direct {p0}, Llro;->h()V

    .line 127
    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Llro;->f:Llqc;

    invoke-virtual {v0, p1}, Llqc;->a(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 202
    const/4 v0, 0x1

    .line 204
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lz;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public finish()V
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Llro;->f:Llqc;

    invoke-virtual {v0}, Llqc;->e()V

    .line 182
    invoke-super {p0}, Lz;->finish()V

    .line 183
    return-void
.end method

.method public l()Llqr;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Llro;->f:Llqc;

    return-object v0
.end method

.method public onActionModeFinished(Landroid/view/ActionMode;)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 195
    iget-object v0, p0, Llro;->f:Llqc;

    invoke-virtual {v0}, Llqc;->g()V

    .line 196
    invoke-super {p0, p1}, Lz;->onActionModeFinished(Landroid/view/ActionMode;)V

    .line 197
    return-void
.end method

.method public onActionModeStarted(Landroid/view/ActionMode;)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 188
    iget-object v0, p0, Llro;->f:Llqc;

    invoke-virtual {v0}, Llqc;->f()V

    .line 189
    invoke-super {p0, p1}, Lz;->onActionModeStarted(Landroid/view/ActionMode;)V

    .line 190
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Llro;->f:Llqc;

    invoke-virtual {v0, p1, p2, p3}, Llqc;->a(IILandroid/content/Intent;)V

    .line 98
    invoke-super {p0, p1, p2, p3}, Lz;->onActivityResult(IILandroid/content/Intent;)V

    .line 99
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Llro;->f:Llqc;

    invoke-virtual {v0}, Llqc;->i()Z

    move-result v0

    if-nez v0, :cond_0

    .line 234
    invoke-super {p0}, Lz;->onBackPressed()V

    .line 236
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Llro;->f:Llqc;

    invoke-virtual {v0, p1}, Llqc;->c(Landroid/os/Bundle;)V

    .line 30
    invoke-super {p0, p1}, Lz;->onCreate(Landroid/os/Bundle;)V

    .line 31
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Llro;->f:Llqc;

    invoke-virtual {v0, p1}, Llqc;->a(Landroid/view/Menu;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 210
    const/4 v0, 0x1

    .line 212
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lz;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Llro;->f:Llqc;

    invoke-virtual {v0}, Llqc;->c()V

    .line 86
    invoke-super {p0}, Lz;->onDestroy()V

    .line 87
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Llro;->f:Llqc;

    invoke-virtual {v0, p1}, Llqc;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 241
    const/4 v0, 0x1

    .line 243
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lz;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Llro;->f:Llqc;

    invoke-virtual {v0, p1}, Llqc;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 249
    const/4 v0, 0x1

    .line 251
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lz;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onLowMemory()V
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Llro;->f:Llqc;

    invoke-virtual {v0}, Llqc;->m()V

    .line 104
    invoke-super {p0}, Lz;->onLowMemory()V

    .line 105
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Llro;->f:Llqc;

    invoke-virtual {v0, p1}, Llqc;->a(Landroid/content/Intent;)V

    .line 170
    invoke-super {p0, p1}, Lz;->onNewIntent(Landroid/content/Intent;)V

    .line 171
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Llro;->f:Llqc;

    invoke-virtual {v0, p1}, Llqc;->a(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 226
    const/4 v0, 0x1

    .line 228
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lz;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Llro;->f:Llqc;

    invoke-virtual {v0}, Llqc;->b()V

    .line 68
    invoke-super {p0}, Lz;->onPause()V

    .line 69
    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Llro;->f:Llqc;

    invoke-virtual {v0, p1}, Llqc;->a(Landroid/os/Bundle;)V

    .line 36
    invoke-super {p0, p1}, Lz;->onPostCreate(Landroid/os/Bundle;)V

    .line 37
    return-void
.end method

.method protected onPostResume()V
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Llro;->f:Llqc;

    invoke-virtual {v0}, Llqc;->a()V

    .line 62
    invoke-super {p0}, Lz;->onPostResume()V

    .line 63
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Llro;->f:Llqc;

    invoke-virtual {v0}, Llqc;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 218
    const/4 v0, 0x1

    .line 220
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lz;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Llro;->f:Llqc;

    invoke-virtual {v0, p1}, Llqc;->b(Landroid/os/Bundle;)V

    .line 49
    invoke-super {p0, p1}, Lz;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 50
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 54
    invoke-virtual {p0}, Llro;->f()Lae;

    move-result-object v0

    invoke-static {v0}, Llrp;->a(Lae;)V

    .line 55
    iget-object v0, p0, Llro;->f:Llqc;

    invoke-virtual {v0}, Llqc;->k()V

    .line 56
    invoke-super {p0}, Lz;->onResume()V

    .line 57
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Llro;->f:Llqc;

    invoke-virtual {v0, p1}, Llqc;->d(Landroid/os/Bundle;)V

    .line 80
    invoke-super {p0, p1}, Lz;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 81
    return-void
.end method

.method protected onStart()V
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p0}, Llro;->f()Lae;

    move-result-object v0

    invoke-static {v0}, Llrp;->a(Lae;)V

    .line 42
    iget-object v0, p0, Llro;->f:Llqc;

    invoke-virtual {v0}, Llqc;->j()V

    .line 43
    invoke-super {p0}, Lz;->onStart()V

    .line 44
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Llro;->f:Llqc;

    invoke-virtual {v0}, Llqc;->l()V

    .line 74
    invoke-super {p0}, Lz;->onStop()V

    .line 75
    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Llro;->f:Llqc;

    invoke-virtual {v0}, Llqc;->d()V

    .line 176
    invoke-super {p0}, Lz;->onUserLeaveHint()V

    .line 177
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Llro;->f:Llqc;

    invoke-virtual {v0, p1}, Llqc;->a(Z)V

    .line 92
    invoke-super {p0, p1}, Lz;->onWindowFocusChanged(Z)V

    .line 93
    return-void
.end method

.method public startActivity(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 109
    invoke-direct {p0, p1}, Llro;->a(Landroid/content/Intent;)V

    .line 110
    invoke-super {p0, p1}, Lz;->startActivity(Landroid/content/Intent;)V

    .line 111
    invoke-direct {p0}, Llro;->h()V

    .line 112
    return-void
.end method

.method public startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V
    .locals 0
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 117
    invoke-direct {p0, p1}, Llro;->a(Landroid/content/Intent;)V

    .line 118
    invoke-super {p0, p1, p2}, Lz;->startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V

    .line 119
    invoke-direct {p0}, Llro;->h()V

    .line 120
    return-void
.end method

.method public startActivityForResult(Landroid/content/Intent;I)V
    .locals 0

    .prologue
    .line 140
    invoke-direct {p0, p1}, Llro;->a(Landroid/content/Intent;)V

    .line 141
    invoke-super {p0, p1, p2}, Lz;->startActivityForResult(Landroid/content/Intent;I)V

    .line 142
    invoke-direct {p0}, Llro;->h()V

    .line 143
    return-void
.end method

.method public startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V
    .locals 0
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 148
    invoke-direct {p0, p1}, Llro;->a(Landroid/content/Intent;)V

    .line 149
    invoke-super {p0, p1, p2, p3}, Lz;->startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V

    .line 150
    invoke-direct {p0}, Llro;->h()V

    .line 151
    return-void
.end method

.method public startActivityFromFragment(Landroid/app/Fragment;Landroid/content/Intent;ILandroid/os/Bundle;)V
    .locals 0
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 133
    invoke-direct {p0, p2}, Llro;->a(Landroid/content/Intent;)V

    .line 134
    invoke-super {p0, p1, p2, p3, p4}, Lz;->startActivityFromFragment(Landroid/app/Fragment;Landroid/content/Intent;ILandroid/os/Bundle;)V

    .line 135
    invoke-direct {p0}, Llro;->h()V

    .line 136
    return-void
.end method
