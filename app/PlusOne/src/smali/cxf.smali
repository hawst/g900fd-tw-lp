.class public Lcxf;
.super Llol;
.source "PG"


# static fields
.field public static final N:Ldgu;


# instance fields
.field private O:Ldgi;

.field private P:Ldgr;

.field private Q:Ldeo;

.field private R:Ldef;

.field private S:Lcom/google/android/apps/plus/views/PhotoView;

.field private T:Z

.field private final U:Lcxk;

.field private V:I

.field private final W:Ljava/lang/Runnable;

.field private final X:Lcxi;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    new-instance v0, Ldgp;

    invoke-direct {v0}, Ldgp;-><init>()V

    sput-object v0, Lcxf;->N:Ldgu;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 35
    invoke-direct {p0}, Llol;-><init>()V

    .line 54
    new-instance v0, Ldep;

    iget-object v1, p0, Lcxf;->av:Llqm;

    new-instance v2, Lcxg;

    invoke-direct {v2, p0}, Lcxg;-><init>(Lcxf;)V

    invoke-direct {v0, v1, v2}, Ldep;-><init>(Llqr;Ldeq;)V

    .line 62
    new-instance v0, Lcxk;

    invoke-direct {v0, p0}, Lcxk;-><init>(Lcxf;)V

    iput-object v0, p0, Lcxf;->U:Lcxk;

    .line 69
    const/4 v0, 0x1

    iput v0, p0, Lcxf;->V:I

    .line 72
    new-instance v0, Lcxh;

    invoke-direct {v0, p0}, Lcxh;-><init>(Lcxf;)V

    iput-object v0, p0, Lcxf;->W:Ljava/lang/Runnable;

    .line 81
    new-instance v0, Lcxi;

    invoke-direct {v0, p0}, Lcxi;-><init>(Lcxf;)V

    iput-object v0, p0, Lcxf;->X:Lcxi;

    .line 262
    return-void
.end method

.method static synthetic a(Lcxf;I)I
    .locals 0

    .prologue
    .line 35
    iput p1, p0, Lcxf;->V:I

    return p1
.end method

.method static synthetic a(Lcxf;)V
    .locals 4

    .prologue
    .line 35
    iget-object v0, p0, Lcxf;->Q:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcxf;->S:Lcom/google/android/apps/plus/views/PhotoView;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcxf;->Q:Ldeo;

    invoke-virtual {v1}, Ldeo;->e()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Lddl;->b()Lizu;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcxf;->S:Lcom/google/android/apps/plus/views/PhotoView;

    invoke-interface {v0}, Lddl;->b()Lizu;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/plus/views/PhotoView;->a(Lizu;Lnzi;)V

    :goto_1
    iget-boolean v1, p0, Lcxf;->T:Z

    if-eqz v1, :cond_2

    invoke-interface {v0}, Lddl;->F()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Lddl;->P()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcxf;->S:Lcom/google/android/apps/plus/views/PhotoView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/PhotoView;->j(Z)V

    :cond_2
    invoke-direct {p0}, Lcxf;->d()V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcxf;->S:Lcom/google/android/apps/plus/views/PhotoView;

    invoke-interface {v0}, Lddl;->a()Lizu;

    move-result-object v2

    invoke-interface {v0}, Lddl;->f()Lnzi;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/plus/views/PhotoView;->a(Lizu;Lnzi;)V

    goto :goto_1
.end method

.method static synthetic a(Lcxf;Z)Z
    .locals 0

    .prologue
    .line 35
    iput-boolean p1, p0, Lcxf;->T:Z

    return p1
.end method

.method static synthetic b(Lcxf;)Ldeo;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcxf;->Q:Ldeo;

    return-object v0
.end method

.method static synthetic c(Lcxf;)Lcom/google/android/apps/plus/views/PhotoView;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcxf;->S:Lcom/google/android/apps/plus/views/PhotoView;

    return-object v0
.end method

.method static synthetic d(Lcxf;)Ldgi;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcxf;->O:Ldgi;

    return-object v0
.end method

.method private d()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 183
    iget-object v0, p0, Lcxf;->S:Lcom/google/android/apps/plus/views/PhotoView;

    iget-object v2, p0, Lcxf;->W:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/PhotoView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 185
    iget-object v0, p0, Lcxf;->O:Ldgi;

    invoke-interface {v0}, Ldgi;->b()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcxf;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "for_animation"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcxf;->Q:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    invoke-interface {v0}, Lddl;->M()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    .line 186
    iget-object v0, p0, Lcxf;->S:Lcom/google/android/apps/plus/views/PhotoView;

    iget-object v1, p0, Lcxf;->W:Ljava/lang/Runnable;

    const-wide/16 v2, 0x96

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/plus/views/PhotoView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 190
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 185
    goto :goto_0

    .line 188
    :cond_1
    iget-object v0, p0, Lcxf;->S:Lcom/google/android/apps/plus/views/PhotoView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/PhotoView;->d(Z)V

    goto :goto_1
.end method

.method static synthetic e(Lcxf;)Ldef;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcxf;->R:Ldef;

    return-object v0
.end method

.method static synthetic f(Lcxf;)Ldgr;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcxf;->P:Ldgr;

    return-object v0
.end method

.method static synthetic g(Lcxf;)Lcxk;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcxf;->U:Lcxk;

    return-object v0
.end method

.method static synthetic h(Lcxf;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcxf;->d()V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 116
    iget-object v0, p0, Lcxf;->Q:Ldeo;

    if-nez v0, :cond_0

    .line 117
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must have a photoModel in onCreateView."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 120
    :cond_0
    const v0, 0x7f04017b

    invoke-virtual {p1, v0, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 121
    iget-object v0, p0, Lcxf;->Q:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v2

    .line 123
    const v0, 0x7f1004a5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/PhotoView;

    iput-object v0, p0, Lcxf;->S:Lcom/google/android/apps/plus/views/PhotoView;

    .line 124
    invoke-virtual {p0}, Lcxf;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "for_animation"

    invoke-virtual {v0, v3, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 125
    iget-object v0, p0, Lcxf;->S:Lcom/google/android/apps/plus/views/PhotoView;

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/views/PhotoView;->g(Z)V

    .line 130
    :cond_1
    iget-object v0, p0, Lcxf;->S:Lcom/google/android/apps/plus/views/PhotoView;

    invoke-interface {v2}, Lddl;->a()Lizu;

    move-result-object v3

    invoke-interface {v2}, Lddl;->f()Lnzi;

    move-result-object v2

    .line 131
    new-instance v4, Lgad;

    invoke-direct {v4, v5, v5, v5, v5}, Lgad;-><init>(IIIZ)V

    iget-object v5, p0, Lcxf;->U:Lcxk;

    .line 130
    invoke-virtual {v0, v3, v2, v4, v5}, Lcom/google/android/apps/plus/views/PhotoView;->a(Lizu;Lnzi;Lgad;Lgae;)V

    .line 132
    iget-object v0, p0, Lcxf;->S:Lcom/google/android/apps/plus/views/PhotoView;

    invoke-virtual {v0, v6}, Lcom/google/android/apps/plus/views/PhotoView;->h(Z)V

    .line 133
    iget-object v0, p0, Lcxf;->S:Lcom/google/android/apps/plus/views/PhotoView;

    new-instance v2, Lcxj;

    invoke-direct {v2, p0}, Lcxj;-><init>(Lcxf;)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/PhotoView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 134
    iget-object v0, p0, Lcxf;->S:Lcom/google/android/apps/plus/views/PhotoView;

    invoke-virtual {v0, v6}, Lcom/google/android/apps/plus/views/PhotoView;->a(Z)V

    .line 135
    iget-object v0, p0, Lcxf;->S:Lcom/google/android/apps/plus/views/PhotoView;

    invoke-virtual {v0, v6}, Lcom/google/android/apps/plus/views/PhotoView;->e(Z)V

    .line 137
    return-object v1
.end method

.method public a(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 142
    invoke-super {p0, p1, p2}, Llol;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 144
    invoke-direct {p0}, Lcxf;->d()V

    .line 145
    return-void
.end method

.method public a()Z
    .locals 2

    .prologue
    .line 172
    iget v0, p0, Lcxf;->V:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aO_()V
    .locals 3

    .prologue
    .line 149
    invoke-super {p0}, Llol;->aO_()V

    .line 151
    iget-object v0, p0, Lcxf;->O:Ldgi;

    iget-object v1, p0, Lcxf;->X:Lcxi;

    invoke-interface {v0, v1}, Ldgi;->a(Ldgl;)V

    .line 153
    invoke-virtual {p0}, Lcxf;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "for_animation"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 154
    iget-object v0, p0, Lcxf;->au:Llnh;

    const-class v1, Lfzz;

    .line 155
    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfzz;

    .line 156
    iget-object v1, p0, Lcxf;->S:Lcom/google/android/apps/plus/views/PhotoView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/views/PhotoView;->a(Lfzz;)V

    .line 159
    :cond_0
    invoke-direct {p0}, Lcxf;->d()V

    .line 160
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 86
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 87
    iget-object v0, p0, Lcxf;->au:Llnh;

    const-class v1, Ldgi;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldgi;

    iput-object v0, p0, Lcxf;->O:Ldgi;

    .line 88
    iget-object v0, p0, Lcxf;->au:Llnh;

    const-class v1, Ldgr;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldgr;

    iput-object v0, p0, Lcxf;->P:Ldgr;

    .line 90
    iget-object v0, p0, Lcxf;->au:Llnh;

    const-class v1, Ldeo;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldeo;

    iput-object v0, p0, Lcxf;->Q:Ldeo;

    .line 91
    iget-object v0, p0, Lcxf;->au:Llnh;

    const-class v1, Ldef;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldef;

    iput-object v0, p0, Lcxf;->R:Ldef;

    .line 92
    return-void
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 179
    iget v1, p0, Lcxf;->V:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public z()V
    .locals 2

    .prologue
    .line 164
    invoke-super {p0}, Llol;->z()V

    .line 165
    iget-object v0, p0, Lcxf;->O:Ldgi;

    iget-object v1, p0, Lcxf;->X:Lcxi;

    invoke-interface {v0, v1}, Ldgi;->b(Ldgl;)V

    .line 166
    return-void
.end method
