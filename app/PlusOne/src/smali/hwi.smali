.class public final Lhwi;
.super Landroid/view/ViewGroup;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Ljbb;
.implements Lljh;
.implements Lljj;


# instance fields
.field a:Lhwn;

.field private final b:Lhvx;

.field private c:Z

.field private d:I

.field private final e:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

.field private f:I

.field private final g:Landroid/widget/BaseAdapter;

.field private h:I

.field private i:Lhwe;

.field private final j:Landroid/view/animation/DecelerateInterpolator;

.field private k:I

.field private l:Landroid/animation/Animator$AnimatorListener;

.field private final m:Landroid/view/View;

.field private n:I

.field private o:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 114
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 135
    invoke-virtual {p0}, Lhwi;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 136
    iput-boolean v3, p0, Lhwi;->c:Z

    .line 138
    invoke-static {v0}, Lhvx;->a(Landroid/content/Context;)Lhvx;

    move-result-object v1

    iput-object v1, p0, Lhwi;->b:Lhvx;

    .line 140
    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    .line 141
    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    iput-object v1, p0, Lhwi;->j:Landroid/view/animation/DecelerateInterpolator;

    .line 143
    new-instance v1, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    invoke-direct {v1, v0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lhwi;->e:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    .line 144
    iget-object v1, p0, Lhwi;->e:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    invoke-static {v1}, Llii;->g(Landroid/view/View;)V

    .line 145
    iget-object v1, p0, Lhwi;->e:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    invoke-virtual {v1, p0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->a(Lljj;)V

    .line 146
    iget-object v1, p0, Lhwi;->e:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    invoke-virtual {v1, v3}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->a(I)V

    .line 147
    iget-object v1, p0, Lhwi;->e:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->a(Z)V

    .line 148
    iget-object v1, p0, Lhwi;->e:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    const v2, 0x7f0b010b

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->setBackgroundResource(I)V

    .line 149
    iget-object v1, p0, Lhwi;->e:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    new-instance v2, Lhwj;

    invoke-direct {v2}, Lhwj;-><init>()V

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->a(Lljl;)V

    .line 158
    new-instance v1, Landroid/view/View;

    invoke-direct {v1, v0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lhwi;->m:Landroid/view/View;

    .line 159
    iget-object v1, p0, Lhwi;->m:Landroid/view/View;

    const v2, 0x7f020416

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 161
    iget-object v1, p0, Lhwi;->m:Landroid/view/View;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 163
    new-instance v1, Lhwe;

    invoke-direct {v1, v0}, Lhwe;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lhwi;->i:Lhwe;

    .line 164
    iget-object v0, p0, Lhwi;->i:Lhwe;

    invoke-virtual {v0, v3}, Lhwe;->setVisibility(I)V

    .line 166
    invoke-static {}, Llsj;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 167
    new-instance v0, Lhwl;

    invoke-direct {v0, p0}, Lhwl;-><init>(Lhwi;)V

    iput-object v0, p0, Lhwi;->l:Landroid/animation/Animator$AnimatorListener;

    .line 170
    :cond_0
    new-instance v0, Lhwk;

    invoke-direct {v0, p0}, Lhwk;-><init>(Lhwi;)V

    iput-object v0, p0, Lhwi;->g:Landroid/widget/BaseAdapter;

    .line 115
    return-void
.end method

.method static synthetic a(Lhwi;)I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lhwi;->d:I

    return v0
.end method

.method private a(I)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v1, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    .line 335
    if-nez p1, :cond_1

    .line 397
    :cond_0
    return-void

    .line 340
    :cond_1
    iget v0, p0, Lhwi;->d:I

    if-le v0, v7, :cond_2

    .line 341
    int-to-float v0, p1

    const v2, 0x3f666666    # 0.9f

    mul-float/2addr v0, v2

    float-to-int p1, v0

    :cond_2
    move v0, v1

    .line 344
    :goto_0
    iget v2, p0, Lhwi;->d:I

    if-ge v0, v2, :cond_6

    .line 345
    iget-object v2, p0, Lhwi;->a:Lhwn;

    invoke-virtual {v2, v0}, Lhwn;->a(I)Lhwm;

    move-result-object v2

    .line 347
    iget-object v3, v2, Lhwm;->a:Lizu;

    invoke-virtual {v3}, Lizu;->g()Ljac;

    move-result-object v3

    .line 348
    iget v4, v2, Lhwm;->c:I

    int-to-float v4, v4

    mul-float/2addr v4, v6

    iget v5, v2, Lhwm;->d:I

    int-to-float v5, v5

    div-float/2addr v4, v5

    .line 354
    const/high16 v5, 0x3f000000    # 0.5f

    cmpl-float v5, v4, v5

    if-ltz v5, :cond_3

    iget v5, v2, Lhwm;->d:I

    if-gt v5, p1, :cond_4

    :cond_3
    iget v5, p0, Lhwi;->d:I

    if-eq v5, v7, :cond_4

    sget-object v5, Ljac;->c:Ljac;

    if-ne v3, v5, :cond_5

    .line 357
    :cond_4
    iput p1, v2, Lhwm;->d:I

    .line 360
    iget v3, v2, Lhwm;->d:I

    int-to-float v3, v3

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, v2, Lhwm;->c:I

    .line 344
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 364
    :cond_6
    const v0, 0x7fffffff

    iput v0, p0, Lhwi;->f:I

    move v0, v1

    .line 366
    :goto_1
    iget v2, p0, Lhwi;->d:I

    if-ge v0, v2, :cond_a

    .line 367
    iget-object v2, p0, Lhwi;->a:Lhwn;

    invoke-virtual {v2, v0}, Lhwn;->a(I)Lhwm;

    move-result-object v2

    .line 370
    iget v3, v2, Lhwm;->d:I

    iget-object v4, p0, Lhwi;->b:Lhvx;

    iget v4, v4, Lhvx;->k:I

    if-ge v3, v4, :cond_7

    .line 371
    iget-object v3, p0, Lhwi;->b:Lhvx;

    iget v3, v3, Lhvx;->k:I

    int-to-float v3, v3

    mul-float/2addr v3, v6

    iget v4, v2, Lhwm;->d:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    .line 372
    iget v4, v2, Lhwm;->d:I

    int-to-float v4, v4

    mul-float/2addr v4, v3

    float-to-int v4, v4

    iput v4, v2, Lhwm;->d:I

    .line 373
    iget v4, v2, Lhwm;->c:I

    int-to-float v4, v4

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, v2, Lhwm;->c:I

    .line 376
    :cond_7
    iget v3, v2, Lhwm;->c:I

    iget-object v4, p0, Lhwi;->b:Lhvx;

    iget v4, v4, Lhvx;->k:I

    if-ge v3, v4, :cond_8

    .line 377
    iget-object v3, p0, Lhwi;->b:Lhvx;

    iget v3, v3, Lhvx;->k:I

    int-to-float v3, v3

    mul-float/2addr v3, v6

    iget v4, v2, Lhwm;->c:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    .line 378
    iget v4, v2, Lhwm;->d:I

    int-to-float v4, v4

    mul-float/2addr v4, v3

    float-to-int v4, v4

    iput v4, v2, Lhwm;->d:I

    .line 379
    iget v4, v2, Lhwm;->c:I

    int-to-float v4, v4

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, v2, Lhwm;->c:I

    .line 383
    :cond_8
    iget v3, v2, Lhwm;->c:I

    iget v4, p0, Lhwi;->f:I

    if-ge v3, v4, :cond_9

    .line 384
    iget v2, v2, Lhwm;->c:I

    iput v2, p0, Lhwi;->f:I

    .line 366
    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 389
    :cond_a
    :goto_2
    iget v0, p0, Lhwi;->d:I

    if-ge v1, v0, :cond_0

    .line 390
    iget-object v0, p0, Lhwi;->a:Lhwn;

    invoke-virtual {v0, v1}, Lhwn;->a(I)Lhwm;

    move-result-object v0

    .line 391
    iget v2, v0, Lhwm;->c:I

    iget v3, p0, Lhwi;->f:I

    if-le v2, v3, :cond_b

    .line 392
    iget v2, p0, Lhwi;->f:I

    int-to-float v2, v2

    mul-float/2addr v2, v6

    iget v3, v0, Lhwm;->c:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    .line 393
    iget v3, v0, Lhwm;->d:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, v0, Lhwm;->d:I

    .line 394
    iget v2, p0, Lhwi;->f:I

    iput v2, v0, Lhwm;->c:I

    .line 389
    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method

.method static synthetic a(Lhwi;I)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lhwi;->b(I)V

    return-void
.end method

.method static synthetic b(Lhwi;)Lhvx;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lhwi;->b:Lhvx;

    return-object v0
.end method

.method private b(I)V
    .locals 8
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const-wide/16 v6, 0x96

    const/4 v5, 0x1

    const/4 v4, 0x4

    const/4 v3, 0x0

    .line 454
    iget v0, p0, Lhwi;->h:I

    if-ne v0, p1, :cond_0

    .line 522
    :goto_0
    return-void

    .line 457
    :cond_0
    iput p1, p0, Lhwi;->h:I

    .line 459
    invoke-static {}, Llsj;->c()Z

    move-result v0

    .line 461
    iget v1, p0, Lhwi;->h:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 463
    :pswitch_0
    iget-object v0, p0, Lhwi;->i:Lhwe;

    invoke-virtual {v0, v3}, Lhwe;->setVisibility(I)V

    .line 464
    iget-object v0, p0, Lhwi;->m:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 465
    iget-object v0, p0, Lhwi;->m:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setClickable(Z)V

    goto :goto_0

    .line 470
    :pswitch_1
    if-eqz v0, :cond_3

    .line 471
    iget-object v0, p0, Lhwi;->i:Lhwe;

    invoke-virtual {v0}, Lhwe;->getAlpha()F

    move-result v0

    const v1, 0x3f7fbe77    # 0.999f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    .line 472
    iget-object v0, p0, Lhwi;->i:Lhwe;

    invoke-virtual {v0, v3}, Lhwe;->setVisibility(I)V

    .line 473
    iget-object v0, p0, Lhwi;->i:Lhwe;

    invoke-static {v0}, Llii;->h(Landroid/view/View;)V

    .line 474
    iget-object v0, p0, Lhwi;->i:Lhwe;

    invoke-virtual {v0}, Lhwe;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 475
    iget-object v1, p0, Lhwi;->j:Landroid/view/animation/DecelerateInterpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const/high16 v2, 0x3f800000    # 1.0f

    .line 476
    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v6, v7}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    iget-object v2, p0, Lhwi;->l:Landroid/animation/Animator$AnimatorListener;

    .line 477
    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 478
    invoke-static {}, Llsj;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 479
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    .line 481
    :cond_1
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 487
    :cond_2
    :goto_1
    iget-object v0, p0, Lhwi;->m:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 488
    iget-object v0, p0, Lhwi;->m:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setClickable(Z)V

    goto :goto_0

    .line 484
    :cond_3
    iput v3, p0, Lhwi;->h:I

    .line 485
    iget-object v0, p0, Lhwi;->i:Lhwe;

    invoke-virtual {v0, v3}, Lhwe;->setVisibility(I)V

    goto :goto_1

    .line 493
    :pswitch_2
    iget-object v0, p0, Lhwi;->i:Lhwe;

    invoke-virtual {v0, v4}, Lhwe;->setVisibility(I)V

    .line 517
    :cond_4
    :goto_2
    iget-object v0, p0, Lhwi;->m:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 518
    iget-object v0, p0, Lhwi;->m:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setClickable(Z)V

    goto :goto_0

    .line 500
    :pswitch_3
    if-eqz v0, :cond_6

    .line 501
    iget-object v0, p0, Lhwi;->i:Lhwe;

    invoke-virtual {v0}, Lhwe;->getAlpha()F

    move-result v0

    const v1, 0x3a83126f    # 0.001f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_4

    .line 502
    iget-object v0, p0, Lhwi;->i:Lhwe;

    invoke-static {v0}, Llii;->h(Landroid/view/View;)V

    .line 503
    iget-object v0, p0, Lhwi;->i:Lhwe;

    invoke-virtual {v0, v3}, Lhwe;->setVisibility(I)V

    .line 504
    iget-object v0, p0, Lhwi;->i:Lhwe;

    invoke-virtual {v0}, Lhwe;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 505
    iget-object v1, p0, Lhwi;->j:Landroid/view/animation/DecelerateInterpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const/4 v2, 0x0

    .line 506
    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v6, v7}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    iget-object v2, p0, Lhwi;->l:Landroid/animation/Animator$AnimatorListener;

    .line 507
    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 508
    invoke-static {}, Llsj;->d()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 509
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    .line 511
    :cond_5
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto :goto_2

    .line 514
    :cond_6
    const/4 v0, 0x2

    iput v0, p0, Lhwi;->h:I

    .line 515
    iget-object v0, p0, Lhwi;->i:Lhwe;

    invoke-virtual {v0, v4}, Lhwe;->setVisibility(I)V

    goto :goto_2

    .line 461
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private b()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 603
    iget-object v1, p0, Lhwi;->e:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getChildCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 604
    iget-object v1, p0, Lhwi;->e:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->a()I

    move-result v1

    if-nez v1, :cond_0

    .line 605
    iget-object v1, p0, Lhwi;->e:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 606
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 609
    :cond_0
    return v0
.end method

.method static synthetic c(Lhwi;)I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lhwi;->f:I

    return v0
.end method

.method private c()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 616
    iget-object v0, p0, Lhwi;->i:Lhwe;

    invoke-virtual {v0}, Lhwe;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-ne v0, p0, :cond_0

    .line 617
    iget-object v0, p0, Lhwi;->i:Lhwe;

    invoke-static {v0}, Llii;->h(Landroid/view/View;)V

    .line 618
    iget-object v0, p0, Lhwi;->i:Lhwe;

    invoke-virtual {v0}, Lhwe;->b()V

    .line 619
    iget-object v0, p0, Lhwi;->i:Lhwe;

    invoke-virtual {p0, v0}, Lhwi;->removeView(Landroid/view/View;)V

    .line 620
    iget-object v0, p0, Lhwi;->m:Landroid/view/View;

    invoke-virtual {p0, v0}, Lhwi;->removeView(Landroid/view/View;)V

    .line 623
    :cond_0
    iget-object v0, p0, Lhwi;->a:Lhwn;

    if-eqz v0, :cond_1

    .line 624
    iget-object v0, p0, Lhwi;->a:Lhwn;

    invoke-virtual {v0}, Lhwn;->a()V

    .line 627
    :cond_1
    iput v1, p0, Lhwi;->k:I

    .line 628
    iput v1, p0, Lhwi;->n:I

    .line 629
    iput v1, p0, Lhwi;->f:I

    .line 630
    iput v1, p0, Lhwi;->d:I

    .line 631
    iput-boolean v1, p0, Lhwi;->c:Z

    .line 632
    iput v1, p0, Lhwi;->o:I

    .line 634
    iget-object v0, p0, Lhwi;->e:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->a(Landroid/widget/BaseAdapter;)V

    .line 635
    iget-object v0, p0, Lhwi;->e:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    invoke-virtual {p0, v0}, Lhwi;->removeView(Landroid/view/View;)V

    .line 636
    return-void
.end method

.method static synthetic d(Lhwi;)I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lhwi;->h:I

    return v0
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 640
    invoke-direct {p0}, Lhwi;->c()V

    .line 641
    return-void
.end method

.method public a(Lcom/google/android/libraries/social/media/ui/MediaView;)V
    .locals 2

    .prologue
    .line 526
    iget-object v0, p0, Lhwi;->i:Lhwe;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lhwe;->a(Z)V

    .line 527
    return-void
.end method

.method public a(Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;I)V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 531
    if-eq p2, v1, :cond_1

    .line 532
    const/4 v0, 0x0

    iput v0, p0, Lhwi;->n:I

    .line 533
    invoke-direct {p0}, Lhwi;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 534
    iget v0, p0, Lhwi;->h:I

    if-eq v0, v1, :cond_0

    iget v0, p0, Lhwi;->h:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 536
    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lhwi;->b(I)V

    .line 540
    :cond_1
    return-void
.end method

.method public a(Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;III)V
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x1

    .line 545
    if-nez p3, :cond_1

    .line 578
    :cond_0
    :goto_0
    return-void

    .line 549
    :cond_1
    iget v0, p0, Lhwi;->n:I

    if-eqz v0, :cond_5

    .line 550
    iget v0, p0, Lhwi;->n:I

    if-gez v0, :cond_4

    .line 552
    if-ltz p3, :cond_5

    .line 553
    iput p3, p0, Lhwi;->n:I

    .line 567
    :goto_1
    iget v0, p0, Lhwi;->n:I

    iget v1, p0, Lhwi;->k:I

    if-gt v0, v1, :cond_2

    invoke-direct {p0}, Lhwi;->b()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 568
    :cond_2
    iget v0, p0, Lhwi;->h:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    iget v0, p0, Lhwi;->h:I

    if-ne v0, v3, :cond_0

    .line 570
    :cond_3
    invoke-direct {p0, v2}, Lhwi;->b(I)V

    goto :goto_0

    .line 558
    :cond_4
    if-gez p3, :cond_5

    .line 559
    iput p3, p0, Lhwi;->n:I

    goto :goto_1

    .line 561
    :cond_5
    iget v0, p0, Lhwi;->n:I

    add-int/2addr v0, p3

    iput v0, p0, Lhwi;->n:I

    goto :goto_1

    .line 572
    :cond_6
    iget v0, p0, Lhwi;->n:I

    iget v1, p0, Lhwi;->k:I

    neg-int v1, v1

    if-ge v0, v1, :cond_0

    .line 573
    iget v0, p0, Lhwi;->h:I

    if-eqz v0, :cond_7

    iget v0, p0, Lhwi;->h:I

    if-ne v0, v2, :cond_0

    .line 575
    :cond_7
    invoke-direct {p0, v3}, Lhwi;->b(I)V

    goto :goto_0
.end method

.method public a(Lhur;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 305
    invoke-direct {p0}, Lhwi;->c()V

    .line 307
    if-nez p1, :cond_0

    .line 331
    :goto_0
    return-void

    .line 310
    :cond_0
    invoke-virtual {p0}, Lhwi;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Lhwi;->k:I

    .line 312
    invoke-virtual {p1}, Lhur;->b()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lhur;->b()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 313
    iget-object v0, p0, Lhwi;->a:Lhwn;

    if-nez v0, :cond_1

    .line 314
    new-instance v0, Lhwn;

    invoke-direct {v0, p0}, Lhwn;-><init>(Lhwi;)V

    iput-object v0, p0, Lhwi;->a:Lhwn;

    .line 316
    :cond_1
    iget-object v0, p0, Lhwi;->a:Lhwn;

    invoke-virtual {p1}, Lhur;->b()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhwn;->a(Ljava/util/ArrayList;)V

    .line 317
    iget-object v0, p0, Lhwi;->a:Lhwn;

    invoke-virtual {v0}, Lhwn;->b()I

    move-result v0

    iput v0, p0, Lhwi;->d:I

    .line 320
    :cond_2
    iget-object v0, p0, Lhwi;->e:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    iget-object v1, p0, Lhwi;->g:Landroid/widget/BaseAdapter;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->a(Landroid/widget/BaseAdapter;)V

    .line 321
    iget-object v0, p0, Lhwi;->e:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    invoke-virtual {p0, v0}, Lhwi;->addView(Landroid/view/View;)V

    .line 323
    invoke-virtual {p1}, Lhur;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lhur;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 324
    :cond_3
    iget-object v0, p0, Lhwi;->i:Lhwe;

    invoke-virtual {v0, p1}, Lhwe;->a(Lhur;)V

    .line 325
    iget-object v0, p0, Lhwi;->i:Lhwe;

    invoke-virtual {v0, v2}, Lhwe;->a(Z)V

    .line 326
    iget-object v0, p0, Lhwi;->i:Lhwe;

    invoke-virtual {p0, v0}, Lhwi;->addView(Landroid/view/View;)V

    .line 327
    iget-object v0, p0, Lhwi;->m:Landroid/view/View;

    invoke-virtual {p0, v0}, Lhwi;->addView(Landroid/view/View;)V

    .line 328
    iput v2, p0, Lhwi;->h:I

    .line 330
    :cond_4
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhwi;->c:Z

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 582
    iget-object v0, p0, Lhwi;->a:Lhwn;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhwi;->a:Lhwn;

    invoke-virtual {v0}, Lhwn;->b()I

    move-result v0

    if-nez v0, :cond_1

    .line 600
    :cond_0
    :goto_0
    return-void

    .line 585
    :cond_1
    iget-object v0, p0, Lhwi;->m:Landroid/view/View;

    if-ne p1, v0, :cond_2

    .line 587
    const-string v0, "ScrollingMediaCardVG"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 591
    :cond_2
    check-cast p1, Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 592
    invoke-virtual {p1}, Lcom/google/android/libraries/social/media/ui/MediaView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 593
    iget-object v1, p0, Lhwi;->a:Lhwn;

    invoke-virtual {v1, v0}, Lhwn;->a(I)Lhwm;

    move-result-object v0

    iget-object v0, v0, Lhwm;->b:Lhjy;

    .line 594
    if-eqz v0, :cond_0

    .line 597
    invoke-virtual {p0}, Lhwi;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lhjz;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 433
    iget-object v0, p0, Lhwi;->e:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    .line 434
    invoke-virtual {p0}, Lhwi;->getMeasuredWidth()I

    move-result v0

    .line 435
    invoke-virtual {p0}, Lhwi;->getMeasuredHeight()I

    move-result v1

    .line 437
    iget-object v2, p0, Lhwi;->e:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    invoke-virtual {v2, v4, v4, v0, v1}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->layout(IIII)V

    .line 439
    iget-object v2, p0, Lhwi;->i:Lhwe;

    invoke-virtual {v2}, Lhwe;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-ne v2, p0, :cond_0

    .line 440
    iget-object v2, p0, Lhwi;->i:Lhwe;

    invoke-virtual {v2}, Lhwe;->getMeasuredHeight()I

    move-result v2

    sub-int v2, v1, v2

    .line 441
    iget-object v3, p0, Lhwi;->i:Lhwe;

    invoke-virtual {v3, v4, v2, v0, v1}, Lhwe;->layout(IIII)V

    .line 443
    iget-object v0, p0, Lhwi;->m:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-ne v0, p0, :cond_0

    .line 444
    iget-object v0, p0, Lhwi;->i:Lhwe;

    invoke-virtual {v0}, Lhwe;->c()Landroid/graphics/Rect;

    move-result-object v0

    .line 445
    iget-object v1, p0, Lhwi;->m:Landroid/view/View;

    iget v3, v0, Landroid/graphics/Rect;->left:I

    iget v4, v0, Landroid/graphics/Rect;->top:I

    add-int/2addr v4, v2

    iget v5, v0, Landroid/graphics/Rect;->right:I

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v3, v4, v5, v0}, Landroid/view/View;->layout(IIII)V

    .line 450
    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/high16 v4, 0x40000000    # 2.0f

    .line 401
    iget-boolean v0, p0, Lhwi;->c:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x40

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " expected to have been bound with valid data. Was bind() called?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 403
    :cond_0
    invoke-virtual {p0}, Lhwi;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lhvj;->a(Landroid/content/Context;I)I

    move-result v0

    .line 404
    invoke-virtual {p0}, Lhwi;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    if-nez v2, :cond_3

    invoke-static {v1}, Llsc;->a(Landroid/content/Context;)Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 407
    :goto_0
    iget v1, p0, Lhwi;->o:I

    if-eq v1, v0, :cond_1

    .line 410
    invoke-direct {p0, v0}, Lhwi;->a(I)V

    .line 411
    iput v0, p0, Lhwi;->o:I

    .line 414
    :cond_1
    iget-object v1, p0, Lhwi;->e:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    iget v3, p0, Lhwi;->f:I

    .line 415
    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 414
    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->measure(II)V

    .line 417
    iget-object v1, p0, Lhwi;->i:Lhwe;

    invoke-virtual {v1}, Lhwe;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-ne v1, p0, :cond_2

    .line 418
    iget-object v1, p0, Lhwi;->i:Lhwe;

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 419
    invoke-static {v5, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 418
    invoke-virtual {v1, v2, v3}, Lhwe;->measure(II)V

    .line 421
    iget-object v1, p0, Lhwi;->m:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-ne v1, p0, :cond_2

    .line 422
    iget-object v1, p0, Lhwi;->i:Lhwe;

    invoke-virtual {v1}, Lhwe;->c()Landroid/graphics/Rect;

    move-result-object v1

    .line 423
    iget-object v2, p0, Lhwi;->m:Landroid/view/View;

    .line 424
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v3

    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 425
    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 423
    invoke-virtual {v2, v3, v1}, Landroid/view/View;->measure(II)V

    .line 428
    :cond_2
    iget v1, p0, Lhwi;->f:I

    invoke-virtual {p0, v0, v1}, Lhwi;->setMeasuredDimension(II)V

    .line 429
    return-void

    .line 404
    :cond_3
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    goto :goto_0
.end method
