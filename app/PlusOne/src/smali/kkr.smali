.class public Lkkr;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llqz;
.implements Llrd;
.implements Llrg;


# instance fields
.field private final a:Landroid/app/Activity;

.field private b:Lkey;

.field private c:Lkng;

.field private d:Ljava/lang/String;

.field private e:Landroid/os/Bundle;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Llqr;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lkkr;->a:Landroid/app/Activity;

    .line 48
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 49
    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)V
    .locals 14

    .prologue
    const/4 v7, 0x0

    .line 53
    if-nez p1, :cond_a

    .line 54
    iget-object v0, p0, Lkkr;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v13

    .line 56
    invoke-virtual {v13}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 57
    if-eqz v0, :cond_3

    .line 58
    const-string v1, "com.google.android.apps.plus.SHARE_GOOGLE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lkkr;->a:Landroid/app/Activity;

    .line 59
    const-string v1, "com.google.android.apps.plus.API_KEY"

    invoke-virtual {v13, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.google.android.apps.plus.CLIENT_ID"

    invoke-virtual {v13, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.google.android.apps.plus.VERSION"

    invoke-virtual {v13, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v0}, Lkey;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    const-string v3, "calling_package"

    invoke-virtual {v13, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0, v3}, Landroid/content/pm/PackageManager;->checkSignatures(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "com.android.vending"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "com.google.android.music"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_0
    const-string v8, "659910861946.apps.googleusercontent.com"

    :cond_1
    new-instance v0, Lkey;

    invoke-static {v3, v9}, Lkje;->a(Ljava/lang/String;Landroid/content/pm/PackageManager;)Ljava/lang/String;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, Lkey;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v6, Lkey;

    invoke-static {v3, v9}, Lkje;->a(Ljava/lang/String;Landroid/content/pm/PackageManager;)Ljava/lang/String;

    move-result-object v10

    move-object v9, v3

    move-object v11, v5

    move-object v12, v0

    invoke-direct/range {v6 .. v12}, Lkey;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lkey;)V

    move-object v7, v6

    .line 60
    :cond_2
    :goto_0
    iput-object v7, p0, Lkkr;->b:Lkey;

    .line 63
    :cond_3
    const-string v0, "com.google.android.apps.plus.CALL_TO_ACTION"

    invoke-virtual {v13, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lkng;->a(Landroid/os/Bundle;)Lkng;

    move-result-object v0

    iput-object v0, p0, Lkkr;->c:Lkng;

    .line 66
    const-string v0, "com.google.android.apps.plus.CONTENT_DEEP_LINK_ID"

    invoke-static {v13, v0}, Lkje;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkkr;->d:Ljava/lang/String;

    .line 69
    const-string v0, "com.google.android.apps.plus.CONTENT_DEEP_LINK_METADATA"

    invoke-virtual {v13, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lkkr;->e:Landroid/os/Bundle;

    .line 78
    :goto_1
    return-void

    .line 59
    :cond_4
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_5
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_6
    iget-object v1, p0, Lkkr;->a:Landroid/app/Activity;

    .line 60
    const-string v0, "android.support.v4.app.EXTRA_CALLING_PACKAGE"

    invoke-virtual {v13, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7

    const-string v0, "calling_package"

    invoke-virtual {v13, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_7
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-virtual {v1}, Landroid/app/Activity;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v2

    if-eqz v2, :cond_8

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    :cond_8
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_9

    const-string v0, "com.google.android.apps.social"

    :cond_9
    const-string v2, "com.google.android.apps.plus.VERSION"

    invoke-virtual {v13, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lkje;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lkey;

    move-result-object v7

    goto :goto_0

    .line 72
    :cond_a
    const-string v0, "apiary_api_info"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lkey;

    iput-object v0, p0, Lkkr;->b:Lkey;

    .line 73
    const-string v0, "call_to_action"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lkng;

    iput-object v0, p0, Lkkr;->c:Lkng;

    .line 74
    const-string v0, "content_deep_link_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkkr;->d:Ljava/lang/String;

    .line 75
    const-string v0, "content_deep_link_metadata"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lkkr;->e:Landroid/os/Bundle;

    goto :goto_1
.end method

.method public a()Z
    .locals 2

    .prologue
    .line 154
    iget-object v0, p0, Lkkr;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lkkr;->e:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lkkr;->e:Landroid/os/Bundle;

    const-string v1, "title"

    .line 156
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lkkr;->e:Landroid/os/Bundle;

    const-string v1, "description"

    .line 158
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Lkey;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lkkr;->b:Lkey;

    return-object v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 82
    const-string v0, "apiary_api_info"

    iget-object v1, p0, Lkkr;->b:Lkey;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 83
    const-string v0, "call_to_action"

    iget-object v1, p0, Lkkr;->c:Lkng;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 84
    const-string v0, "content_deep_link_id"

    iget-object v1, p0, Lkkr;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    const-string v0, "content_deep_link_metadata"

    iget-object v1, p0, Lkkr;->e:Landroid/os/Bundle;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 86
    return-void
.end method

.method public c()Lkng;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lkkr;->c:Lkng;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lkkr;->d:Ljava/lang/String;

    return-object v0
.end method

.method public e()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lkkr;->e:Landroid/os/Bundle;

    return-object v0
.end method
