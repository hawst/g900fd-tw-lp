.class public final enum Ljix;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Ljix;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Ljix;

.field public static final enum b:Ljix;

.field public static final enum c:Ljix;

.field public static final enum d:Ljix;

.field private static final synthetic e:[Ljix;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 9
    new-instance v0, Ljix;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2}, Ljix;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ljix;->a:Ljix;

    .line 10
    new-instance v0, Ljix;

    const-string v1, "POLL"

    invoke-direct {v0, v1, v3}, Ljix;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ljix;->b:Ljix;

    .line 11
    new-instance v0, Ljix;

    const-string v1, "REAL_TIME"

    invoke-direct {v0, v1, v4}, Ljix;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ljix;->c:Ljix;

    .line 12
    new-instance v0, Ljix;

    const-string v1, "USER_INITIATED"

    invoke-direct {v0, v1, v5}, Ljix;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ljix;->d:Ljix;

    .line 8
    const/4 v0, 0x4

    new-array v0, v0, [Ljix;

    sget-object v1, Ljix;->a:Ljix;

    aput-object v1, v0, v2

    sget-object v1, Ljix;->b:Ljix;

    aput-object v1, v0, v3

    sget-object v1, Ljix;->c:Ljix;

    aput-object v1, v0, v4

    sget-object v1, Ljix;->d:Ljix;

    aput-object v1, v0, v5

    sput-object v0, Ljix;->e:[Ljix;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 24
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ljix;
    .locals 1

    .prologue
    .line 8
    const-class v0, Ljix;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ljix;

    return-object v0
.end method

.method public static values()[Ljix;
    .locals 1

    .prologue
    .line 8
    sget-object v0, Ljix;->e:[Ljix;

    invoke-virtual {v0}, [Ljix;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljix;

    return-object v0
.end method
