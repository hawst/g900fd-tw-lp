.class public final Lmze;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmza;

.field private b:Lmzf;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 77
    iput-object v0, p0, Lmze;->a:Lmza;

    .line 80
    iput-object v0, p0, Lmze;->b:Lmzf;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 102
    const/4 v0, 0x0

    .line 103
    iget-object v1, p0, Lmze;->a:Lmza;

    if-eqz v1, :cond_0

    .line 104
    const/4 v0, 0x1

    iget-object v1, p0, Lmze;->a:Lmza;

    .line 105
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 107
    :cond_0
    iget-object v1, p0, Lmze;->b:Lmzf;

    if-eqz v1, :cond_1

    .line 108
    const/4 v1, 0x2

    iget-object v2, p0, Lmze;->b:Lmzf;

    .line 109
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 111
    :cond_1
    iget-object v1, p0, Lmze;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 112
    const/4 v1, 0x3

    iget-object v2, p0, Lmze;->c:Ljava/lang/String;

    .line 113
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 115
    :cond_2
    iget-object v1, p0, Lmze;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 116
    iput v0, p0, Lmze;->ai:I

    .line 117
    return v0
.end method

.method public a(Loxn;)Lmze;
    .locals 2

    .prologue
    .line 125
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 126
    sparse-switch v0, :sswitch_data_0

    .line 130
    iget-object v1, p0, Lmze;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 131
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmze;->ah:Ljava/util/List;

    .line 134
    :cond_1
    iget-object v1, p0, Lmze;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 136
    :sswitch_0
    return-object p0

    .line 141
    :sswitch_1
    iget-object v0, p0, Lmze;->a:Lmza;

    if-nez v0, :cond_2

    .line 142
    new-instance v0, Lmza;

    invoke-direct {v0}, Lmza;-><init>()V

    iput-object v0, p0, Lmze;->a:Lmza;

    .line 144
    :cond_2
    iget-object v0, p0, Lmze;->a:Lmza;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 148
    :sswitch_2
    iget-object v0, p0, Lmze;->b:Lmzf;

    if-nez v0, :cond_3

    .line 149
    new-instance v0, Lmzf;

    invoke-direct {v0}, Lmzf;-><init>()V

    iput-object v0, p0, Lmze;->b:Lmzf;

    .line 151
    :cond_3
    iget-object v0, p0, Lmze;->b:Lmzf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 155
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmze;->c:Ljava/lang/String;

    goto :goto_0

    .line 126
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 87
    iget-object v0, p0, Lmze;->a:Lmza;

    if-eqz v0, :cond_0

    .line 88
    const/4 v0, 0x1

    iget-object v1, p0, Lmze;->a:Lmza;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 90
    :cond_0
    iget-object v0, p0, Lmze;->b:Lmzf;

    if-eqz v0, :cond_1

    .line 91
    const/4 v0, 0x2

    iget-object v1, p0, Lmze;->b:Lmzf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 93
    :cond_1
    iget-object v0, p0, Lmze;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 94
    const/4 v0, 0x3

    iget-object v1, p0, Lmze;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 96
    :cond_2
    iget-object v0, p0, Lmze;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 98
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lmze;->a(Loxn;)Lmze;

    move-result-object v0

    return-object v0
.end method
