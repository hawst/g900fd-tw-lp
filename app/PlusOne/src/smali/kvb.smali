.class public final Lkvb;
.super Lhny;
.source "PG"


# instance fields
.field private final a:I

.field private final b:Lkfo;

.field private final c:Ljava/lang/String;

.field private final d:I

.field private final e:Ljava/lang/String;

.field private final f:I

.field private final h:Lktq;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;ILjava/lang/String;I)V
    .locals 2

    .prologue
    .line 41
    const-string v0, "ReadSquareMembersTask"

    invoke-direct {p0, p1, v0}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 43
    iput p2, p0, Lkvb;->a:I

    .line 44
    new-instance v0, Lkfo;

    iget v1, p0, Lkvb;->a:I

    invoke-direct {v0, p1, v1}, Lkfo;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lkvb;->b:Lkfo;

    .line 45
    iput-object p3, p0, Lkvb;->c:Ljava/lang/String;

    .line 46
    iput p4, p0, Lkvb;->d:I

    .line 47
    iput-object p5, p0, Lkvb;->e:Ljava/lang/String;

    .line 48
    iput p6, p0, Lkvb;->f:I

    .line 49
    const-class v0, Lktq;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lktq;

    iput-object v0, p0, Lkvb;->h:Lktq;

    .line 50
    return-void
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 54
    new-instance v0, Lkva;

    invoke-virtual {p0}, Lkvb;->f()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lkvb;->b:Lkfo;

    iget-object v3, p0, Lkvb;->c:Ljava/lang/String;

    iget v4, p0, Lkvb;->d:I

    iget-object v5, p0, Lkvb;->e:Ljava/lang/String;

    iget v6, p0, Lkvb;->f:I

    invoke-direct/range {v0 .. v6}, Lkva;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;ILjava/lang/String;I)V

    .line 56
    invoke-virtual {v0}, Lkva;->l()V

    .line 58
    invoke-virtual {v0}, Lkva;->t()Z

    move-result v1

    if-nez v1, :cond_2

    .line 59
    iget-object v2, p0, Lkvb;->h:Lktq;

    .line 60
    invoke-virtual {v0}, Lkva;->D()Loxu;

    move-result-object v1

    check-cast v1, Lmib;

    iget-object v1, v1, Lmib;->a:Lntt;

    .line 62
    :try_start_0
    iget-object v3, v1, Lntt;->b:Lnsr;

    if-eqz v3, :cond_0

    .line 63
    iget v3, p0, Lkvb;->a:I

    iget-object v4, v1, Lntt;->b:Lnsr;

    invoke-interface {v2, v3, v4}, Lktq;->a(ILnsr;)Z

    .line 66
    :cond_0
    iget-object v1, v1, Lntt;->a:[Lnry;

    .line 67
    if-nez v1, :cond_1

    .line 68
    sget-object v1, Lnry;->a:[Lnry;

    .line 71
    :cond_1
    iget-object v3, p0, Lkvb;->e:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 72
    iget v3, p0, Lkvb;->a:I

    iget-object v4, p0, Lkvb;->c:Ljava/lang/String;

    invoke-interface {v2, v3, v4, v1}, Lktq;->b(ILjava/lang/String;[Lnry;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 81
    :cond_2
    :goto_0
    new-instance v1, Lhoz;

    iget v2, v0, Lkff;->i:I

    iget-object v0, v0, Lkff;->k:Ljava/lang/Exception;

    invoke-direct {v1, v2, v0, v7}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    move-object v0, v1

    :goto_1
    return-object v0

    .line 74
    :cond_3
    :try_start_1
    iget v3, p0, Lkvb;->a:I

    iget-object v4, p0, Lkvb;->c:Ljava/lang/String;

    invoke-interface {v2, v3, v4, v1}, Lktq;->a(ILjava/lang/String;[Lnry;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 76
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 77
    new-instance v0, Lhoz;

    const/4 v2, 0x0

    invoke-direct {v0, v2, v1, v7}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    goto :goto_1
.end method
