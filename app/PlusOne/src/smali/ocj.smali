.class public final Locj;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field private b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 906
    invoke-direct {p0}, Loxq;-><init>()V

    .line 909
    const/high16 v0, -0x80000000

    iput v0, p0, Locj;->b:I

    .line 906
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 928
    const/4 v0, 0x0

    .line 929
    iget v1, p0, Locj;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 930
    const/4 v0, 0x1

    iget v1, p0, Locj;->b:I

    .line 931
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 933
    :cond_0
    iget-object v1, p0, Locj;->a:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 934
    const/4 v1, 0x2

    iget-object v2, p0, Locj;->a:Ljava/lang/String;

    .line 935
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 937
    :cond_1
    iget-object v1, p0, Locj;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 938
    iput v0, p0, Locj;->ai:I

    .line 939
    return v0
.end method

.method public a(Loxn;)Locj;
    .locals 3

    .prologue
    const/16 v2, 0x12

    .line 947
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 948
    sparse-switch v0, :sswitch_data_0

    .line 952
    iget-object v1, p0, Locj;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 953
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Locj;->ah:Ljava/util/List;

    .line 956
    :cond_1
    iget-object v1, p0, Locj;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 958
    :sswitch_0
    return-object p0

    .line 963
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 964
    if-eq v0, v2, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    const/16 v1, 0x13

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9

    if-eq v0, v1, :cond_2

    const/16 v1, 0x10

    if-eq v0, v1, :cond_2

    const/16 v1, 0x11

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa

    if-eq v0, v1, :cond_2

    const/16 v1, 0xb

    if-eq v0, v1, :cond_2

    const/16 v1, 0xc

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd

    if-eq v0, v1, :cond_2

    const/16 v1, 0x1b

    if-eq v0, v1, :cond_2

    const/16 v1, 0x14

    if-eq v0, v1, :cond_2

    const/16 v1, 0xe

    if-eq v0, v1, :cond_2

    const/16 v1, 0xf

    if-eq v0, v1, :cond_2

    const/16 v1, 0x19

    if-eq v0, v1, :cond_2

    const/16 v1, 0x20

    if-eq v0, v1, :cond_2

    const/16 v1, 0x15

    if-eq v0, v1, :cond_2

    const/16 v1, 0x16

    if-eq v0, v1, :cond_2

    const/16 v1, 0x17

    if-eq v0, v1, :cond_2

    const/16 v1, 0x18

    if-eq v0, v1, :cond_2

    const/16 v1, 0x1a

    if-eq v0, v1, :cond_2

    const/16 v1, 0x1c

    if-eq v0, v1, :cond_2

    const/16 v1, 0x1d

    if-eq v0, v1, :cond_2

    const/16 v1, 0x1e

    if-eq v0, v1, :cond_2

    const/16 v1, 0x1f

    if-ne v0, v1, :cond_3

    .line 996
    :cond_2
    iput v0, p0, Locj;->b:I

    goto/16 :goto_0

    .line 998
    :cond_3
    iput v2, p0, Locj;->b:I

    goto/16 :goto_0

    .line 1003
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Locj;->a:Ljava/lang/String;

    goto/16 :goto_0

    .line 948
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 916
    iget v0, p0, Locj;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 917
    const/4 v0, 0x1

    iget v1, p0, Locj;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 919
    :cond_0
    iget-object v0, p0, Locj;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 920
    const/4 v0, 0x2

    iget-object v1, p0, Locj;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 922
    :cond_1
    iget-object v0, p0, Locj;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 924
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 902
    invoke-virtual {p0, p1}, Locj;->a(Loxn;)Locj;

    move-result-object v0

    return-object v0
.end method
