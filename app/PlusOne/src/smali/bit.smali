.class public Lbit;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/content/ContentResolver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lbit;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbit;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentResolver;)V
    .locals 2

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    const-string v0, "contentResolver"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentResolver;

    iput-object v0, p0, Lbit;->b:Landroid/content/ContentResolver;

    .line 39
    return-void
.end method

.method private static a(B)I
    .locals 0

    .prologue
    .line 137
    if-gez p0, :cond_0

    .line 138
    add-int/lit16 p0, p0, 0x100

    .line 140
    :cond_0
    return p0
.end method

.method private static a([BI)I
    .locals 2

    .prologue
    .line 130
    aget-byte v0, p0, p1

    invoke-static {v0}, Lbit;->a(B)I

    move-result v0

    shl-int/lit8 v0, v0, 0x18

    add-int/lit8 v1, p1, 0x1

    aget-byte v1, p0, v1

    .line 131
    invoke-static {v1}, Lbit;->a(B)I

    move-result v1

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    add-int/lit8 v1, p1, 0x2

    aget-byte v1, p0, v1

    .line 132
    invoke-static {v1}, Lbit;->a(B)I

    move-result v1

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    add-int/lit8 v1, p1, 0x3

    aget-byte v1, p0, v1

    .line 133
    invoke-static {v1}, Lbit;->a(B)I

    move-result v1

    or-int/2addr v0, v1

    return v0
.end method

.method private static a(Landroid/net/Uri;Landroid/content/ContentResolver;)[B
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/16 v2, 0x8

    .line 90
    invoke-static {p0, p1}, Lbqd;->a(Landroid/net/Uri;Landroid/content/ContentResolver;)Ljava/io/InputStream;

    move-result-object v1

    .line 91
    if-nez v1, :cond_0

    .line 108
    :goto_0
    return-object v0

    .line 94
    :cond_0
    new-array v2, v2, [B

    .line 97
    :goto_1
    const/16 v3, 0x8

    :try_start_0
    invoke-static {v1, v2, v3}, Lcgs;->a(Ljava/io/InputStream;[BI)V

    .line 98
    const/4 v3, 0x0

    invoke-static {v2, v3}, Lbit;->a([BI)I

    move-result v3

    .line 99
    const/4 v4, 0x4

    invoke-static {v2, v4}, Lbit;->a([BI)I

    move-result v4

    sget v5, Lbik;->a:I

    if-ne v4, v5, :cond_1

    .line 100
    add-int/lit8 v2, v3, -0x8

    invoke-static {v1, v2}, Lbit;->a(Ljava/io/InputStream;I)[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 108
    invoke-static {v1}, Lcek;->a(Ljava/io/Closeable;)V

    goto :goto_0

    .line 102
    :cond_1
    add-int/lit8 v3, v3, -0x8

    int-to-long v4, v3

    :try_start_1
    invoke-virtual {v1, v4, v5}, Ljava/io/InputStream;->skip(J)J
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v2

    .line 106
    invoke-static {v1}, Lcek;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v1}, Lcek;->a(Ljava/io/Closeable;)V

    throw v0
.end method

.method private static a(Ljava/io/InputStream;I)[B
    .locals 1

    .prologue
    .line 115
    :try_start_0
    invoke-static {p0, p1}, Lcgs;->a(Ljava/io/InputStream;I)[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 117
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a([B)[B
    .locals 3

    .prologue
    .line 70
    :try_start_0
    const-string v0, "MD5"

    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/security/MessageDigest;->digest([B)[B
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 73
    :goto_0
    return-object v0

    .line 71
    :catch_0
    move-exception v0

    .line 72
    sget-object v1, Lbit;->a:Ljava/lang/String;

    const-string v2, "MD5 not supported"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 73
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/net/Uri;)[B
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 46
    iget-object v0, p0, Lbit;->b:Landroid/content/ContentResolver;

    invoke-static {p1, v0}, Lbit;->a(Landroid/net/Uri;Landroid/content/ContentResolver;)[B

    move-result-object v0

    .line 47
    if-nez v0, :cond_2

    .line 49
    iget-object v0, p0, Lbit;->b:Landroid/content/ContentResolver;

    invoke-static {p1, v0}, Lbqd;->a(Landroid/net/Uri;Landroid/content/ContentResolver;)Ljava/io/InputStream;

    move-result-object v2

    if-nez v2, :cond_0

    move-object v0, v1

    .line 50
    :goto_0
    if-nez v0, :cond_1

    .line 51
    sget-object v0, Lbit;->a:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x22

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Cannot compute hash of video file "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 64
    :goto_1
    return-object v0

    .line 49
    :cond_0
    const/16 v0, 0x1000

    invoke-static {v2, v0}, Lbit;->a(Ljava/io/InputStream;I)[B

    move-result-object v0

    invoke-static {v2}, Lcek;->a(Ljava/io/Closeable;)V

    goto :goto_0

    .line 54
    :cond_1
    sget-object v1, Lbit;->a:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x36

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Cannot locate moov box; falling back on start of file "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 58
    :cond_2
    invoke-static {v0}, Lbit;->a([B)[B

    move-result-object v0

    goto :goto_1
.end method
