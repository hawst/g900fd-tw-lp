.class public final Lctj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lctm;


# instance fields
.field final a:Lcre;

.field private final b:Landroid/graphics/Rect;

.field private final c:I


# direct methods
.method public constructor <init>(Lcre;ILandroid/graphics/Rect;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lctj;->a:Lcre;

    .line 33
    iput p2, p0, Lctj;->c:I

    .line 34
    iput-object p3, p0, Lctj;->b:Landroid/graphics/Rect;

    .line 35
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 95
    iget v0, p0, Lctj;->c:I

    return v0
.end method

.method public a(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 39
    if-nez p1, :cond_3

    .line 40
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 41
    const v1, 0x7f040040

    invoke-virtual {v0, v1, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 42
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getMeasuredWidth()I

    move-result v2

    iget-object v0, p0, Lctj;->b:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    if-eq v0, v2, :cond_1

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v0, 0x7f0c0071

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    mul-int/lit8 v4, v2, 0x64

    div-int v0, v4, v0

    const v4, 0x7f0d02a2

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    if-lez v3, :cond_0

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    :cond_0
    iget-object v3, p0, Lctj;->b:Landroid/graphics/Rect;

    invoke-virtual {v3, v5, v5, v2, v0}, Landroid/graphics/Rect;->set(IIII)V

    .line 43
    :cond_1
    new-instance v0, Landroid/widget/AbsListView$LayoutParams;

    iget-object v2, p0, Lctj;->b:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    iget-object v3, p0, Lctj;->b:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    invoke-direct {v0, v2, v3}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :goto_0
    move-object v0, v1

    .line 45
    check-cast v0, Lcom/google/android/apps/plus/views/AlbumCoverView;

    .line 46
    iget-object v2, p0, Lctj;->a:Lcre;

    iget-object v2, v2, Lcre;->c:Lizu;

    if-eqz v2, :cond_2

    .line 47
    iget-object v2, p0, Lctj;->a:Lcre;

    iget-object v2, v2, Lcre;->c:Lizu;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/AlbumCoverView;->a(Lizu;)V

    .line 49
    :cond_2
    iget-object v2, p0, Lctj;->a:Lcre;

    iget-object v2, v2, Lcre;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/AlbumCoverView;->a(Ljava/lang/CharSequence;)V

    .line 50
    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/AlbumCoverView;->requestLayout()V

    .line 51
    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/AlbumCoverView;->invalidate()V

    .line 53
    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/views/AlbumCoverView;->setVisibility(I)V

    .line 56
    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v2, Lctl;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctl;

    .line 58
    new-instance v2, Lctk;

    invoke-direct {v2, p0, v0}, Lctk;-><init>(Lctj;Lctl;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 65
    return-object v1

    :cond_3
    move-object v1, p1

    goto :goto_0
.end method

.method public a(Lctn;)V
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lctj;->a:Lcre;

    invoke-interface {p1, v0}, Lctn;->a(Lcre;)V

    .line 101
    return-void
.end method
