.class public final Lhsb;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:J

.field private static final b:J

.field private static final c:Landroid/net/Uri;

.field private static final d:Lloz;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 47
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lhsb;->a:J

    .line 48
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    .line 49
    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lhsb;->b:J

    .line 55
    const-string v0, "https://support.google.com/plus/?p=auto_backup"

    .line 56
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lhsb;->c:Landroid/net/Uri;

    .line 62
    new-instance v0, Lloz;

    const-string v1, "debug.plus.ab_setup_notif"

    invoke-direct {v0, v1}, Lloz;-><init>(Ljava/lang/String;)V

    sput-object v0, Lhsb;->d:Lloz;

    return-void
.end method

.method public static a()J
    .locals 4

    .prologue
    .line 392
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x5a

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    const-wide/16 v2, 0x2

    div-long/2addr v0, v2

    return-wide v0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 136
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 138
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, ":notifications:setup_backup"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f100055

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 140
    return-void
.end method

.method public static a(Landroid/content/Context;I)V
    .locals 3

    .prologue
    .line 77
    const-string v0, "notification"

    .line 78
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 80
    invoke-static {p0, p1}, Lhsb;->d(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f100056

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 81
    return-void
.end method

.method public static a(Landroid/content/Context;IIIZ)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 96
    sub-int v0, p3, p2

    invoke-static {v0, v6}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 97
    invoke-static {p0, v0}, Lhsb;->b(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    .line 98
    if-eqz p4, :cond_0

    const v0, 0x7f0a0434

    move v1, v0

    .line 100
    :goto_0
    const-class v0, Lhsa;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhsa;

    .line 103
    invoke-interface {v0, p0, p1}, Lhsa;->a(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v0

    .line 104
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 105
    const/high16 v3, 0x14000000

    invoke-virtual {v0, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 107
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    long-to-int v3, v4

    invoke-static {p0, v3, v0, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 109
    new-instance v3, Lbs;

    invoke-direct {v3, p0}, Lbs;-><init>(Landroid/content/Context;)V

    .line 110
    const v4, 0x7f02059f

    invoke-virtual {v3, v4}, Lbs;->a(I)Lbs;

    .line 111
    invoke-virtual {v3, v7}, Lbs;->b(Z)Lbs;

    .line 112
    const v4, 0x7f0a0436

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lbs;->a(Ljava/lang/CharSequence;)Lbs;

    .line 113
    new-array v4, v7, [Ljava/lang/Object;

    aput-object v2, v4, v6

    invoke-virtual {p0, v1, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lbs;->e(Ljava/lang/CharSequence;)Lbs;

    .line 114
    new-array v4, v7, [Ljava/lang/Object;

    aput-object v2, v4, v6

    invoke-virtual {p0, v1, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Lbs;->b(Ljava/lang/CharSequence;)Lbs;

    .line 115
    invoke-virtual {v3, v0}, Lbs;->a(Landroid/app/PendingIntent;)Lbs;

    .line 117
    const-string v0, "notification"

    .line 118
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 120
    invoke-static {p0, p1}, Lhsb;->d(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f100056

    .line 121
    invoke-virtual {v3}, Lbs;->b()Landroid/app/Notification;

    move-result-object v3

    .line 120
    invoke-virtual {v0, v1, v2, v3}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 122
    return-void

    .line 98
    :cond_0
    const v0, 0x7f0a0435

    move v1, v0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 315
    const-class v0, Lhsa;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhsa;

    .line 316
    invoke-interface {v0, p0, p1}, Lhsa;->b(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v0

    .line 317
    invoke-static {p0}, Lda;->a(Landroid/content/Context;)Lda;

    move-result-object v1

    .line 318
    invoke-virtual {v1, v0}, Lda;->b(Landroid/content/Intent;)Lda;

    .line 320
    invoke-static {p0}, Lhqd;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 348
    :goto_0
    return-void

    .line 324
    :cond_0
    new-instance v2, Lbs;

    invoke-direct {v2, p0}, Lbs;-><init>(Landroid/content/Context;)V

    .line 326
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    long-to-int v0, v4

    const/high16 v3, 0x8000000

    .line 325
    invoke-virtual {v1, v0, v3}, Lda;->a(II)Landroid/app/PendingIntent;

    move-result-object v0

    .line 327
    const v1, 0x7f0a043d

    .line 328
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 329
    const v3, 0x7f0a043c

    .line 330
    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 332
    invoke-virtual {v2, v3}, Lbs;->a(Ljava/lang/CharSequence;)Lbs;

    move-result-object v3

    new-array v4, v6, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p2, v4, v5

    .line 333
    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Lbs;->b(Ljava/lang/CharSequence;)Lbs;

    move-result-object v1

    const v3, 0x7f020366

    .line 334
    invoke-virtual {v1, v3}, Lbs;->a(I)Lbs;

    move-result-object v1

    .line 335
    invoke-virtual {v1, v0}, Lbs;->a(Landroid/app/PendingIntent;)Lbs;

    move-result-object v0

    const v1, 0x7f0204ce

    const v3, 0x7f0a043f

    .line 337
    invoke-virtual {p0, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    .line 338
    invoke-static {p0, p3}, Lcom/google/android/libraries/social/autobackup/FolderAutoBackupReceiver;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v4

    .line 336
    invoke-virtual {v0, v1, v3, v4}, Lbs;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Lbs;

    move-result-object v0

    const v1, 0x7f0204f2

    const v3, 0x7f0a043e

    .line 340
    invoke-virtual {p0, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    .line 341
    invoke-static {p0, p3}, Lcom/google/android/libraries/social/autobackup/FolderAutoBackupReceiver;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v4

    .line 339
    invoke-virtual {v0, v1, v3, v4}, Lbs;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Lbs;

    move-result-object v0

    .line 342
    invoke-virtual {v0, v6}, Lbs;->b(Z)Lbs;

    .line 343
    const-string v0, "notification"

    .line 344
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 346
    const v1, 0x7f100057

    .line 347
    invoke-virtual {v2}, Lbs;->c()Landroid/app/Notification;

    move-result-object v2

    .line 346
    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;J)V
    .locals 5

    .prologue
    .line 285
    .line 286
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 287
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 288
    const-string v0, "iu.auto_backup_notification_timestamp"

    invoke-interface {v1, v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 290
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 291
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    .line 292
    const-string v2, "iu.auto_backup_notification_version_code"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 296
    :goto_0
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 297
    return-void

    .line 293
    :catch_0
    move-exception v0

    .line 294
    const-string v2, "ABNotifications"

    const-string v3, "Cannot get package version"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;JZ)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 240
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 242
    invoke-static {p0, v6, p1, p2}, Lcom/google/android/libraries/social/autobackup/AutoBackupStalledReceiver;->a(Landroid/content/Context;ZJ)Landroid/app/PendingIntent;

    move-result-object v0

    .line 245
    if-nez v0, :cond_0

    .line 246
    const-string v0, "alarm"

    .line 247
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 248
    const/4 v1, 0x1

    .line 249
    invoke-static {p0, v1, p1, p2}, Lcom/google/android/libraries/social/autobackup/AutoBackupStalledReceiver;->a(Landroid/content/Context;ZJ)Landroid/app/PendingIntent;

    move-result-object v1

    .line 252
    if-eqz p3, :cond_1

    sget-wide v2, Lhsb;->b:J

    :goto_0
    add-long/2addr v2, v4

    invoke-virtual {v0, v6, v2, v3, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 258
    :cond_0
    return-void

    .line 252
    :cond_1
    sget-wide v2, Lhsb;->a:J

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;I)Ljava/lang/String;
    .locals 10

    .prologue
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 200
    const/16 v0, 0x384

    if-ge p1, v0, :cond_0

    .line 201
    const v0, 0x7f0a0439

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 208
    :goto_0
    return-object v0

    .line 202
    :cond_0
    const v0, 0xe1000

    if-ge p1, v0, :cond_1

    .line 204
    const v0, 0x7f0a043a

    new-array v1, v1, [Ljava/lang/Object;

    int-to-double v2, p1

    const-wide/high16 v4, 0x4090000000000000L    # 1024.0

    div-double/2addr v2, v4

    .line 205
    invoke-static {v2, v3, v8, v9}, Ljava/lang/Math;->max(DD)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v1, v6

    .line 204
    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 208
    :cond_1
    const v0, 0x7f0a043b

    new-array v1, v1, [Ljava/lang/Object;

    int-to-double v2, p1

    const-wide/high16 v4, 0x4130000000000000L    # 1048576.0

    div-double/2addr v2, v4

    .line 209
    invoke-static {v2, v3, v8, v9}, Ljava/lang/Math;->max(DD)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v1, v6

    .line 208
    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 218
    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    .line 219
    invoke-static {p0, v0, v2, v3}, Lcom/google/android/libraries/social/autobackup/AutoBackupStalledReceiver;->a(Landroid/content/Context;ZJ)Landroid/app/PendingIntent;

    move-result-object v1

    .line 222
    if-eqz v1, :cond_0

    .line 223
    const-string v0, "alarm"

    .line 224
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 225
    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 227
    :cond_0
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, ":notifications:backup_stalled"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f100055

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 228
    return-void
.end method

.method public static c(Landroid/content/Context;)J
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 264
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 265
    const-string v0, "iu.auto_backup_notification_timestamp"

    invoke-interface {v2, v0}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 269
    const-string v0, "accounts"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 271
    const-string v1, "ab_notification_timestamp"

    invoke-interface {v0, v1, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 273
    cmp-long v3, v0, v4

    if-lez v3, :cond_0

    .line 274
    invoke-static {p0, v0, v1}, Lhsb;->a(Landroid/content/Context;J)V

    .line 278
    :goto_0
    return-wide v0

    :cond_0
    const-string v0, "iu.auto_backup_notification_timestamp"

    invoke-interface {v2, v0, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    goto :goto_0
.end method

.method public static c(Landroid/content/Context;I)V
    .locals 0

    .prologue
    .line 442
    invoke-static {p0}, Lhsb;->b(Landroid/content/Context;)V

    .line 443
    invoke-static {p0, p1}, Lhsb;->a(Landroid/content/Context;I)V

    .line 444
    return-void
.end method

.method private static d(Landroid/content/Context;I)Ljava/lang/String;
    .locals 5

    .prologue
    .line 383
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 384
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "gaia_id"

    .line 385
    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 386
    const/4 v1, -0x1

    if-ne p1, v1, :cond_0

    const/4 v0, 0x0

    .line 387
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x16

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":notifications:gaiaid:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static d(Landroid/content/Context;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 351
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 352
    const-string v0, "com.google.android.libraries.social.autobackup.notifications.migration_notification"

    invoke-interface {v1, v0, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 353
    if-eqz v0, :cond_0

    .line 376
    :goto_0
    return-void

    .line 357
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    sget-object v3, Lhsb;->c:Landroid/net/Uri;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 359
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    long-to-int v2, v2

    .line 358
    invoke-static {p0, v2, v0, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 360
    new-instance v2, Lbs;

    invoke-direct {v2, p0}, Lbs;-><init>(Landroid/content/Context;)V

    const v3, 0x7f020366

    .line 361
    invoke-virtual {v2, v3}, Lbs;->a(I)Lbs;

    move-result-object v2

    .line 362
    invoke-virtual {v2, v5}, Lbs;->b(Z)Lbs;

    move-result-object v2

    const v3, 0x7f0a0442

    .line 363
    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lbs;->a(Ljava/lang/CharSequence;)Lbs;

    move-result-object v2

    const v3, 0x7f0a0443

    .line 364
    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lbs;->b(Ljava/lang/CharSequence;)Lbs;

    move-result-object v2

    .line 365
    invoke-virtual {v2, v0}, Lbs;->a(Landroid/app/PendingIntent;)Lbs;

    move-result-object v0

    .line 366
    invoke-virtual {v0}, Lbs;->c()Landroid/app/Notification;

    move-result-object v2

    .line 368
    const-string v0, "notification"

    .line 369
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 371
    const v3, 0x7f100058

    invoke-virtual {v0, v3, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 373
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "com.google.android.libraries.social.autobackup.notifications.migration_notification"

    .line 374
    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 375
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0
.end method

.method public static e(Landroid/content/Context;)V
    .locals 9

    .prologue
    .line 399
    const-class v0, Lhpu;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhpu;

    .line 401
    invoke-virtual {v0}, Lhpu;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 427
    :cond_0
    :goto_0
    return-void

    .line 405
    :cond_1
    invoke-static {p0}, Lhqd;->h(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lhsb;->d:Lloz;

    goto :goto_0

    .line 410
    :cond_2
    invoke-static {p0}, Lhqd;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 415
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 416
    invoke-static {p0}, Lhsb;->c(Landroid/content/Context;)J

    move-result-wide v0

    .line 417
    sub-long v0, v2, v0

    sget-object v4, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v6, 0x5a

    invoke-virtual {v4, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    cmp-long v0, v0, v4

    if-ltz v0, :cond_0

    .line 423
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.google"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    new-instance v4, Ljava/util/ArrayList;

    array-length v0, v1

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    array-length v5, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v5, :cond_4

    aget-object v6, v1, v0

    iget-object v7, v6, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "@youtube.com"

    invoke-virtual {v7, v8}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_3

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 426
    const-class v0, Lhsa;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhsa;

    invoke-interface {v0, p0}, Lhsa;->a(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v0, "notification"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-static {p0, v2, v3}, Lhsb;->a(Landroid/content/Context;J)V

    new-instance v2, Lbs;

    invoke-direct {v2, p0}, Lbs;-><init>(Landroid/content/Context;)V

    const v3, 0x7f020366

    invoke-virtual {v2, v3}, Lbs;->a(I)Lbs;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lbs;->b(Z)Lbs;

    const v3, 0x7f0a0437

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lbs;->a(Ljava/lang/CharSequence;)Lbs;

    const v3, 0x7f0a0438

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lbs;->b(Ljava/lang/CharSequence;)Lbs;

    invoke-virtual {v2, v1}, Lbs;->a(Landroid/app/PendingIntent;)Lbs;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v3, ":notifications:setup_backup"

    invoke-virtual {v1, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const v3, 0x7f100055

    invoke-virtual {v2}, Lbs;->c()Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    const-class v0, Lhpg;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhpg;

    invoke-interface {v0, p0}, Lhpg;->a(Landroid/content/Context;)V

    goto/16 :goto_0
.end method

.method public static f(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 433
    invoke-static {p0}, Lhqd;->j(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 434
    invoke-static {p0}, Lhsb;->b(Landroid/content/Context;)V

    .line 436
    :cond_0
    return-void
.end method
