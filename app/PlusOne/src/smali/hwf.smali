.class public final Lhwf;
.super Landroid/view/ViewGroup;
.source "PG"

# interfaces
.implements Lljh;


# instance fields
.field private final a:Lhve;

.field private b:Z

.field private c:Ljava/lang/String;

.field private d:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

.field private e:I

.field private f:Ljava/lang/String;

.field private g:Z

.field private h:Landroid/widget/TextView;

.field private i:Z

.field private j:Landroid/widget/TextView;

.field private k:I

.field private l:Lhuz;

.field private m:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 74
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 89
    invoke-virtual {p0}, Lhwf;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lhve;->a(Landroid/content/Context;)Lhve;

    move-result-object v0

    iput-object v0, p0, Lhwf;->a:Lhve;

    .line 90
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lhwf;->setWillNotDraw(Z)V

    .line 92
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhwf;->b:Z

    .line 75
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 470
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhwf;->b:Z

    .line 471
    return-void
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, Lhwf;->d:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    if-eqz v0, :cond_0

    .line 268
    iget-object v0, p0, Lhwf;->d:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->b(I)V

    .line 270
    :cond_0
    return-void
.end method

.method public a(Lhuf;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 134
    iget-object v0, p0, Lhwf;->l:Lhuz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhwf;->l:Lhuz;

    invoke-virtual {v0, v3}, Lhuz;->a(Lhjy;)V

    :cond_0
    iput-object v3, p0, Lhwf;->c:Ljava/lang/String;

    iput v2, p0, Lhwf;->e:I

    iget-object v0, p0, Lhwf;->d:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhwf;->d:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {p0, v0}, Lhwf;->removeView(Landroid/view/View;)V

    :cond_1
    iput-boolean v2, p0, Lhwf;->g:Z

    iget-object v0, p0, Lhwf;->h:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhwf;->h:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lhwf;->removeView(Landroid/view/View;)V

    :cond_2
    iput-object v3, p0, Lhwf;->m:Ljava/lang/CharSequence;

    iput v2, p0, Lhwf;->k:I

    iput-boolean v2, p0, Lhwf;->i:Z

    iget-object v0, p0, Lhwf;->j:Landroid/widget/TextView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lhwf;->j:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lhwf;->removeView(Landroid/view/View;)V

    .line 136
    :cond_3
    if-nez p1, :cond_4

    .line 148
    :goto_0
    return-void

    .line 140
    :cond_4
    iput-boolean v1, p0, Lhwf;->b:Z

    .line 142
    invoke-virtual {p1}, Lhuf;->a()Lhug;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-virtual {v0}, Lhug;->a()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lhwf;->c:Ljava/lang/String;

    invoke-virtual {v0}, Lhug;->b()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lhwf;->f:Ljava/lang/String;

    iget-object v3, p0, Lhwf;->c:Ljava/lang/String;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lhwf;->d:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    if-nez v3, :cond_5

    new-instance v3, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {p0}, Lhwf;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lhwf;->d:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    :cond_5
    iget-object v3, p0, Lhwf;->d:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v3, v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(I)V

    iget-object v3, p0, Lhwf;->d:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v3, v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->b(I)V

    iget-object v3, p0, Lhwf;->d:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    iget-object v4, p0, Lhwf;->f:Ljava/lang/String;

    iget-object v5, p0, Lhwf;->c:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lhwf;->d:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {p0, v3}, Lhwf;->addView(Landroid/view/View;)V

    invoke-virtual {v0}, Lhug;->c()Lhjy;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v3, p0, Lhwf;->l:Lhuz;

    if-nez v3, :cond_6

    new-instance v3, Lhuz;

    invoke-direct {v3, p0}, Lhuz;-><init>(Landroid/view/View;)V

    iput-object v3, p0, Lhwf;->l:Lhuz;

    :cond_6
    iget-object v3, p0, Lhwf;->l:Lhuz;

    invoke-virtual {v3, v0}, Lhuz;->a(Lhjy;)V

    .line 143
    :cond_7
    invoke-virtual {p1}, Lhuf;->b()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_f

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lhwf;->g:Z

    iget-boolean v0, p0, Lhwf;->g:Z

    if-eqz v0, :cond_9

    iget-object v0, p0, Lhwf;->h:Landroid/widget/TextView;

    if-nez v0, :cond_8

    new-instance v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lhwf;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v0, v4}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lhwf;->h:Landroid/widget/TextView;

    iget-object v0, p0, Lhwf;->h:Landroid/widget/TextView;

    iget-object v4, p0, Lhwf;->a:Lhve;

    iget v4, v4, Lhve;->b:I

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setMaxLines(I)V

    iget-object v0, p0, Lhwf;->h:Landroid/widget/TextView;

    sget-object v4, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    invoke-virtual {p0}, Lhwf;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v4, p0, Lhwf;->h:Landroid/widget/TextView;

    const/16 v5, 0x10

    invoke-static {v0, v4, v5}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    :cond_8
    iget-object v0, p0, Lhwf;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lhwf;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lhwf;->h:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lhwf;->addView(Landroid/view/View;)V

    .line 145
    :cond_9
    invoke-virtual {p1}, Lhuf;->c()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1}, Lhuf;->d()Ljava/lang/CharSequence;

    move-result-object v3

    new-instance v4, Landroid/text/SpannableStringBuilder;

    invoke-direct {v4}, Landroid/text/SpannableStringBuilder;-><init>()V

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_a

    invoke-virtual {v4, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_a
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_c

    invoke-virtual {v4}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_b

    invoke-virtual {p0}, Lhwf;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v5, 0x7f0a046a

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_b
    invoke-virtual {v4, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_c
    invoke-virtual {v4}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_e

    invoke-virtual {v4}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    invoke-virtual {v4, v2, v0}, Landroid/text/SpannableStringBuilder;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_10

    :goto_2
    iput-boolean v1, p0, Lhwf;->i:Z

    iget-boolean v1, p0, Lhwf;->i:Z

    if-eqz v1, :cond_e

    iget-object v1, p0, Lhwf;->j:Landroid/widget/TextView;

    if-nez v1, :cond_d

    new-instance v1, Landroid/widget/TextView;

    invoke-virtual {p0}, Lhwf;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lhwf;->j:Landroid/widget/TextView;

    invoke-virtual {p0}, Lhwf;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lhwf;->j:Landroid/widget/TextView;

    const/16 v3, 0xa

    invoke-static {v1, v2, v3}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    :cond_d
    iget-object v1, p0, Lhwf;->j:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lhwf;->j:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lhwf;->j:Landroid/widget/TextView;

    iget-object v1, p0, Lhwf;->a:Lhve;

    iget v1, v1, Lhve;->n:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    iget-object v0, p0, Lhwf;->j:Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    iget-object v0, p0, Lhwf;->j:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lhwf;->addView(Landroid/view/View;)V

    :cond_e
    invoke-virtual {v4}, Landroid/text/SpannableStringBuilder;->clear()V

    .line 147
    invoke-virtual {p1}, Lhuf;->e()Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lhwf;->m:Ljava/lang/CharSequence;

    goto/16 :goto_0

    :cond_f
    move v0, v2

    .line 143
    goto/16 :goto_1

    :cond_10
    move v1, v2

    .line 145
    goto :goto_2
.end method

.method public getContentDescription()Ljava/lang/CharSequence;
    .locals 1
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "accessibility"
    .end annotation

    .prologue
    .line 385
    iget-object v0, p0, Lhwf;->m:Ljava/lang/CharSequence;

    return-object v0
.end method

.method protected onLayout(ZIIII)V
    .locals 6

    .prologue
    .line 344
    iget-object v0, p0, Lhwf;->a:Lhve;

    iget v0, v0, Lhve;->j:I

    .line 347
    iget-object v1, p0, Lhwf;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lhwf;->d:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    if-eqz v1, :cond_0

    .line 348
    iget-object v1, p0, Lhwf;->d:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a()I

    move-result v1

    .line 349
    iget-object v2, p0, Lhwf;->d:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    iget v3, p0, Lhwf;->e:I

    add-int v4, v0, v1

    iget v5, p0, Lhwf;->e:I

    add-int/2addr v5, v1

    invoke-virtual {v2, v0, v3, v4, v5}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->layout(IIII)V

    .line 351
    add-int/2addr v1, v0

    add-int/2addr v0, v1

    .line 354
    :cond_0
    iget v1, p0, Lhwf;->k:I

    .line 356
    iget-boolean v2, p0, Lhwf;->g:Z

    if-eqz v2, :cond_1

    .line 357
    iget-object v2, p0, Lhwf;->h:Landroid/widget/TextView;

    iget-object v3, p0, Lhwf;->h:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, v0

    iget-object v4, p0, Lhwf;->h:Landroid/widget/TextView;

    .line 358
    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v1

    .line 357
    invoke-virtual {v2, v0, v1, v3, v4}, Landroid/widget/TextView;->layout(IIII)V

    .line 360
    iget-object v2, p0, Lhwf;->h:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v1, v2

    .line 363
    :cond_1
    iget-boolean v2, p0, Lhwf;->i:Z

    if-eqz v2, :cond_2

    .line 364
    iget-object v2, p0, Lhwf;->j:Landroid/widget/TextView;

    iget-object v3, p0, Lhwf;->j:Landroid/widget/TextView;

    .line 365
    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, v0

    iget-object v4, p0, Lhwf;->j:Landroid/widget/TextView;

    .line 366
    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v1

    .line 364
    invoke-virtual {v2, v0, v1, v3, v4}, Landroid/widget/TextView;->layout(IIII)V

    .line 368
    :cond_2
    return-void
.end method

.method protected onMeasure(II)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 279
    iget-boolean v0, p0, Lhwf;->b:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x40

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " expected to have been bound with valid data. Was bind() called?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 281
    :cond_0
    invoke-virtual {p0}, Lhwf;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lhvj;->a(Landroid/content/Context;I)I

    move-result v3

    .line 282
    iget-object v0, p0, Lhwf;->a:Lhve;

    iget v4, v0, Lhve;->j:I

    .line 285
    sub-int v2, v3, v4

    .line 287
    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 290
    iput v4, p0, Lhwf;->k:I

    iput v4, p0, Lhwf;->e:I

    .line 294
    iget-object v0, p0, Lhwf;->c:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 295
    iget-object v0, p0, Lhwf;->d:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0, v5, v5}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->measure(II)V

    .line 296
    iget-object v0, p0, Lhwf;->d:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->getMeasuredWidth()I

    move-result v0

    .line 297
    sub-int/2addr v2, v0

    .line 300
    :goto_0
    const/high16 v6, 0x40000000    # 2.0f

    invoke-static {v2, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    .line 304
    iget-boolean v2, p0, Lhwf;->g:Z

    if-eqz v2, :cond_6

    .line 305
    iget-object v2, p0, Lhwf;->h:Landroid/widget/TextView;

    invoke-virtual {v2, v6, v5}, Landroid/widget/TextView;->measure(II)V

    .line 306
    iget-object v2, p0, Lhwf;->h:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v2

    add-int/lit8 v2, v2, 0x0

    .line 309
    :goto_1
    iget-boolean v7, p0, Lhwf;->i:Z

    if-eqz v7, :cond_1

    .line 310
    iget-object v7, p0, Lhwf;->j:Landroid/widget/TextView;

    invoke-virtual {v7, v6, v5}, Landroid/widget/TextView;->measure(II)V

    .line 311
    iget-object v5, p0, Lhwf;->j:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v2, v5

    .line 316
    :cond_1
    iget-boolean v5, p0, Lhwf;->g:Z

    if-eqz v5, :cond_2

    iget-boolean v5, p0, Lhwf;->i:Z

    if-eqz v5, :cond_2

    .line 317
    iget-object v5, p0, Lhwf;->a:Lhve;

    iget v5, v5, Lhve;->h:I

    add-int/2addr v2, v5

    .line 322
    :cond_2
    if-le v0, v2, :cond_5

    .line 323
    iget v5, p0, Lhwf;->k:I

    sub-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v5

    iput v0, p0, Lhwf;->k:I

    .line 328
    :goto_2
    iget-object v0, p0, Lhwf;->c:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lhwf;->d:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->getMeasuredHeight()I

    move-result v1

    .line 329
    :cond_3
    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 331
    if-lez v0, :cond_4

    .line 332
    mul-int/lit8 v1, v4, 0x2

    add-int/2addr v0, v1

    .line 335
    :cond_4
    invoke-virtual {p0, v3, v0}, Lhwf;->setMeasuredDimension(II)V

    .line 336
    return-void

    .line 325
    :cond_5
    iget v5, p0, Lhwf;->e:I

    sub-int v0, v2, v0

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v5

    iput v0, p0, Lhwf;->e:I

    goto :goto_2

    :cond_6
    move v2, v1

    goto :goto_1

    :cond_7
    move v0, v1

    goto :goto_0
.end method
