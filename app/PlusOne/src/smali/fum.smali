.class public final Lfum;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lfun;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lfum;->a:Landroid/util/SparseArray;

    return-void
.end method

.method private static a(Landroid/content/Context;I)Lfun;
    .locals 8

    .prologue
    const/16 v7, 0x19

    const/16 v6, 0xf

    const v5, 0x7f0200b9

    const v4, 0x7f0200b8

    const/16 v3, 0x1f

    .line 150
    sget-object v0, Lfum;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfun;

    .line 152
    if-nez v0, :cond_0

    .line 153
    new-instance v1, Lfun;

    invoke-direct {v1}, Lfun;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    packed-switch p1, :pswitch_data_0

    .line 154
    :goto_0
    sget-object v0, Lfum;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    move-object v0, v1

    .line 157
    :cond_0
    return-object v0

    .line 153
    :pswitch_0
    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/NinePatchDrawable;

    iput-object v0, v1, Lfun;->a:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/NinePatchDrawable;

    iput-object v0, v1, Lfun;->b:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-static {p0, v7}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v0

    iput-object v0, v1, Lfun;->c:Landroid/text/TextPaint;

    goto :goto_0

    :pswitch_1
    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/NinePatchDrawable;

    iput-object v0, v1, Lfun;->a:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/NinePatchDrawable;

    iput-object v0, v1, Lfun;->b:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-static {p0, v6}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v0

    iput-object v0, v1, Lfun;->c:Landroid/text/TextPaint;

    goto :goto_0

    :pswitch_2
    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/NinePatchDrawable;

    iput-object v0, v1, Lfun;->a:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/NinePatchDrawable;

    iput-object v0, v1, Lfun;->b:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-static {p0, v6}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v0

    iput-object v0, v1, Lfun;->c:Landroid/text/TextPaint;

    goto :goto_0

    :pswitch_3
    const v0, 0x7f0200a9

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/NinePatchDrawable;

    iput-object v0, v1, Lfun;->a:Landroid/graphics/drawable/NinePatchDrawable;

    const v0, 0x7f0200aa

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/NinePatchDrawable;

    iput-object v0, v1, Lfun;->b:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-static {p0, v7}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v0

    iput-object v0, v1, Lfun;->c:Landroid/text/TextPaint;

    goto :goto_0

    :pswitch_4
    const v0, 0x7f0200a6

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/NinePatchDrawable;

    iput-object v0, v1, Lfun;->a:Landroid/graphics/drawable/NinePatchDrawable;

    const v0, 0x7f0200a7

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/NinePatchDrawable;

    iput-object v0, v1, Lfun;->b:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-static {p0, v3}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v0

    iput-object v0, v1, Lfun;->c:Landroid/text/TextPaint;

    goto/16 :goto_0

    :pswitch_5
    const/4 v0, 0x0

    iput-object v0, v1, Lfun;->a:Landroid/graphics/drawable/NinePatchDrawable;

    const/4 v0, 0x0

    iput-object v0, v1, Lfun;->b:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-static {p0, v3}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v0

    iput-object v0, v1, Lfun;->c:Landroid/text/TextPaint;

    goto/16 :goto_0

    :pswitch_6
    const v0, 0x7f020341

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/NinePatchDrawable;

    iput-object v0, v1, Lfun;->a:Landroid/graphics/drawable/NinePatchDrawable;

    const v0, 0x7f020341

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/NinePatchDrawable;

    iput-object v0, v1, Lfun;->b:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-static {p0, v3}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v0

    iput-object v0, v1, Lfun;->c:Landroid/text/TextPaint;

    goto/16 :goto_0

    :pswitch_7
    const v0, 0x7f0203ef

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/NinePatchDrawable;

    iput-object v0, v1, Lfun;->a:Landroid/graphics/drawable/NinePatchDrawable;

    const v0, 0x7f0203ef

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/NinePatchDrawable;

    iput-object v0, v1, Lfun;->b:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-static {p0, v3}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v0

    iput-object v0, v1, Lfun;->c:Landroid/text/TextPaint;

    goto/16 :goto_0

    :pswitch_8
    const v0, 0x7f02033e

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/NinePatchDrawable;

    iput-object v0, v1, Lfun;->a:Landroid/graphics/drawable/NinePatchDrawable;

    const v0, 0x7f02033e

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/NinePatchDrawable;

    iput-object v0, v1, Lfun;->b:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-static {p0, v3}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v0

    iput-object v0, v1, Lfun;->c:Landroid/text/TextPaint;

    goto/16 :goto_0

    :pswitch_9
    const v0, 0x7f02033f

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/NinePatchDrawable;

    iput-object v0, v1, Lfun;->a:Landroid/graphics/drawable/NinePatchDrawable;

    const v0, 0x7f02033f

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/NinePatchDrawable;

    iput-object v0, v1, Lfun;->b:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-static {p0, v3}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v0

    iput-object v0, v1, Lfun;->c:Landroid/text/TextPaint;

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public static a(Landroid/content/Context;Landroid/graphics/Bitmap;IIILfwv;Ljava/lang/CharSequence;Z)Lfwu;
    .locals 10

    .prologue
    .line 254
    invoke-static {p0, p4}, Lfum;->a(Landroid/content/Context;I)Lfun;

    move-result-object v1

    .line 256
    new-instance v0, Lfwu;

    iget-object v3, v1, Lfun;->a:Landroid/graphics/drawable/NinePatchDrawable;

    iget-object v4, v1, Lfun;->b:Landroid/graphics/drawable/NinePatchDrawable;

    move-object v1, p0

    move-object v2, p1

    move-object v5, p5

    move v6, p2

    move v7, p3

    move-object/from16 v8, p6

    move/from16 v9, p7

    invoke-direct/range {v0 .. v9}, Lfwu;-><init>(Landroid/content/Context;Landroid/graphics/Bitmap;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lfwv;IILjava/lang/CharSequence;Z)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/CharSequence;IIILfwv;)Lfwu;
    .locals 15

    .prologue
    .line 248
    move/from16 v0, p2

    invoke-static {p0, v0}, Lfum;->a(Landroid/content/Context;I)Lfun;

    move-result-object v2

    new-instance v1, Lfwu;

    const/4 v3, 0x0

    iget-object v5, v2, Lfun;->c:Landroid/text/TextPaint;

    iget-object v6, v2, Lfun;->a:Landroid/graphics/drawable/NinePatchDrawable;

    iget-object v7, v2, Lfun;->b:Landroid/graphics/drawable/NinePatchDrawable;

    const/4 v12, 0x1

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object v2, p0

    move-object/from16 v4, p1

    move-object/from16 v8, p5

    move/from16 v9, p3

    move/from16 v10, p4

    move-object/from16 v11, p1

    invoke-direct/range {v1 .. v14}, Lfwu;-><init>(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lfwv;IILjava/lang/CharSequence;ZII)V

    return-object v1
.end method
