.class public final Lqac;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Ljava/lang/String;

.field private b:[Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1198
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1201
    sget-object v0, Loxx;->h:[Ljava/lang/Long;

    iput-object v0, p0, Lqac;->b:[Ljava/lang/Long;

    .line 1204
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lqac;->a:[Ljava/lang/String;

    .line 1198
    return-void
.end method


# virtual methods
.method public a()I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1225
    .line 1226
    iget-object v0, p0, Lqac;->b:[Ljava/lang/Long;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lqac;->b:[Ljava/lang/Long;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 1228
    iget-object v3, p0, Lqac;->b:[Ljava/lang/Long;

    array-length v4, v3

    move v0, v1

    move v2, v1

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, v3, v0

    .line 1230
    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Loxo;->f(J)I

    move-result v5

    add-int/2addr v2, v5

    .line 1228
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1233
    :cond_0
    iget-object v0, p0, Lqac;->b:[Ljava/lang/Long;

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v2

    .line 1235
    :goto_1
    iget-object v2, p0, Lqac;->a:[Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lqac;->a:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 1237
    iget-object v3, p0, Lqac;->a:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v1, v4, :cond_1

    aget-object v5, v3, v1

    .line 1239
    invoke-static {v5}, Loxo;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 1237
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1241
    :cond_1
    add-int/2addr v0, v2

    .line 1242
    iget-object v1, p0, Lqac;->a:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1244
    :cond_2
    iget-object v1, p0, Lqac;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1245
    iput v0, p0, Lqac;->ai:I

    .line 1246
    return v0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public a(Loxn;)Lqac;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1254
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1255
    sparse-switch v0, :sswitch_data_0

    .line 1259
    iget-object v1, p0, Lqac;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1260
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lqac;->ah:Ljava/util/List;

    .line 1263
    :cond_1
    iget-object v1, p0, Lqac;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1265
    :sswitch_0
    return-object p0

    .line 1270
    :sswitch_1
    const/16 v0, 0x8

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 1271
    iget-object v0, p0, Lqac;->b:[Ljava/lang/Long;

    array-length v0, v0

    .line 1272
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/Long;

    .line 1273
    iget-object v2, p0, Lqac;->b:[Ljava/lang/Long;

    invoke-static {v2, v4, v1, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1274
    iput-object v1, p0, Lqac;->b:[Ljava/lang/Long;

    .line 1275
    :goto_1
    iget-object v1, p0, Lqac;->b:[Ljava/lang/Long;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 1276
    iget-object v1, p0, Lqac;->b:[Ljava/lang/Long;

    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v0

    .line 1277
    invoke-virtual {p1}, Loxn;->a()I

    .line 1275
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1280
    :cond_2
    iget-object v1, p0, Lqac;->b:[Ljava/lang/Long;

    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    .line 1284
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 1285
    iget-object v0, p0, Lqac;->a:[Ljava/lang/String;

    array-length v0, v0

    .line 1286
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    .line 1287
    iget-object v2, p0, Lqac;->a:[Ljava/lang/String;

    invoke-static {v2, v4, v1, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1288
    iput-object v1, p0, Lqac;->a:[Ljava/lang/String;

    .line 1289
    :goto_2
    iget-object v1, p0, Lqac;->a:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_3

    .line 1290
    iget-object v1, p0, Lqac;->a:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 1291
    invoke-virtual {p1}, Loxn;->a()I

    .line 1289
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1294
    :cond_3
    iget-object v1, p0, Lqac;->a:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto/16 :goto_0

    .line 1255
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 1209
    iget-object v1, p0, Lqac;->b:[Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 1210
    iget-object v2, p0, Lqac;->b:[Ljava/lang/Long;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 1211
    const/4 v5, 0x1

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {p1, v5, v6, v7}, Loxo;->b(IJ)V

    .line 1210
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1214
    :cond_0
    iget-object v1, p0, Lqac;->a:[Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1215
    iget-object v1, p0, Lqac;->a:[Ljava/lang/String;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 1216
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->a(ILjava/lang/String;)V

    .line 1215
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1219
    :cond_1
    iget-object v0, p0, Lqac;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1221
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1194
    invoke-virtual {p0, p1}, Lqac;->a(Loxn;)Lqac;

    move-result-object v0

    return-object v0
.end method
