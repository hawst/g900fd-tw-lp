.class public final Loxi;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Loxi;


# instance fields
.field private b:Ljava/lang/Integer;

.field private c:Ljava/lang/Float;

.field private d:Ljava/lang/Integer;

.field private e:Ljava/lang/Float;

.field private f:Ljava/lang/Float;

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 389
    const/4 v0, 0x0

    new-array v0, v0, [Loxi;

    sput-object v0, Loxi;->a:[Loxi;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 390
    invoke-direct {p0}, Loxq;-><init>()V

    .line 428
    const/high16 v0, -0x80000000

    iput v0, p0, Loxi;->g:I

    .line 390
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 457
    const/4 v0, 0x0

    .line 458
    iget-object v1, p0, Loxi;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 459
    const/4 v0, 0x1

    iget-object v1, p0, Loxi;->b:Ljava/lang/Integer;

    .line 460
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 462
    :cond_0
    iget-object v1, p0, Loxi;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 463
    const/4 v1, 0x2

    iget-object v2, p0, Loxi;->d:Ljava/lang/Integer;

    .line 464
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 466
    :cond_1
    iget-object v1, p0, Loxi;->f:Ljava/lang/Float;

    if-eqz v1, :cond_2

    .line 467
    const/4 v1, 0x3

    iget-object v2, p0, Loxi;->f:Ljava/lang/Float;

    .line 468
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 470
    :cond_2
    iget v1, p0, Loxi;->g:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_3

    .line 471
    const/4 v1, 0x4

    iget v2, p0, Loxi;->g:I

    .line 472
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 474
    :cond_3
    iget-object v1, p0, Loxi;->c:Ljava/lang/Float;

    if-eqz v1, :cond_4

    .line 475
    const/4 v1, 0x5

    iget-object v2, p0, Loxi;->c:Ljava/lang/Float;

    .line 476
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 478
    :cond_4
    iget-object v1, p0, Loxi;->e:Ljava/lang/Float;

    if-eqz v1, :cond_5

    .line 479
    const/4 v1, 0x6

    iget-object v2, p0, Loxi;->e:Ljava/lang/Float;

    .line 480
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 482
    :cond_5
    iget-object v1, p0, Loxi;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 483
    iput v0, p0, Loxi;->ai:I

    .line 484
    return v0
.end method

.method public a(Loxn;)Loxi;
    .locals 3

    .prologue
    const/16 v2, 0x2b

    .line 492
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 493
    sparse-switch v0, :sswitch_data_0

    .line 497
    iget-object v1, p0, Loxi;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 498
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Loxi;->ah:Ljava/util/List;

    .line 501
    :cond_1
    iget-object v1, p0, Loxi;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 503
    :sswitch_0
    return-object p0

    .line 508
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Loxi;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 512
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Loxi;->d:Ljava/lang/Integer;

    goto :goto_0

    .line 516
    :sswitch_3
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Loxi;->f:Ljava/lang/Float;

    goto :goto_0

    .line 520
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 521
    if-eq v0, v2, :cond_2

    const/16 v1, 0x2c

    if-eq v0, v1, :cond_2

    const/16 v1, 0xc8

    if-eq v0, v1, :cond_2

    const/16 v1, 0xdc

    if-eq v0, v1, :cond_2

    const/16 v1, 0xdd

    if-eq v0, v1, :cond_2

    const/16 v1, 0xde

    if-eq v0, v1, :cond_2

    const/16 v1, 0xdf

    if-eq v0, v1, :cond_2

    const/16 v1, 0xe0

    if-eq v0, v1, :cond_2

    const/16 v1, 0xe1

    if-eq v0, v1, :cond_2

    const/16 v1, 0xe2

    if-eq v0, v1, :cond_2

    const/16 v1, 0xe3

    if-eq v0, v1, :cond_2

    const/16 v1, 0x12c

    if-eq v0, v1, :cond_2

    const/16 v1, 0x12e

    if-eq v0, v1, :cond_2

    const/16 v1, 0xf0

    if-eq v0, v1, :cond_2

    const/16 v1, 0xf1

    if-eq v0, v1, :cond_2

    const/16 v1, 0x130

    if-eq v0, v1, :cond_2

    const/16 v1, 0x131

    if-eq v0, v1, :cond_2

    const/16 v1, 0x138

    if-eq v0, v1, :cond_2

    const/16 v1, 0x13a

    if-eq v0, v1, :cond_2

    const/16 v1, 0x13b

    if-eq v0, v1, :cond_2

    const/16 v1, 0x13c

    if-eq v0, v1, :cond_2

    const/16 v1, 0x3a98

    if-ne v0, v1, :cond_3

    .line 543
    :cond_2
    iput v0, p0, Loxi;->g:I

    goto/16 :goto_0

    .line 545
    :cond_3
    iput v2, p0, Loxi;->g:I

    goto/16 :goto_0

    .line 550
    :sswitch_5
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Loxi;->c:Ljava/lang/Float;

    goto/16 :goto_0

    .line 554
    :sswitch_6
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Loxi;->e:Ljava/lang/Float;

    goto/16 :goto_0

    .line 493
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1d -> :sswitch_3
        0x20 -> :sswitch_4
        0x2d -> :sswitch_5
        0x35 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 433
    iget-object v0, p0, Loxi;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 434
    const/4 v0, 0x1

    iget-object v1, p0, Loxi;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 436
    :cond_0
    iget-object v0, p0, Loxi;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 437
    const/4 v0, 0x2

    iget-object v1, p0, Loxi;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 439
    :cond_1
    iget-object v0, p0, Loxi;->f:Ljava/lang/Float;

    if-eqz v0, :cond_2

    .line 440
    const/4 v0, 0x3

    iget-object v1, p0, Loxi;->f:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 442
    :cond_2
    iget v0, p0, Loxi;->g:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_3

    .line 443
    const/4 v0, 0x4

    iget v1, p0, Loxi;->g:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 445
    :cond_3
    iget-object v0, p0, Loxi;->c:Ljava/lang/Float;

    if-eqz v0, :cond_4

    .line 446
    const/4 v0, 0x5

    iget-object v1, p0, Loxi;->c:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 448
    :cond_4
    iget-object v0, p0, Loxi;->e:Ljava/lang/Float;

    if-eqz v0, :cond_5

    .line 449
    const/4 v0, 0x6

    iget-object v1, p0, Loxi;->e:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 451
    :cond_5
    iget-object v0, p0, Loxi;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 453
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 386
    invoke-virtual {p0, p1}, Loxi;->a(Loxn;)Loxi;

    move-result-object v0

    return-object v0
.end method
