.class public final Lmfu;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmyh;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2833
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2836
    iput-object v0, p0, Lmfu;->apiHeader:Llyq;

    .line 2839
    iput-object v0, p0, Lmfu;->a:Lmyh;

    .line 2833
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 2856
    const/4 v0, 0x0

    .line 2857
    iget-object v1, p0, Lmfu;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 2858
    const/4 v0, 0x1

    iget-object v1, p0, Lmfu;->apiHeader:Llyq;

    .line 2859
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2861
    :cond_0
    iget-object v1, p0, Lmfu;->a:Lmyh;

    if-eqz v1, :cond_1

    .line 2862
    const/4 v1, 0x2

    iget-object v2, p0, Lmfu;->a:Lmyh;

    .line 2863
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2865
    :cond_1
    iget-object v1, p0, Lmfu;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2866
    iput v0, p0, Lmfu;->ai:I

    .line 2867
    return v0
.end method

.method public a(Loxn;)Lmfu;
    .locals 2

    .prologue
    .line 2875
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2876
    sparse-switch v0, :sswitch_data_0

    .line 2880
    iget-object v1, p0, Lmfu;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 2881
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmfu;->ah:Ljava/util/List;

    .line 2884
    :cond_1
    iget-object v1, p0, Lmfu;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2886
    :sswitch_0
    return-object p0

    .line 2891
    :sswitch_1
    iget-object v0, p0, Lmfu;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 2892
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmfu;->apiHeader:Llyq;

    .line 2894
    :cond_2
    iget-object v0, p0, Lmfu;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2898
    :sswitch_2
    iget-object v0, p0, Lmfu;->a:Lmyh;

    if-nez v0, :cond_3

    .line 2899
    new-instance v0, Lmyh;

    invoke-direct {v0}, Lmyh;-><init>()V

    iput-object v0, p0, Lmfu;->a:Lmyh;

    .line 2901
    :cond_3
    iget-object v0, p0, Lmfu;->a:Lmyh;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2876
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 2844
    iget-object v0, p0, Lmfu;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 2845
    const/4 v0, 0x1

    iget-object v1, p0, Lmfu;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2847
    :cond_0
    iget-object v0, p0, Lmfu;->a:Lmyh;

    if-eqz v0, :cond_1

    .line 2848
    const/4 v0, 0x2

    iget-object v1, p0, Lmfu;->a:Lmyh;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2850
    :cond_1
    iget-object v0, p0, Lmfu;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2852
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2829
    invoke-virtual {p0, p1}, Lmfu;->a(Loxn;)Lmfu;

    move-result-object v0

    return-object v0
.end method
