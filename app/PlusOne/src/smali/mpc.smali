.class public final Lmpc;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmpc;


# instance fields
.field private b:Lltx;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/Integer;

.field private e:Ljava/lang/String;

.field private f:I

.field private g:Locf;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 459
    const/4 v0, 0x0

    new-array v0, v0, [Lmpc;

    sput-object v0, Lmpc;->a:[Lmpc;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 460
    invoke-direct {p0}, Loxq;-><init>()V

    .line 463
    iput-object v1, p0, Lmpc;->b:Lltx;

    .line 472
    const/high16 v0, -0x80000000

    iput v0, p0, Lmpc;->f:I

    .line 475
    iput-object v1, p0, Lmpc;->g:Locf;

    .line 460
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 504
    const/4 v0, 0x0

    .line 505
    iget-object v1, p0, Lmpc;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 506
    const/4 v0, 0x2

    iget-object v1, p0, Lmpc;->c:Ljava/lang/String;

    .line 507
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 509
    :cond_0
    iget-object v1, p0, Lmpc;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 510
    const/4 v1, 0x3

    iget-object v2, p0, Lmpc;->d:Ljava/lang/Integer;

    .line 511
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 513
    :cond_1
    iget-object v1, p0, Lmpc;->b:Lltx;

    if-eqz v1, :cond_2

    .line 514
    const/4 v1, 0x6

    iget-object v2, p0, Lmpc;->b:Lltx;

    .line 515
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 517
    :cond_2
    iget-object v1, p0, Lmpc;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 518
    const/4 v1, 0x7

    iget-object v2, p0, Lmpc;->e:Ljava/lang/String;

    .line 519
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 521
    :cond_3
    iget v1, p0, Lmpc;->f:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_4

    .line 522
    const/16 v1, 0x8

    iget v2, p0, Lmpc;->f:I

    .line 523
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 525
    :cond_4
    iget-object v1, p0, Lmpc;->g:Locf;

    if-eqz v1, :cond_5

    .line 526
    const/16 v1, 0x9

    iget-object v2, p0, Lmpc;->g:Locf;

    .line 527
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 529
    :cond_5
    iget-object v1, p0, Lmpc;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 530
    iput v0, p0, Lmpc;->ai:I

    .line 531
    return v0
.end method

.method public a(Loxn;)Lmpc;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 539
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 540
    sparse-switch v0, :sswitch_data_0

    .line 544
    iget-object v1, p0, Lmpc;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 545
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmpc;->ah:Ljava/util/List;

    .line 548
    :cond_1
    iget-object v1, p0, Lmpc;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 550
    :sswitch_0
    return-object p0

    .line 555
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmpc;->c:Ljava/lang/String;

    goto :goto_0

    .line 559
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmpc;->d:Ljava/lang/Integer;

    goto :goto_0

    .line 563
    :sswitch_3
    iget-object v0, p0, Lmpc;->b:Lltx;

    if-nez v0, :cond_2

    .line 564
    new-instance v0, Lltx;

    invoke-direct {v0}, Lltx;-><init>()V

    iput-object v0, p0, Lmpc;->b:Lltx;

    .line 566
    :cond_2
    iget-object v0, p0, Lmpc;->b:Lltx;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 570
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmpc;->e:Ljava/lang/String;

    goto :goto_0

    .line 574
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 575
    if-eq v0, v2, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-ne v0, v1, :cond_4

    .line 578
    :cond_3
    iput v0, p0, Lmpc;->f:I

    goto :goto_0

    .line 580
    :cond_4
    iput v2, p0, Lmpc;->f:I

    goto :goto_0

    .line 585
    :sswitch_6
    iget-object v0, p0, Lmpc;->g:Locf;

    if-nez v0, :cond_5

    .line 586
    new-instance v0, Locf;

    invoke-direct {v0}, Locf;-><init>()V

    iput-object v0, p0, Lmpc;->g:Locf;

    .line 588
    :cond_5
    iget-object v0, p0, Lmpc;->g:Locf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 540
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x18 -> :sswitch_2
        0x32 -> :sswitch_3
        0x3a -> :sswitch_4
        0x40 -> :sswitch_5
        0x4a -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 480
    iget-object v0, p0, Lmpc;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 481
    const/4 v0, 0x2

    iget-object v1, p0, Lmpc;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 483
    :cond_0
    iget-object v0, p0, Lmpc;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 484
    const/4 v0, 0x3

    iget-object v1, p0, Lmpc;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 486
    :cond_1
    iget-object v0, p0, Lmpc;->b:Lltx;

    if-eqz v0, :cond_2

    .line 487
    const/4 v0, 0x6

    iget-object v1, p0, Lmpc;->b:Lltx;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 489
    :cond_2
    iget-object v0, p0, Lmpc;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 490
    const/4 v0, 0x7

    iget-object v1, p0, Lmpc;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 492
    :cond_3
    iget v0, p0, Lmpc;->f:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_4

    .line 493
    const/16 v0, 0x8

    iget v1, p0, Lmpc;->f:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 495
    :cond_4
    iget-object v0, p0, Lmpc;->g:Locf;

    if-eqz v0, :cond_5

    .line 496
    const/16 v0, 0x9

    iget-object v1, p0, Lmpc;->g:Locf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 498
    :cond_5
    iget-object v0, p0, Lmpc;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 500
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 456
    invoke-virtual {p0, p1}, Lmpc;->a(Loxn;)Lmpc;

    move-result-object v0

    return-object v0
.end method
