.class final Lfki;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lfkg;

.field private synthetic b:Ljava/util/List;

.field private synthetic c:Ljava/lang/String;


# direct methods
.method constructor <init>(Lfkg;Ljava/util/List;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1007
    iput-object p1, p0, Lfki;->a:Lfkg;

    iput-object p2, p0, Lfki;->b:Ljava/util/List;

    iput-object p3, p0, Lfki;->c:Ljava/lang/String;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs a()Ljava/lang/Boolean;
    .locals 5

    .prologue
    .line 1022
    iget-object v0, p0, Lfki;->a:Lfkg;

    iget-object v0, v0, Lfkg;->a:Lfjx;

    invoke-static {v0}, Lfjx;->t(Lfjx;)Llnl;

    move-result-object v0

    invoke-static {v0}, Lhqd;->d(Landroid/content/Context;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 1025
    iget-object v0, p0, Lfki;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1026
    iget-object v3, p0, Lfki;->a:Lfkg;

    iget-object v3, v3, Lfkg;->a:Lfjx;

    invoke-static {v3}, Lfjx;->u(Lfjx;)Llnl;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v3, v0, v4}, Lesd;->a(Landroid/content/Context;IZ)V

    .line 1027
    iget-object v3, p0, Lfki;->a:Lfkg;

    iget-object v3, v3, Lfkg;->a:Lfjx;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v4, Lhmv;->l:Lhmv;

    invoke-static {v3, v0, v4}, Lfjx;->a(Lfjx;Ljava/lang/Integer;Lhmv;)V

    goto :goto_0

    .line 1030
    :cond_0
    iget-object v0, p0, Lfki;->c:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1031
    iget-object v2, p0, Lfki;->a:Lfkg;

    iget-object v2, v2, Lfkg;->a:Lfjx;

    invoke-static {v2}, Lfjx;->v(Lfjx;)Llnl;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lesd;->a(Landroid/content/Context;I)V

    .line 1032
    iget-object v2, p0, Lfki;->a:Lfkg;

    iget-object v2, v2, Lfkg;->a:Lfjx;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    sget-object v4, Lhmv;->k:Lhmv;

    invoke-static {v2, v3, v4}, Lfjx;->a(Lfjx;Ljava/lang/Integer;Lhmv;)V

    .line 1033
    iget-object v2, p0, Lfki;->a:Lfkg;

    iget-object v2, v2, Lfkg;->a:Lfjx;

    invoke-static {v2}, Lfjx;->w(Lfjx;)Lhov;

    move-result-object v2

    new-instance v3, Lfkj;

    invoke-direct {v3, p0, v0}, Lfkj;-><init>(Lfki;I)V

    invoke-virtual {v2, v3}, Lhov;->a(Ljava/lang/Runnable;)Lhox;

    .line 1042
    return-object v1
.end method

.method protected a(Ljava/lang/Boolean;)V
    .locals 3

    .prologue
    .line 1010
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfki;->a:Lfkg;

    iget-object v0, v0, Lfkg;->a:Lfjx;

    invoke-static {v0}, Lfjx;->r(Lfjx;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1011
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lfki;->a:Lfkg;

    iget-object v1, v1, Lfkg;->a:Lfjx;

    invoke-static {v1}, Lfjx;->s(Lfjx;)Llnl;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0a0af5

    .line 1012
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a0af6

    .line 1013
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a0596

    const/4 v2, 0x0

    .line 1014
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 1015
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 1016
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 1018
    :cond_0
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1007
    invoke-virtual {p0}, Lfki;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1007
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lfki;->a(Ljava/lang/Boolean;)V

    return-void
.end method
