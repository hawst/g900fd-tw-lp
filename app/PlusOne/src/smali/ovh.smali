.class public final Lovh;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lovg;

.field public b:Love;

.field private c:Lovc;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 13
    iput-object v0, p0, Lovh;->a:Lovg;

    .line 16
    iput-object v0, p0, Lovh;->b:Love;

    .line 19
    iput-object v0, p0, Lovh;->c:Lovc;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 39
    const/4 v0, 0x0

    .line 40
    iget-object v1, p0, Lovh;->a:Lovg;

    if-eqz v1, :cond_0

    .line 41
    const/4 v0, 0x1

    iget-object v1, p0, Lovh;->a:Lovg;

    .line 42
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 44
    :cond_0
    iget-object v1, p0, Lovh;->b:Love;

    if-eqz v1, :cond_1

    .line 45
    const/4 v1, 0x2

    iget-object v2, p0, Lovh;->b:Love;

    .line 46
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 48
    :cond_1
    iget-object v1, p0, Lovh;->c:Lovc;

    if-eqz v1, :cond_2

    .line 49
    const/4 v1, 0x3

    iget-object v2, p0, Lovh;->c:Lovc;

    .line 50
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 52
    :cond_2
    iget-object v1, p0, Lovh;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 53
    iput v0, p0, Lovh;->ai:I

    .line 54
    return v0
.end method

.method public a(Loxn;)Lovh;
    .locals 2

    .prologue
    .line 62
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 63
    sparse-switch v0, :sswitch_data_0

    .line 67
    iget-object v1, p0, Lovh;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 68
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lovh;->ah:Ljava/util/List;

    .line 71
    :cond_1
    iget-object v1, p0, Lovh;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 73
    :sswitch_0
    return-object p0

    .line 78
    :sswitch_1
    iget-object v0, p0, Lovh;->a:Lovg;

    if-nez v0, :cond_2

    .line 79
    new-instance v0, Lovg;

    invoke-direct {v0}, Lovg;-><init>()V

    iput-object v0, p0, Lovh;->a:Lovg;

    .line 81
    :cond_2
    iget-object v0, p0, Lovh;->a:Lovg;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 85
    :sswitch_2
    iget-object v0, p0, Lovh;->b:Love;

    if-nez v0, :cond_3

    .line 86
    new-instance v0, Love;

    invoke-direct {v0}, Love;-><init>()V

    iput-object v0, p0, Lovh;->b:Love;

    .line 88
    :cond_3
    iget-object v0, p0, Lovh;->b:Love;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 92
    :sswitch_3
    iget-object v0, p0, Lovh;->c:Lovc;

    if-nez v0, :cond_4

    .line 93
    new-instance v0, Lovc;

    invoke-direct {v0}, Lovc;-><init>()V

    iput-object v0, p0, Lovh;->c:Lovc;

    .line 95
    :cond_4
    iget-object v0, p0, Lovh;->c:Lovc;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 63
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 24
    iget-object v0, p0, Lovh;->a:Lovg;

    if-eqz v0, :cond_0

    .line 25
    const/4 v0, 0x1

    iget-object v1, p0, Lovh;->a:Lovg;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 27
    :cond_0
    iget-object v0, p0, Lovh;->b:Love;

    if-eqz v0, :cond_1

    .line 28
    const/4 v0, 0x2

    iget-object v1, p0, Lovh;->b:Love;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 30
    :cond_1
    iget-object v0, p0, Lovh;->c:Lovc;

    if-eqz v0, :cond_2

    .line 31
    const/4 v0, 0x3

    iget-object v1, p0, Lovh;->c:Lovc;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 33
    :cond_2
    iget-object v0, p0, Lovh;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 35
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lovh;->a(Loxn;)Lovh;

    move-result-object v0

    return-object v0
.end method
