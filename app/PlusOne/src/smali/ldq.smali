.class public Lldq;
.super Lhvs;
.source "PG"

# interfaces
.implements Lhmm;
.implements Lkdd;
.implements Lljh;


# static fields
.field public static z:Llct;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lhmk;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lldq;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0, p1, p2, p3}, Lhvs;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 34
    return-void
.end method


# virtual methods
.method public C()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lldq;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a(Landroid/database/Cursor;Llcr;II)Lldq;
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 51
    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lldq;->r:Ljava/lang/String;

    .line 52
    new-instance v0, Lkre;

    sget-object v3, Lonm;->a:Lhmn;

    iget-object v4, p0, Lldq;->r:Ljava/lang/String;

    invoke-direct {v0, v3, v4}, Lkre;-><init>(Lhmn;Ljava/lang/String;)V

    iput-object v0, p0, Lldq;->b:Lhmk;

    .line 53
    const/16 v0, 0x24

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lldq;->a:Ljava/lang/String;

    .line 54
    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    .line 55
    invoke-virtual {p0}, Lldq;->B()V

    .line 56
    iput p4, p0, Lldq;->w:I

    .line 60
    invoke-virtual {p0}, Lldq;->u()V

    .line 61
    invoke-virtual {p0}, Lldq;->o()Landroid/graphics/Rect;

    move-result-object v0

    iput-object v0, p0, Lldq;->q:Landroid/graphics/Rect;

    .line 62
    iget-object v0, p0, Lldq;->q:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lldq;->q:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget-object v4, p0, Lldq;->q:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    iget-object v5, p0, Lldq;->q:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p0, v0, v3, v4, v5}, Lldq;->setPadding(IIII)V

    .line 64
    iget v0, p0, Lldq;->w:I

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0}, Lldq;->setFocusable(Z)V

    .line 65
    iget v0, p0, Lldq;->w:I

    if-nez v0, :cond_1

    :goto_1
    invoke-virtual {p0, v1}, Lldq;->setClickable(Z)V

    .line 67
    invoke-virtual {p0, p1, p2, p3}, Lldq;->a(Landroid/database/Cursor;Llcr;I)V

    .line 68
    return-object p0

    :cond_0
    move v0, v2

    .line 64
    goto :goto_0

    :cond_1
    move v1, v2

    .line 65
    goto :goto_1
.end method

.method public a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 89
    invoke-super {p0}, Lhvs;->a()V

    .line 90
    iput-object v0, p0, Lldq;->a:Ljava/lang/String;

    .line 91
    iput-object v0, p0, Lldq;->b:Lhmk;

    .line 93
    return-void
.end method

.method protected a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 102
    invoke-super {p0, p1}, Lhvs;->a(Landroid/content/Context;)V

    .line 103
    sget-object v0, Lldq;->z:Llct;

    if-nez v0, :cond_0

    .line 104
    invoke-static {p1}, Llct;->a(Landroid/content/Context;)Llct;

    move-result-object v0

    sput-object v0, Lldq;->z:Llct;

    .line 106
    :cond_0
    return-void
.end method

.method public a(Landroid/database/Cursor;Llcr;I)V
    .locals 0

    .prologue
    .line 74
    return-void
.end method

.method public ac_()Lhmk;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lldq;->b:Lhmk;

    return-object v0
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 80
    if-eqz p1, :cond_0

    .line 81
    new-instance v0, Lhmi;

    invoke-direct {v0, p1}, Lhmi;-><init>(Landroid/view/View$OnClickListener;)V

    iput-object v0, p0, Lldq;->x:Landroid/view/View$OnClickListener;

    .line 85
    :goto_0
    return-void

    .line 83
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lldq;->x:Landroid/view/View$OnClickListener;

    goto :goto_0
.end method
