.class public final Lohi;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6644
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 6665
    const/4 v0, 0x0

    .line 6666
    iget-object v1, p0, Lohi;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 6667
    const/4 v0, 0x1

    iget-object v1, p0, Lohi;->a:Ljava/lang/Integer;

    .line 6668
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 6670
    :cond_0
    iget-object v1, p0, Lohi;->b:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 6671
    const/4 v1, 0x2

    iget-object v2, p0, Lohi;->b:Ljava/lang/Long;

    .line 6672
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 6674
    :cond_1
    iget-object v1, p0, Lohi;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6675
    iput v0, p0, Lohi;->ai:I

    .line 6676
    return v0
.end method

.method public a(Loxn;)Lohi;
    .locals 2

    .prologue
    .line 6684
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 6685
    sparse-switch v0, :sswitch_data_0

    .line 6689
    iget-object v1, p0, Lohi;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 6690
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lohi;->ah:Ljava/util/List;

    .line 6693
    :cond_1
    iget-object v1, p0, Lohi;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6695
    :sswitch_0
    return-object p0

    .line 6700
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lohi;->a:Ljava/lang/Integer;

    goto :goto_0

    .line 6704
    :sswitch_2
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lohi;->b:Ljava/lang/Long;

    goto :goto_0

    .line 6685
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 6653
    iget-object v0, p0, Lohi;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 6654
    const/4 v0, 0x1

    iget-object v1, p0, Lohi;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 6656
    :cond_0
    iget-object v0, p0, Lohi;->b:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 6657
    const/4 v0, 0x2

    iget-object v1, p0, Lohi;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 6659
    :cond_1
    iget-object v0, p0, Lohi;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 6661
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6640
    invoke-virtual {p0, p1}, Lohi;->a(Loxn;)Lohi;

    move-result-object v0

    return-object v0
.end method
