.class public Ldeo;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljlw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljlw",
        "<",
        "Ldeo;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljlx;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljlx",
            "<",
            "Ldeo;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lddl;

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:I


# direct methods
.method public constructor <init>(Lhov;)V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-instance v0, Ljls;

    invoke-direct {v0, p0, p1}, Ljls;-><init>(Ljava/lang/Object;Lhov;)V

    iput-object v0, p0, Ldeo;->a:Ljlx;

    .line 55
    return-void
.end method


# virtual methods
.method public a()Lddl;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Ldeo;->b:Lddl;

    return-object v0
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 109
    iput p1, p0, Ldeo;->f:I

    .line 110
    invoke-virtual {p0}, Ldeo;->b()Ljlx;

    move-result-object v0

    invoke-interface {v0}, Ljlx;->a()V

    .line 111
    return-void
.end method

.method public a(Lddl;)V
    .locals 1

    .prologue
    .line 63
    iput-object p1, p0, Ldeo;->b:Lddl;

    .line 64
    invoke-virtual {p0}, Ldeo;->b()Ljlx;

    move-result-object v0

    invoke-interface {v0}, Ljlx;->a()V

    .line 65
    return-void
.end method

.method public a(Lnzi;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 81
    invoke-virtual {p0}, Ldeo;->a()Lddl;

    move-result-object v0

    invoke-interface {v0, p1}, Lddl;->b(Lnzi;)Lddl;

    move-result-object v0

    .line 82
    invoke-interface {v0, p2}, Lddl;->a(Landroid/content/Context;)V

    .line 83
    invoke-virtual {p0, v0}, Ldeo;->a(Lddl;)V

    .line 84
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 76
    iput-boolean p1, p0, Ldeo;->c:Z

    .line 77
    invoke-virtual {p0}, Ldeo;->b()Ljlx;

    move-result-object v0

    invoke-interface {v0}, Ljlx;->a()V

    .line 78
    return-void
.end method

.method public b()Ljlx;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljlx",
            "<",
            "Ldeo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59
    iget-object v0, p0, Ldeo;->a:Ljlx;

    return-object v0
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 91
    iput-boolean p1, p0, Ldeo;->d:Z

    .line 92
    invoke-virtual {p0}, Ldeo;->b()Ljlx;

    move-result-object v0

    invoke-interface {v0}, Ljlx;->a()V

    .line 93
    return-void
.end method

.method public c(Z)V
    .locals 1

    .prologue
    .line 100
    iput-boolean p1, p0, Ldeo;->e:Z

    .line 101
    invoke-virtual {p0}, Ldeo;->b()Ljlx;

    move-result-object v0

    invoke-interface {v0}, Ljlx;->a()V

    .line 102
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 72
    iget-boolean v0, p0, Ldeo;->c:Z

    return v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 87
    iget-boolean v0, p0, Ldeo;->d:Z

    return v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 96
    iget-boolean v0, p0, Ldeo;->e:Z

    return v0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 118
    iget v0, p0, Ldeo;->f:I

    return v0
.end method
