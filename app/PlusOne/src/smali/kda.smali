.class public abstract Lkda;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private mConsumer:Lkdd;

.field private mConsumerList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lkdb;",
            ">;"
        }
    .end annotation
.end field

.field public mDebugLogEnabled:Z

.field public mDeliveringResource:Z

.field public volatile mDownloading:Z

.field public volatile mHttpStatusCode:I

.field public final mId:Lkdc;

.field public final mManager:Lkdg;

.field public volatile mResource:Ljava/lang/Object;

.field public volatile mResourceType:I

.field public volatile mStatus:I

.field private mTag:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lkdg;Lkdc;)V
    .locals 2

    .prologue
    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 116
    iput-object p1, p0, Lkda;->mManager:Lkdg;

    .line 117
    iput-object p2, p0, Lkda;->mId:Lkdc;

    .line 118
    const-string v0, "EsResource"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    iput-boolean v0, p0, Lkda;->mDebugLogEnabled:Z

    .line 119
    return-void
.end method

.method private appendConsumer(Ljava/lang/StringBuilder;Lkdd;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 478
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 483
    instance-of v0, p2, Landroid/view/View;

    if-eqz v0, :cond_0

    .line 484
    const-string v0, " context: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    check-cast p2, Landroid/view/View;

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 486
    :cond_0
    if-eqz p3, :cond_1

    .line 487
    const-string v0, " tag: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 489
    :cond_1
    return-void
.end method

.method private checkForConcurrentModification()V
    .locals 2

    .prologue
    .line 316
    iget-boolean v0, p0, Lkda;->mDeliveringResource:Z

    if-eqz v0, :cond_0

    .line 317
    new-instance v0, Ljava/util/ConcurrentModificationException;

    const-string v1, "Registering/unregistering resource while delivering status change notification"

    invoke-direct {v0, v1}, Ljava/util/ConcurrentModificationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 320
    :cond_0
    return-void
.end method

.method private isRegistered(Lkdd;Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 300
    iget-object v0, p0, Lkda;->mConsumer:Lkdd;

    if-ne v0, p1, :cond_2

    iget-object v0, p0, Lkda;->mTag:Ljava/lang/Object;

    if-nez v0, :cond_0

    if-eqz p2, :cond_1

    :cond_0
    iget-object v0, p0, Lkda;->mTag:Ljava/lang/Object;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lkda;->mTag:Ljava/lang/Object;

    .line 301
    invoke-virtual {v0, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move v0, v1

    .line 312
    :goto_0
    return v0

    .line 303
    :cond_2
    iget-object v0, p0, Lkda;->mConsumerList:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    .line 304
    iget-object v0, p0, Lkda;->mConsumerList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v3, v2

    .line 305
    :goto_1
    if-ge v3, v4, :cond_4

    .line 306
    iget-object v0, p0, Lkda;->mConsumerList:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkdb;

    .line 307
    invoke-virtual {v0, p1, p2}, Lkdb;->a(Lkdd;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 308
    goto :goto_0

    .line 305
    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_4
    move v0, v2

    .line 312
    goto :goto_0
.end method

.method static statusToString(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 138
    packed-switch p0, :pswitch_data_0

    .line 149
    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 139
    :pswitch_0
    const-string v0, "canceled"

    goto :goto_0

    .line 140
    :pswitch_1
    const-string v0, "loading"

    goto :goto_0

    .line 141
    :pswitch_2
    const-string v0, "not found"

    goto :goto_0

    .line 142
    :pswitch_3
    const-string v0, "out of memory"

    goto :goto_0

    .line 143
    :pswitch_4
    const-string v0, "packed"

    goto :goto_0

    .line 144
    :pswitch_5
    const-string v0, "permanent error"

    goto :goto_0

    .line 145
    :pswitch_6
    const-string v0, "ready"

    goto :goto_0

    .line 146
    :pswitch_7
    const-string v0, "transient error"

    goto :goto_0

    .line 147
    :pswitch_8
    const-string v0, "undefined"

    goto :goto_0

    .line 138
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_8
        :pswitch_6
        :pswitch_1
        :pswitch_2
        :pswitch_7
        :pswitch_5
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public appendDescription(Ljava/lang/StringBuilder;)V
    .locals 0

    .prologue
    .line 475
    return-void
.end method

.method public abstract deliverData(Ljava/lang/Object;)V
.end method

.method public deliverDownloadError(I)V
    .locals 6

    .prologue
    .line 403
    iget v0, p0, Lkda;->mStatus:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    .line 404
    invoke-virtual {p0}, Lkda;->isDebugLogEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 405
    iget-object v0, p0, Lkda;->mId:Lkdc;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 406
    invoke-virtual {p0}, Lkda;->getStatusAsString()Ljava/lang/String;

    move-result-object v1

    .line 407
    invoke-static {p1}, Lkda;->statusToString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x60

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Request no longer needed, not delivering status change: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", current status: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", ignored new status: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 405
    invoke-virtual {p0, v0}, Lkda;->logDebug(Ljava/lang/String;)V

    .line 418
    :cond_0
    :goto_0
    return-void

    .line 412
    :cond_1
    invoke-virtual {p0}, Lkda;->isDebugLogEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 413
    iget-object v0, p0, Lkda;->mId:Lkdc;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 414
    invoke-static {p1}, Lkda;->statusToString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x2d

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Delivering error code to consumers: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " status: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 413
    invoke-virtual {p0, v0}, Lkda;->logDebug(Ljava/lang/String;)V

    .line 417
    :cond_2
    iget-object v0, p0, Lkda;->mManager:Lkdg;

    invoke-interface {v0, p0, p1}, Lkdg;->a(Lkda;I)V

    goto :goto_0
.end method

.method public deliverHttpError(ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 424
    const/16 v0, 0x194

    if-ne p1, v0, :cond_0

    .line 425
    iget-object v0, p0, Lkda;->mManager:Lkdg;

    const/4 v1, 0x3

    invoke-interface {v0, p0, v1}, Lkdg;->a(Lkda;I)V

    .line 429
    :goto_0
    return-void

    .line 427
    :cond_0
    iget-object v0, p0, Lkda;->mManager:Lkdg;

    const/4 v1, 0x5

    invoke-interface {v0, p0, v1, p1}, Lkdg;->a(Lkda;II)V

    goto :goto_0
.end method

.method public deliverResource(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 387
    iget-object v0, p0, Lkda;->mManager:Lkdg;

    const/4 v1, 0x1

    invoke-interface {v0, p0, v1, p1}, Lkdg;->a(Lkda;ILjava/lang/Object;)V

    .line 388
    return-void
.end method

.method public getCachedFile()Ljava/io/File;
    .locals 1

    .prologue
    .line 173
    const/4 v0, 0x0

    return-object v0
.end method

.method public getHttpStatusCode()I
    .locals 1

    .prologue
    .line 153
    iget v0, p0, Lkda;->mHttpStatusCode:I

    return v0
.end method

.method public getIdentifier()Lkdc;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lkda;->mId:Lkdc;

    return-object v0
.end method

.method public getRawFile()Ljava/io/File;
    .locals 1

    .prologue
    .line 165
    const/4 v0, 0x0

    return-object v0
.end method

.method public getResource()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lkda;->mResource:Ljava/lang/Object;

    return-object v0
.end method

.method public getResourceConsumerCount()I
    .locals 1

    .prologue
    .line 334
    iget-object v0, p0, Lkda;->mConsumerList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 335
    iget-object v0, p0, Lkda;->mConsumerList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 339
    :goto_0
    return v0

    .line 336
    :cond_0
    iget-object v0, p0, Lkda;->mConsumer:Lkdd;

    if-eqz v0, :cond_1

    .line 337
    const/4 v0, 0x1

    goto :goto_0

    .line 339
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getStatus()I
    .locals 1

    .prologue
    .line 126
    iget v0, p0, Lkda;->mStatus:I

    return v0
.end method

.method public getStatusAsString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 134
    iget v0, p0, Lkda;->mStatus:I

    invoke-static {v0}, Lkda;->statusToString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isCacheEnabled()Z
    .locals 1

    .prologue
    .line 180
    const/4 v0, 0x1

    return v0
.end method

.method public isDebugLogEnabled()Z
    .locals 1

    .prologue
    .line 191
    iget-boolean v0, p0, Lkda;->mDebugLogEnabled:Z

    return v0
.end method

.method public abstract load()V
.end method

.method public logDebug(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 198
    iget-boolean v0, p0, Lkda;->mDebugLogEnabled:Z

    .line 199
    return-void
.end method

.method public logError(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 211
    const-string v0, "EsResource"

    invoke-static {v0, p1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 212
    return-void
.end method

.method public notifyConsumers()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 347
    invoke-static {}, Llsx;->b()V

    .line 348
    invoke-virtual {p0}, Lkda;->getResourceConsumerCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 349
    invoke-virtual {p0}, Lkda;->recycle()V

    .line 366
    :goto_0
    return-void

    .line 353
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lkda;->mDeliveringResource:Z

    .line 355
    :try_start_0
    iget-object v0, p0, Lkda;->mConsumerList:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 356
    iget-object v0, p0, Lkda;->mConsumerList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v2

    .line 357
    :goto_1
    if-ge v1, v3, :cond_2

    .line 358
    iget-object v0, p0, Lkda;->mConsumerList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkdb;

    .line 359
    iget-object v4, v0, Lkdb;->a:Lkdd;

    iget-object v0, v0, Lkdb;->b:Ljava/lang/Object;

    invoke-interface {v4, p0}, Lkdd;->a(Lkda;)V

    .line 357
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 361
    :cond_1
    iget-object v0, p0, Lkda;->mConsumer:Lkdd;

    if-eqz v0, :cond_2

    .line 362
    iget-object v0, p0, Lkda;->mConsumer:Lkdd;

    iget-object v1, p0, Lkda;->mTag:Ljava/lang/Object;

    invoke-interface {v0, p0}, Lkdd;->a(Lkda;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 365
    :cond_2
    iput-boolean v2, p0, Lkda;->mDeliveringResource:Z

    goto :goto_0

    :catchall_0
    move-exception v0

    iput-boolean v2, p0, Lkda;->mDeliveringResource:Z

    throw v0
.end method

.method public recycle()V
    .locals 1

    .prologue
    .line 374
    const/4 v0, 0x0

    iput v0, p0, Lkda;->mStatus:I

    .line 375
    const/4 v0, 0x0

    iput-object v0, p0, Lkda;->mResource:Ljava/lang/Object;

    .line 376
    return-void
.end method

.method public register(Lkdd;)V
    .locals 1

    .prologue
    .line 258
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lkda;->register(Lkdd;Ljava/lang/Object;)V

    .line 259
    return-void
.end method

.method public register(Lkdd;Ljava/lang/Object;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 224
    invoke-static {}, Llsx;->b()V

    .line 225
    invoke-direct {p0}, Lkda;->checkForConcurrentModification()V

    .line 227
    invoke-direct {p0, p1, p2}, Lkda;->isRegistered(Lkdd;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 252
    :goto_0
    return-void

    .line 231
    :cond_0
    const/4 v0, 0x0

    .line 232
    iget-object v1, p0, Lkda;->mConsumerList:Ljava/util/ArrayList;

    if-eqz v1, :cond_2

    .line 233
    iget-object v0, p0, Lkda;->mConsumerList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    .line 234
    iget-object v1, p0, Lkda;->mConsumerList:Ljava/util/ArrayList;

    new-instance v2, Lkdb;

    invoke-direct {v2, p1, p2}, Lkdb;-><init>(Lkdd;Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 247
    :goto_1
    if-eqz v0, :cond_1

    .line 248
    iget-object v0, p0, Lkda;->mManager:Lkdg;

    invoke-interface {v0, p0}, Lkdg;->a(Lkda;)V

    .line 251
    :cond_1
    invoke-interface {p1, p0}, Lkdd;->a(Lkda;)V

    goto :goto_0

    .line 235
    :cond_2
    iget-object v1, p0, Lkda;->mConsumer:Lkdd;

    if-eqz v1, :cond_3

    .line 236
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lkda;->mConsumerList:Ljava/util/ArrayList;

    .line 237
    iget-object v1, p0, Lkda;->mConsumerList:Ljava/util/ArrayList;

    new-instance v2, Lkdb;

    iget-object v3, p0, Lkda;->mConsumer:Lkdd;

    iget-object v4, p0, Lkda;->mTag:Ljava/lang/Object;

    invoke-direct {v2, v3, v4}, Lkdb;-><init>(Lkdd;Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 238
    iput-object v5, p0, Lkda;->mConsumer:Lkdd;

    .line 239
    iput-object v5, p0, Lkda;->mTag:Ljava/lang/Object;

    .line 240
    iget-object v1, p0, Lkda;->mConsumerList:Ljava/util/ArrayList;

    new-instance v2, Lkdb;

    invoke-direct {v2, p1, p2}, Lkdb;-><init>(Lkdd;Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 242
    :cond_3
    iput-object p1, p0, Lkda;->mConsumer:Lkdd;

    .line 243
    iput-object p2, p0, Lkda;->mTag:Ljava/lang/Object;

    .line 244
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public setStatus(I)V
    .locals 0

    .prologue
    .line 130
    iput p1, p0, Lkda;->mStatus:I

    .line 131
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 440
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 441
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x40

    .line 442
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 443
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n  ID: "

    .line 444
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lkda;->mId:Lkdc;

    .line 445
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n  Status: "

    .line 446
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 447
    invoke-virtual {p0}, Lkda;->getStatusAsString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 448
    iget-boolean v0, p0, Lkda;->mDownloading:Z

    if-eqz v0, :cond_0

    .line 449
    const-string v0, "; downloading"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 451
    :cond_0
    iget-object v0, p0, Lkda;->mResource:Ljava/lang/Object;

    if-eqz v0, :cond_1

    .line 452
    invoke-virtual {p0, v2}, Lkda;->appendDescription(Ljava/lang/StringBuilder;)V

    .line 454
    :cond_1
    const-string v0, "\n  Consumers:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 455
    iget-object v0, p0, Lkda;->mConsumerList:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    .line 456
    iget-object v0, p0, Lkda;->mConsumerList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 457
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_3

    .line 458
    iget-object v0, p0, Lkda;->mConsumerList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkdb;

    .line 459
    const-string v4, "\n   "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 460
    iget-object v4, v0, Lkdb;->a:Lkdd;

    iget-object v0, v0, Lkdb;->b:Ljava/lang/Object;

    invoke-direct {p0, v2, v4, v0}, Lkda;->appendConsumer(Ljava/lang/StringBuilder;Lkdd;Ljava/lang/Object;)V

    .line 457
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 462
    :cond_2
    iget-object v0, p0, Lkda;->mConsumer:Lkdd;

    if-eqz v0, :cond_4

    .line 463
    const-string v0, "\n   "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 464
    iget-object v0, p0, Lkda;->mConsumer:Lkdd;

    iget-object v1, p0, Lkda;->mTag:Ljava/lang/Object;

    invoke-direct {p0, v2, v0, v1}, Lkda;->appendConsumer(Ljava/lang/StringBuilder;Lkdd;Ljava/lang/Object;)V

    .line 468
    :cond_3
    :goto_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 466
    :cond_4
    const-string v0, "\n   none"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method public unregister(Lkdd;)V
    .locals 1

    .prologue
    .line 293
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lkda;->unregister(Lkdd;Ljava/lang/Object;)V

    .line 294
    return-void
.end method

.method public unregister(Lkdd;Ljava/lang/Object;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 266
    invoke-static {}, Llsx;->b()V

    .line 267
    invoke-direct {p0}, Lkda;->checkForConcurrentModification()V

    .line 268
    iget-object v0, p0, Lkda;->mConsumer:Lkdd;

    if-ne v0, p1, :cond_3

    iget-object v0, p0, Lkda;->mTag:Ljava/lang/Object;

    if-nez v0, :cond_0

    if-eqz p2, :cond_1

    :cond_0
    iget-object v0, p0, Lkda;->mTag:Ljava/lang/Object;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lkda;->mTag:Ljava/lang/Object;

    .line 269
    invoke-virtual {v0, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 270
    :cond_1
    iput-object v1, p0, Lkda;->mConsumer:Lkdd;

    .line 271
    iput-object v1, p0, Lkda;->mTag:Ljava/lang/Object;

    .line 272
    iget-object v0, p0, Lkda;->mManager:Lkdg;

    invoke-interface {v0, p0}, Lkdg;->b(Lkda;)V

    .line 287
    :cond_2
    :goto_0
    return-void

    .line 273
    :cond_3
    iget-object v0, p0, Lkda;->mConsumerList:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    .line 274
    iget-object v0, p0, Lkda;->mConsumerList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 275
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_4

    .line 276
    iget-object v0, p0, Lkda;->mConsumerList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkdb;

    .line 277
    invoke-virtual {v0, p1, p2}, Lkdb;->a(Lkdd;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 278
    iget-object v0, p0, Lkda;->mConsumerList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 283
    :cond_4
    iget-object v0, p0, Lkda;->mConsumerList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 284
    iget-object v0, p0, Lkda;->mManager:Lkdg;

    invoke-interface {v0, p0}, Lkdg;->b(Lkda;)V

    goto :goto_0

    .line 275
    :cond_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method
