.class public Ljau;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lheo;


# instance fields
.field private a:Ljaa;

.field private b:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const-class v0, Ljaa;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljaa;

    iput-object v0, p0, Ljau;->a:Ljaa;

    .line 28
    invoke-static {p1}, Lkib;->b(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Ljau;->b:Landroid/content/SharedPreferences;

    .line 29
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Lhem;)V
    .locals 0

    .prologue
    .line 43
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lheq;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 48
    return-void
.end method

.method public a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 32
    iget-object v1, p0, Ljau;->a:Ljaa;

    invoke-interface {v1}, Ljaa;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Ljau;->b:Landroid/content/SharedPreferences;

    const-string v2, "com.google.android.libraries.social.media.settings.MediaSettings.low_bandwidth_key"

    .line 33
    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
