.class public final Ldjc;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Llzq;",
        "Llzr;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Z

.field private final p:I

.field private final q:[Ljava/lang/Long;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/List;ZI)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;ZI)V"
        }
    .end annotation

    .prologue
    .line 32
    const-string v3, "deletephotos"

    new-instance v4, Llzq;

    invoke-direct {v4}, Llzq;-><init>()V

    new-instance v5, Llzr;

    invoke-direct {v5}, Llzr;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 34
    iput-object p3, p0, Ldjc;->a:Ljava/util/List;

    .line 35
    iput p5, p0, Ldjc;->p:I

    .line 36
    iput-boolean p4, p0, Ldjc;->b:Z

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Ldjc;->q:[Ljava/lang/Long;

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I[Ljava/lang/Long;ZI)V
    .locals 6

    .prologue
    .line 45
    const-string v3, "deletephotos"

    new-instance v4, Llzq;

    invoke-direct {v4}, Llzq;-><init>()V

    new-instance v5, Llzr;

    invoke-direct {v5}, Llzr;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 47
    iput-object p3, p0, Ldjc;->q:[Ljava/lang/Long;

    .line 48
    iput p5, p0, Ldjc;->p:I

    .line 49
    iput-boolean p4, p0, Ldjc;->b:Z

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Ldjc;->a:Ljava/util/List;

    .line 51
    return-void
.end method


# virtual methods
.method protected a(Llzq;)V
    .locals 8

    .prologue
    .line 55
    new-instance v0, Lnbb;

    invoke-direct {v0}, Lnbb;-><init>()V

    iput-object v0, p1, Llzq;->a:Lnbb;

    .line 56
    iget-object v2, p1, Llzq;->a:Lnbb;

    .line 58
    iget-object v0, p0, Ldjc;->a:Ljava/util/List;

    if-eqz v0, :cond_3

    .line 60
    iget-object v0, p0, Ldjc;->f:Landroid/content/Context;

    iget v1, p0, Ldjc;->c:I

    iget-object v3, p0, Ldjc;->a:Ljava/util/List;

    .line 61
    invoke-static {v0, v1, v3}, Ljvj;->a(Landroid/content/Context;ILjava/util/List;)Ljava/util/List;

    move-result-object v3

    .line 62
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    .line 63
    new-array v5, v4, [Ljava/lang/Long;

    .line 64
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    .line 65
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljad;

    invoke-virtual {v0}, Ljad;->b()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v5, v1

    .line 64
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 67
    :cond_0
    iput-object v5, v2, Lnbb;->a:[Ljava/lang/Long;

    .line 72
    :cond_1
    :goto_1
    iget v0, p0, Ldjc;->p:I

    iput v0, v2, Lnbb;->b:I

    .line 73
    iget-boolean v0, p0, Ldjc;->b:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v2, Lnbb;->c:Ljava/lang/Boolean;

    .line 74
    iget-object v0, p0, Ldjc;->f:Landroid/content/Context;

    const-class v1, Lfur;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfur;

    invoke-virtual {v0}, Lfur;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 75
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v2, Lnbb;->d:Ljava/lang/Boolean;

    .line 77
    :cond_2
    return-void

    .line 68
    :cond_3
    iget-object v0, p0, Ldjc;->q:[Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 69
    iget-object v0, p0, Ldjc;->q:[Ljava/lang/Long;

    iput-object v0, v2, Lnbb;->a:[Ljava/lang/Long;

    goto :goto_1
.end method

.method protected a(Llzr;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 81
    iget-object v1, p1, Llzr;->a:Lnbc;

    .line 83
    iget-object v2, v1, Lnbc;->a:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 84
    iget-object v2, p0, Ldjc;->f:Landroid/content/Context;

    iget v3, p0, Ldjc;->c:I

    iget-object v1, v1, Lnbc;->b:[Ljava/lang/String;

    iget v4, p0, Ldjc;->p:I

    if-ne v4, v0, :cond_1

    :goto_0
    invoke-static {v2, v3, v1, v0}, Ljvj;->a(Landroid/content/Context;I[Ljava/lang/String;Z)V

    .line 87
    :cond_0
    return-void

    .line 84
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 20
    check-cast p1, Llzq;

    invoke-virtual {p0, p1}, Ldjc;->a(Llzq;)V

    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 93
    iget v0, p0, Ldjc;->p:I

    return v0
.end method

.method protected synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 20
    check-cast p1, Llzr;

    invoke-virtual {p0, p1}, Ldjc;->a(Llzr;)V

    return-void
.end method
