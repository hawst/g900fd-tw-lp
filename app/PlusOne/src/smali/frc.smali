.class public final Lfrc;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Landroid/util/SparseIntArray;


# instance fields
.field private final b:Lmnk;

.field private final c:I

.field private d:I

.field private e:I

.field private f:Ljava/lang/String;

.field private g:Lfrc;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1424
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    .line 1426
    sput-object v0, Lfrc;->a:Landroid/util/SparseIntArray;

    const/16 v1, 0x30

    invoke-virtual {v0, v3, v1}, Landroid/util/SparseIntArray;->put(II)V

    .line 1427
    sget-object v0, Lfrc;->a:Landroid/util/SparseIntArray;

    const/4 v1, 0x2

    const/16 v2, 0x50

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 1428
    sget-object v0, Lfrc;->a:Landroid/util/SparseIntArray;

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 1429
    return-void
.end method

.method public constructor <init>(Lmnk;ILfrc;)V
    .locals 2

    .prologue
    .line 1453
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1456
    if-nez p1, :cond_0

    new-instance v0, Lmnk;

    invoke-direct {v0}, Lmnk;-><init>()V

    :goto_0
    iput-object v0, p0, Lfrc;->b:Lmnk;

    .line 1457
    iput p2, p0, Lfrc;->c:I

    .line 1458
    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->g:[B

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/String;

    iget-object v1, p1, Lmnk;->g:[B

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    :goto_1
    iput-object v0, p0, Lfrc;->f:Ljava/lang/String;

    .line 1460
    iput-object p3, p0, Lfrc;->g:Lfrc;

    .line 1461
    return-void

    :cond_0
    move-object v0, p1

    .line 1456
    goto :goto_0

    .line 1458
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(F)I
    .locals 1

    .prologue
    .line 1726
    iget v0, p0, Lfrc;->c:I

    int-to-float v0, v0

    mul-float/2addr v0, p1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/view/View;)Lfrc;
    .locals 2

    .prologue
    .line 1439
    const v0, 0x7f100031

    invoke-virtual {p0, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    .line 1440
    instance-of v1, v0, Lfrc;

    if-eqz v1, :cond_0

    .line 1441
    check-cast v0, Lfrc;

    .line 1443
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lmoc;Z)Lfri;
    .locals 5

    .prologue
    .line 1959
    if-eqz p1, :cond_4

    new-instance v1, Lfri;

    if-eqz p2, :cond_0

    iget-object v0, p1, Lmoc;->a:Lmnu;

    iget-object v0, v0, Lmnu;->a:Ljava/lang/Float;

    .line 1961
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-direct {p0, v0}, Lfrc;->a(F)I

    move-result v0

    int-to-float v0, v0

    move v4, v0

    :goto_0
    if-eqz p2, :cond_1

    iget-object v0, p1, Lmoc;->a:Lmnu;

    iget-object v0, v0, Lmnu;->b:Ljava/lang/Float;

    .line 1962
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-direct {p0, v0}, Lfrc;->a(F)I

    move-result v0

    int-to-float v0, v0

    move v3, v0

    :goto_1
    if-eqz p2, :cond_2

    iget-object v0, p1, Lmoc;->b:Lmnu;

    iget-object v0, v0, Lmnu;->a:Ljava/lang/Float;

    .line 1963
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-direct {p0, v0}, Lfrc;->a(F)I

    move-result v0

    int-to-float v0, v0

    move v2, v0

    :goto_2
    if-eqz p2, :cond_3

    iget-object v0, p1, Lmoc;->b:Lmnu;

    iget-object v0, v0, Lmnu;->b:Ljava/lang/Float;

    .line 1964
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-direct {p0, v0}, Lfrc;->a(F)I

    move-result v0

    int-to-float v0, v0

    :goto_3
    invoke-direct {v1, v4, v3, v2, v0}, Lfri;-><init>(FFFF)V

    move-object v0, v1

    :goto_4
    return-object v0

    .line 1961
    :cond_0
    iget-object v0, p1, Lmoc;->a:Lmnu;

    iget-object v0, v0, Lmnu;->a:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    move v4, v0

    goto :goto_0

    .line 1962
    :cond_1
    iget-object v0, p1, Lmoc;->a:Lmnu;

    iget-object v0, v0, Lmnu;->b:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    move v3, v0

    goto :goto_1

    .line 1963
    :cond_2
    iget-object v0, p1, Lmoc;->b:Lmnu;

    iget-object v0, v0, Lmnu;->a:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    move v2, v0

    goto :goto_2

    .line 1964
    :cond_3
    iget-object v0, p1, Lmoc;->b:Lmnu;

    iget-object v0, v0, Lmnu;->b:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    goto :goto_3

    :cond_4
    const/4 v0, 0x0

    goto :goto_4
.end method


# virtual methods
.method public A()F
    .locals 1

    .prologue
    .line 1634
    invoke-virtual {p0}, Lfrc;->z()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->C:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method public B()I
    .locals 1

    .prologue
    .line 1638
    iget-object v0, p0, Lfrc;->b:Lmnk;

    invoke-static {v0}, Lfss;->e(Lmnk;)I

    move-result v0

    return v0
.end method

.method public C()I
    .locals 1

    .prologue
    .line 1648
    iget-object v0, p0, Lfrc;->b:Lmnk;

    invoke-static {v0}, Lfss;->f(Lmnk;)I

    move-result v0

    return v0
.end method

.method public D()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1653
    iget-object v1, p0, Lfrc;->b:Lmnk;

    iget-object v1, v1, Lmnk;->c:Lmof;

    if-nez v1, :cond_1

    .line 1660
    :cond_0
    return v0

    .line 1657
    :cond_1
    iget-object v1, p0, Lfrc;->b:Lmnk;

    iget-object v1, v1, Lmnk;->c:Lmof;

    iget-object v3, v1, Lmof;->y:[I

    array-length v4, v3

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    aget v2, v3, v1

    .line 1658
    sget-object v5, Lfrc;->a:Landroid/util/SparseIntArray;

    invoke-virtual {v5, v2}, Landroid/util/SparseIntArray;->get(I)I

    move-result v2

    or-int/2addr v2, v0

    .line 1657
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_0
.end method

.method public E()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 1667
    iget-object v1, p0, Lfrc;->b:Lmnk;

    iget v1, v1, Lmnk;->b:I

    if-ne v1, v0, :cond_0

    iget-object v1, p0, Lfrc;->b:Lmnk;

    iget-object v1, v1, Lmnk;->c:Lmof;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lfrc;->b:Lmnk;

    iget-object v1, v1, Lmnk;->c:Lmof;

    iget-object v1, v1, Lmof;->r:Lmne;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lfrc;->b:Lmnk;

    iget-object v1, v1, Lmnk;->c:Lmof;

    iget-object v1, v1, Lmof;->q:Ljava/lang/Float;

    .line 1670
    invoke-static {v1, v2}, Llsl;->a(Ljava/lang/Float;F)F

    move-result v1

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public F()I
    .locals 2

    .prologue
    .line 1675
    invoke-virtual {p0}, Lfrc;->E()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->q:Ljava/lang/Float;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Llsl;->a(Ljava/lang/Float;F)F

    move-result v0

    iget v1, p0, Lfrc;->c:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public G()I
    .locals 2

    .prologue
    .line 1681
    invoke-virtual {p0}, Lfrc;->E()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->p:Lmnu;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 1682
    :goto_0
    return v0

    .line 1681
    :cond_0
    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->p:Lmnu;

    iget-object v0, v0, Lmnu;->a:Ljava/lang/Float;

    const/4 v1, 0x0

    .line 1682
    invoke-static {v0, v1}, Llsl;->a(Ljava/lang/Float;F)F

    move-result v0

    invoke-direct {p0, v0}, Lfrc;->a(F)I

    move-result v0

    goto :goto_0
.end method

.method public H()I
    .locals 2

    .prologue
    .line 1687
    invoke-virtual {p0}, Lfrc;->E()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->p:Lmnu;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 1688
    :goto_0
    return v0

    .line 1687
    :cond_0
    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->p:Lmnu;

    iget-object v0, v0, Lmnu;->b:Ljava/lang/Float;

    const/4 v1, 0x0

    .line 1688
    invoke-static {v0, v1}, Llsl;->a(Ljava/lang/Float;F)F

    move-result v0

    invoke-direct {p0, v0}, Lfrc;->a(F)I

    move-result v0

    goto :goto_0
.end method

.method public I()I
    .locals 1

    .prologue
    .line 1693
    invoke-virtual {p0}, Lfrc;->E()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->r:Lmne;

    iget-object v0, v0, Lmne;->a:Ljava/lang/Integer;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public J()I
    .locals 2

    .prologue
    const v0, 0x3e19999a    # 0.15f

    .line 1699
    iget-object v1, p0, Lfrc;->b:Lmnk;

    iget-object v1, v1, Lmnk;->c:Lmof;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lfrc;->b:Lmnk;

    iget-object v1, v1, Lmnk;->c:Lmof;

    iget-object v1, v1, Lmof;->t:Ljava/lang/Float;

    .line 1700
    invoke-static {v1, v0}, Llsl;->a(Ljava/lang/Float;F)F

    move-result v0

    .line 1702
    :cond_0
    invoke-direct {p0, v0}, Lfrc;->a(F)I

    move-result v0

    return v0
.end method

.method public K()F
    .locals 2

    .prologue
    const v0, 0x3d23d70a    # 0.04f

    .line 1707
    iget-object v1, p0, Lfrc;->b:Lmnk;

    iget-object v1, v1, Lmnk;->c:Lmof;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lfrc;->b:Lmnk;

    iget-object v1, v1, Lmnk;->c:Lmof;

    iget-object v1, v1, Lmof;->i:Ljava/lang/Float;

    .line 1708
    invoke-static {v1, v0}, Llsl;->a(Ljava/lang/Float;F)F

    move-result v0

    .line 1710
    :cond_0
    iget v1, p0, Lfrc;->c:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    return v0
.end method

.method public L()I
    .locals 2

    .prologue
    .line 1715
    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->u:Ljava/lang/Float;

    const/4 v1, 0x0

    .line 1716
    invoke-static {v0, v1}, Llsl;->a(Ljava/lang/Float;F)F

    move-result v0

    invoke-direct {p0, v0}, Lfrc;->a(F)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public M()Z
    .locals 1

    .prologue
    .line 1721
    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->u:Ljava/lang/Float;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public N()Z
    .locals 1

    .prologue
    .line 1731
    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->o:Lmnq;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public O()I
    .locals 2

    .prologue
    .line 1736
    invoke-virtual {p0}, Lfrc;->N()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->o:Lmnq;

    iget v0, v0, Lmnq;->a:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->o:Lmnq;

    iget v0, v0, Lmnq;->a:I

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public P()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1742
    invoke-virtual {p0}, Lfrc;->N()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->o:Lmnq;

    iget-object v0, v0, Lmnq;->b:Lmod;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->o:Lmnq;

    iget-object v0, v0, Lmnq;->b:Lmod;

    iget-object v0, v0, Lmod;->a:Lmne;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->o:Lmnq;

    iget-object v0, v0, Lmnq;->b:Lmod;

    iget-object v0, v0, Lmod;->b:Ljava/lang/Float;

    .line 1744
    invoke-static {v0, v1}, Llsl;->a(Ljava/lang/Float;F)F

    move-result v0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public Q()I
    .locals 1

    .prologue
    .line 1749
    invoke-virtual {p0}, Lfrc;->P()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->o:Lmnq;

    iget-object v0, v0, Lmnq;->b:Lmod;

    iget-object v0, v0, Lmod;->a:Lmne;

    iget-object v0, v0, Lmne;->a:Ljava/lang/Integer;

    .line 1750
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public R()I
    .locals 1

    .prologue
    .line 1755
    invoke-virtual {p0}, Lfrc;->P()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->o:Lmnq;

    iget-object v0, v0, Lmnq;->b:Lmod;

    iget-object v0, v0, Lmod;->b:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-direct {p0, v0}, Lfrc;->a(F)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public S()Z
    .locals 1

    .prologue
    .line 1760
    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->z:Lmnb;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->z:Lmnb;

    iget-object v0, v0, Lmnb;->a:Lmod;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->z:Lmnb;

    iget-object v0, v0, Lmnb;->a:Lmod;

    iget-object v0, v0, Lmod;->b:Ljava/lang/Float;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->z:Lmnb;

    iget-object v0, v0, Lmnb;->a:Lmod;

    iget-object v0, v0, Lmod;->a:Lmne;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public T()Lfrd;
    .locals 5

    .prologue
    .line 1768
    invoke-virtual {p0}, Lfrc;->S()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1769
    const/4 v0, 0x0

    .line 1777
    :goto_0
    return-object v0

    .line 1772
    :cond_0
    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->z:Lmnb;

    .line 1774
    new-instance v1, Lfrd;

    iget-object v2, v0, Lmnb;->a:Lmod;

    iget-object v2, v2, Lmod;->b:Ljava/lang/Float;

    .line 1775
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-direct {p0, v2}, Lfrc;->a(F)I

    move-result v2

    iget-object v3, v0, Lmnb;->a:Lmod;

    iget-object v3, v3, Lmod;->a:Lmne;

    iget-object v3, v3, Lmne;->a:Ljava/lang/Integer;

    .line 1776
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget-object v4, v0, Lmnb;->b:Ljava/lang/Float;

    if-eqz v4, :cond_1

    iget-object v0, v0, Lmnb;->b:Ljava/lang/Float;

    .line 1777
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-direct {p0, v0}, Lfrc;->a(F)I

    move-result v0

    :goto_1
    invoke-direct {v1, v2, v3, v0}, Lfrd;-><init>(III)V

    move-object v0, v1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public U()I
    .locals 1

    .prologue
    .line 1782
    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 1783
    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->f:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1787
    :goto_0
    return v0

    .line 1784
    :cond_0
    iget-object v0, p0, Lfrc;->g:Lfrc;

    if-eqz v0, :cond_1

    .line 1785
    iget-object v0, p0, Lfrc;->g:Lfrc;

    invoke-virtual {v0}, Lfrc;->U()I

    move-result v0

    goto :goto_0

    .line 1787
    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public V()I
    .locals 1

    .prologue
    .line 1793
    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->l:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->l:Ljava/lang/Integer;

    .line 1794
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const v0, 0x7fffffff

    goto :goto_0
.end method

.method public W()Lfrh;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1799
    invoke-virtual {p0}, Lfrc;->E()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1800
    sget-object v0, Lfrh;->a:Lfrh;

    .line 1807
    :goto_0
    return-object v0

    .line 1802
    :cond_0
    invoke-virtual {p0}, Lfrc;->F()I

    move-result v1

    .line 1803
    invoke-virtual {p0}, Lfrc;->G()I

    move-result v2

    .line 1804
    invoke-virtual {p0}, Lfrc;->H()I

    move-result v3

    .line 1805
    new-instance v0, Lfrh;

    sub-int v4, v1, v2

    .line 1806
    invoke-static {v6, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    sub-int v5, v1, v3

    invoke-static {v6, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    add-int/2addr v2, v1

    .line 1807
    invoke-static {v6, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    add-int/2addr v1, v3

    invoke-static {v6, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-direct {v0, v4, v5, v2, v1}, Lfrh;-><init>(IIII)V

    goto :goto_0
.end method

.method public X()[Lhng;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1811
    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->d:Lmnj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->d:Lmnj;

    iget-object v0, v0, Lmnj;->f:Lmns;

    if-nez v0, :cond_2

    .line 1812
    :cond_0
    new-array v0, v1, [Lhng;

    .line 1818
    :cond_1
    return-object v0

    .line 1814
    :cond_2
    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->d:Lmnj;

    iget-object v0, v0, Lmnj;->f:Lmns;

    iget-object v0, v0, Lmns;->b:[Lmnf;

    invoke-static {v0}, Lfss;->a([Lmnf;)[Lhng;

    move-result-object v0

    .line 1815
    array-length v2, v0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 1816
    iget v4, p0, Lfrc;->c:I

    int-to-float v4, v4

    invoke-virtual {v3, v4}, Lhng;->f(F)V

    .line 1815
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public Y()I
    .locals 1

    .prologue
    .line 1823
    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->v:Lmoe;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->v:Lmoe;

    iget v0, v0, Lmoe;->a:I

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public Z()Z
    .locals 1

    .prologue
    .line 1829
    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->s:Lmnr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->s:Lmnr;

    iget-object v0, v0, Lmnr;->a:Lmne;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()Lfrc;
    .locals 1

    .prologue
    .line 1464
    iget-object v0, p0, Lfrc;->g:Lfrc;

    return-object v0
.end method

.method public a(II)V
    .locals 0

    .prologue
    .line 1516
    iput p1, p0, Lfrc;->e:I

    .line 1517
    iput p2, p0, Lfrc;->d:I

    .line 1518
    return-void
.end method

.method public a(I)[Lfrf;
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    .line 1995
    invoke-virtual {p0}, Lfrc;->ai()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1996
    new-array v1, p1, [Lfrf;

    .line 1997
    :goto_0
    if-ge v0, p1, :cond_0

    .line 1998
    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2, v5, v5, v6, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1999
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3, v5, v5, v6, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 2000
    new-instance v4, Lfrf;

    invoke-direct {v4, v2, v3}, Lfrf;-><init>(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    aput-object v4, v1, v0

    .line 1997
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move-object v0, v1

    .line 2015
    :goto_1
    return-object v0

    .line 2005
    :cond_1
    iget-object v1, p0, Lfrc;->b:Lmnk;

    iget-object v1, v1, Lmnk;->c:Lmof;

    iget-object v1, v1, Lmof;->E:Lmoh;

    iget-object v2, v1, Lmoh;->a:[Lmni;

    .line 2007
    array-length v1, v2

    new-array v1, v1, [Lfrf;

    .line 2008
    :goto_2
    array-length v3, v2

    if-ge v0, v3, :cond_2

    .line 2009
    aget-object v3, v2, v0

    .line 2010
    iget-object v4, v3, Lmni;->b:Lmnw;

    invoke-static {v4}, Lfss;->a(Lmnw;)Landroid/graphics/RectF;

    move-result-object v4

    .line 2011
    iget-object v3, v3, Lmni;->c:Lmnw;

    invoke-static {v3}, Lfss;->a(Lmnw;)Landroid/graphics/RectF;

    move-result-object v3

    .line 2012
    new-instance v5, Lfrf;

    invoke-direct {v5, v4, v3}, Lfrf;-><init>(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    aput-object v5, v1, v0

    .line 2008
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    move-object v0, v1

    .line 2015
    goto :goto_1
.end method

.method public aa()I
    .locals 1

    .prologue
    .line 1835
    invoke-virtual {p0}, Lfrc;->Z()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1836
    const/4 v0, 0x0

    .line 1838
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->s:Lmnr;

    iget-object v0, v0, Lmnr;->a:Lmne;

    iget-object v0, v0, Lmne;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public ab()Z
    .locals 1

    .prologue
    .line 1843
    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->A:Lmnt;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->A:Lmnt;

    iget-object v0, v0, Lmnt;->a:Lmng;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->A:Lmnt;

    iget-object v0, v0, Lmnt;->a:Lmng;

    iget-object v0, v0, Lmng;->a:Ljava/lang/Float;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ac()F
    .locals 1

    .prologue
    .line 1850
    invoke-virtual {p0}, Lfrc;->ab()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->A:Lmnt;

    iget-object v0, v0, Lmnt;->a:Lmng;

    iget-object v0, v0, Lmng;->a:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-direct {p0, v0}, Lfrc;->a(F)I

    move-result v0

    int-to-float v0, v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ad()Z
    .locals 1

    .prologue
    .line 1855
    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->s:Lmnr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->s:Lmnr;

    iget-object v0, v0, Lmnr;->b:Lmno;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ae()Lfrg;
    .locals 4

    .prologue
    .line 1861
    invoke-virtual {p0}, Lfrc;->ad()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1862
    const/4 v0, 0x0

    .line 1869
    :goto_0
    return-object v0

    .line 1865
    :cond_0
    new-instance v0, Lfrg;

    iget-object v1, p0, Lfrc;->b:Lmnk;

    iget-object v1, v1, Lmnk;->c:Lmof;

    iget-object v1, v1, Lmof;->s:Lmnr;

    iget-object v1, v1, Lmnr;->b:Lmno;

    iget-object v1, v1, Lmno;->a:Lmoc;

    const/4 v2, 0x0

    .line 1866
    invoke-direct {p0, v1, v2}, Lfrc;->a(Lmoc;Z)Lfri;

    move-result-object v1

    iget-object v2, p0, Lfrc;->b:Lmnk;

    iget-object v2, v2, Lmnk;->c:Lmof;

    iget-object v2, v2, Lmof;->s:Lmnr;

    iget-object v2, v2, Lmnr;->b:Lmno;

    iget-object v2, v2, Lmno;->b:Lmne;

    iget-object v2, v2, Lmne;->a:Ljava/lang/Integer;

    .line 1868
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v3, p0, Lfrc;->b:Lmnk;

    iget-object v3, v3, Lmnk;->c:Lmof;

    iget-object v3, v3, Lmof;->s:Lmnr;

    iget-object v3, v3, Lmnr;->b:Lmno;

    iget-object v3, v3, Lmno;->c:Lmne;

    iget-object v3, v3, Lmne;->a:Ljava/lang/Integer;

    .line 1869
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lfrg;-><init>(Lfri;II)V

    goto :goto_0
.end method

.method public af()Z
    .locals 1

    .prologue
    .line 1874
    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->D:Lmnd;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ag()Lfre;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1879
    invoke-virtual {p0}, Lfrc;->af()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1880
    const/4 v0, 0x0

    .line 1888
    :goto_0
    return-object v0

    .line 1883
    :cond_0
    new-instance v0, Lfre;

    iget-object v1, p0, Lfrc;->b:Lmnk;

    iget-object v1, v1, Lmnk;->c:Lmof;

    iget-object v1, v1, Lmof;->D:Lmnd;

    iget-object v1, v1, Lmnd;->b:Ljava/lang/Float;

    .line 1884
    invoke-static {v1, v4}, Llsl;->a(Ljava/lang/Float;F)F

    move-result v1

    invoke-direct {p0, v1}, Lfrc;->a(F)I

    move-result v1

    iget-object v2, p0, Lfrc;->b:Lmnk;

    iget-object v2, v2, Lmnk;->c:Lmof;

    iget-object v2, v2, Lmof;->D:Lmnd;

    iget-object v2, v2, Lmnd;->a:Ljava/lang/Float;

    .line 1886
    invoke-static {v2, v4}, Llsl;->a(Ljava/lang/Float;F)F

    move-result v2

    invoke-direct {p0, v2}, Lfrc;->a(F)I

    move-result v2

    iget-object v3, p0, Lfrc;->b:Lmnk;

    iget-object v3, v3, Lmnk;->c:Lmof;

    iget-object v3, v3, Lmof;->D:Lmnd;

    iget-object v3, v3, Lmnd;->c:Ljava/lang/Float;

    .line 1888
    invoke-static {v3, v4}, Llsl;->a(Ljava/lang/Float;F)F

    move-result v3

    invoke-direct {p0, v3}, Lfrc;->a(F)I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lfre;-><init>(III)V

    goto :goto_0
.end method

.method public ah()Lfri;
    .locals 2

    .prologue
    .line 1953
    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->d:Lmnj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->d:Lmnj;

    iget-object v0, v0, Lmnj;->e:Lmoc;

    .line 1955
    :goto_0
    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lfrc;->a(Lmoc;Z)Lfri;

    move-result-object v0

    return-object v0

    .line 1953
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ai()Z
    .locals 1

    .prologue
    .line 1985
    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->E:Lmoh;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->E:Lmoh;

    iget-object v0, v0, Lmoh;->a:[Lmni;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->E:Lmoh;

    iget-object v0, v0, Lmoh;->a:[Lmni;

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 1468
    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->i:Lmnl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->i:Lmnl;

    iget-object v0, v0, Lmnl;->b:Lmnn;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Lmnn;
    .locals 1

    .prologue
    .line 1472
    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->i:Lmnl;

    iget-object v0, v0, Lmnl;->b:Lmnn;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1476
    iget-object v0, p0, Lfrc;->f:Ljava/lang/String;

    return-object v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 1481
    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->a:Lmnu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->a:Lmnu;

    iget-object v0, v0, Lmnu;->a:Ljava/lang/Float;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 1487
    iget-object v0, p0, Lfrc;->b:Lmnk;

    invoke-static {v0}, Lfss;->a(Lmnk;)F

    move-result v0

    invoke-direct {p0, v0}, Lfrc;->a(F)I

    move-result v0

    return v0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 1492
    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->a:Lmnu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->a:Lmnu;

    iget-object v0, v0, Lmnu;->b:Ljava/lang/Float;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()I
    .locals 1

    .prologue
    .line 1498
    iget-object v0, p0, Lfrc;->b:Lmnk;

    invoke-static {v0}, Lfss;->b(Lmnk;)F

    move-result v0

    invoke-direct {p0, v0}, Lfrc;->a(F)I

    move-result v0

    return v0
.end method

.method public i()I
    .locals 1

    .prologue
    .line 1503
    iget v0, p0, Lfrc;->e:I

    return v0
.end method

.method public j()I
    .locals 1

    .prologue
    .line 1508
    iget v0, p0, Lfrc;->d:I

    return v0
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 1522
    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->b:Lmoa;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->b:Lmoa;

    iget-object v0, v0, Lmoa;->a:Ljava/lang/Float;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l()I
    .locals 1

    .prologue
    .line 1528
    iget-object v0, p0, Lfrc;->b:Lmnk;

    invoke-static {v0}, Lfss;->c(Lmnk;)F

    move-result v0

    invoke-direct {p0, v0}, Lfrc;->a(F)I

    move-result v0

    return v0
.end method

.method public m()Z
    .locals 1

    .prologue
    .line 1533
    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->b:Lmoa;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->b:Lmoa;

    iget-object v0, v0, Lmoa;->b:Ljava/lang/Float;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public n()I
    .locals 1

    .prologue
    .line 1539
    iget-object v0, p0, Lfrc;->b:Lmnk;

    invoke-static {v0}, Lfss;->d(Lmnk;)F

    move-result v0

    invoke-direct {p0, v0}, Lfrc;->a(F)I

    move-result v0

    return v0
.end method

.method public o()F
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1547
    iget-object v1, p0, Lfrc;->b:Lmnk;

    iget-object v1, v1, Lmnk;->c:Lmof;

    .line 1548
    if-eqz v1, :cond_0

    iget-object v2, v1, Lmof;->d:Lmnv;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lmof;->d:Lmnv;

    iget-object v2, v2, Lmnv;->a:Lmnu;

    if-eqz v2, :cond_0

    iget-object v1, v1, Lmof;->d:Lmnv;

    iget-object v1, v1, Lmnv;->a:Lmnu;

    iget-object v1, v1, Lmnu;->a:Ljava/lang/Float;

    .line 1550
    invoke-static {v1, v0}, Llsl;->a(Ljava/lang/Float;F)F

    move-result v0

    :cond_0
    return v0
.end method

.method public p()F
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1558
    iget-object v1, p0, Lfrc;->b:Lmnk;

    iget-object v1, v1, Lmnk;->c:Lmof;

    .line 1559
    if-eqz v1, :cond_0

    iget-object v2, v1, Lmof;->d:Lmnv;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lmof;->d:Lmnv;

    iget-object v2, v2, Lmnv;->a:Lmnu;

    if-eqz v2, :cond_0

    iget-object v1, v1, Lmof;->d:Lmnv;

    iget-object v1, v1, Lmnv;->a:Lmnu;

    iget-object v1, v1, Lmnu;->b:Ljava/lang/Float;

    .line 1561
    invoke-static {v1, v0}, Llsl;->a(Ljava/lang/Float;F)F

    move-result v0

    :cond_0
    return v0
.end method

.method public q()Z
    .locals 1

    .prologue
    .line 1568
    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public r()I
    .locals 1

    .prologue
    .line 1576
    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->e:Ljava/lang/Integer;

    .line 1577
    invoke-static {v0}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public s()F
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1583
    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->c:Ljava/lang/Float;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Llsl;->a(Ljava/lang/Float;F)F

    move-result v0

    return v0
.end method

.method public t()I
    .locals 1

    .prologue
    .line 1588
    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->w:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 1589
    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->w:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1593
    :goto_0
    return v0

    .line 1590
    :cond_0
    iget-object v0, p0, Lfrc;->g:Lfrc;

    if-eqz v0, :cond_1

    .line 1591
    iget-object v0, p0, Lfrc;->g:Lfrc;

    invoke-virtual {v0}, Lfrc;->t()I

    move-result v0

    goto :goto_0

    .line 1593
    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public u()I
    .locals 2

    .prologue
    .line 1599
    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->f:Lmnc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->f:Lmnc;

    iget-object v0, v0, Lmnc;->a:Ljava/lang/Float;

    const/4 v1, 0x0

    .line 1600
    invoke-static {v0, v1}, Llsl;->a(Ljava/lang/Float;F)F

    move-result v0

    invoke-direct {p0, v0}, Lfrc;->a(F)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public v()I
    .locals 2

    .prologue
    .line 1605
    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->f:Lmnc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->f:Lmnc;

    iget-object v0, v0, Lmnc;->b:Ljava/lang/Float;

    const/4 v1, 0x0

    .line 1606
    invoke-static {v0, v1}, Llsl;->a(Ljava/lang/Float;F)F

    move-result v0

    invoke-direct {p0, v0}, Lfrc;->a(F)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public w()I
    .locals 2

    .prologue
    .line 1611
    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->f:Lmnc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->f:Lmnc;

    iget-object v0, v0, Lmnc;->d:Ljava/lang/Float;

    const/4 v1, 0x0

    .line 1612
    invoke-static {v0, v1}, Llsl;->a(Ljava/lang/Float;F)F

    move-result v0

    invoke-direct {p0, v0}, Lfrc;->a(F)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public x()I
    .locals 2

    .prologue
    .line 1617
    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->f:Lmnc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->f:Lmnc;

    iget-object v0, v0, Lmnc;->c:Ljava/lang/Float;

    const/4 v1, 0x0

    .line 1618
    invoke-static {v0, v1}, Llsl;->a(Ljava/lang/Float;F)F

    move-result v0

    invoke-direct {p0, v0}, Lfrc;->a(F)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public y()I
    .locals 2

    .prologue
    .line 1623
    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget v0, v0, Lmof;->n:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget v0, v0, Lmof;->n:I

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public z()Z
    .locals 1

    .prologue
    .line 1629
    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrc;->b:Lmnk;

    iget-object v0, v0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->C:Ljava/lang/Float;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
