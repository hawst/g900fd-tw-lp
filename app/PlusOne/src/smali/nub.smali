.class public final Lnub;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnuj;

.field private b:Lnug;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 469
    invoke-direct {p0}, Loxq;-><init>()V

    .line 472
    iput-object v0, p0, Lnub;->a:Lnuj;

    .line 475
    iput-object v0, p0, Lnub;->b:Lnug;

    .line 469
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 492
    const/4 v0, 0x0

    .line 493
    iget-object v1, p0, Lnub;->a:Lnuj;

    if-eqz v1, :cond_0

    .line 494
    const/4 v0, 0x1

    iget-object v1, p0, Lnub;->a:Lnuj;

    .line 495
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 497
    :cond_0
    iget-object v1, p0, Lnub;->b:Lnug;

    if-eqz v1, :cond_1

    .line 498
    const/4 v1, 0x2

    iget-object v2, p0, Lnub;->b:Lnug;

    .line 499
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 501
    :cond_1
    iget-object v1, p0, Lnub;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 502
    iput v0, p0, Lnub;->ai:I

    .line 503
    return v0
.end method

.method public a(Loxn;)Lnub;
    .locals 2

    .prologue
    .line 511
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 512
    sparse-switch v0, :sswitch_data_0

    .line 516
    iget-object v1, p0, Lnub;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 517
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnub;->ah:Ljava/util/List;

    .line 520
    :cond_1
    iget-object v1, p0, Lnub;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 522
    :sswitch_0
    return-object p0

    .line 527
    :sswitch_1
    iget-object v0, p0, Lnub;->a:Lnuj;

    if-nez v0, :cond_2

    .line 528
    new-instance v0, Lnuj;

    invoke-direct {v0}, Lnuj;-><init>()V

    iput-object v0, p0, Lnub;->a:Lnuj;

    .line 530
    :cond_2
    iget-object v0, p0, Lnub;->a:Lnuj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 534
    :sswitch_2
    iget-object v0, p0, Lnub;->b:Lnug;

    if-nez v0, :cond_3

    .line 535
    new-instance v0, Lnug;

    invoke-direct {v0}, Lnug;-><init>()V

    iput-object v0, p0, Lnub;->b:Lnug;

    .line 537
    :cond_3
    iget-object v0, p0, Lnub;->b:Lnug;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 512
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 480
    iget-object v0, p0, Lnub;->a:Lnuj;

    if-eqz v0, :cond_0

    .line 481
    const/4 v0, 0x1

    iget-object v1, p0, Lnub;->a:Lnuj;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 483
    :cond_0
    iget-object v0, p0, Lnub;->b:Lnug;

    if-eqz v0, :cond_1

    .line 484
    const/4 v0, 0x2

    iget-object v1, p0, Lnub;->b:Lnug;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 486
    :cond_1
    iget-object v0, p0, Lnub;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 488
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 465
    invoke-virtual {p0, p1}, Lnub;->a(Loxn;)Lnub;

    move-result-object v0

    return-object v0
.end method
