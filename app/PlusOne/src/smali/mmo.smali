.class public final Lmmo;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmmo;


# instance fields
.field public b:Lofq;

.field public c:I

.field public d:Ljava/lang/String;

.field public e:Lodo;

.field public f:Ljava/lang/String;

.field private g:Lmmf;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 538
    const/4 v0, 0x0

    new-array v0, v0, [Lmmo;

    sput-object v0, Lmmo;->a:[Lmmo;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 539
    invoke-direct {p0}, Loxq;-><init>()V

    .line 551
    iput-object v1, p0, Lmmo;->b:Lofq;

    .line 554
    const/high16 v0, -0x80000000

    iput v0, p0, Lmmo;->c:I

    .line 557
    iput-object v1, p0, Lmmo;->g:Lmmf;

    .line 562
    iput-object v1, p0, Lmmo;->e:Lodo;

    .line 539
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 593
    const/4 v0, 0x0

    .line 594
    iget-object v1, p0, Lmmo;->b:Lofq;

    if-eqz v1, :cond_0

    .line 595
    const/4 v0, 0x1

    iget-object v1, p0, Lmmo;->b:Lofq;

    .line 596
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 598
    :cond_0
    iget v1, p0, Lmmo;->c:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_1

    .line 599
    const/4 v1, 0x2

    iget v2, p0, Lmmo;->c:I

    .line 600
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 602
    :cond_1
    iget-object v1, p0, Lmmo;->g:Lmmf;

    if-eqz v1, :cond_2

    .line 603
    const/4 v1, 0x3

    iget-object v2, p0, Lmmo;->g:Lmmf;

    .line 604
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 606
    :cond_2
    iget-object v1, p0, Lmmo;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 607
    const/4 v1, 0x4

    iget-object v2, p0, Lmmo;->d:Ljava/lang/String;

    .line 608
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 610
    :cond_3
    iget-object v1, p0, Lmmo;->e:Lodo;

    if-eqz v1, :cond_4

    .line 611
    const/4 v1, 0x5

    iget-object v2, p0, Lmmo;->e:Lodo;

    .line 612
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 614
    :cond_4
    iget-object v1, p0, Lmmo;->f:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 615
    const/4 v1, 0x6

    iget-object v2, p0, Lmmo;->f:Ljava/lang/String;

    .line 616
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 618
    :cond_5
    iget-object v1, p0, Lmmo;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 619
    iput v0, p0, Lmmo;->ai:I

    .line 620
    return v0
.end method

.method public a(Loxn;)Lmmo;
    .locals 2

    .prologue
    .line 628
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 629
    sparse-switch v0, :sswitch_data_0

    .line 633
    iget-object v1, p0, Lmmo;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 634
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmmo;->ah:Ljava/util/List;

    .line 637
    :cond_1
    iget-object v1, p0, Lmmo;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 639
    :sswitch_0
    return-object p0

    .line 644
    :sswitch_1
    iget-object v0, p0, Lmmo;->b:Lofq;

    if-nez v0, :cond_2

    .line 645
    new-instance v0, Lofq;

    invoke-direct {v0}, Lofq;-><init>()V

    iput-object v0, p0, Lmmo;->b:Lofq;

    .line 647
    :cond_2
    iget-object v0, p0, Lmmo;->b:Lofq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 651
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 652
    if-eqz v0, :cond_3

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    const/4 v1, 0x4

    if-eq v0, v1, :cond_3

    const/4 v1, 0x5

    if-ne v0, v1, :cond_4

    .line 658
    :cond_3
    iput v0, p0, Lmmo;->c:I

    goto :goto_0

    .line 660
    :cond_4
    const/4 v0, 0x0

    iput v0, p0, Lmmo;->c:I

    goto :goto_0

    .line 665
    :sswitch_3
    iget-object v0, p0, Lmmo;->g:Lmmf;

    if-nez v0, :cond_5

    .line 666
    new-instance v0, Lmmf;

    invoke-direct {v0}, Lmmf;-><init>()V

    iput-object v0, p0, Lmmo;->g:Lmmf;

    .line 668
    :cond_5
    iget-object v0, p0, Lmmo;->g:Lmmf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 672
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmmo;->d:Ljava/lang/String;

    goto :goto_0

    .line 676
    :sswitch_5
    iget-object v0, p0, Lmmo;->e:Lodo;

    if-nez v0, :cond_6

    .line 677
    new-instance v0, Lodo;

    invoke-direct {v0}, Lodo;-><init>()V

    iput-object v0, p0, Lmmo;->e:Lodo;

    .line 679
    :cond_6
    iget-object v0, p0, Lmmo;->e:Lodo;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 683
    :sswitch_6
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmmo;->f:Ljava/lang/String;

    goto :goto_0

    .line 629
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 569
    iget-object v0, p0, Lmmo;->b:Lofq;

    if-eqz v0, :cond_0

    .line 570
    const/4 v0, 0x1

    iget-object v1, p0, Lmmo;->b:Lofq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 572
    :cond_0
    iget v0, p0, Lmmo;->c:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 573
    const/4 v0, 0x2

    iget v1, p0, Lmmo;->c:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 575
    :cond_1
    iget-object v0, p0, Lmmo;->g:Lmmf;

    if-eqz v0, :cond_2

    .line 576
    const/4 v0, 0x3

    iget-object v1, p0, Lmmo;->g:Lmmf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 578
    :cond_2
    iget-object v0, p0, Lmmo;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 579
    const/4 v0, 0x4

    iget-object v1, p0, Lmmo;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 581
    :cond_3
    iget-object v0, p0, Lmmo;->e:Lodo;

    if-eqz v0, :cond_4

    .line 582
    const/4 v0, 0x5

    iget-object v1, p0, Lmmo;->e:Lodo;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 584
    :cond_4
    iget-object v0, p0, Lmmo;->f:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 585
    const/4 v0, 0x6

    iget-object v1, p0, Lmmo;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 587
    :cond_5
    iget-object v0, p0, Lmmo;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 589
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 535
    invoke-virtual {p0, p1}, Lmmo;->a(Loxn;)Lmmo;

    move-result-object v0

    return-object v0
.end method
