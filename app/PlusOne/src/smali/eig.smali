.class final Leig;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lldh;


# instance fields
.field private synthetic a:Lehu;


# direct methods
.method constructor <init>(Lehu;)V
    .locals 0

    .prologue
    .line 3276
    iput-object p1, p0, Leig;->a:Lehu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/text/style/URLSpan;)V
    .locals 1

    .prologue
    .line 3393
    iget-object v0, p0, Leig;->a:Lehu;

    invoke-static {v0}, Lehu;->E(Lehu;)Lfdp;

    move-result-object v0

    invoke-virtual {v0, p1}, Lfdp;->a(Landroid/text/style/URLSpan;)V

    .line 3394
    return-void
.end method

.method public a(Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;)V
    .locals 17

    .prologue
    .line 3280
    move-object/from16 v0, p0

    iget-object v1, v0, Leig;->a:Lehu;

    invoke-static {v1}, Lehu;->k(Lehu;)Lhee;

    move-result-object v1

    invoke-interface {v1}, Lhee;->g()Lhej;

    move-result-object v1

    const-string v2, "gaia_id"

    invoke-interface {v1, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3282
    move-object/from16 v0, p0

    iget-object v2, v0, Leig;->a:Lehu;

    invoke-static {v2}, Lehu;->aC(Lehu;)Llnl;

    move-result-object v2

    invoke-virtual {v2}, Llnl;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 3283
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->d()Ljava/lang/String;

    move-result-object v4

    .line 3284
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->e()Ljava/lang/String;

    move-result-object v5

    .line 3285
    invoke-static {v1, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    .line 3286
    move-object/from16 v0, p0

    iget-object v2, v0, Leig;->a:Lehu;

    invoke-static {v2}, Lehu;->j(Lehu;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v7

    .line 3287
    new-instance v8, Ljava/util/ArrayList;

    const/4 v1, 0x5

    invoke-direct {v8, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 3288
    new-instance v9, Ljava/util/ArrayList;

    const/4 v1, 0x5

    invoke-direct {v9, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 3290
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->i()Z

    move-result v10

    .line 3291
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->h()Ljava/lang/String;

    move-result-object v11

    .line 3292
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->j()I

    move-result v12

    .line 3293
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->k()Z

    move-result v13

    .line 3294
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->l()Z

    move-result v14

    .line 3295
    move-object/from16 v0, p0

    iget-object v1, v0, Leig;->a:Lehu;

    invoke-static {v1}, Lehu;->i(Lehu;)Lfdr;

    move-result-object v1

    if-eqz v1, :cond_7

    move-object/from16 v0, p0

    iget-object v1, v0, Leig;->a:Lehu;

    invoke-static {v1}, Lehu;->i(Lehu;)Lfdr;

    move-result-object v1

    invoke-virtual {v1}, Lfdr;->c()Z

    move-result v1

    if-eqz v1, :cond_7

    const/4 v1, 0x1

    move v2, v1

    .line 3298
    :goto_0
    if-nez v13, :cond_0

    .line 3299
    if-eqz v10, :cond_8

    const v1, 0x7f0a0978

    :goto_1
    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3302
    const/16 v1, 0x25

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3304
    :cond_0
    if-eqz v6, :cond_9

    .line 3305
    const v1, 0x7f0a097d

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3306
    const/16 v1, 0x26

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3307
    const v1, 0x7f0a097f

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3308
    const/16 v1, 0x54

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3338
    :cond_1
    :goto_2
    if-nez v13, :cond_5

    .line 3342
    if-nez v7, :cond_2

    if-nez v6, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Leig;->a:Lehu;

    invoke-static {v1}, Lehu;->t(Lehu;)I

    move-result v1

    invoke-static {v1}, Lkto;->a(I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 3343
    :cond_2
    const v1, 0x7f0a097b

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3344
    const/16 v1, 0x21

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3346
    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, Leig;->a:Lehu;

    invoke-static {v1}, Lehu;->t(Lehu;)I

    move-result v1

    invoke-static {v1}, Lkto;->a(I)Z

    move-result v1

    if-eqz v1, :cond_4

    if-nez v6, :cond_4

    .line 3347
    const v1, 0x7f0a097c

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3349
    const/16 v1, 0x52

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3351
    :cond_4
    if-eqz v11, :cond_5

    if-lez v12, :cond_5

    .line 3352
    const v1, 0x7f0a0987

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3354
    const/16 v1, 0x40

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3357
    :cond_5
    if-eqz v14, :cond_6

    .line 3358
    if-eqz v2, :cond_d

    .line 3359
    move-object/from16 v0, p0

    iget-object v1, v0, Leig;->a:Lehu;

    invoke-virtual {v1}, Lehu;->o()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0977

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3361
    const/16 v1, 0x42

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3369
    :cond_6
    :goto_3
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v2, v1, [Ljava/lang/String;

    .line 3370
    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 3372
    move-object/from16 v0, p0

    iget-object v3, v0, Leig;->a:Lehu;

    if-eqz v13, :cond_e

    const v1, 0x7f0a0973

    :goto_4
    invoke-virtual {v3, v1}, Lehu;->e_(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v2}, Llgr;->a(Ljava/lang/String;[Ljava/lang/String;)Llgr;

    move-result-object v1

    .line 3378
    move-object/from16 v0, p0

    iget-object v2, v0, Leig;->a:Lehu;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Llgr;->a(Lu;I)V

    .line 3379
    invoke-virtual {v1}, Llgr;->k()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "comment_action"

    invoke-virtual {v2, v3, v9}, Landroid/os/Bundle;->putIntegerArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 3380
    invoke-virtual {v1}, Llgr;->k()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "comment_id"

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->f()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v3, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 3381
    invoke-virtual {v1}, Llgr;->k()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "comment_content"

    .line 3382
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->g()Landroid/text/Spanned;

    move-result-object v6

    invoke-static {v6}, Llht;->a(Landroid/text/Spanned;)[B

    move-result-object v6

    .line 3381
    invoke-virtual {v2, v3, v6}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 3383
    invoke-virtual {v1}, Llgr;->k()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "plus_one_id"

    invoke-virtual {v2, v3, v11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 3384
    invoke-virtual {v1}, Llgr;->k()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "plus_one_by_me"

    invoke-virtual {v2, v3, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 3385
    invoke-virtual {v1}, Llgr;->k()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "plus_one_count"

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->j()I

    move-result v6

    invoke-virtual {v2, v3, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 3386
    invoke-virtual {v1}, Llgr;->k()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "comment_author_id"

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 3387
    invoke-virtual {v1}, Llgr;->k()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "comment_author_name"

    invoke-virtual {v2, v3, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 3388
    move-object/from16 v0, p0

    iget-object v2, v0, Leig;->a:Lehu;

    invoke-virtual {v2}, Lehu;->p()Lae;

    move-result-object v2

    const-string v3, "delete_comment"

    invoke-virtual {v1, v2, v3}, Llgr;->a(Lae;Ljava/lang/String;)V

    .line 3389
    return-void

    .line 3295
    :cond_7
    const/4 v1, 0x0

    move v2, v1

    goto/16 :goto_0

    .line 3299
    :cond_8
    const v1, 0x7f0a0975

    goto/16 :goto_1

    .line 3310
    :cond_9
    if-eqz v13, :cond_a

    .line 3311
    const v1, 0x7f0a0980

    .line 3312
    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 3311
    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3313
    const/16 v1, 0x28

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3314
    const v1, 0x7f0a097a

    .line 3315
    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 3314
    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3316
    const/16 v1, 0x23

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3317
    if-eqz v7, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Leig;->a:Lehu;

    invoke-static {v1}, Lehu;->az(Lehu;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 3318
    const v1, 0x7f0a0986

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    aput-object v5, v15, v16

    invoke-virtual {v3, v1, v15}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3321
    const/16 v1, 0x31

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 3324
    :cond_a
    move-object/from16 v0, p0

    iget-object v1, v0, Leig;->a:Lehu;

    invoke-static {v1}, Lehu;->o(Lehu;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 3325
    const v1, 0x7f0a097e

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3326
    const/16 v1, 0x27

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3328
    :cond_b
    const v1, 0x7f0a097f

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3329
    const/16 v1, 0x54

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3330
    const v1, 0x7f0a0979

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3331
    if-eqz v7, :cond_c

    .line 3332
    const/16 v1, 0x24

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 3334
    :cond_c
    const/16 v1, 0x22

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 3363
    :cond_d
    move-object/from16 v0, p0

    iget-object v1, v0, Leig;->a:Lehu;

    invoke-virtual {v1}, Lehu;->o()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0976

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3365
    const/16 v1, 0x41

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 3372
    :cond_e
    const v1, 0x7f0a0972

    goto/16 :goto_4
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 3399
    iget-object v0, p0, Leig;->a:Lehu;

    invoke-static {v0}, Lehu;->aD(Lehu;)Llnh;

    move-result-object v0

    const-class v1, Lhsn;

    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhsn;

    .line 3400
    if-eqz v0, :cond_0

    .line 3401
    invoke-interface {v0, p1}, Lhsn;->a(Ljava/lang/String;)V

    .line 3403
    :cond_0
    return-void
.end method
