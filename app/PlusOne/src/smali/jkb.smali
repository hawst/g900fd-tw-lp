.class final Ljkb;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Ljkb;->a:Landroid/content/Context;

    .line 16
    return-void
.end method


# virtual methods
.method a()Llug;
    .locals 3

    .prologue
    .line 19
    new-instance v1, Llug;

    invoke-direct {v1}, Llug;-><init>()V

    .line 20
    iget-object v0, p0, Ljkb;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    const/16 v2, 0x1e0

    if-lt v0, v2, :cond_0

    const/16 v0, 0x9

    :goto_0
    iput v0, v1, Llug;->a:I

    .line 21
    iget-object v0, p0, Ljkb;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Llug;->b:Ljava/lang/String;

    .line 22
    return-object v1

    .line 20
    :cond_0
    const/16 v2, 0x140

    if-lt v0, v2, :cond_1

    const/4 v0, 0x5

    goto :goto_0

    :cond_1
    const/16 v2, 0xf0

    if-lt v0, v2, :cond_2

    const/4 v0, 0x4

    goto :goto_0

    :cond_2
    const/4 v0, 0x3

    goto :goto_0
.end method
