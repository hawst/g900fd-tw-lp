.class public final Lejc;
.super Leku;
.source "PG"


# instance fields
.field private N:Leky;

.field private O:I

.field private ad:Ljava/lang/String;

.field private ae:Ljava/lang/String;

.field private af:I

.field private ag:Z

.field private final ah:Lbc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbc",
            "<",
            "Lokb;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 75
    invoke-direct {p0}, Leku;-><init>()V

    .line 39
    new-instance v0, Lhnw;

    new-instance v1, Leje;

    invoke-direct {v1, p0}, Leje;-><init>(Lejc;)V

    invoke-direct {v0, p0, v1}, Lhnw;-><init>(Llrn;Lhnv;)V

    .line 52
    new-instance v0, Lejd;

    invoke-direct {v0, p0}, Lejd;-><init>(Lejc;)V

    iput-object v0, p0, Lejc;->ah:Lbc;

    .line 76
    return-void
.end method

.method static synthetic a(Lejc;)I
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lejc;->O:I

    return v0
.end method

.method static synthetic a(Lejc;I)I
    .locals 0

    .prologue
    .line 27
    iput p1, p0, Lejc;->O:I

    return p1
.end method

.method static synthetic a(Lejc;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 27
    iput-object p1, p0, Lejc;->ad:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lejc;Z)Z
    .locals 0

    .prologue
    .line 27
    iput-boolean p1, p0, Lejc;->ag:Z

    return p1
.end method

.method static synthetic b(Lejc;I)I
    .locals 0

    .prologue
    .line 27
    iput p1, p0, Lejc;->af:I

    return p1
.end method

.method static synthetic b(Lejc;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lejc;->ae:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lejc;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 27
    iput-object p1, p0, Lejc;->ae:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic c(Lejc;)I
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lejc;->af:I

    return v0
.end method

.method static synthetic d(Lejc;)Z
    .locals 1

    .prologue
    .line 27
    iget-boolean v0, p0, Lejc;->ag:Z

    return v0
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 118
    sget-object v0, Lhmw;->r:Lhmw;

    return-object v0
.end method

.method public a()I
    .locals 1

    .prologue
    .line 135
    iget v0, p0, Lejc;->O:I

    packed-switch v0, :pswitch_data_0

    .line 143
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 137
    :pswitch_0
    const/16 v0, 0x7f

    goto :goto_0

    .line 139
    :pswitch_1
    const/16 v0, 0xe

    goto :goto_0

    .line 141
    :pswitch_2
    const/16 v0, 0xd

    goto :goto_0

    .line 135
    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 87
    invoke-super {p0, p1, p2, p3}, Leku;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 88
    iget-object v1, p0, Lejc;->N:Leky;

    invoke-virtual {p0, v1}, Lejc;->a(Landroid/widget/ListAdapter;)V

    .line 89
    return-object v0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 80
    invoke-super {p0, p1}, Leku;->a(Landroid/os/Bundle;)V

    .line 81
    new-instance v0, Leky;

    invoke-direct {v0, p0}, Leky;-><init>(Leku;)V

    iput-object v0, p0, Lejc;->N:Leky;

    .line 82
    return-void
.end method

.method public a(Lhjk;)V
    .locals 1

    .prologue
    .line 112
    invoke-super {p0, p1}, Leku;->a(Lhjk;)V

    .line 113
    iget-object v0, p0, Lejc;->ad:Ljava/lang/String;

    invoke-interface {p1, v0}, Lhjk;->a(Ljava/lang/CharSequence;)V

    .line 114
    return-void
.end method

.method public a([Lohv;)V
    .locals 2

    .prologue
    .line 93
    const/16 v0, 0xf

    .line 94
    iget v1, p0, Lejc;->O:I

    packed-switch v1, :pswitch_data_0

    .line 105
    :goto_0
    iget-object v1, p0, Lejc;->N:Leky;

    invoke-virtual {v1, p1, v0}, Leky;->a([Lohv;I)V

    .line 106
    iget-object v0, p0, Lejc;->N:Leky;

    invoke-virtual {v0}, Leky;->notifyDataSetChanged()V

    .line 107
    invoke-virtual {p0}, Lejc;->x()Landroid/view/View;

    invoke-virtual {p0}, Lejc;->aa()V

    .line 108
    return-void

    .line 96
    :pswitch_0
    const/16 v0, 0x9

    .line 97
    goto :goto_0

    .line 99
    :pswitch_1
    const/16 v0, 0xa

    .line 100
    goto :goto_0

    .line 102
    :pswitch_2
    const/16 v0, 0xb

    goto :goto_0

    .line 94
    nop

    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public aR_()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 153
    const/4 v0, 0x4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public b()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 148
    const/16 v0, 0x68

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected d()V
    .locals 4

    .prologue
    .line 123
    const/4 v0, 0x1

    iput-boolean v0, p0, Lejc;->R:Z

    .line 124
    invoke-virtual {p0}, Lejc;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    iget-object v3, p0, Lejc;->ah:Lbc;

    invoke-virtual {v0, v1, v2, v3}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 125
    return-void
.end method

.method protected e()V
    .locals 4

    .prologue
    .line 129
    const/4 v0, 0x1

    iput-boolean v0, p0, Lejc;->R:Z

    .line 130
    invoke-virtual {p0}, Lejc;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    iget-object v3, p0, Lejc;->ah:Lbc;

    invoke-virtual {v0, v1, v2, v3}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 131
    return-void
.end method
