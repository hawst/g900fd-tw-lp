.class public Lcpj;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:I

.field private final c:Ljava/util/concurrent/ThreadPoolExecutor;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcpj;->a:Landroid/content/Context;

    .line 36
    iput p2, p0, Lcpj;->b:I

    .line 38
    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    const-wide/16 v4, 0x0

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v7}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    move v3, v2

    invoke-direct/range {v1 .. v7}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;)V

    iput-object v1, p0, Lcpj;->c:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 40
    return-void
.end method

.method static synthetic a(Lcpj;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcpj;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic b(Lcpj;)I
    .locals 1

    .prologue
    .line 22
    iget v0, p0, Lcpj;->b:I

    return v0
.end method

.method private b(Ljava/util/List;Lcpo;)Ljava/util/concurrent/Future;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lcpo",
            "<",
            "Ljava/util/List",
            "<",
            "Lcpg;",
            ">;>;)",
            "Ljava/util/concurrent/Future;"
        }
    .end annotation

    .prologue
    .line 76
    :try_start_0
    iget-object v0, p0, Lcpj;->c:Ljava/util/concurrent/ThreadPoolExecutor;

    new-instance v1, Lcpm;

    invoke-direct {v1, p0, p1, p2}, Lcpm;-><init>(Lcpj;Ljava/util/List;Lcpo;)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ThreadPoolExecutor;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;
    :try_end_0
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 102
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(JLcpo;)Ljava/util/concurrent/Future;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcpo",
            "<",
            "Lcpg;",
            ">;)",
            "Ljava/util/concurrent/Future;"
        }
    .end annotation

    .prologue
    .line 48
    new-instance v0, Lcpk;

    invoke-direct {v0, p1, p2}, Lcpk;-><init>(J)V

    new-instance v1, Lcpl;

    invoke-direct {v1, p3}, Lcpl;-><init>(Lcpo;)V

    invoke-direct {p0, v0, v1}, Lcpj;->b(Ljava/util/List;Lcpo;)Ljava/util/concurrent/Future;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/util/List;Lcpo;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lcpo",
            "<",
            "Ljava/util/List",
            "<",
            "Lcpg;",
            ">;>;)",
            "Ljava/util/concurrent/Future;"
        }
    .end annotation

    .prologue
    .line 70
    invoke-direct {p0, p1, p2}, Lcpj;->b(Ljava/util/List;Lcpo;)Ljava/util/concurrent/Future;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcpj;->c:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v0}, Ljava/util/concurrent/ThreadPoolExecutor;->shutdown()V

    .line 112
    return-void
.end method
