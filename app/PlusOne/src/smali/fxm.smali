.class public final Lfxm;
.super Lfwo;
.source "PG"


# instance fields
.field private g:Lfxj;

.field private h:Lidh;

.field private i:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lfxm;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lfxm;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0, p1, p2, p3}, Lfwo;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 36
    new-instance v0, Lfxj;

    invoke-direct {v0, p0}, Lfxj;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lfxm;->g:Lfxj;

    .line 37
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lfxm;->a(Z)V

    .line 38
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lfxm;->setFocusable(Z)V

    .line 39
    return-void
.end method


# virtual methods
.method protected a(IIII)I
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Lfxm;->g:Lfxj;

    iget-boolean v1, p0, Lfxm;->i:Z

    invoke-virtual {v0, p1, p2, v1, p3}, Lfxj;->a(IIZI)I

    move-result v0

    return v0
.end method

.method protected a(Landroid/graphics/Canvas;II)I
    .locals 2

    .prologue
    .line 88
    iget-object v0, p0, Lfxm;->g:Lfxj;

    add-int v1, p2, p3

    invoke-virtual {v0, p2, v1, p1}, Lfxj;->a(IILandroid/graphics/Canvas;)I

    move-result v0

    return v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 51
    invoke-super {p0}, Lfwo;->a()V

    .line 52
    iget-object v0, p0, Lfxm;->g:Lfxj;

    invoke-virtual {v0}, Lfxj;->a()V

    .line 53
    const/4 v0, 0x0

    iput-object v0, p0, Lfxm;->h:Lidh;

    .line 54
    return-void
.end method

.method public a(Lidh;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 58
    iput-object p1, p0, Lfxm;->h:Lidh;

    .line 59
    iget-object v0, p0, Lfxm;->g:Lfxj;

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v6}, Lfxj;->a(Lfwo;Lidh;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 61
    invoke-virtual {p0}, Lfxm;->requestLayout()V

    .line 62
    return-void
.end method

.method protected d()V
    .locals 1

    .prologue
    .line 93
    invoke-super {p0}, Lfwo;->d()V

    .line 94
    iget-object v0, p0, Lfxm;->g:Lfxj;

    invoke-virtual {v0}, Lfxj;->d()V

    .line 95
    return-void
.end method

.method protected e()V
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lfxm;->g:Lfxj;

    invoke-virtual {v0}, Lfxj;->e()V

    .line 100
    invoke-super {p0}, Lfwo;->e()V

    .line 101
    return-void
.end method

.method public f()Lidh;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lfxm;->h:Lidh;

    return-object v0
.end method

.method public getContentDescription()Ljava/lang/CharSequence;
    .locals 9

    .prologue
    const v8, 0x7f0a0994

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 109
    invoke-virtual {p0}, Lfxm;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 110
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 112
    new-array v0, v7, [Ljava/lang/CharSequence;

    iget-object v3, p0, Lfxm;->h:Lidh;

    invoke-virtual {v3}, Lidh;->d()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v6

    invoke-static {v2, v0}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 115
    invoke-virtual {p0}, Lfxm;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v3, p0, Lfxm;->h:Lidh;

    invoke-virtual {v3}, Lidh;->l()Loyy;

    move-result-object v3

    .line 114
    invoke-static {v0}, Lidi;->a(Landroid/content/Context;)V

    iget-object v3, v3, Loyy;->b:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v0, v4, v5}, Lidi;->a(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v4, v5}, Lidi;->b(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    const-string v4, "%s %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v3, v5, v6

    aput-object v0, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 117
    new-array v3, v7, [Ljava/lang/CharSequence;

    aput-object v0, v3, v6

    invoke-static {v2, v3}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 119
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 120
    iget-object v3, p0, Lfxm;->h:Lidh;

    invoke-virtual {v3}, Lidh;->l()Loyy;

    move-result-object v3

    iget-object v3, v3, Loyy;->b:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 122
    iget-object v3, p0, Lfxm;->h:Lidh;

    invoke-static {v3}, Ldrm;->c(Lidh;)Z

    move-result v3

    .line 123
    iget-object v4, p0, Lfxm;->h:Lidh;

    .line 124
    invoke-virtual {v4}, Lidh;->l()Loyy;

    move-result-object v4

    iget-object v4, v4, Loyy;->c:Ljava/lang/String;

    .line 123
    invoke-static {v4, v0, v3}, Lidk;->a(Ljava/lang/String;Ljava/util/Calendar;Z)Ljava/lang/String;

    move-result-object v0

    .line 125
    new-array v3, v7, [Ljava/lang/CharSequence;

    aput-object v0, v3, v6

    invoke-static {v2, v3}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 127
    iget-object v0, p0, Lfxm;->h:Lidh;

    invoke-virtual {v0}, Lidh;->o()Lpao;

    move-result-object v3

    .line 128
    if-eqz v3, :cond_0

    iget-object v0, v3, Lpao;->d:Loya;

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, v3, Lpao;->d:Loya;

    sget-object v4, Lpcg;->a:Loxr;

    .line 130
    invoke-virtual {v0, v4}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpcg;

    .line 131
    const v4, 0x7f0a0993

    new-array v5, v7, [Ljava/lang/Object;

    if-eqz v0, :cond_1

    iget-object v0, v0, Lpcg;->c:Ljava/lang/String;

    :goto_0
    aput-object v0, v5, v6

    invoke-virtual {v1, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 133
    new-array v3, v7, [Ljava/lang/CharSequence;

    aput-object v0, v3, v6

    invoke-static {v2, v3}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 137
    :cond_0
    iget-object v0, p0, Lfxm;->h:Lidh;

    invoke-static {v0}, Ldrm;->a(Lidh;)I

    move-result v0

    .line 138
    packed-switch v0, :pswitch_data_0

    .line 154
    :pswitch_0
    const v0, 0x7f0a071b

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 158
    :goto_1
    new-array v1, v7, [Ljava/lang/CharSequence;

    aput-object v0, v1, v6

    invoke-static {v2, v1}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 160
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 131
    :cond_1
    iget-object v0, v3, Lpao;->b:Ljava/lang/String;

    goto :goto_0

    .line 141
    :pswitch_1
    new-array v0, v7, [Ljava/lang/Object;

    const v3, 0x7f0a071d

    .line 142
    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v6

    .line 141
    invoke-virtual {v1, v8, v0}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 146
    :pswitch_2
    new-array v0, v7, [Ljava/lang/Object;

    const v3, 0x7f0a0720

    .line 147
    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v6

    .line 146
    invoke-virtual {v1, v8, v0}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 150
    :pswitch_3
    new-array v0, v7, [Ljava/lang/Object;

    const v3, 0x7f0a071f

    .line 151
    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v6

    .line 150
    invoke-virtual {v1, v8, v0}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 138
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method protected onMeasure(II)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 66
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 67
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 68
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lfxm;->i:Z

    .line 69
    iget-boolean v0, p0, Lfxm;->i:Z

    if-eqz v0, :cond_2

    move v0, v2

    .line 71
    :goto_1
    sget v3, Lfxm;->a:I

    sget v4, Lfxm;->c:I

    sget v5, Lfxm;->a:I

    sget v6, Lfxm;->b:I

    add-int/2addr v5, v6

    sub-int v5, v2, v5

    sget v6, Lfxm;->c:I

    sget v7, Lfxm;->d:I

    add-int/2addr v6, v7

    sub-int v6, v0, v6

    invoke-virtual {p0, v3, v4, v5, v6}, Lfxm;->a(IIII)I

    move-result v3

    .line 75
    iget-boolean v4, p0, Lfxm;->i:Z

    if-eqz v4, :cond_0

    sget v0, Lfxm;->c:I

    add-int/2addr v0, v3

    sget v3, Lfxm;->d:I

    add-int/2addr v0, v3

    sget v3, Lfxm;->e:I

    add-int/2addr v0, v3

    :cond_0
    invoke-virtual {p0, v2, v0}, Lfxm;->setMeasuredDimension(II)V

    .line 78
    iget-object v0, p0, Lfxm;->f:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lfxm;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {p0}, Lfxm;->getMeasuredHeight()I

    move-result v3

    invoke-virtual {v0, v1, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 79
    return-void

    :cond_1
    move v0, v1

    .line 68
    goto :goto_0

    :cond_2
    move v0, v3

    .line 69
    goto :goto_1
.end method
