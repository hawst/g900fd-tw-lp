.class public final Llwy;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Llwy;


# instance fields
.field private b:Llxp;

.field private c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4448
    const/4 v0, 0x0

    new-array v0, v0, [Llwy;

    sput-object v0, Llwy;->a:[Llwy;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 4449
    invoke-direct {p0}, Loxq;-><init>()V

    .line 4452
    const/4 v0, 0x0

    iput-object v0, p0, Llwy;->b:Llxp;

    .line 4449
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 4471
    const/4 v0, 0x0

    .line 4472
    iget-object v1, p0, Llwy;->b:Llxp;

    if-eqz v1, :cond_0

    .line 4473
    const/4 v0, 0x1

    iget-object v1, p0, Llwy;->b:Llxp;

    .line 4474
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 4476
    :cond_0
    iget-object v1, p0, Llwy;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 4477
    const/4 v1, 0x2

    iget-object v2, p0, Llwy;->c:Ljava/lang/String;

    .line 4478
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4480
    :cond_1
    iget-object v1, p0, Llwy;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4481
    iput v0, p0, Llwy;->ai:I

    .line 4482
    return v0
.end method

.method public a(Loxn;)Llwy;
    .locals 2

    .prologue
    .line 4490
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 4491
    sparse-switch v0, :sswitch_data_0

    .line 4495
    iget-object v1, p0, Llwy;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 4496
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llwy;->ah:Ljava/util/List;

    .line 4499
    :cond_1
    iget-object v1, p0, Llwy;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4501
    :sswitch_0
    return-object p0

    .line 4506
    :sswitch_1
    iget-object v0, p0, Llwy;->b:Llxp;

    if-nez v0, :cond_2

    .line 4507
    new-instance v0, Llxp;

    invoke-direct {v0}, Llxp;-><init>()V

    iput-object v0, p0, Llwy;->b:Llxp;

    .line 4509
    :cond_2
    iget-object v0, p0, Llwy;->b:Llxp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 4513
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llwy;->c:Ljava/lang/String;

    goto :goto_0

    .line 4491
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 4459
    iget-object v0, p0, Llwy;->b:Llxp;

    if-eqz v0, :cond_0

    .line 4460
    const/4 v0, 0x1

    iget-object v1, p0, Llwy;->b:Llxp;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 4462
    :cond_0
    iget-object v0, p0, Llwy;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 4463
    const/4 v0, 0x2

    iget-object v1, p0, Llwy;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 4465
    :cond_1
    iget-object v0, p0, Llwy;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 4467
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 4445
    invoke-virtual {p0, p1}, Llwy;->a(Loxn;)Llwy;

    move-result-object v0

    return-object v0
.end method
