.class public final Leky;
.super Lekx;
.source "PG"


# instance fields
.field private a:Ljava/util/ArrayList;

.field private b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnrp;",
            ">;"
        }
    .end annotation
.end field

.field private c:[Lohv;

.field private d:I

.field private synthetic e:Leku;


# direct methods
.method protected constructor <init>(Leku;)V
    .locals 0

    .prologue
    .line 226
    iput-object p1, p0, Leky;->e:Leku;

    invoke-direct {p0}, Lekx;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 306
    iget-object v0, p0, Leky;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 307
    iget-object v0, p0, Leky;->b:Ljava/util/ArrayList;

    invoke-static {v0}, Lemi;->a(Ljava/util/ArrayList;)Z

    .line 311
    :goto_0
    invoke-virtual {p0}, Leky;->notifyDataSetChanged()V

    .line 312
    iget-object v0, p0, Leky;->e:Leku;

    iget-object v1, p0, Leky;->e:Leku;

    invoke-virtual {v1}, Leku;->x()Landroid/view/View;

    invoke-virtual {v0}, Leku;->aa()V

    .line 313
    return-void

    .line 308
    :cond_0
    iget-object v0, p0, Leky;->c:[Lohv;

    goto :goto_0
.end method

.method public a(Ljava/util/ArrayList;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lnrp;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 234
    iput-object p1, p0, Leky;->b:Ljava/util/ArrayList;

    iput-object p1, p0, Leky;->a:Ljava/util/ArrayList;

    .line 235
    const/4 v0, 0x0

    iput-object v0, p0, Leky;->c:[Lohv;

    .line 236
    iput p2, p0, Leky;->d:I

    .line 237
    return-void
.end method

.method public a([Lohv;I)V
    .locals 2

    .prologue
    .line 240
    iput-object p1, p0, Leky;->c:[Lohv;

    .line 241
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Leky;->a:Ljava/util/ArrayList;

    .line 242
    iput p2, p0, Leky;->d:I

    .line 243
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, Leky;->a:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Leky;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 257
    iget-object v0, p0, Leky;->a:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Leky;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 262
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v6, 0x0

    .line 274
    if-nez p2, :cond_1

    .line 275
    iget-object v0, p0, Leky;->e:Leku;

    invoke-virtual {v0, v6}, Leku;->a_(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p0, Leky;->e:Leku;

    .line 276
    invoke-virtual {v1}, Leku;->am_()I

    move-result v1

    invoke-virtual {v0, v1, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;

    move-object p2, v0

    .line 281
    :goto_0
    invoke-virtual {p0, p1}, Leky;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    .line 282
    iget-object v0, p0, Leky;->e:Leku;

    invoke-virtual {v0, v3}, Leku;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 285
    if-eqz v0, :cond_2

    iget-object v1, p0, Leky;->e:Leku;

    iget-object v1, v1, Leku;->Z:Lhee;

    .line 286
    invoke-interface {v1}, Lhee;->g()Lhej;

    move-result-object v1

    const-string v4, "gaia_id"

    invoke-interface {v1, v4}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    move v1, v2

    .line 288
    :goto_1
    invoke-virtual {p2}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/PeopleListRowView;

    .line 289
    iget-object v4, p0, Leky;->e:Leku;

    iget-object v5, p0, Leky;->e:Leku;

    iget-object v5, v5, Leku;->U:Lhxh;

    invoke-virtual {v0, v4, v5, v2}, Lcom/google/android/apps/plus/views/PeopleListRowView;->a(Lfze;Lhxh;Z)V

    .line 290
    iget-object v2, p0, Leky;->e:Leku;

    iget-boolean v2, v2, Leku;->X:Z

    if-eqz v2, :cond_0

    .line 291
    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PeopleListRowView;->b()V

    .line 293
    :cond_0
    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/PeopleListRowView;->b(Z)V

    .line 294
    iget-object v1, p0, Leky;->e:Leku;

    iget-object v1, v1, Leku;->W:Ljava/lang/String;

    iget-object v2, p0, Leky;->e:Leku;

    iget-object v2, v2, Leku;->Y:Lfuw;

    invoke-virtual {v0, v3, v1, v2}, Lcom/google/android/apps/plus/views/PeopleListRowView;->a(Ljava/lang/Object;Ljava/lang/String;Lfuw;)V

    .line 295
    iget-object v1, p0, Leky;->e:Leku;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/PeopleListRowView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 296
    iget v1, p0, Leky;->d:I

    invoke-virtual {v0, v1, v6}, Lcom/google/android/apps/plus/views/PeopleListRowView;->a(ILjava/lang/Object;)V

    .line 298
    invoke-virtual {p2}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->c()V

    .line 301
    return-object p2

    .line 278
    :cond_1
    check-cast p2, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;

    goto :goto_0

    .line 286
    :cond_2
    const/4 v0, 0x0

    move v1, v0

    goto :goto_1
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Leky;->a:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Leky;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEnabled(I)Z
    .locals 1

    .prologue
    .line 267
    const/4 v0, 0x0

    return v0
.end method
