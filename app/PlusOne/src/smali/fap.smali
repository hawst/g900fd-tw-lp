.class public final Lfap;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljgd;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Ljava/util/List;Ljava/util/Map;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljfy;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljfy;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 62
    invoke-interface {p2, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljfy;

    .line 63
    if-eqz v0, :cond_0

    .line 64
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 65
    invoke-interface {p2, p3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Ljava/util/List;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljfy;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljfy;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 24
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 25
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    move v2, v1

    .line 26
    :goto_0
    if-ge v2, v4, :cond_2

    .line 27
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljfy;

    .line 28
    invoke-virtual {v0}, Ljfy;->w()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 29
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Duplicate handle in navigation bar: "

    .line 30
    invoke-virtual {v0}, Ljfy;->w()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 26
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 35
    :cond_2
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 36
    const-string v0, "search"

    invoke-direct {p0, v2, v3, v0}, Lfap;->a(Ljava/util/List;Ljava/util/Map;Ljava/lang/String;)V

    .line 37
    const-string v0, "virtual_circles"

    invoke-direct {p0, v2, v3, v0}, Lfap;->a(Ljava/util/List;Ljava/util/Map;Ljava/lang/String;)V

    .line 38
    const-string v0, "circles"

    invoke-direct {p0, v2, v3, v0}, Lfap;->a(Ljava/util/List;Ljava/util/Map;Ljava/lang/String;)V

    .line 39
    const-string v0, "squares"

    invoke-direct {p0, v2, v3, v0}, Lfap;->a(Ljava/util/List;Ljava/util/Map;Ljava/lang/String;)V

    .line 40
    const-string v0, "clx"

    invoke-direct {p0, v2, v3, v0}, Lfap;->a(Ljava/util/List;Ljava/util/Map;Ljava/lang/String;)V

    .line 41
    const-string v0, "recents_virtual_circles"

    invoke-direct {p0, v2, v3, v0}, Lfap;->a(Ljava/util/List;Ljava/util/Map;Ljava/lang/String;)V

    .line 43
    const-string v0, "explore"

    invoke-direct {p0, v2, v3, v0}, Lfap;->a(Ljava/util/List;Ljava/util/Map;Ljava/lang/String;)V

    .line 47
    :goto_2
    if-ge v1, v4, :cond_4

    .line 48
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljfy;

    invoke-virtual {v0}, Ljfy;->w()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljfy;

    .line 49
    if-eqz v0, :cond_3

    .line 50
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 47
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 54
    :cond_4
    return-object v2
.end method
