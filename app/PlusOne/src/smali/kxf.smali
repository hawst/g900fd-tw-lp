.class public final Lkxf;
.super Lkxk;
.source "PG"


# instance fields
.field private final g:[Ljava/lang/String;

.field private final h:I


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1, p2, p3, p4}, Lkxk;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V

    .line 42
    iput-object p5, p0, Lkxf;->g:[Ljava/lang/String;

    .line 43
    iput p6, p0, Lkxf;->h:I

    .line 44
    return-void
.end method


# virtual methods
.method public synthetic j()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 23
    invoke-virtual {p0}, Lkxf;->l()Lkxl;

    move-result-object v0

    return-object v0
.end method

.method public l()Lkxl;
    .locals 19

    .prologue
    .line 48
    move-object/from16 v0, p0

    iget-object v1, v0, Lkxf;->d:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lkxf;->d:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    move-object/from16 v0, p0

    iget v2, v0, Lkxf;->h:I

    if-ge v1, v2, :cond_1

    .line 49
    :cond_0
    new-instance v1, Lkxl;

    invoke-direct {v1}, Lkxl;-><init>()V

    .line 87
    :goto_0
    return-object v1

    .line 52
    :cond_1
    new-instance v3, Lkxg;

    .line 53
    invoke-virtual/range {p0 .. p0}, Lkxf;->n()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lkfo;

    invoke-virtual/range {p0 .. p0}, Lkxf;->n()Landroid/content/Context;

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, Lkxf;->c:I

    invoke-direct {v2, v4, v5}, Lkfo;-><init>(Landroid/content/Context;I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lkxf;->d:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lkxf;->e:Ljava/lang/String;

    invoke-direct {v3, v1, v2, v4, v5}, Lkxg;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    move-object/from16 v0, p0

    iput-object v3, v0, Lkxf;->f:Lkff;

    .line 57
    :try_start_0
    invoke-virtual {v3}, Lkxg;->l()V

    .line 58
    invoke-virtual {v3}, Lkxg;->n()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 59
    sget-object v1, Lkxf;->b:Lkxl;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 62
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lkxf;->f:Lkff;

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-object v1, v0, Lkxf;->f:Lkff;

    .line 65
    invoke-virtual {v3}, Lkxg;->t()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 66
    const-string v1, "SquareSearch"

    invoke-virtual {v3, v1}, Lkxg;->d(Ljava/lang/String;)V

    .line 67
    iget-object v1, v3, Lkff;->k:Ljava/lang/Exception;

    if-eqz v1, :cond_3

    .line 68
    new-instance v1, Lkxl;

    iget-object v2, v3, Lkff;->k:Ljava/lang/Exception;

    invoke-direct {v1, v2}, Lkxl;-><init>(Ljava/lang/Exception;)V

    goto :goto_0

    .line 62
    :catchall_0
    move-exception v1

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lkxf;->f:Lkff;

    throw v1

    .line 70
    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    .line 73
    :cond_4
    const/4 v1, 0x0

    .line 74
    const/4 v2, 0x0

    .line 76
    invoke-virtual {v3}, Lkxg;->i()Lnna;

    move-result-object v3

    .line 77
    if-eqz v3, :cond_1a

    .line 78
    iget-object v4, v3, Lnna;->c:Lnng;

    if-eqz v4, :cond_5

    .line 79
    iget-object v1, v3, Lnna;->c:Lnng;

    iget-object v1, v1, Lnng;->a:[Lnnm;

    .line 81
    :cond_5
    iget-object v4, v3, Lnna;->a:Lnmy;

    if-eqz v4, :cond_19

    .line 82
    iget-object v2, v3, Lnna;->a:Lnmy;

    iget-object v2, v2, Lnmy;->b:Ljava/lang/String;

    move-object/from16 v18, v2

    move-object v2, v1

    move-object/from16 v1, v18

    .line 86
    :goto_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lkxf;->g:[Ljava/lang/String;

    const/4 v13, 0x0

    const/4 v11, -0x1

    const/4 v10, -0x1

    const/4 v9, -0x1

    const/4 v8, -0x1

    const/4 v7, -0x1

    const/4 v6, -0x1

    const/4 v5, -0x1

    const/4 v4, -0x1

    new-instance v15, Lhym;

    invoke-direct {v15, v12}, Lhym;-><init>([Ljava/lang/String;)V

    const/4 v3, 0x0

    :goto_2
    array-length v14, v12

    if-ge v3, v14, :cond_e

    aget-object v14, v12, v3

    const-string v16, "_id"

    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_7

    move v11, v3

    :cond_6
    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_7
    const-string v16, "square_id"

    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_8

    move v10, v3

    goto :goto_3

    :cond_8
    const-string v16, "square_name"

    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_9

    move v9, v3

    goto :goto_3

    :cond_9
    const-string v16, "photo_url"

    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_a

    move v8, v3

    goto :goto_3

    :cond_a
    const-string v16, "post_visibility"

    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_b

    move v7, v3

    goto :goto_3

    :cond_b
    const-string v16, "member_count"

    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_c

    move v6, v3

    goto :goto_3

    :cond_c
    const-string v16, "membership_status"

    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_d

    move v5, v3

    goto :goto_3

    :cond_d
    const-string v16, "joinability"

    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_6

    move v4, v3

    goto :goto_3

    :cond_e
    if-eqz v2, :cond_16

    array-length v3, v2

    :goto_4
    array-length v12, v12

    new-array v0, v12, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/4 v12, 0x0

    move v14, v12

    :goto_5
    if-ge v14, v3, :cond_17

    const/4 v12, 0x0

    move-object/from16 v0, v16

    invoke-static {v0, v12}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    aget-object v17, v2, v14

    if-ltz v11, :cond_18

    add-int/lit8 v12, v13, 0x1

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v16, v11

    :goto_6
    if-ltz v10, :cond_f

    move-object/from16 v0, v17

    iget-object v13, v0, Lnnm;->b:Lnsr;

    iget-object v13, v13, Lnsr;->b:Lnsb;

    iget-object v13, v13, Lnsb;->a:Ljava/lang/String;

    aput-object v13, v16, v10

    :cond_f
    if-ltz v9, :cond_10

    move-object/from16 v0, v17

    iget-object v13, v0, Lnnm;->b:Lnsr;

    iget-object v13, v13, Lnsr;->b:Lnsb;

    iget-object v13, v13, Lnsb;->b:Lnsc;

    iget-object v13, v13, Lnsc;->a:Ljava/lang/String;

    aput-object v13, v16, v9

    :cond_10
    if-ltz v8, :cond_11

    move-object/from16 v0, v17

    iget-object v13, v0, Lnnm;->b:Lnsr;

    iget-object v13, v13, Lnsr;->b:Lnsb;

    iget-object v13, v13, Lnsb;->b:Lnsc;

    iget-object v13, v13, Lnsc;->c:Ljava/lang/String;

    aput-object v13, v16, v8

    :cond_11
    if-ltz v7, :cond_12

    move-object/from16 v0, v17

    iget-object v13, v0, Lnnm;->b:Lnsr;

    iget-object v13, v13, Lnsr;->b:Lnsb;

    iget-object v13, v13, Lnsb;->c:Lnsf;

    iget v13, v13, Lnsf;->a:I

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v16, v7

    :cond_12
    if-ltz v6, :cond_13

    move-object/from16 v0, v17

    iget-object v13, v0, Lnnm;->b:Lnsr;

    iget-object v13, v13, Lnsr;->d:Lnsv;

    if-eqz v13, :cond_13

    move-object/from16 v0, v17

    iget-object v13, v0, Lnnm;->b:Lnsr;

    iget-object v13, v13, Lnsr;->d:Lnsv;

    iget-object v13, v13, Lnsv;->a:Ljava/lang/Integer;

    aput-object v13, v16, v6

    :cond_13
    if-ltz v5, :cond_14

    move-object/from16 v0, v17

    iget-object v13, v0, Lnnm;->b:Lnsr;

    iget v13, v13, Lnsr;->f:I

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v16, v5

    :cond_14
    if-ltz v4, :cond_15

    move-object/from16 v0, v17

    iget-object v13, v0, Lnnm;->b:Lnsr;

    iget-object v13, v13, Lnsr;->b:Lnsb;

    iget v13, v13, Lnsb;->d:I

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v16, v4

    :cond_15
    invoke-virtual/range {v15 .. v16}, Lhym;->a([Ljava/lang/Object;)V

    add-int/lit8 v13, v14, 0x1

    move v14, v13

    move v13, v12

    goto/16 :goto_5

    :cond_16
    const/4 v3, 0x0

    goto/16 :goto_4

    .line 87
    :cond_17
    new-instance v2, Lkxl;

    move-object/from16 v0, p0

    iget-object v3, v0, Lkxf;->e:Ljava/lang/String;

    invoke-direct {v2, v15, v3, v1}, Lkxl;-><init>(Lhym;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v2

    goto/16 :goto_0

    :cond_18
    move v12, v13

    goto/16 :goto_6

    :cond_19
    move-object/from16 v18, v2

    move-object v2, v1

    move-object/from16 v1, v18

    goto/16 :goto_1

    :cond_1a
    move-object/from16 v18, v2

    move-object v2, v1

    move-object/from16 v1, v18

    goto/16 :goto_1
.end method
