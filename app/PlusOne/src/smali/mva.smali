.class public final Lmva;
.super Loxq;
.source "PG"


# instance fields
.field private a:I

.field private b:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12552
    invoke-direct {p0}, Loxq;-><init>()V

    .line 12567
    const/high16 v0, -0x80000000

    iput v0, p0, Lmva;->a:I

    .line 12552
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 12586
    const/4 v0, 0x0

    .line 12587
    iget v1, p0, Lmva;->a:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 12588
    const/4 v0, 0x1

    iget v1, p0, Lmva;->a:I

    .line 12589
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 12591
    :cond_0
    iget-object v1, p0, Lmva;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 12592
    const/4 v1, 0x2

    iget-object v2, p0, Lmva;->b:Ljava/lang/Integer;

    .line 12593
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 12595
    :cond_1
    iget-object v1, p0, Lmva;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12596
    iput v0, p0, Lmva;->ai:I

    .line 12597
    return v0
.end method

.method public a(Loxn;)Lmva;
    .locals 2

    .prologue
    .line 12605
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 12606
    sparse-switch v0, :sswitch_data_0

    .line 12610
    iget-object v1, p0, Lmva;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 12611
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmva;->ah:Ljava/util/List;

    .line 12614
    :cond_1
    iget-object v1, p0, Lmva;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 12616
    :sswitch_0
    return-object p0

    .line 12621
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 12622
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 12631
    :cond_2
    iput v0, p0, Lmva;->a:I

    goto :goto_0

    .line 12633
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lmva;->a:I

    goto :goto_0

    .line 12638
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmva;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 12606
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 12574
    iget v0, p0, Lmva;->a:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 12575
    const/4 v0, 0x1

    iget v1, p0, Lmva;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 12577
    :cond_0
    iget-object v0, p0, Lmva;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 12578
    const/4 v0, 0x2

    iget-object v1, p0, Lmva;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 12580
    :cond_1
    iget-object v0, p0, Lmva;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 12582
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 12548
    invoke-virtual {p0, p1}, Lmva;->a(Loxn;)Lmva;

    move-result-object v0

    return-object v0
.end method
