.class public final Lfic;
.super Ljava/lang/Thread;
.source "PG"


# instance fields
.field private final a:Landroid/os/Handler;

.field private final b:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/lang/Runnable;

.field private d:Landroid/os/Handler;

.field private e:Lfif;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Ljava/lang/String;Lfif;)V
    .locals 4

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 38
    new-instance v0, Lfid;

    invoke-direct {v0, p0}, Lfid;-><init>(Lfic;)V

    iput-object v0, p0, Lfic;->c:Ljava/lang/Runnable;

    .line 71
    iput-object p1, p0, Lfic;->a:Landroid/os/Handler;

    .line 72
    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfic;->setName(Ljava/lang/String;)V

    .line 73
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lfic;->b:Ljava/util/Queue;

    .line 74
    iput-object p3, p0, Lfic;->e:Lfif;

    .line 75
    return-void
.end method

.method static synthetic a(Lfic;)Ljava/util/Queue;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lfic;->b:Ljava/util/Queue;

    return-object v0
.end method

.method static synthetic b(Lfic;)Lfif;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lfic;->e:Lfif;

    return-object v0
.end method

.method static synthetic c(Lfic;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lfic;->c:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic d(Lfic;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lfic;->d:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lfic;->d:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 134
    iget-object v0, p0, Lfic;->d:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 138
    :cond_0
    iget-object v0, p0, Lfic;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    .line 139
    if-lez v0, :cond_1

    .line 140
    iget-object v0, p0, Lfic;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 142
    :cond_1
    return-void
.end method

.method public a(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Lfic;->b:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 85
    iget-object v0, p0, Lfic;->d:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lfic;->d:Landroid/os/Handler;

    iget-object v1, p0, Lfic;->c:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 88
    :cond_0
    return-void
.end method

.method public run()V
    .locals 2

    .prologue
    .line 106
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 108
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lfic;->d:Landroid/os/Handler;

    .line 111
    iget-object v0, p0, Lfic;->a:Landroid/os/Handler;

    new-instance v1, Lfie;

    invoke-direct {v1, p0}, Lfie;-><init>(Lfic;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 122
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 124
    iget-object v0, p0, Lfic;->e:Lfif;

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lfic;->e:Lfif;

    invoke-interface {v0}, Lfif;->a()V

    .line 127
    :cond_0
    return-void
.end method
