.class public final enum Ljiw;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Ljiw;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Ljiw;

.field public static final enum b:Ljiw;

.field public static final enum c:Ljiw;

.field public static final enum d:Ljiw;

.field public static final enum e:Ljiw;

.field public static final enum f:Ljiw;

.field public static final enum g:Ljiw;

.field private static final synthetic i:[Ljiw;


# instance fields
.field private final h:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 7
    new-instance v0, Ljiw;

    const-string v1, "UNREGISTERED"

    invoke-direct {v0, v1, v4, v4}, Ljiw;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ljiw;->a:Ljiw;

    .line 8
    new-instance v0, Ljiw;

    const-string v1, "FAILED_REGISTRATION"

    invoke-direct {v0, v1, v5, v5}, Ljiw;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ljiw;->b:Ljiw;

    .line 9
    new-instance v0, Ljiw;

    const-string v1, "FAILED_UNREGISTRATION"

    invoke-direct {v0, v1, v6, v6}, Ljiw;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ljiw;->c:Ljiw;

    .line 10
    new-instance v0, Ljiw;

    const-string v1, "PENDING_REGISTRATION"

    invoke-direct {v0, v1, v7, v7}, Ljiw;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ljiw;->d:Ljiw;

    .line 11
    new-instance v0, Ljiw;

    const-string v1, "PENDING_UNREGISTRATION"

    invoke-direct {v0, v1, v8, v8}, Ljiw;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ljiw;->e:Ljiw;

    .line 12
    new-instance v0, Ljiw;

    const-string v1, "REGISTERED"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Ljiw;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ljiw;->f:Ljiw;

    .line 13
    new-instance v0, Ljiw;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Ljiw;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ljiw;->g:Ljiw;

    .line 6
    const/4 v0, 0x7

    new-array v0, v0, [Ljiw;

    sget-object v1, Ljiw;->a:Ljiw;

    aput-object v1, v0, v4

    sget-object v1, Ljiw;->b:Ljiw;

    aput-object v1, v0, v5

    sget-object v1, Ljiw;->c:Ljiw;

    aput-object v1, v0, v6

    sget-object v1, Ljiw;->d:Ljiw;

    aput-object v1, v0, v7

    sget-object v1, Ljiw;->e:Ljiw;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Ljiw;->f:Ljiw;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Ljiw;->g:Ljiw;

    aput-object v2, v0, v1

    sput-object v0, Ljiw;->i:[Ljiw;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 25
    iput p3, p0, Ljiw;->h:I

    .line 26
    return-void
.end method

.method public static a(I)Ljiw;
    .locals 1

    .prologue
    .line 33
    packed-switch p0, :pswitch_data_0

    .line 41
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 34
    :pswitch_0
    sget-object v0, Ljiw;->a:Ljiw;

    goto :goto_0

    .line 35
    :pswitch_1
    sget-object v0, Ljiw;->b:Ljiw;

    goto :goto_0

    .line 36
    :pswitch_2
    sget-object v0, Ljiw;->c:Ljiw;

    goto :goto_0

    .line 37
    :pswitch_3
    sget-object v0, Ljiw;->d:Ljiw;

    goto :goto_0

    .line 38
    :pswitch_4
    sget-object v0, Ljiw;->e:Ljiw;

    goto :goto_0

    .line 39
    :pswitch_5
    sget-object v0, Ljiw;->f:Ljiw;

    goto :goto_0

    .line 40
    :pswitch_6
    sget-object v0, Ljiw;->g:Ljiw;

    goto :goto_0

    .line 33
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Ljiw;
    .locals 1

    .prologue
    .line 6
    const-class v0, Ljiw;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ljiw;

    return-object v0
.end method

.method public static values()[Ljiw;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Ljiw;->i:[Ljiw;

    invoke-virtual {v0}, [Ljiw;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljiw;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 29
    iget v0, p0, Ljiw;->h:I

    return v0
.end method
