.class public Ldpa;
.super Lhny;
.source "PG"


# static fields
.field private static a:Ldnp;


# instance fields
.field private final b:I

.field private final c:Liuh;

.field private final d:Llae;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILiuh;Llae;)V
    .locals 1

    .prologue
    .line 33
    const-string v0, "GetNearbyLocationsTask"

    invoke-direct {p0, p1, v0}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 35
    iput p2, p0, Ldpa;->b:I

    .line 36
    iput-object p3, p0, Ldpa;->c:Liuh;

    .line 37
    iput-object p4, p0, Ldpa;->d:Llae;

    .line 38
    return-void
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 43
    const-class v7, Ldpa;

    monitor-enter v7

    .line 45
    :try_start_0
    sget-object v0, Ldpa;->a:Ldnp;

    if-eqz v0, :cond_0

    sget-object v0, Ldpa;->a:Ldnp;

    invoke-virtual {v0}, Ldnp;->n()Z

    move-result v0

    if-nez v0, :cond_0

    .line 46
    sget-object v0, Ldpa;->a:Ldnp;

    invoke-virtual {v0}, Ldnp;->m()V

    .line 49
    :cond_0
    new-instance v0, Ldnp;

    invoke-virtual {p0}, Ldpa;->f()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Ldpa;->b:I

    iget-object v3, p0, Ldpa;->c:Liuh;

    iget-object v4, p0, Ldpa;->d:Llae;

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Ldnp;-><init>(Landroid/content/Context;ILiuh;Llae;Z)V

    .line 51
    sput-object v0, Ldpa;->a:Ldnp;

    .line 52
    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 54
    invoke-virtual {v0}, Lkff;->l()V

    .line 56
    const-class v1, Ldpa;

    monitor-enter v1

    .line 57
    const/4 v2, 0x0

    :try_start_1
    sput-object v2, Ldpa;->a:Ldnp;

    .line 58
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 60
    invoke-virtual {v0}, Lkff;->n()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 61
    new-instance v0, Lhoz;

    const/16 v1, 0xc8

    invoke-direct {v0, v1, v6, v6}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    .line 64
    :goto_0
    return-object v0

    .line 52
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 58
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    .line 63
    :cond_1
    new-instance v1, Lhoz;

    iget v2, v0, Lkff;->i:I

    iget-object v3, v0, Lkff;->k:Ljava/lang/Exception;

    .line 64
    invoke-virtual {v0}, Lkff;->t()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Ldpa;->f()Landroid/content/Context;

    move-result-object v0

    const v4, 0x7f0a07bf

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-direct {v1, v2, v3, v0}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_0

    :cond_2
    move-object v0, v6

    goto :goto_1
.end method
