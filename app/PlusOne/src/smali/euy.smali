.class public final Leuy;
.super Lhyd;
.source "PG"


# instance fields
.field private final e:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final f:I

.field private final g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Landroid/view/View$OnClickListener;

.field private j:I

.field private k:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;Ljava/lang/String;II)V
    .locals 2

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Lhyd;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    .line 39
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Leuy;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 55
    iput p4, p0, Leuy;->f:I

    .line 56
    iput-object p3, p0, Leuy;->g:Ljava/lang/String;

    .line 57
    iput p5, p0, Leuy;->j:I

    .line 58
    return-void
.end method

.method static synthetic a(Leuy;)V
    .locals 2

    .prologue
    .line 33
    iget-object v0, p0, Leuy;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 119
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 120
    const v1, 0x7f040043

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Leuy;->i:Landroid/view/View$OnClickListener;

    .line 62
    return-void
.end method

.method public a(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 11

    .prologue
    const/16 v8, 0x9

    const/16 v7, 0x8

    const/4 v1, 0x6

    const/4 v10, 0x2

    const/4 v2, 0x1

    .line 125
    check-cast p1, Lcom/google/android/apps/plus/views/AlbumTileView;

    .line 127
    const/4 v0, 0x3

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 128
    invoke-interface {p3, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 130
    :goto_0
    invoke-interface {p3, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v6, 0x5

    invoke-interface {p3, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5}, Ljvj;->a(J)Ljac;

    move-result-object v4

    iget-object v5, p0, Leuy;->c:Landroid/content/Context;

    invoke-static {v5, v1, v6, v4}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v4

    .line 132
    invoke-interface {p3, v7}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, -0x1

    .line 134
    :goto_1
    invoke-interface {p3, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    const-wide/16 v8, 0x2

    and-long/2addr v6, v8

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-eqz v5, :cond_2

    .line 136
    :goto_2
    invoke-interface {p3, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 138
    const v6, 0x7f10007f

    invoke-virtual {p1, v6, v5}, Lcom/google/android/apps/plus/views/AlbumTileView;->setTag(ILjava/lang/Object;)V

    .line 139
    invoke-virtual {p1, v3}, Lcom/google/android/apps/plus/views/AlbumTileView;->a(Ljava/lang/String;)V

    .line 140
    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/views/AlbumTileView;->a(Ljava/lang/Integer;)V

    .line 141
    invoke-virtual {p1, v1, v2}, Lcom/google/android/apps/plus/views/AlbumTileView;->a(IZ)V

    .line 142
    invoke-virtual {p1, v4}, Lcom/google/android/apps/plus/views/AlbumTileView;->a(Lizu;)V

    .line 143
    iget-object v0, p0, Leuy;->i:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/views/AlbumTileView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 145
    iget v0, p0, Leuy;->k:I

    int-to-float v0, v0

    const v1, 0x3f99999a    # 1.2f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 147
    new-instance v1, Llka;

    invoke-direct {v1, v10, v0}, Llka;-><init>(II)V

    .line 149
    invoke-virtual {p1, v1}, Lcom/google/android/apps/plus/views/AlbumTileView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 150
    return-void

    .line 129
    :cond_0
    invoke-interface {p3, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 133
    :cond_1
    invoke-interface {p3, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    goto :goto_1

    .line 134
    :cond_2
    const/4 v2, 0x0

    goto :goto_2
.end method

.method public b(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 2

    .prologue
    .line 71
    if-nez p1, :cond_0

    sget-object v0, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    .line 72
    :goto_0
    const-string v1, "resume_token"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leuy;->h:Ljava/lang/String;

    .line 74
    invoke-super {p0, p1}, Lhyd;->b(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0

    .line 71
    :cond_0
    invoke-interface {p1}, Landroid/database/Cursor;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 79
    invoke-super {p0}, Lhyd;->getCount()I

    move-result v0

    .line 80
    iget-object v1, p0, Leuy;->h:Ljava/lang/String;

    if-eqz v1, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    return v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 90
    invoke-super {p0}, Lhyd;->getCount()I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 91
    const/4 v0, -0x1

    .line 93
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InlinedApi"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 99
    iget-object v0, p0, Leuy;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-super {p0}, Lhyd;->getCount()I

    move-result v0

    sub-int/2addr v0, p1

    const/16 v1, 0x1e

    if-ge v0, v1, :cond_0

    .line 100
    iget-object v0, p0, Leuy;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v4, v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    new-instance v0, Leuz;

    iget-object v1, p0, Leuy;->c:Landroid/content/Context;

    iget v2, p0, Leuy;->f:I

    iget-object v3, p0, Leuy;->g:Ljava/lang/String;

    invoke-direct {v0, v1, v2, p0, v3}, Leuz;-><init>(Landroid/content/Context;ILeuy;Ljava/lang/String;)V

    .line 103
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-ge v1, v2, :cond_1

    .line 104
    new-array v1, v5, [Ljava/lang/String;

    iget-object v2, p0, Leuy;->h:Ljava/lang/String;

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 110
    :cond_0
    :goto_0
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getWidth()I

    move-result v0

    iget v1, p0, Leuy;->j:I

    div-int/2addr v0, v1

    iput v0, p0, Leuy;->k:I

    .line 111
    invoke-super {p0}, Lhyd;->getCount()I

    move-result v0

    if-ne p1, v0, :cond_2

    .line 112
    iget-object v0, p0, Leuy;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400f8

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 114
    :goto_1
    return-object v0

    .line 106
    :cond_1
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v5, [Ljava/lang/String;

    iget-object v3, p0, Leuy;->h:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 114
    :cond_2
    invoke-super {p0, p1, p2, p3}, Lhyd;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_1
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 85
    const/4 v0, 0x2

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x0

    return v0
.end method
