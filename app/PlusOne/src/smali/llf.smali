.class public Lllf;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private synthetic a:Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;


# direct methods
.method constructor <init>(Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;)V
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lllf;->a:Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 15

    .prologue
    .line 54
    invoke-static {}, Llsm;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lllf;->a:Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;->getLayoutDirection()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    move v6, v0

    .line 55
    :goto_0
    iget-object v0, p0, Lllf;->a:Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;->getPaddingLeft()I

    move-result v12

    .line 56
    iget-object v0, p0, Lllf;->a:Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;->getPaddingTop()I

    move-result v13

    .line 57
    const/4 v2, 0x0

    .line 58
    invoke-static {}, Llsm;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lllf;->a:Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;->getPaddingStart()I

    move-result v0

    move v7, v0

    .line 59
    :goto_1
    invoke-static {}, Llsm;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lllf;->a:Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;->getPaddingEnd()I

    move-result v0

    move v11, v0

    .line 60
    :goto_2
    if-eqz v6, :cond_4

    sub-int v0, p1, v7

    .line 61
    :goto_3
    iget-object v1, p0, Lllf;->a:Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;->getPaddingTop()I

    move-result v3

    .line 62
    iget-object v1, p0, Lllf;->a:Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;->getChildCount()I

    move-result v14

    .line 64
    iget-object v1, p0, Lllf;->a:Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;

    const/4 v4, 0x1

    invoke-static {v1, v4}, Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;->a(Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;I)I

    .line 65
    iget-object v1, p0, Lllf;->a:Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;

    const/4 v4, 0x0

    invoke-static {v1, v4}, Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;->b(Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;I)I

    .line 66
    const/4 v1, 0x0

    move v10, v1

    :goto_4
    if-ge v10, v14, :cond_a

    .line 67
    iget-object v1, p0, Lllf;->a:Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;

    invoke-virtual {v1, v10}, Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 68
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v4

    const/16 v5, 0x8

    if-eq v4, v5, :cond_c

    .line 69
    invoke-virtual {p0, v1}, Lllf;->a(Landroid/view/View;)V

    .line 74
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    .line 75
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    .line 76
    iget-object v8, p0, Lllf;->a:Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;

    invoke-static {v8}, Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;->a(Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;)I

    move-result v8

    if-ge v8, v5, :cond_0

    .line 77
    iget-object v8, p0, Lllf;->a:Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;

    invoke-static {v8, v5}, Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;->b(Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;I)I

    .line 80
    :cond_0
    if-eqz v6, :cond_6

    sub-int v8, v0, v4

    if-ge v8, v11, :cond_5

    const/4 v8, 0x1

    :goto_5
    if-eqz v8, :cond_b

    .line 81
    iget-object v0, p0, Lllf;->a:Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;

    invoke-static {v0}, Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;->b(Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;)I

    .line 82
    if-eqz v6, :cond_8

    sub-int v0, p1, v7

    .line 83
    :goto_6
    add-int/2addr v2, v13

    add-int/2addr v3, v2

    .line 84
    const/4 v2, 0x0

    move v8, v0

    move v9, v2

    .line 87
    :goto_7
    if-eqz v6, :cond_9

    .line 88
    sub-int v2, v8, v4

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lllf;->a(Landroid/view/View;IIII)V

    .line 89
    add-int v0, v4, v12

    sub-int v0, v8, v0

    .line 95
    :goto_8
    invoke-static {v9, v5}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 66
    :goto_9
    add-int/lit8 v2, v10, 0x1

    move v10, v2

    move v2, v1

    goto :goto_4

    .line 54
    :cond_1
    const/4 v0, 0x0

    move v6, v0

    goto/16 :goto_0

    .line 58
    :cond_2
    iget-object v0, p0, Lllf;->a:Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;->getPaddingLeft()I

    move-result v0

    move v7, v0

    goto/16 :goto_1

    .line 59
    :cond_3
    iget-object v0, p0, Lllf;->a:Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;->getPaddingRight()I

    move-result v0

    move v11, v0

    goto/16 :goto_2

    :cond_4
    move v0, v7

    .line 60
    goto/16 :goto_3

    .line 80
    :cond_5
    const/4 v8, 0x0

    goto :goto_5

    :cond_6
    add-int v8, v0, v4

    sub-int v9, p1, v11

    if-le v8, v9, :cond_7

    const/4 v8, 0x1

    goto :goto_5

    :cond_7
    const/4 v8, 0x0

    goto :goto_5

    :cond_8
    move v0, v7

    .line 82
    goto :goto_6

    :cond_9
    move-object v0, p0

    move v2, v8

    .line 91
    invoke-virtual/range {v0 .. v5}, Lllf;->a(Landroid/view/View;IIII)V

    .line 92
    add-int v0, v4, v12

    add-int/2addr v0, v8

    goto :goto_8

    .line 97
    :cond_a
    return-void

    :cond_b
    move v8, v0

    move v9, v2

    goto :goto_7

    :cond_c
    move v1, v2

    goto :goto_9
.end method

.method protected a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 123
    return-void
.end method

.method protected a(Landroid/view/View;IIII)V
    .locals 0

    .prologue
    .line 125
    return-void
.end method
