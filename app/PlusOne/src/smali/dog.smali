.class public final Ldog;
.super Lkgg;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkgg",
        "<",
        "Lmky;",
        "Lmkz;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Landroid/os/Bundle;


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final p:[B

.field private final q:Ljava/lang/String;

.field private final r:Ljava/lang/String;

.field private s:Ljava/lang/String;

.field private t:Landroid/graphics/RectF;

.field private u:J


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 34
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "uploadType"

    const-string v2, "multipart"

    invoke-virtual {v0, v1, v2}, Landroid/os/BaseBundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Ldog;->a:Landroid/os/Bundle;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/RectF;)V
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 85
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, v5

    move-object v7, v5

    invoke-direct/range {v0 .. v7}, Ldog;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)V

    .line 87
    iput-object p6, p0, Ldog;->t:Landroid/graphics/RectF;

    .line 88
    iput-object p5, p0, Ldog;->s:Ljava/lang/String;

    .line 89
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 55
    const-string v3, "uploadmedia"

    new-instance v4, Lmky;

    invoke-direct {v4}, Lmky;-><init>()V

    new-instance v5, Lmkz;

    invoke-direct {v5}, Lmkz;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lkgg;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Loxu;Loxu;)V

    .line 57
    iput-object p3, p0, Ldog;->b:Ljava/lang/String;

    .line 58
    iput-object p4, p0, Ldog;->c:Ljava/lang/String;

    .line 59
    iput-object p5, p0, Ldog;->r:Ljava/lang/String;

    .line 60
    iput-object p6, p0, Ldog;->q:Ljava/lang/String;

    .line 61
    iput-object v6, p0, Ldog;->t:Landroid/graphics/RectF;

    .line 62
    iput-object v6, p0, Ldog;->s:Ljava/lang/String;

    .line 63
    iput-object p7, p0, Ldog;->p:[B

    .line 64
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Ljava/lang/String;[B)V
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 79
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, v5

    move-object v7, p5

    invoke-direct/range {v0 .. v7}, Ldog;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)V

    .line 81
    return-void
.end method

.method private static a(Landroid/content/ContentResolver;Ljava/lang/String;)J
    .locals 11

    .prologue
    const/4 v10, 0x0

    const-wide/16 v6, 0x0

    const/4 v3, 0x0

    .line 174
    invoke-static {}, Llsx;->c()V

    .line 175
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 179
    const-string v0, "r"

    invoke-virtual {p0, v1, v0}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    .line 180
    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->getStatSize()J

    move-result-wide v4

    .line 182
    const-wide/16 v8, -0x1

    cmp-long v0, v4, v8

    if-eqz v0, :cond_0

    move-wide v0, v4

    .line 218
    :goto_0
    return-wide v0

    .line 185
    :cond_0
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_size"

    aput-object v0, v2, v10

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    .line 190
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 193
    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 194
    const/4 v2, 0x0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 198
    cmp-long v0, v2, v6

    if-lez v0, :cond_1

    move-wide v0, v2

    .line 199
    goto :goto_0

    .line 201
    :catch_0
    move-exception v0

    .line 202
    const-string v2, "HttpOperation"

    const-string v3, "Invalid length received from contentprovider"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 206
    :cond_1
    invoke-virtual {p0, v1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v2

    move-wide v0, v6

    .line 210
    :goto_1
    const-wide v4, 0x7fffffffffffffffL

    :try_start_1
    invoke-virtual {v2, v4, v5}, Ljava/io/InputStream;->skip(J)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v4

    .line 211
    cmp-long v3, v4, v6

    if-lez v3, :cond_2

    .line 212
    add-long/2addr v0, v4

    .line 215
    goto :goto_1

    .line 218
    :cond_2
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    throw v0
.end method


# virtual methods
.method public P_()[B
    .locals 1

    .prologue
    .line 132
    const/4 v0, 0x0

    return-object v0
.end method

.method public Q_()Ljava/lang/String;
    .locals 5

    .prologue
    .line 68
    iget-object v0, p0, Ldog;->f:Landroid/content/Context;

    const-string v1, "plusi"

    invoke-virtual {p0}, Ldog;->C()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    sget-object v4, Ldog;->a:Landroid/os/Bundle;

    invoke-static {v0, v1, v2, v3, v4}, Lkfy;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLandroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public R_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    const-string v0, "multipart/related; boundary=onetwothreefourfivesixseven"

    return-object v0
.end method

.method public T_()Ljava/nio/channels/ReadableByteChannel;
    .locals 6

    .prologue
    .line 137
    invoke-super {p0}, Lkgg;->P_()[B

    move-result-object v1

    .line 140
    iget-object v0, p0, Ldog;->s:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Ldog;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 142
    iget-object v2, p0, Ldog;->s:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v2

    .line 143
    iget-object v3, p0, Ldog;->s:Ljava/lang/String;

    invoke-static {v0, v3}, Ldog;->a(Landroid/content/ContentResolver;Ljava/lang/String;)J

    move-result-wide v4

    .line 144
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v3, 0x3b

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Multipart with streaming data, length: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 145
    new-instance v0, Ldlk;

    const-string v3, "image/jpeg"

    invoke-direct/range {v0 .. v5}, Ldlk;-><init>([BLjava/io/InputStream;Ljava/lang/String;J)V

    .line 152
    :goto_0
    invoke-virtual {v0}, Ldlk;->a()J

    move-result-wide v2

    iput-wide v2, p0, Ldog;->u:J

    .line 154
    invoke-virtual {v0}, Ldlk;->b()Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, Ljava/nio/channels/Channels;->newChannel(Ljava/io/InputStream;)Ljava/nio/channels/ReadableByteChannel;

    move-result-object v0

    return-object v0

    .line 148
    :cond_0
    iget-object v0, p0, Ldog;->p:[B

    array-length v0, v0

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x30

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Multipart with payload data, length: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 149
    new-instance v0, Ldlk;

    iget-object v2, p0, Ldog;->p:[B

    const-string v3, "image/jpeg"

    invoke-direct {v0, v1, v2, v3}, Ldlk;-><init>([B[BLjava/lang/String;)V

    goto :goto_0
.end method

.method public U_()J
    .locals 2

    .prologue
    .line 159
    iget-wide v0, p0, Ldog;->u:J

    return-wide v0
.end method

.method protected a(Lmky;)V
    .locals 3

    .prologue
    .line 93
    new-instance v0, Lncm;

    invoke-direct {v0}, Lncm;-><init>()V

    .line 94
    iput-object v0, p1, Lmky;->a:Lncm;

    .line 96
    iget-object v1, p0, Ldog;->b:Ljava/lang/String;

    iput-object v1, v0, Lncm;->a:Ljava/lang/String;

    .line 97
    iget-object v1, p0, Ldog;->c:Ljava/lang/String;

    iput-object v1, v0, Lncm;->b:Ljava/lang/String;

    .line 98
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lncm;->e:Ljava/lang/Boolean;

    .line 100
    iget-object v1, p0, Ldog;->q:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 101
    iget-object v1, p0, Ldog;->q:Ljava/lang/String;

    iput-object v1, v0, Lncm;->c:Ljava/lang/String;

    .line 104
    :cond_0
    iget-object v1, p0, Ldog;->r:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 105
    iget-object v1, p0, Ldog;->r:Ljava/lang/String;

    iput-object v1, v0, Lncm;->d:Ljava/lang/String;

    .line 108
    :cond_1
    iget-object v1, p0, Ldog;->t:Landroid/graphics/RectF;

    if-eqz v1, :cond_2

    .line 109
    new-instance v1, Lnjp;

    invoke-direct {v1}, Lnjp;-><init>()V

    .line 110
    iget-object v2, p0, Ldog;->t:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    iput-object v2, v1, Lnjp;->b:Ljava/lang/Float;

    .line 111
    iget-object v2, p0, Ldog;->t:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    iput-object v2, v1, Lnjp;->a:Ljava/lang/Float;

    .line 112
    iget-object v2, p0, Ldog;->t:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->right:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    iput-object v2, v1, Lnjp;->d:Ljava/lang/Float;

    .line 113
    iget-object v2, p0, Ldog;->t:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    iput-object v2, v1, Lnjp;->c:Ljava/lang/Float;

    .line 114
    iput-object v1, v0, Lncm;->i:Lnjp;

    .line 116
    :cond_2
    return-void
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 30
    check-cast p1, Lmky;

    invoke-virtual {p0, p1}, Ldog;->a(Lmky;)V

    return-void
.end method

.method public a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 170
    const/4 v0, 0x0

    return v0
.end method

.method public b([BLjava/lang/String;)V
    .locals 2

    .prologue
    .line 120
    const-string v0, "HttpOperation"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 121
    const-string v0, "profile"

    iget-object v1, p0, Ldog;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 122
    const-string v0, "HttpOperation"

    const-string v1, "Failed to upload and set profile photo"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Lkgg;->b([BLjava/lang/String;)V

    .line 128
    return-void

    .line 123
    :cond_1
    const-string v0, "scrapbook"

    iget-object v1, p0, Ldog;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 124
    const-string v0, "HttpOperation"

    const-string v1, "Failed to upload and set cover photo"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
