.class public final Lmjr;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnpm;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 4372
    invoke-direct {p0}, Loxq;-><init>()V

    .line 4375
    iput-object v0, p0, Lmjr;->apiHeader:Llyr;

    .line 4378
    iput-object v0, p0, Lmjr;->a:Lnpm;

    .line 4372
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 4395
    const/4 v0, 0x0

    .line 4396
    iget-object v1, p0, Lmjr;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 4397
    const/4 v0, 0x1

    iget-object v1, p0, Lmjr;->apiHeader:Llyr;

    .line 4398
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 4400
    :cond_0
    iget-object v1, p0, Lmjr;->a:Lnpm;

    if-eqz v1, :cond_1

    .line 4401
    const/4 v1, 0x2

    iget-object v2, p0, Lmjr;->a:Lnpm;

    .line 4402
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4404
    :cond_1
    iget-object v1, p0, Lmjr;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4405
    iput v0, p0, Lmjr;->ai:I

    .line 4406
    return v0
.end method

.method public a(Loxn;)Lmjr;
    .locals 2

    .prologue
    .line 4414
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 4415
    sparse-switch v0, :sswitch_data_0

    .line 4419
    iget-object v1, p0, Lmjr;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 4420
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmjr;->ah:Ljava/util/List;

    .line 4423
    :cond_1
    iget-object v1, p0, Lmjr;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4425
    :sswitch_0
    return-object p0

    .line 4430
    :sswitch_1
    iget-object v0, p0, Lmjr;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 4431
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmjr;->apiHeader:Llyr;

    .line 4433
    :cond_2
    iget-object v0, p0, Lmjr;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 4437
    :sswitch_2
    iget-object v0, p0, Lmjr;->a:Lnpm;

    if-nez v0, :cond_3

    .line 4438
    new-instance v0, Lnpm;

    invoke-direct {v0}, Lnpm;-><init>()V

    iput-object v0, p0, Lmjr;->a:Lnpm;

    .line 4440
    :cond_3
    iget-object v0, p0, Lmjr;->a:Lnpm;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 4415
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 4383
    iget-object v0, p0, Lmjr;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 4384
    const/4 v0, 0x1

    iget-object v1, p0, Lmjr;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 4386
    :cond_0
    iget-object v0, p0, Lmjr;->a:Lnpm;

    if-eqz v0, :cond_1

    .line 4387
    const/4 v0, 0x2

    iget-object v1, p0, Lmjr;->a:Lnpm;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 4389
    :cond_1
    iget-object v0, p0, Lmjr;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 4391
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 4368
    invoke-virtual {p0, p1}, Lmjr;->a(Loxn;)Lmjr;

    move-result-object v0

    return-object v0
.end method
