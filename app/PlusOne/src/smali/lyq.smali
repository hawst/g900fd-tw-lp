.class public final Llyq;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Boolean;

.field public b:Lpyu;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 15
    const/4 v0, 0x0

    iput-object v0, p0, Llyq;->b:Lpyu;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 42
    const/4 v0, 0x0

    .line 43
    iget-object v1, p0, Llyq;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 44
    const/4 v0, 0x2

    iget-object v1, p0, Llyq;->a:Ljava/lang/Boolean;

    .line 45
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 47
    :cond_0
    iget-object v1, p0, Llyq;->b:Lpyu;

    if-eqz v1, :cond_1

    .line 48
    const/4 v1, 0x3

    iget-object v2, p0, Llyq;->b:Lpyu;

    .line 49
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 51
    :cond_1
    iget-object v1, p0, Llyq;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 52
    const/4 v1, 0x4

    iget-object v2, p0, Llyq;->c:Ljava/lang/String;

    .line 53
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 55
    :cond_2
    iget-object v1, p0, Llyq;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 56
    const/4 v1, 0x5

    iget-object v2, p0, Llyq;->d:Ljava/lang/String;

    .line 57
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 59
    :cond_3
    iget-object v1, p0, Llyq;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 60
    iput v0, p0, Llyq;->ai:I

    .line 61
    return v0
.end method

.method public a(Loxn;)Llyq;
    .locals 2

    .prologue
    .line 69
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 70
    sparse-switch v0, :sswitch_data_0

    .line 74
    iget-object v1, p0, Llyq;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 75
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llyq;->ah:Ljava/util/List;

    .line 78
    :cond_1
    iget-object v1, p0, Llyq;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 80
    :sswitch_0
    return-object p0

    .line 85
    :sswitch_1
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Llyq;->a:Ljava/lang/Boolean;

    goto :goto_0

    .line 89
    :sswitch_2
    iget-object v0, p0, Llyq;->b:Lpyu;

    if-nez v0, :cond_2

    .line 90
    new-instance v0, Lpyu;

    invoke-direct {v0}, Lpyu;-><init>()V

    iput-object v0, p0, Llyq;->b:Lpyu;

    .line 92
    :cond_2
    iget-object v0, p0, Llyq;->b:Lpyu;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 96
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llyq;->c:Ljava/lang/String;

    goto :goto_0

    .line 100
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llyq;->d:Ljava/lang/String;

    goto :goto_0

    .line 70
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x10 -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 24
    iget-object v0, p0, Llyq;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 25
    const/4 v0, 0x2

    iget-object v1, p0, Llyq;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 27
    :cond_0
    iget-object v0, p0, Llyq;->b:Lpyu;

    if-eqz v0, :cond_1

    .line 28
    const/4 v0, 0x3

    iget-object v1, p0, Llyq;->b:Lpyu;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 30
    :cond_1
    iget-object v0, p0, Llyq;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 31
    const/4 v0, 0x4

    iget-object v1, p0, Llyq;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 33
    :cond_2
    iget-object v0, p0, Llyq;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 34
    const/4 v0, 0x5

    iget-object v1, p0, Llyq;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 36
    :cond_3
    iget-object v0, p0, Llyq;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 38
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Llyq;->a(Loxn;)Llyq;

    move-result-object v0

    return-object v0
.end method
