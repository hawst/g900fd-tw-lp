.class public final Ljfd;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Ljfd;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljff;

.field public final b:Lood;

.field public final c:Ljava/lang/String;

.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 103
    new-instance v0, Ljfe;

    invoke-direct {v0}, Ljfe;-><init>()V

    sput-object v0, Ljfd;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljff;ILood;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    if-nez p1, :cond_0

    .line 63
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "status cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 65
    :cond_0
    sget-object v0, Ljff;->a:Ljff;

    if-ne p1, v0, :cond_2

    .line 66
    if-nez p3, :cond_1

    .line 67
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "null storyboard with OK status"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 69
    :cond_1
    if-nez p4, :cond_2

    .line 70
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "null versionId with OK status"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 73
    :cond_2
    iput-object p1, p0, Ljfd;->a:Ljff;

    .line 74
    iput p2, p0, Ljfd;->d:I

    .line 75
    iput-object p3, p0, Ljfd;->b:Lood;

    .line 76
    iput-object p4, p0, Ljfd;->c:Ljava/lang/String;

    .line 77
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 80
    const-string v1, "GetStoryboardResult[%s, %s, %s, %s]"

    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    iget-object v3, p0, Ljfd;->a:Ljff;

    aput-object v3, v2, v0

    const/4 v0, 0x1

    iget v3, p0, Ljfd;->d:I

    .line 82
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v3, 0x2

    iget-object v0, p0, Ljfd;->b:Lood;

    if-nez v0, :cond_0

    const-string v0, "null"

    .line 83
    :goto_0
    aput-object v0, v2, v3

    const/4 v0, 0x3

    iget-object v3, p0, Ljfd;->c:Ljava/lang/String;

    aput-object v3, v2, v0

    .line 80
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 82
    :cond_0
    iget-object v0, p0, Ljfd;->b:Lood;

    .line 83
    invoke-static {v0}, Loxv;->a(Loxu;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Ljfd;->a:Ljff;

    invoke-virtual {v0}, Ljff;->ordinal()I

    move-result v0

    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 95
    iget v0, p0, Ljfd;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 96
    iget-object v0, p0, Ljfd;->b:Lood;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 97
    iget-object v0, p0, Ljfd;->b:Lood;

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Ljfd;->b:Lood;

    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 100
    :cond_0
    iget-object v0, p0, Ljfd;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 101
    return-void

    .line 96
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
