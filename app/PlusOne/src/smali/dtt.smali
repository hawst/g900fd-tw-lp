.class final Ldtt;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:Ljava/lang/String;

.field private b:Landroid/content/Context;

.field private c:I

.field private d:Ldts;

.field private e:I

.field private f:I

.field private g:I

.field private h:Z

.field private i:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Ldns;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/content/Context;ILdvz;Ldts;I)V
    .locals 9

    .prologue
    .line 1745
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1723
    const/4 v0, 0x0

    iput v0, p0, Ldtt;->e:I

    .line 1725
    const/4 v0, 0x0

    iput v0, p0, Ldtt;->g:I

    .line 1726
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldtt;->h:Z

    .line 1731
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Ldtt;->i:Ljava/util/LinkedHashMap;

    .line 1737
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ldtt;->j:Ljava/util/HashMap;

    .line 1742
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ldtt;->k:Ljava/util/HashMap;

    .line 1746
    iput-object p1, p0, Ldtt;->b:Landroid/content/Context;

    .line 1747
    iput p2, p0, Ldtt;->c:I

    .line 1748
    iput-object p4, p0, Ldtt;->d:Ldts;

    .line 1749
    const/16 v0, 0xfa0

    iput v0, p0, Ldtt;->f:I

    .line 1751
    iget-object v0, p0, Ldtt;->b:Landroid/content/Context;

    sget-object v1, Lfit;->b:Lfit;

    invoke-static {v0, p2, v1}, Ldhv;->a(Landroid/content/Context;ILfit;)J

    move-result-wide v4

    .line 1755
    const/4 v0, 0x0

    iput-object v0, p0, Ldtt;->a:Ljava/lang/String;

    .line 1756
    const/4 v2, 0x0

    .line 1757
    invoke-static {p1, p2, p4}, Ldtu;->a(Landroid/content/Context;ILdts;)I

    move-result v3

    .line 1759
    if-eqz p3, :cond_0

    .line 1760
    invoke-virtual {p3, v3}, Ldvz;->e(I)V

    .line 1763
    :cond_0
    iget-object v0, p0, Ldtt;->b:Landroid/content/Context;

    iget v1, p0, Ldtt;->c:I

    invoke-static {v0, v1}, Lcuj;->b(Landroid/content/Context;I)Lnyj;

    move-result-object v0

    sget-object v1, Ldtf;->a:[I

    iget-object v6, p0, Ldtt;->d:Ldts;

    invoke-virtual {v6}, Ldts;->ordinal()I

    move-result v6

    aget v1, v1, v6

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x0

    move v1, v0

    .line 1765
    :goto_0
    if-eqz p3, :cond_1

    invoke-virtual {p3}, Ldvz;->c()Z

    move-result v0

    if-nez v0, :cond_b

    .line 1766
    :cond_1
    const-string v0, "EsTileSync"

    const/4 v6, 0x4

    invoke-static {v0, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1769
    iget v0, p0, Ldtt;->f:I

    iget v6, p0, Ldtt;->e:I

    sub-int/2addr v0, v6

    new-instance v6, Ljava/lang/StringBuilder;

    const/16 v7, 0x50

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "Getting equivalence tokens, request "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " with maxItemsPerPage="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1775
    :cond_2
    new-instance v0, Lkfo;

    iget-object v6, p0, Ldtt;->b:Landroid/content/Context;

    iget v7, p0, Ldtt;->c:I

    invoke-direct {v0, v6, v7, p3}, Lkfo;-><init>(Landroid/content/Context;ILkfp;)V

    .line 1776
    new-instance v6, Ldns;

    iget-object v7, p0, Ldtt;->b:Landroid/content/Context;

    invoke-direct {v6, v7, v0, p2}, Ldns;-><init>(Landroid/content/Context;Lkfo;I)V

    iget-object v0, p0, Ldtt;->a:Ljava/lang/String;

    .line 1778
    invoke-virtual {v6, v0}, Ldns;->a_(Ljava/lang/String;)Ldns;

    move-result-object v0

    const/4 v6, 0x1

    .line 1779
    invoke-virtual {v0, v6}, Ldns;->a(Z)Ldns;

    move-result-object v0

    .line 1780
    invoke-virtual {v0, v1}, Ldns;->b(Z)Ldns;

    move-result-object v0

    iget v6, p0, Ldtt;->f:I

    iget v7, p0, Ldtt;->e:I

    sub-int/2addr v6, v7

    .line 1781
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v0, v6}, Ldns;->a(Ljava/lang/Integer;)Ldns;

    move-result-object v0

    .line 1782
    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v0, v6}, Ldns;->b(Ljava/lang/Integer;)Ldns;

    move-result-object v0

    .line 1783
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v0, v6}, Ldns;->a(Ljava/lang/Long;)Ldns;

    move-result-object v6

    .line 1784
    invoke-virtual {v6}, Ldns;->l()V

    .line 1785
    const-string v0, "EsTileSync"

    invoke-virtual {v6, v0}, Ldns;->e(Ljava/lang/String;)V

    .line 1789
    invoke-virtual {v6}, Ldns;->e()I

    move-result v0

    iput v0, p0, Ldtt;->g:I

    .line 1790
    iget v0, p0, Ldtt;->g:I

    const/4 v7, 0x2

    if-ne v0, v7, :cond_3

    iget-object v0, p0, Ldtt;->d:Ldts;

    sget-object v7, Ldts;->c:Ldts;

    if-ne v0, v7, :cond_b

    .line 1792
    :cond_3
    if-eqz p3, :cond_4

    .line 1796
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Ldvz;->g(I)V

    .line 1799
    :cond_4
    invoke-virtual {v6}, Ldns;->h()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1800
    iget-object v8, p0, Ldtt;->i:Ljava/util/LinkedHashMap;

    invoke-virtual {v8, v0, v6}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 1763
    :pswitch_1
    const/4 v0, 0x1

    move v1, v0

    goto/16 :goto_0

    :pswitch_2
    if-eqz v0, :cond_5

    iget-object v0, v0, Lnyj;->e:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    move v1, v0

    goto/16 :goto_0

    :cond_5
    const/4 v0, 0x0

    move v1, v0

    goto/16 :goto_0

    :pswitch_3
    if-eqz v0, :cond_6

    iget-object v0, v0, Lnyj;->d:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    move v1, v0

    goto/16 :goto_0

    :cond_6
    const/4 v0, 0x0

    move v1, v0

    goto/16 :goto_0

    .line 1802
    :cond_7
    iget v0, p0, Ldtt;->e:I

    invoke-virtual {v6}, Ldns;->g()I

    move-result v7

    add-int/2addr v0, v7

    iput v0, p0, Ldtt;->e:I

    .line 1804
    invoke-direct {p0, v6}, Ldtt;->a(Ldns;)V

    .line 1806
    invoke-virtual {v6}, Ldns;->f()Ljava/lang/String;

    move-result-object v0

    .line 1807
    if-eqz v0, :cond_a

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_a

    iget-object v6, p0, Ldtt;->a:Ljava/lang/String;

    .line 1808
    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 1809
    const-string v0, "EsTileSync"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_b

    move v0, v2

    .line 1823
    :cond_8
    :goto_2
    const-string v1, "EsTileSync"

    const/4 v2, 0x4

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1824
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x44

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Finished getting equivalence tokens with "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " requests issued"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1827
    :cond_9
    return-void

    .line 1817
    :cond_a
    iput-object v0, p0, Ldtt;->a:Ljava/lang/String;

    .line 1818
    add-int/lit8 v0, v2, 0x1

    .line 1819
    if-ge v0, v3, :cond_8

    iget-object v2, p0, Ldtt;->a:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, Ldtt;->a:Ljava/lang/String;

    .line 1821
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_8

    iget v2, p0, Ldtt;->f:I

    iget v6, p0, Ldtt;->e:I

    sub-int/2addr v2, v6

    if-lez v2, :cond_8

    move v2, v0

    goto/16 :goto_0

    :cond_b
    move v0, v2

    goto :goto_2

    .line 1763
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method

.method private a(Ldns;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 1858
    invoke-virtual {p1}, Ldns;->d()Lndu;

    move-result-object v1

    .line 1859
    const/4 v0, 0x0

    :goto_0
    iget-object v2, v1, Lndu;->c:[Loud;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 1860
    iget-object v2, v1, Lndu;->c:[Loud;

    aget-object v2, v2, v0

    .line 1861
    iget-object v3, v2, Loud;->d:Losw;

    if-eqz v3, :cond_0

    .line 1862
    iput-boolean v6, p0, Ldtt;->h:Z

    .line 1863
    iget-object v3, p0, Ldtt;->j:Ljava/util/HashMap;

    iget-object v4, v2, Loud;->b:Ljava/lang/String;

    iget-object v5, v2, Loud;->d:Losw;

    iget-object v5, v5, Losw;->a:Ljava/lang/Integer;

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1865
    :cond_0
    iget-object v3, v2, Loud;->e:Losm;

    if-eqz v3, :cond_1

    .line 1866
    iput-boolean v6, p0, Ldtt;->h:Z

    .line 1867
    iget-object v3, p0, Ldtt;->k:Ljava/util/HashMap;

    iget-object v4, v2, Loud;->b:Ljava/lang/String;

    iget-object v2, v2, Loud;->e:Losm;

    iget-object v2, v2, Losm;->a:Ljava/lang/Integer;

    invoke-virtual {v3, v4, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1859
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1870
    :cond_2
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1886
    iget-object v0, p0, Ldtt;->i:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)Ljava/util/LinkedHashSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/LinkedHashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2011
    iget-object v0, p0, Ldtt;->i:Ljava/util/LinkedHashMap;

    .line 2012
    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldns;

    .line 2013
    invoke-virtual {v0, p1}, Ldns;->c(Ljava/lang/String;)Ljava/util/LinkedHashSet;

    move-result-object v0

    return-object v0
.end method

.method public a(Ldtk;)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1899
    iget v0, p0, Ldtt;->g:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    move v0, v2

    .line 1930
    :goto_0
    return v0

    .line 1904
    :cond_0
    invoke-virtual {p1}, Ldtk;->c()Ljava/util/Iterator;

    move-result-object v4

    .line 1905
    iget-object v0, p0, Ldtt;->i:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1906
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_2

    move v0, v3

    .line 1907
    goto :goto_0

    .line 1911
    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1912
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    move v0, v3

    .line 1913
    goto :goto_0

    .line 1917
    :cond_3
    iget-object v1, p0, Ldtt;->i:Ljava/util/LinkedHashMap;

    .line 1918
    invoke-virtual {v1, v0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldns;

    .line 1920
    invoke-virtual {v1, v0}, Ldns;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1921
    invoke-virtual {p1, v0, v1}, Ldtk;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v3

    .line 1923
    goto :goto_0

    .line 1926
    :cond_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v3

    .line 1928
    goto :goto_0

    :cond_5
    move v0, v2

    .line 1930
    goto :goto_0
.end method

.method public b(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 2064
    iget-object v0, p0, Ldtt;->j:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 2065
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public b(Ldtk;)Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldtk;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1941
    iget-object v0, p0, Ldtt;->d:Ldts;

    sget-object v1, Ldts;->c:Ldts;

    if-ne v0, v1, :cond_1

    .line 1942
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1943
    invoke-virtual {p0}, Ldtt;->a()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1944
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    move-object v0, v1

    .line 1972
    :goto_1
    return-object v0

    .line 1949
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1953
    iget v0, p0, Ldtt;->g:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    move-object v0, v2

    .line 1954
    goto :goto_1

    .line 1957
    :cond_2
    iget-object v0, p0, Ldtt;->i:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1958
    iget-object v1, p0, Ldtt;->i:Ljava/util/LinkedHashMap;

    .line 1959
    invoke-virtual {v1, v0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldns;

    .line 1961
    invoke-virtual {v1, v0}, Ldns;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1966
    if-eqz v1, :cond_3

    .line 1967
    invoke-virtual {p1, v0, v1}, Ldtk;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1969
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_4
    move-object v0, v2

    .line 1972
    goto :goto_1
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 2054
    iget-boolean v0, p0, Ldtt;->h:Z

    return v0
.end method

.method public c(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 2075
    iget-object v0, p0, Ldtt;->k:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 2076
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public c(Ldtk;)Ljava/util/HashSet;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldtk;",
            ")",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1982
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 1983
    iget-object v0, p0, Ldtt;->d:Ldts;

    sget-object v1, Ldts;->c:Ldts;

    if-ne v0, v1, :cond_0

    move-object v0, v2

    .line 2001
    :goto_0
    return-object v0

    .line 1988
    :cond_0
    iget-object v0, p0, Ldtt;->i:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1989
    iget-object v1, p0, Ldtt;->i:Ljava/util/LinkedHashMap;

    .line 1990
    invoke-virtual {v1, v0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldns;

    .line 1992
    invoke-virtual {v1, v0}, Ldns;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1993
    if-eqz v4, :cond_2

    .line 1994
    invoke-virtual {p1, v0, v4}, Ldtk;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    iget v4, p0, Ldtt;->g:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    .line 1997
    :cond_2
    invoke-virtual {v2, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1998
    invoke-virtual {v1, v0}, Ldns;->c(Ljava/lang/String;)Ljava/util/LinkedHashSet;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    :cond_3
    move-object v0, v2

    .line 2001
    goto :goto_0
.end method

.method public d(Ldtk;)Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldtk;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2022
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2027
    iget-object v0, p0, Ldtt;->j:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ldtt;->k:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move-object v0, v2

    .line 2047
    :goto_0
    return-object v0

    .line 2031
    :cond_1
    iget-object v0, p0, Ldtt;->i:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2032
    iget-object v1, p0, Ldtt;->i:Ljava/util/LinkedHashMap;

    .line 2033
    invoke-virtual {v1, v0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldns;

    .line 2034
    invoke-virtual {v1, v0}, Ldns;->c(Ljava/lang/String;)Ljava/util/LinkedHashSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2036
    invoke-virtual {p1, v0}, Ldtk;->c(Ljava/lang/String;)I

    move-result v4

    .line 2037
    invoke-virtual {p0, v0}, Ldtt;->c(Ljava/lang/String;)I

    move-result v5

    .line 2039
    invoke-virtual {p1, v0}, Ldtk;->b(Ljava/lang/String;)I

    move-result v6

    .line 2040
    invoke-virtual {p0, v0}, Ldtt;->b(Ljava/lang/String;)I

    move-result v7

    .line 2041
    if-ne v4, v5, :cond_4

    if-eq v6, v7, :cond_3

    .line 2043
    :cond_4
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_5
    move-object v0, v2

    .line 2047
    goto :goto_0
.end method
