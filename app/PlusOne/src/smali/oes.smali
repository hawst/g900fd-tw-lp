.class public final Loes;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:I

.field private c:Loet;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 421
    invoke-direct {p0}, Loxq;-><init>()V

    .line 431
    const/high16 v0, -0x80000000

    iput v0, p0, Loes;->b:I

    .line 434
    const/4 v0, 0x0

    iput-object v0, p0, Loes;->c:Loet;

    .line 421
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 459
    const/4 v0, 0x0

    .line 460
    iget-object v1, p0, Loes;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 461
    const/4 v0, 0x1

    iget-object v1, p0, Loes;->a:Ljava/lang/String;

    .line 462
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 464
    :cond_0
    iget v1, p0, Loes;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_1

    .line 465
    const/4 v1, 0x2

    iget v2, p0, Loes;->b:I

    .line 466
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 468
    :cond_1
    iget-object v1, p0, Loes;->c:Loet;

    if-eqz v1, :cond_2

    .line 469
    const/4 v1, 0x3

    iget-object v2, p0, Loes;->c:Loet;

    .line 470
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 472
    :cond_2
    iget-object v1, p0, Loes;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 473
    const/4 v1, 0x4

    iget-object v2, p0, Loes;->d:Ljava/lang/String;

    .line 474
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 476
    :cond_3
    iget-object v1, p0, Loes;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 477
    iput v0, p0, Loes;->ai:I

    .line 478
    return v0
.end method

.method public a(Loxn;)Loes;
    .locals 2

    .prologue
    .line 486
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 487
    sparse-switch v0, :sswitch_data_0

    .line 491
    iget-object v1, p0, Loes;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 492
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Loes;->ah:Ljava/util/List;

    .line 495
    :cond_1
    iget-object v1, p0, Loes;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 497
    :sswitch_0
    return-object p0

    .line 502
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loes;->a:Ljava/lang/String;

    goto :goto_0

    .line 506
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 507
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 509
    :cond_2
    iput v0, p0, Loes;->b:I

    goto :goto_0

    .line 511
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Loes;->b:I

    goto :goto_0

    .line 516
    :sswitch_3
    iget-object v0, p0, Loes;->c:Loet;

    if-nez v0, :cond_4

    .line 517
    new-instance v0, Loet;

    invoke-direct {v0}, Loet;-><init>()V

    iput-object v0, p0, Loes;->c:Loet;

    .line 519
    :cond_4
    iget-object v0, p0, Loes;->c:Loet;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 523
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loes;->d:Ljava/lang/String;

    goto :goto_0

    .line 487
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 441
    iget-object v0, p0, Loes;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 442
    const/4 v0, 0x1

    iget-object v1, p0, Loes;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 444
    :cond_0
    iget v0, p0, Loes;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 445
    const/4 v0, 0x2

    iget v1, p0, Loes;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 447
    :cond_1
    iget-object v0, p0, Loes;->c:Loet;

    if-eqz v0, :cond_2

    .line 448
    const/4 v0, 0x3

    iget-object v1, p0, Loes;->c:Loet;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 450
    :cond_2
    iget-object v0, p0, Loes;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 451
    const/4 v0, 0x4

    iget-object v1, p0, Loes;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 453
    :cond_3
    iget-object v0, p0, Loes;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 455
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 417
    invoke-virtual {p0, p1}, Loes;->a(Loxn;)Loes;

    move-result-object v0

    return-object v0
.end method
