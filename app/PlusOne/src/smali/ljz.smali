.class public final Lljz;
.super Landroid/database/DataSetObserver;
.source "PG"


# instance fields
.field private synthetic a:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;)V
    .locals 0

    .prologue
    .line 2758
    iput-object p1, p0, Lljz;->a:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 4

    .prologue
    .line 2761
    iget-object v0, p0, Lljz;->a:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->b(Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;Z)Z

    .line 2762
    iget-object v0, p0, Lljz;->a:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-static {v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;)I

    move-result v1

    .line 2763
    iget-object v0, p0, Lljz;->a:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    iget-object v2, p0, Lljz;->a:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-static {v2}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->b(Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;)Landroid/widget/ListAdapter;

    move-result-object v2

    invoke-interface {v2}, Landroid/widget/ListAdapter;->getCount()I

    move-result v2

    invoke-static {v0, v2}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;I)I

    .line 2765
    iget-object v0, p0, Lljz;->a:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-static {v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;)I

    move-result v0

    if-ge v0, v1, :cond_1

    .line 2767
    iget-object v0, p0, Lljz;->a:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-static {v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->c(Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;)Landroid/util/SparseBooleanArray;

    move-result-object v0

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    .line 2768
    iget-object v2, p0, Lljz;->a:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-static {v2}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->c(Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;)Landroid/util/SparseBooleanArray;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v2

    .line 2769
    iget-object v3, p0, Lljz;->a:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-static {v3}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;)I

    move-result v3

    if-lt v2, v3, :cond_0

    iget-object v3, p0, Lljz;->a:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-static {v3}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->c(Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;)Landroid/util/SparseBooleanArray;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2770
    iget-object v3, p0, Lljz;->a:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-virtual {v3, v2}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->j(I)V

    .line 2767
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 2776
    :cond_1
    iget-object v0, p0, Lljz;->a:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-static {v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->d(Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;)Llkf;

    move-result-object v0

    invoke-virtual {v0}, Llkf;->b()V

    .line 2778
    iget-object v0, p0, Lljz;->a:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-static {v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;)I

    move-result v0

    if-nez v0, :cond_3

    .line 2779
    iget-object v0, p0, Lljz;->a:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-static {v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->e(Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;)V

    .line 2796
    :cond_2
    iget-object v0, p0, Lljz;->a:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->requestLayout()V

    .line 2797
    return-void

    .line 2780
    :cond_3
    iget-object v0, p0, Lljz;->a:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-static {v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->f(Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lljz;->a:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-static {v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;)I

    move-result v0

    if-ge v0, v1, :cond_2

    .line 2782
    :cond_4
    iget-object v0, p0, Lljz;->a:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-static {v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->g(Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;)Lgu;

    move-result-object v0

    invoke-virtual {v0}, Lgu;->c()V

    .line 2783
    iget-object v0, p0, Lljz;->a:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-static {v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->h(Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;)V

    .line 2785
    iget-object v0, p0, Lljz;->a:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-static {v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->i(Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;)[I

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2787
    iget-object v0, p0, Lljz;->a:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-static {v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->j(Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;)I

    move-result v1

    .line 2788
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_2

    .line 2789
    iget-object v2, p0, Lljz;->a:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-static {v2}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->k(Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;)[I

    move-result-object v2

    iget-object v3, p0, Lljz;->a:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-static {v3}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->i(Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;)[I

    move-result-object v3

    aget v3, v3, v0

    aput v3, v2, v0

    .line 2788
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public onInvalidated()V
    .locals 0

    .prologue
    .line 2801
    return-void
.end method
