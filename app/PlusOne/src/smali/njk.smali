.class public final Lnjk;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnja;

.field public b:[Lnjj;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3949
    invoke-direct {p0}, Loxq;-><init>()V

    .line 3952
    const/4 v0, 0x0

    iput-object v0, p0, Lnjk;->a:Lnja;

    .line 3955
    sget-object v0, Lnjj;->a:[Lnjj;

    iput-object v0, p0, Lnjk;->b:[Lnjj;

    .line 3949
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 3976
    .line 3977
    iget-object v0, p0, Lnjk;->a:Lnja;

    if-eqz v0, :cond_2

    .line 3978
    const/4 v0, 0x1

    iget-object v2, p0, Lnjk;->a:Lnja;

    .line 3979
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3981
    :goto_0
    iget-object v2, p0, Lnjk;->b:[Lnjj;

    if-eqz v2, :cond_1

    .line 3982
    iget-object v2, p0, Lnjk;->b:[Lnjj;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 3983
    if-eqz v4, :cond_0

    .line 3984
    const/4 v5, 0x2

    .line 3985
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 3982
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 3989
    :cond_1
    iget-object v1, p0, Lnjk;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3990
    iput v0, p0, Lnjk;->ai:I

    .line 3991
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lnjk;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3999
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 4000
    sparse-switch v0, :sswitch_data_0

    .line 4004
    iget-object v2, p0, Lnjk;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 4005
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnjk;->ah:Ljava/util/List;

    .line 4008
    :cond_1
    iget-object v2, p0, Lnjk;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4010
    :sswitch_0
    return-object p0

    .line 4015
    :sswitch_1
    iget-object v0, p0, Lnjk;->a:Lnja;

    if-nez v0, :cond_2

    .line 4016
    new-instance v0, Lnja;

    invoke-direct {v0}, Lnja;-><init>()V

    iput-object v0, p0, Lnjk;->a:Lnja;

    .line 4018
    :cond_2
    iget-object v0, p0, Lnjk;->a:Lnja;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 4022
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 4023
    iget-object v0, p0, Lnjk;->b:[Lnjj;

    if-nez v0, :cond_4

    move v0, v1

    .line 4024
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnjj;

    .line 4025
    iget-object v3, p0, Lnjk;->b:[Lnjj;

    if-eqz v3, :cond_3

    .line 4026
    iget-object v3, p0, Lnjk;->b:[Lnjj;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4028
    :cond_3
    iput-object v2, p0, Lnjk;->b:[Lnjj;

    .line 4029
    :goto_2
    iget-object v2, p0, Lnjk;->b:[Lnjj;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 4030
    iget-object v2, p0, Lnjk;->b:[Lnjj;

    new-instance v3, Lnjj;

    invoke-direct {v3}, Lnjj;-><init>()V

    aput-object v3, v2, v0

    .line 4031
    iget-object v2, p0, Lnjk;->b:[Lnjj;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 4032
    invoke-virtual {p1}, Loxn;->a()I

    .line 4029
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 4023
    :cond_4
    iget-object v0, p0, Lnjk;->b:[Lnjj;

    array-length v0, v0

    goto :goto_1

    .line 4035
    :cond_5
    iget-object v2, p0, Lnjk;->b:[Lnjj;

    new-instance v3, Lnjj;

    invoke-direct {v3}, Lnjj;-><init>()V

    aput-object v3, v2, v0

    .line 4036
    iget-object v2, p0, Lnjk;->b:[Lnjj;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 4000
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 3960
    iget-object v0, p0, Lnjk;->a:Lnja;

    if-eqz v0, :cond_0

    .line 3961
    const/4 v0, 0x1

    iget-object v1, p0, Lnjk;->a:Lnja;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 3963
    :cond_0
    iget-object v0, p0, Lnjk;->b:[Lnjj;

    if-eqz v0, :cond_2

    .line 3964
    iget-object v1, p0, Lnjk;->b:[Lnjj;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 3965
    if-eqz v3, :cond_1

    .line 3966
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 3964
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3970
    :cond_2
    iget-object v0, p0, Lnjk;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3972
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3945
    invoke-virtual {p0, p1}, Lnjk;->a(Loxn;)Lnjk;

    move-result-object v0

    return-object v0
.end method
