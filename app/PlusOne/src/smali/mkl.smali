.class public final Lmkl;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnnv;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 12958
    invoke-direct {p0}, Loxq;-><init>()V

    .line 12961
    iput-object v0, p0, Lmkl;->apiHeader:Llyr;

    .line 12964
    iput-object v0, p0, Lmkl;->a:Lnnv;

    .line 12958
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 12981
    const/4 v0, 0x0

    .line 12982
    iget-object v1, p0, Lmkl;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 12983
    const/4 v0, 0x1

    iget-object v1, p0, Lmkl;->apiHeader:Llyr;

    .line 12984
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 12986
    :cond_0
    iget-object v1, p0, Lmkl;->a:Lnnv;

    if-eqz v1, :cond_1

    .line 12987
    const/4 v1, 0x2

    iget-object v2, p0, Lmkl;->a:Lnnv;

    .line 12988
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12990
    :cond_1
    iget-object v1, p0, Lmkl;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12991
    iput v0, p0, Lmkl;->ai:I

    .line 12992
    return v0
.end method

.method public a(Loxn;)Lmkl;
    .locals 2

    .prologue
    .line 13000
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 13001
    sparse-switch v0, :sswitch_data_0

    .line 13005
    iget-object v1, p0, Lmkl;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 13006
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmkl;->ah:Ljava/util/List;

    .line 13009
    :cond_1
    iget-object v1, p0, Lmkl;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 13011
    :sswitch_0
    return-object p0

    .line 13016
    :sswitch_1
    iget-object v0, p0, Lmkl;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 13017
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmkl;->apiHeader:Llyr;

    .line 13019
    :cond_2
    iget-object v0, p0, Lmkl;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 13023
    :sswitch_2
    iget-object v0, p0, Lmkl;->a:Lnnv;

    if-nez v0, :cond_3

    .line 13024
    new-instance v0, Lnnv;

    invoke-direct {v0}, Lnnv;-><init>()V

    iput-object v0, p0, Lmkl;->a:Lnnv;

    .line 13026
    :cond_3
    iget-object v0, p0, Lmkl;->a:Lnnv;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 13001
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 12969
    iget-object v0, p0, Lmkl;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 12970
    const/4 v0, 0x1

    iget-object v1, p0, Lmkl;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 12972
    :cond_0
    iget-object v0, p0, Lmkl;->a:Lnnv;

    if-eqz v0, :cond_1

    .line 12973
    const/4 v0, 0x2

    iget-object v1, p0, Lmkl;->a:Lnnv;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 12975
    :cond_1
    iget-object v0, p0, Lmkl;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 12977
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 12954
    invoke-virtual {p0, p1}, Lmkl;->a(Loxn;)Lmkl;

    move-result-object v0

    return-object v0
.end method
