.class public Ldbs;
.super Llol;
.source "PG"

# interfaces
.implements Ldck;
.implements Ldcl;
.implements Llgs;


# static fields
.field private static N:Z

.field private static Q:I

.field private static R:I

.field private static S:I

.field private static T:I


# instance fields
.field private O:Landroid/graphics/drawable/Drawable;

.field private P:Landroid/graphics/drawable/Drawable;

.field private U:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ldbp;",
            ">;"
        }
    .end annotation
.end field

.field private V:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ldbp;",
            ">;"
        }
    .end annotation
.end field

.field private W:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Llip;",
            ">;"
        }
    .end annotation
.end field

.field private X:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Llip;",
            ">;"
        }
    .end annotation
.end field

.field private Y:Llip;

.field private Z:Landroid/widget/AutoCompleteTextView;

.field private aA:Z

.field private final aB:[I

.field private aC:Landroid/graphics/Rect;

.field private final aD:Ldci;

.field private final aE:Ldce;

.field private final aF:Ldcb;

.field private final aG:Ldcj;

.field private final aH:Ldcf;

.field private final aI:Ldgl;

.field private aa:Landroid/widget/ImageView;

.field private ab:Landroid/widget/ImageButton;

.field private ac:Ldbp;

.field private ad:I

.field private ae:Z

.field private af:I

.field private ag:I

.field private ah:Z

.field private ai:Z

.field private final aj:Landroid/text/TextWatcher;

.field private final ak:Landroid/graphics/Rect;

.field private final al:Ldcg;

.field private am:Ldgi;

.field private an:Ldgr;

.field private ao:Ldcm;

.field private ap:Ldeo;

.field private aq:Ldef;

.field private ar:Ldes;

.field private as:Ljava/lang/Integer;

.field private aw:Ljava/lang/Long;

.field private ax:Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;

.field private ay:I

.field private az:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 88
    invoke-direct {p0}, Llol;-><init>()V

    .line 186
    new-instance v0, Ldbt;

    invoke-direct {v0, p0}, Ldbt;-><init>(Ldbs;)V

    iput-object v0, p0, Ldbs;->aj:Landroid/text/TextWatcher;

    .line 658
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Ldbs;->ak:Landroid/graphics/Rect;

    .line 660
    new-instance v0, Ldch;

    invoke-direct {v0, p0}, Ldch;-><init>(Ldbs;)V

    iput-object v0, p0, Ldbs;->al:Ldcg;

    .line 677
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Ldbs;->aB:[I

    .line 678
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Ldbs;->aC:Landroid/graphics/Rect;

    .line 680
    new-instance v0, Ldci;

    invoke-direct {v0, p0}, Ldci;-><init>(Ldbs;)V

    iput-object v0, p0, Ldbs;->aD:Ldci;

    .line 681
    new-instance v0, Ldce;

    invoke-direct {v0, p0}, Ldce;-><init>(Ldbs;)V

    iput-object v0, p0, Ldbs;->aE:Ldce;

    .line 682
    new-instance v0, Ldcb;

    invoke-direct {v0, p0}, Ldcb;-><init>(Ldbs;)V

    iput-object v0, p0, Ldbs;->aF:Ldcb;

    .line 683
    new-instance v0, Ldcj;

    invoke-direct {v0, p0}, Ldcj;-><init>(Ldbs;)V

    iput-object v0, p0, Ldbs;->aG:Ldcj;

    .line 684
    new-instance v0, Ldcf;

    invoke-direct {v0, p0}, Ldcf;-><init>(Ldbs;)V

    iput-object v0, p0, Ldbs;->aH:Ldcf;

    .line 685
    new-instance v0, Ldcc;

    invoke-direct {v0, p0}, Ldcc;-><init>(Ldbs;)V

    iput-object v0, p0, Ldbs;->aI:Ldgl;

    .line 688
    new-instance v0, Ldgn;

    iget-object v1, p0, Ldbs;->av:Llqm;

    sget-object v2, Ldgx;->P:Ldgo;

    new-instance v3, Ldbv;

    invoke-direct {v3, p0}, Ldbv;-><init>(Ldbs;)V

    invoke-direct {v0, v1, v2, v3}, Ldgn;-><init>(Llqr;Ldgu;Ldgv;)V

    .line 694
    new-instance v0, Ldep;

    iget-object v1, p0, Ldbs;->av:Llqm;

    new-instance v2, Ldbw;

    invoke-direct {v2, p0}, Ldbw;-><init>(Ldbs;)V

    invoke-direct {v0, v1, v2}, Ldep;-><init>(Llqr;Ldeq;)V

    .line 700
    new-instance v0, Ldeg;

    iget-object v1, p0, Ldbs;->av:Llqm;

    new-instance v2, Ldbx;

    invoke-direct {v2, p0}, Ldbx;-><init>(Ldbs;)V

    invoke-direct {v0, v1, v2}, Ldeg;-><init>(Llqr;Ldeh;)V

    .line 1542
    return-void
.end method

.method static synthetic A(Ldbs;)Llnh;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Ldbs;->au:Llnh;

    return-object v0
.end method

.method static synthetic B(Ldbs;)Ldes;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Ldbs;->ar:Ldes;

    return-object v0
.end method

.method private V()Z
    .locals 1

    .prologue
    .line 402
    iget v0, p0, Ldbs;->af:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private W()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 407
    iget v1, p0, Ldbs;->af:I

    if-ne v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private X()Z
    .locals 2

    .prologue
    .line 412
    const/4 v0, 0x2

    iget v1, p0, Ldbs;->af:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private Y()Z
    .locals 2

    .prologue
    .line 417
    const/4 v0, 0x3

    iget v1, p0, Ldbs;->af:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private Z()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 489
    iget-object v0, p0, Ldbs;->V:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldbs;->V:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 490
    iget-object v0, p0, Ldbs;->V:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbp;

    .line 491
    iput-boolean v2, p0, Ldbs;->ae:Z

    .line 492
    invoke-virtual {v0, v2}, Ldbp;->b(Z)V

    .line 495
    :cond_0
    iput-boolean v1, p0, Ldbs;->ah:Z

    .line 496
    invoke-direct {p0}, Ldbs;->aa()V

    .line 497
    return-void
.end method

.method static synthetic a(Ldbs;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Ldbs;->ab:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic a(Ldbs;Ldbp;)Ldbp;
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Ldbs;->ac:Ldbp;

    return-object p1
.end method

.method static synthetic a(Ldbs;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Ldbs;->as:Ljava/lang/Integer;

    return-object p1
.end method

.method static synthetic a(Ldbs;Ljava/lang/Long;)Ljava/lang/Long;
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Ldbs;->aw:Ljava/lang/Long;

    return-object p1
.end method

.method static synthetic a(Ldbs;Llip;)Llip;
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Ldbs;->Y:Llip;

    return-object p1
.end method

.method static synthetic a(Ldbs;I)V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0, p1}, Ldbs;->c(I)V

    return-void
.end method

.method static synthetic a(Ldbs;Lhmv;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 88
    const/4 v0, 0x0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "extra_gaia_id"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v0

    :goto_0
    iget-object v0, p0, Ldbs;->au:Llnh;

    const-class v2, Lhms;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    iget-object v3, p0, Ldbs;->at:Llnl;

    iget v4, p0, Ldbs;->ay:I

    invoke-direct {v2, v3, v4}, Lhmr;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v2, p1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v2

    sget-object v3, Lhmw;->ai:Lhmw;

    invoke-virtual {v2, v3}, Lhmr;->a(Lhmw;)Lhmr;

    move-result-object v2

    invoke-virtual {v2, v1}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    return-void

    :cond_0
    move-object v1, v0

    goto :goto_0
.end method

.method static synthetic a(Ldbs;Z)Z
    .locals 0

    .prologue
    .line 88
    iput-boolean p1, p0, Ldbs;->aA:Z

    return p1
.end method

.method private aa()V
    .locals 14

    .prologue
    .line 913
    iget-object v0, p0, Ldbs;->ax:Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;

    if-nez v0, :cond_0

    .line 1035
    :goto_0
    return-void

    .line 917
    :cond_0
    iget-object v0, p0, Ldbs;->Y:Llip;

    if-eqz v0, :cond_1

    .line 920
    iget-object v0, p0, Ldbs;->Y:Llip;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Llip;->a(Z)V

    .line 924
    :cond_1
    iget-object v0, p0, Ldbs;->W:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 925
    iget-object v0, p0, Ldbs;->X:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 926
    iget-object v0, p0, Ldbs;->U:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v8

    .line 927
    const/4 v0, 0x0

    move v7, v0

    :goto_1
    if-ge v7, v8, :cond_9

    .line 928
    iget-object v0, p0, Ldbs;->U:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbp;

    .line 929
    iget-object v1, p0, Ldbs;->W:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 930
    iget-object v1, p0, Ldbs;->X:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 931
    invoke-virtual {v0}, Ldbp;->i()Lnyp;

    move-result-object v2

    .line 933
    iget-object v1, p0, Ldbs;->ak:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    .line 934
    iget-object v3, p0, Ldbs;->ak:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    int-to-float v3, v3

    .line 935
    iget-object v4, v2, Lnyp;->a:Ljava/lang/Double;

    invoke-virtual {v4}, Ljava/lang/Double;->floatValue()F

    move-result v4

    mul-float/2addr v4, v1

    iget-object v5, p0, Ldbs;->ak:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    int-to-float v5, v5

    add-float/2addr v4, v5

    .line 936
    iget-object v5, v2, Lnyp;->b:Ljava/lang/Double;

    invoke-virtual {v5}, Ljava/lang/Double;->floatValue()F

    move-result v5

    mul-float/2addr v1, v5

    iget-object v5, p0, Ldbs;->ak:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    int-to-float v5, v5

    add-float/2addr v1, v5

    .line 938
    sub-float/2addr v1, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v1, v5

    add-float/2addr v1, v4

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 939
    iget-object v4, v2, Lnyp;->c:Ljava/lang/Double;

    invoke-virtual {v4}, Ljava/lang/Double;->floatValue()F

    move-result v4

    mul-float/2addr v4, v3

    iget-object v5, p0, Ldbs;->ak:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    int-to-float v5, v5

    add-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v5

    .line 940
    iget-object v2, v2, Lnyp;->d:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->floatValue()F

    move-result v2

    mul-float/2addr v2, v3

    iget-object v3, p0, Ldbs;->ak:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    add-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 941
    iget-object v2, p0, Ldbs;->ax:Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;->getMeasuredHeight()I

    move-result v2

    iget v4, p0, Ldbs;->ad:I

    mul-int/lit8 v4, v4, 0x2

    sub-int/2addr v2, v4

    .line 942
    invoke-virtual {v0}, Ldbp;->d()I

    move-result v4

    sub-int v4, v2, v4

    .line 944
    const/4 v2, 0x0

    .line 945
    if-le v3, v4, :cond_12

    invoke-direct {p0}, Ldbs;->Y()Z

    move-result v4

    if-nez v4, :cond_12

    .line 947
    const/4 v4, 0x1

    .line 948
    sub-int v6, v3, v5

    .line 950
    iget-object v2, p0, Ldbs;->aq:Ldef;

    invoke-virtual {v2}, Ldef;->a()F

    move-result v2

    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v2, v2, v3

    if-lez v2, :cond_3

    .line 951
    invoke-virtual {v0}, Ldbp;->l()I

    move-result v2

    .line 971
    :goto_2
    add-int/2addr v2, v5

    move v6, v4

    .line 974
    :goto_3
    iget-object v3, p0, Ldbs;->ak:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 975
    iget-object v4, p0, Ldbs;->ak:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    int-to-float v4, v4

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    .line 976
    if-eqz v6, :cond_6

    .line 977
    sget v5, Ldbs;->S:I

    invoke-virtual/range {v0 .. v5}, Ldbp;->a(IIIII)V

    .line 984
    :goto_4
    invoke-virtual {v0}, Ldbp;->e()I

    move-result v1

    invoke-static {v1}, Lfve;->d(I)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-direct {p0}, Ldbs;->Y()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 985
    iget-object v2, p0, Ldbs;->aa:Landroid/widget/ImageView;

    if-eqz v6, :cond_7

    iget-object v1, p0, Ldbs;->P:Landroid/graphics/drawable/Drawable;

    :goto_5
    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 987
    :cond_2
    invoke-virtual {v0}, Ldbp;->e()I

    move-result v1

    invoke-static {v1}, Lfve;->c(I)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 988
    iget-object v1, p0, Ldbs;->X:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 927
    :goto_6
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto/16 :goto_1

    .line 953
    :cond_3
    int-to-float v2, v6

    iget-object v3, p0, Ldbs;->ax:Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;

    invoke-virtual {v3}, Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;->getMeasuredHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    .line 960
    float-to-double v10, v2

    const-wide v12, 0x3feb333333333333L    # 0.85

    cmpl-double v3, v10, v12

    if-lez v3, :cond_4

    .line 961
    const-wide v2, 0x3fd999999999999aL    # 0.4

    .line 967
    :goto_7
    int-to-double v10, v6

    mul-double/2addr v2, v10

    double-to-int v2, v2

    .line 968
    invoke-virtual {v0, v2}, Ldbp;->a(I)V

    goto :goto_2

    .line 962
    :cond_4
    float-to-double v2, v2

    const-wide v10, 0x3fe3333333333333L    # 0.6

    cmpl-double v2, v2, v10

    if-lez v2, :cond_5

    .line 963
    const-wide/high16 v2, 0x3fd0000000000000L    # 0.25

    goto :goto_7

    .line 965
    :cond_5
    const-wide/16 v2, 0x0

    goto :goto_7

    .line 980
    :cond_6
    sget v5, Ldbs;->S:I

    invoke-virtual/range {v0 .. v5}, Ldbp;->b(IIIII)V

    goto :goto_4

    .line 985
    :cond_7
    iget-object v1, p0, Ldbs;->O:Landroid/graphics/drawable/Drawable;

    goto :goto_5

    .line 990
    :cond_8
    iget-object v1, p0, Ldbs;->W:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 995
    :cond_9
    new-instance v5, Landroid/util/SparseBooleanArray;

    invoke-direct {v5, v8}, Landroid/util/SparseBooleanArray;-><init>(I)V

    .line 997
    const/4 v2, 0x0

    .line 998
    const/4 v0, 0x0

    move v4, v0

    :goto_8
    if-ge v4, v8, :cond_10

    .line 999
    iget-object v0, p0, Ldbs;->U:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbp;

    .line 1000
    iget-boolean v1, p0, Ldbs;->ah:Z

    if-nez v1, :cond_11

    iget v1, p0, Ldbs;->af:I

    const/4 v3, 0x2

    if-ne v1, v3, :cond_11

    .line 1002
    invoke-virtual {v0}, Ldbp;->e()I

    move-result v1

    invoke-static {v1}, Lfve;->d(I)Z

    move-result v1

    if-nez v1, :cond_11

    .line 1003
    invoke-virtual {v0}, Ldbp;->e()I

    move-result v1

    const/16 v3, 0x64

    if-eq v1, v3, :cond_11

    .line 1004
    const/4 v1, 0x0

    move v3, v1

    :goto_9
    if-ge v3, v8, :cond_a

    .line 1005
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ldbp;->b(Z)V

    .line 1006
    if-eq v4, v3, :cond_c

    const/4 v1, 0x0

    invoke-virtual {v5, v4, v1}, Landroid/util/SparseBooleanArray;->get(IZ)Z

    move-result v1

    if-nez v1, :cond_c

    .line 1007
    iget-object v1, p0, Ldbs;->U:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldbp;

    .line 1011
    invoke-virtual {v1}, Ldbp;->f()Z

    move-result v6

    if-eqz v6, :cond_b

    .line 1012
    invoke-virtual {v0}, Ldbp;->k()Landroid/graphics/Rect;

    move-result-object v6

    .line 1013
    invoke-virtual {v1}, Ldbp;->k()Landroid/graphics/Rect;

    move-result-object v7

    .line 1012
    invoke-static {v6, v7}, Landroid/graphics/Rect;->intersects(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 1014
    const/4 v1, 0x1

    invoke-virtual {v5, v4, v1}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 1023
    :cond_a
    :goto_a
    const/4 v1, 0x0

    invoke-virtual {v5, v4, v1}, Landroid/util/SparseBooleanArray;->get(IZ)Z

    move-result v1

    if-nez v1, :cond_d

    const/4 v1, 0x1

    :goto_b
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v1}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 1025
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ldbp;->b(Z)V

    move v1, v2

    .line 1031
    :goto_c
    if-nez v1, :cond_f

    const/4 v0, 0x1

    :goto_d
    iput-boolean v0, p0, Ldbs;->ai:Z

    .line 998
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    move v2, v1

    goto :goto_8

    .line 1016
    :cond_b
    invoke-virtual {v0}, Ldbp;->k()Landroid/graphics/Rect;

    move-result-object v6

    .line 1017
    invoke-virtual {v1}, Ldbp;->a()Landroid/graphics/Rect;

    move-result-object v1

    .line 1016
    invoke-static {v6, v1}, Landroid/graphics/Rect;->intersects(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 1018
    const/4 v1, 0x1

    invoke-virtual {v5, v4, v1}, Landroid/util/SparseBooleanArray;->put(IZ)V

    goto :goto_a

    .line 1004
    :cond_c
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_9

    .line 1023
    :cond_d
    const/4 v1, 0x0

    goto :goto_b

    .line 1027
    :cond_e
    add-int/lit8 v0, v2, 0x1

    move v1, v0

    goto :goto_c

    .line 1031
    :cond_f
    const/4 v0, 0x0

    goto :goto_d

    .line 1034
    :cond_10
    iget-object v0, p0, Ldbs;->ax:Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;->invalidate()V

    goto/16 :goto_0

    :cond_11
    move v1, v2

    goto :goto_c

    :cond_12
    move v6, v2

    move v2, v3

    goto/16 :goto_3
.end method

.method static synthetic b(Ldbs;)Z
    .locals 1

    .prologue
    .line 88
    invoke-direct {p0}, Ldbs;->Y()Z

    move-result v0

    return v0
.end method

.method static synthetic b(Ldbs;Z)Z
    .locals 0

    .prologue
    .line 88
    iput-boolean p1, p0, Ldbs;->ah:Z

    return p1
.end method

.method static synthetic c(Ldbs;)Landroid/widget/AutoCompleteTextView;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Ldbs;->Z:Landroid/widget/AutoCompleteTextView;

    return-object v0
.end method

.method private c(I)V
    .locals 10

    .prologue
    const/high16 v9, 0x40000000    # 2.0f

    const/16 v1, 0x8

    const/4 v8, 0x3

    const/4 v2, 0x0

    .line 429
    packed-switch p1, :pswitch_data_0

    .line 479
    :goto_0
    iget-object v3, p0, Ldbs;->Z:Landroid/widget/AutoCompleteTextView;

    iget v0, p0, Ldbs;->af:I

    if-eq v0, v8, :cond_7

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/widget/AutoCompleteTextView;->setVisibility(I)V

    .line 481
    iget-object v3, p0, Ldbs;->aa:Landroid/widget/ImageView;

    iget v0, p0, Ldbs;->af:I

    if-eq v0, v8, :cond_8

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 483
    iget-object v0, p0, Ldbs;->ab:Landroid/widget/ImageButton;

    iget v3, p0, Ldbs;->af:I

    if-eq v3, v8, :cond_9

    :goto_3
    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 485
    :cond_0
    return-void

    .line 431
    :pswitch_0
    invoke-direct {p0}, Ldbs;->Z()V

    .line 432
    iget-boolean v0, p0, Ldbs;->ae:Z

    if-eqz v0, :cond_1

    .line 433
    iput v2, p0, Ldbs;->af:I

    .line 434
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Ldbs;->c(I)V

    goto :goto_0

    .line 436
    :cond_1
    iget v0, p0, Ldbs;->af:I

    invoke-direct {p0, v0}, Ldbs;->d(I)I

    move-result v0

    iput v0, p0, Ldbs;->ag:I

    .line 437
    iput p1, p0, Ldbs;->af:I

    goto :goto_0

    .line 443
    :pswitch_1
    invoke-direct {p0}, Ldbs;->Z()V

    .line 444
    iget v0, p0, Ldbs;->af:I

    if-nez v0, :cond_0

    .line 447
    iget v0, p0, Ldbs;->af:I

    invoke-direct {p0, v0}, Ldbs;->d(I)I

    move-result v0

    iput v0, p0, Ldbs;->ag:I

    .line 448
    iput p1, p0, Ldbs;->af:I

    goto :goto_0

    .line 453
    :pswitch_2
    iget v0, p0, Ldbs;->af:I

    if-ne v0, v8, :cond_2

    .line 454
    iget-object v0, p0, Ldbs;->al:Ldcg;

    invoke-interface {v0}, Ldcg;->b()V

    .line 456
    :cond_2
    iget v0, p0, Ldbs;->af:I

    invoke-direct {p0, v0}, Ldbs;->d(I)I

    move-result v0

    iput v0, p0, Ldbs;->ag:I

    .line 457
    iput p1, p0, Ldbs;->af:I

    .line 460
    invoke-direct {p0}, Ldbs;->Z()V

    goto :goto_0

    .line 465
    :pswitch_3
    iget v0, p0, Ldbs;->af:I

    if-eq v0, v8, :cond_0

    iget v0, p0, Ldbs;->af:I

    if-eqz v0, :cond_0

    .line 470
    iget v0, p0, Ldbs;->af:I

    invoke-direct {p0, v0}, Ldbs;->d(I)I

    move-result v0

    iput v0, p0, Ldbs;->ag:I

    .line 471
    iput p1, p0, Ldbs;->af:I

    .line 472
    iget-object v0, p0, Ldbs;->ax:Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;

    iget-object v3, p0, Ldbs;->Z:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;->removeView(Landroid/view/View;)V

    iget-object v0, p0, Ldbs;->ax:Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;

    iget-object v3, p0, Ldbs;->aa:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;->removeView(Landroid/view/View;)V

    iget-object v0, p0, Ldbs;->ax:Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;

    iget-object v3, p0, Ldbs;->ab:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;->removeView(Landroid/view/View;)V

    iget-object v0, p0, Ldbs;->ax:Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;

    iget-object v3, p0, Ldbs;->Z:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Ldbs;->ax:Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;

    iget-object v3, p0, Ldbs;->aa:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Ldbs;->ac:Ldbp;

    invoke-virtual {v0}, Ldbp;->h()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Ldbs;->ax:Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;

    iget-object v3, p0, Ldbs;->ab:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Ldbs;->ab:Landroid/widget/ImageButton;

    const v3, 0x7f10008e

    iget-object v4, p0, Ldbs;->ac:Ldbp;

    invoke-virtual {v4}, Ldbp;->b()Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/widget/ImageButton;->setTag(ILjava/lang/Object;)V

    :cond_3
    new-instance v4, Ljava/util/ArrayList;

    iget-object v0, p0, Ldbs;->U:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v0, p0, Ldbs;->U:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_4
    if-ltz v3, :cond_6

    iget-object v0, p0, Ldbs;->U:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbp;

    iget-object v5, p0, Ldbs;->ac:Ldbp;

    if-eq v5, v0, :cond_4

    invoke-virtual {v0, v2}, Ldbp;->b(Z)V

    :cond_4
    invoke-virtual {v0}, Ldbp;->c()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_5

    invoke-virtual {v0}, Ldbp;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_5
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_4

    :cond_6
    iget-object v0, p0, Ldbs;->Z:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lely;

    invoke-virtual {v0, v4}, Lely;->a(Ljava/util/Collection;)V

    iget-object v0, p0, Ldbs;->ac:Ldbp;

    invoke-virtual {v0}, Ldbp;->i()Lnyp;

    move-result-object v0

    iget-object v3, p0, Ldbs;->ak:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Ldbs;->ak:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    int-to-float v4, v4

    iget-object v5, v0, Lnyp;->a:Ljava/lang/Double;

    invoke-virtual {v5}, Ljava/lang/Double;->floatValue()F

    move-result v5

    mul-float/2addr v5, v3

    iget-object v6, p0, Ldbs;->ak:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->left:I

    int-to-float v6, v6

    add-float/2addr v5, v6

    iget-object v6, v0, Lnyp;->b:Ljava/lang/Double;

    invoke-virtual {v6}, Ljava/lang/Double;->floatValue()F

    move-result v6

    mul-float/2addr v3, v6

    iget-object v6, p0, Ldbs;->ak:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->left:I

    int-to-float v6, v6

    add-float/2addr v3, v6

    iget-object v6, v0, Lnyp;->c:Ljava/lang/Double;

    invoke-virtual {v6}, Ljava/lang/Double;->floatValue()F

    move-result v6

    mul-float/2addr v6, v4

    iget-object v7, p0, Ldbs;->ak:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->top:I

    int-to-float v7, v7

    add-float/2addr v6, v7

    iget-object v0, v0, Lnyp;->d:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->floatValue()F

    move-result v0

    mul-float/2addr v0, v4

    iget-object v4, p0, Ldbs;->ak:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    int-to-float v4, v4

    add-float/2addr v0, v4

    sub-float/2addr v3, v5

    div-float/2addr v3, v9

    add-float/2addr v3, v5

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    sub-float/2addr v0, v6

    div-float/2addr v0, v9

    add-float/2addr v0, v6

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iget-object v4, p0, Ldbs;->ac:Ldbp;

    invoke-virtual {v4, v3, v0}, Ldbp;->a(II)V

    iget-object v0, p0, Ldbs;->Z:Landroid/widget/AutoCompleteTextView;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Ldbs;->Z:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, v2}, Landroid/widget/AutoCompleteTextView;->setVisibility(I)V

    invoke-static {}, Llsx;->d()Landroid/os/Handler;

    move-result-object v0

    new-instance v3, Ldbu;

    invoke-direct {v3, p0}, Ldbu;-><init>(Ldbs;)V

    const-wide/16 v4, 0x96

    invoke-virtual {v0, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    iget-object v0, p0, Ldbs;->au:Llnh;

    const-class v3, Lhms;

    invoke-virtual {v0, v3}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v3, Lhmr;

    iget-object v4, p0, Ldbs;->at:Llnl;

    iget v5, p0, Ldbs;->ay:I

    invoke-direct {v3, v4, v5}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v4, Lhmw;->ah:Lhmw;

    invoke-virtual {v3, v4}, Lhmr;->a(Lhmw;)Lhmr;

    move-result-object v3

    sget-object v4, Lhmw;->ai:Lhmw;

    invoke-virtual {v3, v4}, Lhmr;->b(Lhmw;)Lhmr;

    move-result-object v3

    invoke-interface {v0, v3}, Lhms;->a(Lhmr;)V

    iget-object v0, p0, Ldbs;->ax:Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;->requestLayout()V

    iget-object v0, p0, Ldbs;->ax:Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;->invalidate()V

    .line 473
    iget-object v0, p0, Ldbs;->al:Ldcg;

    .line 474
    iget-object v0, p0, Ldbs;->al:Ldcg;

    invoke-interface {v0}, Ldcg;->a()V

    goto/16 :goto_0

    :cond_7
    move v0, v2

    .line 479
    goto/16 :goto_1

    :cond_8
    move v0, v2

    .line 481
    goto/16 :goto_2

    :cond_9
    move v1, v2

    .line 483
    goto/16 :goto_3

    .line 429
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private d(I)I
    .locals 1

    .prologue
    .line 612
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    const/4 p1, 0x0

    :cond_0
    return p1
.end method

.method static synthetic d(Ldbs;)Ldcm;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Ldbs;->ao:Ldcm;

    return-object v0
.end method

.method static synthetic e(Ldbs;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 88
    iget-object v0, p0, Ldbs;->ax:Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ldbs;->ap:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    invoke-interface {v0}, Lddl;->z()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Ldbs;->ap:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    invoke-interface {v0}, Lddl;->g()Lnym;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Ldbs;->an:Ldgr;

    sget-object v1, Ldgx;->P:Ldgo;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ldgr;->a(Ldgu;Z)V

    iget-object v0, p0, Ldbs;->ap:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    invoke-interface {v0}, Lddl;->g()Lnym;

    move-result-object v0

    iget-object v0, v0, Lnym;->g:[Lnys;

    iget-object v1, p0, Ldbs;->az:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Ldbs;->a([Lnys;Ljava/lang/String;)I

    move-result v0

    iget-object v1, p0, Ldbs;->ar:Ldes;

    invoke-virtual {v1, v0}, Ldes;->a(I)V

    iget-object v0, p0, Ldbs;->ao:Ldcm;

    invoke-interface {v0}, Ldcm;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Ldbs;->c()V

    :goto_0
    iget-object v0, p0, Ldbs;->aw:Ljava/lang/Long;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldbs;->aw:Ljava/lang/Long;

    invoke-virtual {p0, v0}, Ldbs;->a(Ljava/lang/Long;)Ldbp;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ldbp;->e()I

    move-result v1

    invoke-static {v1}, Lfve;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ldbp;->f()Z

    move-result v0

    if-nez v0, :cond_0

    iput-object v3, p0, Ldbs;->aw:Ljava/lang/Long;

    :cond_0
    :goto_1
    return-void

    :cond_1
    invoke-virtual {p0}, Ldbs;->d()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Ldbs;->an:Ldgr;

    sget-object v1, Ldgx;->P:Ldgo;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ldgr;->a(Ldgu;Z)V

    iget-object v0, p0, Ldbs;->ax:Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldbs;->az:Ljava/lang/String;

    invoke-virtual {p0, v3, v0}, Ldbs;->a([Lnys;Ljava/lang/String;)I

    goto :goto_1
.end method

.method static synthetic f(Ldbs;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 88
    iget-object v2, p0, Ldbs;->ax:Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;

    if-eqz v2, :cond_1

    iget-object v2, p0, Ldbs;->aq:Ldef;

    iget-object v3, p0, Ldbs;->aC:Landroid/graphics/Rect;

    invoke-virtual {v2, v3}, Ldef;->a(Landroid/graphics/Rect;)V

    iget-object v2, p0, Ldbs;->ax:Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;

    iget-object v3, p0, Ldbs;->aC:Landroid/graphics/Rect;

    iget-object v4, p0, Ldbs;->aB:[I

    invoke-virtual {v2, v4}, Landroid/view/View;->getLocationInWindow([I)V

    iget-object v2, p0, Ldbs;->aB:[I

    aget v2, v2, v1

    neg-int v2, v2

    iget-object v4, p0, Ldbs;->aB:[I

    aget v4, v4, v0

    neg-int v4, v4

    invoke-virtual {v3, v2, v4}, Landroid/graphics/Rect;->offset(II)V

    iget-object v2, p0, Ldbs;->ax:Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;

    iget-object v3, p0, Ldbs;->aq:Ldef;

    invoke-virtual {v3}, Ldef;->d()I

    move-result v3

    iget-object v4, p0, Ldbs;->aB:[I

    invoke-virtual {v2, v4}, Landroid/view/View;->getLocationInWindow([I)V

    iget-object v4, p0, Ldbs;->aB:[I

    aget v4, v4, v0

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    add-int/2addr v2, v4

    sub-int/2addr v2, v3

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v2

    iget-object v3, p0, Ldbs;->aC:Landroid/graphics/Rect;

    iget-object v4, p0, Ldbs;->ak:Landroid/graphics/Rect;

    invoke-virtual {v3, v4}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget v3, p0, Ldbs;->ad:I

    if-eq v2, v3, :cond_2

    :cond_0
    :goto_0
    iput v2, p0, Ldbs;->ad:I

    iget-object v2, p0, Ldbs;->ak:Landroid/graphics/Rect;

    iget-object v3, p0, Ldbs;->aC:Landroid/graphics/Rect;

    invoke-virtual {v2, v3}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    iput-boolean v1, p0, Ldbs;->ah:Z

    invoke-direct {p0}, Ldbs;->aa()V

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldbs;->ax:Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;->requestLayout()V

    :cond_1
    return-void

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method static synthetic g(Ldbs;)Ldcg;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Ldbs;->al:Ldcg;

    return-object v0
.end method

.method static synthetic h(Ldbs;)Ldgi;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Ldbs;->am:Ldgi;

    return-object v0
.end method

.method static synthetic i(Ldbs;)Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Ldbs;->ax:Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;

    return-object v0
.end method

.method static synthetic j(Ldbs;)V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0}, Ldbs;->aa()V

    return-void
.end method

.method static synthetic k(Ldbs;)Ldgr;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Ldbs;->an:Ldgr;

    return-object v0
.end method

.method static synthetic l(Ldbs;)V
    .locals 4

    .prologue
    .line 88
    invoke-static {}, Llsx;->d()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Ldca;

    invoke-direct {v1, p0}, Ldca;-><init>(Ldbs;)V

    const-wide/16 v2, 0x96

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method static synthetic m(Ldbs;)Ldeo;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Ldbs;->ap:Ldeo;

    return-object v0
.end method

.method static synthetic n(Ldbs;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Ldbs;->az:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic o(Ldbs;)Ldcf;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Ldbs;->aH:Ldcf;

    return-object v0
.end method

.method static synthetic p(Ldbs;)I
    .locals 1

    .prologue
    .line 88
    iget v0, p0, Ldbs;->ay:I

    return v0
.end method

.method static synthetic q(Ldbs;)Z
    .locals 1

    .prologue
    .line 88
    invoke-direct {p0}, Ldbs;->W()Z

    move-result v0

    return v0
.end method

.method static synthetic r(Ldbs;)Ljava/util/List;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Ldbs;->U:Ljava/util/List;

    return-object v0
.end method

.method static synthetic s(Ldbs;)Z
    .locals 1

    .prologue
    .line 88
    invoke-direct {p0}, Ldbs;->V()Z

    move-result v0

    return v0
.end method

.method static synthetic t(Ldbs;)Ljava/util/List;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Ldbs;->X:Ljava/util/List;

    return-object v0
.end method

.method static synthetic u(Ldbs;)Ljava/util/List;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Ldbs;->W:Ljava/util/List;

    return-object v0
.end method

.method static synthetic v(Ldbs;)Llip;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Ldbs;->Y:Llip;

    return-object v0
.end method

.method static synthetic w(Ldbs;)Z
    .locals 1

    .prologue
    .line 88
    invoke-direct {p0}, Ldbs;->X()Z

    move-result v0

    return v0
.end method

.method static synthetic x(Ldbs;)Z
    .locals 1

    .prologue
    .line 88
    iget-boolean v0, p0, Ldbs;->ai:Z

    return v0
.end method

.method static synthetic y(Ldbs;)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Ldbs;->as:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic z(Ldbs;)Llnl;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Ldbs;->at:Llnl;

    return-object v0
.end method


# virtual methods
.method public U()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 572
    const/4 v0, 0x3

    iget v1, p0, Ldbs;->af:I

    if-eq v0, v1, :cond_0

    .line 608
    :goto_0
    return-void

    .line 576
    :cond_0
    iget-object v0, p0, Ldbs;->ax:Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;

    iget-object v1, p0, Ldbs;->Z:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;->removeView(Landroid/view/View;)V

    .line 577
    iget-object v0, p0, Ldbs;->ax:Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;

    iget-object v1, p0, Ldbs;->aa:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;->removeView(Landroid/view/View;)V

    .line 578
    iget-object v0, p0, Ldbs;->ax:Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;

    iget-object v1, p0, Ldbs;->ab:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;->removeView(Landroid/view/View;)V

    .line 579
    iget-object v0, p0, Ldbs;->ac:Ldbp;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ldbp;->b(Z)V

    .line 580
    iget-object v0, p0, Ldbs;->Z:Landroid/widget/AutoCompleteTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setVisibility(I)V

    .line 582
    invoke-direct {p0}, Ldbs;->Z()V

    .line 583
    iget-object v0, p0, Ldbs;->U:Ljava/util/List;

    iget-object v1, p0, Ldbs;->ac:Ldbp;

    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 584
    if-ltz v0, :cond_1

    .line 585
    iget-object v1, p0, Ldbs;->U:Ljava/util/List;

    iget-object v2, p0, Ldbs;->U:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    iget-object v3, p0, Ldbs;->U:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 587
    :cond_1
    iput-object v4, p0, Ldbs;->ac:Ldbp;

    .line 588
    iget v0, p0, Ldbs;->ag:I

    invoke-direct {p0, v0}, Ldbs;->c(I)V

    .line 590
    iget-object v0, p0, Ldbs;->al:Ldcg;

    .line 591
    iget-object v0, p0, Ldbs;->al:Ldcg;

    invoke-interface {v0}, Ldcg;->b()V

    .line 594
    iget-object v0, p0, Ldbs;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Ldbs;->at:Llnl;

    iget v3, p0, Ldbs;->ay:I

    invoke-direct {v1, v2, v3}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v2, Lhmw;->ai:Lhmw;

    .line 596
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmw;)Lhmr;

    move-result-object v1

    sget-object v2, Lhmw;->ah:Lhmw;

    .line 597
    invoke-virtual {v1, v2}, Lhmr;->b(Lhmw;)Lhmr;

    move-result-object v1

    .line 594
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 603
    iget-object v0, p0, Ldbs;->Z:Landroid/widget/AutoCompleteTextView;

    .line 604
    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lely;

    .line 605
    invoke-virtual {v0, v4}, Lely;->a(Ljava/util/Collection;)V

    .line 607
    iget-object v0, p0, Ldbs;->ax:Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;->invalidate()V

    goto/16 :goto_0
.end method

.method public a([Lnys;Ljava/lang/String;)I
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 282
    iget-object v0, p0, Ldbs;->U:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 283
    iget-object v0, p0, Ldbs;->ar:Ldes;

    invoke-virtual {v0}, Ldes;->d()V

    .line 284
    const/4 v0, 0x0

    iput-object v0, p0, Ldbs;->ac:Ldbp;

    .line 285
    invoke-direct {p0, v2}, Ldbs;->c(I)V

    .line 287
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ldbs;->V:Ljava/util/List;

    .line 288
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 289
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 290
    if-eqz p1, :cond_c

    .line 291
    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    move v4, v0

    :goto_0
    if-ltz v4, :cond_b

    .line 292
    aget-object v7, p1, v4

    .line 293
    iget v0, v7, Lnys;->d:I

    .line 294
    invoke-static {v0}, Lfve;->a(I)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {v0}, Lfve;->b(I)Z

    move-result v3

    if-nez v3, :cond_0

    if-nez v0, :cond_4

    move v3, v1

    :goto_1
    if-nez v3, :cond_0

    invoke-static {v0}, Lfve;->d(I)Z

    move-result v3

    if-eqz v3, :cond_5

    :cond_0
    move v3, v1

    :goto_2
    if-eqz v3, :cond_3

    .line 295
    iget-object v3, v7, Lnys;->c:Lnyz;

    if-eqz v3, :cond_1

    if-nez v0, :cond_6

    move v3, v1

    :goto_3
    if-eqz v3, :cond_1

    iget-object v3, p0, Ldbs;->az:Ljava/lang/String;

    iget-object v8, v7, Lnys;->c:Lnyz;

    iget-object v8, v8, Lnyz;->c:Ljava/lang/String;

    .line 296
    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 299
    iput v1, v7, Lnys;->d:I

    .line 300
    iget v0, v7, Lnys;->d:I

    .line 302
    :cond_1
    iget-object v3, v7, Lnys;->f:Lnyp;

    if-eqz v3, :cond_3

    .line 303
    new-instance v3, Ldbp;

    invoke-direct {v3}, Ldbp;-><init>()V

    .line 307
    iget-object v8, p0, Ldbs;->ax:Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;

    invoke-virtual {v3, v8, v7, p2}, Ldbp;->a(Landroid/view/View;Lnys;Ljava/lang/String;)V

    .line 310
    invoke-static {v0}, Lfve;->a(I)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 311
    iget-object v0, v7, Lnys;->c:Lnyz;

    if-eqz v0, :cond_3

    iget-object v0, v7, Lnys;->c:Lnyz;

    iget-object v0, v0, Lnyz;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 312
    iget-object v0, p0, Ldbs;->ar:Ldes;

    iget-object v7, v7, Lnys;->c:Lnyz;

    iget-object v7, v7, Lnyz;->c:Ljava/lang/String;

    invoke-virtual {v0, v7}, Ldes;->a(Ljava/lang/String;)V

    .line 315
    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 328
    :cond_2
    :goto_4
    iget-object v0, p0, Ldbs;->aF:Ldcb;

    invoke-virtual {v3, v0}, Ldbp;->a(Ldbr;)V

    .line 291
    :cond_3
    add-int/lit8 v0, v4, -0x1

    move v4, v0

    goto :goto_0

    :cond_4
    move v3, v2

    .line 294
    goto :goto_1

    :cond_5
    move v3, v2

    goto :goto_2

    :cond_6
    move v3, v2

    .line 295
    goto :goto_3

    .line 316
    :cond_7
    invoke-static {v0}, Lfve;->b(I)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 317
    iget-object v0, v7, Lnys;->h:[Lnyz;

    if-eqz v0, :cond_3

    iget-object v0, v7, Lnys;->h:[Lnyz;

    array-length v0, v0

    if-eqz v0, :cond_3

    iget-object v0, v7, Lnys;->h:[Lnyz;

    aget-object v0, v0, v2

    iget-object v0, v0, Lnyz;->d:Ljava/lang/String;

    .line 318
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 319
    iget-object v0, p0, Ldbs;->V:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 322
    :cond_8
    invoke-static {v0}, Lfve;->d(I)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 323
    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 324
    :cond_9
    if-nez v0, :cond_a

    move v0, v1

    :goto_5
    if-eqz v0, :cond_2

    .line 325
    iget-object v0, p0, Ldbs;->V:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_a
    move v0, v2

    .line 324
    goto :goto_5

    .line 332
    :cond_b
    iget-object v0, p0, Ldbs;->U:Ljava/util/List;

    iget-object v3, p0, Ldbs;->V:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 333
    iget-object v0, p0, Ldbs;->U:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 334
    iget-object v0, p0, Ldbs;->U:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 336
    iput-boolean v2, p0, Ldbs;->ae:Z

    .line 337
    iget-object v0, p0, Ldbs;->V:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_c

    .line 338
    iput-boolean v1, p0, Ldbs;->ae:Z

    .line 339
    invoke-direct {p0, v1}, Ldbs;->c(I)V

    .line 343
    :cond_c
    iget-object v0, p0, Ldbs;->ax:Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;->requestLayout()V

    .line 344
    iget-object v0, p0, Ldbs;->ax:Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;->invalidate()V

    .line 345
    iget-object v0, p0, Ldbs;->U:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 805
    const v0, 0x7f04018b

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 806
    if-nez v1, :cond_0

    .line 807
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "view must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 810
    :cond_0
    invoke-virtual {p0}, Ldbs;->o()Landroid/content/res/Resources;

    move-result-object v2

    move-object v0, v1

    .line 812
    check-cast v0, Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;

    iput-object v0, p0, Ldbs;->ax:Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;

    .line 813
    iget-object v0, p0, Ldbs;->ax:Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;->a(Ldck;)V

    .line 814
    iget-object v0, p0, Ldbs;->ax:Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;->a(Ldcl;)V

    .line 815
    iget-object v0, p0, Ldbs;->ax:Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;

    iget-object v3, p0, Ldbs;->aG:Ldcj;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 817
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ldbs;->U:Ljava/util/List;

    .line 818
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ldbs;->W:Ljava/util/List;

    .line 819
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ldbs;->X:Ljava/util/List;

    .line 821
    const v0, 0x7f1004c5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AutoCompleteTextView;

    iput-object v0, p0, Ldbs;->Z:Landroid/widget/AutoCompleteTextView;

    .line 822
    iget-object v0, p0, Ldbs;->Z:Landroid/widget/AutoCompleteTextView;

    iget-object v3, p0, Ldbs;->ao:Ldcm;

    invoke-interface {v3}, Ldcm;->c()Lely;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/AutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 823
    iget-object v0, p0, Ldbs;->Z:Landroid/widget/AutoCompleteTextView;

    iget-object v3, p0, Ldbs;->aj:Landroid/text/TextWatcher;

    invoke-virtual {v0, v3}, Landroid/widget/AutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 824
    iget-object v0, p0, Ldbs;->Z:Landroid/widget/AutoCompleteTextView;

    new-instance v3, Ldby;

    invoke-direct {v3, p0}, Ldby;-><init>(Ldbs;)V

    invoke-virtual {v0, v3}, Landroid/widget/AutoCompleteTextView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 842
    const v0, 0x7f02033b

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Ldbs;->O:Landroid/graphics/drawable/Drawable;

    .line 843
    const v0, 0x7f02033c

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Ldbs;->P:Landroid/graphics/drawable/Drawable;

    .line 845
    iget-object v0, p0, Ldbs;->Z:Landroid/widget/AutoCompleteTextView;

    const v3, 0x7f020341

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/AutoCompleteTextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 847
    iput v4, p0, Ldbs;->af:I

    .line 848
    iget-object v0, p0, Ldbs;->Z:Landroid/widget/AutoCompleteTextView;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/widget/AutoCompleteTextView;->setVisibility(I)V

    .line 849
    const v0, 0x7f1004c4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Ldbs;->aa:Landroid/widget/ImageView;

    .line 850
    iget-object v0, p0, Ldbs;->aa:Landroid/widget/ImageView;

    iget-object v3, p0, Ldbs;->O:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 852
    const v0, 0x7f1004c6

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Ldbs;->ab:Landroid/widget/ImageButton;

    .line 853
    iget-object v0, p0, Ldbs;->ab:Landroid/widget/ImageButton;

    new-instance v3, Ldbz;

    invoke-direct {v3, p0}, Ldbz;-><init>(Ldbs;)V

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 861
    sget-boolean v0, Ldbs;->N:Z

    if-nez v0, :cond_1

    .line 862
    const v0, 0x7f0d0312

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    sput v0, Ldbs;->Q:I

    .line 864
    const v0, 0x7f0d0313

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    sput v0, Ldbs;->R:I

    .line 866
    const v0, 0x7f0d0314

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    sput v0, Ldbs;->S:I

    .line 869
    const v0, 0x7f0d0315

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    sput v0, Ldbs;->T:I

    .line 871
    const/4 v0, 0x1

    sput-boolean v0, Ldbs;->N:Z

    .line 874
    :cond_1
    return-object v1
.end method

.method public a(Ljava/lang/Long;)Ldbp;
    .locals 3

    .prologue
    .line 372
    iget-object v0, p0, Ldbs;->U:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 373
    iget-object v0, p0, Ldbs;->U:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbp;

    .line 374
    invoke-virtual {v0}, Ldbp;->b()Ljava/lang/Long;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Ldbp;->b()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 378
    :goto_1
    return-object v0

    .line 372
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 378
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 210
    invoke-direct {p0}, Ldbs;->Y()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 212
    iget-object v0, p0, Ldbs;->ac:Ldbp;

    invoke-virtual {v0}, Ldbp;->a()Landroid/graphics/Rect;

    move-result-object v2

    .line 213
    iget v0, v2, Landroid/graphics/Rect;->left:I

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v3, v0

    .line 214
    iget-object v0, p0, Ldbs;->ac:Ldbp;

    invoke-virtual {v0}, Ldbp;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, v2, Landroid/graphics/Rect;->bottom:I

    .line 215
    :goto_0
    iget-object v4, p0, Ldbs;->O:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    .line 216
    iget-object v5, p0, Ldbs;->O:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    .line 217
    iget-object v6, p0, Ldbs;->aa:Landroid/widget/ImageView;

    div-int/lit8 v7, v4, 0x2

    sub-int v7, v3, v7

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    add-int v4, v0, v5

    invoke-virtual {v6, v7, v0, v3, v4}, Landroid/widget/ImageView;->layout(IIII)V

    .line 221
    iget v0, v2, Landroid/graphics/Rect;->left:I

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v0, v3

    iget v3, v2, Landroid/graphics/Rect;->top:I

    sget v2, Ldbs;->Q:I

    div-int/lit8 v2, v2, 0x2

    sub-int v2, v0, v2

    sget v4, Ldbs;->Q:I

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v0, v4

    sget v4, Ldbs;->R:I

    add-int/2addr v4, v3

    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5, v2, v3, v0, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    sget v6, Ldbs;->S:I

    if-ge v2, v6, :cond_2

    sget v0, Ldbs;->S:I

    sub-int/2addr v0, v2

    move v2, v0

    :goto_1
    if-gez v3, :cond_3

    sget v0, Ldbs;->S:I

    sub-int/2addr v0, v3

    :goto_2
    invoke-virtual {v5, v2, v0}, Landroid/graphics/Rect;->offset(II)V

    .line 222
    iget-object v0, p0, Ldbs;->O:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    .line 223
    iget-object v1, p0, Ldbs;->ac:Ldbp;

    invoke-virtual {v1}, Ldbp;->g()Z

    move-result v1

    if-eqz v1, :cond_4

    iget v0, v5, Landroid/graphics/Rect;->top:I

    sget v1, Ldbs;->T:I

    add-int/2addr v0, v1

    .line 225
    :goto_3
    iget-object v1, p0, Ldbs;->Z:Landroid/widget/AutoCompleteTextView;

    iget v2, v5, Landroid/graphics/Rect;->left:I

    iget v3, v5, Landroid/graphics/Rect;->right:I

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v4

    add-int/2addr v4, v0

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/widget/AutoCompleteTextView;->layout(IIII)V

    .line 227
    iget v1, v5, Landroid/graphics/Rect;->right:I

    iget-object v2, p0, Ldbs;->ab:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->getMeasuredWidth()I

    move-result v2

    sub-int/2addr v1, v2

    sget v2, Ldbs;->T:I

    sub-int/2addr v1, v2

    .line 228
    sget v2, Ldbs;->T:I

    add-int/2addr v0, v2

    .line 229
    iget-object v2, p0, Ldbs;->ab:Landroid/widget/ImageButton;

    iget-object v3, p0, Ldbs;->ab:Landroid/widget/ImageButton;

    invoke-virtual {v3}, Landroid/widget/ImageButton;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, v1

    iget-object v4, p0, Ldbs;->ab:Landroid/widget/ImageButton;

    .line 230
    invoke-virtual {v4}, Landroid/widget/ImageButton;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v0

    .line 229
    invoke-virtual {v2, v1, v0, v3, v4}, Landroid/widget/ImageButton;->layout(IIII)V

    .line 232
    :cond_0
    return-void

    .line 214
    :cond_1
    iget v0, v2, Landroid/graphics/Rect;->top:I

    goto/16 :goto_0

    .line 221
    :cond_2
    iget-object v2, p0, Ldbs;->ax:Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;->getMeasuredWidth()I

    move-result v2

    if-le v0, v2, :cond_6

    iget-object v2, p0, Ldbs;->ax:Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;->getMeasuredWidth()I

    move-result v2

    sub-int v0, v2, v0

    sget v2, Ldbs;->S:I

    sub-int/2addr v0, v2

    move v2, v0

    goto :goto_1

    :cond_3
    iget-object v0, p0, Ldbs;->ax:Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;->getMeasuredHeight()I

    move-result v0

    if-le v4, v0, :cond_5

    iget-object v0, p0, Ldbs;->ax:Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;->getMeasuredHeight()I

    move-result v0

    sub-int/2addr v0, v4

    sget v1, Ldbs;->S:I

    sub-int/2addr v0, v1

    goto :goto_2

    .line 223
    :cond_4
    iget v1, v5, Landroid/graphics/Rect;->top:I

    add-int/2addr v0, v1

    sget v1, Ldbs;->T:I

    sub-int/2addr v0, v1

    goto :goto_3

    :cond_5
    move v0, v1

    goto :goto_2

    :cond_6
    move v2, v1

    goto/16 :goto_1
.end method

.method public a(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1089
    return-void
.end method

.method public a(IZLandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1093
    return-void
.end method

.method public a(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 237
    iget-boolean v0, p0, Ldbs;->aA:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldbs;->ap:Ldeo;

    invoke-virtual {v0}, Ldeo;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Ldbs;->V()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 258
    :cond_0
    :goto_0
    return-void

    .line 241
    :cond_1
    invoke-direct {p0}, Ldbs;->W()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Ldbs;->V:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ldbs;->V:Ljava/util/List;

    .line 242
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 243
    iget-object v0, p0, Ldbs;->V:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbp;

    .line 244
    invoke-virtual {v0}, Ldbp;->e()I

    move-result v1

    invoke-static {v1}, Lfve;->c(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 245
    invoke-virtual {v0, p1}, Ldbp;->a(Landroid/graphics/Canvas;)V

    goto :goto_0

    .line 250
    :cond_2
    invoke-direct {p0}, Ldbs;->X()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 254
    iget-object v0, p0, Ldbs;->U:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_1
    if-ltz v1, :cond_0

    .line 255
    iget-object v0, p0, Ldbs;->U:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbp;

    .line 256
    invoke-virtual {v0, p1}, Ldbp;->a(Landroid/graphics/Canvas;)V

    .line 254
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 710
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 711
    invoke-virtual {p0}, Ldbs;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "account_id"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Ldbs;->ay:I

    .line 713
    iget-object v0, p0, Ldbs;->au:Llnh;

    const-class v1, Lhei;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iget v1, p0, Ldbs;->ay:I

    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldbs;->az:Ljava/lang/String;

    .line 714
    return-void
.end method

.method public a(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 10

    .prologue
    .line 1050
    const-string v0, "pouf_delete_shape"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1051
    invoke-virtual {p0}, Ldbs;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "view_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1052
    iget-object v0, p0, Ldbs;->ap:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v8

    .line 1053
    invoke-virtual {p0}, Ldbs;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Ldbs;->ay:I

    .line 1055
    invoke-interface {v8}, Lddl;->o()J

    move-result-wide v2

    invoke-interface {v8}, Lddl;->j()Ljava/lang/String;

    move-result-object v4

    const-string v5, "shape_id"

    .line 1056
    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v5

    .line 1057
    invoke-interface {v8}, Lddl;->k()Ljava/lang/String;

    move-result-object v8

    const-string v9, "permanent_delete"

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_1

    const/4 v9, 0x1

    .line 1053
    :goto_0
    invoke-static/range {v0 .. v9}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;IJLjava/lang/String;JLjava/lang/String;Ljava/lang/String;Z)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldbs;->as:Ljava/lang/Integer;

    .line 1058
    invoke-virtual {p0}, Ldbs;->p()Lae;

    move-result-object v0

    invoke-virtual {p0}, Ldbs;->n()Lz;

    move-result-object v1

    invoke-static {v0, v1}, Ldhh;->a(Lae;Landroid/content/Context;)V

    .line 1077
    :cond_0
    :goto_1
    return-void

    .line 1057
    :cond_1
    const/4 v9, 0x0

    goto :goto_0

    .line 1059
    :cond_2
    const-string v0, "pouf_create_shape_and_share"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1060
    const-string v0, "shape_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 1061
    const-string v0, "bounds"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    .line 1062
    const-string v1, "taggee_name"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1063
    const-string v1, "taggee_email"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1064
    const-string v1, "taggee_gaia_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1065
    const-wide/16 v8, 0x0

    cmp-long v1, v2, v8

    if-eqz v1, :cond_3

    .line 1066
    iget-object v1, p0, Ldbs;->aH:Ldcf;

    invoke-virtual/range {v1 .. v6}, Ldcf;->a(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1069
    :cond_3
    iget-object v1, p0, Ldbs;->aH:Ldcf;

    invoke-virtual {v1, v0, v4, v5, v6}, Ldcf;->a(Landroid/graphics/RectF;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1072
    :cond_4
    const-string v0, "pouf_accept_shape_and_share"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1073
    const-string v0, "shape_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 1074
    const-string v2, "taggee_gaia_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1075
    iget-object v3, p0, Ldbs;->aH:Ldcf;

    invoke-virtual {v3, v0, v1, v2}, Ldcf;->a(JLjava/lang/String;)V

    goto :goto_1
.end method

.method public aO_()V
    .locals 2

    .prologue
    .line 879
    invoke-super {p0}, Llol;->aO_()V

    .line 880
    iget-object v0, p0, Ldbs;->am:Ldgi;

    iget-object v1, p0, Ldbs;->aI:Ldgl;

    invoke-interface {v0, v1}, Ldgi;->a(Ldgl;)V

    .line 881
    iget-object v0, p0, Ldbs;->ao:Ldcm;

    iget-object v1, p0, Ldbs;->aD:Ldci;

    invoke-interface {v0, v1}, Ldcm;->a(Ldcn;)V

    .line 882
    iget-object v0, p0, Ldbs;->aE:Ldce;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(Lfhh;)V

    .line 883
    return-void
.end method

.method public ae_()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 383
    invoke-super {p0}, Llol;->ae_()V

    .line 384
    iput-object v1, p0, Ldbs;->ax:Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;

    .line 385
    iput-object v1, p0, Ldbs;->W:Ljava/util/List;

    .line 386
    iput-object v1, p0, Ldbs;->X:Ljava/util/List;

    .line 388
    iget-object v0, p0, Ldbs;->U:Ljava/util/List;

    if-nez v0, :cond_0

    .line 398
    :goto_0
    return-void

    .line 391
    :cond_0
    iput-object v1, p0, Ldbs;->U:Ljava/util/List;

    .line 396
    iget-object v0, p0, Ldbs;->Z:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0
.end method

.method public b(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1081
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 262
    iget-object v0, p0, Ldbs;->U:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 263
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Ldbs;->c(I)V

    .line 265
    :cond_0
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 718
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 719
    iget-object v0, p0, Ldbs;->au:Llnh;

    const-class v1, Ldgi;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldgi;

    iput-object v0, p0, Ldbs;->am:Ldgi;

    .line 720
    iget-object v0, p0, Ldbs;->au:Llnh;

    const-class v1, Ldgr;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldgr;

    iput-object v0, p0, Ldbs;->an:Ldgr;

    .line 721
    iget-object v0, p0, Ldbs;->au:Llnh;

    const-class v1, Ldcm;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldcm;

    iput-object v0, p0, Ldbs;->ao:Ldcm;

    .line 722
    iget-object v0, p0, Ldbs;->au:Llnh;

    const-class v1, Ldeo;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldeo;

    iput-object v0, p0, Ldbs;->ap:Ldeo;

    .line 723
    iget-object v0, p0, Ldbs;->au:Llnh;

    const-class v1, Ldef;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldef;

    iput-object v0, p0, Ldbs;->aq:Ldef;

    .line 724
    iget-object v0, p0, Ldbs;->au:Llnh;

    const-class v1, Ldes;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldes;

    iput-object v0, p0, Ldbs;->ar:Ldes;

    .line 725
    return-void
.end method

.method public c(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1085
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 269
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Ldbs;->c(I)V

    .line 270
    return-void
.end method

.method public e()Ldbp;
    .locals 1

    .prologue
    .line 350
    iget-object v0, p0, Ldbs;->ac:Ldbp;

    return-object v0
.end method

.method public z()V
    .locals 2

    .prologue
    .line 887
    invoke-super {p0}, Llol;->z()V

    .line 888
    iget-object v0, p0, Ldbs;->am:Ldgi;

    iget-object v1, p0, Ldbs;->aI:Ldgl;

    invoke-interface {v0, v1}, Ldgi;->b(Ldgl;)V

    .line 889
    iget-object v0, p0, Ldbs;->ao:Ldcm;

    iget-object v1, p0, Ldbs;->aD:Ldci;

    invoke-interface {v0, v1}, Ldcm;->b(Ldcn;)V

    .line 890
    iget-object v0, p0, Ldbs;->aE:Ldce;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(Lfhh;)V

    .line 891
    return-void
.end method
