.class public final Lnbv;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lpme;

.field private c:Lnbl;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1205
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1210
    iput-object v0, p0, Lnbv;->b:Lpme;

    .line 1213
    iput-object v0, p0, Lnbv;->c:Lnbl;

    .line 1205
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1233
    const/4 v0, 0x0

    .line 1234
    iget-object v1, p0, Lnbv;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1235
    const/4 v0, 0x1

    iget-object v1, p0, Lnbv;->a:Ljava/lang/String;

    .line 1236
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1238
    :cond_0
    iget-object v1, p0, Lnbv;->b:Lpme;

    if-eqz v1, :cond_1

    .line 1239
    const/4 v1, 0x2

    iget-object v2, p0, Lnbv;->b:Lpme;

    .line 1240
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1242
    :cond_1
    iget-object v1, p0, Lnbv;->c:Lnbl;

    if-eqz v1, :cond_2

    .line 1243
    const/4 v1, 0x3

    iget-object v2, p0, Lnbv;->c:Lnbl;

    .line 1244
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1246
    :cond_2
    iget-object v1, p0, Lnbv;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1247
    iput v0, p0, Lnbv;->ai:I

    .line 1248
    return v0
.end method

.method public a(Loxn;)Lnbv;
    .locals 2

    .prologue
    .line 1256
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1257
    sparse-switch v0, :sswitch_data_0

    .line 1261
    iget-object v1, p0, Lnbv;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1262
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnbv;->ah:Ljava/util/List;

    .line 1265
    :cond_1
    iget-object v1, p0, Lnbv;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1267
    :sswitch_0
    return-object p0

    .line 1272
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnbv;->a:Ljava/lang/String;

    goto :goto_0

    .line 1276
    :sswitch_2
    iget-object v0, p0, Lnbv;->b:Lpme;

    if-nez v0, :cond_2

    .line 1277
    new-instance v0, Lpme;

    invoke-direct {v0}, Lpme;-><init>()V

    iput-object v0, p0, Lnbv;->b:Lpme;

    .line 1279
    :cond_2
    iget-object v0, p0, Lnbv;->b:Lpme;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1283
    :sswitch_3
    iget-object v0, p0, Lnbv;->c:Lnbl;

    if-nez v0, :cond_3

    .line 1284
    new-instance v0, Lnbl;

    invoke-direct {v0}, Lnbl;-><init>()V

    iput-object v0, p0, Lnbv;->c:Lnbl;

    .line 1286
    :cond_3
    iget-object v0, p0, Lnbv;->c:Lnbl;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1257
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1218
    iget-object v0, p0, Lnbv;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1219
    const/4 v0, 0x1

    iget-object v1, p0, Lnbv;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1221
    :cond_0
    iget-object v0, p0, Lnbv;->b:Lpme;

    if-eqz v0, :cond_1

    .line 1222
    const/4 v0, 0x2

    iget-object v1, p0, Lnbv;->b:Lpme;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1224
    :cond_1
    iget-object v0, p0, Lnbv;->c:Lnbl;

    if-eqz v0, :cond_2

    .line 1225
    const/4 v0, 0x3

    iget-object v1, p0, Lnbv;->c:Lnbl;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1227
    :cond_2
    iget-object v0, p0, Lnbv;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1229
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1201
    invoke-virtual {p0, p1}, Lnbv;->a(Loxn;)Lnbv;

    move-result-object v0

    return-object v0
.end method
