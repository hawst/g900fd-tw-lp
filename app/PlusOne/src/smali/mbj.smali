.class public final Lmbj;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmxk;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 30292
    invoke-direct {p0}, Loxq;-><init>()V

    .line 30295
    iput-object v0, p0, Lmbj;->apiHeader:Llyr;

    .line 30298
    iput-object v0, p0, Lmbj;->a:Lmxk;

    .line 30292
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 30315
    const/4 v0, 0x0

    .line 30316
    iget-object v1, p0, Lmbj;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 30317
    const/4 v0, 0x1

    iget-object v1, p0, Lmbj;->apiHeader:Llyr;

    .line 30318
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 30320
    :cond_0
    iget-object v1, p0, Lmbj;->a:Lmxk;

    if-eqz v1, :cond_1

    .line 30321
    const/4 v1, 0x2

    iget-object v2, p0, Lmbj;->a:Lmxk;

    .line 30322
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 30324
    :cond_1
    iget-object v1, p0, Lmbj;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 30325
    iput v0, p0, Lmbj;->ai:I

    .line 30326
    return v0
.end method

.method public a(Loxn;)Lmbj;
    .locals 2

    .prologue
    .line 30334
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 30335
    sparse-switch v0, :sswitch_data_0

    .line 30339
    iget-object v1, p0, Lmbj;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 30340
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmbj;->ah:Ljava/util/List;

    .line 30343
    :cond_1
    iget-object v1, p0, Lmbj;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 30345
    :sswitch_0
    return-object p0

    .line 30350
    :sswitch_1
    iget-object v0, p0, Lmbj;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 30351
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmbj;->apiHeader:Llyr;

    .line 30353
    :cond_2
    iget-object v0, p0, Lmbj;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 30357
    :sswitch_2
    iget-object v0, p0, Lmbj;->a:Lmxk;

    if-nez v0, :cond_3

    .line 30358
    new-instance v0, Lmxk;

    invoke-direct {v0}, Lmxk;-><init>()V

    iput-object v0, p0, Lmbj;->a:Lmxk;

    .line 30360
    :cond_3
    iget-object v0, p0, Lmbj;->a:Lmxk;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 30335
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 30303
    iget-object v0, p0, Lmbj;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 30304
    const/4 v0, 0x1

    iget-object v1, p0, Lmbj;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 30306
    :cond_0
    iget-object v0, p0, Lmbj;->a:Lmxk;

    if-eqz v0, :cond_1

    .line 30307
    const/4 v0, 0x2

    iget-object v1, p0, Lmbj;->a:Lmxk;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 30309
    :cond_1
    iget-object v0, p0, Lmbj;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 30311
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 30288
    invoke-virtual {p0, p1}, Lmbj;->a(Loxn;)Lmbj;

    move-result-object v0

    return-object v0
.end method
