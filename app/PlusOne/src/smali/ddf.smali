.class public final Lddf;
.super Lddd;
.source "PG"


# instance fields
.field public P:Ljava/lang/Integer;

.field private final Q:Lddj;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lddd;-><init>()V

    .line 32
    new-instance v0, Lddj;

    invoke-direct {v0, p0}, Lddj;-><init>(Lddf;)V

    iput-object v0, p0, Lddf;->Q:Lddj;

    .line 193
    return-void
.end method

.method private W()I
    .locals 3

    .prologue
    .line 102
    invoke-virtual {p0}, Lddf;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "account_id"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 37
    invoke-super {p0, p1}, Lddd;->a(Landroid/os/Bundle;)V

    .line 38
    if-eqz p1, :cond_0

    .line 39
    const-string v0, "refresh_request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 40
    const-string v0, "refresh_request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lddf;->P:Ljava/lang/Integer;

    .line 43
    :cond_0
    return-void
.end method

.method protected a()Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 74
    invoke-direct {p0}, Lddf;->W()I

    move-result v1

    .line 75
    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    .line 76
    const/4 v0, 0x0

    .line 91
    :cond_0
    :goto_0
    return v0

    .line 80
    :cond_1
    invoke-virtual {p0}, Lddf;->w()Lbb;

    move-result-object v1

    invoke-virtual {p0}, Lddf;->k()Landroid/os/Bundle;

    move-result-object v2

    new-instance v3, Lddi;

    invoke-direct {v3, p0}, Lddi;-><init>(Lddf;)V

    invoke-virtual {v1, v0, v2, v3}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 84
    invoke-virtual {p0}, Lddf;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "selection_cluster_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 85
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 87
    invoke-virtual {p0}, Lddf;->w()Lbb;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lddf;->k()Landroid/os/Bundle;

    move-result-object v3

    new-instance v4, Lddh;

    invoke-direct {v4, p0}, Lddh;-><init>(Lddf;)V

    invoke-virtual {v1, v2, v3, v4}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    goto :goto_0
.end method

.method public aO_()V
    .locals 4

    .prologue
    .line 55
    invoke-super {p0}, Lddd;->aO_()V

    .line 56
    iget-object v0, p0, Lddf;->Q:Lddj;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(Lfhh;)V

    .line 57
    iget-object v0, p0, Lddf;->P:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lddf;->P:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 59
    invoke-direct {p0}, Lddf;->W()I

    .line 60
    iget-object v0, p0, Lddf;->P:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(I)Lfib;

    move-result-object v0

    .line 61
    iget-object v1, p0, Lddf;->Q:Lddj;

    iget-object v2, p0, Lddf;->P:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0}, Lddf;->V()Lddl;

    move-result-object v3

    invoke-interface {v3}, Lddl;->o()J

    invoke-virtual {v1, v2, v0}, Lddj;->f(ILfib;)V

    .line 64
    :cond_0
    return-void
.end method

.method protected e()V
    .locals 6

    .prologue
    .line 96
    invoke-direct {p0}, Lddf;->W()I

    move-result v0

    .line 97
    invoke-virtual {p0}, Lddf;->n()Lz;

    move-result-object v1

    .line 98
    invoke-virtual {p0}, Lddf;->V()Lddl;

    move-result-object v2

    invoke-interface {v2}, Lddl;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lddf;->V()Lddl;

    move-result-object v3

    invoke-interface {v3}, Lddl;->o()J

    move-result-wide v4

    .line 97
    invoke-static {v1, v0, v2, v4, v5}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILjava/lang/String;J)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lddf;->P:Ljava/lang/Integer;

    .line 99
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 47
    invoke-super {p0, p1}, Lddd;->e(Landroid/os/Bundle;)V

    .line 48
    iget-object v0, p0, Lddf;->P:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 49
    const-string v0, "refresh_request_id"

    iget-object v1, p0, Lddf;->P:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 51
    :cond_0
    return-void
.end method

.method public z()V
    .locals 1

    .prologue
    .line 68
    invoke-super {p0}, Lddd;->z()V

    .line 69
    iget-object v0, p0, Lddf;->Q:Lddj;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(Lfhh;)V

    .line 70
    return-void
.end method
