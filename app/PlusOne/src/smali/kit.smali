.class public final Lkit;
.super Lkgn;
.source "PG"

# interfaces
.implements Lkig;


# instance fields
.field private Q:Lkif;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 44
    invoke-direct {p0}, Lkgn;-><init>()V

    .line 41
    new-instance v0, Lkif;

    iget-object v1, p0, Lkit;->P:Llqm;

    invoke-direct {v0, p0, v1}, Lkif;-><init>(Lkgn;Llqr;)V

    iput-object v0, p0, Lkit;->Q:Lkif;

    .line 44
    return-void
.end method

.method public static b(Landroid/content/Intent;)Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 33
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 34
    const-string v1, "terms_uri"

    const-string v2, "terms_uri"

    .line 35
    invoke-virtual {p0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 34
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    const-string v1, "privacy_uri"

    const-string v2, "privacy_uri"

    .line 37
    invoke-virtual {p0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 36
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 66
    new-instance v0, Lkiu;

    invoke-direct {v0}, Lkiu;-><init>()V

    .line 67
    invoke-virtual {p0}, Lkit;->c()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkiu;->b(Landroid/net/Uri;)V

    .line 68
    invoke-virtual {p0}, Lkit;->d()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkiu;->a(Landroid/net/Uri;)V

    .line 69
    iget-object v1, p0, Lkit;->Q:Lkif;

    invoke-virtual {v1, v0}, Lkif;->a(Lu;)V

    .line 70
    return-void
.end method

.method protected c()Landroid/net/Uri;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 53
    invoke-virtual {p0}, Lkit;->k()Landroid/os/Bundle;

    move-result-object v1

    .line 54
    if-eqz v1, :cond_1

    const-string v2, "terms_uri"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 55
    :goto_0
    if-eqz v1, :cond_0

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :cond_0
    return-object v0

    :cond_1
    move-object v1, v0

    .line 54
    goto :goto_0
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 48
    invoke-super {p0, p1}, Lkgn;->c(Landroid/os/Bundle;)V

    .line 49
    iget-object v0, p0, Lkit;->O:Llnh;

    const-class v1, Lkij;

    iget-object v2, p0, Lkit;->Q:Lkif;

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 50
    return-void
.end method

.method protected d()Landroid/net/Uri;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 59
    invoke-virtual {p0}, Lkit;->k()Landroid/os/Bundle;

    move-result-object v1

    .line 60
    if-eqz v1, :cond_1

    const-string v2, "privacy_uri"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 61
    :goto_0
    if-eqz v1, :cond_0

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :cond_0
    return-object v0

    :cond_1
    move-object v1, v0

    .line 60
    goto :goto_0
.end method
