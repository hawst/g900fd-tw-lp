.class public final Lmgt;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lndm;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 22840
    invoke-direct {p0}, Loxq;-><init>()V

    .line 22843
    iput-object v0, p0, Lmgt;->apiHeader:Llyr;

    .line 22846
    iput-object v0, p0, Lmgt;->a:Lndm;

    .line 22840
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 22863
    const/4 v0, 0x0

    .line 22864
    iget-object v1, p0, Lmgt;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 22865
    const/4 v0, 0x1

    iget-object v1, p0, Lmgt;->apiHeader:Llyr;

    .line 22866
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 22868
    :cond_0
    iget-object v1, p0, Lmgt;->a:Lndm;

    if-eqz v1, :cond_1

    .line 22869
    const/4 v1, 0x2

    iget-object v2, p0, Lmgt;->a:Lndm;

    .line 22870
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 22872
    :cond_1
    iget-object v1, p0, Lmgt;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 22873
    iput v0, p0, Lmgt;->ai:I

    .line 22874
    return v0
.end method

.method public a(Loxn;)Lmgt;
    .locals 2

    .prologue
    .line 22882
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 22883
    sparse-switch v0, :sswitch_data_0

    .line 22887
    iget-object v1, p0, Lmgt;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 22888
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmgt;->ah:Ljava/util/List;

    .line 22891
    :cond_1
    iget-object v1, p0, Lmgt;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 22893
    :sswitch_0
    return-object p0

    .line 22898
    :sswitch_1
    iget-object v0, p0, Lmgt;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 22899
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmgt;->apiHeader:Llyr;

    .line 22901
    :cond_2
    iget-object v0, p0, Lmgt;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 22905
    :sswitch_2
    iget-object v0, p0, Lmgt;->a:Lndm;

    if-nez v0, :cond_3

    .line 22906
    new-instance v0, Lndm;

    invoke-direct {v0}, Lndm;-><init>()V

    iput-object v0, p0, Lmgt;->a:Lndm;

    .line 22908
    :cond_3
    iget-object v0, p0, Lmgt;->a:Lndm;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 22883
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 22851
    iget-object v0, p0, Lmgt;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 22852
    const/4 v0, 0x1

    iget-object v1, p0, Lmgt;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 22854
    :cond_0
    iget-object v0, p0, Lmgt;->a:Lndm;

    if-eqz v0, :cond_1

    .line 22855
    const/4 v0, 0x2

    iget-object v1, p0, Lmgt;->a:Lndm;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 22857
    :cond_1
    iget-object v0, p0, Lmgt;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 22859
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 22836
    invoke-virtual {p0, p1}, Lmgt;->a(Loxn;)Lmgt;

    move-result-object v0

    return-object v0
.end method
