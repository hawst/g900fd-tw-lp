.class public abstract Llhb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llhi;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Z

.field public c:I

.field public d:F

.field public e:I

.field public f:I

.field private g:I

.field private h:Z


# direct methods
.method protected constructor <init>(Landroid/content/Context;IZ)V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Llhb;->a:Landroid/content/Context;

    .line 45
    iput-boolean p3, p0, Llhb;->b:Z

    .line 46
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Llhb;->g:I

    .line 47
    iput p2, p0, Llhb;->c:I

    .line 48
    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 122
    const/4 v0, 0x0

    iput-boolean v0, p0, Llhb;->h:Z

    .line 123
    iget v0, p0, Llhb;->c:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 124
    invoke-virtual {p0, v2}, Llhb;->a(Z)V

    .line 131
    :cond_0
    :goto_0
    return-void

    .line 125
    :cond_1
    iget v0, p0, Llhb;->c:I

    if-eq v0, v2, :cond_0

    .line 126
    invoke-virtual {p0}, Llhb;->e()V

    .line 127
    iput v2, p0, Llhb;->c:I

    .line 128
    invoke-virtual {p0, v2}, Llhb;->b(Z)F

    move-result v0

    iput v0, p0, Llhb;->d:F

    .line 129
    invoke-virtual {p0, v2}, Llhb;->a(Z)V

    goto :goto_0
.end method

.method public a(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 52
    iget-boolean v0, p0, Llhb;->h:Z

    if-eqz v0, :cond_1

    .line 58
    :cond_0
    :goto_0
    return-void

    .line 55
    :cond_1
    const/4 v0, 0x1

    if-eq p2, v0, :cond_0

    .line 56
    const/4 v0, 0x0

    iput v0, p0, Llhb;->e:I

    goto :goto_0
.end method

.method public a(Landroid/view/View;III)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 62
    iget-boolean v2, p0, Llhb;->h:Z

    if-eqz v2, :cond_1

    .line 118
    :cond_0
    :goto_0
    return-void

    .line 66
    :cond_1
    if-nez p2, :cond_2

    if-nez p4, :cond_2

    if-nez p3, :cond_2

    move v2, v0

    .line 67
    :goto_1
    if-eqz v2, :cond_3

    iget-boolean v2, p0, Llhb;->b:Z

    if-eqz v2, :cond_3

    .line 68
    iput v1, p0, Llhb;->e:I

    .line 69
    invoke-virtual {p0}, Llhb;->a()V

    goto :goto_0

    :cond_2
    move v2, v1

    .line 66
    goto :goto_1

    .line 73
    :cond_3
    if-gez p3, :cond_4

    iget v2, p0, Llhb;->e:I

    if-gtz v2, :cond_5

    :cond_4
    if-lez p3, :cond_8

    iget v2, p0, Llhb;->e:I

    if-gez v2, :cond_8

    :cond_5
    move v2, v0

    .line 75
    :goto_2
    if-eqz v2, :cond_6

    .line 76
    iput v1, p0, Llhb;->e:I

    .line 78
    :cond_6
    iget v2, p0, Llhb;->e:I

    add-int/2addr v2, p3

    iput v2, p0, Llhb;->e:I

    .line 80
    iget v2, p0, Llhb;->e:I

    iget v3, p0, Llhb;->g:I

    neg-int v3, v3

    if-le v2, v3, :cond_7

    iget v2, p0, Llhb;->e:I

    iget v3, p0, Llhb;->g:I

    if-lt v2, v3, :cond_9

    :cond_7
    move v2, v0

    .line 82
    :goto_3
    if-eqz v2, :cond_0

    .line 83
    iget v2, p0, Llhb;->e:I

    if-lez v2, :cond_a

    move v2, v0

    .line 84
    :goto_4
    if-eqz v2, :cond_b

    .line 85
    iget v1, p0, Llhb;->c:I

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 93
    :pswitch_1
    iput v0, p0, Llhb;->c:I

    .line 94
    invoke-virtual {p0, v0}, Llhb;->a(Z)V

    .line 95
    invoke-virtual {p0, v0}, Llhb;->b(Z)F

    move-result v0

    iput v0, p0, Llhb;->d:F

    goto :goto_0

    :cond_8
    move v2, v1

    .line 73
    goto :goto_2

    :cond_9
    move v2, v1

    .line 80
    goto :goto_3

    :cond_a
    move v2, v1

    .line 83
    goto :goto_4

    .line 88
    :pswitch_2
    invoke-virtual {p0}, Llhb;->e()V

    .line 89
    const/4 v1, 0x2

    iput v1, p0, Llhb;->c:I

    .line 90
    invoke-virtual {p0, v0}, Llhb;->a(Z)V

    goto :goto_0

    .line 99
    :cond_b
    if-nez p2, :cond_d

    iget v2, p0, Llhb;->f:I

    iget-boolean v3, p0, Llhb;->b:Z

    if-eqz v3, :cond_c

    neg-int p4, p4

    :cond_c
    if-lt v2, p4, :cond_d

    .line 101
    :goto_5
    if-nez v0, :cond_0

    .line 104
    iget v0, p0, Llhb;->c:I

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 107
    :pswitch_3
    invoke-virtual {p0}, Llhb;->e()V

    .line 108
    iput v1, p0, Llhb;->c:I

    .line 109
    invoke-virtual {p0, v1}, Llhb;->a(Z)V

    goto :goto_0

    :cond_d
    move v0, v1

    .line 99
    goto :goto_5

    .line 112
    :pswitch_4
    const/4 v0, 0x3

    iput v0, p0, Llhb;->c:I

    .line 113
    invoke-virtual {p0, v1}, Llhb;->b(Z)F

    move-result v0

    iput v0, p0, Llhb;->d:F

    goto/16 :goto_0

    .line 85
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch

    .line 104
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public abstract a(Z)V
.end method

.method protected abstract b(Z)F
.end method

.method public b()V
    .locals 3

    .prologue
    const/4 v2, 0x3

    const/4 v1, 0x0

    .line 135
    iget v0, p0, Llhb;->c:I

    if-nez v0, :cond_1

    .line 136
    invoke-virtual {p0, v1}, Llhb;->a(Z)V

    .line 142
    :cond_0
    :goto_0
    return-void

    .line 137
    :cond_1
    iget v0, p0, Llhb;->c:I

    if-eq v0, v2, :cond_0

    .line 138
    invoke-virtual {p0}, Llhb;->e()V

    .line 139
    iput v2, p0, Llhb;->c:I

    .line 140
    invoke-virtual {p0, v1}, Llhb;->b(Z)F

    move-result v0

    iput v0, p0, Llhb;->d:F

    goto :goto_0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 146
    const/4 v0, 0x0

    iput-boolean v0, p0, Llhb;->h:Z

    .line 147
    invoke-virtual {p0}, Llhb;->e()V

    .line 148
    const/4 v0, 0x2

    iput v0, p0, Llhb;->c:I

    .line 149
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Llhb;->a(Z)V

    .line 150
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 154
    const/4 v0, 0x1

    iput-boolean v0, p0, Llhb;->h:Z

    .line 155
    invoke-virtual {p0}, Llhb;->b()V

    .line 156
    return-void
.end method

.method protected abstract e()V
.end method

.method public f()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 176
    iget-boolean v1, p0, Llhb;->h:Z

    if-nez v1, :cond_1

    iget v1, p0, Llhb;->c:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    iget v1, p0, Llhb;->c:I

    if-ne v1, v0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
