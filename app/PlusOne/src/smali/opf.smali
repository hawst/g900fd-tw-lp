.class public final Lopf;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lope;

.field public b:I

.field public c:Lpxr;

.field private d:Loou;

.field private e:Loow;

.field private f:Looy;

.field private g:Lopa;

.field private h:Lopc;

.field private i:Lpij;

.field private j:Lpgb;

.field private k:Lpub;

.field private l:Lpws;

.field private m:Lpxl;

.field private n:Lppp;

.field private o:Lprm;

.field private p:Lplo;

.field private q:Lput;

.field private r:Lpuv;

.field private s:Lpxt;

.field private t:Lpmo;

.field private u:Lpjz;

.field private v:Lpwu;

.field private w:Lpnh;

.field private x:Loph;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 95
    invoke-direct {p0}, Loxq;-><init>()V

    .line 124
    iput-object v1, p0, Lopf;->a:Lope;

    .line 127
    const/high16 v0, -0x80000000

    iput v0, p0, Lopf;->b:I

    .line 130
    iput-object v1, p0, Lopf;->d:Loou;

    .line 133
    iput-object v1, p0, Lopf;->e:Loow;

    .line 136
    iput-object v1, p0, Lopf;->f:Looy;

    .line 139
    iput-object v1, p0, Lopf;->g:Lopa;

    .line 142
    iput-object v1, p0, Lopf;->h:Lopc;

    .line 145
    iput-object v1, p0, Lopf;->i:Lpij;

    .line 148
    iput-object v1, p0, Lopf;->j:Lpgb;

    .line 151
    iput-object v1, p0, Lopf;->c:Lpxr;

    .line 154
    iput-object v1, p0, Lopf;->k:Lpub;

    .line 157
    iput-object v1, p0, Lopf;->l:Lpws;

    .line 160
    iput-object v1, p0, Lopf;->m:Lpxl;

    .line 163
    iput-object v1, p0, Lopf;->n:Lppp;

    .line 166
    iput-object v1, p0, Lopf;->o:Lprm;

    .line 169
    iput-object v1, p0, Lopf;->p:Lplo;

    .line 172
    iput-object v1, p0, Lopf;->q:Lput;

    .line 175
    iput-object v1, p0, Lopf;->r:Lpuv;

    .line 178
    iput-object v1, p0, Lopf;->s:Lpxt;

    .line 181
    iput-object v1, p0, Lopf;->t:Lpmo;

    .line 184
    iput-object v1, p0, Lopf;->u:Lpjz;

    .line 187
    iput-object v1, p0, Lopf;->v:Lpwu;

    .line 190
    iput-object v1, p0, Lopf;->w:Lpnh;

    .line 193
    iput-object v1, p0, Lopf;->x:Loph;

    .line 95
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 276
    const/4 v0, 0x0

    .line 277
    iget-object v1, p0, Lopf;->a:Lope;

    if-eqz v1, :cond_0

    .line 278
    const/4 v0, 0x1

    iget-object v1, p0, Lopf;->a:Lope;

    .line 279
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 281
    :cond_0
    iget v1, p0, Lopf;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_1

    .line 282
    const/4 v1, 0x2

    iget v2, p0, Lopf;->b:I

    .line 283
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 285
    :cond_1
    iget-object v1, p0, Lopf;->d:Loou;

    if-eqz v1, :cond_2

    .line 286
    const/4 v1, 0x3

    iget-object v2, p0, Lopf;->d:Loou;

    .line 287
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 289
    :cond_2
    iget-object v1, p0, Lopf;->e:Loow;

    if-eqz v1, :cond_3

    .line 290
    const/4 v1, 0x4

    iget-object v2, p0, Lopf;->e:Loow;

    .line 291
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 293
    :cond_3
    iget-object v1, p0, Lopf;->f:Looy;

    if-eqz v1, :cond_4

    .line 294
    const/4 v1, 0x5

    iget-object v2, p0, Lopf;->f:Looy;

    .line 295
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 297
    :cond_4
    iget-object v1, p0, Lopf;->g:Lopa;

    if-eqz v1, :cond_5

    .line 298
    const/4 v1, 0x6

    iget-object v2, p0, Lopf;->g:Lopa;

    .line 299
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 301
    :cond_5
    iget-object v1, p0, Lopf;->h:Lopc;

    if-eqz v1, :cond_6

    .line 302
    const/4 v1, 0x7

    iget-object v2, p0, Lopf;->h:Lopc;

    .line 303
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 305
    :cond_6
    iget-object v1, p0, Lopf;->i:Lpij;

    if-eqz v1, :cond_7

    .line 306
    const/16 v1, 0x8

    iget-object v2, p0, Lopf;->i:Lpij;

    .line 307
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 309
    :cond_7
    iget-object v1, p0, Lopf;->j:Lpgb;

    if-eqz v1, :cond_8

    .line 310
    const/16 v1, 0x9

    iget-object v2, p0, Lopf;->j:Lpgb;

    .line 311
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 313
    :cond_8
    iget-object v1, p0, Lopf;->c:Lpxr;

    if-eqz v1, :cond_9

    .line 314
    const/16 v1, 0xa

    iget-object v2, p0, Lopf;->c:Lpxr;

    .line 315
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 317
    :cond_9
    iget-object v1, p0, Lopf;->k:Lpub;

    if-eqz v1, :cond_a

    .line 318
    const/16 v1, 0xb

    iget-object v2, p0, Lopf;->k:Lpub;

    .line 319
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 321
    :cond_a
    iget-object v1, p0, Lopf;->l:Lpws;

    if-eqz v1, :cond_b

    .line 322
    const/16 v1, 0xc

    iget-object v2, p0, Lopf;->l:Lpws;

    .line 323
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 325
    :cond_b
    iget-object v1, p0, Lopf;->m:Lpxl;

    if-eqz v1, :cond_c

    .line 326
    const/16 v1, 0xd

    iget-object v2, p0, Lopf;->m:Lpxl;

    .line 327
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 329
    :cond_c
    iget-object v1, p0, Lopf;->n:Lppp;

    if-eqz v1, :cond_d

    .line 330
    const/16 v1, 0xe

    iget-object v2, p0, Lopf;->n:Lppp;

    .line 331
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 333
    :cond_d
    iget-object v1, p0, Lopf;->o:Lprm;

    if-eqz v1, :cond_e

    .line 334
    const/16 v1, 0xf

    iget-object v2, p0, Lopf;->o:Lprm;

    .line 335
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 337
    :cond_e
    iget-object v1, p0, Lopf;->p:Lplo;

    if-eqz v1, :cond_f

    .line 338
    const/16 v1, 0x10

    iget-object v2, p0, Lopf;->p:Lplo;

    .line 339
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 341
    :cond_f
    iget-object v1, p0, Lopf;->q:Lput;

    if-eqz v1, :cond_10

    .line 342
    const/16 v1, 0x11

    iget-object v2, p0, Lopf;->q:Lput;

    .line 343
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 345
    :cond_10
    iget-object v1, p0, Lopf;->r:Lpuv;

    if-eqz v1, :cond_11

    .line 346
    const/16 v1, 0x12

    iget-object v2, p0, Lopf;->r:Lpuv;

    .line 347
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 349
    :cond_11
    iget-object v1, p0, Lopf;->s:Lpxt;

    if-eqz v1, :cond_12

    .line 350
    const/16 v1, 0x13

    iget-object v2, p0, Lopf;->s:Lpxt;

    .line 351
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 353
    :cond_12
    iget-object v1, p0, Lopf;->t:Lpmo;

    if-eqz v1, :cond_13

    .line 354
    const/16 v1, 0x14

    iget-object v2, p0, Lopf;->t:Lpmo;

    .line 355
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 357
    :cond_13
    iget-object v1, p0, Lopf;->u:Lpjz;

    if-eqz v1, :cond_14

    .line 358
    const/16 v1, 0x15

    iget-object v2, p0, Lopf;->u:Lpjz;

    .line 359
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 361
    :cond_14
    iget-object v1, p0, Lopf;->v:Lpwu;

    if-eqz v1, :cond_15

    .line 362
    const/16 v1, 0x16

    iget-object v2, p0, Lopf;->v:Lpwu;

    .line 363
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 365
    :cond_15
    iget-object v1, p0, Lopf;->w:Lpnh;

    if-eqz v1, :cond_16

    .line 366
    const/16 v1, 0x17

    iget-object v2, p0, Lopf;->w:Lpnh;

    .line 367
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 369
    :cond_16
    iget-object v1, p0, Lopf;->x:Loph;

    if-eqz v1, :cond_17

    .line 370
    const/16 v1, 0x18

    iget-object v2, p0, Lopf;->x:Loph;

    .line 371
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 373
    :cond_17
    iget-object v1, p0, Lopf;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 374
    iput v0, p0, Lopf;->ai:I

    .line 375
    return v0
.end method

.method public a(Loxn;)Lopf;
    .locals 2

    .prologue
    .line 383
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 384
    sparse-switch v0, :sswitch_data_0

    .line 388
    iget-object v1, p0, Lopf;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 389
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lopf;->ah:Ljava/util/List;

    .line 392
    :cond_1
    iget-object v1, p0, Lopf;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 394
    :sswitch_0
    return-object p0

    .line 399
    :sswitch_1
    iget-object v0, p0, Lopf;->a:Lope;

    if-nez v0, :cond_2

    .line 400
    new-instance v0, Lope;

    invoke-direct {v0}, Lope;-><init>()V

    iput-object v0, p0, Lopf;->a:Lope;

    .line 402
    :cond_2
    iget-object v0, p0, Lopf;->a:Lope;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 406
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 407
    if-eqz v0, :cond_3

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    const/4 v1, 0x4

    if-eq v0, v1, :cond_3

    const/4 v1, 0x5

    if-eq v0, v1, :cond_3

    const/4 v1, 0x6

    if-eq v0, v1, :cond_3

    const/4 v1, 0x7

    if-eq v0, v1, :cond_3

    const/16 v1, 0x8

    if-eq v0, v1, :cond_3

    const/16 v1, 0x9

    if-eq v0, v1, :cond_3

    const/16 v1, 0xa

    if-eq v0, v1, :cond_3

    const/16 v1, 0xb

    if-eq v0, v1, :cond_3

    const/16 v1, 0xc

    if-eq v0, v1, :cond_3

    const/16 v1, 0xd

    if-eq v0, v1, :cond_3

    const/16 v1, 0xe

    if-eq v0, v1, :cond_3

    const/16 v1, 0xf

    if-eq v0, v1, :cond_3

    const/16 v1, 0x10

    if-eq v0, v1, :cond_3

    const/16 v1, 0x11

    if-eq v0, v1, :cond_3

    const/16 v1, 0x12

    if-eq v0, v1, :cond_3

    const/16 v1, 0x13

    if-eq v0, v1, :cond_3

    const/16 v1, 0x14

    if-eq v0, v1, :cond_3

    const/16 v1, 0x15

    if-eq v0, v1, :cond_3

    const/16 v1, 0x16

    if-ne v0, v1, :cond_4

    .line 430
    :cond_3
    iput v0, p0, Lopf;->b:I

    goto/16 :goto_0

    .line 432
    :cond_4
    const/4 v0, 0x0

    iput v0, p0, Lopf;->b:I

    goto/16 :goto_0

    .line 437
    :sswitch_3
    iget-object v0, p0, Lopf;->d:Loou;

    if-nez v0, :cond_5

    .line 438
    new-instance v0, Loou;

    invoke-direct {v0}, Loou;-><init>()V

    iput-object v0, p0, Lopf;->d:Loou;

    .line 440
    :cond_5
    iget-object v0, p0, Lopf;->d:Loou;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 444
    :sswitch_4
    iget-object v0, p0, Lopf;->e:Loow;

    if-nez v0, :cond_6

    .line 445
    new-instance v0, Loow;

    invoke-direct {v0}, Loow;-><init>()V

    iput-object v0, p0, Lopf;->e:Loow;

    .line 447
    :cond_6
    iget-object v0, p0, Lopf;->e:Loow;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 451
    :sswitch_5
    iget-object v0, p0, Lopf;->f:Looy;

    if-nez v0, :cond_7

    .line 452
    new-instance v0, Looy;

    invoke-direct {v0}, Looy;-><init>()V

    iput-object v0, p0, Lopf;->f:Looy;

    .line 454
    :cond_7
    iget-object v0, p0, Lopf;->f:Looy;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 458
    :sswitch_6
    iget-object v0, p0, Lopf;->g:Lopa;

    if-nez v0, :cond_8

    .line 459
    new-instance v0, Lopa;

    invoke-direct {v0}, Lopa;-><init>()V

    iput-object v0, p0, Lopf;->g:Lopa;

    .line 461
    :cond_8
    iget-object v0, p0, Lopf;->g:Lopa;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 465
    :sswitch_7
    iget-object v0, p0, Lopf;->h:Lopc;

    if-nez v0, :cond_9

    .line 466
    new-instance v0, Lopc;

    invoke-direct {v0}, Lopc;-><init>()V

    iput-object v0, p0, Lopf;->h:Lopc;

    .line 468
    :cond_9
    iget-object v0, p0, Lopf;->h:Lopc;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 472
    :sswitch_8
    iget-object v0, p0, Lopf;->i:Lpij;

    if-nez v0, :cond_a

    .line 473
    new-instance v0, Lpij;

    invoke-direct {v0}, Lpij;-><init>()V

    iput-object v0, p0, Lopf;->i:Lpij;

    .line 475
    :cond_a
    iget-object v0, p0, Lopf;->i:Lpij;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 479
    :sswitch_9
    iget-object v0, p0, Lopf;->j:Lpgb;

    if-nez v0, :cond_b

    .line 480
    new-instance v0, Lpgb;

    invoke-direct {v0}, Lpgb;-><init>()V

    iput-object v0, p0, Lopf;->j:Lpgb;

    .line 482
    :cond_b
    iget-object v0, p0, Lopf;->j:Lpgb;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 486
    :sswitch_a
    iget-object v0, p0, Lopf;->c:Lpxr;

    if-nez v0, :cond_c

    .line 487
    new-instance v0, Lpxr;

    invoke-direct {v0}, Lpxr;-><init>()V

    iput-object v0, p0, Lopf;->c:Lpxr;

    .line 489
    :cond_c
    iget-object v0, p0, Lopf;->c:Lpxr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 493
    :sswitch_b
    iget-object v0, p0, Lopf;->k:Lpub;

    if-nez v0, :cond_d

    .line 494
    new-instance v0, Lpub;

    invoke-direct {v0}, Lpub;-><init>()V

    iput-object v0, p0, Lopf;->k:Lpub;

    .line 496
    :cond_d
    iget-object v0, p0, Lopf;->k:Lpub;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 500
    :sswitch_c
    iget-object v0, p0, Lopf;->l:Lpws;

    if-nez v0, :cond_e

    .line 501
    new-instance v0, Lpws;

    invoke-direct {v0}, Lpws;-><init>()V

    iput-object v0, p0, Lopf;->l:Lpws;

    .line 503
    :cond_e
    iget-object v0, p0, Lopf;->l:Lpws;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 507
    :sswitch_d
    iget-object v0, p0, Lopf;->m:Lpxl;

    if-nez v0, :cond_f

    .line 508
    new-instance v0, Lpxl;

    invoke-direct {v0}, Lpxl;-><init>()V

    iput-object v0, p0, Lopf;->m:Lpxl;

    .line 510
    :cond_f
    iget-object v0, p0, Lopf;->m:Lpxl;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 514
    :sswitch_e
    iget-object v0, p0, Lopf;->n:Lppp;

    if-nez v0, :cond_10

    .line 515
    new-instance v0, Lppp;

    invoke-direct {v0}, Lppp;-><init>()V

    iput-object v0, p0, Lopf;->n:Lppp;

    .line 517
    :cond_10
    iget-object v0, p0, Lopf;->n:Lppp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 521
    :sswitch_f
    iget-object v0, p0, Lopf;->o:Lprm;

    if-nez v0, :cond_11

    .line 522
    new-instance v0, Lprm;

    invoke-direct {v0}, Lprm;-><init>()V

    iput-object v0, p0, Lopf;->o:Lprm;

    .line 524
    :cond_11
    iget-object v0, p0, Lopf;->o:Lprm;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 528
    :sswitch_10
    iget-object v0, p0, Lopf;->p:Lplo;

    if-nez v0, :cond_12

    .line 529
    new-instance v0, Lplo;

    invoke-direct {v0}, Lplo;-><init>()V

    iput-object v0, p0, Lopf;->p:Lplo;

    .line 531
    :cond_12
    iget-object v0, p0, Lopf;->p:Lplo;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 535
    :sswitch_11
    iget-object v0, p0, Lopf;->q:Lput;

    if-nez v0, :cond_13

    .line 536
    new-instance v0, Lput;

    invoke-direct {v0}, Lput;-><init>()V

    iput-object v0, p0, Lopf;->q:Lput;

    .line 538
    :cond_13
    iget-object v0, p0, Lopf;->q:Lput;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 542
    :sswitch_12
    iget-object v0, p0, Lopf;->r:Lpuv;

    if-nez v0, :cond_14

    .line 543
    new-instance v0, Lpuv;

    invoke-direct {v0}, Lpuv;-><init>()V

    iput-object v0, p0, Lopf;->r:Lpuv;

    .line 545
    :cond_14
    iget-object v0, p0, Lopf;->r:Lpuv;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 549
    :sswitch_13
    iget-object v0, p0, Lopf;->s:Lpxt;

    if-nez v0, :cond_15

    .line 550
    new-instance v0, Lpxt;

    invoke-direct {v0}, Lpxt;-><init>()V

    iput-object v0, p0, Lopf;->s:Lpxt;

    .line 552
    :cond_15
    iget-object v0, p0, Lopf;->s:Lpxt;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 556
    :sswitch_14
    iget-object v0, p0, Lopf;->t:Lpmo;

    if-nez v0, :cond_16

    .line 557
    new-instance v0, Lpmo;

    invoke-direct {v0}, Lpmo;-><init>()V

    iput-object v0, p0, Lopf;->t:Lpmo;

    .line 559
    :cond_16
    iget-object v0, p0, Lopf;->t:Lpmo;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 563
    :sswitch_15
    iget-object v0, p0, Lopf;->u:Lpjz;

    if-nez v0, :cond_17

    .line 564
    new-instance v0, Lpjz;

    invoke-direct {v0}, Lpjz;-><init>()V

    iput-object v0, p0, Lopf;->u:Lpjz;

    .line 566
    :cond_17
    iget-object v0, p0, Lopf;->u:Lpjz;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 570
    :sswitch_16
    iget-object v0, p0, Lopf;->v:Lpwu;

    if-nez v0, :cond_18

    .line 571
    new-instance v0, Lpwu;

    invoke-direct {v0}, Lpwu;-><init>()V

    iput-object v0, p0, Lopf;->v:Lpwu;

    .line 573
    :cond_18
    iget-object v0, p0, Lopf;->v:Lpwu;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 577
    :sswitch_17
    iget-object v0, p0, Lopf;->w:Lpnh;

    if-nez v0, :cond_19

    .line 578
    new-instance v0, Lpnh;

    invoke-direct {v0}, Lpnh;-><init>()V

    iput-object v0, p0, Lopf;->w:Lpnh;

    .line 580
    :cond_19
    iget-object v0, p0, Lopf;->w:Lpnh;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 584
    :sswitch_18
    iget-object v0, p0, Lopf;->x:Loph;

    if-nez v0, :cond_1a

    .line 585
    new-instance v0, Loph;

    invoke-direct {v0}, Loph;-><init>()V

    iput-object v0, p0, Lopf;->x:Loph;

    .line 587
    :cond_1a
    iget-object v0, p0, Lopf;->x:Loph;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 384
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
        0x82 -> :sswitch_10
        0x8a -> :sswitch_11
        0x92 -> :sswitch_12
        0x9a -> :sswitch_13
        0xa2 -> :sswitch_14
        0xaa -> :sswitch_15
        0xb2 -> :sswitch_16
        0xba -> :sswitch_17
        0xc2 -> :sswitch_18
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 198
    iget-object v0, p0, Lopf;->a:Lope;

    if-eqz v0, :cond_0

    .line 199
    const/4 v0, 0x1

    iget-object v1, p0, Lopf;->a:Lope;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 201
    :cond_0
    iget v0, p0, Lopf;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 202
    const/4 v0, 0x2

    iget v1, p0, Lopf;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 204
    :cond_1
    iget-object v0, p0, Lopf;->d:Loou;

    if-eqz v0, :cond_2

    .line 205
    const/4 v0, 0x3

    iget-object v1, p0, Lopf;->d:Loou;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 207
    :cond_2
    iget-object v0, p0, Lopf;->e:Loow;

    if-eqz v0, :cond_3

    .line 208
    const/4 v0, 0x4

    iget-object v1, p0, Lopf;->e:Loow;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 210
    :cond_3
    iget-object v0, p0, Lopf;->f:Looy;

    if-eqz v0, :cond_4

    .line 211
    const/4 v0, 0x5

    iget-object v1, p0, Lopf;->f:Looy;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 213
    :cond_4
    iget-object v0, p0, Lopf;->g:Lopa;

    if-eqz v0, :cond_5

    .line 214
    const/4 v0, 0x6

    iget-object v1, p0, Lopf;->g:Lopa;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 216
    :cond_5
    iget-object v0, p0, Lopf;->h:Lopc;

    if-eqz v0, :cond_6

    .line 217
    const/4 v0, 0x7

    iget-object v1, p0, Lopf;->h:Lopc;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 219
    :cond_6
    iget-object v0, p0, Lopf;->i:Lpij;

    if-eqz v0, :cond_7

    .line 220
    const/16 v0, 0x8

    iget-object v1, p0, Lopf;->i:Lpij;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 222
    :cond_7
    iget-object v0, p0, Lopf;->j:Lpgb;

    if-eqz v0, :cond_8

    .line 223
    const/16 v0, 0x9

    iget-object v1, p0, Lopf;->j:Lpgb;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 225
    :cond_8
    iget-object v0, p0, Lopf;->c:Lpxr;

    if-eqz v0, :cond_9

    .line 226
    const/16 v0, 0xa

    iget-object v1, p0, Lopf;->c:Lpxr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 228
    :cond_9
    iget-object v0, p0, Lopf;->k:Lpub;

    if-eqz v0, :cond_a

    .line 229
    const/16 v0, 0xb

    iget-object v1, p0, Lopf;->k:Lpub;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 231
    :cond_a
    iget-object v0, p0, Lopf;->l:Lpws;

    if-eqz v0, :cond_b

    .line 232
    const/16 v0, 0xc

    iget-object v1, p0, Lopf;->l:Lpws;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 234
    :cond_b
    iget-object v0, p0, Lopf;->m:Lpxl;

    if-eqz v0, :cond_c

    .line 235
    const/16 v0, 0xd

    iget-object v1, p0, Lopf;->m:Lpxl;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 237
    :cond_c
    iget-object v0, p0, Lopf;->n:Lppp;

    if-eqz v0, :cond_d

    .line 238
    const/16 v0, 0xe

    iget-object v1, p0, Lopf;->n:Lppp;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 240
    :cond_d
    iget-object v0, p0, Lopf;->o:Lprm;

    if-eqz v0, :cond_e

    .line 241
    const/16 v0, 0xf

    iget-object v1, p0, Lopf;->o:Lprm;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 243
    :cond_e
    iget-object v0, p0, Lopf;->p:Lplo;

    if-eqz v0, :cond_f

    .line 244
    const/16 v0, 0x10

    iget-object v1, p0, Lopf;->p:Lplo;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 246
    :cond_f
    iget-object v0, p0, Lopf;->q:Lput;

    if-eqz v0, :cond_10

    .line 247
    const/16 v0, 0x11

    iget-object v1, p0, Lopf;->q:Lput;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 249
    :cond_10
    iget-object v0, p0, Lopf;->r:Lpuv;

    if-eqz v0, :cond_11

    .line 250
    const/16 v0, 0x12

    iget-object v1, p0, Lopf;->r:Lpuv;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 252
    :cond_11
    iget-object v0, p0, Lopf;->s:Lpxt;

    if-eqz v0, :cond_12

    .line 253
    const/16 v0, 0x13

    iget-object v1, p0, Lopf;->s:Lpxt;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 255
    :cond_12
    iget-object v0, p0, Lopf;->t:Lpmo;

    if-eqz v0, :cond_13

    .line 256
    const/16 v0, 0x14

    iget-object v1, p0, Lopf;->t:Lpmo;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 258
    :cond_13
    iget-object v0, p0, Lopf;->u:Lpjz;

    if-eqz v0, :cond_14

    .line 259
    const/16 v0, 0x15

    iget-object v1, p0, Lopf;->u:Lpjz;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 261
    :cond_14
    iget-object v0, p0, Lopf;->v:Lpwu;

    if-eqz v0, :cond_15

    .line 262
    const/16 v0, 0x16

    iget-object v1, p0, Lopf;->v:Lpwu;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 264
    :cond_15
    iget-object v0, p0, Lopf;->w:Lpnh;

    if-eqz v0, :cond_16

    .line 265
    const/16 v0, 0x17

    iget-object v1, p0, Lopf;->w:Lpnh;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 267
    :cond_16
    iget-object v0, p0, Lopf;->x:Loph;

    if-eqz v0, :cond_17

    .line 268
    const/16 v0, 0x18

    iget-object v1, p0, Lopf;->x:Loph;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 270
    :cond_17
    iget-object v0, p0, Lopf;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 272
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 91
    invoke-virtual {p0, p1}, Lopf;->a(Loxn;)Lopf;

    move-result-object v0

    return-object v0
.end method
