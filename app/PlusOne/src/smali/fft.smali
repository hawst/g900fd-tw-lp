.class public final Lfft;
.super Landroid/widget/BaseAdapter;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/plus/service/DreamSettingsActivity;

.field private final b:Landroid/view/LayoutInflater;

.field private final c:Landroid/content/Context;

.field private d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private e:Landroid/database/Cursor;

.field private f:Landroid/database/Cursor;

.field private g:Landroid/database/Cursor;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/service/DreamSettingsActivity;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 223
    iput-object p1, p0, Lfft;->a:Lcom/google/android/apps/plus/service/DreamSettingsActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 218
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lfft;->d:Ljava/util/Set;

    .line 224
    iput-object p2, p0, Lfft;->c:Landroid/content/Context;

    .line 225
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lfft;->b:Landroid/view/LayoutInflater;

    .line 226
    iget-object v0, p0, Lfft;->c:Landroid/content/Context;

    invoke-static {p1}, Lcom/google/android/apps/plus/service/DreamSettingsActivity;->a(Lcom/google/android/apps/plus/service/DreamSettingsActivity;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/service/EsDreamService;->b(Landroid/content/Context;I)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lfft;->d:Ljava/util/Set;

    .line 227
    return-void
.end method

.method static synthetic a(Lfft;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lfft;->d:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic a(Lfft;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 215
    iget-object v0, p0, Lfft;->c:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/apps/plus/service/EsDreamService;->a(Landroid/content/Context;I)V

    iget-object v0, p0, Lfft;->c:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/apps/plus/service/EsDreamService;->b(Landroid/content/Context;I)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lfft;->d:Ljava/util/Set;

    iput-object v3, p0, Lfft;->g:Landroid/database/Cursor;

    iget-object v0, p0, Lfft;->f:Landroid/database/Cursor;

    invoke-virtual {p0, v0, v3}, Lfft;->a(Landroid/database/Cursor;Landroid/database/Cursor;)V

    iget-object v0, p0, Lfft;->a:Lcom/google/android/apps/plus/service/DreamSettingsActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/DreamSettingsActivity;->g()Lbb;

    move-result-object v0

    const/4 v1, 0x1

    iget-object v2, p0, Lfft;->a:Lcom/google/android/apps/plus/service/DreamSettingsActivity;

    invoke-static {v2}, Lcom/google/android/apps/plus/service/DreamSettingsActivity;->c(Lcom/google/android/apps/plus/service/DreamSettingsActivity;)Lbc;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    return-void
.end method


# virtual methods
.method public a(Landroid/database/Cursor;Landroid/database/Cursor;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 249
    iget-object v0, p0, Lfft;->a:Lcom/google/android/apps/plus/service/DreamSettingsActivity;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/DreamSettingsActivity;->d(Lcom/google/android/apps/plus/service/DreamSettingsActivity;)Landroid/database/Cursor;

    move-result-object v0

    .line 251
    if-eqz p1, :cond_2

    .line 252
    iput-object p1, p0, Lfft;->f:Landroid/database/Cursor;

    .line 261
    :cond_0
    :goto_0
    if-eqz p2, :cond_3

    .line 262
    iput-object p2, p0, Lfft;->g:Landroid/database/Cursor;

    .line 271
    :cond_1
    :goto_1
    iget-object v1, p0, Lfft;->c:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsDreamService;->a(Landroid/content/Context;)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_4

    .line 272
    new-instance v1, Landroid/database/MergeCursor;

    const/4 v2, 0x3

    new-array v2, v2, [Landroid/database/Cursor;

    aput-object v0, v2, v4

    iget-object v0, p0, Lfft;->f:Landroid/database/Cursor;

    aput-object v0, v2, v5

    iget-object v0, p0, Lfft;->g:Landroid/database/Cursor;

    aput-object v0, v2, v6

    invoke-direct {v1, v2}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    iput-object v1, p0, Lfft;->e:Landroid/database/Cursor;

    .line 276
    :goto_2
    invoke-virtual {p0}, Lfft;->notifyDataSetChanged()V

    .line 277
    return-void

    .line 253
    :cond_2
    iget-object v1, p0, Lfft;->f:Landroid/database/Cursor;

    if-nez v1, :cond_0

    .line 255
    new-instance v1, Landroid/database/MergeCursor;

    new-array v2, v6, [Landroid/database/Cursor;

    iget-object v3, p0, Lfft;->c:Landroid/content/Context;

    .line 256
    invoke-static {v3}, Lcom/google/android/apps/plus/service/DreamSettingsActivity;->a(Landroid/content/Context;)Landroid/database/Cursor;

    move-result-object v3

    aput-object v3, v2, v4

    .line 257
    invoke-static {}, Lcom/google/android/apps/plus/service/DreamSettingsActivity;->h()Landroid/database/Cursor;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-direct {v1, v2}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    iput-object v1, p0, Lfft;->f:Landroid/database/Cursor;

    goto :goto_0

    .line 263
    :cond_3
    iget-object v1, p0, Lfft;->g:Landroid/database/Cursor;

    if-nez v1, :cond_1

    .line 265
    new-instance v1, Landroid/database/MergeCursor;

    new-array v2, v6, [Landroid/database/Cursor;

    iget-object v3, p0, Lfft;->c:Landroid/content/Context;

    .line 266
    invoke-static {v3}, Lcom/google/android/apps/plus/service/DreamSettingsActivity;->b(Landroid/content/Context;)Landroid/database/Cursor;

    move-result-object v3

    aput-object v3, v2, v4

    .line 267
    invoke-static {}, Lcom/google/android/apps/plus/service/DreamSettingsActivity;->h()Landroid/database/Cursor;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-direct {v1, v2}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    iput-object v1, p0, Lfft;->g:Landroid/database/Cursor;

    goto :goto_1

    .line 274
    :cond_4
    iput-object v0, p0, Lfft;->e:Landroid/database/Cursor;

    goto :goto_2
.end method

.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 311
    const/4 v0, 0x0

    return v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 281
    iget-object v0, p0, Lfft;->e:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfft;->e:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 286
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 291
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 2

    .prologue
    .line 305
    iget-object v0, p0, Lfft;->e:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 306
    iget-object v0, p0, Lfft;->e:Landroid/database/Cursor;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    const v6, 0x7f100118

    const/4 v5, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 321
    iget-object v0, p0, Lfft;->e:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 322
    invoke-virtual {p0, p1}, Lfft;->getItemViewType(I)I

    move-result v1

    .line 323
    if-nez p2, :cond_0

    .line 324
    if-nez v1, :cond_2

    iget-object v0, p0, Lfft;->b:Landroid/view/LayoutInflater;

    const v4, 0x7f040081

    invoke-virtual {v0, v4, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    :goto_0
    move-object p2, v0

    .line 326
    :cond_0
    if-nez v1, :cond_5

    iget-object v0, p0, Lfft;->e:Landroid/database/Cursor;

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lfft;->e:Landroid/database/Cursor;

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lfft;->e:Landroid/database/Cursor;

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f100244

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    invoke-static {v2, v4}, Lcom/google/android/apps/plus/service/EsDreamService;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lfft;->d:Ljava/util/Set;

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    new-instance v0, Lffv;

    invoke-direct {v0, p0, v2}, Lffv;-><init>(Lfft;Ljava/lang/String;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 327
    :cond_1
    :goto_1
    return-object p2

    .line 324
    :cond_2
    if-ne v1, v2, :cond_3

    iget-object v0, p0, Lfft;->b:Landroid/view/LayoutInflater;

    const v4, 0x7f040082

    invoke-virtual {v0, v4, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :cond_3
    if-ne v1, v5, :cond_4

    iget-object v0, p0, Lfft;->b:Landroid/view/LayoutInflater;

    const v4, 0x7f04007f

    invoke-virtual {v0, v4, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lfft;->b:Landroid/view/LayoutInflater;

    const v4, 0x7f040083

    invoke-virtual {v0, v4, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 326
    :cond_5
    if-ne v1, v2, :cond_6

    iget-object v0, p0, Lfft;->e:Landroid/database/Cursor;

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_6
    if-ne v1, v5, :cond_1

    iget-object v0, p0, Lfft;->e:Landroid/database/Cursor;

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iget-object v0, p0, Lfft;->e:Landroid/database/Cursor;

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f100241

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lfft;->a:Lcom/google/android/apps/plus/service/DreamSettingsActivity;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/DreamSettingsActivity;->a(Lcom/google/android/apps/plus/service/DreamSettingsActivity;)I

    move-result v0

    const/4 v5, -0x1

    if-eq v0, v5, :cond_7

    if-ne v0, v4, :cond_7

    move v0, v2

    :goto_2
    invoke-virtual {v1, v0}, Landroid/widget/RadioButton;->setChecked(Z)V

    new-instance v0, Lffu;

    invoke-direct {v0, p0, v1, v4}, Lffu;-><init>(Lfft;Landroid/widget/RadioButton;I)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    :cond_7
    move v0, v3

    goto :goto_2
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 300
    const/4 v0, 0x4

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1

    .prologue
    .line 316
    invoke-virtual {p0, p1}, Lfft;->getItemViewType(I)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
