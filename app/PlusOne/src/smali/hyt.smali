.class public Lhyt;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lhyt;",
            ">;"
        }
    .end annotation
.end field

.field private static final a:Lloy;


# instance fields
.field private b:Loxu;

.field private c:[B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 19
    new-instance v0, Lloy;

    const-string v1, "debug.social.die_hard"

    invoke-direct {v0, v1}, Lloy;-><init>(Ljava/lang/String;)V

    sput-object v0, Lhyt;->a:Lloy;

    .line 114
    new-instance v0, Lhyu;

    invoke-direct {v0}, Lhyu;-><init>()V

    sput-object v0, Lhyt;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 111
    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v0

    iput-object v0, p0, Lhyt;->c:[B

    .line 112
    return-void
.end method

.method public constructor <init>(Loxu;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lhyt;->b:Loxu;

    .line 33
    return-void
.end method


# virtual methods
.method public a(Loxu;)Loxu;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Loxu;",
            ">(TT;)TT;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 49
    iget-object v1, p0, Lhyt;->b:Loxu;

    if-nez v1, :cond_0

    iget-object v1, p0, Lhyt;->c:[B

    if-eqz v1, :cond_0

    .line 51
    :try_start_0
    iget-object v1, p0, Lhyt;->c:[B

    invoke-static {p1, v1}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v1

    iput-object v1, p0, Lhyt;->b:Loxu;

    .line 52
    const/4 v1, 0x0

    iput-object v1, p0, Lhyt;->c:[B
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    .line 59
    :cond_0
    :goto_0
    :try_start_1
    iget-object v0, p0, Lhyt;->b:Loxu;
    :try_end_1
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_1} :catch_1

    .line 73
    :goto_1
    return-object v0

    .line 53
    :catch_0
    move-exception v1

    .line 54
    const-string v2, "MessageNano"

    const-string v3, "Failed to deserialize"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 55
    sget-object v1, Lhyt;->a:Lloy;

    goto :goto_0

    .line 61
    :catch_1
    move-exception v1

    sget-object v1, Lhyt;->a:Lloy;

    .line 67
    :try_start_2
    iget-object v1, p0, Lhyt;->b:Loxu;

    invoke-static {v1}, Loxu;->a(Loxu;)[B

    move-result-object v1

    invoke-static {p1, v1}, Loxu;->a(Loxu;[B)Loxu;
    :try_end_2
    .catch Loxt; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v0

    goto :goto_1

    .line 71
    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 98
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 79
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ParcelableMessageNano("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 80
    iget-object v1, p0, Lhyt;->b:Loxu;

    if-eqz v1, :cond_0

    .line 81
    iget-object v1, p0, Lhyt;->b:Loxu;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 87
    :goto_0
    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 82
    :cond_0
    iget-object v1, p0, Lhyt;->c:[B

    if-eqz v1, :cond_1

    .line 83
    iget-object v1, p0, Lhyt;->c:[B

    array-length v1, v1

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x11

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "byte["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 85
    :cond_1
    const-string v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lhyt;->c:[B

    if-nez v0, :cond_0

    iget-object v0, p0, Lhyt;->b:Loxu;

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lhyt;->b:Loxu;

    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    iput-object v0, p0, Lhyt;->c:[B

    .line 105
    const/4 v0, 0x0

    iput-object v0, p0, Lhyt;->b:Loxu;

    .line 107
    :cond_0
    iget-object v0, p0, Lhyt;->c:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 108
    return-void
.end method
