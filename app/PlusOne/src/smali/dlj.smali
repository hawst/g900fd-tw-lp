.class public final Ldlj;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmfc;",
        "Lmfd;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final p:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)V
    .locals 6

    .prologue
    .line 31
    const-string v3, "modifycircleproperties"

    new-instance v4, Lmfc;

    invoke-direct {v4}, Lmfc;-><init>()V

    new-instance v5, Lmfd;

    invoke-direct {v5}, Lmfd;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 34
    iput-object p3, p0, Ldlj;->a:Ljava/lang/String;

    .line 35
    iput-object p4, p0, Ldlj;->b:Ljava/lang/String;

    .line 36
    iput-boolean p5, p0, Ldlj;->p:Z

    .line 37
    return-void
.end method


# virtual methods
.method protected a(Lmfc;)V
    .locals 3

    .prologue
    .line 41
    new-instance v0, Lojp;

    invoke-direct {v0}, Lojp;-><init>()V

    iput-object v0, p1, Lmfc;->a:Lojp;

    .line 42
    iget-object v0, p1, Lmfc;->a:Lojp;

    .line 43
    iget-object v1, p0, Ldlj;->a:Ljava/lang/String;

    const-string v2, "f."

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 44
    iget-object v1, p0, Ldlj;->a:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lojp;->a:Ljava/lang/String;

    .line 49
    :goto_0
    iget-object v1, p0, Ldlj;->b:Ljava/lang/String;

    iput-object v1, v0, Lojp;->b:Ljava/lang/String;

    .line 50
    iget-boolean v1, p0, Ldlj;->p:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lojp;->c:Ljava/lang/Boolean;

    .line 51
    return-void

    .line 46
    :cond_0
    iget-object v1, p0, Ldlj;->a:Ljava/lang/String;

    iput-object v1, v0, Lojp;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 14
    check-cast p1, Lmfc;

    invoke-virtual {p0, p1}, Ldlj;->a(Lmfc;)V

    return-void
.end method

.method protected b()V
    .locals 5

    .prologue
    .line 55
    iget-object v0, p0, Ldlj;->f:Landroid/content/Context;

    iget v1, p0, Ldlj;->c:I

    iget-object v2, p0, Ldlj;->a:Ljava/lang/String;

    iget-object v3, p0, Ldlj;->b:Ljava/lang/String;

    iget-boolean v4, p0, Ldlj;->p:Z

    invoke-static {v0, v1, v2, v3, v4}, Ldsm;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)V

    .line 57
    return-void
.end method

.method protected bridge synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 14
    invoke-virtual {p0}, Ldlj;->b()V

    return-void
.end method
