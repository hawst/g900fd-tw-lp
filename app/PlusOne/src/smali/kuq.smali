.class public final Lkuq;
.super Lhyd;
.source "PG"


# instance fields
.field private e:I

.field private f:Lkut;

.field private g:Landroid/view/LayoutInflater;

.field private h:Liax;

.field private i:Lkvq;

.field private j:Llkc;

.field private k:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;ILkut;Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;Lkvq;Llkc;Z)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 51
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lhyd;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    .line 53
    iput p2, p0, Lkuq;->e:I

    .line 54
    iput-object p3, p0, Lkuq;->f:Lkut;

    .line 55
    iput-object p5, p0, Lkuq;->i:Lkvq;

    .line 56
    iput-object p6, p0, Lkuq;->j:Llkc;

    .line 57
    iput-boolean p7, p0, Lkuq;->k:Z

    .line 59
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lkuq;->g:Landroid/view/LayoutInflater;

    .line 61
    const-class v0, Liax;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liax;

    iput-object v0, p0, Lkuq;->h:Liax;

    .line 63
    const/4 v0, 0x2

    invoke-virtual {p4, v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->c(I)V

    .line 64
    invoke-static {p1}, Llcm;->b(Landroid/content/Context;)I

    move-result v0

    invoke-virtual {p4, v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(I)V

    .line 66
    invoke-static {p1}, Llcm;->a(Landroid/content/Context;)I

    move-result v2

    .line 67
    invoke-virtual {p4, v2}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->b(I)V

    .line 68
    iget-boolean v0, p0, Lkuq;->k:Z

    if-eqz v0, :cond_0

    neg-int v0, v2

    div-int/lit8 v0, v0, 0x2

    :goto_0
    invoke-virtual {p4, v2, v0, v2, v1}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->setPadding(IIII)V

    .line 71
    new-instance v0, Lkur;

    invoke-direct {v0}, Lkur;-><init>()V

    invoke-virtual {p4, v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(Llkg;)V

    .line 81
    new-instance v0, Lkus;

    invoke-direct {v0, p0}, Lkus;-><init>(Lkuq;)V

    invoke-virtual {p4, v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(Llkc;)V

    .line 112
    return-void

    :cond_0
    move v0, v1

    .line 68
    goto :goto_0
.end method

.method static synthetic a(Lkuq;)Liax;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lkuq;->h:Liax;

    return-object v0
.end method

.method static synthetic b(Lkuq;)Llkc;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lkuq;->j:Llkc;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v6, 0x2

    const/4 v5, -0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 155
    const/16 v1, 0x11

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 190
    :goto_0
    return-object v0

    .line 157
    :pswitch_0
    check-cast p3, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    .line 158
    const/4 v1, 0x5

    .line 159
    invoke-static {p1, v0, v2, v1}, Llhx;->a(Landroid/content/Context;Landroid/util/AttributeSet;II)Landroid/widget/TextView;

    move-result-object v0

    .line 161
    new-instance v1, Llka;

    const/4 v2, -0x3

    .line 164
    invoke-virtual {p3}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->b()I

    move-result v3

    invoke-direct {v1, v6, v2, v3, v4}, Llka;-><init>(IIII)V

    .line 165
    iput v5, v1, Llka;->height:I

    .line 166
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 170
    :pswitch_1
    const/16 v0, 0x13

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    packed-switch v0, :pswitch_data_1

    .line 177
    iget-object v0, p0, Lkuq;->g:Landroid/view/LayoutInflater;

    const v1, 0x7f0401f7

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 180
    :goto_1
    new-instance v1, Llka;

    invoke-direct {v1, v6, v5, v4, v4}, Llka;-><init>(IIII)V

    .line 184
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 173
    :pswitch_2
    iget-object v0, p0, Lkuq;->g:Landroid/view/LayoutInflater;

    const v1, 0x7f0401f6

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_1

    .line 155
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 170
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
    .end packed-switch
.end method

.method public a(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 4

    .prologue
    .line 195
    const/16 v0, 0x11

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 203
    :goto_0
    return-void

    .line 197
    :pswitch_0
    check-cast p1, Landroid/widget/TextView;

    const/16 v0, 0x12

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    packed-switch v0, :pswitch_data_1

    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x26

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unknown square header type "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_1
    const v0, 0x7f0a04da

    :goto_1
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :pswitch_2
    const v0, 0x7f0a04d9

    goto :goto_1

    :pswitch_3
    const v0, 0x7f0a04db

    goto :goto_1

    .line 200
    :pswitch_4
    invoke-virtual {p0, p1, p3}, Lkuq;->a(Landroid/view/View;Landroid/database/Cursor;)V

    goto :goto_0

    .line 195
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_4
    .end packed-switch

    .line 197
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected a(Landroid/view/View;Landroid/database/Cursor;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 227
    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    invoke-virtual {p0}, Lkuq;->getCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 228
    const/16 v0, 0x13

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 229
    check-cast p1, Lcom/google/android/libraries/social/squares/list/SquareListItemView;

    .line 231
    const/4 v2, 0x2

    if-ne v0, v2, :cond_1

    move v0, v1

    .line 232
    :goto_0
    iget-object v2, p0, Lkuq;->f:Lkut;

    invoke-virtual {p1, p2, v2, v1, v0}, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->a(Landroid/database/Cursor;Lkut;ZZ)V

    .line 234
    iget-object v0, p0, Lkuq;->i:Lkvq;

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->a(Lkvq;)V

    .line 236
    :cond_0
    return-void

    .line 231
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()V
    .locals 4

    .prologue
    .line 242
    iget-object v0, p0, Lkuq;->h:Liax;

    iget-object v1, p0, Lkuq;->c:Landroid/content/Context;

    iget v2, p0, Lkuq;->e:I

    const/16 v3, 0x1b

    invoke-interface {v0, v1, v2, v3}, Liax;->a(Landroid/content/Context;II)V

    .line 244
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 248
    iget-boolean v0, p0, Lkuq;->k:Z

    if-eqz v0, :cond_0

    .line 249
    invoke-super {p0}, Lhyd;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 251
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lhyd;->getCount()I

    move-result v0

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 126
    iget-boolean v0, p0, Lkuq;->k:Z

    if-eqz v0, :cond_1

    .line 127
    if-nez p1, :cond_0

    .line 128
    const/4 v0, -0x1

    .line 149
    :goto_0
    return v0

    .line 130
    :cond_0
    add-int/lit8 p1, p1, -0x1

    .line 132
    :cond_1
    invoke-virtual {p0, p1}, Lkuq;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 134
    if-nez v0, :cond_2

    move v0, v1

    .line 135
    goto :goto_0

    .line 138
    :cond_2
    const/16 v2, 0x11

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    move v0, v1

    .line 149
    goto :goto_0

    .line 140
    :pswitch_0
    const/4 v0, 0x2

    goto :goto_0

    .line 142
    :pswitch_1
    const/16 v2, 0x13

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    packed-switch v0, :pswitch_data_1

    move v0, v1

    .line 146
    goto :goto_0

    .line 144
    :pswitch_2
    const/4 v0, 0x1

    goto :goto_0

    .line 138
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 142
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
    .end packed-switch
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    .line 257
    iget-boolean v0, p0, Lkuq;->k:Z

    if-eqz v0, :cond_1

    .line 258
    if-nez p1, :cond_0

    .line 259
    new-instance v0, Landroid/view/View;

    iget-object v1, p0, Lkuq;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 260
    check-cast p3, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    .line 261
    new-instance v1, Llka;

    const/4 v2, 0x2

    const/4 v3, -0x3

    .line 263
    invoke-virtual {p3}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->b()I

    move-result v4

    const/4 v5, 0x1

    invoke-direct {v1, v2, v3, v4, v5}, Llka;-><init>(IIII)V

    .line 265
    iget-object v2, p0, Lkuq;->c:Landroid/content/Context;

    invoke-static {v2}, Ljgh;->a(Landroid/content/Context;)F

    move-result v2

    float-to-int v2, v2

    iput v2, v1, Llka;->height:I

    .line 266
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 271
    :goto_0
    return-object v0

    .line 269
    :cond_0
    add-int/lit8 p1, p1, -0x1

    .line 271
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lhyd;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 121
    const/4 v0, 0x3

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 116
    const/4 v0, 0x0

    return v0
.end method
