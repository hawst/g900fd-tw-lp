.class public final Lgaq;
.super Lgap;
.source "PG"


# instance fields
.field private b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 23
    invoke-direct {p0}, Lgap;-><init>()V

    .line 24
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lgaq;->b:Ljava/util/HashMap;

    .line 25
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 49
    iget-object v0, p0, Lgaq;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 50
    iget-object v0, p0, Lgaq;->a:Landroid/widget/Button;

    if-eqz v0, :cond_0

    .line 51
    iget-object v0, p0, Lgaq;->a:Landroid/widget/Button;

    invoke-static {v0}, Llii;->h(Landroid/view/View;)V

    .line 52
    iget-object v0, p0, Lgaq;->a:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setScaleX(F)V

    .line 53
    iget-object v0, p0, Lgaq;->a:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setScaleY(F)V

    .line 56
    :cond_0
    invoke-super {p0}, Lgap;->a()V

    .line 57
    return-void
.end method

.method public a(Landroid/widget/Button;Lgbz;)V
    .locals 2

    .prologue
    .line 29
    invoke-super {p0, p1, p2}, Lgap;->a(Landroid/widget/Button;Lgbz;)V

    .line 30
    iget-object v0, p0, Lgaq;->b:Ljava/util/HashMap;

    const-string v1, "ButtonOn"

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    return-void
.end method

.method public b()V
    .locals 4

    .prologue
    .line 35
    iget-object v0, p0, Lgaq;->a:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getContext()Landroid/content/Context;

    .line 36
    invoke-static {}, Llsj;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 45
    :goto_0
    return-void

    .line 39
    :cond_0
    invoke-static {}, Lhni;->a()Lhni;

    move-result-object v0

    iget-object v1, p0, Lgaq;->a:Landroid/widget/Button;

    .line 41
    invoke-virtual {v1}, Landroid/widget/Button;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f080025

    iget-object v3, p0, Lgaq;->b:Ljava/util/HashMap;

    .line 40
    invoke-virtual {v0, v1, v2, v3}, Lhni;->a(Landroid/content/Context;ILjava/util/Map;)Landroid/animation/Animator;

    move-result-object v0

    .line 44
    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    goto :goto_0
.end method
