.class public final Lmtm;
.super Loxq;
.source "PG"


# instance fields
.field private a:[I

.field private b:[Lmud;

.field private c:[Lmue;

.field private d:[Lmvm;

.field private e:Ljava/lang/Integer;

.field private f:Ljava/lang/Float;

.field private g:Ljava/lang/Float;

.field private h:Ljava/lang/Float;

.field private i:Ljava/lang/Integer;

.field private j:Ljava/lang/Integer;

.field private k:Ljava/lang/Integer;

.field private l:Ljava/lang/Integer;

.field private m:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1835
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1848
    sget-object v0, Loxx;->a:[I

    iput-object v0, p0, Lmtm;->a:[I

    .line 1851
    sget-object v0, Lmud;->a:[Lmud;

    iput-object v0, p0, Lmtm;->b:[Lmud;

    .line 1854
    sget-object v0, Lmue;->a:[Lmue;

    iput-object v0, p0, Lmtm;->c:[Lmue;

    .line 1857
    sget-object v0, Lmvm;->a:[Lmvm;

    iput-object v0, p0, Lmtm;->d:[Lmvm;

    .line 1835
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1939
    .line 1940
    iget-object v0, p0, Lmtm;->a:[I

    if-eqz v0, :cond_10

    iget-object v0, p0, Lmtm;->a:[I

    array-length v0, v0

    if-lez v0, :cond_10

    .line 1942
    iget-object v3, p0, Lmtm;->a:[I

    array-length v4, v3

    move v0, v1

    move v2, v1

    :goto_0
    if-ge v0, v4, :cond_0

    aget v5, v3, v0

    .line 1944
    invoke-static {v5}, Loxo;->i(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 1942
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1947
    :cond_0
    iget-object v0, p0, Lmtm;->a:[I

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v2

    .line 1949
    :goto_1
    iget-object v2, p0, Lmtm;->b:[Lmud;

    if-eqz v2, :cond_2

    .line 1950
    iget-object v3, p0, Lmtm;->b:[Lmud;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 1951
    if-eqz v5, :cond_1

    .line 1952
    const/4 v6, 0x2

    .line 1953
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 1950
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1957
    :cond_2
    iget-object v2, p0, Lmtm;->c:[Lmue;

    if-eqz v2, :cond_4

    .line 1958
    iget-object v3, p0, Lmtm;->c:[Lmue;

    array-length v4, v3

    move v2, v1

    :goto_3
    if-ge v2, v4, :cond_4

    aget-object v5, v3, v2

    .line 1959
    if-eqz v5, :cond_3

    .line 1960
    const/4 v6, 0x3

    .line 1961
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 1958
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 1965
    :cond_4
    iget-object v2, p0, Lmtm;->d:[Lmvm;

    if-eqz v2, :cond_6

    .line 1966
    iget-object v2, p0, Lmtm;->d:[Lmvm;

    array-length v3, v2

    :goto_4
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    .line 1967
    if-eqz v4, :cond_5

    .line 1968
    const/4 v5, 0x4

    .line 1969
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 1966
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 1973
    :cond_6
    iget-object v1, p0, Lmtm;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    .line 1974
    const/4 v1, 0x5

    iget-object v2, p0, Lmtm;->e:Ljava/lang/Integer;

    .line 1975
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1977
    :cond_7
    iget-object v1, p0, Lmtm;->f:Ljava/lang/Float;

    if-eqz v1, :cond_8

    .line 1978
    const/4 v1, 0x6

    iget-object v2, p0, Lmtm;->f:Ljava/lang/Float;

    .line 1979
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 1981
    :cond_8
    iget-object v1, p0, Lmtm;->g:Ljava/lang/Float;

    if-eqz v1, :cond_9

    .line 1982
    const/4 v1, 0x7

    iget-object v2, p0, Lmtm;->g:Ljava/lang/Float;

    .line 1983
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 1985
    :cond_9
    iget-object v1, p0, Lmtm;->h:Ljava/lang/Float;

    if-eqz v1, :cond_a

    .line 1986
    const/16 v1, 0x8

    iget-object v2, p0, Lmtm;->h:Ljava/lang/Float;

    .line 1987
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 1989
    :cond_a
    iget-object v1, p0, Lmtm;->i:Ljava/lang/Integer;

    if-eqz v1, :cond_b

    .line 1990
    const/16 v1, 0x9

    iget-object v2, p0, Lmtm;->i:Ljava/lang/Integer;

    .line 1991
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1993
    :cond_b
    iget-object v1, p0, Lmtm;->j:Ljava/lang/Integer;

    if-eqz v1, :cond_c

    .line 1994
    const/16 v1, 0xa

    iget-object v2, p0, Lmtm;->j:Ljava/lang/Integer;

    .line 1995
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1997
    :cond_c
    iget-object v1, p0, Lmtm;->k:Ljava/lang/Integer;

    if-eqz v1, :cond_d

    .line 1998
    const/16 v1, 0xb

    iget-object v2, p0, Lmtm;->k:Ljava/lang/Integer;

    .line 1999
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2001
    :cond_d
    iget-object v1, p0, Lmtm;->l:Ljava/lang/Integer;

    if-eqz v1, :cond_e

    .line 2002
    const/16 v1, 0xc

    iget-object v2, p0, Lmtm;->l:Ljava/lang/Integer;

    .line 2003
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2005
    :cond_e
    iget-object v1, p0, Lmtm;->m:Ljava/lang/Integer;

    if-eqz v1, :cond_f

    .line 2006
    const/16 v1, 0xd

    iget-object v2, p0, Lmtm;->m:Ljava/lang/Integer;

    .line 2007
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2009
    :cond_f
    iget-object v1, p0, Lmtm;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2010
    iput v0, p0, Lmtm;->ai:I

    .line 2011
    return v0

    :cond_10
    move v0, v1

    goto/16 :goto_1
.end method

.method public a(Loxn;)Lmtm;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2019
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2020
    sparse-switch v0, :sswitch_data_0

    .line 2024
    iget-object v2, p0, Lmtm;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 2025
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmtm;->ah:Ljava/util/List;

    .line 2028
    :cond_1
    iget-object v2, p0, Lmtm;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2030
    :sswitch_0
    return-object p0

    .line 2035
    :sswitch_1
    const/16 v0, 0x8

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 2036
    iget-object v0, p0, Lmtm;->a:[I

    array-length v0, v0

    .line 2037
    add-int/2addr v2, v0

    new-array v2, v2, [I

    .line 2038
    iget-object v3, p0, Lmtm;->a:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2039
    iput-object v2, p0, Lmtm;->a:[I

    .line 2040
    :goto_1
    iget-object v2, p0, Lmtm;->a:[I

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_2

    .line 2041
    iget-object v2, p0, Lmtm;->a:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v3

    aput v3, v2, v0

    .line 2042
    invoke-virtual {p1}, Loxn;->a()I

    .line 2040
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2045
    :cond_2
    iget-object v2, p0, Lmtm;->a:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v3

    aput v3, v2, v0

    goto :goto_0

    .line 2049
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 2050
    iget-object v0, p0, Lmtm;->b:[Lmud;

    if-nez v0, :cond_4

    move v0, v1

    .line 2051
    :goto_2
    add-int/2addr v2, v0

    new-array v2, v2, [Lmud;

    .line 2052
    iget-object v3, p0, Lmtm;->b:[Lmud;

    if-eqz v3, :cond_3

    .line 2053
    iget-object v3, p0, Lmtm;->b:[Lmud;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2055
    :cond_3
    iput-object v2, p0, Lmtm;->b:[Lmud;

    .line 2056
    :goto_3
    iget-object v2, p0, Lmtm;->b:[Lmud;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 2057
    iget-object v2, p0, Lmtm;->b:[Lmud;

    new-instance v3, Lmud;

    invoke-direct {v3}, Lmud;-><init>()V

    aput-object v3, v2, v0

    .line 2058
    iget-object v2, p0, Lmtm;->b:[Lmud;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 2059
    invoke-virtual {p1}, Loxn;->a()I

    .line 2056
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 2050
    :cond_4
    iget-object v0, p0, Lmtm;->b:[Lmud;

    array-length v0, v0

    goto :goto_2

    .line 2062
    :cond_5
    iget-object v2, p0, Lmtm;->b:[Lmud;

    new-instance v3, Lmud;

    invoke-direct {v3}, Lmud;-><init>()V

    aput-object v3, v2, v0

    .line 2063
    iget-object v2, p0, Lmtm;->b:[Lmud;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 2067
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 2068
    iget-object v0, p0, Lmtm;->c:[Lmue;

    if-nez v0, :cond_7

    move v0, v1

    .line 2069
    :goto_4
    add-int/2addr v2, v0

    new-array v2, v2, [Lmue;

    .line 2070
    iget-object v3, p0, Lmtm;->c:[Lmue;

    if-eqz v3, :cond_6

    .line 2071
    iget-object v3, p0, Lmtm;->c:[Lmue;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2073
    :cond_6
    iput-object v2, p0, Lmtm;->c:[Lmue;

    .line 2074
    :goto_5
    iget-object v2, p0, Lmtm;->c:[Lmue;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_8

    .line 2075
    iget-object v2, p0, Lmtm;->c:[Lmue;

    new-instance v3, Lmue;

    invoke-direct {v3}, Lmue;-><init>()V

    aput-object v3, v2, v0

    .line 2076
    iget-object v2, p0, Lmtm;->c:[Lmue;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 2077
    invoke-virtual {p1}, Loxn;->a()I

    .line 2074
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 2068
    :cond_7
    iget-object v0, p0, Lmtm;->c:[Lmue;

    array-length v0, v0

    goto :goto_4

    .line 2080
    :cond_8
    iget-object v2, p0, Lmtm;->c:[Lmue;

    new-instance v3, Lmue;

    invoke-direct {v3}, Lmue;-><init>()V

    aput-object v3, v2, v0

    .line 2081
    iget-object v2, p0, Lmtm;->c:[Lmue;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 2085
    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 2086
    iget-object v0, p0, Lmtm;->d:[Lmvm;

    if-nez v0, :cond_a

    move v0, v1

    .line 2087
    :goto_6
    add-int/2addr v2, v0

    new-array v2, v2, [Lmvm;

    .line 2088
    iget-object v3, p0, Lmtm;->d:[Lmvm;

    if-eqz v3, :cond_9

    .line 2089
    iget-object v3, p0, Lmtm;->d:[Lmvm;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2091
    :cond_9
    iput-object v2, p0, Lmtm;->d:[Lmvm;

    .line 2092
    :goto_7
    iget-object v2, p0, Lmtm;->d:[Lmvm;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_b

    .line 2093
    iget-object v2, p0, Lmtm;->d:[Lmvm;

    new-instance v3, Lmvm;

    invoke-direct {v3}, Lmvm;-><init>()V

    aput-object v3, v2, v0

    .line 2094
    iget-object v2, p0, Lmtm;->d:[Lmvm;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 2095
    invoke-virtual {p1}, Loxn;->a()I

    .line 2092
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 2086
    :cond_a
    iget-object v0, p0, Lmtm;->d:[Lmvm;

    array-length v0, v0

    goto :goto_6

    .line 2098
    :cond_b
    iget-object v2, p0, Lmtm;->d:[Lmvm;

    new-instance v3, Lmvm;

    invoke-direct {v3}, Lmvm;-><init>()V

    aput-object v3, v2, v0

    .line 2099
    iget-object v2, p0, Lmtm;->d:[Lmvm;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 2103
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmtm;->e:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 2107
    :sswitch_6
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lmtm;->f:Ljava/lang/Float;

    goto/16 :goto_0

    .line 2111
    :sswitch_7
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lmtm;->g:Ljava/lang/Float;

    goto/16 :goto_0

    .line 2115
    :sswitch_8
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lmtm;->h:Ljava/lang/Float;

    goto/16 :goto_0

    .line 2119
    :sswitch_9
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmtm;->i:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 2123
    :sswitch_a
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmtm;->j:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 2127
    :sswitch_b
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmtm;->k:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 2131
    :sswitch_c
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmtm;->l:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 2135
    :sswitch_d
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmtm;->m:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 2020
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x35 -> :sswitch_6
        0x3d -> :sswitch_7
        0x45 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1880
    iget-object v1, p0, Lmtm;->a:[I

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmtm;->a:[I

    array-length v1, v1

    if-lez v1, :cond_0

    .line 1881
    iget-object v2, p0, Lmtm;->a:[I

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget v4, v2, v1

    .line 1882
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Loxo;->a(II)V

    .line 1881
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1885
    :cond_0
    iget-object v1, p0, Lmtm;->b:[Lmud;

    if-eqz v1, :cond_2

    .line 1886
    iget-object v2, p0, Lmtm;->b:[Lmud;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 1887
    if-eqz v4, :cond_1

    .line 1888
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 1886
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1892
    :cond_2
    iget-object v1, p0, Lmtm;->c:[Lmue;

    if-eqz v1, :cond_4

    .line 1893
    iget-object v2, p0, Lmtm;->c:[Lmue;

    array-length v3, v2

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 1894
    if-eqz v4, :cond_3

    .line 1895
    const/4 v5, 0x3

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 1893
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1899
    :cond_4
    iget-object v1, p0, Lmtm;->d:[Lmvm;

    if-eqz v1, :cond_6

    .line 1900
    iget-object v1, p0, Lmtm;->d:[Lmvm;

    array-length v2, v1

    :goto_3
    if-ge v0, v2, :cond_6

    aget-object v3, v1, v0

    .line 1901
    if-eqz v3, :cond_5

    .line 1902
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 1900
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 1906
    :cond_6
    iget-object v0, p0, Lmtm;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_7

    .line 1907
    const/4 v0, 0x5

    iget-object v1, p0, Lmtm;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1909
    :cond_7
    iget-object v0, p0, Lmtm;->f:Ljava/lang/Float;

    if-eqz v0, :cond_8

    .line 1910
    const/4 v0, 0x6

    iget-object v1, p0, Lmtm;->f:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 1912
    :cond_8
    iget-object v0, p0, Lmtm;->g:Ljava/lang/Float;

    if-eqz v0, :cond_9

    .line 1913
    const/4 v0, 0x7

    iget-object v1, p0, Lmtm;->g:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 1915
    :cond_9
    iget-object v0, p0, Lmtm;->h:Ljava/lang/Float;

    if-eqz v0, :cond_a

    .line 1916
    const/16 v0, 0x8

    iget-object v1, p0, Lmtm;->h:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 1918
    :cond_a
    iget-object v0, p0, Lmtm;->i:Ljava/lang/Integer;

    if-eqz v0, :cond_b

    .line 1919
    const/16 v0, 0x9

    iget-object v1, p0, Lmtm;->i:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1921
    :cond_b
    iget-object v0, p0, Lmtm;->j:Ljava/lang/Integer;

    if-eqz v0, :cond_c

    .line 1922
    const/16 v0, 0xa

    iget-object v1, p0, Lmtm;->j:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1924
    :cond_c
    iget-object v0, p0, Lmtm;->k:Ljava/lang/Integer;

    if-eqz v0, :cond_d

    .line 1925
    const/16 v0, 0xb

    iget-object v1, p0, Lmtm;->k:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1927
    :cond_d
    iget-object v0, p0, Lmtm;->l:Ljava/lang/Integer;

    if-eqz v0, :cond_e

    .line 1928
    const/16 v0, 0xc

    iget-object v1, p0, Lmtm;->l:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1930
    :cond_e
    iget-object v0, p0, Lmtm;->m:Ljava/lang/Integer;

    if-eqz v0, :cond_f

    .line 1931
    const/16 v0, 0xd

    iget-object v1, p0, Lmtm;->m:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1933
    :cond_f
    iget-object v0, p0, Lmtm;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1935
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1831
    invoke-virtual {p0, p1}, Lmtm;->a(Loxn;)Lmtm;

    move-result-object v0

    return-object v0
.end method
