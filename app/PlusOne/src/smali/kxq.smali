.class public final Lkxq;
.super Landroid/view/View;
.source "PG"

# interfaces
.implements Lkdd;


# static fields
.field private static j:Landroid/graphics/Paint;

.field private static k:Landroid/graphics/Paint;

.field private static l:Landroid/view/animation/Interpolator;

.field private static m:Z

.field private static n:I

.field private static o:I

.field private static p:I

.field private static q:Landroid/graphics/Bitmap;

.field private static r:Landroid/graphics/drawable/Drawable;

.field private static s:I


# instance fields
.field private a:Lkzz;

.field private b:Lizu;

.field private c:Lkda;

.field private d:Landroid/graphics/Rect;

.field private e:Landroid/graphics/Rect;

.field private f:Landroid/text/StaticLayout;

.field private g:Landroid/text/StaticLayout;

.field private h:I

.field private i:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 53
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v0, Lkxq;->j:Landroid/graphics/Paint;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 67
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 69
    sget-boolean v0, Lkxq;->m:Z

    if-nez v0, :cond_0

    .line 70
    const/4 v0, 0x1

    sput-boolean v0, Lkxq;->m:Z

    .line 72
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 74
    const v1, 0x7f0d01f7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lkxq;->n:I

    .line 75
    const v1, 0x7f0d01f5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lkxq;->o:I

    .line 76
    const v1, 0x7f0d01f4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lkxq;->p:I

    .line 78
    const v1, 0x7f020199

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lkxq;->q:Landroid/graphics/Bitmap;

    .line 80
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const v2, 0x7f0b0121

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    sput-object v1, Lkxq;->r:Landroid/graphics/drawable/Drawable;

    .line 82
    const v1, 0x7f0d01f6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lkxq;->s:I

    .line 84
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 85
    sput-object v1, Lkxq;->k:Landroid/graphics/Paint;

    const v2, 0x7f0b0144

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 88
    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lkxq;->d:Landroid/graphics/Rect;

    .line 89
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lkxq;->e:Landroid/graphics/Rect;

    .line 90
    return-void
.end method


# virtual methods
.method protected a()I
    .locals 1

    .prologue
    .line 143
    sget v0, Lkxq;->o:I

    return v0
.end method

.method public a(Lkda;)V
    .locals 0

    .prologue
    .line 139
    invoke-virtual {p0}, Lkxq;->invalidate()V

    .line 140
    return-void
.end method

.method public a(Lkzz;)V
    .locals 3

    .prologue
    .line 93
    iput-object p1, p0, Lkxq;->a:Lkzz;

    .line 95
    iget-object v0, p0, Lkxq;->a:Lkzz;

    invoke-virtual {v0}, Lkzz;->c()Ljava/lang/String;

    move-result-object v0

    .line 96
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 97
    invoke-virtual {p0}, Lkxq;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Ljac;->a:Ljac;

    invoke-static {v1, v0, v2}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v0

    iput-object v0, p0, Lkxq;->b:Lizu;

    .line 99
    invoke-static {}, Llsj;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    const v0, 0x3a83126f    # 0.001f

    invoke-virtual {p0, v0}, Lkxq;->setAlpha(F)V

    .line 104
    :cond_0
    invoke-virtual {p0}, Lkxq;->b()V

    .line 105
    invoke-virtual {p0}, Lkxq;->requestLayout()V

    .line 106
    invoke-virtual {p0}, Lkxq;->invalidate()V

    .line 107
    return-void
.end method

.method public b()V
    .locals 3

    .prologue
    .line 123
    invoke-static {p0}, Llii;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lkxq;->b:Lizu;

    if-eqz v0, :cond_0

    .line 124
    invoke-virtual {p0}, Lkxq;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lizs;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizs;

    iget-object v1, p0, Lkxq;->b:Lizu;

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, p0}, Lizs;->a(Lizu;ILkdd;)Lcom/google/android/libraries/social/media/MediaResource;

    move-result-object v0

    iput-object v0, p0, Lkxq;->c:Lkda;

    .line 127
    :cond_0
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lkxq;->c:Lkda;

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lkxq;->c:Lkda;

    invoke-virtual {v0, p0}, Lkda;->unregister(Lkdd;)V

    .line 133
    const/4 v0, 0x0

    iput-object v0, p0, Lkxq;->c:Lkda;

    .line 135
    :cond_0
    return-void
.end method

.method protected d()I
    .locals 1

    .prologue
    .line 147
    sget v0, Lkxq;->p:I

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 0

    .prologue
    .line 111
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    .line 112
    invoke-virtual {p0}, Lkxq;->b()V

    .line 113
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 117
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 118
    invoke-virtual {p0}, Lkxq;->c()V

    .line 119
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 11

    .prologue
    const/4 v1, 0x0

    const/4 v10, 0x0

    const/4 v9, 0x0

    .line 193
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 195
    invoke-virtual {p0}, Lkxq;->getWidth()I

    move-result v2

    .line 197
    iget-object v0, p0, Lkxq;->c:Lkda;

    if-nez v0, :cond_4

    move-object v0, v1

    .line 199
    :goto_0
    if-nez v0, :cond_5

    .line 200
    sget-object v0, Lkxq;->r:Landroid/graphics/drawable/Drawable;

    iget-object v3, p0, Lkxq;->e:Landroid/graphics/Rect;

    invoke-virtual {v0, v3}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 201
    sget-object v0, Lkxq;->r:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 214
    :goto_1
    iget-object v0, p0, Lkxq;->e:Landroid/graphics/Rect;

    sget-object v3, Lkxq;->k:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 216
    iget v3, p0, Lkxq;->h:I

    iget-object v4, p0, Lkxq;->f:Landroid/text/StaticLayout;

    iget-object v5, p0, Lkxq;->g:Landroid/text/StaticLayout;

    sget-object v0, Lkxq;->q:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    sget v6, Lkxq;->s:I

    add-int/2addr v0, v6

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Landroid/text/StaticLayout;->getHeight()I

    move-result v6

    sget v7, Lkxq;->s:I

    add-int/2addr v6, v7

    add-int/2addr v0, v6

    :cond_0
    if-eqz v5, :cond_1

    invoke-virtual {v5}, Landroid/text/StaticLayout;->getHeight()I

    move-result v6

    sget v7, Lkxq;->s:I

    add-int/2addr v6, v7

    add-int/2addr v0, v6

    :cond_1
    sub-int v0, v3, v0

    div-int/lit8 v0, v0, 0x2

    sget-object v3, Lkxq;->q:Landroid/graphics/Bitmap;

    sget-object v6, Lkxq;->q:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    sub-int/2addr v2, v6

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    int-to-float v6, v0

    invoke-virtual {p1, v3, v2, v6, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    sget-object v1, Lkxq;->q:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    sget v2, Lkxq;->s:I

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    if-eqz v4, :cond_2

    int-to-float v1, v0

    invoke-virtual {p1, v9, v1}, Landroid/graphics/Canvas;->translate(FF)V

    invoke-virtual {v4, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v1, v0

    int-to-float v1, v1

    invoke-virtual {p1, v9, v1}, Landroid/graphics/Canvas;->translate(FF)V

    invoke-virtual {v4}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    sget v2, Lkxq;->s:I

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    :cond_2
    if-eqz v5, :cond_3

    int-to-float v1, v0

    invoke-virtual {p1, v9, v1}, Landroid/graphics/Canvas;->translate(FF)V

    invoke-virtual {v5, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p1, v9, v0}, Landroid/graphics/Canvas;->translate(FF)V

    invoke-virtual {v5}, Landroid/text/StaticLayout;->getHeight()I

    sget v0, Lkxq;->s:I

    .line 218
    :cond_3
    return-void

    .line 197
    :cond_4
    iget-object v0, p0, Lkxq;->c:Lkda;

    invoke-virtual {v0}, Lkda;->getResource()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    goto/16 :goto_0

    .line 203
    :cond_5
    iget-boolean v3, p0, Lkxq;->i:Z

    if-nez v3, :cond_8

    .line 204
    invoke-static {}, Llsj;->c()Z

    move-result v3

    if-eqz v3, :cond_7

    sget-object v3, Lkxq;->l:Landroid/view/animation/Interpolator;

    if-nez v3, :cond_6

    new-instance v3, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    sput-object v3, Lkxq;->l:Landroid/view/animation/Interpolator;

    :cond_6
    invoke-virtual {p0}, Lkxq;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v3, v4}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    const-wide/16 v4, 0x1f4

    invoke-virtual {v3, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    sget-object v4, Lkxq;->l:Landroid/view/animation/Interpolator;

    invoke-virtual {v3, v4}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    .line 205
    :cond_7
    const/4 v3, 0x1

    iput-boolean v3, p0, Lkxq;->i:Z

    .line 208
    :cond_8
    iget-object v3, p0, Lkxq;->d:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 209
    iget-object v3, p0, Lkxq;->e:Landroid/graphics/Rect;

    iget-object v4, p0, Lkxq;->d:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    int-to-float v7, v5

    int-to-float v8, v6

    div-float/2addr v7, v8

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v8

    int-to-float v8, v8

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    int-to-float v3, v3

    div-float v3, v8, v3

    cmpl-float v7, v7, v3

    if-lez v7, :cond_a

    int-to-float v7, v6

    mul-float/2addr v3, v7

    float-to-int v3, v3

    sub-int v3, v5, v3

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v5, v3

    invoke-virtual {v4, v3, v10, v5, v6}, Landroid/graphics/Rect;->set(IIII)V

    .line 211
    :cond_9
    :goto_2
    iget-object v3, p0, Lkxq;->d:Landroid/graphics/Rect;

    iget-object v4, p0, Lkxq;->e:Landroid/graphics/Rect;

    sget-object v5, Lkxq;->j:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto/16 :goto_1

    .line 209
    :cond_a
    int-to-float v7, v5

    div-float v3, v7, v3

    float-to-int v3, v3

    sub-int v3, v6, v3

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v6, v3

    invoke-virtual {v4, v10, v3, v5, v6}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_2
.end method

.method public onLayout(ZIIII)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v9, 0x0

    .line 152
    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    .line 154
    invoke-virtual {p0}, Lkxq;->getMeasuredWidth()I

    move-result v2

    .line 155
    invoke-virtual {p0}, Lkxq;->getMeasuredHeight()I

    move-result v3

    .line 157
    sget v0, Lkxq;->n:I

    if-gt v2, v0, :cond_3

    .line 158
    iput v3, p0, Lkxq;->h:I

    .line 167
    :goto_0
    iget v0, p0, Lkxq;->h:I

    sget-object v4, Lkxq;->q:Landroid/graphics/Bitmap;

    .line 168
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    sub-int/2addr v0, v4

    sget v4, Lkxq;->s:I

    mul-int/lit8 v4, v4, 0x3

    sub-int v4, v0, v4

    .line 173
    invoke-virtual {p0}, Lkxq;->getContext()Landroid/content/Context;

    move-result-object v5

    .line 175
    iget-object v0, p0, Lkxq;->a:Lkzz;

    const/16 v6, 0x21

    invoke-static {v5, v6}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v6

    invoke-virtual {v0}, Lkzz;->b()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_5

    sub-int v7, v4, p3

    invoke-static {v6}, Llib;->a(Landroid/text/TextPaint;)I

    move-result v8

    div-int/2addr v7, v8

    if-lez v7, :cond_5

    invoke-virtual {v0}, Lkzz;->b()Ljava/lang/String;

    move-result-object v0

    sget-object v8, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-static {v6, v0, v2, v7, v8}, Llib;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;IILandroid/text/Layout$Alignment;)Landroid/text/StaticLayout;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lkxq;->g:Landroid/text/StaticLayout;

    .line 176
    iget-object v0, p0, Lkxq;->g:Landroid/text/StaticLayout;

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lkxq;->g:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    sget v6, Lkxq;->s:I

    add-int/2addr v0, v6

    add-int/2addr p3, v0

    .line 180
    :cond_0
    iget-object v0, p0, Lkxq;->a:Lkzz;

    invoke-virtual {v0}, Lkzz;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 181
    const/16 v0, 0x1e

    invoke-static {v5, v0}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v0

    sub-int/2addr v4, p3

    invoke-static {v0}, Llib;->a(Landroid/text/TextPaint;)I

    move-result v6

    div-int/2addr v4, v6

    if-lez v4, :cond_1

    const v1, 0x7f0a0457

    invoke-virtual {v5, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v5, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-static {v0, v1, v2, v4, v5}, Llib;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;IILandroid/text/Layout$Alignment;)Landroid/text/StaticLayout;

    move-result-object v1

    :cond_1
    iput-object v1, p0, Lkxq;->f:Landroid/text/StaticLayout;

    .line 182
    iget-object v0, p0, Lkxq;->f:Landroid/text/StaticLayout;

    if-eqz v0, :cond_2

    .line 183
    iget-object v0, p0, Lkxq;->f:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    sget v0, Lkxq;->s:I

    .line 187
    :cond_2
    iget-object v0, p0, Lkxq;->d:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 188
    iget-object v0, p0, Lkxq;->e:Landroid/graphics/Rect;

    invoke-virtual {v0, v9, v9, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 189
    return-void

    .line 160
    :cond_3
    invoke-virtual {p0}, Lkxq;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v4, 0x2

    if-ne v0, v4, :cond_4

    .line 161
    invoke-virtual {p0}, Lkxq;->a()I

    move-result v0

    sub-int v0, v3, v0

    iput v0, p0, Lkxq;->h:I

    goto/16 :goto_0

    .line 163
    :cond_4
    invoke-virtual {p0}, Lkxq;->d()I

    move-result v0

    sub-int v0, v3, v0

    iput v0, p0, Lkxq;->h:I

    goto/16 :goto_0

    :cond_5
    move-object v0, v1

    .line 175
    goto :goto_1
.end method
