.class public final Lfxq;
.super Lcom/google/android/libraries/social/ui/views/EsScrollView;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lljh;


# static fields
.field private static i:Z

.field private static j:I

.field private static k:I

.field private static l:F

.field private static m:Landroid/graphics/drawable/NinePatchDrawable;

.field private static n:I

.field private static o:I

.field private static p:I

.field private static q:I

.field private static r:I


# instance fields
.field private a:Lfxr;

.field private b:Lfxs;

.field private c:Lfxt;

.field private d:Lfxw;

.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 98
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lfxq;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 99
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 102
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lfxq;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 103
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7

    .prologue
    const/4 v3, -0x1

    const/4 v4, 0x2

    const/4 v2, -0x2

    const/4 v1, 0x1

    .line 106
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/libraries/social/ui/views/EsScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 108
    sget-boolean v0, Lfxq;->i:Z

    if-nez v0, :cond_0

    .line 109
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 111
    const v0, 0x7f0d02c0

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lfxq;->j:I

    .line 113
    const v0, 0x7f0d02bf

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lfxq;->k:I

    .line 115
    const v0, 0x7f0d02c1

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    sput v0, Lfxq;->l:F

    .line 118
    const v0, 0x7f020065

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/NinePatchDrawable;

    sput-object v0, Lfxq;->m:Landroid/graphics/drawable/NinePatchDrawable;

    .line 120
    const v0, 0x7f0d0246

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lfxq;->n:I

    .line 121
    const v0, 0x7f0d0248

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lfxq;->o:I

    .line 122
    const v0, 0x7f0d0247

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lfxq;->p:I

    .line 123
    const v0, 0x7f0d0249

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lfxq;->q:I

    .line 124
    const v0, 0x7f0d02b7

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lfxq;->r:I

    .line 126
    sput-boolean v1, Lfxq;->i:Z

    .line 129
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v4, :cond_3

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lfxq;->e:Z

    .line 131
    invoke-static {p1}, Llsc;->a(Landroid/content/Context;)Landroid/util/DisplayMetrics;

    move-result-object v0

    invoke-static {v0}, Llsc;->b(Landroid/util/DisplayMetrics;)Z

    move-result v0

    iput-boolean v0, p0, Lfxq;->f:Z

    .line 133
    new-instance v0, Lfxr;

    invoke-direct {v0, p1, p2, p3}, Lfxr;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lfxq;->a:Lfxr;

    .line 134
    iget-object v5, p0, Lfxq;->a:Lfxr;

    new-instance v6, Landroid/widget/FrameLayout$LayoutParams;

    iget-boolean v0, p0, Lfxq;->e:Z

    if-eqz v0, :cond_4

    move v0, v2

    :goto_1
    invoke-direct {v6, v3, v0}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v5, v6}, Lfxr;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 136
    iget-object v0, p0, Lfxq;->a:Lfxr;

    invoke-virtual {p0, v0}, Lfxq;->addView(Landroid/view/View;)V

    .line 138
    new-instance v0, Lfxs;

    invoke-direct {v0, p1, p2, p3}, Lfxs;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lfxq;->b:Lfxs;

    .line 139
    iget-object v0, p0, Lfxq;->b:Lfxs;

    const v3, 0x7f1000a8

    invoke-virtual {v0, v3}, Lfxs;->setId(I)V

    .line 140
    iget-object v0, p0, Lfxq;->a:Lfxr;

    iget-object v3, p0, Lfxq;->b:Lfxs;

    invoke-virtual {v0, v3}, Lfxr;->addView(Landroid/view/View;)V

    .line 142
    iget-boolean v0, p0, Lfxq;->e:Z

    iput-boolean v0, p0, Lfxq;->g:Z

    .line 144
    new-instance v0, Lfxt;

    invoke-direct {v0, p1, p2, p3}, Lfxt;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lfxq;->c:Lfxt;

    .line 145
    iget-boolean v0, p0, Lfxq;->g:Z

    if-eqz v0, :cond_1

    .line 146
    iget-object v0, p0, Lfxq;->a:Lfxr;

    iget-object v3, p0, Lfxq;->c:Lfxt;

    invoke-virtual {v0, v3}, Lfxr;->addView(Landroid/view/View;)V

    .line 148
    :cond_1
    iget-object v0, p0, Lfxq;->c:Lfxt;

    const v3, 0x7f1000a9

    invoke-virtual {v0, v3}, Lfxt;->setId(I)V

    .line 150
    new-instance v0, Lfxw;

    invoke-direct {v0, p1, p2, p3}, Lfxw;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lfxq;->d:Lfxw;

    .line 151
    iget-boolean v0, p0, Lfxq;->g:Z

    if-eqz v0, :cond_2

    .line 152
    iget-object v0, p0, Lfxq;->a:Lfxr;

    iget-object v3, p0, Lfxq;->d:Lfxw;

    invoke-virtual {v0, v3}, Lfxr;->addView(Landroid/view/View;)V

    .line 155
    :cond_2
    iget-boolean v0, p0, Lfxq;->f:Z

    if-eqz v0, :cond_5

    move v0, v4

    .line 156
    :goto_2
    iget-boolean v3, p0, Lfxq;->e:Z

    if-eqz v3, :cond_6

    .line 157
    new-instance v3, Llka;

    invoke-direct {v3, v1, v2, v0, v0}, Llka;-><init>(IIII)V

    invoke-virtual {p0, v3}, Lfxq;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 163
    :goto_3
    return-void

    .line 129
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    :cond_4
    move v0, v3

    .line 134
    goto :goto_1

    :cond_5
    move v0, v1

    .line 155
    goto :goto_2

    .line 160
    :cond_6
    new-instance v1, Llka;

    invoke-direct {v1, v4, v2, v0, v0}, Llka;-><init>(IIII)V

    invoke-virtual {p0, v1}, Lfxq;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_3
.end method

.method static synthetic b()I
    .locals 1

    .prologue
    .line 30
    sget v0, Lfxq;->q:I

    return v0
.end method

.method private c()V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 311
    iget-boolean v2, p0, Lfxq;->g:Z

    if-nez v2, :cond_0

    .line 312
    iget-object v2, p0, Lfxq;->a:Lfxr;

    iget-object v3, p0, Lfxq;->c:Lfxt;

    invoke-virtual {v2, v3}, Lfxr;->addView(Landroid/view/View;)V

    .line 313
    iget-object v2, p0, Lfxq;->a:Lfxr;

    iget-object v3, p0, Lfxq;->d:Lfxw;

    invoke-virtual {v2, v3}, Lfxr;->addView(Landroid/view/View;)V

    .line 314
    iget-object v2, p0, Lfxq;->b:Lfxs;

    invoke-virtual {v2, v0}, Lfxs;->a(Z)V

    .line 321
    :goto_0
    iget-boolean v2, p0, Lfxq;->g:Z

    if-nez v2, :cond_1

    :goto_1
    iput-boolean v0, p0, Lfxq;->g:Z

    .line 322
    return-void

    .line 316
    :cond_0
    iget-object v2, p0, Lfxq;->a:Lfxr;

    iget-object v3, p0, Lfxq;->c:Lfxt;

    invoke-virtual {v2, v3}, Lfxr;->removeView(Landroid/view/View;)V

    .line 317
    iget-object v2, p0, Lfxq;->a:Lfxr;

    iget-object v3, p0, Lfxq;->d:Lfxw;

    invoke-virtual {v2, v3}, Lfxr;->removeView(Landroid/view/View;)V

    .line 318
    iget-object v2, p0, Lfxq;->b:Lfxs;

    invoke-virtual {v2, v1}, Lfxs;->a(Z)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 321
    goto :goto_1
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 299
    iget-object v0, p0, Lfxq;->b:Lfxs;

    invoke-virtual {v0}, Lfxs;->a()V

    .line 300
    iget-object v0, p0, Lfxq;->c:Lfxt;

    invoke-virtual {v0}, Lfxt;->b()V

    .line 301
    iget-object v0, p0, Lfxq;->d:Lfxw;

    invoke-virtual {v0}, Lfxw;->b()V

    .line 302
    return-void
.end method

.method public a(Lidh;Llah;Ljava/lang/String;Leaq;Lfxf;)V
    .locals 8

    .prologue
    .line 279
    iget-boolean v0, p0, Lfxq;->h:Z

    if-nez v0, :cond_0

    iget-boolean v0, p4, Leaq;->h:Z

    if-nez v0, :cond_0

    .line 280
    iget-boolean v0, p0, Lfxq;->e:Z

    if-nez v0, :cond_1

    iget-boolean v0, p4, Leaq;->i:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 282
    :goto_0
    if-eqz v0, :cond_2

    iget-boolean v1, p0, Lfxq;->g:Z

    if-eqz v1, :cond_2

    .line 283
    invoke-direct {p0}, Lfxq;->c()V

    .line 290
    :cond_0
    :goto_1
    iget-object v0, p0, Lfxq;->b:Lfxs;

    iget-boolean v1, p0, Lfxq;->e:Z

    if-eqz v1, :cond_3

    const/4 v5, 0x0

    :goto_2
    iget-boolean v6, p0, Lfxq;->f:Z

    move-object v1, p1

    move-object v2, p4

    move-object v3, p2

    move-object v4, p3

    move-object v7, p5

    invoke-virtual/range {v0 .. v7}, Lfxs;->a(Lidh;Leaq;Llah;Ljava/lang/String;Landroid/view/View$OnClickListener;ZLfxf;)V

    .line 292
    iget-object v0, p0, Lfxq;->c:Lfxt;

    invoke-virtual {v0, p1, p4, p5}, Lfxt;->a(Lidh;Leaq;Lfxf;)V

    .line 294
    iget-object v0, p0, Lfxq;->d:Lfxw;

    invoke-virtual {v0, p1, p4, p5}, Lfxw;->a(Lidh;Leaq;Lfxf;)V

    .line 295
    return-void

    .line 280
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 284
    :cond_2
    if-nez v0, :cond_0

    iget-boolean v0, p0, Lfxq;->g:Z

    if-nez v0, :cond_0

    .line 285
    invoke-direct {p0}, Lfxq;->c()V

    goto :goto_1

    :cond_3
    move-object v5, p0

    .line 290
    goto :goto_2
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 306
    invoke-direct {p0}, Lfxq;->c()V

    .line 307
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfxq;->h:Z

    .line 308
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 221
    sget-object v0, Lfxq;->m:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual {p0}, Lfxq;->getMeasuredWidth()I

    move-result v1

    iget-object v2, p0, Lfxq;->a:Lfxr;

    invoke-virtual {v2}, Lfxr;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 222
    sget-object v0, Lfxq;->m:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 224
    invoke-super {p0, p1}, Lcom/google/android/libraries/social/ui/views/EsScrollView;->onDraw(Landroid/graphics/Canvas;)V

    .line 225
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 229
    invoke-super/range {p0 .. p5}, Lcom/google/android/libraries/social/ui/views/EsScrollView;->onLayout(ZIIII)V

    .line 231
    invoke-virtual {p0}, Lfxq;->getMeasuredWidth()I

    move-result v0

    .line 233
    iget-object v1, p0, Lfxq;->a:Lfxr;

    iget-object v2, p0, Lfxq;->a:Lfxr;

    invoke-virtual {v2}, Lfxr;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v1, v5, v5, v0, v2}, Lfxr;->layout(IIII)V

    .line 235
    iget-object v1, p0, Lfxq;->b:Lfxs;

    invoke-virtual {v1}, Lfxs;->getMeasuredHeight()I

    move-result v1

    .line 236
    sget v2, Lfxq;->r:I

    add-int/2addr v2, v1

    .line 237
    iget-object v3, p0, Lfxq;->b:Lfxs;

    sget v4, Lfxq;->n:I

    invoke-virtual {v3, v4, v5, v0, v1}, Lfxs;->layout(IIII)V

    .line 239
    sget v0, Lfxq;->n:I

    sget v0, Lfxq;->p:I

    .line 241
    iget-boolean v0, p0, Lfxq;->g:Z

    if-eqz v0, :cond_0

    .line 242
    sget v0, Lfxq;->n:I

    iget-object v1, p0, Lfxq;->c:Lfxt;

    invoke-virtual {v1}, Lfxt;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v0, v1

    .line 243
    iget-object v1, p0, Lfxq;->c:Lfxt;

    invoke-virtual {v1}, Lfxt;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v1, v2

    .line 244
    iget-object v3, p0, Lfxq;->c:Lfxt;

    sget v4, Lfxq;->n:I

    invoke-virtual {v3, v4, v2, v0, v1}, Lfxt;->layout(IIII)V

    .line 246
    iget-object v3, p0, Lfxq;->b:Lfxs;

    iget-boolean v4, p0, Lfxq;->f:Z

    invoke-virtual {v3, v4}, Lfxs;->b(Z)V

    .line 248
    iget-boolean v3, p0, Lfxq;->f:Z

    if-eqz v3, :cond_1

    .line 249
    iget-object v1, p0, Lfxq;->a:Lfxr;

    invoke-virtual {v1, v0, v2}, Lfxr;->a(II)V

    .line 251
    iget-object v1, p0, Lfxq;->d:Lfxw;

    sget v3, Lfxq;->j:I

    add-int/2addr v3, v0

    sget v4, Lfxq;->j:I

    add-int/2addr v0, v4

    iget-object v4, p0, Lfxq;->d:Lfxw;

    .line 252
    invoke-virtual {v4}, Lfxw;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v0, v4

    iget-object v4, p0, Lfxq;->d:Lfxw;

    .line 253
    invoke-virtual {v4}, Lfxw;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v2

    .line 251
    invoke-virtual {v1, v3, v2, v0, v4}, Lfxw;->layout(IIII)V

    .line 262
    :cond_0
    :goto_0
    return-void

    .line 255
    :cond_1
    iget-object v0, p0, Lfxq;->a:Lfxr;

    invoke-virtual {v0}, Lfxr;->a()V

    .line 256
    sget v0, Lfxq;->k:I

    add-int/2addr v0, v1

    .line 257
    iget-object v1, p0, Lfxq;->d:Lfxw;

    sget v2, Lfxq;->n:I

    sget v3, Lfxq;->j:I

    add-int/2addr v2, v3

    sget v3, Lfxq;->n:I

    sget v4, Lfxq;->j:I

    add-int/2addr v3, v4

    iget-object v4, p0, Lfxq;->d:Lfxw;

    .line 258
    invoke-virtual {v4}, Lfxw;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v3, v4

    iget-object v4, p0, Lfxq;->d:Lfxw;

    .line 259
    invoke-virtual {v4}, Lfxw;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v0

    .line 257
    invoke-virtual {v1, v2, v0, v3, v4}, Lfxw;->layout(IIII)V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/high16 v7, 0x40000000    # 2.0f

    .line 167
    invoke-super {p0, p1, p2}, Lcom/google/android/libraries/social/ui/views/EsScrollView;->onMeasure(II)V

    .line 169
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 170
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 171
    if-nez v0, :cond_0

    move v0, v1

    .line 175
    :cond_0
    sget v2, Lfxq;->o:I

    .line 176
    sget v3, Lfxq;->n:I

    sget v4, Lfxq;->p:I

    add-int/2addr v3, v4

    sub-int v3, v0, v3

    .line 178
    iget-object v0, p0, Lfxq;->b:Lfxs;

    invoke-static {v3, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 179
    invoke-static {v1, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 178
    invoke-virtual {v0, v4, v5}, Lfxs;->measure(II)V

    .line 180
    iget-object v0, p0, Lfxq;->b:Lfxs;

    invoke-virtual {v0}, Lfxs;->getMeasuredHeight()I

    move-result v0

    sget v4, Lfxq;->r:I

    add-int/2addr v0, v4

    add-int v4, v2, v0

    .line 182
    iget-boolean v0, p0, Lfxq;->g:Z

    if-eqz v0, :cond_3

    .line 187
    iget-boolean v0, p0, Lfxq;->f:Z

    if-eqz v0, :cond_1

    .line 188
    int-to-float v0, v3

    sget v2, Lfxq;->l:F

    mul-float/2addr v0, v2

    float-to-int v2, v0

    .line 189
    sub-int v0, v3, v2

    sget v5, Lfxq;->j:I

    mul-int/lit8 v5, v5, 0x2

    sub-int/2addr v0, v5

    .line 195
    :goto_0
    iget-object v5, p0, Lfxq;->c:Lfxt;

    invoke-static {v2, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 196
    invoke-static {v1, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    .line 195
    invoke-virtual {v5, v2, v6}, Lfxt;->measure(II)V

    .line 197
    iget-object v2, p0, Lfxq;->d:Lfxw;

    .line 198
    invoke-static {v0, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 199
    invoke-static {v1, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 197
    invoke-virtual {v2, v0, v1}, Lfxw;->measure(II)V

    .line 201
    iget-boolean v0, p0, Lfxq;->f:Z

    if-eqz v0, :cond_2

    .line 202
    iget-object v0, p0, Lfxq;->c:Lfxt;

    invoke-virtual {v0}, Lfxt;->getMeasuredHeight()I

    move-result v0

    iget-object v1, p0, Lfxq;->d:Lfxw;

    .line 203
    invoke-virtual {v1}, Lfxw;->getMeasuredHeight()I

    move-result v1

    .line 202
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/2addr v0, v4

    .line 210
    :goto_1
    sget v1, Lfxq;->q:I

    add-int/2addr v0, v1

    .line 213
    iget-object v1, p0, Lfxq;->a:Lfxr;

    invoke-static {v3, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 214
    invoke-static {v0, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 213
    invoke-virtual {v1, v2, v4}, Lfxr;->measure(II)V

    .line 216
    sget v1, Lfxq;->n:I

    add-int/2addr v1, v3

    sget v2, Lfxq;->p:I

    add-int/2addr v1, v2

    invoke-virtual {p0, v1, v0}, Lfxq;->setMeasuredDimension(II)V

    .line 217
    return-void

    .line 192
    :cond_1
    sget v0, Lfxq;->j:I

    mul-int/lit8 v0, v0, 0x2

    sub-int v0, v3, v0

    move v2, v3

    goto :goto_0

    .line 205
    :cond_2
    iget-object v0, p0, Lfxq;->c:Lfxt;

    invoke-virtual {v0}, Lfxt;->getMeasuredHeight()I

    move-result v0

    iget-object v1, p0, Lfxq;->d:Lfxw;

    invoke-virtual {v1}, Lfxw;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    sget v1, Lfxq;->k:I

    add-int/2addr v0, v1

    add-int/2addr v0, v4

    goto :goto_1

    :cond_3
    move v0, v4

    goto :goto_1
.end method
