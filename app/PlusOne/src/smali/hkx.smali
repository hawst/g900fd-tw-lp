.class public final enum Lhkx;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lhkx;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lhkx;

.field public static final enum b:Lhkx;

.field public static final enum c:Lhkx;

.field private static final synthetic d:[Lhkx;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 155
    new-instance v0, Lhkx;

    const-string v1, "Account"

    invoke-direct {v0, v1, v2}, Lhkx;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhkx;->a:Lhkx;

    .line 157
    new-instance v0, Lhkx;

    const-string v1, "Album"

    invoke-direct {v0, v1, v3}, Lhkx;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhkx;->b:Lhkx;

    .line 159
    new-instance v0, Lhkx;

    const-string v1, "Batch"

    invoke-direct {v0, v1, v4}, Lhkx;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhkx;->c:Lhkx;

    .line 153
    const/4 v0, 0x3

    new-array v0, v0, [Lhkx;

    sget-object v1, Lhkx;->a:Lhkx;

    aput-object v1, v0, v2

    sget-object v1, Lhkx;->b:Lhkx;

    aput-object v1, v0, v3

    sget-object v1, Lhkx;->c:Lhkx;

    aput-object v1, v0, v4

    sput-object v0, Lhkx;->d:[Lhkx;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 153
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lhkx;
    .locals 1

    .prologue
    .line 153
    const-class v0, Lhkx;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lhkx;

    return-object v0
.end method

.method public static values()[Lhkx;
    .locals 1

    .prologue
    .line 153
    sget-object v0, Lhkx;->d:[Lhkx;

    invoke-virtual {v0}, [Lhkx;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhkx;

    return-object v0
.end method
