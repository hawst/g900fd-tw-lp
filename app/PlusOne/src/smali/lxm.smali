.class public final Llxm;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Llxm;


# instance fields
.field public b:Ljava/lang/String;

.field private c:Llxp;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4294
    const/4 v0, 0x0

    new-array v0, v0, [Llxm;

    sput-object v0, Llxm;->a:[Llxm;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 4295
    invoke-direct {p0}, Loxq;-><init>()V

    .line 4298
    const/4 v0, 0x0

    iput-object v0, p0, Llxm;->c:Llxp;

    .line 4295
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 4317
    const/4 v0, 0x0

    .line 4318
    iget-object v1, p0, Llxm;->c:Llxp;

    if-eqz v1, :cond_0

    .line 4319
    const/4 v0, 0x1

    iget-object v1, p0, Llxm;->c:Llxp;

    .line 4320
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 4322
    :cond_0
    iget-object v1, p0, Llxm;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 4323
    const/4 v1, 0x2

    iget-object v2, p0, Llxm;->b:Ljava/lang/String;

    .line 4324
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4326
    :cond_1
    iget-object v1, p0, Llxm;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4327
    iput v0, p0, Llxm;->ai:I

    .line 4328
    return v0
.end method

.method public a(Loxn;)Llxm;
    .locals 2

    .prologue
    .line 4336
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 4337
    sparse-switch v0, :sswitch_data_0

    .line 4341
    iget-object v1, p0, Llxm;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 4342
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llxm;->ah:Ljava/util/List;

    .line 4345
    :cond_1
    iget-object v1, p0, Llxm;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4347
    :sswitch_0
    return-object p0

    .line 4352
    :sswitch_1
    iget-object v0, p0, Llxm;->c:Llxp;

    if-nez v0, :cond_2

    .line 4353
    new-instance v0, Llxp;

    invoke-direct {v0}, Llxp;-><init>()V

    iput-object v0, p0, Llxm;->c:Llxp;

    .line 4355
    :cond_2
    iget-object v0, p0, Llxm;->c:Llxp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 4359
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llxm;->b:Ljava/lang/String;

    goto :goto_0

    .line 4337
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 4305
    iget-object v0, p0, Llxm;->c:Llxp;

    if-eqz v0, :cond_0

    .line 4306
    const/4 v0, 0x1

    iget-object v1, p0, Llxm;->c:Llxp;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 4308
    :cond_0
    iget-object v0, p0, Llxm;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 4309
    const/4 v0, 0x2

    iget-object v1, p0, Llxm;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 4311
    :cond_1
    iget-object v0, p0, Llxm;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 4313
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 4291
    invoke-virtual {p0, p1}, Llxm;->a(Loxn;)Llxm;

    move-result-object v0

    return-object v0
.end method
