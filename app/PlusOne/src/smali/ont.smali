.class public final Lont;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lonu;

.field public b:Ljava/lang/Long;

.field public c:Lonv;

.field public d:[Loob;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 786
    invoke-direct {p0}, Loxq;-><init>()V

    .line 789
    iput-object v0, p0, Lont;->a:Lonu;

    .line 794
    iput-object v0, p0, Lont;->c:Lonv;

    .line 797
    sget-object v0, Loob;->a:[Loob;

    iput-object v0, p0, Lont;->d:[Loob;

    .line 786
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 824
    .line 825
    iget-object v0, p0, Lont;->a:Lonu;

    if-eqz v0, :cond_4

    .line 826
    const/4 v0, 0x1

    iget-object v2, p0, Lont;->a:Lonu;

    .line 827
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 829
    :goto_0
    iget-object v2, p0, Lont;->b:Ljava/lang/Long;

    if-eqz v2, :cond_0

    .line 830
    const/4 v2, 0x2

    iget-object v3, p0, Lont;->b:Ljava/lang/Long;

    .line 831
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Loxo;->f(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 833
    :cond_0
    iget-object v2, p0, Lont;->c:Lonv;

    if-eqz v2, :cond_1

    .line 834
    const/4 v2, 0x3

    iget-object v3, p0, Lont;->c:Lonv;

    .line 835
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 837
    :cond_1
    iget-object v2, p0, Lont;->d:[Loob;

    if-eqz v2, :cond_3

    .line 838
    iget-object v2, p0, Lont;->d:[Loob;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 839
    if-eqz v4, :cond_2

    .line 840
    const/4 v5, 0x4

    .line 841
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 838
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 845
    :cond_3
    iget-object v1, p0, Lont;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 846
    iput v0, p0, Lont;->ai:I

    .line 847
    return v0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lont;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 855
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 856
    sparse-switch v0, :sswitch_data_0

    .line 860
    iget-object v2, p0, Lont;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 861
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lont;->ah:Ljava/util/List;

    .line 864
    :cond_1
    iget-object v2, p0, Lont;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 866
    :sswitch_0
    return-object p0

    .line 871
    :sswitch_1
    iget-object v0, p0, Lont;->a:Lonu;

    if-nez v0, :cond_2

    .line 872
    new-instance v0, Lonu;

    invoke-direct {v0}, Lonu;-><init>()V

    iput-object v0, p0, Lont;->a:Lonu;

    .line 874
    :cond_2
    iget-object v0, p0, Lont;->a:Lonu;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 878
    :sswitch_2
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lont;->b:Ljava/lang/Long;

    goto :goto_0

    .line 882
    :sswitch_3
    iget-object v0, p0, Lont;->c:Lonv;

    if-nez v0, :cond_3

    .line 883
    new-instance v0, Lonv;

    invoke-direct {v0}, Lonv;-><init>()V

    iput-object v0, p0, Lont;->c:Lonv;

    .line 885
    :cond_3
    iget-object v0, p0, Lont;->c:Lonv;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 889
    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 890
    iget-object v0, p0, Lont;->d:[Loob;

    if-nez v0, :cond_5

    move v0, v1

    .line 891
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Loob;

    .line 892
    iget-object v3, p0, Lont;->d:[Loob;

    if-eqz v3, :cond_4

    .line 893
    iget-object v3, p0, Lont;->d:[Loob;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 895
    :cond_4
    iput-object v2, p0, Lont;->d:[Loob;

    .line 896
    :goto_2
    iget-object v2, p0, Lont;->d:[Loob;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 897
    iget-object v2, p0, Lont;->d:[Loob;

    new-instance v3, Loob;

    invoke-direct {v3}, Loob;-><init>()V

    aput-object v3, v2, v0

    .line 898
    iget-object v2, p0, Lont;->d:[Loob;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 899
    invoke-virtual {p1}, Loxn;->a()I

    .line 896
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 890
    :cond_5
    iget-object v0, p0, Lont;->d:[Loob;

    array-length v0, v0

    goto :goto_1

    .line 902
    :cond_6
    iget-object v2, p0, Lont;->d:[Loob;

    new-instance v3, Loob;

    invoke-direct {v3}, Loob;-><init>()V

    aput-object v3, v2, v0

    .line 903
    iget-object v2, p0, Lont;->d:[Loob;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 856
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 802
    iget-object v0, p0, Lont;->a:Lonu;

    if-eqz v0, :cond_0

    .line 803
    const/4 v0, 0x1

    iget-object v1, p0, Lont;->a:Lonu;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 805
    :cond_0
    iget-object v0, p0, Lont;->b:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 806
    const/4 v0, 0x2

    iget-object v1, p0, Lont;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 808
    :cond_1
    iget-object v0, p0, Lont;->c:Lonv;

    if-eqz v0, :cond_2

    .line 809
    const/4 v0, 0x3

    iget-object v1, p0, Lont;->c:Lonv;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 811
    :cond_2
    iget-object v0, p0, Lont;->d:[Loob;

    if-eqz v0, :cond_4

    .line 812
    iget-object v1, p0, Lont;->d:[Loob;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 813
    if-eqz v3, :cond_3

    .line 814
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 812
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 818
    :cond_4
    iget-object v0, p0, Lont;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 820
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 782
    invoke-virtual {p0, p1}, Lont;->a(Loxn;)Lont;

    move-result-object v0

    return-object v0
.end method
