.class public final Lmob;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmof;

.field public b:Lmns;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 5507
    invoke-direct {p0}, Loxq;-><init>()V

    .line 5510
    iput-object v0, p0, Lmob;->a:Lmof;

    .line 5513
    iput-object v0, p0, Lmob;->b:Lmns;

    .line 5507
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 5530
    const/4 v0, 0x0

    .line 5531
    iget-object v1, p0, Lmob;->a:Lmof;

    if-eqz v1, :cond_0

    .line 5532
    const/4 v0, 0x1

    iget-object v1, p0, Lmob;->a:Lmof;

    .line 5533
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 5535
    :cond_0
    iget-object v1, p0, Lmob;->b:Lmns;

    if-eqz v1, :cond_1

    .line 5536
    const/4 v1, 0x2

    iget-object v2, p0, Lmob;->b:Lmns;

    .line 5537
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5539
    :cond_1
    iget-object v1, p0, Lmob;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5540
    iput v0, p0, Lmob;->ai:I

    .line 5541
    return v0
.end method

.method public a(Loxn;)Lmob;
    .locals 2

    .prologue
    .line 5549
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 5550
    sparse-switch v0, :sswitch_data_0

    .line 5554
    iget-object v1, p0, Lmob;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 5555
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmob;->ah:Ljava/util/List;

    .line 5558
    :cond_1
    iget-object v1, p0, Lmob;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5560
    :sswitch_0
    return-object p0

    .line 5565
    :sswitch_1
    iget-object v0, p0, Lmob;->a:Lmof;

    if-nez v0, :cond_2

    .line 5566
    new-instance v0, Lmof;

    invoke-direct {v0}, Lmof;-><init>()V

    iput-object v0, p0, Lmob;->a:Lmof;

    .line 5568
    :cond_2
    iget-object v0, p0, Lmob;->a:Lmof;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 5572
    :sswitch_2
    iget-object v0, p0, Lmob;->b:Lmns;

    if-nez v0, :cond_3

    .line 5573
    new-instance v0, Lmns;

    invoke-direct {v0}, Lmns;-><init>()V

    iput-object v0, p0, Lmob;->b:Lmns;

    .line 5575
    :cond_3
    iget-object v0, p0, Lmob;->b:Lmns;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 5550
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 5518
    iget-object v0, p0, Lmob;->a:Lmof;

    if-eqz v0, :cond_0

    .line 5519
    const/4 v0, 0x1

    iget-object v1, p0, Lmob;->a:Lmof;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 5521
    :cond_0
    iget-object v0, p0, Lmob;->b:Lmns;

    if-eqz v0, :cond_1

    .line 5522
    const/4 v0, 0x2

    iget-object v1, p0, Lmob;->b:Lmns;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 5524
    :cond_1
    iget-object v0, p0, Lmob;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 5526
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5503
    invoke-virtual {p0, p1}, Lmob;->a(Loxn;)Lmob;

    move-result-object v0

    return-object v0
.end method
