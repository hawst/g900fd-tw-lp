.class public final Lndm;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Boolean;

.field public b:Loae;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 187
    invoke-direct {p0}, Loxq;-><init>()V

    .line 192
    const/4 v0, 0x0

    iput-object v0, p0, Lndm;->b:Loae;

    .line 187
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 209
    const/4 v0, 0x0

    .line 210
    iget-object v1, p0, Lndm;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 211
    const/4 v0, 0x1

    iget-object v1, p0, Lndm;->a:Ljava/lang/Boolean;

    .line 212
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 214
    :cond_0
    iget-object v1, p0, Lndm;->b:Loae;

    if-eqz v1, :cond_1

    .line 215
    const/4 v1, 0x2

    iget-object v2, p0, Lndm;->b:Loae;

    .line 216
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 218
    :cond_1
    iget-object v1, p0, Lndm;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 219
    iput v0, p0, Lndm;->ai:I

    .line 220
    return v0
.end method

.method public a(Loxn;)Lndm;
    .locals 2

    .prologue
    .line 228
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 229
    sparse-switch v0, :sswitch_data_0

    .line 233
    iget-object v1, p0, Lndm;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 234
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lndm;->ah:Ljava/util/List;

    .line 237
    :cond_1
    iget-object v1, p0, Lndm;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 239
    :sswitch_0
    return-object p0

    .line 244
    :sswitch_1
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lndm;->a:Ljava/lang/Boolean;

    goto :goto_0

    .line 248
    :sswitch_2
    iget-object v0, p0, Lndm;->b:Loae;

    if-nez v0, :cond_2

    .line 249
    new-instance v0, Loae;

    invoke-direct {v0}, Loae;-><init>()V

    iput-object v0, p0, Lndm;->b:Loae;

    .line 251
    :cond_2
    iget-object v0, p0, Lndm;->b:Loae;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 229
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 197
    iget-object v0, p0, Lndm;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 198
    const/4 v0, 0x1

    iget-object v1, p0, Lndm;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 200
    :cond_0
    iget-object v0, p0, Lndm;->b:Loae;

    if-eqz v0, :cond_1

    .line 201
    const/4 v0, 0x2

    iget-object v1, p0, Lndm;->b:Loae;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 203
    :cond_1
    iget-object v0, p0, Lndm;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 205
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 183
    invoke-virtual {p0, p1}, Lndm;->a(Loxn;)Lndm;

    move-result-object v0

    return-object v0
.end method
