.class public final Llwv;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Llwv;


# instance fields
.field private b:I

.field private c:Ljava/lang/Double;

.field private d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4930
    const/4 v0, 0x0

    new-array v0, v0, [Llwv;

    sput-object v0, Llwv;->a:[Llwv;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 4931
    invoke-direct {p0}, Loxq;-><init>()V

    .line 4947
    const/high16 v0, -0x80000000

    iput v0, p0, Llwv;->b:I

    .line 4931
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 4971
    const/4 v0, 0x0

    .line 4972
    iget v1, p0, Llwv;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 4973
    const/4 v0, 0x1

    iget v1, p0, Llwv;->b:I

    .line 4974
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 4976
    :cond_0
    iget-object v1, p0, Llwv;->c:Ljava/lang/Double;

    if-eqz v1, :cond_1

    .line 4977
    const/4 v1, 0x2

    iget-object v2, p0, Llwv;->c:Ljava/lang/Double;

    .line 4978
    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 4980
    :cond_1
    iget-object v1, p0, Llwv;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 4981
    const/4 v1, 0x3

    iget-object v2, p0, Llwv;->d:Ljava/lang/String;

    .line 4982
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4984
    :cond_2
    iget-object v1, p0, Llwv;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4985
    iput v0, p0, Llwv;->ai:I

    .line 4986
    return v0
.end method

.method public a(Loxn;)Llwv;
    .locals 2

    .prologue
    .line 4994
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 4995
    sparse-switch v0, :sswitch_data_0

    .line 4999
    iget-object v1, p0, Llwv;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 5000
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llwv;->ah:Ljava/util/List;

    .line 5003
    :cond_1
    iget-object v1, p0, Llwv;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5005
    :sswitch_0
    return-object p0

    .line 5010
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 5011
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9

    if-ne v0, v1, :cond_3

    .line 5021
    :cond_2
    iput v0, p0, Llwv;->b:I

    goto :goto_0

    .line 5023
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Llwv;->b:I

    goto :goto_0

    .line 5028
    :sswitch_2
    invoke-virtual {p1}, Loxn;->c()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Llwv;->c:Ljava/lang/Double;

    goto :goto_0

    .line 5032
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llwv;->d:Ljava/lang/String;

    goto :goto_0

    .line 4995
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x11 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 4956
    iget v0, p0, Llwv;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 4957
    const/4 v0, 0x1

    iget v1, p0, Llwv;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 4959
    :cond_0
    iget-object v0, p0, Llwv;->c:Ljava/lang/Double;

    if-eqz v0, :cond_1

    .line 4960
    const/4 v0, 0x2

    iget-object v1, p0, Llwv;->c:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(ID)V

    .line 4962
    :cond_1
    iget-object v0, p0, Llwv;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 4963
    const/4 v0, 0x3

    iget-object v1, p0, Llwv;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 4965
    :cond_2
    iget-object v0, p0, Llwv;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 4967
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 4927
    invoke-virtual {p0, p1}, Llwv;->a(Loxn;)Llwv;

    move-result-object v0

    return-object v0
.end method
