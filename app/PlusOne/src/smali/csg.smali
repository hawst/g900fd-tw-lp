.class public Lcsg;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhob;
.implements Llnx;
.implements Llrg;


# instance fields
.field private final a:Lz;

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcsh;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lhoc;

.field private d:Z

.field private e:Lcsd;

.field private f:Lctz;

.field private g:Lhee;


# direct methods
.method public constructor <init>(Lz;Llqr;)V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcsg;->b:Ljava/util/Set;

    .line 64
    iput-object p1, p0, Lcsg;->a:Lz;

    .line 66
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 67
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Llnh;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 79
    const-class v0, Lcsd;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcsd;

    iput-object v0, p0, Lcsg;->e:Lcsd;

    .line 80
    const-class v0, Lctz;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctz;

    iput-object v0, p0, Lcsg;->f:Lctz;

    .line 81
    const-class v0, Lhee;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Lcsg;->g:Lhee;

    .line 82
    const-class v0, Lhoc;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    iput-object v0, p0, Lcsg;->c:Lhoc;

    .line 83
    iget-object v0, p0, Lcsg;->c:Lhoc;

    invoke-virtual {v0, p0}, Lhoc;->a(Lhob;)Lhoc;

    .line 84
    return-void
.end method

.method public a(Lcsh;)V
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcsg;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 71
    return-void
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 140
    const-string v0, "MovePhotosToTrashTask"

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 141
    invoke-virtual {p3, p1}, Lhos;->a(Ljava/lang/String;)V

    .line 142
    iget-object v0, p0, Lcsg;->e:Lcsd;

    invoke-virtual {v0}, Lcsd;->b()V

    .line 144
    iget-boolean v0, p0, Lcsg;->d:Z

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p0, Lcsg;->f:Lctz;

    invoke-virtual {v0}, Lctz;->c()V

    .line 146
    iput-boolean v1, p0, Lcsg;->d:Z

    .line 149
    :cond_0
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v0

    move-object v3, v0

    .line 150
    :goto_0
    if-eqz v3, :cond_1

    const-string v0, "resolver"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 169
    :cond_1
    return-void

    :cond_2
    move-object v3, v2

    .line 149
    goto :goto_0

    .line 154
    :cond_3
    const-string v0, "resolver"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lduo;

    .line 156
    const-string v4, "db_rows"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 157
    const-string v2, "db_rows"

    invoke-virtual {v3, v2}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v4

    .line 158
    array-length v2, v4

    new-array v3, v2, [Ljuy;

    move v2, v1

    .line 160
    :goto_1
    array-length v1, v4

    if-ge v2, v1, :cond_4

    .line 161
    aget-object v1, v4, v2

    check-cast v1, Ljuy;

    aput-object v1, v3, v2

    .line 160
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    :cond_4
    move-object v2, v3

    .line 165
    :cond_5
    iget-object v1, p0, Lcsg;->b:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcsh;

    .line 166
    invoke-interface {v1, v0, v2}, Lcsh;->a(Lduo;[Ljuy;)V

    goto :goto_2
.end method

.method public a(Ljcn;Z)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 90
    const-class v0, Ldqm;

    .line 91
    invoke-virtual {p1, v0}, Ljcn;->a(Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v0

    .line 92
    if-eqz v0, :cond_0

    .line 93
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    move v0, v1

    .line 95
    :goto_0
    if-eqz p2, :cond_1

    if-eqz v0, :cond_1

    .line 96
    invoke-static {p1}, Lcrz;->a(Ljcn;)Lcrz;

    move-result-object v0

    iget-object v1, p0, Lcsg;->a:Lz;

    invoke-virtual {v1}, Lz;->f()Lae;

    move-result-object v1

    invoke-virtual {v1}, Lae;->a()Lat;

    move-result-object v1

    const-string v2, "first_time_trash_info"

    invoke-virtual {v1, v0, v2}, Lat;->a(Lu;Ljava/lang/String;)Lat;

    invoke-virtual {v1}, Lat;->c()I

    .line 115
    :goto_1
    return-void

    .line 93
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 100
    :cond_1
    invoke-virtual {p1}, Ljcn;->e()Z

    move-result v0

    .line 101
    iget-object v2, p0, Lcsg;->e:Lcsd;

    invoke-virtual {v2}, Lcsd;->c()V

    .line 102
    new-instance v2, Ldup;

    invoke-virtual {p1}, Ljcn;->a()Ljcn;

    move-result-object v3

    invoke-direct {v2, v3}, Ldup;-><init>(Ljcn;)V

    .line 103
    iput-boolean v1, p0, Lcsg;->d:Z

    .line 105
    new-instance v1, Ldpl;

    iget-object v3, p0, Lcsg;->a:Lz;

    iget-object v4, p0, Lcsg;->g:Lhee;

    .line 106
    invoke-interface {v4}, Lhee;->d()I

    move-result v4

    invoke-direct {v1, v3, v4, v2, v0}, Ldpl;-><init>(Landroid/content/Context;ILdup;Z)V

    .line 108
    new-instance v2, Lhpd;

    iget-object v0, p0, Lcsg;->a:Lz;

    iget-object v3, p0, Lcsg;->a:Lz;

    .line 109
    invoke-virtual {v3}, Lz;->f()Lae;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Lhpd;-><init>(Landroid/content/Context;Lae;)V

    .line 110
    invoke-virtual {p1}, Ljcn;->b()Z

    move-result v0

    .line 111
    invoke-virtual {p1}, Ljcn;->c()Z

    move-result v3

    invoke-virtual {p1}, Ljcn;->k()I

    move-result v4

    .line 110
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    const v0, 0x7f11004e

    :goto_2
    iget-object v3, p0, Lcsg;->a:Lz;

    invoke-virtual {v3}, Lz;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v0

    .line 112
    invoke-virtual {v1}, Ldpl;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lhos;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    iget-object v0, p0, Lcsg;->c:Lhoc;

    invoke-virtual {v0, v1}, Lhoc;->b(Lhny;)V

    goto :goto_1

    .line 110
    :cond_2
    if-eqz v0, :cond_3

    const v0, 0x7f11004f

    goto :goto_2

    :cond_3
    const v0, 0x7f110050

    goto :goto_2
.end method
