.class public final Lgcj;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:I

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput p1, p0, Lgcj;->a:I

    .line 54
    iput-object p2, p0, Lgcj;->b:Ljava/lang/String;

    .line 55
    iput-object p3, p0, Lgcj;->c:Ljava/lang/String;

    .line 56
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 71
    check-cast p1, Lgcj;

    .line 72
    iget v0, p0, Lgcj;->a:I

    iget v1, p1, Lgcj;->a:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lgcj;->b:Ljava/lang/String;

    iget-object v1, p1, Lgcj;->b:Ljava/lang/String;

    .line 73
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 60
    iget v0, p0, Lgcj;->a:I

    .line 62
    iget-object v1, p0, Lgcj;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 63
    iget-object v1, p0, Lgcj;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 66
    :cond_0
    return v0
.end method
