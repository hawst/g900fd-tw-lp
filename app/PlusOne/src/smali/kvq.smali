.class public Lkvq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lbc;
.implements Lhob;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/view/View$OnClickListener;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lhob;"
    }
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field private b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lkvt;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Lkvv;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private e:Landroid/content/Context;

.field private f:I

.field private g:Lhoc;

.field private final h:Lkvr;

.field private i:Liwk;

.field private j:Z

.field private k:Lkvs;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 69
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "square_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "joinability"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "membership_status"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "suggestion_id"

    aput-object v2, v0, v1

    sput-object v0, Lkvq;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lu;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    const/4 v0, 0x0

    iput-boolean v0, p0, Lkvq;->j:Z

    .line 97
    iput-object v2, p0, Lkvq;->k:Lkvs;

    .line 101
    iput-object p1, p0, Lkvq;->e:Landroid/content/Context;

    .line 102
    iput p3, p0, Lkvq;->f:I

    .line 103
    const-class v0, Lhoc;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    iput-object v0, p0, Lkvq;->g:Lhoc;

    .line 104
    new-instance v0, Lkwj;

    invoke-direct {v0, p1, p2, p3, p0}, Lkwj;-><init>(Landroid/content/Context;Lu;ILkvq;)V

    iput-object v0, p0, Lkvq;->h:Lkvr;

    .line 106
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lkvq;->b:Ljava/util/Map;

    .line 107
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lkvq;->c:Ljava/util/Map;

    .line 108
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lkvq;->d:Ljava/util/WeakHashMap;

    .line 110
    iget-object v0, p0, Lkvq;->g:Lhoc;

    invoke-virtual {v0, p0}, Lhoc;->a(Lhob;)Lhoc;

    .line 111
    iget-object v0, p0, Lkvq;->e:Landroid/content/Context;

    const-class v1, Lhee;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v0

    .line 112
    const-string v1, "is_google_plus"

    invoke-interface {v0, v1}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 113
    invoke-virtual {p2}, Lu;->w()Lbb;

    move-result-object v0

    const v1, 0x2de4714

    invoke-virtual {v0, v1, v2, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 115
    :cond_0
    const-class v0, Lkvs;

    invoke-static {p1, v0}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkvs;

    iput-object v0, p0, Lkvq;->k:Lkvs;

    .line 116
    new-instance v0, Liwk;

    invoke-direct {v0, p1, p3}, Liwk;-><init>(Landroid/content/Context;I)V

    const-class v1, Lixj;

    .line 117
    invoke-virtual {v0, v1}, Liwk;->a(Ljava/lang/Class;)Liwk;

    move-result-object v0

    iput-object v0, p0, Lkvq;->i:Liwk;

    .line 118
    return-void
.end method

.method private b(Lkvv;)V
    .locals 2

    .prologue
    .line 155
    iget-object v0, p0, Lkvq;->b:Ljava/util/Map;

    invoke-interface {p1}, Lkvv;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 156
    iget-object v0, p0, Lkvq;->b:Ljava/util/Map;

    invoke-interface {p1}, Lkvv;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkvt;

    iget v0, v0, Lkvt;->a:I

    .line 157
    iget-boolean v1, p0, Lkvq;->j:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 158
    const/4 v0, 0x6

    .line 160
    :cond_0
    invoke-interface {p1, v0}, Lkvv;->a(I)V

    .line 162
    :cond_1
    return-void
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 175
    new-instance v0, Lktu;

    iget-object v1, p0, Lkvq;->e:Landroid/content/Context;

    iget v2, p0, Lkvq;->f:I

    sget-object v3, Lkvq;->a:[Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lktu;-><init>(Landroid/content/Context;I[Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lkvq;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lkvq;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkvt;

    iget-object v0, v0, Lkvt;->b:Ljava/lang/String;

    .line 151
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 180
    if-nez p1, :cond_1

    .line 200
    :cond_0
    return-void

    .line 184
    :cond_1
    const/4 v0, -0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 185
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 186
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 187
    const/4 v1, 0x2

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 188
    const/4 v2, 0x1

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 189
    const/4 v3, 0x3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 190
    invoke-static {v1, v2}, Lkvu;->a(II)I

    move-result v1

    .line 191
    iget-object v2, p0, Lkvq;->b:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 192
    iget-object v2, p0, Lkvq;->b:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkvt;

    .line 193
    iput v1, v0, Lkvt;->a:I

    .line 194
    iput-object v3, v0, Lkvt;->b:Ljava/lang/String;

    goto :goto_0

    .line 196
    :cond_2
    iget-object v2, p0, Lkvq;->b:Ljava/util/Map;

    new-instance v4, Lkvt;

    invoke-direct {v4, v1, v3}, Lkvt;-><init>(ILjava/lang/String;)V

    invoke-interface {v2, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 199
    :cond_3
    iget-object v0, p0, Lkvq;->d:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkvv;

    invoke-direct {p0, v0}, Lkvq;->b(Lkvv;)V

    goto :goto_1
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 203
    return-void
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 39
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p2}, Lkvq;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method public a(Ljava/lang/String;I)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 215
    iget-object v0, p0, Lkvq;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 247
    :cond_0
    :goto_0
    return-void

    .line 220
    :cond_1
    iget-object v0, p0, Lkvq;->i:Liwk;

    invoke-virtual {v0}, Liwk;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 221
    iget-object v0, p0, Lkvq;->e:Landroid/content/Context;

    iget-object v1, p0, Lkvq;->i:Liwk;

    invoke-virtual {v1}, Liwk;->b()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 225
    :cond_2
    const/4 v0, 0x6

    if-ne p2, v0, :cond_3

    .line 226
    iget-object v2, p0, Lkvq;->e:Landroid/content/Context;

    iget-object v0, p0, Lkvq;->e:Landroid/content/Context;

    const-class v3, Lkte;

    invoke-static {v0, v3}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkte;

    iget v3, p0, Lkvq;->f:I

    .line 227
    invoke-interface {v0, v3, p1, v1}, Lkte;->a(ILjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 226
    invoke-virtual {v2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 233
    :cond_3
    packed-switch p2, :pswitch_data_0

    .line 234
    :goto_1
    if-eqz v1, :cond_0

    .line 238
    iget-object v0, p0, Lkvq;->c:Ljava/util/Map;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 240
    iget-object v0, p0, Lkvq;->g:Lhoc;

    invoke-virtual {v0}, Lhoc;->d()Lhos;

    move-result-object v2

    iget-object v3, p0, Lkvq;->e:Landroid/content/Context;

    .line 241
    packed-switch p2, :pswitch_data_1

    const v0, 0x7f0a058d

    :goto_2
    invoke-virtual {v3, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v3, "join_action"

    .line 240
    invoke-virtual {v2, v0, v3}, Lhos;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lkvu;->a(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 243
    iget-object v0, p0, Lkvq;->h:Lkvr;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, p1, v1}, Lkvr;->a(Ljava/lang/String;I)V

    goto :goto_0

    .line 233
    :pswitch_0
    const/4 v0, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    move-object v1, v0

    goto :goto_1

    :pswitch_1
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    move-object v1, v0

    goto :goto_1

    :pswitch_2
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    move-object v1, v0

    goto :goto_1

    :pswitch_3
    const/16 v0, 0x14

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    move-object v1, v0

    goto :goto_1

    .line 241
    :pswitch_4
    const v0, 0x7f0a04b4

    goto :goto_2

    :pswitch_5
    const v0, 0x7f0a04b5

    goto :goto_2

    :pswitch_6
    const v0, 0x7f0a04b6

    goto :goto_2

    .line 245
    :cond_4
    invoke-virtual {p0, p1}, Lkvq;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 233
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 241
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 3

    .prologue
    .line 268
    const-string v0, "join_action"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    .line 269
    invoke-virtual {p2}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "square_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 270
    iget-object v0, p0, Lkvq;->c:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 271
    invoke-virtual {p0, v1}, Lkvq;->c(Ljava/lang/String;)V

    .line 272
    invoke-virtual {p2}, Lhoz;->f()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 281
    :cond_0
    :goto_0
    return-void

    .line 276
    :cond_1
    iget-object v2, p0, Lkvq;->h:Lkvr;

    invoke-interface {v2, p3}, Lkvr;->a(Lhos;)V

    .line 277
    iget-object v2, p0, Lkvq;->k:Lkvs;

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    .line 278
    iget-object v2, p0, Lkvq;->k:Lkvs;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v2, v1, v0, p2}, Lkvs;->a(Ljava/lang/String;ILhoz;)V

    goto :goto_0
.end method

.method public a(Lkvs;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 122
    iput-object p1, p0, Lkvq;->k:Lkvs;

    .line 123
    return-void
.end method

.method public a(Lkvv;)V
    .locals 2

    .prologue
    .line 135
    invoke-static {}, Llsx;->b()V

    .line 137
    invoke-interface {p1}, Lkvv;->b()Ljava/lang/String;

    move-result-object v0

    .line 138
    if-nez v0, :cond_0

    .line 139
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "JoinButton missing Square Id during registration."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 142
    :cond_0
    iget-object v1, p0, Lkvq;->d:Ljava/util/WeakHashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    invoke-interface {p1, p0}, Lkvv;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 144
    invoke-direct {p0, p1}, Lkvq;->b(Lkvv;)V

    .line 145
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 131
    iput-boolean p1, p0, Lkvq;->j:Z

    .line 132
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 250
    iget-object v0, p0, Lkvq;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 263
    :goto_0
    return-void

    .line 255
    :cond_0
    iget-object v0, p0, Lkvq;->k:Lkvs;

    if-eqz v0, :cond_1

    .line 256
    iget-object v1, p0, Lkvq;->k:Lkvs;

    iget-object v0, p0, Lkvq;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v1, p1, v0}, Lkvs;->a(Ljava/lang/String;I)V

    .line 259
    :cond_1
    new-instance v1, Lkvp;

    iget-object v2, p0, Lkvq;->e:Landroid/content/Context;

    iget v3, p0, Lkvq;->f:I

    iget-object v0, p0, Lkvq;->c:Ljava/util/Map;

    .line 260
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {v1, v2, v3, p1, v0}, Lkvp;-><init>(Landroid/content/Context;ILjava/lang/String;I)V

    .line 261
    const-string v0, "join_action"

    invoke-virtual {v1, v0}, Lhny;->b(Ljava/lang/String;)Lhny;

    .line 262
    iget-object v0, p0, Lkvq;->g:Lhoc;

    invoke-virtual {v0, v1}, Lhoc;->b(Lhny;)V

    goto :goto_0
.end method

.method public c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 284
    iget-object v0, p0, Lkvq;->g:Lhoc;

    invoke-virtual {v0}, Lhoc;->d()Lhos;

    move-result-object v0

    const-string v1, "join_action"

    invoke-virtual {v0, v1}, Lhos;->a(Ljava/lang/String;)V

    .line 285
    iget-object v0, p0, Lkvq;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 286
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 207
    move-object v0, p1

    check-cast v0, Lkvv;

    .line 208
    instance-of v1, p1, Lhmm;

    if-eqz v1, :cond_0

    .line 209
    const/4 v1, 0x4

    invoke-static {p1, v1}, Lhly;->a(Landroid/view/View;I)V

    .line 211
    :cond_0
    invoke-interface {v0}, Lkvv;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0}, Lkvv;->c()I

    move-result v0

    invoke-virtual {p0, v1, v0}, Lkvq;->a(Ljava/lang/String;I)V

    .line 212
    return-void
.end method
