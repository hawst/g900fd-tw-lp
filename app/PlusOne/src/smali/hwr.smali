.class public final Lhwr;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static a:Lhxa;

.field public static b:Lhxa;

.field private static c:Lhxa;

.field private static d:Lhxa;

.field private static e:Lhxa;

.field private static f:Lhxa;

.field private static g:Lhxa;

.field private static h:Lhxa;


# direct methods
.method public static a(Lpeo;)Landroid/text/SpannableStringBuilder;
    .locals 3

    .prologue
    .line 83
    sget-object v0, Lhwr;->e:Lhxa;

    if-nez v0, :cond_0

    new-instance v0, Lhwu;

    invoke-direct {v0}, Lhwu;-><init>()V

    sput-object v0, Lhwr;->e:Lhxa;

    :cond_0
    sget-object v0, Lhwr;->e:Lhxa;

    .line 84
    invoke-static {}, Lhwr;->d()Lhxa;

    move-result-object v1

    invoke-static {}, Lhwr;->c()Lhxa;

    move-result-object v2

    .line 83
    invoke-static {p0, v0, v1, v2}, Lhwr;->a(Lpeo;Lhxa;Lhxa;Lhxa;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lpeo;Lhxa;Lhxa;Lhxa;)Landroid/text/SpannableStringBuilder;
    .locals 7

    .prologue
    .line 101
    .line 102
    invoke-static {}, Lhwr;->a()Lhxa;

    move-result-object v4

    invoke-static {}, Lhwr;->b()Lhxa;

    move-result-object v5

    invoke-static {}, Lhwr;->e()Lhxa;

    move-result-object v6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    .line 101
    invoke-static/range {v0 .. v6}, Lhwr;->a(Lpeo;Lhxa;Lhxa;Lhxa;Lhxa;Lhxa;Lhxa;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lpeo;Lhxa;Lhxa;Lhxa;Lhxa;Lhxa;Lhxa;)Landroid/text/SpannableStringBuilder;
    .locals 7

    .prologue
    .line 50
    if-nez p0, :cond_1

    .line 51
    const/4 v0, 0x0

    .line 79
    :cond_0
    return-object v0

    .line 53
    :cond_1
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 54
    const/4 v1, 0x0

    iget-object v2, p0, Lpeo;->b:[Lpen;

    array-length v2, v2

    :goto_0
    if-ge v1, v2, :cond_0

    .line 55
    iget-object v3, p0, Lpeo;->b:[Lpen;

    aget-object v3, v3, v1

    .line 56
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    .line 58
    iget v5, v3, Lpen;->b:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_4

    iget-object v5, v3, Lpen;->e:Lpem;

    if-eqz v5, :cond_4

    if-eqz p1, :cond_4

    .line 60
    invoke-interface {p1, v0, v4, v3}, Lhxa;->a(Landroid/text/SpannableStringBuilder;ILpen;)V

    .line 75
    :cond_2
    :goto_1
    iget-object v5, v3, Lpen;->d:Lpek;

    if-eqz v5, :cond_3

    if-eqz p6, :cond_3

    .line 76
    invoke-interface {p6, v0, v4, v3}, Lhxa;->a(Landroid/text/SpannableStringBuilder;ILpen;)V

    .line 54
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 61
    :cond_4
    iget v5, v3, Lpen;->b:I

    const/4 v6, 0x3

    if-ne v5, v6, :cond_5

    if-eqz p2, :cond_5

    .line 63
    invoke-interface {p2, v0, v4, v3}, Lhxa;->a(Landroid/text/SpannableStringBuilder;ILpen;)V

    goto :goto_1

    .line 64
    :cond_5
    iget v5, v3, Lpen;->b:I

    const/4 v6, 0x4

    if-ne v5, v6, :cond_6

    iget-object v5, v3, Lpen;->g:Lpel;

    if-eqz v5, :cond_6

    iget-object v5, v3, Lpen;->g:Lpel;

    iget-object v5, v5, Lpel;->a:Ljava/lang/String;

    .line 65
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_6

    if-eqz p3, :cond_6

    .line 67
    invoke-interface {p3, v0, v4, v3}, Lhxa;->a(Landroid/text/SpannableStringBuilder;ILpen;)V

    goto :goto_1

    .line 68
    :cond_6
    iget v5, v3, Lpen;->b:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_7

    if-eqz p4, :cond_7

    .line 70
    invoke-interface {p4, v0, v4, v3}, Lhxa;->a(Landroid/text/SpannableStringBuilder;ILpen;)V

    goto :goto_1

    .line 71
    :cond_7
    if-eqz p5, :cond_2

    .line 72
    invoke-interface {p5, v0, v4, v3}, Lhxa;->a(Landroid/text/SpannableStringBuilder;ILpen;)V

    goto :goto_1
.end method

.method public static a()Lhxa;
    .locals 1

    .prologue
    .line 106
    sget-object v0, Lhwr;->c:Lhxa;

    if-nez v0, :cond_0

    .line 107
    new-instance v0, Lhws;

    invoke-direct {v0}, Lhws;-><init>()V

    sput-object v0, Lhwr;->c:Lhxa;

    .line 115
    :cond_0
    sget-object v0, Lhwr;->c:Lhxa;

    return-object v0
.end method

.method public static b()Lhxa;
    .locals 1

    .prologue
    .line 119
    sget-object v0, Lhwr;->d:Lhxa;

    if-nez v0, :cond_0

    .line 120
    new-instance v0, Lhwt;

    invoke-direct {v0}, Lhwt;-><init>()V

    sput-object v0, Lhwr;->d:Lhxa;

    .line 128
    :cond_0
    sget-object v0, Lhwr;->d:Lhxa;

    return-object v0
.end method

.method public static c()Lhxa;
    .locals 1

    .prologue
    .line 171
    sget-object v0, Lhwr;->g:Lhxa;

    if-nez v0, :cond_0

    .line 172
    new-instance v0, Lhww;

    invoke-direct {v0}, Lhww;-><init>()V

    sput-object v0, Lhwr;->g:Lhxa;

    .line 184
    :cond_0
    sget-object v0, Lhwr;->g:Lhxa;

    return-object v0
.end method

.method public static d()Lhxa;
    .locals 1

    .prologue
    .line 190
    sget-object v0, Lhwr;->f:Lhxa;

    if-nez v0, :cond_0

    .line 191
    new-instance v0, Lhwx;

    invoke-direct {v0}, Lhwx;-><init>()V

    sput-object v0, Lhwr;->f:Lhxa;

    .line 207
    :cond_0
    sget-object v0, Lhwr;->f:Lhxa;

    return-object v0
.end method

.method public static e()Lhxa;
    .locals 1

    .prologue
    .line 226
    sget-object v0, Lhwr;->h:Lhxa;

    if-nez v0, :cond_0

    .line 227
    new-instance v0, Lhwz;

    invoke-direct {v0}, Lhwz;-><init>()V

    sput-object v0, Lhwr;->h:Lhxa;

    .line 253
    :cond_0
    sget-object v0, Lhwr;->h:Lhxa;

    return-object v0
.end method
