.class public final Lmqi;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Boolean;

.field public b:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 556
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 577
    const/4 v0, 0x0

    .line 578
    iget-object v1, p0, Lmqi;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 579
    const/4 v0, 0x1

    iget-object v1, p0, Lmqi;->a:Ljava/lang/Boolean;

    .line 580
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 582
    :cond_0
    iget-object v1, p0, Lmqi;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 583
    const/4 v1, 0x2

    iget-object v2, p0, Lmqi;->b:Ljava/lang/Boolean;

    .line 584
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 586
    :cond_1
    iget-object v1, p0, Lmqi;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 587
    iput v0, p0, Lmqi;->ai:I

    .line 588
    return v0
.end method

.method public a(Loxn;)Lmqi;
    .locals 2

    .prologue
    .line 596
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 597
    sparse-switch v0, :sswitch_data_0

    .line 601
    iget-object v1, p0, Lmqi;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 602
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmqi;->ah:Ljava/util/List;

    .line 605
    :cond_1
    iget-object v1, p0, Lmqi;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 607
    :sswitch_0
    return-object p0

    .line 612
    :sswitch_1
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmqi;->a:Ljava/lang/Boolean;

    goto :goto_0

    .line 616
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmqi;->b:Ljava/lang/Boolean;

    goto :goto_0

    .line 597
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 565
    iget-object v0, p0, Lmqi;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 566
    const/4 v0, 0x1

    iget-object v1, p0, Lmqi;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 568
    :cond_0
    iget-object v0, p0, Lmqi;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 569
    const/4 v0, 0x2

    iget-object v1, p0, Lmqi;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 571
    :cond_1
    iget-object v0, p0, Lmqi;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 573
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 552
    invoke-virtual {p0, p1}, Lmqi;->a(Loxn;)Lmqi;

    move-result-object v0

    return-object v0
.end method
