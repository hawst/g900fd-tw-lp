.class public final Lmog;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmog;


# instance fields
.field public b:I

.field public c:Ljava/lang/Long;

.field public d:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4423
    const/4 v0, 0x0

    new-array v0, v0, [Lmog;

    sput-object v0, Lmog;->a:[Lmog;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 4424
    invoke-direct {p0}, Loxq;-><init>()V

    .line 4434
    const/high16 v0, -0x80000000

    iput v0, p0, Lmog;->b:I

    .line 4424
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 4458
    const/4 v0, 0x0

    .line 4459
    iget v1, p0, Lmog;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 4460
    const/4 v0, 0x1

    iget v1, p0, Lmog;->b:I

    .line 4461
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 4463
    :cond_0
    iget-object v1, p0, Lmog;->c:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 4464
    const/4 v1, 0x2

    iget-object v2, p0, Lmog;->c:Ljava/lang/Long;

    .line 4465
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 4467
    :cond_1
    iget-object v1, p0, Lmog;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 4468
    const/4 v1, 0x3

    iget-object v2, p0, Lmog;->d:Ljava/lang/Integer;

    .line 4469
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4471
    :cond_2
    iget-object v1, p0, Lmog;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4472
    iput v0, p0, Lmog;->ai:I

    .line 4473
    return v0
.end method

.method public a(Loxn;)Lmog;
    .locals 2

    .prologue
    .line 4481
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 4482
    sparse-switch v0, :sswitch_data_0

    .line 4486
    iget-object v1, p0, Lmog;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 4487
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmog;->ah:Ljava/util/List;

    .line 4490
    :cond_1
    iget-object v1, p0, Lmog;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4492
    :sswitch_0
    return-object p0

    .line 4497
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 4498
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 4502
    :cond_2
    iput v0, p0, Lmog;->b:I

    goto :goto_0

    .line 4504
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lmog;->b:I

    goto :goto_0

    .line 4509
    :sswitch_2
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lmog;->c:Ljava/lang/Long;

    goto :goto_0

    .line 4513
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmog;->d:Ljava/lang/Integer;

    goto :goto_0

    .line 4482
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 4443
    iget v0, p0, Lmog;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 4444
    const/4 v0, 0x1

    iget v1, p0, Lmog;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 4446
    :cond_0
    iget-object v0, p0, Lmog;->c:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 4447
    const/4 v0, 0x2

    iget-object v1, p0, Lmog;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 4449
    :cond_1
    iget-object v0, p0, Lmog;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 4450
    const/4 v0, 0x3

    iget-object v1, p0, Lmog;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 4452
    :cond_2
    iget-object v0, p0, Lmog;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 4454
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 4420
    invoke-virtual {p0, p1}, Lmog;->a(Loxn;)Lmog;

    move-result-object v0

    return-object v0
.end method
