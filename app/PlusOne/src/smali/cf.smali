.class public final Lcf;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbt;


# instance fields
.field private a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lbn;",
            ">;"
        }
    .end annotation
.end field

.field private b:I

.field private c:Landroid/app/PendingIntent;

.field private d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/Notification;",
            ">;"
        }
    .end annotation
.end field

.field private e:Landroid/graphics/Bitmap;

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2303
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2287
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcf;->a:Ljava/util/ArrayList;

    .line 2288
    const/4 v0, 0x1

    iput v0, p0, Lcf;->b:I

    .line 2290
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcf;->d:Ljava/util/ArrayList;

    .line 2293
    const v0, 0x800005

    iput v0, p0, Lcf;->g:I

    .line 2294
    const/4 v0, -0x1

    iput v0, p0, Lcf;->h:I

    .line 2295
    const/4 v0, 0x0

    iput v0, p0, Lcf;->i:I

    .line 2297
    const/16 v0, 0x50

    iput v0, p0, Lcf;->k:I

    .line 2304
    return-void
.end method


# virtual methods
.method public a(Lbs;)Lbs;
    .locals 5

    .prologue
    .line 2346
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2348
    iget-object v0, p0, Lcf;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2349
    const-string v2, "actions"

    invoke-static {}, Lbm;->a()Lbv;

    move-result-object v3

    iget-object v0, p0, Lcf;->a:Ljava/util/ArrayList;

    iget-object v4, p0, Lcf;->a:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v4, v4, [Lbn;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbn;

    invoke-interface {v3, v0}, Lbv;->a([Lbn;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2353
    :cond_0
    iget v0, p0, Lcf;->b:I

    const/4 v2, 0x1

    if-eq v0, v2, :cond_1

    .line 2354
    const-string v0, "flags"

    iget v2, p0, Lcf;->b:I

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2356
    :cond_1
    iget-object v0, p0, Lcf;->c:Landroid/app/PendingIntent;

    if-eqz v0, :cond_2

    .line 2357
    const-string v0, "displayIntent"

    iget-object v2, p0, Lcf;->c:Landroid/app/PendingIntent;

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2359
    :cond_2
    iget-object v0, p0, Lcf;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2360
    const-string v2, "pages"

    iget-object v0, p0, Lcf;->d:Ljava/util/ArrayList;

    iget-object v3, p0, Lcf;->d:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v3, v3, [Landroid/app/Notification;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/os/Parcelable;

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 2363
    :cond_3
    iget-object v0, p0, Lcf;->e:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_4

    .line 2364
    const-string v0, "background"

    iget-object v2, p0, Lcf;->e:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2366
    :cond_4
    iget v0, p0, Lcf;->f:I

    if-eqz v0, :cond_5

    .line 2367
    const-string v0, "contentIcon"

    iget v2, p0, Lcf;->f:I

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2369
    :cond_5
    iget v0, p0, Lcf;->g:I

    const v2, 0x800005

    if-eq v0, v2, :cond_6

    .line 2370
    const-string v0, "contentIconGravity"

    iget v2, p0, Lcf;->g:I

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2372
    :cond_6
    iget v0, p0, Lcf;->h:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_7

    .line 2373
    const-string v0, "contentActionIndex"

    iget v2, p0, Lcf;->h:I

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2376
    :cond_7
    iget v0, p0, Lcf;->i:I

    if-eqz v0, :cond_8

    .line 2377
    const-string v0, "customSizePreset"

    iget v2, p0, Lcf;->i:I

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2379
    :cond_8
    iget v0, p0, Lcf;->j:I

    if-eqz v0, :cond_9

    .line 2380
    const-string v0, "customContentHeight"

    iget v2, p0, Lcf;->j:I

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2382
    :cond_9
    iget v0, p0, Lcf;->k:I

    const/16 v2, 0x50

    if-eq v0, v2, :cond_a

    .line 2383
    const-string v0, "gravity"

    iget v2, p0, Lcf;->k:I

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2386
    :cond_a
    invoke-virtual {p1}, Lbs;->a()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "android.wearable.EXTENSIONS"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2387
    return-object p1
.end method

.method public a()Lcf;
    .locals 3

    .prologue
    .line 2392
    new-instance v0, Lcf;

    invoke-direct {v0}, Lcf;-><init>()V

    .line 2393
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcf;->a:Ljava/util/ArrayList;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, v0, Lcf;->a:Ljava/util/ArrayList;

    .line 2394
    iget v1, p0, Lcf;->b:I

    iput v1, v0, Lcf;->b:I

    .line 2395
    iget-object v1, p0, Lcf;->c:Landroid/app/PendingIntent;

    iput-object v1, v0, Lcf;->c:Landroid/app/PendingIntent;

    .line 2396
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcf;->d:Ljava/util/ArrayList;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, v0, Lcf;->d:Ljava/util/ArrayList;

    .line 2397
    iget-object v1, p0, Lcf;->e:Landroid/graphics/Bitmap;

    iput-object v1, v0, Lcf;->e:Landroid/graphics/Bitmap;

    .line 2398
    iget v1, p0, Lcf;->f:I

    iput v1, v0, Lcf;->f:I

    .line 2399
    iget v1, p0, Lcf;->g:I

    iput v1, v0, Lcf;->g:I

    .line 2400
    iget v1, p0, Lcf;->h:I

    iput v1, v0, Lcf;->h:I

    .line 2401
    iget v1, p0, Lcf;->i:I

    iput v1, v0, Lcf;->i:I

    .line 2402
    iget v1, p0, Lcf;->j:I

    iput v1, v0, Lcf;->j:I

    .line 2403
    iget v1, p0, Lcf;->k:I

    iput v1, v0, Lcf;->k:I

    .line 2404
    return-object v0
.end method

.method public a(Lbn;)Lcf;
    .locals 1

    .prologue
    .line 2420
    iget-object v0, p0, Lcf;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2421
    return-object p0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2204
    invoke-virtual {p0}, Lcf;->a()Lcf;

    move-result-object v0

    return-object v0
.end method
