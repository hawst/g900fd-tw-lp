.class public final Lpxz;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lpxz;


# instance fields
.field public b:I

.field public c:Ljava/lang/String;

.field public d:I

.field public e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 302
    const/4 v0, 0x0

    new-array v0, v0, [Lpxz;

    sput-object v0, Lpxz;->a:[Lpxz;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/high16 v0, -0x80000000

    .line 303
    invoke-direct {p0}, Loxq;-><init>()V

    .line 306
    iput v0, p0, Lpxz;->b:I

    .line 311
    iput v0, p0, Lpxz;->d:I

    .line 314
    iput v0, p0, Lpxz;->e:I

    .line 303
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    const/high16 v3, -0x80000000

    .line 337
    const/4 v0, 0x0

    .line 338
    iget v1, p0, Lpxz;->b:I

    if-eq v1, v3, :cond_0

    .line 339
    const/4 v0, 0x1

    iget v1, p0, Lpxz;->b:I

    .line 340
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 342
    :cond_0
    iget-object v1, p0, Lpxz;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 343
    const/4 v1, 0x2

    iget-object v2, p0, Lpxz;->c:Ljava/lang/String;

    .line 344
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 346
    :cond_1
    iget v1, p0, Lpxz;->d:I

    if-eq v1, v3, :cond_2

    .line 347
    const/4 v1, 0x3

    iget v2, p0, Lpxz;->d:I

    .line 348
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 350
    :cond_2
    iget v1, p0, Lpxz;->e:I

    if-eq v1, v3, :cond_3

    .line 351
    const/4 v1, 0x4

    iget v2, p0, Lpxz;->e:I

    .line 352
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 354
    :cond_3
    iget-object v1, p0, Lpxz;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 355
    iput v0, p0, Lpxz;->ai:I

    .line 356
    return v0
.end method

.method public a(Loxn;)Lpxz;
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 364
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 365
    sparse-switch v0, :sswitch_data_0

    .line 369
    iget-object v1, p0, Lpxz;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 370
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpxz;->ah:Ljava/util/List;

    .line 373
    :cond_1
    iget-object v1, p0, Lpxz;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 375
    :sswitch_0
    return-object p0

    .line 380
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 381
    if-eqz v0, :cond_2

    if-eq v0, v3, :cond_2

    if-eq v0, v4, :cond_2

    const/16 v1, 0xb

    if-eq v0, v1, :cond_2

    const/16 v1, 0xc

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd

    if-eq v0, v1, :cond_2

    const/16 v1, 0xe

    if-eq v0, v1, :cond_2

    const/16 v1, 0xf

    if-ne v0, v1, :cond_3

    .line 389
    :cond_2
    iput v0, p0, Lpxz;->b:I

    goto :goto_0

    .line 391
    :cond_3
    iput v2, p0, Lpxz;->b:I

    goto :goto_0

    .line 396
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpxz;->c:Ljava/lang/String;

    goto :goto_0

    .line 400
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 401
    if-eqz v0, :cond_4

    if-eq v0, v3, :cond_4

    if-eq v0, v4, :cond_4

    if-eq v0, v5, :cond_4

    if-eq v0, v6, :cond_4

    const/4 v1, 0x5

    if-eq v0, v1, :cond_4

    const/4 v1, 0x6

    if-eq v0, v1, :cond_4

    const/4 v1, 0x7

    if-eq v0, v1, :cond_4

    const/16 v1, 0x8

    if-eq v0, v1, :cond_4

    const/16 v1, 0x9

    if-eq v0, v1, :cond_4

    const/16 v1, 0xa

    if-ne v0, v1, :cond_5

    .line 412
    :cond_4
    iput v0, p0, Lpxz;->d:I

    goto :goto_0

    .line 414
    :cond_5
    iput v2, p0, Lpxz;->d:I

    goto :goto_0

    .line 419
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 420
    if-eqz v0, :cond_6

    if-eq v0, v3, :cond_6

    if-eq v0, v4, :cond_6

    if-eq v0, v5, :cond_6

    if-eq v0, v6, :cond_6

    const/4 v1, 0x5

    if-eq v0, v1, :cond_6

    const/4 v1, 0x6

    if-eq v0, v1, :cond_6

    const/4 v1, 0x7

    if-eq v0, v1, :cond_6

    const/16 v1, 0x8

    if-eq v0, v1, :cond_6

    const/16 v1, 0x9

    if-eq v0, v1, :cond_6

    const/16 v1, 0xa

    if-ne v0, v1, :cond_7

    .line 431
    :cond_6
    iput v0, p0, Lpxz;->e:I

    goto/16 :goto_0

    .line 433
    :cond_7
    iput v2, p0, Lpxz;->e:I

    goto/16 :goto_0

    .line 365
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 319
    iget v0, p0, Lpxz;->b:I

    if-eq v0, v2, :cond_0

    .line 320
    const/4 v0, 0x1

    iget v1, p0, Lpxz;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 322
    :cond_0
    iget-object v0, p0, Lpxz;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 323
    const/4 v0, 0x2

    iget-object v1, p0, Lpxz;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 325
    :cond_1
    iget v0, p0, Lpxz;->d:I

    if-eq v0, v2, :cond_2

    .line 326
    const/4 v0, 0x3

    iget v1, p0, Lpxz;->d:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 328
    :cond_2
    iget v0, p0, Lpxz;->e:I

    if-eq v0, v2, :cond_3

    .line 329
    const/4 v0, 0x4

    iget v1, p0, Lpxz;->e:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 331
    :cond_3
    iget-object v0, p0, Lpxz;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 333
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 299
    invoke-virtual {p0, p1}, Lpxz;->a(Loxn;)Lpxz;

    move-result-object v0

    return-object v0
.end method
