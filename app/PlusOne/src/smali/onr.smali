.class public final Lonr;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lonr;


# instance fields
.field public b:I

.field public c:Lonw;

.field public d:Lons;

.field public e:Lont;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 915
    const/4 v0, 0x0

    new-array v0, v0, [Lonr;

    sput-object v0, Lonr;->a:[Lonr;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 916
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1003
    const/high16 v0, -0x80000000

    iput v0, p0, Lonr;->b:I

    .line 1006
    iput-object v1, p0, Lonr;->c:Lonw;

    .line 1009
    iput-object v1, p0, Lonr;->d:Lons;

    .line 1012
    iput-object v1, p0, Lonr;->e:Lont;

    .line 916
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1035
    const/4 v0, 0x0

    .line 1036
    iget v1, p0, Lonr;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 1037
    const/4 v0, 0x1

    iget v1, p0, Lonr;->b:I

    .line 1038
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1040
    :cond_0
    iget-object v1, p0, Lonr;->c:Lonw;

    if-eqz v1, :cond_1

    .line 1041
    const/4 v1, 0x2

    iget-object v2, p0, Lonr;->c:Lonw;

    .line 1042
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1044
    :cond_1
    iget-object v1, p0, Lonr;->d:Lons;

    if-eqz v1, :cond_2

    .line 1045
    const/4 v1, 0x3

    iget-object v2, p0, Lonr;->d:Lons;

    .line 1046
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1048
    :cond_2
    iget-object v1, p0, Lonr;->e:Lont;

    if-eqz v1, :cond_3

    .line 1049
    const/4 v1, 0x4

    iget-object v2, p0, Lonr;->e:Lont;

    .line 1050
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1052
    :cond_3
    iget-object v1, p0, Lonr;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1053
    iput v0, p0, Lonr;->ai:I

    .line 1054
    return v0
.end method

.method public a(Loxn;)Lonr;
    .locals 2

    .prologue
    .line 1062
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1063
    sparse-switch v0, :sswitch_data_0

    .line 1067
    iget-object v1, p0, Lonr;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1068
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lonr;->ah:Ljava/util/List;

    .line 1071
    :cond_1
    iget-object v1, p0, Lonr;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1073
    :sswitch_0
    return-object p0

    .line 1078
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1079
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-ne v0, v1, :cond_3

    .line 1087
    :cond_2
    iput v0, p0, Lonr;->b:I

    goto :goto_0

    .line 1089
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lonr;->b:I

    goto :goto_0

    .line 1094
    :sswitch_2
    iget-object v0, p0, Lonr;->c:Lonw;

    if-nez v0, :cond_4

    .line 1095
    new-instance v0, Lonw;

    invoke-direct {v0}, Lonw;-><init>()V

    iput-object v0, p0, Lonr;->c:Lonw;

    .line 1097
    :cond_4
    iget-object v0, p0, Lonr;->c:Lonw;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1101
    :sswitch_3
    iget-object v0, p0, Lonr;->d:Lons;

    if-nez v0, :cond_5

    .line 1102
    new-instance v0, Lons;

    invoke-direct {v0}, Lons;-><init>()V

    iput-object v0, p0, Lonr;->d:Lons;

    .line 1104
    :cond_5
    iget-object v0, p0, Lonr;->d:Lons;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1108
    :sswitch_4
    iget-object v0, p0, Lonr;->e:Lont;

    if-nez v0, :cond_6

    .line 1109
    new-instance v0, Lont;

    invoke-direct {v0}, Lont;-><init>()V

    iput-object v0, p0, Lonr;->e:Lont;

    .line 1111
    :cond_6
    iget-object v0, p0, Lonr;->e:Lont;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1063
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1017
    iget v0, p0, Lonr;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 1018
    const/4 v0, 0x1

    iget v1, p0, Lonr;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1020
    :cond_0
    iget-object v0, p0, Lonr;->c:Lonw;

    if-eqz v0, :cond_1

    .line 1021
    const/4 v0, 0x2

    iget-object v1, p0, Lonr;->c:Lonw;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1023
    :cond_1
    iget-object v0, p0, Lonr;->d:Lons;

    if-eqz v0, :cond_2

    .line 1024
    const/4 v0, 0x3

    iget-object v1, p0, Lonr;->d:Lons;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1026
    :cond_2
    iget-object v0, p0, Lonr;->e:Lont;

    if-eqz v0, :cond_3

    .line 1027
    const/4 v0, 0x4

    iget-object v1, p0, Lonr;->e:Lont;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1029
    :cond_3
    iget-object v0, p0, Lonr;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1031
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 912
    invoke-virtual {p0, p1}, Lonr;->a(Loxn;)Lonr;

    move-result-object v0

    return-object v0
.end method
