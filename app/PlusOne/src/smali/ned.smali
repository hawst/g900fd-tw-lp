.class public final Lned;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/Boolean;

.field public d:Ljava/lang/String;

.field private e:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 837
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 869
    const/4 v0, 0x1

    iget-object v1, p0, Lned;->a:Ljava/lang/String;

    .line 871
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 872
    const/4 v1, 0x2

    iget-object v2, p0, Lned;->b:Ljava/lang/String;

    .line 873
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 874
    iget-object v1, p0, Lned;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 875
    const/4 v1, 0x3

    iget-object v2, p0, Lned;->c:Ljava/lang/Boolean;

    .line 876
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 878
    :cond_0
    iget-object v1, p0, Lned;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 879
    const/4 v1, 0x4

    iget-object v2, p0, Lned;->e:Ljava/lang/Boolean;

    .line 880
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 882
    :cond_1
    iget-object v1, p0, Lned;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 883
    const/4 v1, 0x5

    iget-object v2, p0, Lned;->d:Ljava/lang/String;

    .line 884
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 886
    :cond_2
    iget-object v1, p0, Lned;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 887
    iput v0, p0, Lned;->ai:I

    .line 888
    return v0
.end method

.method public a(Loxn;)Lned;
    .locals 2

    .prologue
    .line 896
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 897
    sparse-switch v0, :sswitch_data_0

    .line 901
    iget-object v1, p0, Lned;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 902
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lned;->ah:Ljava/util/List;

    .line 905
    :cond_1
    iget-object v1, p0, Lned;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 907
    :sswitch_0
    return-object p0

    .line 912
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lned;->a:Ljava/lang/String;

    goto :goto_0

    .line 916
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lned;->b:Ljava/lang/String;

    goto :goto_0

    .line 920
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lned;->c:Ljava/lang/Boolean;

    goto :goto_0

    .line 924
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lned;->e:Ljava/lang/Boolean;

    goto :goto_0

    .line 928
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lned;->d:Ljava/lang/String;

    goto :goto_0

    .line 897
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 852
    const/4 v0, 0x1

    iget-object v1, p0, Lned;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 853
    const/4 v0, 0x2

    iget-object v1, p0, Lned;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 854
    iget-object v0, p0, Lned;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 855
    const/4 v0, 0x3

    iget-object v1, p0, Lned;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 857
    :cond_0
    iget-object v0, p0, Lned;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 858
    const/4 v0, 0x4

    iget-object v1, p0, Lned;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 860
    :cond_1
    iget-object v0, p0, Lned;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 861
    const/4 v0, 0x5

    iget-object v1, p0, Lned;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 863
    :cond_2
    iget-object v0, p0, Lned;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 865
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 833
    invoke-virtual {p0, p1}, Lned;->a(Loxn;)Lned;

    move-result-object v0

    return-object v0
.end method
