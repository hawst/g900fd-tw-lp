.class public final Lmkp;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnaj;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 22516
    invoke-direct {p0}, Loxq;-><init>()V

    .line 22519
    iput-object v0, p0, Lmkp;->apiHeader:Llyr;

    .line 22522
    iput-object v0, p0, Lmkp;->a:Lnaj;

    .line 22516
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 22539
    const/4 v0, 0x0

    .line 22540
    iget-object v1, p0, Lmkp;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 22541
    const/4 v0, 0x1

    iget-object v1, p0, Lmkp;->apiHeader:Llyr;

    .line 22542
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 22544
    :cond_0
    iget-object v1, p0, Lmkp;->a:Lnaj;

    if-eqz v1, :cond_1

    .line 22545
    const/4 v1, 0x2

    iget-object v2, p0, Lmkp;->a:Lnaj;

    .line 22546
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 22548
    :cond_1
    iget-object v1, p0, Lmkp;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 22549
    iput v0, p0, Lmkp;->ai:I

    .line 22550
    return v0
.end method

.method public a(Loxn;)Lmkp;
    .locals 2

    .prologue
    .line 22558
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 22559
    sparse-switch v0, :sswitch_data_0

    .line 22563
    iget-object v1, p0, Lmkp;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 22564
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmkp;->ah:Ljava/util/List;

    .line 22567
    :cond_1
    iget-object v1, p0, Lmkp;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 22569
    :sswitch_0
    return-object p0

    .line 22574
    :sswitch_1
    iget-object v0, p0, Lmkp;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 22575
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmkp;->apiHeader:Llyr;

    .line 22577
    :cond_2
    iget-object v0, p0, Lmkp;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 22581
    :sswitch_2
    iget-object v0, p0, Lmkp;->a:Lnaj;

    if-nez v0, :cond_3

    .line 22582
    new-instance v0, Lnaj;

    invoke-direct {v0}, Lnaj;-><init>()V

    iput-object v0, p0, Lmkp;->a:Lnaj;

    .line 22584
    :cond_3
    iget-object v0, p0, Lmkp;->a:Lnaj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 22559
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 22527
    iget-object v0, p0, Lmkp;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 22528
    const/4 v0, 0x1

    iget-object v1, p0, Lmkp;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 22530
    :cond_0
    iget-object v0, p0, Lmkp;->a:Lnaj;

    if-eqz v0, :cond_1

    .line 22531
    const/4 v0, 0x2

    iget-object v1, p0, Lmkp;->a:Lnaj;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 22533
    :cond_1
    iget-object v0, p0, Lmkp;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 22535
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 22512
    invoke-virtual {p0, p1}, Lmkp;->a(Loxn;)Lmkp;

    move-result-object v0

    return-object v0
.end method
