.class public Ldbj;
.super Llol;
.source "PG"


# instance fields
.field private final N:Ldbl;

.field private final O:Ldbm;

.field private final P:Ldbn;

.field private final Q:Ldbo;

.field private R:Ldgi;

.field private S:Ldeo;

.field private T:I

.field private U:Z

.field private V:Landroid/widget/Button;

.field private W:Landroid/widget/Toast;

.field private X:Landroid/widget/Toast;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 33
    invoke-direct {p0}, Llol;-><init>()V

    .line 39
    new-instance v0, Ldbl;

    invoke-direct {v0, p0}, Ldbl;-><init>(Ldbj;)V

    iput-object v0, p0, Ldbj;->N:Ldbl;

    .line 41
    new-instance v0, Ldbm;

    invoke-direct {v0, p0}, Ldbm;-><init>(Ldbj;)V

    iput-object v0, p0, Ldbj;->O:Ldbm;

    .line 43
    new-instance v0, Ldbn;

    invoke-direct {v0, p0}, Ldbn;-><init>(Ldbj;)V

    iput-object v0, p0, Ldbj;->P:Ldbn;

    .line 45
    new-instance v0, Ldbo;

    invoke-direct {v0, p0}, Ldbo;-><init>(Ldbj;)V

    iput-object v0, p0, Ldbj;->Q:Ldbo;

    .line 60
    new-instance v0, Ldep;

    iget-object v1, p0, Ldbj;->av:Llqm;

    new-instance v2, Ldbk;

    invoke-direct {v2, p0}, Ldbk;-><init>(Ldbj;)V

    invoke-direct {v0, v1, v2}, Ldep;-><init>(Llqr;Ldeq;)V

    .line 485
    return-void
.end method

.method static synthetic a(Ldbj;)V
    .locals 10

    .prologue
    const/16 v9, 0x8

    const/4 v1, 0x1

    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 33
    iget-object v0, p0, Ldbj;->S:Ldeo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldbj;->S:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldbj;->S:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    invoke-interface {v0}, Lddl;->g()Lnym;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldbj;->V:Landroid/widget/Button;

    if-nez v0, :cond_2

    :cond_0
    iget-object v0, p0, Ldbj;->V:Landroid/widget/Button;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldbj;->V:Landroid/widget/Button;

    invoke-virtual {v0, v9}, Landroid/widget/Button;->setVisibility(I)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Ldbj;->S:Ldeo;

    invoke-virtual {v0}, Ldeo;->c()Z

    move-result v4

    iget-object v0, p0, Ldbj;->S:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    invoke-interface {v0}, Lddl;->e()Lnzi;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Ldbj;->S:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    invoke-interface {v0}, Lddl;->e()Lnzi;

    move-result-object v0

    iget-object v5, p0, Ldbj;->S:Ldeo;

    invoke-virtual {v5}, Ldeo;->a()Lddl;

    move-result-object v5

    invoke-interface {v5}, Lddl;->f()Lnzi;

    move-result-object v5

    if-ne v0, v5, :cond_4

    move v0, v1

    :goto_1
    iget-object v5, p0, Ldbj;->S:Ldeo;

    invoke-virtual {v5}, Ldeo;->a()Lddl;

    move-result-object v5

    invoke-interface {v5}, Lddl;->g()Lnym;

    move-result-object v6

    invoke-interface {v5}, Lddl;->a()Lizu;

    move-result-object v7

    invoke-virtual {v7}, Lizu;->g()Ljac;

    move-result-object v7

    invoke-interface {v5}, Lddl;->e()Lnzi;

    move-result-object v8

    invoke-static {v8}, Ljub;->b(Lnzi;)Z

    move-result v8

    invoke-static {v6, v7, v4, v0, v8}, Ljvd;->a(Lnym;Ljac;ZZZ)I

    move-result v0

    iput v0, p0, Ldbj;->T:I

    iget v0, p0, Ldbj;->T:I

    sparse-switch v0, :sswitch_data_0

    const/4 v0, 0x2

    :goto_2
    packed-switch v0, :pswitch_data_0

    move v0, v3

    :goto_3
    iget v4, p0, Ldbj;->T:I

    packed-switch v4, :pswitch_data_1

    :pswitch_0
    move v4, v3

    :goto_4
    if-eq v0, v3, :cond_5

    iget-object v6, p0, Ldbj;->V:Landroid/widget/Button;

    invoke-virtual {v6, v0, v2, v2, v2}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    :goto_5
    if-eq v4, v3, :cond_6

    iget-object v6, p0, Ldbj;->V:Landroid/widget/Button;

    invoke-virtual {v6, v4}, Landroid/widget/Button;->setText(I)V

    iget-object v6, p0, Ldbj;->V:Landroid/widget/Button;

    iget-object v7, p0, Ldbj;->at:Llnl;

    invoke-virtual {v7}, Llnl;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v6, p0, Ldbj;->V:Landroid/widget/Button;

    invoke-virtual {v6, v9}, Landroid/widget/Button;->setCompoundDrawablePadding(I)V

    :goto_6
    if-ne v4, v3, :cond_3

    if-eq v0, v3, :cond_7

    :cond_3
    :goto_7
    if-eqz v1, :cond_8

    invoke-interface {v5}, Lddl;->L()Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Ldbj;->V:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto/16 :goto_0

    :cond_4
    move v0, v2

    goto :goto_1

    :sswitch_0
    move v0, v1

    goto :goto_2

    :sswitch_1
    const/4 v0, 0x3

    goto :goto_2

    :sswitch_2
    move v0, v2

    goto :goto_2

    :pswitch_1
    const v0, 0x7f0201cf

    goto :goto_3

    :pswitch_2
    const v0, 0x7f020359

    goto :goto_3

    :pswitch_3
    const v0, 0x7f02019a

    goto :goto_3

    :pswitch_4
    const v4, 0x7f0a08c6

    goto :goto_4

    :pswitch_5
    const v4, 0x7f0a08c8

    goto :goto_4

    :pswitch_6
    const v4, 0x7f0a08c9

    goto :goto_4

    :pswitch_7
    const v4, 0x7f0a08ca

    goto :goto_4

    :cond_5
    iget-object v6, p0, Ldbj;->V:Landroid/widget/Button;

    invoke-virtual {v6, v2, v2, v2, v2}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto :goto_5

    :cond_6
    iget-object v6, p0, Ldbj;->V:Landroid/widget/Button;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v6, p0, Ldbj;->V:Landroid/widget/Button;

    iget-object v7, p0, Ldbj;->at:Llnl;

    invoke-virtual {v7}, Llnl;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0a065d

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v6, p0, Ldbj;->V:Landroid/widget/Button;

    invoke-virtual {v6, v2}, Landroid/widget/Button;->setCompoundDrawablePadding(I)V

    goto :goto_6

    :cond_7
    move v1, v2

    goto :goto_7

    :cond_8
    iget-object v0, p0, Ldbj;->V:Landroid/widget/Button;

    invoke-virtual {v0, v9}, Landroid/widget/Button;->setVisibility(I)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_2
        0x1 -> :sswitch_1
        0x2 -> :sswitch_0
        0xe -> :sswitch_0
        0x10 -> :sswitch_0
        0x11 -> :sswitch_0
        0x12 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x10
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method static synthetic a(Ldbj;I)V
    .locals 8

    .prologue
    const v1, 0x7f0201d0

    const/4 v3, 0x0

    const v0, 0x7f02035a

    .line 33
    invoke-direct {p0}, Ldbj;->e()Z

    move-result v2

    if-nez v2, :cond_0

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    const v2, 0x7f0a08b5

    const v1, 0x7f0a08cd

    move v4, v1

    move v5, v2

    move v2, v0

    :goto_1
    invoke-virtual {p0}, Ldbj;->n()Lz;

    move-result-object v6

    invoke-static {v6}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040189

    const/4 v7, 0x0

    invoke-virtual {v0, v1, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    if-nez v7, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must define a tooltip layout"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    const v2, 0x7f0a08b4

    const v1, 0x7f0a08cc

    move v4, v1

    move v5, v2

    move v2, v0

    goto :goto_1

    :pswitch_2
    const v2, 0x7f0a08b7

    const v1, 0x7f0a08ce

    move v4, v1

    move v5, v2

    move v2, v0

    goto :goto_1

    :pswitch_3
    const v2, 0x7f0a08b8

    const v1, 0x7f0a08d3

    move v4, v1

    move v5, v2

    move v2, v0

    goto :goto_1

    :pswitch_4
    const v2, 0x7f0a08ba

    const v1, 0x7f0a08cf

    move v4, v1

    move v5, v2

    move v2, v0

    goto :goto_1

    :pswitch_5
    const v2, 0x7f0a08bb

    const v1, 0x7f0a08d0

    move v4, v1

    move v5, v2

    move v2, v0

    goto :goto_1

    :pswitch_6
    const v2, 0x7f0a08bc

    const v1, 0x7f0a08d1

    move v4, v1

    move v5, v2

    move v2, v0

    goto :goto_1

    :pswitch_7
    const v2, 0x7f0a08bd

    const v1, 0x7f0a08d2

    move v4, v1

    move v5, v2

    move v2, v0

    goto :goto_1

    :pswitch_8
    const v2, 0x7f0a08bf

    const v1, 0x7f0a08d4

    move v4, v1

    move v5, v2

    move v2, v0

    goto :goto_1

    :pswitch_9
    const v2, 0x7f0a08c0

    const v1, 0x7f0a08d5

    move v4, v1

    move v5, v2

    move v2, v0

    goto :goto_1

    :pswitch_a
    const v2, 0x7f0a08c1

    const v1, 0x7f0a08da

    move v4, v1

    move v5, v2

    move v2, v0

    goto :goto_1

    :pswitch_b
    const v2, 0x7f0a08c5

    const v0, 0x7f0a08d6

    move v4, v0

    move v5, v2

    move v2, v1

    goto/16 :goto_1

    :pswitch_c
    const v2, 0x7f0a08c7

    const v0, 0x7f0a08d7

    move v4, v0

    move v5, v2

    move v2, v1

    goto/16 :goto_1

    :pswitch_d
    const v2, 0x7f0a08c8

    const v0, 0x7f0a08d8

    move v4, v0

    move v5, v2

    move v2, v1

    goto/16 :goto_1

    :pswitch_e
    const v1, 0x7f0a08c9

    const v0, 0x7f0a08d9

    move v2, v3

    move v4, v0

    move v5, v1

    goto/16 :goto_1

    :pswitch_f
    const v2, 0x7f0a08c2

    const v1, 0x7f0a08db

    move v4, v1

    move v5, v2

    move v2, v0

    goto/16 :goto_1

    :pswitch_10
    const v2, 0x7f0a08c3

    const v1, 0x7f0a08dc

    move v4, v1

    move v5, v2

    move v2, v0

    goto/16 :goto_1

    :pswitch_11
    const v2, 0x7f0a08c4

    const v1, 0x7f0a08dd

    move v4, v1

    move v5, v2

    move v2, v0

    goto/16 :goto_1

    :cond_1
    const v0, 0x7f1001e6

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f1001e9

    invoke-virtual {v7, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(I)V

    invoke-virtual {v0, v2, v3, v3, v3}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(I)V

    invoke-virtual {v1, v3, v3, v3, v3}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    iget-object v0, p0, Ldbj;->X:Landroid/widget/Toast;

    if-nez v0, :cond_2

    new-instance v0, Landroid/widget/Toast;

    invoke-direct {v0, v6}, Landroid/widget/Toast;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Ldbj;->X:Landroid/widget/Toast;

    iget-object v0, p0, Ldbj;->X:Landroid/widget/Toast;

    const/16 v1, 0x10

    invoke-virtual {v0, v1, v3, v3}, Landroid/widget/Toast;->setGravity(III)V

    iget-object v0, p0, Ldbj;->X:Landroid/widget/Toast;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Toast;->setDuration(I)V

    :cond_2
    iget-object v0, p0, Ldbj;->X:Landroid/widget/Toast;

    invoke-virtual {v0, v7}, Landroid/widget/Toast;->setView(Landroid/view/View;)V

    iget-object v0, p0, Ldbj;->X:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_f
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_10
        :pswitch_7
        :pswitch_7
        :pswitch_11
    .end packed-switch
.end method

.method static synthetic a(Ldbj;ZZ)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Ldbj;->a(ZZ)V

    return-void
.end method

.method private a(ZZ)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 124
    iget-object v0, p0, Ldbj;->S:Ldeo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldbj;->S:Ldeo;

    invoke-virtual {v0}, Ldeo;->e()Z

    move-result v0

    if-eq p1, v0, :cond_0

    if-eqz p1, :cond_1

    invoke-direct {p0}, Ldbj;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 142
    :cond_0
    :goto_0
    return-void

    .line 128
    :cond_1
    iget-object v0, p0, Ldbj;->S:Ldeo;

    invoke-virtual {v0, p1}, Ldeo;->c(Z)V

    .line 129
    invoke-direct {p0}, Ldbj;->d()V

    .line 130
    invoke-direct {p0}, Ldbj;->e()Z

    .line 131
    if-eqz p2, :cond_2

    .line 132
    iget-object v0, p0, Ldbj;->S:Ldeo;

    invoke-virtual {v0}, Ldeo;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Ldbj;->o()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a08cb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {p0}, Ldbj;->n()Lz;

    move-result-object v1

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Ldbj;->W:Landroid/widget/Toast;

    iget-object v0, p0, Ldbj;->W:Landroid/widget/Toast;

    const/16 v1, 0x31

    invoke-virtual {v0, v1, v2, v2}, Landroid/widget/Toast;->setGravity(III)V

    iget-object v0, p0, Ldbj;->W:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 135
    :cond_2
    iget-object v0, p0, Ldbj;->R:Ldgi;

    if-eqz v0, :cond_0

    .line 136
    iget v0, p0, Ldbj;->T:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 141
    :pswitch_0
    iget-object v0, p0, Ldbj;->R:Ldgi;

    sget-object v1, Lhmv;->dX:Lhmv;

    invoke-interface {v0, v1}, Ldgi;->a(Lhmv;)V

    goto :goto_0

    .line 132
    :cond_3
    iget v0, p0, Ldbj;->T:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    invoke-virtual {p0}, Ldbj;->o()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a08b3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_4
    invoke-virtual {p0}, Ldbj;->o()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a08b2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 138
    :pswitch_1
    iget-object v0, p0, Ldbj;->R:Ldgi;

    sget-object v1, Lhmv;->dY:Lhmv;

    invoke-interface {v0, v1}, Ldgi;->a(Lhmv;)V

    goto :goto_0

    .line 136
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 254
    iget v1, p0, Ldbj;->T:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    iget v1, p0, Ldbj;->T:I

    if-ne v1, v0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Ldbj;Z)Z
    .locals 0

    .prologue
    .line 33
    iput-boolean p1, p0, Ldbj;->U:Z

    return p1
.end method

.method static synthetic b(Ldbj;)Z
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ldbj;->a()Z

    move-result v0

    return v0
.end method

.method static synthetic c(Ldbj;)Ldeo;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Ldbj;->S:Ldeo;

    return-object v0
.end method

.method static synthetic d(Ldbj;)I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Ldbj;->T:I

    return v0
.end method

.method private d()V
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Ldbj;->W:Landroid/widget/Toast;

    if-eqz v0, :cond_0

    .line 260
    iget-object v0, p0, Ldbj;->W:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 262
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Ldbj;->W:Landroid/widget/Toast;

    .line 263
    return-void
.end method

.method private e()Z
    .locals 1

    .prologue
    .line 266
    iget-object v0, p0, Ldbj;->X:Landroid/widget/Toast;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldbj;->X:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 267
    iget-object v0, p0, Ldbj;->X:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 268
    const/4 v0, 0x0

    iput-object v0, p0, Ldbj;->X:Landroid/widget/Toast;

    .line 269
    const/4 v0, 0x1

    .line 272
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic e(Ldbj;)Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Ldbj;->U:Z

    return v0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 86
    const v0, 0x7f040187

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Ldbj;->V:Landroid/widget/Button;

    .line 89
    iget-object v0, p0, Ldbj;->V:Landroid/widget/Button;

    iget-object v1, p0, Ldbj;->N:Ldbl;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 90
    iget-object v0, p0, Ldbj;->V:Landroid/widget/Button;

    iget-object v1, p0, Ldbj;->O:Ldbm;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 91
    iget-object v0, p0, Ldbj;->V:Landroid/widget/Button;

    iget-object v1, p0, Ldbj;->P:Ldbn;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 93
    iget-object v0, p0, Ldbj;->V:Landroid/widget/Button;

    return-object v0
.end method

.method public aO_()V
    .locals 2

    .prologue
    .line 105
    invoke-super {p0}, Llol;->aO_()V

    .line 106
    iget-object v0, p0, Ldbj;->R:Ldgi;

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Ldbj;->R:Ldgi;

    iget-object v1, p0, Ldbj;->Q:Ldbo;

    invoke-interface {v0, v1}, Ldgi;->a(Ldgl;)V

    .line 109
    :cond_0
    return-void
.end method

.method public ae_()V
    .locals 0

    .prologue
    .line 98
    invoke-super {p0}, Llol;->ae_()V

    .line 99
    invoke-direct {p0}, Ldbj;->d()V

    .line 100
    invoke-direct {p0}, Ldbj;->e()Z

    .line 101
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 70
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 72
    iget-object v0, p0, Ldbj;->au:Llnh;

    const-class v1, Ldgi;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldgi;

    iput-object v0, p0, Ldbj;->R:Ldgi;

    .line 74
    iget-object v0, p0, Ldbj;->au:Llnh;

    const-class v1, Ldeo;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldeo;

    iput-object v0, p0, Ldbj;->S:Ldeo;

    .line 75
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 79
    invoke-super {p0}, Llol;->f()V

    .line 80
    invoke-direct {p0, v0, v0}, Ldbj;->a(ZZ)V

    .line 81
    return-void
.end method

.method public z()V
    .locals 2

    .prologue
    .line 113
    invoke-super {p0}, Llol;->z()V

    .line 114
    iget-object v0, p0, Ldbj;->R:Ldgi;

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Ldbj;->R:Ldgi;

    iget-object v1, p0, Ldbj;->Q:Ldbo;

    invoke-interface {v0, v1}, Ldgi;->b(Ldgl;)V

    .line 117
    :cond_0
    return-void
.end method
