.class public final Lktn;
.super Lhye;
.source "PG"


# instance fields
.field private final b:I

.field private final c:Ljava/lang/String;

.field private d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 28
    invoke-static {p3}, Lktp;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lhye;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    .line 22
    const/4 v0, 0x0

    iput-boolean v0, p0, Lktn;->d:Z

    .line 29
    iput p2, p0, Lktn;->b:I

    .line 30
    iput-object p3, p0, Lktn;->c:Ljava/lang/String;

    .line 31
    invoke-virtual {p0, p4}, Lktn;->a([Ljava/lang/String;)V

    .line 32
    return-void
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/Cursor;
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 73
    const-string v1, "squares"

    invoke-virtual {p0}, Lktn;->j()[Ljava/lang/String;

    move-result-object v2

    const-string v3, "square_id=?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    iget-object v6, p0, Lktn;->c:Ljava/lang/String;

    aput-object v6, v4, v0

    move-object v0, p1

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public C()Landroid/database/Cursor;
    .locals 10

    .prologue
    const/4 v1, 0x1

    .line 36
    invoke-virtual {p0}, Lktn;->n()Landroid/content/Context;

    move-result-object v3

    .line 38
    invoke-virtual {p0}, Lktn;->n()Landroid/content/Context;

    move-result-object v0

    iget v2, p0, Lktn;->b:I

    invoke-static {v0, v2}, Lhzt;->b(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 39
    invoke-direct {p0, v4}, Lktn;->a(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/Cursor;

    move-result-object v2

    .line 41
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lktn;->d:Z

    .line 42
    iget-boolean v0, p0, Lktn;->d:Z

    if-nez v0, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 43
    const-string v0, "last_sync"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 44
    if-ltz v0, :cond_0

    .line 45
    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 46
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long v6, v8, v6

    const-wide/32 v8, 0x124f80

    cmp-long v0, v6, v8

    if-lez v0, :cond_0

    .line 48
    iput-boolean v1, p0, Lktn;->d:Z

    .line 50
    :cond_0
    iget-boolean v0, p0, Lktn;->d:Z

    if-eqz v0, :cond_3

    .line 56
    new-instance v0, Lkti;

    iget v1, p0, Lktn;->b:I

    iget-object v5, p0, Lktn;->c:Ljava/lang/String;

    invoke-direct {v0, v3, v1, v5}, Lkti;-><init>(Landroid/content/Context;ILjava/lang/String;)V

    .line 58
    invoke-static {v0}, Lhoc;->a(Lhny;)Lhoz;

    move-result-object v0

    .line 59
    invoke-virtual {v0}, Lhoz;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 69
    :goto_1
    return-object v2

    .line 41
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 65
    :cond_2
    invoke-direct {p0, v4}, Lktn;->a(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/Cursor;

    move-result-object v0

    :goto_2
    move-object v2, v0

    .line 69
    goto :goto_1

    :cond_3
    move-object v0, v2

    goto :goto_2
.end method
