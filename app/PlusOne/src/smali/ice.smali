.class public final Lice;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llqi;
.implements Llqz;
.implements Llra;
.implements Llrd;
.implements Llrg;


# instance fields
.field a:Lz;

.field final b:Lici;

.field c:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# direct methods
.method public constructor <init>(Llqr;Lici;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p2, p0, Lice;->b:Lici;

    .line 34
    invoke-virtual {p1, p0}, Llqr;->a(Llrg;)Llrg;

    .line 35
    return-void
.end method


# virtual methods
.method public E_()V
    .locals 2

    .prologue
    .line 113
    iget-object v0, p0, Lice;->c:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Lice;->a:Lz;

    iget-object v1, p0, Lice;->c:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-static {v0, v1}, Libq;->a(Landroid/content/Context;Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 116
    :cond_0
    return-void
.end method

.method public a(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 120
    check-cast p1, Lz;

    iput-object p1, p0, Lice;->a:Lz;

    .line 121
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 93
    iget-object v0, p0, Lice;->a:Lz;

    .line 94
    invoke-virtual {v0}, Lz;->f()Lae;

    move-result-object v0

    const-string v1, "editor_dialog_tag"

    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Licg;

    .line 96
    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    const-string v1, "download_triggered_by_user_was_running"

    const/4 v2, 0x0

    .line 97
    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 98
    iget-object v1, p0, Lice;->b:Lici;

    invoke-virtual {v0, v1}, Licg;->a(Lici;)V

    .line 99
    iget-object v0, p0, Lice;->a:Lz;

    new-instance v1, Licf;

    invoke-direct {v1, p0}, Licf;-><init>(Lice;)V

    .line 100
    invoke-static {v0, v1}, Libq;->a(Landroid/content/Context;Ljava/lang/Runnable;)Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    move-result-object v0

    iput-object v0, p0, Lice;->c:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .line 102
    :cond_0
    return-void
.end method

.method public b()V
    .locals 3

    .prologue
    .line 38
    iget-object v0, p0, Lice;->a:Lz;

    invoke-static {v0}, Libq;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 39
    new-instance v0, Licg;

    invoke-direct {v0}, Licg;-><init>()V

    .line 40
    iget-object v1, p0, Lice;->b:Lici;

    invoke-virtual {v0, v1}, Licg;->a(Lici;)V

    .line 41
    iget-object v1, p0, Lice;->a:Lz;

    invoke-virtual {v1}, Lz;->f()Lae;

    move-result-object v1

    const-string v2, "editor_dialog_tag"

    invoke-virtual {v0, v1, v2}, Licg;->a(Lae;Ljava/lang/String;)V

    .line 42
    iget-object v0, p0, Lice;->c:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    if-nez v0, :cond_0

    .line 43
    iget-object v0, p0, Lice;->a:Lz;

    new-instance v1, Licf;

    invoke-direct {v1, p0}, Licf;-><init>(Lice;)V

    .line 44
    invoke-static {v0, v1}, Libq;->a(Landroid/content/Context;Ljava/lang/Runnable;)Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    move-result-object v0

    iput-object v0, p0, Lice;->c:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .line 47
    :cond_0
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 106
    if-eqz p1, :cond_0

    .line 107
    const-string v1, "download_triggered_by_user_was_running"

    iget-object v0, p0, Lice;->c:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 109
    :cond_0
    return-void

    .line 107
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
