.class public final Lfyo;
.super Lgbz;
.source "PG"


# instance fields
.field private A:Lkzu;

.field private B:I

.field private y:Lldg;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lfyo;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lfyo;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0, p1, p2, p3}, Lgbz;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    new-instance v0, Lldg;

    invoke-direct {v0, p1, p2, p3}, Lldg;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lfyo;->y:Lldg;

    .line 43
    return-void
.end method


# virtual methods
.method protected a(III)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 82
    iput p2, p0, Lfyo;->B:I

    .line 83
    iget-object v0, p0, Lfyo;->y:Lldg;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {p3, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 84
    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 83
    invoke-virtual {v0, v1, v2}, Lldg;->measure(II)V

    .line 85
    iget-object v0, p0, Lfyo;->y:Lldg;

    invoke-virtual {v0}, Lldg;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v0, p2

    return v0
.end method

.method protected a(Landroid/graphics/Canvas;I)I
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lfyo;->y:Lldg;

    invoke-virtual {v0}, Lldg;->getHeight()I

    move-result v0

    add-int/2addr v0, p2

    return v0
.end method

.method protected a(Landroid/database/Cursor;J)Landroid/text/SpannableStringBuilder;
    .locals 8

    .prologue
    const/16 v7, 0x11

    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 153
    invoke-super {p0, p1, p2, p3}, Lgbz;->a(Landroid/database/Cursor;J)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 154
    const/16 v1, 0x17

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    .line 155
    if-eqz v1, :cond_2

    .line 156
    invoke-static {v1}, Lkzu;->a([B)Lkzu;

    move-result-object v1

    .line 158
    invoke-virtual {p0}, Lfyo;->getContext()Landroid/content/Context;

    move-result-object v3

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lkzu;->g()I

    move-result v1

    if-lez v1, :cond_2

    if-nez v0, :cond_0

    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    const-string v4, "     "

    invoke-virtual {v0, v2, v4}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :goto_0
    if-ge v2, v1, :cond_1

    new-instance v4, Landroid/text/style/ImageSpan;

    sget-object v5, Lldg;->a:Llct;

    iget-object v5, v5, Llct;->aM:Landroid/graphics/Bitmap;

    invoke-direct {v4, v3, v5, v6}, Landroid/text/style/ImageSpan;-><init>(Landroid/content/Context;Landroid/graphics/Bitmap;I)V

    add-int/lit8 v5, v2, 0x1

    invoke-virtual {v0, v4, v2, v5, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    const-string v4, "      - "

    invoke-virtual {v0, v2, v4}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_0

    :cond_1
    :goto_1
    const/4 v2, 0x5

    if-ge v1, v2, :cond_2

    new-instance v2, Landroid/text/style/ImageSpan;

    sget-object v4, Lldg;->a:Llct;

    iget-object v4, v4, Llct;->aO:Landroid/graphics/Bitmap;

    invoke-direct {v2, v3, v4, v6}, Landroid/text/style/ImageSpan;-><init>(Landroid/content/Context;Landroid/graphics/Bitmap;I)V

    add-int/lit8 v4, v1, 0x1

    invoke-virtual {v0, v2, v1, v4, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 160
    :cond_2
    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 47
    invoke-super {p0}, Lgbz;->a()V

    .line 49
    iget-object v0, p0, Lfyo;->y:Lldg;

    invoke-virtual {v0}, Lldg;->a()V

    .line 50
    const/4 v0, 0x0

    iput v0, p0, Lfyo;->B:I

    .line 54
    return-void
.end method

.method protected a(Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 58
    const/16 v0, 0x17

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 59
    if-eqz v0, :cond_0

    .line 60
    invoke-static {v0}, Lkzu;->a([B)Lkzu;

    move-result-object v0

    iput-object v0, p0, Lfyo;->A:Lkzu;

    .line 63
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/StringBuilder;)V
    .locals 3

    .prologue
    .line 145
    iget-object v0, p0, Lfyo;->y:Lldg;

    if-eqz v0, :cond_0

    .line 146
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/CharSequence;

    const/4 v1, 0x0

    iget-object v2, p0, Lfyo;->y:Lldg;

    .line 147
    invoke-virtual {v2}, Lldg;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v2

    aput-object v2, v0, v1

    .line 146
    invoke-static {p1, v0}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 149
    :cond_0
    return-void
.end method

.method protected a(ZIIII)V
    .locals 6

    .prologue
    .line 95
    invoke-super/range {p0 .. p5}, Lgbz;->a(ZIIII)V

    .line 96
    iget-object v0, p0, Lfyo;->y:Lldg;

    invoke-virtual {v0}, Lldg;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-ne v0, p0, :cond_0

    .line 97
    iget-object v0, p0, Lfyo;->y:Lldg;

    iget-object v1, p0, Lfyo;->q:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget v2, p0, Lfyo;->B:I

    iget-object v3, p0, Lfyo;->q:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget-object v4, p0, Lfyo;->y:Lldg;

    .line 98
    invoke-virtual {v4}, Lldg;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v3, v4

    iget v4, p0, Lfyo;->B:I

    iget-object v5, p0, Lfyo;->y:Lldg;

    .line 99
    invoke-virtual {v5}, Lldg;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v4, v5

    .line 97
    invoke-virtual {v0, v1, v2, v3, v4}, Lldg;->layout(IIII)V

    .line 102
    :cond_0
    sget-object v0, Llap;->a:Lloz;

    .line 103
    return-void
.end method

.method protected aD_()Z
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x1

    return v0
.end method

.method protected a_(Landroid/database/Cursor;Llcr;I)V
    .locals 3

    .prologue
    .line 67
    iget-object v0, p0, Lfyo;->A:Lkzu;

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lfyo;->y:Lldg;

    invoke-virtual {p0, v0}, Lfyo;->removeView(Landroid/view/View;)V

    .line 69
    iget-object v0, p0, Lfyo;->y:Lldg;

    iget-object v1, p0, Lfyo;->A:Lkzu;

    iget v2, p0, Lfyo;->o:I

    .line 70
    invoke-virtual {p2, v2}, Llcr;->a(I)I

    move-result v2

    .line 69
    invoke-virtual {v0, v1, v2}, Lldg;->a(Lkzu;I)V

    .line 71
    iget-object v0, p0, Lfyo;->y:Lldg;

    invoke-virtual {p0, v0}, Lfyo;->addView(Landroid/view/View;)V

    .line 73
    :cond_0
    return-void
.end method

.method protected b(III)I
    .locals 1

    .prologue
    .line 112
    sget-object v0, Llap;->a:Lloz;

    .line 113
    invoke-super {p0, p1, p2, p3}, Lgbz;->b(III)I

    move-result v0

    return v0
.end method
