.class public final Lhmr;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Landroid/content/Context;

.field private b:I

.field private c:Lhmv;

.field private d:Lhmw;

.field private e:Lhmw;

.field private f:Ljava/lang/Long;

.field private g:Ljava/lang/Long;

.field private h:Landroid/os/Bundle;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    const/4 v0, -0x1

    iput v0, p0, Lhmr;->b:I

    .line 19
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lhmr;->h:Landroid/os/Bundle;

    .line 22
    iput-object p1, p0, Lhmr;->a:Landroid/content/Context;

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lhmr;-><init>(Landroid/content/Context;)V

    .line 28
    iput p2, p0, Lhmr;->b:I

    .line 29
    return-void
.end method


# virtual methods
.method public a(I)Lhmr;
    .locals 0

    .prologue
    .line 32
    iput p1, p0, Lhmr;->b:I

    .line 33
    return-object p0
.end method

.method public a(Landroid/os/Bundle;)Lhmr;
    .locals 1

    .prologue
    .line 52
    if-eqz p1, :cond_0

    .line 53
    iget-object v0, p0, Lhmr;->h:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 55
    :cond_0
    return-object p0
.end method

.method public a(Lhmv;)Lhmr;
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Lhmr;->c:Lhmv;

    .line 38
    return-object p0
.end method

.method public a(Lhmw;)Lhmr;
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lhmr;->d:Lhmw;

    .line 43
    return-object p0
.end method

.method public a(Ljava/lang/Long;)Lhmr;
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lhmr;->f:Ljava/lang/Long;

    .line 70
    return-object p0
.end method

.method public a(Ljava/lang/String;I)Lhmr;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lhmr;->h:Landroid/os/Bundle;

    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 65
    return-object p0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Lhmr;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lhmr;->h:Landroid/os/Bundle;

    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    return-object p0
.end method

.method public a()Lhmv;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lhmr;->c:Lhmv;

    return-object v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 97
    iget v0, p0, Lhmr;->b:I

    return v0
.end method

.method public b(Landroid/os/Bundle;)Lhmr;
    .locals 2

    .prologue
    .line 79
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/os/Bundle;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 80
    iget-object v0, p0, Lhmr;->h:Landroid/os/Bundle;

    const-string v1, "extra_start_view_extras"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 82
    :cond_0
    return-object p0
.end method

.method public b(Lhmw;)Lhmr;
    .locals 0

    .prologue
    .line 47
    iput-object p1, p0, Lhmr;->e:Lhmw;

    .line 48
    return-object p0
.end method

.method public b(Ljava/lang/Long;)Lhmr;
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lhmr;->g:Ljava/lang/Long;

    .line 75
    return-object p0
.end method

.method public c(Landroid/os/Bundle;)Lhmr;
    .locals 2

    .prologue
    .line 86
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/os/Bundle;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 87
    iget-object v0, p0, Lhmr;->h:Landroid/os/Bundle;

    const-string v1, "extra_end_view_extras"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 89
    :cond_0
    return-object p0
.end method

.method public c()Lhmw;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lhmr;->d:Lhmw;

    return-object v0
.end method

.method public d()Lhmw;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lhmr;->e:Lhmw;

    return-object v0
.end method

.method public e()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lhmr;->f:Ljava/lang/Long;

    return-object v0
.end method

.method public f()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lhmr;->g:Ljava/lang/Long;

    return-object v0
.end method

.method public g()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lhmr;->h:Landroid/os/Bundle;

    return-object v0
.end method

.method public h()Landroid/content/Context;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lhmr;->a:Landroid/content/Context;

    return-object v0
.end method
