.class public final Llzq;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnbb;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 24217
    invoke-direct {p0}, Loxq;-><init>()V

    .line 24220
    iput-object v0, p0, Llzq;->apiHeader:Llyq;

    .line 24223
    iput-object v0, p0, Llzq;->a:Lnbb;

    .line 24217
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 24240
    const/4 v0, 0x0

    .line 24241
    iget-object v1, p0, Llzq;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 24242
    const/4 v0, 0x1

    iget-object v1, p0, Llzq;->apiHeader:Llyq;

    .line 24243
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 24245
    :cond_0
    iget-object v1, p0, Llzq;->a:Lnbb;

    if-eqz v1, :cond_1

    .line 24246
    const/4 v1, 0x2

    iget-object v2, p0, Llzq;->a:Lnbb;

    .line 24247
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 24249
    :cond_1
    iget-object v1, p0, Llzq;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 24250
    iput v0, p0, Llzq;->ai:I

    .line 24251
    return v0
.end method

.method public a(Loxn;)Llzq;
    .locals 2

    .prologue
    .line 24259
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 24260
    sparse-switch v0, :sswitch_data_0

    .line 24264
    iget-object v1, p0, Llzq;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 24265
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llzq;->ah:Ljava/util/List;

    .line 24268
    :cond_1
    iget-object v1, p0, Llzq;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 24270
    :sswitch_0
    return-object p0

    .line 24275
    :sswitch_1
    iget-object v0, p0, Llzq;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 24276
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Llzq;->apiHeader:Llyq;

    .line 24278
    :cond_2
    iget-object v0, p0, Llzq;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 24282
    :sswitch_2
    iget-object v0, p0, Llzq;->a:Lnbb;

    if-nez v0, :cond_3

    .line 24283
    new-instance v0, Lnbb;

    invoke-direct {v0}, Lnbb;-><init>()V

    iput-object v0, p0, Llzq;->a:Lnbb;

    .line 24285
    :cond_3
    iget-object v0, p0, Llzq;->a:Lnbb;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 24260
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 24228
    iget-object v0, p0, Llzq;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 24229
    const/4 v0, 0x1

    iget-object v1, p0, Llzq;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 24231
    :cond_0
    iget-object v0, p0, Llzq;->a:Lnbb;

    if-eqz v0, :cond_1

    .line 24232
    const/4 v0, 0x2

    iget-object v1, p0, Llzq;->a:Lnbb;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 24234
    :cond_1
    iget-object v0, p0, Llzq;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 24236
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 24213
    invoke-virtual {p0, p1}, Llzq;->a(Loxn;)Llzq;

    move-result-object v0

    return-object v0
.end method
