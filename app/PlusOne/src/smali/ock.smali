.class public final Lock;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Locn;

.field public b:Locn;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 404
    invoke-direct {p0}, Loxq;-><init>()V

    .line 407
    sget-object v0, Locn;->a:[Locn;

    iput-object v0, p0, Lock;->a:[Locn;

    .line 410
    const/4 v0, 0x0

    iput-object v0, p0, Lock;->b:Locn;

    .line 404
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 431
    .line 432
    iget-object v1, p0, Lock;->a:[Locn;

    if-eqz v1, :cond_1

    .line 433
    iget-object v2, p0, Lock;->a:[Locn;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 434
    if-eqz v4, :cond_0

    .line 435
    const/4 v5, 0x1

    .line 436
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 433
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 440
    :cond_1
    iget-object v1, p0, Lock;->b:Locn;

    if-eqz v1, :cond_2

    .line 441
    const/4 v1, 0x2

    iget-object v2, p0, Lock;->b:Locn;

    .line 442
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 444
    :cond_2
    iget-object v1, p0, Lock;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 445
    iput v0, p0, Lock;->ai:I

    .line 446
    return v0
.end method

.method public a(Loxn;)Lock;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 454
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 455
    sparse-switch v0, :sswitch_data_0

    .line 459
    iget-object v2, p0, Lock;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 460
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lock;->ah:Ljava/util/List;

    .line 463
    :cond_1
    iget-object v2, p0, Lock;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 465
    :sswitch_0
    return-object p0

    .line 470
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 471
    iget-object v0, p0, Lock;->a:[Locn;

    if-nez v0, :cond_3

    move v0, v1

    .line 472
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Locn;

    .line 473
    iget-object v3, p0, Lock;->a:[Locn;

    if-eqz v3, :cond_2

    .line 474
    iget-object v3, p0, Lock;->a:[Locn;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 476
    :cond_2
    iput-object v2, p0, Lock;->a:[Locn;

    .line 477
    :goto_2
    iget-object v2, p0, Lock;->a:[Locn;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 478
    iget-object v2, p0, Lock;->a:[Locn;

    new-instance v3, Locn;

    invoke-direct {v3}, Locn;-><init>()V

    aput-object v3, v2, v0

    .line 479
    iget-object v2, p0, Lock;->a:[Locn;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 480
    invoke-virtual {p1}, Loxn;->a()I

    .line 477
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 471
    :cond_3
    iget-object v0, p0, Lock;->a:[Locn;

    array-length v0, v0

    goto :goto_1

    .line 483
    :cond_4
    iget-object v2, p0, Lock;->a:[Locn;

    new-instance v3, Locn;

    invoke-direct {v3}, Locn;-><init>()V

    aput-object v3, v2, v0

    .line 484
    iget-object v2, p0, Lock;->a:[Locn;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 488
    :sswitch_2
    iget-object v0, p0, Lock;->b:Locn;

    if-nez v0, :cond_5

    .line 489
    new-instance v0, Locn;

    invoke-direct {v0}, Locn;-><init>()V

    iput-object v0, p0, Lock;->b:Locn;

    .line 491
    :cond_5
    iget-object v0, p0, Lock;->b:Locn;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 455
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 415
    iget-object v0, p0, Lock;->a:[Locn;

    if-eqz v0, :cond_1

    .line 416
    iget-object v1, p0, Lock;->a:[Locn;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 417
    if-eqz v3, :cond_0

    .line 418
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 416
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 422
    :cond_1
    iget-object v0, p0, Lock;->b:Locn;

    if-eqz v0, :cond_2

    .line 423
    const/4 v0, 0x2

    iget-object v1, p0, Lock;->b:Locn;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 425
    :cond_2
    iget-object v0, p0, Lock;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 427
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 400
    invoke-virtual {p0, p1}, Lock;->a(Loxn;)Lock;

    move-result-object v0

    return-object v0
.end method
