.class public final Lmhe;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lngr;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 33451
    invoke-direct {p0}, Loxq;-><init>()V

    .line 33454
    iput-object v0, p0, Lmhe;->apiHeader:Llyq;

    .line 33457
    iput-object v0, p0, Lmhe;->a:Lngr;

    .line 33451
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 33474
    const/4 v0, 0x0

    .line 33475
    iget-object v1, p0, Lmhe;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 33476
    const/4 v0, 0x1

    iget-object v1, p0, Lmhe;->apiHeader:Llyq;

    .line 33477
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 33479
    :cond_0
    iget-object v1, p0, Lmhe;->a:Lngr;

    if-eqz v1, :cond_1

    .line 33480
    const/4 v1, 0x2

    iget-object v2, p0, Lmhe;->a:Lngr;

    .line 33481
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 33483
    :cond_1
    iget-object v1, p0, Lmhe;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 33484
    iput v0, p0, Lmhe;->ai:I

    .line 33485
    return v0
.end method

.method public a(Loxn;)Lmhe;
    .locals 2

    .prologue
    .line 33493
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 33494
    sparse-switch v0, :sswitch_data_0

    .line 33498
    iget-object v1, p0, Lmhe;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 33499
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmhe;->ah:Ljava/util/List;

    .line 33502
    :cond_1
    iget-object v1, p0, Lmhe;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 33504
    :sswitch_0
    return-object p0

    .line 33509
    :sswitch_1
    iget-object v0, p0, Lmhe;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 33510
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmhe;->apiHeader:Llyq;

    .line 33512
    :cond_2
    iget-object v0, p0, Lmhe;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 33516
    :sswitch_2
    iget-object v0, p0, Lmhe;->a:Lngr;

    if-nez v0, :cond_3

    .line 33517
    new-instance v0, Lngr;

    invoke-direct {v0}, Lngr;-><init>()V

    iput-object v0, p0, Lmhe;->a:Lngr;

    .line 33519
    :cond_3
    iget-object v0, p0, Lmhe;->a:Lngr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 33494
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 33462
    iget-object v0, p0, Lmhe;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 33463
    const/4 v0, 0x1

    iget-object v1, p0, Lmhe;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 33465
    :cond_0
    iget-object v0, p0, Lmhe;->a:Lngr;

    if-eqz v0, :cond_1

    .line 33466
    const/4 v0, 0x2

    iget-object v1, p0, Lmhe;->a:Lngr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 33468
    :cond_1
    iget-object v0, p0, Lmhe;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 33470
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 33447
    invoke-virtual {p0, p1}, Lmhe;->a(Loxn;)Lmhe;

    move-result-object v0

    return-object v0
.end method
