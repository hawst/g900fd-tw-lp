.class public final Lhwg;
.super Landroid/widget/Button;
.source "PG"

# interfaces
.implements Lhmm;
.implements Lhva;
.implements Lljh;


# instance fields
.field private a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private b:I

.field private c:Z

.field private d:Lhwp;

.field private e:Lhmk;

.field private f:Z

.field private g:Lhuz;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 68
    invoke-direct {p0, p1}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 83
    invoke-virtual {p0}, Lhwg;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lhwp;->a(Landroid/content/Context;)Lhwp;

    move-result-object v0

    iput-object v0, p0, Lhwg;->d:Lhwp;

    .line 86
    invoke-virtual {p0, v1}, Lhwg;->setMinimumHeight(I)V

    .line 87
    invoke-virtual {p0, v1}, Lhwg;->setMinimumWidth(I)V

    .line 88
    invoke-virtual {p0, v1}, Lhwg;->setMinHeight(I)V

    .line 89
    invoke-virtual {p0, v1}, Lhwg;->setMinWidth(I)V

    .line 91
    iput-boolean v2, p0, Lhwg;->f:Z

    .line 92
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v2}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lhwg;->a:Ljava/util/HashMap;

    .line 93
    iget-object v0, p0, Lhwg;->a:Ljava/util/HashMap;

    const-string v1, "ButtonOn"

    invoke-virtual {v0, v1, p0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    new-instance v0, Lhmk;

    sget-object v1, Lone;->a:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    iput-object v0, p0, Lhwg;->e:Lhmk;

    .line 69
    return-void
.end method

.method private c()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 155
    invoke-virtual {p0}, Lhwg;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a059c

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lhwg;->b:I

    .line 157
    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 155
    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 159
    invoke-virtual {p0, v0}, Lhwg;->setText(Ljava/lang/CharSequence;)V

    .line 160
    invoke-virtual {p0, v0}, Lhwg;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 161
    return-void
.end method

.method private d()V
    .locals 4

    .prologue
    .line 167
    iget-boolean v0, p0, Lhwg;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    .line 169
    :goto_0
    invoke-virtual {p0}, Lhwg;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lhwg;->d:Lhwp;

    iget v2, v2, Lhwp;->a:I

    const/4 v3, 0x0

    invoke-static {v1, p0, v0, v2, v3}, Lkcr;->a(Landroid/content/Context;Landroid/widget/Button;III)V

    .line 171
    return-void

    .line 167
    :cond_0
    const/16 v0, 0xb

    goto :goto_0
.end method

.method private e()V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 204
    invoke-static {}, Llsj;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 205
    invoke-static {p0}, Llii;->h(Landroid/view/View;)V

    .line 206
    invoke-virtual {p0, v1}, Lhwg;->setScaleX(F)V

    .line 207
    invoke-virtual {p0, v1}, Lhwg;->setScaleY(F)V

    .line 209
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 253
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhwg;->f:Z

    .line 254
    invoke-direct {p0}, Lhwg;->e()V

    .line 255
    return-void
.end method

.method public a(Lhus;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 114
    iput v1, p0, Lhwg;->b:I

    iput-boolean v1, p0, Lhwg;->c:Z

    invoke-virtual {p0, v2}, Lhwg;->setText(Ljava/lang/CharSequence;)V

    const-string v0, ""

    invoke-virtual {p0, v0}, Lhwg;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lhwg;->g:Lhuz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhwg;->g:Lhuz;

    invoke-virtual {v0, v2}, Lhuz;->a(Lhjy;)V

    :cond_0
    invoke-direct {p0}, Lhwg;->e()V

    .line 116
    if-eqz p1, :cond_4

    .line 117
    invoke-virtual {p1}, Lhus;->b()Z

    move-result v0

    iput-boolean v0, p0, Lhwg;->c:Z

    .line 118
    invoke-virtual {p1}, Lhus;->a()I

    move-result v0

    iput v0, p0, Lhwg;->b:I

    .line 120
    iget v0, p0, Lhwg;->b:I

    if-gez v0, :cond_1

    .line 121
    iput v1, p0, Lhwg;->b:I

    .line 124
    :cond_1
    invoke-direct {p0}, Lhwg;->c()V

    .line 125
    invoke-direct {p0}, Lhwg;->d()V

    .line 127
    invoke-virtual {p1}, Lhus;->c()Lhjy;

    move-result-object v0

    .line 128
    if-eqz v0, :cond_3

    .line 129
    iget-object v1, p0, Lhwg;->g:Lhuz;

    if-nez v1, :cond_2

    .line 130
    new-instance v1, Lhuz;

    invoke-direct {v1, p0}, Lhuz;-><init>(Landroid/view/View;)V

    iput-object v1, p0, Lhwg;->g:Lhuz;

    .line 131
    iget-object v1, p0, Lhwg;->g:Lhuz;

    invoke-virtual {v1, p0}, Lhuz;->a(Lhva;)V

    .line 134
    :cond_2
    iget-object v1, p0, Lhwg;->g:Lhuz;

    invoke-virtual {v1, v0}, Lhuz;->a(Lhjy;)V

    .line 140
    :cond_3
    :goto_0
    return-void

    .line 137
    :cond_4
    invoke-virtual {p0, v1}, Lhwg;->setHeight(I)V

    .line 138
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lhwg;->setVisibility(I)V

    goto :goto_0
.end method

.method public ac_()Lhmk;
    .locals 1

    .prologue
    .line 262
    iget-object v0, p0, Lhwg;->e:Lhmk;

    return-object v0
.end method

.method public b()V
    .locals 4

    .prologue
    .line 217
    iget-boolean v0, p0, Lhwg;->c:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lhwg;->c:Z

    .line 219
    iget-boolean v0, p0, Lhwg;->c:Z

    if-eqz v0, :cond_2

    .line 220
    iget v0, p0, Lhwg;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lhwg;->b:I

    .line 225
    :goto_1
    invoke-direct {p0}, Lhwg;->c()V

    .line 226
    invoke-direct {p0}, Lhwg;->d()V

    .line 228
    iget-boolean v0, p0, Lhwg;->c:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lhwg;->f:Z

    if-eqz v0, :cond_0

    .line 229
    invoke-virtual {p0}, Lhwg;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Llsj;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lhni;->a()Lhni;

    move-result-object v1

    const v2, 0x7f080025

    iget-object v3, p0, Lhwg;->a:Ljava/util/HashMap;

    invoke-virtual {v1, v0, v2, v3}, Lhni;->a(Landroid/content/Context;ILjava/util/Map;)Landroid/animation/Animator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 231
    :cond_0
    return-void

    .line 217
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 222
    :cond_2
    iget v0, p0, Lhwg;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lhwg;->b:I

    goto :goto_1
.end method
