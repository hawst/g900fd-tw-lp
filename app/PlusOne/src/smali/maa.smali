.class public final Lmaa;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnuy;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 6235
    invoke-direct {p0}, Loxq;-><init>()V

    .line 6238
    iput-object v0, p0, Lmaa;->apiHeader:Llyq;

    .line 6241
    iput-object v0, p0, Lmaa;->a:Lnuy;

    .line 6235
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 6258
    const/4 v0, 0x0

    .line 6259
    iget-object v1, p0, Lmaa;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 6260
    const/4 v0, 0x1

    iget-object v1, p0, Lmaa;->apiHeader:Llyq;

    .line 6261
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 6263
    :cond_0
    iget-object v1, p0, Lmaa;->a:Lnuy;

    if-eqz v1, :cond_1

    .line 6264
    const/4 v1, 0x2

    iget-object v2, p0, Lmaa;->a:Lnuy;

    .line 6265
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6267
    :cond_1
    iget-object v1, p0, Lmaa;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6268
    iput v0, p0, Lmaa;->ai:I

    .line 6269
    return v0
.end method

.method public a(Loxn;)Lmaa;
    .locals 2

    .prologue
    .line 6277
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 6278
    sparse-switch v0, :sswitch_data_0

    .line 6282
    iget-object v1, p0, Lmaa;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 6283
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmaa;->ah:Ljava/util/List;

    .line 6286
    :cond_1
    iget-object v1, p0, Lmaa;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6288
    :sswitch_0
    return-object p0

    .line 6293
    :sswitch_1
    iget-object v0, p0, Lmaa;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 6294
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmaa;->apiHeader:Llyq;

    .line 6296
    :cond_2
    iget-object v0, p0, Lmaa;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 6300
    :sswitch_2
    iget-object v0, p0, Lmaa;->a:Lnuy;

    if-nez v0, :cond_3

    .line 6301
    new-instance v0, Lnuy;

    invoke-direct {v0}, Lnuy;-><init>()V

    iput-object v0, p0, Lmaa;->a:Lnuy;

    .line 6303
    :cond_3
    iget-object v0, p0, Lmaa;->a:Lnuy;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 6278
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 6246
    iget-object v0, p0, Lmaa;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 6247
    const/4 v0, 0x1

    iget-object v1, p0, Lmaa;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 6249
    :cond_0
    iget-object v0, p0, Lmaa;->a:Lnuy;

    if-eqz v0, :cond_1

    .line 6250
    const/4 v0, 0x2

    iget-object v1, p0, Lmaa;->a:Lnuy;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 6252
    :cond_1
    iget-object v0, p0, Lmaa;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 6254
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6231
    invoke-virtual {p0, p1}, Lmaa;->a(Loxn;)Lmaa;

    move-result-object v0

    return-object v0
.end method
