.class public final Lmrm;
.super Loxq;
.source "PG"


# instance fields
.field public a:I

.field public b:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 9
    const v0, 0x1e240

    new-instance v1, Lmrn;

    invoke-direct {v1}, Lmrn;-><init>()V

    .line 14
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    .line 13
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/high16 v0, -0x80000000

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 17
    iput v0, p0, Lmrm;->a:I

    .line 20
    iput v0, p0, Lmrm;->b:I

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 37
    const/4 v0, 0x0

    .line 38
    iget v1, p0, Lmrm;->a:I

    if-eq v1, v2, :cond_0

    .line 39
    const/4 v0, 0x1

    iget v1, p0, Lmrm;->a:I

    .line 40
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 42
    :cond_0
    iget v1, p0, Lmrm;->b:I

    if-eq v1, v2, :cond_1

    .line 43
    const/4 v1, 0x2

    iget v2, p0, Lmrm;->b:I

    .line 44
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 46
    :cond_1
    iget-object v1, p0, Lmrm;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 47
    iput v0, p0, Lmrm;->ai:I

    .line 48
    return v0
.end method

.method public a(Loxn;)Lmrm;
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 56
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 57
    sparse-switch v0, :sswitch_data_0

    .line 61
    iget-object v1, p0, Lmrm;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 62
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmrm;->ah:Ljava/util/List;

    .line 65
    :cond_1
    iget-object v1, p0, Lmrm;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 67
    :sswitch_0
    return-object p0

    .line 72
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 73
    if-eqz v0, :cond_2

    if-eq v0, v2, :cond_2

    if-ne v0, v3, :cond_3

    .line 76
    :cond_2
    iput v0, p0, Lmrm;->a:I

    goto :goto_0

    .line 78
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lmrm;->a:I

    goto :goto_0

    .line 83
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 84
    if-eq v0, v2, :cond_4

    if-eq v0, v3, :cond_4

    const/4 v1, 0x3

    if-eq v0, v1, :cond_4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_4

    const/4 v1, 0x5

    if-eq v0, v1, :cond_4

    const/4 v1, 0x6

    if-eq v0, v1, :cond_4

    const/4 v1, 0x7

    if-eq v0, v1, :cond_4

    const/16 v1, 0x8

    if-eq v0, v1, :cond_4

    const/16 v1, 0x9

    if-eq v0, v1, :cond_4

    const/16 v1, 0xa

    if-eq v0, v1, :cond_4

    const/16 v1, 0xb

    if-eq v0, v1, :cond_4

    const/16 v1, 0xc

    if-eq v0, v1, :cond_4

    const/16 v1, 0xd

    if-eq v0, v1, :cond_4

    const/16 v1, 0xe

    if-eq v0, v1, :cond_4

    const/16 v1, 0xf

    if-eq v0, v1, :cond_4

    const/16 v1, 0x10

    if-eq v0, v1, :cond_4

    const/16 v1, 0x11

    if-eq v0, v1, :cond_4

    const/16 v1, 0x12

    if-eq v0, v1, :cond_4

    const/16 v1, 0x13

    if-eq v0, v1, :cond_4

    const/16 v1, 0x14

    if-eq v0, v1, :cond_4

    const/16 v1, 0x16

    if-eq v0, v1, :cond_4

    const/16 v1, 0x17

    if-eq v0, v1, :cond_4

    const/16 v1, 0x18

    if-eq v0, v1, :cond_4

    const/16 v1, 0x19

    if-eq v0, v1, :cond_4

    const/16 v1, 0x1a

    if-eq v0, v1, :cond_4

    const/16 v1, 0x1b

    if-eq v0, v1, :cond_4

    const/16 v1, 0x1c

    if-eq v0, v1, :cond_4

    const/16 v1, 0x1d

    if-eq v0, v1, :cond_4

    const/16 v1, 0x1f

    if-eq v0, v1, :cond_4

    const/16 v1, 0x20

    if-eq v0, v1, :cond_4

    const/16 v1, 0x21

    if-eq v0, v1, :cond_4

    const/16 v1, 0x22

    if-eq v0, v1, :cond_4

    const/16 v1, 0x23

    if-eq v0, v1, :cond_4

    const/16 v1, 0x24

    if-eq v0, v1, :cond_4

    const/16 v1, 0x25

    if-eq v0, v1, :cond_4

    const/16 v1, 0x26

    if-eq v0, v1, :cond_4

    const/16 v1, 0x27

    if-eq v0, v1, :cond_4

    const/16 v1, 0x28

    if-eq v0, v1, :cond_4

    const/16 v1, 0x1e

    if-eq v0, v1, :cond_4

    const/16 v1, 0x15

    if-ne v0, v1, :cond_5

    .line 124
    :cond_4
    iput v0, p0, Lmrm;->b:I

    goto/16 :goto_0

    .line 126
    :cond_5
    iput v2, p0, Lmrm;->b:I

    goto/16 :goto_0

    .line 57
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 25
    iget v0, p0, Lmrm;->a:I

    if-eq v0, v2, :cond_0

    .line 26
    const/4 v0, 0x1

    iget v1, p0, Lmrm;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 28
    :cond_0
    iget v0, p0, Lmrm;->b:I

    if-eq v0, v2, :cond_1

    .line 29
    const/4 v0, 0x2

    iget v1, p0, Lmrm;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 31
    :cond_1
    iget-object v0, p0, Lmrm;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 33
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lmrm;->a(Loxn;)Lmrm;

    move-result-object v0

    return-object v0
.end method
