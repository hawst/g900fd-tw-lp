.class public final Litz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lijm;
.implements Lijn;
.implements Liun;


# instance fields
.field final a:Landroid/content/Context;

.field final b:Landroid/accounts/Account;

.field c:Limf;

.field d:Landroid/content/Intent;

.field private final e:Landroid/content/BroadcastReceiver;

.field private final f:Lae;

.field private final g:Z

.field private h:Lijk;

.field private i:Lily;

.field private j:Liue;

.field private k:I

.field private l:Z

.field private m:Lium;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lae;Liue;)V
    .locals 6

    .prologue
    .line 116
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Litz;-><init>(Landroid/content/Context;Ljava/lang/String;Lae;Liue;Z)V

    .line 117
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lae;Liue;Z)V
    .locals 2

    .prologue
    .line 130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    new-instance v0, Liua;

    invoke-direct {v0, p0}, Liua;-><init>(Litz;)V

    iput-object v0, p0, Litz;->e:Landroid/content/BroadcastReceiver;

    .line 101
    const/4 v0, 0x0

    iput v0, p0, Litz;->k:I

    .line 131
    iput-object p1, p0, Litz;->a:Landroid/content/Context;

    .line 132
    new-instance v0, Landroid/accounts/Account;

    const-string v1, "com.google"

    invoke-direct {v0, p2, v1}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Litz;->b:Landroid/accounts/Account;

    .line 133
    iput-object p3, p0, Litz;->f:Lae;

    .line 134
    iput-object p4, p0, Litz;->j:Liue;

    .line 135
    iput-boolean p5, p0, Litz;->g:Z

    .line 136
    const-class v0, Lily;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lily;

    iput-object v0, p0, Litz;->i:Lily;

    .line 137
    const-class v0, Lijl;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lijl;

    const-class v1, Lilz;

    .line 138
    invoke-static {p1, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lijj;

    invoke-interface {v0, v1}, Lijl;->a(Lijj;)Lijl;

    move-result-object v0

    .line 139
    invoke-interface {v0, p0}, Lijl;->a(Lijm;)Lijl;

    move-result-object v0

    .line 140
    invoke-interface {v0, p0}, Lijl;->a(Lijn;)Lijl;

    move-result-object v0

    .line 141
    invoke-interface {v0}, Lijl;->a()Lijk;

    move-result-object v0

    iput-object v0, p0, Litz;->h:Lijk;

    .line 142
    iget-object v0, p0, Litz;->h:Lijk;

    invoke-interface {v0}, Lijk;->b()V

    .line 144
    const-string v0, "enable_location_reporting_auto"

    invoke-direct {p0, v0}, Litz;->b(Ljava/lang/String;)V

    const-string v0, "enable_location_reporting_manual "

    invoke-direct {p0, v0}, Litz;->b(Ljava/lang/String;)V

    const-string v0, "enable_location_reporting_manual_multi_account"

    invoke-direct {p0, v0}, Litz;->b(Ljava/lang/String;)V

    const-string v0, "enable_location_reporting_error"

    invoke-direct {p0, v0}, Litz;->b(Ljava/lang/String;)V

    .line 145
    return-void
.end method

.method static a(Landroid/content/Context;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 530
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 531
    const-string v2, "com.google"

    invoke-virtual {v1, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    array-length v1, v1

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 410
    iget-object v0, p0, Litz;->f:Lae;

    .line 411
    invoke-virtual {v0, p1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Liuj;

    .line 412
    if-eqz v0, :cond_0

    .line 413
    invoke-virtual {v0, p0}, Liuj;->a(Liun;)V

    .line 415
    :cond_0
    return-void
.end method

.method static b(Landroid/content/Context;)Z
    .locals 3

    .prologue
    .line 559
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 560
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget-object v2, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 561
    const-class v0, Lija;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lija;

    .line 562
    invoke-interface {v0, v1, v2}, Lija;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private c(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 425
    iget-object v0, p0, Litz;->f:Lae;

    invoke-virtual {v0, p1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 441
    invoke-virtual {p0, v3}, Litz;->a(Z)V

    .line 444
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 445
    const-string v1, "com.google.android.gms.location.reporting.SETTINGS_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 446
    iget-boolean v1, p0, Litz;->l:Z

    if-nez v1, :cond_0

    .line 447
    iget-object v1, p0, Litz;->a:Landroid/content/Context;

    iget-object v2, p0, Litz;->e:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 448
    iput-boolean v3, p0, Litz;->l:Z

    .line 450
    :cond_0
    return-void
.end method

.method public a(Liiw;)V
    .locals 4

    .prologue
    .line 464
    invoke-interface {p1}, Liiw;->b()I

    move-result v0

    iput v0, p0, Litz;->k:I

    .line 465
    const-string v0, "GmsLocationReporting"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 466
    const-string v0, "GmsLocationReporting"

    iget v1, p0, Litz;->k:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x29

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "onConnectionFailed: errorCode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 468
    :cond_0
    invoke-virtual {p0}, Litz;->n()V

    .line 469
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 430
    const-string v0, "enable_location_reporting_auto"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 431
    invoke-virtual {p0}, Litz;->j()V

    .line 437
    :cond_0
    :goto_0
    return-void

    .line 432
    :cond_1
    const-string v0, "enable_location_reporting_manual "

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "enable_location_reporting_manual_multi_account"

    .line 433
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "enable_location_reporting_error"

    .line 434
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 435
    :cond_2
    invoke-virtual {p0}, Litz;->i()V

    goto :goto_0
.end method

.method a(Z)V
    .locals 3

    .prologue
    .line 472
    invoke-virtual {p0}, Litz;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 499
    :goto_0
    return-void

    .line 476
    :cond_0
    iget-object v0, p0, Litz;->i:Lily;

    iget-object v1, p0, Litz;->h:Lijk;

    iget-object v2, p0, Litz;->b:Landroid/accounts/Account;

    invoke-interface {v0, v1, v2}, Lily;->a(Lijk;Landroid/accounts/Account;)Lijq;

    move-result-object v0

    new-instance v1, Liud;

    invoke-direct {v1, p0, p1}, Liud;-><init>(Litz;Z)V

    invoke-interface {v0, v1}, Lijq;->a(Lijt;)V

    goto :goto_0
.end method

.method public a(Lium;)Z
    .locals 8

    .prologue
    const v7, 0x7f0a03d9

    const/4 v6, 0x2

    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 165
    iput-object p1, p0, Litz;->m:Lium;

    .line 166
    invoke-virtual {p0}, Litz;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Litz;->c:Limf;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Litz;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Litz;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Litz;->a:Landroid/content/Context;

    const-string v4, "ulr_googlelocation"

    const-string v5, "https://www.google.com/support/mobile/?hl=%locale%"

    invoke-static {v2, v4, v5}, Litk;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Litz;->b:Landroid/accounts/Account;

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {p0}, Litz;->g()Z

    move-result v5

    if-eqz v5, :cond_2

    const v5, 0x7f0a03e0

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v4, v6, v0

    aput-object v2, v6, v3

    invoke-virtual {v1, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v1, "enable_location_reporting_auto"

    iget-object v0, p0, Litz;->a:Landroid/content/Context;

    const v4, 0x7f0a03da

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    iget-object v4, p0, Litz;->a:Landroid/content/Context;

    const v5, 0x7f0a03db

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Litz;->m:Lium;

    invoke-interface {v5, v4}, Lium;->a(Ljava/lang/String;)Liuj;

    move-result-object v4

    invoke-virtual {v4, v2, v0}, Liuj;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, p0}, Liuj;->a(Liun;)V

    iget-object v0, p0, Litz;->f:Lae;

    invoke-virtual {v4, v0, v1}, Liuj;->a(Lae;Ljava/lang/String;)V

    move v0, v3

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Litz;->h()Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Litz;->a:Landroid/content/Context;

    invoke-static {v5}, Litz;->a(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_3

    const v5, 0x7f0a03df

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v4, v6, v0

    aput-object v2, v6, v3

    invoke-virtual {v1, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v1, "enable_location_reporting_manual_multi_account"

    iget-object v0, p0, Litz;->a:Landroid/content/Context;

    invoke-virtual {v0, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    const v4, 0x7f0a03de

    new-array v5, v3, [Ljava/lang/Object;

    aput-object v2, v5, v0

    invoke-virtual {v1, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v1, "enable_location_reporting_manual "

    iget-object v0, p0, Litz;->a:Landroid/content/Context;

    invoke-virtual {v0, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public b()V
    .locals 1

    .prologue
    .line 458
    const/4 v0, 0x0

    iput-object v0, p0, Litz;->c:Limf;

    .line 459
    invoke-virtual {p0}, Litz;->n()V

    .line 460
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 151
    iget-boolean v0, p0, Litz;->l:Z

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Litz;->a:Landroid/content/Context;

    iget-object v1, p0, Litz;->e:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 153
    const/4 v0, 0x0

    iput-boolean v0, p0, Litz;->l:Z

    .line 155
    :cond_0
    iget-object v0, p0, Litz;->h:Lijk;

    invoke-interface {v0}, Lijk;->c()V

    .line 156
    return-void
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Litz;->h:Lijk;

    invoke-interface {v0}, Lijk;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Litz;->k:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 240
    iget v0, p0, Litz;->k:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 248
    invoke-virtual {p0}, Litz;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Litz;->c:Limf;

    if-nez v0, :cond_1

    .line 260
    :cond_0
    :goto_0
    return v2

    .line 252
    :cond_1
    iget-object v0, p0, Litz;->c:Limf;

    .line 253
    invoke-interface {v0}, Limf;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Litz;->c:Limf;

    invoke-interface {v0}, Limf;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 256
    :goto_1
    iget-boolean v3, p0, Litz;->g:Z

    if-eqz v3, :cond_3

    .line 257
    if-eqz v0, :cond_0

    iget-object v0, p0, Litz;->c:Limf;

    invoke-interface {v0}, Limf;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    move v2, v1

    goto :goto_0

    :cond_2
    move v0, v2

    .line 253
    goto :goto_1

    :cond_3
    move v2, v0

    .line 260
    goto :goto_0
.end method

.method public g()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 267
    invoke-virtual {p0}, Litz;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Litz;->c:Limf;

    if-nez v1, :cond_1

    .line 271
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Litz;->c:Limf;

    invoke-interface {v1}, Limf;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Litz;->f()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public h()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 279
    invoke-virtual {p0}, Litz;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Litz;->c:Limf;

    if-nez v1, :cond_1

    .line 283
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Litz;->c:Limf;

    invoke-interface {v1}, Limf;->b()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Litz;->d:Landroid/content/Intent;

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public i()V
    .locals 2

    .prologue
    .line 304
    invoke-virtual {p0}, Litz;->h()Z

    move-result v0

    if-nez v0, :cond_1

    .line 313
    :cond_0
    :goto_0
    return-void

    .line 308
    :cond_1
    iget-object v0, p0, Litz;->c:Limf;

    invoke-interface {v0}, Limf;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 309
    iget-object v0, p0, Litz;->a:Landroid/content/Context;

    const-class v1, Limn;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Limn;

    iget-object v1, p0, Litz;->a:Landroid/content/Context;

    invoke-interface {v0, v1}, Limn;->a(Landroid/content/Context;)V

    goto :goto_0

    .line 310
    :cond_2
    iget-object v0, p0, Litz;->d:Landroid/content/Intent;

    if-eqz v0, :cond_0

    .line 311
    iget-object v0, p0, Litz;->a:Landroid/content/Context;

    iget-object v1, p0, Litz;->d:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public j()V
    .locals 3

    .prologue
    .line 321
    invoke-virtual {p0}, Litz;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 322
    invoke-virtual {p0}, Litz;->l()V

    .line 339
    :goto_0
    return-void

    .line 326
    :cond_0
    iget-object v0, p0, Litz;->i:Lily;

    iget-object v1, p0, Litz;->h:Lijk;

    iget-object v2, p0, Litz;->b:Landroid/accounts/Account;

    invoke-interface {v0, v1, v2}, Lily;->b(Lijk;Landroid/accounts/Account;)Lijq;

    move-result-object v0

    new-instance v1, Liub;

    invoke-direct {v1, p0}, Liub;-><init>(Litz;)V

    invoke-interface {v0, v1}, Lijq;->a(Lijt;)V

    goto :goto_0
.end method

.method public k()V
    .locals 7

    .prologue
    .line 345
    invoke-virtual {p0}, Litz;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Litz;->c:Limf;

    if-nez v0, :cond_1

    .line 373
    :cond_0
    :goto_0
    return-void

    .line 352
    :cond_1
    invoke-virtual {p0}, Litz;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 361
    iget-object v0, p0, Litz;->a:Landroid/content/Context;

    const-class v1, Limk;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Limk;

    iget-object v2, p0, Litz;->b:Landroid/accounts/Account;

    const-string v3, "one-shot update for location tab"

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    .line 362
    invoke-interface/range {v1 .. v6}, Limk;->a(Landroid/accounts/Account;Ljava/lang/String;JI)Limj;

    move-result-object v0

    .line 364
    iget-object v1, p0, Litz;->i:Lily;

    iget-object v2, p0, Litz;->h:Lijk;

    invoke-interface {v1, v2, v0}, Lily;->a(Lijk;Limj;)Lijq;

    move-result-object v0

    new-instance v1, Liuc;

    invoke-direct {v1}, Liuc;-><init>()V

    invoke-interface {v0, v1}, Lijq;->a(Lijt;)V

    goto :goto_0
.end method

.method l()V
    .locals 6

    .prologue
    .line 381
    iget-object v0, p0, Litz;->a:Landroid/content/Context;

    const v1, 0x7f0a03dc

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 386
    iget-object v1, p0, Litz;->m:Lium;

    if-nez v1, :cond_0

    .line 387
    invoke-static {v0}, Litu;->a(Ljava/lang/String;)Litu;

    move-result-object v0

    .line 392
    :goto_0
    iget-object v1, p0, Litz;->a:Landroid/content/Context;

    const v2, 0x7f0a03dd

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Litz;->b:Landroid/accounts/Account;

    iget-object v5, v5, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v5, v3, v4

    .line 393
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Litz;->a:Landroid/content/Context;

    const v3, 0x7f0a03d9

    .line 394
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 392
    invoke-virtual {v0, v1, v2}, Liuj;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 395
    invoke-virtual {v0, p0}, Liuj;->a(Liun;)V

    .line 396
    iget-object v1, p0, Litz;->f:Lae;

    const-string v2, "enable_location_reporting_error"

    invoke-virtual {v0, v1, v2}, Liuj;->a(Lae;Ljava/lang/String;)V

    .line 397
    return-void

    .line 389
    :cond_0
    iget-object v1, p0, Litz;->m:Lium;

    invoke-interface {v1, v0}, Lium;->a(Ljava/lang/String;)Liuj;

    move-result-object v0

    goto :goto_0
.end method

.method public m()Z
    .locals 1

    .prologue
    .line 418
    const-string v0, "enable_location_reporting_auto"

    invoke-direct {p0, v0}, Litz;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "enable_location_reporting_error"

    .line 419
    invoke-direct {p0, v0}, Litz;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "enable_location_reporting_manual "

    .line 420
    invoke-direct {p0, v0}, Litz;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "enable_location_reporting_manual_multi_account"

    .line 421
    invoke-direct {p0, v0}, Litz;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method n()V
    .locals 1

    .prologue
    .line 502
    iget-object v0, p0, Litz;->j:Liue;

    if-eqz v0, :cond_0

    .line 503
    iget-object v0, p0, Litz;->j:Liue;

    invoke-interface {v0}, Liue;->a()V

    .line 505
    :cond_0
    return-void
.end method
