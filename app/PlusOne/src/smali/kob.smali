.class public final Lkob;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Lkoc;

.field private final b:J

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lkoc;J)V
    .locals 4

    .prologue
    .line 152
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 153
    const-wide/16 v0, -0x1

    cmp-long v0, p2, v0

    if-nez v0, :cond_0

    .line 154
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x43

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "LongId cannot equal INVALID_PHOTO_ID.  LongId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 157
    :cond_0
    iput-object p1, p0, Lkob;->a:Lkoc;

    .line 158
    iput-wide p2, p0, Lkob;->b:J

    .line 159
    const/4 v0, 0x0

    iput-object v0, p0, Lkob;->c:Ljava/lang/String;

    .line 160
    return-void
.end method

.method public constructor <init>(Lkoc;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 165
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 166
    if-nez p2, :cond_0

    .line 167
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "StringId cannot be null."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 169
    :cond_0
    iput-object p1, p0, Lkob;->a:Lkoc;

    .line 170
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lkob;->b:J

    .line 171
    iput-object p2, p0, Lkob;->c:Ljava/lang/String;

    .line 172
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 178
    iget-object v0, p0, Lkob;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 179
    iget-wide v0, p0, Lkob;->b:J

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    .line 181
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lkob;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method public b()Lqbg;
    .locals 4

    .prologue
    .line 189
    new-instance v0, Lqbg;

    invoke-direct {v0}, Lqbg;-><init>()V

    .line 190
    sget-object v1, Lkoa;->a:[I

    iget-object v2, p0, Lkob;->a:Lkoc;

    invoke-virtual {v2}, Lkoc;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 208
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Unknown ShareIdType: "

    iget-object v0, p0, Lkob;->a:Lkoc;

    invoke-virtual {v0}, Lkoc;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 192
    :pswitch_0
    new-instance v1, Lqbf;

    invoke-direct {v1}, Lqbf;-><init>()V

    iput-object v1, v0, Lqbg;->c:Lqbf;

    .line 193
    iget-object v1, v0, Lqbg;->c:Lqbf;

    iget-wide v2, p0, Lkob;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lqbf;->a:Ljava/lang/Long;

    .line 210
    :goto_1
    return-object v0

    .line 196
    :pswitch_1
    new-instance v1, Lqbf;

    invoke-direct {v1}, Lqbf;-><init>()V

    iput-object v1, v0, Lqbg;->b:Lqbf;

    .line 197
    iget-object v1, v0, Lqbg;->b:Lqbf;

    iget-object v2, p0, Lkob;->c:Ljava/lang/String;

    iput-object v2, v1, Lqbf;->b:Ljava/lang/String;

    goto :goto_1

    .line 200
    :pswitch_2
    new-instance v1, Lqbf;

    invoke-direct {v1}, Lqbf;-><init>()V

    iput-object v1, v0, Lqbg;->b:Lqbf;

    .line 201
    iget-object v1, v0, Lqbg;->b:Lqbf;

    iget-object v2, p0, Lkob;->c:Ljava/lang/String;

    iput-object v2, v1, Lqbf;->d:Ljava/lang/String;

    goto :goto_1

    .line 204
    :pswitch_3
    new-instance v1, Lqbf;

    invoke-direct {v1}, Lqbf;-><init>()V

    iput-object v1, v0, Lqbg;->d:Lqbf;

    .line 205
    iget-object v1, v0, Lqbg;->d:Lqbf;

    iget-object v2, p0, Lkob;->c:Ljava/lang/String;

    iput-object v2, v1, Lqbf;->c:Ljava/lang/String;

    goto :goto_1

    .line 208
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 190
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 215
    const-string v0, "{id: %s, type: %s}"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lkob;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lkob;->a:Lkoc;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
