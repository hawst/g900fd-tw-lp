.class public final Ljuu;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Liht;


# instance fields
.field a:Lnzb;

.field b:Z

.field private c:Liii;

.field private d:I

.field private e:I

.field private f:Ljuw;

.field private g:Lihr;


# direct methods
.method public constructor <init>(Lihr;Liii;)V
    .locals 2

    .prologue
    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    iput-object p1, p0, Ljuu;->g:Lihr;

    .line 98
    iput-object p2, p0, Ljuu;->c:Liii;

    .line 99
    iget-object v0, p0, Ljuu;->c:Liii;

    new-instance v1, Ljuv;

    invoke-direct {v1, p0}, Ljuv;-><init>(Ljuu;)V

    invoke-interface {v0, v1}, Liii;->a(Liij;)V

    .line 108
    const-string v0, "urn:x-cast:com.google.cast.plusphotos"

    invoke-interface {p1, v0, p0}, Lihr;->a(Ljava/lang/String;Liht;)V

    .line 109
    return-void
.end method

.method private static a([Lnzc;I)Ljava/lang/String;
    .locals 4

    .prologue
    .line 353
    array-length v1, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    aget-object v2, p0, v0

    .line 354
    iget-object v3, v2, Lnzc;->b:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-ne v3, p1, :cond_0

    .line 355
    iget-object v0, v2, Lnzc;->d:Ljava/lang/String;

    .line 359
    :goto_1
    return-object v0

    .line 353
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 359
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static a(Ljava/util/List;)Lorg/json/JSONObject;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lorg/json/JSONObject;"
        }
    .end annotation

    .prologue
    .line 327
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 329
    :try_start_0
    const-string v0, "streams"

    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2, p0}, Lorg/json/JSONArray;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 333
    :goto_0
    return-object v1

    .line 330
    :catch_0
    move-exception v0

    .line 331
    const-string v2, "RemoteMediaController"

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x24

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Error encoding customData for urls: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method


# virtual methods
.method a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 2

    .prologue
    .line 313
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 315
    :try_start_0
    const-string v1, "location"

    invoke-virtual {v0, v1, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 316
    const-string v1, "type"

    invoke-virtual {v0, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 317
    const-string v1, "url"

    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 319
    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Ljuu;->c:Liii;

    invoke-interface {v0}, Liii;->d()V

    .line 202
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Ljuu;->a(Z)V

    .line 203
    return-void
.end method

.method public a(D)V
    .locals 5

    .prologue
    .line 235
    iget-object v0, p0, Ljuu;->c:Liii;

    const-wide v2, 0x408f400000000000L    # 1000.0

    mul-double/2addr v2, p1

    double-to-long v2, v2

    invoke-interface {v0, v2, v3}, Liii;->a(J)V

    .line 236
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Ljuu;->a(Z)V

    .line 237
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 113
    const-string v0, "urn:x-cast:com.google.cast.plusphotos"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 115
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 118
    const-string v1, "newSession"

    const-string v2, "name"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 119
    const-string v1, "payload"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 120
    const-string v1, "windowWidth"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Ljuu;->d:I

    .line 121
    const-string v1, "windowHeight"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Ljuu;->e:I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 127
    :cond_0
    :goto_0
    return-void

    .line 123
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 124
    const-string v2, "RemoteMediaController"

    const-string v3, "Error decoding message from receiver: "

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v2, v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method a(Ljava/lang/String;Lorg/json/JSONObject;)V
    .locals 3

    .prologue
    .line 297
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 299
    :try_start_0
    const-string v1, "name"

    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 300
    const-string v1, "version"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 301
    if-eqz p2, :cond_0

    .line 302
    const-string v1, "payload"

    invoke-virtual {v0, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 304
    :cond_0
    iget-object v1, p0, Ljuu;->g:Lihr;

    const-string v2, "urn:x-cast:com.google.cast.plusphotos"

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Lihr;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 309
    :goto_0
    return-void

    .line 306
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public a(Ljuw;)V
    .locals 0

    .prologue
    .line 284
    iput-object p1, p0, Ljuu;->f:Ljuw;

    .line 285
    return-void
.end method

.method public a(Lnzb;)V
    .locals 6

    .prologue
    const/4 v5, 0x3

    .line 191
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-array v2, v5, [I

    fill-array-data v2, :array_0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v5, :cond_1

    aget v3, v2, v0

    iget-object v4, p1, Lnzb;->c:[Lnzc;

    invoke-static {v4, v3}, Ljuu;->a([Lnzc;I)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 192
    :cond_1
    iget-object v0, p0, Ljuu;->c:Liii;

    const-string v2, "video/mp4"

    invoke-static {v1}, Ljuu;->a(Ljava/util/List;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Liii;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 193
    iput-object p1, p0, Ljuu;->a:Lnzb;

    .line 194
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Ljuu;->a(Z)V

    .line 195
    return-void

    .line 191
    :array_0
    .array-data 4
        0x25
        0x16
        0x12
    .end array-data
.end method

.method a(Z)V
    .locals 1

    .prologue
    .line 288
    iget-boolean v0, p0, Ljuu;->b:Z

    if-eq p1, v0, :cond_1

    const/4 v0, 0x1

    .line 289
    :goto_0
    iput-boolean p1, p0, Ljuu;->b:Z

    .line 290
    if-eqz v0, :cond_0

    iget-object v0, p0, Ljuu;->f:Ljuw;

    if-eqz v0, :cond_0

    .line 291
    iget-object v0, p0, Ljuu;->f:Ljuw;

    invoke-interface {v0}, Ljuw;->j()V

    .line 293
    :cond_0
    return-void

    .line 288
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Ljuu;->c:Liii;

    invoke-interface {v0}, Liii;->e()V

    .line 207
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ljuu;->a(Z)V

    .line 208
    return-void
.end method

.method public b(Lnzb;)Z
    .locals 2

    .prologue
    .line 268
    iget-object v0, p0, Ljuu;->a:Lnzb;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lnzb;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lnzb;->a:Ljava/lang/String;

    iget-object v1, p0, Ljuu;->a:Lnzb;

    iget-object v1, v1, Lnzb;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()V
    .locals 6

    .prologue
    .line 214
    iget-object v0, p0, Ljuu;->g:Lihr;

    invoke-interface {v0}, Lihr;->d()D

    move-result-wide v0

    .line 215
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    cmpg-double v2, v0, v2

    if-gez v2, :cond_0

    .line 216
    iget-object v2, p0, Ljuu;->g:Lihr;

    const-wide v4, 0x3fb999999999999aL    # 0.1

    add-double/2addr v0, v4

    invoke-interface {v2, v0, v1}, Lihr;->a(D)V

    .line 218
    :cond_0
    return-void
.end method

.method public d()V
    .locals 6

    .prologue
    .line 224
    iget-object v0, p0, Ljuu;->g:Lihr;

    invoke-interface {v0}, Lihr;->d()D

    move-result-wide v0

    .line 225
    const-wide/16 v2, 0x0

    cmpl-double v2, v0, v2

    if-lez v2, :cond_0

    .line 226
    iget-object v2, p0, Ljuu;->g:Lihr;

    const-wide v4, 0x3fb999999999999aL    # 0.1

    sub-double/2addr v0, v4

    invoke-interface {v2, v0, v1}, Lihr;->a(D)V

    .line 228
    :cond_0
    return-void
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Ljuu;->c:Liii;

    invoke-interface {v0}, Liii;->a()Z

    move-result v0

    return v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 247
    iget v0, p0, Ljuu;->e:I

    if-eqz v0, :cond_0

    iget v0, p0, Ljuu;->d:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()I
    .locals 1

    .prologue
    .line 254
    iget v0, p0, Ljuu;->e:I

    return v0
.end method

.method public h()I
    .locals 1

    .prologue
    .line 261
    iget v0, p0, Ljuu;->d:I

    return v0
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 272
    iget-boolean v0, p0, Ljuu;->b:Z

    return v0
.end method

.method public j()D
    .locals 4

    .prologue
    .line 276
    iget-object v0, p0, Ljuu;->c:Liii;

    invoke-interface {v0}, Liii;->b()J

    move-result-wide v0

    long-to-double v0, v0

    const-wide v2, 0x408f400000000000L    # 1000.0

    div-double/2addr v0, v2

    return-wide v0
.end method

.method public k()D
    .locals 4

    .prologue
    .line 280
    iget-object v0, p0, Ljuu;->c:Liii;

    invoke-interface {v0}, Liii;->c()J

    move-result-wide v0

    long-to-double v0, v0

    const-wide v2, 0x408f400000000000L    # 1000.0

    div-double/2addr v0, v2

    return-wide v0
.end method
