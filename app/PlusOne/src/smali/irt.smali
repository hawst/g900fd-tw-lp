.class public final Lirt;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/widget/AbsListView$MultiChoiceModeListener;


# instance fields
.field private a:Z

.field private synthetic b:Lcom/google/android/libraries/social/ingest/IngestActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/social/ingest/IngestActivity;)V
    .locals 1

    .prologue
    .line 123
    iput-object p1, p0, Lirt;->b:Lcom/google/android/libraries/social/ingest/IngestActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 124
    const/4 v0, 0x0

    iput-boolean v0, p0, Lirt;->a:Z

    return-void
.end method

.method private a(Landroid/view/ActionMode;)V
    .locals 6

    .prologue
    .line 127
    iget-object v0, p0, Lirt;->b:Lcom/google/android/libraries/social/ingest/IngestActivity;

    invoke-static {v0}, Lcom/google/android/libraries/social/ingest/IngestActivity;->a(Lcom/google/android/libraries/social/ingest/IngestActivity;)Lcom/google/android/libraries/social/ingest/ui/IngestGridView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ingest/ui/IngestGridView;->getCheckedItemCount()I

    move-result v0

    .line 128
    iget-object v1, p0, Lirt;->b:Lcom/google/android/libraries/social/ingest/IngestActivity;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/ingest/IngestActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f110016

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 129
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    .line 128
    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/ActionMode;->setTitle(Ljava/lang/CharSequence;)V

    .line 130
    return-void
.end method


# virtual methods
.method public onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lirt;->b:Lcom/google/android/libraries/social/ingest/IngestActivity;

    invoke-virtual {v0, p2}, Lcom/google/android/libraries/social/ingest/IngestActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 3

    .prologue
    .line 185
    invoke-virtual {p1}, Landroid/view/ActionMode;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 186
    const v1, 0x7f12000a

    invoke-virtual {v0, v1, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 187
    invoke-direct {p0, p1}, Lirt;->a(Landroid/view/ActionMode;)V

    .line 188
    iget-object v0, p0, Lirt;->b:Lcom/google/android/libraries/social/ingest/IngestActivity;

    invoke-static {v0, p1}, Lcom/google/android/libraries/social/ingest/IngestActivity;->a(Lcom/google/android/libraries/social/ingest/IngestActivity;Landroid/view/ActionMode;)Landroid/view/ActionMode;

    .line 189
    iget-object v0, p0, Lirt;->b:Lcom/google/android/libraries/social/ingest/IngestActivity;

    const v1, 0x7f1006e8

    invoke-interface {p2, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/ingest/IngestActivity;->a(Lcom/google/android/libraries/social/ingest/IngestActivity;Landroid/view/MenuItem;)Landroid/view/MenuItem;

    .line 190
    iget-object v0, p0, Lirt;->b:Lcom/google/android/libraries/social/ingest/IngestActivity;

    iget-object v1, p0, Lirt;->b:Lcom/google/android/libraries/social/ingest/IngestActivity;

    invoke-static {v1}, Lcom/google/android/libraries/social/ingest/IngestActivity;->d(Lcom/google/android/libraries/social/ingest/IngestActivity;)Landroid/view/MenuItem;

    move-result-object v1

    iget-object v2, p0, Lirt;->b:Lcom/google/android/libraries/social/ingest/IngestActivity;

    invoke-static {v2}, Lcom/google/android/libraries/social/ingest/IngestActivity;->e(Lcom/google/android/libraries/social/ingest/IngestActivity;)Z

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/google/android/libraries/social/ingest/IngestActivity;->a(Lcom/google/android/libraries/social/ingest/IngestActivity;Landroid/view/MenuItem;Z)V

    .line 191
    const/4 v0, 0x1

    return v0
.end method

.method public onDestroyActionMode(Landroid/view/ActionMode;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 196
    iget-object v0, p0, Lirt;->b:Lcom/google/android/libraries/social/ingest/IngestActivity;

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/ingest/IngestActivity;->a(Lcom/google/android/libraries/social/ingest/IngestActivity;Landroid/view/ActionMode;)Landroid/view/ActionMode;

    .line 197
    iget-object v0, p0, Lirt;->b:Lcom/google/android/libraries/social/ingest/IngestActivity;

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/ingest/IngestActivity;->a(Lcom/google/android/libraries/social/ingest/IngestActivity;Landroid/view/MenuItem;)Landroid/view/MenuItem;

    .line 198
    iget-object v0, p0, Lirt;->b:Lcom/google/android/libraries/social/ingest/IngestActivity;

    invoke-static {v0}, Lcom/google/android/libraries/social/ingest/IngestActivity;->f(Lcom/google/android/libraries/social/ingest/IngestActivity;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 199
    return-void
.end method

.method public onItemCheckedStateChanged(Landroid/view/ActionMode;IJZ)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 135
    iget-boolean v0, p0, Lirt;->a:Z

    if-eqz v0, :cond_0

    .line 176
    :goto_0
    return-void

    .line 138
    :cond_0
    iget-object v0, p0, Lirt;->b:Lcom/google/android/libraries/social/ingest/IngestActivity;

    invoke-static {v0}, Lcom/google/android/libraries/social/ingest/IngestActivity;->b(Lcom/google/android/libraries/social/ingest/IngestActivity;)Lisd;

    move-result-object v0

    invoke-virtual {v0, p2}, Lisd;->a(I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 139
    iget-object v0, p0, Lirt;->b:Lcom/google/android/libraries/social/ingest/IngestActivity;

    invoke-static {v0}, Lcom/google/android/libraries/social/ingest/IngestActivity;->a(Lcom/google/android/libraries/social/ingest/IngestActivity;)Lcom/google/android/libraries/social/ingest/ui/IngestGridView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ingest/ui/IngestGridView;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v4

    .line 140
    iput-boolean v1, p0, Lirt;->a:Z

    .line 141
    iget-object v0, p0, Lirt;->b:Lcom/google/android/libraries/social/ingest/IngestActivity;

    invoke-static {v0}, Lcom/google/android/libraries/social/ingest/IngestActivity;->a(Lcom/google/android/libraries/social/ingest/IngestActivity;)Lcom/google/android/libraries/social/ingest/ui/IngestGridView;

    move-result-object v0

    invoke-virtual {v0, p2, v2}, Lcom/google/android/libraries/social/ingest/ui/IngestGridView;->setItemChecked(IZ)V

    .line 145
    iget-object v0, p0, Lirt;->b:Lcom/google/android/libraries/social/ingest/IngestActivity;

    invoke-static {v0}, Lcom/google/android/libraries/social/ingest/IngestActivity;->b(Lcom/google/android/libraries/social/ingest/IngestActivity;)Lisd;

    move-result-object v0

    iget-object v3, p0, Lirt;->b:Lcom/google/android/libraries/social/ingest/IngestActivity;

    .line 146
    invoke-static {v3}, Lcom/google/android/libraries/social/ingest/IngestActivity;->b(Lcom/google/android/libraries/social/ingest/IngestActivity;)Lisd;

    move-result-object v3

    invoke-virtual {v3, p2}, Lisd;->getSectionForPosition(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    .line 145
    invoke-virtual {v0, v3}, Lisd;->getPositionForSection(I)I

    move-result v0

    .line 147
    if-ne v0, p2, :cond_1

    .line 148
    iget-object v0, p0, Lirt;->b:Lcom/google/android/libraries/social/ingest/IngestActivity;

    invoke-static {v0}, Lcom/google/android/libraries/social/ingest/IngestActivity;->b(Lcom/google/android/libraries/social/ingest/IngestActivity;)Lisd;

    move-result-object v0

    invoke-virtual {v0}, Lisd;->getCount()I

    move-result v0

    .line 155
    :cond_1
    add-int/lit8 v3, p2, 0x1

    :goto_1
    if-ge v3, v0, :cond_6

    .line 156
    invoke-virtual {v4, v3}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v5

    if-nez v5, :cond_3

    .line 163
    :goto_2
    add-int/lit8 v3, p2, 0x1

    :goto_3
    if-ge v3, v0, :cond_4

    .line 164
    invoke-virtual {v4, v3}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v5

    if-eq v5, v1, :cond_2

    .line 165
    iget-object v5, p0, Lirt;->b:Lcom/google/android/libraries/social/ingest/IngestActivity;

    invoke-static {v5}, Lcom/google/android/libraries/social/ingest/IngestActivity;->a(Lcom/google/android/libraries/social/ingest/IngestActivity;)Lcom/google/android/libraries/social/ingest/ui/IngestGridView;

    move-result-object v5

    invoke-virtual {v5, v3, v1}, Lcom/google/android/libraries/social/ingest/ui/IngestGridView;->setItemChecked(IZ)V

    .line 163
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 155
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 169
    :cond_4
    iget-object v0, p0, Lirt;->b:Lcom/google/android/libraries/social/ingest/IngestActivity;

    invoke-static {v0}, Lcom/google/android/libraries/social/ingest/IngestActivity;->c(Lcom/google/android/libraries/social/ingest/IngestActivity;)Lirx;

    move-result-object v0

    invoke-virtual {v0}, Lirx;->b()V

    .line 170
    iput-boolean v2, p0, Lirt;->a:Z

    .line 174
    :goto_4
    iget-object v0, p0, Lirt;->b:Lcom/google/android/libraries/social/ingest/IngestActivity;

    invoke-static {v0, p2}, Lcom/google/android/libraries/social/ingest/IngestActivity;->a(Lcom/google/android/libraries/social/ingest/IngestActivity;I)I

    .line 175
    invoke-direct {p0, p1}, Lirt;->a(Landroid/view/ActionMode;)V

    goto :goto_0

    .line 172
    :cond_5
    iget-object v0, p0, Lirt;->b:Lcom/google/android/libraries/social/ingest/IngestActivity;

    invoke-static {v0}, Lcom/google/android/libraries/social/ingest/IngestActivity;->c(Lcom/google/android/libraries/social/ingest/IngestActivity;)Lirx;

    move-result-object v0

    invoke-virtual {v0, p2, p5}, Lirx;->b(IZ)V

    goto :goto_4

    :cond_6
    move v1, v2

    goto :goto_2
.end method

.method public onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 203
    invoke-direct {p0, p1}, Lirt;->a(Landroid/view/ActionMode;)V

    .line 204
    const/4 v0, 0x0

    return v0
.end method
