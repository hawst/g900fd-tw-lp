.class public final Lfxb;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private synthetic a:Lcom/google/android/apps/plus/views/DreamViewFlipper;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/views/DreamViewFlipper;)V
    .locals 0

    .prologue
    .line 224
    iput-object p1, p0, Lfxb;->a:Lcom/google/android/apps/plus/views/DreamViewFlipper;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/String;)Landroid/database/Cursor;
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 228
    aget-object v2, p1, v0

    invoke-static {v2}, Lcom/google/android/apps/plus/service/EsDreamService;->a(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 229
    aget-object v3, v2, v0

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 230
    aget-object v2, v2, v1

    .line 231
    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    move v0, v1

    .line 232
    :cond_0
    if-eqz v3, :cond_1

    if-eqz v0, :cond_2

    .line 233
    :cond_1
    iget-object v1, p0, Lfxb;->a:Lcom/google/android/apps/plus/views/DreamViewFlipper;

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/plus/views/DreamViewFlipper;->a(Lcom/google/android/apps/plus/views/DreamViewFlipper;Ljava/lang/String;Z)Landroid/database/Cursor;

    move-result-object v0

    .line 237
    :goto_0
    return-object v0

    .line 234
    :cond_2
    if-ne v3, v1, :cond_3

    .line 235
    iget-object v0, p0, Lfxb;->a:Lcom/google/android/apps/plus/views/DreamViewFlipper;

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/views/DreamViewFlipper;->a(Lcom/google/android/apps/plus/views/DreamViewFlipper;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    .line 237
    :cond_3
    iget-object v0, p0, Lfxb;->a:Lcom/google/android/apps/plus/views/DreamViewFlipper;

    invoke-static {v0}, Lcom/google/android/apps/plus/views/DreamViewFlipper;->k(Lcom/google/android/apps/plus/views/DreamViewFlipper;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0
.end method

.method protected a(Landroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 243
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 244
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 245
    iget-object v0, p0, Lfxb;->a:Lcom/google/android/apps/plus/views/DreamViewFlipper;

    invoke-static {v0}, Lcom/google/android/apps/plus/views/DreamViewFlipper;->g(Lcom/google/android/apps/plus/views/DreamViewFlipper;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 246
    iget-object v0, p0, Lfxb;->a:Lcom/google/android/apps/plus/views/DreamViewFlipper;

    invoke-static {v0}, Lcom/google/android/apps/plus/views/DreamViewFlipper;->g(Lcom/google/android/apps/plus/views/DreamViewFlipper;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 248
    :cond_0
    iget-object v0, p0, Lfxb;->a:Lcom/google/android/apps/plus/views/DreamViewFlipper;

    invoke-static {v0, p1}, Lcom/google/android/apps/plus/views/DreamViewFlipper;->a(Lcom/google/android/apps/plus/views/DreamViewFlipper;Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 249
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 252
    iget-object v0, p0, Lfxb;->a:Lcom/google/android/apps/plus/views/DreamViewFlipper;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/views/DreamViewFlipper;->a(Lcom/google/android/apps/plus/views/DreamViewFlipper;Z)Z

    .line 256
    iget-object v0, p0, Lfxb;->a:Lcom/google/android/apps/plus/views/DreamViewFlipper;

    invoke-static {v0}, Lcom/google/android/apps/plus/views/DreamViewFlipper;->f(Lcom/google/android/apps/plus/views/DreamViewFlipper;)Lcom/google/android/libraries/social/media/ui/MediaView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->setAlpha(F)V

    .line 257
    iget-object v0, p0, Lfxb;->a:Lcom/google/android/apps/plus/views/DreamViewFlipper;

    iget-object v1, p0, Lfxb;->a:Lcom/google/android/apps/plus/views/DreamViewFlipper;

    invoke-static {v1}, Lcom/google/android/apps/plus/views/DreamViewFlipper;->f(Lcom/google/android/apps/plus/views/DreamViewFlipper;)Lcom/google/android/libraries/social/media/ui/MediaView;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/google/android/apps/plus/views/DreamViewFlipper;->a(Lcom/google/android/apps/plus/views/DreamViewFlipper;Landroid/database/Cursor;Lcom/google/android/libraries/social/media/ui/MediaView;)V

    .line 263
    :goto_0
    return-void

    .line 260
    :cond_1
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 261
    iget-object v0, p0, Lfxb;->a:Lcom/google/android/apps/plus/views/DreamViewFlipper;

    invoke-static {v0}, Lcom/google/android/apps/plus/views/DreamViewFlipper;->d(Lcom/google/android/apps/plus/views/DreamViewFlipper;)V

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 224
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lfxb;->a([Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 224
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Lfxb;->a(Landroid/database/Cursor;)V

    return-void
.end method
