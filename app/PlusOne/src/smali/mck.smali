.class public final Lmck;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmpi;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 9637
    invoke-direct {p0}, Loxq;-><init>()V

    .line 9640
    iput-object v0, p0, Lmck;->apiHeader:Llyq;

    .line 9643
    iput-object v0, p0, Lmck;->a:Lmpi;

    .line 9637
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 9660
    const/4 v0, 0x0

    .line 9661
    iget-object v1, p0, Lmck;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 9662
    const/4 v0, 0x1

    iget-object v1, p0, Lmck;->apiHeader:Llyq;

    .line 9663
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 9665
    :cond_0
    iget-object v1, p0, Lmck;->a:Lmpi;

    if-eqz v1, :cond_1

    .line 9666
    const/4 v1, 0x2

    iget-object v2, p0, Lmck;->a:Lmpi;

    .line 9667
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9669
    :cond_1
    iget-object v1, p0, Lmck;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9670
    iput v0, p0, Lmck;->ai:I

    .line 9671
    return v0
.end method

.method public a(Loxn;)Lmck;
    .locals 2

    .prologue
    .line 9679
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 9680
    sparse-switch v0, :sswitch_data_0

    .line 9684
    iget-object v1, p0, Lmck;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 9685
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmck;->ah:Ljava/util/List;

    .line 9688
    :cond_1
    iget-object v1, p0, Lmck;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 9690
    :sswitch_0
    return-object p0

    .line 9695
    :sswitch_1
    iget-object v0, p0, Lmck;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 9696
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmck;->apiHeader:Llyq;

    .line 9698
    :cond_2
    iget-object v0, p0, Lmck;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 9702
    :sswitch_2
    iget-object v0, p0, Lmck;->a:Lmpi;

    if-nez v0, :cond_3

    .line 9703
    new-instance v0, Lmpi;

    invoke-direct {v0}, Lmpi;-><init>()V

    iput-object v0, p0, Lmck;->a:Lmpi;

    .line 9705
    :cond_3
    iget-object v0, p0, Lmck;->a:Lmpi;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 9680
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 9648
    iget-object v0, p0, Lmck;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 9649
    const/4 v0, 0x1

    iget-object v1, p0, Lmck;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 9651
    :cond_0
    iget-object v0, p0, Lmck;->a:Lmpi;

    if-eqz v0, :cond_1

    .line 9652
    const/4 v0, 0x2

    iget-object v1, p0, Lmck;->a:Lmpi;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 9654
    :cond_1
    iget-object v0, p0, Lmck;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 9656
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 9633
    invoke-virtual {p0, p1}, Lmck;->a(Loxn;)Lmck;

    move-result-object v0

    return-object v0
.end method
