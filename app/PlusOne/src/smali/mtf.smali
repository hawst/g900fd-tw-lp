.class public final Lmtf;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Integer;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3055
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 3081
    const/4 v0, 0x0

    .line 3082
    iget-object v1, p0, Lmtf;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 3083
    const/4 v0, 0x1

    iget-object v1, p0, Lmtf;->a:Ljava/lang/Integer;

    .line 3084
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3086
    :cond_0
    iget-object v1, p0, Lmtf;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 3087
    const/4 v1, 0x2

    iget-object v2, p0, Lmtf;->b:Ljava/lang/String;

    .line 3088
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3090
    :cond_1
    iget-object v1, p0, Lmtf;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 3091
    const/4 v1, 0x3

    iget-object v2, p0, Lmtf;->c:Ljava/lang/String;

    .line 3092
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3094
    :cond_2
    iget-object v1, p0, Lmtf;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3095
    iput v0, p0, Lmtf;->ai:I

    .line 3096
    return v0
.end method

.method public a(Loxn;)Lmtf;
    .locals 2

    .prologue
    .line 3104
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3105
    sparse-switch v0, :sswitch_data_0

    .line 3109
    iget-object v1, p0, Lmtf;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 3110
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmtf;->ah:Ljava/util/List;

    .line 3113
    :cond_1
    iget-object v1, p0, Lmtf;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3115
    :sswitch_0
    return-object p0

    .line 3120
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmtf;->a:Ljava/lang/Integer;

    goto :goto_0

    .line 3124
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmtf;->b:Ljava/lang/String;

    goto :goto_0

    .line 3128
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmtf;->c:Ljava/lang/String;

    goto :goto_0

    .line 3105
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 3066
    iget-object v0, p0, Lmtf;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 3067
    const/4 v0, 0x1

    iget-object v1, p0, Lmtf;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 3069
    :cond_0
    iget-object v0, p0, Lmtf;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 3070
    const/4 v0, 0x2

    iget-object v1, p0, Lmtf;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3072
    :cond_1
    iget-object v0, p0, Lmtf;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 3073
    const/4 v0, 0x3

    iget-object v1, p0, Lmtf;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3075
    :cond_2
    iget-object v0, p0, Lmtf;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3077
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3051
    invoke-virtual {p0, p1}, Lmtf;->a(Loxn;)Lmtf;

    move-result-object v0

    return-object v0
.end method
