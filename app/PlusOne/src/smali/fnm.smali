.class public final Lfnm;
.super Lkgg;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkgg",
        "<",
        "Lmdg;",
        "Lmdh;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final p:Z

.field private final q:Z

.field private final r:[Landroid/graphics/Point;

.field private final s:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ[Landroid/graphics/Point;Z)V
    .locals 6

    .prologue
    .line 43
    const-string v3, "getstory"

    new-instance v4, Lmdg;

    invoke-direct {v4}, Lmdg;-><init>()V

    new-instance v5, Lmdh;

    invoke-direct {v5}, Lmdh;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lkgg;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Loxu;Loxu;)V

    .line 44
    iput-object p3, p0, Lfnm;->a:Ljava/lang/String;

    .line 45
    iput-object p4, p0, Lfnm;->b:Ljava/lang/String;

    .line 46
    iput-object p5, p0, Lfnm;->c:Ljava/lang/String;

    .line 47
    iput-boolean p6, p0, Lfnm;->p:Z

    .line 48
    iput-boolean p7, p0, Lfnm;->q:Z

    .line 49
    iput-object p8, p0, Lfnm;->r:[Landroid/graphics/Point;

    .line 50
    iput-boolean p9, p0, Lfnm;->s:Z

    .line 51
    return-void
.end method


# virtual methods
.method protected a(Lmdg;)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v1, 0x0

    .line 55
    new-instance v0, Lmlq;

    invoke-direct {v0}, Lmlq;-><init>()V

    iput-object v0, p1, Lmdg;->a:Lmlq;

    .line 56
    iget-object v4, p1, Lmdg;->a:Lmlq;

    .line 57
    new-instance v0, Lmlw;

    invoke-direct {v0}, Lmlw;-><init>()V

    iput-object v0, v4, Lmlq;->a:Lmlw;

    .line 58
    iget-object v0, v4, Lmlq;->a:Lmlw;

    iget-object v2, p0, Lfnm;->b:Ljava/lang/String;

    iput-object v2, v0, Lmlw;->b:Ljava/lang/String;

    .line 59
    iget-object v0, v4, Lmlq;->a:Lmlw;

    iget-object v2, p0, Lfnm;->c:Ljava/lang/String;

    iput-object v2, v0, Lmlw;->c:Ljava/lang/String;

    .line 60
    iget-object v0, v4, Lmlq;->a:Lmlw;

    new-instance v2, Lmlh;

    invoke-direct {v2}, Lmlh;-><init>()V

    iput-object v2, v0, Lmlw;->a:Lmlh;

    .line 61
    iget-object v0, v4, Lmlq;->a:Lmlw;

    iget-object v0, v0, Lmlw;->a:Lmlh;

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v0, Lmlh;->b:Ljava/lang/Boolean;

    .line 62
    iget-object v0, p0, Lfnm;->r:[Landroid/graphics/Point;

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, v4, Lmlq;->a:Lmlw;

    iget-object v0, v0, Lmlw;->a:Lmlh;

    iget-object v2, p0, Lfnm;->r:[Landroid/graphics/Point;

    array-length v2, v2

    new-array v2, v2, [Lmli;

    iput-object v2, v0, Lmlh;->a:[Lmli;

    .line 66
    iget-object v5, p0, Lfnm;->r:[Landroid/graphics/Point;

    array-length v6, v5

    move v0, v1

    move v2, v1

    :goto_0
    if-ge v0, v6, :cond_0

    aget-object v3, v5, v0

    .line 67
    new-instance v7, Lmli;

    invoke-direct {v7}, Lmli;-><init>()V

    .line 69
    iget v8, v3, Landroid/graphics/Point;->x:I

    int-to-float v8, v8

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    iput-object v8, v7, Lmli;->b:Ljava/lang/Float;

    .line 70
    iget v3, v3, Landroid/graphics/Point;->y:I

    int-to-float v3, v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    iput-object v3, v7, Lmli;->c:Ljava/lang/Float;

    .line 71
    iget-object v3, p0, Lfnm;->f:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    iput-object v3, v7, Lmli;->d:Ljava/lang/Float;

    .line 72
    iget-object v3, v4, Lmlq;->a:Lmlw;

    iget-object v3, v3, Lmlw;->a:Lmlh;

    iget-object v8, v3, Lmlh;->a:[Lmli;

    add-int/lit8 v3, v2, 0x1

    aput-object v7, v8, v2

    .line 66
    add-int/lit8 v0, v0, 0x1

    move v2, v3

    goto :goto_0

    .line 75
    :cond_0
    new-instance v0, Lmok;

    invoke-direct {v0}, Lmok;-><init>()V

    iput-object v0, v4, Lmlq;->b:Lmok;

    .line 76
    iget-object v0, v4, Lmlq;->b:Lmok;

    iget-object v2, p0, Lfnm;->a:Ljava/lang/String;

    iput-object v2, v0, Lmok;->a:Ljava/lang/String;

    .line 77
    new-instance v0, Lmlx;

    invoke-direct {v0}, Lmlx;-><init>()V

    iput-object v0, v4, Lmlq;->c:Lmlx;

    .line 78
    iget-object v0, v4, Lmlq;->c:Lmlx;

    iget-boolean v2, p0, Lfnm;->p:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v0, Lmlx;->a:Ljava/lang/Boolean;

    .line 79
    iget-object v0, v4, Lmlq;->c:Lmlx;

    iget-boolean v2, p0, Lfnm;->q:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v0, Lmlx;->b:Ljava/lang/Boolean;

    .line 80
    new-instance v0, Lnei;

    invoke-direct {v0}, Lnei;-><init>()V

    iput-object v0, v4, Lmlq;->d:Lnei;

    .line 81
    iget-object v0, v4, Lmlq;->d:Lnei;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v0, Lnei;->g:Ljava/lang/Boolean;

    .line 82
    iget-object v0, v4, Lmlq;->d:Lnei;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v0, Lnei;->i:Ljava/lang/Boolean;

    .line 83
    iget-object v0, v4, Lmlq;->d:Lnei;

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v0, Lnei;->l:Ljava/lang/Boolean;

    .line 84
    iget-object v0, v4, Lmlq;->d:Lnei;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lnei;->e:Ljava/lang/Boolean;

    .line 85
    iget-boolean v0, p0, Lfnm;->s:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v4, Lmlq;->e:Ljava/lang/Boolean;

    .line 86
    return-void
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 18
    check-cast p1, Lmdg;

    invoke-virtual {p0, p1}, Lfnm;->a(Lmdg;)V

    return-void
.end method
