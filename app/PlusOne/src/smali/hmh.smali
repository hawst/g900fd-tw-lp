.class public final Lhmh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field private final a:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field private final b:Lhmk;

.field private final c:Lhmk;


# direct methods
.method public constructor <init>(Lhmk;Lhmk;Landroid/widget/CompoundButton$OnCheckedChangeListener;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p3, p0, Lhmh;->a:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 31
    iput-object p1, p0, Lhmh;->b:Lhmk;

    .line 32
    iput-object p2, p0, Lhmh;->c:Lhmk;

    .line 33
    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 1

    .prologue
    .line 37
    invoke-virtual {p1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 38
    iget-object v0, p0, Lhmh;->b:Lhmk;

    invoke-static {p1, v0}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    .line 42
    :goto_0
    const/4 v0, 0x4

    invoke-static {p1, v0}, Lhly;->a(Landroid/view/View;I)V

    .line 43
    iget-object v0, p0, Lhmh;->a:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-interface {v0, p1, p2}, Landroid/widget/CompoundButton$OnCheckedChangeListener;->onCheckedChanged(Landroid/widget/CompoundButton;Z)V

    .line 44
    return-void

    .line 40
    :cond_0
    iget-object v0, p0, Lhmh;->c:Lhmk;

    invoke-static {p1, v0}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    goto :goto_0
.end method
