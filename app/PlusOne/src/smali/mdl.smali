.class public final Lmdl;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lncj;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 21706
    invoke-direct {p0}, Loxq;-><init>()V

    .line 21709
    iput-object v0, p0, Lmdl;->apiHeader:Llyr;

    .line 21712
    iput-object v0, p0, Lmdl;->a:Lncj;

    .line 21706
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 21729
    const/4 v0, 0x0

    .line 21730
    iget-object v1, p0, Lmdl;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 21731
    const/4 v0, 0x1

    iget-object v1, p0, Lmdl;->apiHeader:Llyr;

    .line 21732
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 21734
    :cond_0
    iget-object v1, p0, Lmdl;->a:Lncj;

    if-eqz v1, :cond_1

    .line 21735
    const/4 v1, 0x2

    iget-object v2, p0, Lmdl;->a:Lncj;

    .line 21736
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 21738
    :cond_1
    iget-object v1, p0, Lmdl;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 21739
    iput v0, p0, Lmdl;->ai:I

    .line 21740
    return v0
.end method

.method public a(Loxn;)Lmdl;
    .locals 2

    .prologue
    .line 21748
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 21749
    sparse-switch v0, :sswitch_data_0

    .line 21753
    iget-object v1, p0, Lmdl;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 21754
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmdl;->ah:Ljava/util/List;

    .line 21757
    :cond_1
    iget-object v1, p0, Lmdl;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 21759
    :sswitch_0
    return-object p0

    .line 21764
    :sswitch_1
    iget-object v0, p0, Lmdl;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 21765
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmdl;->apiHeader:Llyr;

    .line 21767
    :cond_2
    iget-object v0, p0, Lmdl;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 21771
    :sswitch_2
    iget-object v0, p0, Lmdl;->a:Lncj;

    if-nez v0, :cond_3

    .line 21772
    new-instance v0, Lncj;

    invoke-direct {v0}, Lncj;-><init>()V

    iput-object v0, p0, Lmdl;->a:Lncj;

    .line 21774
    :cond_3
    iget-object v0, p0, Lmdl;->a:Lncj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 21749
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 21717
    iget-object v0, p0, Lmdl;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 21718
    const/4 v0, 0x1

    iget-object v1, p0, Lmdl;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 21720
    :cond_0
    iget-object v0, p0, Lmdl;->a:Lncj;

    if-eqz v0, :cond_1

    .line 21721
    const/4 v0, 0x2

    iget-object v1, p0, Lmdl;->a:Lncj;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 21723
    :cond_1
    iget-object v0, p0, Lmdl;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 21725
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 21702
    invoke-virtual {p0, p1}, Lmdl;->a(Loxn;)Lmdl;

    move-result-object v0

    return-object v0
.end method
