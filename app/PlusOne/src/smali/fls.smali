.class final Lfls;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lkhq;


# instance fields
.field private synthetic a:Lflp;


# direct methods
.method constructor <init>(Lflp;)V
    .locals 0

    .prologue
    .line 299
    iput-object p1, p0, Lfls;->a:Lflp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lkhl;)Z
    .locals 5

    .prologue
    .line 303
    :try_start_0
    invoke-virtual {p1}, Lkhl;->u()Ljava/lang/String;

    move-result-object v0

    .line 306
    invoke-static {}, Lfvc;->c()Z

    move-result v1

    if-nez v1, :cond_0

    .line 307
    const-string v1, "about_photo_location"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 308
    iget-object v0, p0, Lfls;->a:Lflp;

    invoke-static {v0}, Lflp;->e(Lflp;)Llnl;

    move-result-object v0

    const-class v1, Limn;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Limn;

    iget-object v1, p0, Lfls;->a:Lflp;

    invoke-static {v1}, Lflp;->d(Lflp;)Llnl;

    move-result-object v1

    invoke-interface {v0, v1}, Limn;->a(Landroid/content/Context;)V

    .line 319
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 309
    :cond_1
    const-string v1, "about_find_my_face"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 310
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    iget-object v2, p0, Lfls;->a:Lflp;

    .line 311
    invoke-static {v2}, Lflp;->f(Lflp;)Llnl;

    move-result-object v2

    const-string v3, "find_my_face"

    const-string v4, "https://support.google.com/plus/?hl=%locale%"

    invoke-static {v2, v3, v4}, Litk;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    .line 310
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 312
    iget-object v1, p0, Lfls;->a:Lflp;

    invoke-virtual {v1, v0}, Lflp;->a(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 316
    :catch_0
    move-exception v0

    iget-object v0, p0, Lfls;->a:Lflp;

    invoke-static {v0}, Lflp;->g(Lflp;)Llnl;

    move-result-object v0

    const v1, 0x7f0a0655

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 317
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
