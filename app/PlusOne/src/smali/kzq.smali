.class public final Lkzq;
.super Lllq;
.source "PG"

# interfaces
.implements Lhtu;


# instance fields
.field private a:I

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lllq;-><init>()V

    .line 22
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lllq;-><init>()V

    .line 25
    iput p1, p0, Lkzq;->a:I

    .line 26
    iput-object p2, p0, Lkzq;->b:Ljava/lang/String;

    .line 27
    iput-object p3, p0, Lkzq;->c:Ljava/lang/String;

    .line 28
    return-void
.end method

.method public static a(Ljava/nio/ByteBuffer;)Lkzq;
    .locals 1

    .prologue
    .line 54
    new-instance v0, Lkzq;

    invoke-direct {v0}, Lkzq;-><init>()V

    .line 55
    invoke-virtual {v0, p0}, Lkzq;->b(Ljava/nio/ByteBuffer;)V

    .line 56
    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lkzq;->b:Ljava/lang/String;

    return-object v0
.end method

.method public a(Ljava/io/DataOutputStream;)V
    .locals 1

    .prologue
    .line 47
    iget v0, p0, Lkzq;->a:I

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 48
    iget-object v0, p0, Lkzq;->b:Ljava/lang/String;

    invoke-static {p1, v0}, Lkzq;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 49
    iget-object v0, p0, Lkzq;->c:Ljava/lang/String;

    invoke-static {p1, v0}, Lkzq;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 51
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lkzq;->c:Ljava/lang/String;

    return-object v0
.end method

.method public b(Ljava/nio/ByteBuffer;)V
    .locals 1

    .prologue
    .line 72
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    iput v0, p0, Lkzq;->a:I

    .line 73
    invoke-static {p1}, Lkzq;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkzq;->b:Ljava/lang/String;

    .line 74
    invoke-static {p1}, Lkzq;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkzq;->c:Ljava/lang/String;

    .line 75
    return-void
.end method

.method public c()Z
    .locals 2

    .prologue
    .line 93
    iget v0, p0, Lkzq;->a:I

    const/16 v1, 0xb

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
