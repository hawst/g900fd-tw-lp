.class public final Lmgj;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnfh;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 24136
    invoke-direct {p0}, Loxq;-><init>()V

    .line 24139
    iput-object v0, p0, Lmgj;->apiHeader:Llyr;

    .line 24142
    iput-object v0, p0, Lmgj;->a:Lnfh;

    .line 24136
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 24159
    const/4 v0, 0x0

    .line 24160
    iget-object v1, p0, Lmgj;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 24161
    const/4 v0, 0x1

    iget-object v1, p0, Lmgj;->apiHeader:Llyr;

    .line 24162
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 24164
    :cond_0
    iget-object v1, p0, Lmgj;->a:Lnfh;

    if-eqz v1, :cond_1

    .line 24165
    const/4 v1, 0x2

    iget-object v2, p0, Lmgj;->a:Lnfh;

    .line 24166
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 24168
    :cond_1
    iget-object v1, p0, Lmgj;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 24169
    iput v0, p0, Lmgj;->ai:I

    .line 24170
    return v0
.end method

.method public a(Loxn;)Lmgj;
    .locals 2

    .prologue
    .line 24178
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 24179
    sparse-switch v0, :sswitch_data_0

    .line 24183
    iget-object v1, p0, Lmgj;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 24184
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmgj;->ah:Ljava/util/List;

    .line 24187
    :cond_1
    iget-object v1, p0, Lmgj;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 24189
    :sswitch_0
    return-object p0

    .line 24194
    :sswitch_1
    iget-object v0, p0, Lmgj;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 24195
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmgj;->apiHeader:Llyr;

    .line 24197
    :cond_2
    iget-object v0, p0, Lmgj;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 24201
    :sswitch_2
    iget-object v0, p0, Lmgj;->a:Lnfh;

    if-nez v0, :cond_3

    .line 24202
    new-instance v0, Lnfh;

    invoke-direct {v0}, Lnfh;-><init>()V

    iput-object v0, p0, Lmgj;->a:Lnfh;

    .line 24204
    :cond_3
    iget-object v0, p0, Lmgj;->a:Lnfh;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 24179
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 24147
    iget-object v0, p0, Lmgj;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 24148
    const/4 v0, 0x1

    iget-object v1, p0, Lmgj;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 24150
    :cond_0
    iget-object v0, p0, Lmgj;->a:Lnfh;

    if-eqz v0, :cond_1

    .line 24151
    const/4 v0, 0x2

    iget-object v1, p0, Lmgj;->a:Lnfh;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 24153
    :cond_1
    iget-object v0, p0, Lmgj;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 24155
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 24132
    invoke-virtual {p0, p1}, Lmgj;->a(Loxn;)Lmgj;

    move-result-object v0

    return-object v0
.end method
