.class public final Llzl;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lojz;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10366
    invoke-direct {p0}, Loxq;-><init>()V

    .line 10369
    iput-object v0, p0, Llzl;->apiHeader:Llyr;

    .line 10372
    iput-object v0, p0, Llzl;->a:Lojz;

    .line 10366
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 10389
    const/4 v0, 0x0

    .line 10390
    iget-object v1, p0, Llzl;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 10391
    const/4 v0, 0x1

    iget-object v1, p0, Llzl;->apiHeader:Llyr;

    .line 10392
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 10394
    :cond_0
    iget-object v1, p0, Llzl;->a:Lojz;

    if-eqz v1, :cond_1

    .line 10395
    const/4 v1, 0x2

    iget-object v2, p0, Llzl;->a:Lojz;

    .line 10396
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10398
    :cond_1
    iget-object v1, p0, Llzl;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10399
    iput v0, p0, Llzl;->ai:I

    .line 10400
    return v0
.end method

.method public a(Loxn;)Llzl;
    .locals 2

    .prologue
    .line 10408
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 10409
    sparse-switch v0, :sswitch_data_0

    .line 10413
    iget-object v1, p0, Llzl;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 10414
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llzl;->ah:Ljava/util/List;

    .line 10417
    :cond_1
    iget-object v1, p0, Llzl;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 10419
    :sswitch_0
    return-object p0

    .line 10424
    :sswitch_1
    iget-object v0, p0, Llzl;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 10425
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Llzl;->apiHeader:Llyr;

    .line 10427
    :cond_2
    iget-object v0, p0, Llzl;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 10431
    :sswitch_2
    iget-object v0, p0, Llzl;->a:Lojz;

    if-nez v0, :cond_3

    .line 10432
    new-instance v0, Lojz;

    invoke-direct {v0}, Lojz;-><init>()V

    iput-object v0, p0, Llzl;->a:Lojz;

    .line 10434
    :cond_3
    iget-object v0, p0, Llzl;->a:Lojz;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 10409
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 10377
    iget-object v0, p0, Llzl;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 10378
    const/4 v0, 0x1

    iget-object v1, p0, Llzl;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 10380
    :cond_0
    iget-object v0, p0, Llzl;->a:Lojz;

    if-eqz v0, :cond_1

    .line 10381
    const/4 v0, 0x2

    iget-object v1, p0, Llzl;->a:Lojz;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 10383
    :cond_1
    iget-object v0, p0, Llzl;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 10385
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 10362
    invoke-virtual {p0, p1}, Llzl;->a(Loxn;)Llzl;

    move-result-object v0

    return-object v0
.end method
