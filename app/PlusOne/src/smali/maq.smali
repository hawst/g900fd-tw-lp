.class public final Lmaq;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmqj;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 18223
    invoke-direct {p0}, Loxq;-><init>()V

    .line 18226
    iput-object v0, p0, Lmaq;->apiHeader:Llyq;

    .line 18229
    iput-object v0, p0, Lmaq;->a:Lmqj;

    .line 18223
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 18246
    const/4 v0, 0x0

    .line 18247
    iget-object v1, p0, Lmaq;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 18248
    const/4 v0, 0x1

    iget-object v1, p0, Lmaq;->apiHeader:Llyq;

    .line 18249
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 18251
    :cond_0
    iget-object v1, p0, Lmaq;->a:Lmqj;

    if-eqz v1, :cond_1

    .line 18252
    const/4 v1, 0x2

    iget-object v2, p0, Lmaq;->a:Lmqj;

    .line 18253
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 18255
    :cond_1
    iget-object v1, p0, Lmaq;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 18256
    iput v0, p0, Lmaq;->ai:I

    .line 18257
    return v0
.end method

.method public a(Loxn;)Lmaq;
    .locals 2

    .prologue
    .line 18265
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 18266
    sparse-switch v0, :sswitch_data_0

    .line 18270
    iget-object v1, p0, Lmaq;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 18271
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmaq;->ah:Ljava/util/List;

    .line 18274
    :cond_1
    iget-object v1, p0, Lmaq;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 18276
    :sswitch_0
    return-object p0

    .line 18281
    :sswitch_1
    iget-object v0, p0, Lmaq;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 18282
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmaq;->apiHeader:Llyq;

    .line 18284
    :cond_2
    iget-object v0, p0, Lmaq;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 18288
    :sswitch_2
    iget-object v0, p0, Lmaq;->a:Lmqj;

    if-nez v0, :cond_3

    .line 18289
    new-instance v0, Lmqj;

    invoke-direct {v0}, Lmqj;-><init>()V

    iput-object v0, p0, Lmaq;->a:Lmqj;

    .line 18291
    :cond_3
    iget-object v0, p0, Lmaq;->a:Lmqj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 18266
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 18234
    iget-object v0, p0, Lmaq;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 18235
    const/4 v0, 0x1

    iget-object v1, p0, Lmaq;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 18237
    :cond_0
    iget-object v0, p0, Lmaq;->a:Lmqj;

    if-eqz v0, :cond_1

    .line 18238
    const/4 v0, 0x2

    iget-object v1, p0, Lmaq;->a:Lmqj;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 18240
    :cond_1
    iget-object v0, p0, Lmaq;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 18242
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 18219
    invoke-virtual {p0, p1}, Lmaq;->a(Loxn;)Lmaq;

    move-result-object v0

    return-object v0
.end method
