.class public final Lmfo;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnli;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 889
    invoke-direct {p0}, Loxq;-><init>()V

    .line 892
    iput-object v0, p0, Lmfo;->apiHeader:Llyq;

    .line 895
    iput-object v0, p0, Lmfo;->a:Lnli;

    .line 889
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 912
    const/4 v0, 0x0

    .line 913
    iget-object v1, p0, Lmfo;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 914
    const/4 v0, 0x1

    iget-object v1, p0, Lmfo;->apiHeader:Llyq;

    .line 915
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 917
    :cond_0
    iget-object v1, p0, Lmfo;->a:Lnli;

    if-eqz v1, :cond_1

    .line 918
    const/4 v1, 0x2

    iget-object v2, p0, Lmfo;->a:Lnli;

    .line 919
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 921
    :cond_1
    iget-object v1, p0, Lmfo;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 922
    iput v0, p0, Lmfo;->ai:I

    .line 923
    return v0
.end method

.method public a(Loxn;)Lmfo;
    .locals 2

    .prologue
    .line 931
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 932
    sparse-switch v0, :sswitch_data_0

    .line 936
    iget-object v1, p0, Lmfo;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 937
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmfo;->ah:Ljava/util/List;

    .line 940
    :cond_1
    iget-object v1, p0, Lmfo;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 942
    :sswitch_0
    return-object p0

    .line 947
    :sswitch_1
    iget-object v0, p0, Lmfo;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 948
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmfo;->apiHeader:Llyq;

    .line 950
    :cond_2
    iget-object v0, p0, Lmfo;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 954
    :sswitch_2
    iget-object v0, p0, Lmfo;->a:Lnli;

    if-nez v0, :cond_3

    .line 955
    new-instance v0, Lnli;

    invoke-direct {v0}, Lnli;-><init>()V

    iput-object v0, p0, Lmfo;->a:Lnli;

    .line 957
    :cond_3
    iget-object v0, p0, Lmfo;->a:Lnli;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 932
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 900
    iget-object v0, p0, Lmfo;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 901
    const/4 v0, 0x1

    iget-object v1, p0, Lmfo;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 903
    :cond_0
    iget-object v0, p0, Lmfo;->a:Lnli;

    if-eqz v0, :cond_1

    .line 904
    const/4 v0, 0x2

    iget-object v1, p0, Lmfo;->a:Lnli;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 906
    :cond_1
    iget-object v0, p0, Lmfo;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 908
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 885
    invoke-virtual {p0, p1}, Lmfo;->a(Loxn;)Lmfo;

    move-result-object v0

    return-object v0
.end method
