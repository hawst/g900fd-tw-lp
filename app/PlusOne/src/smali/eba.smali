.class public final Leba;
.super Lt;
.source "PG"


# instance fields
.field private N:Lfyc;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lt;-><init>()V

    return-void
.end method

.method static U()Leba;
    .locals 1

    .prologue
    .line 34
    new-instance v0, Leba;

    invoke-direct {v0}, Leba;-><init>()V

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, -0x1

    .line 71
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 72
    invoke-virtual {p0}, Leba;->n()Lz;

    move-result-object v0

    move-object v1, v0

    .line 77
    :goto_0
    new-instance v2, Landroid/widget/LinearLayout;

    invoke-direct {v2, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 79
    new-instance v3, Lfxi;

    invoke-direct {v3, v1}, Lfxi;-><init>(Landroid/content/Context;)V

    .line 82
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v4, -0x2

    invoke-direct {v0, v5, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v0}, Lfxi;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 84
    invoke-virtual {v3, v6}, Lfxi;->a(Z)V

    .line 85
    invoke-virtual {p0}, Leba;->u_()Lu;

    move-result-object v0

    .line 87
    iget-object v4, p0, Leba;->N:Lfyc;

    check-cast v0, Lfxf;

    invoke-virtual {v3, v4, v0, v6}, Lfxi;->a(Lfyc;Lfxf;Z)V

    .line 89
    new-instance v0, Landroid/widget/ScrollView;

    invoke-direct {v0, v1}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    .line 90
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v5, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 92
    invoke-virtual {v0, v3}, Landroid/widget/ScrollView;->addView(Landroid/view/View;)V

    .line 94
    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 95
    const v0, 0x106000b

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 99
    return-object v2

    .line 74
    :cond_0
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Leba;->n()Lz;

    move-result-object v1

    const v2, 0x103000b

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    move-object v1, v0

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 48
    invoke-super {p0, p1}, Lt;->a(Landroid/os/Bundle;)V

    .line 50
    const/4 v0, 0x1

    invoke-virtual {p0}, Leba;->j_()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Leba;->a(II)V

    .line 52
    if-eqz p1, :cond_0

    .line 53
    new-instance v0, Lfyc;

    invoke-direct {v0}, Lfyc;-><init>()V

    iput-object v0, p0, Leba;->N:Lfyc;

    .line 54
    iget-object v0, p0, Leba;->N:Lfyc;

    const-string v1, "eventupdate"

    invoke-virtual {v0, p1, v1}, Lfyc;->b(Landroid/os/Bundle;Ljava/lang/String;)V

    .line 56
    :cond_0
    return-void
.end method

.method public a(Lfyc;)V
    .locals 0

    .prologue
    .line 43
    iput-object p1, p0, Leba;->N:Lfyc;

    .line 44
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 60
    iget-object v0, p0, Leba;->N:Lfyc;

    const-string v1, "eventupdate"

    invoke-virtual {v0, p1, v1}, Lfyc;->a(Landroid/os/Bundle;Ljava/lang/String;)V

    .line 62
    invoke-super {p0, p1}, Lt;->e(Landroid/os/Bundle;)V

    .line 63
    return-void
.end method
