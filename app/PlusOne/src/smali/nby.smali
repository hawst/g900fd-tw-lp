.class public final Lnby;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lota;

.field public b:I

.field private c:Lnbz;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 113
    invoke-direct {p0}, Loxq;-><init>()V

    .line 213
    iput-object v1, p0, Lnby;->a:Lota;

    .line 216
    const/high16 v0, -0x80000000

    iput v0, p0, Lnby;->b:I

    .line 219
    iput-object v1, p0, Lnby;->c:Lnbz;

    .line 113
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 244
    const/4 v0, 0x0

    .line 245
    iget-object v1, p0, Lnby;->a:Lota;

    if-eqz v1, :cond_0

    .line 246
    const/4 v0, 0x1

    iget-object v1, p0, Lnby;->a:Lota;

    .line 247
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 249
    :cond_0
    iget v1, p0, Lnby;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_1

    .line 250
    const/4 v1, 0x2

    iget v2, p0, Lnby;->b:I

    .line 251
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 253
    :cond_1
    iget-object v1, p0, Lnby;->c:Lnbz;

    if-eqz v1, :cond_2

    .line 254
    const/4 v1, 0x3

    iget-object v2, p0, Lnby;->c:Lnbz;

    .line 255
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 257
    :cond_2
    iget-object v1, p0, Lnby;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 258
    const/4 v1, 0x4

    iget-object v2, p0, Lnby;->d:Ljava/lang/String;

    .line 259
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 261
    :cond_3
    iget-object v1, p0, Lnby;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 262
    iput v0, p0, Lnby;->ai:I

    .line 263
    return v0
.end method

.method public a(Loxn;)Lnby;
    .locals 2

    .prologue
    .line 271
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 272
    sparse-switch v0, :sswitch_data_0

    .line 276
    iget-object v1, p0, Lnby;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 277
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnby;->ah:Ljava/util/List;

    .line 280
    :cond_1
    iget-object v1, p0, Lnby;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 282
    :sswitch_0
    return-object p0

    .line 287
    :sswitch_1
    iget-object v0, p0, Lnby;->a:Lota;

    if-nez v0, :cond_2

    .line 288
    new-instance v0, Lota;

    invoke-direct {v0}, Lota;-><init>()V

    iput-object v0, p0, Lnby;->a:Lota;

    .line 290
    :cond_2
    iget-object v0, p0, Lnby;->a:Lota;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 294
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 295
    if-eqz v0, :cond_3

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    .line 298
    :cond_3
    iput v0, p0, Lnby;->b:I

    goto :goto_0

    .line 300
    :cond_4
    const/4 v0, 0x0

    iput v0, p0, Lnby;->b:I

    goto :goto_0

    .line 305
    :sswitch_3
    iget-object v0, p0, Lnby;->c:Lnbz;

    if-nez v0, :cond_5

    .line 306
    new-instance v0, Lnbz;

    invoke-direct {v0}, Lnbz;-><init>()V

    iput-object v0, p0, Lnby;->c:Lnbz;

    .line 308
    :cond_5
    iget-object v0, p0, Lnby;->c:Lnbz;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 312
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnby;->d:Ljava/lang/String;

    goto :goto_0

    .line 272
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 226
    iget-object v0, p0, Lnby;->a:Lota;

    if-eqz v0, :cond_0

    .line 227
    const/4 v0, 0x1

    iget-object v1, p0, Lnby;->a:Lota;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 229
    :cond_0
    iget v0, p0, Lnby;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 230
    const/4 v0, 0x2

    iget v1, p0, Lnby;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 232
    :cond_1
    iget-object v0, p0, Lnby;->c:Lnbz;

    if-eqz v0, :cond_2

    .line 233
    const/4 v0, 0x3

    iget-object v1, p0, Lnby;->c:Lnbz;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 235
    :cond_2
    iget-object v0, p0, Lnby;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 236
    const/4 v0, 0x4

    iget-object v1, p0, Lnby;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 238
    :cond_3
    iget-object v0, p0, Lnby;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 240
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 109
    invoke-virtual {p0, p1}, Lnby;->a(Loxn;)Lnby;

    move-result-object v0

    return-object v0
.end method
