.class public final Lmkh;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnuq;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 5668
    invoke-direct {p0}, Loxq;-><init>()V

    .line 5671
    iput-object v0, p0, Lmkh;->apiHeader:Llyr;

    .line 5674
    iput-object v0, p0, Lmkh;->a:Lnuq;

    .line 5668
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 5691
    const/4 v0, 0x0

    .line 5692
    iget-object v1, p0, Lmkh;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 5693
    const/4 v0, 0x1

    iget-object v1, p0, Lmkh;->apiHeader:Llyr;

    .line 5694
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 5696
    :cond_0
    iget-object v1, p0, Lmkh;->a:Lnuq;

    if-eqz v1, :cond_1

    .line 5697
    const/4 v1, 0x2

    iget-object v2, p0, Lmkh;->a:Lnuq;

    .line 5698
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5700
    :cond_1
    iget-object v1, p0, Lmkh;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5701
    iput v0, p0, Lmkh;->ai:I

    .line 5702
    return v0
.end method

.method public a(Loxn;)Lmkh;
    .locals 2

    .prologue
    .line 5710
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 5711
    sparse-switch v0, :sswitch_data_0

    .line 5715
    iget-object v1, p0, Lmkh;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 5716
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmkh;->ah:Ljava/util/List;

    .line 5719
    :cond_1
    iget-object v1, p0, Lmkh;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5721
    :sswitch_0
    return-object p0

    .line 5726
    :sswitch_1
    iget-object v0, p0, Lmkh;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 5727
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmkh;->apiHeader:Llyr;

    .line 5729
    :cond_2
    iget-object v0, p0, Lmkh;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 5733
    :sswitch_2
    iget-object v0, p0, Lmkh;->a:Lnuq;

    if-nez v0, :cond_3

    .line 5734
    new-instance v0, Lnuq;

    invoke-direct {v0}, Lnuq;-><init>()V

    iput-object v0, p0, Lmkh;->a:Lnuq;

    .line 5736
    :cond_3
    iget-object v0, p0, Lmkh;->a:Lnuq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 5711
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 5679
    iget-object v0, p0, Lmkh;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 5680
    const/4 v0, 0x1

    iget-object v1, p0, Lmkh;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 5682
    :cond_0
    iget-object v0, p0, Lmkh;->a:Lnuq;

    if-eqz v0, :cond_1

    .line 5683
    const/4 v0, 0x2

    iget-object v1, p0, Lmkh;->a:Lnuq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 5685
    :cond_1
    iget-object v0, p0, Lmkh;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 5687
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5664
    invoke-virtual {p0, p1}, Lmkh;->a(Loxn;)Lmkh;

    move-result-object v0

    return-object v0
.end method
