.class public final Lfnr;
.super Lhny;
.source "PG"


# instance fields
.field private final a:I

.field private final b:Ljava/lang/String;

.field private final c:Z

.field private final d:Z

.field private final e:Lmol;

.field private final f:[Lmmr;

.field private final h:[Landroid/graphics/Point;

.field private final i:Lkfd;

.field private final j:Lhei;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;ZZLmol;[Lmmr;[Landroid/graphics/Point;)V
    .locals 1

    .prologue
    .line 42
    const-string v0, "EditStoryTask"

    invoke-direct {p0, p1, v0}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 43
    iput p2, p0, Lfnr;->a:I

    .line 44
    iput-object p3, p0, Lfnr;->b:Ljava/lang/String;

    .line 45
    iput-boolean p4, p0, Lfnr;->c:Z

    .line 46
    iput-boolean p5, p0, Lfnr;->d:Z

    .line 47
    iput-object p6, p0, Lfnr;->e:Lmol;

    .line 48
    iput-object p7, p0, Lfnr;->f:[Lmmr;

    .line 49
    iput-object p8, p0, Lfnr;->h:[Landroid/graphics/Point;

    .line 50
    const-class v0, Lkfd;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkfd;

    iput-object v0, p0, Lfnr;->i:Lkfd;

    .line 51
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Lfnr;->j:Lhei;

    .line 52
    return-void
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 12

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v11, 0x0

    .line 56
    invoke-virtual {p0}, Lfnr;->f()Landroid/content/Context;

    move-result-object v1

    .line 57
    new-instance v2, Lkfo;

    iget v0, p0, Lfnr;->a:I

    invoke-direct {v2, v1, v0}, Lkfo;-><init>(Landroid/content/Context;I)V

    .line 58
    new-instance v0, Lfnk;

    iget-object v3, p0, Lfnr;->b:Ljava/lang/String;

    iget-boolean v4, p0, Lfnr;->c:Z

    iget-boolean v5, p0, Lfnr;->d:Z

    iget-object v6, p0, Lfnr;->e:Lmol;

    iget-object v7, p0, Lfnr;->f:[Lmmr;

    iget-object v8, p0, Lfnr;->h:[Landroid/graphics/Point;

    invoke-direct/range {v0 .. v8}, Lfnk;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;ZZLmol;[Lmmr;[Landroid/graphics/Point;)V

    .line 60
    iget-object v2, p0, Lfnr;->i:Lkfd;

    invoke-interface {v2, v0}, Lkfd;->a(Lkff;)V

    .line 61
    invoke-virtual {v0}, Lfnk;->i()Lman;

    move-result-object v3

    .line 62
    invoke-virtual {v0}, Lfnk;->t()Z

    move-result v2

    if-nez v2, :cond_0

    if-eqz v3, :cond_0

    iget-object v2, v3, Lman;->a:Lmln;

    if-eqz v2, :cond_0

    iget-object v2, v3, Lman;->a:Lmln;

    iget-object v2, v2, Lmln;->a:Lmmq;

    if-nez v2, :cond_1

    .line 65
    :cond_0
    new-instance v1, Lhoz;

    iget v2, v0, Lkff;->i:I

    iget-object v0, v0, Lkff;->k:Ljava/lang/Exception;

    invoke-direct {v1, v2, v0, v11}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    .line 66
    invoke-virtual {v1}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "edit_succeeded"

    invoke-virtual {v0, v2, v9}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    move-object v0, v1

    .line 95
    :goto_0
    return-object v0

    .line 70
    :cond_1
    iget-object v4, p0, Lfnr;->f:[Lmmr;

    if-eqz v4, :cond_5

    array-length v5, v4

    move v2, v9

    :goto_1
    if-ge v2, v5, :cond_5

    aget-object v6, v4, v2

    iget v7, v6, Lmmr;->b:I

    if-eq v7, v10, :cond_2

    iget v7, v6, Lmmr;->b:I

    const/4 v8, 0x2

    if-eq v7, v8, :cond_2

    iget v7, v6, Lmmr;->b:I

    const/4 v8, 0x5

    if-eq v7, v8, :cond_2

    iget v6, v6, Lmmr;->b:I

    const/16 v7, 0xe

    if-ne v6, v7, :cond_4

    :cond_2
    move v2, v10

    :goto_2
    if-eqz v2, :cond_3

    .line 73
    invoke-virtual {p0}, Lfnr;->f()Landroid/content/Context;

    move-result-object v2

    new-instance v4, Lfnt;

    iget v5, p0, Lfnr;->a:I

    invoke-direct {v4, v1, v5, v11}, Lfnt;-><init>(Landroid/content/Context;ILjava/lang/String;)V

    invoke-static {v2, v4}, Lhoc;->a(Landroid/content/Context;Lhny;)V

    .line 77
    :cond_3
    iget v2, p0, Lfnr;->a:I

    iget-object v3, v3, Lman;->a:Lmln;

    iget-object v3, v3, Lmln;->a:Lmmq;

    iget-object v4, p0, Lfnr;->f:[Lmmr;

    invoke-static {v1, v2, v3, v4}, Lfoa;->a(Landroid/content/Context;ILmmq;[Lmmr;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 79
    new-instance v1, Lhoz;

    iget v2, v0, Lkff;->i:I

    iget-object v0, v0, Lkff;->k:Ljava/lang/Exception;

    invoke-direct {v1, v2, v0, v11}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    .line 80
    invoke-virtual {v1}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "edit_succeeded"

    invoke-virtual {v0, v2, v9}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    move-object v0, v1

    .line 81
    goto :goto_0

    .line 70
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_5
    move v2, v9

    goto :goto_2

    .line 86
    :cond_6
    iget-object v0, p0, Lfnr;->j:Lhei;

    iget v2, p0, Lfnr;->a:I

    invoke-interface {v0, v2}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v2, "account_name"

    invoke-interface {v0, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 87
    new-instance v0, Lfnm;

    new-instance v2, Lkfo;

    invoke-direct {v2, v3, v11}, Lkfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lfnr;->b:Ljava/lang/String;

    iget-object v8, p0, Lfnr;->h:[Landroid/graphics/Point;

    move-object v4, v11

    move-object v5, v11

    move v6, v10

    move v7, v9

    move v9, v10

    invoke-direct/range {v0 .. v9}, Lfnm;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ[Landroid/graphics/Point;Z)V

    .line 89
    iget-object v2, p0, Lfnr;->i:Lkfd;

    invoke-interface {v2, v0}, Lkfd;->a(Lkff;)V

    .line 90
    invoke-virtual {v0}, Lfnm;->t()Z

    move-result v2

    if-nez v2, :cond_7

    .line 91
    iget v3, p0, Lfnr;->a:I

    invoke-virtual {v0}, Lfnm;->D()Loxu;

    move-result-object v2

    check-cast v2, Lmdh;

    iget-object v2, v2, Lmdh;->a:Lmlr;

    iget-object v2, v2, Lmlr;->a:Lmmq;

    invoke-static {v1, v3, v2}, Lfoa;->a(Landroid/content/Context;ILmmq;)V

    .line 93
    :cond_7
    new-instance v1, Lhoz;

    iget v2, v0, Lkff;->i:I

    iget-object v0, v0, Lkff;->k:Ljava/lang/Exception;

    invoke-direct {v1, v2, v0, v11}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    .line 94
    invoke-virtual {v1}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "edit_succeeded"

    invoke-virtual {v0, v2, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    move-object v0, v1

    .line 95
    goto/16 :goto_0
.end method
