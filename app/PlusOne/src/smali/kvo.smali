.class public final Lkvo;
.super Lkgg;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkgg",
        "<",
        "Lmai;",
        "Lmaj;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private final p:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lkfo;Ljava/lang/String;I)V
    .locals 6

    .prologue
    .line 34
    const-string v3, "editsquaremembership"

    new-instance v4, Lmai;

    invoke-direct {v4}, Lmai;-><init>()V

    new-instance v5, Lmaj;

    invoke-direct {v5}, Lmaj;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lkgg;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Loxu;Loxu;)V

    .line 37
    iput-object p3, p0, Lkvo;->a:Ljava/lang/String;

    .line 38
    iput p4, p0, Lkvo;->p:I

    .line 39
    return-void
.end method


# virtual methods
.method protected a(Lmai;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 55
    new-instance v0, Lnsz;

    invoke-direct {v0}, Lnsz;-><init>()V

    iput-object v0, p1, Lmai;->a:Lnsz;

    .line 56
    iget-object v0, p1, Lmai;->a:Lnsz;

    iget-object v1, p0, Lkvo;->a:Ljava/lang/String;

    iput-object v1, v0, Lnsz;->a:Ljava/lang/String;

    .line 58
    iget-object v0, p0, Lkvo;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 59
    iget-object v0, p1, Lmai;->a:Lnsz;

    new-array v1, v2, [Ljava/lang/String;

    iget-object v2, p0, Lkvo;->b:Ljava/lang/String;

    aput-object v2, v1, v3

    iput-object v1, v0, Lnsz;->c:[Ljava/lang/String;

    .line 64
    :cond_0
    :goto_0
    iget-object v0, p1, Lmai;->a:Lnsz;

    iget v1, p0, Lkvo;->p:I

    iput v1, v0, Lnsz;->b:I

    .line 65
    return-void

    .line 60
    :cond_1
    iget-object v0, p0, Lkvo;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p1, Lmai;->a:Lnsz;

    new-array v1, v2, [Lnsa;

    new-instance v2, Lnsa;

    invoke-direct {v2}, Lnsa;-><init>()V

    aput-object v2, v1, v3

    iput-object v1, v0, Lnsz;->d:[Lnsa;

    .line 62
    iget-object v0, p1, Lmai;->a:Lnsz;

    iget-object v0, v0, Lnsz;->d:[Lnsa;

    aget-object v0, v0, v3

    iget-object v1, p0, Lkvo;->c:Ljava/lang/String;

    iput-object v1, v0, Lnsa;->b:Ljava/lang/String;

    goto :goto_0
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 17
    check-cast p1, Lmai;

    invoke-virtual {p0, p1}, Lkvo;->a(Lmai;)V

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lkvo;->b:Ljava/lang/String;

    .line 47
    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lkvo;->c:Ljava/lang/String;

    .line 51
    return-void
.end method
