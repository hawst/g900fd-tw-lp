.class public final Lmwe;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Boolean;

.field public b:Ljava/lang/Boolean;

.field public c:Ljava/lang/Boolean;

.field private d:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12651
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 12682
    const/4 v0, 0x0

    .line 12683
    iget-object v1, p0, Lmwe;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 12684
    const/4 v0, 0x1

    iget-object v1, p0, Lmwe;->a:Ljava/lang/Boolean;

    .line 12685
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 12687
    :cond_0
    iget-object v1, p0, Lmwe;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 12688
    const/4 v1, 0x2

    iget-object v2, p0, Lmwe;->b:Ljava/lang/Boolean;

    .line 12689
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 12691
    :cond_1
    iget-object v1, p0, Lmwe;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 12692
    const/4 v1, 0x3

    iget-object v2, p0, Lmwe;->d:Ljava/lang/Boolean;

    .line 12693
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 12695
    :cond_2
    iget-object v1, p0, Lmwe;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 12696
    const/4 v1, 0x4

    iget-object v2, p0, Lmwe;->c:Ljava/lang/Boolean;

    .line 12697
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 12699
    :cond_3
    iget-object v1, p0, Lmwe;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12700
    iput v0, p0, Lmwe;->ai:I

    .line 12701
    return v0
.end method

.method public a(Loxn;)Lmwe;
    .locals 2

    .prologue
    .line 12709
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 12710
    sparse-switch v0, :sswitch_data_0

    .line 12714
    iget-object v1, p0, Lmwe;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 12715
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmwe;->ah:Ljava/util/List;

    .line 12718
    :cond_1
    iget-object v1, p0, Lmwe;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 12720
    :sswitch_0
    return-object p0

    .line 12725
    :sswitch_1
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmwe;->a:Ljava/lang/Boolean;

    goto :goto_0

    .line 12729
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmwe;->b:Ljava/lang/Boolean;

    goto :goto_0

    .line 12733
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmwe;->d:Ljava/lang/Boolean;

    goto :goto_0

    .line 12737
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmwe;->c:Ljava/lang/Boolean;

    goto :goto_0

    .line 12710
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 12664
    iget-object v0, p0, Lmwe;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 12665
    const/4 v0, 0x1

    iget-object v1, p0, Lmwe;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 12667
    :cond_0
    iget-object v0, p0, Lmwe;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 12668
    const/4 v0, 0x2

    iget-object v1, p0, Lmwe;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 12670
    :cond_1
    iget-object v0, p0, Lmwe;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 12671
    const/4 v0, 0x3

    iget-object v1, p0, Lmwe;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 12673
    :cond_2
    iget-object v0, p0, Lmwe;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 12674
    const/4 v0, 0x4

    iget-object v1, p0, Lmwe;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 12676
    :cond_3
    iget-object v0, p0, Lmwe;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 12678
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 12647
    invoke-virtual {p0, p1}, Lmwe;->a(Loxn;)Lmwe;

    move-result-object v0

    return-object v0
.end method
