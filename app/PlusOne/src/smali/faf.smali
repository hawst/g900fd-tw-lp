.class public final Lfaf;
.super Lhye;
.source "PG"


# instance fields
.field private final b:I

.field private final c:Landroid/content/Context;

.field private final d:I

.field private final e:Z

.field private final f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;IIZZ)V
    .locals 0

    .prologue
    .line 100
    invoke-direct {p0, p1}, Lhye;-><init>(Landroid/content/Context;)V

    .line 102
    iput-object p1, p0, Lfaf;->c:Landroid/content/Context;

    .line 103
    iput p2, p0, Lfaf;->b:I

    .line 104
    iput p3, p0, Lfaf;->d:I

    .line 105
    iput-boolean p4, p0, Lfaf;->e:Z

    .line 106
    iput-boolean p5, p0, Lfaf;->f:Z

    .line 107
    return-void
.end method

.method private D()Lhym;
    .locals 13

    .prologue
    const/4 v1, 0x2

    const/4 v12, 0x0

    const/4 v11, 0x3

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 110
    new-instance v2, Lhym;

    sget-object v0, Lfag;->a:[Ljava/lang/String;

    invoke-direct {v2, v0}, Lhym;-><init>([Ljava/lang/String;)V

    .line 111
    sget-object v0, Lfag;->a:[Ljava/lang/String;

    const/4 v0, 0x5

    new-array v3, v0, [Ljava/lang/Object;

    .line 114
    invoke-static {v3, v12}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 115
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v10

    .line 116
    iget-object v0, p0, Lfaf;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a07df

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v11

    .line 118
    invoke-virtual {v2, v3}, Lhym;->a([Ljava/lang/Object;)V

    .line 121
    invoke-static {v3, v12}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 122
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v10

    .line 123
    iget-object v0, p0, Lfaf;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a07e0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v11

    .line 125
    invoke-virtual {v2, v3}, Lhym;->a([Ljava/lang/Object;)V

    .line 127
    iget-object v0, p0, Lfaf;->c:Landroid/content/Context;

    const-class v1, Lhei;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iget v1, p0, Lfaf;->b:I

    .line 128
    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 131
    const-string v1, "is_child"

    invoke-interface {v0, v1}, Lhej;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 132
    const/4 v0, 0x5

    move v1, v0

    .line 139
    :goto_0
    const/16 v0, 0x9

    if-ne v1, v0, :cond_0

    iget-boolean v0, p0, Lfaf;->f:Z

    if-eqz v0, :cond_4

    .line 140
    :cond_0
    invoke-direct {p0}, Lfaf;->F()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhxc;

    .line 141
    invoke-virtual {v0}, Lhxc;->c()I

    move-result v5

    .line 142
    if-ne v5, v1, :cond_1

    .line 143
    invoke-static {v3, v12}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 149
    :try_start_0
    new-instance v6, Lhgw;

    invoke-direct {v6, v0}, Lhgw;-><init>(Lhxc;)V

    .line 150
    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v3, v7

    .line 151
    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v3, v7

    .line 152
    const/4 v7, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v7

    .line 153
    const/4 v5, 0x3

    invoke-virtual {v0}, Lhxc;->b()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v5

    .line 154
    const/4 v0, 0x4

    invoke-static {v6}, Lhhg;->a(Lhgw;)[B

    move-result-object v5

    aput-object v5, v3, v0

    .line 155
    invoke-virtual {v2, v3}, Lhym;->a([Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 159
    :catch_0
    move-exception v0

    goto :goto_1

    .line 133
    :cond_2
    const-string v1, "is_default_restricted"

    invoke-interface {v0, v1}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 134
    const/16 v0, 0x8

    move v1, v0

    goto :goto_0

    .line 136
    :cond_3
    const/16 v0, 0x9

    move v1, v0

    goto :goto_0

    .line 163
    :cond_4
    iget-object v0, p0, Lfaf;->c:Landroid/content/Context;

    const-class v1, Lhhh;

    invoke-static {v0, v1}, Llnh;->c(Landroid/content/Context;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_5
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhhh;

    .line 164
    iget-object v4, p0, Lfaf;->c:Landroid/content/Context;

    iget v4, p0, Lfaf;->b:I

    invoke-virtual {v0}, Lhhh;->f()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 165
    invoke-static {v3, v12}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 168
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v9

    .line 169
    invoke-virtual {v0}, Lhhh;->a()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v10

    .line 170
    iget-object v4, p0, Lfaf;->c:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v0}, Lhhh;->b()I

    move-result v0

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v11

    .line 171
    invoke-virtual {v2, v3}, Lhym;->a([Ljava/lang/Object;)V

    goto :goto_2

    .line 175
    :cond_6
    return-object v2
.end method

.method private E()Landroid/database/Cursor;
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 179
    iget-object v0, p0, Lfaf;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->a:Landroid/net/Uri;

    iget v2, p0, Lfaf;->b:I

    .line 180
    invoke-static {v1, v2}, Lcom/google/android/apps/plus/content/EsProvider;->a(Landroid/net/Uri;I)Landroid/net/Uri;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/String;

    const-string v4, "audience_history"

    aput-object v4, v2, v6

    move-object v4, v3

    move-object v5, v3

    .line 179
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 185
    :try_start_0
    new-instance v3, Lhym;

    sget-object v0, Lfai;->a:[Ljava/lang/String;

    invoke-direct {v3, v0}, Lhym;-><init>([Ljava/lang/String;)V

    .line 186
    if-eqz v2, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 187
    const/4 v0, 0x0

    .line 188
    invoke-interface {v2, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Lhhg;->a([B)Ljava/util/ArrayList;

    move-result-object v4

    .line 189
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x3

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    move v1, v6

    .line 190
    :goto_0
    if-ge v1, v5, :cond_0

    .line 192
    const/4 v0, 0x2

    :try_start_1
    new-array v6, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    const/4 v7, 0x1

    .line 193
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v0

    const/4 v7, 0x1

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhgw;

    invoke-static {v0}, Lhhg;->a(Lhgw;)[B

    move-result-object v0

    aput-object v0, v6, v7

    .line 192
    invoke-virtual {v3, v6}, Lhym;->a([Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 190
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 201
    :cond_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    return-object v3

    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method private F()Ljava/util/ArrayList;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lhxc;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v12, 0x8

    const/4 v11, 0x5

    const/16 v10, 0x9

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 233
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 234
    const/4 v0, 0x0

    .line 237
    :try_start_0
    invoke-virtual {p0}, Lfaf;->n()Landroid/content/Context;

    move-result-object v1

    iget v5, p0, Lfaf;->b:I

    const/16 v6, 0xe

    sget-object v7, Lfah;->a:[Ljava/lang/String;

    invoke-static {v1, v5, v6, v7}, Ldsm;->a(Landroid/content/Context;II[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 240
    if-eqz v0, :cond_3

    :try_start_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 242
    :cond_0
    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 243
    const/4 v1, 0x2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 244
    const/4 v1, 0x3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 245
    const/4 v1, 0x4

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 247
    iget-boolean v1, p0, Lfaf;->e:Z

    iget v9, p0, Lfaf;->d:I

    if-eqz v1, :cond_5

    if-ne v7, v10, :cond_5

    move v1, v2

    :goto_0
    if-eqz v1, :cond_2

    .line 248
    if-eq v7, v10, :cond_1

    if-eq v7, v11, :cond_1

    if-ne v7, v12, :cond_a

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    :cond_1
    move v1, v3

    :goto_1
    if-eqz v1, :cond_2

    .line 249
    new-instance v1, Lhxc;

    invoke-direct {v1, v6, v7, v5, v8}, Lhxc;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 251
    :cond_2
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v1

    if-nez v1, :cond_0

    .line 256
    :cond_3
    if-eqz v0, :cond_4

    .line 257
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 261
    :cond_4
    :goto_2
    return-object v4

    .line 247
    :cond_5
    sparse-switch v9, :sswitch_data_0

    :cond_6
    move v1, v3

    goto :goto_0

    :sswitch_0
    if-eq v7, v10, :cond_7

    if-ne v7, v12, :cond_6

    :cond_7
    move v1, v2

    goto :goto_0

    :sswitch_1
    if-eq v7, v3, :cond_8

    if-ne v7, v11, :cond_9

    :cond_8
    move v1, v3

    goto :goto_0

    :cond_9
    move v1, v2

    goto :goto_0

    :cond_a
    move v1, v2

    .line 248
    goto :goto_1

    .line 256
    :catch_0
    move-exception v1

    if-eqz v0, :cond_4

    .line 257
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 256
    :catchall_0
    move-exception v1

    move-object v13, v1

    move-object v1, v0

    move-object v0, v13

    :goto_3
    if-eqz v1, :cond_b

    .line 257
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_b
    throw v0

    .line 256
    :catchall_1
    move-exception v1

    move-object v13, v1

    move-object v1, v0

    move-object v0, v13

    goto :goto_3

    .line 247
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_0
        0x10 -> :sswitch_1
    .end sparse-switch
.end method


# virtual methods
.method public C()Landroid/database/Cursor;
    .locals 4

    .prologue
    .line 266
    new-instance v0, Landroid/database/MergeCursor;

    const/4 v1, 0x2

    new-array v1, v1, [Landroid/database/Cursor;

    const/4 v2, 0x0

    invoke-direct {p0}, Lfaf;->D()Lhym;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-direct {p0}, Lfaf;->E()Landroid/database/Cursor;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    return-object v0
.end method
