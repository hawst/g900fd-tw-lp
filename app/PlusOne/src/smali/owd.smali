.class public final Lowd;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Loso;

.field private c:I

.field private d:Lowe;

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 24
    iput-object v1, p0, Lowd;->b:Loso;

    .line 27
    const/high16 v0, -0x80000000

    iput v0, p0, Lowd;->c:I

    .line 30
    iput-object v1, p0, Lowd;->d:Lowe;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 58
    const/4 v0, 0x0

    .line 59
    iget-object v1, p0, Lowd;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 60
    const/4 v0, 0x1

    iget-object v1, p0, Lowd;->a:Ljava/lang/String;

    .line 61
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 63
    :cond_0
    iget-object v1, p0, Lowd;->b:Loso;

    if-eqz v1, :cond_1

    .line 64
    const/4 v1, 0x2

    iget-object v2, p0, Lowd;->b:Loso;

    .line 65
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 67
    :cond_1
    iget v1, p0, Lowd;->c:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_2

    .line 68
    const/4 v1, 0x3

    iget v2, p0, Lowd;->c:I

    .line 69
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 71
    :cond_2
    iget-object v1, p0, Lowd;->d:Lowe;

    if-eqz v1, :cond_3

    .line 72
    const/4 v1, 0x4

    iget-object v2, p0, Lowd;->d:Lowe;

    .line 73
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 75
    :cond_3
    iget-object v1, p0, Lowd;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 76
    const/4 v1, 0x5

    iget-object v2, p0, Lowd;->e:Ljava/lang/String;

    .line 77
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 79
    :cond_4
    iget-object v1, p0, Lowd;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 80
    iput v0, p0, Lowd;->ai:I

    .line 81
    return v0
.end method

.method public a(Loxn;)Lowd;
    .locals 2

    .prologue
    .line 89
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 90
    sparse-switch v0, :sswitch_data_0

    .line 94
    iget-object v1, p0, Lowd;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 95
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lowd;->ah:Ljava/util/List;

    .line 98
    :cond_1
    iget-object v1, p0, Lowd;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 100
    :sswitch_0
    return-object p0

    .line 105
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lowd;->a:Ljava/lang/String;

    goto :goto_0

    .line 109
    :sswitch_2
    iget-object v0, p0, Lowd;->b:Loso;

    if-nez v0, :cond_2

    .line 110
    new-instance v0, Loso;

    invoke-direct {v0}, Loso;-><init>()V

    iput-object v0, p0, Lowd;->b:Loso;

    .line 112
    :cond_2
    iget-object v0, p0, Lowd;->b:Loso;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 116
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 117
    if-eqz v0, :cond_3

    const/4 v1, 0x5

    if-eq v0, v1, :cond_3

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    const/4 v1, 0x4

    if-ne v0, v1, :cond_4

    .line 123
    :cond_3
    iput v0, p0, Lowd;->c:I

    goto :goto_0

    .line 125
    :cond_4
    const/4 v0, 0x0

    iput v0, p0, Lowd;->c:I

    goto :goto_0

    .line 130
    :sswitch_4
    iget-object v0, p0, Lowd;->d:Lowe;

    if-nez v0, :cond_5

    .line 131
    new-instance v0, Lowe;

    invoke-direct {v0}, Lowe;-><init>()V

    iput-object v0, p0, Lowd;->d:Lowe;

    .line 133
    :cond_5
    iget-object v0, p0, Lowd;->d:Lowe;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 137
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lowd;->e:Ljava/lang/String;

    goto :goto_0

    .line 90
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, Lowd;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 38
    const/4 v0, 0x1

    iget-object v1, p0, Lowd;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 40
    :cond_0
    iget-object v0, p0, Lowd;->b:Loso;

    if-eqz v0, :cond_1

    .line 41
    const/4 v0, 0x2

    iget-object v1, p0, Lowd;->b:Loso;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 43
    :cond_1
    iget v0, p0, Lowd;->c:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_2

    .line 44
    const/4 v0, 0x3

    iget v1, p0, Lowd;->c:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 46
    :cond_2
    iget-object v0, p0, Lowd;->d:Lowe;

    if-eqz v0, :cond_3

    .line 47
    const/4 v0, 0x4

    iget-object v1, p0, Lowd;->d:Lowe;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 49
    :cond_3
    iget-object v0, p0, Lowd;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 50
    const/4 v0, 0x5

    iget-object v1, p0, Lowd;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 52
    :cond_4
    iget-object v0, p0, Lowd;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 54
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lowd;->a(Loxn;)Lowd;

    move-result-object v0

    return-object v0
.end method
