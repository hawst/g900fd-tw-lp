.class public final Lndd;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lndd;


# instance fields
.field public b:I

.field public c:Lndc;

.field public d:Ljava/lang/Integer;

.field public e:Lndc;

.field public f:Ljava/lang/Integer;

.field public g:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 169
    const/4 v0, 0x0

    new-array v0, v0, [Lndd;

    sput-object v0, Lndd;->a:[Lndd;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 170
    invoke-direct {p0}, Loxq;-><init>()V

    .line 186
    const/high16 v0, -0x80000000

    iput v0, p0, Lndd;->b:I

    .line 189
    iput-object v1, p0, Lndd;->c:Lndc;

    .line 194
    iput-object v1, p0, Lndd;->e:Lndc;

    .line 170
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 227
    const/4 v0, 0x0

    .line 228
    iget v1, p0, Lndd;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 229
    const/4 v0, 0x1

    iget v1, p0, Lndd;->b:I

    .line 230
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 232
    :cond_0
    iget-object v1, p0, Lndd;->c:Lndc;

    if-eqz v1, :cond_1

    .line 233
    const/4 v1, 0x2

    iget-object v2, p0, Lndd;->c:Lndc;

    .line 234
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 236
    :cond_1
    iget-object v1, p0, Lndd;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 237
    const/4 v1, 0x3

    iget-object v2, p0, Lndd;->d:Ljava/lang/Integer;

    .line 238
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 240
    :cond_2
    iget-object v1, p0, Lndd;->e:Lndc;

    if-eqz v1, :cond_3

    .line 241
    const/4 v1, 0x4

    iget-object v2, p0, Lndd;->e:Lndc;

    .line 242
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 244
    :cond_3
    iget-object v1, p0, Lndd;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 245
    const/4 v1, 0x5

    iget-object v2, p0, Lndd;->f:Ljava/lang/Integer;

    .line 246
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 248
    :cond_4
    iget-object v1, p0, Lndd;->g:Ljava/lang/Long;

    if-eqz v1, :cond_5

    .line 249
    const/4 v1, 0x6

    iget-object v2, p0, Lndd;->g:Ljava/lang/Long;

    .line 250
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 252
    :cond_5
    iget-object v1, p0, Lndd;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 253
    iput v0, p0, Lndd;->ai:I

    .line 254
    return v0
.end method

.method public a(Loxn;)Lndd;
    .locals 2

    .prologue
    .line 262
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 263
    sparse-switch v0, :sswitch_data_0

    .line 267
    iget-object v1, p0, Lndd;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 268
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lndd;->ah:Ljava/util/List;

    .line 271
    :cond_1
    iget-object v1, p0, Lndd;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 273
    :sswitch_0
    return-object p0

    .line 278
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 279
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9

    if-ne v0, v1, :cond_3

    .line 289
    :cond_2
    iput v0, p0, Lndd;->b:I

    goto :goto_0

    .line 291
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lndd;->b:I

    goto :goto_0

    .line 296
    :sswitch_2
    iget-object v0, p0, Lndd;->c:Lndc;

    if-nez v0, :cond_4

    .line 297
    new-instance v0, Lndc;

    invoke-direct {v0}, Lndc;-><init>()V

    iput-object v0, p0, Lndd;->c:Lndc;

    .line 299
    :cond_4
    iget-object v0, p0, Lndd;->c:Lndc;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 303
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lndd;->d:Ljava/lang/Integer;

    goto :goto_0

    .line 307
    :sswitch_4
    iget-object v0, p0, Lndd;->e:Lndc;

    if-nez v0, :cond_5

    .line 308
    new-instance v0, Lndc;

    invoke-direct {v0}, Lndc;-><init>()V

    iput-object v0, p0, Lndd;->e:Lndc;

    .line 310
    :cond_5
    iget-object v0, p0, Lndd;->e:Lndc;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 314
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lndd;->f:Ljava/lang/Integer;

    goto :goto_0

    .line 318
    :sswitch_6
    invoke-virtual {p1}, Loxn;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lndd;->g:Ljava/lang/Long;

    goto/16 :goto_0

    .line 263
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 203
    iget v0, p0, Lndd;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 204
    const/4 v0, 0x1

    iget v1, p0, Lndd;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 206
    :cond_0
    iget-object v0, p0, Lndd;->c:Lndc;

    if-eqz v0, :cond_1

    .line 207
    const/4 v0, 0x2

    iget-object v1, p0, Lndd;->c:Lndc;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 209
    :cond_1
    iget-object v0, p0, Lndd;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 210
    const/4 v0, 0x3

    iget-object v1, p0, Lndd;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 212
    :cond_2
    iget-object v0, p0, Lndd;->e:Lndc;

    if-eqz v0, :cond_3

    .line 213
    const/4 v0, 0x4

    iget-object v1, p0, Lndd;->e:Lndc;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 215
    :cond_3
    iget-object v0, p0, Lndd;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 216
    const/4 v0, 0x5

    iget-object v1, p0, Lndd;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 218
    :cond_4
    iget-object v0, p0, Lndd;->g:Ljava/lang/Long;

    if-eqz v0, :cond_5

    .line 219
    const/4 v0, 0x6

    iget-object v1, p0, Lndd;->g:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(IJ)V

    .line 221
    :cond_5
    iget-object v0, p0, Lndd;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 223
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 166
    invoke-virtual {p0, p1}, Lndd;->a(Loxn;)Lndd;

    move-result-object v0

    return-object v0
.end method
