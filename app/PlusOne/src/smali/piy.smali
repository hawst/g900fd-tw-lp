.class public final Lpiy;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:[Lpix;

.field private c:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 26
    sget-object v0, Lpix;->a:[Lpix;

    iput-object v0, p0, Lpiy;->b:[Lpix;

    .line 29
    const/high16 v0, -0x80000000

    iput v0, p0, Lpiy;->c:I

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 53
    .line 54
    iget v0, p0, Lpiy;->c:I

    const/high16 v2, -0x80000000

    if-eq v0, v2, :cond_3

    .line 55
    const/4 v0, 0x1

    iget v2, p0, Lpiy;->c:I

    .line 56
    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 58
    :goto_0
    iget-object v2, p0, Lpiy;->a:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 59
    const/4 v2, 0x2

    iget-object v3, p0, Lpiy;->a:Ljava/lang/String;

    .line 60
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 62
    :cond_0
    iget-object v2, p0, Lpiy;->b:[Lpix;

    if-eqz v2, :cond_2

    .line 63
    iget-object v2, p0, Lpiy;->b:[Lpix;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 64
    if-eqz v4, :cond_1

    .line 65
    const/4 v5, 0x3

    .line 66
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 63
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 70
    :cond_2
    iget-object v1, p0, Lpiy;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 71
    iput v0, p0, Lpiy;->ai:I

    .line 72
    return v0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lpiy;
    .locals 5

    .prologue
    const/16 v4, 0x190

    const/4 v1, 0x0

    .line 80
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 81
    sparse-switch v0, :sswitch_data_0

    .line 85
    iget-object v2, p0, Lpiy;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 86
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lpiy;->ah:Ljava/util/List;

    .line 89
    :cond_1
    iget-object v2, p0, Lpiy;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 91
    :sswitch_0
    return-object p0

    .line 96
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 97
    if-eq v0, v4, :cond_2

    const/16 v2, 0x193

    if-eq v0, v2, :cond_2

    const/16 v2, 0x194

    if-eq v0, v2, :cond_2

    const/16 v2, 0x199

    if-eq v0, v2, :cond_2

    const/16 v2, 0x19a

    if-eq v0, v2, :cond_2

    const/16 v2, 0x19c

    if-eq v0, v2, :cond_2

    const/16 v2, 0x1f4

    if-eq v0, v2, :cond_2

    const/16 v2, 0x1f7

    if-ne v0, v2, :cond_3

    .line 105
    :cond_2
    iput v0, p0, Lpiy;->c:I

    goto :goto_0

    .line 107
    :cond_3
    iput v4, p0, Lpiy;->c:I

    goto :goto_0

    .line 112
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpiy;->a:Ljava/lang/String;

    goto :goto_0

    .line 116
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 117
    iget-object v0, p0, Lpiy;->b:[Lpix;

    if-nez v0, :cond_5

    move v0, v1

    .line 118
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lpix;

    .line 119
    iget-object v3, p0, Lpiy;->b:[Lpix;

    if-eqz v3, :cond_4

    .line 120
    iget-object v3, p0, Lpiy;->b:[Lpix;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 122
    :cond_4
    iput-object v2, p0, Lpiy;->b:[Lpix;

    .line 123
    :goto_2
    iget-object v2, p0, Lpiy;->b:[Lpix;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 124
    iget-object v2, p0, Lpiy;->b:[Lpix;

    new-instance v3, Lpix;

    invoke-direct {v3}, Lpix;-><init>()V

    aput-object v3, v2, v0

    .line 125
    iget-object v2, p0, Lpiy;->b:[Lpix;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 126
    invoke-virtual {p1}, Loxn;->a()I

    .line 123
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 117
    :cond_5
    iget-object v0, p0, Lpiy;->b:[Lpix;

    array-length v0, v0

    goto :goto_1

    .line 129
    :cond_6
    iget-object v2, p0, Lpiy;->b:[Lpix;

    new-instance v3, Lpix;

    invoke-direct {v3}, Lpix;-><init>()V

    aput-object v3, v2, v0

    .line 130
    iget-object v2, p0, Lpiy;->b:[Lpix;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 81
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 34
    iget v0, p0, Lpiy;->c:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 35
    const/4 v0, 0x1

    iget v1, p0, Lpiy;->c:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 37
    :cond_0
    iget-object v0, p0, Lpiy;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 38
    const/4 v0, 0x2

    iget-object v1, p0, Lpiy;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 40
    :cond_1
    iget-object v0, p0, Lpiy;->b:[Lpix;

    if-eqz v0, :cond_3

    .line 41
    iget-object v1, p0, Lpiy;->b:[Lpix;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 42
    if-eqz v3, :cond_2

    .line 43
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 41
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 47
    :cond_3
    iget-object v0, p0, Lpiy;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 49
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lpiy;->a(Loxn;)Lpiy;

    move-result-object v0

    return-object v0
.end method
