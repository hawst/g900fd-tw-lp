.class public final Lkj;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static c:Lkr;


# instance fields
.field private a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private b:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 596
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 597
    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    .line 598
    new-instance v0, Lkq;

    invoke-direct {v0}, Lkq;-><init>()V

    sput-object v0, Lkj;->c:Lkr;

    .line 608
    :goto_0
    return-void

    .line 599
    :cond_0
    const/16 v1, 0x12

    if-lt v0, v1, :cond_1

    .line 600
    new-instance v0, Lko;

    invoke-direct {v0}, Lko;-><init>()V

    sput-object v0, Lkj;->c:Lkr;

    goto :goto_0

    .line 601
    :cond_1
    const/16 v1, 0x10

    if-lt v0, v1, :cond_2

    .line 602
    new-instance v0, Lkp;

    invoke-direct {v0}, Lkp;-><init>()V

    sput-object v0, Lkj;->c:Lkr;

    goto :goto_0

    .line 603
    :cond_2
    const/16 v1, 0xe

    if-lt v0, v1, :cond_3

    .line 604
    new-instance v0, Lkm;

    invoke-direct {v0}, Lkm;-><init>()V

    sput-object v0, Lkj;->c:Lkr;

    goto :goto_0

    .line 606
    :cond_3
    new-instance v0, Lkk;

    invoke-direct {v0}, Lkk;-><init>()V

    sput-object v0, Lkj;->c:Lkr;

    goto :goto_0
.end method

.method constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const/4 v0, -0x1

    iput v0, p0, Lkj;->b:I

    .line 36
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lkj;->a:Ljava/lang/ref/WeakReference;

    .line 37
    return-void
.end method

.method static synthetic a(Lkj;)I
    .locals 1

    .prologue
    .line 24
    iget v0, p0, Lkj;->b:I

    return v0
.end method

.method static synthetic a(Lkj;I)I
    .locals 0

    .prologue
    .line 24
    iput p1, p0, Lkj;->b:I

    return p1
.end method

.method static synthetic c()Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x0

    return-object v0
.end method

.method static synthetic d()Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x0

    return-object v0
.end method


# virtual methods
.method public a(F)Lkj;
    .locals 2

    .prologue
    .line 640
    iget-object v0, p0, Lkj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    .line 641
    sget-object v1, Lkj;->c:Lkr;

    invoke-interface {v1, p0, v0, p1}, Lkr;->a(Lkj;Landroid/view/View;F)V

    .line 643
    :cond_0
    return-object p0
.end method

.method public a(J)Lkj;
    .locals 3

    .prologue
    .line 623
    iget-object v0, p0, Lkj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    .line 624
    sget-object v1, Lkj;->c:Lkr;

    invoke-interface {v1, v0, p1, p2}, Lkr;->a(Landroid/view/View;J)V

    .line 626
    :cond_0
    return-object p0
.end method

.method public a(Landroid/view/animation/Interpolator;)Lkj;
    .locals 2

    .prologue
    .line 763
    iget-object v0, p0, Lkj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    .line 764
    sget-object v1, Lkj;->c:Lkr;

    invoke-interface {v1, v0, p1}, Lkr;->a(Landroid/view/View;Landroid/view/animation/Interpolator;)V

    .line 766
    :cond_0
    return-object p0
.end method

.method public a(Lky;)Lkj;
    .locals 2

    .prologue
    .line 1195
    iget-object v0, p0, Lkj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1196
    sget-object v1, Lkj;->c:Lkr;

    invoke-interface {v1, p0, v0, p1}, Lkr;->a(Lkj;Landroid/view/View;Lky;)V

    .line 1198
    :cond_0
    return-object p0
.end method

.method public a(Lla;)Lkj;
    .locals 2

    .prologue
    .line 1214
    iget-object v0, p0, Lkj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1215
    sget-object v1, Lkj;->c:Lkr;

    invoke-interface {v1, v0, p1}, Lkr;->a(Landroid/view/View;Lla;)V

    .line 1217
    :cond_0
    return-object p0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 997
    iget-object v0, p0, Lkj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    .line 998
    sget-object v1, Lkj;->c:Lkr;

    invoke-interface {v1, p0, v0}, Lkr;->a(Lkj;Landroid/view/View;)V

    .line 1000
    :cond_0
    return-void
.end method

.method public b(F)Lkj;
    .locals 2

    .prologue
    .line 674
    iget-object v0, p0, Lkj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    .line 675
    sget-object v1, Lkj;->c:Lkr;

    invoke-interface {v1, p0, v0, p1}, Lkr;->b(Lkj;Landroid/view/View;F)V

    .line 677
    :cond_0
    return-object p0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 1115
    iget-object v0, p0, Lkj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1116
    sget-object v1, Lkj;->c:Lkr;

    invoke-interface {v1, p0, v0}, Lkr;->b(Lkj;Landroid/view/View;)V

    .line 1118
    :cond_0
    return-void
.end method

.method public c(F)Lkj;
    .locals 2

    .prologue
    .line 691
    iget-object v0, p0, Lkj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    .line 692
    sget-object v1, Lkj;->c:Lkr;

    invoke-interface {v1, p0, v0, p1}, Lkr;->c(Lkj;Landroid/view/View;F)V

    .line 694
    :cond_0
    return-object p0
.end method

.method public d(F)Lkj;
    .locals 2

    .prologue
    .line 969
    iget-object v0, p0, Lkj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    .line 970
    sget-object v1, Lkj;->c:Lkr;

    invoke-interface {v1, p0, v0, p1}, Lkr;->d(Lkj;Landroid/view/View;F)V

    .line 972
    :cond_0
    return-object p0
.end method
