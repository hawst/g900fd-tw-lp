.class public final Lmwg;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Integer;

.field public d:I

.field private e:Ljava/lang/Boolean;

.field private f:Ljava/lang/Boolean;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 7593
    invoke-direct {p0}, Loxq;-><init>()V

    .line 7610
    const/high16 v0, -0x80000000

    iput v0, p0, Lmwg;->d:I

    .line 7593
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 7653
    const/4 v0, 0x0

    .line 7654
    iget-object v1, p0, Lmwg;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 7655
    const/4 v0, 0x1

    iget-object v1, p0, Lmwg;->a:Ljava/lang/Integer;

    .line 7656
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 7658
    :cond_0
    iget-object v1, p0, Lmwg;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 7659
    const/4 v1, 0x2

    iget-object v2, p0, Lmwg;->b:Ljava/lang/Integer;

    .line 7660
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7662
    :cond_1
    iget-object v1, p0, Lmwg;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 7663
    const/4 v1, 0x3

    iget-object v2, p0, Lmwg;->c:Ljava/lang/Integer;

    .line 7664
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7666
    :cond_2
    iget v1, p0, Lmwg;->d:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_3

    .line 7667
    const/4 v1, 0x4

    iget v2, p0, Lmwg;->d:I

    .line 7668
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7670
    :cond_3
    iget-object v1, p0, Lmwg;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 7671
    const/4 v1, 0x5

    iget-object v2, p0, Lmwg;->e:Ljava/lang/Boolean;

    .line 7672
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 7674
    :cond_4
    iget-object v1, p0, Lmwg;->f:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    .line 7675
    const/4 v1, 0x6

    iget-object v2, p0, Lmwg;->f:Ljava/lang/Boolean;

    .line 7676
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 7678
    :cond_5
    iget-object v1, p0, Lmwg;->g:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 7679
    const/4 v1, 0x7

    iget-object v2, p0, Lmwg;->g:Ljava/lang/String;

    .line 7680
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7682
    :cond_6
    iget-object v1, p0, Lmwg;->h:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 7683
    const/16 v1, 0x8

    iget-object v2, p0, Lmwg;->h:Ljava/lang/String;

    .line 7684
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7686
    :cond_7
    iget-object v1, p0, Lmwg;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7687
    iput v0, p0, Lmwg;->ai:I

    .line 7688
    return v0
.end method

.method public a(Loxn;)Lmwg;
    .locals 2

    .prologue
    .line 7696
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 7697
    sparse-switch v0, :sswitch_data_0

    .line 7701
    iget-object v1, p0, Lmwg;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 7702
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmwg;->ah:Ljava/util/List;

    .line 7705
    :cond_1
    iget-object v1, p0, Lmwg;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 7707
    :sswitch_0
    return-object p0

    .line 7712
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmwg;->a:Ljava/lang/Integer;

    goto :goto_0

    .line 7716
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmwg;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 7720
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmwg;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 7724
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 7725
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    .line 7730
    :cond_2
    iput v0, p0, Lmwg;->d:I

    goto :goto_0

    .line 7732
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lmwg;->d:I

    goto :goto_0

    .line 7737
    :sswitch_5
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmwg;->e:Ljava/lang/Boolean;

    goto :goto_0

    .line 7741
    :sswitch_6
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmwg;->f:Ljava/lang/Boolean;

    goto :goto_0

    .line 7745
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmwg;->g:Ljava/lang/String;

    goto :goto_0

    .line 7749
    :sswitch_8
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmwg;->h:Ljava/lang/String;

    goto :goto_0

    .line 7697
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 7623
    iget-object v0, p0, Lmwg;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 7624
    const/4 v0, 0x1

    iget-object v1, p0, Lmwg;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 7626
    :cond_0
    iget-object v0, p0, Lmwg;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 7627
    const/4 v0, 0x2

    iget-object v1, p0, Lmwg;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 7629
    :cond_1
    iget-object v0, p0, Lmwg;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 7630
    const/4 v0, 0x3

    iget-object v1, p0, Lmwg;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 7632
    :cond_2
    iget v0, p0, Lmwg;->d:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_3

    .line 7633
    const/4 v0, 0x4

    iget v1, p0, Lmwg;->d:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 7635
    :cond_3
    iget-object v0, p0, Lmwg;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 7636
    const/4 v0, 0x5

    iget-object v1, p0, Lmwg;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 7638
    :cond_4
    iget-object v0, p0, Lmwg;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 7639
    const/4 v0, 0x6

    iget-object v1, p0, Lmwg;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 7641
    :cond_5
    iget-object v0, p0, Lmwg;->g:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 7642
    const/4 v0, 0x7

    iget-object v1, p0, Lmwg;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 7644
    :cond_6
    iget-object v0, p0, Lmwg;->h:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 7645
    const/16 v0, 0x8

    iget-object v1, p0, Lmwg;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 7647
    :cond_7
    iget-object v0, p0, Lmwg;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 7649
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 7589
    invoke-virtual {p0, p1}, Lmwg;->a(Loxn;)Lmwg;

    move-result-object v0

    return-object v0
.end method
