.class public interface abstract Lcul;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Lief;

.field public static final b:Lief;

.field public static final c:Lief;

.field public static final d:Lief;

.field public static final e:Lief;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    .line 187
    new-instance v0, Lief;

    const-string v1, "debug.hlsync.pages"

    const-string v2, "8"

    const-string v3, "21f44869"

    invoke-direct {v0, v1, v2, v3, v6}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lcul;->a:Lief;

    .line 196
    new-instance v0, Lief;

    const-string v1, "debug.hlsync.period"

    sget-object v2, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x6

    .line 198
    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    const-string v3, "e92e79a3"

    invoke-direct {v0, v1, v2, v3, v6}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lcul;->b:Lief;

    .line 208
    new-instance v0, Lief;

    const-string v1, "debug.hlsync.items"

    const-string v2, "100"

    const-string v3, "f244dcfc"

    invoke-direct {v0, v1, v2, v3, v6}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lcul;->c:Lief;

    .line 217
    new-instance v0, Lief;

    const-string v1, "debug.hlsync.social_on_period"

    const-string v2, "false"

    const-string v3, "e2eb234d"

    invoke-direct {v0, v1, v2, v3, v6}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lcul;->d:Lief;

    .line 226
    new-instance v0, Lief;

    const-string v1, "debug.hlsync.social_on_ping"

    const-string v2, "false"

    const-string v3, "904f73d6"

    invoke-direct {v0, v1, v2, v3, v6}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lcul;->e:Lief;

    return-void
.end method
