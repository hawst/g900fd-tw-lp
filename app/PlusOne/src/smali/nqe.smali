.class public final Lnqe;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnqe;


# instance fields
.field public b:I

.field public c:I

.field public d:Ljava/lang/String;

.field public e:Lnqh;

.field public f:Lnqc;

.field public g:Ljava/lang/Boolean;

.field public h:Ljava/lang/Boolean;

.field public i:[Lnqb;

.field public j:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 781
    const/4 v0, 0x0

    new-array v0, v0, [Lnqe;

    sput-object v0, Lnqe;->a:[Lnqe;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/high16 v0, -0x80000000

    .line 782
    invoke-direct {p0}, Loxq;-><init>()V

    .line 810
    iput v0, p0, Lnqe;->b:I

    .line 813
    iput v0, p0, Lnqe;->c:I

    .line 818
    iput-object v1, p0, Lnqe;->e:Lnqh;

    .line 821
    iput-object v1, p0, Lnqe;->f:Lnqc;

    .line 828
    sget-object v0, Lnqb;->a:[Lnqb;

    iput-object v0, p0, Lnqe;->i:[Lnqb;

    .line 782
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/high16 v3, -0x80000000

    .line 872
    .line 873
    iget v0, p0, Lnqe;->b:I

    if-eq v0, v3, :cond_9

    .line 874
    const/4 v0, 0x1

    iget v2, p0, Lnqe;->b:I

    .line 875
    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 877
    :goto_0
    iget v2, p0, Lnqe;->c:I

    if-eq v2, v3, :cond_0

    .line 878
    const/4 v2, 0x2

    iget v3, p0, Lnqe;->c:I

    .line 879
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 881
    :cond_0
    iget-object v2, p0, Lnqe;->d:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 882
    const/4 v2, 0x3

    iget-object v3, p0, Lnqe;->d:Ljava/lang/String;

    .line 883
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 885
    :cond_1
    iget-object v2, p0, Lnqe;->f:Lnqc;

    if-eqz v2, :cond_2

    .line 886
    const/4 v2, 0x4

    iget-object v3, p0, Lnqe;->f:Lnqc;

    .line 887
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 889
    :cond_2
    iget-object v2, p0, Lnqe;->g:Ljava/lang/Boolean;

    if-eqz v2, :cond_3

    .line 890
    const/4 v2, 0x5

    iget-object v3, p0, Lnqe;->g:Ljava/lang/Boolean;

    .line 891
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 893
    :cond_3
    iget-object v2, p0, Lnqe;->h:Ljava/lang/Boolean;

    if-eqz v2, :cond_4

    .line 894
    const/4 v2, 0x6

    iget-object v3, p0, Lnqe;->h:Ljava/lang/Boolean;

    .line 895
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 897
    :cond_4
    iget-object v2, p0, Lnqe;->i:[Lnqb;

    if-eqz v2, :cond_6

    .line 898
    iget-object v2, p0, Lnqe;->i:[Lnqb;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    .line 899
    if-eqz v4, :cond_5

    .line 900
    const/4 v5, 0x7

    .line 901
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 898
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 905
    :cond_6
    iget-object v1, p0, Lnqe;->j:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 906
    const/16 v1, 0x8

    iget-object v2, p0, Lnqe;->j:Ljava/lang/String;

    .line 907
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 909
    :cond_7
    iget-object v1, p0, Lnqe;->e:Lnqh;

    if-eqz v1, :cond_8

    .line 910
    const/16 v1, 0x9

    iget-object v2, p0, Lnqe;->e:Lnqh;

    .line 911
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 913
    :cond_8
    iget-object v1, p0, Lnqe;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 914
    iput v0, p0, Lnqe;->ai:I

    .line 915
    return v0

    :cond_9
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lnqe;
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 923
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 924
    sparse-switch v0, :sswitch_data_0

    .line 928
    iget-object v2, p0, Lnqe;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 929
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnqe;->ah:Ljava/util/List;

    .line 932
    :cond_1
    iget-object v2, p0, Lnqe;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 934
    :sswitch_0
    return-object p0

    .line 939
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 940
    if-eqz v0, :cond_2

    if-eq v0, v4, :cond_2

    if-eq v0, v5, :cond_2

    if-eq v0, v6, :cond_2

    if-eq v0, v7, :cond_2

    const/4 v2, 0x5

    if-eq v0, v2, :cond_2

    const/4 v2, 0x6

    if-eq v0, v2, :cond_2

    const/4 v2, 0x7

    if-eq v0, v2, :cond_2

    const/16 v2, 0x8

    if-eq v0, v2, :cond_2

    const/16 v2, 0x9

    if-eq v0, v2, :cond_2

    const/16 v2, 0xa

    if-eq v0, v2, :cond_2

    const/16 v2, 0xb

    if-eq v0, v2, :cond_2

    const/16 v2, 0xc

    if-eq v0, v2, :cond_2

    const/16 v2, 0xd

    if-ne v0, v2, :cond_3

    .line 954
    :cond_2
    iput v0, p0, Lnqe;->b:I

    goto :goto_0

    .line 956
    :cond_3
    iput v1, p0, Lnqe;->b:I

    goto :goto_0

    .line 961
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 962
    if-eq v0, v4, :cond_4

    if-eq v0, v5, :cond_4

    if-eq v0, v6, :cond_4

    if-eq v0, v7, :cond_4

    const/4 v2, 0x5

    if-ne v0, v2, :cond_5

    .line 967
    :cond_4
    iput v0, p0, Lnqe;->c:I

    goto :goto_0

    .line 969
    :cond_5
    iput v4, p0, Lnqe;->c:I

    goto :goto_0

    .line 974
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnqe;->d:Ljava/lang/String;

    goto :goto_0

    .line 978
    :sswitch_4
    iget-object v0, p0, Lnqe;->f:Lnqc;

    if-nez v0, :cond_6

    .line 979
    new-instance v0, Lnqc;

    invoke-direct {v0}, Lnqc;-><init>()V

    iput-object v0, p0, Lnqe;->f:Lnqc;

    .line 981
    :cond_6
    iget-object v0, p0, Lnqe;->f:Lnqc;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 985
    :sswitch_5
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnqe;->g:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 989
    :sswitch_6
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnqe;->h:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 993
    :sswitch_7
    const/16 v0, 0x3a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 994
    iget-object v0, p0, Lnqe;->i:[Lnqb;

    if-nez v0, :cond_8

    move v0, v1

    .line 995
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnqb;

    .line 996
    iget-object v3, p0, Lnqe;->i:[Lnqb;

    if-eqz v3, :cond_7

    .line 997
    iget-object v3, p0, Lnqe;->i:[Lnqb;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 999
    :cond_7
    iput-object v2, p0, Lnqe;->i:[Lnqb;

    .line 1000
    :goto_2
    iget-object v2, p0, Lnqe;->i:[Lnqb;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    .line 1001
    iget-object v2, p0, Lnqe;->i:[Lnqb;

    new-instance v3, Lnqb;

    invoke-direct {v3}, Lnqb;-><init>()V

    aput-object v3, v2, v0

    .line 1002
    iget-object v2, p0, Lnqe;->i:[Lnqb;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1003
    invoke-virtual {p1}, Loxn;->a()I

    .line 1000
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 994
    :cond_8
    iget-object v0, p0, Lnqe;->i:[Lnqb;

    array-length v0, v0

    goto :goto_1

    .line 1006
    :cond_9
    iget-object v2, p0, Lnqe;->i:[Lnqb;

    new-instance v3, Lnqb;

    invoke-direct {v3}, Lnqb;-><init>()V

    aput-object v3, v2, v0

    .line 1007
    iget-object v2, p0, Lnqe;->i:[Lnqb;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1011
    :sswitch_8
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnqe;->j:Ljava/lang/String;

    goto/16 :goto_0

    .line 1015
    :sswitch_9
    iget-object v0, p0, Lnqe;->e:Lnqh;

    if-nez v0, :cond_a

    .line 1016
    new-instance v0, Lnqh;

    invoke-direct {v0}, Lnqh;-><init>()V

    iput-object v0, p0, Lnqe;->e:Lnqh;

    .line 1018
    :cond_a
    iget-object v0, p0, Lnqe;->e:Lnqh;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 924
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    const/high16 v2, -0x80000000

    .line 835
    iget v0, p0, Lnqe;->b:I

    if-eq v0, v2, :cond_0

    .line 836
    const/4 v0, 0x1

    iget v1, p0, Lnqe;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 838
    :cond_0
    iget v0, p0, Lnqe;->c:I

    if-eq v0, v2, :cond_1

    .line 839
    const/4 v0, 0x2

    iget v1, p0, Lnqe;->c:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 841
    :cond_1
    iget-object v0, p0, Lnqe;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 842
    const/4 v0, 0x3

    iget-object v1, p0, Lnqe;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 844
    :cond_2
    iget-object v0, p0, Lnqe;->f:Lnqc;

    if-eqz v0, :cond_3

    .line 845
    const/4 v0, 0x4

    iget-object v1, p0, Lnqe;->f:Lnqc;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 847
    :cond_3
    iget-object v0, p0, Lnqe;->g:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 848
    const/4 v0, 0x5

    iget-object v1, p0, Lnqe;->g:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 850
    :cond_4
    iget-object v0, p0, Lnqe;->h:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 851
    const/4 v0, 0x6

    iget-object v1, p0, Lnqe;->h:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 853
    :cond_5
    iget-object v0, p0, Lnqe;->i:[Lnqb;

    if-eqz v0, :cond_7

    .line 854
    iget-object v1, p0, Lnqe;->i:[Lnqb;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_7

    aget-object v3, v1, v0

    .line 855
    if-eqz v3, :cond_6

    .line 856
    const/4 v4, 0x7

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 854
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 860
    :cond_7
    iget-object v0, p0, Lnqe;->j:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 861
    const/16 v0, 0x8

    iget-object v1, p0, Lnqe;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 863
    :cond_8
    iget-object v0, p0, Lnqe;->e:Lnqh;

    if-eqz v0, :cond_9

    .line 864
    const/16 v0, 0x9

    iget-object v1, p0, Lnqe;->e:Lnqh;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 866
    :cond_9
    iget-object v0, p0, Lnqe;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 868
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 778
    invoke-virtual {p0, p1}, Lnqe;->a(Loxn;)Lnqe;

    move-result-object v0

    return-object v0
.end method
