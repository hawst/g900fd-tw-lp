.class public final Ljfo;
.super Lhxz;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lhxz",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljfy;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ldp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldo",
            "<",
            "Ljava/lang/Integer;",
            ">.dp;"
        }
    .end annotation
.end field

.field private d:Ljfp;

.field private e:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljfp;)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lhxz;-><init>(Landroid/content/Context;)V

    .line 14
    new-instance v0, Ldp;

    invoke-direct {v0, p0}, Ldp;-><init>(Ldo;)V

    iput-object v0, p0, Ljfo;->c:Ldp;

    .line 18
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ljfo;->e:Ljava/lang/Integer;

    .line 22
    iput-object p2, p0, Ljfo;->d:Ljfp;

    .line 23
    return-void
.end method


# virtual methods
.method protected D_()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 53
    iget-object v1, p0, Ljfo;->b:Ljava/util/List;

    if-eqz v1, :cond_1

    .line 54
    iget-object v1, p0, Ljfo;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    move v1, v0

    .line 55
    :goto_0
    if-ge v1, v2, :cond_0

    .line 56
    iget-object v0, p0, Ljfo;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljfy;

    iget-object v3, p0, Ljfo;->c:Ldp;

    invoke-virtual {v0, v3}, Ljfy;->a(Landroid/database/ContentObserver;)V

    .line 55
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 58
    :cond_0
    const/4 v0, 0x1

    .line 60
    :cond_1
    return v0
.end method

.method protected i()V
    .locals 3

    .prologue
    .line 27
    invoke-super {p0}, Lhxz;->i()V

    .line 28
    iget-object v0, p0, Ljfo;->b:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 29
    iget-object v0, p0, Ljfo;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 30
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 31
    iget-object v0, p0, Ljfo;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljfy;

    invoke-virtual {v0}, Ljfy;->s()V

    .line 30
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 34
    :cond_0
    return-void
.end method

.method public synthetic j()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0}, Ljfo;->l()Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected k()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 65
    iget-object v1, p0, Ljfo;->b:Ljava/util/List;

    if-eqz v1, :cond_1

    .line 66
    iget-object v1, p0, Ljfo;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    move v1, v0

    .line 67
    :goto_0
    if-ge v1, v2, :cond_0

    .line 68
    iget-object v0, p0, Ljfo;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljfy;

    iget-object v3, p0, Ljfo;->c:Ldp;

    invoke-virtual {v0, v3}, Ljfy;->b(Landroid/database/ContentObserver;)V

    .line 67
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 70
    :cond_0
    const/4 v0, 0x1

    .line 72
    :cond_1
    return v0
.end method

.method public l()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 38
    iget-object v0, p0, Ljfo;->d:Ljfp;

    invoke-virtual {v0}, Ljfp;->f()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Ljfo;->b:Ljava/util/List;

    .line 39
    iget-object v0, p0, Ljfo;->b:Ljava/util/List;

    if-nez v0, :cond_0

    .line 40
    iget-object v0, p0, Ljfo;->e:Ljava/lang/Integer;

    iget-object v1, p0, Ljfo;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Ljfo;->e:Ljava/lang/Integer;

    .line 48
    :goto_0
    return-object v0

    .line 42
    :cond_0
    invoke-virtual {p0}, Ljfo;->D()V

    .line 43
    iget-object v0, p0, Ljfo;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 44
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_1

    .line 45
    iget-object v0, p0, Ljfo;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljfy;

    invoke-virtual {v0}, Ljfy;->as_()V

    .line 44
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 47
    :cond_1
    invoke-virtual {p0}, Ljfo;->C()V

    .line 48
    iget-object v0, p0, Ljfo;->e:Ljava/lang/Integer;

    iget-object v1, p0, Ljfo;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Ljfo;->e:Ljava/lang/Integer;

    goto :goto_0
.end method
