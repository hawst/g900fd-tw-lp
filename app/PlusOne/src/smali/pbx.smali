.class public final Lpbx;
.super Loxq;
.source "PG"


# static fields
.field public static final a:Loxr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Loxr",
            "<",
            "Lpbx;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:Loya;

.field public c:Loya;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Lpdi;

.field private j:Ljava/lang/String;

.field private k:[Loya;

.field private l:Loya;

.field private m:[Loya;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:Loya;

.field private s:Loya;

.field private t:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 9
    const v0, 0x2933c3c

    new-instance v1, Lpby;

    invoke-direct {v1}, Lpby;-><init>()V

    .line 14
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    move-result-object v0

    sput-object v0, Lpbx;->a:Loxr;

    .line 13
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 27
    iput-object v1, p0, Lpbx;->i:Lpdi;

    .line 32
    sget-object v0, Loya;->a:[Loya;

    iput-object v0, p0, Lpbx;->k:[Loya;

    .line 35
    iput-object v1, p0, Lpbx;->l:Loya;

    .line 38
    sget-object v0, Loya;->a:[Loya;

    iput-object v0, p0, Lpbx;->m:[Loya;

    .line 49
    iput-object v1, p0, Lpbx;->r:Loya;

    .line 52
    iput-object v1, p0, Lpbx;->b:Loya;

    .line 55
    iput-object v1, p0, Lpbx;->c:Loya;

    .line 58
    iput-object v1, p0, Lpbx;->s:Loya;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 136
    .line 137
    iget-object v0, p0, Lpbx;->d:Ljava/lang/String;

    if-eqz v0, :cond_14

    .line 138
    const/4 v0, 0x1

    iget-object v2, p0, Lpbx;->d:Ljava/lang/String;

    .line 139
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 141
    :goto_0
    iget-object v2, p0, Lpbx;->e:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 142
    const/4 v2, 0x2

    iget-object v3, p0, Lpbx;->e:Ljava/lang/String;

    .line 143
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 145
    :cond_0
    iget-object v2, p0, Lpbx;->f:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 146
    const/4 v2, 0x3

    iget-object v3, p0, Lpbx;->f:Ljava/lang/String;

    .line 147
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 149
    :cond_1
    iget-object v2, p0, Lpbx;->g:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 150
    const/4 v2, 0x4

    iget-object v3, p0, Lpbx;->g:Ljava/lang/String;

    .line 151
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 153
    :cond_2
    iget-object v2, p0, Lpbx;->h:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 154
    const/4 v2, 0x5

    iget-object v3, p0, Lpbx;->h:Ljava/lang/String;

    .line 155
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 157
    :cond_3
    iget-object v2, p0, Lpbx;->i:Lpdi;

    if-eqz v2, :cond_4

    .line 158
    const/4 v2, 0x6

    iget-object v3, p0, Lpbx;->i:Lpdi;

    .line 159
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 161
    :cond_4
    iget-object v2, p0, Lpbx;->j:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 162
    const/4 v2, 0x7

    iget-object v3, p0, Lpbx;->j:Ljava/lang/String;

    .line 163
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 165
    :cond_5
    iget-object v2, p0, Lpbx;->k:[Loya;

    if-eqz v2, :cond_7

    .line 166
    iget-object v3, p0, Lpbx;->k:[Loya;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_7

    aget-object v5, v3, v2

    .line 167
    if-eqz v5, :cond_6

    .line 168
    const/16 v6, 0x8

    .line 169
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 166
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 173
    :cond_7
    iget-object v2, p0, Lpbx;->l:Loya;

    if-eqz v2, :cond_8

    .line 174
    const/16 v2, 0x9

    iget-object v3, p0, Lpbx;->l:Loya;

    .line 175
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 177
    :cond_8
    iget-object v2, p0, Lpbx;->m:[Loya;

    if-eqz v2, :cond_a

    .line 178
    iget-object v2, p0, Lpbx;->m:[Loya;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_a

    aget-object v4, v2, v1

    .line 179
    if-eqz v4, :cond_9

    .line 180
    const/16 v5, 0xb

    .line 181
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 178
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 185
    :cond_a
    iget-object v1, p0, Lpbx;->n:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 186
    const/16 v1, 0xc

    iget-object v2, p0, Lpbx;->n:Ljava/lang/String;

    .line 187
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 189
    :cond_b
    iget-object v1, p0, Lpbx;->o:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 190
    const/16 v1, 0x1b

    iget-object v2, p0, Lpbx;->o:Ljava/lang/String;

    .line 191
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 193
    :cond_c
    iget-object v1, p0, Lpbx;->p:Ljava/lang/String;

    if-eqz v1, :cond_d

    .line 194
    const/16 v1, 0x4b

    iget-object v2, p0, Lpbx;->p:Ljava/lang/String;

    .line 195
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 197
    :cond_d
    iget-object v1, p0, Lpbx;->q:Ljava/lang/String;

    if-eqz v1, :cond_e

    .line 198
    const/16 v1, 0xb0

    iget-object v2, p0, Lpbx;->q:Ljava/lang/String;

    .line 199
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 201
    :cond_e
    iget-object v1, p0, Lpbx;->r:Loya;

    if-eqz v1, :cond_f

    .line 202
    const/16 v1, 0xb1

    iget-object v2, p0, Lpbx;->r:Loya;

    .line 203
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 205
    :cond_f
    iget-object v1, p0, Lpbx;->b:Loya;

    if-eqz v1, :cond_10

    .line 206
    const/16 v1, 0xb2

    iget-object v2, p0, Lpbx;->b:Loya;

    .line 207
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 209
    :cond_10
    iget-object v1, p0, Lpbx;->c:Loya;

    if-eqz v1, :cond_11

    .line 210
    const/16 v1, 0xb3

    iget-object v2, p0, Lpbx;->c:Loya;

    .line 211
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 213
    :cond_11
    iget-object v1, p0, Lpbx;->s:Loya;

    if-eqz v1, :cond_12

    .line 214
    const/16 v1, 0xb9

    iget-object v2, p0, Lpbx;->s:Loya;

    .line 215
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 217
    :cond_12
    iget-object v1, p0, Lpbx;->t:Ljava/lang/String;

    if-eqz v1, :cond_13

    .line 218
    const/16 v1, 0xfe

    iget-object v2, p0, Lpbx;->t:Ljava/lang/String;

    .line 219
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 221
    :cond_13
    iget-object v1, p0, Lpbx;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 222
    iput v0, p0, Lpbx;->ai:I

    .line 223
    return v0

    :cond_14
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lpbx;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 231
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 232
    sparse-switch v0, :sswitch_data_0

    .line 236
    iget-object v2, p0, Lpbx;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 237
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lpbx;->ah:Ljava/util/List;

    .line 240
    :cond_1
    iget-object v2, p0, Lpbx;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 242
    :sswitch_0
    return-object p0

    .line 247
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpbx;->d:Ljava/lang/String;

    goto :goto_0

    .line 251
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpbx;->e:Ljava/lang/String;

    goto :goto_0

    .line 255
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpbx;->f:Ljava/lang/String;

    goto :goto_0

    .line 259
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpbx;->g:Ljava/lang/String;

    goto :goto_0

    .line 263
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpbx;->h:Ljava/lang/String;

    goto :goto_0

    .line 267
    :sswitch_6
    iget-object v0, p0, Lpbx;->i:Lpdi;

    if-nez v0, :cond_2

    .line 268
    new-instance v0, Lpdi;

    invoke-direct {v0}, Lpdi;-><init>()V

    iput-object v0, p0, Lpbx;->i:Lpdi;

    .line 270
    :cond_2
    iget-object v0, p0, Lpbx;->i:Lpdi;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 274
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpbx;->j:Ljava/lang/String;

    goto :goto_0

    .line 278
    :sswitch_8
    const/16 v0, 0x42

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 279
    iget-object v0, p0, Lpbx;->k:[Loya;

    if-nez v0, :cond_4

    move v0, v1

    .line 280
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Loya;

    .line 281
    iget-object v3, p0, Lpbx;->k:[Loya;

    if-eqz v3, :cond_3

    .line 282
    iget-object v3, p0, Lpbx;->k:[Loya;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 284
    :cond_3
    iput-object v2, p0, Lpbx;->k:[Loya;

    .line 285
    :goto_2
    iget-object v2, p0, Lpbx;->k:[Loya;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 286
    iget-object v2, p0, Lpbx;->k:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 287
    iget-object v2, p0, Lpbx;->k:[Loya;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 288
    invoke-virtual {p1}, Loxn;->a()I

    .line 285
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 279
    :cond_4
    iget-object v0, p0, Lpbx;->k:[Loya;

    array-length v0, v0

    goto :goto_1

    .line 291
    :cond_5
    iget-object v2, p0, Lpbx;->k:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 292
    iget-object v2, p0, Lpbx;->k:[Loya;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 296
    :sswitch_9
    iget-object v0, p0, Lpbx;->l:Loya;

    if-nez v0, :cond_6

    .line 297
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    iput-object v0, p0, Lpbx;->l:Loya;

    .line 299
    :cond_6
    iget-object v0, p0, Lpbx;->l:Loya;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 303
    :sswitch_a
    const/16 v0, 0x5a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 304
    iget-object v0, p0, Lpbx;->m:[Loya;

    if-nez v0, :cond_8

    move v0, v1

    .line 305
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Loya;

    .line 306
    iget-object v3, p0, Lpbx;->m:[Loya;

    if-eqz v3, :cond_7

    .line 307
    iget-object v3, p0, Lpbx;->m:[Loya;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 309
    :cond_7
    iput-object v2, p0, Lpbx;->m:[Loya;

    .line 310
    :goto_4
    iget-object v2, p0, Lpbx;->m:[Loya;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    .line 311
    iget-object v2, p0, Lpbx;->m:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 312
    iget-object v2, p0, Lpbx;->m:[Loya;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 313
    invoke-virtual {p1}, Loxn;->a()I

    .line 310
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 304
    :cond_8
    iget-object v0, p0, Lpbx;->m:[Loya;

    array-length v0, v0

    goto :goto_3

    .line 316
    :cond_9
    iget-object v2, p0, Lpbx;->m:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 317
    iget-object v2, p0, Lpbx;->m:[Loya;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 321
    :sswitch_b
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpbx;->n:Ljava/lang/String;

    goto/16 :goto_0

    .line 325
    :sswitch_c
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpbx;->o:Ljava/lang/String;

    goto/16 :goto_0

    .line 329
    :sswitch_d
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpbx;->p:Ljava/lang/String;

    goto/16 :goto_0

    .line 333
    :sswitch_e
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpbx;->q:Ljava/lang/String;

    goto/16 :goto_0

    .line 337
    :sswitch_f
    iget-object v0, p0, Lpbx;->r:Loya;

    if-nez v0, :cond_a

    .line 338
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    iput-object v0, p0, Lpbx;->r:Loya;

    .line 340
    :cond_a
    iget-object v0, p0, Lpbx;->r:Loya;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 344
    :sswitch_10
    iget-object v0, p0, Lpbx;->b:Loya;

    if-nez v0, :cond_b

    .line 345
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    iput-object v0, p0, Lpbx;->b:Loya;

    .line 347
    :cond_b
    iget-object v0, p0, Lpbx;->b:Loya;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 351
    :sswitch_11
    iget-object v0, p0, Lpbx;->c:Loya;

    if-nez v0, :cond_c

    .line 352
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    iput-object v0, p0, Lpbx;->c:Loya;

    .line 354
    :cond_c
    iget-object v0, p0, Lpbx;->c:Loya;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 358
    :sswitch_12
    iget-object v0, p0, Lpbx;->s:Loya;

    if-nez v0, :cond_d

    .line 359
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    iput-object v0, p0, Lpbx;->s:Loya;

    .line 361
    :cond_d
    iget-object v0, p0, Lpbx;->s:Loya;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 365
    :sswitch_13
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpbx;->t:Ljava/lang/String;

    goto/16 :goto_0

    .line 232
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x5a -> :sswitch_a
        0x62 -> :sswitch_b
        0xda -> :sswitch_c
        0x25a -> :sswitch_d
        0x582 -> :sswitch_e
        0x58a -> :sswitch_f
        0x592 -> :sswitch_10
        0x59a -> :sswitch_11
        0x5ca -> :sswitch_12
        0x7f2 -> :sswitch_13
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 65
    iget-object v1, p0, Lpbx;->d:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 66
    const/4 v1, 0x1

    iget-object v2, p0, Lpbx;->d:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 68
    :cond_0
    iget-object v1, p0, Lpbx;->e:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 69
    const/4 v1, 0x2

    iget-object v2, p0, Lpbx;->e:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 71
    :cond_1
    iget-object v1, p0, Lpbx;->f:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 72
    const/4 v1, 0x3

    iget-object v2, p0, Lpbx;->f:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 74
    :cond_2
    iget-object v1, p0, Lpbx;->g:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 75
    const/4 v1, 0x4

    iget-object v2, p0, Lpbx;->g:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 77
    :cond_3
    iget-object v1, p0, Lpbx;->h:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 78
    const/4 v1, 0x5

    iget-object v2, p0, Lpbx;->h:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 80
    :cond_4
    iget-object v1, p0, Lpbx;->i:Lpdi;

    if-eqz v1, :cond_5

    .line 81
    const/4 v1, 0x6

    iget-object v2, p0, Lpbx;->i:Lpdi;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 83
    :cond_5
    iget-object v1, p0, Lpbx;->j:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 84
    const/4 v1, 0x7

    iget-object v2, p0, Lpbx;->j:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 86
    :cond_6
    iget-object v1, p0, Lpbx;->k:[Loya;

    if-eqz v1, :cond_8

    .line 87
    iget-object v2, p0, Lpbx;->k:[Loya;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_8

    aget-object v4, v2, v1

    .line 88
    if-eqz v4, :cond_7

    .line 89
    const/16 v5, 0x8

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 87
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 93
    :cond_8
    iget-object v1, p0, Lpbx;->l:Loya;

    if-eqz v1, :cond_9

    .line 94
    const/16 v1, 0x9

    iget-object v2, p0, Lpbx;->l:Loya;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 96
    :cond_9
    iget-object v1, p0, Lpbx;->m:[Loya;

    if-eqz v1, :cond_b

    .line 97
    iget-object v1, p0, Lpbx;->m:[Loya;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_b

    aget-object v3, v1, v0

    .line 98
    if-eqz v3, :cond_a

    .line 99
    const/16 v4, 0xb

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 97
    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 103
    :cond_b
    iget-object v0, p0, Lpbx;->n:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 104
    const/16 v0, 0xc

    iget-object v1, p0, Lpbx;->n:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 106
    :cond_c
    iget-object v0, p0, Lpbx;->o:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 107
    const/16 v0, 0x1b

    iget-object v1, p0, Lpbx;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 109
    :cond_d
    iget-object v0, p0, Lpbx;->p:Ljava/lang/String;

    if-eqz v0, :cond_e

    .line 110
    const/16 v0, 0x4b

    iget-object v1, p0, Lpbx;->p:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 112
    :cond_e
    iget-object v0, p0, Lpbx;->q:Ljava/lang/String;

    if-eqz v0, :cond_f

    .line 113
    const/16 v0, 0xb0

    iget-object v1, p0, Lpbx;->q:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 115
    :cond_f
    iget-object v0, p0, Lpbx;->r:Loya;

    if-eqz v0, :cond_10

    .line 116
    const/16 v0, 0xb1

    iget-object v1, p0, Lpbx;->r:Loya;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 118
    :cond_10
    iget-object v0, p0, Lpbx;->b:Loya;

    if-eqz v0, :cond_11

    .line 119
    const/16 v0, 0xb2

    iget-object v1, p0, Lpbx;->b:Loya;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 121
    :cond_11
    iget-object v0, p0, Lpbx;->c:Loya;

    if-eqz v0, :cond_12

    .line 122
    const/16 v0, 0xb3

    iget-object v1, p0, Lpbx;->c:Loya;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 124
    :cond_12
    iget-object v0, p0, Lpbx;->s:Loya;

    if-eqz v0, :cond_13

    .line 125
    const/16 v0, 0xb9

    iget-object v1, p0, Lpbx;->s:Loya;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 127
    :cond_13
    iget-object v0, p0, Lpbx;->t:Ljava/lang/String;

    if-eqz v0, :cond_14

    .line 128
    const/16 v0, 0xfe

    iget-object v1, p0, Lpbx;->t:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 130
    :cond_14
    iget-object v0, p0, Lpbx;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 132
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lpbx;->a(Loxn;)Lpbx;

    move-result-object v0

    return-object v0
.end method
