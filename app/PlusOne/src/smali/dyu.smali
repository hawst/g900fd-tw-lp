.class public final Ldyu;
.super Lt;
.source "PG"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private N:[I

.field private O:Z

.field private P:Z

.field private Q:Z

.field private R:Z

.field private S:Ljava/lang/Long;

.field private T:I

.field private U:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 71
    invoke-direct {p0}, Lt;-><init>()V

    .line 60
    const/4 v0, 0x6

    new-array v0, v0, [I

    iput-object v0, p0, Ldyu;->N:[I

    .line 64
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldyu;->Q:Z

    .line 67
    const v0, 0x7f0a0755

    iput v0, p0, Ldyu;->T:I

    .line 72
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 74
    invoke-direct {p0}, Lt;-><init>()V

    .line 60
    const/4 v0, 0x6

    new-array v0, v0, [I

    iput-object v0, p0, Ldyu;->N:[I

    .line 64
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldyu;->Q:Z

    .line 67
    const v0, 0x7f0a0755

    iput v0, p0, Ldyu;->T:I

    .line 75
    iput p1, p0, Ldyu;->T:I

    .line 76
    return-void
.end method


# virtual methods
.method public a(Lae;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 189
    iget-boolean v0, p0, Ldyu;->O:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Ldyu;->P:Z

    if-eqz v0, :cond_2

    .line 190
    :cond_0
    invoke-super {p0, p1, p2}, Lt;->a(Lae;Ljava/lang/String;)V

    .line 197
    :cond_1
    :goto_0
    return-void

    .line 192
    :cond_2
    invoke-virtual {p0}, Ldyu;->u_()Lu;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 193
    invoke-virtual {p0}, Ldyu;->u_()Lu;

    move-result-object v0

    check-cast v0, Ldyv;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ldyv;->a(I)V

    goto :goto_0
.end method

.method public a(ZZLjava/lang/Long;)V
    .locals 0

    .prologue
    .line 108
    iput-boolean p1, p0, Ldyu;->P:Z

    .line 109
    iput-boolean p2, p0, Ldyu;->R:Z

    .line 110
    iput-object p3, p0, Ldyu;->S:Ljava/lang/Long;

    .line 111
    return-void
.end method

.method public c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 126
    invoke-super {p0, p1}, Lt;->c(Landroid/os/Bundle;)Landroid/app/Dialog;

    .line 128
    if-eqz p1, :cond_0

    .line 129
    const-string v0, "is_camera_supported"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Ldyu;->O:Z

    .line 130
    const-string v0, "title"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Ldyu;->T:I

    .line 131
    const-string v0, "only_instant_upload"

    .line 132
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Ldyu;->U:Z

    .line 133
    const-string v0, "has_scrapbook"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Ldyu;->P:Z

    .line 134
    const-string v0, "has_scrapbook"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Ldyu;->R:Z

    .line 135
    const-string v0, "local_folders_only"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Ldyu;->Q:Z

    .line 137
    const-string v0, "cover_photo_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 138
    const-string v0, "cover_photo_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldyu;->S:Ljava/lang/Long;

    .line 142
    :cond_0
    invoke-virtual {p0}, Ldyu;->n()Lz;

    move-result-object v0

    .line 144
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 146
    iget-boolean v2, p0, Ldyu;->O:Z

    if-eqz v2, :cond_1

    .line 147
    iget-object v2, p0, Ldyu;->N:[I

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    aput v5, v2, v3

    .line 148
    const v2, 0x7f0a08e6

    invoke-virtual {v0, v2}, Lz;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 151
    :cond_1
    iget-object v2, p0, Ldyu;->N:[I

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v4, 0x2

    aput v4, v2, v3

    .line 152
    iget-boolean v2, p0, Ldyu;->U:Z

    if-eqz v2, :cond_5

    .line 153
    const v2, 0x7f0a08e8

    invoke-virtual {v0, v2}, Lz;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 158
    :goto_0
    iget-boolean v2, p0, Ldyu;->Q:Z

    if-nez v2, :cond_2

    .line 164
    iget-object v2, p0, Ldyu;->N:[I

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v4, 0x6

    aput v4, v2, v3

    .line 165
    const v2, 0x7f0a08ea

    invoke-virtual {v0, v2}, Lz;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 168
    :cond_2
    iget-boolean v2, p0, Ldyu;->P:Z

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Ldyu;->R:Z

    if-eqz v2, :cond_3

    .line 169
    iget-object v2, p0, Ldyu;->N:[I

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v4, 0x4

    aput v4, v2, v3

    .line 170
    const v2, 0x7f0a08eb

    invoke-virtual {v0, v2}, Lz;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 173
    :cond_3
    iget-boolean v2, p0, Ldyu;->P:Z

    if-eqz v2, :cond_4

    .line 174
    iget-object v2, p0, Ldyu;->N:[I

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v4, 0x3

    aput v4, v2, v3

    .line 175
    const v2, 0x7f0a08e9

    invoke-virtual {v0, v2}, Lz;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 178
    :cond_4
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 179
    iget v3, p0, Ldyu;->T:I

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 180
    new-instance v3, Landroid/widget/ArrayAdapter;

    const v4, 0x1090011

    invoke-direct {v3, v0, v4, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v2, v3, p0}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 182
    invoke-virtual {v2, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 183
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    .line 155
    :cond_5
    const v2, 0x7f0a08e7

    invoke-virtual {v0, v2}, Lz;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 201
    invoke-super {p0, p1}, Lt;->e(Landroid/os/Bundle;)V

    .line 202
    const-string v0, "is_camera_supported"

    iget-boolean v1, p0, Ldyu;->O:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 203
    const-string v0, "title"

    iget v1, p0, Ldyu;->T:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 204
    const-string v0, "only_instant_upload"

    iget-boolean v1, p0, Ldyu;->U:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 205
    const-string v0, "has_scrapbook"

    iget-boolean v1, p0, Ldyu;->P:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 206
    const-string v0, "has_scrapbook"

    iget-boolean v1, p0, Ldyu;->R:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 207
    const-string v0, "local_folders_only"

    iget-boolean v1, p0, Ldyu;->Q:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 208
    iget-object v0, p0, Ldyu;->S:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 209
    const-string v0, "cover_photo_id"

    iget-object v1, p0, Ldyu;->S:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 211
    :cond_0
    return-void
.end method

.method public i(Z)V
    .locals 0

    .prologue
    .line 84
    iput-boolean p1, p0, Ldyu;->O:Z

    .line 85
    return-void
.end method

.method public j(Z)V
    .locals 0

    .prologue
    .line 97
    iput-boolean p1, p0, Ldyu;->Q:Z

    .line 98
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    .prologue
    .line 215
    invoke-virtual {p0}, Ldyu;->t()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 248
    :cond_0
    :goto_0
    return-void

    .line 219
    :cond_1
    invoke-virtual {p0}, Ldyu;->a()V

    .line 221
    invoke-virtual {p0}, Ldyu;->u_()Lu;

    move-result-object v0

    .line 222
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lu;->n()Lz;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 226
    check-cast v0, Ldyv;

    .line 227
    iget-object v1, p0, Ldyu;->N:[I

    aget v1, v1, p2

    .line 228
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 230
    :pswitch_0
    invoke-interface {v0}, Ldyv;->i()V

    goto :goto_0

    .line 233
    :pswitch_1
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ldyv;->a(I)V

    goto :goto_0

    .line 236
    :pswitch_2
    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ldyv;->a(I)V

    goto :goto_0

    .line 239
    :pswitch_3
    const/4 v1, 0x3

    invoke-interface {v0, v1}, Ldyv;->a(I)V

    goto :goto_0

    .line 242
    :pswitch_4
    const/4 v1, 0x2

    invoke-interface {v0, v1}, Ldyv;->a(I)V

    goto :goto_0

    .line 245
    :pswitch_5
    const/4 v1, 0x4

    invoke-interface {v0, v1}, Ldyv;->a(I)V

    goto :goto_0

    .line 228
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_3
        :pswitch_5
    .end packed-switch
.end method
