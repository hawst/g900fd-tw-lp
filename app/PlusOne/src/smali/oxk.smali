.class public final Loxk;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Float;

.field private b:Ljava/lang/Float;

.field private c:Ljava/lang/Float;

.field private d:Ljava/lang/Float;

.field private e:Ljava/lang/Float;

.field private f:Ljava/lang/Float;

.field private g:Ljava/lang/Float;

.field private h:Ljava/lang/Float;

.field private i:Ljava/lang/Float;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 567
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 605
    const/4 v0, 0x1

    iget-object v1, p0, Loxk;->a:Ljava/lang/Float;

    .line 607
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x0

    .line 608
    const/4 v1, 0x2

    iget-object v2, p0, Loxk;->b:Ljava/lang/Float;

    .line 609
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 610
    const/4 v1, 0x3

    iget-object v2, p0, Loxk;->c:Ljava/lang/Float;

    .line 611
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 612
    const/4 v1, 0x4

    iget-object v2, p0, Loxk;->d:Ljava/lang/Float;

    .line 613
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 614
    const/4 v1, 0x5

    iget-object v2, p0, Loxk;->e:Ljava/lang/Float;

    .line 615
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 616
    const/4 v1, 0x6

    iget-object v2, p0, Loxk;->f:Ljava/lang/Float;

    .line 617
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 618
    const/4 v1, 0x7

    iget-object v2, p0, Loxk;->g:Ljava/lang/Float;

    .line 619
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 620
    const/16 v1, 0x8

    iget-object v2, p0, Loxk;->h:Ljava/lang/Float;

    .line 621
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 622
    const/16 v1, 0x9

    iget-object v2, p0, Loxk;->i:Ljava/lang/Float;

    .line 623
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 624
    iget-object v1, p0, Loxk;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 625
    iput v0, p0, Loxk;->ai:I

    .line 626
    return v0
.end method

.method public a(Loxn;)Loxk;
    .locals 2

    .prologue
    .line 634
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 635
    sparse-switch v0, :sswitch_data_0

    .line 639
    iget-object v1, p0, Loxk;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 640
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Loxk;->ah:Ljava/util/List;

    .line 643
    :cond_1
    iget-object v1, p0, Loxk;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 645
    :sswitch_0
    return-object p0

    .line 650
    :sswitch_1
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Loxk;->a:Ljava/lang/Float;

    goto :goto_0

    .line 654
    :sswitch_2
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Loxk;->b:Ljava/lang/Float;

    goto :goto_0

    .line 658
    :sswitch_3
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Loxk;->c:Ljava/lang/Float;

    goto :goto_0

    .line 662
    :sswitch_4
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Loxk;->d:Ljava/lang/Float;

    goto :goto_0

    .line 666
    :sswitch_5
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Loxk;->e:Ljava/lang/Float;

    goto :goto_0

    .line 670
    :sswitch_6
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Loxk;->f:Ljava/lang/Float;

    goto :goto_0

    .line 674
    :sswitch_7
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Loxk;->g:Ljava/lang/Float;

    goto :goto_0

    .line 678
    :sswitch_8
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Loxk;->h:Ljava/lang/Float;

    goto :goto_0

    .line 682
    :sswitch_9
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Loxk;->i:Ljava/lang/Float;

    goto :goto_0

    .line 635
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x15 -> :sswitch_2
        0x1d -> :sswitch_3
        0x25 -> :sswitch_4
        0x2d -> :sswitch_5
        0x35 -> :sswitch_6
        0x3d -> :sswitch_7
        0x45 -> :sswitch_8
        0x4d -> :sswitch_9
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 590
    const/4 v0, 0x1

    iget-object v1, p0, Loxk;->a:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 591
    const/4 v0, 0x2

    iget-object v1, p0, Loxk;->b:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 592
    const/4 v0, 0x3

    iget-object v1, p0, Loxk;->c:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 593
    const/4 v0, 0x4

    iget-object v1, p0, Loxk;->d:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 594
    const/4 v0, 0x5

    iget-object v1, p0, Loxk;->e:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 595
    const/4 v0, 0x6

    iget-object v1, p0, Loxk;->f:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 596
    const/4 v0, 0x7

    iget-object v1, p0, Loxk;->g:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 597
    const/16 v0, 0x8

    iget-object v1, p0, Loxk;->h:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 598
    const/16 v0, 0x9

    iget-object v1, p0, Loxk;->i:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 599
    iget-object v0, p0, Loxk;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 601
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 563
    invoke-virtual {p0, p1}, Loxk;->a(Loxn;)Loxk;

    move-result-object v0

    return-object v0
.end method
