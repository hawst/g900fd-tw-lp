.class public final Lmue;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmue;


# instance fields
.field private b:Ljava/lang/Long;

.field private c:Ljava/lang/Long;

.field private d:Ljava/lang/Boolean;

.field private e:Ljava/lang/Integer;

.field private f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3688
    const/4 v0, 0x0

    new-array v0, v0, [Lmue;

    sput-object v0, Lmue;->a:[Lmue;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3689
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 3725
    const/4 v0, 0x0

    .line 3726
    iget-object v1, p0, Lmue;->b:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 3727
    const/4 v0, 0x1

    iget-object v1, p0, Lmue;->b:Ljava/lang/Long;

    .line 3728
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Loxo;->e(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3730
    :cond_0
    iget-object v1, p0, Lmue;->c:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 3731
    const/4 v1, 0x2

    iget-object v2, p0, Lmue;->c:Ljava/lang/Long;

    .line 3732
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3734
    :cond_1
    iget-object v1, p0, Lmue;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 3735
    const/4 v1, 0x3

    iget-object v2, p0, Lmue;->d:Ljava/lang/Boolean;

    .line 3736
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3738
    :cond_2
    iget-object v1, p0, Lmue;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 3739
    const/4 v1, 0x4

    iget-object v2, p0, Lmue;->e:Ljava/lang/Integer;

    .line 3740
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3742
    :cond_3
    iget-object v1, p0, Lmue;->f:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 3743
    const/4 v1, 0x5

    iget-object v2, p0, Lmue;->f:Ljava/lang/String;

    .line 3744
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3746
    :cond_4
    iget-object v1, p0, Lmue;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3747
    iput v0, p0, Lmue;->ai:I

    .line 3748
    return v0
.end method

.method public a(Loxn;)Lmue;
    .locals 2

    .prologue
    .line 3756
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3757
    sparse-switch v0, :sswitch_data_0

    .line 3761
    iget-object v1, p0, Lmue;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 3762
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmue;->ah:Ljava/util/List;

    .line 3765
    :cond_1
    iget-object v1, p0, Lmue;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3767
    :sswitch_0
    return-object p0

    .line 3772
    :sswitch_1
    invoke-virtual {p1}, Loxn;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lmue;->b:Ljava/lang/Long;

    goto :goto_0

    .line 3776
    :sswitch_2
    invoke-virtual {p1}, Loxn;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lmue;->c:Ljava/lang/Long;

    goto :goto_0

    .line 3780
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmue;->d:Ljava/lang/Boolean;

    goto :goto_0

    .line 3784
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmue;->e:Ljava/lang/Integer;

    goto :goto_0

    .line 3788
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmue;->f:Ljava/lang/String;

    goto :goto_0

    .line 3757
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 3704
    iget-object v0, p0, Lmue;->b:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 3705
    const/4 v0, 0x1

    iget-object v1, p0, Lmue;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(IJ)V

    .line 3707
    :cond_0
    iget-object v0, p0, Lmue;->c:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 3708
    const/4 v0, 0x2

    iget-object v1, p0, Lmue;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(IJ)V

    .line 3710
    :cond_1
    iget-object v0, p0, Lmue;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 3711
    const/4 v0, 0x3

    iget-object v1, p0, Lmue;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 3713
    :cond_2
    iget-object v0, p0, Lmue;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 3714
    const/4 v0, 0x4

    iget-object v1, p0, Lmue;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 3716
    :cond_3
    iget-object v0, p0, Lmue;->f:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 3717
    const/4 v0, 0x5

    iget-object v1, p0, Lmue;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3719
    :cond_4
    iget-object v0, p0, Lmue;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3721
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3685
    invoke-virtual {p0, p1}, Lmue;->a(Loxn;)Lmue;

    move-result-object v0

    return-object v0
.end method
