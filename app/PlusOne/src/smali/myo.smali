.class public final Lmyo;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 814
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 835
    const/4 v0, 0x0

    .line 836
    iget-object v1, p0, Lmyo;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 837
    const/4 v0, 0x1

    iget-object v1, p0, Lmyo;->a:Ljava/lang/String;

    .line 838
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 840
    :cond_0
    iget-object v1, p0, Lmyo;->b:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 841
    const/4 v1, 0x2

    iget-object v2, p0, Lmyo;->b:Ljava/lang/Long;

    .line 842
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 844
    :cond_1
    iget-object v1, p0, Lmyo;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 845
    iput v0, p0, Lmyo;->ai:I

    .line 846
    return v0
.end method

.method public a(Loxn;)Lmyo;
    .locals 2

    .prologue
    .line 854
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 855
    sparse-switch v0, :sswitch_data_0

    .line 859
    iget-object v1, p0, Lmyo;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 860
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmyo;->ah:Ljava/util/List;

    .line 863
    :cond_1
    iget-object v1, p0, Lmyo;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 865
    :sswitch_0
    return-object p0

    .line 870
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmyo;->a:Ljava/lang/String;

    goto :goto_0

    .line 874
    :sswitch_2
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lmyo;->b:Ljava/lang/Long;

    goto :goto_0

    .line 855
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 823
    iget-object v0, p0, Lmyo;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 824
    const/4 v0, 0x1

    iget-object v1, p0, Lmyo;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 826
    :cond_0
    iget-object v0, p0, Lmyo;->b:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 827
    const/4 v0, 0x2

    iget-object v1, p0, Lmyo;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 829
    :cond_1
    iget-object v0, p0, Lmyo;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 831
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 810
    invoke-virtual {p0, p1}, Lmyo;->a(Loxn;)Lmyo;

    move-result-object v0

    return-object v0
.end method
