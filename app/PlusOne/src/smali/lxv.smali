.class public final Llxv;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Llxv;


# instance fields
.field private b:Llxp;

.field private c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3882
    const/4 v0, 0x0

    new-array v0, v0, [Llxv;

    sput-object v0, Llxv;->a:[Llxv;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3883
    invoke-direct {p0}, Loxq;-><init>()V

    .line 3886
    const/4 v0, 0x0

    iput-object v0, p0, Llxv;->b:Llxp;

    .line 3883
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 3905
    const/4 v0, 0x0

    .line 3906
    iget-object v1, p0, Llxv;->b:Llxp;

    if-eqz v1, :cond_0

    .line 3907
    const/4 v0, 0x1

    iget-object v1, p0, Llxv;->b:Llxp;

    .line 3908
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3910
    :cond_0
    iget-object v1, p0, Llxv;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 3911
    const/4 v1, 0x2

    iget-object v2, p0, Llxv;->c:Ljava/lang/String;

    .line 3912
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3914
    :cond_1
    iget-object v1, p0, Llxv;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3915
    iput v0, p0, Llxv;->ai:I

    .line 3916
    return v0
.end method

.method public a(Loxn;)Llxv;
    .locals 2

    .prologue
    .line 3924
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3925
    sparse-switch v0, :sswitch_data_0

    .line 3929
    iget-object v1, p0, Llxv;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 3930
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llxv;->ah:Ljava/util/List;

    .line 3933
    :cond_1
    iget-object v1, p0, Llxv;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3935
    :sswitch_0
    return-object p0

    .line 3940
    :sswitch_1
    iget-object v0, p0, Llxv;->b:Llxp;

    if-nez v0, :cond_2

    .line 3941
    new-instance v0, Llxp;

    invoke-direct {v0}, Llxp;-><init>()V

    iput-object v0, p0, Llxv;->b:Llxp;

    .line 3943
    :cond_2
    iget-object v0, p0, Llxv;->b:Llxp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3947
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llxv;->c:Ljava/lang/String;

    goto :goto_0

    .line 3925
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 3893
    iget-object v0, p0, Llxv;->b:Llxp;

    if-eqz v0, :cond_0

    .line 3894
    const/4 v0, 0x1

    iget-object v1, p0, Llxv;->b:Llxp;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 3896
    :cond_0
    iget-object v0, p0, Llxv;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 3897
    const/4 v0, 0x2

    iget-object v1, p0, Llxv;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3899
    :cond_1
    iget-object v0, p0, Llxv;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3901
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3879
    invoke-virtual {p0, p1}, Llxv;->a(Loxn;)Llxv;

    move-result-object v0

    return-object v0
.end method
