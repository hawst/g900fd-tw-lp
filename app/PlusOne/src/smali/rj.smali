.class public final Lrj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lsa;


# instance fields
.field a:Landroid/view/LayoutInflater;

.field b:Lrl;

.field c:I

.field private d:Landroid/content/Context;

.field private e:Landroid/support/v7/internal/view/menu/ExpandedMenuView;

.field private f:I

.field private g:Lsb;

.field private h:Lrk;


# direct methods
.method public constructor <init>(II)V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    iput p1, p0, Lrj;->c:I

    .line 79
    iput p2, p0, Lrj;->f:I

    .line 80
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Lrj;-><init>(II)V

    .line 68
    iput-object p1, p0, Lrj;->d:Landroid/content/Context;

    .line 69
    iget-object v0, p0, Lrj;->d:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lrj;->a:Landroid/view/LayoutInflater;

    .line 70
    return-void
.end method

.method static synthetic c()I
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public a()Landroid/widget/ListAdapter;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lrj;->h:Lrk;

    if-nez v0, :cond_0

    .line 122
    new-instance v0, Lrk;

    invoke-direct {v0, p0}, Lrk;-><init>(Lrj;)V

    iput-object v0, p0, Lrj;->h:Lrk;

    .line 124
    :cond_0
    iget-object v0, p0, Lrj;->h:Lrk;

    return-object v0
.end method

.method public a(Landroid/view/ViewGroup;)Lsc;
    .locals 3

    .prologue
    .line 101
    iget-object v0, p0, Lrj;->e:Landroid/support/v7/internal/view/menu/ExpandedMenuView;

    if-nez v0, :cond_1

    .line 102
    iget-object v0, p0, Lrj;->a:Landroid/view/LayoutInflater;

    const v1, 0x7f04000a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/view/menu/ExpandedMenuView;

    iput-object v0, p0, Lrj;->e:Landroid/support/v7/internal/view/menu/ExpandedMenuView;

    .line 104
    iget-object v0, p0, Lrj;->h:Lrk;

    if-nez v0, :cond_0

    .line 105
    new-instance v0, Lrk;

    invoke-direct {v0, p0}, Lrk;-><init>(Lrj;)V

    iput-object v0, p0, Lrj;->h:Lrk;

    .line 107
    :cond_0
    iget-object v0, p0, Lrj;->e:Landroid/support/v7/internal/view/menu/ExpandedMenuView;

    iget-object v1, p0, Lrj;->h:Lrk;

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/ExpandedMenuView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 108
    iget-object v0, p0, Lrj;->e:Landroid/support/v7/internal/view/menu/ExpandedMenuView;

    invoke-virtual {v0, p0}, Landroid/support/v7/internal/view/menu/ExpandedMenuView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 110
    :cond_1
    iget-object v0, p0, Lrj;->e:Landroid/support/v7/internal/view/menu/ExpandedMenuView;

    return-object v0
.end method

.method public a(Landroid/content/Context;Lrl;)V
    .locals 2

    .prologue
    .line 84
    iget v0, p0, Lrj;->f:I

    if-eqz v0, :cond_2

    .line 85
    new-instance v0, Landroid/view/ContextThemeWrapper;

    iget v1, p0, Lrj;->f:I

    invoke-direct {v0, p1, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lrj;->d:Landroid/content/Context;

    .line 86
    iget-object v0, p0, Lrj;->d:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lrj;->a:Landroid/view/LayoutInflater;

    .line 93
    :cond_0
    :goto_0
    iput-object p2, p0, Lrj;->b:Lrl;

    .line 94
    iget-object v0, p0, Lrj;->h:Lrk;

    if-eqz v0, :cond_1

    .line 95
    iget-object v0, p0, Lrj;->h:Lrk;

    invoke-virtual {v0}, Lrk;->notifyDataSetChanged()V

    .line 97
    :cond_1
    return-void

    .line 87
    :cond_2
    iget-object v0, p0, Lrj;->d:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 88
    iput-object p1, p0, Lrj;->d:Landroid/content/Context;

    .line 89
    iget-object v0, p0, Lrj;->a:Landroid/view/LayoutInflater;

    if-nez v0, :cond_0

    .line 90
    iget-object v0, p0, Lrj;->d:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lrj;->a:Landroid/view/LayoutInflater;

    goto :goto_0
.end method

.method public a(Lrl;Z)V
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lrj;->g:Lsb;

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lrj;->g:Lsb;

    invoke-interface {v0, p1, p2}, Lsb;->a(Lrl;Z)V

    .line 154
    :cond_0
    return-void
.end method

.method public a(Lsb;)V
    .locals 0

    .prologue
    .line 134
    iput-object p1, p0, Lrj;->g:Lsb;

    .line 135
    return-void
.end method

.method public a(Lsg;)Z
    .locals 2

    .prologue
    .line 139
    invoke-virtual {p1}, Lsg;->hasVisibleItems()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 146
    :goto_0
    return v0

    .line 142
    :cond_0
    new-instance v0, Lro;

    invoke-direct {v0, p1}, Lro;-><init>(Lrl;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lro;->a(Landroid/os/IBinder;)V

    .line 143
    iget-object v0, p0, Lrj;->g:Lsb;

    if-eqz v0, :cond_1

    .line 144
    iget-object v0, p0, Lrj;->g:Lsb;

    invoke-interface {v0, p1}, Lsb;->a(Lrl;)Z

    .line 146
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lrj;->h:Lrk;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lrj;->h:Lrk;

    invoke-virtual {v0}, Lrk;->notifyDataSetChanged()V

    .line 130
    :cond_0
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 174
    const/4 v0, 0x0

    return v0
.end method

.method public b(Lrp;)Z
    .locals 1

    .prologue
    .line 178
    const/4 v0, 0x0

    return v0
.end method

.method public c(Lrp;)Z
    .locals 1

    .prologue
    .line 182
    const/4 v0, 0x0

    return v0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 169
    iget-object v0, p0, Lrj;->b:Lrl;

    iget-object v1, p0, Lrj;->h:Lrk;

    invoke-virtual {v1, p3}, Lrk;->a(I)Lrp;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Lrl;->a(Landroid/view/MenuItem;Lsa;I)Z

    .line 170
    return-void
.end method
