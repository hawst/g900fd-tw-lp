.class public final Lnef;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Lnei;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1358
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1365
    const/4 v0, 0x0

    iput-object v0, p0, Lnef;->c:Lnei;

    .line 1358
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1390
    const/4 v0, 0x0

    .line 1391
    iget-object v1, p0, Lnef;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1392
    const/4 v0, 0x1

    iget-object v1, p0, Lnef;->a:Ljava/lang/String;

    .line 1393
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1395
    :cond_0
    iget-object v1, p0, Lnef;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1396
    const/4 v1, 0x2

    iget-object v2, p0, Lnef;->b:Ljava/lang/String;

    .line 1397
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1399
    :cond_1
    iget-object v1, p0, Lnef;->c:Lnei;

    if-eqz v1, :cond_2

    .line 1400
    const/4 v1, 0x3

    iget-object v2, p0, Lnef;->c:Lnei;

    .line 1401
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1403
    :cond_2
    iget-object v1, p0, Lnef;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 1404
    const/4 v1, 0x4

    iget-object v2, p0, Lnef;->d:Ljava/lang/String;

    .line 1405
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1407
    :cond_3
    iget-object v1, p0, Lnef;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1408
    iput v0, p0, Lnef;->ai:I

    .line 1409
    return v0
.end method

.method public a(Loxn;)Lnef;
    .locals 2

    .prologue
    .line 1417
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1418
    sparse-switch v0, :sswitch_data_0

    .line 1422
    iget-object v1, p0, Lnef;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1423
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnef;->ah:Ljava/util/List;

    .line 1426
    :cond_1
    iget-object v1, p0, Lnef;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1428
    :sswitch_0
    return-object p0

    .line 1433
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnef;->a:Ljava/lang/String;

    goto :goto_0

    .line 1437
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnef;->b:Ljava/lang/String;

    goto :goto_0

    .line 1441
    :sswitch_3
    iget-object v0, p0, Lnef;->c:Lnei;

    if-nez v0, :cond_2

    .line 1442
    new-instance v0, Lnei;

    invoke-direct {v0}, Lnei;-><init>()V

    iput-object v0, p0, Lnef;->c:Lnei;

    .line 1444
    :cond_2
    iget-object v0, p0, Lnef;->c:Lnei;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1448
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnef;->d:Ljava/lang/String;

    goto :goto_0

    .line 1418
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1372
    iget-object v0, p0, Lnef;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1373
    const/4 v0, 0x1

    iget-object v1, p0, Lnef;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1375
    :cond_0
    iget-object v0, p0, Lnef;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1376
    const/4 v0, 0x2

    iget-object v1, p0, Lnef;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1378
    :cond_1
    iget-object v0, p0, Lnef;->c:Lnei;

    if-eqz v0, :cond_2

    .line 1379
    const/4 v0, 0x3

    iget-object v1, p0, Lnef;->c:Lnei;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1381
    :cond_2
    iget-object v0, p0, Lnef;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1382
    const/4 v0, 0x4

    iget-object v1, p0, Lnef;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1384
    :cond_3
    iget-object v0, p0, Lnef;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1386
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1354
    invoke-virtual {p0, p1}, Lnef;->a(Loxn;)Lnef;

    move-result-object v0

    return-object v0
.end method
