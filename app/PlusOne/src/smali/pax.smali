.class public final Lpax;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lpax;


# instance fields
.field public b:Lpaf;

.field public c:I

.field public d:Ljava/lang/Integer;

.field public e:Lpaf;

.field public f:Ljava/lang/Boolean;

.field public g:Ljava/lang/Boolean;

.field public h:I

.field private i:Lpcu;

.field private j:Lpcu;

.field private k:[Lpbb;

.field private l:Ljava/lang/String;

.field private m:Lltm;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 784
    const/4 v0, 0x0

    new-array v0, v0, [Lpax;

    sput-object v0, Lpax;->a:[Lpax;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    const/4 v1, 0x0

    .line 785
    invoke-direct {p0}, Loxq;-><init>()V

    .line 794
    iput-object v1, p0, Lpax;->b:Lpaf;

    .line 797
    iput-object v1, p0, Lpax;->i:Lpcu;

    .line 800
    iput v2, p0, Lpax;->c:I

    .line 805
    iput-object v1, p0, Lpax;->e:Lpaf;

    .line 808
    iput-object v1, p0, Lpax;->j:Lpcu;

    .line 813
    sget-object v0, Lpbb;->a:[Lpbb;

    iput-object v0, p0, Lpax;->k:[Lpbb;

    .line 820
    iput v2, p0, Lpax;->h:I

    .line 823
    iput-object v1, p0, Lpax;->m:Lltm;

    .line 785
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/high16 v6, -0x80000000

    .line 874
    .line 875
    iget-object v0, p0, Lpax;->b:Lpaf;

    if-eqz v0, :cond_c

    .line 876
    const/4 v0, 0x1

    iget-object v2, p0, Lpax;->b:Lpaf;

    .line 877
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 879
    :goto_0
    iget v2, p0, Lpax;->c:I

    if-eq v2, v6, :cond_0

    .line 880
    const/4 v2, 0x2

    iget v3, p0, Lpax;->c:I

    .line 881
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 883
    :cond_0
    iget-object v2, p0, Lpax;->d:Ljava/lang/Integer;

    if-eqz v2, :cond_1

    .line 884
    const/4 v2, 0x3

    iget-object v3, p0, Lpax;->d:Ljava/lang/Integer;

    .line 885
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 887
    :cond_1
    iget-object v2, p0, Lpax;->e:Lpaf;

    if-eqz v2, :cond_2

    .line 888
    const/4 v2, 0x4

    iget-object v3, p0, Lpax;->e:Lpaf;

    .line 889
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 891
    :cond_2
    iget-object v2, p0, Lpax;->f:Ljava/lang/Boolean;

    if-eqz v2, :cond_3

    .line 892
    const/4 v2, 0x5

    iget-object v3, p0, Lpax;->f:Ljava/lang/Boolean;

    .line 893
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 895
    :cond_3
    iget-object v2, p0, Lpax;->k:[Lpbb;

    if-eqz v2, :cond_5

    .line 896
    iget-object v2, p0, Lpax;->k:[Lpbb;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    .line 897
    if-eqz v4, :cond_4

    .line 898
    const/4 v5, 0x6

    .line 899
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 896
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 903
    :cond_5
    iget-object v1, p0, Lpax;->g:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    .line 904
    const/4 v1, 0x7

    iget-object v2, p0, Lpax;->g:Ljava/lang/Boolean;

    .line 905
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 907
    :cond_6
    iget-object v1, p0, Lpax;->l:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 908
    const/16 v1, 0x8

    iget-object v2, p0, Lpax;->l:Ljava/lang/String;

    .line 909
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 911
    :cond_7
    iget-object v1, p0, Lpax;->i:Lpcu;

    if-eqz v1, :cond_8

    .line 912
    const/16 v1, 0x9

    iget-object v2, p0, Lpax;->i:Lpcu;

    .line 913
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 915
    :cond_8
    iget-object v1, p0, Lpax;->m:Lltm;

    if-eqz v1, :cond_9

    .line 916
    const/16 v1, 0xa

    iget-object v2, p0, Lpax;->m:Lltm;

    .line 917
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 919
    :cond_9
    iget-object v1, p0, Lpax;->j:Lpcu;

    if-eqz v1, :cond_a

    .line 920
    const/16 v1, 0xb

    iget-object v2, p0, Lpax;->j:Lpcu;

    .line 921
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 923
    :cond_a
    iget v1, p0, Lpax;->h:I

    if-eq v1, v6, :cond_b

    .line 924
    const/16 v1, 0xc

    iget v2, p0, Lpax;->h:I

    .line 925
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 927
    :cond_b
    iget-object v1, p0, Lpax;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 928
    iput v0, p0, Lpax;->ai:I

    .line 929
    return v0

    :cond_c
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lpax;
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v1, 0x0

    const/4 v4, 0x1

    .line 937
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 938
    sparse-switch v0, :sswitch_data_0

    .line 942
    iget-object v2, p0, Lpax;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 943
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lpax;->ah:Ljava/util/List;

    .line 946
    :cond_1
    iget-object v2, p0, Lpax;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 948
    :sswitch_0
    return-object p0

    .line 953
    :sswitch_1
    iget-object v0, p0, Lpax;->b:Lpaf;

    if-nez v0, :cond_2

    .line 954
    new-instance v0, Lpaf;

    invoke-direct {v0}, Lpaf;-><init>()V

    iput-object v0, p0, Lpax;->b:Lpaf;

    .line 956
    :cond_2
    iget-object v0, p0, Lpax;->b:Lpaf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 960
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 961
    if-eq v0, v4, :cond_3

    if-eq v0, v5, :cond_3

    if-eq v0, v6, :cond_3

    const/4 v2, 0x4

    if-eq v0, v2, :cond_3

    const/4 v2, 0x5

    if-eq v0, v2, :cond_3

    const/4 v2, 0x6

    if-eq v0, v2, :cond_3

    const/4 v2, 0x7

    if-ne v0, v2, :cond_4

    .line 968
    :cond_3
    iput v0, p0, Lpax;->c:I

    goto :goto_0

    .line 970
    :cond_4
    iput v4, p0, Lpax;->c:I

    goto :goto_0

    .line 975
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lpax;->d:Ljava/lang/Integer;

    goto :goto_0

    .line 979
    :sswitch_4
    iget-object v0, p0, Lpax;->e:Lpaf;

    if-nez v0, :cond_5

    .line 980
    new-instance v0, Lpaf;

    invoke-direct {v0}, Lpaf;-><init>()V

    iput-object v0, p0, Lpax;->e:Lpaf;

    .line 982
    :cond_5
    iget-object v0, p0, Lpax;->e:Lpaf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 986
    :sswitch_5
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lpax;->f:Ljava/lang/Boolean;

    goto :goto_0

    .line 990
    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 991
    iget-object v0, p0, Lpax;->k:[Lpbb;

    if-nez v0, :cond_7

    move v0, v1

    .line 992
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lpbb;

    .line 993
    iget-object v3, p0, Lpax;->k:[Lpbb;

    if-eqz v3, :cond_6

    .line 994
    iget-object v3, p0, Lpax;->k:[Lpbb;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 996
    :cond_6
    iput-object v2, p0, Lpax;->k:[Lpbb;

    .line 997
    :goto_2
    iget-object v2, p0, Lpax;->k:[Lpbb;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_8

    .line 998
    iget-object v2, p0, Lpax;->k:[Lpbb;

    new-instance v3, Lpbb;

    invoke-direct {v3}, Lpbb;-><init>()V

    aput-object v3, v2, v0

    .line 999
    iget-object v2, p0, Lpax;->k:[Lpbb;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1000
    invoke-virtual {p1}, Loxn;->a()I

    .line 997
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 991
    :cond_7
    iget-object v0, p0, Lpax;->k:[Lpbb;

    array-length v0, v0

    goto :goto_1

    .line 1003
    :cond_8
    iget-object v2, p0, Lpax;->k:[Lpbb;

    new-instance v3, Lpbb;

    invoke-direct {v3}, Lpbb;-><init>()V

    aput-object v3, v2, v0

    .line 1004
    iget-object v2, p0, Lpax;->k:[Lpbb;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1008
    :sswitch_7
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lpax;->g:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 1012
    :sswitch_8
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpax;->l:Ljava/lang/String;

    goto/16 :goto_0

    .line 1016
    :sswitch_9
    iget-object v0, p0, Lpax;->i:Lpcu;

    if-nez v0, :cond_9

    .line 1017
    new-instance v0, Lpcu;

    invoke-direct {v0}, Lpcu;-><init>()V

    iput-object v0, p0, Lpax;->i:Lpcu;

    .line 1019
    :cond_9
    iget-object v0, p0, Lpax;->i:Lpcu;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1023
    :sswitch_a
    iget-object v0, p0, Lpax;->m:Lltm;

    if-nez v0, :cond_a

    .line 1024
    new-instance v0, Lltm;

    invoke-direct {v0}, Lltm;-><init>()V

    iput-object v0, p0, Lpax;->m:Lltm;

    .line 1026
    :cond_a
    iget-object v0, p0, Lpax;->m:Lltm;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1030
    :sswitch_b
    iget-object v0, p0, Lpax;->j:Lpcu;

    if-nez v0, :cond_b

    .line 1031
    new-instance v0, Lpcu;

    invoke-direct {v0}, Lpcu;-><init>()V

    iput-object v0, p0, Lpax;->j:Lpcu;

    .line 1033
    :cond_b
    iget-object v0, p0, Lpax;->j:Lpcu;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1037
    :sswitch_c
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1038
    if-eq v0, v4, :cond_c

    if-eq v0, v5, :cond_c

    if-ne v0, v6, :cond_d

    .line 1041
    :cond_c
    iput v0, p0, Lpax;->h:I

    goto/16 :goto_0

    .line 1043
    :cond_d
    iput v4, p0, Lpax;->h:I

    goto/16 :goto_0

    .line 938
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x60 -> :sswitch_c
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/high16 v5, -0x80000000

    .line 828
    iget-object v0, p0, Lpax;->b:Lpaf;

    if-eqz v0, :cond_0

    .line 829
    const/4 v0, 0x1

    iget-object v1, p0, Lpax;->b:Lpaf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 831
    :cond_0
    iget v0, p0, Lpax;->c:I

    if-eq v0, v5, :cond_1

    .line 832
    const/4 v0, 0x2

    iget v1, p0, Lpax;->c:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 834
    :cond_1
    iget-object v0, p0, Lpax;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 835
    const/4 v0, 0x3

    iget-object v1, p0, Lpax;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 837
    :cond_2
    iget-object v0, p0, Lpax;->e:Lpaf;

    if-eqz v0, :cond_3

    .line 838
    const/4 v0, 0x4

    iget-object v1, p0, Lpax;->e:Lpaf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 840
    :cond_3
    iget-object v0, p0, Lpax;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 841
    const/4 v0, 0x5

    iget-object v1, p0, Lpax;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 843
    :cond_4
    iget-object v0, p0, Lpax;->k:[Lpbb;

    if-eqz v0, :cond_6

    .line 844
    iget-object v1, p0, Lpax;->k:[Lpbb;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_6

    aget-object v3, v1, v0

    .line 845
    if-eqz v3, :cond_5

    .line 846
    const/4 v4, 0x6

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 844
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 850
    :cond_6
    iget-object v0, p0, Lpax;->g:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    .line 851
    const/4 v0, 0x7

    iget-object v1, p0, Lpax;->g:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 853
    :cond_7
    iget-object v0, p0, Lpax;->l:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 854
    const/16 v0, 0x8

    iget-object v1, p0, Lpax;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 856
    :cond_8
    iget-object v0, p0, Lpax;->i:Lpcu;

    if-eqz v0, :cond_9

    .line 857
    const/16 v0, 0x9

    iget-object v1, p0, Lpax;->i:Lpcu;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 859
    :cond_9
    iget-object v0, p0, Lpax;->m:Lltm;

    if-eqz v0, :cond_a

    .line 860
    const/16 v0, 0xa

    iget-object v1, p0, Lpax;->m:Lltm;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 862
    :cond_a
    iget-object v0, p0, Lpax;->j:Lpcu;

    if-eqz v0, :cond_b

    .line 863
    const/16 v0, 0xb

    iget-object v1, p0, Lpax;->j:Lpcu;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 865
    :cond_b
    iget v0, p0, Lpax;->h:I

    if-eq v0, v5, :cond_c

    .line 866
    const/16 v0, 0xc

    iget v1, p0, Lpax;->h:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 868
    :cond_c
    iget-object v0, p0, Lpax;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 870
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 781
    invoke-virtual {p0, p1}, Lpax;->a(Loxn;)Lpax;

    move-result-object v0

    return-object v0
.end method
