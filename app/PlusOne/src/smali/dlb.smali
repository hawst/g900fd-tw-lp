.class public final Ldlb;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmgm;",
        "Lmgn;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljeo;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjeo;)V
    .locals 6

    .prologue
    .line 24
    const-string v3, "photosmoviemakeredits"

    new-instance v4, Lmgm;

    invoke-direct {v4}, Lmgm;-><init>()V

    new-instance v5, Lmgn;

    invoke-direct {v5}, Lmgn;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 27
    iput-object p3, p0, Ldlb;->a:Ljeo;

    .line 28
    return-void
.end method

.method private static a(Ljeq;)Lndc;
    .locals 8

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 120
    if-nez p0, :cond_0

    .line 121
    const/4 v0, 0x0

    .line 129
    :goto_0
    return-object v0

    .line 123
    :cond_0
    new-instance v4, Lndc;

    invoke-direct {v4}, Lndc;-><init>()V

    .line 124
    iget v0, p0, Ljeq;->a:I

    packed-switch v0, :pswitch_data_0

    move v0, v3

    :goto_1
    iput v0, v4, Lndc;->b:I

    .line 125
    iget-object v0, p0, Ljeq;->b:Ljava/lang/String;

    iput-object v0, v4, Lndc;->c:Ljava/lang/String;

    .line 126
    iget-wide v6, p0, Ljeq;->c:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v4, Lndc;->d:Ljava/lang/Long;

    .line 127
    iget-wide v6, p0, Ljeq;->d:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v4, Lndc;->e:Ljava/lang/Long;

    .line 128
    iget v0, p0, Ljeq;->e:I

    packed-switch v0, :pswitch_data_1

    move v1, v3

    :goto_2
    :pswitch_0
    iput v1, v4, Lndc;->f:I

    move-object v0, v4

    .line 129
    goto :goto_0

    :pswitch_1
    move v0, v1

    .line 124
    goto :goto_1

    :pswitch_2
    move v0, v2

    goto :goto_1

    :pswitch_3
    move v1, v2

    .line 128
    goto :goto_2

    :pswitch_4
    const/4 v1, 0x3

    goto :goto_2

    :pswitch_5
    const/4 v1, 0x4

    goto :goto_2

    :pswitch_6
    const/4 v1, 0x5

    goto :goto_2

    .line 124
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 128
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private static a(Ljava/util/List;)[Lndc;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljeq;",
            ">;)[",
            "Lndc;"
        }
    .end annotation

    .prologue
    .line 108
    if-nez p0, :cond_0

    .line 109
    const/4 v0, 0x0

    .line 116
    :goto_0
    return-object v0

    .line 111
    :cond_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    .line 112
    new-array v1, v3, [Lndc;

    .line 113
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_1

    .line 114
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljeq;

    invoke-static {v0}, Ldlb;->a(Ljeq;)Lndc;

    move-result-object v0

    aput-object v0, v1, v2

    .line 113
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 116
    goto :goto_0
.end method


# virtual methods
.method protected a(Lmgm;)V
    .locals 10

    .prologue
    .line 32
    iget-object v6, p0, Ldlb;->a:Ljeo;

    if-nez v6, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p1, Lmgm;->a:Lndb;

    .line 34
    return-void

    .line 32
    :cond_0
    new-instance v4, Lndb;

    invoke-direct {v4}, Lndb;-><init>()V

    iget-object v1, v6, Ljeo;->a:Ljeu;

    if-nez v1, :cond_1

    const/4 v0, 0x0

    :goto_1
    iput-object v0, v4, Lndb;->a:Lnde;

    iget-object v0, v6, Ljeo;->b:Ljew;

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :goto_2
    iput-object v0, v4, Lndb;->b:Lndf;

    iget-object v7, v6, Ljeo;->c:Ljava/util/List;

    if-nez v7, :cond_4

    const/4 v0, 0x0

    :goto_3
    iput-object v0, v4, Lndb;->c:[Lndd;

    iget v0, v6, Ljeo;->d:I

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_4
    iput v0, v4, Lndb;->d:I

    iget v0, v6, Ljeo;->e:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v4, Lndb;->e:Ljava/lang/Integer;

    move-object v0, v4

    goto :goto_0

    :cond_1
    new-instance v0, Lnde;

    invoke-direct {v0}, Lnde;-><init>()V

    iget-object v2, v1, Ljeu;->a:Ljava/lang/String;

    iput-object v2, v0, Lnde;->a:Ljava/lang/String;

    iget-object v2, v1, Ljeu;->b:Ljava/lang/String;

    iput-object v2, v0, Lnde;->b:Ljava/lang/String;

    iget-object v1, v1, Ljeu;->c:Ljava/lang/String;

    iput-object v1, v0, Lnde;->c:Ljava/lang/String;

    goto :goto_1

    :cond_2
    new-instance v1, Lndf;

    invoke-direct {v1}, Lndf;-><init>()V

    iget-object v2, v0, Ljew;->a:Ljava/util/List;

    invoke-static {v2}, Ldlb;->a(Ljava/util/List;)[Lndc;

    move-result-object v2

    iput-object v2, v1, Lndf;->a:[Lndc;

    iget-object v2, v0, Ljew;->b:Ljava/util/List;

    invoke-static {v2}, Ldlb;->a(Ljava/util/List;)[Lndc;

    move-result-object v2

    iput-object v2, v1, Lndf;->b:[Lndc;

    iget-object v2, v0, Ljew;->c:Ljava/util/List;

    invoke-static {v2}, Ldlb;->a(Ljava/util/List;)[Lndc;

    move-result-object v2

    iput-object v2, v1, Lndf;->c:[Lndc;

    iget-wide v2, v0, Ljew;->d:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lndf;->d:Ljava/lang/Long;

    iget-object v2, v0, Ljew;->e:Ljey;

    if-nez v2, :cond_3

    const/4 v0, 0x0

    :goto_5
    iput-object v0, v1, Lndf;->e:Lndg;

    move-object v0, v1

    goto :goto_2

    :cond_3
    new-instance v0, Lndg;

    invoke-direct {v0}, Lndg;-><init>()V

    iget-wide v8, v2, Ljey;->a:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iput-object v3, v0, Lndg;->a:Ljava/lang/Long;

    iget-wide v8, v2, Ljey;->b:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iput-object v3, v0, Lndg;->b:Ljava/lang/Long;

    iget v3, v2, Ljey;->c:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v0, Lndg;->c:Ljava/lang/Integer;

    iget-object v3, v2, Ljey;->d:Ljava/util/List;

    invoke-static {v3}, Ldlb;->a(Ljava/util/List;)[Lndc;

    move-result-object v3

    iput-object v3, v0, Lndg;->d:[Lndc;

    iget-object v2, v2, Ljey;->e:Ljava/util/List;

    invoke-static {v2}, Ldlb;->a(Ljava/util/List;)[Lndc;

    move-result-object v2

    iput-object v2, v0, Lndg;->e:[Lndc;

    goto :goto_5

    :cond_4
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v8

    new-array v3, v8, [Lndd;

    const/4 v0, 0x0

    move v5, v0

    :goto_6
    if-ge v5, v8, :cond_6

    invoke-interface {v7, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljes;

    if-nez v0, :cond_5

    const/4 v0, 0x0

    :goto_7
    aput-object v0, v3, v5

    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_6

    :cond_5
    new-instance v2, Lndd;

    invoke-direct {v2}, Lndd;-><init>()V

    iget v1, v0, Ljes;->a:I

    packed-switch v1, :pswitch_data_1

    const/4 v1, 0x0

    :goto_8
    iput v1, v2, Lndd;->b:I

    iget-object v1, v0, Ljes;->b:Ljeq;

    invoke-static {v1}, Ldlb;->a(Ljeq;)Lndc;

    move-result-object v1

    iput-object v1, v2, Lndd;->c:Lndc;

    iget v1, v0, Ljes;->c:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v2, Lndd;->d:Ljava/lang/Integer;

    iget-object v1, v0, Ljes;->d:Ljeq;

    invoke-static {v1}, Ldlb;->a(Ljeq;)Lndc;

    move-result-object v1

    iput-object v1, v2, Lndd;->e:Lndc;

    iget v1, v0, Ljes;->e:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v2, Lndd;->f:Ljava/lang/Integer;

    iget-wide v0, v0, Ljes;->f:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v2, Lndd;->g:Ljava/lang/Long;

    move-object v0, v2

    goto :goto_7

    :pswitch_0
    const/4 v1, 0x1

    goto :goto_8

    :pswitch_1
    const/4 v1, 0x2

    goto :goto_8

    :pswitch_2
    const/4 v1, 0x3

    goto :goto_8

    :pswitch_3
    const/4 v1, 0x4

    goto :goto_8

    :pswitch_4
    const/4 v1, 0x5

    goto :goto_8

    :pswitch_5
    const/4 v1, 0x6

    goto :goto_8

    :pswitch_6
    const/4 v1, 0x7

    goto :goto_8

    :pswitch_7
    const/16 v1, 0x8

    goto :goto_8

    :pswitch_8
    const/16 v1, 0x9

    goto :goto_8

    :cond_6
    move-object v0, v3

    goto/16 :goto_3

    :pswitch_9
    const/4 v0, 0x1

    goto/16 :goto_4

    :pswitch_a
    const/4 v0, 0x2

    goto/16 :goto_4

    :pswitch_b
    const/4 v0, 0x3

    goto/16 :goto_4

    :pswitch_c
    const/4 v0, 0x4

    goto/16 :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 16
    check-cast p1, Lmgm;

    invoke-virtual {p0, p1}, Ldlb;->a(Lmgm;)V

    return-void
.end method

.method protected bridge synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 16
    return-void
.end method
