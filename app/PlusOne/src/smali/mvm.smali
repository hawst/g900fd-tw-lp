.class public final Lmvm;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmvm;


# instance fields
.field private b:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3568
    const/4 v0, 0x0

    new-array v0, v0, [Lmvm;

    sput-object v0, Lmvm;->a:[Lmvm;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3569
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 3585
    const/4 v0, 0x0

    .line 3586
    iget-object v1, p0, Lmvm;->b:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 3587
    const/4 v0, 0x1

    iget-object v1, p0, Lmvm;->b:Ljava/lang/Long;

    .line 3588
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Loxo;->e(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3590
    :cond_0
    iget-object v1, p0, Lmvm;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3591
    iput v0, p0, Lmvm;->ai:I

    .line 3592
    return v0
.end method

.method public a(Loxn;)Lmvm;
    .locals 2

    .prologue
    .line 3600
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3601
    sparse-switch v0, :sswitch_data_0

    .line 3605
    iget-object v1, p0, Lmvm;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 3606
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmvm;->ah:Ljava/util/List;

    .line 3609
    :cond_1
    iget-object v1, p0, Lmvm;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3611
    :sswitch_0
    return-object p0

    .line 3616
    :sswitch_1
    invoke-virtual {p1}, Loxn;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lmvm;->b:Ljava/lang/Long;

    goto :goto_0

    .line 3601
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 3576
    iget-object v0, p0, Lmvm;->b:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 3577
    const/4 v0, 0x1

    iget-object v1, p0, Lmvm;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(IJ)V

    .line 3579
    :cond_0
    iget-object v0, p0, Lmvm;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3581
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3565
    invoke-virtual {p0, p1}, Lmvm;->a(Loxn;)Lmvm;

    move-result-object v0

    return-object v0
.end method
