.class public final Lkwe;
.super Lloj;
.source "PG"


# instance fields
.field private Q:Landroid/content/Context;

.field private R:I

.field private S:Landroid/widget/Button;

.field private T:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 51
    invoke-direct {p0}, Lloj;-><init>()V

    .line 62
    new-instance v0, Lhmg;

    sget-object v1, Lomv;->q:Lhmn;

    invoke-direct {v0, v1}, Lhmg;-><init>(Lhmn;)V

    iget-object v1, p0, Lkwe;->O:Llnh;

    .line 63
    invoke-virtual {v0, v1}, Lhmg;->a(Llnh;)Lhmg;

    .line 65
    new-instance v0, Lhmf;

    iget-object v1, p0, Lkwe;->P:Llqm;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lhmf;-><init>(Llqr;B)V

    .line 66
    return-void
.end method

.method public static U()Lt;
    .locals 1

    .prologue
    .line 58
    new-instance v0, Lkwe;

    invoke-direct {v0}, Lkwe;-><init>()V

    return-object v0
.end method

.method static synthetic a(Lkwe;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lkwe;->Q:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic b(Lkwe;)I
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lkwe;->R:I

    return v0
.end method

.method static synthetic c(Lkwe;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lkwe;->T:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic d(Lkwe;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lkwe;->S:Landroid/widget/Button;

    return-object v0
.end method


# virtual methods
.method public c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 8

    .prologue
    .line 76
    iget-object v0, p0, Lkwe;->O:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    iput v0, p0, Lkwe;->R:I

    .line 78
    if-nez p1, :cond_0

    .line 79
    new-instance v1, Lkwi;

    iget-object v0, p0, Lkwe;->Q:Landroid/content/Context;

    iget v2, p0, Lkwe;->R:I

    const/4 v3, 0x3

    invoke-direct {v1, v0, v2, v3}, Lkwi;-><init>(Landroid/content/Context;II)V

    .line 81
    iget-object v0, p0, Lkwe;->Q:Landroid/content/Context;

    const-class v2, Lhoc;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    invoke-virtual {v0, v1}, Lhoc;->b(Lhny;)V

    .line 84
    :cond_0
    iget-object v0, p0, Lkwe;->Q:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 85
    const v1, 0x7f040123

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 88
    const v0, 0x7f1003a8

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 89
    iget-object v2, p0, Lkwe;->N:Llnl;

    const-string v3, "plus_profile_tab"

    const-string v4, "https://support.google.com/plus/?hl=%locale%"

    invoke-static {v2, v3, v4}, Litk;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    .line 90
    new-instance v3, Landroid/text/SpannableStringBuilder;

    iget-object v4, p0, Lkwe;->N:Llnl;

    const v5, 0x7f0a04d7

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v2, v6, v7

    .line 91
    invoke-virtual {v4, v5, v6}, Llnl;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Llhv;->a(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-direct {v3, v2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 92
    invoke-static {v3}, Llju;->a(Landroid/text/Spannable;)V

    .line 93
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 94
    invoke-static {}, Lljw;->a()Lljw;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 96
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lkwe;->Q:Landroid/content/Context;

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 97
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a04d6

    new-instance v2, Lkwg;

    invoke-direct {v2, p0}, Lkwg;-><init>(Lkwe;)V

    .line 98
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a04d3

    new-instance v2, Lkwf;

    invoke-direct {v2, p0}, Lkwf;-><init>(Lkwe;)V

    .line 114
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 129
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public g()V
    .locals 4

    .prologue
    .line 134
    invoke-super {p0}, Lloj;->g()V

    .line 136
    invoke-virtual {p0}, Lkwe;->c()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/AlertDialog;

    .line 137
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    iput-object v1, p0, Lkwe;->S:Landroid/widget/Button;

    .line 138
    iget-object v1, p0, Lkwe;->S:Landroid/widget/Button;

    new-instance v2, Lhmk;

    sget-object v3, Lomv;->L:Lhmn;

    invoke-direct {v2, v3}, Lhmk;-><init>(Lhmn;)V

    invoke-static {v1, v2}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    .line 140
    const/4 v1, -0x2

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    iput-object v0, p0, Lkwe;->T:Landroid/widget/Button;

    .line 141
    iget-object v0, p0, Lkwe;->T:Landroid/widget/Button;

    new-instance v1, Lhmk;

    sget-object v2, Lomv;->K:Lhmn;

    invoke-direct {v1, v2}, Lhmk;-><init>(Lhmn;)V

    invoke-static {v0, v1}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    .line 143
    return-void
.end method

.method public k(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 70
    invoke-super {p0, p1}, Lloj;->k(Landroid/os/Bundle;)V

    .line 71
    new-instance v0, Landroid/view/ContextThemeWrapper;

    iget-object v1, p0, Lkwe;->N:Llnl;

    const v2, 0x7f0900a9

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lkwe;->Q:Landroid/content/Context;

    .line 72
    return-void
.end method
