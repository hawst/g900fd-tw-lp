.class public final Lfhk;
.super Landroid/content/AbstractThreadedSyncAdapter;
.source "PG"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lhei;

.field private final c:Lieh;

.field private final d:Liwc;

.field private final e:Lhms;

.field private final f:Lhpu;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 532
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/content/AbstractThreadedSyncAdapter;-><init>(Landroid/content/Context;Z)V

    .line 533
    iput-object p1, p0, Lfhk;->a:Landroid/content/Context;

    .line 534
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Lfhk;->b:Lhei;

    .line 535
    const-class v0, Lieh;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    iput-object v0, p0, Lfhk;->c:Lieh;

    .line 536
    const-class v0, Liwc;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liwc;

    iput-object v0, p0, Lfhk;->d:Liwc;

    .line 537
    const-class v0, Lhms;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    iput-object v0, p0, Lfhk;->e:Lhms;

    .line 538
    const-class v0, Lhpu;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhpu;

    iput-object v0, p0, Lfhk;->f:Lhpu;

    .line 539
    return-void
.end method

.method private a(Ljava/lang/String;)Lfhi;
    .locals 3

    .prologue
    .line 649
    invoke-static {}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->e()Ljava/util/Map;

    move-result-object v1

    monitor-enter v1

    .line 650
    :try_start_0
    invoke-static {}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->e()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhi;

    .line 651
    if-nez v0, :cond_0

    .line 652
    new-instance v0, Lfhi;

    invoke-direct {v0}, Lfhi;-><init>()V

    .line 653
    invoke-static {}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->e()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 655
    :cond_0
    monitor-exit v1

    return-object v0

    .line 656
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private a(ILkfp;Lles;Ldsl;)V
    .locals 10

    .prologue
    .line 1107
    iget-object v0, p0, Lfhk;->b:Lhei;

    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "is_google_plus"

    invoke-interface {v0, v1}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfhk;->a:Landroid/content/Context;

    .line 1108
    invoke-static {v0, p1}, Ldhv;->i(Landroid/content/Context;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1110
    new-instance v0, Ldmq;

    iget-object v1, p0, Lfhk;->a:Landroid/content/Context;

    new-instance v2, Lkfo;

    iget-object v3, p0, Lfhk;->a:Landroid/content/Context;

    invoke-direct {v2, v3, p1, p2}, Lkfo;-><init>(Landroid/content/Context;ILkfp;)V

    invoke-direct {v0, v1, v2, p1}, Ldmq;-><init>(Landroid/content/Context;Lkfo;I)V

    .line 1112
    invoke-virtual {v0}, Ldmq;->l()V

    .line 1113
    const-string v1, "EsSyncAdapterService"

    invoke-virtual {v0, v1}, Ldmq;->e(Ljava/lang/String;)V

    .line 1116
    :cond_0
    iget-object v0, p0, Lfhk;->a:Landroid/content/Context;

    const-class v1, Llet;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llet;

    .line 1117
    invoke-interface {v0, p1}, Llet;->a(I)Z

    move-result v8

    .line 1119
    iget-object v0, p0, Lfhk;->b:Lhei;

    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "account_name"

    .line 1120
    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 1121
    iget-object v0, p0, Lfhk;->a:Landroid/content/Context;

    invoke-static {v0, v9}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1122
    if-eqz v8, :cond_1

    .line 1123
    new-instance v0, Lkoe;

    const/16 v1, 0x29

    invoke-direct {v0, v1}, Lkoe;-><init>(I)V

    invoke-virtual {v0, v9}, Lkoe;->a(Ljava/lang/String;)Lkoe;

    move-result-object v0

    iget-object v1, p0, Lfhk;->a:Landroid/content/Context;

    .line 1124
    invoke-virtual {v0, v1}, Lkoe;->a(Landroid/content/Context;)V

    .line 1126
    :cond_1
    iget-object v0, p0, Lfhk;->a:Landroid/content/Context;

    sget-object v1, Lfit;->a:Lfit;

    invoke-static {v0, p1, v1}, Ldhv;->a(Landroid/content/Context;ILfit;)J

    move-result-wide v2

    iget-object v0, p0, Lfhk;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Ldsf;->g(Landroid/content/Context;I)J

    move-result-wide v0

    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-gtz v4, :cond_2

    sget-object v0, Lfit;->a:Lfit;

    iget-wide v0, v0, Lfit;->f:J

    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v2, v4, v2

    cmp-long v0, v2, v0

    if-lez v0, :cond_3

    iget-object v0, p0, Lfhk;->a:Landroid/content/Context;

    const/4 v5, 0x1

    const/4 v6, 0x4

    const/4 v7, 0x0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-static/range {v0 .. v7}, Ldsf;->a(Landroid/content/Context;ILkfp;Lles;Ldsl;II[B)V

    .line 1127
    :cond_3
    if-eqz v8, :cond_4

    .line 1128
    new-instance v0, Lkoe;

    const/16 v1, 0x2a

    invoke-direct {v0, v1}, Lkoe;-><init>(I)V

    invoke-virtual {v0, v9}, Lkoe;->a(Ljava/lang/String;)Lkoe;

    move-result-object v0

    iget-object v1, p0, Lfhk;->a:Landroid/content/Context;

    .line 1129
    invoke-virtual {v0, v1}, Lkoe;->a(Landroid/content/Context;)V

    .line 1133
    :cond_4
    iget-object v0, p0, Lfhk;->b:Lhei;

    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "is_google_plus"

    invoke-interface {v0, v1}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1134
    if-eqz v8, :cond_5

    .line 1135
    new-instance v0, Lkoe;

    const/16 v1, 0x31

    invoke-direct {v0, v1}, Lkoe;-><init>(I)V

    .line 1136
    invoke-virtual {v0, v9}, Lkoe;->a(Ljava/lang/String;)Lkoe;

    move-result-object v0

    iget-object v1, p0, Lfhk;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lkoe;->a(Landroid/content/Context;)V

    .line 1138
    :cond_5
    iget-object v0, p0, Lfhk;->a:Landroid/content/Context;

    sget-object v1, Lfit;->c:Lfit;

    invoke-static {v0, p1, v1}, Ldhv;->a(Landroid/content/Context;ILfit;)J

    move-result-wide v2

    iget-object v0, p0, Lfhk;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Ldhv;->x(Landroid/content/Context;I)J

    move-result-wide v0

    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-gtz v4, :cond_6

    sget-object v0, Lfit;->c:Lfit;

    iget-wide v0, v0, Lfit;->f:J

    :cond_6
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v2, v4, v2

    cmp-long v0, v2, v0

    if-lez v0, :cond_7

    iget-object v0, p0, Lfhk;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Ldhv;->r(Landroid/content/Context;I)V

    .line 1139
    :cond_7
    if-eqz v8, :cond_8

    .line 1140
    new-instance v0, Lkoe;

    const/16 v1, 0x32

    invoke-direct {v0, v1}, Lkoe;-><init>(I)V

    .line 1141
    invoke-virtual {v0, v9}, Lkoe;->a(Ljava/lang/String;)Lkoe;

    move-result-object v0

    iget-object v1, p0, Lfhk;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lkoe;->a(Landroid/content/Context;)V

    .line 1145
    :cond_8
    iget-object v0, p0, Lfhk;->a:Landroid/content/Context;

    const-class v1, Llex;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llex;

    .line 1146
    invoke-virtual {v0, p2, p1, p3}, Llex;->b(Lkfp;ILles;)V

    .line 1149
    iget-object v0, p0, Lfhk;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/apps/plus/content/EsProvider;->a(Landroid/content/Context;I)V

    .line 1150
    return-void
.end method

.method private a(Landroid/accounts/Account;)V
    .locals 9

    .prologue
    const/4 v6, 0x3

    const/4 v8, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 708
    invoke-static {}, Landroid/content/ContentResolver;->getMasterSyncAutomatically()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "com.google.android.apps.plus.content.EsProvider"

    invoke-static {p1, v0}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v2

    :goto_0
    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lfhk;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v3, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "https://m.google.com/app/feed/notifications?authority=com.google.plus.notifications"

    const-string v3, "webupdates"

    invoke-direct {p0, p1, v0, v3}, Lfhk;->a(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "https://m.google.com/app/feed/notifications?authority=com.google.plus.notifications"

    const-string v3, "webupdates"

    invoke-direct {p0, p1, v0, v3}, Lfhk;->b(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    :goto_1
    const-string v3, "com.google.plus.events"

    const-string v4, "events"

    invoke-direct {p0, p1, v3, v4}, Lfhk;->a(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v0, "com.google.plus.events"

    const-string v3, "events"

    invoke-direct {p0, p1, v0, v3}, Lfhk;->b(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    :cond_0
    if-eqz v0, :cond_1

    new-array v0, v6, [Ljava/lang/String;

    const-string v3, "com.google.plus.notifications"

    aput-object v3, v0, v1

    const-string v1, "https://m.google.com/app/feed/notifications?authority=com.google.plus.notifications"

    aput-object v1, v0, v2

    iget-object v1, p1, Landroid/accounts/Account;->type:Ljava/lang/String;

    aput-object v1, v0, v8

    invoke-virtual {p0}, Lfhk;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lfis;->a:Landroid/net/Uri;

    const-string v3, "authority = ? AND feed = ? AND _sync_account_type = ?"

    invoke-virtual {v1, v2, v3, v0}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 709
    :cond_1
    :goto_2
    return-void

    :cond_2
    move v0, v1

    .line 708
    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lfhk;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "com.google.android.apps.plus.content.EsProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_sync_account=?"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " AND _sync_account_type=?"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " AND authority=?"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v5, Lfis;->a:Landroid/net/Uri;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-array v6, v6, [Ljava/lang/String;

    iget-object v7, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v7, v6, v1

    iget-object v1, p1, Landroid/accounts/Account;->type:Ljava/lang/String;

    aput-object v1, v6, v2

    aput-object v3, v6, v8

    invoke-virtual {v0, v5, v4, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_2

    :cond_4
    move v0, v2

    goto :goto_1
.end method

.method private a(Ljava/util/List;Landroid/os/Bundle;Lfhi;Landroid/content/SyncResult;)V
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Landroid/os/Bundle;",
            "Lfhi;",
            "Landroid/content/SyncResult;",
            ")V"
        }
    .end annotation

    .prologue
    .line 889
    if-nez p2, :cond_0

    .line 890
    new-instance p2, Landroid/os/Bundle;

    invoke-direct/range {p2 .. p2}, Landroid/os/Bundle;-><init>()V

    .line 893
    :cond_0
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-direct {v0, v1, v2}, Lfhk;->a(Landroid/os/Bundle;Lfhi;)Z

    move-result v4

    .line 895
    if-eqz v4, :cond_1f

    .line 898
    :cond_1
    move-object/from16 v0, p3

    iget-object v5, v0, Lfhi;->b:Ljava/util/concurrent/LinkedBlockingQueue;

    monitor-enter v5

    .line 899
    :try_start_0
    move-object/from16 v0, p3

    iget-object v4, v0, Lfhi;->b:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v4}, Ljava/util/concurrent/LinkedBlockingQueue;->poll()Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Landroid/os/Bundle;

    move-object v12, v0

    .line 900
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 901
    if-eqz v12, :cond_1f

    .line 902
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :cond_2
    :goto_0
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 905
    move-object/from16 v0, p0

    iget-object v4, v0, Lfhk;->b:Lhei;

    invoke-interface {v4, v5}, Lhei;->a(I)Lhej;

    move-result-object v4

    .line 906
    const-string v6, "logged_out"

    invoke-interface {v4, v6}, Lhej;->c(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 908
    :try_start_1
    move-object/from16 v0, p3

    iget-object v6, v0, Lfhi;->a:Lkfp;

    new-instance v7, Lles;

    move-object/from16 v0, p4

    invoke-direct {v7, v0}, Lles;-><init>(Landroid/content/SyncResult;)V

    const/16 v13, 0x25

    const/16 v11, 0x26

    const/4 v10, 0x0

    const-string v9, "full, explicit"

    const/4 v4, 0x0

    if-eqz v12, :cond_20

    const-string v8, "sync_what"

    const/4 v14, 0x0

    invoke-virtual {v12, v8, v14}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v8

    const/4 v14, 0x2

    if-ne v8, v14, :cond_7

    const/4 v9, 0x2

    const-string v8, "notifications"

    const/16 v11, 0x29

    const/16 v10, 0x2a

    move v14, v10

    move v10, v9

    move-object v9, v4

    move-object v4, v8

    :goto_1
    const-string v8, "sync_from_tickle"

    const/4 v13, 0x0

    invoke-virtual {v12, v8, v13}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v13, ", from tickle"

    invoke-virtual {v4, v13}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :cond_3
    const-string v13, "EsSyncAdapterService"

    const/16 v16, 0x4

    move/from16 v0, v16

    invoke-static {v13, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v13

    if-eqz v13, :cond_4

    const-string v13, "onPerformSync: ====> Starting sync: "

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v17

    if-eqz v17, :cond_d

    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    :cond_4
    :goto_2
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v16

    add-int/lit8 v16, v16, 0xa

    move/from16 v0, v16

    invoke-direct {v13, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v16, "G+ sync ("

    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v13, ")"

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Lkfp;->b(Ljava/lang/String;)V

    if-eqz v8, :cond_e

    sget-object v8, Ldsl;->a:Ldsl;
    :try_end_1
    .catch Lfhj; {:try_start_1 .. :try_end_1} :catch_0

    :goto_3
    const/4 v13, 0x1

    :try_start_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lfhk;->b:Lhei;

    invoke-interface {v4, v5}, Lhei;->a(I)Lhej;

    move-result-object v4

    const-string v16, "account_name"

    move-object/from16 v0, v16

    invoke-interface {v4, v0}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catch Lhel; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lfhj; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v16

    :try_start_3
    new-instance v4, Lkoe;

    invoke-direct {v4, v11}, Lkoe;-><init>(I)V

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Lkoe;->a(Ljava/lang/String;)Lkoe;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v11, v0, Lfhk;->a:Landroid/content/Context;

    invoke-virtual {v4, v11}, Lkoe;->a(Landroid/content/Context;)V

    packed-switch v10, :pswitch_data_0

    const-string v4, "EsSyncAdapterService"

    const-string v7, "Ignoring unsolicited sync request"

    invoke-static {v4, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :cond_5
    :goto_4
    :try_start_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lfhk;->b:Lhei;

    invoke-interface {v4, v5}, Lhei;->c(I)Z

    move-result v4

    if-nez v4, :cond_6

    move-object/from16 v0, p4

    iget-object v4, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    const-wide/16 v8, 0x0

    iput-wide v8, v4, Landroid/content/SyncStats;->numIoExceptions:J

    :cond_6
    invoke-virtual {v6}, Lkfp;->g()V

    new-instance v4, Lkoe;

    invoke-direct {v4, v14}, Lkoe;-><init>(I)V

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Lkoe;->a(Ljava/lang/String;)Lkoe;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lfhk;->a:Landroid/content/Context;

    invoke-virtual {v4, v5}, Lkoe;->a(Landroid/content/Context;)V

    :goto_5
    const-string v4, "EsSyncAdapterService"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual/range {p4 .. p4}, Landroid/content/SyncResult;->hasError()Z
    :try_end_4
    .catch Lfhj; {:try_start_4 .. :try_end_4} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v4

    goto/16 :goto_0

    .line 900
    :catchall_0
    move-exception v4

    :try_start_5
    monitor-exit v5
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    throw v4

    .line 908
    :cond_7
    :try_start_6
    const-string v8, "sync_what"

    const/4 v14, 0x0

    invoke-virtual {v12, v8, v14}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v8

    const/4 v14, 0x3

    if-ne v8, v14, :cond_8

    const/4 v9, 0x3

    const-string v8, "events"

    const/16 v11, 0x1f

    const/16 v10, 0x20

    move v14, v10

    move v10, v9

    move-object v9, v4

    move-object v4, v8

    goto/16 :goto_1

    :cond_8
    const-string v8, "sync_mandatory"

    invoke-virtual {v12, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_9

    const/4 v9, 0x4

    const-string v8, "mandatory"

    const/16 v11, 0x27

    const/16 v10, 0x28

    move v14, v10

    move v10, v9

    move-object v9, v4

    move-object v4, v8

    goto/16 :goto_1

    :cond_9
    const-string v8, "sync_share_queue"

    invoke-virtual {v12, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_a

    const/4 v9, 0x5

    const-string v8, "share queue"

    const/16 v11, 0x37

    const/16 v10, 0x38

    move v14, v10

    move v10, v9

    move-object v9, v4

    move-object v4, v8

    goto/16 :goto_1

    :cond_a
    const-string v8, "sync_periodic"

    invoke-virtual {v12, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_b

    const/4 v9, 0x1

    const-string v8, "full, periodic"

    const/16 v11, 0x2b

    const/16 v10, 0x2c

    move v14, v10

    move v10, v9

    move-object v9, v4

    move-object v4, v8

    goto/16 :goto_1

    :cond_b
    const-string v8, "sync_check_and_engage"

    invoke-virtual {v12, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_c

    const/4 v9, 0x6

    const-string v8, "check and engage"

    const/16 v11, 0x17

    const/16 v10, 0x18

    move v14, v10

    move v10, v9

    move-object v9, v4

    move-object v4, v8

    goto/16 :goto_1

    :cond_c
    const-string v8, "synclet_name"

    invoke-virtual {v12, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_20

    move-object/from16 v0, p0

    iget-object v4, v0, Lfhk;->a:Landroid/content/Context;

    const-class v9, Llex;

    invoke-static {v4, v9}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Llex;

    invoke-virtual {v4, v8}, Llex;->b(Ljava/lang/String;)Llev;

    move-result-object v9

    invoke-virtual {v4, v8}, Llex;->a(Ljava/lang/String;)Lleu;

    move-result-object v4

    invoke-interface {v9}, Llev;->b()I

    move-result v11

    invoke-interface {v9}, Llev;->c()I

    move-result v10

    const/4 v9, 0x7

    move v14, v10

    move v10, v9

    move-object v9, v4

    move-object v4, v8

    goto/16 :goto_1

    :cond_d
    new-instance v16, Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-direct {v0, v13}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_e
    sget-object v8, Ldsl;->b:Ldsl;

    goto/16 :goto_3

    :catch_1
    move-exception v4

    new-instance v5, Lfhj;

    invoke-direct {v5, v4}, Lfhj;-><init>(Ljava/lang/Throwable;)V

    throw v5
    :try_end_6
    .catch Lfhj; {:try_start_6 .. :try_end_6} :catch_0

    :pswitch_0
    :try_start_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lfhk;->a:Landroid/content/Context;

    move-object/from16 v0, v16

    invoke-static {v4, v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_11

    const-string v4, "EsSyncAdapterService"

    const/4 v9, 0x4

    invoke-static {v4, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_f

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v9, 0x50

    invoke-direct {v4, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v9, " doFullSync: ====> Starting EsNotification.syncNotifications"

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_f
    move-object/from16 v0, p0

    iget-object v4, v0, Lfhk;->a:Landroid/content/Context;

    const/4 v9, 0x1

    const/4 v10, 0x4

    const/4 v11, 0x0

    invoke-static/range {v4 .. v11}, Ldsf;->a(Landroid/content/Context;ILkfp;Lles;Ldsl;II[B)V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto/16 :goto_4

    :catch_2
    move-exception v4

    move v7, v13

    :goto_6
    :try_start_8
    const-string v8, "EsSyncAdapterService"

    const-string v9, "Sync failure"

    invoke-static {v8, v9, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    if-eqz v7, :cond_1e

    move-object/from16 v0, p4

    iget-object v4, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v8, v4, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v10, 0x1

    add-long/2addr v8, v10

    iput-wide v8, v4, Landroid/content/SyncStats;->numIoExceptions:J
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :goto_7
    :try_start_9
    move-object/from16 v0, p0

    iget-object v4, v0, Lfhk;->b:Lhei;

    invoke-interface {v4, v5}, Lhei;->c(I)Z

    move-result v4

    if-nez v4, :cond_10

    move-object/from16 v0, p4

    iget-object v4, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    const-wide/16 v8, 0x0

    iput-wide v8, v4, Landroid/content/SyncStats;->numIoExceptions:J

    :cond_10
    invoke-virtual {v6}, Lkfp;->g()V

    new-instance v4, Lkoe;

    invoke-direct {v4, v14}, Lkoe;-><init>(I)V

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Lkoe;->a(Ljava/lang/String;)Lkoe;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lfhk;->a:Landroid/content/Context;

    invoke-virtual {v4, v5}, Lkoe;->a(Landroid/content/Context;)V
    :try_end_9
    .catch Lfhj; {:try_start_9 .. :try_end_9} :catch_0

    goto/16 :goto_5

    :cond_11
    :try_start_a
    new-instance v4, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v4}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v4
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_2
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    :catch_3
    move-exception v4

    :try_start_b
    const-string v7, "EsSyncAdapterService"

    const/4 v8, 0x6

    invoke-static {v7, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_12

    const-string v7, "EsSyncAdapterService"

    const-string v8, "Sync failure"

    invoke-static {v7, v8, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_12
    move-object/from16 v0, p4

    iget-object v4, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v8, v4, Landroid/content/SyncStats;->numParseExceptions:J

    const-wide/16 v10, 0x1

    add-long/2addr v8, v10

    iput-wide v8, v4, Landroid/content/SyncStats;->numParseExceptions:J
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    :try_start_c
    move-object/from16 v0, p0

    iget-object v4, v0, Lfhk;->b:Lhei;

    invoke-interface {v4, v5}, Lhei;->c(I)Z

    move-result v4

    if-nez v4, :cond_13

    move-object/from16 v0, p4

    iget-object v4, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    const-wide/16 v8, 0x0

    iput-wide v8, v4, Landroid/content/SyncStats;->numIoExceptions:J

    :cond_13
    invoke-virtual {v6}, Lkfp;->g()V

    new-instance v4, Lkoe;

    invoke-direct {v4, v14}, Lkoe;-><init>(I)V

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Lkoe;->a(Ljava/lang/String;)Lkoe;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lfhk;->a:Landroid/content/Context;

    invoke-virtual {v4, v5}, Lkoe;->a(Landroid/content/Context;)V
    :try_end_c
    .catch Lfhj; {:try_start_c .. :try_end_c} :catch_0

    goto/16 :goto_5

    :pswitch_1
    :try_start_d
    move-object/from16 v0, p0

    iget-object v4, v0, Lfhk;->b:Lhei;

    invoke-interface {v4, v5}, Lhei;->a(I)Lhej;

    move-result-object v4

    const-string v8, "is_google_plus"

    invoke-interface {v4, v8}, Lhej;->c(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    move-object/from16 v0, p0

    iget-object v4, v0, Lfhk;->a:Landroid/content/Context;

    invoke-static {v4, v5, v6, v7}, Ldrm;->b(Landroid/content/Context;ILkfp;Lles;)V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_2
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_3
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    goto/16 :goto_4

    :catchall_1
    move-exception v4

    :try_start_e
    move-object/from16 v0, p0

    iget-object v7, v0, Lfhk;->b:Lhei;

    invoke-interface {v7, v5}, Lhei;->c(I)Z

    move-result v5

    if-nez v5, :cond_14

    move-object/from16 v0, p4

    iget-object v5, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    const-wide/16 v8, 0x0

    iput-wide v8, v5, Landroid/content/SyncStats;->numIoExceptions:J

    :cond_14
    invoke-virtual {v6}, Lkfp;->g()V

    new-instance v5, Lkoe;

    invoke-direct {v5, v14}, Lkoe;-><init>(I)V

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Lkoe;->a(Ljava/lang/String;)Lkoe;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lfhk;->a:Landroid/content/Context;

    invoke-virtual {v5, v6}, Lkoe;->a(Landroid/content/Context;)V

    throw v4
    :try_end_e
    .catch Lfhj; {:try_start_e .. :try_end_e} :catch_0

    :pswitch_2
    :try_start_f
    move-object/from16 v0, p0

    iget-object v4, v0, Lfhk;->a:Landroid/content/Context;

    const-class v8, Llex;

    invoke-static {v4, v8}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Llex;

    new-instance v8, Lkfp;

    invoke-direct {v8}, Lkfp;-><init>()V

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lkfp;->a(Z)V

    const-string v9, "Mandatory sync"

    invoke-virtual {v8, v9}, Lkfp;->b(Ljava/lang/String;)V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_2
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_3
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    :try_start_10
    move-object/from16 v0, p0

    iget-object v9, v0, Lfhk;->b:Lhei;

    invoke-interface {v9, v5}, Lhei;->a(I)Lhej;

    move-result-object v9

    const-string v10, "account_name"

    invoke-interface {v9, v10}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v10, "experiments"

    invoke-virtual {v4, v10, v8, v5, v7}, Llex;->a(Ljava/lang/String;Lkfp;ILles;)V

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lfhk;->a(I)Z

    move-result v10

    if-nez v10, :cond_15

    move-object/from16 v0, p0

    iget-object v4, v0, Lfhk;->d:Liwc;

    sget-object v7, Liwc;->a:Liwe;

    invoke-interface {v4, v9, v7}, Liwc;->a(Ljava/lang/String;Liwe;)V
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_2

    :try_start_11
    invoke-virtual {v8}, Lkfp;->g()V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_2
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_3
    .catchall {:try_start_11 .. :try_end_11} :catchall_1

    goto/16 :goto_4

    :cond_15
    :try_start_12
    invoke-virtual {v4, v8, v5, v7}, Llex;->a(Lkfp;ILles;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lfhk;->a:Landroid/content/Context;

    const-class v7, Liae;

    invoke-static {v4, v7}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Liae;

    move-object/from16 v0, p0

    iget-object v7, v0, Lfhk;->a:Landroid/content/Context;

    const/4 v9, 0x0

    invoke-interface {v4, v7, v5, v9}, Liae;->a(Landroid/content/Context;IZ)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lfhk;->a:Landroid/content/Context;

    invoke-static {v4, v5, v8}, Ldhv;->a(Landroid/content/Context;ILkfp;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lfhk;->a:Landroid/content/Context;

    invoke-static {v4, v5}, Lfho;->a(Landroid/content/Context;I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lfhk;->b:Lhei;

    invoke-interface {v4, v5}, Lhei;->a(I)Lhej;

    move-result-object v4

    const-string v7, "is_google_plus"

    invoke-interface {v4, v7}, Lhej;->c(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_16

    new-instance v7, Lmxa;

    invoke-direct {v7}, Lmxa;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lfhk;->a:Landroid/content/Context;

    invoke-static {v4}, Lhre;->a(Landroid/content/Context;)Lhre;

    move-result-object v9

    new-instance v10, Lmtp;

    invoke-direct {v10}, Lmtp;-><init>()V

    invoke-interface {v9}, Lhrd;->e()Z

    move-result v4

    if-nez v4, :cond_17

    const/4 v4, 0x1

    :goto_8
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iput-object v4, v10, Lmtp;->f:Ljava/lang/Boolean;

    move-object/from16 v0, p0

    iget-object v4, v0, Lfhk;->f:Lhpu;

    invoke-virtual {v4, v5}, Lhpu;->e(I)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iput-object v4, v10, Lmtp;->a:Ljava/lang/Boolean;

    invoke-interface {v9}, Lhrd;->b()Z

    move-result v4

    if-eqz v4, :cond_18

    const/4 v4, 0x1

    :goto_9
    iput v4, v10, Lmtp;->c:I

    invoke-interface {v9}, Lhrd;->j()Z

    move-result v4

    if-eqz v4, :cond_19

    const/4 v4, 0x1

    :goto_a
    iput v4, v10, Lmtp;->b:I

    invoke-interface {v9}, Lhrd;->d()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iput-object v4, v10, Lmtp;->e:Ljava/lang/Boolean;

    invoke-interface {v9}, Lhrd;->c()Z

    move-result v4

    if-eqz v4, :cond_1a

    const/4 v4, 0x1

    :goto_b
    iput v4, v10, Lmtp;->d:I

    iput-object v10, v7, Lmxa;->b:Lmtp;

    move-object/from16 v0, p0

    iget-object v4, v0, Lfhk;->b:Lhei;

    invoke-interface {v4, v5}, Lhei;->a(I)Lhej;

    move-result-object v4

    const-string v9, "account_name"

    invoke-interface {v4, v9}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v9, v0, Lfhk;->a:Landroid/content/Context;

    invoke-static {v9, v4}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1b

    new-instance v4, Lmwe;

    invoke-direct {v4}, Lmwe;-><init>()V

    move-object/from16 v0, p0

    iget-object v9, v0, Lfhk;->a:Landroid/content/Context;

    invoke-static {v9, v5}, Leum;->b(Landroid/content/Context;I)Z

    move-result v9

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    iput-object v9, v4, Lmwe;->c:Ljava/lang/Boolean;

    move-object/from16 v0, p0

    iget-object v9, v0, Lfhk;->a:Landroid/content/Context;

    invoke-static {v9, v5}, Leum;->a(Landroid/content/Context;I)Z

    move-result v9

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    iput-object v9, v4, Lmwe;->a:Ljava/lang/Boolean;

    move-object/from16 v0, p0

    iget-object v9, v0, Lfhk;->a:Landroid/content/Context;

    invoke-static {v9, v5}, Leum;->d(Landroid/content/Context;I)Z

    move-result v9

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    iput-object v9, v4, Lmwe;->b:Ljava/lang/Boolean;

    iput-object v4, v7, Lmxa;->a:Lmwe;

    move-object/from16 v0, p0

    iget-object v4, v0, Lfhk;->a:Landroid/content/Context;

    new-instance v9, Lmtr;

    invoke-direct {v9}, Lmtr;-><init>()V

    invoke-static {v4, v5}, Ldhv;->j(Landroid/content/Context;I)Z

    move-result v10

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    iput-object v10, v9, Lmtr;->a:Ljava/lang/Boolean;

    invoke-static {v4, v5}, Ldhv;->l(Landroid/content/Context;I)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iput-object v4, v9, Lmtr;->b:Ljava/lang/Boolean;

    iput-object v9, v7, Lmxa;->c:Lmtr;

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    const-string v9, "extra_user_settings_state"

    invoke-static {v7}, Loxu;->a(Loxu;)[B

    move-result-object v7

    invoke-virtual {v4, v9, v7}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    move-object/from16 v0, p0

    iget-object v7, v0, Lfhk;->e:Lhms;

    new-instance v9, Lhmr;

    move-object/from16 v0, p0

    iget-object v10, v0, Lfhk;->a:Landroid/content/Context;

    invoke-direct {v9, v10, v5}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v10, Lhmv;->v:Lhmv;

    invoke-virtual {v9, v10}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v9

    sget-object v10, Lhmw;->o:Lhmw;

    invoke-virtual {v9, v10}, Lhmr;->a(Lhmw;)Lhmr;

    move-result-object v9

    invoke-virtual {v9, v4}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v4

    invoke-interface {v7, v4}, Lhms;->a(Lhmr;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lfhk;->a:Landroid/content/Context;

    const-class v7, Liax;

    invoke-static {v4, v7}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Liax;

    move-object/from16 v0, p0

    iget-object v7, v0, Lfhk;->a:Landroid/content/Context;

    invoke-interface {v4, v7, v5}, Liax;->a(Landroid/content/Context;I)V
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_2

    :cond_16
    :try_start_13
    invoke-virtual {v8}, Lkfp;->g()V
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_2
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_3
    .catchall {:try_start_13 .. :try_end_13} :catchall_1

    goto/16 :goto_4

    :cond_17
    const/4 v4, 0x0

    goto/16 :goto_8

    :cond_18
    const/4 v4, 0x2

    goto/16 :goto_9

    :cond_19
    const/4 v4, 0x2

    goto/16 :goto_a

    :cond_1a
    const/4 v4, 0x2

    goto/16 :goto_b

    :cond_1b
    :try_start_14
    new-instance v4, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v4}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v4
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_2

    :catchall_2
    move-exception v4

    :try_start_15
    invoke-virtual {v8}, Lkfp;->g()V

    throw v4

    :pswitch_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lfhk;->b:Lhei;

    invoke-interface {v4, v5}, Lhei;->a(I)Lhej;

    move-result-object v4

    const-string v7, "is_google_plus"

    invoke-interface {v4, v7}, Lhej;->c(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    move-object/from16 v0, p0

    iget-object v4, v0, Lfhk;->c:Lieh;

    sget-object v7, Ljhj;->a:Lief;

    invoke-interface {v4, v7, v5}, Lieh;->b(Lief;I)Z

    move-result v4

    if-nez v4, :cond_5

    move-object/from16 v0, p0

    iget-object v4, v0, Lfhk;->a:Landroid/content/Context;

    const-class v7, Lkly;

    invoke-static {v4, v7}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lkly;

    invoke-virtual {v4, v5, v6}, Lkly;->b(ILkfp;)V

    goto/16 :goto_4

    :pswitch_4
    new-instance v7, Lkfp;

    invoke-direct {v7}, Lkfp;-><init>()V

    const/4 v4, 0x0

    invoke-virtual {v7, v4}, Lkfp;->a(Z)V

    const-string v4, "CheckAndEngage sync"

    invoke-virtual {v7, v4}, Lkfp;->b(Ljava/lang/String;)V
    :try_end_15
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_15} :catch_2
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_15} :catch_3
    .catchall {:try_start_15 .. :try_end_15} :catchall_1

    :try_start_16
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lfhk;->a(I)Z
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_3

    move-result v4

    if-nez v4, :cond_1c

    :try_start_17
    invoke-virtual {v7}, Lkfp;->g()V
    :try_end_17
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_17} :catch_2
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_17} :catch_3
    .catchall {:try_start_17 .. :try_end_17} :catchall_1

    goto/16 :goto_4

    :cond_1c
    :try_start_18
    move-object/from16 v0, p0

    iget-object v8, v0, Lfhk;->a:Landroid/content/Context;

    const-class v4, Lieh;

    invoke-static {v8, v4}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lieh;

    sget-object v9, Ldxd;->s:Lief;

    invoke-interface {v4, v9, v5}, Lieh;->b(Lief;I)Z

    move-result v4

    if-eqz v4, :cond_1d

    new-instance v4, Ldim;

    invoke-direct {v4, v8, v5}, Ldim;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v4}, Ldim;->l()V

    invoke-virtual {v4}, Ldim;->t()Z

    move-result v8

    if-eqz v8, :cond_1d

    const-string v8, "CloseTiesNotifications"

    invoke-virtual {v4, v8}, Ldim;->d(Ljava/lang/String;)V
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_3

    :cond_1d
    :try_start_19
    invoke-virtual {v7}, Lkfp;->g()V

    goto/16 :goto_4

    :catchall_3
    move-exception v4

    invoke-virtual {v7}, Lkfp;->g()V

    throw v4

    :pswitch_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lfhk;->a:Landroid/content/Context;

    invoke-static {v4, v5}, Ldhv;->c(Landroid/content/Context;I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lfhk;->a:Landroid/content/Context;

    const-class v9, Llex;

    invoke-static {v4, v9}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Llex;

    invoke-virtual {v4, v5}, Llex;->a(I)V

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v6, v7, v8}, Lfhk;->a(ILkfp;Lles;Ldsl;)V
    :try_end_19
    .catch Ljava/io/IOException; {:try_start_19 .. :try_end_19} :catch_2
    .catch Ljava/lang/Exception; {:try_start_19 .. :try_end_19} :catch_3
    .catchall {:try_start_19 .. :try_end_19} :catchall_1

    goto/16 :goto_4

    :pswitch_6
    const/4 v9, 0x0

    const/4 v4, 0x0

    :try_start_1a
    invoke-virtual {v6, v4}, Lkfp;->a(Z)V

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v6, v7, v8}, Lfhk;->a(ILkfp;Lles;Ldsl;)V

    move-object/from16 v0, p4

    iget-object v4, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    const-wide/16 v10, 0x0

    iput-wide v10, v4, Landroid/content/SyncStats;->numIoExceptions:J
    :try_end_1a
    .catch Ljava/io/IOException; {:try_start_1a .. :try_end_1a} :catch_4
    .catch Ljava/lang/Exception; {:try_start_1a .. :try_end_1a} :catch_3
    .catchall {:try_start_1a .. :try_end_1a} :catchall_1

    goto/16 :goto_4

    :catch_4
    move-exception v4

    move v7, v9

    goto/16 :goto_6

    :pswitch_7
    :try_start_1b
    invoke-interface {v9, v6, v5, v7}, Lleu;->a(Lkfp;ILles;)V
    :try_end_1b
    .catch Ljava/io/IOException; {:try_start_1b .. :try_end_1b} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1b .. :try_end_1b} :catch_3
    .catchall {:try_start_1b .. :try_end_1b} :catchall_1

    goto/16 :goto_4

    :cond_1e
    :try_start_1c
    move-object/from16 v0, p4

    iget-object v4, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    const-wide/16 v8, 0x0

    iput-wide v8, v4, Landroid/content/SyncStats;->numIoExceptions:J
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_1

    goto/16 :goto_7

    .line 916
    :cond_1f
    return-void

    :cond_20
    move v14, v11

    move v11, v13

    move-object/from16 v18, v4

    move-object v4, v9

    move-object/from16 v9, v18

    goto/16 :goto_1

    .line 908
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_7
    .end packed-switch
.end method

.method private a(I)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 844
    :try_start_0
    iget-object v1, p0, Lfhk;->b:Lhei;

    invoke-interface {v1, p1}, Lhei;->a(I)Lhej;

    move-result-object v1

    .line 845
    const-string v2, "is_google_plus"

    invoke-interface {v1, v2}, Lhej;->c(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "gplus_no_mobile_tos"

    .line 846
    invoke-interface {v1, v2}, Lhej;->c(Ljava/lang/String;)Z
    :try_end_0
    .catch Lhel; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 849
    :cond_1
    :goto_0
    return v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private a(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v6, 0x0

    .line 749
    const/4 v0, 0x5

    new-array v4, v0, [Ljava/lang/String;

    const-string v0, "com.google.android.apps.plus.content.EsProvider"

    aput-object v0, v4, v6

    aput-object p2, v4, v2

    const/4 v0, 0x2

    iget-object v1, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v1, v4, v0

    const/4 v0, 0x3

    iget-object v1, p1, Landroid/accounts/Account;->type:Ljava/lang/String;

    aput-object v1, v4, v0

    const/4 v0, 0x4

    aput-object p3, v4, v0

    .line 756
    invoke-virtual {p0}, Lfhk;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 757
    sget-object v1, Lfis;->a:Landroid/net/Uri;

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "_id"

    aput-object v3, v2, v6

    const-string v3, "authority = ? AND feed = ? AND _sync_account = ? AND _sync_account_type = ? AND service = ?"

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 759
    if-nez v1, :cond_0

    move v0, v6

    .line 766
    :goto_0
    return v0

    .line 764
    :cond_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 766
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private a(Landroid/os/Bundle;Lfhi;)Z
    .locals 5

    .prologue
    .line 861
    iget-object v1, p2, Lfhi;->b:Ljava/util/concurrent/LinkedBlockingQueue;

    .line 863
    monitor-enter v1

    .line 864
    :try_start_0
    invoke-virtual {v1}, Ljava/util/concurrent/LinkedBlockingQueue;->isEmpty()Z

    move-result v0

    .line 865
    if-nez p1, :cond_0

    .line 866
    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    .line 868
    :cond_0
    const-string v2, "EsSyncAdapterService"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 869
    const-string v2, ">>>>>>>> Adding bundle"

    invoke-virtual {p1}, Landroid/os/Bundle;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 870
    :goto_0
    invoke-virtual {v1, p1}, Ljava/util/concurrent/LinkedBlockingQueue;->contains(Ljava/lang/Object;)Z

    .line 871
    :cond_1
    invoke-virtual {v1, p1}, Ljava/util/concurrent/LinkedBlockingQueue;->offer(Ljava/lang/Object;)Z

    .line 875
    monitor-exit v1

    .line 876
    return v0

    .line 869
    :cond_2
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 875
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private b(Ljava/lang/String;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 820
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 822
    iget-object v0, p0, Lfhk;->b:Lhei;

    invoke-interface {v0, p1}, Lhei;->a(Ljava/lang/String;)I

    move-result v0

    .line 823
    const/4 v2, -0x1

    if-eq v0, v2, :cond_1

    .line 824
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 826
    iget-object v0, p0, Lfhk;->b:Lhei;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "logged_in"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "is_managed_account"

    aput-object v4, v2, v3

    invoke-interface {v0, v2}, Lhei;->a([Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 829
    iget-object v3, p0, Lfhk;->b:Lhei;

    invoke-interface {v3, v0}, Lhei;->a(I)Lhej;

    move-result-object v3

    .line 830
    const-string v4, "account_name"

    invoke-interface {v3, v4}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 831
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 836
    :cond_1
    return-object v1
.end method

.method private b(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 776
    invoke-virtual {p0}, Lfhk;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    invoke-static {v0, p2, p1, v1, p3}, Lfir;->a(Landroid/content/ContentResolver;Ljava/lang/String;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    .line 782
    return-void
.end method


# virtual methods
.method public onPerformSync(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v6, 0x4

    const/4 v7, -0x1

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 545
    const-string v2, "EsSyncAdapterService"

    invoke-static {v2, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 546
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x41

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " onPerformSync: ====> Starting onPerformSync "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 549
    :cond_0
    iget-object v3, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 550
    if-eqz p2, :cond_2

    const-string v2, "initialize"

    .line 551
    invoke-virtual {p2, v2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_2

    move v2, v0

    .line 553
    :goto_0
    if-eqz v2, :cond_7

    .line 554
    invoke-static {v3}, Lfud;->a(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v4

    const-string v5, "com.google.android.apps.plus.content.EsProvider"

    .line 555
    invoke-direct {p0, v3}, Lfhk;->b(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_6

    .line 554
    :goto_2
    invoke-static {v4, v5, v0}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 556
    invoke-direct {p0, p1}, Lfhk;->a(Landroid/accounts/Account;)V

    .line 641
    :cond_1
    :goto_3
    return-void

    :cond_2
    move v2, v1

    .line 551
    goto :goto_0

    .line 555
    :cond_3
    iget-object v2, p0, Lfhk;->b:Lhei;

    invoke-interface {v2, v3}, Lhei;->a(Ljava/lang/String;)I

    move-result v2

    if-eq v2, v7, :cond_4

    iget-object v6, p0, Lfhk;->b:Lhei;

    invoke-interface {v6, v2}, Lhei;->a(I)Lhej;

    move-result-object v2

    const-string v6, "logged_out"

    invoke-interface {v2, v6}, Lhej;->c(Ljava/lang/String;)Z

    move-result v2

    :goto_4
    if-nez v2, :cond_5

    iget-object v2, p0, Lfhk;->b:Lhei;

    invoke-interface {v2, v3}, Lhei;->a(Ljava/lang/String;)I

    move-result v2

    if-eq v2, v7, :cond_5

    invoke-direct {p0, v2}, Lfhk;->a(I)Z

    move-result v2

    if-eqz v2, :cond_5

    move v2, v0

    goto :goto_1

    :cond_4
    move v2, v1

    goto :goto_4

    :cond_5
    move v2, v1

    goto :goto_1

    :cond_6
    move v0, v1

    goto :goto_2

    .line 562
    :cond_7
    invoke-direct {p0, p1}, Lfhk;->a(Landroid/accounts/Account;)V

    .line 567
    :try_start_0
    iget-object v1, p0, Lfhk;->d:Liwc;

    invoke-static {}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->c()Liwe;

    move-result-object v2

    invoke-interface {v1, v3, v2}, Liwc;->a(Ljava/lang/String;Liwe;)V
    :try_end_0
    .catch Livr; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 576
    invoke-direct {p0, v3}, Lfhk;->b(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 578
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 585
    if-eqz p2, :cond_c

    const-string v2, "feed"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 586
    const-string v2, "feed"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 588
    const-string v4, "EsSyncAdapterService"

    invoke-static {v4, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 589
    const-string v4, "  --> Sync specific feed: "

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_a

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 592
    :cond_8
    :goto_5
    const-string v4, "sync_from_tickle"

    invoke-virtual {p2, v4, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 594
    const-string v0, "https://m.google.com/app/feed/notifications?authority=com.google.plus.notifications"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 595
    iget-object v0, p0, Lfhk;->a:Landroid/content/Context;

    iget-object v2, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 596
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 597
    invoke-static {}, Lfvc;->d()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 598
    iget-object v0, p0, Lfhk;->a:Landroid/content/Context;

    const-class v5, Ljip;

    invoke-static {v0, v5}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljip;

    invoke-interface {v0, v4}, Ljip;->a(I)V

    .line 600
    :cond_9
    iget-object v0, p0, Lfhk;->e:Lhms;

    new-instance v5, Lhmr;

    iget-object v6, p0, Lfhk;->a:Landroid/content/Context;

    invoke-direct {v5, v6, v4}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v4, Lhmv;->by:Lhmv;

    .line 602
    invoke-virtual {v5, v4}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v4

    sget-object v5, Lhmw;->y:Lhmw;

    .line 603
    invoke-virtual {v4, v5}, Lhmr;->a(Lhmw;)Lhmr;

    move-result-object v4

    .line 600
    invoke-interface {v0, v4}, Lhms;->a(Lhmr;)V

    goto :goto_6

    .line 572
    :catch_0
    move-exception v0

    iget-object v0, p5, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v0, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v0, Landroid/content/SyncStats;->numIoExceptions:J

    goto/16 :goto_3

    .line 589
    :cond_a
    new-instance v5, Ljava/lang/String;

    invoke-direct {v5, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_5

    .line 608
    :cond_b
    const-string v0, "sync_what"

    const/4 v2, 0x2

    invoke-virtual {p2, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 625
    :cond_c
    monitor-enter p0

    .line 626
    :try_start_1
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 627
    monitor-exit p0

    goto/16 :goto_3

    .line 633
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 610
    :cond_d
    const-string v0, "com.google.plus.events"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 612
    const-string v0, "sync_what"

    const/4 v2, 0x3

    invoke-virtual {p2, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 613
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_7
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 614
    iget-object v4, p0, Lfhk;->e:Lhms;

    new-instance v5, Lhmr;

    iget-object v6, p0, Lfhk;->a:Landroid/content/Context;

    invoke-direct {v5, v6, v0}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v0, Lhmv;->bz:Lhmv;

    .line 616
    invoke-virtual {v5, v0}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v0

    sget-object v5, Lhmw;->y:Lhmw;

    .line 617
    invoke-virtual {v0, v5}, Lhmr;->a(Lhmw;)Lhmr;

    move-result-object v0

    .line 614
    invoke-interface {v4, v0}, Lhms;->a(Lhmr;)V

    goto :goto_7

    .line 621
    :cond_e
    const-string v1, "EsSyncAdapterService"

    const-string v3, "Unexpected feed: "

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_f

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_8
    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    :cond_f
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_8

    .line 632
    :cond_10
    :try_start_2
    invoke-direct {p0, v3}, Lfhk;->a(Ljava/lang/String;)Lfhi;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->a(Lfhi;)Lfhi;

    .line 633
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 635
    :try_start_3
    invoke-static {}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->d()Lfhi;

    move-result-object v0

    iget-object v0, v0, Lfhi;->a:Lkfp;

    invoke-virtual {v0}, Lkfp;->c()Z

    move-result v0

    if-nez v0, :cond_11

    .line 636
    invoke-static {}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->d()Lfhi;

    move-result-object v0

    invoke-direct {p0, v1, p2, v0, p5}, Lfhk;->a(Ljava/util/List;Landroid/os/Bundle;Lfhi;Landroid/content/SyncResult;)V

    .line 637
    iget-object v0, p0, Lfhk;->a:Landroid/content/Context;

    invoke-static {v0}, Lgci;->a(Landroid/content/Context;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 640
    :cond_11
    invoke-static {v8}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->a(Lfhi;)Lfhi;

    goto/16 :goto_3

    :catchall_1
    move-exception v0

    invoke-static {v8}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->a(Lfhi;)Lfhi;

    throw v0

    .line 570
    :catch_1
    move-exception v0

    goto/16 :goto_3
.end method

.method public onSyncCanceled()V
    .locals 1

    .prologue
    .line 661
    invoke-super {p0}, Landroid/content/AbstractThreadedSyncAdapter;->onSyncCanceled()V

    .line 665
    invoke-static {}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->d()Lfhi;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 666
    invoke-static {}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->d()Lfhi;

    move-result-object v0

    iget-object v0, v0, Lfhi;->a:Lkfp;

    invoke-virtual {v0}, Lkfp;->b()V

    .line 668
    :cond_0
    return-void
.end method
