.class public abstract Ljfh;
.super Ljfy;
.source "PG"


# instance fields
.field private c:Landroid/database/Cursor;

.field private volatile d:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljfy;-><init>()V

    return-void
.end method

.method static synthetic a(Ljfh;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Ljfh;->c:Landroid/database/Cursor;

    return-object v0
.end method

.method static synthetic a(Ljfh;Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 0

    .prologue
    .line 15
    iput-object p1, p0, Ljfh;->c:Landroid/database/Cursor;

    return-object p1
.end method

.method private f(I)V
    .locals 2

    .prologue
    .line 44
    invoke-virtual {p0}, Ljfh;->aq_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Ljfh;->c:Landroid/database/Cursor;

    add-int/lit8 v1, p1, -0x1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 49
    :goto_0
    return-void

    .line 47
    :cond_0
    iget-object v0, p0, Ljfh;->c:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 67
    const/4 v0, 0x0

    .line 69
    invoke-virtual {p0}, Ljfh;->aq_()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 70
    const/4 v0, 0x1

    .line 73
    :cond_0
    invoke-virtual {p0}, Ljfh;->ar_()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 74
    add-int/lit8 v0, v0, 0x1

    .line 77
    :cond_1
    invoke-virtual {p0}, Ljfh;->u()I

    move-result v1

    invoke-virtual {p0}, Ljfh;->m()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Ljfh;->n()I

    move-result v2

    if-le v1, v2, :cond_2

    invoke-virtual {p0}, Ljfh;->n()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    .line 78
    return v0
.end method

.method public abstract a(Landroid/database/Cursor;)I
.end method

.method public a(I)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 186
    if-ltz p1, :cond_0

    iget-object v0, p0, Ljfh;->c:Landroid/database/Cursor;

    if-nez v0, :cond_1

    .line 187
    :cond_0
    const/4 v0, 0x0

    .line 195
    :goto_0
    return-object v0

    .line 189
    :cond_1
    if-nez p1, :cond_2

    invoke-virtual {p0}, Ljfh;->aq_()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 190
    invoke-virtual {p0}, Ljfh;->e()Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 191
    :cond_2
    invoke-virtual {p0}, Ljfh;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_3

    invoke-virtual {p0}, Ljfh;->ar_()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 192
    invoke-virtual {p0}, Ljfh;->i()Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 194
    :cond_3
    invoke-direct {p0, p1}, Ljfh;->f(I)V

    .line 195
    iget-object v0, p0, Ljfh;->c:Landroid/database/Cursor;

    invoke-virtual {p0, v0}, Ljfh;->b(Landroid/database/Cursor;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method public a(ILjgb;)V
    .locals 1

    .prologue
    .line 97
    if-nez p1, :cond_1

    invoke-virtual {p0}, Ljfh;->aq_()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 98
    invoke-virtual {p0, p2}, Ljfh;->a(Ljgb;)V

    .line 105
    :cond_0
    :goto_0
    return-void

    .line 99
    :cond_1
    invoke-virtual {p0}, Ljfh;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_2

    invoke-virtual {p0}, Ljfh;->ar_()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 100
    invoke-virtual {p0, p2}, Ljfh;->b(Ljgb;)V

    goto :goto_0

    .line 101
    :cond_2
    if-ltz p1, :cond_0

    .line 102
    invoke-direct {p0, p1}, Ljfh;->f(I)V

    .line 103
    iget-object v0, p0, Ljfh;->c:Landroid/database/Cursor;

    invoke-virtual {p0, v0, p2}, Ljfh;->a(Landroid/database/Cursor;Ljgb;)V

    goto :goto_0
.end method

.method public a(JLjgb;)V
    .locals 1

    .prologue
    .line 289
    invoke-virtual {p0, p1, p2}, Ljfh;->d(J)I

    move-result v0

    .line 290
    invoke-virtual {p0, v0, p3}, Ljfh;->a(ILjgb;)V

    .line 291
    return-void
.end method

.method public abstract a(Landroid/database/Cursor;Ljgb;)V
.end method

.method public a(Ljfz;J)V
    .locals 2

    .prologue
    .line 247
    const-wide/high16 v0, -0x8000000000000000L

    cmp-long v0, p2, v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Ljfh;->aq_()Z

    move-result v0

    if-nez v0, :cond_3

    .line 248
    :cond_0
    const-wide v0, 0x7fffffffffffffffL

    cmp-long v0, p2, v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Ljfh;->ar_()Z

    move-result v0

    if-nez v0, :cond_3

    .line 250
    :cond_1
    invoke-virtual {p0}, Ljfh;->u()I

    move-result v0

    if-lez v0, :cond_3

    .line 252
    iget-object v0, p0, Ljfh;->c:Landroid/database/Cursor;

    const/4 v1, -0x1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 253
    :cond_2
    iget-object v0, p0, Ljfh;->c:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 254
    iget-object v0, p0, Ljfh;->c:Landroid/database/Cursor;

    invoke-virtual {p0, v0}, Ljfh;->c(Landroid/database/Cursor;)J

    move-result-wide v0

    cmp-long v0, v0, p2

    if-nez v0, :cond_2

    .line 255
    iget-object v0, p0, Ljfh;->c:Landroid/database/Cursor;

    invoke-virtual {p0, p1, v0}, Ljfh;->a(Ljfz;Landroid/database/Cursor;)V

    .line 260
    :cond_3
    return-void
.end method

.method public a(Ljfz;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 243
    return-void
.end method

.method public abstract a(Ljgb;)V
.end method

.method public a(J)Z
    .locals 1

    .prologue
    .line 271
    invoke-virtual {p0, p1, p2}, Ljfh;->d(J)I

    move-result v0

    .line 272
    invoke-virtual {p0, v0}, Ljfh;->c(I)Z

    move-result v0

    return v0
.end method

.method public abstract aq_()Z
.end method

.method public abstract ar_()Z
.end method

.method public as_()V
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v3, 0x0

    .line 146
    iput-boolean v0, p0, Ljfh;->d:Z

    .line 147
    invoke-virtual {p0}, Ljfh;->l()Landroid/database/Cursor;

    move-result-object v1

    .line 148
    if-nez v1, :cond_0

    .line 168
    :goto_0
    return-void

    .line 152
    :cond_0
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xb

    if-lt v2, v4, :cond_2

    move v4, v0

    .line 153
    :goto_1
    if-eqz v4, :cond_3

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    new-instance v0, Lhym;

    invoke-interface {v1}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v5

    invoke-direct {v0, v2, v5}, Lhym;-><init>([Ljava/lang/String;I)V

    invoke-virtual {v0}, Lhym;->getColumnCount()I

    move-result v5

    const/4 v2, -0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v0}, Lhym;->a()Lhyn;

    move-result-object v6

    move v2, v3

    :goto_2
    if-ge v2, v5, :cond_1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getType(I)I

    move-result v7

    packed-switch v7, :pswitch_data_0

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    :cond_2
    move v4, v3

    .line 152
    goto :goto_1

    .line 153
    :pswitch_0
    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lhyn;->a(Ljava/lang/Object;)Lhyn;

    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :pswitch_1
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v7

    invoke-virtual {v6, v7}, Lhyn;->a(Ljava/lang/Object;)Lhyn;

    goto :goto_3

    :pswitch_2
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Lhyn;->a(Ljava/lang/Object;)Lhyn;

    goto :goto_3

    :pswitch_3
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v7

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    invoke-virtual {v6, v7}, Lhyn;->a(Ljava/lang/Object;)Lhyn;

    goto :goto_3

    :pswitch_4
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lhyn;->a(Ljava/lang/Object;)Lhyn;

    goto :goto_3

    :cond_3
    move-object v0, v1

    .line 154
    :cond_4
    if-eqz v4, :cond_5

    .line 155
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 157
    :cond_5
    iput-boolean v3, p0, Ljfh;->d:Z

    .line 158
    new-instance v1, Ljfi;

    invoke-direct {v1, p0, v0}, Ljfi;-><init>(Ljfh;Landroid/database/Cursor;)V

    invoke-static {v1}, Llsx;->a(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 153
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_1
    .end packed-switch
.end method

.method public at_()V
    .locals 0

    .prologue
    .line 172
    invoke-virtual {p0}, Ljfh;->s()V

    .line 173
    return-void
.end method

.method public b(J)I
    .locals 1

    .prologue
    .line 277
    invoke-virtual {p0, p1, p2}, Ljfh;->d(J)I

    move-result v0

    .line 278
    invoke-virtual {p0, v0}, Ljfh;->d(I)I

    move-result v0

    return v0
.end method

.method public abstract b(Landroid/database/Cursor;)Landroid/content/Intent;
.end method

.method public abstract b(Ljgb;)V
.end method

.method public abstract b()Z
.end method

.method public abstract c()I
.end method

.method public abstract c(Landroid/database/Cursor;)J
.end method

.method public c(J)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 283
    invoke-virtual {p0, p1, p2}, Ljfh;->d(J)I

    move-result v0

    .line 284
    invoke-virtual {p0, v0}, Ljfh;->a(I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public c(I)Z
    .locals 1

    .prologue
    .line 53
    if-nez p1, :cond_0

    invoke-virtual {p0}, Ljfh;->aq_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 54
    invoke-virtual {p0}, Ljfh;->b()Z

    move-result v0

    .line 61
    :goto_0
    return v0

    .line 55
    :cond_0
    invoke-virtual {p0}, Ljfh;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_1

    invoke-virtual {p0}, Ljfh;->ar_()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 56
    invoke-virtual {p0}, Ljfh;->g()Z

    move-result v0

    goto :goto_0

    .line 57
    :cond_1
    if-ltz p1, :cond_2

    .line 58
    invoke-direct {p0, p1}, Ljfh;->f(I)V

    .line 59
    iget-object v0, p0, Ljfh;->c:Landroid/database/Cursor;

    invoke-virtual {p0}, Ljfh;->j()Z

    move-result v0

    goto :goto_0

    .line 61
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d(I)I
    .locals 1

    .prologue
    .line 83
    if-nez p1, :cond_0

    invoke-virtual {p0}, Ljfh;->aq_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 84
    invoke-virtual {p0}, Ljfh;->c()I

    move-result v0

    .line 91
    :goto_0
    return v0

    .line 85
    :cond_0
    invoke-virtual {p0}, Ljfh;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_1

    invoke-virtual {p0}, Ljfh;->ar_()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 86
    invoke-virtual {p0}, Ljfh;->h()I

    move-result v0

    goto :goto_0

    .line 87
    :cond_1
    if-ltz p1, :cond_2

    .line 88
    invoke-direct {p0, p1}, Ljfh;->f(I)V

    .line 89
    iget-object v0, p0, Ljfh;->c:Landroid/database/Cursor;

    invoke-virtual {p0, v0}, Ljfh;->a(Landroid/database/Cursor;)I

    move-result v0

    goto :goto_0

    .line 91
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d(J)I
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 295
    const-wide/high16 v4, -0x8000000000000000L

    cmp-long v0, p1, v4

    if-nez v0, :cond_0

    invoke-virtual {p0}, Ljfh;->aq_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 310
    :goto_0
    return v2

    .line 299
    :cond_0
    iget-object v0, p0, Ljfh;->c:Landroid/database/Cursor;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ljfh;->c:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_3

    .line 301
    iget-object v0, p0, Ljfh;->c:Landroid/database/Cursor;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move v0, v1

    .line 302
    :cond_1
    iget-object v3, p0, Ljfh;->c:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 303
    add-int/lit8 v0, v0, 0x1

    .line 304
    iget-object v3, p0, Ljfh;->c:Landroid/database/Cursor;

    invoke-virtual {p0, v3}, Ljfh;->c(Landroid/database/Cursor;)J

    move-result-wide v4

    .line 305
    cmp-long v3, v4, p1

    if-nez v3, :cond_1

    .line 306
    invoke-virtual {p0}, Ljfh;->aq_()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :goto_1
    add-int v2, v0, v1

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1

    :cond_3
    move v2, v1

    .line 310
    goto :goto_0
.end method

.method public e(I)J
    .locals 2

    .prologue
    .line 201
    if-nez p1, :cond_0

    invoke-virtual {p0}, Ljfh;->aq_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 202
    const-wide/high16 v0, -0x8000000000000000L

    .line 207
    :goto_0
    return-wide v0

    .line 203
    :cond_0
    invoke-virtual {p0}, Ljfh;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_1

    invoke-virtual {p0}, Ljfh;->ar_()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 204
    const-wide v0, 0x7fffffffffffffffL

    goto :goto_0

    .line 206
    :cond_1
    invoke-direct {p0, p1}, Ljfh;->f(I)V

    .line 207
    iget-object v0, p0, Ljfh;->c:Landroid/database/Cursor;

    invoke-virtual {p0, v0}, Ljfh;->c(Landroid/database/Cursor;)J

    move-result-wide v0

    goto :goto_0
.end method

.method public abstract e()Landroid/content/Intent;
.end method

.method public abstract g()Z
.end method

.method public abstract h()I
.end method

.method public abstract i()Landroid/content/Intent;
.end method

.method public abstract j()Z
.end method

.method public abstract l()Landroid/database/Cursor;
.end method

.method protected s()V
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Ljfh;->c:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 178
    iget-object v0, p0, Ljfh;->c:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 179
    const/4 v0, 0x0

    iput-object v0, p0, Ljfh;->c:Landroid/database/Cursor;

    .line 181
    :cond_0
    invoke-virtual {p0}, Ljfh;->v()V

    .line 182
    return-void
.end method

.method public t()Z
    .locals 1

    .prologue
    .line 212
    invoke-virtual {p0}, Ljfh;->u()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public u()I
    .locals 2

    .prologue
    .line 216
    const/4 v0, 0x0

    .line 218
    iget-object v1, p0, Ljfh;->c:Landroid/database/Cursor;

    if-eqz v1, :cond_0

    iget-object v1, p0, Ljfh;->c:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 219
    iget-object v0, p0, Ljfh;->c:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 222
    :cond_0
    return v0
.end method

.method protected v()V
    .locals 1

    .prologue
    .line 264
    iget-boolean v0, p0, Ljfh;->d:Z

    if-nez v0, :cond_0

    .line 265
    invoke-super {p0}, Ljfy;->v()V

    .line 267
    :cond_0
    return-void
.end method
