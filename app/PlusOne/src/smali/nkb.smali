.class public final Lnkb;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field private b:Ljava/lang/Integer;

.field private c:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 663
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 685
    const/4 v0, 0x1

    iget-object v1, p0, Lnkb;->b:Ljava/lang/Integer;

    .line 687
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 688
    const/4 v1, 0x2

    iget-object v2, p0, Lnkb;->c:Ljava/lang/Integer;

    .line 689
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 690
    iget-object v1, p0, Lnkb;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 691
    const/4 v1, 0x3

    iget-object v2, p0, Lnkb;->a:Ljava/lang/String;

    .line 692
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 694
    :cond_0
    iget-object v1, p0, Lnkb;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 695
    iput v0, p0, Lnkb;->ai:I

    .line 696
    return v0
.end method

.method public a(Loxn;)Lnkb;
    .locals 2

    .prologue
    .line 704
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 705
    sparse-switch v0, :sswitch_data_0

    .line 709
    iget-object v1, p0, Lnkb;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 710
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnkb;->ah:Ljava/util/List;

    .line 713
    :cond_1
    iget-object v1, p0, Lnkb;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 715
    :sswitch_0
    return-object p0

    .line 720
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnkb;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 724
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnkb;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 728
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnkb;->a:Ljava/lang/String;

    goto :goto_0

    .line 705
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 674
    const/4 v0, 0x1

    iget-object v1, p0, Lnkb;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 675
    const/4 v0, 0x2

    iget-object v1, p0, Lnkb;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 676
    iget-object v0, p0, Lnkb;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 677
    const/4 v0, 0x3

    iget-object v1, p0, Lnkb;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 679
    :cond_0
    iget-object v0, p0, Lnkb;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 681
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 659
    invoke-virtual {p0, p1}, Lnkb;->a(Loxn;)Lnkb;

    move-result-object v0

    return-object v0
.end method
