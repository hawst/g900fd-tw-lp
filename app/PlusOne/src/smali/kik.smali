.class public Lkik;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llpw;
.implements Llrg;


# instance fields
.field private a:Los;

.field private b:Lkhs;


# direct methods
.method public constructor <init>(Los;Lkhs;Llqr;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object p1, p0, Lkik;->a:Los;

    .line 60
    iput-object p2, p0, Lkik;->b:Lkhs;

    .line 61
    invoke-virtual {p3, p0}, Llqr;->a(Llrg;)Llrg;

    .line 62
    return-void
.end method

.method public constructor <init>(Los;Llqr;)V
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lkik;-><init>(Los;Lkhs;Llqr;)V

    .line 55
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Intent;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x0

    return-object v0
.end method

.method public a()Lkhs;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lkik;->b:Lkhs;

    return-object v0
.end method

.method public b_(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 67
    if-nez p1, :cond_2

    .line 68
    invoke-virtual {p0}, Lkik;->a()Lkhs;

    move-result-object v0

    .line 69
    if-nez v0, :cond_0

    .line 70
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "A PreferenceFragment must be provided!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 72
    :cond_0
    iget-object v1, p0, Lkik;->a:Los;

    invoke-virtual {v1}, Los;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lkik;->a(Landroid/content/Intent;)Landroid/os/Bundle;

    move-result-object v1

    .line 73
    if-eqz v1, :cond_1

    .line 74
    invoke-virtual {v0, v1}, Lkhs;->f(Landroid/os/Bundle;)V

    .line 76
    :cond_1
    iget-object v1, p0, Lkik;->a:Los;

    invoke-virtual {v1}, Los;->f()Lae;

    move-result-object v1

    invoke-virtual {v1}, Lae;->a()Lat;

    move-result-object v1

    const v2, 0x7f0a056d

    invoke-virtual {v1, v2, v0}, Lat;->a(ILu;)Lat;

    move-result-object v0

    .line 77
    invoke-virtual {v0}, Lat;->b()I

    .line 79
    :cond_2
    return-void
.end method
