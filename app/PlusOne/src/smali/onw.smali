.class public final Lonw;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lonz;

.field public b:Lony;

.field public c:Looa;

.field public d:Lonx;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 302
    iput-object v0, p0, Lonw;->a:Lonz;

    .line 305
    iput-object v0, p0, Lonw;->b:Lony;

    .line 308
    iput-object v0, p0, Lonw;->c:Looa;

    .line 311
    iput-object v0, p0, Lonw;->d:Lonx;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 334
    const/4 v0, 0x0

    .line 335
    iget-object v1, p0, Lonw;->a:Lonz;

    if-eqz v1, :cond_0

    .line 336
    const/4 v0, 0x1

    iget-object v1, p0, Lonw;->a:Lonz;

    .line 337
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 339
    :cond_0
    iget-object v1, p0, Lonw;->b:Lony;

    if-eqz v1, :cond_1

    .line 340
    const/4 v1, 0x2

    iget-object v2, p0, Lonw;->b:Lony;

    .line 341
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 343
    :cond_1
    iget-object v1, p0, Lonw;->c:Looa;

    if-eqz v1, :cond_2

    .line 344
    const/4 v1, 0x3

    iget-object v2, p0, Lonw;->c:Looa;

    .line 345
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 347
    :cond_2
    iget-object v1, p0, Lonw;->d:Lonx;

    if-eqz v1, :cond_3

    .line 348
    const/4 v1, 0x4

    iget-object v2, p0, Lonw;->d:Lonx;

    .line 349
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 351
    :cond_3
    iget-object v1, p0, Lonw;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 352
    iput v0, p0, Lonw;->ai:I

    .line 353
    return v0
.end method

.method public a(Loxn;)Lonw;
    .locals 2

    .prologue
    .line 361
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 362
    sparse-switch v0, :sswitch_data_0

    .line 366
    iget-object v1, p0, Lonw;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 367
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lonw;->ah:Ljava/util/List;

    .line 370
    :cond_1
    iget-object v1, p0, Lonw;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 372
    :sswitch_0
    return-object p0

    .line 377
    :sswitch_1
    iget-object v0, p0, Lonw;->a:Lonz;

    if-nez v0, :cond_2

    .line 378
    new-instance v0, Lonz;

    invoke-direct {v0}, Lonz;-><init>()V

    iput-object v0, p0, Lonw;->a:Lonz;

    .line 380
    :cond_2
    iget-object v0, p0, Lonw;->a:Lonz;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 384
    :sswitch_2
    iget-object v0, p0, Lonw;->b:Lony;

    if-nez v0, :cond_3

    .line 385
    new-instance v0, Lony;

    invoke-direct {v0}, Lony;-><init>()V

    iput-object v0, p0, Lonw;->b:Lony;

    .line 387
    :cond_3
    iget-object v0, p0, Lonw;->b:Lony;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 391
    :sswitch_3
    iget-object v0, p0, Lonw;->c:Looa;

    if-nez v0, :cond_4

    .line 392
    new-instance v0, Looa;

    invoke-direct {v0}, Looa;-><init>()V

    iput-object v0, p0, Lonw;->c:Looa;

    .line 394
    :cond_4
    iget-object v0, p0, Lonw;->c:Looa;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 398
    :sswitch_4
    iget-object v0, p0, Lonw;->d:Lonx;

    if-nez v0, :cond_5

    .line 399
    new-instance v0, Lonx;

    invoke-direct {v0}, Lonx;-><init>()V

    iput-object v0, p0, Lonw;->d:Lonx;

    .line 401
    :cond_5
    iget-object v0, p0, Lonw;->d:Lonx;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 362
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 316
    iget-object v0, p0, Lonw;->a:Lonz;

    if-eqz v0, :cond_0

    .line 317
    const/4 v0, 0x1

    iget-object v1, p0, Lonw;->a:Lonz;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 319
    :cond_0
    iget-object v0, p0, Lonw;->b:Lony;

    if-eqz v0, :cond_1

    .line 320
    const/4 v0, 0x2

    iget-object v1, p0, Lonw;->b:Lony;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 322
    :cond_1
    iget-object v0, p0, Lonw;->c:Looa;

    if-eqz v0, :cond_2

    .line 323
    const/4 v0, 0x3

    iget-object v1, p0, Lonw;->c:Looa;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 325
    :cond_2
    iget-object v0, p0, Lonw;->d:Lonx;

    if-eqz v0, :cond_3

    .line 326
    const/4 v0, 0x4

    iget-object v1, p0, Lonw;->d:Lonx;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 328
    :cond_3
    iget-object v0, p0, Lonw;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 330
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lonw;->a(Loxn;)Lonw;

    move-result-object v0

    return-object v0
.end method
