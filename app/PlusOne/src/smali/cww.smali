.class public Lcww;
.super Llol;
.source "PG"


# instance fields
.field private N:Lhei;

.field private O:Lhee;

.field private P:Ldeo;

.field private Q:Landroid/view/View;

.field private R:Landroid/view/animation/Animation;

.field private S:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 25
    invoke-direct {p0}, Llol;-><init>()V

    .line 37
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcww;->S:Z

    .line 40
    new-instance v0, Ldem;

    iget-object v1, p0, Lcww;->av:Llqm;

    new-instance v2, Lcwx;

    invoke-direct {v2, p0}, Lcwx;-><init>(Lcww;)V

    invoke-direct {v0, v1, v2}, Ldem;-><init>(Llqr;Lden;)V

    .line 49
    return-void
.end method

.method static synthetic a(Lcww;)Z
    .locals 1

    .prologue
    .line 25
    iget-boolean v0, p0, Lcww;->S:Z

    return v0
.end method

.method static synthetic a(Lcww;Z)Z
    .locals 0

    .prologue
    .line 25
    iput-boolean p1, p0, Lcww;->S:Z

    return p1
.end method

.method static synthetic b(Lcww;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 25
    iget-object v0, p0, Lcww;->O:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lcww;->N:Lhei;

    invoke-interface {v1, v0}, Lhei;->a(I)Lhej;

    move-result-object v1

    const-string v2, "showed_cloud_aam_editing_promo"

    invoke-interface {v1, v2, v3}, Lhej;->a(Ljava/lang/String;Z)Z

    move-result v1

    iget-object v2, p0, Lcww;->P:Ldeo;

    invoke-virtual {v2}, Ldeo;->a()Lddl;

    move-result-object v2

    if-nez v1, :cond_0

    if-eqz v2, :cond_0

    invoke-interface {v2}, Lddl;->U()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Lddl;->r()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcww;->N:Lhei;

    invoke-interface {v1, v0}, Lhei;->b(I)Lhek;

    move-result-object v0

    const-string v1, "showed_cloud_aam_editing_promo"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lhek;->b(Ljava/lang/String;Z)Lhek;

    move-result-object v0

    invoke-interface {v0}, Lhek;->c()I

    iget-object v0, p0, Lcww;->Q:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcww;->Q:Landroid/view/View;

    iget-object v1, p0, Lcww;->R:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_0
    return-void
.end method

.method static synthetic c(Lcww;)Landroid/view/View;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcww;->Q:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 71
    const v0, 0x7f04006d

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcww;->Q:Landroid/view/View;

    .line 72
    iget-object v0, p0, Lcww;->Q:Landroid/view/View;

    return-object v0
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 53
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 54
    iget-object v0, p0, Lcww;->au:Llnh;

    const-class v1, Lhei;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Lcww;->N:Lhei;

    .line 55
    iget-object v0, p0, Lcww;->au:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Lcww;->O:Lhee;

    .line 57
    iget-object v0, p0, Lcww;->au:Llnh;

    const-class v1, Ldeo;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldeo;

    iput-object v0, p0, Lcww;->P:Ldeo;

    .line 59
    invoke-virtual {p0}, Lcww;->n()Lz;

    move-result-object v0

    const v1, 0x7f050014

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcww;->R:Landroid/view/animation/Animation;

    .line 60
    iget-object v0, p0, Lcww;->R:Landroid/view/animation/Animation;

    new-instance v1, Lcwy;

    invoke-direct {v1, p0}, Lcwy;-><init>(Lcww;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 66
    return-void
.end method
