.class public final Leaf;
.super Lt;
.source "PG"

# interfaces
.implements Landroid/app/TimePickerDialog$OnTimeSetListener;
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private N:I

.field private O:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1478
    invoke-direct {p0}, Lt;-><init>()V

    .line 1474
    const/4 v0, -0x1

    iput v0, p0, Leaf;->N:I

    .line 1479
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 1487
    invoke-direct {p0}, Lt;-><init>()V

    .line 1474
    const/4 v0, -0x1

    iput v0, p0, Leaf;->N:I

    .line 1489
    iput p1, p0, Leaf;->N:I

    .line 1490
    return-void
.end method


# virtual methods
.method public c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6

    .prologue
    const/4 v1, -0x1

    .line 1494
    iget v0, p0, Leaf;->N:I

    if-ne v0, v1, :cond_0

    .line 1495
    const-string v0, "type"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Leaf;->N:I

    .line 1496
    const-string v0, "cancelled"

    iget-boolean v1, p0, Leaf;->O:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Leaf;->O:Z

    .line 1498
    :cond_0
    invoke-virtual {p0}, Leaf;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "date_time"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 1499
    invoke-virtual {p0}, Leaf;->k()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "time_zone"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1500
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    .line 1501
    invoke-static {v2}, Lidk;->a(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 1502
    invoke-virtual {v4, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1503
    new-instance v0, Landroid/app/TimePickerDialog;

    invoke-virtual {p0}, Leaf;->n()Lz;

    move-result-object v1

    const/16 v2, 0xb

    .line 1504
    invoke-virtual {v4, v2}, Ljava/util/Calendar;->get(I)I

    move-result v3

    const/16 v2, 0xc

    invoke-virtual {v4, v2}, Ljava/util/Calendar;->get(I)I

    move-result v4

    .line 1505
    invoke-virtual {p0}, Leaf;->n()Lz;

    move-result-object v2

    invoke-static {v2}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v5

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Landroid/app/TimePickerDialog;-><init>(Landroid/content/Context;Landroid/app/TimePickerDialog$OnTimeSetListener;IIZ)V

    .line 1506
    iget v1, p0, Leaf;->N:I

    if-nez v1, :cond_1

    .line 1507
    const/4 v1, -0x2

    const v2, 0x7f0a07fc

    invoke-virtual {p0, v2}, Leaf;->e_(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p0}, Landroid/app/TimePickerDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 1509
    :cond_1
    return-object v0
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1539
    invoke-super {p0, p1}, Lt;->e(Landroid/os/Bundle;)V

    .line 1541
    const-string v0, "type"

    iget v1, p0, Leaf;->N:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1542
    const-string v0, "cancelled"

    iget-boolean v1, p0, Leaf;->O:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1543
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1

    .prologue
    .line 1528
    invoke-virtual {p0}, Leaf;->u_()Lu;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/fragments/EditEventFragment;

    .line 1529
    packed-switch p2, :pswitch_data_0

    .line 1535
    :goto_0
    return-void

    .line 1531
    :pswitch_0
    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->V()V

    .line 1532
    const/4 v0, 0x1

    iput-boolean v0, p0, Leaf;->O:Z

    goto :goto_0

    .line 1529
    nop

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_0
    .end packed-switch
.end method

.method public onTimeSet(Landroid/widget/TimePicker;II)V
    .locals 3

    .prologue
    .line 1514
    iget-boolean v0, p0, Leaf;->O:Z

    if-eqz v0, :cond_0

    .line 1524
    :goto_0
    return-void

    .line 1518
    :cond_0
    invoke-virtual {p0}, Leaf;->u_()Lu;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/fragments/EditEventFragment;

    .line 1519
    iget v1, p0, Leaf;->N:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 1520
    invoke-virtual {v0, p2, p3}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->a(II)V

    goto :goto_0

    .line 1522
    :cond_1
    invoke-virtual {v0, p2, p3}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->b(II)V

    goto :goto_0
.end method
