.class public final Ljta;
.super Lhxz;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lhxz",
        "<",
        "Ljava/util/List",
        "<",
        "Ljtf;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final b:I

.field private final c:Ljava/lang/String;

.field private d:Ljtb;

.field private e:Ljava/lang/String;

.field private f:Z

.field private g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;ZLjava/lang/String;Z)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lhxz;-><init>(Landroid/content/Context;)V

    .line 39
    iput p2, p0, Ljta;->b:I

    .line 40
    iput-object p3, p0, Ljta;->c:Ljava/lang/String;

    .line 41
    iput-boolean p4, p0, Ljta;->f:Z

    .line 42
    iput-object p5, p0, Ljta;->e:Ljava/lang/String;

    .line 43
    iput-boolean p6, p0, Ljta;->g:Z

    .line 44
    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 21
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Ljta;->a(Ljava/util/List;)V

    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljtf;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 222
    invoke-super {p0, p1}, Lhxz;->a(Ljava/lang/Object;)V

    .line 223
    iget-object v0, p0, Ljta;->d:Ljtb;

    if-eqz v0, :cond_0

    .line 227
    iget-object v0, p0, Ljta;->d:Ljtb;

    invoke-virtual {v0}, Ljtb;->m()V

    .line 229
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Ljta;->d:Ljtb;

    .line 230
    return-void
.end method

.method public synthetic j()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0}, Ljta;->m()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Ljta;->e:Ljava/lang/String;

    return-object v0
.end method

.method public m()Ljava/util/List;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljtf;",
            ">;"
        }
    .end annotation

    .prologue
    .line 57
    new-instance v1, Ljtb;

    .line 58
    invoke-virtual/range {p0 .. p0}, Ljta;->n()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, p0

    iget v3, v0, Ljta;->b:I

    move-object/from16 v0, p0

    iget-object v4, v0, Ljta;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Ljta;->e:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v4, v5}, Ljtb;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V

    .line 60
    move-object/from16 v0, p0

    iput-object v1, v0, Ljta;->d:Ljtb;

    .line 62
    :try_start_0
    invoke-virtual {v1}, Ljtb;->l()V

    .line 63
    invoke-virtual {v1}, Ljtb;->n()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_0

    .line 64
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-object v1, v0, Ljta;->d:Ljtb;

    const/4 v1, 0x0

    .line 88
    :goto_0
    return-object v1

    .line 64
    :cond_0
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Ljta;->d:Ljtb;

    .line 70
    invoke-virtual {v1}, Ljtb;->t()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 71
    const-string v2, "ACMergedPeople"

    invoke-virtual {v1, v2}, Ljtb;->d(Ljava/lang/String;)V

    .line 72
    const/4 v1, 0x0

    goto :goto_0

    .line 67
    :catchall_0
    move-exception v1

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Ljta;->d:Ljtb;

    throw v1

    .line 75
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Ljta;->e:Ljava/lang/String;

    .line 76
    invoke-virtual {v1}, Ljtb;->i()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Ljta;->e:Ljava/lang/String;

    .line 78
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 80
    invoke-virtual {v1}, Ljtb;->j()[Llww;

    move-result-object v15

    .line 81
    const/4 v1, 0x0

    move v11, v1

    :goto_1
    array-length v1, v15

    if-ge v11, v1, :cond_1b

    .line 82
    aget-object v1, v15, v11

    iget-object v12, v1, Llww;->b:Llxo;

    iget-object v1, v12, Llxo;->b:Llxq;

    if-nez v1, :cond_3

    const/4 v3, 0x0

    :goto_2
    iget-object v1, v12, Llxo;->f:[Llxa;

    if-eqz v1, :cond_4

    array-length v2, v1

    if-lez v2, :cond_4

    const/4 v2, 0x0

    aget-object v1, v1, v2

    iget-object v1, v1, Llxa;->b:Ljava/lang/String;

    :goto_3
    move-object/from16 v0, p0

    iget-boolean v2, v0, Ljta;->f:Z

    if-eqz v2, :cond_5

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v1, 0x0

    .line 83
    :goto_4
    if-eqz v1, :cond_2

    .line 84
    invoke-interface {v13, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 81
    :cond_2
    add-int/lit8 v1, v11, 0x1

    move v11, v1

    goto :goto_1

    .line 82
    :cond_3
    iget-object v3, v1, Llxq;->a:Ljava/lang/String;

    goto :goto_2

    :cond_4
    const/4 v1, 0x0

    goto :goto_3

    :cond_5
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7

    const-string v2, "g:"

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_6

    invoke-virtual {v2, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_5
    move-object v2, v1

    :goto_6
    iget-object v1, v12, Llxo;->c:[Llxk;

    if-eqz v1, :cond_b

    array-length v4, v1

    if-lez v4, :cond_b

    const/4 v4, 0x0

    aget-object v1, v1, v4

    :goto_7
    if-nez v1, :cond_c

    const/4 v1, 0x0

    goto :goto_4

    :cond_6
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_5

    :cond_7
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_9

    const-string v2, "e:"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_8

    invoke-virtual {v2, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_8
    move-object v2, v1

    goto :goto_6

    :cond_8
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_8

    :cond_9
    const-string v1, "ACMergedPeople"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x27

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "invalid response, no gaiaId nor email: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_a
    const/4 v1, 0x0

    goto :goto_4

    :cond_b
    const/4 v1, 0x0

    goto :goto_7

    :cond_c
    iget-object v4, v1, Llxk;->c:Ljava/lang/String;

    iget-object v5, v1, Llxk;->b:Llxp;

    if-nez v5, :cond_d

    const/4 v9, 0x0

    :goto_9
    iget-object v1, v12, Llxo;->d:[Llxs;

    if-eqz v1, :cond_e

    array-length v5, v1

    if-lez v5, :cond_e

    const/4 v5, 0x0

    aget-object v1, v1, v5

    iget-object v5, v1, Llxs;->b:Ljava/lang/String;

    :goto_a
    iget-object v6, v12, Llxo;->b:Llxq;

    if-eqz v6, :cond_f

    iget-object v1, v6, Llxq;->c:Ljava/lang/Boolean;

    invoke-static {v1}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v1

    if-eqz v1, :cond_f

    const/4 v1, 0x1

    move v8, v1

    :goto_b
    move-object/from16 v0, p0

    iget-boolean v1, v0, Ljta;->g:Z

    if-eqz v1, :cond_10

    if-nez v8, :cond_10

    const/4 v1, 0x0

    goto/16 :goto_4

    :cond_d
    iget-object v1, v1, Llxk;->b:Llxp;

    iget-object v1, v1, Llxp;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    goto :goto_9

    :cond_e
    const/4 v5, 0x0

    goto :goto_a

    :cond_f
    const/4 v1, 0x0

    move v8, v1

    goto :goto_b

    :cond_10
    if-nez v6, :cond_13

    const/4 v6, -0x1

    :goto_c
    const-string v7, ""

    iget-object v1, v12, Llxo;->e:[Llya;

    if-eqz v1, :cond_15

    array-length v10, v1

    if-lez v10, :cond_15

    const/4 v10, 0x0

    aget-object v1, v1, v10

    iget-object v1, v1, Llya;->b:Ljava/lang/String;

    :goto_d
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_1d

    :goto_e
    iget-object v7, v12, Llxo;->g:[Llxn;

    if-eqz v7, :cond_16

    array-length v10, v7

    if-lez v10, :cond_16

    const/4 v10, 0x0

    aget-object v7, v7, v10

    iget-object v7, v7, Llxn;->b:Ljava/lang/String;

    :goto_f
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_17

    const/4 v10, 0x1

    :goto_10
    iget-object v12, v12, Llxo;->h:[Llxm;

    if-eqz v12, :cond_18

    array-length v14, v12

    if-lez v14, :cond_18

    const/4 v14, 0x0

    aget-object v12, v12, v14

    iget-object v12, v12, Llxm;->b:Ljava/lang/String;

    :goto_11
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_19

    const/4 v14, 0x1

    :goto_12
    if-eqz v10, :cond_11

    if-eqz v14, :cond_11

    invoke-virtual/range {p0 .. p0}, Ljta;->n()Landroid/content/Context;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    const v17, 0x7f0a0587

    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    aput-object v12, v18, v19

    const/16 v19, 0x1

    aput-object v7, v18, v19

    invoke-virtual/range {v16 .. v18}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    :cond_11
    if-eqz v10, :cond_1c

    move-object v10, v7

    :goto_13
    if-eqz v14, :cond_12

    move-object v10, v12

    :cond_12
    new-instance v1, Ljtf;

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    if-eqz v8, :cond_1a

    const/4 v8, 0x2

    :goto_14
    invoke-direct/range {v1 .. v10}, Ljtf;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/util/ArrayList;IZLjava/lang/String;)V

    goto/16 :goto_4

    :cond_13
    iget v1, v6, Llxq;->b:I

    packed-switch v1, :pswitch_data_0

    const-string v1, "ACMergedPeople"

    const/4 v7, 0x5

    invoke-static {v1, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_14

    iget v1, v6, Llxq;->b:I

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, 0x20

    invoke-direct {v7, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v10, "invalid objectType "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, ": "

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_14
    const/4 v6, 0x1

    goto/16 :goto_c

    :pswitch_0
    const/4 v6, 0x1

    goto/16 :goto_c

    :pswitch_1
    const/4 v6, 0x2

    goto/16 :goto_c

    :cond_15
    const/4 v1, 0x0

    goto/16 :goto_d

    :cond_16
    const/4 v7, 0x0

    goto/16 :goto_f

    :cond_17
    const/4 v10, 0x0

    goto/16 :goto_10

    :cond_18
    const/4 v12, 0x0

    goto/16 :goto_11

    :cond_19
    const/4 v14, 0x0

    goto/16 :goto_12

    :cond_1a
    const/4 v8, 0x1

    goto :goto_14

    :cond_1b
    move-object v1, v13

    .line 88
    goto/16 :goto_0

    :cond_1c
    move-object v10, v1

    goto :goto_13

    :cond_1d
    move-object v1, v7

    goto/16 :goto_e

    .line 82
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
