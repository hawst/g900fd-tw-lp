.class public final Lenp;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field private final a:Landroid/widget/EditText;

.field private final b:Lenq;

.field private c:Ljava/lang/String;

.field private final d:Z

.field private synthetic e:Lenl;


# direct methods
.method public constructor <init>(Lenl;Landroid/widget/EditText;Lenq;Z)V
    .locals 0

    .prologue
    .line 1112
    iput-object p1, p0, Lenp;->e:Lenl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1113
    iput-object p2, p0, Lenp;->a:Landroid/widget/EditText;

    .line 1114
    iput-object p3, p0, Lenp;->b:Lenq;

    .line 1115
    iput-boolean p4, p0, Lenp;->d:Z

    .line 1116
    return-void
.end method

.method public constructor <init>(Lenl;Z)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1106
    iput-object p1, p0, Lenp;->e:Lenl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1107
    iput-object v0, p0, Lenp;->a:Landroid/widget/EditText;

    .line 1108
    iput-object v0, p0, Lenp;->b:Lenq;

    .line 1109
    iput-boolean p2, p0, Lenp;->d:Z

    .line 1110
    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1120
    iget-object v1, p0, Lenp;->a:Landroid/widget/EditText;

    if-eqz v1, :cond_0

    .line 1121
    if-eqz p2, :cond_1

    .line 1122
    iget-object v1, p0, Lenp;->a:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lenp;->c:Ljava/lang/String;

    .line 1125
    iget-object v1, p0, Lenp;->e:Lenl;

    iget-object v2, p0, Lenp;->a:Landroid/widget/EditText;

    invoke-virtual {v1, v2}, Lenl;->c(Landroid/view/View;)V

    .line 1126
    iget-object v1, p0, Lenp;->a:Landroid/widget/EditText;

    iget-object v2, p0, Lenp;->b:Lenq;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1128
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 1129
    iget-object v2, p0, Lenp;->a:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1137
    :goto_0
    iget-object v1, p0, Lenp;->a:Landroid/widget/EditText;

    if-nez p2, :cond_3

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 1140
    :cond_0
    iget-boolean v0, p0, Lenp;->d:Z

    if-ne v0, p2, :cond_4

    .line 1141
    iget-object v0, p0, Lenp;->e:Lenl;

    invoke-virtual {v0, p1}, Lenl;->c(Landroid/view/View;)V

    .line 1145
    :goto_2
    return-void

    .line 1131
    :cond_1
    iget-object v1, p0, Lenp;->c:Ljava/lang/String;

    if-nez v1, :cond_2

    .line 1132
    iget-object v1, p0, Lenp;->a:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lenp;->c:Ljava/lang/String;

    .line 1134
    :cond_2
    iget-object v1, p0, Lenp;->a:Landroid/widget/EditText;

    iget-object v2, p0, Lenp;->b:Lenq;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1135
    iget-object v1, p0, Lenp;->a:Landroid/widget/EditText;

    iget-object v2, p0, Lenp;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1137
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 1143
    :cond_4
    iget-object v0, p0, Lenp;->e:Lenl;

    invoke-virtual {v0, p1}, Lenl;->d(Landroid/view/View;)V

    goto :goto_2
.end method
