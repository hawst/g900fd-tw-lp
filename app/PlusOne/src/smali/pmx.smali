.class public final Lpmx;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lpmx;


# instance fields
.field private A:Lprv;

.field private B:I

.field private C:Ljava/lang/String;

.field private D:Ljava/lang/String;

.field private E:Ljava/lang/String;

.field private F:Ljava/lang/String;

.field public b:Lpgx;

.field public c:Ljava/lang/Integer;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Lpxp;

.field public h:Ljava/lang/String;

.field private i:Ljava/lang/Boolean;

.field private j:Ljava/lang/Boolean;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Lpvy;

.field private o:Lpmy;

.field private p:Lppf;

.field private q:[Ljava/lang/String;

.field private r:[Ljava/lang/String;

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/Boolean;

.field private u:Lppf;

.field private v:Lpmy;

.field private w:Lpna;

.field private x:[Lpps;

.field private y:[Lppr;

.field private z:Lprw;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x0

    new-array v0, v0, [Lpmx;

    sput-object v0, Lpmx;->a:[Lpmx;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 26
    invoke-direct {p0}, Loxq;-><init>()V

    .line 147
    iput-object v1, p0, Lpmx;->b:Lpgx;

    .line 166
    iput-object v1, p0, Lpmx;->n:Lpvy;

    .line 169
    iput-object v1, p0, Lpmx;->o:Lpmy;

    .line 172
    iput-object v1, p0, Lpmx;->p:Lppf;

    .line 175
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lpmx;->q:[Ljava/lang/String;

    .line 178
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lpmx;->r:[Ljava/lang/String;

    .line 187
    iput-object v1, p0, Lpmx;->u:Lppf;

    .line 190
    iput-object v1, p0, Lpmx;->v:Lpmy;

    .line 193
    iput-object v1, p0, Lpmx;->w:Lpna;

    .line 196
    sget-object v0, Lpps;->a:[Lpps;

    iput-object v0, p0, Lpmx;->x:[Lpps;

    .line 199
    sget-object v0, Lppr;->a:[Lppr;

    iput-object v0, p0, Lpmx;->y:[Lppr;

    .line 202
    iput-object v1, p0, Lpmx;->g:Lpxp;

    .line 205
    iput-object v1, p0, Lpmx;->z:Lprw;

    .line 208
    iput-object v1, p0, Lpmx;->A:Lprv;

    .line 211
    const/high16 v0, -0x80000000

    iput v0, p0, Lpmx;->B:I

    .line 26
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 337
    .line 338
    iget-object v0, p0, Lpmx;->b:Lpgx;

    if-eqz v0, :cond_22

    .line 339
    const/4 v0, 0x1

    iget-object v2, p0, Lpmx;->b:Lpgx;

    .line 340
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 342
    :goto_0
    iget-object v2, p0, Lpmx;->c:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    .line 343
    const/4 v2, 0x2

    iget-object v3, p0, Lpmx;->c:Ljava/lang/Integer;

    .line 344
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 346
    :cond_0
    iget-object v2, p0, Lpmx;->k:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 347
    const/4 v2, 0x3

    iget-object v3, p0, Lpmx;->k:Ljava/lang/String;

    .line 348
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 350
    :cond_1
    iget-object v2, p0, Lpmx;->d:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 351
    const/4 v2, 0x4

    iget-object v3, p0, Lpmx;->d:Ljava/lang/String;

    .line 352
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 354
    :cond_2
    iget-object v2, p0, Lpmx;->m:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 355
    const/4 v2, 0x5

    iget-object v3, p0, Lpmx;->m:Ljava/lang/String;

    .line 356
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 358
    :cond_3
    iget-object v2, p0, Lpmx;->e:Ljava/lang/String;

    if-eqz v2, :cond_4

    .line 359
    const/4 v2, 0x6

    iget-object v3, p0, Lpmx;->e:Ljava/lang/String;

    .line 360
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 362
    :cond_4
    iget-object v2, p0, Lpmx;->o:Lpmy;

    if-eqz v2, :cond_5

    .line 363
    const/4 v2, 0x7

    iget-object v3, p0, Lpmx;->o:Lpmy;

    .line 364
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 366
    :cond_5
    iget-object v2, p0, Lpmx;->p:Lppf;

    if-eqz v2, :cond_6

    .line 367
    const/16 v2, 0x8

    iget-object v3, p0, Lpmx;->p:Lppf;

    .line 368
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 370
    :cond_6
    iget-object v2, p0, Lpmx;->q:[Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lpmx;->q:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_8

    .line 372
    iget-object v4, p0, Lpmx;->q:[Ljava/lang/String;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_1
    if-ge v2, v5, :cond_7

    aget-object v6, v4, v2

    .line 374
    invoke-static {v6}, Loxo;->b(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v3, v6

    .line 372
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 376
    :cond_7
    add-int/2addr v0, v3

    .line 377
    iget-object v2, p0, Lpmx;->q:[Ljava/lang/String;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 379
    :cond_8
    iget-object v2, p0, Lpmx;->r:[Ljava/lang/String;

    if-eqz v2, :cond_a

    iget-object v2, p0, Lpmx;->r:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_a

    .line 381
    iget-object v4, p0, Lpmx;->r:[Ljava/lang/String;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_2
    if-ge v2, v5, :cond_9

    aget-object v6, v4, v2

    .line 383
    invoke-static {v6}, Loxo;->b(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v3, v6

    .line 381
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 385
    :cond_9
    add-int/2addr v0, v3

    .line 386
    iget-object v2, p0, Lpmx;->r:[Ljava/lang/String;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 388
    :cond_a
    iget-object v2, p0, Lpmx;->f:Ljava/lang/String;

    if-eqz v2, :cond_b

    .line 389
    const/16 v2, 0xb

    iget-object v3, p0, Lpmx;->f:Ljava/lang/String;

    .line 390
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 392
    :cond_b
    iget-object v2, p0, Lpmx;->s:Ljava/lang/String;

    if-eqz v2, :cond_c

    .line 393
    const/16 v2, 0xc

    iget-object v3, p0, Lpmx;->s:Ljava/lang/String;

    .line 394
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 396
    :cond_c
    iget-object v2, p0, Lpmx;->t:Ljava/lang/Boolean;

    if-eqz v2, :cond_d

    .line 397
    const/16 v2, 0xd

    iget-object v3, p0, Lpmx;->t:Ljava/lang/Boolean;

    .line 398
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 400
    :cond_d
    iget-object v2, p0, Lpmx;->u:Lppf;

    if-eqz v2, :cond_e

    .line 401
    const/16 v2, 0xe

    iget-object v3, p0, Lpmx;->u:Lppf;

    .line 402
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 404
    :cond_e
    iget-object v2, p0, Lpmx;->v:Lpmy;

    if-eqz v2, :cond_f

    .line 405
    const/16 v2, 0xf

    iget-object v3, p0, Lpmx;->v:Lpmy;

    .line 406
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 408
    :cond_f
    iget-object v2, p0, Lpmx;->j:Ljava/lang/Boolean;

    if-eqz v2, :cond_10

    .line 409
    const/16 v2, 0x10

    iget-object v3, p0, Lpmx;->j:Ljava/lang/Boolean;

    .line 410
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 412
    :cond_10
    iget-object v2, p0, Lpmx;->x:[Lpps;

    if-eqz v2, :cond_12

    .line 413
    iget-object v3, p0, Lpmx;->x:[Lpps;

    array-length v4, v3

    move v2, v1

    :goto_3
    if-ge v2, v4, :cond_12

    aget-object v5, v3, v2

    .line 414
    if-eqz v5, :cond_11

    .line 415
    const/16 v6, 0x11

    .line 416
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 413
    :cond_11
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 420
    :cond_12
    iget-object v2, p0, Lpmx;->g:Lpxp;

    if-eqz v2, :cond_13

    .line 421
    const/16 v2, 0x12

    iget-object v3, p0, Lpmx;->g:Lpxp;

    .line 422
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 424
    :cond_13
    iget-object v2, p0, Lpmx;->z:Lprw;

    if-eqz v2, :cond_14

    .line 425
    const/16 v2, 0x13

    iget-object v3, p0, Lpmx;->z:Lprw;

    .line 426
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 428
    :cond_14
    iget-object v2, p0, Lpmx;->A:Lprv;

    if-eqz v2, :cond_15

    .line 429
    const/16 v2, 0x14

    iget-object v3, p0, Lpmx;->A:Lprv;

    .line 430
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 432
    :cond_15
    iget-object v2, p0, Lpmx;->n:Lpvy;

    if-eqz v2, :cond_16

    .line 433
    const/16 v2, 0x19

    iget-object v3, p0, Lpmx;->n:Lpvy;

    .line 434
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 436
    :cond_16
    iget v2, p0, Lpmx;->B:I

    const/high16 v3, -0x80000000

    if-eq v2, v3, :cond_17

    .line 437
    const/16 v2, 0x1a

    iget v3, p0, Lpmx;->B:I

    .line 438
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 440
    :cond_17
    iget-object v2, p0, Lpmx;->w:Lpna;

    if-eqz v2, :cond_18

    .line 441
    const/16 v2, 0x1b

    iget-object v3, p0, Lpmx;->w:Lpna;

    .line 442
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 444
    :cond_18
    iget-object v2, p0, Lpmx;->C:Ljava/lang/String;

    if-eqz v2, :cond_19

    .line 445
    const/16 v2, 0x1c

    iget-object v3, p0, Lpmx;->C:Ljava/lang/String;

    .line 446
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 448
    :cond_19
    iget-object v2, p0, Lpmx;->y:[Lppr;

    if-eqz v2, :cond_1b

    .line 449
    iget-object v2, p0, Lpmx;->y:[Lppr;

    array-length v3, v2

    :goto_4
    if-ge v1, v3, :cond_1b

    aget-object v4, v2, v1

    .line 450
    if-eqz v4, :cond_1a

    .line 451
    const/16 v5, 0x1d

    .line 452
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 449
    :cond_1a
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 456
    :cond_1b
    iget-object v1, p0, Lpmx;->D:Ljava/lang/String;

    if-eqz v1, :cond_1c

    .line 457
    const/16 v1, 0x1e

    iget-object v2, p0, Lpmx;->D:Ljava/lang/String;

    .line 458
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 460
    :cond_1c
    iget-object v1, p0, Lpmx;->E:Ljava/lang/String;

    if-eqz v1, :cond_1d

    .line 461
    const/16 v1, 0x1f

    iget-object v2, p0, Lpmx;->E:Ljava/lang/String;

    .line 462
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 464
    :cond_1d
    iget-object v1, p0, Lpmx;->h:Ljava/lang/String;

    if-eqz v1, :cond_1e

    .line 465
    const/16 v1, 0x20

    iget-object v2, p0, Lpmx;->h:Ljava/lang/String;

    .line 466
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 468
    :cond_1e
    iget-object v1, p0, Lpmx;->l:Ljava/lang/String;

    if-eqz v1, :cond_1f

    .line 469
    const/16 v1, 0x21

    iget-object v2, p0, Lpmx;->l:Ljava/lang/String;

    .line 470
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 472
    :cond_1f
    iget-object v1, p0, Lpmx;->F:Ljava/lang/String;

    if-eqz v1, :cond_20

    .line 473
    const/16 v1, 0x22

    iget-object v2, p0, Lpmx;->F:Ljava/lang/String;

    .line 474
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 476
    :cond_20
    iget-object v1, p0, Lpmx;->i:Ljava/lang/Boolean;

    if-eqz v1, :cond_21

    .line 477
    const/16 v1, 0x23

    iget-object v2, p0, Lpmx;->i:Ljava/lang/Boolean;

    .line 478
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 480
    :cond_21
    iget-object v1, p0, Lpmx;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 481
    iput v0, p0, Lpmx;->ai:I

    .line 482
    return v0

    :cond_22
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lpmx;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 490
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 491
    sparse-switch v0, :sswitch_data_0

    .line 495
    iget-object v2, p0, Lpmx;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 496
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lpmx;->ah:Ljava/util/List;

    .line 499
    :cond_1
    iget-object v2, p0, Lpmx;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 501
    :sswitch_0
    return-object p0

    .line 506
    :sswitch_1
    iget-object v0, p0, Lpmx;->b:Lpgx;

    if-nez v0, :cond_2

    .line 507
    new-instance v0, Lpgx;

    invoke-direct {v0}, Lpgx;-><init>()V

    iput-object v0, p0, Lpmx;->b:Lpgx;

    .line 509
    :cond_2
    iget-object v0, p0, Lpmx;->b:Lpgx;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 513
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lpmx;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 517
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpmx;->k:Ljava/lang/String;

    goto :goto_0

    .line 521
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpmx;->d:Ljava/lang/String;

    goto :goto_0

    .line 525
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpmx;->m:Ljava/lang/String;

    goto :goto_0

    .line 529
    :sswitch_6
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpmx;->e:Ljava/lang/String;

    goto :goto_0

    .line 533
    :sswitch_7
    iget-object v0, p0, Lpmx;->o:Lpmy;

    if-nez v0, :cond_3

    .line 534
    new-instance v0, Lpmy;

    invoke-direct {v0}, Lpmy;-><init>()V

    iput-object v0, p0, Lpmx;->o:Lpmy;

    .line 536
    :cond_3
    iget-object v0, p0, Lpmx;->o:Lpmy;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 540
    :sswitch_8
    iget-object v0, p0, Lpmx;->p:Lppf;

    if-nez v0, :cond_4

    .line 541
    new-instance v0, Lppf;

    invoke-direct {v0}, Lppf;-><init>()V

    iput-object v0, p0, Lpmx;->p:Lppf;

    .line 543
    :cond_4
    iget-object v0, p0, Lpmx;->p:Lppf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 547
    :sswitch_9
    const/16 v0, 0x4a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 548
    iget-object v0, p0, Lpmx;->q:[Ljava/lang/String;

    array-length v0, v0

    .line 549
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 550
    iget-object v3, p0, Lpmx;->q:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 551
    iput-object v2, p0, Lpmx;->q:[Ljava/lang/String;

    .line 552
    :goto_1
    iget-object v2, p0, Lpmx;->q:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 553
    iget-object v2, p0, Lpmx;->q:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 554
    invoke-virtual {p1}, Loxn;->a()I

    .line 552
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 557
    :cond_5
    iget-object v2, p0, Lpmx;->q:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    .line 561
    :sswitch_a
    const/16 v0, 0x52

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 562
    iget-object v0, p0, Lpmx;->r:[Ljava/lang/String;

    array-length v0, v0

    .line 563
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 564
    iget-object v3, p0, Lpmx;->r:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 565
    iput-object v2, p0, Lpmx;->r:[Ljava/lang/String;

    .line 566
    :goto_2
    iget-object v2, p0, Lpmx;->r:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 567
    iget-object v2, p0, Lpmx;->r:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 568
    invoke-virtual {p1}, Loxn;->a()I

    .line 566
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 571
    :cond_6
    iget-object v2, p0, Lpmx;->r:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    .line 575
    :sswitch_b
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpmx;->f:Ljava/lang/String;

    goto/16 :goto_0

    .line 579
    :sswitch_c
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpmx;->s:Ljava/lang/String;

    goto/16 :goto_0

    .line 583
    :sswitch_d
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lpmx;->t:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 587
    :sswitch_e
    iget-object v0, p0, Lpmx;->u:Lppf;

    if-nez v0, :cond_7

    .line 588
    new-instance v0, Lppf;

    invoke-direct {v0}, Lppf;-><init>()V

    iput-object v0, p0, Lpmx;->u:Lppf;

    .line 590
    :cond_7
    iget-object v0, p0, Lpmx;->u:Lppf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 594
    :sswitch_f
    iget-object v0, p0, Lpmx;->v:Lpmy;

    if-nez v0, :cond_8

    .line 595
    new-instance v0, Lpmy;

    invoke-direct {v0}, Lpmy;-><init>()V

    iput-object v0, p0, Lpmx;->v:Lpmy;

    .line 597
    :cond_8
    iget-object v0, p0, Lpmx;->v:Lpmy;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 601
    :sswitch_10
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lpmx;->j:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 605
    :sswitch_11
    const/16 v0, 0x8a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 606
    iget-object v0, p0, Lpmx;->x:[Lpps;

    if-nez v0, :cond_a

    move v0, v1

    .line 607
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lpps;

    .line 608
    iget-object v3, p0, Lpmx;->x:[Lpps;

    if-eqz v3, :cond_9

    .line 609
    iget-object v3, p0, Lpmx;->x:[Lpps;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 611
    :cond_9
    iput-object v2, p0, Lpmx;->x:[Lpps;

    .line 612
    :goto_4
    iget-object v2, p0, Lpmx;->x:[Lpps;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_b

    .line 613
    iget-object v2, p0, Lpmx;->x:[Lpps;

    new-instance v3, Lpps;

    invoke-direct {v3}, Lpps;-><init>()V

    aput-object v3, v2, v0

    .line 614
    iget-object v2, p0, Lpmx;->x:[Lpps;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 615
    invoke-virtual {p1}, Loxn;->a()I

    .line 612
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 606
    :cond_a
    iget-object v0, p0, Lpmx;->x:[Lpps;

    array-length v0, v0

    goto :goto_3

    .line 618
    :cond_b
    iget-object v2, p0, Lpmx;->x:[Lpps;

    new-instance v3, Lpps;

    invoke-direct {v3}, Lpps;-><init>()V

    aput-object v3, v2, v0

    .line 619
    iget-object v2, p0, Lpmx;->x:[Lpps;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 623
    :sswitch_12
    iget-object v0, p0, Lpmx;->g:Lpxp;

    if-nez v0, :cond_c

    .line 624
    new-instance v0, Lpxp;

    invoke-direct {v0}, Lpxp;-><init>()V

    iput-object v0, p0, Lpmx;->g:Lpxp;

    .line 626
    :cond_c
    iget-object v0, p0, Lpmx;->g:Lpxp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 630
    :sswitch_13
    iget-object v0, p0, Lpmx;->z:Lprw;

    if-nez v0, :cond_d

    .line 631
    new-instance v0, Lprw;

    invoke-direct {v0}, Lprw;-><init>()V

    iput-object v0, p0, Lpmx;->z:Lprw;

    .line 633
    :cond_d
    iget-object v0, p0, Lpmx;->z:Lprw;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 637
    :sswitch_14
    iget-object v0, p0, Lpmx;->A:Lprv;

    if-nez v0, :cond_e

    .line 638
    new-instance v0, Lprv;

    invoke-direct {v0}, Lprv;-><init>()V

    iput-object v0, p0, Lpmx;->A:Lprv;

    .line 640
    :cond_e
    iget-object v0, p0, Lpmx;->A:Lprv;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 644
    :sswitch_15
    iget-object v0, p0, Lpmx;->n:Lpvy;

    if-nez v0, :cond_f

    .line 645
    new-instance v0, Lpvy;

    invoke-direct {v0}, Lpvy;-><init>()V

    iput-object v0, p0, Lpmx;->n:Lpvy;

    .line 647
    :cond_f
    iget-object v0, p0, Lpmx;->n:Lpvy;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 651
    :sswitch_16
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 652
    if-eq v0, v4, :cond_10

    const/4 v2, 0x2

    if-eq v0, v2, :cond_10

    const/4 v2, 0x3

    if-ne v0, v2, :cond_11

    .line 655
    :cond_10
    iput v0, p0, Lpmx;->B:I

    goto/16 :goto_0

    .line 657
    :cond_11
    iput v4, p0, Lpmx;->B:I

    goto/16 :goto_0

    .line 662
    :sswitch_17
    iget-object v0, p0, Lpmx;->w:Lpna;

    if-nez v0, :cond_12

    .line 663
    new-instance v0, Lpna;

    invoke-direct {v0}, Lpna;-><init>()V

    iput-object v0, p0, Lpmx;->w:Lpna;

    .line 665
    :cond_12
    iget-object v0, p0, Lpmx;->w:Lpna;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 669
    :sswitch_18
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpmx;->C:Ljava/lang/String;

    goto/16 :goto_0

    .line 673
    :sswitch_19
    const/16 v0, 0xea

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 674
    iget-object v0, p0, Lpmx;->y:[Lppr;

    if-nez v0, :cond_14

    move v0, v1

    .line 675
    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lppr;

    .line 676
    iget-object v3, p0, Lpmx;->y:[Lppr;

    if-eqz v3, :cond_13

    .line 677
    iget-object v3, p0, Lpmx;->y:[Lppr;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 679
    :cond_13
    iput-object v2, p0, Lpmx;->y:[Lppr;

    .line 680
    :goto_6
    iget-object v2, p0, Lpmx;->y:[Lppr;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_15

    .line 681
    iget-object v2, p0, Lpmx;->y:[Lppr;

    new-instance v3, Lppr;

    invoke-direct {v3}, Lppr;-><init>()V

    aput-object v3, v2, v0

    .line 682
    iget-object v2, p0, Lpmx;->y:[Lppr;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 683
    invoke-virtual {p1}, Loxn;->a()I

    .line 680
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 674
    :cond_14
    iget-object v0, p0, Lpmx;->y:[Lppr;

    array-length v0, v0

    goto :goto_5

    .line 686
    :cond_15
    iget-object v2, p0, Lpmx;->y:[Lppr;

    new-instance v3, Lppr;

    invoke-direct {v3}, Lppr;-><init>()V

    aput-object v3, v2, v0

    .line 687
    iget-object v2, p0, Lpmx;->y:[Lppr;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 691
    :sswitch_1a
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpmx;->D:Ljava/lang/String;

    goto/16 :goto_0

    .line 695
    :sswitch_1b
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpmx;->E:Ljava/lang/String;

    goto/16 :goto_0

    .line 699
    :sswitch_1c
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpmx;->h:Ljava/lang/String;

    goto/16 :goto_0

    .line 703
    :sswitch_1d
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpmx;->l:Ljava/lang/String;

    goto/16 :goto_0

    .line 707
    :sswitch_1e
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpmx;->F:Ljava/lang/String;

    goto/16 :goto_0

    .line 711
    :sswitch_1f
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lpmx;->i:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 491
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x68 -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
        0x80 -> :sswitch_10
        0x8a -> :sswitch_11
        0x92 -> :sswitch_12
        0x9a -> :sswitch_13
        0xa2 -> :sswitch_14
        0xca -> :sswitch_15
        0xd0 -> :sswitch_16
        0xda -> :sswitch_17
        0xe2 -> :sswitch_18
        0xea -> :sswitch_19
        0xf2 -> :sswitch_1a
        0xfa -> :sswitch_1b
        0x102 -> :sswitch_1c
        0x10a -> :sswitch_1d
        0x112 -> :sswitch_1e
        0x118 -> :sswitch_1f
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 226
    iget-object v1, p0, Lpmx;->b:Lpgx;

    if-eqz v1, :cond_0

    .line 227
    const/4 v1, 0x1

    iget-object v2, p0, Lpmx;->b:Lpgx;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 229
    :cond_0
    iget-object v1, p0, Lpmx;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 230
    const/4 v1, 0x2

    iget-object v2, p0, Lpmx;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 232
    :cond_1
    iget-object v1, p0, Lpmx;->k:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 233
    const/4 v1, 0x3

    iget-object v2, p0, Lpmx;->k:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 235
    :cond_2
    iget-object v1, p0, Lpmx;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 236
    const/4 v1, 0x4

    iget-object v2, p0, Lpmx;->d:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 238
    :cond_3
    iget-object v1, p0, Lpmx;->m:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 239
    const/4 v1, 0x5

    iget-object v2, p0, Lpmx;->m:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 241
    :cond_4
    iget-object v1, p0, Lpmx;->e:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 242
    const/4 v1, 0x6

    iget-object v2, p0, Lpmx;->e:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 244
    :cond_5
    iget-object v1, p0, Lpmx;->o:Lpmy;

    if-eqz v1, :cond_6

    .line 245
    const/4 v1, 0x7

    iget-object v2, p0, Lpmx;->o:Lpmy;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 247
    :cond_6
    iget-object v1, p0, Lpmx;->p:Lppf;

    if-eqz v1, :cond_7

    .line 248
    const/16 v1, 0x8

    iget-object v2, p0, Lpmx;->p:Lppf;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 250
    :cond_7
    iget-object v1, p0, Lpmx;->q:[Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 251
    iget-object v2, p0, Lpmx;->q:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_8

    aget-object v4, v2, v1

    .line 252
    const/16 v5, 0x9

    invoke-virtual {p1, v5, v4}, Loxo;->a(ILjava/lang/String;)V

    .line 251
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 255
    :cond_8
    iget-object v1, p0, Lpmx;->r:[Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 256
    iget-object v2, p0, Lpmx;->r:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_9

    aget-object v4, v2, v1

    .line 257
    const/16 v5, 0xa

    invoke-virtual {p1, v5, v4}, Loxo;->a(ILjava/lang/String;)V

    .line 256
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 260
    :cond_9
    iget-object v1, p0, Lpmx;->f:Ljava/lang/String;

    if-eqz v1, :cond_a

    .line 261
    const/16 v1, 0xb

    iget-object v2, p0, Lpmx;->f:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 263
    :cond_a
    iget-object v1, p0, Lpmx;->s:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 264
    const/16 v1, 0xc

    iget-object v2, p0, Lpmx;->s:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 266
    :cond_b
    iget-object v1, p0, Lpmx;->t:Ljava/lang/Boolean;

    if-eqz v1, :cond_c

    .line 267
    const/16 v1, 0xd

    iget-object v2, p0, Lpmx;->t:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IZ)V

    .line 269
    :cond_c
    iget-object v1, p0, Lpmx;->u:Lppf;

    if-eqz v1, :cond_d

    .line 270
    const/16 v1, 0xe

    iget-object v2, p0, Lpmx;->u:Lppf;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 272
    :cond_d
    iget-object v1, p0, Lpmx;->v:Lpmy;

    if-eqz v1, :cond_e

    .line 273
    const/16 v1, 0xf

    iget-object v2, p0, Lpmx;->v:Lpmy;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 275
    :cond_e
    iget-object v1, p0, Lpmx;->j:Ljava/lang/Boolean;

    if-eqz v1, :cond_f

    .line 276
    const/16 v1, 0x10

    iget-object v2, p0, Lpmx;->j:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IZ)V

    .line 278
    :cond_f
    iget-object v1, p0, Lpmx;->x:[Lpps;

    if-eqz v1, :cond_11

    .line 279
    iget-object v2, p0, Lpmx;->x:[Lpps;

    array-length v3, v2

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_11

    aget-object v4, v2, v1

    .line 280
    if-eqz v4, :cond_10

    .line 281
    const/16 v5, 0x11

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 279
    :cond_10
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 285
    :cond_11
    iget-object v1, p0, Lpmx;->g:Lpxp;

    if-eqz v1, :cond_12

    .line 286
    const/16 v1, 0x12

    iget-object v2, p0, Lpmx;->g:Lpxp;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 288
    :cond_12
    iget-object v1, p0, Lpmx;->z:Lprw;

    if-eqz v1, :cond_13

    .line 289
    const/16 v1, 0x13

    iget-object v2, p0, Lpmx;->z:Lprw;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 291
    :cond_13
    iget-object v1, p0, Lpmx;->A:Lprv;

    if-eqz v1, :cond_14

    .line 292
    const/16 v1, 0x14

    iget-object v2, p0, Lpmx;->A:Lprv;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 294
    :cond_14
    iget-object v1, p0, Lpmx;->n:Lpvy;

    if-eqz v1, :cond_15

    .line 295
    const/16 v1, 0x19

    iget-object v2, p0, Lpmx;->n:Lpvy;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 297
    :cond_15
    iget v1, p0, Lpmx;->B:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_16

    .line 298
    const/16 v1, 0x1a

    iget v2, p0, Lpmx;->B:I

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 300
    :cond_16
    iget-object v1, p0, Lpmx;->w:Lpna;

    if-eqz v1, :cond_17

    .line 301
    const/16 v1, 0x1b

    iget-object v2, p0, Lpmx;->w:Lpna;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 303
    :cond_17
    iget-object v1, p0, Lpmx;->C:Ljava/lang/String;

    if-eqz v1, :cond_18

    .line 304
    const/16 v1, 0x1c

    iget-object v2, p0, Lpmx;->C:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 306
    :cond_18
    iget-object v1, p0, Lpmx;->y:[Lppr;

    if-eqz v1, :cond_1a

    .line 307
    iget-object v1, p0, Lpmx;->y:[Lppr;

    array-length v2, v1

    :goto_3
    if-ge v0, v2, :cond_1a

    aget-object v3, v1, v0

    .line 308
    if-eqz v3, :cond_19

    .line 309
    const/16 v4, 0x1d

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 307
    :cond_19
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 313
    :cond_1a
    iget-object v0, p0, Lpmx;->D:Ljava/lang/String;

    if-eqz v0, :cond_1b

    .line 314
    const/16 v0, 0x1e

    iget-object v1, p0, Lpmx;->D:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 316
    :cond_1b
    iget-object v0, p0, Lpmx;->E:Ljava/lang/String;

    if-eqz v0, :cond_1c

    .line 317
    const/16 v0, 0x1f

    iget-object v1, p0, Lpmx;->E:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 319
    :cond_1c
    iget-object v0, p0, Lpmx;->h:Ljava/lang/String;

    if-eqz v0, :cond_1d

    .line 320
    const/16 v0, 0x20

    iget-object v1, p0, Lpmx;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 322
    :cond_1d
    iget-object v0, p0, Lpmx;->l:Ljava/lang/String;

    if-eqz v0, :cond_1e

    .line 323
    const/16 v0, 0x21

    iget-object v1, p0, Lpmx;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 325
    :cond_1e
    iget-object v0, p0, Lpmx;->F:Ljava/lang/String;

    if-eqz v0, :cond_1f

    .line 326
    const/16 v0, 0x22

    iget-object v1, p0, Lpmx;->F:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 328
    :cond_1f
    iget-object v0, p0, Lpmx;->i:Ljava/lang/Boolean;

    if-eqz v0, :cond_20

    .line 329
    const/16 v0, 0x23

    iget-object v1, p0, Lpmx;->i:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 331
    :cond_20
    iget-object v0, p0, Lpmx;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 333
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0, p1}, Lpmx;->a(Loxn;)Lpmx;

    move-result-object v0

    return-object v0
.end method
