.class public final Ljaq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lizp;


# instance fields
.field private a:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    invoke-static {p1}, Lkib;->b(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Ljaq;->a:Landroid/content/SharedPreferences;

    .line 18
    return-void
.end method


# virtual methods
.method public a()Z
    .locals 3

    .prologue
    .line 22
    iget-object v0, p0, Ljaq;->a:Landroid/content/SharedPreferences;

    const-string v1, "overlay_key"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 61
    const-string v0, "W"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    const/4 v0, 0x1

    .line 65
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Z
    .locals 3

    .prologue
    .line 27
    iget-object v0, p0, Ljaq;->a:Landroid/content/SharedPreferences;

    const-string v1, "com.google.android.libraries.social.media.settings.MediaSettings.low_bandwidth_key"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public c()Z
    .locals 3

    .prologue
    .line 32
    iget-object v0, p0, Ljaq;->a:Landroid/content/SharedPreferences;

    const-string v1, "automatic_quality_key"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public d()Ljava/lang/String;
    .locals 3

    .prologue
    .line 38
    iget-object v0, p0, Ljaq;->a:Landroid/content/SharedPreferences;

    const-string v1, "standard_quality_key"

    const-string v2, "WL80"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 3

    .prologue
    .line 44
    iget-object v0, p0, Ljaq;->a:Landroid/content/SharedPreferences;

    const-string v1, "low_quality_key"

    const-string v2, "WL60"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public f()Ljava/lang/String;
    .locals 3

    .prologue
    .line 50
    iget-object v0, p0, Ljaq;->a:Landroid/content/SharedPreferences;

    const-string v1, "very_low_quality_key"

    const-string v2, "WL40"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public g()Z
    .locals 3

    .prologue
    .line 56
    iget-object v0, p0, Ljaq;->a:Landroid/content/SharedPreferences;

    const-string v1, "metered_connection_key"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public h()Z
    .locals 3

    .prologue
    .line 70
    iget-object v0, p0, Ljaq;->a:Landroid/content/SharedPreferences;

    const-string v1, "automatic_network_key"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public i()Ljava/lang/String;
    .locals 3

    .prologue
    .line 76
    iget-object v0, p0, Ljaq;->a:Landroid/content/SharedPreferences;

    const-string v1, "network_classification_key"

    const-string v2, "standard_speed"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
