.class public final Ldom;
.super Lhny;
.source "PG"


# instance fields
.field private final a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lizu;",
            ">;"
        }
    .end annotation
.end field

.field private final c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/List;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Lizu;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 38
    const-string v0, "CreateMediaBundleTask"

    invoke-direct {p0, p1, v0}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 39
    iput p2, p0, Ldom;->a:I

    .line 40
    iput-object p3, p0, Ldom;->b:Ljava/util/List;

    .line 41
    iput p4, p0, Ldom;->c:I

    .line 42
    return-void
.end method

.method private static a(Ljava/lang/String;J)Ljava/lang/String;
    .locals 5

    .prologue
    .line 115
    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x8

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "CKEY:p:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 10

    .prologue
    .line 47
    invoke-virtual {p0}, Ldom;->f()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Ldom;->a:I

    iget-object v2, p0, Ldom;->b:Ljava/util/List;

    invoke-static {v0, v1, v2}, Ljvj;->b(Landroid/content/Context;ILjava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 49
    new-instance v2, Lduj;

    invoke-direct {v2}, Lduj;-><init>()V

    .line 50
    iput-object v0, v2, Lduj;->a:Ljava/util/List;

    .line 51
    iget v1, p0, Ldom;->c:I

    iput v1, v2, Lduj;->b:I

    .line 54
    :try_start_0
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizu;

    invoke-virtual {v0}, Lizu;->j()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v0}, Lizu;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lizu;->c()J

    move-result-wide v6

    invoke-static {v5, v6, v7}, Ldom;->a(Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v0, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ldon; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 56
    :catch_0
    move-exception v0

    new-instance v0, Lhoz;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lhoz;-><init>(Z)V

    .line 57
    invoke-virtual {v0}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "hint_message"

    .line 58
    invoke-virtual {p0}, Ldom;->f()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0a0b3b

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 57
    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    :cond_0
    :goto_1
    return-object v0

    .line 54
    :cond_1
    :try_start_1
    invoke-virtual {v0}, Lizu;->i()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {p0}, Ldom;->f()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lhrx;->a(Landroid/content/Context;)Lhrx;

    move-result-object v5

    invoke-virtual {v0}, Lizu;->e()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lhrx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    new-instance v0, Ldon;

    invoke-direct {v0}, Ldon;-><init>()V

    throw v0

    :cond_3
    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_5

    invoke-virtual {p0}, Ldom;->f()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lhei;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iget v1, p0, Ldom;->a:I

    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p0}, Ldom;->f()Landroid/content/Context;

    move-result-object v0

    const-class v6, Lkfd;

    invoke-static {v0, v6}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkfd;

    new-instance v6, Ljvu;

    invoke-virtual {p0}, Ldom;->f()Landroid/content/Context;

    move-result-object v7

    iget v8, p0, Ldom;->a:I

    invoke-direct {v6, v7, v8, v5, v1}, Ljvu;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/util/List;)V

    invoke-interface {v0, v6}, Lkfd;->a(Lkff;)V

    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lizu;

    invoke-virtual {v6, v0}, Ljvu;->b(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_4

    new-instance v0, Ldon;

    invoke-direct {v0}, Ldon;-><init>()V

    throw v0

    :cond_4
    invoke-virtual {v6, v0}, Ljvu;->c(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v5, v8, v9}, Ldom;->a(Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ldon; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 61
    :cond_5
    new-instance v1, Ldit;

    .line 62
    invoke-virtual {p0}, Ldom;->f()Landroid/content/Context;

    move-result-object v0

    iget v4, p0, Ldom;->a:I

    invoke-direct {v1, v0, v4, v2, v3}, Ldit;-><init>(Landroid/content/Context;ILduj;Ljava/util/Map;)V

    .line 63
    invoke-virtual {v1}, Ldit;->l()V

    .line 64
    new-instance v0, Lhoz;

    iget v2, v1, Lkff;->i:I

    iget-object v3, v1, Lkff;->k:Ljava/lang/Exception;

    const/4 v4, 0x0

    invoke-direct {v0, v2, v3, v4}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    .line 65
    invoke-virtual {v1}, Ldit;->b()Lizu;

    move-result-object v2

    if-eqz v2, :cond_6

    .line 66
    invoke-virtual {v0}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "result_media"

    invoke-virtual {v1}, Ldit;->b()Lizu;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 68
    :cond_6
    invoke-virtual {v1}, Ldit;->c()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 69
    invoke-virtual {v0}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "hint_message"

    invoke-virtual {v1}, Ldit;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method
