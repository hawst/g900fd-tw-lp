.class public final Ldjh;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmac;",
        "Lmad;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final p:Ljava/lang/String;

.field private final q:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 38
    const-string v3, "editcomment"

    new-instance v4, Lmac;

    invoke-direct {v4}, Lmac;-><init>()V

    new-instance v5, Lmad;

    invoke-direct {v5}, Lmad;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 40
    iput-object p3, p0, Ldjh;->a:Ljava/lang/String;

    .line 41
    iput-object p4, p0, Ldjh;->b:Ljava/lang/String;

    .line 42
    iput-object p5, p0, Ldjh;->p:Ljava/lang/String;

    .line 43
    iput-object p6, p0, Ldjh;->q:Ljava/lang/String;

    .line 44
    return-void
.end method


# virtual methods
.method protected a(Lmac;)V
    .locals 2

    .prologue
    .line 48
    new-instance v0, Lnuz;

    invoke-direct {v0}, Lnuz;-><init>()V

    iput-object v0, p1, Lmac;->a:Lnuz;

    .line 49
    iget-object v0, p1, Lmac;->a:Lnuz;

    .line 50
    iget-object v1, p0, Ldjh;->a:Ljava/lang/String;

    iput-object v1, v0, Lnuz;->a:Ljava/lang/String;

    .line 51
    iget-object v1, p0, Ldjh;->b:Ljava/lang/String;

    iput-object v1, v0, Lnuz;->b:Ljava/lang/String;

    .line 52
    iget-object v1, p0, Ldjh;->p:Ljava/lang/String;

    iput-object v1, v0, Lnuz;->c:Ljava/lang/String;

    .line 53
    iget-object v1, p0, Ldjh;->q:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 55
    const/4 v1, 0x2

    iput v1, v0, Lnuz;->d:I

    .line 57
    :cond_0
    return-void
.end method

.method protected a(Lmad;)V
    .locals 4

    .prologue
    .line 61
    iget-object v0, p1, Lmad;->a:Lnvz;

    .line 63
    if-nez v0, :cond_1

    .line 90
    :cond_0
    :goto_0
    return-void

    .line 68
    :cond_1
    iget-object v0, v0, Lnvz;->a:Lofa;

    .line 69
    if-eqz v0, :cond_0

    .line 74
    iget-object v1, p0, Ldjh;->b:Ljava/lang/String;

    iget-object v2, v0, Lofa;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 79
    iget-object v1, p0, Ldjh;->q:Ljava/lang/String;

    if-nez v1, :cond_2

    .line 81
    const/4 v1, 0x0

    iput-object v1, v0, Lofa;->l:Loae;

    .line 82
    iget-object v1, p0, Ldjh;->f:Landroid/content/Context;

    iget v2, p0, Ldjh;->c:I

    iget-object v3, v0, Lofa;->h:Ljava/lang/String;

    invoke-static {v1, v2, v3, v0}, Llap;->b(Landroid/content/Context;ILjava/lang/String;Lofa;)V

    goto :goto_0

    .line 84
    :cond_2
    new-instance v1, Lnyf;

    invoke-direct {v1}, Lnyf;-><init>()V

    .line 85
    iget-object v2, p0, Ldjh;->b:Ljava/lang/String;

    iput-object v2, v1, Lnyf;->b:Ljava/lang/String;

    .line 86
    iget-object v0, v0, Lofa;->c:Ljava/lang/String;

    iput-object v0, v1, Lnyf;->c:Ljava/lang/String;

    .line 87
    iget-object v0, p0, Ldjh;->f:Landroid/content/Context;

    iget v2, p0, Ldjh;->c:I

    iget-object v3, p0, Ldjh;->q:Ljava/lang/String;

    invoke-static {v0, v2, v3, v1}, Ljvd;->a(Landroid/content/Context;ILjava/lang/String;Lnyf;)V

    goto :goto_0
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 21
    check-cast p1, Lmac;

    invoke-virtual {p0, p1}, Ldjh;->a(Lmac;)V

    return-void
.end method

.method protected synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 21
    check-cast p1, Lmad;

    invoke-virtual {p0, p1}, Ldjh;->a(Lmad;)V

    return-void
.end method
