.class public Lfjh;
.super Lkgt;
.source "PG"


# instance fields
.field private b:[Ljava/lang/CharSequence;

.field private c:[Ljava/lang/CharSequence;

.field private d:[Ljava/lang/CharSequence;

.field private e:Ljava/lang/String;

.field private f:I

.field private g:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lfjh;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 42
    invoke-direct {p0, p1, p2}, Lkgt;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 43
    sget-object v0, Ldht;->b:[I

    invoke-virtual {p1, p2, v0, v1, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 45
    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Lfjh;->b:[Ljava/lang/CharSequence;

    .line 46
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Lfjh;->c:[Ljava/lang/CharSequence;

    .line 47
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Lfjh;->d:[Ljava/lang/CharSequence;

    .line 48
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 50
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lfjh;->g:Landroid/view/LayoutInflater;

    .line 51
    return-void
.end method

.method static synthetic a(Lfjh;I)I
    .locals 0

    .prologue
    .line 32
    iput p1, p0, Lfjh;->f:I

    return p1
.end method

.method static synthetic a(Lfjh;)[Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lfjh;->b:[Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic b(Lfjh;)[Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lfjh;->c:[Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic c(Lfjh;)Landroid/view/LayoutInflater;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lfjh;->g:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method static synthetic d()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    return-object v0
.end method

.method static synthetic d(Lfjh;)[Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lfjh;->d:[Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic e(Lfjh;)I
    .locals 1

    .prologue
    .line 32
    iget v0, p0, Lfjh;->f:I

    return v0
.end method


# virtual methods
.method protected a(Landroid/content/res/TypedArray;I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 356
    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected a(Landroid/app/AlertDialog$Builder;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 222
    invoke-super {p0, p1}, Lkgt;->a(Landroid/app/AlertDialog$Builder;)V

    .line 224
    iget-object v0, p0, Lfjh;->b:[Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfjh;->c:[Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfjh;->d:[Ljava/lang/CharSequence;

    if-nez v0, :cond_1

    .line 225
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "ListPreference requires an entries array and an entryValues array."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 229
    :cond_1
    iget-object v0, p0, Lfjh;->e:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lfjh;->b(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lfjh;->f:I

    .line 230
    new-instance v0, Lfji;

    invoke-direct {v0, p0}, Lfji;-><init>(Lfjh;)V

    iget v1, p0, Lfjh;->f:I

    new-instance v2, Lfjj;

    invoke-direct {v2, p0}, Lfjj;-><init>(Lfjh;)V

    invoke-virtual {p1, v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 339
    invoke-virtual {p1, v3, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 340
    return-void
.end method

.method protected a(Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 379
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lfjk;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 381
    :cond_0
    invoke-super {p0, p1}, Lkgt;->a(Landroid/os/Parcelable;)V

    .line 388
    :goto_0
    return-void

    .line 385
    :cond_1
    check-cast p1, Lfjk;

    .line 386
    invoke-virtual {p1}, Lfjk;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Lkgt;->a(Landroid/os/Parcelable;)V

    .line 387
    iget-object v0, p1, Lfjk;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lfjh;->d_(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 213
    const v0, 0x1020010

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 214
    if-eqz v0, :cond_0

    .line 215
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAutoLinkMask(I)V

    .line 217
    :cond_0
    invoke-super {p0, p1}, Lkgt;->a(Landroid/view/View;)V

    .line 218
    return-void
.end method

.method protected a(Z)V
    .locals 2

    .prologue
    .line 344
    invoke-super {p0, p1}, Lkgt;->a(Z)V

    .line 346
    if-eqz p1, :cond_0

    iget v0, p0, Lfjh;->f:I

    if-ltz v0, :cond_0

    iget-object v0, p0, Lfjh;->c:[Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    .line 347
    iget-object v0, p0, Lfjh;->c:[Ljava/lang/CharSequence;

    iget v1, p0, Lfjh;->f:I

    aget-object v0, v0, v1

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 348
    invoke-virtual {p0, v0}, Lfjh;->b(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 349
    invoke-virtual {p0, v0}, Lfjh;->d_(Ljava/lang/String;)V

    .line 352
    :cond_0
    return-void
.end method

.method protected a(ZLjava/lang/Object;)V
    .locals 1

    .prologue
    .line 361
    if-eqz p1, :cond_0

    iget-object v0, p0, Lfjh;->e:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lfjh;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    :goto_0
    invoke-virtual {p0, p2}, Lfjh;->d_(Ljava/lang/String;)V

    .line 362
    return-void

    .line 361
    :cond_0
    check-cast p2, Ljava/lang/String;

    goto :goto_0
.end method

.method public a([Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 68
    iput-object p1, p0, Lfjh;->b:[Ljava/lang/CharSequence;

    .line 69
    return-void
.end method

.method public b(Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 197
    if-eqz p1, :cond_1

    iget-object v0, p0, Lfjh;->c:[Ljava/lang/CharSequence;

    if-eqz v0, :cond_1

    .line 198
    iget-object v0, p0, Lfjh;->c:[Ljava/lang/CharSequence;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    .line 199
    iget-object v1, p0, Lfjh;->c:[Ljava/lang/CharSequence;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 204
    :goto_1
    return v0

    .line 198
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 204
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lfjh;->e:Ljava/lang/String;

    return-object v0
.end method

.method public b([Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 100
    iput-object p1, p0, Lfjh;->d:[Ljava/lang/CharSequence;

    .line 101
    return-void
.end method

.method protected c()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 366
    invoke-super {p0}, Lkgt;->c()Landroid/os/Parcelable;

    move-result-object v0

    .line 367
    invoke-virtual {p0}, Lfjh;->x()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 374
    :goto_0
    return-object v0

    .line 372
    :cond_0
    new-instance v1, Lfjk;

    invoke-direct {v1, v0}, Lfjk;-><init>(Landroid/os/Parcelable;)V

    .line 373
    invoke-virtual {p0}, Lfjh;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lfjk;->a:Ljava/lang/String;

    move-object v0, v1

    .line 374
    goto :goto_0
.end method

.method public c([Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 127
    iput-object p1, p0, Lfjh;->c:[Ljava/lang/CharSequence;

    .line 128
    return-void
.end method

.method public d_(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 154
    iput-object p1, p0, Lfjh;->e:Ljava/lang/String;

    .line 156
    invoke-virtual {p0, p1}, Lfjh;->g(Ljava/lang/String;)Z

    .line 157
    return-void
.end method
