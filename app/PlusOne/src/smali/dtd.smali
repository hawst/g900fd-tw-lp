.class public final Ldtd;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:[Ljava/lang/String;

.field private static final c:[Ljava/lang/String;

.field private static final d:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 198
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "token"

    aput-object v1, v0, v3

    sput-object v0, Ldtd;->a:[Ljava/lang/String;

    .line 202
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "cluster_id"

    aput-object v1, v0, v4

    const-string v1, "parent_id"

    aput-object v1, v0, v5

    const/4 v1, 0x3

    const-string v2, "type"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "media_key"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "comment_count"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "plusone_count"

    aput-object v2, v0, v1

    sput-object v0, Ldtd;->b:[Ljava/lang/String;

    .line 212
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "cluster_id"

    aput-object v1, v0, v3

    const-string v1, "equivalence_token"

    aput-object v1, v0, v4

    sput-object v0, Ldtd;->c:[Ljava/lang/String;

    .line 221
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Ldtd;->d:Ljava/util/HashMap;

    return-void
.end method

.method private static a(Landroid/content/Context;ILdtm;I)Ldto;
    .locals 9

    .prologue
    .line 534
    const-string v0, "in_progress"

    const/4 v1, 0x2

    invoke-static {p0, p1, v0, v1}, Ldtd;->a(Landroid/content/Context;ILjava/lang/String;I)V

    .line 536
    const/4 v1, 0x0

    .line 537
    const/4 v0, 0x0

    .line 538
    :goto_0
    invoke-virtual {p2}, Ldtm;->c()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 539
    const-string v2, "EsTileSync"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 540
    const-string v2, "Syncing updated photos, pages read so far:"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 541
    invoke-virtual {p2}, Ldtm;->e()I

    move-result v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0xb

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 540
    :cond_0
    invoke-virtual {p2}, Ldtm;->b()Ldth;

    move-result-object v6

    .line 544
    invoke-static {v0}, Ldtd;->a(Ljava/lang/Thread;)V

    .line 545
    invoke-virtual {p2}, Ldtm;->a()Z

    move-result v3

    .line 546
    invoke-virtual {p2}, Ldtm;->e()I

    move-result v4

    .line 549
    iget-object v0, v6, Ldth;->a:Lnyz;

    if-eqz v0, :cond_6

    iget-object v0, v6, Ldth;->a:Lnyz;

    iget-object v0, v0, Lnyz;->f:Lnza;

    if-eqz v0, :cond_6

    iget-object v0, v6, Ldth;->a:Lnyz;

    iget-object v0, v0, Lnyz;->f:Lnza;

    iget-object v0, v0, Lnza;->a:Ljava/lang/Long;

    if-eqz v0, :cond_6

    .line 551
    iget-object v0, v6, Ldth;->a:Lnyz;

    iget-object v0, v0, Lnyz;->f:Lnza;

    iget-object v1, v0, Lnza;->a:Ljava/lang/Long;

    move-object v8, v1

    .line 554
    :goto_1
    new-instance v7, Ljava/lang/Thread;

    new-instance v0, Ldte;

    move-object v1, p0

    move v2, p1

    move v5, p3

    invoke-direct/range {v0 .. v6}, Ldte;-><init>(Landroid/content/Context;IZIILdth;)V

    invoke-direct {v7, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 563
    invoke-virtual {v7}, Ljava/lang/Thread;->start()V

    move-object v0, v7

    move-object v1, v8

    .line 564
    goto :goto_0

    .line 566
    :cond_1
    invoke-static {v0}, Ldtd;->a(Ljava/lang/Thread;)V

    .line 572
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Ldtd;->a(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v0

    .line 573
    const-string v2, "EsTileSync"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 574
    const-string v2, "Got next sync token from db="

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 577
    :cond_2
    :goto_2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 578
    const-class v0, Lfew;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfew;

    .line 579
    invoke-virtual {v0, p1}, Lfew;->d(I)V

    .line 580
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "We\'ve completed a sync without getting a non null sync token, pages read: "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 582
    invoke-virtual {p2}, Ldtm;->e()I

    move-result v2

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x39

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", photoPager: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", resume token type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 574
    :cond_3
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 589
    :cond_4
    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-static {p0, p1, v2, v3}, Ldtd;->a(Landroid/content/Context;ILjava/lang/String;I)V

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {p0, p1, v2, v3}, Ldtd;->a(Landroid/content/Context;ILjava/lang/String;I)V

    const/4 v2, 0x0

    invoke-static {p0, p1, v0, v2}, Ldtd;->a(Landroid/content/Context;ILjava/lang/String;I)V

    .line 591
    const-string v2, "EsTileSync"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 592
    const-string v2, "Finished all photos update, stop reason: "

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 593
    invoke-virtual {p2}, Ldtm;->d()Ldtn;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 594
    invoke-virtual {p2}, Ldtm;->e()I

    move-result v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x41

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", pages retrieved: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", syncToken: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", account: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 592
    :cond_5
    new-instance v0, Ldto;

    invoke-virtual {p2}, Ldtm;->d()Ldtn;

    move-result-object v2

    invoke-direct {v0, v2, v1}, Ldto;-><init>(Ldtn;Ljava/lang/Long;)V

    return-object v0

    :cond_6
    move-object v8, v1

    goto/16 :goto_1
.end method

.method private static a(Landroid/content/Context;ILdvz;ILdtg;ZI)Ldto;
    .locals 8

    .prologue
    .line 493
    const/4 v3, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move v4, p3

    move-object v5, p4

    move v6, p5

    move v7, p6

    invoke-static/range {v0 .. v7}, Ldtd;->a(Landroid/content/Context;ILdvz;Ljava/lang/String;ILdtg;ZI)Ldto;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/Context;ILdvz;Ljava/lang/String;ILdtg;ZI)Ldto;
    .locals 8

    .prologue
    .line 514
    const-string v0, "EsTileSync"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 515
    const-string v0, "updateRemotePhotos starting, fetcher: ("

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 519
    invoke-static {p0, p1}, Ljvd;->b(Landroid/content/Context;I)J

    move-result-wide v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit16 v5, v5, 0x9e

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "), resumeToken: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", resumeTokenType: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", metadata so far: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", max metadata: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", is initial sync: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", account: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 515
    :cond_0
    new-instance v0, Ldtm;

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p7

    move-object v6, p5

    move v7, p6

    invoke-direct/range {v0 .. v7}, Ldtm;-><init>(Landroid/content/Context;ILdvz;Ljava/lang/String;ILdtg;Z)V

    invoke-static {p0, p1, v0, p4}, Ldtd;->a(Landroid/content/Context;ILdtm;I)Ldto;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/Context;ILjava/lang/String;ILdtg;ZI)Ldto;
    .locals 8

    .prologue
    .line 505
    const/4 v2, 0x0

    move-object v0, p0

    move v1, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    move v6, p5

    move v7, p6

    invoke-static/range {v0 .. v7}, Ldtd;->a(Landroid/content/Context;ILdvz;Ljava/lang/String;ILdtg;ZI)Ldto;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;ILdvz;Ldts;I)Ldtp;
    .locals 8

    .prologue
    .line 330
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Ldtd;->a(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v1

    .line 332
    const/4 v0, 0x0

    .line 333
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Ldts;->c:Ldts;

    if-eq p3, v2, :cond_0

    const/4 v2, 0x3

    invoke-static {p0, p1, v2}, Ldtd;->a(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-static {p0, p1}, Ljvd;->b(Landroid/content/Context;I)J

    move-result-wide v2

    int-to-long v4, p4

    cmp-long v2, v2, v4

    if-gez v2, :cond_3

    const/4 v2, 0x1

    :goto_0
    if-eqz v2, :cond_4

    :cond_0
    const/4 v2, 0x1

    :goto_1
    if-eqz v2, :cond_e

    .line 335
    const/4 v0, 0x3

    invoke-static {p0, p1, v0}, Ldtd;->a(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ldtl;

    invoke-direct {v4, p0, p1, p3}, Ldtl;-><init>(Landroid/content/Context;ILdts;)V

    sget-object v0, Ldts;->c:Ldts;

    if-eq p3, v0, :cond_1

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_1
    sget-object v0, Ldts;->c:Ldts;

    if-ne p3, v0, :cond_2

    invoke-static {p0, p1}, Ldtd;->a(Landroid/content/Context;I)V

    :cond_2
    const/4 v3, 0x3

    const/4 v5, 0x1

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move v6, p4

    invoke-static/range {v0 .. v6}, Ldtd;->a(Landroid/content/Context;ILdvz;ILdtg;ZI)Ldto;

    move-result-object v0

    :goto_2
    iget-object v1, v0, Ldto;->a:Ldtn;

    sget-object v2, Ldtn;->a:Ldtn;

    if-ne v1, v2, :cond_8

    new-instance v1, Ldtp;

    sget-object v2, Ldtr;->b:Ldtr;

    iget-object v0, v0, Ldto;->b:Ljava/lang/Long;

    invoke-direct {v1, v2, v0}, Ldtp;-><init>(Ldtr;Ljava/lang/Long;)V

    move-object v0, v1

    .line 339
    :goto_3
    const/4 v1, 0x0

    invoke-static {p0, p1, v1}, Ldtd;->a(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v1

    move-object v7, v0

    move-object v0, v1

    .line 344
    :goto_4
    const/4 v1, 0x4

    invoke-static {p0, p1, v1}, Ldtd;->a(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ldtj;

    invoke-direct {v4, p0, p1, p3, v0}, Ldtj;-><init>(Landroid/content/Context;ILdts;Ljava/lang/String;)V

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_b

    const/4 v3, 0x4

    const/4 v5, 0x0

    const v6, 0x7fffffff

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    invoke-static/range {v0 .. v6}, Ldtd;->a(Landroid/content/Context;ILdvz;ILdtg;ZI)Ldto;

    move-result-object v0

    :goto_5
    iget-object v1, v0, Ldto;->a:Ldtn;

    sget-object v2, Ldtn;->a:Ldtn;

    if-ne v1, v2, :cond_c

    new-instance v1, Ldtp;

    sget-object v2, Ldtr;->c:Ldtr;

    iget-object v0, v0, Ldto;->b:Ljava/lang/Long;

    invoke-direct {v1, v2, v0}, Ldtp;-><init>(Ldtr;Ljava/lang/Long;)V

    .line 348
    if-eqz v7, :cond_d

    .line 351
    :goto_6
    return-object v7

    .line 333
    :cond_3
    const/4 v2, 0x0

    goto :goto_0

    :cond_4
    const/4 v2, 0x0

    goto :goto_1

    .line 335
    :cond_5
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Ldtd;->a(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    const/4 v1, 0x1

    invoke-static {p0, p1, v0, v1}, Ldtd;->a(Landroid/content/Context;ILjava/lang/String;I)V

    :cond_6
    const/4 v3, 0x3

    const/4 v5, 0x1

    move-object v0, p0

    move v1, p1

    move v6, p4

    invoke-static/range {v0 .. v6}, Ldtd;->a(Landroid/content/Context;ILjava/lang/String;ILdtg;ZI)Ldto;

    move-result-object v0

    goto :goto_2

    :cond_7
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Ldtd;->a(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-class v0, Lfew;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfew;

    invoke-virtual {v0, p1}, Lfew;->d(I)V

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Trying to resume initial sync with empty current and next sync tokens, syncType: "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x0

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    iget-object v1, v0, Ldto;->a:Ldtn;

    sget-object v2, Ldtn;->b:Ldtn;

    if-ne v1, v2, :cond_9

    new-instance v1, Ldtp;

    sget-object v2, Ldtr;->a:Ldtr;

    iget-object v0, v0, Ldto;->b:Ljava/lang/Long;

    invoke-direct {v1, v2, v0}, Ldtp;-><init>(Ldtr;Ljava/lang/Long;)V

    move-object v0, v1

    goto/16 :goto_3

    :cond_9
    iget-object v1, v0, Ldto;->a:Ldtn;

    sget-object v2, Ldtn;->c:Ldtn;

    if-ne v1, v2, :cond_a

    new-instance v1, Ldtp;

    sget-object v2, Ldtr;->d:Ldtr;

    iget-object v0, v0, Ldto;->b:Ljava/lang/Long;

    invoke-direct {v1, v2, v0}, Ldtp;-><init>(Ldtr;Ljava/lang/Long;)V

    move-object v0, v1

    goto/16 :goto_3

    :cond_a
    new-instance v1, Ljava/lang/IllegalStateException;

    iget-object v0, v0, Ldto;->a:Ldtn;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x27

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Initial sync ended for unknown reason: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 344
    :cond_b
    const/4 v3, 0x4

    const/4 v5, 0x0

    const v6, 0x7fffffff

    move-object v0, p0

    move v1, p1

    invoke-static/range {v0 .. v6}, Ldtd;->a(Landroid/content/Context;ILjava/lang/String;ILdtg;ZI)Ldto;

    move-result-object v0

    goto/16 :goto_5

    :cond_c
    new-instance v1, Ljava/lang/IllegalArgumentException;

    iget-object v0, v0, Ldto;->a:Ldtn;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x25

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Delta sync ended for unknown reason: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_d
    move-object v7, v1

    .line 351
    goto/16 :goto_6

    :cond_e
    move-object v7, v0

    move-object v0, v1

    goto/16 :goto_4
.end method

.method private static a(Landroid/content/Context;II)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 228
    .line 229
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 231
    const-string v1, "photo_requests"

    sget-object v2, Ldtd;->a:[Ljava/lang/String;

    const-string v3, "token_type = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    .line 233
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v7

    move-object v6, v5

    move-object v7, v5

    .line 231
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 237
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 238
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    .line 241
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 244
    return-object v5

    .line 241
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static a(Landroid/content/Context;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 248
    .line 249
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 251
    const-string v1, "photo_requests"

    invoke-virtual {v0, v1, v2, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 252
    return-void
.end method

.method private static a(Landroid/content/Context;ILdtt;Ldti;Ldtq;Ljava/util/ArrayList;Ldts;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ldtt;",
            "Ldti;",
            "Ldtq;",
            "Ljava/util/ArrayList",
            "<",
            "Ljvk;",
            ">;",
            "Ldts;",
            "Landroid/database/sqlite/SQLiteDatabase;",
            ")V"
        }
    .end annotation

    .prologue
    .line 841
    invoke-static/range {p0 .. p1}, Ljvj;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v8

    .line 842
    const/4 v4, 0x1

    new-array v5, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v8, v5, v4

    .line 846
    new-instance v18, Ldtk;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    move/from16 v2, p1

    invoke-direct {v0, v1, v2}, Ldtk;-><init>(Landroid/content/Context;I)V

    .line 849
    const/4 v4, 0x0

    .line 850
    sget-object v6, Ldts;->c:Ldts;

    move-object/from16 v0, p6

    if-ne v0, v6, :cond_1

    .line 852
    const-string v6, "all_tiles"

    const-string v7, "view_id = ? AND media_attr & 512 == 0"

    move-object/from16 v0, p7

    invoke-virtual {v0, v6, v7, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 853
    move-object/from16 v0, p4

    iget-wide v6, v0, Ldtq;->a:J

    invoke-virtual/range {v18 .. v18}, Ldtk;->b()I

    move-result v5

    int-to-long v10, v5

    add-long/2addr v6, v10

    move-object/from16 v0, p4

    iput-wide v6, v0, Ldtq;->a:J

    move-object/from16 v17, v4

    .line 870
    :goto_0
    const-wide/32 v10, 0xf4240

    .line 872
    invoke-virtual/range {p2 .. p2}, Ldtt;->a()Ljava/lang/Iterable;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :cond_0
    :goto_1
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_a

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 873
    if-eqz v17, :cond_2

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 875
    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ldtk;->a(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 876
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 877
    const-string v7, "view_order"

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v6, v7, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 878
    const-string v7, "all_tiles"

    const-string v9, "_id = ?"

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/String;

    const/4 v13, 0x0

    aput-object v4, v12, v13

    move-object/from16 v0, p7

    invoke-virtual {v0, v7, v6, v9, v12}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 880
    move-object/from16 v0, p4

    iget-wide v6, v0, Ldtq;->b:J

    const-wide/16 v12, 0x1

    add-long/2addr v6, v12

    move-object/from16 v0, p4

    iput-wide v6, v0, Ldtq;->b:J

    .line 881
    const-wide/16 v6, 0x1

    add-long/2addr v10, v6

    .line 882
    goto :goto_2

    .line 858
    :cond_1
    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ldtt;->c(Ldtk;)Ljava/util/HashSet;

    move-result-object v5

    .line 860
    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ldtk;->a(Ljava/util/HashSet;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_b

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 861
    const-string v7, "all_tiles"

    const-string v9, "_id = ?"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    aput-object v4, v10, v11

    move-object/from16 v0, p7

    invoke-virtual {v0, v7, v9, v10}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 863
    move-object/from16 v0, p4

    iget-wide v10, v0, Ldtq;->a:J

    const-wide/16 v12, 0x1

    add-long/2addr v10, v12

    move-object/from16 v0, p4

    iput-wide v10, v0, Ldtq;->a:J

    goto :goto_3

    .line 884
    :cond_2
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Ldti;->a(Ljava/lang/String;)Lnzx;

    move-result-object v20

    .line 885
    if-eqz v20, :cond_9

    .line 890
    move-object/from16 v0, v20

    iget-object v0, v0, Lnzx;->j:[Lnzx;

    move-object/from16 v21, v0

    .line 891
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 892
    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Ldtt;->a(Ljava/lang/String;)Ljava/util/LinkedHashSet;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_3
    :goto_4
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 894
    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v5}, Ldti;->a(Ljava/lang/String;Ljava/lang/String;)Lnzx;

    move-result-object v5

    .line 895
    if-eqz v5, :cond_3

    .line 896
    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 899
    :cond_4
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v4, v4, [Lnzx;

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Lnzx;

    move-object/from16 v0, v20

    iput-object v4, v0, Lnzx;->j:[Lnzx;

    .line 902
    move-object/from16 v0, v20

    iget-object v4, v0, Lnzx;->j:[Lnzx;

    array-length v4, v4

    if-lez v4, :cond_6

    .line 903
    sget-object v4, Lnzr;->a:Loxr;

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_5

    move-object/from16 v0, v20

    iget v4, v0, Lnzx;->k:I

    const/4 v5, 0x2

    if-eq v4, v5, :cond_7

    .line 905
    :cond_5
    :goto_5
    const/4 v4, 0x1

    new-array v9, v4, [Lnzx;

    const/4 v4, 0x0

    aput-object v20, v9, v4

    .line 906
    const/4 v12, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    new-instance v16, Ljava/util/HashSet;

    invoke-direct/range {v16 .. v16}, Ljava/util/HashSet;-><init>()V

    move-object/from16 v5, p0

    move/from16 v6, p1

    move-object/from16 v7, p7

    move-object/from16 v13, p5

    invoke-static/range {v5 .. v16}, Ljvj;->a(Landroid/content/Context;ILandroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Lnzx;JLjava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)J

    move-result-wide v4

    .line 911
    move-object/from16 v0, p4

    iget-wide v6, v0, Ldtq;->c:J

    add-long/2addr v6, v4

    move-object/from16 v0, p4

    iput-wide v6, v0, Ldtq;->c:J

    .line 912
    add-long/2addr v10, v4

    .line 916
    :cond_6
    move-object/from16 v0, v21

    move-object/from16 v1, v20

    iput-object v0, v1, Lnzx;->j:[Lnzx;

    goto/16 :goto_1

    .line 903
    :cond_7
    sget-object v4, Lnzr;->a:Loxr;

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lnzr;

    iget-object v6, v4, Lnzr;->b:Lnxr;

    iget-object v4, v6, Lnxr;->i:Ljava/lang/Long;

    invoke-static {v4}, Llsl;->a(Ljava/lang/Long;)J

    move-result-wide v12

    iget-object v4, v6, Lnxr;->j:Ljava/lang/Long;

    invoke-static {v4}, Llsl;->a(Ljava/lang/Long;)J

    move-result-wide v4

    const-wide/16 v14, 0x0

    cmp-long v7, v4, v14

    if-nez v7, :cond_8

    move-object/from16 v0, v20

    iget-object v7, v0, Lnzx;->j:[Lnzx;

    if-eqz v7, :cond_8

    move-object/from16 v0, v20

    iget-object v4, v0, Lnzx;->j:[Lnzx;

    array-length v4, v4

    int-to-long v4, v4

    :cond_8
    const-wide/16 v14, 0x0

    cmp-long v7, v12, v14

    if-eqz v7, :cond_5

    const-wide/16 v14, 0x0

    cmp-long v7, v4, v14

    if-eqz v7, :cond_5

    cmp-long v7, v12, v4

    if-eqz v7, :cond_5

    move-object/from16 v0, v20

    iget-object v7, v0, Lnzx;->j:[Lnzx;

    if-eqz v7, :cond_5

    iget-object v7, v6, Lnxr;->b:Ljava/lang/String;

    iget-object v9, v6, Lnxr;->f:Ljava/lang/String;

    iget-object v14, v6, Lnxr;->c:Ljava/lang/String;

    iget v6, v6, Lnxr;->d:I

    invoke-static {v7, v9, v14, v6}, Ljvj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v6

    sub-long v4, v12, v4

    invoke-static {v6, v4, v5}, Ljvb;->a(Ljava/lang/String;J)Lnzx;

    move-result-object v4

    move-object/from16 v0, v20

    iget-object v5, v0, Lnzx;->j:[Lnzx;

    array-length v5, v5

    add-int/lit8 v5, v5, 0x1

    new-array v5, v5, [Lnzx;

    move-object/from16 v0, v20

    iget-object v6, v0, Lnzx;->j:[Lnzx;

    const/4 v7, 0x0

    const/4 v9, 0x0

    move-object/from16 v0, v20

    iget-object v12, v0, Lnzx;->j:[Lnzx;

    array-length v12, v12

    invoke-static {v6, v7, v5, v9, v12}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object/from16 v0, v20

    iget-object v6, v0, Lnzx;->j:[Lnzx;

    array-length v6, v6

    aput-object v4, v5, v6

    move-object/from16 v0, v20

    iput-object v5, v0, Lnzx;->j:[Lnzx;

    goto/16 :goto_5

    .line 918
    :cond_9
    const-string v5, "EsTileSync"

    const/4 v6, 0x5

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 919
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x29

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Could not find collection ID "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " during sync"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 925
    :cond_a
    return-void

    :cond_b
    move-object/from16 v17, v5

    goto/16 :goto_0
.end method

.method public static a(Landroid/content/Context;ILdvz;Ldts;)V
    .locals 20

    .prologue
    .line 680
    sget-object v6, Ldtd;->d:Ljava/util/HashMap;

    monitor-enter v6

    .line 681
    :try_start_0
    sget-object v4, Ldtd;->d:Ljava/util/HashMap;

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .line 683
    if-eqz v5, :cond_5

    .line 684
    sget-object v4, Ldts;->f:Ldts;

    move-object/from16 v0, p3

    if-eq v0, v4, :cond_0

    sget-object v4, Ldts;->e:Ldts;

    move-object/from16 v0, p3

    if-ne v0, v4, :cond_3

    .line 685
    :cond_0
    const-string v4, "EsTileSync"

    const/4 v7, 0x4

    invoke-static {v4, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_13

    .line 686
    const-string v4, "syncHighlightsPhotos: Account already being synced. Since type="

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static/range {p3 .. p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, 0x32

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v9, v10

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, ", we will wait and then sync again. id="

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-object v13, v5

    .line 706
    :goto_0
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 709
    :try_start_1
    monitor-enter v13
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    .line 710
    :try_start_2
    const-string v4, "EsTileSync"

    const/4 v5, 0x4

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-static/range {p3 .. p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x25

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "syncHighlightsPhotos: Starting. type="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    if-eqz p2, :cond_6

    const-class v4, Ljgn;

    move-object/from16 v0, p0

    invoke-static {v0, v4}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljgn;

    invoke-virtual/range {p2 .. p2}, Lkfp;->c()Z

    move-result v5

    if-nez v5, :cond_2

    invoke-interface {v4}, Ljgn;->j()Z

    move-result v4

    if-nez v4, :cond_6

    .line 711
    :cond_2
    :goto_1
    monitor-exit v13
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 713
    sget-object v5, Ldtd;->d:Ljava/util/HashMap;

    monitor-enter v5

    .line 714
    :try_start_3
    sget-object v4, Ldtd;->d:Ljava/util/HashMap;

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 715
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_5

    :goto_2
    return-void

    .line 691
    :cond_3
    :try_start_4
    const-string v4, "EsTileSync"

    const/4 v7, 0x4

    invoke-static {v4, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 692
    const-string v4, "syncHighlightsPhotos: Account already being synced. Since type="

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static/range {p3 .. p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, 0x24

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v9, v10

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, ", we will do nothing. id="

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 698
    :cond_4
    monitor-enter v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 699
    :try_start_5
    monitor-exit v5
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    monitor-exit v6

    goto :goto_2

    .line 706
    :catchall_0
    move-exception v4

    monitor-exit v6
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    throw v4

    .line 700
    :catchall_1
    move-exception v4

    :try_start_7
    monitor-exit v5
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    throw v4

    .line 703
    :cond_5
    new-instance v4, Ljava/lang/Object;

    invoke-direct {v4}, Ljava/lang/Object;-><init>()V

    .line 704
    sget-object v5, Ldtd;->d:Ljava/util/HashMap;

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v7, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    move-object v13, v4

    goto/16 :goto_0

    .line 710
    :cond_6
    :try_start_9
    invoke-static/range {p0 .. p1}, Lcuj;->b(Landroid/content/Context;I)Lnyj;

    move-result-object v4

    if-eqz v4, :cond_2

    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, p3

    invoke-static {v0, v1, v2}, Ldtu;->a(Landroid/content/Context;ILdts;)I

    move-result v4

    if-lez v4, :cond_2

    new-instance v14, Ldti;

    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v14, v0, v1, v2}, Ldti;-><init>(Landroid/content/Context;ILdvz;)V

    invoke-static/range {p0 .. p1}, Ljvj;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v15

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    new-instance v18, Ldtq;

    invoke-direct/range {v18 .. v18}, Ldtq;-><init>()V

    new-instance v19, Ldtk;

    invoke-direct/range {v19 .. v21}, Ldtk;-><init>(Landroid/content/Context;I)V

    new-instance v4, Ldtt;

    invoke-virtual/range {v19 .. v19}, Ldtk;->a()Z

    move-result v5

    if-eqz v5, :cond_c

    const/4 v9, 0x1

    :goto_3
    move-object/from16 v5, p0

    move/from16 v6, p1

    move-object/from16 v7, p2

    move-object/from16 v8, p3

    invoke-direct/range {v4 .. v9}, Ldtt;-><init>(Landroid/content/Context;ILdvz;Ldts;I)V

    invoke-static/range {p0 .. p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v5

    invoke-virtual {v5}, Liab;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v12

    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    sget-object v5, Ldts;->c:Ldts;

    move-object/from16 v0, p3

    if-eq v0, v5, :cond_7

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ldtt;->a(Ldtk;)Z

    move-result v5

    if-nez v5, :cond_11

    :cond_7
    const/4 v5, 0x1

    :goto_4
    if-eqz v5, :cond_8

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ldtt;->b(Ldtk;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v14, v6}, Ldti;->a(Ljava/util/ArrayList;)V

    :cond_8
    invoke-virtual {v12}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    if-eqz v5, :cond_9

    move-object/from16 v5, p0

    move/from16 v6, p1

    move-object v7, v4

    move-object v8, v14

    move-object/from16 v9, v18

    move-object/from16 v11, p3

    :try_start_a
    invoke-static/range {v5 .. v12}, Ldtd;->a(Landroid/content/Context;ILdtt;Ldti;Ldtq;Ljava/util/ArrayList;Ldts;Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_9
    invoke-virtual {v4}, Ldtt;->b()Z

    move-result v5

    if-eqz v5, :cond_a

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-static {v4, v0, v1, v12}, Ldtd;->a(Ldtt;Ldtk;Ldtq;Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_a
    iget-object v4, v4, Ldtt;->a:Ljava/lang/String;

    const/4 v5, 0x1

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-static {v0, v1, v15, v4, v5}, Ljvj;->b(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v12}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    :try_start_b
    invoke-virtual {v12}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    const-string v4, "EsTileSync"

    const/4 v5, 0x4

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_b

    const-string v4, "[UPDATE_BEST_PHOTOS], rows deleted = "

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v18

    iget-wide v6, v0, Ldtq;->a:J

    move-object/from16 v0, v18

    iget-wide v8, v0, Ldtq;->b:J

    move-object/from16 v0, v18

    iget-wide v10, v0, Ldtq;->c:J

    move-object/from16 v0, v18

    iget-wide v0, v0, Ldtq;->d:J

    move-wide/from16 v18, v0

    invoke-static/range {v16 .. v17}, Llse;->a(J)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v16

    move/from16 v0, v16

    add-int/lit16 v0, v0, 0x9f

    move/from16 v16, v0

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v17

    add-int v16, v16, v17

    move/from16 v0, v16

    invoke-direct {v12, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ", rows reordered = "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ", rows inserted = "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ", rows with new social data = "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v18

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ", duration: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_b
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {v15}, Ljvj;->q(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    invoke-virtual {v14}, Ldti;->a()V

    const-class v4, Lhei;

    move-object/from16 v0, p0

    invoke-static {v0, v4}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lhei;

    move/from16 v0, p1

    invoke-interface {v4, v0}, Lhei;->a(I)Lhej;

    move-result-object v4

    const-string v5, "gaia_id"

    invoke-interface {v4, v5}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static/range {p0 .. p0}, Ljem;->a(Landroid/content/Context;)Ljem;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljem;->a(Ljava/lang/String;)V

    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->d(Landroid/content/Context;I)V

    goto/16 :goto_1

    .line 711
    :catchall_2
    move-exception v4

    monitor-exit v13
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    :try_start_c
    throw v4
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    .line 716
    :catchall_3
    move-exception v4

    sget-object v5, Ldtd;->d:Ljava/util/HashMap;

    monitor-enter v5

    .line 714
    :try_start_d
    sget-object v6, Ldtd;->d:Ljava/util/HashMap;

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 715
    monitor-exit v5
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_6

    throw v4

    .line 710
    :cond_c
    :try_start_e
    sget-object v5, Ldts;->e:Ldts;

    move-object/from16 v0, p3

    if-ne v0, v5, :cond_d

    const/4 v9, 0x2

    goto/16 :goto_3

    :cond_d
    sget-object v5, Ldts;->f:Ldts;

    move-object/from16 v0, p3

    if-ne v0, v5, :cond_e

    const/4 v9, 0x3

    goto/16 :goto_3

    :cond_e
    sget-object v5, Ldts;->c:Ldts;

    move-object/from16 v0, p3

    if-ne v0, v5, :cond_f

    const/4 v9, 0x5

    goto/16 :goto_3

    :cond_f
    sget-object v5, Ldts;->b:Ldts;

    move-object/from16 v0, p3

    if-ne v0, v5, :cond_10

    const/4 v9, 0x4

    goto/16 :goto_3

    :cond_10
    const/4 v9, 0x0

    goto/16 :goto_3

    :cond_11
    const/4 v5, 0x0

    goto/16 :goto_4

    :catchall_4
    move-exception v4

    invoke-virtual {v12}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    const-string v5, "EsTileSync"

    const/4 v6, 0x4

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_12

    const-string v5, "[UPDATE_BEST_PHOTOS], rows deleted = "

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v18

    iget-wide v6, v0, Ldtq;->a:J

    move-object/from16 v0, v18

    iget-wide v8, v0, Ldtq;->b:J

    move-object/from16 v0, v18

    iget-wide v10, v0, Ldtq;->c:J

    move-object/from16 v0, v18

    iget-wide v14, v0, Ldtq;->d:J

    invoke-static/range {v16 .. v17}, Llse;->a(J)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v17

    move/from16 v0, v17

    add-int/lit16 v0, v0, 0x9f

    move/from16 v17, v0

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v18

    add-int v17, v17, v18

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(I)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", rows reordered = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", rows inserted = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", rows with new social data = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", duration: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_12
    throw v4
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    .line 715
    :catchall_5
    move-exception v4

    :try_start_f
    monitor-exit v5
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_5

    throw v4

    :catchall_6
    move-exception v4

    :try_start_10
    monitor-exit v5
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_6

    throw v4

    :cond_13
    move-object v13, v5

    goto/16 :goto_0
.end method

.method private static a(Landroid/content/Context;ILjava/lang/String;I)V
    .locals 2

    .prologue
    .line 271
    .line 272
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 273
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 275
    :try_start_0
    invoke-static {v1, p2, p3}, Ldtd;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)V

    .line 276
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 278
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 279
    return-void

    .line 278
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method static synthetic a(Landroid/content/Context;IZIILdth;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    .line 67
    invoke-virtual {p5}, Ldth;->c()[Lnzx;

    move-result-object v2

    invoke-virtual {p5}, Ldth;->e()[Lotf;

    move-result-object v3

    iget-object v4, p5, Ldth;->a:Lnyz;

    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    invoke-static/range {v0 .. v5}, Ljvd;->a(Landroid/content/Context;I[Lnzx;[Lotf;Lnyz;Z)V

    if-eqz p2, :cond_3

    invoke-virtual {p5}, Ldth;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "EsTileSync"

    invoke-static {v1, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x47

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Got next sync token in current page account: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " nextSyncToken="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    if-nez v0, :cond_1

    const-class v0, Lfew;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfew;

    invoke-virtual {v0, p1}, Lfew;->d(I)V

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "We got a null sync token from the server, pages read: "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x2b

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", resume token type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    const-string v1, "EsTileSync"

    invoke-static {v1, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p5}, Ldth;->a()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x6c

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "insertResumeAndNextSyncTokens account="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " resume token="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " nextSyncToken="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " resume token type="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_2
    invoke-virtual {p5}, Ldth;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v2

    invoke-virtual {v2}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    :try_start_0
    invoke-static {v2, v1, p4}, Ldtd;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)V

    const/4 v1, 0x1

    invoke-static {v2, v0, v1}, Ldtd;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)V

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    :goto_0
    return-void

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0

    :cond_3
    const-string v0, "EsTileSync"

    invoke-static {v0, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p5}, Ldth;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x52

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "insertRequestToken account="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " resume token="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " resume token type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_4
    invoke-virtual {p5}, Ldth;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, v0, p4}, Ldtd;->a(Landroid/content/Context;ILjava/lang/String;I)V

    goto :goto_0
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)V
    .locals 6

    .prologue
    .line 284
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 285
    const-string v1, "token_type"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 286
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 287
    const-string v1, "token"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 292
    :goto_0
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 294
    const-string v2, "SELECT COUNT(*) FROM photo_requests WHERE token_type = ?"

    invoke-static {p0, v2, v1}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    .line 296
    const-string v1, "photo_requests"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 301
    :goto_1
    return-void

    .line 289
    :cond_0
    const-string v1, "token"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 298
    :cond_1
    const-string v2, "photo_requests"

    const-string v3, "token_type = ?"

    invoke-virtual {p0, v2, v0, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_1
.end method

.method private static a(Ldtt;Ldtk;Ldtq;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 6

    .prologue
    .line 933
    .line 934
    invoke-virtual {p0, p1}, Ldtt;->d(Ldtk;)Ljava/util/ArrayList;

    move-result-object v1

    .line 935
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 952
    :goto_0
    return-void

    .line 942
    :cond_0
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 943
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 944
    invoke-virtual {v2}, Landroid/content/ContentValues;->clear()V

    .line 945
    const-string v4, "comment_count"

    invoke-virtual {p0, v0}, Ldtt;->c(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 946
    const-string v4, "plusone_count"

    invoke-virtual {p0, v0}, Ldtt;->b(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 947
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    .line 948
    const-string v0, "all_tiles"

    const-string v5, "tile_id = ?"

    invoke-virtual {p3, v0, v2, v5, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_1

    .line 951
    :cond_1
    iget-wide v2, p2, Ldtq;->d:J

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    int-to-long v0, v0

    add-long/2addr v0, v2

    iput-wide v0, p2, Ldtq;->d:J

    goto :goto_0
.end method

.method private static a(Ljava/lang/Thread;)V
    .locals 3

    .prologue
    .line 603
    if-eqz p0, :cond_0

    .line 605
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 610
    :cond_0
    return-void

    .line 606
    :catch_0
    move-exception v0

    .line 607
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Insert page thread interrupted!"

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method static synthetic a()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    sget-object v0, Ldtd;->b:[Ljava/lang/String;

    return-object v0
.end method

.method public static b(Landroid/content/Context;I)Z
    .locals 1

    .prologue
    .line 308
    const/4 v0, 0x2

    invoke-static {p0, p1, v0}, Ldtd;->a(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    sget-object v0, Ldtd;->c:[Ljava/lang/String;

    return-object v0
.end method

.method public static c(Landroid/content/Context;I)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 392
    invoke-static {p0, p1, v0}, Ldtd;->a(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v1

    .line 393
    const/4 v2, 0x3

    invoke-static {p0, p1, v2}, Ldtd;->a(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v2

    .line 395
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
