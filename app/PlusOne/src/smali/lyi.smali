.class public final Llyi;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/Integer;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/Boolean;

.field private f:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 94
    invoke-direct {p0}, Loxq;-><init>()V

    .line 107
    const/high16 v0, -0x80000000

    iput v0, p0, Llyi;->f:I

    .line 94
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 136
    const/4 v0, 0x0

    .line 137
    iget-object v1, p0, Llyi;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 138
    const/4 v0, 0x1

    iget-object v1, p0, Llyi;->a:Ljava/lang/String;

    .line 139
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 141
    :cond_0
    iget-object v1, p0, Llyi;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 142
    const/4 v1, 0x2

    iget-object v2, p0, Llyi;->b:Ljava/lang/String;

    .line 143
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 145
    :cond_1
    iget-object v1, p0, Llyi;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 146
    const/4 v1, 0x3

    iget-object v2, p0, Llyi;->c:Ljava/lang/Integer;

    .line 147
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 149
    :cond_2
    iget-object v1, p0, Llyi;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 150
    const/4 v1, 0x4

    iget-object v2, p0, Llyi;->d:Ljava/lang/String;

    .line 151
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 153
    :cond_3
    iget-object v1, p0, Llyi;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 154
    const/4 v1, 0x5

    iget-object v2, p0, Llyi;->e:Ljava/lang/Boolean;

    .line 155
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 157
    :cond_4
    iget v1, p0, Llyi;->f:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_5

    .line 158
    const/4 v1, 0x6

    iget v2, p0, Llyi;->f:I

    .line 159
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 161
    :cond_5
    iget-object v1, p0, Llyi;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 162
    iput v0, p0, Llyi;->ai:I

    .line 163
    return v0
.end method

.method public a(Loxn;)Llyi;
    .locals 3

    .prologue
    const/16 v2, 0x63

    .line 171
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 172
    sparse-switch v0, :sswitch_data_0

    .line 176
    iget-object v1, p0, Llyi;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 177
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llyi;->ah:Ljava/util/List;

    .line 180
    :cond_1
    iget-object v1, p0, Llyi;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 182
    :sswitch_0
    return-object p0

    .line 187
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llyi;->a:Ljava/lang/String;

    goto :goto_0

    .line 191
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llyi;->b:Ljava/lang/String;

    goto :goto_0

    .line 195
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Llyi;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 199
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llyi;->d:Ljava/lang/String;

    goto :goto_0

    .line 203
    :sswitch_5
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Llyi;->e:Ljava/lang/Boolean;

    goto :goto_0

    .line 207
    :sswitch_6
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 208
    if-eq v0, v2, :cond_2

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 213
    :cond_2
    iput v0, p0, Llyi;->f:I

    goto :goto_0

    .line 215
    :cond_3
    iput v2, p0, Llyi;->f:I

    goto :goto_0

    .line 172
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 112
    iget-object v0, p0, Llyi;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 113
    const/4 v0, 0x1

    iget-object v1, p0, Llyi;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 115
    :cond_0
    iget-object v0, p0, Llyi;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 116
    const/4 v0, 0x2

    iget-object v1, p0, Llyi;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 118
    :cond_1
    iget-object v0, p0, Llyi;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 119
    const/4 v0, 0x3

    iget-object v1, p0, Llyi;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 121
    :cond_2
    iget-object v0, p0, Llyi;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 122
    const/4 v0, 0x4

    iget-object v1, p0, Llyi;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 124
    :cond_3
    iget-object v0, p0, Llyi;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 125
    const/4 v0, 0x5

    iget-object v1, p0, Llyi;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 127
    :cond_4
    iget v0, p0, Llyi;->f:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_5

    .line 128
    const/4 v0, 0x6

    iget v1, p0, Llyi;->f:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 130
    :cond_5
    iget-object v0, p0, Llyi;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 132
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 90
    invoke-virtual {p0, p1}, Llyi;->a(Loxn;)Llyi;

    move-result-object v0

    return-object v0
.end method
