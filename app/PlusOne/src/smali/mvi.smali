.class public final Lmvi;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Integer;

.field private b:I

.field private c:Lmvj;

.field private d:Lmvj;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10319
    invoke-direct {p0}, Loxq;-><init>()V

    .line 10422
    const/high16 v0, -0x80000000

    iput v0, p0, Lmvi;->b:I

    .line 10425
    iput-object v1, p0, Lmvi;->c:Lmvj;

    .line 10428
    iput-object v1, p0, Lmvi;->d:Lmvj;

    .line 10319
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 10451
    const/4 v0, 0x0

    .line 10452
    iget-object v1, p0, Lmvi;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 10453
    const/4 v0, 0x1

    iget-object v1, p0, Lmvi;->a:Ljava/lang/Integer;

    .line 10454
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 10456
    :cond_0
    iget v1, p0, Lmvi;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_1

    .line 10457
    const/4 v1, 0x2

    iget v2, p0, Lmvi;->b:I

    .line 10458
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 10460
    :cond_1
    iget-object v1, p0, Lmvi;->c:Lmvj;

    if-eqz v1, :cond_2

    .line 10461
    const/4 v1, 0x3

    iget-object v2, p0, Lmvi;->c:Lmvj;

    .line 10462
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10464
    :cond_2
    iget-object v1, p0, Lmvi;->d:Lmvj;

    if-eqz v1, :cond_3

    .line 10465
    const/4 v1, 0x4

    iget-object v2, p0, Lmvi;->d:Lmvj;

    .line 10466
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10468
    :cond_3
    iget-object v1, p0, Lmvi;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10469
    iput v0, p0, Lmvi;->ai:I

    .line 10470
    return v0
.end method

.method public a(Loxn;)Lmvi;
    .locals 2

    .prologue
    .line 10478
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 10479
    sparse-switch v0, :sswitch_data_0

    .line 10483
    iget-object v1, p0, Lmvi;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 10484
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmvi;->ah:Ljava/util/List;

    .line 10487
    :cond_1
    iget-object v1, p0, Lmvi;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 10489
    :sswitch_0
    return-object p0

    .line 10494
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmvi;->a:Ljava/lang/Integer;

    goto :goto_0

    .line 10498
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 10499
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 10503
    :cond_2
    iput v0, p0, Lmvi;->b:I

    goto :goto_0

    .line 10505
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lmvi;->b:I

    goto :goto_0

    .line 10510
    :sswitch_3
    iget-object v0, p0, Lmvi;->c:Lmvj;

    if-nez v0, :cond_4

    .line 10511
    new-instance v0, Lmvj;

    invoke-direct {v0}, Lmvj;-><init>()V

    iput-object v0, p0, Lmvi;->c:Lmvj;

    .line 10513
    :cond_4
    iget-object v0, p0, Lmvi;->c:Lmvj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 10517
    :sswitch_4
    iget-object v0, p0, Lmvi;->d:Lmvj;

    if-nez v0, :cond_5

    .line 10518
    new-instance v0, Lmvj;

    invoke-direct {v0}, Lmvj;-><init>()V

    iput-object v0, p0, Lmvi;->d:Lmvj;

    .line 10520
    :cond_5
    iget-object v0, p0, Lmvi;->d:Lmvj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 10479
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 10433
    iget-object v0, p0, Lmvi;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 10434
    const/4 v0, 0x1

    iget-object v1, p0, Lmvi;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 10436
    :cond_0
    iget v0, p0, Lmvi;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 10437
    const/4 v0, 0x2

    iget v1, p0, Lmvi;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 10439
    :cond_1
    iget-object v0, p0, Lmvi;->c:Lmvj;

    if-eqz v0, :cond_2

    .line 10440
    const/4 v0, 0x3

    iget-object v1, p0, Lmvi;->c:Lmvj;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 10442
    :cond_2
    iget-object v0, p0, Lmvi;->d:Lmvj;

    if-eqz v0, :cond_3

    .line 10443
    const/4 v0, 0x4

    iget-object v1, p0, Lmvi;->d:Lmvj;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 10445
    :cond_3
    iget-object v0, p0, Lmvi;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 10447
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 10315
    invoke-virtual {p0, p1}, Lmvi;->a(Loxn;)Lmvi;

    move-result-object v0

    return-object v0
.end method
