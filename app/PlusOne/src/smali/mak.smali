.class public final Lmak;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnta;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 17413
    invoke-direct {p0}, Loxq;-><init>()V

    .line 17416
    iput-object v0, p0, Lmak;->apiHeader:Llyq;

    .line 17419
    iput-object v0, p0, Lmak;->a:Lnta;

    .line 17413
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 17436
    const/4 v0, 0x0

    .line 17437
    iget-object v1, p0, Lmak;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 17438
    const/4 v0, 0x1

    iget-object v1, p0, Lmak;->apiHeader:Llyq;

    .line 17439
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 17441
    :cond_0
    iget-object v1, p0, Lmak;->a:Lnta;

    if-eqz v1, :cond_1

    .line 17442
    const/4 v1, 0x2

    iget-object v2, p0, Lmak;->a:Lnta;

    .line 17443
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 17445
    :cond_1
    iget-object v1, p0, Lmak;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 17446
    iput v0, p0, Lmak;->ai:I

    .line 17447
    return v0
.end method

.method public a(Loxn;)Lmak;
    .locals 2

    .prologue
    .line 17455
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 17456
    sparse-switch v0, :sswitch_data_0

    .line 17460
    iget-object v1, p0, Lmak;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 17461
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmak;->ah:Ljava/util/List;

    .line 17464
    :cond_1
    iget-object v1, p0, Lmak;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 17466
    :sswitch_0
    return-object p0

    .line 17471
    :sswitch_1
    iget-object v0, p0, Lmak;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 17472
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmak;->apiHeader:Llyq;

    .line 17474
    :cond_2
    iget-object v0, p0, Lmak;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 17478
    :sswitch_2
    iget-object v0, p0, Lmak;->a:Lnta;

    if-nez v0, :cond_3

    .line 17479
    new-instance v0, Lnta;

    invoke-direct {v0}, Lnta;-><init>()V

    iput-object v0, p0, Lmak;->a:Lnta;

    .line 17481
    :cond_3
    iget-object v0, p0, Lmak;->a:Lnta;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 17456
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 17424
    iget-object v0, p0, Lmak;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 17425
    const/4 v0, 0x1

    iget-object v1, p0, Lmak;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 17427
    :cond_0
    iget-object v0, p0, Lmak;->a:Lnta;

    if-eqz v0, :cond_1

    .line 17428
    const/4 v0, 0x2

    iget-object v1, p0, Lmak;->a:Lnta;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 17430
    :cond_1
    iget-object v0, p0, Lmak;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 17432
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 17409
    invoke-virtual {p0, p1}, Lmak;->a(Loxn;)Lmak;

    move-result-object v0

    return-object v0
.end method
