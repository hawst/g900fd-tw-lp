.class public final Lfny;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lctm;


# instance fields
.field private a:Landroid/content/Context;

.field private b:I

.field private final c:Lcrh;

.field private final d:I

.field private final e:I

.field private final f:Z

.field private final g:Lhmw;


# direct methods
.method public constructor <init>(Lcrh;IIIZLhmw;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lfny;->c:Lcrh;

    .line 45
    iput p2, p0, Lfny;->b:I

    .line 46
    iput p3, p0, Lfny;->d:I

    .line 47
    iput p4, p0, Lfny;->e:I

    .line 48
    iput-boolean p5, p0, Lfny;->f:Z

    .line 49
    iput-object p6, p0, Lfny;->g:Lhmw;

    .line 50
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 104
    iget v0, p0, Lfny;->d:I

    return v0
.end method

.method public a(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10

    .prologue
    const/4 v2, 0x0

    const v9, 0x7f10009c

    const/4 v6, 0x1

    const/16 v8, 0x8

    const/4 v3, 0x0

    .line 54
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lfny;->a:Landroid/content/Context;

    .line 55
    if-nez p1, :cond_0

    .line 56
    iget-object v0, p0, Lfny;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 57
    const v1, 0x7f040057

    invoke-virtual {v0, v1, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 60
    :cond_0
    iget-object v0, p0, Lfny;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f110006

    iget-object v4, p0, Lfny;->c:Lcrh;

    iget v4, v4, Lcrh;->e:I

    invoke-virtual {v0, v1, v4}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v0

    new-array v1, v6, [Ljava/lang/Object;

    iget-object v4, p0, Lfny;->c:Lcrh;

    iget v4, v4, Lcrh;->e:I

    .line 61
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v3

    .line 60
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 63
    const-string v0, ""

    .line 64
    iget-object v4, p0, Lfny;->c:Lcrh;

    iget-object v4, v4, Lcrh;->f:Ljava/lang/String;

    if-eqz v4, :cond_1

    .line 65
    iget-object v0, p0, Lfny;->c:Lcrh;

    iget-object v0, v0, Lcrh;->f:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 66
    iget-object v4, p0, Lfny;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f110007

    invoke-virtual {v4, v5, v0}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v4

    new-array v5, v6, [Ljava/lang/Object;

    .line 67
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v3

    .line 66
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 70
    :cond_1
    iget-object v4, p0, Lfny;->c:Lcrh;

    iget-object v4, v4, Lcrh;->d:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x0

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 72
    const v0, 0x7f1001cf

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/stories/views/StoryCardHeroView;

    .line 73
    iget-object v1, p0, Lfny;->c:Lcrh;

    iget-object v1, v1, Lcrh;->g:Ljava/util/List;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lfny;->c:Lcrh;

    iget-object v1, v1, Lcrh;->g:Ljava/util/List;

    .line 74
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lfny;->c:Lcrh;

    iget-object v1, v1, Lcrh;->g:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcrb;

    move-object v4, v1

    .line 75
    :goto_0
    if-eqz v4, :cond_4

    iget-object v1, v4, Lcrb;->c:Lizu;

    :goto_1
    iget-object v2, p0, Lfny;->c:Lcrh;

    iget-object v6, v2, Lcrh;->a:Ljava/lang/String;

    if-eqz v4, :cond_5

    iget-object v2, v4, Lcrb;->k:Ljava/lang/Integer;

    if-eqz v2, :cond_5

    iget-object v2, v4, Lcrb;->k:Ljava/lang/Integer;

    .line 76
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 75
    :goto_2
    invoke-virtual {v0, v1, v6, v5, v2}, Lcom/google/android/apps/plus/stories/views/StoryCardHeroView;->a(Lizu;Ljava/lang/String;Ljava/lang/String;I)V

    .line 78
    const v0, 0x7f1001ce

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 79
    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    iget-object v1, p0, Lfny;->c:Lcrh;

    iget-object v1, v1, Lcrh;->c:Ljava/lang/String;

    invoke-virtual {v0, v9, v1}, Landroid/widget/ImageView;->setTag(ILjava/lang/Object;)V

    .line 82
    const v1, 0x7f1001cd

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 83
    iget-object v2, p0, Lfny;->c:Lcrh;

    iget-object v2, v2, Lcrh;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 84
    iget-object v2, p0, Lfny;->c:Lcrh;

    iget-object v2, v2, Lcrh;->c:Ljava/lang/String;

    invoke-virtual {p1, v9, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 85
    const v2, 0x7f10007a

    iget v4, p0, Lfny;->e:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p1, v2, v4}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 86
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 88
    iget-boolean v2, p0, Lfny;->f:Z

    if-nez v2, :cond_2

    .line 89
    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 90
    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 91
    const v0, 0x7f1001cc

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 93
    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 95
    const v0, 0x7f1001d0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 96
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 99
    :cond_2
    return-object p1

    :cond_3
    move-object v4, v2

    .line 74
    goto :goto_0

    :cond_4
    move-object v1, v2

    .line 75
    goto :goto_1

    :cond_5
    move v2, v3

    .line 76
    goto :goto_2
.end method

.method public a(Lctn;)V
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lfny;->c:Lcrh;

    invoke-interface {p1, v0}, Lctn;->a(Lcrh;)V

    .line 110
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    const v1, 0x7f10009c

    .line 114
    .line 115
    invoke-virtual {p1, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lfss;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    .line 117
    invoke-virtual {p1, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lfss;->b(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v5

    .line 118
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f1001ce

    if-ne v0, v1, :cond_0

    .line 119
    iget-object v0, p0, Lfny;->a:Landroid/content/Context;

    iget-object v1, p0, Lfny;->a:Landroid/content/Context;

    iget v3, p0, Lfny;->b:I

    invoke-static {v1, v3, v2}, Lfqt;->a(Landroid/content/Context;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 120
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 121
    const-string v0, "story_id"

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    iget-object v0, p0, Lfny;->a:Landroid/content/Context;

    const-class v2, Lhms;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    iget-object v3, p0, Lfny;->a:Landroid/content/Context;

    invoke-direct {v2, v3}, Lhmr;-><init>(Landroid/content/Context;)V

    iget v3, p0, Lfny;->b:I

    .line 123
    invoke-virtual {v2, v3}, Lhmr;->a(I)Lhmr;

    move-result-object v2

    sget-object v3, Lhmv;->eY:Lhmv;

    .line 124
    invoke-virtual {v2, v3}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v2

    iget-object v3, p0, Lfny;->g:Lhmw;

    .line 125
    invoke-virtual {v2, v3}, Lhmr;->a(Lhmw;)Lhmr;

    move-result-object v2

    .line 126
    invoke-virtual {v2, v1}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    .line 122
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 132
    :goto_0
    return-void

    .line 128
    :cond_0
    iget-object v6, p0, Lfny;->a:Landroid/content/Context;

    iget-object v0, p0, Lfny;->a:Landroid/content/Context;

    iget v1, p0, Lfny;->b:I

    move-object v4, v3

    .line 129
    invoke-static/range {v0 .. v5}, Lfqt;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 128
    invoke-virtual {v6, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
