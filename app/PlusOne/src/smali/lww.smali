.class public final Llww;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Llww;


# instance fields
.field public b:Llxo;

.field public c:Ljava/lang/String;

.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 5104
    const/4 v0, 0x0

    new-array v0, v0, [Llww;

    sput-object v0, Llww;->a:[Llww;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5105
    invoke-direct {p0}, Loxq;-><init>()V

    .line 5116
    const/4 v0, 0x0

    iput-object v0, p0, Llww;->b:Llxo;

    .line 5121
    const/high16 v0, -0x80000000

    iput v0, p0, Llww;->d:I

    .line 5105
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 5141
    const/4 v0, 0x0

    .line 5142
    iget-object v1, p0, Llww;->b:Llxo;

    if-eqz v1, :cond_0

    .line 5143
    const/4 v0, 0x1

    iget-object v1, p0, Llww;->b:Llxo;

    .line 5144
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 5146
    :cond_0
    iget-object v1, p0, Llww;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 5147
    const/4 v1, 0x2

    iget-object v2, p0, Llww;->c:Ljava/lang/String;

    .line 5148
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5150
    :cond_1
    iget v1, p0, Llww;->d:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_2

    .line 5151
    const/4 v1, 0x3

    iget v2, p0, Llww;->d:I

    .line 5152
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5154
    :cond_2
    iget-object v1, p0, Llww;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5155
    iput v0, p0, Llww;->ai:I

    .line 5156
    return v0
.end method

.method public a(Loxn;)Llww;
    .locals 2

    .prologue
    .line 5164
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 5165
    sparse-switch v0, :sswitch_data_0

    .line 5169
    iget-object v1, p0, Llww;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 5170
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llww;->ah:Ljava/util/List;

    .line 5173
    :cond_1
    iget-object v1, p0, Llww;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5175
    :sswitch_0
    return-object p0

    .line 5180
    :sswitch_1
    iget-object v0, p0, Llww;->b:Llxo;

    if-nez v0, :cond_2

    .line 5181
    new-instance v0, Llxo;

    invoke-direct {v0}, Llxo;-><init>()V

    iput-object v0, p0, Llww;->b:Llxo;

    .line 5183
    :cond_2
    iget-object v0, p0, Llww;->b:Llxo;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 5187
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llww;->c:Ljava/lang/String;

    goto :goto_0

    .line 5191
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 5192
    if-eqz v0, :cond_3

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    const/4 v1, 0x4

    if-ne v0, v1, :cond_4

    .line 5197
    :cond_3
    iput v0, p0, Llww;->d:I

    goto :goto_0

    .line 5199
    :cond_4
    const/4 v0, 0x0

    iput v0, p0, Llww;->d:I

    goto :goto_0

    .line 5165
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 5126
    iget-object v0, p0, Llww;->b:Llxo;

    if-eqz v0, :cond_0

    .line 5127
    const/4 v0, 0x1

    iget-object v1, p0, Llww;->b:Llxo;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 5129
    :cond_0
    iget-object v0, p0, Llww;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 5130
    const/4 v0, 0x2

    iget-object v1, p0, Llww;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 5132
    :cond_1
    iget v0, p0, Llww;->d:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_2

    .line 5133
    const/4 v0, 0x3

    iget v1, p0, Llww;->d:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 5135
    :cond_2
    iget-object v0, p0, Llww;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 5137
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5101
    invoke-virtual {p0, p1}, Llww;->a(Loxn;)Llww;

    move-result-object v0

    return-object v0
.end method
