.class public final Lmrs;
.super Loxq;
.source "PG"


# instance fields
.field private a:[Lmrp;

.field private b:[Lmrp;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:[Lmrt;

.field private f:Lmrr;

.field private g:Lmrq;

.field private h:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 468
    invoke-direct {p0}, Loxq;-><init>()V

    .line 544
    sget-object v0, Lmrp;->a:[Lmrp;

    iput-object v0, p0, Lmrs;->a:[Lmrp;

    .line 547
    sget-object v0, Lmrp;->a:[Lmrp;

    iput-object v0, p0, Lmrs;->b:[Lmrp;

    .line 554
    sget-object v0, Lmrt;->a:[Lmrt;

    iput-object v0, p0, Lmrs;->e:[Lmrt;

    .line 557
    iput-object v1, p0, Lmrs;->f:Lmrr;

    .line 560
    iput-object v1, p0, Lmrs;->g:Lmrq;

    .line 468
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 609
    .line 610
    iget-object v0, p0, Lmrs;->a:[Lmrp;

    if-eqz v0, :cond_1

    .line 611
    iget-object v3, p0, Lmrs;->a:[Lmrp;

    array-length v4, v3

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 612
    if-eqz v5, :cond_0

    .line 613
    const/4 v6, 0x1

    .line 614
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 611
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 618
    :cond_2
    iget-object v2, p0, Lmrs;->b:[Lmrp;

    if-eqz v2, :cond_4

    .line 619
    iget-object v3, p0, Lmrs;->b:[Lmrp;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_4

    aget-object v5, v3, v2

    .line 620
    if-eqz v5, :cond_3

    .line 621
    const/4 v6, 0x2

    .line 622
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 619
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 626
    :cond_4
    iget-object v2, p0, Lmrs;->c:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 627
    const/4 v2, 0x3

    iget-object v3, p0, Lmrs;->c:Ljava/lang/String;

    .line 628
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 630
    :cond_5
    iget-object v2, p0, Lmrs;->d:Ljava/lang/String;

    if-eqz v2, :cond_6

    .line 631
    const/4 v2, 0x4

    iget-object v3, p0, Lmrs;->d:Ljava/lang/String;

    .line 632
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 634
    :cond_6
    iget-object v2, p0, Lmrs;->e:[Lmrt;

    if-eqz v2, :cond_8

    .line 635
    iget-object v2, p0, Lmrs;->e:[Lmrt;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_8

    aget-object v4, v2, v1

    .line 636
    if-eqz v4, :cond_7

    .line 637
    const/4 v5, 0x5

    .line 638
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 635
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 642
    :cond_8
    iget-object v1, p0, Lmrs;->f:Lmrr;

    if-eqz v1, :cond_9

    .line 643
    const/4 v1, 0x6

    iget-object v2, p0, Lmrs;->f:Lmrr;

    .line 644
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 646
    :cond_9
    iget-object v1, p0, Lmrs;->g:Lmrq;

    if-eqz v1, :cond_a

    .line 647
    const/4 v1, 0x7

    iget-object v2, p0, Lmrs;->g:Lmrq;

    .line 648
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 650
    :cond_a
    iget-object v1, p0, Lmrs;->h:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    .line 651
    const/16 v1, 0x8

    iget-object v2, p0, Lmrs;->h:Ljava/lang/Boolean;

    .line 652
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 654
    :cond_b
    iget-object v1, p0, Lmrs;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 655
    iput v0, p0, Lmrs;->ai:I

    .line 656
    return v0
.end method

.method public a(Loxn;)Lmrs;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 664
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 665
    sparse-switch v0, :sswitch_data_0

    .line 669
    iget-object v2, p0, Lmrs;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 670
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmrs;->ah:Ljava/util/List;

    .line 673
    :cond_1
    iget-object v2, p0, Lmrs;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 675
    :sswitch_0
    return-object p0

    .line 680
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 681
    iget-object v0, p0, Lmrs;->a:[Lmrp;

    if-nez v0, :cond_3

    move v0, v1

    .line 682
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lmrp;

    .line 683
    iget-object v3, p0, Lmrs;->a:[Lmrp;

    if-eqz v3, :cond_2

    .line 684
    iget-object v3, p0, Lmrs;->a:[Lmrp;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 686
    :cond_2
    iput-object v2, p0, Lmrs;->a:[Lmrp;

    .line 687
    :goto_2
    iget-object v2, p0, Lmrs;->a:[Lmrp;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 688
    iget-object v2, p0, Lmrs;->a:[Lmrp;

    new-instance v3, Lmrp;

    invoke-direct {v3}, Lmrp;-><init>()V

    aput-object v3, v2, v0

    .line 689
    iget-object v2, p0, Lmrs;->a:[Lmrp;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 690
    invoke-virtual {p1}, Loxn;->a()I

    .line 687
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 681
    :cond_3
    iget-object v0, p0, Lmrs;->a:[Lmrp;

    array-length v0, v0

    goto :goto_1

    .line 693
    :cond_4
    iget-object v2, p0, Lmrs;->a:[Lmrp;

    new-instance v3, Lmrp;

    invoke-direct {v3}, Lmrp;-><init>()V

    aput-object v3, v2, v0

    .line 694
    iget-object v2, p0, Lmrs;->a:[Lmrp;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 698
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 699
    iget-object v0, p0, Lmrs;->b:[Lmrp;

    if-nez v0, :cond_6

    move v0, v1

    .line 700
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lmrp;

    .line 701
    iget-object v3, p0, Lmrs;->b:[Lmrp;

    if-eqz v3, :cond_5

    .line 702
    iget-object v3, p0, Lmrs;->b:[Lmrp;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 704
    :cond_5
    iput-object v2, p0, Lmrs;->b:[Lmrp;

    .line 705
    :goto_4
    iget-object v2, p0, Lmrs;->b:[Lmrp;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    .line 706
    iget-object v2, p0, Lmrs;->b:[Lmrp;

    new-instance v3, Lmrp;

    invoke-direct {v3}, Lmrp;-><init>()V

    aput-object v3, v2, v0

    .line 707
    iget-object v2, p0, Lmrs;->b:[Lmrp;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 708
    invoke-virtual {p1}, Loxn;->a()I

    .line 705
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 699
    :cond_6
    iget-object v0, p0, Lmrs;->b:[Lmrp;

    array-length v0, v0

    goto :goto_3

    .line 711
    :cond_7
    iget-object v2, p0, Lmrs;->b:[Lmrp;

    new-instance v3, Lmrp;

    invoke-direct {v3}, Lmrp;-><init>()V

    aput-object v3, v2, v0

    .line 712
    iget-object v2, p0, Lmrs;->b:[Lmrp;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 716
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmrs;->c:Ljava/lang/String;

    goto/16 :goto_0

    .line 720
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmrs;->d:Ljava/lang/String;

    goto/16 :goto_0

    .line 724
    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 725
    iget-object v0, p0, Lmrs;->e:[Lmrt;

    if-nez v0, :cond_9

    move v0, v1

    .line 726
    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lmrt;

    .line 727
    iget-object v3, p0, Lmrs;->e:[Lmrt;

    if-eqz v3, :cond_8

    .line 728
    iget-object v3, p0, Lmrs;->e:[Lmrt;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 730
    :cond_8
    iput-object v2, p0, Lmrs;->e:[Lmrt;

    .line 731
    :goto_6
    iget-object v2, p0, Lmrs;->e:[Lmrt;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_a

    .line 732
    iget-object v2, p0, Lmrs;->e:[Lmrt;

    new-instance v3, Lmrt;

    invoke-direct {v3}, Lmrt;-><init>()V

    aput-object v3, v2, v0

    .line 733
    iget-object v2, p0, Lmrs;->e:[Lmrt;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 734
    invoke-virtual {p1}, Loxn;->a()I

    .line 731
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 725
    :cond_9
    iget-object v0, p0, Lmrs;->e:[Lmrt;

    array-length v0, v0

    goto :goto_5

    .line 737
    :cond_a
    iget-object v2, p0, Lmrs;->e:[Lmrt;

    new-instance v3, Lmrt;

    invoke-direct {v3}, Lmrt;-><init>()V

    aput-object v3, v2, v0

    .line 738
    iget-object v2, p0, Lmrs;->e:[Lmrt;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 742
    :sswitch_6
    iget-object v0, p0, Lmrs;->f:Lmrr;

    if-nez v0, :cond_b

    .line 743
    new-instance v0, Lmrr;

    invoke-direct {v0}, Lmrr;-><init>()V

    iput-object v0, p0, Lmrs;->f:Lmrr;

    .line 745
    :cond_b
    iget-object v0, p0, Lmrs;->f:Lmrr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 749
    :sswitch_7
    iget-object v0, p0, Lmrs;->g:Lmrq;

    if-nez v0, :cond_c

    .line 750
    new-instance v0, Lmrq;

    invoke-direct {v0}, Lmrq;-><init>()V

    iput-object v0, p0, Lmrs;->g:Lmrq;

    .line 752
    :cond_c
    iget-object v0, p0, Lmrs;->g:Lmrq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 756
    :sswitch_8
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmrs;->h:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 665
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 567
    iget-object v1, p0, Lmrs;->a:[Lmrp;

    if-eqz v1, :cond_1

    .line 568
    iget-object v2, p0, Lmrs;->a:[Lmrp;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 569
    if-eqz v4, :cond_0

    .line 570
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 568
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 574
    :cond_1
    iget-object v1, p0, Lmrs;->b:[Lmrp;

    if-eqz v1, :cond_3

    .line 575
    iget-object v2, p0, Lmrs;->b:[Lmrp;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 576
    if-eqz v4, :cond_2

    .line 577
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 575
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 581
    :cond_3
    iget-object v1, p0, Lmrs;->c:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 582
    const/4 v1, 0x3

    iget-object v2, p0, Lmrs;->c:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 584
    :cond_4
    iget-object v1, p0, Lmrs;->d:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 585
    const/4 v1, 0x4

    iget-object v2, p0, Lmrs;->d:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 587
    :cond_5
    iget-object v1, p0, Lmrs;->e:[Lmrt;

    if-eqz v1, :cond_7

    .line 588
    iget-object v1, p0, Lmrs;->e:[Lmrt;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_7

    aget-object v3, v1, v0

    .line 589
    if-eqz v3, :cond_6

    .line 590
    const/4 v4, 0x5

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 588
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 594
    :cond_7
    iget-object v0, p0, Lmrs;->f:Lmrr;

    if-eqz v0, :cond_8

    .line 595
    const/4 v0, 0x6

    iget-object v1, p0, Lmrs;->f:Lmrr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 597
    :cond_8
    iget-object v0, p0, Lmrs;->g:Lmrq;

    if-eqz v0, :cond_9

    .line 598
    const/4 v0, 0x7

    iget-object v1, p0, Lmrs;->g:Lmrq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 600
    :cond_9
    iget-object v0, p0, Lmrs;->h:Ljava/lang/Boolean;

    if-eqz v0, :cond_a

    .line 601
    const/16 v0, 0x8

    iget-object v1, p0, Lmrs;->h:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 603
    :cond_a
    iget-object v0, p0, Lmrs;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 605
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 464
    invoke-virtual {p0, p1}, Lmrs;->a(Loxn;)Lmrs;

    move-result-object v0

    return-object v0
.end method
