.class public final Lnmo;
.super Loxq;
.source "PG"


# static fields
.field public static final a:Loxr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Loxr",
            "<",
            "Lnmo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:[Lnmn;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 681
    const v0, 0x2aa1f4f

    new-instance v1, Lnmp;

    invoke-direct {v1}, Lnmp;-><init>()V

    .line 686
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    move-result-object v0

    sput-object v0, Lnmo;->a:Loxr;

    .line 685
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 682
    invoke-direct {p0}, Loxq;-><init>()V

    .line 689
    sget-object v0, Lnmn;->a:[Lnmn;

    iput-object v0, p0, Lnmo;->b:[Lnmn;

    .line 682
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 707
    .line 708
    iget-object v1, p0, Lnmo;->b:[Lnmn;

    if-eqz v1, :cond_1

    .line 709
    iget-object v2, p0, Lnmo;->b:[Lnmn;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 710
    if-eqz v4, :cond_0

    .line 711
    const/4 v5, 0x1

    .line 712
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 709
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 716
    :cond_1
    iget-object v1, p0, Lnmo;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 717
    iput v0, p0, Lnmo;->ai:I

    .line 718
    return v0
.end method

.method public a(Loxn;)Lnmo;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 726
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 727
    sparse-switch v0, :sswitch_data_0

    .line 731
    iget-object v2, p0, Lnmo;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 732
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnmo;->ah:Ljava/util/List;

    .line 735
    :cond_1
    iget-object v2, p0, Lnmo;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 737
    :sswitch_0
    return-object p0

    .line 742
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 743
    iget-object v0, p0, Lnmo;->b:[Lnmn;

    if-nez v0, :cond_3

    move v0, v1

    .line 744
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnmn;

    .line 745
    iget-object v3, p0, Lnmo;->b:[Lnmn;

    if-eqz v3, :cond_2

    .line 746
    iget-object v3, p0, Lnmo;->b:[Lnmn;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 748
    :cond_2
    iput-object v2, p0, Lnmo;->b:[Lnmn;

    .line 749
    :goto_2
    iget-object v2, p0, Lnmo;->b:[Lnmn;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 750
    iget-object v2, p0, Lnmo;->b:[Lnmn;

    new-instance v3, Lnmn;

    invoke-direct {v3}, Lnmn;-><init>()V

    aput-object v3, v2, v0

    .line 751
    iget-object v2, p0, Lnmo;->b:[Lnmn;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 752
    invoke-virtual {p1}, Loxn;->a()I

    .line 749
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 743
    :cond_3
    iget-object v0, p0, Lnmo;->b:[Lnmn;

    array-length v0, v0

    goto :goto_1

    .line 755
    :cond_4
    iget-object v2, p0, Lnmo;->b:[Lnmn;

    new-instance v3, Lnmn;

    invoke-direct {v3}, Lnmn;-><init>()V

    aput-object v3, v2, v0

    .line 756
    iget-object v2, p0, Lnmo;->b:[Lnmn;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 727
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 694
    iget-object v0, p0, Lnmo;->b:[Lnmn;

    if-eqz v0, :cond_1

    .line 695
    iget-object v1, p0, Lnmo;->b:[Lnmn;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 696
    if-eqz v3, :cond_0

    .line 697
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 695
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 701
    :cond_1
    iget-object v0, p0, Lnmo;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 703
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 678
    invoke-virtual {p0, p1}, Lnmo;->a(Loxn;)Lnmo;

    move-result-object v0

    return-object v0
.end method
