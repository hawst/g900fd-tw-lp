.class public final Lfnt;
.super Lhny;
.source "PG"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:I

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 36
    const-string v0, "ListStoriesTask"

    invoke-direct {p0, p1, v0}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 37
    iput-object p1, p0, Lfnt;->a:Landroid/content/Context;

    .line 38
    iput p2, p0, Lfnt;->b:I

    .line 39
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    invoke-interface {v0, p2}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfnt;->c:Ljava/lang/String;

    .line 41
    iput-object p3, p0, Lfnt;->d:Ljava/lang/String;

    .line 42
    return-void
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 10

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x1

    .line 46
    new-instance v8, Lfnn;

    iget-object v0, p0, Lfnt;->a:Landroid/content/Context;

    iget v1, p0, Lfnt;->b:I

    iget-object v2, p0, Lfnt;->d:Ljava/lang/String;

    invoke-direct {v8, v0, v1, v2}, Lfnn;-><init>(Landroid/content/Context;ILjava/lang/String;)V

    .line 48
    iget-object v0, p0, Lfnt;->a:Landroid/content/Context;

    const-class v1, Lkfd;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkfd;

    invoke-interface {v0, v8}, Lkfd;->a(Lkff;)V

    .line 50
    invoke-virtual {v8}, Lfnn;->D()Loxu;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 51
    invoke-virtual {v8}, Lfnn;->D()Loxu;

    move-result-object v0

    check-cast v0, Lmej;

    iget-object v0, v0, Lmej;->a:Lmlt;

    .line 52
    invoke-static {v0}, Ljvj;->a(Lmlt;)[Lnzx;

    move-result-object v3

    .line 54
    const/16 v1, 0x8

    new-array v2, v7, [Ljava/lang/String;

    iget-object v4, p0, Lfnt;->c:Ljava/lang/String;

    aput-object v4, v2, v5

    invoke-static {v1, v2}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 55
    iget-object v1, p0, Lfnt;->a:Landroid/content/Context;

    iget v4, p0, Lfnt;->b:I

    iget-object v9, v0, Lmlt;->d:Ljava/lang/String;

    iget-object v0, p0, Lfnt;->d:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v7

    :goto_0
    invoke-static {v1, v4, v2, v9, v0}, Ljvj;->b(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)V

    .line 57
    iget-object v0, p0, Lfnt;->a:Landroid/content/Context;

    iget v1, p0, Lfnt;->b:I

    iget-object v4, p0, Lfnt;->d:Ljava/lang/String;

    if-nez v4, :cond_2

    move v4, v7

    :goto_1
    invoke-static/range {v0 .. v7}, Ljvj;->a(Landroid/content/Context;ILjava/lang/String;[Lnzx;ZZLjava/lang/String;Z)V

    .line 62
    :goto_2
    new-instance v0, Lhoz;

    iget v1, v8, Lkff;->i:I

    iget-object v2, v8, Lkff;->k:Ljava/lang/Exception;

    invoke-direct {v0, v1, v2, v6}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    .line 63
    if-eqz v3, :cond_0

    array-length v1, v3

    if-lez v1, :cond_0

    .line 64
    invoke-virtual {v0}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "has_stories"

    invoke-virtual {v1, v2, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 66
    :cond_0
    return-object v0

    :cond_1
    move v0, v5

    .line 55
    goto :goto_0

    :cond_2
    move v4, v5

    .line 57
    goto :goto_1

    :cond_3
    move-object v3, v6

    goto :goto_2
.end method
