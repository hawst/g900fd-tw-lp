.class public final Lpui;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lpuj;

.field public b:[Lpuj;

.field private c:Lppf;

.field private d:Ljava/lang/Long;

.field private e:Ljava/lang/Long;

.field private f:Ljava/lang/Integer;

.field private g:Lppf;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 18
    invoke-direct {p0}, Loxq;-><init>()V

    .line 108
    iput-object v1, p0, Lpui;->c:Lppf;

    .line 111
    iput-object v1, p0, Lpui;->a:Lpuj;

    .line 120
    sget-object v0, Lpuj;->a:[Lpuj;

    iput-object v0, p0, Lpui;->b:[Lpuj;

    .line 123
    iput-object v1, p0, Lpui;->g:Lppf;

    .line 18
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 159
    .line 160
    iget-object v0, p0, Lpui;->c:Lppf;

    if-eqz v0, :cond_7

    .line 161
    const/4 v0, 0x1

    iget-object v2, p0, Lpui;->c:Lppf;

    .line 162
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 164
    :goto_0
    iget-object v2, p0, Lpui;->a:Lpuj;

    if-eqz v2, :cond_0

    .line 165
    const/4 v2, 0x2

    iget-object v3, p0, Lpui;->a:Lpuj;

    .line 166
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 168
    :cond_0
    iget-object v2, p0, Lpui;->d:Ljava/lang/Long;

    if-eqz v2, :cond_1

    .line 169
    const/4 v2, 0x3

    iget-object v3, p0, Lpui;->d:Ljava/lang/Long;

    .line 170
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Loxo;->f(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 172
    :cond_1
    iget-object v2, p0, Lpui;->e:Ljava/lang/Long;

    if-eqz v2, :cond_2

    .line 173
    const/4 v2, 0x4

    iget-object v3, p0, Lpui;->e:Ljava/lang/Long;

    .line 174
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Loxo;->f(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 176
    :cond_2
    iget-object v2, p0, Lpui;->f:Ljava/lang/Integer;

    if-eqz v2, :cond_3

    .line 177
    const/4 v2, 0x5

    iget-object v3, p0, Lpui;->f:Ljava/lang/Integer;

    .line 178
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->g(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 180
    :cond_3
    iget-object v2, p0, Lpui;->b:[Lpuj;

    if-eqz v2, :cond_5

    .line 181
    iget-object v2, p0, Lpui;->b:[Lpuj;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    .line 182
    if-eqz v4, :cond_4

    .line 183
    const/4 v5, 0x6

    .line 184
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 181
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 188
    :cond_5
    iget-object v1, p0, Lpui;->g:Lppf;

    if-eqz v1, :cond_6

    .line 189
    const/4 v1, 0x7

    iget-object v2, p0, Lpui;->g:Lppf;

    .line 190
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 192
    :cond_6
    iget-object v1, p0, Lpui;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 193
    iput v0, p0, Lpui;->ai:I

    .line 194
    return v0

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lpui;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 202
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 203
    sparse-switch v0, :sswitch_data_0

    .line 207
    iget-object v2, p0, Lpui;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 208
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lpui;->ah:Ljava/util/List;

    .line 211
    :cond_1
    iget-object v2, p0, Lpui;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 213
    :sswitch_0
    return-object p0

    .line 218
    :sswitch_1
    iget-object v0, p0, Lpui;->c:Lppf;

    if-nez v0, :cond_2

    .line 219
    new-instance v0, Lppf;

    invoke-direct {v0}, Lppf;-><init>()V

    iput-object v0, p0, Lpui;->c:Lppf;

    .line 221
    :cond_2
    iget-object v0, p0, Lpui;->c:Lppf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 225
    :sswitch_2
    iget-object v0, p0, Lpui;->a:Lpuj;

    if-nez v0, :cond_3

    .line 226
    new-instance v0, Lpuj;

    invoke-direct {v0}, Lpuj;-><init>()V

    iput-object v0, p0, Lpui;->a:Lpuj;

    .line 228
    :cond_3
    iget-object v0, p0, Lpui;->a:Lpuj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 232
    :sswitch_3
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lpui;->d:Ljava/lang/Long;

    goto :goto_0

    .line 236
    :sswitch_4
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lpui;->e:Ljava/lang/Long;

    goto :goto_0

    .line 240
    :sswitch_5
    invoke-virtual {p1}, Loxn;->m()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lpui;->f:Ljava/lang/Integer;

    goto :goto_0

    .line 244
    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 245
    iget-object v0, p0, Lpui;->b:[Lpuj;

    if-nez v0, :cond_5

    move v0, v1

    .line 246
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lpuj;

    .line 247
    iget-object v3, p0, Lpui;->b:[Lpuj;

    if-eqz v3, :cond_4

    .line 248
    iget-object v3, p0, Lpui;->b:[Lpuj;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 250
    :cond_4
    iput-object v2, p0, Lpui;->b:[Lpuj;

    .line 251
    :goto_2
    iget-object v2, p0, Lpui;->b:[Lpuj;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 252
    iget-object v2, p0, Lpui;->b:[Lpuj;

    new-instance v3, Lpuj;

    invoke-direct {v3}, Lpuj;-><init>()V

    aput-object v3, v2, v0

    .line 253
    iget-object v2, p0, Lpui;->b:[Lpuj;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 254
    invoke-virtual {p1}, Loxn;->a()I

    .line 251
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 245
    :cond_5
    iget-object v0, p0, Lpui;->b:[Lpuj;

    array-length v0, v0

    goto :goto_1

    .line 257
    :cond_6
    iget-object v2, p0, Lpui;->b:[Lpuj;

    new-instance v3, Lpuj;

    invoke-direct {v3}, Lpuj;-><init>()V

    aput-object v3, v2, v0

    .line 258
    iget-object v2, p0, Lpui;->b:[Lpuj;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 262
    :sswitch_7
    iget-object v0, p0, Lpui;->g:Lppf;

    if-nez v0, :cond_7

    .line 263
    new-instance v0, Lppf;

    invoke-direct {v0}, Lppf;-><init>()V

    iput-object v0, p0, Lpui;->g:Lppf;

    .line 265
    :cond_7
    iget-object v0, p0, Lpui;->g:Lppf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 203
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 128
    iget-object v0, p0, Lpui;->c:Lppf;

    if-eqz v0, :cond_0

    .line 129
    const/4 v0, 0x1

    iget-object v1, p0, Lpui;->c:Lppf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 131
    :cond_0
    iget-object v0, p0, Lpui;->a:Lpuj;

    if-eqz v0, :cond_1

    .line 132
    const/4 v0, 0x2

    iget-object v1, p0, Lpui;->a:Lpuj;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 134
    :cond_1
    iget-object v0, p0, Lpui;->d:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 135
    const/4 v0, 0x3

    iget-object v1, p0, Lpui;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 137
    :cond_2
    iget-object v0, p0, Lpui;->e:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 138
    const/4 v0, 0x4

    iget-object v1, p0, Lpui;->e:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 140
    :cond_3
    iget-object v0, p0, Lpui;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 141
    const/4 v0, 0x5

    iget-object v1, p0, Lpui;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->c(II)V

    .line 143
    :cond_4
    iget-object v0, p0, Lpui;->b:[Lpuj;

    if-eqz v0, :cond_6

    .line 144
    iget-object v1, p0, Lpui;->b:[Lpuj;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_6

    aget-object v3, v1, v0

    .line 145
    if-eqz v3, :cond_5

    .line 146
    const/4 v4, 0x6

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 144
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 150
    :cond_6
    iget-object v0, p0, Lpui;->g:Lppf;

    if-eqz v0, :cond_7

    .line 151
    const/4 v0, 0x7

    iget-object v1, p0, Lpui;->g:Lppf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 153
    :cond_7
    iget-object v0, p0, Lpui;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 155
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 14
    invoke-virtual {p0, p1}, Lpui;->a(Loxn;)Lpui;

    move-result-object v0

    return-object v0
.end method
