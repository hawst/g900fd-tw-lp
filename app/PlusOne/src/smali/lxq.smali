.class public final Llxq;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:I

.field public c:Ljava/lang/Boolean;

.field private d:Ljava/lang/Boolean;

.field private e:[Ljava/lang/String;

.field private f:[Ljava/lang/String;

.field private g:[Ljava/lang/String;

.field private h:[I

.field private i:[Ljava/lang/Long;

.field private j:I

.field private k:[I

.field private l:[Llwv;

.field private m:[Ljava/lang/Long;

.field private n:[Ljava/lang/Long;

.field private o:Ljava/lang/Boolean;

.field private p:[I

.field private q:[Ljava/lang/String;

.field private r:Ljava/lang/Long;

.field private s:Llxt;

.field private t:Llyc;

.field private u:Llxe;

.field private v:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/high16 v1, -0x80000000

    .line 1197
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1232
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Llxq;->e:[Ljava/lang/String;

    .line 1235
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Llxq;->f:[Ljava/lang/String;

    .line 1240
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Llxq;->g:[Ljava/lang/String;

    .line 1243
    sget-object v0, Loxx;->a:[I

    iput-object v0, p0, Llxq;->h:[I

    .line 1246
    sget-object v0, Loxx;->h:[Ljava/lang/Long;

    iput-object v0, p0, Llxq;->i:[Ljava/lang/Long;

    .line 1249
    iput v1, p0, Llxq;->b:I

    .line 1252
    iput v1, p0, Llxq;->j:I

    .line 1255
    sget-object v0, Loxx;->a:[I

    iput-object v0, p0, Llxq;->k:[I

    .line 1258
    sget-object v0, Llwv;->a:[Llwv;

    iput-object v0, p0, Llxq;->l:[Llwv;

    .line 1261
    sget-object v0, Loxx;->h:[Ljava/lang/Long;

    iput-object v0, p0, Llxq;->m:[Ljava/lang/Long;

    .line 1264
    sget-object v0, Loxx;->h:[Ljava/lang/Long;

    iput-object v0, p0, Llxq;->n:[Ljava/lang/Long;

    .line 1269
    sget-object v0, Loxx;->a:[I

    iput-object v0, p0, Llxq;->p:[I

    .line 1274
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Llxq;->q:[Ljava/lang/String;

    .line 1279
    iput-object v2, p0, Llxq;->s:Llxt;

    .line 1282
    iput-object v2, p0, Llxq;->t:Llyc;

    .line 1285
    iput-object v2, p0, Llxq;->u:Llxe;

    .line 1288
    iput v1, p0, Llxq;->v:I

    .line 1197
    return-void
.end method


# virtual methods
.method public a()I
    .locals 9

    .prologue
    const/high16 v8, -0x80000000

    const/4 v1, 0x0

    .line 1389
    .line 1390
    iget-object v0, p0, Llxq;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_20

    .line 1391
    const/4 v0, 0x1

    iget-object v2, p0, Llxq;->d:Ljava/lang/Boolean;

    .line 1392
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 1394
    :goto_0
    iget-object v2, p0, Llxq;->m:[Ljava/lang/Long;

    if-eqz v2, :cond_1

    iget-object v2, p0, Llxq;->m:[Ljava/lang/Long;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 1396
    iget-object v4, p0, Llxq;->m:[Ljava/lang/Long;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_1
    if-ge v2, v5, :cond_0

    aget-object v6, v4, v2

    .line 1398
    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Loxo;->f(J)I

    move-result v6

    add-int/2addr v3, v6

    .line 1396
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1400
    :cond_0
    add-int/2addr v0, v3

    .line 1401
    iget-object v2, p0, Llxq;->m:[Ljava/lang/Long;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1403
    :cond_1
    iget-object v2, p0, Llxq;->n:[Ljava/lang/Long;

    if-eqz v2, :cond_3

    iget-object v2, p0, Llxq;->n:[Ljava/lang/Long;

    array-length v2, v2

    if-lez v2, :cond_3

    .line 1405
    iget-object v4, p0, Llxq;->n:[Ljava/lang/Long;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_2
    if-ge v2, v5, :cond_2

    aget-object v6, v4, v2

    .line 1407
    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Loxo;->f(J)I

    move-result v6

    add-int/2addr v3, v6

    .line 1405
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1409
    :cond_2
    add-int/2addr v0, v3

    .line 1410
    iget-object v2, p0, Llxq;->n:[Ljava/lang/Long;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1412
    :cond_3
    iget-object v2, p0, Llxq;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    .line 1413
    const/4 v2, 0x4

    iget-object v3, p0, Llxq;->a:Ljava/lang/String;

    .line 1414
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1416
    :cond_4
    iget v2, p0, Llxq;->b:I

    if-eq v2, v8, :cond_5

    .line 1417
    const/4 v2, 0x5

    iget v3, p0, Llxq;->b:I

    .line 1418
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 1420
    :cond_5
    iget-object v2, p0, Llxq;->i:[Ljava/lang/Long;

    if-eqz v2, :cond_7

    iget-object v2, p0, Llxq;->i:[Ljava/lang/Long;

    array-length v2, v2

    if-lez v2, :cond_7

    .line 1422
    iget-object v4, p0, Llxq;->i:[Ljava/lang/Long;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_3
    if-ge v2, v5, :cond_6

    aget-object v6, v4, v2

    .line 1424
    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Loxo;->f(J)I

    move-result v6

    add-int/2addr v3, v6

    .line 1422
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 1426
    :cond_6
    add-int/2addr v0, v3

    .line 1427
    iget-object v2, p0, Llxq;->i:[Ljava/lang/Long;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1429
    :cond_7
    iget-object v2, p0, Llxq;->o:Ljava/lang/Boolean;

    if-eqz v2, :cond_8

    .line 1430
    const/4 v2, 0x7

    iget-object v3, p0, Llxq;->o:Ljava/lang/Boolean;

    .line 1431
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1433
    :cond_8
    iget-object v2, p0, Llxq;->e:[Ljava/lang/String;

    if-eqz v2, :cond_a

    iget-object v2, p0, Llxq;->e:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_a

    .line 1435
    iget-object v4, p0, Llxq;->e:[Ljava/lang/String;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_4
    if-ge v2, v5, :cond_9

    aget-object v6, v4, v2

    .line 1437
    invoke-static {v6}, Loxo;->b(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v3, v6

    .line 1435
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 1439
    :cond_9
    add-int/2addr v0, v3

    .line 1440
    iget-object v2, p0, Llxq;->e:[Ljava/lang/String;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1442
    :cond_a
    iget-object v2, p0, Llxq;->f:[Ljava/lang/String;

    if-eqz v2, :cond_c

    iget-object v2, p0, Llxq;->f:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_c

    .line 1444
    iget-object v4, p0, Llxq;->f:[Ljava/lang/String;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_5
    if-ge v2, v5, :cond_b

    aget-object v6, v4, v2

    .line 1446
    invoke-static {v6}, Loxo;->b(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v3, v6

    .line 1444
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 1448
    :cond_b
    add-int/2addr v0, v3

    .line 1449
    iget-object v2, p0, Llxq;->f:[Ljava/lang/String;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1451
    :cond_c
    iget-object v2, p0, Llxq;->k:[I

    if-eqz v2, :cond_e

    iget-object v2, p0, Llxq;->k:[I

    array-length v2, v2

    if-lez v2, :cond_e

    .line 1453
    iget-object v4, p0, Llxq;->k:[I

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_6
    if-ge v2, v5, :cond_d

    aget v6, v4, v2

    .line 1455
    invoke-static {v6}, Loxo;->i(I)I

    move-result v6

    add-int/2addr v3, v6

    .line 1453
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 1457
    :cond_d
    add-int/2addr v0, v3

    .line 1458
    iget-object v2, p0, Llxq;->k:[I

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1460
    :cond_e
    iget-object v2, p0, Llxq;->p:[I

    if-eqz v2, :cond_10

    iget-object v2, p0, Llxq;->p:[I

    array-length v2, v2

    if-lez v2, :cond_10

    .line 1462
    iget-object v4, p0, Llxq;->p:[I

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_7
    if-ge v2, v5, :cond_f

    aget v6, v4, v2

    .line 1464
    invoke-static {v6}, Loxo;->i(I)I

    move-result v6

    add-int/2addr v3, v6

    .line 1462
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    .line 1466
    :cond_f
    add-int/2addr v0, v3

    .line 1467
    iget-object v2, p0, Llxq;->p:[I

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1469
    :cond_10
    iget-object v2, p0, Llxq;->c:Ljava/lang/Boolean;

    if-eqz v2, :cond_11

    .line 1470
    const/16 v2, 0xc

    iget-object v3, p0, Llxq;->c:Ljava/lang/Boolean;

    .line 1471
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1473
    :cond_11
    iget-object v2, p0, Llxq;->h:[I

    if-eqz v2, :cond_13

    iget-object v2, p0, Llxq;->h:[I

    array-length v2, v2

    if-lez v2, :cond_13

    .line 1475
    iget-object v4, p0, Llxq;->h:[I

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_8
    if-ge v2, v5, :cond_12

    aget v6, v4, v2

    .line 1477
    invoke-static {v6}, Loxo;->i(I)I

    move-result v6

    add-int/2addr v3, v6

    .line 1475
    add-int/lit8 v2, v2, 0x1

    goto :goto_8

    .line 1479
    :cond_12
    add-int/2addr v0, v3

    .line 1480
    iget-object v2, p0, Llxq;->h:[I

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1482
    :cond_13
    iget v2, p0, Llxq;->j:I

    if-eq v2, v8, :cond_14

    .line 1483
    const/16 v2, 0xe

    iget v3, p0, Llxq;->j:I

    .line 1484
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 1486
    :cond_14
    iget-object v2, p0, Llxq;->q:[Ljava/lang/String;

    if-eqz v2, :cond_16

    iget-object v2, p0, Llxq;->q:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_16

    .line 1488
    iget-object v4, p0, Llxq;->q:[Ljava/lang/String;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_9
    if-ge v2, v5, :cond_15

    aget-object v6, v4, v2

    .line 1490
    invoke-static {v6}, Loxo;->b(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v3, v6

    .line 1488
    add-int/lit8 v2, v2, 0x1

    goto :goto_9

    .line 1492
    :cond_15
    add-int/2addr v0, v3

    .line 1493
    iget-object v2, p0, Llxq;->q:[Ljava/lang/String;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1495
    :cond_16
    iget-object v2, p0, Llxq;->r:Ljava/lang/Long;

    if-eqz v2, :cond_17

    .line 1496
    const/16 v2, 0x10

    iget-object v3, p0, Llxq;->r:Ljava/lang/Long;

    .line 1497
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Loxo;->f(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 1499
    :cond_17
    iget-object v2, p0, Llxq;->l:[Llwv;

    if-eqz v2, :cond_19

    .line 1500
    iget-object v3, p0, Llxq;->l:[Llwv;

    array-length v4, v3

    move v2, v1

    :goto_a
    if-ge v2, v4, :cond_19

    aget-object v5, v3, v2

    .line 1501
    if-eqz v5, :cond_18

    .line 1502
    const/16 v6, 0x11

    .line 1503
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 1500
    :cond_18
    add-int/lit8 v2, v2, 0x1

    goto :goto_a

    .line 1507
    :cond_19
    iget-object v2, p0, Llxq;->g:[Ljava/lang/String;

    if-eqz v2, :cond_1b

    iget-object v2, p0, Llxq;->g:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_1b

    .line 1509
    iget-object v3, p0, Llxq;->g:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_b
    if-ge v1, v4, :cond_1a

    aget-object v5, v3, v1

    .line 1511
    invoke-static {v5}, Loxo;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 1509
    add-int/lit8 v1, v1, 0x1

    goto :goto_b

    .line 1513
    :cond_1a
    add-int/2addr v0, v2

    .line 1514
    iget-object v1, p0, Llxq;->g:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    .line 1516
    :cond_1b
    iget-object v1, p0, Llxq;->t:Llyc;

    if-eqz v1, :cond_1c

    .line 1517
    const/16 v1, 0x14

    iget-object v2, p0, Llxq;->t:Llyc;

    .line 1518
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1520
    :cond_1c
    iget-object v1, p0, Llxq;->s:Llxt;

    if-eqz v1, :cond_1d

    .line 1521
    const/16 v1, 0x15

    iget-object v2, p0, Llxq;->s:Llxt;

    .line 1522
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1524
    :cond_1d
    iget-object v1, p0, Llxq;->u:Llxe;

    if-eqz v1, :cond_1e

    .line 1525
    const/16 v1, 0x16

    iget-object v2, p0, Llxq;->u:Llxe;

    .line 1526
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1528
    :cond_1e
    iget v1, p0, Llxq;->v:I

    if-eq v1, v8, :cond_1f

    .line 1529
    const/16 v1, 0x17

    iget v2, p0, Llxq;->v:I

    .line 1530
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1532
    :cond_1f
    iget-object v1, p0, Llxq;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1533
    iput v0, p0, Llxq;->ai:I

    .line 1534
    return v0

    :cond_20
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Llxq;
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 1542
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1543
    sparse-switch v0, :sswitch_data_0

    .line 1547
    iget-object v2, p0, Llxq;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 1548
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Llxq;->ah:Ljava/util/List;

    .line 1551
    :cond_1
    iget-object v2, p0, Llxq;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1553
    :sswitch_0
    return-object p0

    .line 1558
    :sswitch_1
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Llxq;->d:Ljava/lang/Boolean;

    goto :goto_0

    .line 1562
    :sswitch_2
    const/16 v0, 0x10

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1563
    iget-object v0, p0, Llxq;->m:[Ljava/lang/Long;

    array-length v0, v0

    .line 1564
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/Long;

    .line 1565
    iget-object v3, p0, Llxq;->m:[Ljava/lang/Long;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1566
    iput-object v2, p0, Llxq;->m:[Ljava/lang/Long;

    .line 1567
    :goto_1
    iget-object v2, p0, Llxq;->m:[Ljava/lang/Long;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_2

    .line 1568
    iget-object v2, p0, Llxq;->m:[Ljava/lang/Long;

    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v0

    .line 1569
    invoke-virtual {p1}, Loxn;->a()I

    .line 1567
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1572
    :cond_2
    iget-object v2, p0, Llxq;->m:[Ljava/lang/Long;

    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v0

    goto :goto_0

    .line 1576
    :sswitch_3
    const/16 v0, 0x18

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1577
    iget-object v0, p0, Llxq;->n:[Ljava/lang/Long;

    array-length v0, v0

    .line 1578
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/Long;

    .line 1579
    iget-object v3, p0, Llxq;->n:[Ljava/lang/Long;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1580
    iput-object v2, p0, Llxq;->n:[Ljava/lang/Long;

    .line 1581
    :goto_2
    iget-object v2, p0, Llxq;->n:[Ljava/lang/Long;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_3

    .line 1582
    iget-object v2, p0, Llxq;->n:[Ljava/lang/Long;

    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v0

    .line 1583
    invoke-virtual {p1}, Loxn;->a()I

    .line 1581
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1586
    :cond_3
    iget-object v2, p0, Llxq;->n:[Ljava/lang/Long;

    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    .line 1590
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llxq;->a:Ljava/lang/String;

    goto/16 :goto_0

    .line 1594
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1595
    if-eq v0, v7, :cond_4

    if-eqz v0, :cond_4

    if-ne v0, v6, :cond_5

    .line 1598
    :cond_4
    iput v0, p0, Llxq;->b:I

    goto/16 :goto_0

    .line 1600
    :cond_5
    iput v7, p0, Llxq;->b:I

    goto/16 :goto_0

    .line 1605
    :sswitch_6
    const/16 v0, 0x30

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1606
    iget-object v0, p0, Llxq;->i:[Ljava/lang/Long;

    array-length v0, v0

    .line 1607
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/Long;

    .line 1608
    iget-object v3, p0, Llxq;->i:[Ljava/lang/Long;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1609
    iput-object v2, p0, Llxq;->i:[Ljava/lang/Long;

    .line 1610
    :goto_3
    iget-object v2, p0, Llxq;->i:[Ljava/lang/Long;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 1611
    iget-object v2, p0, Llxq;->i:[Ljava/lang/Long;

    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v0

    .line 1612
    invoke-virtual {p1}, Loxn;->a()I

    .line 1610
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 1615
    :cond_6
    iget-object v2, p0, Llxq;->i:[Ljava/lang/Long;

    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    .line 1619
    :sswitch_7
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Llxq;->o:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 1623
    :sswitch_8
    const/16 v0, 0x42

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1624
    iget-object v0, p0, Llxq;->e:[Ljava/lang/String;

    array-length v0, v0

    .line 1625
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 1626
    iget-object v3, p0, Llxq;->e:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1627
    iput-object v2, p0, Llxq;->e:[Ljava/lang/String;

    .line 1628
    :goto_4
    iget-object v2, p0, Llxq;->e:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    .line 1629
    iget-object v2, p0, Llxq;->e:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 1630
    invoke-virtual {p1}, Loxn;->a()I

    .line 1628
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 1633
    :cond_7
    iget-object v2, p0, Llxq;->e:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    .line 1637
    :sswitch_9
    const/16 v0, 0x4a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1638
    iget-object v0, p0, Llxq;->f:[Ljava/lang/String;

    array-length v0, v0

    .line 1639
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 1640
    iget-object v3, p0, Llxq;->f:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1641
    iput-object v2, p0, Llxq;->f:[Ljava/lang/String;

    .line 1642
    :goto_5
    iget-object v2, p0, Llxq;->f:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_8

    .line 1643
    iget-object v2, p0, Llxq;->f:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 1644
    invoke-virtual {p1}, Loxn;->a()I

    .line 1642
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 1647
    :cond_8
    iget-object v2, p0, Llxq;->f:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    .line 1651
    :sswitch_a
    const/16 v0, 0x50

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1652
    iget-object v0, p0, Llxq;->k:[I

    array-length v0, v0

    .line 1653
    add-int/2addr v2, v0

    new-array v2, v2, [I

    .line 1654
    iget-object v3, p0, Llxq;->k:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1655
    iput-object v2, p0, Llxq;->k:[I

    .line 1656
    :goto_6
    iget-object v2, p0, Llxq;->k:[I

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    .line 1657
    iget-object v2, p0, Llxq;->k:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v3

    aput v3, v2, v0

    .line 1658
    invoke-virtual {p1}, Loxn;->a()I

    .line 1656
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 1661
    :cond_9
    iget-object v2, p0, Llxq;->k:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v3

    aput v3, v2, v0

    goto/16 :goto_0

    .line 1665
    :sswitch_b
    const/16 v0, 0x58

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1666
    iget-object v0, p0, Llxq;->p:[I

    array-length v0, v0

    .line 1667
    add-int/2addr v2, v0

    new-array v2, v2, [I

    .line 1668
    iget-object v3, p0, Llxq;->p:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1669
    iput-object v2, p0, Llxq;->p:[I

    .line 1670
    :goto_7
    iget-object v2, p0, Llxq;->p:[I

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_a

    .line 1671
    iget-object v2, p0, Llxq;->p:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v3

    aput v3, v2, v0

    .line 1672
    invoke-virtual {p1}, Loxn;->a()I

    .line 1670
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 1675
    :cond_a
    iget-object v2, p0, Llxq;->p:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v3

    aput v3, v2, v0

    goto/16 :goto_0

    .line 1679
    :sswitch_c
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Llxq;->c:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 1683
    :sswitch_d
    const/16 v0, 0x68

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1684
    iget-object v0, p0, Llxq;->h:[I

    array-length v0, v0

    .line 1685
    add-int/2addr v2, v0

    new-array v2, v2, [I

    .line 1686
    iget-object v3, p0, Llxq;->h:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1687
    iput-object v2, p0, Llxq;->h:[I

    .line 1688
    :goto_8
    iget-object v2, p0, Llxq;->h:[I

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_b

    .line 1689
    iget-object v2, p0, Llxq;->h:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v3

    aput v3, v2, v0

    .line 1690
    invoke-virtual {p1}, Loxn;->a()I

    .line 1688
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 1693
    :cond_b
    iget-object v2, p0, Llxq;->h:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v3

    aput v3, v2, v0

    goto/16 :goto_0

    .line 1697
    :sswitch_e
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1698
    if-eqz v0, :cond_c

    if-ne v0, v6, :cond_d

    .line 1700
    :cond_c
    iput v0, p0, Llxq;->j:I

    goto/16 :goto_0

    .line 1702
    :cond_d
    iput v1, p0, Llxq;->j:I

    goto/16 :goto_0

    .line 1707
    :sswitch_f
    const/16 v0, 0x7a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1708
    iget-object v0, p0, Llxq;->q:[Ljava/lang/String;

    array-length v0, v0

    .line 1709
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 1710
    iget-object v3, p0, Llxq;->q:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1711
    iput-object v2, p0, Llxq;->q:[Ljava/lang/String;

    .line 1712
    :goto_9
    iget-object v2, p0, Llxq;->q:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_e

    .line 1713
    iget-object v2, p0, Llxq;->q:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 1714
    invoke-virtual {p1}, Loxn;->a()I

    .line 1712
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 1717
    :cond_e
    iget-object v2, p0, Llxq;->q:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    .line 1721
    :sswitch_10
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Llxq;->r:Ljava/lang/Long;

    goto/16 :goto_0

    .line 1725
    :sswitch_11
    const/16 v0, 0x8a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1726
    iget-object v0, p0, Llxq;->l:[Llwv;

    if-nez v0, :cond_10

    move v0, v1

    .line 1727
    :goto_a
    add-int/2addr v2, v0

    new-array v2, v2, [Llwv;

    .line 1728
    iget-object v3, p0, Llxq;->l:[Llwv;

    if-eqz v3, :cond_f

    .line 1729
    iget-object v3, p0, Llxq;->l:[Llwv;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1731
    :cond_f
    iput-object v2, p0, Llxq;->l:[Llwv;

    .line 1732
    :goto_b
    iget-object v2, p0, Llxq;->l:[Llwv;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_11

    .line 1733
    iget-object v2, p0, Llxq;->l:[Llwv;

    new-instance v3, Llwv;

    invoke-direct {v3}, Llwv;-><init>()V

    aput-object v3, v2, v0

    .line 1734
    iget-object v2, p0, Llxq;->l:[Llwv;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1735
    invoke-virtual {p1}, Loxn;->a()I

    .line 1732
    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    .line 1726
    :cond_10
    iget-object v0, p0, Llxq;->l:[Llwv;

    array-length v0, v0

    goto :goto_a

    .line 1738
    :cond_11
    iget-object v2, p0, Llxq;->l:[Llwv;

    new-instance v3, Llwv;

    invoke-direct {v3}, Llwv;-><init>()V

    aput-object v3, v2, v0

    .line 1739
    iget-object v2, p0, Llxq;->l:[Llwv;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1743
    :sswitch_12
    const/16 v0, 0x9a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1744
    iget-object v0, p0, Llxq;->g:[Ljava/lang/String;

    array-length v0, v0

    .line 1745
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 1746
    iget-object v3, p0, Llxq;->g:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1747
    iput-object v2, p0, Llxq;->g:[Ljava/lang/String;

    .line 1748
    :goto_c
    iget-object v2, p0, Llxq;->g:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_12

    .line 1749
    iget-object v2, p0, Llxq;->g:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 1750
    invoke-virtual {p1}, Loxn;->a()I

    .line 1748
    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    .line 1753
    :cond_12
    iget-object v2, p0, Llxq;->g:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    .line 1757
    :sswitch_13
    iget-object v0, p0, Llxq;->t:Llyc;

    if-nez v0, :cond_13

    .line 1758
    new-instance v0, Llyc;

    invoke-direct {v0}, Llyc;-><init>()V

    iput-object v0, p0, Llxq;->t:Llyc;

    .line 1760
    :cond_13
    iget-object v0, p0, Llxq;->t:Llyc;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1764
    :sswitch_14
    iget-object v0, p0, Llxq;->s:Llxt;

    if-nez v0, :cond_14

    .line 1765
    new-instance v0, Llxt;

    invoke-direct {v0}, Llxt;-><init>()V

    iput-object v0, p0, Llxq;->s:Llxt;

    .line 1767
    :cond_14
    iget-object v0, p0, Llxq;->s:Llxt;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1771
    :sswitch_15
    iget-object v0, p0, Llxq;->u:Llxe;

    if-nez v0, :cond_15

    .line 1772
    new-instance v0, Llxe;

    invoke-direct {v0}, Llxe;-><init>()V

    iput-object v0, p0, Llxq;->u:Llxe;

    .line 1774
    :cond_15
    iget-object v0, p0, Llxq;->u:Llxe;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1778
    :sswitch_16
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1779
    if-eqz v0, :cond_16

    if-eq v0, v6, :cond_16

    if-ne v0, v7, :cond_17

    .line 1782
    :cond_16
    iput v0, p0, Llxq;->v:I

    goto/16 :goto_0

    .line 1784
    :cond_17
    iput v1, p0, Llxq;->v:I

    goto/16 :goto_0

    .line 1543
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
        0x70 -> :sswitch_e
        0x7a -> :sswitch_f
        0x80 -> :sswitch_10
        0x8a -> :sswitch_11
        0x9a -> :sswitch_12
        0xa2 -> :sswitch_13
        0xaa -> :sswitch_14
        0xb2 -> :sswitch_15
        0xb8 -> :sswitch_16
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 9

    .prologue
    const/high16 v8, -0x80000000

    const/4 v0, 0x0

    .line 1293
    iget-object v1, p0, Llxq;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 1294
    const/4 v1, 0x1

    iget-object v2, p0, Llxq;->d:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IZ)V

    .line 1296
    :cond_0
    iget-object v1, p0, Llxq;->m:[Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 1297
    iget-object v2, p0, Llxq;->m:[Ljava/lang/Long;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 1298
    const/4 v5, 0x2

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {p1, v5, v6, v7}, Loxo;->b(IJ)V

    .line 1297
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1301
    :cond_1
    iget-object v1, p0, Llxq;->n:[Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 1302
    iget-object v2, p0, Llxq;->n:[Ljava/lang/Long;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 1303
    const/4 v5, 0x3

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {p1, v5, v6, v7}, Loxo;->b(IJ)V

    .line 1302
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1306
    :cond_2
    iget-object v1, p0, Llxq;->a:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 1307
    const/4 v1, 0x4

    iget-object v2, p0, Llxq;->a:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 1309
    :cond_3
    iget v1, p0, Llxq;->b:I

    if-eq v1, v8, :cond_4

    .line 1310
    const/4 v1, 0x5

    iget v2, p0, Llxq;->b:I

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 1312
    :cond_4
    iget-object v1, p0, Llxq;->i:[Ljava/lang/Long;

    if-eqz v1, :cond_5

    .line 1313
    iget-object v2, p0, Llxq;->i:[Ljava/lang/Long;

    array-length v3, v2

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    .line 1314
    const/4 v5, 0x6

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {p1, v5, v6, v7}, Loxo;->b(IJ)V

    .line 1313
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1317
    :cond_5
    iget-object v1, p0, Llxq;->o:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    .line 1318
    const/4 v1, 0x7

    iget-object v2, p0, Llxq;->o:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IZ)V

    .line 1320
    :cond_6
    iget-object v1, p0, Llxq;->e:[Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 1321
    iget-object v2, p0, Llxq;->e:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_3
    if-ge v1, v3, :cond_7

    aget-object v4, v2, v1

    .line 1322
    const/16 v5, 0x8

    invoke-virtual {p1, v5, v4}, Loxo;->a(ILjava/lang/String;)V

    .line 1321
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 1325
    :cond_7
    iget-object v1, p0, Llxq;->f:[Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 1326
    iget-object v2, p0, Llxq;->f:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_4
    if-ge v1, v3, :cond_8

    aget-object v4, v2, v1

    .line 1327
    const/16 v5, 0x9

    invoke-virtual {p1, v5, v4}, Loxo;->a(ILjava/lang/String;)V

    .line 1326
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 1330
    :cond_8
    iget-object v1, p0, Llxq;->k:[I

    if-eqz v1, :cond_9

    iget-object v1, p0, Llxq;->k:[I

    array-length v1, v1

    if-lez v1, :cond_9

    .line 1331
    iget-object v2, p0, Llxq;->k:[I

    array-length v3, v2

    move v1, v0

    :goto_5
    if-ge v1, v3, :cond_9

    aget v4, v2, v1

    .line 1332
    const/16 v5, 0xa

    invoke-virtual {p1, v5, v4}, Loxo;->a(II)V

    .line 1331
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 1335
    :cond_9
    iget-object v1, p0, Llxq;->p:[I

    if-eqz v1, :cond_a

    iget-object v1, p0, Llxq;->p:[I

    array-length v1, v1

    if-lez v1, :cond_a

    .line 1336
    iget-object v2, p0, Llxq;->p:[I

    array-length v3, v2

    move v1, v0

    :goto_6
    if-ge v1, v3, :cond_a

    aget v4, v2, v1

    .line 1337
    const/16 v5, 0xb

    invoke-virtual {p1, v5, v4}, Loxo;->a(II)V

    .line 1336
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 1340
    :cond_a
    iget-object v1, p0, Llxq;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    .line 1341
    const/16 v1, 0xc

    iget-object v2, p0, Llxq;->c:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IZ)V

    .line 1343
    :cond_b
    iget-object v1, p0, Llxq;->h:[I

    if-eqz v1, :cond_c

    iget-object v1, p0, Llxq;->h:[I

    array-length v1, v1

    if-lez v1, :cond_c

    .line 1344
    iget-object v2, p0, Llxq;->h:[I

    array-length v3, v2

    move v1, v0

    :goto_7
    if-ge v1, v3, :cond_c

    aget v4, v2, v1

    .line 1345
    const/16 v5, 0xd

    invoke-virtual {p1, v5, v4}, Loxo;->a(II)V

    .line 1344
    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    .line 1348
    :cond_c
    iget v1, p0, Llxq;->j:I

    if-eq v1, v8, :cond_d

    .line 1349
    const/16 v1, 0xe

    iget v2, p0, Llxq;->j:I

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 1351
    :cond_d
    iget-object v1, p0, Llxq;->q:[Ljava/lang/String;

    if-eqz v1, :cond_e

    .line 1352
    iget-object v2, p0, Llxq;->q:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_8
    if-ge v1, v3, :cond_e

    aget-object v4, v2, v1

    .line 1353
    const/16 v5, 0xf

    invoke-virtual {p1, v5, v4}, Loxo;->a(ILjava/lang/String;)V

    .line 1352
    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    .line 1356
    :cond_e
    iget-object v1, p0, Llxq;->r:Ljava/lang/Long;

    if-eqz v1, :cond_f

    .line 1357
    const/16 v1, 0x10

    iget-object v2, p0, Llxq;->r:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Loxo;->b(IJ)V

    .line 1359
    :cond_f
    iget-object v1, p0, Llxq;->l:[Llwv;

    if-eqz v1, :cond_11

    .line 1360
    iget-object v2, p0, Llxq;->l:[Llwv;

    array-length v3, v2

    move v1, v0

    :goto_9
    if-ge v1, v3, :cond_11

    aget-object v4, v2, v1

    .line 1361
    if-eqz v4, :cond_10

    .line 1362
    const/16 v5, 0x11

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 1360
    :cond_10
    add-int/lit8 v1, v1, 0x1

    goto :goto_9

    .line 1366
    :cond_11
    iget-object v1, p0, Llxq;->g:[Ljava/lang/String;

    if-eqz v1, :cond_12

    .line 1367
    iget-object v1, p0, Llxq;->g:[Ljava/lang/String;

    array-length v2, v1

    :goto_a
    if-ge v0, v2, :cond_12

    aget-object v3, v1, v0

    .line 1368
    const/16 v4, 0x13

    invoke-virtual {p1, v4, v3}, Loxo;->a(ILjava/lang/String;)V

    .line 1367
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 1371
    :cond_12
    iget-object v0, p0, Llxq;->t:Llyc;

    if-eqz v0, :cond_13

    .line 1372
    const/16 v0, 0x14

    iget-object v1, p0, Llxq;->t:Llyc;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1374
    :cond_13
    iget-object v0, p0, Llxq;->s:Llxt;

    if-eqz v0, :cond_14

    .line 1375
    const/16 v0, 0x15

    iget-object v1, p0, Llxq;->s:Llxt;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1377
    :cond_14
    iget-object v0, p0, Llxq;->u:Llxe;

    if-eqz v0, :cond_15

    .line 1378
    const/16 v0, 0x16

    iget-object v1, p0, Llxq;->u:Llxe;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1380
    :cond_15
    iget v0, p0, Llxq;->v:I

    if-eq v0, v8, :cond_16

    .line 1381
    const/16 v0, 0x17

    iget v1, p0, Llxq;->v:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1383
    :cond_16
    iget-object v0, p0, Llxq;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1385
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1193
    invoke-virtual {p0, p1}, Llxq;->a(Loxn;)Llxq;

    move-result-object v0

    return-object v0
.end method
