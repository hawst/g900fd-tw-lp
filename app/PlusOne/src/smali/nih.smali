.class public final Lnih;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnhz;

.field public b:Lnhz;

.field public c:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1729
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1732
    iput-object v0, p0, Lnih;->a:Lnhz;

    .line 1735
    iput-object v0, p0, Lnih;->b:Lnhz;

    .line 1729
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1757
    const/4 v0, 0x0

    .line 1758
    iget-object v1, p0, Lnih;->a:Lnhz;

    if-eqz v1, :cond_0

    .line 1759
    const/4 v0, 0x1

    iget-object v1, p0, Lnih;->a:Lnhz;

    .line 1760
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1762
    :cond_0
    iget-object v1, p0, Lnih;->b:Lnhz;

    if-eqz v1, :cond_1

    .line 1763
    const/4 v1, 0x2

    iget-object v2, p0, Lnih;->b:Lnhz;

    .line 1764
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1766
    :cond_1
    iget-object v1, p0, Lnih;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 1767
    const/4 v1, 0x3

    iget-object v2, p0, Lnih;->c:Ljava/lang/Boolean;

    .line 1768
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1770
    :cond_2
    iget-object v1, p0, Lnih;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1771
    iput v0, p0, Lnih;->ai:I

    .line 1772
    return v0
.end method

.method public a(Loxn;)Lnih;
    .locals 2

    .prologue
    .line 1780
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1781
    sparse-switch v0, :sswitch_data_0

    .line 1785
    iget-object v1, p0, Lnih;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1786
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnih;->ah:Ljava/util/List;

    .line 1789
    :cond_1
    iget-object v1, p0, Lnih;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1791
    :sswitch_0
    return-object p0

    .line 1796
    :sswitch_1
    iget-object v0, p0, Lnih;->a:Lnhz;

    if-nez v0, :cond_2

    .line 1797
    new-instance v0, Lnhz;

    invoke-direct {v0}, Lnhz;-><init>()V

    iput-object v0, p0, Lnih;->a:Lnhz;

    .line 1799
    :cond_2
    iget-object v0, p0, Lnih;->a:Lnhz;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1803
    :sswitch_2
    iget-object v0, p0, Lnih;->b:Lnhz;

    if-nez v0, :cond_3

    .line 1804
    new-instance v0, Lnhz;

    invoke-direct {v0}, Lnhz;-><init>()V

    iput-object v0, p0, Lnih;->b:Lnhz;

    .line 1806
    :cond_3
    iget-object v0, p0, Lnih;->b:Lnhz;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1810
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnih;->c:Ljava/lang/Boolean;

    goto :goto_0

    .line 1781
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1742
    iget-object v0, p0, Lnih;->a:Lnhz;

    if-eqz v0, :cond_0

    .line 1743
    const/4 v0, 0x1

    iget-object v1, p0, Lnih;->a:Lnhz;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1745
    :cond_0
    iget-object v0, p0, Lnih;->b:Lnhz;

    if-eqz v0, :cond_1

    .line 1746
    const/4 v0, 0x2

    iget-object v1, p0, Lnih;->b:Lnhz;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1748
    :cond_1
    iget-object v0, p0, Lnih;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 1749
    const/4 v0, 0x3

    iget-object v1, p0, Lnih;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 1751
    :cond_2
    iget-object v0, p0, Lnih;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1753
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1725
    invoke-virtual {p0, p1}, Lnih;->a(Loxn;)Lnih;

    move-result-object v0

    return-object v0
.end method
