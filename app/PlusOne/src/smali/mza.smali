.class public final Lmza;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmzb;

.field private b:Lmzc;

.field private c:Lmzd;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 245
    invoke-direct {p0}, Loxq;-><init>()V

    .line 446
    iput-object v0, p0, Lmza;->a:Lmzb;

    .line 449
    iput-object v0, p0, Lmza;->b:Lmzc;

    .line 452
    iput-object v0, p0, Lmza;->c:Lmzd;

    .line 245
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 472
    const/4 v0, 0x0

    .line 473
    iget-object v1, p0, Lmza;->a:Lmzb;

    if-eqz v1, :cond_0

    .line 474
    const/4 v0, 0x1

    iget-object v1, p0, Lmza;->a:Lmzb;

    .line 475
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 477
    :cond_0
    iget-object v1, p0, Lmza;->b:Lmzc;

    if-eqz v1, :cond_1

    .line 478
    const/4 v1, 0x2

    iget-object v2, p0, Lmza;->b:Lmzc;

    .line 479
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 481
    :cond_1
    iget-object v1, p0, Lmza;->c:Lmzd;

    if-eqz v1, :cond_2

    .line 482
    const/4 v1, 0x3

    iget-object v2, p0, Lmza;->c:Lmzd;

    .line 483
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 485
    :cond_2
    iget-object v1, p0, Lmza;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 486
    iput v0, p0, Lmza;->ai:I

    .line 487
    return v0
.end method

.method public a(Loxn;)Lmza;
    .locals 2

    .prologue
    .line 495
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 496
    sparse-switch v0, :sswitch_data_0

    .line 500
    iget-object v1, p0, Lmza;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 501
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmza;->ah:Ljava/util/List;

    .line 504
    :cond_1
    iget-object v1, p0, Lmza;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 506
    :sswitch_0
    return-object p0

    .line 511
    :sswitch_1
    iget-object v0, p0, Lmza;->a:Lmzb;

    if-nez v0, :cond_2

    .line 512
    new-instance v0, Lmzb;

    invoke-direct {v0}, Lmzb;-><init>()V

    iput-object v0, p0, Lmza;->a:Lmzb;

    .line 514
    :cond_2
    iget-object v0, p0, Lmza;->a:Lmzb;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 518
    :sswitch_2
    iget-object v0, p0, Lmza;->b:Lmzc;

    if-nez v0, :cond_3

    .line 519
    new-instance v0, Lmzc;

    invoke-direct {v0}, Lmzc;-><init>()V

    iput-object v0, p0, Lmza;->b:Lmzc;

    .line 521
    :cond_3
    iget-object v0, p0, Lmza;->b:Lmzc;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 525
    :sswitch_3
    iget-object v0, p0, Lmza;->c:Lmzd;

    if-nez v0, :cond_4

    .line 526
    new-instance v0, Lmzd;

    invoke-direct {v0}, Lmzd;-><init>()V

    iput-object v0, p0, Lmza;->c:Lmzd;

    .line 528
    :cond_4
    iget-object v0, p0, Lmza;->c:Lmzd;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 496
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 457
    iget-object v0, p0, Lmza;->a:Lmzb;

    if-eqz v0, :cond_0

    .line 458
    const/4 v0, 0x1

    iget-object v1, p0, Lmza;->a:Lmzb;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 460
    :cond_0
    iget-object v0, p0, Lmza;->b:Lmzc;

    if-eqz v0, :cond_1

    .line 461
    const/4 v0, 0x2

    iget-object v1, p0, Lmza;->b:Lmzc;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 463
    :cond_1
    iget-object v0, p0, Lmza;->c:Lmzd;

    if-eqz v0, :cond_2

    .line 464
    const/4 v0, 0x3

    iget-object v1, p0, Lmza;->c:Lmzd;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 466
    :cond_2
    iget-object v0, p0, Lmza;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 468
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 241
    invoke-virtual {p0, p1}, Lmza;->a(Loxn;)Lmza;

    move-result-object v0

    return-object v0
.end method
