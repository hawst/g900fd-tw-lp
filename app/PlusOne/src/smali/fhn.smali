.class public final Lfhn;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lleu;


# instance fields
.field private final a:Lhei;

.field private final b:Lieh;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Lfhn;->a:Lhei;

    .line 38
    const-class v0, Lieh;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    iput-object v0, p0, Lfhn;->b:Lieh;

    .line 39
    return-void
.end method


# virtual methods
.method public a()Llev;
    .locals 4

    .prologue
    .line 42
    new-instance v0, Ller;

    invoke-direct {v0}, Ller;-><init>()V

    const-string v1, "experiments"

    .line 43
    invoke-virtual {v0, v1}, Ller;->a(Ljava/lang/String;)Ller;

    move-result-object v0

    const/16 v1, 0x23

    .line 44
    invoke-virtual {v0, v1}, Ller;->a(I)Ller;

    move-result-object v0

    const/16 v1, 0x24

    .line 45
    invoke-virtual {v0, v1}, Ller;->b(I)Ller;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x4

    .line 46
    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ller;->a(J)Ller;

    move-result-object v0

    .line 47
    invoke-virtual {v0}, Ller;->b()Llev;

    move-result-object v0

    return-object v0
.end method

.method public a(Lkfp;ILles;)V
    .locals 2

    .prologue
    .line 52
    iget-object v0, p0, Lfhn;->a:Lhei;

    invoke-interface {v0, p2}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 53
    invoke-virtual {p1}, Lkfp;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 62
    :goto_0
    return-void

    .line 57
    :cond_0
    :try_start_0
    const-string v1, "Sync experiments"

    invoke-virtual {p1, v1}, Lkfp;->c(Ljava/lang/String;)V

    .line 59
    iget-object v1, p0, Lfhn;->b:Lieh;

    invoke-interface {v1, v0}, Lieh;->a(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 61
    invoke-virtual {p1}, Lkfp;->f()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Lkfp;->f()V

    throw v0
.end method
