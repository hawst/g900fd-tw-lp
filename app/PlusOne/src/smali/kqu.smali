.class public final Lkqu;
.super Lhmk;
.source "PG"


# instance fields
.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lhmn;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lhmk;-><init>(Lhmn;)V

    .line 18
    const-string v0, "f."

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 19
    const/4 v0, 0x2

    invoke-virtual {p2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p2

    .line 21
    :cond_0
    iput-object p2, p0, Lkqu;->b:Ljava/lang/String;

    .line 22
    return-void
.end method


# virtual methods
.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lkqu;->b:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 30
    invoke-super {p0, p1}, Lhmk;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 31
    check-cast p1, Lkqu;

    .line 32
    iget-object v0, p0, Lkqu;->b:Ljava/lang/String;

    iget-object v1, p1, Lkqu;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Llsh;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    .line 34
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 39
    iget-object v0, p0, Lkqu;->b:Ljava/lang/String;

    invoke-super {p0}, Lhmk;->hashCode()I

    move-result v1

    invoke-static {v0, v1}, Llsh;->a(Ljava/lang/Object;I)I

    move-result v0

    return v0
.end method
