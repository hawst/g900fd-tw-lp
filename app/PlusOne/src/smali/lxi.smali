.class public final Llxi;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Llxi;


# instance fields
.field private b:Llxp;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3508
    const/4 v0, 0x0

    new-array v0, v0, [Llxi;

    sput-object v0, Llxi;->a:[Llxi;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3509
    invoke-direct {p0}, Loxq;-><init>()V

    .line 3512
    const/4 v0, 0x0

    iput-object v0, p0, Llxi;->b:Llxp;

    .line 3509
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 3536
    const/4 v0, 0x0

    .line 3537
    iget-object v1, p0, Llxi;->b:Llxp;

    if-eqz v1, :cond_0

    .line 3538
    const/4 v0, 0x1

    iget-object v1, p0, Llxi;->b:Llxp;

    .line 3539
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3541
    :cond_0
    iget-object v1, p0, Llxi;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 3542
    const/4 v1, 0x2

    iget-object v2, p0, Llxi;->c:Ljava/lang/String;

    .line 3543
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3545
    :cond_1
    iget-object v1, p0, Llxi;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 3546
    const/4 v1, 0x3

    iget-object v2, p0, Llxi;->d:Ljava/lang/Boolean;

    .line 3547
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3549
    :cond_2
    iget-object v1, p0, Llxi;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3550
    iput v0, p0, Llxi;->ai:I

    .line 3551
    return v0
.end method

.method public a(Loxn;)Llxi;
    .locals 2

    .prologue
    .line 3559
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3560
    sparse-switch v0, :sswitch_data_0

    .line 3564
    iget-object v1, p0, Llxi;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 3565
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llxi;->ah:Ljava/util/List;

    .line 3568
    :cond_1
    iget-object v1, p0, Llxi;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3570
    :sswitch_0
    return-object p0

    .line 3575
    :sswitch_1
    iget-object v0, p0, Llxi;->b:Llxp;

    if-nez v0, :cond_2

    .line 3576
    new-instance v0, Llxp;

    invoke-direct {v0}, Llxp;-><init>()V

    iput-object v0, p0, Llxi;->b:Llxp;

    .line 3578
    :cond_2
    iget-object v0, p0, Llxi;->b:Llxp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3582
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llxi;->c:Ljava/lang/String;

    goto :goto_0

    .line 3586
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Llxi;->d:Ljava/lang/Boolean;

    goto :goto_0

    .line 3560
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 3521
    iget-object v0, p0, Llxi;->b:Llxp;

    if-eqz v0, :cond_0

    .line 3522
    const/4 v0, 0x1

    iget-object v1, p0, Llxi;->b:Llxp;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 3524
    :cond_0
    iget-object v0, p0, Llxi;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 3525
    const/4 v0, 0x2

    iget-object v1, p0, Llxi;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3527
    :cond_1
    iget-object v0, p0, Llxi;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 3528
    const/4 v0, 0x3

    iget-object v1, p0, Llxi;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 3530
    :cond_2
    iget-object v0, p0, Llxi;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3532
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3505
    invoke-virtual {p0, p1}, Llxi;->a(Loxn;)Llxi;

    move-result-object v0

    return-object v0
.end method
