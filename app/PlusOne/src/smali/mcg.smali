.class public final Lmcg;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnef;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 25351
    invoke-direct {p0}, Loxq;-><init>()V

    .line 25354
    iput-object v0, p0, Lmcg;->apiHeader:Llyq;

    .line 25357
    iput-object v0, p0, Lmcg;->a:Lnef;

    .line 25351
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 25374
    const/4 v0, 0x0

    .line 25375
    iget-object v1, p0, Lmcg;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 25376
    const/4 v0, 0x1

    iget-object v1, p0, Lmcg;->apiHeader:Llyq;

    .line 25377
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 25379
    :cond_0
    iget-object v1, p0, Lmcg;->a:Lnef;

    if-eqz v1, :cond_1

    .line 25380
    const/4 v1, 0x2

    iget-object v2, p0, Lmcg;->a:Lnef;

    .line 25381
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 25383
    :cond_1
    iget-object v1, p0, Lmcg;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 25384
    iput v0, p0, Lmcg;->ai:I

    .line 25385
    return v0
.end method

.method public a(Loxn;)Lmcg;
    .locals 2

    .prologue
    .line 25393
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 25394
    sparse-switch v0, :sswitch_data_0

    .line 25398
    iget-object v1, p0, Lmcg;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 25399
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmcg;->ah:Ljava/util/List;

    .line 25402
    :cond_1
    iget-object v1, p0, Lmcg;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 25404
    :sswitch_0
    return-object p0

    .line 25409
    :sswitch_1
    iget-object v0, p0, Lmcg;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 25410
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmcg;->apiHeader:Llyq;

    .line 25412
    :cond_2
    iget-object v0, p0, Lmcg;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 25416
    :sswitch_2
    iget-object v0, p0, Lmcg;->a:Lnef;

    if-nez v0, :cond_3

    .line 25417
    new-instance v0, Lnef;

    invoke-direct {v0}, Lnef;-><init>()V

    iput-object v0, p0, Lmcg;->a:Lnef;

    .line 25419
    :cond_3
    iget-object v0, p0, Lmcg;->a:Lnef;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 25394
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 25362
    iget-object v0, p0, Lmcg;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 25363
    const/4 v0, 0x1

    iget-object v1, p0, Lmcg;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 25365
    :cond_0
    iget-object v0, p0, Lmcg;->a:Lnef;

    if-eqz v0, :cond_1

    .line 25366
    const/4 v0, 0x2

    iget-object v1, p0, Lmcg;->a:Lnef;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 25368
    :cond_1
    iget-object v0, p0, Lmcg;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 25370
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 25347
    invoke-virtual {p0, p1}, Lmcg;->a(Loxn;)Lmcg;

    move-result-object v0

    return-object v0
.end method
