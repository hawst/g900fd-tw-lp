.class final Lhrq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lheq;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    const-string v0, "com.google.android.libraries.social.autobackup.VideoReuploader-MigrateToAccountSpecificUploadWindow"

    return-object v0
.end method

.method public a(Landroid/content/Context;Lhem;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 118
    invoke-static {p1}, Lcom/google/android/libraries/social/autobackup/AutoBackupProvider;->c(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 119
    const-string v1, "video_upload_redo_window:start_time_seconds"

    invoke-interface {v0, v1, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 120
    const-string v1, "video_upload_redo_window:end_time_seconds"

    invoke-interface {v0, v1, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 121
    const-string v4, "com.google.android.libraries.social.autobackup.VideoReuploader"

    invoke-interface {p2, v4}, Lhem;->h(Ljava/lang/String;)Lhem;

    move-result-object v4

    .line 122
    const-string v5, "window_start_time_seconds"

    invoke-interface {v4, v5, v2, v3}, Lhem;->c(Ljava/lang/String;J)Lhem;

    .line 123
    const-string v2, "window_end_time_seconds"

    invoke-interface {v4, v2, v0, v1}, Lhem;->c(Ljava/lang/String;J)Lhem;

    .line 124
    return-void
.end method
