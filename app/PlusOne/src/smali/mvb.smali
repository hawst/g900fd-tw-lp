.class public final Lmvb;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Lmvc;

.field private d:Lmvd;

.field private e:Lmva;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 12260
    invoke-direct {p0}, Loxq;-><init>()V

    .line 12267
    iput-object v0, p0, Lmvb;->c:Lmvc;

    .line 12270
    iput-object v0, p0, Lmvb;->d:Lmvd;

    .line 12273
    iput-object v0, p0, Lmvb;->e:Lmva;

    .line 12260
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 12299
    const/4 v0, 0x0

    .line 12300
    iget-object v1, p0, Lmvb;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 12301
    const/4 v0, 0x1

    iget-object v1, p0, Lmvb;->a:Ljava/lang/String;

    .line 12302
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 12304
    :cond_0
    iget-object v1, p0, Lmvb;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 12305
    const/4 v1, 0x2

    iget-object v2, p0, Lmvb;->b:Ljava/lang/String;

    .line 12306
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12308
    :cond_1
    iget-object v1, p0, Lmvb;->c:Lmvc;

    if-eqz v1, :cond_2

    .line 12309
    const/4 v1, 0x3

    iget-object v2, p0, Lmvb;->c:Lmvc;

    .line 12310
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12312
    :cond_2
    iget-object v1, p0, Lmvb;->d:Lmvd;

    if-eqz v1, :cond_3

    .line 12313
    const/4 v1, 0x4

    iget-object v2, p0, Lmvb;->d:Lmvd;

    .line 12314
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12316
    :cond_3
    iget-object v1, p0, Lmvb;->e:Lmva;

    if-eqz v1, :cond_4

    .line 12317
    const/4 v1, 0x5

    iget-object v2, p0, Lmvb;->e:Lmva;

    .line 12318
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12320
    :cond_4
    iget-object v1, p0, Lmvb;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12321
    iput v0, p0, Lmvb;->ai:I

    .line 12322
    return v0
.end method

.method public a(Loxn;)Lmvb;
    .locals 2

    .prologue
    .line 12330
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 12331
    sparse-switch v0, :sswitch_data_0

    .line 12335
    iget-object v1, p0, Lmvb;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 12336
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmvb;->ah:Ljava/util/List;

    .line 12339
    :cond_1
    iget-object v1, p0, Lmvb;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 12341
    :sswitch_0
    return-object p0

    .line 12346
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmvb;->a:Ljava/lang/String;

    goto :goto_0

    .line 12350
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmvb;->b:Ljava/lang/String;

    goto :goto_0

    .line 12354
    :sswitch_3
    iget-object v0, p0, Lmvb;->c:Lmvc;

    if-nez v0, :cond_2

    .line 12355
    new-instance v0, Lmvc;

    invoke-direct {v0}, Lmvc;-><init>()V

    iput-object v0, p0, Lmvb;->c:Lmvc;

    .line 12357
    :cond_2
    iget-object v0, p0, Lmvb;->c:Lmvc;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 12361
    :sswitch_4
    iget-object v0, p0, Lmvb;->d:Lmvd;

    if-nez v0, :cond_3

    .line 12362
    new-instance v0, Lmvd;

    invoke-direct {v0}, Lmvd;-><init>()V

    iput-object v0, p0, Lmvb;->d:Lmvd;

    .line 12364
    :cond_3
    iget-object v0, p0, Lmvb;->d:Lmvd;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 12368
    :sswitch_5
    iget-object v0, p0, Lmvb;->e:Lmva;

    if-nez v0, :cond_4

    .line 12369
    new-instance v0, Lmva;

    invoke-direct {v0}, Lmva;-><init>()V

    iput-object v0, p0, Lmvb;->e:Lmva;

    .line 12371
    :cond_4
    iget-object v0, p0, Lmvb;->e:Lmva;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 12331
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 12278
    iget-object v0, p0, Lmvb;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 12279
    const/4 v0, 0x1

    iget-object v1, p0, Lmvb;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 12281
    :cond_0
    iget-object v0, p0, Lmvb;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 12282
    const/4 v0, 0x2

    iget-object v1, p0, Lmvb;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 12284
    :cond_1
    iget-object v0, p0, Lmvb;->c:Lmvc;

    if-eqz v0, :cond_2

    .line 12285
    const/4 v0, 0x3

    iget-object v1, p0, Lmvb;->c:Lmvc;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 12287
    :cond_2
    iget-object v0, p0, Lmvb;->d:Lmvd;

    if-eqz v0, :cond_3

    .line 12288
    const/4 v0, 0x4

    iget-object v1, p0, Lmvb;->d:Lmvd;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 12290
    :cond_3
    iget-object v0, p0, Lmvb;->e:Lmva;

    if-eqz v0, :cond_4

    .line 12291
    const/4 v0, 0x5

    iget-object v1, p0, Lmvb;->e:Lmva;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 12293
    :cond_4
    iget-object v0, p0, Lmvb;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 12295
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 12256
    invoke-virtual {p0, p1}, Lmvb;->a(Loxn;)Lmvb;

    move-result-object v0

    return-object v0
.end method
