.class public final Ldmy;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmiy;",
        "Lmiz;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:I

.field private final p:Ljava/lang/String;

.field private final q:Ljava/lang/String;

.field private final r:Lkzl;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 43
    const-string v3, "searchquery"

    new-instance v4, Lmiy;

    invoke-direct {v4}, Lmiy;-><init>()V

    new-instance v5, Lmiz;

    invoke-direct {v5}, Lmiz;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 44
    iget-object v0, p0, Ldmy;->f:Landroid/content/Context;

    const-class v1, Lkzl;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkzl;

    iput-object v0, p0, Ldmy;->r:Lkzl;

    .line 45
    iput-object p3, p0, Ldmy;->a:Ljava/lang/String;

    .line 46
    iput-object p5, p0, Ldmy;->p:Ljava/lang/String;

    .line 47
    iput p4, p0, Ldmy;->b:I

    .line 48
    iput-object p6, p0, Ldmy;->q:Ljava/lang/String;

    .line 49
    return-void
.end method


# virtual methods
.method protected a(Lmiy;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 53
    new-instance v0, Lnnq;

    invoke-direct {v0}, Lnnq;-><init>()V

    iput-object v0, p1, Lmiy;->a:Lnnq;

    .line 54
    iget-object v0, p1, Lmiy;->a:Lnnq;

    .line 55
    new-instance v1, Loej;

    invoke-direct {v1}, Loej;-><init>()V

    iput-object v1, v0, Lnnq;->a:Loej;

    .line 56
    iget-object v1, v0, Lnnq;->a:Loej;

    iget-object v2, p0, Ldmy;->a:Ljava/lang/String;

    iput-object v2, v1, Loej;->a:Ljava/lang/String;

    .line 57
    iget v1, p0, Ldmy;->b:I

    packed-switch v1, :pswitch_data_0

    .line 67
    :goto_0
    iget-object v1, v0, Lnnq;->a:Loej;

    const/4 v2, 0x3

    iput v2, v1, Loej;->b:I

    .line 69
    iget-object v1, p0, Ldmy;->q:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 70
    iget-object v1, v0, Lnnq;->a:Loej;

    new-instance v2, Loeg;

    invoke-direct {v2}, Loeg;-><init>()V

    iput-object v2, v1, Loej;->d:Loeg;

    .line 71
    iget-object v1, v0, Lnnq;->a:Loej;

    iget-object v1, v1, Loej;->d:Loeg;

    iget-object v2, p0, Ldmy;->q:Ljava/lang/String;

    iput-object v2, v1, Loeg;->a:Ljava/lang/String;

    .line 74
    iget-object v1, v0, Lnnq;->a:Loej;

    iget-object v1, v1, Loej;->d:Loeg;

    const-string v2, ""

    iput-object v2, v1, Loeg;->b:Ljava/lang/String;

    .line 77
    :cond_0
    new-instance v1, Lnmt;

    invoke-direct {v1}, Lnmt;-><init>()V

    iput-object v1, v0, Lnnq;->c:Lnmt;

    .line 78
    iget-object v1, p0, Ldmy;->p:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 79
    iget-object v1, v0, Lnnq;->c:Lnmt;

    iget-object v2, p0, Ldmy;->p:Ljava/lang/String;

    iput-object v2, v1, Lnmt;->a:Ljava/lang/String;

    .line 82
    :cond_1
    iget-object v1, v0, Lnnq;->c:Lnmt;

    new-instance v2, Lnmr;

    invoke-direct {v2}, Lnmr;-><init>()V

    iput-object v2, v1, Lnmt;->b:Lnmr;

    .line 83
    iget-object v1, v0, Lnnq;->c:Lnmt;

    iget-object v1, v1, Lnmt;->b:Lnmr;

    new-instance v2, Lofl;

    invoke-direct {v2}, Lofl;-><init>()V

    iput-object v2, v1, Lnmr;->a:Lofl;

    .line 84
    iget-object v1, v0, Lnnq;->c:Lnmt;

    iget-object v1, v1, Lnmt;->b:Lnmr;

    iget-object v1, v1, Lnmr;->a:Lofl;

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v2, v1, Lofl;->e:Ljava/lang/Boolean;

    .line 86
    iget-object v1, v0, Lnnq;->c:Lnmt;

    iget-object v1, v1, Lnmt;->b:Lnmr;

    iget-object v1, v1, Lnmr;->a:Lofl;

    sget-object v2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    iput-object v2, v1, Lofl;->b:Ljava/lang/Boolean;

    .line 88
    iget-object v1, v0, Lnnq;->c:Lnmt;

    iget-object v1, v1, Lnmt;->b:Lnmr;

    iget-object v1, v1, Lnmr;->a:Lofl;

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v2, v1, Lofl;->c:Ljava/lang/Boolean;

    .line 90
    iget-object v1, v0, Lnnq;->c:Lnmt;

    iget-object v1, v1, Lnmt;->b:Lnmr;

    new-instance v2, Logu;

    invoke-direct {v2}, Logu;-><init>()V

    iput-object v2, v1, Lnmr;->b:Logu;

    .line 91
    iget-object v1, v0, Lnnq;->c:Lnmt;

    iget-object v1, v1, Lnmt;->b:Lnmr;

    iget-object v1, v1, Lnmr;->b:Logu;

    iget-object v2, p0, Ldmy;->r:Lkzl;

    const/4 v3, 0x0

    iget v4, p0, Ldmy;->c:I

    .line 92
    invoke-interface {v2, v3}, Lkzl;->a(Z)[I

    move-result-object v2

    iput-object v2, v1, Logu;->a:[I

    .line 93
    iget-object v1, v0, Lnnq;->c:Lnmt;

    iget-object v1, v1, Lnmt;->b:Lnmr;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lnmr;->c:Ljava/lang/Boolean;

    .line 95
    new-instance v1, Loxz;

    invoke-direct {v1}, Loxz;-><init>()V

    iput-object v1, v0, Lnnq;->e:Loxz;

    .line 96
    iget-object v1, v0, Lnnq;->e:Loxz;

    iget-object v2, p0, Ldmy;->r:Lkzl;

    iget-object v3, p0, Ldmy;->f:Landroid/content/Context;

    iget v4, p0, Ldmy;->c:I

    invoke-interface {v2, v3, v4}, Lkzl;->a(Landroid/content/Context;I)[I

    move-result-object v2

    iput-object v2, v1, Loxz;->a:[I

    .line 97
    iput v6, v0, Lnnq;->d:I

    .line 98
    return-void

    .line 59
    :pswitch_0
    iget-object v1, v0, Lnnq;->a:Loej;

    iput v5, v1, Loej;->c:I

    goto/16 :goto_0

    .line 62
    :pswitch_1
    iget-object v1, v0, Lnnq;->a:Loej;

    iput v6, v1, Loej;->c:I

    goto/16 :goto_0

    .line 57
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected a(Lmiz;)V
    .locals 10

    .prologue
    const/4 v7, 0x0

    .line 102
    iget-object v9, p1, Lmiz;->a:Lnnu;

    .line 103
    iget v1, p0, Ldmy;->c:I

    .line 104
    iget-object v0, v9, Lnnu;->a:Lnna;

    if-eqz v0, :cond_0

    iget-object v0, v9, Lnnu;->a:Lnna;

    iget-object v0, v0, Lnna;->b:Lnmu;

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Ldmy;->f:Landroid/content/Context;

    iget-object v2, p0, Ldmy;->a:Ljava/lang/String;

    iget v3, p0, Ldmy;->b:I

    .line 106
    invoke-static {v2, v3}, Lfvd;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, v9, Lnnu;->a:Lnna;

    iget-object v3, v3, Lnna;->b:Lnmu;

    iget-object v3, v3, Lnmu;->a:Logi;

    iget-object v3, v3, Logi;->a:[Logr;

    const/4 v4, 0x1

    iget-object v5, p0, Ldmy;->p:Ljava/lang/String;

    iget-object v6, v9, Lnnu;->a:Lnna;

    iget-object v6, v6, Lnna;->b:Lnmu;

    iget-object v6, v6, Lnmu;->b:Ljava/lang/String;

    const/4 v8, 0x0

    .line 105
    invoke-static/range {v0 .. v8}, Llap;->a(Landroid/content/Context;ILjava/lang/String;[Logr;ILjava/lang/String;Ljava/lang/String;Lkfp;Z)V

    .line 110
    iget-object v0, p0, Ldmy;->f:Landroid/content/Context;

    iget-object v2, p0, Ldmy;->a:Ljava/lang/String;

    iget v3, p0, Ldmy;->b:I

    iget-object v4, v9, Lnnu;->a:Lnna;

    iget-object v4, v4, Lnna;->b:Lnmu;

    iget-object v4, v4, Lnmu;->b:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, Lfvd;->a(Landroid/content/Context;ILjava/lang/String;ILjava/lang/String;)V

    .line 115
    :goto_0
    return-void

    .line 113
    :cond_0
    iget-object v0, p0, Ldmy;->f:Landroid/content/Context;

    iget v2, p0, Ldmy;->b:I

    invoke-static {v0, v1, v7, v2, v7}, Lfvd;->a(Landroid/content/Context;ILjava/lang/String;ILjava/lang/String;)V

    goto :goto_0
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 32
    check-cast p1, Lmiy;

    invoke-virtual {p0, p1}, Ldmy;->a(Lmiy;)V

    return-void
.end method

.method protected synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 32
    check-cast p1, Lmiz;

    invoke-virtual {p0, p1}, Ldmy;->a(Lmiz;)V

    return-void
.end method
