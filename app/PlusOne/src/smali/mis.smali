.class public final Lmis;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lntj;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 16279
    invoke-direct {p0}, Loxq;-><init>()V

    .line 16282
    iput-object v0, p0, Lmis;->apiHeader:Llyq;

    .line 16285
    iput-object v0, p0, Lmis;->a:Lntj;

    .line 16279
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 16302
    const/4 v0, 0x0

    .line 16303
    iget-object v1, p0, Lmis;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 16304
    const/4 v0, 0x1

    iget-object v1, p0, Lmis;->apiHeader:Llyq;

    .line 16305
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 16307
    :cond_0
    iget-object v1, p0, Lmis;->a:Lntj;

    if-eqz v1, :cond_1

    .line 16308
    const/4 v1, 0x2

    iget-object v2, p0, Lmis;->a:Lntj;

    .line 16309
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 16311
    :cond_1
    iget-object v1, p0, Lmis;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 16312
    iput v0, p0, Lmis;->ai:I

    .line 16313
    return v0
.end method

.method public a(Loxn;)Lmis;
    .locals 2

    .prologue
    .line 16321
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 16322
    sparse-switch v0, :sswitch_data_0

    .line 16326
    iget-object v1, p0, Lmis;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 16327
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmis;->ah:Ljava/util/List;

    .line 16330
    :cond_1
    iget-object v1, p0, Lmis;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 16332
    :sswitch_0
    return-object p0

    .line 16337
    :sswitch_1
    iget-object v0, p0, Lmis;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 16338
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmis;->apiHeader:Llyq;

    .line 16340
    :cond_2
    iget-object v0, p0, Lmis;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 16344
    :sswitch_2
    iget-object v0, p0, Lmis;->a:Lntj;

    if-nez v0, :cond_3

    .line 16345
    new-instance v0, Lntj;

    invoke-direct {v0}, Lntj;-><init>()V

    iput-object v0, p0, Lmis;->a:Lntj;

    .line 16347
    :cond_3
    iget-object v0, p0, Lmis;->a:Lntj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 16322
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 16290
    iget-object v0, p0, Lmis;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 16291
    const/4 v0, 0x1

    iget-object v1, p0, Lmis;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 16293
    :cond_0
    iget-object v0, p0, Lmis;->a:Lntj;

    if-eqz v0, :cond_1

    .line 16294
    const/4 v0, 0x2

    iget-object v1, p0, Lmis;->a:Lntj;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 16296
    :cond_1
    iget-object v0, p0, Lmis;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 16298
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 16275
    invoke-virtual {p0, p1}, Lmis;->a(Loxn;)Lmis;

    move-result-object v0

    return-object v0
.end method
