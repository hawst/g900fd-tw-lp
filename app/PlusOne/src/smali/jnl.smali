.class public abstract Ljnl;
.super Landroid/widget/LinearLayout;
.source "PG"

# interfaces
.implements Lljh;


# static fields
.field private static a:I

.field public static b:I

.field public static c:I

.field public static d:I

.field public static e:Landroid/content/res/ColorStateList;

.field public static f:I

.field public static g:I

.field public static h:I

.field public static i:Ljava/lang/String;

.field private static o:I

.field private static p:I

.field private static q:I


# instance fields
.field public j:Z

.field public k:Z

.field public l:Z

.field public m:Ljnt;

.field public n:Landroid/view/View;

.field private r:Landroid/view/View;

.field private s:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const v3, 0x7f0d024c

    const v2, 0x7f0b0141

    .line 49
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 61
    sget v0, Ljnl;->b:I

    if-nez v0, :cond_0

    .line 62
    invoke-virtual {p0}, Ljnl;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 63
    const v1, 0x7f0d024d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Ljnl;->b:I

    .line 64
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Ljnl;->a:I

    .line 65
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Ljnl;->c:I

    .line 66
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Ljnl;->o:I

    .line 67
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Ljnl;->p:I

    .line 68
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Ljnl;->q:I

    .line 69
    const v1, 0x7f0b013e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    .line 70
    const v1, 0x7f0b0148

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Ljnl;->d:I

    .line 71
    const v1, 0x7f0b034a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    sput-object v1, Ljnl;->e:Landroid/content/res/ColorStateList;

    .line 73
    invoke-virtual {p0}, Ljnl;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0357

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Ljnl;->i:Ljava/lang/String;

    .line 74
    const v1, 0x7f0d0127

    .line 75
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Ljnl;->f:I

    .line 76
    const v1, 0x7f0d0126

    .line 77
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Ljnl;->g:I

    .line 78
    const v1, 0x7f0d0125

    .line 79
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Ljnl;->h:I

    .line 50
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const v3, 0x7f0d024c

    const v2, 0x7f0b0141

    .line 53
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 61
    sget v0, Ljnl;->b:I

    if-nez v0, :cond_0

    .line 62
    invoke-virtual {p0}, Ljnl;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 63
    const v1, 0x7f0d024d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Ljnl;->b:I

    .line 64
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Ljnl;->a:I

    .line 65
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Ljnl;->c:I

    .line 66
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Ljnl;->o:I

    .line 67
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Ljnl;->p:I

    .line 68
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Ljnl;->q:I

    .line 69
    const v1, 0x7f0b013e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    .line 70
    const v1, 0x7f0b0148

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Ljnl;->d:I

    .line 71
    const v1, 0x7f0b034a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    sput-object v1, Ljnl;->e:Landroid/content/res/ColorStateList;

    .line 73
    invoke-virtual {p0}, Ljnl;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0357

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Ljnl;->i:Ljava/lang/String;

    .line 74
    const v1, 0x7f0d0127

    .line 75
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Ljnl;->f:I

    .line 76
    const v1, 0x7f0d0126

    .line 77
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Ljnl;->g:I

    .line 78
    const v1, 0x7f0d0125

    .line 79
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Ljnl;->h:I

    .line 54
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const v3, 0x7f0d024c

    const v2, 0x7f0b0141

    .line 57
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 61
    sget v0, Ljnl;->b:I

    if-nez v0, :cond_0

    .line 62
    invoke-virtual {p0}, Ljnl;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 63
    const v1, 0x7f0d024d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Ljnl;->b:I

    .line 64
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Ljnl;->a:I

    .line 65
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Ljnl;->c:I

    .line 66
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Ljnl;->o:I

    .line 67
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Ljnl;->p:I

    .line 68
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Ljnl;->q:I

    .line 69
    const v1, 0x7f0b013e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    .line 70
    const v1, 0x7f0b0148

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Ljnl;->d:I

    .line 71
    const v1, 0x7f0b034a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    sput-object v1, Ljnl;->e:Landroid/content/res/ColorStateList;

    .line 73
    invoke-virtual {p0}, Ljnl;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0357

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Ljnl;->i:Ljava/lang/String;

    .line 74
    const v1, 0x7f0d0127

    .line 75
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Ljnl;->f:I

    .line 76
    const v1, 0x7f0d0126

    .line 77
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Ljnl;->g:I

    .line 78
    const v1, 0x7f0d0125

    .line 79
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Ljnl;->h:I

    .line 58
    :cond_0
    return-void
.end method

.method private a(Landroid/widget/TextView;II)V
    .locals 2

    .prologue
    .line 179
    const/4 v0, 0x0

    int-to-float v1, p2

    invoke-virtual {p1, v0, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 180
    invoke-virtual {p1, p3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 181
    return-void
.end method


# virtual methods
.method public varargs a(I[Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    invoke-virtual {p0}, Ljnl;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 109
    invoke-virtual {v0, p1, p2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 96
    iget-object v0, p0, Ljnl;->n:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Ljnl;->n:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    iget-object v0, p0, Ljnl;->n:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 100
    :cond_0
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 189
    sget v0, Ljnl;->f:I

    sget v1, Ljnl;->g:I

    sget v2, Ljnl;->f:I

    sget v3, Ljnl;->g:I

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 190
    return-void
.end method

.method public a(Landroid/widget/TextView;)V
    .locals 2

    .prologue
    .line 151
    sget v0, Ljnl;->o:I

    const/4 v1, 0x1

    invoke-virtual {p0, p1, v0, v1}, Ljnl;->a(Landroid/widget/TextView;IZ)V

    .line 152
    return-void
.end method

.method public a(Landroid/widget/TextView;IZ)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 140
    sget v0, Ljnl;->b:I

    invoke-direct {p0, p1, v0, p2}, Ljnl;->a(Landroid/widget/TextView;II)V

    .line 141
    const/4 v0, 0x0

    const/4 v2, 0x1

    invoke-virtual {p1, v0, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 142
    invoke-virtual {p1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 143
    if-eqz v0, :cond_1

    .line 144
    if-eqz p3, :cond_0

    :goto_0
    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 148
    :goto_1
    return-void

    .line 144
    :cond_0
    sget v1, Ljnl;->f:I

    goto :goto_0

    .line 146
    :cond_1
    if-eqz p3, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v1, v0, v1, v1}, Landroid/widget/TextView;->setPadding(IIII)V

    goto :goto_1

    :cond_2
    sget v0, Ljnl;->f:I

    goto :goto_2
.end method

.method public a(Landroid/widget/TextView;Z)V
    .locals 1

    .prologue
    .line 155
    sget v0, Ljnl;->o:I

    invoke-virtual {p0, p1, v0, p2}, Ljnl;->a(Landroid/widget/TextView;IZ)V

    .line 156
    return-void
.end method

.method public a(Ljnt;)V
    .locals 3

    .prologue
    .line 113
    iput-object p1, p0, Ljnl;->m:Ljnt;

    .line 114
    iget-object v0, p0, Ljnl;->n:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 115
    if-eqz p1, :cond_1

    .line 116
    iget-object v0, p0, Ljnl;->n:Landroid/view/View;

    new-instance v1, Ljnm;

    invoke-direct {v1, p0}, Ljnm;-><init>(Ljnl;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 124
    iget-object v0, p0, Ljnl;->n:Landroid/view/View;

    .line 125
    invoke-virtual {p0}, Ljnl;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020416

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 124
    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 126
    iget-object v0, p0, Ljnl;->n:Landroid/view/View;

    invoke-virtual {p0, v0}, Ljnl;->b(Landroid/view/View;)V

    .line 127
    iget-object v0, p0, Ljnl;->r:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 133
    :cond_0
    :goto_0
    return-void

    .line 129
    :cond_1
    iget-object v0, p0, Ljnl;->n:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 130
    iget-object v0, p0, Ljnl;->r:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public abstract a(Lnjt;)V
.end method

.method public a(ZZZLjnt;)V
    .locals 1

    .prologue
    .line 198
    iput-boolean p1, p0, Ljnl;->j:Z

    .line 199
    iput-boolean p2, p0, Ljnl;->k:Z

    .line 200
    iput-boolean p3, p0, Ljnl;->l:Z

    .line 201
    invoke-virtual {p0, p4}, Ljnl;->a(Ljnt;)V

    .line 202
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Ljnl;->setFocusable(Z)V

    .line 203
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 136
    const/4 v0, 0x0

    return v0
.end method

.method protected b(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 193
    sget v0, Ljnl;->f:I

    sget v1, Ljnl;->f:I

    sget v2, Ljnl;->f:I

    sget v3, Ljnl;->f:I

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 194
    return-void
.end method

.method public b(Landroid/widget/TextView;)V
    .locals 2

    .prologue
    .line 159
    sget v0, Ljnl;->d:I

    const/4 v1, 0x1

    invoke-virtual {p0, p1, v0, v1}, Ljnl;->a(Landroid/widget/TextView;IZ)V

    .line 160
    return-void
.end method

.method public b(Landroid/widget/TextView;Z)V
    .locals 1

    .prologue
    .line 163
    sget v0, Ljnl;->d:I

    invoke-virtual {p0, p1, v0, p2}, Ljnl;->a(Landroid/widget/TextView;IZ)V

    .line 164
    return-void
.end method

.method public c(Landroid/widget/TextView;)V
    .locals 2

    .prologue
    .line 167
    sget v0, Ljnl;->a:I

    sget v1, Ljnl;->p:I

    invoke-direct {p0, p1, v0, v1}, Ljnl;->a(Landroid/widget/TextView;II)V

    .line 168
    return-void
.end method

.method public d(Landroid/widget/TextView;)V
    .locals 2

    .prologue
    .line 171
    sget v0, Ljnl;->c:I

    sget v1, Ljnl;->q:I

    invoke-direct {p0, p1, v0, v1}, Ljnl;->a(Landroid/widget/TextView;II)V

    .line 172
    return-void
.end method

.method public e(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    invoke-virtual {p0}, Ljnl;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 104
    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e(Landroid/widget/TextView;)V
    .locals 2

    .prologue
    .line 175
    sget v0, Ljnl;->c:I

    sget v1, Ljnl;->d:I

    invoke-direct {p0, p1, v0, v1}, Ljnl;->a(Landroid/widget/TextView;II)V

    .line 176
    return-void
.end method

.method public onFinishInflate()V
    .locals 3

    .prologue
    .line 85
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 86
    const v0, 0x7f100118

    invoke-virtual {p0, v0}, Ljnl;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ljnl;->s:Landroid/widget/TextView;

    .line 87
    iget-object v0, p0, Ljnl;->s:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 88
    invoke-virtual {p0}, Ljnl;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Ljnl;->s:Landroid/widget/TextView;

    const/16 v2, 0x11

    invoke-static {v0, v1, v2}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 90
    :cond_0
    const v0, 0x7f1003e4

    invoke-virtual {p0, v0}, Ljnl;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ljnl;->n:Landroid/view/View;

    .line 91
    const v0, 0x7f100061

    invoke-virtual {p0, v0}, Ljnl;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ljnl;->r:Landroid/view/View;

    .line 92
    return-void
.end method
