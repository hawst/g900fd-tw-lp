.class public final Lcod;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhyo;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lhyo",
        "<",
        "Lcpu;",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcpu;


# direct methods
.method public constructor <init>(Lcpu;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcod;->a:Lcpu;

    .line 17
    return-void
.end method


# virtual methods
.method public a()Lcpu;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcod;->a:Lcpu;

    return-object v0
.end method

.method public a(I)Ljava/lang/Long;
    .locals 2

    .prologue
    .line 41
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcod;->a:Lcpu;

    invoke-virtual {v0}, Lcpu;->a()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 42
    iget-object v0, p0, Lcod;->a:Lcpu;

    invoke-virtual {v0, p1}, Lcpu;->a(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 44
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/database/DataSetObserver;)V
    .locals 0

    .prologue
    .line 32
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcod;->a:Lcpu;

    invoke-virtual {v0}, Lcpu;->a()I

    move-result v0

    return v0
.end method

.method public b(Landroid/database/DataSetObserver;)V
    .locals 0

    .prologue
    .line 37
    return-void
.end method

.method public b(I)Z
    .locals 1

    .prologue
    .line 49
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcod;->a:Lcpu;

    invoke-virtual {v0}, Lcpu;->a()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic c(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 11
    invoke-virtual {p0, p1}, Lcod;->a(I)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcod;->a:Lcpu;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic d()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 11
    invoke-virtual {p0}, Lcod;->a()Lcpu;

    move-result-object v0

    return-object v0
.end method
