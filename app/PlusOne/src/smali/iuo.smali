.class public final Liuo;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a([Lnij;)Lnij;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 143
    if-nez p0, :cond_1

    .line 157
    :cond_0
    :goto_0
    return-object v0

    .line 147
    :cond_1
    array-length v3, p0

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_0

    aget-object v1, p0, v2

    .line 148
    iget-object v4, v1, Lnij;->c:Ljava/lang/Double;

    if-eqz v4, :cond_3

    iget-object v4, v1, Lnij;->d:Ljava/lang/Double;

    if-eqz v4, :cond_3

    .line 149
    iget-object v2, v1, Lnij;->g:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 153
    iput-object v0, v1, Lnij;->g:Ljava/lang/String;

    :cond_2
    move-object v0, v1

    .line 155
    goto :goto_0

    .line 147
    :cond_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;)Lpro;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 79
    if-nez p0, :cond_1

    .line 96
    :cond_0
    :goto_0
    return-object v0

    .line 83
    :cond_1
    const/16 v1, 0xa

    invoke-static {p0, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v2

    .line 85
    new-instance v1, Lpro;

    invoke-direct {v1}, Lpro;-><init>()V

    .line 87
    :try_start_0
    invoke-static {v1, v2}, Lpro;->a(Loxu;[B)Loxu;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 92
    invoke-static {v1, v2}, Liuo;->a(Lpro;[B)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 96
    goto :goto_0

    .line 89
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 177
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    .line 178
    invoke-static {p0}, Liuo;->b(Landroid/content/Context;)Z

    move-result v0

    .line 189
    :goto_0
    return v0

    .line 182
    :cond_0
    invoke-static {p0}, Liuf;->a(Landroid/content/Context;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 189
    invoke-static {p0}, Liuo;->b(Landroid/content/Context;)Z

    move-result v0

    goto :goto_0

    .line 184
    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 182
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method private static a(Lpro;[B)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 106
    new-instance v1, Lpro;

    invoke-direct {v1}, Lpro;-><init>()V

    .line 107
    iget-object v2, p0, Lpro;->a:Lolu;

    iput-object v2, v1, Lpro;->a:Lolu;

    .line 108
    iget-object v2, p0, Lpro;->b:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 109
    iget-object v2, p0, Lpro;->b:Ljava/lang/String;

    iput-object v2, v1, Lpro;->b:Ljava/lang/String;

    .line 112
    :cond_0
    invoke-virtual {v1}, Lpro;->a()I

    move-result v2

    new-array v2, v2, [B

    .line 113
    array-length v3, v2

    invoke-static {v2, v0, v3}, Loxo;->a([BII)Loxo;

    move-result-object v3

    .line 115
    :try_start_0
    invoke-virtual {v1, v3}, Lpro;->a(Loxo;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 119
    invoke-static {v2, p1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    :goto_0
    return v0

    .line 117
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private static b(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 200
    const-string v0, "location"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    .line 202
    const-string v1, "network"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "gps"

    .line 203
    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b([Lnij;)Z
    .locals 2

    .prologue
    .line 164
    invoke-static {p0}, Liuo;->a([Lnij;)Lnij;

    move-result-object v0

    .line 165
    if-eqz v0, :cond_0

    iget v0, v0, Lnij;->b:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
