.class public Lcwz;
.super Llol;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private N:Ldeo;

.field private O:Ldef;

.field private P:Ldel;

.field private Q:Landroid/widget/ImageButton;

.field private R:Z

.field private S:Landroid/view/animation/Animation;

.field private T:Landroid/view/animation/Animation;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 35
    invoke-direct {p0}, Llol;-><init>()V

    .line 48
    new-instance v0, Ldep;

    iget-object v1, p0, Lcwz;->av:Llqm;

    new-instance v2, Lcxa;

    invoke-direct {v2, p0}, Lcxa;-><init>(Lcwz;)V

    invoke-direct {v0, v1, v2}, Ldep;-><init>(Llqr;Ldeq;)V

    .line 54
    new-instance v0, Ldem;

    iget-object v1, p0, Lcwz;->av:Llqm;

    new-instance v2, Lcxb;

    invoke-direct {v2, p0}, Lcxb;-><init>(Lcwz;)V

    invoke-direct {v0, v1, v2}, Ldem;-><init>(Llqr;Lden;)V

    .line 60
    new-instance v0, Ldeg;

    iget-object v1, p0, Lcwz;->av:Llqm;

    new-instance v2, Lcxc;

    invoke-direct {v2, p0}, Lcxc;-><init>(Lcwz;)V

    invoke-direct {v0, v1, v2}, Ldeg;-><init>(Llqr;Ldeh;)V

    .line 73
    return-void
.end method

.method static synthetic a(Lcwz;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 35
    iget-object v0, p0, Lcwz;->Q:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcwz;->O:Ldef;

    invoke-virtual {v0}, Ldef;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcwz;->N:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    iget-object v1, p0, Lcwz;->P:Ldel;

    invoke-virtual {v1}, Ldel;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Lddl;->Q()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcwz;->N:Ldeo;

    invoke-virtual {v1}, Ldeo;->c()Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    iget-object v0, p0, Lcwz;->Q:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0

    :cond_3
    invoke-interface {v0}, Lddl;->F()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Lddl;->P()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v0, p0, Lcwz;->Q:Landroid/widget/ImageButton;

    const v1, 0x7f020477

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    iget-object v0, p0, Lcwz;->Q:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v0, p0, Lcwz;->Q:Landroid/widget/ImageButton;

    const v1, 0x7f0a0b05

    invoke-virtual {p0, v1}, Lcwz;->e_(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_4
    invoke-interface {v0}, Lddl;->G()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcwz;->Q:Landroid/widget/ImageButton;

    const v1, 0x7f02046f

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    iget-object v0, p0, Lcwz;->Q:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcwz;->Q:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic a(Lcwz;Z)Z
    .locals 0

    .prologue
    .line 35
    iput-boolean p1, p0, Lcwz;->R:Z

    return p1
.end method

.method static synthetic b(Lcwz;)Ldef;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcwz;->O:Ldef;

    return-object v0
.end method

.method static synthetic c(Lcwz;)Z
    .locals 1

    .prologue
    .line 35
    iget-boolean v0, p0, Lcwz;->R:Z

    return v0
.end method

.method static synthetic d(Lcwz;)Landroid/view/animation/Animation;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcwz;->T:Landroid/view/animation/Animation;

    return-object v0
.end method

.method static synthetic e(Lcwz;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcwz;->Q:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic f(Lcwz;)Landroid/view/animation/Animation;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcwz;->S:Landroid/view/animation/Animation;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 105
    const v0, 0x7f040107

    const/4 v1, 0x0

    .line 106
    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcwz;->Q:Landroid/widget/ImageButton;

    .line 107
    iget-object v0, p0, Lcwz;->Q:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 108
    iget-object v0, p0, Lcwz;->Q:Landroid/widget/ImageButton;

    return-object v0
.end method

.method public aO_()V
    .locals 2

    .prologue
    .line 157
    invoke-super {p0}, Llol;->aO_()V

    .line 160
    iget-object v0, p0, Lcwz;->P:Ldel;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ldel;->b(Z)V

    .line 161
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 77
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 78
    iget-object v0, p0, Lcwz;->au:Llnh;

    const-class v1, Ldeo;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldeo;

    iput-object v0, p0, Lcwz;->N:Ldeo;

    .line 79
    iget-object v0, p0, Lcwz;->au:Llnh;

    const-class v1, Ldef;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldef;

    iput-object v0, p0, Lcwz;->O:Ldef;

    .line 80
    iget-object v0, p0, Lcwz;->au:Llnh;

    const-class v1, Ldel;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldel;

    iput-object v0, p0, Lcwz;->P:Ldel;

    .line 82
    iget-object v0, p0, Lcwz;->O:Ldef;

    invoke-virtual {v0}, Ldef;->c()Z

    move-result v0

    iput-boolean v0, p0, Lcwz;->R:Z

    .line 84
    invoke-virtual {p0}, Lcwz;->n()Lz;

    move-result-object v0

    const v1, 0x7f050013

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcwz;->S:Landroid/view/animation/Animation;

    .line 85
    invoke-virtual {p0}, Lcwz;->n()Lz;

    move-result-object v0

    const v1, 0x7f050015

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcwz;->T:Landroid/view/animation/Animation;

    .line 87
    iget-object v0, p0, Lcwz;->S:Landroid/view/animation/Animation;

    new-instance v1, Lcxd;

    invoke-direct {v1, p0}, Lcxd;-><init>(Lcwz;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 94
    iget-object v0, p0, Lcwz;->T:Landroid/view/animation/Animation;

    new-instance v1, Lcxe;

    invoke-direct {v1, p0}, Lcxe;-><init>(Lcwz;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 100
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v3, -0x1

    .line 138
    iget-object v0, p0, Lcwz;->N:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v1

    .line 139
    invoke-virtual {p0}, Lcwz;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "account_id"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 141
    invoke-interface {v1}, Lddl;->F()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 142
    if-eq v2, v3, :cond_0

    .line 143
    iget-object v0, p0, Lcwz;->au:Llnh;

    const-class v3, Lhms;

    invoke-virtual {v0, v3}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v3, Lhmr;

    iget-object v4, p0, Lcwz;->at:Llnl;

    invoke-direct {v3, v4, v2}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v2, Lhmv;->dT:Lhmv;

    .line 145
    invoke-virtual {v3, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v2

    .line 143
    invoke-interface {v0, v2}, Lhms;->a(Lhmr;)V

    .line 147
    :cond_0
    invoke-interface {v1}, Lddl;->P()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcwz;->n()Lz;

    move-result-object v0

    invoke-interface {v1, v0}, Lddl;->d(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcwz;->a(Landroid/content/Intent;)V

    iget-object v0, p0, Lcwz;->P:Ldel;

    invoke-virtual {v0, v5}, Ldel;->b(Z)V

    .line 153
    :cond_1
    :goto_0
    return-void

    .line 147
    :cond_2
    const v0, 0x7f0a0651

    invoke-virtual {p0, v0}, Lcwz;->e_(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcwz;->n()Lz;

    move-result-object v1

    invoke-static {v1, v0, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 148
    :cond_3
    invoke-interface {v1}, Lddl;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 151
    invoke-virtual {p0}, Lcwz;->n()Lz;

    move-result-object v0

    invoke-interface {v1}, Lddl;->a()Lizu;

    move-result-object v1

    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;

    invoke-direct {v3, v0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "android.intent.action.VIEW"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "account_id"

    invoke-virtual {v3, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "photo_ref"

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 150
    invoke-virtual {p0, v3}, Lcwz;->a(Landroid/content/Intent;)V

    goto :goto_0
.end method
