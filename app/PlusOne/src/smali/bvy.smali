.class public final Lbvy;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field final a:Ljava/lang/Object;

.field b:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field c:Z

.field private final d:Landroid/content/Context;

.field private final e:Landroid/content/Intent;

.field private f:Landroid/content/ServiceConnection;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lbvy;->a:Ljava/lang/Object;

    .line 46
    const/4 v0, 0x0

    iput-object v0, p0, Lbvy;->b:Ljava/lang/Object;

    .line 52
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbvy;->c:Z

    .line 54
    new-instance v0, Lbvz;

    invoke-direct {v0, p0}, Lbvz;-><init>(Lbvy;)V

    iput-object v0, p0, Lbvy;->f:Landroid/content/ServiceConnection;

    .line 80
    iput-object p1, p0, Lbvy;->d:Landroid/content/Context;

    .line 81
    iput-object p2, p0, Lbvy;->e:Landroid/content/Intent;

    .line 82
    return-void
.end method


# virtual methods
.method public a()V
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 85
    iget-object v1, p0, Lbvy;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 86
    :try_start_0
    iget-boolean v2, p0, Lbvy;->c:Z

    if-nez v2, :cond_0

    :goto_0
    const-string v2, "already trying to connect"

    invoke-static {v0, v2}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 87
    iget-object v0, p0, Lbvy;->b:Ljava/lang/Object;

    const-string v2, "mBinder"

    const-string v3, "already connected"

    invoke-static {v0, v2, v3}, Lcgp;->b(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 88
    iget-object v0, p0, Lbvy;->d:Landroid/content/Context;

    iget-object v2, p0, Lbvy;->e:Landroid/content/Intent;

    iget-object v3, p0, Lbvy;->f:Landroid/content/ServiceConnection;

    const/4 v4, 0x1

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    iput-boolean v0, p0, Lbvy;->c:Z

    .line 89
    const/4 v0, 0x0

    iput-object v0, p0, Lbvy;->b:Ljava/lang/Object;

    .line 90
    monitor-exit v1

    return-void

    .line 86
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 90
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b()V
    .locals 3

    .prologue
    .line 94
    iget-object v1, p0, Lbvy;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 95
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lbvy;->c:Z

    .line 96
    const/4 v0, 0x0

    iput-object v0, p0, Lbvy;->b:Ljava/lang/Object;

    .line 97
    iget-object v0, p0, Lbvy;->d:Landroid/content/Context;

    iget-object v2, p0, Lbvy;->f:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 98
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public c()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 110
    iget-object v1, p0, Lbvy;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 111
    :goto_0
    :try_start_0
    iget-boolean v0, p0, Lbvy;->c:Z

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lbvy;->a:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V

    goto :goto_0

    .line 118
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 114
    :cond_0
    :try_start_1
    iget-object v0, p0, Lbvy;->b:Ljava/lang/Object;

    if-nez v0, :cond_1

    .line 115
    new-instance v0, Lbwa;

    invoke-direct {v0}, Lbwa;-><init>()V

    throw v0

    .line 117
    :cond_1
    iget-object v0, p0, Lbvy;->b:Ljava/lang/Object;

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v0
.end method

.method public d()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 128
    iget-object v1, p0, Lbvy;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 129
    :try_start_0
    iget-boolean v0, p0, Lbvy;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lbvy;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 130
    new-instance v0, Lbwa;

    invoke-direct {v0}, Lbwa;-><init>()V

    throw v0

    .line 134
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 133
    :cond_0
    :try_start_1
    iget-object v0, p0, Lbvy;->b:Ljava/lang/Object;

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v0
.end method
