.class public final Lmhu;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmzu;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 22273
    invoke-direct {p0}, Loxq;-><init>()V

    .line 22276
    iput-object v0, p0, Lmhu;->apiHeader:Llyq;

    .line 22279
    iput-object v0, p0, Lmhu;->a:Lmzu;

    .line 22273
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 22296
    const/4 v0, 0x0

    .line 22297
    iget-object v1, p0, Lmhu;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 22298
    const/4 v0, 0x1

    iget-object v1, p0, Lmhu;->apiHeader:Llyq;

    .line 22299
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 22301
    :cond_0
    iget-object v1, p0, Lmhu;->a:Lmzu;

    if-eqz v1, :cond_1

    .line 22302
    const/4 v1, 0x2

    iget-object v2, p0, Lmhu;->a:Lmzu;

    .line 22303
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 22305
    :cond_1
    iget-object v1, p0, Lmhu;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 22306
    iput v0, p0, Lmhu;->ai:I

    .line 22307
    return v0
.end method

.method public a(Loxn;)Lmhu;
    .locals 2

    .prologue
    .line 22315
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 22316
    sparse-switch v0, :sswitch_data_0

    .line 22320
    iget-object v1, p0, Lmhu;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 22321
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmhu;->ah:Ljava/util/List;

    .line 22324
    :cond_1
    iget-object v1, p0, Lmhu;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 22326
    :sswitch_0
    return-object p0

    .line 22331
    :sswitch_1
    iget-object v0, p0, Lmhu;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 22332
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmhu;->apiHeader:Llyq;

    .line 22334
    :cond_2
    iget-object v0, p0, Lmhu;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 22338
    :sswitch_2
    iget-object v0, p0, Lmhu;->a:Lmzu;

    if-nez v0, :cond_3

    .line 22339
    new-instance v0, Lmzu;

    invoke-direct {v0}, Lmzu;-><init>()V

    iput-object v0, p0, Lmhu;->a:Lmzu;

    .line 22341
    :cond_3
    iget-object v0, p0, Lmhu;->a:Lmzu;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 22316
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 22284
    iget-object v0, p0, Lmhu;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 22285
    const/4 v0, 0x1

    iget-object v1, p0, Lmhu;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 22287
    :cond_0
    iget-object v0, p0, Lmhu;->a:Lmzu;

    if-eqz v0, :cond_1

    .line 22288
    const/4 v0, 0x2

    iget-object v1, p0, Lmhu;->a:Lmzu;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 22290
    :cond_1
    iget-object v0, p0, Lmhu;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 22292
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 22269
    invoke-virtual {p0, p1}, Lmhu;->a(Loxn;)Lmhu;

    move-result-object v0

    return-object v0
.end method
