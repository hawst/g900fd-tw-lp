.class public final Lfbv;
.super Ljvc;
.source "PG"

# interfaces
.implements Lfdi;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljvc;",
        "Lfdi",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# static fields
.field private static b:[Ljava/lang/String;


# instance fields
.field private f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private final i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljuf;",
            ">;"
        }
    .end annotation
.end field

.field private j:Lizu;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 39
    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "tile_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "view_id"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "all_tiles.image_url"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "view_order"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "media_attr"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "user_actions"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "NULL as virtual_photo_id"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "NULL as virtual_gaia_id"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "view_id as virtual_selection_cluster_id"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "0 as virtual_media_type"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "NULL as virtual_local_uri"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "NULL as virtual_signature"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "-1 as virtual_all_photos_row_id"

    aput-object v2, v0, v1

    sput-object v0, Lfbv;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/util/List;I)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/List",
            "<",
            "Ljuf;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 118
    sget-object v4, Lfbv;->b:[Ljava/lang/String;

    .line 119
    invoke-static/range {p7 .. p7}, Lfbv;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move/from16 v7, p6

    move/from16 v9, p8

    .line 118
    invoke-direct/range {v0 .. v9}, Ljvc;-><init>(Landroid/content/Context;ILjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZI)V

    .line 121
    const/4 v0, 0x0

    iput-object v0, p0, Lfbv;->i:Ljava/util/List;

    .line 122
    const/4 v0, 0x0

    iput-object v0, p0, Lfbv;->j:Lizu;

    .line 123
    iput-object p4, p0, Lfbv;->f:Ljava/lang/String;

    .line 124
    iput-object p5, p0, Lfbv;->g:Ljava/lang/String;

    .line 125
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "SELECT count(*) FROM all_tiles WHERE %s AND view_order < ( %s )"

    const/4 v0, 0x2

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    iget-object v4, p0, Lfbv;->e:Ljava/lang/String;

    aput-object v4, v3, v0

    const/4 v4, 0x1

    if-eqz p6, :cond_0

    const-string v0, "SELECT view_order FROM all_tiles WHERE media_attr & 512 != 0 AND view_id = ?  AND tile_id = ?"

    :goto_0
    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfbv;->h:Ljava/lang/String;

    .line 127
    return-void

    .line 125
    :cond_0
    const-string v0, "SELECT view_order FROM all_tiles WHERE media_attr & 512 == 0 AND view_id = ?  AND tile_id = ?"

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/util/List;Lizu;Ljava/util/List;I)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljuf;",
            ">;",
            "Lizu;",
            "Ljava/util/List",
            "<",
            "Ljuf;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 105
    sget-object v4, Lfbv;->b:[Ljava/lang/String;

    .line 106
    invoke-static/range {p6 .. p6}, Lfbv;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move/from16 v9, p7

    .line 105
    invoke-direct/range {v0 .. v9}, Ljvc;-><init>(Landroid/content/Context;ILjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZI)V

    .line 108
    iput-object p4, p0, Lfbv;->i:Ljava/util/List;

    .line 109
    iput-object p5, p0, Lfbv;->j:Lizu;

    .line 110
    const/4 v0, 0x0

    iput-object v0, p0, Lfbv;->g:Ljava/lang/String;

    .line 111
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "SELECT count(*) FROM all_tiles WHERE %s AND view_order < ( %s )"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lfbv;->e:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "SELECT view_order FROM all_tiles WHERE media_attr & 512 == 0 AND view_id = ?  AND tile_id = ?"

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfbv;->h:Ljava/lang/String;

    .line 113
    return-void
.end method

.method private static a(Ljava/util/List;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljuf;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 226
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 228
    if-eqz p0, :cond_1

    .line 229
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 230
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljuf;

    invoke-interface {v0}, Ljuf;->f()Lizu;

    move-result-object v0

    invoke-virtual {v0}, Lizu;->a()Ljava/lang/String;

    move-result-object v0

    .line 231
    if-eqz v0, :cond_0

    .line 232
    invoke-static {v2, v0}, Landroid/database/DatabaseUtils;->appendEscapedSQLString(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 233
    const/16 v0, 0x2c

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 229
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 238
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 239
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 240
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "( type == 4 AND tile_id NOT IN (%s) )"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v4

    invoke-static {v0, v1, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 242
    :goto_1
    return-object v0

    :cond_2
    const-string v0, "type == 4"

    goto :goto_1
.end method


# virtual methods
.method public C()Landroid/database/Cursor;
    .locals 15

    .prologue
    const/16 v14, 0xd

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 131
    iget-object v0, p0, Lfbv;->i:Ljava/util/List;

    if-eqz v0, :cond_7

    .line 132
    new-instance v6, Lhym;

    sget-object v0, Lfbv;->b:[Ljava/lang/String;

    invoke-direct {v6, v0}, Lhym;-><init>([Ljava/lang/String;)V

    iget-object v0, p0, Lfbv;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v7

    sget-object v0, Lfbv;->b:[Ljava/lang/String;

    const/16 v0, 0xe

    new-array v8, v0, [Ljava/lang/Object;

    move v2, v5

    move v1, v5

    :goto_0
    if-ge v2, v7, :cond_5

    iget-object v0, p0, Lfbv;->i:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljuf;

    invoke-interface {v0}, Ljuf;->f()Lizu;

    move-result-object v9

    iget-object v3, p0, Lfbv;->j:Lizu;

    invoke-virtual {v9, v3}, Lizu;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v1, v2

    :cond_0
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v8, v5

    const/4 v3, 0x1

    invoke-virtual {v9}, Lizu;->a()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v3

    const/4 v10, 0x3

    invoke-virtual {v9}, Lizu;->h()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v9}, Lizu;->d()Ljava/lang/String;

    move-result-object v3

    :goto_1
    aput-object v3, v8, v10

    const/4 v3, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v3

    const/4 v3, 0x5

    invoke-interface {v0}, Ljuf;->i()J

    move-result-wide v10

    const-wide/16 v12, 0x4000

    or-long/2addr v10, v12

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v8, v3

    const/4 v3, 0x7

    invoke-virtual {v9}, Lizu;->c()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v8, v3

    const/16 v3, 0x8

    invoke-virtual {v9}, Lizu;->b()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v3

    const/16 v3, 0xa

    invoke-virtual {v9}, Lizu;->g()Ljac;

    move-result-object v10

    iget v10, v10, Ljac;->e:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v3

    const/16 v10, 0xb

    invoke-virtual {v9}, Lizu;->i()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v9}, Lizu;->e()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    :goto_2
    aput-object v3, v8, v10

    const/16 v3, 0xc

    invoke-virtual {v9}, Lizu;->f()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v3

    const/4 v3, -0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v8, v14

    instance-of v3, v0, Ljuc;

    if-eqz v3, :cond_4

    check-cast v0, Ljuc;

    const/4 v3, 0x2

    invoke-virtual {v0}, Ljuc;->k()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v3

    const/16 v3, 0x9

    invoke-virtual {v0}, Ljuc;->j()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v8, v3

    :cond_1
    :goto_3
    invoke-virtual {v6, v8}, Lhym;->a([Ljava/lang/Object;)V

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    :cond_2
    move-object v3, v4

    goto :goto_1

    :cond_3
    move-object v3, v4

    goto :goto_2

    :cond_4
    instance-of v3, v0, Ldqm;

    if-eqz v3, :cond_1

    check-cast v0, Ldqm;

    invoke-virtual {v0}, Ldqm;->a()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v8, v14

    goto :goto_3

    :cond_5
    invoke-virtual {v6}, Lhym;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "start_position"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    move-object v4, v6

    .line 166
    :cond_6
    :goto_4
    return-object v4

    .line 135
    :cond_7
    invoke-super {p0}, Ljvc;->C()Landroid/database/Cursor;

    move-result-object v1

    .line 136
    if-eqz v1, :cond_6

    .line 140
    invoke-virtual {p0}, Lfbv;->n()Landroid/content/Context;

    move-result-object v0

    iget v2, p0, Lfbv;->d:I

    invoke-static {v0, v2}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    .line 141
    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 144
    iget-object v0, p0, Lfbv;->f:Ljava/lang/String;

    if-nez v0, :cond_8

    iget-object v0, p0, Lfbv;->g:Ljava/lang/String;

    if-nez v0, :cond_c

    :cond_8
    iget-object v0, p0, Lfbv;->f:Ljava/lang/String;

    move-object v2, v0

    .line 148
    :goto_5
    if-eqz v2, :cond_d

    .line 149
    invoke-virtual {p0}, Lfbv;->l()[Ljava/lang/String;

    move-result-object v4

    .line 151
    array-length v0, v4

    add-int/lit8 v0, v0, 0x2

    .line 152
    invoke-static {v4, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 153
    array-length v5, v4

    iget-object v6, p0, Lfbv;->c:Ljava/lang/String;

    aput-object v6, v0, v5

    .line 154
    array-length v4, v4

    add-int/lit8 v4, v4, 0x1

    aput-object v2, v0, v4

    .line 155
    iget-object v2, p0, Lfbv;->h:Ljava/lang/String;

    invoke-static {v3, v2, v0}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v2

    long-to-int v0, v2

    move v2, v0

    .line 157
    :goto_6
    instance-of v0, v1, Lhxy;

    if-eqz v0, :cond_b

    move-object v0, v1

    .line 158
    check-cast v0, Lhxy;

    .line 159
    invoke-interface {v0}, Lhxy;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    .line 160
    sget-object v4, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    if-eq v3, v4, :cond_9

    if-nez v3, :cond_a

    .line 161
    :cond_9
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 163
    :cond_a
    const-string v4, "start_position"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 164
    invoke-interface {v0, v3}, Lhxy;->a(Landroid/os/Bundle;)V

    :cond_b
    move-object v4, v1

    .line 166
    goto :goto_4

    .line 144
    :cond_c
    iget-object v0, p0, Lfbv;->g:Ljava/lang/String;

    .line 145
    invoke-virtual {p0, v3, v0}, Lfbv;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    goto :goto_5

    :cond_d
    move v2, v5

    goto :goto_6
.end method

.method public a(Landroid/database/Cursor;I)V
    .locals 1

    .prologue
    .line 248
    invoke-interface {p1, p2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 249
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfbv;->f:Ljava/lang/String;

    .line 250
    return-void
.end method

.method public a(Lizu;)V
    .locals 0

    .prologue
    .line 254
    iput-object p1, p0, Lfbv;->j:Lizu;

    .line 255
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;I)V
    .locals 0

    .prologue
    .line 35
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lfbv;->a(Landroid/database/Cursor;I)V

    return-void
.end method
