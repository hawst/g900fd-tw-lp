.class public final Lpnm;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Long;

.field private b:Ljava/lang/String;

.field private c:I

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/Boolean;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/Boolean;

.field private i:Ljava/lang/Boolean;

.field private j:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 224
    invoke-direct {p0}, Loxq;-><init>()V

    .line 231
    const/high16 v0, -0x80000000

    iput v0, p0, Lpnm;->c:I

    .line 224
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 282
    const/4 v0, 0x1

    iget-object v1, p0, Lpnm;->a:Ljava/lang/Long;

    .line 284
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Loxo;->f(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 285
    const/4 v1, 0x2

    iget-object v2, p0, Lpnm;->b:Ljava/lang/String;

    .line 286
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 287
    iget v1, p0, Lpnm;->c:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 288
    const/4 v1, 0x3

    iget v2, p0, Lpnm;->c:I

    .line 289
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 291
    :cond_0
    iget-object v1, p0, Lpnm;->d:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 292
    const/4 v1, 0x4

    iget-object v2, p0, Lpnm;->d:Ljava/lang/String;

    .line 293
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 295
    :cond_1
    iget-object v1, p0, Lpnm;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 296
    const/4 v1, 0x5

    iget-object v2, p0, Lpnm;->e:Ljava/lang/Boolean;

    .line 297
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 299
    :cond_2
    iget-object v1, p0, Lpnm;->f:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 300
    const/4 v1, 0x6

    iget-object v2, p0, Lpnm;->f:Ljava/lang/String;

    .line 301
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 303
    :cond_3
    iget-object v1, p0, Lpnm;->g:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 304
    const/4 v1, 0x7

    iget-object v2, p0, Lpnm;->g:Ljava/lang/String;

    .line 305
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 307
    :cond_4
    iget-object v1, p0, Lpnm;->h:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    .line 308
    const/16 v1, 0x8

    iget-object v2, p0, Lpnm;->h:Ljava/lang/Boolean;

    .line 309
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 311
    :cond_5
    iget-object v1, p0, Lpnm;->i:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    .line 312
    const/16 v1, 0x9

    iget-object v2, p0, Lpnm;->i:Ljava/lang/Boolean;

    .line 313
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 315
    :cond_6
    iget-object v1, p0, Lpnm;->j:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    .line 316
    const/16 v1, 0xa

    iget-object v2, p0, Lpnm;->j:Ljava/lang/Boolean;

    .line 317
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 319
    :cond_7
    iget-object v1, p0, Lpnm;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 320
    iput v0, p0, Lpnm;->ai:I

    .line 321
    return v0
.end method

.method public a(Loxn;)Lpnm;
    .locals 2

    .prologue
    .line 329
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 330
    sparse-switch v0, :sswitch_data_0

    .line 334
    iget-object v1, p0, Lpnm;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 335
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpnm;->ah:Ljava/util/List;

    .line 338
    :cond_1
    iget-object v1, p0, Lpnm;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 340
    :sswitch_0
    return-object p0

    .line 345
    :sswitch_1
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lpnm;->a:Ljava/lang/Long;

    goto :goto_0

    .line 349
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpnm;->b:Ljava/lang/String;

    goto :goto_0

    .line 353
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 354
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 357
    :cond_2
    iput v0, p0, Lpnm;->c:I

    goto :goto_0

    .line 359
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lpnm;->c:I

    goto :goto_0

    .line 364
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpnm;->d:Ljava/lang/String;

    goto :goto_0

    .line 368
    :sswitch_5
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lpnm;->e:Ljava/lang/Boolean;

    goto :goto_0

    .line 372
    :sswitch_6
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpnm;->f:Ljava/lang/String;

    goto :goto_0

    .line 376
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpnm;->g:Ljava/lang/String;

    goto :goto_0

    .line 380
    :sswitch_8
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lpnm;->h:Ljava/lang/Boolean;

    goto :goto_0

    .line 384
    :sswitch_9
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lpnm;->i:Ljava/lang/Boolean;

    goto :goto_0

    .line 388
    :sswitch_a
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lpnm;->j:Ljava/lang/Boolean;

    goto :goto_0

    .line 330
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 250
    const/4 v0, 0x1

    iget-object v1, p0, Lpnm;->a:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 251
    const/4 v0, 0x2

    iget-object v1, p0, Lpnm;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 252
    iget v0, p0, Lpnm;->c:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 253
    const/4 v0, 0x3

    iget v1, p0, Lpnm;->c:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 255
    :cond_0
    iget-object v0, p0, Lpnm;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 256
    const/4 v0, 0x4

    iget-object v1, p0, Lpnm;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 258
    :cond_1
    iget-object v0, p0, Lpnm;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 259
    const/4 v0, 0x5

    iget-object v1, p0, Lpnm;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 261
    :cond_2
    iget-object v0, p0, Lpnm;->f:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 262
    const/4 v0, 0x6

    iget-object v1, p0, Lpnm;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 264
    :cond_3
    iget-object v0, p0, Lpnm;->g:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 265
    const/4 v0, 0x7

    iget-object v1, p0, Lpnm;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 267
    :cond_4
    iget-object v0, p0, Lpnm;->h:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 268
    const/16 v0, 0x8

    iget-object v1, p0, Lpnm;->h:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 270
    :cond_5
    iget-object v0, p0, Lpnm;->i:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    .line 271
    const/16 v0, 0x9

    iget-object v1, p0, Lpnm;->i:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 273
    :cond_6
    iget-object v0, p0, Lpnm;->j:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    .line 274
    const/16 v0, 0xa

    iget-object v1, p0, Lpnm;->j:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 276
    :cond_7
    iget-object v0, p0, Lpnm;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 278
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 220
    invoke-virtual {p0, p1}, Lpnm;->a(Loxn;)Lpnm;

    move-result-object v0

    return-object v0
.end method
