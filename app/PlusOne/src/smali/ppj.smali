.class public final Lppj;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lppj;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Lppf;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Lppf;

.field private l:[Lppl;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const/4 v0, 0x0

    new-array v0, v0, [Lppj;

    sput-object v0, Lppj;->a:[Lppj;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 14
    invoke-direct {p0}, Loxq;-><init>()V

    .line 27
    iput-object v0, p0, Lppj;->g:Lppf;

    .line 36
    iput-object v0, p0, Lppj;->k:Lppf;

    .line 39
    sget-object v0, Lppl;->a:[Lppl;

    iput-object v0, p0, Lppj;->l:[Lppl;

    .line 14
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 87
    .line 88
    iget-object v0, p0, Lppj;->c:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 89
    const/4 v0, 0x1

    iget-object v2, p0, Lppj;->c:Ljava/lang/String;

    .line 90
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 92
    :goto_0
    iget-object v2, p0, Lppj;->d:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 93
    const/4 v2, 0x2

    iget-object v3, p0, Lppj;->d:Ljava/lang/String;

    .line 94
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 96
    :cond_0
    iget-object v2, p0, Lppj;->g:Lppf;

    if-eqz v2, :cond_1

    .line 97
    const/4 v2, 0x4

    iget-object v3, p0, Lppj;->g:Lppf;

    .line 98
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 100
    :cond_1
    iget-object v2, p0, Lppj;->h:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 101
    const/4 v2, 0x5

    iget-object v3, p0, Lppj;->h:Ljava/lang/String;

    .line 102
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 104
    :cond_2
    iget-object v2, p0, Lppj;->j:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 105
    const/4 v2, 0x6

    iget-object v3, p0, Lppj;->j:Ljava/lang/String;

    .line 106
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 108
    :cond_3
    iget-object v2, p0, Lppj;->k:Lppf;

    if-eqz v2, :cond_4

    .line 109
    const/4 v2, 0x7

    iget-object v3, p0, Lppj;->k:Lppf;

    .line 110
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 112
    :cond_4
    iget-object v2, p0, Lppj;->i:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 113
    const/16 v2, 0x8

    iget-object v3, p0, Lppj;->i:Ljava/lang/String;

    .line 114
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 116
    :cond_5
    iget-object v2, p0, Lppj;->l:[Lppl;

    if-eqz v2, :cond_7

    .line 117
    iget-object v2, p0, Lppj;->l:[Lppl;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_7

    aget-object v4, v2, v1

    .line 118
    if-eqz v4, :cond_6

    .line 119
    const/16 v5, 0x9

    .line 120
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 117
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 124
    :cond_7
    iget-object v1, p0, Lppj;->b:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 125
    const/16 v1, 0xa

    iget-object v2, p0, Lppj;->b:Ljava/lang/String;

    .line 126
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 128
    :cond_8
    iget-object v1, p0, Lppj;->e:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 129
    const/16 v1, 0xb

    iget-object v2, p0, Lppj;->e:Ljava/lang/String;

    .line 130
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 132
    :cond_9
    iget-object v1, p0, Lppj;->f:Ljava/lang/String;

    if-eqz v1, :cond_a

    .line 133
    const/16 v1, 0xc

    iget-object v2, p0, Lppj;->f:Ljava/lang/String;

    .line 134
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 136
    :cond_a
    iget-object v1, p0, Lppj;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 137
    iput v0, p0, Lppj;->ai:I

    .line 138
    return v0

    :cond_b
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lppj;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 146
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 147
    sparse-switch v0, :sswitch_data_0

    .line 151
    iget-object v2, p0, Lppj;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 152
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lppj;->ah:Ljava/util/List;

    .line 155
    :cond_1
    iget-object v2, p0, Lppj;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 157
    :sswitch_0
    return-object p0

    .line 162
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lppj;->c:Ljava/lang/String;

    goto :goto_0

    .line 166
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lppj;->d:Ljava/lang/String;

    goto :goto_0

    .line 170
    :sswitch_3
    iget-object v0, p0, Lppj;->g:Lppf;

    if-nez v0, :cond_2

    .line 171
    new-instance v0, Lppf;

    invoke-direct {v0}, Lppf;-><init>()V

    iput-object v0, p0, Lppj;->g:Lppf;

    .line 173
    :cond_2
    iget-object v0, p0, Lppj;->g:Lppf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 177
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lppj;->h:Ljava/lang/String;

    goto :goto_0

    .line 181
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lppj;->j:Ljava/lang/String;

    goto :goto_0

    .line 185
    :sswitch_6
    iget-object v0, p0, Lppj;->k:Lppf;

    if-nez v0, :cond_3

    .line 186
    new-instance v0, Lppf;

    invoke-direct {v0}, Lppf;-><init>()V

    iput-object v0, p0, Lppj;->k:Lppf;

    .line 188
    :cond_3
    iget-object v0, p0, Lppj;->k:Lppf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 192
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lppj;->i:Ljava/lang/String;

    goto :goto_0

    .line 196
    :sswitch_8
    const/16 v0, 0x4a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 197
    iget-object v0, p0, Lppj;->l:[Lppl;

    if-nez v0, :cond_5

    move v0, v1

    .line 198
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lppl;

    .line 199
    iget-object v3, p0, Lppj;->l:[Lppl;

    if-eqz v3, :cond_4

    .line 200
    iget-object v3, p0, Lppj;->l:[Lppl;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 202
    :cond_4
    iput-object v2, p0, Lppj;->l:[Lppl;

    .line 203
    :goto_2
    iget-object v2, p0, Lppj;->l:[Lppl;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 204
    iget-object v2, p0, Lppj;->l:[Lppl;

    new-instance v3, Lppl;

    invoke-direct {v3}, Lppl;-><init>()V

    aput-object v3, v2, v0

    .line 205
    iget-object v2, p0, Lppj;->l:[Lppl;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 206
    invoke-virtual {p1}, Loxn;->a()I

    .line 203
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 197
    :cond_5
    iget-object v0, p0, Lppj;->l:[Lppl;

    array-length v0, v0

    goto :goto_1

    .line 209
    :cond_6
    iget-object v2, p0, Lppj;->l:[Lppl;

    new-instance v3, Lppl;

    invoke-direct {v3}, Lppl;-><init>()V

    aput-object v3, v2, v0

    .line 210
    iget-object v2, p0, Lppj;->l:[Lppl;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 214
    :sswitch_9
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lppj;->b:Ljava/lang/String;

    goto/16 :goto_0

    .line 218
    :sswitch_a
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lppj;->e:Ljava/lang/String;

    goto/16 :goto_0

    .line 222
    :sswitch_b
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lppj;->f:Ljava/lang/String;

    goto/16 :goto_0

    .line 147
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
        0x3a -> :sswitch_6
        0x42 -> :sswitch_7
        0x4a -> :sswitch_8
        0x52 -> :sswitch_9
        0x5a -> :sswitch_a
        0x62 -> :sswitch_b
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 44
    iget-object v0, p0, Lppj;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 45
    const/4 v0, 0x1

    iget-object v1, p0, Lppj;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 47
    :cond_0
    iget-object v0, p0, Lppj;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 48
    const/4 v0, 0x2

    iget-object v1, p0, Lppj;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 50
    :cond_1
    iget-object v0, p0, Lppj;->g:Lppf;

    if-eqz v0, :cond_2

    .line 51
    const/4 v0, 0x4

    iget-object v1, p0, Lppj;->g:Lppf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 53
    :cond_2
    iget-object v0, p0, Lppj;->h:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 54
    const/4 v0, 0x5

    iget-object v1, p0, Lppj;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 56
    :cond_3
    iget-object v0, p0, Lppj;->j:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 57
    const/4 v0, 0x6

    iget-object v1, p0, Lppj;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 59
    :cond_4
    iget-object v0, p0, Lppj;->k:Lppf;

    if-eqz v0, :cond_5

    .line 60
    const/4 v0, 0x7

    iget-object v1, p0, Lppj;->k:Lppf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 62
    :cond_5
    iget-object v0, p0, Lppj;->i:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 63
    const/16 v0, 0x8

    iget-object v1, p0, Lppj;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 65
    :cond_6
    iget-object v0, p0, Lppj;->l:[Lppl;

    if-eqz v0, :cond_8

    .line 66
    iget-object v1, p0, Lppj;->l:[Lppl;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_8

    aget-object v3, v1, v0

    .line 67
    if-eqz v3, :cond_7

    .line 68
    const/16 v4, 0x9

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 66
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 72
    :cond_8
    iget-object v0, p0, Lppj;->b:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 73
    const/16 v0, 0xa

    iget-object v1, p0, Lppj;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 75
    :cond_9
    iget-object v0, p0, Lppj;->e:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 76
    const/16 v0, 0xb

    iget-object v1, p0, Lppj;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 78
    :cond_a
    iget-object v0, p0, Lppj;->f:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 79
    const/16 v0, 0xc

    iget-object v1, p0, Lppj;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 81
    :cond_b
    iget-object v0, p0, Lppj;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 83
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 10
    invoke-virtual {p0, p1}, Lppj;->a(Loxn;)Lppj;

    move-result-object v0

    return-object v0
.end method
