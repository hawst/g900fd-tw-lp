.class public final Llct;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static bq:Llct;


# instance fields
.field public final A:Landroid/graphics/Rect;

.field public final B:Landroid/graphics/Rect;

.field public final C:I

.field public final D:I

.field public final E:I

.field public final F:I

.field public final G:I

.field public final H:Landroid/graphics/Rect;

.field public final I:Landroid/graphics/Paint;

.field public final J:I

.field public final K:Landroid/graphics/Bitmap;

.field public final L:Landroid/graphics/Bitmap;

.field public final M:Landroid/graphics/Bitmap;

.field public final N:I

.field public final O:I

.field public final P:I

.field public final Q:I

.field public final R:I

.field public final S:I

.field public final T:I

.field public final U:I

.field public final V:I

.field public final W:I

.field public final X:I

.field public final Y:I

.field public final Z:I

.field public final a:Lfo;

.field public final aA:I

.field public final aB:I

.field public final aC:I

.field public final aD:Landroid/graphics/drawable/NinePatchDrawable;

.field public final aE:I

.field public final aF:I

.field public final aG:I

.field public final aH:I

.field public final aI:I

.field public final aJ:F

.field public final aK:F

.field public final aL:F

.field public final aM:Landroid/graphics/Bitmap;

.field public final aN:Landroid/graphics/Bitmap;

.field public final aO:Landroid/graphics/Bitmap;

.field public final aP:I

.field public final aQ:I

.field public final aR:I

.field public final aS:I

.field public final aT:I

.field public final aU:I

.field public final aV:I

.field public final aW:I

.field public final aX:I

.field public final aY:I

.field public final aZ:I

.field public final aa:Landroid/graphics/Bitmap;

.field public final ab:I

.field public final ac:I

.field public final ad:Landroid/graphics/drawable/NinePatchDrawable;

.field public final ae:I

.field public final af:Landroid/text/style/StyleSpan;

.field public final ag:I

.field public final ah:Landroid/graphics/Bitmap;

.field public final ai:Landroid/graphics/Bitmap;

.field public final aj:Landroid/graphics/Bitmap;

.field public final ak:Landroid/graphics/drawable/NinePatchDrawable;

.field public final al:Landroid/graphics/drawable/NinePatchDrawable;

.field public final am:Landroid/graphics/drawable/ShapeDrawable;

.field public final an:I

.field public final ao:I

.field public final ap:Landroid/graphics/Bitmap;

.field public final aq:Landroid/graphics/Bitmap;

.field public final ar:Landroid/graphics/Bitmap;

.field public final as:I

.field public final at:I

.field public final au:Landroid/graphics/Bitmap;

.field public final av:Landroid/graphics/Bitmap;

.field public final aw:Landroid/graphics/Bitmap;

.field public final ax:I

.field public final ay:Landroid/graphics/Bitmap;

.field public final az:I

.field public final b:Landroid/view/animation/AccelerateInterpolator;

.field public final ba:I

.field public final bb:I

.field public final bc:Landroid/graphics/Bitmap;

.field public final bd:I

.field public final be:I

.field public final bf:I

.field public final bg:I

.field public final bh:I

.field public final bi:Landroid/graphics/Paint;

.field public final bj:I

.field public final bk:I

.field public final bl:I

.field public final bm:I

.field public final bn:I

.field public final bo:I

.field public final bp:I

.field private br:Landroid/graphics/Paint;

.field private bs:[Landroid/graphics/Paint;

.field private bt:[Landroid/graphics/Paint;

.field public final c:Landroid/view/animation/DecelerateInterpolator;

.field public final d:Lizs;

.field public final e:Landroid/graphics/Bitmap;

.field public final f:Landroid/graphics/Bitmap;

.field public final g:Landroid/graphics/Bitmap;

.field public final h:Landroid/graphics/Bitmap;

.field public final i:Landroid/graphics/Bitmap;

.field public final j:Landroid/graphics/drawable/Drawable;

.field public final k:I

.field public final l:I

.field public final m:I

.field public final n:I

.field public final o:I

.field public final p:[Landroid/graphics/Bitmap;

.field public final q:I

.field public final r:I

.field public final s:Landroid/graphics/Paint;

.field public final t:Landroid/graphics/Paint;

.field public final u:Landroid/graphics/Paint;

.field public final v:Landroid/graphics/Paint;

.field public final w:Landroid/graphics/Paint;

.field public final x:Landroid/graphics/Paint;

.field public final y:Landroid/graphics/drawable/Drawable;

.field public final z:Landroid/graphics/Bitmap;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 11

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 235
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 236
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 238
    invoke-static {}, Lfo;->a()Lfo;

    move-result-object v0

    iput-object v0, p0, Llct;->a:Lfo;

    .line 240
    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    iput-object v0, p0, Llct;->b:Landroid/view/animation/AccelerateInterpolator;

    .line 241
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    iput-object v0, p0, Llct;->c:Landroid/view/animation/DecelerateInterpolator;

    .line 243
    const-class v0, Lizs;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizs;

    iput-object v0, p0, Llct;->d:Lizs;

    .line 245
    const v0, 0x7f02018f

    invoke-static {v1, v0}, Llct;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Llct;->e:Landroid/graphics/Bitmap;

    .line 249
    const v0, 0x7f02046b

    invoke-static {v1, v0}, Llct;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    .line 250
    const v0, 0x7f02046f

    invoke-static {v1, v0}, Llct;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    .line 251
    const v0, 0x7f020477

    invoke-static {v1, v0}, Llct;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Llct;->f:Landroid/graphics/Bitmap;

    .line 252
    const v0, 0x7f020480

    invoke-static {v1, v0}, Llct;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Llct;->g:Landroid/graphics/Bitmap;

    .line 253
    const v0, 0x7f0203e4

    invoke-static {v1, v0}, Llct;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    .line 254
    const v0, 0x7f020185

    invoke-static {v1, v0}, Llct;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    .line 255
    const v0, 0x7f02055b

    invoke-static {v1, v0}, Llct;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Llct;->h:Landroid/graphics/Bitmap;

    .line 256
    const v0, 0x7f020510

    invoke-static {v1, v0}, Llct;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Llct;->i:Landroid/graphics/Bitmap;

    .line 258
    const v0, 0x7f020047

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Llct;->j:Landroid/graphics/drawable/Drawable;

    .line 260
    const v0, 0x7f0d0199

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Llct;->k:I

    .line 261
    const v0, 0x7f0d019a

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Llct;->l:I

    .line 263
    const v0, 0x7f0d019b

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Llct;->m:I

    .line 264
    const v0, 0x7f0d01a6

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Llct;->o:I

    .line 265
    const v0, 0x7f0d01a4

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Llct;->n:I

    .line 268
    new-array v0, v7, [Landroid/graphics/Bitmap;

    const v2, 0x7f0202a5

    .line 269
    invoke-static {v1, v2}, Llct;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    aput-object v2, v0, v8

    const v2, 0x7f02027c

    .line 270
    invoke-static {v1, v2}, Llct;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    aput-object v2, v0, v6

    iput-object v0, p0, Llct;->p:[Landroid/graphics/Bitmap;

    .line 271
    const v0, 0x7f0d01cb

    .line 272
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Llct;->q:I

    .line 273
    const v0, 0x7f0d01cc

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Llct;->r:I

    .line 276
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Llct;->s:Landroid/graphics/Paint;

    .line 277
    iget-object v0, p0, Llct;->s:Landroid/graphics/Paint;

    const v2, 0x7f0b010d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 279
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Llct;->t:Landroid/graphics/Paint;

    .line 280
    iget-object v0, p0, Llct;->t:Landroid/graphics/Paint;

    const v2, 0x7f0b010e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 282
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Llct;->br:Landroid/graphics/Paint;

    .line 283
    iget-object v0, p0, Llct;->br:Landroid/graphics/Paint;

    const v2, 0x7f0b0112

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 285
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Llct;->u:Landroid/graphics/Paint;

    .line 286
    iget-object v0, p0, Llct;->u:Landroid/graphics/Paint;

    const v2, 0x7f0b0114

    .line 287
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 286
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 289
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Llct;->v:Landroid/graphics/Paint;

    .line 290
    iget-object v0, p0, Llct;->v:Landroid/graphics/Paint;

    const v2, 0x7f0b0113

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 291
    iget-object v0, p0, Llct;->v:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 292
    iget-object v0, p0, Llct;->v:Landroid/graphics/Paint;

    const v2, 0x7f0d01ad

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 295
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Llct;->w:Landroid/graphics/Paint;

    .line 296
    iget-object v0, p0, Llct;->w:Landroid/graphics/Paint;

    const v2, 0x7f0b010f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 297
    iget-object v0, p0, Llct;->w:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 298
    iget-object v0, p0, Llct;->w:Landroid/graphics/Paint;

    const v2, 0x7f0d019c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 300
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Llct;->x:Landroid/graphics/Paint;

    .line 301
    iget-object v0, p0, Llct;->x:Landroid/graphics/Paint;

    const v2, 0x7f0b0110

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 302
    iget-object v0, p0, Llct;->x:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 303
    iget-object v0, p0, Llct;->x:Landroid/graphics/Paint;

    const v2, 0x7f0d019c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 305
    const v0, 0x7f020415

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Llct;->y:Landroid/graphics/drawable/Drawable;

    .line 307
    const v0, 0x7f0201a7

    invoke-static {v1, v0}, Llct;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Llct;->z:Landroid/graphics/Bitmap;

    .line 310
    new-instance v0, Landroid/graphics/Rect;

    const v2, 0x7f0d019e

    .line 311
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    const v3, 0x7f0d01a0

    .line 312
    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    const v4, 0x7f0d019f

    .line 313
    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    const v5, 0x7f0d01a1

    .line 314
    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    invoke-direct {v0, v2, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Llct;->A:Landroid/graphics/Rect;

    .line 315
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Llct;->B:Landroid/graphics/Rect;

    .line 317
    const v0, 0x7f0d0244

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Llct;->C:I

    .line 319
    iget v0, p0, Llct;->C:I

    .line 320
    const v0, 0x7f0d0245

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Llct;->D:I

    .line 322
    iget v0, p0, Llct;->D:I

    .line 324
    const v0, 0x7f0d01a7

    .line 325
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Llct;->E:I

    .line 327
    const v0, 0x7f0d01ae

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Llct;->F:I

    .line 328
    const v0, 0x7f0d01af

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Llct;->G:I

    .line 330
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Llct;->H:Landroid/graphics/Rect;

    .line 332
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v7}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Llct;->I:Landroid/graphics/Paint;

    .line 335
    const v0, 0x7f0d01b0

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Llct;->J:I

    .line 336
    const v0, 0x7f020267

    invoke-static {v1, v0}, Llct;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Llct;->K:Landroid/graphics/Bitmap;

    .line 338
    const v0, 0x7f02026b

    invoke-static {v1, v0}, Llct;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Llct;->L:Landroid/graphics/Bitmap;

    .line 339
    const v0, 0x7f020135

    invoke-static {v1, v0}, Llct;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Llct;->M:Landroid/graphics/Bitmap;

    .line 340
    const v0, 0x7f0c0050

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Llct;->N:I

    .line 341
    const v0, 0x7f0b0115

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Llct;->O:I

    .line 342
    const v0, 0x7f0d01b1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Llct;->P:I

    .line 343
    const v0, 0x7f0d01b2

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Llct;->Q:I

    .line 345
    const v0, 0x7f0d01b3

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Llct;->R:I

    .line 347
    const v0, 0x7f0d01b4

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Llct;->S:I

    .line 349
    const v0, 0x7f0c0043

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Llct;->T:I

    .line 351
    const v0, 0x7f0c0044

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Llct;->U:I

    .line 354
    const v0, 0x7f0c0051

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Llct;->V:I

    .line 355
    const v0, 0x7f0c0052

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Llct;->W:I

    .line 356
    const v0, 0x7f0c0053

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Llct;->X:I

    .line 359
    const v0, 0x7f0c0054

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Llct;->Y:I

    .line 360
    const v0, 0x7f0c0055

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Llct;->Z:I

    .line 363
    const v0, 0x7f020195

    invoke-static {v1, v0}, Llct;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Llct;->aa:Landroid/graphics/Bitmap;

    .line 366
    const v0, 0x7f0d01a8

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Llct;->ab:I

    .line 367
    const v0, 0x7f0d01a9

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Llct;->ac:I

    .line 369
    const v0, 0x7f020475

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/NinePatchDrawable;

    iput-object v0, p0, Llct;->ad:Landroid/graphics/drawable/NinePatchDrawable;

    .line 373
    const v0, 0x7f0d01aa

    .line 374
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Llct;->ae:I

    .line 376
    new-instance v0, Landroid/text/style/StyleSpan;

    invoke-direct {v0, v6}, Landroid/text/style/StyleSpan;-><init>(I)V

    iput-object v0, p0, Llct;->af:Landroid/text/style/StyleSpan;

    .line 378
    const v0, 0x7f0c004f

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    .line 380
    const v0, 0x7f0c004d

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Llct;->ag:I

    .line 382
    new-array v0, v10, [Landroid/graphics/Paint;

    iput-object v0, p0, Llct;->bs:[Landroid/graphics/Paint;

    .line 383
    iget-object v0, p0, Llct;->bs:[Landroid/graphics/Paint;

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    aput-object v2, v0, v8

    .line 384
    iget-object v0, p0, Llct;->bs:[Landroid/graphics/Paint;

    aget-object v0, v0, v8

    const v2, 0x7f0b0104

    .line 385
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 384
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 386
    iget-object v0, p0, Llct;->bs:[Landroid/graphics/Paint;

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    aput-object v2, v0, v6

    .line 387
    iget-object v0, p0, Llct;->bs:[Landroid/graphics/Paint;

    aget-object v0, v0, v6

    const v2, 0x7f0b0105

    .line 388
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 387
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 389
    iget-object v0, p0, Llct;->bs:[Landroid/graphics/Paint;

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    aput-object v2, v0, v7

    .line 390
    iget-object v0, p0, Llct;->bs:[Landroid/graphics/Paint;

    aget-object v0, v0, v7

    const v2, 0x7f0b0106

    .line 391
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 390
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 393
    new-array v0, v10, [Landroid/graphics/Paint;

    iput-object v0, p0, Llct;->bt:[Landroid/graphics/Paint;

    .line 394
    iget-object v0, p0, Llct;->bt:[Landroid/graphics/Paint;

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    aput-object v2, v0, v8

    .line 395
    iget-object v0, p0, Llct;->bt:[Landroid/graphics/Paint;

    aget-object v0, v0, v8

    const v2, 0x7f0b0107

    .line 396
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 395
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 397
    iget-object v0, p0, Llct;->bt:[Landroid/graphics/Paint;

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    aput-object v2, v0, v6

    .line 398
    iget-object v0, p0, Llct;->bt:[Landroid/graphics/Paint;

    aget-object v0, v0, v6

    const v2, 0x7f0b0108

    .line 399
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 398
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 400
    iget-object v0, p0, Llct;->bt:[Landroid/graphics/Paint;

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    aput-object v2, v0, v7

    .line 401
    iget-object v0, p0, Llct;->bt:[Landroid/graphics/Paint;

    aget-object v0, v0, v7

    const v2, 0x7f0b0109

    .line 402
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 401
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 404
    const v0, 0x7f0d019d

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    .line 406
    const v0, 0x7f020557

    invoke-static {v1, v0}, Llct;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Llct;->ah:Landroid/graphics/Bitmap;

    .line 407
    const v0, 0x7f020556

    invoke-static {v1, v0}, Llct;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Llct;->ai:Landroid/graphics/Bitmap;

    .line 409
    const v0, 0x7f020555

    invoke-static {v1, v0}, Llct;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Llct;->aj:Landroid/graphics/Bitmap;

    .line 411
    const v0, 0x7f020086

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/NinePatchDrawable;

    iput-object v0, p0, Llct;->ak:Landroid/graphics/drawable/NinePatchDrawable;

    .line 413
    const v0, 0x7f0200a6

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/NinePatchDrawable;

    iput-object v0, p0, Llct;->al:Landroid/graphics/drawable/NinePatchDrawable;

    .line 416
    const v0, 0x7f0d01b5

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    .line 417
    new-instance v2, Landroid/graphics/drawable/ShapeDrawable;

    new-instance v3, Landroid/graphics/drawable/shapes/RoundRectShape;

    const/16 v4, 0x8

    new-array v4, v4, [F

    aput v0, v4, v8

    aput v0, v4, v6

    aput v0, v4, v7

    aput v0, v4, v10

    const/4 v0, 0x4

    aput v9, v4, v0

    const/4 v0, 0x5

    aput v9, v4, v0

    const/4 v0, 0x6

    aput v9, v4, v0

    const/4 v0, 0x7

    aput v9, v4, v0

    const/4 v0, 0x0

    const/4 v5, 0x0

    invoke-direct {v3, v4, v0, v5}, Landroid/graphics/drawable/shapes/RoundRectShape;-><init>([FLandroid/graphics/RectF;[F)V

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    iput-object v2, p0, Llct;->am:Landroid/graphics/drawable/ShapeDrawable;

    .line 421
    const v0, 0x7f0d01a5

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Llct;->ao:I

    .line 423
    const v0, 0x7f0b0116

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Llct;->an:I

    .line 424
    const v0, 0x7f020196

    invoke-static {v1, v0}, Llct;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Llct;->ap:Landroid/graphics/Bitmap;

    .line 425
    const v0, 0x7f020193

    invoke-static {v1, v0}, Llct;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Llct;->aq:Landroid/graphics/Bitmap;

    .line 426
    const v0, 0x7f0201a8

    invoke-static {v1, v0}, Llct;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Llct;->ar:Landroid/graphics/Bitmap;

    .line 427
    const v0, 0x7f0c0056

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Llct;->as:I

    .line 430
    const v0, 0x7f0b0117

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Llct;->at:I

    .line 431
    const v0, 0x7f0201c2

    invoke-static {v1, v0}, Llct;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Llct;->au:Landroid/graphics/Bitmap;

    .line 433
    const v0, 0x7f0201bf

    invoke-static {v1, v0}, Llct;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Llct;->av:Landroid/graphics/Bitmap;

    .line 435
    const v0, 0x7f0203a6

    invoke-static {v1, v0}, Llct;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Llct;->aw:Landroid/graphics/Bitmap;

    .line 437
    const v0, 0x7f0b0118

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Llct;->ax:I

    .line 439
    const v0, 0x7f020160

    invoke-static {v1, v0}, Llct;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Llct;->ay:Landroid/graphics/Bitmap;

    .line 441
    const v0, 0x106000b

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Llct;->az:I

    .line 443
    const v0, 0x7f0d01ab

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Llct;->aA:I

    .line 444
    const v0, 0x7f0d01ac

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Llct;->aB:I

    .line 446
    const v0, 0x7f0d01b6

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Llct;->aC:I

    .line 448
    const v0, 0x7f020063

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/NinePatchDrawable;

    iput-object v0, p0, Llct;->aD:Landroid/graphics/drawable/NinePatchDrawable;

    .line 450
    const v0, 0x7f0c0045

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Llct;->aE:I

    .line 453
    const v0, 0x106000b

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    .line 455
    const v0, 0x7f0d01b8

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Llct;->aF:I

    .line 457
    const v0, 0x7f0d01b7

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Llct;->aG:I

    .line 460
    const v0, 0x7f0d01b9

    .line 461
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Llct;->aH:I

    .line 462
    const v0, 0x7f0d01ba

    .line 463
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Llct;->aI:I

    .line 464
    const v0, 0x7f0d01bb

    .line 465
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Llct;->aJ:F

    .line 466
    const v0, 0x7f0d01bc

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Llct;->aK:F

    .line 467
    const v0, 0x7f0d01bd

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Llct;->aL:F

    .line 469
    const v0, 0x7f0203f6

    invoke-static {v1, v0}, Llct;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Llct;->aM:Landroid/graphics/Bitmap;

    .line 470
    const v0, 0x7f0203f3

    invoke-static {v1, v0}, Llct;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Llct;->aN:Landroid/graphics/Bitmap;

    .line 471
    const v0, 0x7f0203f1

    invoke-static {v1, v0}, Llct;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Llct;->aO:Landroid/graphics/Bitmap;

    .line 473
    const v0, 0x7f0c0057

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Llct;->aP:I

    .line 474
    const v0, 0x7f0c0058

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Llct;->aQ:I

    .line 475
    const v0, 0x7f0c0059

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Llct;->aS:I

    .line 476
    const v0, 0x7f0c005a

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Llct;->aR:I

    .line 478
    const v0, 0x7f0d01be

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Llct;->aT:I

    .line 479
    const v0, 0x7f0d01c1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Llct;->aU:I

    .line 481
    const v0, 0x7f0d01bf

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Llct;->aV:I

    .line 483
    const v0, 0x7f0d01c0

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Llct;->aW:I

    .line 485
    const v0, 0x7f0d01c2

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Llct;->aX:I

    .line 487
    const v0, 0x7f0d01c3

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Llct;->aY:I

    .line 490
    const v0, 0x7f0b011c

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    .line 492
    const v0, 0x7f0d01ea

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Llct;->aZ:I

    .line 494
    const v0, 0x7f0d0187

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Llct;->ba:I

    .line 496
    invoke-static {p1}, Llsg;->a(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Llct;->bb:I

    .line 498
    const v0, 0x7f020473

    invoke-static {v1, v0}, Llct;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Llct;->bc:Landroid/graphics/Bitmap;

    .line 500
    const v0, 0x7f0d01cd

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Llct;->bd:I

    .line 502
    const v0, 0x7f0d01d0

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Llct;->be:I

    .line 503
    const v0, 0x7f0d01cf

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Llct;->bf:I

    .line 505
    const v0, 0x7f0d01df

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Llct;->bg:I

    .line 506
    const v0, 0x7f0b0120

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Llct;->bh:I

    .line 508
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Llct;->bi:Landroid/graphics/Paint;

    .line 509
    iget-object v0, p0, Llct;->bi:Landroid/graphics/Paint;

    const v2, 0x7f0b011f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 510
    iget-object v0, p0, Llct;->bi:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 511
    iget-object v0, p0, Llct;->bi:Landroid/graphics/Paint;

    const v2, 0x7f0d01e1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 514
    const v0, 0x7f0d01e0

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Llct;->bk:I

    .line 515
    const v0, 0x7f0d01e2

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Llct;->bl:I

    .line 516
    const v0, 0x7f0d01e3

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Llct;->bm:I

    .line 517
    const v0, 0x7f0d01e4

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Llct;->bj:I

    .line 518
    const v0, 0x7f0d01e5

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Llct;->bn:I

    .line 520
    const v0, 0x7f0c005b

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Llct;->bo:I

    .line 521
    const v0, 0x7f0c005c

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Llct;->bp:I

    .line 522
    return-void
.end method

.method private static a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 527
    :try_start_0
    invoke-static {p0, p1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 530
    :goto_0
    return-object v0

    .line 528
    :catch_0
    move-exception v0

    .line 529
    const-string v1, "ImageUtils"

    const-string v2, "ImageUtils#decodeResource(Resources, int) threw an OOME"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 530
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)Llct;
    .locals 1

    .prologue
    .line 228
    sget-object v0, Llct;->bq:Llct;

    if-nez v0, :cond_0

    .line 229
    new-instance v0, Llct;

    invoke-direct {v0, p0}, Llct;-><init>(Landroid/content/Context;)V

    sput-object v0, Llct;->bq:Llct;

    .line 232
    :cond_0
    sget-object v0, Llct;->bq:Llct;

    return-object v0
.end method
