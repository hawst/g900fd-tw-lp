.class public final Lokl;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lokl;


# instance fields
.field private b:Ljava/lang/Integer;

.field private c:Ljava/lang/Integer;

.field private d:Ljava/lang/Integer;

.field private e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1097
    const/4 v0, 0x0

    new-array v0, v0, [Lokl;

    sput-object v0, Lokl;->a:[Lokl;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1098
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1129
    const/4 v0, 0x0

    .line 1130
    iget-object v1, p0, Lokl;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 1131
    const/4 v0, 0x1

    iget-object v1, p0, Lokl;->b:Ljava/lang/Integer;

    .line 1132
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1134
    :cond_0
    iget-object v1, p0, Lokl;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 1135
    const/4 v1, 0x2

    iget-object v2, p0, Lokl;->c:Ljava/lang/Integer;

    .line 1136
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1138
    :cond_1
    iget-object v1, p0, Lokl;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 1139
    const/4 v1, 0x3

    iget-object v2, p0, Lokl;->d:Ljava/lang/Integer;

    .line 1140
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1142
    :cond_2
    iget-object v1, p0, Lokl;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 1143
    const/4 v1, 0x4

    iget-object v2, p0, Lokl;->e:Ljava/lang/String;

    .line 1144
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1146
    :cond_3
    iget-object v1, p0, Lokl;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1147
    iput v0, p0, Lokl;->ai:I

    .line 1148
    return v0
.end method

.method public a(Loxn;)Lokl;
    .locals 2

    .prologue
    .line 1156
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1157
    sparse-switch v0, :sswitch_data_0

    .line 1161
    iget-object v1, p0, Lokl;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1162
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lokl;->ah:Ljava/util/List;

    .line 1165
    :cond_1
    iget-object v1, p0, Lokl;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1167
    :sswitch_0
    return-object p0

    .line 1172
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lokl;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 1176
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lokl;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 1180
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lokl;->d:Ljava/lang/Integer;

    goto :goto_0

    .line 1184
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lokl;->e:Ljava/lang/String;

    goto :goto_0

    .line 1157
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1111
    iget-object v0, p0, Lokl;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 1112
    const/4 v0, 0x1

    iget-object v1, p0, Lokl;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1114
    :cond_0
    iget-object v0, p0, Lokl;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 1115
    const/4 v0, 0x2

    iget-object v1, p0, Lokl;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1117
    :cond_1
    iget-object v0, p0, Lokl;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 1118
    const/4 v0, 0x3

    iget-object v1, p0, Lokl;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1120
    :cond_2
    iget-object v0, p0, Lokl;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1121
    const/4 v0, 0x4

    iget-object v1, p0, Lokl;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1123
    :cond_3
    iget-object v0, p0, Lokl;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1125
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1094
    invoke-virtual {p0, p1}, Lokl;->a(Loxn;)Lokl;

    move-result-object v0

    return-object v0
.end method
