.class public final Lnvq;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/Long;

.field public e:I

.field public f:Logw;

.field private g:Loep;

.field private h:I

.field private i:Ljava/lang/String;

.field private j:Logy;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/high16 v1, -0x80000000

    const/4 v0, 0x0

    .line 1541
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1552
    iput-object v0, p0, Lnvq;->g:Loep;

    .line 1555
    iput v1, p0, Lnvq;->e:I

    .line 1558
    iput v1, p0, Lnvq;->h:I

    .line 1563
    iput-object v0, p0, Lnvq;->j:Logy;

    .line 1566
    iput-object v0, p0, Lnvq;->f:Logw;

    .line 1541
    return-void
.end method


# virtual methods
.method public a()I
    .locals 5

    .prologue
    const/high16 v4, -0x80000000

    .line 1607
    const/4 v0, 0x0

    .line 1608
    iget-object v1, p0, Lnvq;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1609
    const/4 v0, 0x1

    iget-object v1, p0, Lnvq;->a:Ljava/lang/String;

    .line 1610
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1612
    :cond_0
    iget-object v1, p0, Lnvq;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1613
    const/4 v1, 0x2

    iget-object v2, p0, Lnvq;->b:Ljava/lang/String;

    .line 1614
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1616
    :cond_1
    iget-object v1, p0, Lnvq;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1617
    const/4 v1, 0x3

    iget-object v2, p0, Lnvq;->c:Ljava/lang/String;

    .line 1618
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1620
    :cond_2
    iget-object v1, p0, Lnvq;->d:Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 1621
    const/4 v1, 0x4

    iget-object v2, p0, Lnvq;->d:Ljava/lang/Long;

    .line 1622
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1624
    :cond_3
    iget-object v1, p0, Lnvq;->g:Loep;

    if-eqz v1, :cond_4

    .line 1625
    const/4 v1, 0x5

    iget-object v2, p0, Lnvq;->g:Loep;

    .line 1626
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1628
    :cond_4
    iget v1, p0, Lnvq;->e:I

    if-eq v1, v4, :cond_5

    .line 1629
    const/4 v1, 0x6

    iget v2, p0, Lnvq;->e:I

    .line 1630
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1632
    :cond_5
    iget v1, p0, Lnvq;->h:I

    if-eq v1, v4, :cond_6

    .line 1633
    const/4 v1, 0x7

    iget v2, p0, Lnvq;->h:I

    .line 1634
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1636
    :cond_6
    iget-object v1, p0, Lnvq;->i:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 1637
    const/16 v1, 0x8

    iget-object v2, p0, Lnvq;->i:Ljava/lang/String;

    .line 1638
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1640
    :cond_7
    iget-object v1, p0, Lnvq;->j:Logy;

    if-eqz v1, :cond_8

    .line 1641
    const/16 v1, 0x9

    iget-object v2, p0, Lnvq;->j:Logy;

    .line 1642
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1644
    :cond_8
    iget-object v1, p0, Lnvq;->f:Logw;

    if-eqz v1, :cond_9

    .line 1645
    const/16 v1, 0xa

    iget-object v2, p0, Lnvq;->f:Logw;

    .line 1646
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1648
    :cond_9
    iget-object v1, p0, Lnvq;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1649
    iput v0, p0, Lnvq;->ai:I

    .line 1650
    return v0
.end method

.method public a(Loxn;)Lnvq;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1658
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1659
    sparse-switch v0, :sswitch_data_0

    .line 1663
    iget-object v1, p0, Lnvq;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1664
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnvq;->ah:Ljava/util/List;

    .line 1667
    :cond_1
    iget-object v1, p0, Lnvq;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1669
    :sswitch_0
    return-object p0

    .line 1674
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnvq;->a:Ljava/lang/String;

    goto :goto_0

    .line 1678
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnvq;->b:Ljava/lang/String;

    goto :goto_0

    .line 1682
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnvq;->c:Ljava/lang/String;

    goto :goto_0

    .line 1686
    :sswitch_4
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lnvq;->d:Ljava/lang/Long;

    goto :goto_0

    .line 1690
    :sswitch_5
    iget-object v0, p0, Lnvq;->g:Loep;

    if-nez v0, :cond_2

    .line 1691
    new-instance v0, Loep;

    invoke-direct {v0}, Loep;-><init>()V

    iput-object v0, p0, Lnvq;->g:Loep;

    .line 1693
    :cond_2
    iget-object v0, p0, Lnvq;->g:Loep;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1697
    :sswitch_6
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1698
    if-eqz v0, :cond_3

    if-eq v0, v3, :cond_3

    if-ne v0, v4, :cond_4

    .line 1701
    :cond_3
    iput v0, p0, Lnvq;->e:I

    goto :goto_0

    .line 1703
    :cond_4
    iput v2, p0, Lnvq;->e:I

    goto :goto_0

    .line 1708
    :sswitch_7
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1709
    if-eqz v0, :cond_5

    if-eq v0, v3, :cond_5

    if-eq v0, v4, :cond_5

    const/16 v1, 0xe

    if-eq v0, v1, :cond_5

    const/16 v1, 0xa

    if-eq v0, v1, :cond_5

    const/16 v1, 0xb

    if-eq v0, v1, :cond_5

    const/4 v1, 0x3

    if-eq v0, v1, :cond_5

    const/4 v1, 0x4

    if-eq v0, v1, :cond_5

    const/4 v1, 0x5

    if-eq v0, v1, :cond_5

    const/4 v1, 0x6

    if-eq v0, v1, :cond_5

    const/4 v1, 0x7

    if-eq v0, v1, :cond_5

    const/16 v1, 0x9

    if-eq v0, v1, :cond_5

    const/16 v1, 0xc

    if-eq v0, v1, :cond_5

    const/16 v1, 0xd

    if-eq v0, v1, :cond_5

    const/16 v1, 0xf

    if-eq v0, v1, :cond_5

    const/16 v1, 0x10

    if-eq v0, v1, :cond_5

    const/16 v1, 0x27

    if-eq v0, v1, :cond_5

    const/16 v1, 0x16

    if-eq v0, v1, :cond_5

    const/16 v1, 0x11

    if-eq v0, v1, :cond_5

    const/16 v1, 0x12

    if-eq v0, v1, :cond_5

    const/16 v1, 0x13

    if-eq v0, v1, :cond_5

    const/16 v1, 0x14

    if-eq v0, v1, :cond_5

    const/16 v1, 0x15

    if-eq v0, v1, :cond_5

    const/16 v1, 0x17

    if-eq v0, v1, :cond_5

    const/16 v1, 0x18

    if-eq v0, v1, :cond_5

    const/16 v1, 0x19

    if-eq v0, v1, :cond_5

    const/16 v1, 0x1a

    if-eq v0, v1, :cond_5

    const/16 v1, 0x1b

    if-eq v0, v1, :cond_5

    const/16 v1, 0x1c

    if-eq v0, v1, :cond_5

    const/16 v1, 0x1d

    if-eq v0, v1, :cond_5

    const/16 v1, 0x1e

    if-eq v0, v1, :cond_5

    const/16 v1, 0x1f

    if-eq v0, v1, :cond_5

    const/16 v1, 0x2c

    if-eq v0, v1, :cond_5

    const/16 v1, 0x2d

    if-eq v0, v1, :cond_5

    const/16 v1, 0x31

    if-eq v0, v1, :cond_5

    const/16 v1, 0x32

    if-eq v0, v1, :cond_5

    const/16 v1, 0x22

    if-eq v0, v1, :cond_5

    const/16 v1, 0x2e

    if-eq v0, v1, :cond_5

    const/16 v1, 0x20

    if-eq v0, v1, :cond_5

    const/16 v1, 0x21

    if-eq v0, v1, :cond_5

    const/16 v1, 0x23

    if-eq v0, v1, :cond_5

    const/16 v1, 0x24

    if-eq v0, v1, :cond_5

    const/16 v1, 0x25

    if-eq v0, v1, :cond_5

    const/16 v1, 0x26

    if-eq v0, v1, :cond_5

    const/16 v1, 0x28

    if-eq v0, v1, :cond_5

    const/16 v1, 0x29

    if-eq v0, v1, :cond_5

    const/16 v1, 0x2a

    if-eq v0, v1, :cond_5

    const/16 v1, 0x2b

    if-eq v0, v1, :cond_5

    const/16 v1, 0x2f

    if-eq v0, v1, :cond_5

    const/16 v1, 0x30

    if-eq v0, v1, :cond_5

    const/16 v1, 0x33

    if-eq v0, v1, :cond_5

    const/16 v1, 0x34

    if-ne v0, v1, :cond_6

    .line 1761
    :cond_5
    iput v0, p0, Lnvq;->h:I

    goto/16 :goto_0

    .line 1763
    :cond_6
    iput v2, p0, Lnvq;->h:I

    goto/16 :goto_0

    .line 1768
    :sswitch_8
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnvq;->i:Ljava/lang/String;

    goto/16 :goto_0

    .line 1772
    :sswitch_9
    iget-object v0, p0, Lnvq;->j:Logy;

    if-nez v0, :cond_7

    .line 1773
    new-instance v0, Logy;

    invoke-direct {v0}, Logy;-><init>()V

    iput-object v0, p0, Lnvq;->j:Logy;

    .line 1775
    :cond_7
    iget-object v0, p0, Lnvq;->j:Logy;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1779
    :sswitch_a
    iget-object v0, p0, Lnvq;->f:Logw;

    if-nez v0, :cond_8

    .line 1780
    new-instance v0, Logw;

    invoke-direct {v0}, Logw;-><init>()V

    iput-object v0, p0, Lnvq;->f:Logw;

    .line 1782
    :cond_8
    iget-object v0, p0, Lnvq;->f:Logw;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1659
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    const/high16 v4, -0x80000000

    .line 1571
    iget-object v0, p0, Lnvq;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1572
    const/4 v0, 0x1

    iget-object v1, p0, Lnvq;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1574
    :cond_0
    iget-object v0, p0, Lnvq;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1575
    const/4 v0, 0x2

    iget-object v1, p0, Lnvq;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1577
    :cond_1
    iget-object v0, p0, Lnvq;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1578
    const/4 v0, 0x3

    iget-object v1, p0, Lnvq;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1580
    :cond_2
    iget-object v0, p0, Lnvq;->d:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 1581
    const/4 v0, 0x4

    iget-object v1, p0, Lnvq;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 1583
    :cond_3
    iget-object v0, p0, Lnvq;->g:Loep;

    if-eqz v0, :cond_4

    .line 1584
    const/4 v0, 0x5

    iget-object v1, p0, Lnvq;->g:Loep;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1586
    :cond_4
    iget v0, p0, Lnvq;->e:I

    if-eq v0, v4, :cond_5

    .line 1587
    const/4 v0, 0x6

    iget v1, p0, Lnvq;->e:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1589
    :cond_5
    iget v0, p0, Lnvq;->h:I

    if-eq v0, v4, :cond_6

    .line 1590
    const/4 v0, 0x7

    iget v1, p0, Lnvq;->h:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1592
    :cond_6
    iget-object v0, p0, Lnvq;->i:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 1593
    const/16 v0, 0x8

    iget-object v1, p0, Lnvq;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1595
    :cond_7
    iget-object v0, p0, Lnvq;->j:Logy;

    if-eqz v0, :cond_8

    .line 1596
    const/16 v0, 0x9

    iget-object v1, p0, Lnvq;->j:Logy;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1598
    :cond_8
    iget-object v0, p0, Lnvq;->f:Logw;

    if-eqz v0, :cond_9

    .line 1599
    const/16 v0, 0xa

    iget-object v1, p0, Lnvq;->f:Logw;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1601
    :cond_9
    iget-object v0, p0, Lnvq;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1603
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1537
    invoke-virtual {p0, p1}, Lnvq;->a(Loxn;)Lnvq;

    move-result-object v0

    return-object v0
.end method
