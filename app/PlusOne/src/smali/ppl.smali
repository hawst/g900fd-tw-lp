.class public final Lppl;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lppl;


# instance fields
.field private b:Lppf;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 234
    const/4 v0, 0x0

    new-array v0, v0, [Lppl;

    sput-object v0, Lppl;->a:[Lppl;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 235
    invoke-direct {p0}, Loxq;-><init>()V

    .line 238
    const/4 v0, 0x0

    iput-object v0, p0, Lppl;->b:Lppf;

    .line 235
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 262
    const/4 v0, 0x0

    .line 263
    iget-object v1, p0, Lppl;->b:Lppf;

    if-eqz v1, :cond_0

    .line 264
    const/4 v0, 0x1

    iget-object v1, p0, Lppl;->b:Lppf;

    .line 265
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 267
    :cond_0
    iget-object v1, p0, Lppl;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 268
    const/4 v1, 0x2

    iget-object v2, p0, Lppl;->c:Ljava/lang/String;

    .line 269
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 271
    :cond_1
    iget-object v1, p0, Lppl;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 272
    const/4 v1, 0x3

    iget-object v2, p0, Lppl;->d:Ljava/lang/String;

    .line 273
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 275
    :cond_2
    iget-object v1, p0, Lppl;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 276
    iput v0, p0, Lppl;->ai:I

    .line 277
    return v0
.end method

.method public a(Loxn;)Lppl;
    .locals 2

    .prologue
    .line 285
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 286
    sparse-switch v0, :sswitch_data_0

    .line 290
    iget-object v1, p0, Lppl;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 291
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lppl;->ah:Ljava/util/List;

    .line 294
    :cond_1
    iget-object v1, p0, Lppl;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 296
    :sswitch_0
    return-object p0

    .line 301
    :sswitch_1
    iget-object v0, p0, Lppl;->b:Lppf;

    if-nez v0, :cond_2

    .line 302
    new-instance v0, Lppf;

    invoke-direct {v0}, Lppf;-><init>()V

    iput-object v0, p0, Lppl;->b:Lppf;

    .line 304
    :cond_2
    iget-object v0, p0, Lppl;->b:Lppf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 308
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lppl;->c:Ljava/lang/String;

    goto :goto_0

    .line 312
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lppl;->d:Ljava/lang/String;

    goto :goto_0

    .line 286
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 247
    iget-object v0, p0, Lppl;->b:Lppf;

    if-eqz v0, :cond_0

    .line 248
    const/4 v0, 0x1

    iget-object v1, p0, Lppl;->b:Lppf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 250
    :cond_0
    iget-object v0, p0, Lppl;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 251
    const/4 v0, 0x2

    iget-object v1, p0, Lppl;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 253
    :cond_1
    iget-object v0, p0, Lppl;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 254
    const/4 v0, 0x3

    iget-object v1, p0, Lppl;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 256
    :cond_2
    iget-object v0, p0, Lppl;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 258
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 231
    invoke-virtual {p0, p1}, Lppl;->a(Loxn;)Lppl;

    move-result-object v0

    return-object v0
.end method
