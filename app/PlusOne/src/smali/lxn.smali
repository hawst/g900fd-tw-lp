.class public final Llxn;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Llxn;


# instance fields
.field public b:Ljava/lang/String;

.field private c:Llxp;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:I

.field private l:Ljava/lang/Long;

.field private m:Ljava/lang/Long;

.field private n:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3274
    const/4 v0, 0x0

    new-array v0, v0, [Llxn;

    sput-object v0, Llxn;->a:[Llxn;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3275
    invoke-direct {p0}, Loxq;-><init>()V

    .line 3284
    const/4 v0, 0x0

    iput-object v0, p0, Llxn;->c:Llxp;

    .line 3303
    const/high16 v0, -0x80000000

    iput v0, p0, Llxn;->k:I

    .line 3275
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 3359
    const/4 v0, 0x0

    .line 3360
    iget-object v1, p0, Llxn;->c:Llxp;

    if-eqz v1, :cond_0

    .line 3361
    const/4 v0, 0x1

    iget-object v1, p0, Llxn;->c:Llxp;

    .line 3362
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3364
    :cond_0
    iget-object v1, p0, Llxn;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 3365
    const/4 v1, 0x2

    iget-object v2, p0, Llxn;->b:Ljava/lang/String;

    .line 3366
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3368
    :cond_1
    iget-object v1, p0, Llxn;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 3369
    const/4 v1, 0x3

    iget-object v2, p0, Llxn;->d:Ljava/lang/String;

    .line 3370
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3372
    :cond_2
    iget-object v1, p0, Llxn;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 3373
    const/4 v1, 0x4

    iget-object v2, p0, Llxn;->e:Ljava/lang/String;

    .line 3374
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3376
    :cond_3
    iget-object v1, p0, Llxn;->f:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 3377
    const/4 v1, 0x5

    iget-object v2, p0, Llxn;->f:Ljava/lang/String;

    .line 3378
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3380
    :cond_4
    iget-object v1, p0, Llxn;->g:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 3381
    const/4 v1, 0x6

    iget-object v2, p0, Llxn;->g:Ljava/lang/String;

    .line 3382
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3384
    :cond_5
    iget-object v1, p0, Llxn;->h:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 3385
    const/4 v1, 0x7

    iget-object v2, p0, Llxn;->h:Ljava/lang/String;

    .line 3386
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3388
    :cond_6
    iget-object v1, p0, Llxn;->i:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 3389
    const/16 v1, 0x8

    iget-object v2, p0, Llxn;->i:Ljava/lang/String;

    .line 3390
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3392
    :cond_7
    iget-object v1, p0, Llxn;->j:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 3393
    const/16 v1, 0x9

    iget-object v2, p0, Llxn;->j:Ljava/lang/String;

    .line 3394
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3396
    :cond_8
    iget v1, p0, Llxn;->k:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_9

    .line 3397
    const/16 v1, 0xa

    iget v2, p0, Llxn;->k:I

    .line 3398
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3400
    :cond_9
    iget-object v1, p0, Llxn;->l:Ljava/lang/Long;

    if-eqz v1, :cond_a

    .line 3401
    const/16 v1, 0xb

    iget-object v2, p0, Llxn;->l:Ljava/lang/Long;

    .line 3402
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3404
    :cond_a
    iget-object v1, p0, Llxn;->m:Ljava/lang/Long;

    if-eqz v1, :cond_b

    .line 3405
    const/16 v1, 0xc

    iget-object v2, p0, Llxn;->m:Ljava/lang/Long;

    .line 3406
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3408
    :cond_b
    iget-object v1, p0, Llxn;->n:Ljava/lang/Boolean;

    if-eqz v1, :cond_c

    .line 3409
    const/16 v1, 0xd

    iget-object v2, p0, Llxn;->n:Ljava/lang/Boolean;

    .line 3410
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3412
    :cond_c
    iget-object v1, p0, Llxn;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3413
    iput v0, p0, Llxn;->ai:I

    .line 3414
    return v0
.end method

.method public a(Loxn;)Llxn;
    .locals 2

    .prologue
    .line 3422
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3423
    sparse-switch v0, :sswitch_data_0

    .line 3427
    iget-object v1, p0, Llxn;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 3428
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llxn;->ah:Ljava/util/List;

    .line 3431
    :cond_1
    iget-object v1, p0, Llxn;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3433
    :sswitch_0
    return-object p0

    .line 3438
    :sswitch_1
    iget-object v0, p0, Llxn;->c:Llxp;

    if-nez v0, :cond_2

    .line 3439
    new-instance v0, Llxp;

    invoke-direct {v0}, Llxp;-><init>()V

    iput-object v0, p0, Llxn;->c:Llxp;

    .line 3441
    :cond_2
    iget-object v0, p0, Llxn;->c:Llxp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3445
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llxn;->b:Ljava/lang/String;

    goto :goto_0

    .line 3449
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llxn;->d:Ljava/lang/String;

    goto :goto_0

    .line 3453
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llxn;->e:Ljava/lang/String;

    goto :goto_0

    .line 3457
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llxn;->f:Ljava/lang/String;

    goto :goto_0

    .line 3461
    :sswitch_6
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llxn;->g:Ljava/lang/String;

    goto :goto_0

    .line 3465
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llxn;->h:Ljava/lang/String;

    goto :goto_0

    .line 3469
    :sswitch_8
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llxn;->i:Ljava/lang/String;

    goto :goto_0

    .line 3473
    :sswitch_9
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llxn;->j:Ljava/lang/String;

    goto :goto_0

    .line 3477
    :sswitch_a
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 3478
    if-eqz v0, :cond_3

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    .line 3481
    :cond_3
    iput v0, p0, Llxn;->k:I

    goto :goto_0

    .line 3483
    :cond_4
    const/4 v0, 0x0

    iput v0, p0, Llxn;->k:I

    goto :goto_0

    .line 3488
    :sswitch_b
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Llxn;->l:Ljava/lang/Long;

    goto/16 :goto_0

    .line 3492
    :sswitch_c
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Llxn;->m:Ljava/lang/Long;

    goto/16 :goto_0

    .line 3496
    :sswitch_d
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Llxn;->n:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 3423
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 3314
    iget-object v0, p0, Llxn;->c:Llxp;

    if-eqz v0, :cond_0

    .line 3315
    const/4 v0, 0x1

    iget-object v1, p0, Llxn;->c:Llxp;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 3317
    :cond_0
    iget-object v0, p0, Llxn;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 3318
    const/4 v0, 0x2

    iget-object v1, p0, Llxn;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3320
    :cond_1
    iget-object v0, p0, Llxn;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 3321
    const/4 v0, 0x3

    iget-object v1, p0, Llxn;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3323
    :cond_2
    iget-object v0, p0, Llxn;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 3324
    const/4 v0, 0x4

    iget-object v1, p0, Llxn;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3326
    :cond_3
    iget-object v0, p0, Llxn;->f:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 3327
    const/4 v0, 0x5

    iget-object v1, p0, Llxn;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3329
    :cond_4
    iget-object v0, p0, Llxn;->g:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 3330
    const/4 v0, 0x6

    iget-object v1, p0, Llxn;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3332
    :cond_5
    iget-object v0, p0, Llxn;->h:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 3333
    const/4 v0, 0x7

    iget-object v1, p0, Llxn;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3335
    :cond_6
    iget-object v0, p0, Llxn;->i:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 3336
    const/16 v0, 0x8

    iget-object v1, p0, Llxn;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3338
    :cond_7
    iget-object v0, p0, Llxn;->j:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 3339
    const/16 v0, 0x9

    iget-object v1, p0, Llxn;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3341
    :cond_8
    iget v0, p0, Llxn;->k:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_9

    .line 3342
    const/16 v0, 0xa

    iget v1, p0, Llxn;->k:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 3344
    :cond_9
    iget-object v0, p0, Llxn;->l:Ljava/lang/Long;

    if-eqz v0, :cond_a

    .line 3345
    const/16 v0, 0xb

    iget-object v1, p0, Llxn;->l:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 3347
    :cond_a
    iget-object v0, p0, Llxn;->m:Ljava/lang/Long;

    if-eqz v0, :cond_b

    .line 3348
    const/16 v0, 0xc

    iget-object v1, p0, Llxn;->m:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 3350
    :cond_b
    iget-object v0, p0, Llxn;->n:Ljava/lang/Boolean;

    if-eqz v0, :cond_c

    .line 3351
    const/16 v0, 0xd

    iget-object v1, p0, Llxn;->n:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 3353
    :cond_c
    iget-object v0, p0, Llxn;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3355
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3271
    invoke-virtual {p0, p1}, Llxn;->a(Loxn;)Llxn;

    move-result-object v0

    return-object v0
.end method
