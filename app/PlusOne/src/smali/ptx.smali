.class public final Lptx;
.super Loxq;
.source "PG"


# static fields
.field public static final a:Loxr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Loxr",
            "<",
            "Lptx;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:Lptw;

.field private c:Lptw;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 230
    const v0, 0x2e5756f

    new-instance v1, Lpty;

    invoke-direct {v1}, Lpty;-><init>()V

    .line 235
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    move-result-object v0

    sput-object v0, Lptx;->a:Loxr;

    .line 234
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 258
    const/4 v0, 0x0

    .line 259
    iget-object v1, p0, Lptx;->b:Lptw;

    if-eqz v1, :cond_0

    .line 260
    const/4 v0, 0x1

    iget-object v1, p0, Lptx;->b:Lptw;

    .line 261
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 263
    :cond_0
    iget-object v1, p0, Lptx;->c:Lptw;

    if-eqz v1, :cond_1

    .line 264
    const/4 v1, 0x2

    iget-object v2, p0, Lptx;->c:Lptw;

    .line 265
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 267
    :cond_1
    iget-object v1, p0, Lptx;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 268
    iput v0, p0, Lptx;->ai:I

    .line 269
    return v0
.end method

.method public a(Loxn;)Lptx;
    .locals 2

    .prologue
    .line 277
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 278
    sparse-switch v0, :sswitch_data_0

    .line 282
    iget-object v1, p0, Lptx;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 283
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lptx;->ah:Ljava/util/List;

    .line 286
    :cond_1
    iget-object v1, p0, Lptx;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 288
    :sswitch_0
    return-object p0

    .line 293
    :sswitch_1
    iget-object v0, p0, Lptx;->b:Lptw;

    if-nez v0, :cond_2

    .line 294
    new-instance v0, Lptw;

    invoke-direct {v0}, Lptw;-><init>()V

    iput-object v0, p0, Lptx;->b:Lptw;

    .line 296
    :cond_2
    iget-object v0, p0, Lptx;->b:Lptw;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 300
    :sswitch_2
    iget-object v0, p0, Lptx;->c:Lptw;

    if-nez v0, :cond_3

    .line 301
    new-instance v0, Lptw;

    invoke-direct {v0}, Lptw;-><init>()V

    iput-object v0, p0, Lptx;->c:Lptw;

    .line 303
    :cond_3
    iget-object v0, p0, Lptx;->c:Lptw;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 278
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 246
    iget-object v0, p0, Lptx;->b:Lptw;

    if-eqz v0, :cond_0

    .line 247
    const/4 v0, 0x1

    iget-object v1, p0, Lptx;->b:Lptw;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 249
    :cond_0
    iget-object v0, p0, Lptx;->c:Lptw;

    if-eqz v0, :cond_1

    .line 250
    const/4 v0, 0x2

    iget-object v1, p0, Lptx;->c:Lptw;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 252
    :cond_1
    iget-object v0, p0, Lptx;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 254
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 227
    invoke-virtual {p0, p1}, Lptx;->a(Loxn;)Lptx;

    move-result-object v0

    return-object v0
.end method
