.class public final Lpss;
.super Loxq;
.source "PG"


# static fields
.field public static final a:Loxr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Loxr",
            "<",
            "Lpss;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:Ljava/lang/Float;

.field public c:Ljava/lang/Float;

.field public d:Ljava/lang/Float;

.field public e:Ljava/lang/Float;

.field public f:Ljava/lang/Float;

.field public g:Ljava/lang/Float;

.field public h:Ljava/lang/Float;

.field public i:Ljava/lang/Integer;

.field public j:Ljava/lang/Integer;

.field public k:Ljava/lang/Float;

.field public l:Ljava/lang/Float;

.field public m:Ljava/lang/Float;

.field public n:Ljava/lang/Float;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 9
    const v0, 0x2a5ab17

    new-instance v1, Lpst;

    invoke-direct {v1}, Lpst;-><init>()V

    .line 14
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    move-result-object v0

    sput-object v0, Lpss;->a:Loxr;

    .line 13
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 90
    const/4 v0, 0x0

    .line 91
    iget-object v1, p0, Lpss;->b:Ljava/lang/Float;

    if-eqz v1, :cond_0

    .line 92
    const/4 v0, 0x1

    iget-object v1, p0, Lpss;->b:Ljava/lang/Float;

    .line 93
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x0

    .line 95
    :cond_0
    iget-object v1, p0, Lpss;->c:Ljava/lang/Float;

    if-eqz v1, :cond_1

    .line 96
    const/4 v1, 0x2

    iget-object v2, p0, Lpss;->c:Ljava/lang/Float;

    .line 97
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 99
    :cond_1
    iget-object v1, p0, Lpss;->d:Ljava/lang/Float;

    if-eqz v1, :cond_2

    .line 100
    const/4 v1, 0x3

    iget-object v2, p0, Lpss;->d:Ljava/lang/Float;

    .line 101
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 103
    :cond_2
    iget-object v1, p0, Lpss;->e:Ljava/lang/Float;

    if-eqz v1, :cond_3

    .line 104
    const/4 v1, 0x4

    iget-object v2, p0, Lpss;->e:Ljava/lang/Float;

    .line 105
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 107
    :cond_3
    iget-object v1, p0, Lpss;->f:Ljava/lang/Float;

    if-eqz v1, :cond_4

    .line 108
    const/4 v1, 0x5

    iget-object v2, p0, Lpss;->f:Ljava/lang/Float;

    .line 109
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 111
    :cond_4
    iget-object v1, p0, Lpss;->g:Ljava/lang/Float;

    if-eqz v1, :cond_5

    .line 112
    const/4 v1, 0x6

    iget-object v2, p0, Lpss;->g:Ljava/lang/Float;

    .line 113
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 115
    :cond_5
    iget-object v1, p0, Lpss;->h:Ljava/lang/Float;

    if-eqz v1, :cond_6

    .line 116
    const/4 v1, 0x7

    iget-object v2, p0, Lpss;->h:Ljava/lang/Float;

    .line 117
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 119
    :cond_6
    iget-object v1, p0, Lpss;->i:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    .line 120
    const/16 v1, 0x8

    iget-object v2, p0, Lpss;->i:Ljava/lang/Integer;

    .line 121
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 123
    :cond_7
    iget-object v1, p0, Lpss;->j:Ljava/lang/Integer;

    if-eqz v1, :cond_8

    .line 124
    const/16 v1, 0x9

    iget-object v2, p0, Lpss;->j:Ljava/lang/Integer;

    .line 125
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 127
    :cond_8
    iget-object v1, p0, Lpss;->k:Ljava/lang/Float;

    if-eqz v1, :cond_9

    .line 128
    const/16 v1, 0xa

    iget-object v2, p0, Lpss;->k:Ljava/lang/Float;

    .line 129
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 131
    :cond_9
    iget-object v1, p0, Lpss;->l:Ljava/lang/Float;

    if-eqz v1, :cond_a

    .line 132
    const/16 v1, 0xb

    iget-object v2, p0, Lpss;->l:Ljava/lang/Float;

    .line 133
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 135
    :cond_a
    iget-object v1, p0, Lpss;->m:Ljava/lang/Float;

    if-eqz v1, :cond_b

    .line 136
    const/16 v1, 0xc

    iget-object v2, p0, Lpss;->m:Ljava/lang/Float;

    .line 137
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 139
    :cond_b
    iget-object v1, p0, Lpss;->n:Ljava/lang/Float;

    if-eqz v1, :cond_c

    .line 140
    const/16 v1, 0xd

    iget-object v2, p0, Lpss;->n:Ljava/lang/Float;

    .line 141
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 143
    :cond_c
    iget-object v1, p0, Lpss;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 144
    iput v0, p0, Lpss;->ai:I

    .line 145
    return v0
.end method

.method public a(Loxn;)Lpss;
    .locals 2

    .prologue
    .line 153
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 154
    sparse-switch v0, :sswitch_data_0

    .line 158
    iget-object v1, p0, Lpss;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 159
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpss;->ah:Ljava/util/List;

    .line 162
    :cond_1
    iget-object v1, p0, Lpss;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 164
    :sswitch_0
    return-object p0

    .line 169
    :sswitch_1
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpss;->b:Ljava/lang/Float;

    goto :goto_0

    .line 173
    :sswitch_2
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpss;->c:Ljava/lang/Float;

    goto :goto_0

    .line 177
    :sswitch_3
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpss;->d:Ljava/lang/Float;

    goto :goto_0

    .line 181
    :sswitch_4
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpss;->e:Ljava/lang/Float;

    goto :goto_0

    .line 185
    :sswitch_5
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpss;->f:Ljava/lang/Float;

    goto :goto_0

    .line 189
    :sswitch_6
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpss;->g:Ljava/lang/Float;

    goto :goto_0

    .line 193
    :sswitch_7
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpss;->h:Ljava/lang/Float;

    goto :goto_0

    .line 197
    :sswitch_8
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lpss;->i:Ljava/lang/Integer;

    goto :goto_0

    .line 201
    :sswitch_9
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lpss;->j:Ljava/lang/Integer;

    goto :goto_0

    .line 205
    :sswitch_a
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpss;->k:Ljava/lang/Float;

    goto/16 :goto_0

    .line 209
    :sswitch_b
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpss;->l:Ljava/lang/Float;

    goto/16 :goto_0

    .line 213
    :sswitch_c
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpss;->m:Ljava/lang/Float;

    goto/16 :goto_0

    .line 217
    :sswitch_d
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpss;->n:Ljava/lang/Float;

    goto/16 :goto_0

    .line 154
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x15 -> :sswitch_2
        0x1d -> :sswitch_3
        0x25 -> :sswitch_4
        0x2d -> :sswitch_5
        0x35 -> :sswitch_6
        0x3d -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x55 -> :sswitch_a
        0x5d -> :sswitch_b
        0x65 -> :sswitch_c
        0x6d -> :sswitch_d
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 45
    iget-object v0, p0, Lpss;->b:Ljava/lang/Float;

    if-eqz v0, :cond_0

    .line 46
    const/4 v0, 0x1

    iget-object v1, p0, Lpss;->b:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 48
    :cond_0
    iget-object v0, p0, Lpss;->c:Ljava/lang/Float;

    if-eqz v0, :cond_1

    .line 49
    const/4 v0, 0x2

    iget-object v1, p0, Lpss;->c:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 51
    :cond_1
    iget-object v0, p0, Lpss;->d:Ljava/lang/Float;

    if-eqz v0, :cond_2

    .line 52
    const/4 v0, 0x3

    iget-object v1, p0, Lpss;->d:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 54
    :cond_2
    iget-object v0, p0, Lpss;->e:Ljava/lang/Float;

    if-eqz v0, :cond_3

    .line 55
    const/4 v0, 0x4

    iget-object v1, p0, Lpss;->e:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 57
    :cond_3
    iget-object v0, p0, Lpss;->f:Ljava/lang/Float;

    if-eqz v0, :cond_4

    .line 58
    const/4 v0, 0x5

    iget-object v1, p0, Lpss;->f:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 60
    :cond_4
    iget-object v0, p0, Lpss;->g:Ljava/lang/Float;

    if-eqz v0, :cond_5

    .line 61
    const/4 v0, 0x6

    iget-object v1, p0, Lpss;->g:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 63
    :cond_5
    iget-object v0, p0, Lpss;->h:Ljava/lang/Float;

    if-eqz v0, :cond_6

    .line 64
    const/4 v0, 0x7

    iget-object v1, p0, Lpss;->h:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 66
    :cond_6
    iget-object v0, p0, Lpss;->i:Ljava/lang/Integer;

    if-eqz v0, :cond_7

    .line 67
    const/16 v0, 0x8

    iget-object v1, p0, Lpss;->i:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 69
    :cond_7
    iget-object v0, p0, Lpss;->j:Ljava/lang/Integer;

    if-eqz v0, :cond_8

    .line 70
    const/16 v0, 0x9

    iget-object v1, p0, Lpss;->j:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 72
    :cond_8
    iget-object v0, p0, Lpss;->k:Ljava/lang/Float;

    if-eqz v0, :cond_9

    .line 73
    const/16 v0, 0xa

    iget-object v1, p0, Lpss;->k:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 75
    :cond_9
    iget-object v0, p0, Lpss;->l:Ljava/lang/Float;

    if-eqz v0, :cond_a

    .line 76
    const/16 v0, 0xb

    iget-object v1, p0, Lpss;->l:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 78
    :cond_a
    iget-object v0, p0, Lpss;->m:Ljava/lang/Float;

    if-eqz v0, :cond_b

    .line 79
    const/16 v0, 0xc

    iget-object v1, p0, Lpss;->m:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 81
    :cond_b
    iget-object v0, p0, Lpss;->n:Ljava/lang/Float;

    if-eqz v0, :cond_c

    .line 82
    const/16 v0, 0xd

    iget-object v1, p0, Lpss;->n:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 84
    :cond_c
    iget-object v0, p0, Lpss;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 86
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lpss;->a(Loxn;)Lpss;

    move-result-object v0

    return-object v0
.end method
