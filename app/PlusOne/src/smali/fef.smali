.class public final Lfef;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private synthetic a:Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;)V
    .locals 0

    .prologue
    .line 240
    iput-object p1, p0, Lfef;->a:Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 243
    iget-object v0, p0, Lfef;->a:Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;

    invoke-static {v0}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->a(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;)Lhee;

    move-result-object v0

    invoke-interface {v0}, Lhee;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 267
    :goto_0
    return-object v4

    .line 247
    :cond_0
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 254
    :pswitch_1
    new-instance v4, Lhxg;

    iget-object v0, p0, Lfef;->a:Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;

    iget-object v1, p0, Lfef;->a:Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->c(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;)Lhee;

    move-result-object v1

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    const/4 v2, 0x5

    sget-object v3, Lkmt;->a:[Ljava/lang/String;

    invoke-direct {v4, v0, v1, v2, v3}, Lhxg;-><init>(Landroid/content/Context;II[Ljava/lang/String;)V

    goto :goto_0

    .line 249
    :pswitch_2
    new-instance v0, Lhye;

    iget-object v1, p0, Lfef;->a:Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;

    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->a:Landroid/net/Uri;

    iget-object v3, p0, Lfef;->a:Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;

    .line 250
    invoke-static {v3}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->b(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;)Lhee;

    move-result-object v3

    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    .line 249
    invoke-static {v2, v3}, Lcom/google/android/apps/plus/content/EsProvider;->a(Landroid/net/Uri;I)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lkmr;->a:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Lhye;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    move-object v4, v0

    goto :goto_0

    .line 258
    :pswitch_3
    new-instance v4, Lkln;

    iget-object v0, p0, Lfef;->a:Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;

    iget-object v1, p0, Lfef;->a:Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->d(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;)Lhee;

    move-result-object v1

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    iget-object v2, p0, Lfef;->a:Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;

    .line 259
    invoke-static {v2}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->e(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;)Ljbx;

    move-result-object v2

    invoke-virtual {v2}, Ljbx;->g()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v0, v1, v2}, Lkln;-><init>(Landroid/content/Context;ILjava/lang/String;)V

    goto :goto_0

    .line 262
    :pswitch_4
    new-instance v4, Lkjy;

    iget-object v0, p0, Lfef;->a:Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;

    iget-object v1, p0, Lfef;->a:Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->f(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;)Lhee;

    move-result-object v1

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    sget-object v2, Llbf;->b:[Ljava/lang/String;

    iget-object v3, p0, Lfef;->a:Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;

    .line 264
    invoke-static {v3}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->g(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;)Lkki;

    move-result-object v3

    invoke-virtual {v3}, Lkki;->c()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v0, v1, v2, v3}, Lkjy;-><init>(Landroid/content/Context;I[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 247
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 410
    return-void
.end method

.method public a(Ldo;Landroid/database/Cursor;)V
    .locals 9
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DefaultLocale"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 284
    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 406
    :cond_0
    :goto_0
    return-void

    .line 288
    :cond_1
    invoke-virtual {p1}, Ldo;->o()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 345
    :cond_2
    :goto_1
    :pswitch_1
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 346
    invoke-interface {p2, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 347
    invoke-interface {p2, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 348
    invoke-interface {p2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 349
    invoke-interface {p2, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 350
    new-instance v5, Lhxc;

    invoke-direct {v5, v1, v3, v0, v4}, Lhxc;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    .line 351
    iget-object v0, p0, Lfef;->a:Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;

    invoke-static {v0}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->p(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0, v1, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 352
    const/16 v0, 0x8

    if-ne v3, v0, :cond_2

    .line 353
    iget-object v0, p0, Lfef;->a:Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;

    new-instance v1, Lhgw;

    invoke-direct {v1, v5}, Lhgw;-><init>(Lhxc;)V

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->c(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;Lhgw;)Lhgw;

    goto :goto_1

    .line 292
    :pswitch_2
    iget-object v0, p0, Lfef;->a:Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;

    invoke-static {v0}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->h(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;)Z

    move-result v0

    if-ne v0, v2, :cond_3

    .line 293
    iget-object v0, p0, Lfef;->a:Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->a(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;Z)Z

    .line 294
    iget-object v0, p0, Lfef;->a:Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;

    invoke-static {v0}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->i(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 298
    :cond_3
    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 299
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    .line 301
    iget-object v0, p0, Lfef;->a:Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->M()Lhgw;

    move-result-object v0

    .line 302
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lhgw;->h()I

    move-result v4

    if-nez v4, :cond_4

    .line 303
    invoke-virtual {v0}, Lhgw;->g()I

    move-result v4

    if-nez v4, :cond_4

    invoke-virtual {v0}, Lhgw;->i()I

    move-result v4

    if-nez v4, :cond_4

    .line 304
    invoke-virtual {v0}, Lhgw;->j()I

    move-result v0

    if-eqz v0, :cond_6

    :cond_4
    move v0, v2

    .line 307
    :goto_2
    if-nez v0, :cond_5

    iget-object v0, p0, Lfef;->a:Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;

    invoke-static {v0}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->j(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 308
    :cond_5
    iget-object v0, p0, Lfef;->a:Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->b(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;Z)Z

    .line 309
    iget-object v0, p0, Lfef;->a:Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;

    invoke-static {v0}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->k(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    :cond_6
    move v0, v1

    .line 304
    goto :goto_2

    .line 314
    :cond_7
    if-nez v3, :cond_9

    .line 315
    iget-object v0, p0, Lfef;->a:Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;

    invoke-static {v0}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->l(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;)Lhxc;

    move-result-object v0

    .line 317
    if-eqz v0, :cond_0

    .line 321
    iget-object v3, p0, Lfef;->a:Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;

    new-instance v4, Lhgw;

    invoke-direct {v4, v0}, Lhgw;-><init>(Lhxc;)V

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->a(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;Lhgw;)V

    .line 340
    :cond_8
    :goto_3
    iget-object v0, p0, Lfef;->a:Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->c(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;Z)Z

    .line 341
    iget-object v0, p0, Lfef;->a:Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;

    invoke-static {v0}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->o(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 324
    :cond_9
    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, Lhhg;->a(Ljava/nio/ByteBuffer;)Lhgw;

    move-result-object v3

    .line 326
    invoke-virtual {v3}, Lhgw;->b()[Lhxc;

    move-result-object v4

    move v0, v1

    .line 327
    :goto_4
    array-length v5, v4

    if-ge v0, v5, :cond_10

    .line 328
    iget-object v5, p0, Lfef;->a:Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;

    invoke-static {v5}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->m(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;)Ljava/util/HashMap;

    move-result-object v5

    aget-object v6, v4, v0

    invoke-virtual {v6}, Lhxc;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_a

    aget-object v5, v4, v0

    .line 329
    invoke-virtual {v5}, Lhxc;->c()I

    move-result v5

    const/16 v6, 0x9

    if-ne v5, v6, :cond_b

    iget-object v5, p0, Lfef;->a:Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;

    invoke-static {v5}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->n(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;)Z

    move-result v5

    if-eqz v5, :cond_b

    :cond_a
    move v0, v1

    .line 335
    :goto_5
    if-eqz v0, :cond_8

    .line 336
    iget-object v0, p0, Lfef;->a:Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->b(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;Lhgw;)V

    goto :goto_3

    .line 327
    :cond_b
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 360
    :cond_c
    iget-object v0, p0, Lfef;->a:Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->g()Lbb;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lfef;->a:Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;

    invoke-static {v2}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->q(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;)Lfef;

    move-result-object v2

    invoke-virtual {v0, v6, v1, v2}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    goto/16 :goto_0

    .line 364
    :pswitch_3
    if-eqz p2, :cond_d

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_e

    .line 365
    :cond_d
    iget-object v0, p0, Lfef;->a:Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;

    const/16 v1, 0x6e27

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->showDialog(I)V

    goto/16 :goto_0

    .line 368
    :cond_e
    iget-object v0, p0, Lfef;->a:Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;

    invoke-static {v0}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->r(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;)Ljbx;

    move-result-object v0

    .line 369
    invoke-interface {p2}, Landroid/database/Cursor;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "album_name"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 368
    invoke-virtual {v0, v1, p0}, Ljbx;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 372
    iget-object v0, p0, Lfef;->a:Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->l()Landroid/widget/BaseAdapter;

    move-result-object v0

    check-cast v0, Lemx;

    invoke-virtual {v0, p2}, Lemx;->b(Landroid/database/Cursor;)Landroid/database/Cursor;

    goto/16 :goto_0

    .line 376
    :pswitch_4
    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 378
    const/16 v0, 0xe

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_f

    .line 380
    iget-object v0, p0, Lfef;->a:Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;

    const/16 v3, 0xe

    .line 381
    invoke-interface {p2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 380
    invoke-static {v0, v3}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->a(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 382
    const/16 v0, 0xf

    .line 383
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 384
    const/16 v3, 0x10

    .line 385
    invoke-interface {p2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 386
    iget-object v4, p0, Lfef;->a:Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;

    invoke-static {v4}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->t(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;)Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    move-result-object v4

    iget-object v5, p0, Lfef;->a:Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;

    invoke-static {v5}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->s(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    iget-object v3, p0, Lfef;->a:Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;

    invoke-static {v3}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->u(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;)Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setVisibility(I)V

    .line 397
    :goto_6
    iget-object v3, p0, Lfef;->a:Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;

    invoke-static {v3}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->y(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;)Landroid/widget/TextView;

    move-result-object v3

    iget-object v4, p0, Lfef;->a:Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;

    const v5, 0x7f0a07d4

    new-array v2, v2, [Ljava/lang/Object;

    .line 399
    invoke-static {}, Lfo;->a()Lfo;

    move-result-object v6

    invoke-virtual {v6, v0}, Lfo;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v1

    .line 398
    invoke-virtual {v4, v5, v2}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 399
    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    .line 397
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 400
    iget-object v0, p0, Lfef;->a:Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;

    invoke-static {v0}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->z(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 401
    iget-object v0, p0, Lfef;->a:Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;

    invoke-static {v0}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->A(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 389
    :cond_f
    iget-object v0, p0, Lfef;->a:Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;

    invoke-interface {p2, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->b(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 390
    invoke-interface {p2, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 391
    const/4 v3, 0x5

    .line 392
    invoke-interface {p2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 393
    iget-object v4, p0, Lfef;->a:Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;

    invoke-static {v4}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->w(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;)Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    move-result-object v4

    iget-object v5, p0, Lfef;->a:Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;

    .line 394
    invoke-static {v5}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->v(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3}, Lhst;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 393
    invoke-virtual {v4, v5, v3}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 395
    iget-object v3, p0, Lfef;->a:Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;

    invoke-static {v3}, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;->x(Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;)Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setVisibility(I)V

    goto :goto_6

    :cond_10
    move v0, v2

    goto/16 :goto_5

    .line 288
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DefaultLocale"
        }
    .end annotation

    .prologue
    .line 240
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lfef;->a(Ldo;Landroid/database/Cursor;)V

    return-void
.end method
