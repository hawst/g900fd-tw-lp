.class public Lkoh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhlv;


# instance fields
.field private final a:I

.field private final b:F

.field private final c:I

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>(IFI)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput p1, p0, Lkoh;->a:I

    .line 36
    iput p2, p0, Lkoh;->b:F

    .line 37
    iput p3, p0, Lkoh;->c:I

    .line 38
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lkoh;->a:I

    return v0
.end method

.method public a(Landroid/content/Context;Lhlx;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lkoh;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lkoh;->d:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p2, p1}, Lhlx;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Lkoh;
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lkoh;->d:Ljava/lang/String;

    .line 64
    return-object p0
.end method

.method public a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 44
    const-class v0, Lhlw;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhlw;

    invoke-interface {v0, p1, p0}, Lhlw;->a(Landroid/content/Context;Lhlv;)V

    .line 45
    return-void
.end method

.method public a(Lhmp;)V
    .locals 0

    .prologue
    .line 69
    return-void
.end method

.method public b()F
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lkoh;->b:F

    return v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lkoh;->c:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 73
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "NetworkSamplingEvent networkType=%d, networkSpeedMbps=%f, networkSpeedBucket=%d"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lkoh;->a:I

    .line 75
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, p0, Lkoh;->b:F

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget v4, p0, Lkoh;->c:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 73
    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
