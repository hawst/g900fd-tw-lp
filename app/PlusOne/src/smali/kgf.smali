.class public final Lkgf;
.super Lkfm;
.source "PG"


# static fields
.field private static final serialVersionUID:J = -0x59eca412221c9248L


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lkgd;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 48
    invoke-virtual {p1}, Lkgd;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lkgd;->c()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x2

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ": "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lkfm;-><init>(Ljava/lang/String;)V

    .line 49
    invoke-virtual {p1}, Lkgd;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkgf;->b:Ljava/lang/String;

    .line 50
    iput-object p2, p0, Lkgf;->a:Ljava/lang/String;

    .line 51
    return-void
.end method

.method public static a(Ljava/lang/Exception;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 64
    instance-of v0, p0, Lkgf;

    if-eqz v0, :cond_0

    .line 65
    check-cast p0, Lkgf;

    invoke-virtual {p0}, Lkgf;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    .line 69
    :goto_0
    return v0

    .line 66
    :cond_0
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    instance-of v0, v0, Lkgf;

    if-eqz v0, :cond_1

    .line 67
    invoke-virtual {p0}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, Lkgf;

    invoke-virtual {v0}, Lkgf;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    goto :goto_0

    .line 69
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lkgf;->b:Ljava/lang/String;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lkgf;->a:Ljava/lang/String;

    return-object v0
.end method
