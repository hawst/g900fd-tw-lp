.class public final Lmgl;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lnbf;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 23650
    invoke-direct {p0}, Loxq;-><init>()V

    .line 23653
    iput-object v0, p0, Lmgl;->apiHeader:Llyr;

    .line 23656
    iput-object v0, p0, Lmgl;->a:Lnbf;

    .line 23650
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 23673
    const/4 v0, 0x0

    .line 23674
    iget-object v1, p0, Lmgl;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 23675
    const/4 v0, 0x1

    iget-object v1, p0, Lmgl;->apiHeader:Llyr;

    .line 23676
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 23678
    :cond_0
    iget-object v1, p0, Lmgl;->a:Lnbf;

    if-eqz v1, :cond_1

    .line 23679
    const/4 v1, 0x2

    iget-object v2, p0, Lmgl;->a:Lnbf;

    .line 23680
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 23682
    :cond_1
    iget-object v1, p0, Lmgl;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 23683
    iput v0, p0, Lmgl;->ai:I

    .line 23684
    return v0
.end method

.method public a(Loxn;)Lmgl;
    .locals 2

    .prologue
    .line 23692
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 23693
    sparse-switch v0, :sswitch_data_0

    .line 23697
    iget-object v1, p0, Lmgl;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 23698
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmgl;->ah:Ljava/util/List;

    .line 23701
    :cond_1
    iget-object v1, p0, Lmgl;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 23703
    :sswitch_0
    return-object p0

    .line 23708
    :sswitch_1
    iget-object v0, p0, Lmgl;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 23709
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmgl;->apiHeader:Llyr;

    .line 23711
    :cond_2
    iget-object v0, p0, Lmgl;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 23715
    :sswitch_2
    iget-object v0, p0, Lmgl;->a:Lnbf;

    if-nez v0, :cond_3

    .line 23716
    new-instance v0, Lnbf;

    invoke-direct {v0}, Lnbf;-><init>()V

    iput-object v0, p0, Lmgl;->a:Lnbf;

    .line 23718
    :cond_3
    iget-object v0, p0, Lmgl;->a:Lnbf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 23693
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 23661
    iget-object v0, p0, Lmgl;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 23662
    const/4 v0, 0x1

    iget-object v1, p0, Lmgl;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 23664
    :cond_0
    iget-object v0, p0, Lmgl;->a:Lnbf;

    if-eqz v0, :cond_1

    .line 23665
    const/4 v0, 0x2

    iget-object v1, p0, Lmgl;->a:Lnbf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 23667
    :cond_1
    iget-object v0, p0, Lmgl;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 23669
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 23646
    invoke-virtual {p0, p1}, Lmgl;->a(Loxn;)Lmgl;

    move-result-object v0

    return-object v0
.end method
