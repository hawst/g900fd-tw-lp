.class public Ldhe;
.super Llol;
.source "PG"

# interfaces
.implements Lhjj;


# instance fields
.field private N:Lhje;

.field private O:Ldgi;

.field private P:Ldgr;

.field private Q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ldgq;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 22
    invoke-direct {p0}, Llol;-><init>()V

    .line 23
    new-instance v0, Lhje;

    iget-object v1, p0, Ldhe;->av:Llqm;

    invoke-direct {v0, p0, v1, p0}, Lhje;-><init>(Lu;Llqr;Lhjj;)V

    iput-object v0, p0, Ldhe;->N:Lhje;

    .line 29
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ldhe;->Q:Ljava/util/List;

    return-void
.end method

.method static synthetic a(Ldhe;)Ldgr;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Ldhe;->P:Ldgr;

    return-object v0
.end method

.method static synthetic a(Ldhe;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 22
    iput-object p1, p0, Ldhe;->Q:Ljava/util/List;

    return-object p1
.end method

.method static synthetic b(Ldhe;)Lhje;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Ldhe;->N:Lhje;

    return-object v0
.end method


# virtual methods
.method public a(Lhjk;)V
    .locals 3

    .prologue
    .line 56
    iget-object v0, p0, Ldhe;->O:Ldgi;

    invoke-interface {v0}, Ldgi;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 66
    :cond_0
    return-void

    .line 61
    :cond_1
    iget-object v0, p0, Ldhe;->Q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldgq;

    .line 62
    iget-object v2, p0, Ldhe;->P:Ldgr;

    invoke-virtual {v2, v0}, Ldgr;->b(Ldgu;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 63
    invoke-virtual {v0}, Ldgq;->a()I

    move-result v0

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public a(Loo;)V
    .locals 0

    .prologue
    .line 48
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 70
    iget-object v0, p0, Ldhe;->O:Ldgi;

    invoke-interface {v0}, Ldgi;->a()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 81
    :goto_0
    return v0

    .line 74
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    .line 75
    iget-object v0, p0, Ldhe;->Q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldgq;

    .line 76
    invoke-virtual {v0}, Ldgq;->a()I

    move-result v4

    if-ne v4, v2, :cond_1

    .line 77
    iget-object v1, p0, Ldhe;->P:Ldgr;

    invoke-virtual {v1, v0}, Ldgr;->d(Ldgu;)V

    .line 78
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 81
    goto :goto_0
.end method

.method public b(Loo;)V
    .locals 0

    .prologue
    .line 52
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 33
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 34
    iget-object v0, p0, Ldhe;->au:Llnh;

    const-class v1, Ldgi;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldgi;

    iput-object v0, p0, Ldhe;->O:Ldgi;

    .line 35
    iget-object v0, p0, Ldhe;->au:Llnh;

    const-class v1, Ldgr;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldgr;

    iput-object v0, p0, Ldhe;->P:Ldgr;

    .line 37
    iget-object v0, p0, Ldhe;->P:Ldgr;

    new-instance v1, Ldhf;

    invoke-direct {v1, p0}, Ldhf;-><init>(Ldhe;)V

    invoke-virtual {v0, v1}, Ldgr;->a(Ldgw;)V

    .line 44
    return-void
.end method
