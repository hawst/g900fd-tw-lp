.class public final Ldyd;
.super Lepp;
.source "PG"


# instance fields
.field private d:I

.field private e:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;ILept;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0, p1, p2, p3}, Lepp;-><init>(Landroid/content/Context;ILept;)V

    .line 52
    return-void
.end method

.method private g()Z
    .locals 9

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 140
    .line 142
    :try_start_0
    iget-object v0, p0, Ldyd;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 143
    iget-object v1, p0, Ldyd;->c:Landroid/content/Context;

    .line 144
    invoke-static {v1}, Lhqv;->b(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "sync_on_wifi_only"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 146
    if-eqz v1, :cond_1

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-nez v0, :cond_1

    move v0, v6

    .line 148
    :goto_0
    if-eqz v1, :cond_0

    .line 149
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    return v0

    :cond_1
    move v0, v7

    .line 146
    goto :goto_0

    .line 148
    :catchall_0
    move-exception v0

    move-object v1, v8

    :goto_1
    if-eqz v1, :cond_2

    .line 149
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    .line 148
    :catchall_1
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 127
    iget-object v0, p0, Ldyd;->c:Landroid/content/Context;

    iget v1, p0, Ldyd;->b:I

    new-instance v2, Ldyf;

    invoke-direct {v2, v0, v1}, Ldyf;-><init>(Landroid/content/Context;I)V

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Void;

    const/4 v1, 0x0

    const/4 v3, 0x0

    aput-object v3, v0, v1

    invoke-virtual {v2, v0}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 128
    invoke-virtual {p0}, Ldyd;->f()V

    .line 129
    return-void
.end method

.method protected a(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 56
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    if-nez v0, :cond_0

    .line 57
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 59
    :cond_0
    iget-object v0, p0, Ldyd;->c:Landroid/content/Context;

    const v1, 0x7f0a060f

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Ldyd;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 60
    iget-object v0, p0, Ldyd;->c:Landroid/content/Context;

    const v1, 0x7f0a060e

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Ldyd;->b(Landroid/view/View;Ljava/lang/String;)V

    .line 61
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f110039

    iget v2, p0, Ldyd;->d:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget v5, p0, Ldyd;->d:I

    .line 62
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    .line 61
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 63
    const v0, 0x7f1001b4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 65
    const v0, 0x7f1001c2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Ldye;

    invoke-direct {v1, p0}, Ldye;-><init>(Ldyd;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 75
    const v0, 0x7f1001c1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 76
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-boolean v1, p0, Ldyd;->e:Z

    if-eqz v1, :cond_1

    const v1, 0x7f0a060c

    :goto_0
    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 75
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    return-void

    .line 76
    :cond_1
    const v1, 0x7f0a060b

    goto :goto_0
.end method

.method public a(Lnyq;)Z
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    .line 85
    invoke-super {p0, p1}, Lepp;->a(Lnyq;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 122
    :goto_0
    return v0

    .line 91
    :cond_0
    iget-object v0, p0, Ldyd;->c:Landroid/content/Context;

    const-class v2, Lhpu;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhpu;

    .line 94
    iget v2, p0, Ldyd;->b:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    iget v2, p0, Ldyd;->b:I

    .line 95
    invoke-virtual {v0, v2}, Lhpu;->d(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldyd;->c:Landroid/content/Context;

    .line 96
    invoke-static {v0}, Lhqd;->h(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    move v0, v1

    .line 97
    goto :goto_0

    .line 101
    :cond_2
    iget-object v0, p0, Ldyd;->c:Landroid/content/Context;

    iget v2, p0, Ldyd;->b:I

    invoke-virtual {p0}, Ldyd;->e()Lepn;

    move-result-object v3

    invoke-static {v0, v2, v3}, Ldhv;->a(Landroid/content/Context;ILepn;)Ldwi;

    move-result-object v0

    .line 102
    invoke-virtual {v0}, Ldwi;->b()J

    move-result-wide v2

    cmp-long v0, v2, v4

    if-lez v0, :cond_3

    move v0, v1

    .line 103
    goto :goto_0

    .line 109
    :cond_3
    iget-object v0, p0, Ldyd;->c:Landroid/content/Context;

    iget v2, p0, Ldyd;->b:I

    invoke-static {v0, v2}, Lhqd;->j(Landroid/content/Context;I)J

    move-result-wide v2

    .line 110
    cmp-long v0, v2, v4

    if-eqz v0, :cond_4

    const-wide v4, 0x14616ed7800L

    cmp-long v0, v2, v4

    if-lez v0, :cond_5

    :cond_4
    move v0, v1

    .line 111
    goto :goto_0

    .line 115
    :cond_5
    iget-object v0, p0, Ldyd;->c:Landroid/content/Context;

    iget v2, p0, Ldyd;->b:I

    invoke-static {v0, v2}, Lhqd;->h(Landroid/content/Context;I)I

    move-result v0

    if-lez v0, :cond_6

    move v0, v1

    .line 116
    goto :goto_0

    .line 119
    :cond_6
    invoke-direct {p0}, Ldyd;->g()Z

    move-result v0

    iput-boolean v0, p0, Ldyd;->e:Z

    .line 121
    iget-object v0, p0, Ldyd;->c:Landroid/content/Context;

    iget v2, p0, Ldyd;->b:I

    invoke-static {v0, v2}, Lhqd;->i(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, Ldyd;->d:I

    .line 122
    iget v0, p0, Ldyd;->d:I

    if-lez v0, :cond_7

    const/4 v0, 0x1

    goto :goto_0

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 133
    invoke-virtual {p0}, Ldyd;->f()V

    .line 134
    return-void
.end method

.method public d()I
    .locals 1

    .prologue
    .line 167
    const v0, 0x7f040054

    return v0
.end method

.method public e()Lepn;
    .locals 1

    .prologue
    .line 172
    sget-object v0, Lepn;->i:Lepn;

    return-object v0
.end method
