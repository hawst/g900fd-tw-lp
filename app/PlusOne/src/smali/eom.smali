.class final Leom;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic a:Leog;


# direct methods
.method constructor <init>(Leog;)V
    .locals 0

    .prologue
    .line 522
    iput-object p1, p0, Leom;->a:Leog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 525
    iget-object v0, p0, Leom;->a:Leog;

    invoke-virtual {v0}, Leog;->t()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Leom;->a:Leog;

    invoke-virtual {v0}, Leog;->s()Z

    move-result v0

    if-nez v0, :cond_1

    .line 562
    :cond_0
    :goto_0
    return-void

    .line 530
    :cond_1
    iget-object v0, p0, Leom;->a:Leog;

    invoke-static {v0}, Leog;->g(Leog;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    .line 531
    instance-of v1, v0, Landroid/text/SpannableStringBuilder;

    if-eqz v1, :cond_2

    .line 532
    check-cast v0, Landroid/text/SpannableStringBuilder;

    .line 538
    :goto_1
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    move v2, v3

    .line 540
    :goto_2
    if-eq v2, v1, :cond_3

    invoke-virtual {v0, v2}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    move-result v4

    invoke-static {v4}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 541
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 534
    :cond_2
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    move-object v0, v1

    goto :goto_1

    .line 543
    :cond_3
    if-eqz v2, :cond_4

    .line 544
    invoke-virtual {v0, v3, v2}, Landroid/text/SpannableStringBuilder;->delete(II)Landroid/text/SpannableStringBuilder;

    .line 545
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    .line 548
    :cond_4
    add-int/lit8 v2, v1, -0x1

    .line 549
    :goto_3
    if-ltz v2, :cond_5

    invoke-virtual {v0, v2}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    move-result v4

    invoke-static {v4}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 550
    add-int/lit8 v2, v2, -0x1

    goto :goto_3

    .line 552
    :cond_5
    add-int/lit8 v4, v1, -0x1

    if-eq v2, v4, :cond_6

    .line 554
    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v0, v2, v1}, Landroid/text/SpannableStringBuilder;->delete(II)Landroid/text/SpannableStringBuilder;

    .line 557
    :cond_6
    iget-object v1, p0, Leom;->a:Leog;

    invoke-static {v1}, Leog;->h(Leog;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 558
    iget-object v0, p0, Leom;->a:Leog;

    invoke-static {v0}, Leog;->h(Leog;)Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/text/util/Linkify;->addLinks(Landroid/widget/TextView;I)Z

    .line 559
    iget-object v0, p0, Leom;->a:Leog;

    invoke-static {v0}, Leog;->h(Leog;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 560
    iget-object v0, p0, Leom;->a:Leog;

    invoke-static {v0}, Leog;->h(Leog;)Landroid/widget/TextView;

    move-result-object v0

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 561
    iget-object v0, p0, Leom;->a:Leog;

    invoke-static {v0}, Leog;->i(Leog;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0
.end method
