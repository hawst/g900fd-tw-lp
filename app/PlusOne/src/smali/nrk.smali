.class public final Lnrk;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lnri;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1281
    const v0, 0x2fa6b14

    new-instance v1, Lnrl;

    invoke-direct {v1}, Lnrl;-><init>()V

    .line 1286
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    .line 1285
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1282
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1289
    sget-object v0, Lnri;->a:[Lnri;

    iput-object v0, p0, Lnrk;->a:[Lnri;

    .line 1282
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1307
    .line 1308
    iget-object v1, p0, Lnrk;->a:[Lnri;

    if-eqz v1, :cond_1

    .line 1309
    iget-object v2, p0, Lnrk;->a:[Lnri;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 1310
    if-eqz v4, :cond_0

    .line 1311
    const/4 v5, 0x1

    .line 1312
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 1309
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1316
    :cond_1
    iget-object v1, p0, Lnrk;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1317
    iput v0, p0, Lnrk;->ai:I

    .line 1318
    return v0
.end method

.method public a(Loxn;)Lnrk;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1326
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1327
    sparse-switch v0, :sswitch_data_0

    .line 1331
    iget-object v2, p0, Lnrk;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 1332
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnrk;->ah:Ljava/util/List;

    .line 1335
    :cond_1
    iget-object v2, p0, Lnrk;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1337
    :sswitch_0
    return-object p0

    .line 1342
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1343
    iget-object v0, p0, Lnrk;->a:[Lnri;

    if-nez v0, :cond_3

    move v0, v1

    .line 1344
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnri;

    .line 1345
    iget-object v3, p0, Lnrk;->a:[Lnri;

    if-eqz v3, :cond_2

    .line 1346
    iget-object v3, p0, Lnrk;->a:[Lnri;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1348
    :cond_2
    iput-object v2, p0, Lnrk;->a:[Lnri;

    .line 1349
    :goto_2
    iget-object v2, p0, Lnrk;->a:[Lnri;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 1350
    iget-object v2, p0, Lnrk;->a:[Lnri;

    new-instance v3, Lnri;

    invoke-direct {v3}, Lnri;-><init>()V

    aput-object v3, v2, v0

    .line 1351
    iget-object v2, p0, Lnrk;->a:[Lnri;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1352
    invoke-virtual {p1}, Loxn;->a()I

    .line 1349
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1343
    :cond_3
    iget-object v0, p0, Lnrk;->a:[Lnri;

    array-length v0, v0

    goto :goto_1

    .line 1355
    :cond_4
    iget-object v2, p0, Lnrk;->a:[Lnri;

    new-instance v3, Lnri;

    invoke-direct {v3}, Lnri;-><init>()V

    aput-object v3, v2, v0

    .line 1356
    iget-object v2, p0, Lnrk;->a:[Lnri;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1327
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 1294
    iget-object v0, p0, Lnrk;->a:[Lnri;

    if-eqz v0, :cond_1

    .line 1295
    iget-object v1, p0, Lnrk;->a:[Lnri;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 1296
    if-eqz v3, :cond_0

    .line 1297
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 1295
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1301
    :cond_1
    iget-object v0, p0, Lnrk;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1303
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1278
    invoke-virtual {p0, p1}, Lnrk;->a(Loxn;)Lnrk;

    move-result-object v0

    return-object v0
.end method
