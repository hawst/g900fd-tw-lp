.class public final Lohq;
.super Loxq;
.source "PG"


# instance fields
.field private A:Ljava/lang/String;

.field private B:I

.field private C:[Ljava/lang/String;

.field private D:[I

.field private E:Loic;

.field private F:[Ljava/lang/String;

.field private G:Ljava/lang/String;

.field private H:Ljava/lang/String;

.field private I:[Loie;

.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/Boolean;

.field public g:I

.field public h:Ljava/lang/Boolean;

.field public i:Ljava/lang/Boolean;

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;

.field public n:Ljava/lang/String;

.field public o:Lohs;

.field public p:[Loib;

.field public q:[Loim;

.field public r:[Lohr;

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/String;

.field private u:Ljava/lang/String;

.field private v:Ljava/lang/Double;

.field private w:Ljava/lang/Boolean;

.field private x:Ljava/lang/Boolean;

.field private y:Ljava/lang/Boolean;

.field private z:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/high16 v0, -0x80000000

    .line 1379
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1695
    iput v0, p0, Lohq;->g:I

    .line 1716
    iput v0, p0, Lohq;->B:I

    .line 1719
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lohq;->C:[Ljava/lang/String;

    .line 1722
    iput-object v1, p0, Lohq;->o:Lohs;

    .line 1725
    sget-object v0, Loxx;->a:[I

    iput-object v0, p0, Lohq;->D:[I

    .line 1728
    iput-object v1, p0, Lohq;->E:Loic;

    .line 1731
    sget-object v0, Loib;->a:[Loib;

    iput-object v0, p0, Lohq;->p:[Loib;

    .line 1734
    sget-object v0, Loim;->a:[Loim;

    iput-object v0, p0, Lohq;->q:[Loim;

    .line 1737
    sget-object v0, Lohr;->a:[Lohr;

    iput-object v0, p0, Lohq;->r:[Lohr;

    .line 1740
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lohq;->F:[Ljava/lang/String;

    .line 1747
    sget-object v0, Loie;->a:[Loie;

    iput-object v0, p0, Lohq;->I:[Loie;

    .line 1379
    return-void
.end method


# virtual methods
.method public a()I
    .locals 8

    .prologue
    const/high16 v7, -0x80000000

    const/4 v1, 0x0

    .line 1885
    .line 1886
    iget-object v0, p0, Lohq;->a:Ljava/lang/String;

    if-eqz v0, :cond_29

    .line 1887
    const/4 v0, 0x1

    iget-object v2, p0, Lohq;->a:Ljava/lang/String;

    .line 1888
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1890
    :goto_0
    iget-object v2, p0, Lohq;->v:Ljava/lang/Double;

    if-eqz v2, :cond_0

    .line 1891
    const/4 v2, 0x4

    iget-object v3, p0, Lohq;->v:Ljava/lang/Double;

    .line 1892
    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x8

    add-int/2addr v0, v2

    .line 1894
    :cond_0
    iget-object v2, p0, Lohq;->e:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 1895
    const/4 v2, 0x5

    iget-object v3, p0, Lohq;->e:Ljava/lang/String;

    .line 1896
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1898
    :cond_1
    iget-object v2, p0, Lohq;->b:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 1899
    const/4 v2, 0x6

    iget-object v3, p0, Lohq;->b:Ljava/lang/String;

    .line 1900
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1902
    :cond_2
    iget-object v2, p0, Lohq;->t:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 1903
    const/4 v2, 0x7

    iget-object v3, p0, Lohq;->t:Ljava/lang/String;

    .line 1904
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1906
    :cond_3
    iget-object v2, p0, Lohq;->w:Ljava/lang/Boolean;

    if-eqz v2, :cond_4

    .line 1907
    const/16 v2, 0x8

    iget-object v3, p0, Lohq;->w:Ljava/lang/Boolean;

    .line 1908
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1910
    :cond_4
    iget-object v2, p0, Lohq;->c:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 1911
    const/16 v2, 0x9

    iget-object v3, p0, Lohq;->c:Ljava/lang/String;

    .line 1912
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1914
    :cond_5
    iget-object v2, p0, Lohq;->y:Ljava/lang/Boolean;

    if-eqz v2, :cond_6

    .line 1915
    const/16 v2, 0xb

    iget-object v3, p0, Lohq;->y:Ljava/lang/Boolean;

    .line 1916
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1918
    :cond_6
    iget-object v2, p0, Lohq;->j:Ljava/lang/String;

    if-eqz v2, :cond_7

    .line 1919
    const/16 v2, 0xc

    iget-object v3, p0, Lohq;->j:Ljava/lang/String;

    .line 1920
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1922
    :cond_7
    iget-object v2, p0, Lohq;->k:Ljava/lang/String;

    if-eqz v2, :cond_8

    .line 1923
    const/16 v2, 0xd

    iget-object v3, p0, Lohq;->k:Ljava/lang/String;

    .line 1924
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1926
    :cond_8
    iget-object v2, p0, Lohq;->l:Ljava/lang/String;

    if-eqz v2, :cond_9

    .line 1927
    const/16 v2, 0xe

    iget-object v3, p0, Lohq;->l:Ljava/lang/String;

    .line 1928
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1930
    :cond_9
    iget-object v2, p0, Lohq;->m:Ljava/lang/String;

    if-eqz v2, :cond_a

    .line 1931
    const/16 v2, 0xf

    iget-object v3, p0, Lohq;->m:Ljava/lang/String;

    .line 1932
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1934
    :cond_a
    iget v2, p0, Lohq;->B:I

    if-eq v2, v7, :cond_b

    .line 1935
    const/16 v2, 0x10

    iget v3, p0, Lohq;->B:I

    .line 1936
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 1938
    :cond_b
    iget-object v2, p0, Lohq;->h:Ljava/lang/Boolean;

    if-eqz v2, :cond_c

    .line 1939
    const/16 v2, 0x11

    iget-object v3, p0, Lohq;->h:Ljava/lang/Boolean;

    .line 1940
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1942
    :cond_c
    iget-object v2, p0, Lohq;->C:[Ljava/lang/String;

    if-eqz v2, :cond_e

    iget-object v2, p0, Lohq;->C:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_e

    .line 1944
    iget-object v4, p0, Lohq;->C:[Ljava/lang/String;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_1
    if-ge v2, v5, :cond_d

    aget-object v6, v4, v2

    .line 1946
    invoke-static {v6}, Loxo;->b(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v3, v6

    .line 1944
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1948
    :cond_d
    add-int/2addr v0, v3

    .line 1949
    iget-object v2, p0, Lohq;->C:[Ljava/lang/String;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    .line 1951
    :cond_e
    iget-object v2, p0, Lohq;->o:Lohs;

    if-eqz v2, :cond_f

    .line 1952
    const/16 v2, 0x13

    iget-object v3, p0, Lohq;->o:Lohs;

    .line 1953
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1955
    :cond_f
    iget-object v2, p0, Lohq;->d:Ljava/lang/String;

    if-eqz v2, :cond_10

    .line 1956
    const/16 v2, 0x14

    iget-object v3, p0, Lohq;->d:Ljava/lang/String;

    .line 1957
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1959
    :cond_10
    iget-object v2, p0, Lohq;->i:Ljava/lang/Boolean;

    if-eqz v2, :cond_11

    .line 1960
    const/16 v2, 0x15

    iget-object v3, p0, Lohq;->i:Ljava/lang/Boolean;

    .line 1961
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1963
    :cond_11
    iget-object v2, p0, Lohq;->n:Ljava/lang/String;

    if-eqz v2, :cond_12

    .line 1964
    const/16 v2, 0x16

    iget-object v3, p0, Lohq;->n:Ljava/lang/String;

    .line 1965
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1967
    :cond_12
    iget-object v2, p0, Lohq;->D:[I

    if-eqz v2, :cond_14

    iget-object v2, p0, Lohq;->D:[I

    array-length v2, v2

    if-lez v2, :cond_14

    .line 1969
    iget-object v4, p0, Lohq;->D:[I

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_2
    if-ge v2, v5, :cond_13

    aget v6, v4, v2

    .line 1971
    invoke-static {v6}, Loxo;->i(I)I

    move-result v6

    add-int/2addr v3, v6

    .line 1969
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1973
    :cond_13
    add-int/2addr v0, v3

    .line 1974
    iget-object v2, p0, Lohq;->D:[I

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    .line 1976
    :cond_14
    iget-object v2, p0, Lohq;->E:Loic;

    if-eqz v2, :cond_15

    .line 1977
    const/16 v2, 0x18

    iget-object v3, p0, Lohq;->E:Loic;

    .line 1978
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1980
    :cond_15
    iget-object v2, p0, Lohq;->u:Ljava/lang/String;

    if-eqz v2, :cond_16

    .line 1981
    const/16 v2, 0x19

    iget-object v3, p0, Lohq;->u:Ljava/lang/String;

    .line 1982
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1984
    :cond_16
    iget-object v2, p0, Lohq;->p:[Loib;

    if-eqz v2, :cond_18

    .line 1985
    iget-object v3, p0, Lohq;->p:[Loib;

    array-length v4, v3

    move v2, v1

    :goto_3
    if-ge v2, v4, :cond_18

    aget-object v5, v3, v2

    .line 1986
    if-eqz v5, :cond_17

    .line 1987
    const/16 v6, 0x1a

    .line 1988
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 1985
    :cond_17
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 1992
    :cond_18
    iget-object v2, p0, Lohq;->q:[Loim;

    if-eqz v2, :cond_1a

    .line 1993
    iget-object v3, p0, Lohq;->q:[Loim;

    array-length v4, v3

    move v2, v1

    :goto_4
    if-ge v2, v4, :cond_1a

    aget-object v5, v3, v2

    .line 1994
    if-eqz v5, :cond_19

    .line 1995
    const/16 v6, 0x1b

    .line 1996
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 1993
    :cond_19
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 2000
    :cond_1a
    iget-object v2, p0, Lohq;->z:Ljava/lang/Boolean;

    if-eqz v2, :cond_1b

    .line 2001
    const/16 v2, 0x1c

    iget-object v3, p0, Lohq;->z:Ljava/lang/Boolean;

    .line 2002
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 2004
    :cond_1b
    iget v2, p0, Lohq;->g:I

    if-eq v2, v7, :cond_1c

    .line 2005
    const/16 v2, 0x1d

    iget v3, p0, Lohq;->g:I

    .line 2006
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 2008
    :cond_1c
    iget-object v2, p0, Lohq;->f:Ljava/lang/Boolean;

    if-eqz v2, :cond_1d

    .line 2009
    const/16 v2, 0x1e

    iget-object v3, p0, Lohq;->f:Ljava/lang/Boolean;

    .line 2010
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 2012
    :cond_1d
    iget-object v2, p0, Lohq;->s:Ljava/lang/String;

    if-eqz v2, :cond_1e

    .line 2013
    const/16 v2, 0x1f

    iget-object v3, p0, Lohq;->s:Ljava/lang/String;

    .line 2014
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2016
    :cond_1e
    iget-object v2, p0, Lohq;->r:[Lohr;

    if-eqz v2, :cond_20

    .line 2017
    iget-object v3, p0, Lohq;->r:[Lohr;

    array-length v4, v3

    move v2, v1

    :goto_5
    if-ge v2, v4, :cond_20

    aget-object v5, v3, v2

    .line 2018
    if-eqz v5, :cond_1f

    .line 2019
    const/16 v6, 0x20

    .line 2020
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 2017
    :cond_1f
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 2024
    :cond_20
    iget-object v2, p0, Lohq;->F:[Ljava/lang/String;

    if-eqz v2, :cond_22

    iget-object v2, p0, Lohq;->F:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_22

    .line 2026
    iget-object v4, p0, Lohq;->F:[Ljava/lang/String;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_6
    if-ge v2, v5, :cond_21

    aget-object v6, v4, v2

    .line 2028
    invoke-static {v6}, Loxo;->b(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v3, v6

    .line 2026
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 2030
    :cond_21
    add-int/2addr v0, v3

    .line 2031
    iget-object v2, p0, Lohq;->F:[Ljava/lang/String;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    .line 2033
    :cond_22
    iget-object v2, p0, Lohq;->G:Ljava/lang/String;

    if-eqz v2, :cond_23

    .line 2034
    const/16 v2, 0x23

    iget-object v3, p0, Lohq;->G:Ljava/lang/String;

    .line 2035
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2037
    :cond_23
    iget-object v2, p0, Lohq;->x:Ljava/lang/Boolean;

    if-eqz v2, :cond_24

    .line 2038
    const/16 v2, 0x24

    iget-object v3, p0, Lohq;->x:Ljava/lang/Boolean;

    .line 2039
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 2041
    :cond_24
    iget-object v2, p0, Lohq;->A:Ljava/lang/String;

    if-eqz v2, :cond_25

    .line 2042
    const/16 v2, 0x25

    iget-object v3, p0, Lohq;->A:Ljava/lang/String;

    .line 2043
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2045
    :cond_25
    iget-object v2, p0, Lohq;->H:Ljava/lang/String;

    if-eqz v2, :cond_26

    .line 2046
    const/16 v2, 0x26

    iget-object v3, p0, Lohq;->H:Ljava/lang/String;

    .line 2047
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2049
    :cond_26
    iget-object v2, p0, Lohq;->I:[Loie;

    if-eqz v2, :cond_28

    .line 2050
    iget-object v2, p0, Lohq;->I:[Loie;

    array-length v3, v2

    :goto_7
    if-ge v1, v3, :cond_28

    aget-object v4, v2, v1

    .line 2051
    if-eqz v4, :cond_27

    .line 2052
    const/16 v5, 0x27

    .line 2053
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 2050
    :cond_27
    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    .line 2057
    :cond_28
    iget-object v1, p0, Lohq;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2058
    iput v0, p0, Lohq;->ai:I

    .line 2059
    return v0

    :cond_29
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lohq;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 2067
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2068
    sparse-switch v0, :sswitch_data_0

    .line 2072
    iget-object v2, p0, Lohq;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 2073
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lohq;->ah:Ljava/util/List;

    .line 2076
    :cond_1
    iget-object v2, p0, Lohq;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2078
    :sswitch_0
    return-object p0

    .line 2083
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lohq;->a:Ljava/lang/String;

    goto :goto_0

    .line 2087
    :sswitch_2
    invoke-virtual {p1}, Loxn;->c()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lohq;->v:Ljava/lang/Double;

    goto :goto_0

    .line 2091
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lohq;->e:Ljava/lang/String;

    goto :goto_0

    .line 2095
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lohq;->b:Ljava/lang/String;

    goto :goto_0

    .line 2099
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lohq;->t:Ljava/lang/String;

    goto :goto_0

    .line 2103
    :sswitch_6
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lohq;->w:Ljava/lang/Boolean;

    goto :goto_0

    .line 2107
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lohq;->c:Ljava/lang/String;

    goto :goto_0

    .line 2111
    :sswitch_8
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lohq;->y:Ljava/lang/Boolean;

    goto :goto_0

    .line 2115
    :sswitch_9
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lohq;->j:Ljava/lang/String;

    goto :goto_0

    .line 2119
    :sswitch_a
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lohq;->k:Ljava/lang/String;

    goto :goto_0

    .line 2123
    :sswitch_b
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lohq;->l:Ljava/lang/String;

    goto :goto_0

    .line 2127
    :sswitch_c
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lohq;->m:Ljava/lang/String;

    goto :goto_0

    .line 2131
    :sswitch_d
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 2132
    if-eqz v0, :cond_2

    if-eq v0, v4, :cond_2

    if-ne v0, v5, :cond_3

    .line 2135
    :cond_2
    iput v0, p0, Lohq;->B:I

    goto/16 :goto_0

    .line 2137
    :cond_3
    iput v1, p0, Lohq;->B:I

    goto/16 :goto_0

    .line 2142
    :sswitch_e
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lohq;->h:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 2146
    :sswitch_f
    const/16 v0, 0x92

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 2147
    iget-object v0, p0, Lohq;->C:[Ljava/lang/String;

    array-length v0, v0

    .line 2148
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 2149
    iget-object v3, p0, Lohq;->C:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2150
    iput-object v2, p0, Lohq;->C:[Ljava/lang/String;

    .line 2151
    :goto_1
    iget-object v2, p0, Lohq;->C:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 2152
    iget-object v2, p0, Lohq;->C:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 2153
    invoke-virtual {p1}, Loxn;->a()I

    .line 2151
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2156
    :cond_4
    iget-object v2, p0, Lohq;->C:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    .line 2160
    :sswitch_10
    iget-object v0, p0, Lohq;->o:Lohs;

    if-nez v0, :cond_5

    .line 2161
    new-instance v0, Lohs;

    invoke-direct {v0}, Lohs;-><init>()V

    iput-object v0, p0, Lohq;->o:Lohs;

    .line 2163
    :cond_5
    iget-object v0, p0, Lohq;->o:Lohs;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 2167
    :sswitch_11
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lohq;->d:Ljava/lang/String;

    goto/16 :goto_0

    .line 2171
    :sswitch_12
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lohq;->i:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 2175
    :sswitch_13
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lohq;->n:Ljava/lang/String;

    goto/16 :goto_0

    .line 2179
    :sswitch_14
    const/16 v0, 0xb8

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 2180
    iget-object v0, p0, Lohq;->D:[I

    array-length v0, v0

    .line 2181
    add-int/2addr v2, v0

    new-array v2, v2, [I

    .line 2182
    iget-object v3, p0, Lohq;->D:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2183
    iput-object v2, p0, Lohq;->D:[I

    .line 2184
    :goto_2
    iget-object v2, p0, Lohq;->D:[I

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 2185
    iget-object v2, p0, Lohq;->D:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v3

    aput v3, v2, v0

    .line 2186
    invoke-virtual {p1}, Loxn;->a()I

    .line 2184
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2189
    :cond_6
    iget-object v2, p0, Lohq;->D:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v3

    aput v3, v2, v0

    goto/16 :goto_0

    .line 2193
    :sswitch_15
    iget-object v0, p0, Lohq;->E:Loic;

    if-nez v0, :cond_7

    .line 2194
    new-instance v0, Loic;

    invoke-direct {v0}, Loic;-><init>()V

    iput-object v0, p0, Lohq;->E:Loic;

    .line 2196
    :cond_7
    iget-object v0, p0, Lohq;->E:Loic;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 2200
    :sswitch_16
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lohq;->u:Ljava/lang/String;

    goto/16 :goto_0

    .line 2204
    :sswitch_17
    const/16 v0, 0xd2

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 2205
    iget-object v0, p0, Lohq;->p:[Loib;

    if-nez v0, :cond_9

    move v0, v1

    .line 2206
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Loib;

    .line 2207
    iget-object v3, p0, Lohq;->p:[Loib;

    if-eqz v3, :cond_8

    .line 2208
    iget-object v3, p0, Lohq;->p:[Loib;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2210
    :cond_8
    iput-object v2, p0, Lohq;->p:[Loib;

    .line 2211
    :goto_4
    iget-object v2, p0, Lohq;->p:[Loib;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_a

    .line 2212
    iget-object v2, p0, Lohq;->p:[Loib;

    new-instance v3, Loib;

    invoke-direct {v3}, Loib;-><init>()V

    aput-object v3, v2, v0

    .line 2213
    iget-object v2, p0, Lohq;->p:[Loib;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 2214
    invoke-virtual {p1}, Loxn;->a()I

    .line 2211
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 2205
    :cond_9
    iget-object v0, p0, Lohq;->p:[Loib;

    array-length v0, v0

    goto :goto_3

    .line 2217
    :cond_a
    iget-object v2, p0, Lohq;->p:[Loib;

    new-instance v3, Loib;

    invoke-direct {v3}, Loib;-><init>()V

    aput-object v3, v2, v0

    .line 2218
    iget-object v2, p0, Lohq;->p:[Loib;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 2222
    :sswitch_18
    const/16 v0, 0xda

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 2223
    iget-object v0, p0, Lohq;->q:[Loim;

    if-nez v0, :cond_c

    move v0, v1

    .line 2224
    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Loim;

    .line 2225
    iget-object v3, p0, Lohq;->q:[Loim;

    if-eqz v3, :cond_b

    .line 2226
    iget-object v3, p0, Lohq;->q:[Loim;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2228
    :cond_b
    iput-object v2, p0, Lohq;->q:[Loim;

    .line 2229
    :goto_6
    iget-object v2, p0, Lohq;->q:[Loim;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_d

    .line 2230
    iget-object v2, p0, Lohq;->q:[Loim;

    new-instance v3, Loim;

    invoke-direct {v3}, Loim;-><init>()V

    aput-object v3, v2, v0

    .line 2231
    iget-object v2, p0, Lohq;->q:[Loim;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 2232
    invoke-virtual {p1}, Loxn;->a()I

    .line 2229
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 2223
    :cond_c
    iget-object v0, p0, Lohq;->q:[Loim;

    array-length v0, v0

    goto :goto_5

    .line 2235
    :cond_d
    iget-object v2, p0, Lohq;->q:[Loim;

    new-instance v3, Loim;

    invoke-direct {v3}, Loim;-><init>()V

    aput-object v3, v2, v0

    .line 2236
    iget-object v2, p0, Lohq;->q:[Loim;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 2240
    :sswitch_19
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lohq;->z:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 2244
    :sswitch_1a
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 2245
    if-eqz v0, :cond_e

    if-eq v0, v4, :cond_e

    if-eq v0, v5, :cond_e

    const/4 v2, 0x3

    if-eq v0, v2, :cond_e

    const/4 v2, 0x4

    if-ne v0, v2, :cond_f

    .line 2250
    :cond_e
    iput v0, p0, Lohq;->g:I

    goto/16 :goto_0

    .line 2252
    :cond_f
    iput v1, p0, Lohq;->g:I

    goto/16 :goto_0

    .line 2257
    :sswitch_1b
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lohq;->f:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 2261
    :sswitch_1c
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lohq;->s:Ljava/lang/String;

    goto/16 :goto_0

    .line 2265
    :sswitch_1d
    const/16 v0, 0x102

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 2266
    iget-object v0, p0, Lohq;->r:[Lohr;

    if-nez v0, :cond_11

    move v0, v1

    .line 2267
    :goto_7
    add-int/2addr v2, v0

    new-array v2, v2, [Lohr;

    .line 2268
    iget-object v3, p0, Lohq;->r:[Lohr;

    if-eqz v3, :cond_10

    .line 2269
    iget-object v3, p0, Lohq;->r:[Lohr;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2271
    :cond_10
    iput-object v2, p0, Lohq;->r:[Lohr;

    .line 2272
    :goto_8
    iget-object v2, p0, Lohq;->r:[Lohr;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_12

    .line 2273
    iget-object v2, p0, Lohq;->r:[Lohr;

    new-instance v3, Lohr;

    invoke-direct {v3}, Lohr;-><init>()V

    aput-object v3, v2, v0

    .line 2274
    iget-object v2, p0, Lohq;->r:[Lohr;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 2275
    invoke-virtual {p1}, Loxn;->a()I

    .line 2272
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 2266
    :cond_11
    iget-object v0, p0, Lohq;->r:[Lohr;

    array-length v0, v0

    goto :goto_7

    .line 2278
    :cond_12
    iget-object v2, p0, Lohq;->r:[Lohr;

    new-instance v3, Lohr;

    invoke-direct {v3}, Lohr;-><init>()V

    aput-object v3, v2, v0

    .line 2279
    iget-object v2, p0, Lohq;->r:[Lohr;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 2283
    :sswitch_1e
    const/16 v0, 0x112

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 2284
    iget-object v0, p0, Lohq;->F:[Ljava/lang/String;

    array-length v0, v0

    .line 2285
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 2286
    iget-object v3, p0, Lohq;->F:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2287
    iput-object v2, p0, Lohq;->F:[Ljava/lang/String;

    .line 2288
    :goto_9
    iget-object v2, p0, Lohq;->F:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_13

    .line 2289
    iget-object v2, p0, Lohq;->F:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 2290
    invoke-virtual {p1}, Loxn;->a()I

    .line 2288
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 2293
    :cond_13
    iget-object v2, p0, Lohq;->F:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    .line 2297
    :sswitch_1f
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lohq;->G:Ljava/lang/String;

    goto/16 :goto_0

    .line 2301
    :sswitch_20
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lohq;->x:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 2305
    :sswitch_21
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lohq;->A:Ljava/lang/String;

    goto/16 :goto_0

    .line 2309
    :sswitch_22
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lohq;->H:Ljava/lang/String;

    goto/16 :goto_0

    .line 2313
    :sswitch_23
    const/16 v0, 0x13a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 2314
    iget-object v0, p0, Lohq;->I:[Loie;

    if-nez v0, :cond_15

    move v0, v1

    .line 2315
    :goto_a
    add-int/2addr v2, v0

    new-array v2, v2, [Loie;

    .line 2316
    iget-object v3, p0, Lohq;->I:[Loie;

    if-eqz v3, :cond_14

    .line 2317
    iget-object v3, p0, Lohq;->I:[Loie;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2319
    :cond_14
    iput-object v2, p0, Lohq;->I:[Loie;

    .line 2320
    :goto_b
    iget-object v2, p0, Lohq;->I:[Loie;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_16

    .line 2321
    iget-object v2, p0, Lohq;->I:[Loie;

    new-instance v3, Loie;

    invoke-direct {v3}, Loie;-><init>()V

    aput-object v3, v2, v0

    .line 2322
    iget-object v2, p0, Lohq;->I:[Loie;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 2323
    invoke-virtual {p1}, Loxn;->a()I

    .line 2320
    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    .line 2314
    :cond_15
    iget-object v0, p0, Lohq;->I:[Loie;

    array-length v0, v0

    goto :goto_a

    .line 2326
    :cond_16
    iget-object v2, p0, Lohq;->I:[Loie;

    new-instance v3, Loie;

    invoke-direct {v3}, Loie;-><init>()V

    aput-object v3, v2, v0

    .line 2327
    iget-object v2, p0, Lohq;->I:[Loie;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 2068
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x21 -> :sswitch_2
        0x2a -> :sswitch_3
        0x32 -> :sswitch_4
        0x3a -> :sswitch_5
        0x40 -> :sswitch_6
        0x4a -> :sswitch_7
        0x58 -> :sswitch_8
        0x62 -> :sswitch_9
        0x6a -> :sswitch_a
        0x72 -> :sswitch_b
        0x7a -> :sswitch_c
        0x80 -> :sswitch_d
        0x88 -> :sswitch_e
        0x92 -> :sswitch_f
        0x9a -> :sswitch_10
        0xa2 -> :sswitch_11
        0xa8 -> :sswitch_12
        0xb2 -> :sswitch_13
        0xb8 -> :sswitch_14
        0xc2 -> :sswitch_15
        0xca -> :sswitch_16
        0xd2 -> :sswitch_17
        0xda -> :sswitch_18
        0xe0 -> :sswitch_19
        0xe8 -> :sswitch_1a
        0xf0 -> :sswitch_1b
        0xfa -> :sswitch_1c
        0x102 -> :sswitch_1d
        0x112 -> :sswitch_1e
        0x11a -> :sswitch_1f
        0x120 -> :sswitch_20
        0x12a -> :sswitch_21
        0x132 -> :sswitch_22
        0x13a -> :sswitch_23
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 7

    .prologue
    const/high16 v6, -0x80000000

    const/4 v0, 0x0

    .line 1752
    iget-object v1, p0, Lohq;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1753
    const/4 v1, 0x1

    iget-object v2, p0, Lohq;->a:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 1755
    :cond_0
    iget-object v1, p0, Lohq;->v:Ljava/lang/Double;

    if-eqz v1, :cond_1

    .line 1756
    const/4 v1, 0x4

    iget-object v2, p0, Lohq;->v:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Loxo;->a(ID)V

    .line 1758
    :cond_1
    iget-object v1, p0, Lohq;->e:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1759
    const/4 v1, 0x5

    iget-object v2, p0, Lohq;->e:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 1761
    :cond_2
    iget-object v1, p0, Lohq;->b:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 1762
    const/4 v1, 0x6

    iget-object v2, p0, Lohq;->b:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 1764
    :cond_3
    iget-object v1, p0, Lohq;->t:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 1765
    const/4 v1, 0x7

    iget-object v2, p0, Lohq;->t:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 1767
    :cond_4
    iget-object v1, p0, Lohq;->w:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    .line 1768
    const/16 v1, 0x8

    iget-object v2, p0, Lohq;->w:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IZ)V

    .line 1770
    :cond_5
    iget-object v1, p0, Lohq;->c:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 1771
    const/16 v1, 0x9

    iget-object v2, p0, Lohq;->c:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 1773
    :cond_6
    iget-object v1, p0, Lohq;->y:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    .line 1774
    const/16 v1, 0xb

    iget-object v2, p0, Lohq;->y:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IZ)V

    .line 1776
    :cond_7
    iget-object v1, p0, Lohq;->j:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 1777
    const/16 v1, 0xc

    iget-object v2, p0, Lohq;->j:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 1779
    :cond_8
    iget-object v1, p0, Lohq;->k:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 1780
    const/16 v1, 0xd

    iget-object v2, p0, Lohq;->k:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 1782
    :cond_9
    iget-object v1, p0, Lohq;->l:Ljava/lang/String;

    if-eqz v1, :cond_a

    .line 1783
    const/16 v1, 0xe

    iget-object v2, p0, Lohq;->l:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 1785
    :cond_a
    iget-object v1, p0, Lohq;->m:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 1786
    const/16 v1, 0xf

    iget-object v2, p0, Lohq;->m:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 1788
    :cond_b
    iget v1, p0, Lohq;->B:I

    if-eq v1, v6, :cond_c

    .line 1789
    const/16 v1, 0x10

    iget v2, p0, Lohq;->B:I

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 1791
    :cond_c
    iget-object v1, p0, Lohq;->h:Ljava/lang/Boolean;

    if-eqz v1, :cond_d

    .line 1792
    const/16 v1, 0x11

    iget-object v2, p0, Lohq;->h:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IZ)V

    .line 1794
    :cond_d
    iget-object v1, p0, Lohq;->C:[Ljava/lang/String;

    if-eqz v1, :cond_e

    .line 1795
    iget-object v2, p0, Lohq;->C:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_e

    aget-object v4, v2, v1

    .line 1796
    const/16 v5, 0x12

    invoke-virtual {p1, v5, v4}, Loxo;->a(ILjava/lang/String;)V

    .line 1795
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1799
    :cond_e
    iget-object v1, p0, Lohq;->o:Lohs;

    if-eqz v1, :cond_f

    .line 1800
    const/16 v1, 0x13

    iget-object v2, p0, Lohq;->o:Lohs;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 1802
    :cond_f
    iget-object v1, p0, Lohq;->d:Ljava/lang/String;

    if-eqz v1, :cond_10

    .line 1803
    const/16 v1, 0x14

    iget-object v2, p0, Lohq;->d:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 1805
    :cond_10
    iget-object v1, p0, Lohq;->i:Ljava/lang/Boolean;

    if-eqz v1, :cond_11

    .line 1806
    const/16 v1, 0x15

    iget-object v2, p0, Lohq;->i:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IZ)V

    .line 1808
    :cond_11
    iget-object v1, p0, Lohq;->n:Ljava/lang/String;

    if-eqz v1, :cond_12

    .line 1809
    const/16 v1, 0x16

    iget-object v2, p0, Lohq;->n:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 1811
    :cond_12
    iget-object v1, p0, Lohq;->D:[I

    if-eqz v1, :cond_13

    iget-object v1, p0, Lohq;->D:[I

    array-length v1, v1

    if-lez v1, :cond_13

    .line 1812
    iget-object v2, p0, Lohq;->D:[I

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_13

    aget v4, v2, v1

    .line 1813
    const/16 v5, 0x17

    invoke-virtual {p1, v5, v4}, Loxo;->a(II)V

    .line 1812
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1816
    :cond_13
    iget-object v1, p0, Lohq;->E:Loic;

    if-eqz v1, :cond_14

    .line 1817
    const/16 v1, 0x18

    iget-object v2, p0, Lohq;->E:Loic;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 1819
    :cond_14
    iget-object v1, p0, Lohq;->u:Ljava/lang/String;

    if-eqz v1, :cond_15

    .line 1820
    const/16 v1, 0x19

    iget-object v2, p0, Lohq;->u:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 1822
    :cond_15
    iget-object v1, p0, Lohq;->p:[Loib;

    if-eqz v1, :cond_17

    .line 1823
    iget-object v2, p0, Lohq;->p:[Loib;

    array-length v3, v2

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_17

    aget-object v4, v2, v1

    .line 1824
    if-eqz v4, :cond_16

    .line 1825
    const/16 v5, 0x1a

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 1823
    :cond_16
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1829
    :cond_17
    iget-object v1, p0, Lohq;->q:[Loim;

    if-eqz v1, :cond_19

    .line 1830
    iget-object v2, p0, Lohq;->q:[Loim;

    array-length v3, v2

    move v1, v0

    :goto_3
    if-ge v1, v3, :cond_19

    aget-object v4, v2, v1

    .line 1831
    if-eqz v4, :cond_18

    .line 1832
    const/16 v5, 0x1b

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 1830
    :cond_18
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 1836
    :cond_19
    iget-object v1, p0, Lohq;->z:Ljava/lang/Boolean;

    if-eqz v1, :cond_1a

    .line 1837
    const/16 v1, 0x1c

    iget-object v2, p0, Lohq;->z:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IZ)V

    .line 1839
    :cond_1a
    iget v1, p0, Lohq;->g:I

    if-eq v1, v6, :cond_1b

    .line 1840
    const/16 v1, 0x1d

    iget v2, p0, Lohq;->g:I

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 1842
    :cond_1b
    iget-object v1, p0, Lohq;->f:Ljava/lang/Boolean;

    if-eqz v1, :cond_1c

    .line 1843
    const/16 v1, 0x1e

    iget-object v2, p0, Lohq;->f:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IZ)V

    .line 1845
    :cond_1c
    iget-object v1, p0, Lohq;->s:Ljava/lang/String;

    if-eqz v1, :cond_1d

    .line 1846
    const/16 v1, 0x1f

    iget-object v2, p0, Lohq;->s:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 1848
    :cond_1d
    iget-object v1, p0, Lohq;->r:[Lohr;

    if-eqz v1, :cond_1f

    .line 1849
    iget-object v2, p0, Lohq;->r:[Lohr;

    array-length v3, v2

    move v1, v0

    :goto_4
    if-ge v1, v3, :cond_1f

    aget-object v4, v2, v1

    .line 1850
    if-eqz v4, :cond_1e

    .line 1851
    const/16 v5, 0x20

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 1849
    :cond_1e
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 1855
    :cond_1f
    iget-object v1, p0, Lohq;->F:[Ljava/lang/String;

    if-eqz v1, :cond_20

    .line 1856
    iget-object v2, p0, Lohq;->F:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_5
    if-ge v1, v3, :cond_20

    aget-object v4, v2, v1

    .line 1857
    const/16 v5, 0x22

    invoke-virtual {p1, v5, v4}, Loxo;->a(ILjava/lang/String;)V

    .line 1856
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 1860
    :cond_20
    iget-object v1, p0, Lohq;->G:Ljava/lang/String;

    if-eqz v1, :cond_21

    .line 1861
    const/16 v1, 0x23

    iget-object v2, p0, Lohq;->G:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 1863
    :cond_21
    iget-object v1, p0, Lohq;->x:Ljava/lang/Boolean;

    if-eqz v1, :cond_22

    .line 1864
    const/16 v1, 0x24

    iget-object v2, p0, Lohq;->x:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IZ)V

    .line 1866
    :cond_22
    iget-object v1, p0, Lohq;->A:Ljava/lang/String;

    if-eqz v1, :cond_23

    .line 1867
    const/16 v1, 0x25

    iget-object v2, p0, Lohq;->A:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 1869
    :cond_23
    iget-object v1, p0, Lohq;->H:Ljava/lang/String;

    if-eqz v1, :cond_24

    .line 1870
    const/16 v1, 0x26

    iget-object v2, p0, Lohq;->H:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 1872
    :cond_24
    iget-object v1, p0, Lohq;->I:[Loie;

    if-eqz v1, :cond_26

    .line 1873
    iget-object v1, p0, Lohq;->I:[Loie;

    array-length v2, v1

    :goto_6
    if-ge v0, v2, :cond_26

    aget-object v3, v1, v0

    .line 1874
    if-eqz v3, :cond_25

    .line 1875
    const/16 v4, 0x27

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 1873
    :cond_25
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 1879
    :cond_26
    iget-object v0, p0, Lohq;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1881
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1375
    invoke-virtual {p0, p1}, Lohq;->a(Loxn;)Lohq;

    move-result-object v0

    return-object v0
.end method
