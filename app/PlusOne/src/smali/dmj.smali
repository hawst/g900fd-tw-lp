.class public final Ldmj;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmhm;",
        "Lmhn;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/util/Random;


# instance fields
.field private final b:Ljava/lang/String;

.field private final p:Ljava/lang/String;

.field private final q:Ljava/lang/String;

.field private final r:Ljava/lang/String;

.field private final s:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, Ldmj;->a:Ljava/util/Random;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)V
    .locals 6

    .prologue
    .line 46
    const-string v3, "postcomment"

    new-instance v4, Lmhm;

    invoke-direct {v4}, Lmhm;-><init>()V

    new-instance v5, Lmhn;

    invoke-direct {v5}, Lmhn;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 47
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 48
    invoke-interface {v0, p2}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "gaia_id"

    .line 49
    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldmj;->r:Ljava/lang/String;

    .line 50
    iput-object p3, p0, Ldmj;->b:Ljava/lang/String;

    .line 51
    iput-object p5, p0, Ldmj;->p:Ljava/lang/String;

    .line 52
    iput-object p4, p0, Ldmj;->q:Ljava/lang/String;

    .line 53
    iput-object p6, p0, Ldmj;->s:[B

    .line 54
    return-void
.end method


# virtual methods
.method protected a(Lmhm;)V
    .locals 7

    .prologue
    .line 58
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 59
    new-instance v2, Lnvq;

    invoke-direct {v2}, Lnvq;-><init>()V

    iput-object v2, p1, Lmhm;->a:Lnvq;

    .line 60
    iget-object v2, p1, Lmhm;->a:Lnvq;

    .line 61
    iget-object v3, p0, Ldmj;->r:Ljava/lang/String;

    sget-object v4, Ldmj;->a:Ljava/util/Random;

    invoke-virtual {v4}, Ljava/util/Random;->nextInt()I

    move-result v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x1f

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lnvq;->b:Ljava/lang/String;

    .line 62
    iget-object v3, p0, Ldmj;->b:Ljava/lang/String;

    iput-object v3, v2, Lnvq;->a:Ljava/lang/String;

    .line 63
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v2, Lnvq;->d:Ljava/lang/Long;

    .line 64
    iget-object v0, p0, Ldmj;->p:Ljava/lang/String;

    iput-object v0, v2, Lnvq;->c:Ljava/lang/String;

    .line 66
    iget-object v0, p0, Ldmj;->s:[B

    invoke-static {v0}, Llap;->a([B)Lnwr;

    move-result-object v0

    .line 67
    if-eqz v0, :cond_0

    .line 68
    new-instance v1, Logw;

    invoke-direct {v1}, Logw;-><init>()V

    iput-object v1, v2, Lnvq;->f:Logw;

    .line 69
    iget-object v1, v2, Lnvq;->f:Logw;

    iput-object v0, v1, Logw;->a:Lnwr;

    .line 78
    :cond_0
    return-void
.end method

.method protected a(Lmhn;)V
    .locals 6

    .prologue
    .line 82
    iget-object v0, p1, Lmhn;->a:Lnvz;

    .line 83
    if-nez v0, :cond_1

    .line 125
    :cond_0
    :goto_0
    return-void

    .line 88
    :cond_1
    iget-object v0, v0, Lnvz;->a:Lofa;

    .line 89
    if-eqz v0, :cond_0

    .line 94
    iget-object v1, p0, Ldmj;->b:Ljava/lang/String;

    iget-object v2, v0, Lofa;->h:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 99
    new-instance v1, Ldrq;

    invoke-direct {v1}, Ldrq;-><init>()V

    .line 100
    const/4 v2, 0x5

    iput v2, v1, Ldrq;->a:I

    .line 101
    iget-object v2, v0, Lofa;->c:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    iput-object v2, v1, Ldrq;->e:[B

    .line 102
    iget-object v2, v0, Lofa;->b:Ljava/lang/String;

    iput-object v2, v1, Ldrq;->d:Ljava/lang/String;

    .line 103
    iget-object v2, v0, Lofa;->g:Ljava/lang/String;

    iput-object v2, v1, Ldrq;->c:Ljava/lang/String;

    .line 104
    iget-object v2, v0, Lofa;->e:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iput-wide v2, v1, Ldrq;->b:J

    .line 106
    new-instance v2, Ldru;

    invoke-direct {v2}, Ldru;-><init>()V

    .line 107
    iget-object v3, v0, Lofa;->f:Ljava/lang/String;

    iput-object v3, v2, Ldru;->commentId:Ljava/lang/String;

    .line 108
    iget-object v3, v0, Lofa;->c:Ljava/lang/String;

    iput-object v3, v2, Ldru;->text:Ljava/lang/String;

    .line 109
    iget-object v3, v0, Lofa;->j:Ljava/lang/Boolean;

    if-eqz v3, :cond_2

    .line 110
    iget-object v3, v0, Lofa;->j:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    iput-boolean v3, v2, Ldru;->ownedByViewer:Z

    .line 112
    :cond_2
    iget-object v3, v0, Lofa;->l:Loae;

    if-eqz v3, :cond_3

    iget-object v3, v0, Lofa;->l:Loae;

    iget-object v3, v3, Loae;->e:Ljava/lang/Integer;

    if-eqz v3, :cond_3

    .line 113
    iget-object v0, v0, Lofa;->l:Loae;

    iget-object v0, v0, Loae;->e:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v4, v0

    iput-wide v4, v2, Ldru;->totalPlusOnes:J

    .line 117
    :cond_3
    iget-object v0, v1, Ldrq;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, v2, Ldru;->text:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 118
    sget-object v0, Ldrm;->a:Lesn;

    invoke-virtual {v0, v2}, Lesn;->a(Ljava/lang/Object;)[B

    move-result-object v0

    iput-object v0, v1, Ldrq;->e:[B

    .line 119
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 120
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 121
    iget-object v0, p0, Ldmj;->f:Landroid/content/Context;

    iget v1, p0, Ldmj;->c:I

    iget-object v2, p0, Ldmj;->q:Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v5, 0x1

    invoke-static/range {v0 .. v5}, Ldrm;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/util/List;Z)V

    goto/16 :goto_0
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 28
    check-cast p1, Lmhm;

    invoke-virtual {p0, p1}, Ldmj;->a(Lmhm;)V

    return-void
.end method

.method protected synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 28
    check-cast p1, Lmhn;

    invoke-virtual {p0, p1}, Ldmj;->a(Lmhn;)V

    return-void
.end method
