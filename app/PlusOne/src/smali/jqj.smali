.class public final Ljqj;
.super Lhny;
.source "PG"


# instance fields
.field private final a:Lipj;

.field private final b:Liok;

.field private final c:Landroid/content/Context;

.field private final d:I

.field private final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 34
    const-string v0, "RemoveCircleTask"

    invoke-direct {p0, p1, v0}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 35
    const-class v0, Liof;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liof;

    .line 36
    const-class v1, Liok;

    invoke-static {p1, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Liok;

    iput-object v1, p0, Ljqj;->b:Liok;

    .line 37
    invoke-interface {v0}, Liof;->a()Lipj;

    move-result-object v0

    iput-object v0, p0, Ljqj;->a:Lipj;

    .line 38
    iput-object p1, p0, Ljqj;->c:Landroid/content/Context;

    .line 39
    iput p2, p0, Ljqj;->d:I

    .line 40
    iput-object p3, p0, Ljqj;->e:Ljava/lang/String;

    .line 41
    return-void
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 46
    iget-object v0, p0, Ljqj;->c:Landroid/content/Context;

    const-class v1, Lhei;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iget v1, p0, Ljqj;->d:I

    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 47
    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 48
    const-string v2, "effective_gaia_id"

    invoke-interface {v0, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 51
    iget-object v2, p0, Ljqj;->a:Lipj;

    const-wide/16 v4, 0xa

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, v4, v5, v3}, Lipj;->a(JLjava/util/concurrent/TimeUnit;)Liiw;

    move-result-object v2

    .line 54
    invoke-interface {v2}, Liiw;->a()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 55
    iget-object v2, p0, Ljqj;->b:Liok;

    iget-object v3, p0, Ljqj;->a:Lipj;

    iget-object v4, p0, Ljqj;->e:Ljava/lang/String;

    invoke-interface {v2, v3, v1, v0, v4}, Liok;->a(Lipj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lijq;

    move-result-object v0

    .line 56
    invoke-interface {v0}, Lijq;->a()Lijs;

    move-result-object v2

    .line 57
    invoke-interface {v2}, Lijs;->a()Lika;

    move-result-object v0

    invoke-interface {v0}, Lika;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0xc8

    .line 59
    :goto_0
    iget-object v3, p0, Ljqj;->c:Landroid/content/Context;

    const/16 v4, 0xa

    invoke-static {v3, v1, v4, v2}, Ljpe;->a(Landroid/content/Context;Ljava/lang/String;ILijs;)V

    .line 64
    :goto_1
    iget-object v1, p0, Ljqj;->a:Lipj;

    invoke-interface {v1}, Lipj;->d()V

    .line 66
    invoke-static {}, Ljpe;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 67
    const-string v1, "accountId: %s. circleId: %s. statusCode: %s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Ljqj;->d:I

    .line 68
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Ljqj;->e:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 67
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    invoke-static {}, Ljpe;->b()V

    .line 71
    :cond_0
    new-instance v1, Lhoz;

    invoke-direct {v1, v0, v6, v6}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    return-object v1

    .line 58
    :cond_1
    invoke-interface {v2}, Lijs;->a()Lika;

    move-result-object v0

    invoke-interface {v0}, Lika;->c()I

    move-result v0

    goto :goto_0

    .line 62
    :cond_2
    invoke-interface {v2}, Liiw;->b()I

    move-result v0

    goto :goto_1
.end method
