.class public final Ldnp;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmjw;",
        "Lmjx;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Liuh;

.field private final b:Llae;

.field private final p:Z

.field private final q:Z

.field private r:Llae;

.field private s:Llae;

.field private t:Llae;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILiuh;Llae;Z)V
    .locals 6

    .prologue
    .line 43
    const-string v3, "snaptoplace"

    new-instance v4, Lmjw;

    invoke-direct {v4}, Lmjw;-><init>()V

    new-instance v5, Lmjx;

    invoke-direct {v5}, Lmjx;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 44
    iput-object p3, p0, Ldnp;->a:Liuh;

    .line 45
    iput-object p4, p0, Ldnp;->b:Llae;

    .line 46
    iput-boolean p5, p0, Ldnp;->p:Z

    .line 47
    iget-object v0, p0, Ldnp;->a:Liuh;

    invoke-virtual {v0}, Liuh;->b()Z

    move-result v0

    iput-boolean v0, p0, Ldnp;->q:Z

    .line 48
    return-void
.end method


# virtual methods
.method public S_()Z
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Ldnp;->t:Llae;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Lmjw;)V
    .locals 6

    .prologue
    const-wide v4, 0x416312d000000000L    # 1.0E7

    .line 57
    new-instance v0, Lmse;

    invoke-direct {v0}, Lmse;-><init>()V

    iput-object v0, p1, Lmjw;->a:Lmse;

    .line 58
    iget-object v0, p1, Lmjw;->a:Lmse;

    .line 59
    iget-object v1, p0, Ldnp;->a:Liuh;

    invoke-virtual {v1}, Liuh;->a()Landroid/location/Location;

    move-result-object v1

    .line 60
    invoke-virtual {v1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    mul-double/2addr v2, v4

    double-to-int v2, v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v0, Lmse;->a:Ljava/lang/Integer;

    .line 61
    invoke-virtual {v1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    mul-double/2addr v2, v4

    double-to-int v2, v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v0, Lmse;->b:Ljava/lang/Integer;

    .line 62
    invoke-virtual {v1}, Landroid/location/Location;->hasAccuracy()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 63
    invoke-virtual {v1}, Landroid/location/Location;->getAccuracy()F

    move-result v1

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    iput-object v1, v0, Lmse;->c:Ljava/lang/Double;

    .line 65
    :cond_0
    iget-object v1, p0, Ldnp;->a:Liuh;

    invoke-virtual {v1}, Liuh;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 66
    iget-object v1, p0, Ldnp;->a:Liuh;

    invoke-virtual {v1}, Liuh;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lmse;->d:Ljava/lang/String;

    .line 68
    :cond_1
    return-void
.end method

.method protected a(Lmjx;)V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 76
    iget-object v3, p1, Lmjx;->a:Lmsg;

    .line 77
    iget-object v0, v3, Lmsg;->b:Lmsc;

    if-eqz v0, :cond_0

    .line 78
    new-instance v0, Llae;

    const/4 v4, 0x1

    iget-object v5, v3, Lmsg;->b:Lmsc;

    iget-object v5, v5, Lmsc;->b:Lofq;

    invoke-direct {v0, v4, v5}, Llae;-><init>(ILofq;)V

    iput-object v0, p0, Ldnp;->r:Llae;

    .line 82
    :cond_0
    iget-object v0, v3, Lmsg;->c:Lmsc;

    if-eqz v0, :cond_1

    .line 83
    new-instance v0, Llae;

    const/4 v4, 0x2

    iget-object v5, v3, Lmsg;->c:Lmsc;

    iget-object v5, v5, Lmsc;->b:Lofq;

    invoke-direct {v0, v4, v5}, Llae;-><init>(ILofq;)V

    iput-object v0, p0, Ldnp;->s:Llae;

    .line 86
    :cond_1
    iget-object v4, v3, Lmsg;->a:[Lmsc;

    .line 87
    if-nez v4, :cond_4

    move v0, v1

    .line 88
    :goto_0
    iget-object v3, v3, Lmsg;->d:Ljava/lang/Boolean;

    invoke-static {v3}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 89
    if-lez v0, :cond_2

    .line 90
    new-instance v3, Llae;

    aget-object v5, v4, v1

    iget-object v5, v5, Lmsc;->b:Lofq;

    invoke-direct {v3, v7, v5}, Llae;-><init>(ILofq;)V

    iput-object v3, p0, Ldnp;->t:Llae;

    .line 96
    :cond_2
    iget-boolean v3, p0, Ldnp;->p:Z

    if-eqz v3, :cond_7

    .line 99
    if-lez v0, :cond_5

    .line 100
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 101
    :goto_1
    if-ge v1, v0, :cond_6

    .line 102
    aget-object v3, v4, v1

    iget-object v3, v3, Lmsc;->b:Lofq;

    .line 103
    new-instance v6, Llae;

    invoke-direct {v6, v7, v3}, Llae;-><init>(ILofq;)V

    .line 105
    iget-object v3, p0, Ldnp;->b:Llae;

    invoke-virtual {v6, v3}, Llae;->b(Llae;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 106
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 101
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 87
    :cond_4
    array-length v0, v4

    goto :goto_0

    :cond_5
    move-object v5, v2

    .line 111
    :cond_6
    iget-object v0, p0, Ldnp;->r:Llae;

    .line 112
    iget-object v4, p0, Ldnp;->s:Llae;

    .line 113
    iget-boolean v1, p0, Ldnp;->q:Z

    if-eqz v1, :cond_8

    move-object v4, v2

    move-object v3, v2

    .line 124
    :goto_2
    iget-object v0, p0, Ldnp;->f:Landroid/content/Context;

    iget v1, p0, Ldnp;->c:I

    iget-object v2, p0, Ldnp;->a:Liuh;

    invoke-static/range {v0 .. v5}, Ldrm;->a(Landroid/content/Context;ILiuh;Llae;Llae;Ljava/util/ArrayList;)V

    .line 127
    :cond_7
    return-void

    .line 116
    :cond_8
    iget-object v1, p0, Ldnp;->b:Llae;

    if-eqz v1, :cond_a

    .line 117
    iget-object v1, p0, Ldnp;->b:Llae;

    iget-object v3, p0, Ldnp;->r:Llae;

    invoke-virtual {v1, v3}, Llae;->b(Llae;)Z

    move-result v1

    if-eqz v1, :cond_9

    move-object v0, v2

    .line 120
    :cond_9
    iget-object v1, p0, Ldnp;->b:Llae;

    iget-object v3, p0, Ldnp;->s:Llae;

    invoke-virtual {v1, v3}, Llae;->b(Llae;)Z

    move-result v1

    if-eqz v1, :cond_a

    move-object v4, v2

    move-object v3, v0

    .line 121
    goto :goto_2

    :cond_a
    move-object v3, v0

    goto :goto_2
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 23
    check-cast p1, Lmjw;

    invoke-virtual {p0, p1}, Ldnp;->a(Lmjw;)V

    return-void
.end method

.method protected synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 23
    check-cast p1, Lmjx;

    invoke-virtual {p0, p1}, Ldnp;->a(Lmjx;)V

    return-void
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Ldnp;->r:Llae;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Llae;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Ldnp;->r:Llae;

    return-object v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Ldnp;->s:Llae;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Llae;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Ldnp;->s:Llae;

    return-object v0
.end method

.method public i()Llae;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Ldnp;->t:Llae;

    return-object v0
.end method
