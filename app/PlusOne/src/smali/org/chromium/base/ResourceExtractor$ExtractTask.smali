.class Lorg/chromium/base/ResourceExtractor$ExtractTask;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method private a(Ljava/io/File;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x0

    .line 200
    invoke-static {v1}, Lorg/chromium/base/ResourceExtractor;->c(Lorg/chromium/base/ResourceExtractor;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 202
    const/4 v2, 0x0

    :try_start_0
    invoke-static {v2}, Lorg/chromium/base/ResourceExtractor;->c(Lorg/chromium/base/ResourceExtractor;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 210
    if-nez v0, :cond_1

    .line 211
    const-string v0, "pak_timestamp-"

    .line 234
    :cond_0
    :goto_0
    return-object v0

    .line 207
    :catch_0
    move-exception v0

    const-string v0, "pak_timestamp-"

    goto :goto_0

    .line 214
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "pak_timestamp-"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, v0, Landroid/content/pm/PackageInfo;->lastUpdateTime:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 216
    new-instance v2, Lorg/chromium/base/ResourceExtractor$ExtractTask$1;

    invoke-direct {v2}, Lorg/chromium/base/ResourceExtractor$ExtractTask$1;-><init>()V

    invoke-virtual {p1, v2}, Ljava/io/File;->list(Ljava/io/FilenameFilter;)[Ljava/lang/String;

    move-result-object v2

    .line 223
    array-length v3, v2

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 229
    aget-object v2, v2, v6

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 234
    goto :goto_0
.end method


# virtual methods
.method protected varargs a()Ljava/lang/Void;
    .locals 22

    .prologue
    .line 57
    const/4 v2, 0x0

    invoke-static {v2}, Lorg/chromium/base/ResourceExtractor;->a(Lorg/chromium/base/ResourceExtractor;)Ljava/io/File;

    move-result-object v7

    .line 58
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v7}, Ljava/io/File;->mkdirs()Z

    move-result v2

    if-nez v2, :cond_0

    .line 59
    const-string v2, "ResourceExtractor"

    const-string v3, "Unable to create pak resources directory!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    const/4 v2, 0x0

    .line 189
    :goto_0
    return-object v2

    .line 63
    :cond_0
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lorg/chromium/base/ResourceExtractor$ExtractTask;->a(Ljava/io/File;)Ljava/lang/String;

    move-result-object v10

    .line 64
    if-eqz v10, :cond_1

    .line 65
    const/4 v2, 0x0

    invoke-static {v2}, Lorg/chromium/base/ResourceExtractor;->b(Lorg/chromium/base/ResourceExtractor;)V

    .line 68
    :cond_1
    const/4 v2, 0x0

    invoke-static {v2}, Lorg/chromium/base/ResourceExtractor;->c(Lorg/chromium/base/ResourceExtractor;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v11

    .line 69
    const-string v2, "Pak filenames"

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v11, v2, v3}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v2

    check-cast v2, Ljava/util/HashSet;

    .line 71
    invoke-static {}, Lorg/chromium/base/LocaleUtils;->getDefaultLocale()Ljava/lang/String;

    move-result-object v3

    .line 72
    const-string v4, "-"

    const/4 v5, 0x2

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aget-object v5, v3, v4

    .line 74
    const-string v3, "Last language"

    const-string v4, ""

    invoke-interface {v11, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v2}, Ljava/util/HashSet;->size()I

    move-result v3

    invoke-static {}, Lorg/chromium/base/ResourceExtractor;->a()[Ljava/lang/String;

    move-result-object v4

    array-length v4, v4

    if-lt v3, v4, :cond_3

    .line 76
    const/4 v4, 0x1

    .line 77
    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_17

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 78
    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v7, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_2

    .line 79
    const/4 v3, 0x0

    .line 83
    :goto_1
    if-eqz v3, :cond_4

    const/4 v2, 0x0

    goto :goto_0

    .line 85
    :cond_3
    invoke-interface {v11}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "Last language"

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 88
    :cond_4
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 89
    invoke-static {}, Lorg/chromium/base/ResourceExtractor;->a()[Ljava/lang/String;

    move-result-object v6

    array-length v8, v6

    const/4 v3, 0x0

    :goto_2
    if-ge v3, v8, :cond_6

    aget-object v9, v6, v3

    .line 90
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v12

    if-lez v12, :cond_5

    const/16 v12, 0x7c

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 91
    :cond_5
    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "\\Q"

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v12, "\\E"

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 94
    :cond_6
    invoke-static {}, Lorg/chromium/base/ResourceExtractor;->b()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 95
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-lez v3, :cond_7

    const/16 v3, 0x7c

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 99
    :cond_7
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 100
    const-string v3, "(-\\w+)?\\.pak"

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 103
    :cond_8
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v12

    .line 105
    const/4 v3, 0x0

    invoke-static {v3}, Lorg/chromium/base/ResourceExtractor;->c(Lorg/chromium/base/ResourceExtractor;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v13

    .line 110
    const/4 v4, 0x0

    .line 111
    :try_start_0
    const-string v3, ""

    invoke-virtual {v13, v3}, Landroid/content/res/AssetManager;->list(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v14

    .line 112
    array-length v15, v14

    const/4 v3, 0x0

    move v9, v3

    move-object v3, v4

    :goto_3
    if-ge v9, v15, :cond_15

    aget-object v16, v14, v9

    .line 113
    move-object/from16 v0, v16

    invoke-virtual {v12, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/regex/Matcher;->matches()Z

    move-result v4

    if-eqz v4, :cond_12

    .line 114
    const-string v4, "icudtl.dat"

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_9

    const-string v4, "natives_blob.bin"

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_9

    const-string v4, "snapshot_blob.bin"

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    :cond_9
    const/4 v4, 0x1

    move v8, v4

    .line 119
    :goto_4
    new-instance v17, Ljava/io/File;

    if-eqz v8, :cond_e

    const/4 v4, 0x0

    invoke-static {v4}, Lorg/chromium/base/ResourceExtractor;->d(Lorg/chromium/base/ResourceExtractor;)Ljava/io/File;

    move-result-object v4

    :goto_5
    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-direct {v0, v4, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 121
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->exists()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-nez v4, :cond_12

    .line 122
    const/4 v4, 0x0

    .line 126
    const/4 v5, 0x0

    .line 128
    :try_start_1
    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    move-result-object v6

    .line 129
    :try_start_2
    new-instance v4, Ljava/io/FileOutputStream;

    move-object/from16 v0, v17

    invoke-direct {v4, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_4

    .line 130
    if-nez v3, :cond_a

    .line 132
    const/16 v3, 0x4000

    :try_start_3
    new-array v3, v3, [B

    .line 135
    :cond_a
    :goto_6
    const/4 v5, 0x0

    const/16 v18, 0x4000

    move/from16 v0, v18

    invoke-virtual {v6, v3, v5, v0}, Ljava/io/InputStream;->read([BII)I

    move-result v5

    const/16 v18, -0x1

    move/from16 v0, v18

    if-eq v5, v0, :cond_f

    .line 137
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v4, v3, v0, v5}, Ljava/io/OutputStream;->write([BII)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_6

    .line 163
    :catchall_0
    move-exception v2

    move-object v3, v4

    move-object v4, v6

    .line 155
    :goto_7
    if-eqz v4, :cond_b

    .line 156
    :try_start_4
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 159
    :cond_b
    if-eqz v3, :cond_c

    .line 160
    :try_start_5
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V

    :cond_c
    throw v2
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    .line 165
    :catch_0
    move-exception v2

    .line 170
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Exception unpacking required pak resources: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 171
    const/4 v2, 0x0

    invoke-static {v2}, Lorg/chromium/base/ResourceExtractor;->b(Lorg/chromium/base/ResourceExtractor;)V

    .line 172
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 114
    :cond_d
    const/4 v4, 0x0

    move v8, v4

    goto :goto_4

    :cond_e
    move-object v4, v7

    .line 119
    goto :goto_5

    .line 139
    :cond_f
    :try_start_6
    invoke-virtual {v4}, Ljava/io/OutputStream;->flush()V

    .line 142
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->length()J

    move-result-wide v18

    const-wide/16 v20, 0x0

    cmp-long v5, v18, v20

    if-nez v5, :cond_10

    .line 143
    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " extracted with 0 length!"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 146
    :cond_10
    if-nez v8, :cond_13

    .line 147
    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 155
    :goto_8
    if-eqz v6, :cond_11

    .line 156
    :try_start_7
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 159
    :cond_11
    :try_start_8
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_0

    .line 112
    :cond_12
    add-int/lit8 v4, v9, 0x1

    move v9, v4

    goto/16 :goto_3

    .line 151
    :cond_13
    const/4 v5, 0x1

    const/4 v8, 0x0

    :try_start_9
    move-object/from16 v0, v17

    invoke-virtual {v0, v5, v8}, Ljava/io/File;->setReadable(ZZ)Z
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_8

    .line 159
    :catchall_1
    move-exception v2

    .line 160
    :try_start_a
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V

    throw v2

    .line 159
    :catchall_2
    move-exception v2

    if-eqz v3, :cond_14

    .line 160
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V

    :cond_14
    throw v2
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_0

    .line 177
    :cond_15
    if-eqz v10, :cond_16

    .line 179
    :try_start_b
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v7, v10}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->createNewFile()Z
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_1

    .line 183
    :cond_16
    :goto_9
    invoke-interface {v11}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "Pak filenames"

    invoke-interface {v3, v4}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 188
    invoke-interface {v11}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "Pak filenames"

    invoke-interface {v3, v4, v2}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 189
    const/4 v2, 0x0

    goto/16 :goto_0

    :catch_1
    move-exception v3

    goto :goto_9

    .line 163
    :catchall_3
    move-exception v2

    move-object v3, v5

    goto/16 :goto_7

    :catchall_4
    move-exception v2

    move-object v3, v5

    move-object v4, v6

    goto/16 :goto_7

    :cond_17
    move v3, v4

    goto/16 :goto_1
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 49
    invoke-virtual {p0}, Lorg/chromium/base/ResourceExtractor$ExtractTask;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
