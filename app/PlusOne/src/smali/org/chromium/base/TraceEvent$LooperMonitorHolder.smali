.class final Lorg/chromium/base/TraceEvent$LooperMonitorHolder;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Lorg/chromium/base/TraceEvent$BasicLooperMonitor;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 160
    invoke-static {}, Lorg/chromium/base/CommandLine;->b()Lorg/chromium/base/CommandLine;

    move-result-object v0

    const-string v1, "enable-idle-tracing"

    invoke-virtual {v0, v1}, Lorg/chromium/base/CommandLine;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lorg/chromium/base/TraceEvent$IdleTracingLooperMonitor;

    invoke-direct {v0}, Lorg/chromium/base/TraceEvent$IdleTracingLooperMonitor;-><init>()V

    :goto_0
    sput-object v0, Lorg/chromium/base/TraceEvent$LooperMonitorHolder;->a:Lorg/chromium/base/TraceEvent$BasicLooperMonitor;

    return-void

    :cond_0
    new-instance v0, Lorg/chromium/base/TraceEvent$BasicLooperMonitor;

    invoke-direct {v0}, Lorg/chromium/base/TraceEvent$BasicLooperMonitor;-><init>()V

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 159
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a()Lorg/chromium/base/TraceEvent$BasicLooperMonitor;
    .locals 1

    .prologue
    .line 159
    sget-object v0, Lorg/chromium/base/TraceEvent$LooperMonitorHolder;->a:Lorg/chromium/base/TraceEvent$BasicLooperMonitor;

    return-object v0
.end method
