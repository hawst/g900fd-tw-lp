.class public Lorg/chromium/base/BaseChromiumApplication;
.super Landroid/app/Application;
.source "PG"


# instance fields
.field private a:Lorg/chromium/base/ObserverList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/chromium/base/ObserverList",
            "<",
            "Lorg/chromium/base/BaseChromiumApplication$WindowFocusChangedListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    .line 28
    new-instance v0, Lorg/chromium/base/ObserverList;

    invoke-direct {v0}, Lorg/chromium/base/ObserverList;-><init>()V

    iput-object v0, p0, Lorg/chromium/base/BaseChromiumApplication;->a:Lorg/chromium/base/ObserverList;

    return-void
.end method

.method static synthetic a(Lorg/chromium/base/BaseChromiumApplication;)Lorg/chromium/base/ObserverList;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lorg/chromium/base/BaseChromiumApplication;->a:Lorg/chromium/base/ObserverList;

    return-object v0
.end method


# virtual methods
.method public a(Lorg/chromium/base/BaseChromiumApplication$WindowFocusChangedListener;)V
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lorg/chromium/base/BaseChromiumApplication;->a:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0, p1}, Lorg/chromium/base/ObserverList;->a(Ljava/lang/Object;)Z

    .line 90
    return-void
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 33
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 34
    invoke-static {p0}, Lorg/chromium/base/ApplicationStatus;->a(Lorg/chromium/base/BaseChromiumApplication;)V

    .line 36
    new-instance v0, Lorg/chromium/base/BaseChromiumApplication$1;

    invoke-direct {v0, p0}, Lorg/chromium/base/BaseChromiumApplication$1;-><init>(Lorg/chromium/base/BaseChromiumApplication;)V

    invoke-virtual {p0, v0}, Lorg/chromium/base/BaseChromiumApplication;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 82
    return-void
.end method
