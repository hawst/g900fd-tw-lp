.class Lorg/chromium/base/ObserverList$ObserverListIterator;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lorg/chromium/base/ObserverList$RewindableIterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lorg/chromium/base/ObserverList$RewindableIterator",
        "<TE;>;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Z

.field private synthetic d:Lorg/chromium/base/ObserverList;


# direct methods
.method constructor <init>(Lorg/chromium/base/ObserverList;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 190
    iput-object p1, p0, Lorg/chromium/base/ObserverList$ObserverListIterator;->d:Lorg/chromium/base/ObserverList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 187
    iput v0, p0, Lorg/chromium/base/ObserverList$ObserverListIterator;->b:I

    .line 188
    iput-boolean v0, p0, Lorg/chromium/base/ObserverList$ObserverListIterator;->c:Z

    .line 191
    invoke-static {p1}, Lorg/chromium/base/ObserverList;->a(Lorg/chromium/base/ObserverList;)V

    .line 192
    invoke-static {p1}, Lorg/chromium/base/ObserverList;->b(Lorg/chromium/base/ObserverList;)I

    move-result v0

    iput v0, p0, Lorg/chromium/base/ObserverList$ObserverListIterator;->a:I

    .line 193
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 237
    iget-boolean v0, p0, Lorg/chromium/base/ObserverList$ObserverListIterator;->c:Z

    if-nez v0, :cond_0

    .line 238
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/chromium/base/ObserverList$ObserverListIterator;->c:Z

    .line 239
    iget-object v0, p0, Lorg/chromium/base/ObserverList$ObserverListIterator;->d:Lorg/chromium/base/ObserverList;

    invoke-static {v0}, Lorg/chromium/base/ObserverList;->c(Lorg/chromium/base/ObserverList;)V

    .line 241
    :cond_0
    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 2

    .prologue
    .line 206
    iget v0, p0, Lorg/chromium/base/ObserverList$ObserverListIterator;->b:I

    .line 208
    :goto_0
    iget v1, p0, Lorg/chromium/base/ObserverList$ObserverListIterator;->a:I

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lorg/chromium/base/ObserverList$ObserverListIterator;->d:Lorg/chromium/base/ObserverList;

    invoke-static {v1, v0}, Lorg/chromium/base/ObserverList;->a(Lorg/chromium/base/ObserverList;I)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    .line 209
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 211
    :cond_0
    iget v1, p0, Lorg/chromium/base/ObserverList$ObserverListIterator;->a:I

    if-ge v0, v1, :cond_1

    const/4 v0, 0x1

    .line 215
    :goto_1
    return v0

    .line 214
    :cond_1
    invoke-direct {p0}, Lorg/chromium/base/ObserverList$ObserverListIterator;->a()V

    .line 215
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public next()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 221
    :goto_0
    iget v0, p0, Lorg/chromium/base/ObserverList$ObserverListIterator;->b:I

    iget v1, p0, Lorg/chromium/base/ObserverList$ObserverListIterator;->a:I

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lorg/chromium/base/ObserverList$ObserverListIterator;->d:Lorg/chromium/base/ObserverList;

    iget v1, p0, Lorg/chromium/base/ObserverList$ObserverListIterator;->b:I

    invoke-static {v0, v1}, Lorg/chromium/base/ObserverList;->a(Lorg/chromium/base/ObserverList;I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 222
    iget v0, p0, Lorg/chromium/base/ObserverList$ObserverListIterator;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/chromium/base/ObserverList$ObserverListIterator;->b:I

    goto :goto_0

    .line 224
    :cond_0
    iget v0, p0, Lorg/chromium/base/ObserverList$ObserverListIterator;->b:I

    iget v1, p0, Lorg/chromium/base/ObserverList$ObserverListIterator;->a:I

    if-ge v0, v1, :cond_1

    iget-object v0, p0, Lorg/chromium/base/ObserverList$ObserverListIterator;->d:Lorg/chromium/base/ObserverList;

    iget v1, p0, Lorg/chromium/base/ObserverList$ObserverListIterator;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/chromium/base/ObserverList$ObserverListIterator;->b:I

    invoke-static {v0, v1}, Lorg/chromium/base/ObserverList;->a(Lorg/chromium/base/ObserverList;I)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    .line 227
    :cond_1
    invoke-direct {p0}, Lorg/chromium/base/ObserverList$ObserverListIterator;->a()V

    .line 228
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 233
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
