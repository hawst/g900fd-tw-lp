.class final Lorg/chromium/net/CronetUrlRequest$OnDataReceivedRunnable;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field a:Ljava/nio/ByteBuffer;

.field private synthetic b:Lorg/chromium/net/CronetUrlRequest;


# direct methods
.method constructor <init>(Lorg/chromium/net/CronetUrlRequest;)V
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, Lorg/chromium/net/CronetUrlRequest$OnDataReceivedRunnable;->b:Lorg/chromium/net/CronetUrlRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 80
    iget-object v0, p0, Lorg/chromium/net/CronetUrlRequest$OnDataReceivedRunnable;->b:Lorg/chromium/net/CronetUrlRequest;

    invoke-virtual {v0}, Lorg/chromium/net/CronetUrlRequest;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 116
    :goto_0
    return-void

    .line 84
    :cond_0
    :try_start_0
    iget-object v0, p0, Lorg/chromium/net/CronetUrlRequest$OnDataReceivedRunnable;->b:Lorg/chromium/net/CronetUrlRequest;

    invoke-static {v0}, Lorg/chromium/net/CronetUrlRequest;->a(Lorg/chromium/net/CronetUrlRequest;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 85
    :try_start_1
    iget-object v0, p0, Lorg/chromium/net/CronetUrlRequest$OnDataReceivedRunnable;->b:Lorg/chromium/net/CronetUrlRequest;

    invoke-static {}, Lorg/chromium/net/CronetUrlRequest;->c()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_2

    .line 86
    monitor-exit v1

    goto :goto_0

    .line 95
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 115
    :catch_0
    move-exception v0

    .line 108
    iget-object v1, p0, Lorg/chromium/net/CronetUrlRequest$OnDataReceivedRunnable;->b:Lorg/chromium/net/CronetUrlRequest;

    invoke-static {v1}, Lorg/chromium/net/CronetUrlRequest;->a(Lorg/chromium/net/CronetUrlRequest;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 109
    :try_start_3
    iget-object v2, p0, Lorg/chromium/net/CronetUrlRequest$OnDataReceivedRunnable;->b:Lorg/chromium/net/CronetUrlRequest;

    .line 110
    iget-object v2, p0, Lorg/chromium/net/CronetUrlRequest$OnDataReceivedRunnable;->b:Lorg/chromium/net/CronetUrlRequest;

    invoke-virtual {v2}, Lorg/chromium/net/CronetUrlRequest;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 111
    iget-object v2, p0, Lorg/chromium/net/CronetUrlRequest$OnDataReceivedRunnable;->b:Lorg/chromium/net/CronetUrlRequest;

    invoke-static {v2}, Lorg/chromium/net/CronetUrlRequest;->c(Lorg/chromium/net/CronetUrlRequest;)V

    .line 113
    :cond_1
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 114
    iget-object v1, p0, Lorg/chromium/net/CronetUrlRequest$OnDataReceivedRunnable;->b:Lorg/chromium/net/CronetUrlRequest;

    invoke-static {v1, v0}, Lorg/chromium/net/CronetUrlRequest;->a(Lorg/chromium/net/CronetUrlRequest;Ljava/lang/Exception;)V

    goto :goto_0

    .line 94
    :cond_2
    :try_start_4
    iget-object v0, p0, Lorg/chromium/net/CronetUrlRequest$OnDataReceivedRunnable;->b:Lorg/chromium/net/CronetUrlRequest;

    .line 95
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 96
    :try_start_5
    iget-object v0, p0, Lorg/chromium/net/CronetUrlRequest$OnDataReceivedRunnable;->b:Lorg/chromium/net/CronetUrlRequest;

    iget-object v0, p0, Lorg/chromium/net/CronetUrlRequest$OnDataReceivedRunnable;->b:Lorg/chromium/net/CronetUrlRequest;

    iget-object v0, p0, Lorg/chromium/net/CronetUrlRequest$OnDataReceivedRunnable;->b:Lorg/chromium/net/CronetUrlRequest;

    invoke-static {v0}, Lorg/chromium/net/CronetUrlRequest;->b(Lorg/chromium/net/CronetUrlRequest;)Lorg/chromium/net/CronetUrlRequest$NativeResponseInfo;

    iget-object v0, p0, Lorg/chromium/net/CronetUrlRequest$OnDataReceivedRunnable;->a:Ljava/nio/ByteBuffer;

    .line 98
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/chromium/net/CronetUrlRequest$OnDataReceivedRunnable;->a:Ljava/nio/ByteBuffer;

    .line 99
    iget-object v0, p0, Lorg/chromium/net/CronetUrlRequest$OnDataReceivedRunnable;->b:Lorg/chromium/net/CronetUrlRequest;

    invoke-static {v0}, Lorg/chromium/net/CronetUrlRequest;->a(Lorg/chromium/net/CronetUrlRequest;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    .line 100
    :try_start_6
    iget-object v0, p0, Lorg/chromium/net/CronetUrlRequest$OnDataReceivedRunnable;->b:Lorg/chromium/net/CronetUrlRequest;

    .line 101
    iget-object v0, p0, Lorg/chromium/net/CronetUrlRequest$OnDataReceivedRunnable;->b:Lorg/chromium/net/CronetUrlRequest;

    invoke-virtual {v0}, Lorg/chromium/net/CronetUrlRequest;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 102
    iget-object v0, p0, Lorg/chromium/net/CronetUrlRequest$OnDataReceivedRunnable;->b:Lorg/chromium/net/CronetUrlRequest;

    invoke-static {v0}, Lorg/chromium/net/CronetUrlRequest;->c(Lorg/chromium/net/CronetUrlRequest;)V

    .line 103
    monitor-exit v1

    goto :goto_0

    .line 106
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    throw v0
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0

    .line 105
    :cond_3
    :try_start_8
    iget-object v0, p0, Lorg/chromium/net/CronetUrlRequest$OnDataReceivedRunnable;->b:Lorg/chromium/net/CronetUrlRequest;

    iget-object v2, p0, Lorg/chromium/net/CronetUrlRequest$OnDataReceivedRunnable;->b:Lorg/chromium/net/CronetUrlRequest;

    invoke-static {}, Lorg/chromium/net/CronetUrlRequest;->c()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lorg/chromium/net/CronetUrlRequest;->a(Lorg/chromium/net/CronetUrlRequest;J)V

    .line 106
    monitor-exit v1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_0

    .line 113
    :catchall_2
    move-exception v0

    :try_start_9
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    throw v0
.end method
