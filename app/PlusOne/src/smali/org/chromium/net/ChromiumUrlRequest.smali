.class public Lorg/chromium/net/ChromiumUrlRequest;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lorg/chromium/net/HttpUrlRequest;


# annotations
.annotation runtime Lorg/chromium/base/JNINamespace;
.end annotation


# instance fields
.field private A:Z

.field private B:J

.field private final C:Ljava/lang/Object;

.field private a:J

.field private final b:Lorg/chromium/net/ChromiumUrlRequestContext;

.field private final c:Ljava/lang/String;

.field private final d:I

.field private final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/nio/channels/WritableByteChannel;

.field private g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:[B

.field private k:Ljava/nio/channels/ReadableByteChannel;

.field private l:Ljava/io/IOException;

.field private volatile m:Z

.field private volatile n:Z

.field private volatile o:Z

.field private volatile p:Z

.field private q:Z

.field private r:Ljava/lang/String;

.field private s:J

.field private final t:Lorg/chromium/net/HttpUrlRequestListener;

.field private u:Z

.field private v:J

.field private w:J

.field private x:J

.field private y:Z

.field private z:Z


# direct methods
.method public constructor <init>(Lorg/chromium/net/ChromiumUrlRequestContext;Ljava/lang/String;ILjava/util/Map;Ljava/nio/channels/WritableByteChannel;Lorg/chromium/net/HttpUrlRequestListener;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/chromium/net/ChromiumUrlRequestContext;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/nio/channels/WritableByteChannel;",
            "Lorg/chromium/net/HttpUrlRequestListener;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x3

    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lorg/chromium/net/ChromiumUrlRequest;->C:Ljava/lang/Object;

    .line 88
    if-nez p1, :cond_0

    .line 89
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Context is required"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 91
    :cond_0
    if-nez p2, :cond_1

    .line 92
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "URL is required"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 94
    :cond_1
    iput-object p1, p0, Lorg/chromium/net/ChromiumUrlRequest;->b:Lorg/chromium/net/ChromiumUrlRequestContext;

    .line 95
    iput-object p2, p0, Lorg/chromium/net/ChromiumUrlRequest;->c:Ljava/lang/String;

    .line 96
    packed-switch p3, :pswitch_data_0

    :goto_0
    :pswitch_0
    iput v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->d:I

    .line 97
    iput-object p4, p0, Lorg/chromium/net/ChromiumUrlRequest;->e:Ljava/util/Map;

    .line 98
    iput-object p5, p0, Lorg/chromium/net/ChromiumUrlRequest;->f:Ljava/nio/channels/WritableByteChannel;

    .line 99
    iget-object v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->b:Lorg/chromium/net/ChromiumUrlRequestContext;

    invoke-virtual {v0}, Lorg/chromium/net/ChromiumUrlRequestContext;->b()J

    move-result-wide v0

    iget-object v2, p0, Lorg/chromium/net/ChromiumUrlRequest;->c:Ljava/lang/String;

    iget v3, p0, Lorg/chromium/net/ChromiumUrlRequest;->d:I

    invoke-direct {p0, v0, v1, v2, v3}, Lorg/chromium/net/ChromiumUrlRequest;->nativeCreateRequestAdapter(JLjava/lang/String;I)J

    move-result-wide v0

    iput-wide v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->a:J

    .line 103
    iput-object p6, p0, Lorg/chromium/net/ChromiumUrlRequest;->t:Lorg/chromium/net/HttpUrlRequestListener;

    .line 104
    return-void

    .line 96
    :pswitch_1
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x2

    goto :goto_0

    :pswitch_4
    const/4 v0, 0x4

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public constructor <init>(Lorg/chromium/net/ChromiumUrlRequestContext;Ljava/lang/String;ILjava/util/Map;Lorg/chromium/net/HttpUrlRequestListener;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/chromium/net/ChromiumUrlRequestContext;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lorg/chromium/net/HttpUrlRequestListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 70
    new-instance v5, Lorg/chromium/net/ChunkedWritableByteChannel;

    invoke-direct {v5}, Lorg/chromium/net/ChunkedWritableByteChannel;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lorg/chromium/net/ChromiumUrlRequest;-><init>(Lorg/chromium/net/ChromiumUrlRequestContext;Ljava/lang/String;ILjava/util/Map;Ljava/nio/channels/WritableByteChannel;Lorg/chromium/net/HttpUrlRequestListener;)V

    .line 72
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->u:Z

    .line 73
    return-void
.end method

.method private a(Ljava/lang/Exception;)V
    .locals 3

    .prologue
    .line 498
    new-instance v0, Ljava/io/IOException;

    const-string v1, "CalledByNative method has thrown an exception"

    invoke-direct {v0, v1, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    iput-object v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->l:Ljava/io/IOException;

    .line 500
    const-string v0, "ChromiumNetwork"

    const-string v1, "Exception in CalledByNative method"

    invoke-static {v0, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 503
    :try_start_0
    invoke-virtual {p0}, Lorg/chromium/net/ChromiumUrlRequest;->h()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 508
    :goto_0
    return-void

    .line 504
    :catch_0
    move-exception v0

    .line 505
    const-string v1, "ChromiumNetwork"

    const-string v2, "Exception trying to cancel request"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 486
    if-nez p1, :cond_0

    .line 487
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "contentType is required"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 489
    :cond_0
    return-void
.end method

.method private finish()V
    .locals 4
    .annotation build Lorg/chromium/base/CalledByNative;
    .end annotation

    .prologue
    .line 602
    :try_start_0
    iget-object v1, p0, Lorg/chromium/net/ChromiumUrlRequest;->C:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 603
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->p:Z

    .line 608
    iget-boolean v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->o:Z

    if-eqz v0, :cond_0

    .line 609
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 631
    :goto_0
    return-void

    .line 612
    :cond_0
    :try_start_2
    iget-object v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->f:Ljava/nio/channels/WritableByteChannel;

    invoke-interface {v0}, Ljava/nio/channels/WritableByteChannel;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 617
    :goto_1
    :try_start_3
    iget-object v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->k:Ljava/nio/channels/ReadableByteChannel;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->k:Ljava/nio/channels/ReadableByteChannel;

    invoke-interface {v0}, Ljava/nio/channels/ReadableByteChannel;->isOpen()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 618
    iget-object v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->k:Ljava/nio/channels/ReadableByteChannel;

    invoke-interface {v0}, Ljava/nio/channels/ReadableByteChannel;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 623
    :cond_1
    :goto_2
    :try_start_4
    iget-object v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->t:Lorg/chromium/net/HttpUrlRequestListener;

    invoke-interface {v0, p0}, Lorg/chromium/net/HttpUrlRequestListener;->onRequestComplete(Lorg/chromium/net/HttpUrlRequest;)V

    .line 624
    iget-wide v2, p0, Lorg/chromium/net/ChromiumUrlRequest;->a:J

    invoke-direct {p0, v2, v3}, Lorg/chromium/net/ChromiumUrlRequest;->nativeDestroyRequestAdapter(J)V

    .line 625
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lorg/chromium/net/ChromiumUrlRequest;->a:J

    .line 626
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->o:Z

    .line 627
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v0
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    .line 630
    :catch_0
    move-exception v0

    .line 629
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Exception in finish"

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    iput-object v1, p0, Lorg/chromium/net/ChromiumUrlRequest;->l:Ljava/io/IOException;

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_2

    :catch_2
    move-exception v0

    goto :goto_1
.end method

.method private m()V
    .locals 1

    .prologue
    .line 456
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->z:Z

    .line 457
    invoke-virtual {p0}, Lorg/chromium/net/ChromiumUrlRequest;->h()V

    .line 458
    return-void
.end method

.method private n()V
    .locals 2

    .prologue
    .line 468
    iget-boolean v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->o:Z

    if-eqz v0, :cond_0

    .line 469
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Accessing recycled request"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 471
    :cond_0
    return-void
.end method

.method private native nativeAddHeader(JLjava/lang/String;Ljava/lang/String;)V
.end method

.method private native nativeAppendChunk(JLjava/nio/ByteBuffer;IZ)V
.end method

.method private native nativeCancel(J)V
.end method

.method private native nativeCreateRequestAdapter(JLjava/lang/String;I)J
.end method

.method private native nativeDestroyRequestAdapter(J)V
.end method

.method private native nativeDisableRedirects(J)V
.end method

.method private native nativeEnableChunkedUpload(JLjava/lang/String;)V
.end method

.method private native nativeGetAllHeaders(JLorg/chromium/net/ChromiumUrlRequest$ResponseHeadersMap;)V
.end method

.method private native nativeGetContentLength(J)J
.end method

.method private native nativeGetContentType(J)Ljava/lang/String;
.end method

.method private native nativeGetErrorCode(J)I
.end method

.method private native nativeGetErrorString(J)Ljava/lang/String;
.end method

.method private native nativeGetHeader(JLjava/lang/String;)Ljava/lang/String;
.end method

.method private native nativeGetHttpStatusCode(J)I
.end method

.method private native nativeGetHttpStatusText(J)Ljava/lang/String;
.end method

.method private native nativeGetNegotiatedProtocol(J)Ljava/lang/String;
.end method

.method private native nativeSetMethod(JLjava/lang/String;)V
.end method

.method private native nativeSetUploadChannel(JLjava/lang/String;J)V
.end method

.method private native nativeSetUploadData(JLjava/lang/String;[B)V
.end method

.method private native nativeStart(J)V
.end method

.method private o()V
    .locals 2

    .prologue
    .line 474
    iget-boolean v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->m:Z

    if-eqz v0, :cond_0

    .line 475
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Request already started"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 477
    :cond_0
    return-void
.end method

.method private onAppendResponseHeader(Lorg/chromium/net/ChromiumUrlRequest$ResponseHeadersMap;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation build Lorg/chromium/base/CalledByNative;
    .end annotation

    .prologue
    .line 641
    :try_start_0
    invoke-virtual {p1, p2}, Lorg/chromium/net/ChromiumUrlRequest$ResponseHeadersMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 642
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1, p2, v0}, Lorg/chromium/net/ChromiumUrlRequest$ResponseHeadersMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 644
    :cond_0
    invoke-virtual {p1, p2}, Lorg/chromium/net/ChromiumUrlRequest$ResponseHeadersMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 648
    :goto_0
    return-void

    .line 645
    :catch_0
    move-exception v0

    .line 646
    invoke-direct {p0, v0}, Lorg/chromium/net/ChromiumUrlRequest;->a(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method private onBytesRead(Ljava/nio/ByteBuffer;)V
    .locals 8
    .annotation build Lorg/chromium/base/CalledByNative;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 563
    :try_start_0
    iget-boolean v1, p0, Lorg/chromium/net/ChromiumUrlRequest;->z:Z

    if-eqz v1, :cond_1

    .line 593
    :cond_0
    :goto_0
    return-void

    .line 567
    :cond_1
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    .line 568
    iget-wide v2, p0, Lorg/chromium/net/ChromiumUrlRequest;->B:J

    int-to-long v4, v1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lorg/chromium/net/ChromiumUrlRequest;->B:J

    .line 569
    iget-boolean v2, p0, Lorg/chromium/net/ChromiumUrlRequest;->A:Z

    if-eqz v2, :cond_2

    .line 570
    iget-wide v2, p0, Lorg/chromium/net/ChromiumUrlRequest;->B:J

    iget-wide v4, p0, Lorg/chromium/net/ChromiumUrlRequest;->v:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 573
    const/4 v2, 0x0

    iput-boolean v2, p0, Lorg/chromium/net/ChromiumUrlRequest;->A:Z

    .line 574
    iget-wide v2, p0, Lorg/chromium/net/ChromiumUrlRequest;->v:J

    iget-wide v4, p0, Lorg/chromium/net/ChromiumUrlRequest;->B:J

    int-to-long v6, v1

    sub-long/2addr v4, v6

    sub-long/2addr v2, v4

    long-to-int v2, v2

    invoke-virtual {p1, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 578
    :cond_2
    iget-wide v2, p0, Lorg/chromium/net/ChromiumUrlRequest;->x:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    iget-wide v2, p0, Lorg/chromium/net/ChromiumUrlRequest;->B:J

    iget-wide v4, p0, Lorg/chromium/net/ChromiumUrlRequest;->x:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_3

    const/4 v0, 0x1

    .line 580
    :cond_3
    if-eqz v0, :cond_4

    .line 581
    iget-wide v2, p0, Lorg/chromium/net/ChromiumUrlRequest;->B:J

    iget-wide v4, p0, Lorg/chromium/net/ChromiumUrlRequest;->x:J

    sub-long/2addr v2, v4

    long-to-int v2, v2

    sub-int/2addr v1, v2

    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 584
    :cond_4
    :goto_1
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 585
    iget-object v1, p0, Lorg/chromium/net/ChromiumUrlRequest;->f:Ljava/nio/channels/WritableByteChannel;

    invoke-interface {v1, p1}, Ljava/nio/channels/WritableByteChannel;->write(Ljava/nio/ByteBuffer;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 590
    :catch_0
    move-exception v0

    .line 591
    invoke-direct {p0, v0}, Lorg/chromium/net/ChromiumUrlRequest;->a(Ljava/lang/Exception;)V

    goto :goto_0

    .line 587
    :cond_5
    if-eqz v0, :cond_0

    .line 588
    :try_start_1
    invoke-direct {p0}, Lorg/chromium/net/ChromiumUrlRequest;->m()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private onResponseStarted()V
    .locals 8
    .annotation build Lorg/chromium/base/CalledByNative;
    .end annotation

    .prologue
    const-wide/16 v6, 0x0

    const-wide/16 v4, -0x1

    .line 516
    :try_start_0
    iget-wide v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->a:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/net/ChromiumUrlRequest;->nativeGetContentType(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->r:Ljava/lang/String;

    .line 517
    iget-wide v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->a:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/net/ChromiumUrlRequest;->nativeGetContentLength(J)J

    move-result-wide v0

    iput-wide v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->w:J

    .line 518
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->q:Z

    .line 520
    iget-wide v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->x:J

    cmp-long v0, v0, v6

    if-lez v0, :cond_0

    iget-wide v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->w:J

    iget-wide v2, p0, Lorg/chromium/net/ChromiumUrlRequest;->x:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-boolean v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->y:Z

    if-eqz v0, :cond_0

    .line 523
    invoke-direct {p0}, Lorg/chromium/net/ChromiumUrlRequest;->m()V

    .line 551
    :goto_0
    return-void

    .line 527
    :cond_0
    iget-boolean v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->u:Z

    if-eqz v0, :cond_1

    iget-wide v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->w:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->z:Z

    if-nez v0, :cond_1

    .line 529
    invoke-virtual {p0}, Lorg/chromium/net/ChromiumUrlRequest;->f()Ljava/nio/channels/WritableByteChannel;

    move-result-object v0

    check-cast v0, Lorg/chromium/net/ChunkedWritableByteChannel;

    iget-wide v2, p0, Lorg/chromium/net/ChromiumUrlRequest;->w:J

    long-to-int v1, v2

    invoke-virtual {v0, v1}, Lorg/chromium/net/ChunkedWritableByteChannel;->a(I)V

    .line 533
    :cond_1
    iget-wide v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->v:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_3

    .line 537
    iget-wide v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->a:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/net/ChromiumUrlRequest;->nativeGetHttpStatusCode(J)I

    move-result v0

    const/16 v1, 0xc8

    if-ne v0, v1, :cond_4

    .line 539
    iget-wide v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->w:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_2

    .line 540
    iget-wide v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->w:J

    iget-wide v2, p0, Lorg/chromium/net/ChromiumUrlRequest;->v:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->w:J

    .line 542
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->A:Z

    .line 547
    :cond_3
    :goto_1
    iget-object v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->t:Lorg/chromium/net/HttpUrlRequestListener;

    invoke-interface {v0, p0}, Lorg/chromium/net/HttpUrlRequestListener;->onResponseStarted(Lorg/chromium/net/HttpUrlRequest;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 548
    :catch_0
    move-exception v0

    .line 549
    invoke-direct {p0, v0}, Lorg/chromium/net/ChromiumUrlRequest;->a(Ljava/lang/Exception;)V

    goto :goto_0

    .line 544
    :cond_4
    :try_start_1
    iget-wide v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->v:J

    iput-wide v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->B:J
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method private p()V
    .locals 2

    .prologue
    .line 480
    iget-boolean v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->q:Z

    if-nez v0, :cond_0

    .line 481
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Response headers not available"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 483
    :cond_0
    return-void
.end method

.method private readFromUploadChannel(Ljava/nio/ByteBuffer;)I
    .locals 2
    .annotation build Lorg/chromium/base/CalledByNative;
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 660
    :try_start_0
    iget-object v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->k:Ljava/nio/channels/ReadableByteChannel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->k:Ljava/nio/channels/ReadableByteChannel;

    invoke-interface {v0}, Ljava/nio/channels/ReadableByteChannel;->isOpen()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    move v0, v1

    .line 671
    :cond_1
    :goto_0
    return v0

    .line 662
    :cond_2
    iget-object v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->k:Ljava/nio/channels/ReadableByteChannel;

    invoke-interface {v0, p1}, Ljava/nio/channels/ReadableByteChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 663
    if-gez v0, :cond_1

    .line 664
    iget-object v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->k:Ljava/nio/channels/ReadableByteChannel;

    invoke-interface {v0}, Ljava/nio/channels/ReadableByteChannel;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 665
    const/4 v0, 0x0

    goto :goto_0

    .line 668
    :catch_0
    move-exception v0

    .line 669
    invoke-direct {p0, v0}, Lorg/chromium/net/ChromiumUrlRequest;->a(Ljava/lang/Exception;)V

    move v0, v1

    .line 671
    goto :goto_0
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 123
    iget-wide v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->w:J

    return-wide v0
.end method

.method public a(J)V
    .locals 3

    .prologue
    .line 108
    iput-wide p1, p0, Lorg/chromium/net/ChromiumUrlRequest;->v:J

    .line 109
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-eqz v0, :cond_0

    .line 110
    const-string v0, "Range"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "bytes="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lorg/chromium/net/ChromiumUrlRequest;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    :cond_0
    return-void
.end method

.method public a(JZ)V
    .locals 1

    .prologue
    .line 128
    iput-wide p1, p0, Lorg/chromium/net/ChromiumUrlRequest;->x:J

    .line 129
    iput-boolean p3, p0, Lorg/chromium/net/ChromiumUrlRequest;->y:Z

    .line 130
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 312
    invoke-direct {p0}, Lorg/chromium/net/ChromiumUrlRequest;->o()V

    .line 313
    iput-object p1, p0, Lorg/chromium/net/ChromiumUrlRequest;->i:Ljava/lang/String;

    .line 314
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 208
    iget-object v1, p0, Lorg/chromium/net/ChromiumUrlRequest;->C:Ljava/lang/Object;

    monitor-enter v1

    .line 209
    :try_start_0
    invoke-direct {p0}, Lorg/chromium/net/ChromiumUrlRequest;->o()V

    .line 210
    iget-object v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->g:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 211
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->g:Ljava/util/Map;

    .line 213
    :cond_0
    iget-object v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->g:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 214
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Ljava/lang/String;Ljava/nio/channels/ReadableByteChannel;J)V
    .locals 3

    .prologue
    .line 248
    iget-object v1, p0, Lorg/chromium/net/ChromiumUrlRequest;->C:Ljava/lang/Object;

    monitor-enter v1

    .line 249
    :try_start_0
    invoke-direct {p0}, Lorg/chromium/net/ChromiumUrlRequest;->o()V

    .line 250
    invoke-direct {p0, p1}, Lorg/chromium/net/ChromiumUrlRequest;->c(Ljava/lang/String;)V

    .line 251
    iput-object p1, p0, Lorg/chromium/net/ChromiumUrlRequest;->h:Ljava/lang/String;

    .line 252
    iput-object p2, p0, Lorg/chromium/net/ChromiumUrlRequest;->k:Ljava/nio/channels/ReadableByteChannel;

    .line 253
    iput-wide p3, p0, Lorg/chromium/net/ChromiumUrlRequest;->s:J

    .line 254
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->j:[B

    .line 255
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Ljava/lang/String;[B)V
    .locals 2

    .prologue
    .line 226
    iget-object v1, p0, Lorg/chromium/net/ChromiumUrlRequest;->C:Ljava/lang/Object;

    monitor-enter v1

    .line 227
    :try_start_0
    invoke-direct {p0}, Lorg/chromium/net/ChromiumUrlRequest;->o()V

    .line 228
    invoke-direct {p0, p1}, Lorg/chromium/net/ChromiumUrlRequest;->c(Ljava/lang/String;)V

    .line 229
    iput-object p1, p0, Lorg/chromium/net/ChromiumUrlRequest;->h:Ljava/lang/String;

    .line 230
    iput-object p2, p0, Lorg/chromium/net/ChromiumUrlRequest;->j:[B

    .line 231
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->k:Ljava/nio/channels/ReadableByteChannel;

    .line 232
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b()I
    .locals 2

    .prologue
    .line 134
    iget-wide v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->a:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/net/ChromiumUrlRequest;->nativeGetHttpStatusCode(J)I

    move-result v0

    .line 140
    const/16 v1, 0xce

    if-ne v0, v1, :cond_0

    .line 141
    const/16 v0, 0xc8

    .line 143
    :cond_0
    return v0
.end method

.method public b(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 418
    invoke-direct {p0}, Lorg/chromium/net/ChromiumUrlRequest;->n()V

    .line 419
    invoke-direct {p0}, Lorg/chromium/net/ChromiumUrlRequest;->p()V

    .line 420
    iget-wide v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->a:J

    invoke-direct {p0, v0, v1, p1}, Lorg/chromium/net/ChromiumUrlRequest;->nativeGetHeader(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/io/IOException;
    .locals 4

    .prologue
    .line 157
    iget-object v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->l:Ljava/io/IOException;

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->l:Ljava/io/IOException;

    .line 186
    :goto_0
    return-object v0

    .line 161
    :cond_0
    invoke-direct {p0}, Lorg/chromium/net/ChromiumUrlRequest;->n()V

    .line 163
    iget-wide v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->a:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/net/ChromiumUrlRequest;->nativeGetErrorCode(J)I

    move-result v0

    .line 164
    packed-switch v0, :pswitch_data_0

    .line 189
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unrecognized error code: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 166
    :pswitch_0
    iget-boolean v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->z:Z

    if-eqz v0, :cond_1

    .line 167
    new-instance v0, Lorg/chromium/net/ResponseTooLargeException;

    invoke-direct {v0}, Lorg/chromium/net/ResponseTooLargeException;-><init>()V

    goto :goto_0

    .line 169
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 171
    :pswitch_1
    new-instance v0, Ljava/io/IOException;

    iget-wide v2, p0, Lorg/chromium/net/ChromiumUrlRequest;->a:J

    invoke-direct {p0, v2, v3}, Lorg/chromium/net/ChromiumUrlRequest;->nativeGetErrorString(J)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 174
    :pswitch_2
    new-instance v0, Ljava/net/MalformedURLException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Malformed URL: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/chromium/net/ChromiumUrlRequest;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/MalformedURLException;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 176
    :pswitch_3
    new-instance v0, Lorg/apache/http/conn/ConnectTimeoutException;

    const-string v1, "Connection timed out"

    invoke-direct {v0, v1}, Lorg/apache/http/conn/ConnectTimeoutException;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 180
    :pswitch_4
    :try_start_0
    new-instance v0, Ljava/net/URL;

    iget-object v1, p0, Lorg/chromium/net/ChromiumUrlRequest;->c:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/URL;->getHost()Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 184
    :goto_1
    new-instance v1, Ljava/net/UnknownHostException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown host: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/net/UnknownHostException;-><init>(Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_0

    .line 182
    :catch_0
    move-exception v0

    iget-object v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->c:Ljava/lang/String;

    goto :goto_1

    .line 186
    :pswitch_5
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Request failed because there were too many redirects or redirects have been disabled"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 164
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public d()Ljava/nio/ByteBuffer;
    .locals 1

    .prologue
    .line 196
    invoke-virtual {p0}, Lorg/chromium/net/ChromiumUrlRequest;->f()Ljava/nio/channels/WritableByteChannel;

    move-result-object v0

    check-cast v0, Lorg/chromium/net/ChunkedWritableByteChannel;

    invoke-virtual {v0}, Lorg/chromium/net/ChunkedWritableByteChannel;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    return-object v0
.end method

.method public e()[B
    .locals 1

    .prologue
    .line 201
    invoke-virtual {p0}, Lorg/chromium/net/ChromiumUrlRequest;->f()Ljava/nio/channels/WritableByteChannel;

    move-result-object v0

    check-cast v0, Lorg/chromium/net/ChunkedWritableByteChannel;

    invoke-virtual {v0}, Lorg/chromium/net/ChunkedWritableByteChannel;->b()[B

    move-result-object v0

    return-object v0
.end method

.method public f()Ljava/nio/channels/WritableByteChannel;
    .locals 1

    .prologue
    .line 324
    iget-object v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->f:Ljava/nio/channels/WritableByteChannel;

    return-object v0
.end method

.method public g()V
    .locals 7

    .prologue
    .line 329
    iget-object v6, p0, Lorg/chromium/net/ChromiumUrlRequest;->C:Ljava/lang/Object;

    monitor-enter v6

    .line 330
    :try_start_0
    iget-boolean v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->n:Z

    if-eqz v0, :cond_0

    .line 331
    monitor-exit v6

    .line 373
    :goto_0
    return-void

    .line 334
    :cond_0
    invoke-direct {p0}, Lorg/chromium/net/ChromiumUrlRequest;->o()V

    .line 335
    invoke-direct {p0}, Lorg/chromium/net/ChromiumUrlRequest;->n()V

    .line 337
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->m:Z

    .line 339
    iget-object v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->e:Ljava/util/Map;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 340
    iget-object v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 341
    iget-wide v4, p0, Lorg/chromium/net/ChromiumUrlRequest;->a:J

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v4, v5, v1, v0}, Lorg/chromium/net/ChromiumUrlRequest;->nativeAddHeader(JLjava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 373
    :catchall_0
    move-exception v0

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 346
    :cond_1
    :try_start_1
    iget-object v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->g:Ljava/util/Map;

    if-eqz v0, :cond_2

    .line 348
    iget-object v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 349
    iget-wide v4, p0, Lorg/chromium/net/ChromiumUrlRequest;->a:J

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v4, v5, v1, v0}, Lorg/chromium/net/ChromiumUrlRequest;->nativeAddHeader(JLjava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 354
    :cond_2
    iget-object v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->j:[B

    if-eqz v0, :cond_5

    iget-object v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->j:[B

    array-length v0, v0

    if-lez v0, :cond_5

    .line 355
    iget-wide v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->a:J

    iget-object v2, p0, Lorg/chromium/net/ChromiumUrlRequest;->h:Ljava/lang/String;

    iget-object v3, p0, Lorg/chromium/net/ChromiumUrlRequest;->j:[B

    invoke-direct {p0, v0, v1, v2, v3}, Lorg/chromium/net/ChromiumUrlRequest;->nativeSetUploadData(JLjava/lang/String;[B)V

    .line 360
    :cond_3
    :goto_3
    iget-object v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->i:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 369
    iget-wide v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->a:J

    iget-object v2, p0, Lorg/chromium/net/ChromiumUrlRequest;->i:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2}, Lorg/chromium/net/ChromiumUrlRequest;->nativeSetMethod(JLjava/lang/String;)V

    .line 372
    :cond_4
    iget-wide v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->a:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/net/ChromiumUrlRequest;->nativeStart(J)V

    .line 373
    monitor-exit v6

    goto/16 :goto_0

    .line 357
    :cond_5
    iget-object v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->k:Ljava/nio/channels/ReadableByteChannel;

    if-eqz v0, :cond_3

    .line 358
    iget-wide v1, p0, Lorg/chromium/net/ChromiumUrlRequest;->a:J

    iget-object v3, p0, Lorg/chromium/net/ChromiumUrlRequest;->h:Ljava/lang/String;

    iget-wide v4, p0, Lorg/chromium/net/ChromiumUrlRequest;->s:J

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lorg/chromium/net/ChromiumUrlRequest;->nativeSetUploadChannel(JLjava/lang/String;J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3
.end method

.method public h()V
    .locals 4

    .prologue
    .line 378
    iget-object v1, p0, Lorg/chromium/net/ChromiumUrlRequest;->C:Ljava/lang/Object;

    monitor-enter v1

    .line 379
    :try_start_0
    iget-boolean v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->n:Z

    if-eqz v0, :cond_0

    .line 380
    monitor-exit v1

    .line 388
    :goto_0
    return-void

    .line 383
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->n:Z

    .line 385
    iget-boolean v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->o:Z

    if-nez v0, :cond_1

    .line 386
    iget-wide v2, p0, Lorg/chromium/net/ChromiumUrlRequest;->a:J

    invoke-direct {p0, v2, v3}, Lorg/chromium/net/ChromiumUrlRequest;->nativeCancel(J)V

    .line 388
    :cond_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public i()Z
    .locals 2

    .prologue
    .line 393
    iget-object v1, p0, Lorg/chromium/net/ChromiumUrlRequest;->C:Ljava/lang/Object;

    monitor-enter v1

    .line 394
    :try_start_0
    iget-boolean v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->n:Z

    monitor-exit v1

    return v0

    .line 395
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public j()Ljava/lang/String;
    .locals 2

    .prologue
    .line 406
    invoke-direct {p0}, Lorg/chromium/net/ChromiumUrlRequest;->n()V

    .line 407
    invoke-direct {p0}, Lorg/chromium/net/ChromiumUrlRequest;->p()V

    .line 408
    iget-wide v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->a:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/net/ChromiumUrlRequest;->nativeGetNegotiatedProtocol(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 413
    iget-object v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->r:Ljava/lang/String;

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 435
    iget-object v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->c:Ljava/lang/String;

    return-object v0
.end method
