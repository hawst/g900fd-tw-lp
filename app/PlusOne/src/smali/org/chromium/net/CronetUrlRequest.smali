.class final Lorg/chromium/net/CronetUrlRequest;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lorg/chromium/net/UrlRequest;


# annotations
.annotation runtime Lorg/chromium/base/JNINamespace;
.end annotation


# instance fields
.field private final a:Ljava/lang/Object;

.field private final b:Lorg/chromium/net/CronetUrlRequestContext;

.field private final c:Ljava/util/concurrent/Executor;

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lorg/chromium/net/CronetUrlRequest$NativeResponseInfo;

.field private f:Lorg/chromium/net/CronetUrlRequest$OnDataReceivedRunnable;


# direct methods
.method static synthetic a(Lorg/chromium/net/CronetUrlRequest;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lorg/chromium/net/CronetUrlRequest;->a:Ljava/lang/Object;

    return-object v0
.end method

.method private a(I)Lorg/chromium/net/CronetUrlRequest$NativeResponseInfo;
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 334
    iget-object v1, p0, Lorg/chromium/net/CronetUrlRequest;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 335
    cmp-long v0, v4, v4

    if-nez v0, :cond_0

    .line 336
    const/4 v0, 0x0

    :try_start_0
    monitor-exit v1

    .line 352
    :goto_0
    return-object v0

    .line 342
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 344
    new-instance v0, Lorg/chromium/net/CronetUrlRequest$NativeResponseInfo;

    iget-object v1, p0, Lorg/chromium/net/CronetUrlRequest;->d:Ljava/util/List;

    iget-object v2, p0, Lorg/chromium/net/CronetUrlRequest;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    invoke-direct {p0, v4, v5}, Lorg/chromium/net/CronetUrlRequest;->nativeGetHttpStatusText(J)Ljava/lang/String;

    invoke-direct {p0, v4, v5}, Lorg/chromium/net/CronetUrlRequest;->nativeGetWasCached(J)Z

    invoke-direct {p0, v4, v5}, Lorg/chromium/net/CronetUrlRequest;->nativeGetNegotiatedProtocol(J)Ljava/lang/String;

    invoke-direct {v0}, Lorg/chromium/net/CronetUrlRequest$NativeResponseInfo;-><init>()V

    .line 350
    invoke-static {v0}, Lorg/chromium/net/CronetUrlRequest$NativeResponseInfo;->a(Lorg/chromium/net/CronetUrlRequest$NativeResponseInfo;)Lorg/chromium/net/CronetUrlRequest$HeadersMap;

    move-result-object v1

    invoke-direct {p0, v4, v5, v1}, Lorg/chromium/net/CronetUrlRequest;->nativePopulateResponseHeaders(JLorg/chromium/net/CronetUrlRequest$HeadersMap;)V

    goto :goto_0

    .line 342
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private a(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 311
    iget-object v0, p0, Lorg/chromium/net/CronetUrlRequest;->c:Ljava/util/concurrent/Executor;

    invoke-interface {v0, p1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 312
    return-void
.end method

.method static synthetic a(Lorg/chromium/net/CronetUrlRequest;J)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Lorg/chromium/net/CronetUrlRequest;->nativeReceiveData(J)V

    return-void
.end method

.method static synthetic a(Lorg/chromium/net/CronetUrlRequest;Ljava/lang/Exception;)V
    .locals 3

    .prologue
    .line 30
    new-instance v0, Lorg/chromium/net/UrlRequestException;

    const-string v1, "CalledByNative method has thrown an exception"

    invoke-direct {v0, v1, p1}, Lorg/chromium/net/UrlRequestException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    const-string v0, "ChromiumNetwork"

    const-string v1, "Exception in CalledByNative method"

    invoke-static {v0, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iget-object v1, p0, Lorg/chromium/net/CronetUrlRequest;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0}, Lorg/chromium/net/CronetUrlRequest;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lorg/chromium/net/CronetUrlRequest;->a()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v0, p0, Lorg/chromium/net/CronetUrlRequest;->e:Lorg/chromium/net/CronetUrlRequest$NativeResponseInfo;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catch_0
    move-exception v0

    const-string v1, "ChromiumNetwork"

    const-string v2, "Exception notifying of failed request"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method static synthetic b(Lorg/chromium/net/CronetUrlRequest;)Lorg/chromium/net/CronetUrlRequest$NativeResponseInfo;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lorg/chromium/net/CronetUrlRequest;->e:Lorg/chromium/net/CronetUrlRequest$NativeResponseInfo;

    return-object v0
.end method

.method static synthetic b(Lorg/chromium/net/CronetUrlRequest;J)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Lorg/chromium/net/CronetUrlRequest;->nativeFollowDeferredRedirect(J)V

    return-void
.end method

.method static synthetic c()J
    .locals 2

    .prologue
    .line 30
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method static synthetic c(Lorg/chromium/net/CronetUrlRequest;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 30
    iget-object v1, p0, Lorg/chromium/net/CronetUrlRequest;->a:Ljava/lang/Object;

    monitor-enter v1

    cmp-long v0, v2, v2

    if-nez v0, :cond_0

    :try_start_0
    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    const-wide/16 v2, 0x0

    invoke-direct {p0, v2, v3}, Lorg/chromium/net/CronetUrlRequest;->nativeDestroyRequestAdapter(J)V

    iget-object v0, p0, Lorg/chromium/net/CronetUrlRequest;->b:Lorg/chromium/net/CronetUrlRequestContext;

    invoke-virtual {v0}, Lorg/chromium/net/CronetUrlRequestContext;->a()V

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic d(Lorg/chromium/net/CronetUrlRequest;)Ljava/util/List;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lorg/chromium/net/CronetUrlRequest;->d:Ljava/util/List;

    return-object v0
.end method

.method private native nativeAddHeader(JLjava/lang/String;Ljava/lang/String;)Z
.end method

.method private native nativeCreateRequestAdapter(JLjava/lang/String;I)J
.end method

.method private native nativeDestroyRequestAdapter(J)V
.end method

.method private native nativeFollowDeferredRedirect(J)V
.end method

.method private native nativeGetHttpStatusText(J)Ljava/lang/String;
.end method

.method private native nativeGetNegotiatedProtocol(J)Ljava/lang/String;
.end method

.method private native nativeGetTotalReceivedBytes(J)J
.end method

.method private native nativeGetWasCached(J)Z
.end method

.method private native nativePopulateResponseHeaders(JLorg/chromium/net/CronetUrlRequest$HeadersMap;)V
.end method

.method private native nativeReceiveData(J)V
.end method

.method private native nativeSetHttpMethod(JLjava/lang/String;)Z
.end method

.method private native nativeStart(J)V
.end method

.method private onAppendResponseHeader(Lorg/chromium/net/CronetUrlRequest$HeadersMap;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .annotation build Lorg/chromium/base/CalledByNative;
    .end annotation

    .prologue
    .line 573
    :try_start_0
    invoke-virtual {p1, p2}, Lorg/chromium/net/CronetUrlRequest$HeadersMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 574
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1, p2, v0}, Lorg/chromium/net/CronetUrlRequest$HeadersMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 576
    :cond_0
    invoke-virtual {p1, p2}, Lorg/chromium/net/CronetUrlRequest$HeadersMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 581
    :goto_0
    return-void

    .line 577
    :catch_0
    move-exception v0

    .line 578
    const-string v1, "ChromiumNetwork"

    const-string v2, "Exception in onAppendResponseHeader method"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private onDataReceived(Ljava/nio/ByteBuffer;)V
    .locals 1
    .annotation build Lorg/chromium/base/CalledByNative;
    .end annotation

    .prologue
    .line 484
    iget-object v0, p0, Lorg/chromium/net/CronetUrlRequest;->f:Lorg/chromium/net/CronetUrlRequest$OnDataReceivedRunnable;

    if-nez v0, :cond_0

    .line 485
    new-instance v0, Lorg/chromium/net/CronetUrlRequest$OnDataReceivedRunnable;

    invoke-direct {v0, p0}, Lorg/chromium/net/CronetUrlRequest$OnDataReceivedRunnable;-><init>(Lorg/chromium/net/CronetUrlRequest;)V

    iput-object v0, p0, Lorg/chromium/net/CronetUrlRequest;->f:Lorg/chromium/net/CronetUrlRequest$OnDataReceivedRunnable;

    .line 487
    :cond_0
    iget-object v0, p0, Lorg/chromium/net/CronetUrlRequest;->f:Lorg/chromium/net/CronetUrlRequest$OnDataReceivedRunnable;

    iput-object p1, v0, Lorg/chromium/net/CronetUrlRequest$OnDataReceivedRunnable;->a:Ljava/nio/ByteBuffer;

    .line 488
    iget-object v0, p0, Lorg/chromium/net/CronetUrlRequest;->f:Lorg/chromium/net/CronetUrlRequest$OnDataReceivedRunnable;

    invoke-direct {p0, v0}, Lorg/chromium/net/CronetUrlRequest;->a(Ljava/lang/Runnable;)V

    .line 489
    return-void
.end method

.method private onError(ILjava/lang/String;)V
    .locals 1
    .annotation build Lorg/chromium/base/CalledByNative;
    .end annotation

    .prologue
    .line 541
    new-instance v0, Lorg/chromium/net/CronetUrlRequest$4;

    invoke-direct {v0, p0, p2, p1}, Lorg/chromium/net/CronetUrlRequest$4;-><init>(Lorg/chromium/net/CronetUrlRequest;Ljava/lang/String;I)V

    .line 562
    invoke-direct {p0, v0}, Lorg/chromium/net/CronetUrlRequest;->a(Ljava/lang/Runnable;)V

    .line 563
    return-void
.end method

.method private onRedirect(Ljava/lang/String;I)V
    .locals 2
    .annotation build Lorg/chromium/base/CalledByNative;
    .end annotation

    .prologue
    .line 416
    invoke-direct {p0, p2}, Lorg/chromium/net/CronetUrlRequest;->a(I)Lorg/chromium/net/CronetUrlRequest$NativeResponseInfo;

    move-result-object v0

    .line 418
    new-instance v1, Lorg/chromium/net/CronetUrlRequest$1;

    invoke-direct {v1, p0, v0, p1}, Lorg/chromium/net/CronetUrlRequest$1;-><init>(Lorg/chromium/net/CronetUrlRequest;Lorg/chromium/net/CronetUrlRequest$NativeResponseInfo;Ljava/lang/String;)V

    .line 440
    invoke-direct {p0, v1}, Lorg/chromium/net/CronetUrlRequest;->a(Ljava/lang/Runnable;)V

    .line 441
    return-void
.end method

.method private onResponseStarted(I)V
    .locals 1
    .annotation build Lorg/chromium/base/CalledByNative;
    .end annotation

    .prologue
    .line 450
    invoke-direct {p0, p1}, Lorg/chromium/net/CronetUrlRequest;->a(I)Lorg/chromium/net/CronetUrlRequest$NativeResponseInfo;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/net/CronetUrlRequest;->e:Lorg/chromium/net/CronetUrlRequest$NativeResponseInfo;

    .line 451
    new-instance v0, Lorg/chromium/net/CronetUrlRequest$2;

    invoke-direct {v0, p0}, Lorg/chromium/net/CronetUrlRequest$2;-><init>(Lorg/chromium/net/CronetUrlRequest;)V

    .line 470
    invoke-direct {p0, v0}, Lorg/chromium/net/CronetUrlRequest;->a(Ljava/lang/Runnable;)V

    .line 471
    return-void
.end method

.method private onSucceeded()V
    .locals 4
    .annotation build Lorg/chromium/base/CalledByNative;
    .end annotation

    .prologue
    const-wide/16 v2, 0x0

    .line 499
    iget-object v1, p0, Lorg/chromium/net/CronetUrlRequest;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 500
    cmp-long v0, v2, v2

    if-nez v0, :cond_0

    .line 501
    :try_start_0
    monitor-exit v1

    .line 530
    :goto_0
    return-void

    .line 503
    :cond_0
    const-wide/16 v2, 0x0

    invoke-direct {p0, v2, v3}, Lorg/chromium/net/CronetUrlRequest;->nativeGetTotalReceivedBytes(J)J

    .line 505
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 507
    new-instance v0, Lorg/chromium/net/CronetUrlRequest$NativeExtendedResponseInfo;

    iget-object v1, p0, Lorg/chromium/net/CronetUrlRequest;->e:Lorg/chromium/net/CronetUrlRequest$NativeResponseInfo;

    invoke-direct {v0}, Lorg/chromium/net/CronetUrlRequest$NativeExtendedResponseInfo;-><init>()V

    .line 510
    new-instance v1, Lorg/chromium/net/CronetUrlRequest$3;

    invoke-direct {v1, p0, v0}, Lorg/chromium/net/CronetUrlRequest$3;-><init>(Lorg/chromium/net/CronetUrlRequest;Lorg/chromium/net/CronetUrlRequest$NativeExtendedResponseInfo;)V

    .line 529
    invoke-direct {p0, v1}, Lorg/chromium/net/CronetUrlRequest;->a(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 505
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 271
    iget-object v1, p0, Lorg/chromium/net/CronetUrlRequest;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 272
    :try_start_0
    monitor-exit v1

    return-void

    .line 275
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b()Z
    .locals 2

    .prologue
    .line 286
    iget-object v1, p0, Lorg/chromium/net/CronetUrlRequest;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 287
    const/4 v0, 0x0

    :try_start_0
    monitor-exit v1

    return v0

    .line 288
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
