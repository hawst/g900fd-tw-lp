.class public Lorg/chromium/net/CronetUrlRequestContext;
.super Lorg/chromium/net/UrlRequestContext;
.source "PG"


# annotations
.annotation runtime Lorg/chromium/base/JNINamespace;
.end annotation


# direct methods
.method private initNetworkThread()V
    .locals 2
    .annotation build Lorg/chromium/base/CalledByNative;
    .end annotation

    .prologue
    .line 121
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    .line 122
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    const-string v1, "ChromiumNet"

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 123
    const/16 v0, 0xa

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 124
    return-void
.end method

.method private native nativeCreateRequestContextAdapter(Landroid/content/Context;Ljava/lang/String;)J
.end method

.method private native nativeDestroyRequestContextAdapter(J)V
.end method

.method private native nativeSetMinLogLevel(I)I
.end method

.method private native nativeStartNetLogToFile(JLjava/lang/String;)V
.end method

.method private native nativeStopNetLog(J)V
.end method


# virtual methods
.method a()V
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    .line 93
    return-void
.end method
