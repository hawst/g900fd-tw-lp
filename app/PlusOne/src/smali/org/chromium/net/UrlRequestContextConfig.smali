.class public Lorg/chromium/net/UrlRequestContextConfig;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:Lorg/json/JSONObject;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 214
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    iput-object v0, p0, Lorg/chromium/net/UrlRequestContextConfig;->a:Lorg/json/JSONObject;

    .line 21
    invoke-virtual {p0, v1}, Lorg/chromium/net/UrlRequestContextConfig;->a(Z)Lorg/chromium/net/UrlRequestContextConfig;

    .line 22
    invoke-virtual {p0, v1}, Lorg/chromium/net/UrlRequestContextConfig;->b(Z)Lorg/chromium/net/UrlRequestContextConfig;

    .line 23
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lorg/chromium/net/UrlRequestContextConfig;->c(Z)Lorg/chromium/net/UrlRequestContextConfig;

    .line 24
    sget-object v0, Lorg/chromium/net/UrlRequestContextConfig$HttpCache;->b:Lorg/chromium/net/UrlRequestContextConfig$HttpCache;

    const-wide/32 v2, 0x19000

    invoke-virtual {p0, v0, v2, v3}, Lorg/chromium/net/UrlRequestContextConfig;->a(Lorg/chromium/net/UrlRequestContextConfig$HttpCache;J)Lorg/chromium/net/UrlRequestContextConfig;

    .line 25
    return-void
.end method

.method private a(Ljava/lang/String;J)Lorg/chromium/net/UrlRequestContextConfig;
    .locals 2

    .prologue
    .line 194
    :try_start_0
    iget-object v0, p0, Lorg/chromium/net/UrlRequestContextConfig;->a:Lorg/json/JSONObject;

    invoke-virtual {v0, p1, p2, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 198
    :goto_0
    return-object p0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Lorg/chromium/net/UrlRequestContextConfig;
    .locals 1

    .prologue
    .line 207
    :try_start_0
    iget-object v0, p0, Lorg/chromium/net/UrlRequestContextConfig;->a:Lorg/json/JSONObject;

    invoke-virtual {v0, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 211
    :goto_0
    return-object p0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Z)Lorg/chromium/net/UrlRequestContextConfig;
    .locals 1

    .prologue
    .line 181
    :try_start_0
    iget-object v0, p0, Lorg/chromium/net/UrlRequestContextConfig;->a:Lorg/json/JSONObject;

    invoke-virtual {v0, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 185
    :goto_0
    return-object p0

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lorg/chromium/net/UrlRequestContextConfig;
    .locals 1

    .prologue
    .line 49
    const-string v0, "STORAGE_PATH"

    invoke-direct {p0, v0, p1}, Lorg/chromium/net/UrlRequestContextConfig;->a(Ljava/lang/String;Ljava/lang/String;)Lorg/chromium/net/UrlRequestContextConfig;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;II)Lorg/chromium/net/UrlRequestContextConfig;
    .locals 3

    .prologue
    .line 130
    const-string v0, "/"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 131
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Illegal QUIC Hint Host: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 135
    :cond_0
    :try_start_0
    iget-object v0, p0, Lorg/chromium/net/UrlRequestContextConfig;->a:Lorg/json/JSONObject;

    const-string v1, "QUIC_HINTS"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 137
    if-nez v0, :cond_1

    .line 138
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    .line 139
    iget-object v1, p0, Lorg/chromium/net/UrlRequestContextConfig;->a:Lorg/json/JSONObject;

    const-string v2, "QUIC_HINTS"

    invoke-virtual {v1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 142
    :cond_1
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 143
    const-string v2, "QUIC_HINT_HOST"

    invoke-virtual {v1, v2, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 144
    const-string v2, "QUIC_HINT_PORT"

    invoke-virtual {v1, v2, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 145
    const-string v2, "QUIC_HINT_ALT_PORT"

    invoke-virtual {v1, v2, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 147
    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 151
    :goto_0
    return-object p0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public a(Lorg/chromium/net/UrlRequestContextConfig$HttpCache;J)Lorg/chromium/net/UrlRequestContextConfig;
    .locals 2

    .prologue
    .line 100
    sget-object v0, Lorg/chromium/net/UrlRequestContextConfig$1;->a:[I

    invoke-virtual {p1}, Lorg/chromium/net/UrlRequestContextConfig$HttpCache;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 115
    :goto_0
    return-object p0

    .line 102
    :pswitch_0
    const-string v0, "HTTP_CACHE"

    const-string v1, "HTTP_CACHE_DISABLED"

    invoke-direct {p0, v0, v1}, Lorg/chromium/net/UrlRequestContextConfig;->a(Ljava/lang/String;Ljava/lang/String;)Lorg/chromium/net/UrlRequestContextConfig;

    move-result-object p0

    goto :goto_0

    .line 105
    :pswitch_1
    const-string v0, "HTTP_CACHE_MAX_SIZE"

    invoke-direct {p0, v0, p2, p3}, Lorg/chromium/net/UrlRequestContextConfig;->a(Ljava/lang/String;J)Lorg/chromium/net/UrlRequestContextConfig;

    .line 107
    const-string v0, "HTTP_CACHE"

    const-string v1, "HTTP_CACHE_DISK"

    invoke-direct {p0, v0, v1}, Lorg/chromium/net/UrlRequestContextConfig;->a(Ljava/lang/String;Ljava/lang/String;)Lorg/chromium/net/UrlRequestContextConfig;

    move-result-object p0

    goto :goto_0

    .line 110
    :pswitch_2
    const-string v0, "HTTP_CACHE_MAX_SIZE"

    invoke-direct {p0, v0, p2, p3}, Lorg/chromium/net/UrlRequestContextConfig;->a(Ljava/lang/String;J)Lorg/chromium/net/UrlRequestContextConfig;

    .line 112
    const-string v0, "HTTP_CACHE"

    const-string v1, "HTTP_CACHE_MEMORY"

    invoke-direct {p0, v0, v1}, Lorg/chromium/net/UrlRequestContextConfig;->a(Ljava/lang/String;Ljava/lang/String;)Lorg/chromium/net/UrlRequestContextConfig;

    move-result-object p0

    goto :goto_0

    .line 100
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(Z)Lorg/chromium/net/UrlRequestContextConfig;
    .locals 1

    .prologue
    .line 57
    const-string v0, "ENABLE_LEGACY_MODE"

    invoke-direct {p0, v0, p1}, Lorg/chromium/net/UrlRequestContextConfig;->a(Ljava/lang/String;Z)Lorg/chromium/net/UrlRequestContextConfig;

    move-result-object v0

    return-object v0
.end method

.method a()Z
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Lorg/chromium/net/UrlRequestContextConfig;->a:Lorg/json/JSONObject;

    const-string v1, "ENABLE_LEGACY_MODE"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method b()Ljava/lang/String;
    .locals 3

    .prologue
    .line 75
    iget-object v0, p0, Lorg/chromium/net/UrlRequestContextConfig;->a:Lorg/json/JSONObject;

    const-string v1, "NATIVE_LIBRARY_NAME"

    const-string v2, "cronet"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lorg/chromium/net/UrlRequestContextConfig;
    .locals 1

    .prologue
    .line 163
    const-string v0, "QUIC_OPTIONS"

    invoke-direct {p0, v0, p1}, Lorg/chromium/net/UrlRequestContextConfig;->a(Ljava/lang/String;Ljava/lang/String;)Lorg/chromium/net/UrlRequestContextConfig;

    move-result-object v0

    return-object v0
.end method

.method public b(Z)Lorg/chromium/net/UrlRequestContextConfig;
    .locals 1

    .prologue
    .line 83
    const-string v0, "ENABLE_QUIC"

    invoke-direct {p0, v0, p1}, Lorg/chromium/net/UrlRequestContextConfig;->a(Ljava/lang/String;Z)Lorg/chromium/net/UrlRequestContextConfig;

    move-result-object v0

    return-object v0
.end method

.method public c(Z)Lorg/chromium/net/UrlRequestContextConfig;
    .locals 1

    .prologue
    .line 90
    const-string v0, "ENABLE_SPDY"

    invoke-direct {p0, v0, p1}, Lorg/chromium/net/UrlRequestContextConfig;->a(Ljava/lang/String;Z)Lorg/chromium/net/UrlRequestContextConfig;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lorg/chromium/net/UrlRequestContextConfig;->a:Lorg/json/JSONObject;

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
