.class Lorg/chromium/net/CronetUrlRequest$1;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic a:Lorg/chromium/net/CronetUrlRequest$NativeResponseInfo;

.field private synthetic b:Ljava/lang/String;

.field private synthetic c:Lorg/chromium/net/CronetUrlRequest;


# direct methods
.method constructor <init>(Lorg/chromium/net/CronetUrlRequest;Lorg/chromium/net/CronetUrlRequest$NativeResponseInfo;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 418
    iput-object p1, p0, Lorg/chromium/net/CronetUrlRequest$1;->c:Lorg/chromium/net/CronetUrlRequest;

    iput-object p2, p0, Lorg/chromium/net/CronetUrlRequest$1;->a:Lorg/chromium/net/CronetUrlRequest$NativeResponseInfo;

    iput-object p3, p0, Lorg/chromium/net/CronetUrlRequest$1;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 420
    iget-object v0, p0, Lorg/chromium/net/CronetUrlRequest$1;->c:Lorg/chromium/net/CronetUrlRequest;

    invoke-virtual {v0}, Lorg/chromium/net/CronetUrlRequest;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 438
    :goto_0
    return-void

    .line 424
    :cond_0
    :try_start_0
    iget-object v0, p0, Lorg/chromium/net/CronetUrlRequest$1;->c:Lorg/chromium/net/CronetUrlRequest;

    iget-object v0, p0, Lorg/chromium/net/CronetUrlRequest$1;->c:Lorg/chromium/net/CronetUrlRequest;

    iget-object v0, p0, Lorg/chromium/net/CronetUrlRequest$1;->a:Lorg/chromium/net/CronetUrlRequest$NativeResponseInfo;

    iget-object v0, p0, Lorg/chromium/net/CronetUrlRequest$1;->b:Ljava/lang/String;

    .line 426
    iget-object v0, p0, Lorg/chromium/net/CronetUrlRequest$1;->c:Lorg/chromium/net/CronetUrlRequest;

    invoke-static {v0}, Lorg/chromium/net/CronetUrlRequest;->a(Lorg/chromium/net/CronetUrlRequest;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 427
    :try_start_1
    iget-object v0, p0, Lorg/chromium/net/CronetUrlRequest$1;->c:Lorg/chromium/net/CronetUrlRequest;

    invoke-virtual {v0}, Lorg/chromium/net/CronetUrlRequest;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 428
    monitor-exit v1

    goto :goto_0

    .line 434
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 437
    :catch_0
    move-exception v0

    .line 436
    iget-object v1, p0, Lorg/chromium/net/CronetUrlRequest$1;->c:Lorg/chromium/net/CronetUrlRequest;

    invoke-static {v1, v0}, Lorg/chromium/net/CronetUrlRequest;->a(Lorg/chromium/net/CronetUrlRequest;Ljava/lang/Exception;)V

    goto :goto_0

    .line 432
    :cond_1
    :try_start_3
    iget-object v0, p0, Lorg/chromium/net/CronetUrlRequest$1;->c:Lorg/chromium/net/CronetUrlRequest;

    invoke-static {v0}, Lorg/chromium/net/CronetUrlRequest;->d(Lorg/chromium/net/CronetUrlRequest;)Ljava/util/List;

    move-result-object v0

    iget-object v2, p0, Lorg/chromium/net/CronetUrlRequest$1;->b:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 433
    iget-object v0, p0, Lorg/chromium/net/CronetUrlRequest$1;->c:Lorg/chromium/net/CronetUrlRequest;

    iget-object v2, p0, Lorg/chromium/net/CronetUrlRequest$1;->c:Lorg/chromium/net/CronetUrlRequest;

    invoke-static {}, Lorg/chromium/net/CronetUrlRequest;->c()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lorg/chromium/net/CronetUrlRequest;->b(Lorg/chromium/net/CronetUrlRequest;J)V

    .line 434
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method
