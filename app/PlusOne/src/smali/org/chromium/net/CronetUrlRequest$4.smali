.class Lorg/chromium/net/CronetUrlRequest$4;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic a:Ljava/lang/String;

.field private synthetic b:I

.field private synthetic c:Lorg/chromium/net/CronetUrlRequest;


# direct methods
.method constructor <init>(Lorg/chromium/net/CronetUrlRequest;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 541
    iput-object p1, p0, Lorg/chromium/net/CronetUrlRequest$4;->c:Lorg/chromium/net/CronetUrlRequest;

    iput-object p2, p0, Lorg/chromium/net/CronetUrlRequest$4;->a:Ljava/lang/String;

    iput p3, p0, Lorg/chromium/net/CronetUrlRequest$4;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 543
    iget-object v0, p0, Lorg/chromium/net/CronetUrlRequest$4;->c:Lorg/chromium/net/CronetUrlRequest;

    invoke-virtual {v0}, Lorg/chromium/net/CronetUrlRequest;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 560
    :goto_0
    return-void

    .line 548
    :cond_0
    iget-object v0, p0, Lorg/chromium/net/CronetUrlRequest$4;->c:Lorg/chromium/net/CronetUrlRequest;

    invoke-static {v0}, Lorg/chromium/net/CronetUrlRequest;->c(Lorg/chromium/net/CronetUrlRequest;)V

    .line 550
    :try_start_0
    new-instance v0, Lorg/chromium/net/UrlRequestException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Exception in CronetUrlRequest: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/chromium/net/CronetUrlRequest$4;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lorg/chromium/net/CronetUrlRequest$4;->b:I

    invoke-direct {v0, v1}, Lorg/chromium/net/UrlRequestException;-><init>(Ljava/lang/String;)V

    .line 553
    iget-object v0, p0, Lorg/chromium/net/CronetUrlRequest$4;->c:Lorg/chromium/net/CronetUrlRequest;

    iget-object v0, p0, Lorg/chromium/net/CronetUrlRequest$4;->c:Lorg/chromium/net/CronetUrlRequest;

    iget-object v0, p0, Lorg/chromium/net/CronetUrlRequest$4;->c:Lorg/chromium/net/CronetUrlRequest;

    invoke-static {v0}, Lorg/chromium/net/CronetUrlRequest;->b(Lorg/chromium/net/CronetUrlRequest;)Lorg/chromium/net/CronetUrlRequest$NativeResponseInfo;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 556
    :catch_0
    move-exception v0

    .line 557
    const-string v1, "ChromiumNetwork"

    const-string v2, "Exception in onError method"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
