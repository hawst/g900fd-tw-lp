.class Lorg/chromium/net/HttpUrlConnectionUrlRequest;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lorg/chromium/net/HttpUrlRequest;


# static fields
.field private static y:Ljava/util/concurrent/ExecutorService;

.field private static final z:Ljava/lang/Object;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/nio/channels/WritableByteChannel;

.field private final e:Lorg/chromium/net/HttpUrlRequestListener;

.field private f:Ljava/io/IOException;

.field private g:Ljava/net/HttpURLConnection;

.field private h:J

.field private i:I

.field private j:I

.field private k:J

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:J

.field private p:Ljava/lang/String;

.field private q:[B

.field private r:Ljava/nio/channels/ReadableByteChannel;

.field private s:Ljava/lang/String;

.field private t:I

.field private u:Z

.field private v:Ljava/lang/String;

.field private w:Ljava/io/InputStream;

.field private final x:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 96
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->z:Ljava/lang/Object;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;Ljava/nio/channels/WritableByteChannel;Lorg/chromium/net/HttpUrlRequestListener;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/nio/channels/WritableByteChannel;",
            "Lorg/chromium/net/HttpUrlRequestListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    if-nez p1, :cond_0

    .line 109
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Context is required"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 111
    :cond_0
    if-nez p2, :cond_1

    .line 112
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "URL is required"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 114
    :cond_1
    iput-object p1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->a:Landroid/content/Context;

    .line 115
    iput-object p2, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->b:Ljava/lang/String;

    .line 116
    iput-object p3, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->c:Ljava/util/Map;

    .line 117
    iput-object p4, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->d:Ljava/nio/channels/WritableByteChannel;

    .line 118
    iput-object p5, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->e:Lorg/chromium/net/HttpUrlRequestListener;

    .line 119
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->x:Ljava/lang/Object;

    .line 120
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;Lorg/chromium/net/HttpUrlRequestListener;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lorg/chromium/net/HttpUrlRequestListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 101
    new-instance v4, Lorg/chromium/net/ChunkedWritableByteChannel;

    invoke-direct {v4}, Lorg/chromium/net/ChunkedWritableByteChannel;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lorg/chromium/net/HttpUrlConnectionUrlRequest;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;Ljava/nio/channels/WritableByteChannel;Lorg/chromium/net/HttpUrlRequestListener;)V

    .line 103
    return-void
.end method

.method static synthetic a(Lorg/chromium/net/HttpUrlConnectionUrlRequest;)V
    .locals 11

    .prologue
    const/4 v3, 0x0

    const/4 v10, -0x1

    const-wide/16 v8, 0x0

    const/4 v2, 0x1

    .line 34
    :try_start_0
    iget-object v1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->x:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-boolean v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->u:Z

    if-eqz v0, :cond_2

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->r:Ljava/nio/channels/ReadableByteChannel;

    if-eqz v0, :cond_0

    :try_start_2
    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->r:Ljava/nio/channels/ReadableByteChannel;

    invoke-interface {v0}, Ljava/nio/channels/ReadableByteChannel;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_8

    :cond_0
    :goto_0
    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->e:Lorg/chromium/net/HttpUrlRequestListener;

    invoke-interface {v0, p0}, Lorg/chromium/net/HttpUrlRequestListener;->onRequestComplete(Lorg/chromium/net/HttpUrlRequest;)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    new-instance v0, Ljava/net/URL;

    iget-object v1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->b:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    iput-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->g:Ljava/net/HttpURLConnection;

    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->v:Ljava/lang/String;
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    if-eqz v0, :cond_3

    :try_start_5
    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->g:Ljava/net/HttpURLConnection;

    iget-object v1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->v:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/net/ProtocolException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :cond_3
    :try_start_6
    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->g:Ljava/net/HttpURLConnection;

    const/16 v1, 0xbb8

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->g:Ljava/net/HttpURLConnection;

    const v1, 0x15f90

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->g:Ljava/net/HttpURLConnection;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setInstanceFollowRedirects(Z)V

    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->c:Ljava/util/Map;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    iget-object v5, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->g:Ljava/net/HttpURLConnection;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v5, v1, v0}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_2

    :catch_0
    move-exception v0

    :goto_3
    :try_start_7
    iput-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->f:Ljava/io/IOException;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->r:Ljava/nio/channels/ReadableByteChannel;

    if-eqz v0, :cond_4

    :try_start_8
    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->r:Ljava/nio/channels/ReadableByteChannel;

    invoke-interface {v0}, Ljava/nio/channels/ReadableByteChannel;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3

    :cond_4
    :goto_4
    if-nez v3, :cond_1

    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->e:Lorg/chromium/net/HttpUrlRequestListener;

    invoke-interface {v0, p0}, Lorg/chromium/net/HttpUrlRequestListener;->onRequestComplete(Lorg/chromium/net/HttpUrlRequest;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    :try_start_9
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :try_start_a
    throw v0
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_0
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    :catchall_1
    move-exception v0

    :goto_5
    iget-object v1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->r:Ljava/nio/channels/ReadableByteChannel;

    if-eqz v1, :cond_5

    :try_start_b
    iget-object v1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->r:Ljava/nio/channels/ReadableByteChannel;

    invoke-interface {v1}, Ljava/nio/channels/ReadableByteChannel;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_2

    :cond_5
    :goto_6
    if-nez v3, :cond_6

    iget-object v1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->e:Lorg/chromium/net/HttpUrlRequestListener;

    invoke-interface {v1, p0}, Lorg/chromium/net/HttpUrlRequestListener;->onRequestComplete(Lorg/chromium/net/HttpUrlRequest;)V

    :cond_6
    throw v0

    :catch_1
    move-exception v0

    :try_start_c
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_7
    iget-wide v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->h:J

    cmp-long v0, v0, v8

    if-eqz v0, :cond_8

    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->g:Ljava/net/HttpURLConnection;

    const-string v1, "Range"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "bytes="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v6, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->h:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "-"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->g:Ljava/net/HttpURLConnection;

    const-string v1, "User-Agent"

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->getRequestProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_9

    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->g:Ljava/net/HttpURLConnection;

    const-string v1, "User-Agent"

    iget-object v4, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->a:Landroid/content/Context;

    invoke-static {v4}, Lorg/chromium/net/UserAgent;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->q:[B

    if-nez v0, :cond_a

    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->r:Ljava/nio/channels/ReadableByteChannel;

    if-eqz v0, :cond_b

    :cond_a
    invoke-direct {p0}, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->m()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_0
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    :cond_b
    const/4 v0, 0x0

    :try_start_d
    iget-object v1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->g:Ljava/net/HttpURLConnection;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;
    :try_end_d
    .catch Ljava/io/FileNotFoundException; {:try_start_d .. :try_end_d} :catch_7
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_0
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    move-result-object v0

    :goto_7
    :try_start_e
    iget-object v1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->g:Ljava/net/HttpURLConnection;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v1

    iput v1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->t:I

    iget-object v1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->g:Ljava/net/HttpURLConnection;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getResponseMessage()Ljava/lang/String;

    iget-object v1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->g:Ljava/net/HttpURLConnection;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getContentType()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->s:Ljava/lang/String;

    iget-object v1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->g:Ljava/net/HttpURLConnection;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getContentLength()I

    move-result v1

    iput v1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->i:I

    iget-wide v4, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->k:J

    cmp-long v1, v4, v8

    if-lez v1, :cond_d

    iget v1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->i:I

    int-to-long v4, v1

    iget-wide v6, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->k:J

    cmp-long v1, v4, v6

    if-lez v1, :cond_d

    iget-boolean v1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->l:Z

    if-eqz v1, :cond_d

    invoke-direct {p0}, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->n()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_0
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->r:Ljava/nio/channels/ReadableByteChannel;

    if-eqz v0, :cond_c

    :try_start_f
    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->r:Ljava/nio/channels/ReadableByteChannel;

    invoke-interface {v0}, Ljava/nio/channels/ReadableByteChannel;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_6

    :cond_c
    :goto_8
    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->e:Lorg/chromium/net/HttpUrlRequestListener;

    invoke-interface {v0, p0}, Lorg/chromium/net/HttpUrlRequestListener;->onRequestComplete(Lorg/chromium/net/HttpUrlRequest;)V

    goto/16 :goto_1

    :cond_d
    :try_start_10
    iget-object v1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->e:Lorg/chromium/net/HttpUrlRequestListener;

    invoke-interface {v1, p0}, Lorg/chromium/net/HttpUrlRequestListener;->onResponseStarted(Lorg/chromium/net/HttpUrlRequest;)V

    iget v1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->t:I

    div-int/lit8 v1, v1, 0x64

    const/4 v4, 0x2

    if-eq v1, v4, :cond_13

    move v1, v2

    :goto_9
    if-eqz v1, :cond_e

    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->g:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getErrorStream()Ljava/io/InputStream;

    move-result-object v0

    :cond_e
    iput-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->w:Ljava/io/InputStream;

    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->w:Ljava/io/InputStream;

    if-eqz v0, :cond_f

    const-string v0, "gzip"

    iget-object v1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->g:Ljava/net/HttpURLConnection;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getContentEncoding()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    new-instance v0, Ljava/util/zip/GZIPInputStream;

    iget-object v1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->w:Ljava/io/InputStream;

    invoke-direct {v0, v1}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->w:Ljava/io/InputStream;

    const/4 v0, -0x1

    iput v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->i:I

    :cond_f
    iget-wide v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->h:J

    cmp-long v0, v0, v8

    if-eqz v0, :cond_11

    iget v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->t:I

    const/16 v1, 0xc8

    if-ne v0, v1, :cond_14

    iget v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->i:I

    if-eq v0, v10, :cond_10

    iget v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->i:I

    int-to-long v0, v0

    iget-wide v4, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->h:J

    sub-long/2addr v0, v4

    long-to-int v0, v0

    iput v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->i:I

    :cond_10
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->n:Z

    :cond_11
    :goto_a
    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->w:Ljava/io/InputStream;
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_0
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    if-eqz v0, :cond_15

    :try_start_11
    invoke-static {}, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->f()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lorg/chromium/net/HttpUrlConnectionUrlRequest$3;

    invoke-direct {v1, p0}, Lorg/chromium/net/HttpUrlConnectionUrlRequest$3;-><init>(Lorg/chromium/net/HttpUrlConnectionUrlRequest;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_4
    .catchall {:try_start_11 .. :try_end_11} :catchall_2

    move v0, v2

    :goto_b
    iget-object v1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->r:Ljava/nio/channels/ReadableByteChannel;

    if-eqz v1, :cond_12

    :try_start_12
    iget-object v1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->r:Ljava/nio/channels/ReadableByteChannel;

    invoke-interface {v1}, Ljava/nio/channels/ReadableByteChannel;->close()V
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_5

    :cond_12
    :goto_c
    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->e:Lorg/chromium/net/HttpUrlRequestListener;

    invoke-interface {v0, p0}, Lorg/chromium/net/HttpUrlRequestListener;->onRequestComplete(Lorg/chromium/net/HttpUrlRequest;)V

    goto/16 :goto_1

    :cond_13
    move v1, v3

    goto :goto_9

    :cond_14
    :try_start_13
    iget-wide v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->h:J

    iput-wide v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->o:J
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_0
    .catchall {:try_start_13 .. :try_end_13} :catchall_1

    goto :goto_a

    :catch_2
    move-exception v1

    goto/16 :goto_6

    :catchall_2
    move-exception v0

    move v3, v2

    goto/16 :goto_5

    :catch_3
    move-exception v0

    goto/16 :goto_4

    :catch_4
    move-exception v0

    move v3, v2

    goto/16 :goto_3

    :catch_5
    move-exception v1

    goto :goto_c

    :catch_6
    move-exception v0

    goto/16 :goto_8

    :catch_7
    move-exception v1

    goto/16 :goto_7

    :catch_8
    move-exception v0

    goto/16 :goto_0

    :cond_15
    move v0, v3

    goto :goto_b
.end method

.method static synthetic b(Lorg/chromium/net/HttpUrlConnectionUrlRequest;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 34
    :try_start_0
    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->w:Ljava/io/InputStream;

    if-eqz v0, :cond_2

    const/16 v0, 0x2000

    new-array v3, v0, [B

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->i()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->w:Ljava/io/InputStream;

    invoke-virtual {v0, v3}, Ljava/io/InputStream;->read([B)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    iget-wide v4, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->o:J

    int-to-long v6, v0

    add-long/2addr v4, v6

    iput-wide v4, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->o:J

    iget-boolean v1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->n:Z

    if-eqz v1, :cond_6

    iget-wide v4, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->o:J

    iget-wide v6, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->h:J

    cmp-long v1, v4, v6

    if-lez v1, :cond_0

    const/4 v1, 0x0

    iput-boolean v1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->n:Z

    iget-wide v4, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->h:J

    iget-wide v6, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->o:J

    int-to-long v8, v0

    sub-long/2addr v6, v8

    sub-long/2addr v4, v6

    long-to-int v1, v4

    sub-int/2addr v0, v1

    :goto_1
    iget-wide v4, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->k:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_4

    iget-wide v4, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->o:J

    iget-wide v6, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->k:J

    cmp-long v4, v4, v6

    if-lez v4, :cond_4

    iget-wide v4, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->o:J

    iget-wide v6, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->k:J

    sub-long/2addr v4, v6

    long-to-int v2, v4

    sub-int/2addr v0, v2

    if-lez v0, :cond_1

    iget-object v2, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->d:Ljava/nio/channels/WritableByteChannel;

    invoke-static {v3, v1, v0}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/nio/channels/WritableByteChannel;->write(Ljava/nio/ByteBuffer;)I

    :cond_1
    invoke-direct {p0}, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->n()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    :try_start_1
    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->g:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_1
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_6

    :goto_2
    :try_start_2
    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->d:Ljava/nio/channels/WritableByteChannel;

    invoke-interface {v0}, Ljava/nio/channels/WritableByteChannel;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_3
    :goto_3
    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->e:Lorg/chromium/net/HttpUrlRequestListener;

    invoke-interface {v0, p0}, Lorg/chromium/net/HttpUrlRequestListener;->onRequestComplete(Lorg/chromium/net/HttpUrlRequest;)V

    return-void

    :cond_4
    :try_start_3
    iget-object v4, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->d:Ljava/nio/channels/WritableByteChannel;

    invoke-static {v3, v1, v0}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/nio/channels/WritableByteChannel;->write(Ljava/nio/ByteBuffer;)I
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_4
    iput-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->f:Ljava/io/IOException;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->g:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_5
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_5 .. :try_end_5} :catch_5

    :goto_4
    :try_start_6
    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->d:Ljava/nio/channels/WritableByteChannel;

    invoke-interface {v0}, Ljava/nio/channels/WritableByteChannel;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_3

    :catch_1
    move-exception v0

    iget-object v1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->f:Ljava/io/IOException;

    if-nez v1, :cond_3

    iput-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->f:Ljava/io/IOException;

    goto :goto_3

    :catch_2
    move-exception v0

    iget-object v1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->f:Ljava/io/IOException;

    if-nez v1, :cond_3

    iput-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->f:Ljava/io/IOException;

    goto :goto_3

    :catchall_0
    move-exception v0

    :try_start_7
    iget-object v1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->g:Ljava/net/HttpURLConnection;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_7
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_7 .. :try_end_7} :catch_4

    :goto_5
    :try_start_8
    iget-object v1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->d:Ljava/nio/channels/WritableByteChannel;

    invoke-interface {v1}, Ljava/nio/channels/WritableByteChannel;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3

    :cond_5
    :goto_6
    throw v0

    :catch_3
    move-exception v1

    iget-object v2, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->f:Ljava/io/IOException;

    if-nez v2, :cond_5

    iput-object v1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->f:Ljava/io/IOException;

    goto :goto_6

    :catch_4
    move-exception v1

    goto :goto_5

    :catch_5
    move-exception v0

    goto :goto_4

    :catch_6
    move-exception v0

    goto :goto_2

    :cond_6
    move v1, v2

    goto :goto_1
.end method

.method private static f()Ljava/util/concurrent/ExecutorService;
    .locals 2

    .prologue
    .line 123
    sget-object v1, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->z:Ljava/lang/Object;

    monitor-enter v1

    .line 124
    :try_start_0
    sget-object v0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->y:Ljava/util/concurrent/ExecutorService;

    if-nez v0, :cond_0

    .line 125
    new-instance v0, Lorg/chromium/net/HttpUrlConnectionUrlRequest$1;

    invoke-direct {v0}, Lorg/chromium/net/HttpUrlConnectionUrlRequest$1;-><init>()V

    .line 139
    invoke-static {v0}, Ljava/util/concurrent/Executors;->newCachedThreadPool(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->y:Ljava/util/concurrent/ExecutorService;

    .line 141
    :cond_0
    sget-object v0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->y:Ljava/util/concurrent/ExecutorService;

    monitor-exit v1

    return-object v0

    .line 142
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private m()V
    .locals 5

    .prologue
    .line 318
    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->g:Ljava/net/HttpURLConnection;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 319
    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->p:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 320
    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->g:Ljava/net/HttpURLConnection;

    const-string v1, "Content-Type"

    iget-object v2, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->p:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    :cond_0
    const/4 v1, 0x0

    .line 325
    :try_start_0
    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->q:[B

    if-eqz v0, :cond_3

    .line 326
    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->g:Ljava/net/HttpURLConnection;

    iget-object v2, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->q:[B

    array-length v2, v2

    invoke-virtual {v0, v2}, Ljava/net/HttpURLConnection;->setFixedLengthStreamingMode(I)V

    .line 327
    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->g:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    .line 328
    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->q:[B

    invoke-virtual {v1, v0}, Ljava/io/OutputStream;->write([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 341
    :cond_1
    if-eqz v1, :cond_2

    .line 342
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    .line 345
    :cond_2
    return-void

    .line 330
    :cond_3
    :try_start_1
    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->g:Ljava/net/HttpURLConnection;

    iget v2, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->j:I

    invoke-virtual {v0, v2}, Ljava/net/HttpURLConnection;->setFixedLengthStreamingMode(I)V

    .line 331
    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->g:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    .line 332
    const/16 v0, 0x2000

    new-array v0, v0, [B

    .line 333
    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 334
    :goto_0
    iget-object v3, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->r:Ljava/nio/channels/ReadableByteChannel;

    invoke-interface {v3, v2}, Ljava/nio/channels/ReadableByteChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v3

    if-lez v3, :cond_1

    .line 335
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 336
    const/4 v3, 0x0

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->limit()I

    move-result v4

    invoke-virtual {v1, v0, v3, v4}, Ljava/io/OutputStream;->write([BII)V

    .line 337
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 341
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_4

    .line 342
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    :cond_4
    throw v0
.end method

.method private n()V
    .locals 1

    .prologue
    .line 463
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->m:Z

    .line 464
    invoke-virtual {p0}, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->h()V

    .line 465
    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 486
    iget v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->i:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 152
    iput-wide p1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->h:J

    .line 153
    return-void
.end method

.method public a(JZ)V
    .locals 1

    .prologue
    .line 157
    iput-wide p1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->k:J

    .line 158
    iput-boolean p3, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->l:Z

    .line 159
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 186
    iput-object p1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->v:Ljava/lang/String;

    .line 188
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/nio/channels/ReadableByteChannel;J)V
    .locals 3

    .prologue
    .line 172
    const-wide/32 v0, 0x7fffffff

    cmp-long v0, p3, v0

    if-lez v0, :cond_0

    .line 174
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Upload contentLength is too big."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 177
    :cond_0
    long-to-int v0, p3

    iput v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->j:I

    .line 178
    iput-object p1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->p:Ljava/lang/String;

    .line 179
    iput-object p2, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->r:Ljava/nio/channels/ReadableByteChannel;

    .line 180
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->q:[B

    .line 181
    return-void
.end method

.method public a(Ljava/lang/String;[B)V
    .locals 1

    .prologue
    .line 163
    iput-object p1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->p:Ljava/lang/String;

    .line 165
    iput-object p2, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->q:[B

    .line 166
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->r:Ljava/nio/channels/ReadableByteChannel;

    .line 167
    return-void
.end method

.method public b()I
    .locals 2

    .prologue
    .line 436
    iget v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->t:I

    .line 443
    const/16 v1, 0xce

    if-ne v0, v1, :cond_0

    .line 444
    const/16 v0, 0xc8

    .line 446
    :cond_0
    return v0
.end method

.method public b(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 496
    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->g:Ljava/net/HttpURLConnection;

    if-nez v0, :cond_0

    .line 497
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Response headers not available"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 499
    :cond_0
    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->g:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getHeaderFields()Ljava/util/Map;

    move-result-object v0

    .line 500
    if-eqz v0, :cond_1

    .line 501
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 502
    if-eqz v0, :cond_1

    .line 503
    const-string v1, ", "

    invoke-static {v1, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    .line 506
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Ljava/io/IOException;
    .locals 1

    .prologue
    .line 456
    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->f:Ljava/io/IOException;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->m:Z

    if-eqz v0, :cond_0

    .line 457
    new-instance v0, Lorg/chromium/net/ResponseTooLargeException;

    invoke-direct {v0}, Lorg/chromium/net/ResponseTooLargeException;-><init>()V

    iput-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->f:Ljava/io/IOException;

    .line 459
    :cond_0
    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->f:Ljava/io/IOException;

    return-object v0
.end method

.method public d()Ljava/nio/ByteBuffer;
    .locals 1

    .prologue
    .line 476
    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->d:Ljava/nio/channels/WritableByteChannel;

    check-cast v0, Lorg/chromium/net/ChunkedWritableByteChannel;

    invoke-virtual {v0}, Lorg/chromium/net/ChunkedWritableByteChannel;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    return-object v0
.end method

.method public e()[B
    .locals 1

    .prologue
    .line 481
    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->d:Ljava/nio/channels/WritableByteChannel;

    check-cast v0, Lorg/chromium/net/ChunkedWritableByteChannel;

    invoke-virtual {v0}, Lorg/chromium/net/ChunkedWritableByteChannel;->b()[B

    move-result-object v0

    return-object v0
.end method

.method public g()V
    .locals 2

    .prologue
    .line 198
    invoke-static {}, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->f()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lorg/chromium/net/HttpUrlConnectionUrlRequest$2;

    invoke-direct {v1, p0}, Lorg/chromium/net/HttpUrlConnectionUrlRequest$2;-><init>(Lorg/chromium/net/HttpUrlConnectionUrlRequest;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 204
    return-void
.end method

.method public h()V
    .locals 2

    .prologue
    .line 413
    iget-object v1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->x:Ljava/lang/Object;

    monitor-enter v1

    .line 414
    :try_start_0
    iget-boolean v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->u:Z

    if-eqz v0, :cond_0

    .line 415
    monitor-exit v1

    .line 419
    :goto_0
    return-void

    .line 418
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->u:Z

    .line 419
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public i()Z
    .locals 2

    .prologue
    .line 424
    iget-object v1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->x:Ljava/lang/Object;

    monitor-enter v1

    .line 425
    :try_start_0
    iget-boolean v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->u:Z

    monitor-exit v1

    return v0

    .line 426
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 431
    const-string v0, ""

    return-object v0
.end method

.method public k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 491
    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->s:Ljava/lang/String;

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->b:Ljava/lang/String;

    return-object v0
.end method
