.class Lorg/chromium/net/CronetUrlRequest$3;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic a:Lorg/chromium/net/CronetUrlRequest$NativeExtendedResponseInfo;

.field private synthetic b:Lorg/chromium/net/CronetUrlRequest;


# direct methods
.method constructor <init>(Lorg/chromium/net/CronetUrlRequest;Lorg/chromium/net/CronetUrlRequest$NativeExtendedResponseInfo;)V
    .locals 0

    .prologue
    .line 510
    iput-object p1, p0, Lorg/chromium/net/CronetUrlRequest$3;->b:Lorg/chromium/net/CronetUrlRequest;

    iput-object p2, p0, Lorg/chromium/net/CronetUrlRequest$3;->a:Lorg/chromium/net/CronetUrlRequest$NativeExtendedResponseInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 512
    iget-object v0, p0, Lorg/chromium/net/CronetUrlRequest$3;->b:Lorg/chromium/net/CronetUrlRequest;

    invoke-static {v0}, Lorg/chromium/net/CronetUrlRequest;->a(Lorg/chromium/net/CronetUrlRequest;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 513
    :try_start_0
    iget-object v0, p0, Lorg/chromium/net/CronetUrlRequest$3;->b:Lorg/chromium/net/CronetUrlRequest;

    invoke-virtual {v0}, Lorg/chromium/net/CronetUrlRequest;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 514
    monitor-exit v1

    .line 527
    :goto_0
    return-void

    .line 518
    :cond_0
    iget-object v0, p0, Lorg/chromium/net/CronetUrlRequest$3;->b:Lorg/chromium/net/CronetUrlRequest;

    invoke-static {v0}, Lorg/chromium/net/CronetUrlRequest;->c(Lorg/chromium/net/CronetUrlRequest;)V

    .line 519
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 521
    :try_start_1
    iget-object v0, p0, Lorg/chromium/net/CronetUrlRequest$3;->b:Lorg/chromium/net/CronetUrlRequest;

    iget-object v0, p0, Lorg/chromium/net/CronetUrlRequest$3;->b:Lorg/chromium/net/CronetUrlRequest;

    iget-object v0, p0, Lorg/chromium/net/CronetUrlRequest$3;->a:Lorg/chromium/net/CronetUrlRequest$NativeExtendedResponseInfo;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 519
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 523
    :catch_0
    move-exception v0

    .line 524
    const-string v1, "ChromiumNetwork"

    const-string v2, "Exception in onComplete method"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
