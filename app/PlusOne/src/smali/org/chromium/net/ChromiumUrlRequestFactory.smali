.class public Lorg/chromium/net/ChromiumUrlRequestFactory;
.super Lorg/chromium/net/HttpUrlRequestFactory;
.source "PG"


# annotations
.annotation build Lorg/chromium/base/UsedByReflection;
.end annotation


# instance fields
.field private a:Lorg/chromium/net/ChromiumUrlRequestContext;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lorg/chromium/net/UrlRequestContextConfig;)V
    .locals 4
    .annotation build Lorg/chromium/base/UsedByReflection;
    .end annotation

    .prologue
    .line 24
    invoke-direct {p0}, Lorg/chromium/net/HttpUrlRequestFactory;-><init>()V

    .line 25
    invoke-virtual {p0}, Lorg/chromium/net/ChromiumUrlRequestFactory;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 26
    invoke-virtual {p2}, Lorg/chromium/net/UrlRequestContextConfig;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 27
    new-instance v0, Lorg/chromium/net/ChromiumUrlRequestContext;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p1}, Lorg/chromium/net/UserAgent;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lorg/chromium/net/UrlRequestContextConfig;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lorg/chromium/net/ChromiumUrlRequestContext;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lorg/chromium/net/ChromiumUrlRequestFactory;->a:Lorg/chromium/net/ChromiumUrlRequestContext;

    .line 31
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;ILjava/util/Map;Ljava/nio/channels/WritableByteChannel;Lorg/chromium/net/HttpUrlRequestListener;)Lorg/chromium/net/ChromiumUrlRequest;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/nio/channels/WritableByteChannel;",
            "Lorg/chromium/net/HttpUrlRequestListener;",
            ")",
            "Lorg/chromium/net/ChromiumUrlRequest;"
        }
    .end annotation

    .prologue
    .line 54
    new-instance v0, Lorg/chromium/net/ChromiumUrlRequest;

    iget-object v1, p0, Lorg/chromium/net/ChromiumUrlRequestFactory;->a:Lorg/chromium/net/ChromiumUrlRequestContext;

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lorg/chromium/net/ChromiumUrlRequest;-><init>(Lorg/chromium/net/ChromiumUrlRequestContext;Ljava/lang/String;ILjava/util/Map;Ljava/nio/channels/WritableByteChannel;Lorg/chromium/net/HttpUrlRequestListener;)V

    return-object v0
.end method

.method public a(Ljava/lang/String;ILjava/util/Map;Lorg/chromium/net/HttpUrlRequestListener;)Lorg/chromium/net/ChromiumUrlRequest;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lorg/chromium/net/HttpUrlRequestListener;",
            ")",
            "Lorg/chromium/net/ChromiumUrlRequest;"
        }
    .end annotation

    .prologue
    .line 46
    new-instance v0, Lorg/chromium/net/ChromiumUrlRequest;

    iget-object v1, p0, Lorg/chromium/net/ChromiumUrlRequestFactory;->a:Lorg/chromium/net/ChromiumUrlRequestContext;

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lorg/chromium/net/ChromiumUrlRequest;-><init>(Lorg/chromium/net/ChromiumUrlRequestContext;Ljava/lang/String;ILjava/util/Map;Lorg/chromium/net/HttpUrlRequestListener;)V

    return-object v0
.end method

.method public a()Z
    .locals 2

    .prologue
    .line 35
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 40
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Chromium/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lorg/chromium/net/ChromiumUrlRequestContext;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Ljava/lang/String;ILjava/util/Map;Ljava/nio/channels/WritableByteChannel;Lorg/chromium/net/HttpUrlRequestListener;)Lorg/chromium/net/HttpUrlRequest;
    .locals 1

    .prologue
    .line 18
    invoke-virtual/range {p0 .. p5}, Lorg/chromium/net/ChromiumUrlRequestFactory;->a(Ljava/lang/String;ILjava/util/Map;Ljava/nio/channels/WritableByteChannel;Lorg/chromium/net/HttpUrlRequestListener;)Lorg/chromium/net/ChromiumUrlRequest;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Ljava/lang/String;ILjava/util/Map;Lorg/chromium/net/HttpUrlRequestListener;)Lorg/chromium/net/HttpUrlRequest;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0, p1, p2, p3, p4}, Lorg/chromium/net/ChromiumUrlRequestFactory;->a(Ljava/lang/String;ILjava/util/Map;Lorg/chromium/net/HttpUrlRequestListener;)Lorg/chromium/net/ChromiumUrlRequest;

    move-result-object v0

    return-object v0
.end method
