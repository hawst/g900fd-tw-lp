.class Lorg/chromium/net/HttpUrlConnectionUrlRequestFactory;
.super Lorg/chromium/net/HttpUrlRequestFactory;
.source "PG"


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Lorg/chromium/net/HttpUrlRequestFactory;-><init>()V

    .line 23
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequestFactory;->a:Landroid/content/Context;

    .line 24
    return-void
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x1

    return v0
.end method

.method public b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 33
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "HttpUrlConnection/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lorg/chromium/net/Version;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;ILjava/util/Map;Ljava/nio/channels/WritableByteChannel;Lorg/chromium/net/HttpUrlRequestListener;)Lorg/chromium/net/HttpUrlRequest;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/nio/channels/WritableByteChannel;",
            "Lorg/chromium/net/HttpUrlRequestListener;",
            ")",
            "Lorg/chromium/net/HttpUrlRequest;"
        }
    .end annotation

    .prologue
    .line 47
    new-instance v0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;

    iget-object v1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequestFactory;->a:Landroid/content/Context;

    move-object v2, p1

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lorg/chromium/net/HttpUrlConnectionUrlRequest;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;Ljava/nio/channels/WritableByteChannel;Lorg/chromium/net/HttpUrlRequestListener;)V

    return-object v0
.end method

.method public b(Ljava/lang/String;ILjava/util/Map;Lorg/chromium/net/HttpUrlRequestListener;)Lorg/chromium/net/HttpUrlRequest;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lorg/chromium/net/HttpUrlRequestListener;",
            ")",
            "Lorg/chromium/net/HttpUrlRequest;"
        }
    .end annotation

    .prologue
    .line 39
    new-instance v0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;

    iget-object v1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequestFactory;->a:Landroid/content/Context;

    invoke-direct {v0, v1, p1, p3, p4}, Lorg/chromium/net/HttpUrlConnectionUrlRequest;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;Lorg/chromium/net/HttpUrlRequestListener;)V

    return-object v0
.end method
