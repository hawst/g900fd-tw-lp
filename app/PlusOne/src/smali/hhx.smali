.class public Lhhx;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhzn;
.implements Llnx;
.implements Llpw;
.implements Llrd;
.implements Llrg;


# instance fields
.field private a:Los;

.field private b:Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;

.field private c:Lhzl;

.field private d:Z

.field private e:Z

.field private f:I


# direct methods
.method public constructor <init>(Los;Llqr;)V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhhx;->d:Z

    .line 45
    const/4 v0, 0x0

    iput v0, p0, Lhhx;->f:I

    .line 48
    iput-object p1, p0, Lhhx;->a:Los;

    .line 49
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 50
    return-void
.end method

.method private c(Z)V
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lhhx;->b:Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lhhx;->b:Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->b(Z)V

    .line 124
    :cond_0
    return-void
.end method

.method private c()Z
    .locals 1

    .prologue
    .line 108
    iget-boolean v0, p0, Lhhx;->e:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lhhx;->c:Lhzl;

    invoke-interface {v0}, Lhzl;->b()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()Landroid/support/v7/widget/SearchView;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lhhx;->b:Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->c()Landroid/support/v7/widget/SearchView;

    move-result-object v0

    return-object v0
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 150
    iput p1, p0, Lhhx;->f:I

    .line 151
    iget-object v0, p0, Lhhx;->b:Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lhhx;->b:Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->a(I)V

    .line 154
    :cond_0
    return-void
.end method

.method public a(ILandroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 99
    invoke-direct {p0}, Lhhx;->c()Z

    move-result v0

    invoke-direct {p0, v0}, Lhhx;->c(Z)V

    .line 100
    return-void
.end method

.method public a(ILjava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Collection",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 104
    invoke-direct {p0}, Lhhx;->c()Z

    move-result v0

    invoke-direct {p0, v0}, Lhhx;->c(Z)V

    .line 105
    return-void
.end method

.method public a(Landroid/content/Context;Llnh;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 94
    const-class v0, Lhzl;

    invoke-virtual {p2, v0}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhzl;

    iput-object v0, p0, Lhhx;->c:Lhzl;

    .line 95
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lhhx;->b:Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;

    if-eqz v0, :cond_0

    .line 128
    iget-object v0, p0, Lhhx;->b:Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->a(Ljava/lang/CharSequence;)V

    .line 130
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 139
    iput-boolean p1, p0, Lhhx;->d:Z

    .line 140
    iget-object v0, p0, Lhhx;->b:Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lhhx;->b:Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->a(Z)V

    .line 143
    :cond_0
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lhhx;->b:Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;

    if-eqz v0, :cond_0

    .line 134
    iget-object v0, p0, Lhhx;->b:Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->a()V

    .line 136
    :cond_0
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 113
    const-string v0, "AclPickerActionBarHandler.search_mode_enabled"

    iget-object v1, p0, Lhhx;->b:Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->d()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 114
    invoke-virtual {p0}, Lhhx;->a()Landroid/support/v7/widget/SearchView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView;->c()Ljava/lang/CharSequence;

    move-result-object v0

    .line 115
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 116
    const-string v1, "AclPickerActionBarHandler.search_mode_query"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 118
    :cond_0
    return-void
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 146
    iput-boolean p1, p0, Lhhx;->e:Z

    .line 147
    return-void
.end method

.method public b_(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 54
    iget-object v0, p0, Lhhx;->a:Los;

    invoke-virtual {v0}, Los;->h()Loo;

    move-result-object v1

    .line 55
    invoke-virtual {v1, v5}, Loo;->a(Ljava/lang/CharSequence;)V

    .line 56
    invoke-virtual {v1, v4}, Loo;->b(Z)V

    .line 58
    iget-object v0, p0, Lhhx;->a:Los;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 60
    const v2, 0x7f04001f

    .line 61
    invoke-virtual {v0, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;

    iput-object v0, p0, Lhhx;->b:Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;

    .line 63
    new-instance v0, Lop;

    invoke-direct {v0, v3, v3}, Lop;-><init>(II)V

    .line 66
    iget-object v2, p0, Lhhx;->b:Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;

    invoke-virtual {v1, v2, v0}, Loo;->a(Landroid/view/View;Lop;)V

    .line 67
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Loo;->e(Z)V

    .line 69
    if-eqz p1, :cond_0

    .line 70
    const-string v0, "AclPickerActionBarHandler.search_mode_enabled"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    const-string v0, "AclPickerActionBarHandler.search_mode_query"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 72
    iget-object v1, p0, Lhhx;->b:Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->a(Ljava/lang/CharSequence;)V

    .line 76
    :cond_0
    iget-object v0, p0, Lhhx;->c:Lhzl;

    instance-of v0, v0, Lhzp;

    if-eqz v0, :cond_1

    .line 77
    invoke-direct {p0}, Lhhx;->c()Z

    move-result v0

    invoke-direct {p0, v0}, Lhhx;->c(Z)V

    .line 78
    iget-object v0, p0, Lhhx;->c:Lhzl;

    check-cast v0, Lhzp;

    invoke-interface {v0, p0}, Lhzp;->a(Lhzn;)V

    .line 81
    :cond_1
    iget-object v0, p0, Lhhx;->b:Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;

    iget-boolean v1, p0, Lhhx;->d:Z

    invoke-virtual {v0, v1, v4}, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->a(ZZ)V

    .line 83
    iget v0, p0, Lhhx;->f:I

    if-eqz v0, :cond_2

    .line 84
    iget-object v0, p0, Lhhx;->b:Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;

    iget v1, p0, Lhhx;->f:I

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->a(I)V

    .line 86
    :cond_2
    return-void
.end method
