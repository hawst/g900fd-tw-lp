.class public final Lmnn;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmms;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5887
    invoke-direct {p0}, Loxq;-><init>()V

    .line 5890
    const/4 v0, 0x0

    iput-object v0, p0, Lmnn;->a:Lmms;

    .line 5887
    return-void
.end method


# virtual methods
.method public a()I
    .locals 2

    .prologue
    .line 5904
    const/4 v0, 0x0

    .line 5905
    iget-object v1, p0, Lmnn;->a:Lmms;

    if-eqz v1, :cond_0

    .line 5906
    const/4 v0, 0x1

    iget-object v1, p0, Lmnn;->a:Lmms;

    .line 5907
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 5909
    :cond_0
    iget-object v1, p0, Lmnn;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5910
    iput v0, p0, Lmnn;->ai:I

    .line 5911
    return v0
.end method

.method public a(Loxn;)Lmnn;
    .locals 2

    .prologue
    .line 5919
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 5920
    sparse-switch v0, :sswitch_data_0

    .line 5924
    iget-object v1, p0, Lmnn;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 5925
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmnn;->ah:Ljava/util/List;

    .line 5928
    :cond_1
    iget-object v1, p0, Lmnn;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5930
    :sswitch_0
    return-object p0

    .line 5935
    :sswitch_1
    iget-object v0, p0, Lmnn;->a:Lmms;

    if-nez v0, :cond_2

    .line 5936
    new-instance v0, Lmms;

    invoke-direct {v0}, Lmms;-><init>()V

    iput-object v0, p0, Lmnn;->a:Lmms;

    .line 5938
    :cond_2
    iget-object v0, p0, Lmnn;->a:Lmms;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 5920
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 5895
    iget-object v0, p0, Lmnn;->a:Lmms;

    if-eqz v0, :cond_0

    .line 5896
    const/4 v0, 0x1

    iget-object v1, p0, Lmnn;->a:Lmms;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 5898
    :cond_0
    iget-object v0, p0, Lmnn;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 5900
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5883
    invoke-virtual {p0, p1}, Lmnn;->a(Loxn;)Lmnn;

    move-result-object v0

    return-object v0
.end method
