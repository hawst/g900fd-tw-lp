.class public final Ljql;
.super Lhny;
.source "PG"


# instance fields
.field private final a:Lipj;

.field private final b:Liof;

.field private final c:Linz;

.field private final d:Landroid/content/Context;

.field private e:I

.field private f:Z

.field private h:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;IZ[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 42
    const-string v0, "SetContactsSyncSettingsTask"

    invoke-direct {p0, p1, v0}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 43
    const-class v0, Liof;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liof;

    iput-object v0, p0, Ljql;->b:Liof;

    .line 44
    const-class v0, Linz;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Linz;

    iput-object v0, p0, Ljql;->c:Linz;

    .line 45
    iget-object v0, p0, Ljql;->b:Liof;

    invoke-interface {v0}, Liof;->a()Lipj;

    move-result-object v0

    iput-object v0, p0, Ljql;->a:Lipj;

    .line 46
    iput-object p1, p0, Ljql;->d:Landroid/content/Context;

    .line 47
    iput p2, p0, Ljql;->e:I

    .line 48
    iput-boolean p3, p0, Ljql;->f:Z

    .line 49
    iput-object p4, p0, Ljql;->h:[Ljava/lang/String;

    .line 50
    return-void
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/16 v4, 0xc8

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 54
    .line 55
    iget-object v0, p0, Ljql;->d:Landroid/content/Context;

    const-class v3, Lhei;

    invoke-static {v0, v3}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iget v3, p0, Ljql;->e:I

    invoke-interface {v0, v3}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 56
    const-string v3, "account_name"

    invoke-interface {v0, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 59
    iget-object v0, p0, Ljql;->a:Lipj;

    const-wide/16 v8, 0xa

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v8, v9, v3}, Lipj;->a(JLjava/util/concurrent/TimeUnit;)Liiw;

    move-result-object v0

    .line 61
    invoke-interface {v0}, Liiw;->a()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 62
    new-instance v0, Liot;

    invoke-direct {v0}, Liot;-><init>()V

    .line 63
    iget-object v3, p0, Ljql;->b:Liof;

    iget-object v5, p0, Ljql;->a:Lipj;

    .line 64
    invoke-interface {v3, v5, v0}, Liof;->a(Lipj;Liot;)Lijq;

    move-result-object v0

    invoke-interface {v0}, Lijq;->a()Lijs;

    move-result-object v0

    check-cast v0, Liou;

    .line 65
    invoke-interface {v0}, Liou;->a()Lika;

    move-result-object v3

    invoke-interface {v3}, Lika;->b()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 66
    iget-boolean v7, p0, Ljql;->f:Z

    invoke-interface {v0}, Liou;->b()Liqg;

    move-result-object v8

    move v5, v2

    move v3, v1

    :goto_0
    if-eqz v8, :cond_1

    invoke-interface {v8}, Liqg;->b()I

    move-result v0

    if-ge v5, v0, :cond_1

    invoke-interface {v8, v5}, Liqg;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liqf;

    invoke-interface {v0}, Liqf;->a()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    invoke-interface {v0}, Liqf;->b()Z

    move-result v0

    if-eq v7, v0, :cond_0

    move v0, v1

    :goto_1
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    move v3, v0

    goto :goto_0

    :cond_0
    move v0, v2

    goto :goto_1

    :cond_1
    if-eqz v3, :cond_5

    .line 67
    iget-object v0, p0, Ljql;->c:Linz;

    iget-object v3, p0, Ljql;->a:Lipj;

    iget-boolean v5, p0, Ljql;->f:Z

    iget-object v7, p0, Ljql;->h:[Ljava/lang/String;

    invoke-interface {v0, v3, v6, v5, v7}, Linz;->a(Lipj;Ljava/lang/String;Z[Ljava/lang/String;)Lijq;

    move-result-object v0

    .line 68
    invoke-interface {v0}, Lijq;->a()Lijs;

    move-result-object v3

    .line 69
    invoke-interface {v3}, Lijs;->a()Lika;

    move-result-object v0

    invoke-interface {v0}, Lika;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v4

    .line 71
    :goto_2
    iget-object v4, p0, Ljql;->d:Landroid/content/Context;

    const/16 v5, 0x12

    invoke-static {v4, v6, v5, v3}, Ljpe;->a(Landroid/content/Context;Ljava/lang/String;ILijs;)V

    .line 82
    :goto_3
    iget-object v3, p0, Ljql;->a:Lipj;

    invoke-interface {v3}, Lipj;->d()V

    .line 84
    invoke-static {}, Ljpe;->a()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 85
    const-string v3, "accountId: %s. isSyncEnabled: %s. statusCode: %s"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    iget v5, p0, Ljql;->e:I

    .line 87
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    iget-boolean v2, p0, Ljql;->f:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v4, v1

    const/4 v1, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v1

    .line 86
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 85
    invoke-static {}, Ljpe;->b()V

    .line 90
    :cond_2
    new-instance v1, Lhoz;

    invoke-direct {v1, v0, v10, v10}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    return-object v1

    .line 70
    :cond_3
    invoke-interface {v3}, Lijs;->a()Lika;

    move-result-object v0

    invoke-interface {v0}, Lika;->c()I

    move-result v0

    goto :goto_2

    .line 75
    :cond_4
    invoke-interface {v0}, Liou;->a()Lika;

    move-result-object v3

    invoke-interface {v3}, Lika;->c()I

    move-result v4

    .line 76
    iget-object v3, p0, Ljql;->d:Landroid/content/Context;

    invoke-static {v3, v6, v1, v0}, Ljpe;->a(Landroid/content/Context;Ljava/lang/String;ILijs;)V

    :cond_5
    move v0, v4

    .line 79
    goto :goto_3

    .line 80
    :cond_6
    invoke-interface {v0}, Liiw;->b()I

    move-result v0

    goto :goto_3

    :cond_7
    move v0, v3

    goto :goto_1
.end method
