.class public final Lgen_binder/root/RootModule$Generated;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llnt;


# annotations
.annotation build Lcom/google/android/apps/common/proguard/UsedByReflection;
.end annotation


# instance fields
.field private a:Lhnx;

.field private b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 84
    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    .line 85
    const-string v1, "com.google.android.libraries.social.async.AsyncModule"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 86
    return-object v0
.end method

.method public a(Landroid/content/Context;Ljava/lang/Class;Llnh;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<*>;",
            "Llnh;",
            ")V"
        }
    .end annotation

    .prologue
    .line 36
    iget-object v0, p0, Lgen_binder/root/RootModule$Generated;->b:Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 37
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lgen_binder/root/RootModule$Generated;->b:Ljava/util/HashMap;

    iget-object v0, p0, Lgen_binder/root/RootModule$Generated;->b:Ljava/util/HashMap;

    const-class v1, Llop;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lgen_binder/root/RootModule$Generated;->b:Ljava/util/HashMap;

    const-class v1, Lhoa;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lgen_binder/root/RootModule$Generated;->b:Ljava/util/HashMap;

    const-class v1, Lloc;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lgen_binder/root/RootModule$Generated;->b:Ljava/util/HashMap;

    const-class v1, Lhor;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lgen_binder/root/RootModule$Generated;->b:Ljava/util/HashMap;

    const-class v1, Llow;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    :cond_0
    iget-object v0, p0, Lgen_binder/root/RootModule$Generated;->b:Ljava/util/HashMap;

    invoke-virtual {p2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 40
    if-nez v0, :cond_1

    .line 80
    :goto_0
    return-void

    .line 43
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 45
    :pswitch_0
    iget-object v0, p0, Lgen_binder/root/RootModule$Generated;->a:Lhnx;

    if-nez v0, :cond_2

    .line 46
    new-instance v0, Lhnx;

    invoke-direct {v0}, Lhnx;-><init>()V

    iput-object v0, p0, Lgen_binder/root/RootModule$Generated;->a:Lhnx;

    .line 48
    :cond_2
    const-class v0, Llop;

    iget-object v1, p0, Lgen_binder/root/RootModule$Generated;->a:Lhnx;

    .line 49
    invoke-virtual {v1}, Lhnx;->c()[Llop;

    move-result-object v1

    .line 48
    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    goto :goto_0

    .line 52
    :pswitch_1
    iget-object v0, p0, Lgen_binder/root/RootModule$Generated;->a:Lhnx;

    if-nez v0, :cond_3

    .line 53
    new-instance v0, Lhnx;

    invoke-direct {v0}, Lhnx;-><init>()V

    iput-object v0, p0, Lgen_binder/root/RootModule$Generated;->a:Lhnx;

    .line 55
    :cond_3
    const-class v0, Lhoa;

    iget-object v1, p0, Lgen_binder/root/RootModule$Generated;->a:Lhnx;

    .line 56
    invoke-virtual {v1, p1}, Lhnx;->b(Landroid/content/Context;)Lhoa;

    move-result-object v1

    .line 55
    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto :goto_0

    .line 59
    :pswitch_2
    iget-object v0, p0, Lgen_binder/root/RootModule$Generated;->a:Lhnx;

    if-nez v0, :cond_4

    .line 60
    new-instance v0, Lhnx;

    invoke-direct {v0}, Lhnx;-><init>()V

    iput-object v0, p0, Lgen_binder/root/RootModule$Generated;->a:Lhnx;

    .line 62
    :cond_4
    const-class v0, Lloc;

    iget-object v1, p0, Lgen_binder/root/RootModule$Generated;->a:Lhnx;

    .line 63
    invoke-virtual {v1}, Lhnx;->b()[Lloc;

    move-result-object v1

    .line 62
    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    goto :goto_0

    .line 66
    :pswitch_3
    iget-object v0, p0, Lgen_binder/root/RootModule$Generated;->a:Lhnx;

    if-nez v0, :cond_5

    .line 67
    new-instance v0, Lhnx;

    invoke-direct {v0}, Lhnx;-><init>()V

    iput-object v0, p0, Lgen_binder/root/RootModule$Generated;->a:Lhnx;

    .line 69
    :cond_5
    const-class v0, Lhor;

    iget-object v1, p0, Lgen_binder/root/RootModule$Generated;->a:Lhnx;

    .line 70
    invoke-virtual {v1, p1}, Lhnx;->a(Landroid/content/Context;)Lhor;

    move-result-object v1

    .line 69
    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto :goto_0

    .line 73
    :pswitch_4
    iget-object v0, p0, Lgen_binder/root/RootModule$Generated;->a:Lhnx;

    if-nez v0, :cond_6

    .line 74
    new-instance v0, Lhnx;

    invoke-direct {v0}, Lhnx;-><init>()V

    iput-object v0, p0, Lgen_binder/root/RootModule$Generated;->a:Lhnx;

    .line 76
    :cond_6
    const-class v0, Llow;

    iget-object v1, p0, Lgen_binder/root/RootModule$Generated;->a:Lhnx;

    .line 77
    invoke-virtual {v1}, Lhnx;->a()[Llow;

    move-result-object v1

    .line 76
    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    goto :goto_0

    .line 43
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
