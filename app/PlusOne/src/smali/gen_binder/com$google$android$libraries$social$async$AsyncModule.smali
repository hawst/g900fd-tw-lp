.class public final Lgen_binder/com$google$android$libraries$social$async$AsyncModule;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llnq;


# annotations
.annotation build Lcom/google/android/apps/common/proguard/UsedByReflection;
.end annotation


# instance fields
.field private a:Lhnx;

.field private b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Ljava/lang/Class;Llnh;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<*>;",
            "Llnh;",
            ")V"
        }
    .end annotation

    .prologue
    .line 34
    iget-object v0, p0, Lgen_binder/com$google$android$libraries$social$async$AsyncModule;->b:Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 35
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lgen_binder/com$google$android$libraries$social$async$AsyncModule;->b:Ljava/util/HashMap;

    iget-object v0, p0, Lgen_binder/com$google$android$libraries$social$async$AsyncModule;->b:Ljava/util/HashMap;

    const-class v1, Llop;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lgen_binder/com$google$android$libraries$social$async$AsyncModule;->b:Ljava/util/HashMap;

    const-class v1, Llow;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lgen_binder/com$google$android$libraries$social$async$AsyncModule;->b:Ljava/util/HashMap;

    const-class v1, Lloc;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lgen_binder/com$google$android$libraries$social$async$AsyncModule;->b:Ljava/util/HashMap;

    const-class v1, Lhoa;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lgen_binder/com$google$android$libraries$social$async$AsyncModule;->b:Ljava/util/HashMap;

    const-class v1, Lhor;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    :cond_0
    iget-object v0, p0, Lgen_binder/com$google$android$libraries$social$async$AsyncModule;->b:Ljava/util/HashMap;

    invoke-virtual {p2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 38
    if-nez v0, :cond_1

    .line 78
    :goto_0
    return-void

    .line 41
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 43
    :pswitch_0
    iget-object v0, p0, Lgen_binder/com$google$android$libraries$social$async$AsyncModule;->a:Lhnx;

    if-nez v0, :cond_2

    .line 44
    new-instance v0, Lhnx;

    invoke-direct {v0}, Lhnx;-><init>()V

    iput-object v0, p0, Lgen_binder/com$google$android$libraries$social$async$AsyncModule;->a:Lhnx;

    .line 46
    :cond_2
    const-class v0, Llop;

    iget-object v1, p0, Lgen_binder/com$google$android$libraries$social$async$AsyncModule;->a:Lhnx;

    .line 47
    invoke-virtual {v1}, Lhnx;->c()[Llop;

    move-result-object v1

    .line 46
    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    goto :goto_0

    .line 50
    :pswitch_1
    iget-object v0, p0, Lgen_binder/com$google$android$libraries$social$async$AsyncModule;->a:Lhnx;

    if-nez v0, :cond_3

    .line 51
    new-instance v0, Lhnx;

    invoke-direct {v0}, Lhnx;-><init>()V

    iput-object v0, p0, Lgen_binder/com$google$android$libraries$social$async$AsyncModule;->a:Lhnx;

    .line 53
    :cond_3
    const-class v0, Llow;

    iget-object v1, p0, Lgen_binder/com$google$android$libraries$social$async$AsyncModule;->a:Lhnx;

    .line 54
    invoke-virtual {v1}, Lhnx;->a()[Llow;

    move-result-object v1

    .line 53
    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    goto :goto_0

    .line 57
    :pswitch_2
    iget-object v0, p0, Lgen_binder/com$google$android$libraries$social$async$AsyncModule;->a:Lhnx;

    if-nez v0, :cond_4

    .line 58
    new-instance v0, Lhnx;

    invoke-direct {v0}, Lhnx;-><init>()V

    iput-object v0, p0, Lgen_binder/com$google$android$libraries$social$async$AsyncModule;->a:Lhnx;

    .line 60
    :cond_4
    const-class v0, Lloc;

    iget-object v1, p0, Lgen_binder/com$google$android$libraries$social$async$AsyncModule;->a:Lhnx;

    .line 61
    invoke-virtual {v1}, Lhnx;->b()[Lloc;

    move-result-object v1

    .line 60
    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    goto :goto_0

    .line 64
    :pswitch_3
    iget-object v0, p0, Lgen_binder/com$google$android$libraries$social$async$AsyncModule;->a:Lhnx;

    if-nez v0, :cond_5

    .line 65
    new-instance v0, Lhnx;

    invoke-direct {v0}, Lhnx;-><init>()V

    iput-object v0, p0, Lgen_binder/com$google$android$libraries$social$async$AsyncModule;->a:Lhnx;

    .line 67
    :cond_5
    const-class v0, Lhoa;

    iget-object v1, p0, Lgen_binder/com$google$android$libraries$social$async$AsyncModule;->a:Lhnx;

    .line 68
    invoke-virtual {v1, p1}, Lhnx;->b(Landroid/content/Context;)Lhoa;

    move-result-object v1

    .line 67
    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto :goto_0

    .line 71
    :pswitch_4
    iget-object v0, p0, Lgen_binder/com$google$android$libraries$social$async$AsyncModule;->a:Lhnx;

    if-nez v0, :cond_6

    .line 72
    new-instance v0, Lhnx;

    invoke-direct {v0}, Lhnx;-><init>()V

    iput-object v0, p0, Lgen_binder/com$google$android$libraries$social$async$AsyncModule;->a:Lhnx;

    .line 74
    :cond_6
    const-class v0, Lhor;

    iget-object v1, p0, Lgen_binder/com$google$android$libraries$social$async$AsyncModule;->a:Lhnx;

    .line 75
    invoke-virtual {v1, p1}, Lhnx;->a(Landroid/content/Context;)Lhor;

    move-result-object v1

    .line 74
    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto :goto_0

    .line 41
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
