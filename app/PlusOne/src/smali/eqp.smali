.class final Leqp;
.super Lhye;
.source "PG"


# instance fields
.field private b:Z

.field private final c:Ldp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">.dp;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 491
    invoke-direct {p0, p1, p2}, Lhye;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    .line 488
    new-instance v0, Ldp;

    invoke-direct {v0, p0}, Ldp;-><init>(Ldo;)V

    iput-object v0, p0, Leqp;->c:Ldp;

    .line 492
    return-void
.end method


# virtual methods
.method public C()Landroid/database/Cursor;
    .locals 22

    .prologue
    .line 514
    invoke-virtual/range {p0 .. p0}, Leqp;->n()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 515
    invoke-virtual/range {p0 .. p0}, Leqp;->n()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lhqv;->a(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Leqs;->a:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "media_url DESC, upload_reason ASC"

    .line 514
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 517
    new-instance v3, Lhym;

    sget-object v0, Leqs;->a:[Ljava/lang/String;

    invoke-direct {v3, v0}, Lhym;-><init>([Ljava/lang/String;)V

    .line 519
    sget-object v0, Leqs;->a:[Ljava/lang/String;

    const/16 v0, 0xa

    new-array v4, v0, [Ljava/lang/Object;

    .line 521
    :goto_0
    if-eqz v2, :cond_4

    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 522
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 523
    const/4 v0, 0x2

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 524
    const/4 v0, 0x1

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 525
    const/4 v0, 0x3

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 526
    const/4 v0, 0x4

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 527
    const/4 v0, 0x6

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    .line 528
    const/4 v0, 0x7

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 529
    const/16 v0, 0x8

    .line 530
    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    .line 531
    const/16 v0, 0x9

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    .line 533
    const/4 v0, 0x0

    aget-object v0, v4, v0

    if-eqz v0, :cond_1

    .line 534
    const/4 v0, 0x2

    aget-object v0, v4, v0

    check-cast v0, Ljava/lang/String;

    .line 536
    const/4 v1, 0x6

    aget-object v1, v4, v1

    check-cast v1, Ljava/lang/Integer;

    .line 537
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-nez v1, :cond_3

    const/4 v1, 0x1

    .line 539
    :goto_1
    const-wide/16 v20, -0x1

    cmp-long v13, v8, v20

    if-eqz v13, :cond_0

    invoke-static {v5, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 540
    if-nez v1, :cond_1

    .line 543
    :cond_0
    invoke-virtual {v3, v4}, Lhym;->a([Ljava/lang/Object;)V

    .line 548
    const/4 v0, 0x0

    invoke-static {v4, v0}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 551
    :cond_1
    const/4 v0, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v4, v0

    .line 552
    const/4 v0, 0x1

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v4, v0

    .line 553
    const/4 v0, 0x2

    aput-object v5, v4, v0

    .line 554
    const/4 v0, 0x3

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v0

    .line 555
    const/4 v0, 0x4

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v0

    .line 556
    const/4 v0, 0x6

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v0

    .line 557
    const/4 v0, 0x7

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v4, v0

    .line 558
    const/16 v0, 0x8

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v4, v0

    .line 559
    const/16 v0, 0x9

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v4, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    .line 565
    :catchall_0
    move-exception v0

    if-eqz v2, :cond_2

    .line 566
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    .line 537
    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    .line 561
    :cond_4
    const/4 v0, 0x0

    :try_start_1
    aget-object v0, v4, v0

    if-eqz v0, :cond_5

    .line 562
    invoke-virtual {v3, v4}, Lhym;->a([Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 565
    :cond_5
    if-eqz v2, :cond_6

    .line 566
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 569
    :cond_6
    return-object v3
.end method

.method protected g()V
    .locals 4

    .prologue
    .line 496
    invoke-super {p0}, Lhye;->g()V

    .line 497
    iget-boolean v0, p0, Leqp;->b:Z

    if-nez v0, :cond_0

    .line 498
    invoke-virtual {p0}, Leqp;->n()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 499
    invoke-virtual {p0}, Leqp;->n()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lhqv;->a(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Leqp;->c:Ldp;

    .line 498
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 500
    const/4 v0, 0x1

    iput-boolean v0, p0, Leqp;->b:Z

    .line 502
    :cond_0
    return-void
.end method

.method protected w()V
    .locals 2

    .prologue
    .line 506
    iget-boolean v0, p0, Leqp;->b:Z

    if-eqz v0, :cond_0

    .line 507
    invoke-virtual {p0}, Leqp;->n()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Leqp;->c:Ldp;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 508
    const/4 v0, 0x0

    iput-boolean v0, p0, Leqp;->b:Z

    .line 510
    :cond_0
    return-void
.end method
