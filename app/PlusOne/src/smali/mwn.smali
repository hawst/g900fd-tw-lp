.class public final Lmwn;
.super Loxq;
.source "PG"


# instance fields
.field private a:[Lmwo;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9131
    invoke-direct {p0}, Loxq;-><init>()V

    .line 9134
    sget-object v0, Lmwo;->a:[Lmwo;

    iput-object v0, p0, Lmwn;->a:[Lmwo;

    .line 9131
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 9152
    .line 9153
    iget-object v1, p0, Lmwn;->a:[Lmwo;

    if-eqz v1, :cond_1

    .line 9154
    iget-object v2, p0, Lmwn;->a:[Lmwo;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 9155
    if-eqz v4, :cond_0

    .line 9156
    const/4 v5, 0x1

    .line 9157
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 9154
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 9161
    :cond_1
    iget-object v1, p0, Lmwn;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9162
    iput v0, p0, Lmwn;->ai:I

    .line 9163
    return v0
.end method

.method public a(Loxn;)Lmwn;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 9171
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 9172
    sparse-switch v0, :sswitch_data_0

    .line 9176
    iget-object v2, p0, Lmwn;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 9177
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmwn;->ah:Ljava/util/List;

    .line 9180
    :cond_1
    iget-object v2, p0, Lmwn;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 9182
    :sswitch_0
    return-object p0

    .line 9187
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 9188
    iget-object v0, p0, Lmwn;->a:[Lmwo;

    if-nez v0, :cond_3

    move v0, v1

    .line 9189
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lmwo;

    .line 9190
    iget-object v3, p0, Lmwn;->a:[Lmwo;

    if-eqz v3, :cond_2

    .line 9191
    iget-object v3, p0, Lmwn;->a:[Lmwo;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 9193
    :cond_2
    iput-object v2, p0, Lmwn;->a:[Lmwo;

    .line 9194
    :goto_2
    iget-object v2, p0, Lmwn;->a:[Lmwo;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 9195
    iget-object v2, p0, Lmwn;->a:[Lmwo;

    new-instance v3, Lmwo;

    invoke-direct {v3}, Lmwo;-><init>()V

    aput-object v3, v2, v0

    .line 9196
    iget-object v2, p0, Lmwn;->a:[Lmwo;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 9197
    invoke-virtual {p1}, Loxn;->a()I

    .line 9194
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 9188
    :cond_3
    iget-object v0, p0, Lmwn;->a:[Lmwo;

    array-length v0, v0

    goto :goto_1

    .line 9200
    :cond_4
    iget-object v2, p0, Lmwn;->a:[Lmwo;

    new-instance v3, Lmwo;

    invoke-direct {v3}, Lmwo;-><init>()V

    aput-object v3, v2, v0

    .line 9201
    iget-object v2, p0, Lmwn;->a:[Lmwo;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 9172
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 9139
    iget-object v0, p0, Lmwn;->a:[Lmwo;

    if-eqz v0, :cond_1

    .line 9140
    iget-object v1, p0, Lmwn;->a:[Lmwo;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 9141
    if-eqz v3, :cond_0

    .line 9142
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 9140
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 9146
    :cond_1
    iget-object v0, p0, Lmwn;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 9148
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 9127
    invoke-virtual {p0, p1}, Lmwn;->a(Loxn;)Lmwn;

    move-result-object v0

    return-object v0
.end method
