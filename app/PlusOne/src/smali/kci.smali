.class public final Lkci;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llnx;
.implements Llov;
.implements Llqz;
.implements Llrc;
.implements Llrd;
.implements Llre;
.implements Llrg;


# instance fields
.field private a:Landroid/app/Activity;

.field private b:Lu;

.field private c:Z

.field private d:Z

.field private e:Z

.field private final f:I

.field private final g:Lkch;

.field private h:Lkcj;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Llqr;ILkch;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p1, p0, Lkci;->a:Landroid/app/Activity;

    .line 54
    iput p3, p0, Lkci;->f:I

    .line 55
    iput-object p4, p0, Lkci;->g:Lkch;

    .line 56
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 57
    return-void
.end method

.method public constructor <init>(Lu;Llqr;ILkch;)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput-object p1, p0, Lkci;->b:Lu;

    .line 64
    iput p3, p0, Lkci;->f:I

    .line 65
    iput-object p4, p0, Lkci;->g:Lkch;

    .line 66
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 67
    return-void
.end method

.method private g()Z
    .locals 1

    .prologue
    .line 164
    iget-boolean v0, p0, Lkci;->d:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lkci;->e:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 138
    iget-object v0, p0, Lkci;->b:Lu;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lkci;->b:Lu;

    .line 139
    invoke-virtual {v0}, Lu;->x()Landroid/view/View;

    move-result-object v0

    iget v1, p0, Lkci;->f:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 141
    :goto_0
    check-cast v0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;

    .line 142
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    :goto_1
    iput-boolean v1, p0, Lkci;->e:Z

    .line 143
    iget-boolean v1, p0, Lkci;->e:Z

    if-eqz v1, :cond_0

    .line 144
    iget-object v1, p0, Lkci;->g:Lkch;

    iget-object v2, p0, Lkci;->h:Lkcj;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;->a(Lkch;Lkcp;)V

    .line 146
    :cond_0
    return-void

    .line 139
    :cond_1
    iget-object v0, p0, Lkci;->a:Landroid/app/Activity;

    iget v1, p0, Lkci;->f:I

    .line 140
    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 142
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public a(Landroid/content/Context;Llnh;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 121
    const-class v0, Llot;

    invoke-virtual {p2, v0}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llot;

    .line 122
    if-eqz v0, :cond_0

    .line 123
    invoke-virtual {v0, p0}, Llot;->a(Llov;)Llov;

    .line 126
    :cond_0
    const-class v0, Lkcj;

    invoke-virtual {p2, v0}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkcj;

    iput-object v0, p0, Lkci;->h:Lkcj;

    .line 127
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 131
    if-eqz p1, :cond_0

    .line 132
    const-string v0, "PullToRefresh.Syncing"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lkci;->c:Z

    .line 134
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 160
    invoke-virtual {p0, p1}, Lkci;->b(Z)V

    .line 161
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 150
    invoke-virtual {p0}, Lkci;->e()V

    .line 151
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 155
    const-string v0, "PullToRefresh.Syncing"

    iget-boolean v1, p0, Lkci;->c:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 156
    return-void
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 111
    iput-boolean p1, p0, Lkci;->d:Z

    .line 112
    invoke-virtual {p0}, Lkci;->e()V

    .line 113
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 73
    const/4 v0, 0x1

    iput-boolean v0, p0, Lkci;->c:Z

    .line 74
    invoke-direct {p0}, Lkci;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75
    iget-object v0, p0, Lkci;->h:Lkcj;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lkcj;->c(I)V

    .line 77
    :cond_0
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 83
    const/4 v0, 0x1

    iput-boolean v0, p0, Lkci;->c:Z

    .line 84
    invoke-direct {p0}, Lkci;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lkci;->h:Lkcj;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lkcj;->c(I)V

    .line 87
    :cond_0
    return-void
.end method

.method public e()V
    .locals 4

    .prologue
    .line 93
    iget-object v0, p0, Lkci;->g:Lkch;

    invoke-interface {v0}, Lkch;->aQ_()Z

    move-result v0

    .line 94
    invoke-direct {p0}, Lkci;->g()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 95
    iget-object v1, p0, Lkci;->h:Lkcj;

    invoke-virtual {v1}, Lkcj;->d()I

    move-result v1

    .line 96
    if-eqz v0, :cond_0

    if-nez v1, :cond_0

    .line 97
    iget-object v2, p0, Lkci;->h:Lkcj;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lkcj;->c(I)V

    .line 99
    :cond_0
    if-nez v0, :cond_1

    if-eqz v1, :cond_1

    .line 100
    iget-object v1, p0, Lkci;->h:Lkcj;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lkcj;->c(I)V

    .line 103
    :cond_1
    iput-boolean v0, p0, Lkci;->c:Z

    .line 104
    return-void
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 116
    iget-boolean v0, p0, Lkci;->c:Z

    return v0
.end method
