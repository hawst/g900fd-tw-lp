.class public final Llyb;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Llyb;


# instance fields
.field private b:Llxp;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4525
    const/4 v0, 0x0

    new-array v0, v0, [Llyb;

    sput-object v0, Llyb;->a:[Llyb;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 4526
    invoke-direct {p0}, Loxq;-><init>()V

    .line 4529
    const/4 v0, 0x0

    iput-object v0, p0, Llyb;->b:Llxp;

    .line 4526
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 4553
    const/4 v0, 0x0

    .line 4554
    iget-object v1, p0, Llyb;->b:Llxp;

    if-eqz v1, :cond_0

    .line 4555
    const/4 v0, 0x1

    iget-object v1, p0, Llyb;->b:Llxp;

    .line 4556
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 4558
    :cond_0
    iget-object v1, p0, Llyb;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 4559
    const/4 v1, 0x2

    iget-object v2, p0, Llyb;->c:Ljava/lang/String;

    .line 4560
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4562
    :cond_1
    iget-object v1, p0, Llyb;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 4563
    const/4 v1, 0x3

    iget-object v2, p0, Llyb;->d:Ljava/lang/String;

    .line 4564
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4566
    :cond_2
    iget-object v1, p0, Llyb;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4567
    iput v0, p0, Llyb;->ai:I

    .line 4568
    return v0
.end method

.method public a(Loxn;)Llyb;
    .locals 2

    .prologue
    .line 4576
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 4577
    sparse-switch v0, :sswitch_data_0

    .line 4581
    iget-object v1, p0, Llyb;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 4582
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llyb;->ah:Ljava/util/List;

    .line 4585
    :cond_1
    iget-object v1, p0, Llyb;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4587
    :sswitch_0
    return-object p0

    .line 4592
    :sswitch_1
    iget-object v0, p0, Llyb;->b:Llxp;

    if-nez v0, :cond_2

    .line 4593
    new-instance v0, Llxp;

    invoke-direct {v0}, Llxp;-><init>()V

    iput-object v0, p0, Llyb;->b:Llxp;

    .line 4595
    :cond_2
    iget-object v0, p0, Llyb;->b:Llxp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 4599
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llyb;->c:Ljava/lang/String;

    goto :goto_0

    .line 4603
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llyb;->d:Ljava/lang/String;

    goto :goto_0

    .line 4577
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 4538
    iget-object v0, p0, Llyb;->b:Llxp;

    if-eqz v0, :cond_0

    .line 4539
    const/4 v0, 0x1

    iget-object v1, p0, Llyb;->b:Llxp;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 4541
    :cond_0
    iget-object v0, p0, Llyb;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 4542
    const/4 v0, 0x2

    iget-object v1, p0, Llyb;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 4544
    :cond_1
    iget-object v0, p0, Llyb;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 4545
    const/4 v0, 0x3

    iget-object v1, p0, Llyb;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 4547
    :cond_2
    iget-object v0, p0, Llyb;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 4549
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 4522
    invoke-virtual {p0, p1}, Llyb;->a(Loxn;)Llyb;

    move-result-object v0

    return-object v0
.end method
