.class public final Lebd;
.super Lhxz;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lhxz",
        "<",
        "Lojw;",
        ">;"
    }
.end annotation


# instance fields
.field final b:I

.field final c:I

.field private final d:Lebh;

.field private final e:Lebi;

.field private final f:J

.field private g:Lojw;

.field private h:Ldp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldo",
            "<",
            "Lojw;",
            ">.dp;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;IIJ)V
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 82
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-wide v4, p4

    move-object v7, v6

    invoke-direct/range {v0 .. v7}, Lebd;-><init>(Landroid/content/Context;IIJLebi;Lebh;)V

    .line 83
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;IIJLebi;Lebh;)V
    .locals 2

    .prologue
    .line 88
    invoke-direct {p0, p1}, Lhxz;-><init>(Landroid/content/Context;)V

    .line 78
    new-instance v0, Ldp;

    invoke-direct {v0, p0}, Ldp;-><init>(Ldo;)V

    iput-object v0, p0, Lebd;->h:Ldp;

    .line 89
    iput p2, p0, Lebd;->b:I

    .line 90
    iput p3, p0, Lebd;->c:I

    .line 91
    iput-wide p4, p0, Lebd;->f:J

    .line 92
    if-eqz p6, :cond_0

    :goto_0
    iput-object p6, p0, Lebd;->e:Lebi;

    .line 95
    if-eqz p7, :cond_1

    :goto_1
    iput-object p7, p0, Lebd;->d:Lebh;

    .line 98
    return-void

    .line 92
    :cond_0
    new-instance p6, Lebg;

    invoke-direct {p6, p0}, Lebg;-><init>(Lebd;)V

    goto :goto_0

    .line 95
    :cond_1
    new-instance p7, Lebf;

    invoke-direct {p7, p0}, Lebf;-><init>(Lebd;)V

    goto :goto_1
.end method


# virtual methods
.method protected D_()Z
    .locals 4

    .prologue
    .line 218
    invoke-virtual {p0}, Lebd;->n()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->c:Landroid/net/Uri;

    const/4 v2, 0x0

    iget-object v3, p0, Lebd;->h:Ldp;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 220
    const/4 v0, 0x1

    return v0
.end method

.method protected i()V
    .locals 1

    .prologue
    .line 231
    invoke-super {p0}, Lhxz;->i()V

    .line 232
    const/4 v0, 0x0

    iput-object v0, p0, Lebd;->g:Lojw;

    .line 233
    return-void
.end method

.method public synthetic j()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0}, Lebd;->l()Lojw;

    move-result-object v0

    return-object v0
.end method

.method protected k()Z
    .locals 2

    .prologue
    .line 225
    invoke-virtual {p0}, Lebd;->n()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lebd;->h:Ldp;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 226
    const/4 v0, 0x1

    return v0
.end method

.method public l()Lojw;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 102
    .line 106
    new-instance v3, Landroid/os/ConditionVariable;

    invoke-direct {v3}, Landroid/os/ConditionVariable;-><init>()V

    .line 108
    iget-object v0, p0, Lebd;->g:Lojw;

    if-nez v0, :cond_3

    .line 110
    invoke-virtual {p0}, Lebd;->n()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Ldwn;->a(Landroid/content/Context;)Ldwn;

    move-result-object v0

    iget v2, p0, Lebd;->c:I

    invoke-static {v2}, Ldwn;->a(I)Ljava/lang/String;

    move-result-object v2

    iget-wide v4, p0, Lebd;->f:J

    invoke-virtual {v0, v2, v4, v5}, Ldwn;->a(Ljava/lang/String;J)[B

    move-result-object v0

    .line 111
    if-eqz v0, :cond_1

    .line 113
    :try_start_0
    new-instance v2, Lojw;

    invoke-direct {v2}, Lojw;-><init>()V

    invoke-static {v2, v0}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lojw;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v2, v0

    .line 115
    :goto_0
    if-nez v2, :cond_2

    .line 123
    iget-object v0, p0, Lebd;->e:Lebi;

    invoke-interface {v0}, Lebi;->a()Ldjx;

    move-result-object v0

    new-instance v1, Ljava/lang/Thread;

    new-instance v4, Lebe;

    invoke-direct {v4, v0, v3}, Lebe;-><init>(Ldjx;Landroid/os/ConditionVariable;)V

    const-string v5, "GetCelebritySuggestionsLoader"

    invoke-direct {v1, v4, v5}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    move-object v1, v0

    move-object v0, v2

    .line 139
    :goto_1
    iget-object v2, p0, Lebd;->d:Lebh;

    invoke-interface {v2}, Lebh;->a()Ljava/util/HashMap;

    move-result-object v4

    .line 142
    const-wide/32 v6, 0x1d4c0

    invoke-virtual {v3, v6, v7}, Landroid/os/ConditionVariable;->block(J)Z

    .line 144
    if-nez v0, :cond_8

    .line 145
    invoke-virtual {v1}, Ldjx;->b()Lojw;

    move-result-object v0

    move-object v2, v0

    .line 148
    :goto_2
    if-eqz v2, :cond_7

    .line 149
    if-eqz v1, :cond_0

    .line 151
    invoke-virtual {p0}, Lebd;->n()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Ldwn;->a(Landroid/content/Context;)Ldwn;

    move-result-object v0

    iget v3, p0, Lebd;->c:I

    invoke-static {v3}, Ldwn;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3, v2}, Ldwn;->a(Ljava/lang/String;Loxu;)V

    .line 155
    :cond_0
    iget-object v3, v2, Lojw;->a:[Lois;

    .line 156
    if-eqz v3, :cond_4

    .line 157
    const/4 v0, 0x0

    :goto_3
    array-length v5, v3

    if-ge v0, v5, :cond_4

    .line 158
    aget-object v5, v3, v0

    .line 159
    iget-object v5, v5, Lois;->d:[Loiu;

    .line 160
    invoke-static {v5, v4}, Ldsm;->a([Loiu;Ljava/util/HashMap;)V

    .line 157
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :catch_0
    move-exception v0

    :cond_1
    move-object v2, v1

    goto :goto_0

    .line 125
    :cond_2
    invoke-virtual {v3}, Landroid/os/ConditionVariable;->open()V

    move-object v0, v2

    .line 128
    goto :goto_1

    .line 131
    :cond_3
    new-instance v0, Lojw;

    invoke-direct {v0}, Lojw;-><init>()V

    .line 132
    iget-object v2, p0, Lebd;->g:Lojw;

    iget-object v2, v2, Lojw;->a:[Lois;

    iput-object v2, v0, Lojw;->a:[Lois;

    .line 133
    iget-object v2, p0, Lebd;->g:Lojw;

    iget-object v2, v2, Lojw;->b:[Loiu;

    iput-object v2, v0, Lojw;->b:[Loiu;

    .line 136
    invoke-virtual {v3}, Landroid/os/ConditionVariable;->open()V

    goto :goto_1

    .line 167
    :cond_4
    new-instance v3, Ljava/util/ArrayList;

    iget-object v0, v2, Lojw;->b:[Loiu;

    .line 168
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 169
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 170
    :cond_5
    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 171
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Loiu;

    .line 172
    iget-object v6, v0, Loiu;->b:Lohv;

    invoke-static {v6, v4}, Ldsm;->a(Lohv;Ljava/util/HashMap;)V

    .line 173
    if-eqz v1, :cond_5

    iget-object v6, v0, Loiu;->b:Lohv;

    iget-object v6, v6, Lohv;->d:[Loij;

    if-eqz v6, :cond_5

    iget-object v0, v0, Loiu;->b:Lohv;

    iget-object v0, v0, Lohv;->d:[Loij;

    array-length v0, v0

    if-lez v0, :cond_5

    .line 176
    invoke-interface {v5}, Ljava/util/Iterator;->remove()V

    goto :goto_4

    .line 179
    :cond_6
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Loiu;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Loiu;

    iput-object v0, v2, Lojw;->b:[Loiu;

    .line 182
    :cond_7
    iput-object v2, p0, Lebd;->g:Lojw;

    .line 183
    return-object v2

    :cond_8
    move-object v2, v0

    goto/16 :goto_2
.end method
