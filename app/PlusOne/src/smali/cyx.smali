.class public Lcyx;
.super Llol;
.source "PG"

# interfaces
.implements Lhec;


# static fields
.field private static final N:Ldgq;


# instance fields
.field private final O:Lhoc;

.field private P:Ldgr;

.field private Q:Ldeo;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 29
    new-instance v0, Ldgq;

    const v1, 0x7f1006e0

    invoke-direct {v0, v1}, Ldgq;-><init>(I)V

    sput-object v0, Lcyx;->N:Ldgq;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    .line 26
    invoke-direct {p0}, Llol;-><init>()V

    .line 33
    new-instance v0, Lhoc;

    iget-object v1, p0, Lcyx;->av:Llqm;

    invoke-direct {v0, p0, v1}, Lhoc;-><init>(Lu;Llqr;)V

    iput-object v0, p0, Lcyx;->O:Lhoc;

    .line 40
    new-instance v0, Ldgn;

    iget-object v1, p0, Lcyx;->av:Llqm;

    sget-object v2, Lcyx;->N:Ldgq;

    new-instance v3, Lcyy;

    invoke-direct {v3, p0}, Lcyy;-><init>(Lcyx;)V

    invoke-direct {v0, v1, v2, v3}, Ldgn;-><init>(Llqr;Ldgu;Ldgv;)V

    .line 47
    new-instance v0, Ldep;

    iget-object v1, p0, Lcyx;->av:Llqm;

    new-instance v2, Lcyz;

    invoke-direct {v2, p0}, Lcyz;-><init>(Lcyx;)V

    invoke-direct {v0, v1, v2}, Ldep;-><init>(Llqr;Ldeq;)V

    .line 53
    return-void
.end method

.method static synthetic a(Lcyx;)V
    .locals 3

    .prologue
    .line 26
    iget-object v0, p0, Lcyx;->Q:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcyx;->P:Ldgr;

    sget-object v2, Lcyx;->N:Ldgq;

    iget-object v0, p0, Lcyx;->Q:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    invoke-interface {v0}, Lddl;->N()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v2, v0}, Ldgr;->a(Ldgu;Z)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public c(I)V
    .locals 6

    .prologue
    .line 76
    invoke-virtual {p0}, Lcyx;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "account_id"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 78
    new-instance v0, Lcza;

    invoke-virtual {p0}, Lcyx;->n()Lz;

    move-result-object v1

    iget-object v3, p0, Lcyx;->Q:Ldeo;

    .line 79
    invoke-virtual {v3}, Ldeo;->a()Lddl;

    move-result-object v3

    invoke-interface {v3}, Lddl;->o()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    iget-object v3, p0, Lcyx;->Q:Ldeo;

    invoke-virtual {v3}, Ldeo;->a()Lddl;

    move-result-object v3

    invoke-interface {v3}, Lddl;->j()Ljava/lang/String;

    move-result-object v5

    move v3, p1

    invoke-direct/range {v0 .. v5}, Lcza;-><init>(Landroid/content/Context;IILjava/lang/Long;Ljava/lang/String;)V

    .line 80
    iget-object v1, p0, Lcyx;->O:Lhoc;

    invoke-virtual {v1, v0}, Lhoc;->c(Lhny;)V

    .line 81
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 57
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 58
    iget-object v0, p0, Lcyx;->au:Llnh;

    const-class v1, Ldgr;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldgr;

    iput-object v0, p0, Lcyx;->P:Ldgr;

    .line 59
    iget-object v0, p0, Lcyx;->au:Llnh;

    const-class v1, Ldeo;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldeo;

    iput-object v0, p0, Lcyx;->Q:Ldeo;

    .line 60
    iget-object v0, p0, Lcyx;->au:Llnh;

    const-class v1, Lhec;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 61
    return-void
.end method
