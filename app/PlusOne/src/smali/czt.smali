.class public final Lczt;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lctm;


# instance fields
.field a:Lldh;

.field b:Ljava/lang/String;

.field c:Ljava/lang/String;

.field d:Ljava/lang/String;

.field e:Ljava/lang/String;

.field f:Ljava/lang/String;

.field g:Z

.field h:J

.field i:Z

.field j:I

.field private k:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lczt;->k:Landroid/content/Context;

    .line 38
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 63
    const/4 v0, 0x0

    return v0
.end method

.method public a(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 42
    check-cast p1, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;

    .line 43
    if-nez p1, :cond_0

    .line 44
    iget-object v0, p0, Lczt;->k:Landroid/content/Context;

    const-string v1, "layout_inflater"

    .line 45
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 46
    const v1, 0x7f04014a

    invoke-virtual {v0, v1, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;

    .line 50
    :goto_0
    iget-object v1, p0, Lczt;->b:Ljava/lang/String;

    iget-object v2, p0, Lczt;->c:Ljava/lang/String;

    iget-object v3, p0, Lczt;->d:Ljava/lang/String;

    invoke-static {v3}, Lhst;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    iget-object v1, p0, Lczt;->e:Ljava/lang/String;

    iget-object v2, p0, Lczt;->f:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    iget-boolean v3, p0, Lczt;->g:Z

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->a(Ljava/lang/String;Landroid/text/Spanned;ZZ)V

    .line 53
    iget-wide v2, p0, Lczt;->h:J

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->a(J)V

    .line 54
    iget-boolean v1, p0, Lczt;->i:Z

    iget v2, p0, Lczt;->j:I

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->a(ZILjava/lang/String;)V

    .line 55
    iget-object v1, p0, Lczt;->a:Lldh;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->a(Lldh;)Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;

    .line 56
    invoke-virtual {v0}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->requestLayout()V

    .line 58
    return-object v0

    :cond_0
    move-object v0, p1

    goto :goto_0
.end method

.method public a(Lctn;)V
    .locals 0

    .prologue
    .line 69
    return-void
.end method
