.class public Lhlh;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/os/Handler;

.field private final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lhlj;",
            ">;"
        }
    .end annotation
.end field

.field private d:Landroid/os/Handler$Callback;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    new-instance v0, Lhli;

    invoke-direct {v0, p0}, Lhli;-><init>(Lhlh;)V

    iput-object v0, p0, Lhlh;->d:Landroid/os/Handler$Callback;

    .line 30
    const-string v0, "context must be non-null"

    invoke-static {p1, v0}, Llsk;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    iput-object p1, p0, Lhlh;->a:Landroid/content/Context;

    .line 33
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    iget-object v2, p0, Lhlh;->d:Landroid/os/Handler$Callback;

    invoke-direct {v0, v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lhlh;->b:Landroid/os/Handler;

    .line 34
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lhlh;->c:Ljava/util/Set;

    .line 35
    return-void
.end method

.method static synthetic a(Lhlh;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lhlh;->c:Ljava/util/Set;

    return-object v0
.end method


# virtual methods
.method public a(Lhkv;)V
    .locals 7

    .prologue
    .line 56
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 57
    new-instance v2, Lhlo;

    iget-object v0, p0, Lhlh;->a:Landroid/content/Context;

    invoke-virtual {p1}, Lhkv;->a()I

    move-result v3

    invoke-direct {v2, v0, v3}, Lhlo;-><init>(Landroid/content/Context;I)V

    .line 61
    iget-object v3, p0, Lhlh;->c:Ljava/util/Set;

    monitor-enter v3

    .line 62
    :try_start_0
    iget-object v0, p0, Lhlh;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhlj;

    .line 63
    iget-object v5, v0, Lhlj;->b:Lhkv;

    invoke-virtual {v5, p1}, Lhkv;->a(Lhkv;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 64
    iget-object v0, v0, Lhlj;->b:Lhkv;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 73
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 69
    :cond_1
    :try_start_1
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhkv;

    .line 70
    invoke-virtual {v2, v0}, Lhlo;->b(Lhkv;)Lhky;

    move-result-object v0

    .line 71
    iget-object v4, p0, Lhlh;->b:Landroid/os/Handler;

    iget-object v5, p0, Lhlh;->b:Landroid/os/Handler;

    const/4 v6, 0x1

    invoke-virtual {v5, v6, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/os/Handler;->dispatchMessage(Landroid/os/Message;)V

    goto :goto_1

    .line 73
    :cond_2
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method a(Lhle;Lhkv;)V
    .locals 2

    .prologue
    .line 39
    iget-object v0, p0, Lhlh;->c:Ljava/util/Set;

    new-instance v1, Lhlj;

    invoke-direct {v1, p1, p2}, Lhlj;-><init>(Lhle;Lhkv;)V

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 40
    return-void
.end method
