.class public Lana;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lavz;",
        "Ljava/lang/Void;",
        "Lamz;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljfb;

.field private final b:Lane;

.field private final c:Lanh;

.field private final d:Lbqg;

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lana;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljfb;Lane;Lanh;Lbqg;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 97
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 98
    const-string v0, "plusDataProvider"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljfb;

    iput-object v0, p0, Lana;->a:Ljfb;

    .line 99
    const-string v0, "listener"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lane;

    iput-object v0, p0, Lana;->b:Lane;

    .line 100
    const-string v0, "gservicesSettings"

    invoke-static {p3, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lanh;

    iput-object v0, p0, Lana;->c:Lanh;

    .line 101
    const-string v0, "uriMimeTypeRetriever"

    invoke-static {p4, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbqg;

    iput-object v0, p0, Lana;->d:Lbqg;

    .line 102
    return-void
.end method

.method public static a(Ljfb;Lanh;Lbqg;)Land;
    .locals 1

    .prologue
    .line 70
    new-instance v0, Lanb;

    invoke-direct {v0, p0, p1, p2}, Lanb;-><init>(Ljfb;Lanh;Lbqg;)V

    return-object v0
.end method


# virtual methods
.method protected varargs a([Lavz;)Lamz;
    .locals 10

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 106
    const-string v0, "params"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 110
    array-length v0, p1

    invoke-static {v0}, Lcfm;->a(I)Ljava/util/HashMap;

    move-result-object v5

    .line 112
    iput v3, p0, Lana;->e:I

    .line 113
    iput v3, p0, Lana;->f:I

    .line 114
    iput v3, p0, Lana;->g:I

    .line 115
    iput v3, p0, Lana;->h:I

    .line 116
    iput v3, p0, Lana;->i:I

    .line 117
    iput v3, p0, Lana;->j:I

    .line 118
    iput-boolean v3, p0, Lana;->k:Z

    move v2, v3

    .line 120
    :goto_0
    array-length v0, p1

    if-ge v2, v0, :cond_e

    .line 121
    invoke-virtual {p0}, Lana;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 122
    invoke-static {}, Lamz;->a()Lamz;

    move-result-object v0

    .line 146
    :goto_1
    return-object v0

    .line 124
    :cond_0
    aget-object v6, p1, v2

    .line 127
    :try_start_0
    iget-object v0, p0, Lana;->d:Lbqg;

    iget-object v7, v6, Lavz;->a:Landroid/net/Uri;

    invoke-interface {v0, v7}, Lbqg;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_2

    const-string v0, "video/"

    invoke-virtual {v7, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lana;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lana;->e:I

    :cond_1
    const-string v0, "image/"

    invoke-virtual {v7, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, Lana;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lana;->h:I

    :cond_2
    iget-object v0, p0, Lana;->a:Ljfb;

    iget-object v8, v6, Lavz;->a:Landroid/net/Uri;

    invoke-virtual {v0, v8}, Ljfb;->d(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    iget-object v8, p0, Lana;->a:Ljfb;

    invoke-virtual {v8, v0}, Ljfb;->a(Landroid/net/Uri;)Z

    move-result v8

    if-eqz v8, :cond_5

    if-nez v7, :cond_3

    new-instance v0, Lanf;

    invoke-direct {v0}, Lanf;-><init>()V

    throw v0
    :try_end_0
    .catch Lanc; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lanf; {:try_start_0 .. :try_end_0} :catch_1

    .line 135
    :catch_0
    move-exception v0

    invoke-static {}, Lamz;->a()Lamz;

    move-result-object v0

    goto :goto_1

    .line 127
    :cond_3
    :try_start_1
    iget-object v8, p0, Lana;->a:Ljfb;

    iget-object v9, v6, Lavz;->a:Landroid/net/Uri;

    invoke-virtual {v8, v9}, Ljfb;->b(Landroid/net/Uri;)Z

    move-result v8

    if-eqz v8, :cond_7

    const-string v8, "mimeType"

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    const-string v8, "video/"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    iget v8, p0, Lana;->f:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lana;->f:I

    :cond_4
    const-string v8, "image/"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    iget v7, p0, Lana;->i:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, Lana;->i:I
    :try_end_1
    .catch Lanc; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lanf; {:try_start_1 .. :try_end_1} :catch_1

    .line 139
    :cond_5
    :goto_2
    if-eqz v0, :cond_6

    .line 141
    iget-object v6, v6, Lavz;->a:Landroid/net/Uri;

    invoke-interface {v5, v6, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    :cond_6
    :goto_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_0

    .line 127
    :cond_7
    :try_start_2
    const-string v0, "mimeType"

    const/4 v8, 0x0

    invoke-static {v7, v0, v8}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    const-string v0, "video/"

    invoke-virtual {v7, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget v0, p0, Lana;->f:I

    iget v8, p0, Lana;->g:I

    add-int/2addr v0, v8

    iget-object v8, p0, Lana;->c:Lanh;

    invoke-virtual {v8}, Lanh;->ag()I

    move-result v8

    if-lt v0, v8, :cond_8

    move v0, v3

    :goto_4
    if-nez v0, :cond_a

    move-object v0, v1

    goto :goto_2

    :cond_8
    const-string v0, "image/"

    invoke-virtual {v7, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    iget v0, p0, Lana;->i:I

    iget v8, p0, Lana;->j:I

    add-int/2addr v0, v8

    iget-object v8, p0, Lana;->c:Lanh;

    invoke-virtual {v8}, Lanh;->af()I

    move-result v8

    if-lt v0, v8, :cond_9

    move v0, v3

    goto :goto_4

    :cond_9
    move v0, v4

    goto :goto_4

    :cond_a
    iget-boolean v0, p0, Lana;->k:Z

    if-nez v0, :cond_b

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {p0, v0}, Lana;->publishProgress([Ljava/lang/Object;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lana;->k:Z

    :cond_b
    iget-object v0, p0, Lana;->a:Ljfb;

    iget-object v8, v6, Lavz;->a:Landroid/net/Uri;

    invoke-virtual {v0, v8}, Ljfb;->c(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lana;->a:Ljfb;

    iget-object v8, v6, Lavz;->a:Landroid/net/Uri;

    invoke-virtual {v0, v8}, Ljfb;->d(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    const-string v8, "mimeType"

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    const-string v8, "video/"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_c

    iget v8, p0, Lana;->g:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lana;->g:I

    :cond_c
    const-string v8, "image/"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    iget v7, p0, Lana;->j:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, Lana;->j:I

    goto/16 :goto_2

    .line 137
    :catch_1
    move-exception v0

    goto/16 :goto_3

    .line 127
    :cond_d
    new-instance v0, Lanc;

    invoke-direct {v0}, Lanc;-><init>()V

    throw v0
    :try_end_2
    .catch Lanc; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lanf; {:try_start_2 .. :try_end_2} :catch_1

    .line 146
    :cond_e
    iget v0, p0, Lana;->e:I

    iget v1, p0, Lana;->g:I

    iget v2, p0, Lana;->h:I

    iget v3, p0, Lana;->j:I

    invoke-static {v5, v0, v1, v2, v3}, Lamz;->a(Ljava/util/Map;IIII)Lamz;

    move-result-object v0

    goto/16 :goto_1
.end method

.method protected varargs a()V
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Lana;->b:Lane;

    invoke-interface {v0}, Lane;->a()V

    .line 279
    return-void
.end method

.method protected a(Lamz;)V
    .locals 1

    .prologue
    .line 273
    iget-object v0, p0, Lana;->b:Lane;

    invoke-interface {v0, p0, p1}, Lane;->a(Lana;Lamz;)V

    .line 274
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 21
    check-cast p1, [Lavz;

    invoke-virtual {p0, p1}, Lana;->a([Lavz;)Lamz;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 21
    check-cast p1, Lamz;

    invoke-virtual {p0, p1}, Lana;->a(Lamz;)V

    return-void
.end method

.method protected synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 21
    invoke-virtual {p0}, Lana;->a()V

    return-void
.end method
