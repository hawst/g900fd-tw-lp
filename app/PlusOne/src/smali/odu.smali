.class public final Lodu;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lodu;


# instance fields
.field private b:Lods;

.field private c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 431
    const/4 v0, 0x0

    new-array v0, v0, [Lodu;

    sput-object v0, Lodu;->a:[Lodu;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 432
    invoke-direct {p0}, Loxq;-><init>()V

    .line 435
    const/4 v0, 0x0

    iput-object v0, p0, Lodu;->b:Lods;

    .line 438
    const/high16 v0, -0x80000000

    iput v0, p0, Lodu;->c:I

    .line 432
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 453
    const/4 v0, 0x0

    .line 454
    iget-object v1, p0, Lodu;->b:Lods;

    if-eqz v1, :cond_0

    .line 455
    const/4 v0, 0x1

    iget-object v1, p0, Lodu;->b:Lods;

    .line 456
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 458
    :cond_0
    const/4 v1, 0x2

    iget v2, p0, Lodu;->c:I

    .line 459
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 460
    iget-object v1, p0, Lodu;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 461
    iput v0, p0, Lodu;->ai:I

    .line 462
    return v0
.end method

.method public a(Loxn;)Lodu;
    .locals 2

    .prologue
    .line 470
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 471
    sparse-switch v0, :sswitch_data_0

    .line 475
    iget-object v1, p0, Lodu;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 476
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lodu;->ah:Ljava/util/List;

    .line 479
    :cond_1
    iget-object v1, p0, Lodu;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 481
    :sswitch_0
    return-object p0

    .line 486
    :sswitch_1
    iget-object v0, p0, Lodu;->b:Lods;

    if-nez v0, :cond_2

    .line 487
    new-instance v0, Lods;

    invoke-direct {v0}, Lods;-><init>()V

    iput-object v0, p0, Lodu;->b:Lods;

    .line 489
    :cond_2
    iget-object v0, p0, Lodu;->b:Lods;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 493
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 494
    if-eqz v0, :cond_3

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    const/4 v1, 0x4

    if-ne v0, v1, :cond_4

    .line 499
    :cond_3
    iput v0, p0, Lodu;->c:I

    goto :goto_0

    .line 501
    :cond_4
    const/4 v0, 0x0

    iput v0, p0, Lodu;->c:I

    goto :goto_0

    .line 471
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 443
    iget-object v0, p0, Lodu;->b:Lods;

    if-eqz v0, :cond_0

    .line 444
    const/4 v0, 0x1

    iget-object v1, p0, Lodu;->b:Lods;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 446
    :cond_0
    const/4 v0, 0x2

    iget v1, p0, Lodu;->c:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 447
    iget-object v0, p0, Lodu;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 449
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 428
    invoke-virtual {p0, p1}, Lodu;->a(Loxn;)Lodu;

    move-result-object v0

    return-object v0
.end method
