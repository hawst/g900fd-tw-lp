.class public final Lfyq;
.super Landroid/view/ViewGroup;
.source "PG"


# static fields
.field private static a:I

.field private static b:I

.field private static c:I


# instance fields
.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/ImageView;

.field private f:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

.field private g:Z

.field private h:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lfyq;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lfyq;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 50
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 52
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lfyq;->d:Landroid/widget/TextView;

    .line 53
    iget-object v0, p0, Lfyq;->d:Landroid/widget/TextView;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 54
    iget-object v0, p0, Lfyq;->d:Landroid/widget/TextView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 55
    iget-object v0, p0, Lfyq;->d:Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 56
    iget-object v0, p0, Lfyq;->d:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 57
    iget-object v0, p0, Lfyq;->d:Landroid/widget/TextView;

    const/16 v1, 0x13

    invoke-static {p1, v0, v1}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 59
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lfyq;->e:Landroid/widget/ImageView;

    .line 60
    iget-object v0, p0, Lfyq;->e:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 62
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 64
    const v1, 0x7f020416

    invoke-virtual {p0, v1}, Lfyq;->setBackgroundResource(I)V

    .line 66
    sget v1, Lfyq;->a:I

    if-nez v1, :cond_0

    .line 67
    const v1, 0x7f0d0372

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lfyq;->a:I

    .line 68
    const v1, 0x7f0d0373

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lfyq;->b:I

    .line 69
    const v1, 0x7f0d0374

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lfyq;->c:I

    .line 71
    :cond_0
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 112
    invoke-virtual {p0}, Lfyq;->removeAllViews()V

    .line 113
    iget-object v0, p0, Lfyq;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 114
    iget-object v0, p0, Lfyq;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 115
    iget-object v0, p0, Lfyq;->f:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    if-eqz v0, :cond_0

    move v0, v1

    .line 116
    :goto_0
    const/4 v2, 0x3

    if-ge v0, v2, :cond_0

    .line 117
    iget-object v2, p0, Lfyq;->f:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->d()V

    .line 116
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 120
    :cond_0
    iput v1, p0, Lfyq;->h:I

    .line 121
    return-void
.end method


# virtual methods
.method public a(Lhgw;Landroid/graphics/drawable/Drawable;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x3

    const/4 v1, 0x0

    .line 85
    invoke-direct {p0}, Lfyq;->a()V

    .line 87
    if-nez p2, :cond_2

    .line 88
    invoke-virtual {p1}, Lhgw;->g()I

    move-result v0

    .line 89
    if-lez v0, :cond_3

    .line 90
    invoke-static {v0, v6}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lfyq;->h:I

    .line 91
    iget v0, p0, Lfyq;->h:I

    if-ne v0, v7, :cond_0

    const/4 v0, 0x2

    .line 93
    :goto_0
    iget-object v2, p0, Lfyq;->f:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    if-nez v2, :cond_1

    new-array v2, v6, [Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    iput-object v2, p0, Lfyq;->f:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    move v2, v1

    :goto_1
    if-ge v2, v6, :cond_1

    iget-object v3, p0, Lfyq;->f:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    new-instance v4, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {p0}, Lfyq;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;-><init>(Landroid/content/Context;)V

    aput-object v4, v3, v2

    iget-object v3, p0, Lfyq;->f:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    aget-object v3, v3, v2

    invoke-virtual {v3, v7}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(I)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_0
    move v0, v1

    .line 91
    goto :goto_0

    .line 94
    :cond_1
    :goto_2
    iget v2, p0, Lfyq;->h:I

    if-ge v1, v2, :cond_3

    .line 95
    invoke-virtual {p1, v1}, Lhgw;->a(I)Ljqs;

    move-result-object v2

    .line 96
    iget-object v3, p0, Lfyq;->f:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    aget-object v3, v3, v1

    invoke-virtual {v3, v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->b(I)V

    .line 97
    iget-object v3, p0, Lfyq;->f:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    aget-object v3, v3, v1

    invoke-virtual {v2}, Ljqs;->a()Ljava/lang/String;

    move-result-object v4

    .line 98
    invoke-virtual {v2}, Ljqs;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lhst;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 97
    invoke-virtual {v3, v4, v2}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    iget-object v2, p0, Lfyq;->f:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    aget-object v2, v2, v1

    invoke-virtual {p0, v2}, Lfyq;->addView(Landroid/view/View;)V

    .line 94
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 103
    :cond_2
    iget-object v0, p0, Lfyq;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 104
    iget-object v0, p0, Lfyq;->e:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lfyq;->addView(Landroid/view/View;)V

    .line 107
    :cond_3
    iget-object v0, p0, Lfyq;->d:Landroid/widget/TextView;

    invoke-virtual {p0}, Lfyq;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1, v1}, Lhgw;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 108
    iget-object v0, p0, Lfyq;->d:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lfyq;->addView(Landroid/view/View;)V

    .line 109
    return-void
.end method

.method public a(Ljava/lang/String;Landroid/graphics/drawable/Drawable;Z)V
    .locals 1

    .prologue
    .line 74
    invoke-direct {p0}, Lfyq;->a()V

    .line 75
    iput-boolean p3, p0, Lfyq;->g:Z

    .line 76
    if-eqz p2, :cond_0

    .line 77
    iget-object v0, p0, Lfyq;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 78
    iget-object v0, p0, Lfyq;->e:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lfyq;->addView(Landroid/view/View;)V

    .line 80
    :cond_0
    iget-object v0, p0, Lfyq;->d:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    iget-object v0, p0, Lfyq;->d:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lfyq;->addView(Landroid/view/View;)V

    .line 82
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v6, 0x0

    .line 172
    invoke-virtual {p0}, Lfyq;->getMeasuredWidth()I

    move-result v0

    .line 173
    invoke-virtual {p0}, Lfyq;->getMeasuredHeight()I

    move-result v1

    sget v2, Lfyq;->a:I

    mul-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    .line 175
    invoke-virtual {p0}, Lfyq;->getContext()Landroid/content/Context;

    move-result-object v2

    const/16 v3, 0x13

    .line 174
    invoke-static {v2, v3}, Llib;->a(Landroid/content/Context;I)I

    move-result v2

    .line 176
    mul-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    .line 177
    iget-object v2, p0, Lfyq;->f:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    if-eqz v2, :cond_0

    .line 178
    iget v2, p0, Lfyq;->h:I

    if-ne v2, v8, :cond_3

    .line 179
    sget v2, Lfyq;->c:I

    sub-int v2, v0, v2

    div-int/lit8 v2, v2, 0x2

    .line 180
    sget v3, Lfyq;->a:I

    sget v4, Lfyq;->c:I

    sub-int v4, v1, v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    .line 181
    iget-object v4, p0, Lfyq;->f:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    aget-object v4, v4, v6

    sget v5, Lfyq;->c:I

    add-int/2addr v5, v2

    sget v6, Lfyq;->c:I

    add-int/2addr v6, v3

    invoke-virtual {v4, v2, v3, v5, v6}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->layout(IIII)V

    .line 206
    :cond_0
    :goto_0
    iget-object v2, p0, Lfyq;->e:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-ne v2, p0, :cond_1

    .line 207
    iget-object v2, p0, Lfyq;->e:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v2

    sub-int v2, v0, v2

    div-int/lit8 v2, v2, 0x2

    .line 208
    iget-object v3, p0, Lfyq;->e:Landroid/widget/ImageView;

    sget v4, Lfyq;->a:I

    iget-object v5, p0, Lfyq;->e:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v5

    add-int/2addr v5, v2

    sget v6, Lfyq;->a:I

    iget-object v7, p0, Lfyq;->e:Landroid/widget/ImageView;

    .line 209
    invoke-virtual {v7}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v7

    add-int/2addr v6, v7

    .line 208
    invoke-virtual {v3, v2, v4, v5, v6}, Landroid/widget/ImageView;->layout(IIII)V

    .line 212
    :cond_1
    iget-object v2, p0, Lfyq;->d:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-ne v2, p0, :cond_2

    .line 213
    iget-object v2, p0, Lfyq;->d:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v2

    sub-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x2

    .line 214
    iget-object v2, p0, Lfyq;->d:Landroid/widget/TextView;

    sget v3, Lfyq;->a:I

    add-int/2addr v3, v1

    iget-object v4, p0, Lfyq;->d:Landroid/widget/TextView;

    .line 215
    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v0

    sget v5, Lfyq;->a:I

    add-int/2addr v1, v5

    iget-object v5, p0, Lfyq;->d:Landroid/widget/TextView;

    .line 216
    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v1, v5

    .line 214
    invoke-virtual {v2, v0, v3, v4, v1}, Landroid/widget/TextView;->layout(IIII)V

    .line 218
    :cond_2
    return-void

    .line 183
    :cond_3
    iget v2, p0, Lfyq;->h:I

    if-ne v2, v9, :cond_4

    .line 184
    invoke-virtual {p0}, Lfyq;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lhss;->a(Landroid/content/Context;)I

    move-result v2

    .line 185
    sub-int v3, v0, v2

    div-int/lit8 v3, v3, 0x2

    .line 186
    sget v4, Lfyq;->a:I

    div-int/lit8 v5, v1, 0x2

    add-int/2addr v4, v5

    sub-int/2addr v4, v2

    .line 187
    iget-object v5, p0, Lfyq;->f:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    aget-object v5, v5, v6

    add-int v6, v3, v2

    add-int v7, v4, v2

    invoke-virtual {v5, v3, v4, v6, v7}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->layout(IIII)V

    .line 189
    div-int/lit8 v3, v0, 0x2

    sub-int/2addr v3, v2

    .line 190
    iget-object v5, p0, Lfyq;->f:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    aget-object v5, v5, v8

    add-int v6, v4, v2

    add-int v7, v3, v2

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v2, v4

    invoke-virtual {v5, v3, v6, v7, v2}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->layout(IIII)V

    goto/16 :goto_0

    .line 192
    :cond_4
    iget v2, p0, Lfyq;->h:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_0

    .line 193
    invoke-virtual {p0}, Lfyq;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lhss;->a(Landroid/content/Context;)I

    move-result v2

    .line 194
    sub-int v3, v0, v2

    div-int/lit8 v3, v3, 0x2

    .line 195
    sget v4, Lfyq;->a:I

    div-int/lit8 v5, v1, 0x2

    add-int/2addr v4, v5

    sub-int/2addr v4, v2

    .line 196
    iget-object v5, p0, Lfyq;->f:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    aget-object v5, v5, v6

    add-int v6, v3, v2

    add-int v7, v4, v2

    invoke-virtual {v5, v3, v4, v6, v7}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->layout(IIII)V

    .line 198
    div-int/lit8 v3, v0, 0x2

    sub-int/2addr v3, v2

    .line 199
    iget-object v5, p0, Lfyq;->f:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    aget-object v5, v5, v8

    add-int v6, v4, v2

    add-int v7, v3, v2

    mul-int/lit8 v8, v2, 0x2

    add-int/2addr v8, v4

    invoke-virtual {v5, v3, v6, v7, v8}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->layout(IIII)V

    .line 201
    iget-object v5, p0, Lfyq;->f:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    aget-object v5, v5, v9

    add-int v6, v3, v2

    add-int v7, v4, v2

    mul-int/lit8 v8, v2, 0x2

    add-int/2addr v3, v8

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v2, v4

    invoke-virtual {v5, v6, v7, v3, v2}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->layout(IIII)V

    goto/16 :goto_0
.end method

.method public onMeasure(II)V
    .locals 8

    .prologue
    const/high16 v7, 0x40000000    # 2.0f

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 135
    invoke-virtual {p0}, Lfyq;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 136
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 137
    sget v0, Lfyq;->a:I

    .line 138
    invoke-static {v5, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 140
    iget-object v4, p0, Lfyq;->d:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    if-ne v4, p0, :cond_0

    .line 141
    iget-object v4, p0, Lfyq;->d:Landroid/widget/TextView;

    invoke-virtual {v4, p1, v3}, Landroid/widget/TextView;->measure(II)V

    .line 143
    const/16 v4, 0x13

    invoke-static {v1, v4}, Llib;->a(Landroid/content/Context;I)I

    move-result v1

    .line 145
    mul-int/lit8 v1, v1, 0x2

    sget v4, Lfyq;->a:I

    add-int/2addr v1, v4

    add-int/2addr v0, v1

    .line 148
    :cond_0
    iget-object v1, p0, Lfyq;->e:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-ne v1, p0, :cond_1

    .line 149
    iget-boolean v1, p0, Lfyq;->g:Z

    if-eqz v1, :cond_4

    .line 150
    sget v1, Lfyq;->b:I

    invoke-static {v1, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 151
    iget-object v3, p0, Lfyq;->e:Landroid/widget/ImageView;

    invoke-virtual {v3, v1, v1}, Landroid/widget/ImageView;->measure(II)V

    .line 155
    :goto_0
    iget-object v1, p0, Lfyq;->e:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v1

    sget v3, Lfyq;->a:I

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 158
    :cond_1
    iget v1, p0, Lfyq;->h:I

    if-lez v1, :cond_3

    .line 159
    iget v1, p0, Lfyq;->h:I

    if-ne v1, v6, :cond_2

    .line 160
    sget v1, Lfyq;->c:I

    invoke-static {v1, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 161
    iget-object v3, p0, Lfyq;->f:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    aget-object v3, v3, v5

    invoke-virtual {v3, v6}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Z)V

    .line 162
    iget-object v3, p0, Lfyq;->f:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    aget-object v3, v3, v5

    invoke-virtual {v3, v1, v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->measure(II)V

    .line 164
    :cond_2
    sget v1, Lfyq;->c:I

    sget v3, Lfyq;->a:I

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 167
    :cond_3
    invoke-virtual {p0, v2, v0}, Lfyq;->setMeasuredDimension(II)V

    .line 168
    return-void

    .line 153
    :cond_4
    iget-object v1, p0, Lfyq;->e:Landroid/widget/ImageView;

    invoke-virtual {v1, v3, v3}, Landroid/widget/ImageView;->measure(II)V

    goto :goto_0
.end method
