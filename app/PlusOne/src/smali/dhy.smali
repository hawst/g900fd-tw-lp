.class public final Ldhy;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhjc;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:I

.field private c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    const/4 v0, -0x1

    iput v0, p0, Ldhy;->c:I

    .line 48
    iput-object p1, p0, Ldhy;->a:Landroid/content/Context;

    .line 49
    iput p2, p0, Ldhy;->b:I

    .line 50
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 58
    const/4 v0, -0x1

    iput v0, p0, Ldhy;->c:I

    .line 59
    return-void
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 53
    iput p1, p0, Ldhy;->c:I

    .line 54
    return-void
.end method

.method public a(Landroid/view/Menu;)V
    .locals 4

    .prologue
    .line 63
    iget-object v1, p0, Ldhy;->a:Landroid/content/Context;

    .line 65
    iget v0, p0, Ldhy;->c:I

    const/4 v2, -0x1

    if-ne v0, v2, :cond_1

    .line 82
    :cond_0
    :goto_0
    return-void

    .line 69
    :cond_1
    const-class v0, Lieh;

    invoke-static {v1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    .line 70
    sget-object v2, Ldxd;->m:Lief;

    iget v3, p0, Ldhy;->c:I

    invoke-interface {v0, v2, v3}, Lieh;->b(Lief;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    iget v0, p0, Ldhy;->b:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 76
    const-class v0, Ljuk;

    invoke-static {v1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljuk;

    .line 78
    invoke-static {v2}, Lie;->a(Landroid/view/MenuItem;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v7/app/MediaRouteButton;

    .line 79
    invoke-virtual {v0, v1}, Ljuk;->a(Landroid/support/v7/app/MediaRouteButton;)V

    .line 80
    invoke-virtual {v0}, Ljuk;->a()Z

    move-result v0

    .line 81
    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method
