.class public Lezk;
.super Lhye;
.source "PG"

# interfaces
.implements Lfdi;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lhye;",
        "Lfdi",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# static fields
.field private static final b:Lloy;

.field private static final c:[Ljava/lang/String;

.field private static final d:[Ljava/lang/String;

.field private static e:[Landroid/net/Uri;

.field private static final f:[Z

.field private static g:[Landroid/net/Uri;

.field private static final h:[Z

.field private static i:[Landroid/net/Uri;

.field private static final j:[Z


# instance fields
.field private k:Z

.field private final l:Ldp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">.dp;"
        }
    .end annotation
.end field

.field private m:Lizu;

.field private final n:[Landroid/net/Uri;

.field private final o:[Z

.field private final p:[Ljava/lang/String;

.field private final q:Ljava/lang/String;

.field private final r:Z


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x2

    .line 42
    new-instance v0, Lloy;

    const-string v1, "debug_invalid_uris"

    invoke-direct {v0, v1}, Lloy;-><init>(Ljava/lang/String;)V

    sput-object v0, Lezk;->b:Lloy;

    .line 44
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "corrected_date_taken"

    aput-object v1, v0, v4

    const-string v1, "_id"

    aput-object v1, v0, v5

    sput-object v0, Lezk;->c:[Ljava/lang/String;

    .line 47
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "corrected_added_modified"

    aput-object v1, v0, v4

    const-string v1, "_id"

    aput-object v1, v0, v5

    sput-object v0, Lezk;->d:[Ljava/lang/String;

    .line 108
    new-array v0, v6, [Landroid/net/Uri;

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v1, v0, v4

    sget-object v1, Lhsf;->a:Landroid/net/Uri;

    aput-object v1, v0, v5

    sget-object v1, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v1, v0, v3

    const/4 v1, 0x3

    sget-object v2, Lhsf;->b:Landroid/net/Uri;

    aput-object v2, v0, v1

    sput-object v0, Lezk;->e:[Landroid/net/Uri;

    .line 116
    new-array v0, v6, [Z

    fill-array-data v0, :array_0

    sput-object v0, Lezk;->f:[Z

    .line 123
    new-array v0, v3, [Landroid/net/Uri;

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v1, v0, v4

    sget-object v1, Lhsf;->a:Landroid/net/Uri;

    aput-object v1, v0, v5

    sput-object v0, Lezk;->g:[Landroid/net/Uri;

    .line 129
    new-array v0, v3, [Z

    fill-array-data v0, :array_1

    sput-object v0, Lezk;->h:[Z

    .line 136
    new-array v0, v3, [Landroid/net/Uri;

    sget-object v1, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v1, v0, v4

    sget-object v1, Lhsf;->b:Landroid/net/Uri;

    aput-object v1, v0, v5

    sput-object v0, Lezk;->i:[Landroid/net/Uri;

    .line 142
    new-array v0, v3, [Z

    fill-array-data v0, :array_2

    sput-object v0, Lezk;->j:[Z

    return-void

    .line 116
    :array_0
    .array-data 1
        0x0t
        0x0t
        0x1t
        0x1t
    .end array-data

    .line 129
    :array_1
    .array-data 1
        0x0t
        0x0t
    .end array-data

    .line 142
    nop

    :array_2
    .array-data 1
        0x1t
        0x1t
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/List;ILizu;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Ljuf;",
            ">;I",
            "Lizu;",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            ")V"
        }
    .end annotation

    .prologue
    .line 173
    invoke-direct {p0, p1, p6}, Lhye;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    .line 153
    new-instance v0, Ldp;

    invoke-direct {v0, p0}, Ldp;-><init>(Ldo;)V

    iput-object v0, p0, Lezk;->l:Ldp;

    .line 176
    and-int/lit8 v0, p3, 0x1

    if-eqz v0, :cond_0

    .line 177
    sget-object v0, Lezk;->g:[Landroid/net/Uri;

    iput-object v0, p0, Lezk;->n:[Landroid/net/Uri;

    .line 178
    sget-object v0, Lezk;->h:[Z

    iput-object v0, p0, Lezk;->o:[Z

    .line 187
    :goto_0
    iget-object v0, p0, Lezk;->n:[Landroid/net/Uri;

    array-length v0, v0

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lezk;->p:[Ljava/lang/String;

    .line 188
    iget-object v0, p0, Lezk;->p:[Ljava/lang/String;

    array-length v2, v0

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_2

    iget-object v1, p0, Lezk;->p:[Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v3, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 179
    :cond_0
    and-int/lit8 v0, p3, 0x2

    if-eqz v0, :cond_1

    .line 180
    sget-object v0, Lezk;->i:[Landroid/net/Uri;

    iput-object v0, p0, Lezk;->n:[Landroid/net/Uri;

    .line 181
    sget-object v0, Lezk;->j:[Z

    iput-object v0, p0, Lezk;->o:[Z

    goto :goto_0

    .line 183
    :cond_1
    sget-object v0, Lezk;->e:[Landroid/net/Uri;

    iput-object v0, p0, Lezk;->n:[Landroid/net/Uri;

    .line 184
    sget-object v0, Lezk;->f:[Z

    iput-object v0, p0, Lezk;->o:[Z

    goto :goto_0

    .line 188
    :cond_2
    if-eqz p2, :cond_3

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_4

    .line 189
    :cond_3
    iput-object p4, p0, Lezk;->m:Lizu;

    .line 191
    invoke-static {}, Ljvj;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {p5, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_b

    const/4 v0, 0x1

    :goto_2
    iput-boolean v0, p0, Lezk;->r:Z

    .line 193
    iget-boolean v0, p0, Lezk;->r:Z

    if-eqz v0, :cond_d

    if-eqz p5, :cond_d

    invoke-static {p5}, Ljvj;->m(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 194
    invoke-static {p5}, Ljvj;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 195
    invoke-static {v0}, Ljvj;->h(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 196
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_c

    :goto_3
    iput-object v0, p0, Lezk;->q:Ljava/lang/String;

    .line 200
    :goto_4
    return-void

    .line 188
    :cond_4
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v3

    new-array v4, v2, [Ljava/lang/StringBuilder;

    new-array v5, v2, [Ljava/lang/String;

    const/4 v0, 0x0

    :goto_5
    if-ge v0, v2, :cond_5

    iget-object v1, p0, Lezk;->n:[Landroid/net/Uri;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_5
    const/4 v0, 0x0

    move v1, v0

    :goto_6
    if-ge v1, v3, :cond_9

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljuf;

    invoke-interface {v0}, Ljuf;->f()Lizu;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lizu;->i()Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-virtual {v0}, Lizu;->e()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v0, 0x0

    :goto_7
    if-ge v0, v2, :cond_6

    aget-object v8, v5, v0

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_8

    aget-object v7, v4, v0

    if-nez v7, :cond_7

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "_data LIKE \'%/DCIM/%\' AND _id NOT IN ("

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aput-object v7, v4, v0

    :goto_8
    aget-object v0, v4, v0

    invoke-static {v6}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v6

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    :cond_7
    aget-object v7, v4, v0

    const/16 v8, 0x2c

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_8

    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    :cond_9
    const/4 v0, 0x0

    :goto_9
    if-ge v0, v2, :cond_3

    aget-object v1, v4, v0

    if-eqz v1, :cond_a

    iget-object v1, p0, Lezk;->p:[Ljava/lang/String;

    aget-object v3, v4, v0

    const/16 v5, 0x29

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v0

    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 191
    :cond_b
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 196
    :cond_c
    const/4 v0, 0x0

    goto/16 :goto_3

    .line 198
    :cond_d
    const/4 v0, 0x0

    iput-object v0, p0, Lezk;->q:Ljava/lang/String;

    goto/16 :goto_4
.end method

.method public static a(Landroid/content/Context;Landroid/database/Cursor;)Lizu;
    .locals 9

    .prologue
    const/4 v8, 0x6

    const/4 v2, 0x0

    .line 406
    move-object v0, p1

    check-cast v0, Lhyx;

    .line 407
    invoke-virtual {v0}, Lhyx;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v3, "media_uris"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, [Landroid/net/Uri;

    .line 409
    invoke-virtual {v0}, Lhyx;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "media_is_video"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getBooleanArray(Ljava/lang/String;)[Z

    move-result-object v3

    .line 411
    invoke-virtual {v0}, Lhyx;->a()I

    move-result v4

    .line 412
    aget-object v0, v1, v4

    .line 413
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 412
    invoke-static {v0, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 414
    const/4 v0, 0x5

    .line 416
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 417
    invoke-interface {p1, v8}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v2

    .line 418
    :goto_0
    const/4 v2, 0x2

    .line 419
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 415
    invoke-static {v5, v0, v6, v7}, Lhsf;->a(Ljava/lang/String;IJ)Ljava/lang/String;

    move-result-object v2

    .line 420
    aget-boolean v0, v3, v4

    if-eqz v0, :cond_1

    sget-object v0, Ljac;->b:Ljac;

    .line 423
    :goto_1
    invoke-static {p0, v1, v0, v2}, Lizu;->a(Landroid/content/Context;Landroid/net/Uri;Ljac;Ljava/lang/String;)Lizu;

    move-result-object v0

    return-object v0

    .line 418
    :cond_0
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    goto :goto_0

    .line 420
    :cond_1
    sget-object v0, Ljac;->a:Ljac;

    goto :goto_1
.end method


# virtual methods
.method public C()Landroid/database/Cursor;
    .locals 12

    .prologue
    .line 279
    const/4 v0, 0x0

    .line 280
    const-string v1, "LocalCollectionLoader"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_16

    .line 281
    new-instance v0, Lfvg;

    invoke-direct {v0}, Lfvg;-><init>()V

    invoke-virtual {v0}, Lfvg;->a()Lfvg;

    move-result-object v0

    .line 282
    const-string v1, "esLoadInBackground: BEGIN thread="

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-object v6, v0

    .line 285
    :goto_0
    iget-object v0, p0, Lezk;->n:[Landroid/net/Uri;

    array-length v10, v0

    .line 286
    new-array v11, v10, [Landroid/database/Cursor;

    .line 289
    :try_start_0
    iget-object v0, p0, Lezk;->q:Ljava/lang/String;

    if-nez v0, :cond_4

    invoke-virtual {p0}, Lezk;->n()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a0432

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 291
    :goto_1
    const/4 v1, 0x0

    move v9, v1

    move-object v7, v0

    :goto_2
    if-ge v9, v10, :cond_d

    .line 292
    iget-object v0, p0, Lezk;->n:[Landroid/net/Uri;

    aget-object v0, v0, v9

    invoke-virtual {p0, v0}, Lezk;->a(Landroid/net/Uri;)V

    .line 293
    iget-object v0, p0, Lezk;->o:[Z

    aget-boolean v0, v0, v9

    if-eqz v0, :cond_5

    sget-object v0, Lezl;->b:[Ljava/lang/String;

    :goto_3
    invoke-virtual {p0, v0}, Lezk;->a([Ljava/lang/String;)V

    .line 295
    iget-object v0, p0, Lezk;->q:Ljava/lang/String;

    if-eqz v0, :cond_6

    const-string v0, "corrected_added_modified DESC, _id DESC"

    :goto_4
    invoke-virtual {p0, v0}, Lezk;->b(Ljava/lang/String;)V

    .line 298
    iget-boolean v0, p0, Lezk;->r:Z

    if-eqz v0, :cond_0

    .line 301
    iget-object v0, p0, Lezk;->q:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 302
    iget-object v0, p0, Lezk;->o:[Z

    aget-boolean v0, v0, v9

    if-eqz v0, :cond_7

    .line 303
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "bucket_id = %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lezk;->q:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 319
    :goto_5
    invoke-virtual {p0, v0}, Lezk;->a(Ljava/lang/String;)V

    .line 321
    :cond_0
    invoke-super {p0}, Lhye;->C()Landroid/database/Cursor;

    move-result-object v0

    aput-object v0, v11, v9

    .line 323
    aget-object v0, v11, v9

    if-eqz v0, :cond_1

    .line 324
    aget-object v0, v11, v9

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 327
    :cond_1
    iget-object v0, p0, Lezk;->q:Ljava/lang/String;

    if-eqz v0, :cond_15

    if-nez v7, :cond_15

    .line 328
    invoke-virtual {p0}, Lezk;->n()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 329
    iget-object v1, p0, Lezk;->o:[Z

    aget-boolean v1, v1, v9

    if-eqz v1, :cond_a

    sget-object v2, Lezl;->d:[Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 333
    :goto_6
    const/4 v8, 0x0

    .line 336
    :try_start_1
    iget-object v1, p0, Lezk;->n:[Landroid/net/Uri;

    aget-object v1, v1, v9

    invoke-virtual {p0}, Lezk;->k()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    .line 338
    if-eqz v1, :cond_14

    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 339
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v7

    move-object v0, v7

    .line 342
    :goto_7
    if-eqz v1, :cond_2

    .line 343
    :try_start_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_0

    .line 291
    :cond_2
    :goto_8
    add-int/lit8 v1, v9, 0x1

    move v9, v1

    move-object v7, v0

    goto/16 :goto_2

    .line 282
    :cond_3
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    move-object v6, v0

    goto/16 :goto_0

    .line 289
    :cond_4
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 293
    :cond_5
    :try_start_4
    sget-object v0, Lezl;->a:[Ljava/lang/String;

    goto/16 :goto_3

    .line 295
    :cond_6
    const-string v0, "corrected_date_taken DESC,_id DESC"

    goto/16 :goto_4

    .line 307
    :cond_7
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "bucket_id = %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lezk;->q:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    .line 312
    :cond_8
    iget-object v0, p0, Lezk;->p:[Ljava/lang/String;

    aget-object v0, v0, v9

    if-eqz v0, :cond_9

    .line 313
    iget-object v0, p0, Lezk;->p:[Ljava/lang/String;

    aget-object v0, v0, v9

    goto :goto_5

    .line 315
    :cond_9
    const-string v0, "_data LIKE \'%/DCIM/%\'"

    goto/16 :goto_5

    .line 329
    :cond_a
    sget-object v2, Lezl;->c:[Ljava/lang/String;

    goto :goto_6

    .line 342
    :catchall_0
    move-exception v0

    move-object v1, v8

    :goto_9
    if-eqz v1, :cond_b

    .line 343
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_b
    throw v0
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_0

    .line 389
    :catch_0
    move-exception v0

    .line 390
    array-length v2, v11

    const/4 v1, 0x0

    :goto_a
    if-ge v1, v2, :cond_13

    aget-object v3, v11, v1

    .line 391
    if-eqz v3, :cond_c

    .line 392
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 390
    :cond_c
    add-int/lit8 v1, v1, 0x1

    goto :goto_a

    .line 349
    :cond_d
    :try_start_5
    iget-object v0, p0, Lezk;->q:Ljava/lang/String;

    if-eqz v0, :cond_10

    sget-object v0, Lezk;->d:[Ljava/lang/String;

    .line 350
    :goto_b
    new-instance v4, Lhyx;

    const/4 v1, 0x0

    invoke-direct {v4, v11, v0, v1}, Lhyx;-><init>([Landroid/database/Cursor;[Ljava/lang/String;I)V

    .line 352
    invoke-virtual {v4}, Lhyx;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    .line 353
    const-string v0, "media_is_video"

    iget-object v1, p0, Lezk;->o:[Z

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putBooleanArray(Ljava/lang/String;[Z)V

    .line 354
    const-string v0, "media_uris"

    iget-object v1, p0, Lezk;->n:[Landroid/net/Uri;

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 355
    const-string v0, "title"

    invoke-virtual {v5, v0, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 357
    iget-object v0, p0, Lezk;->m:Lizu;
    :try_end_5
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_0

    if-eqz v0, :cond_e

    .line 359
    const-wide/16 v0, -0x1

    .line 361
    :try_start_6
    iget-object v2, p0, Lezk;->m:Lizu;

    invoke-virtual {v2}, Lizu;->e()Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J
    :try_end_6
    .catch Ljava/lang/NumberFormatException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_0

    move-result-wide v0

    move-wide v2, v0

    .line 364
    :goto_c
    const-wide/16 v0, -0x1

    cmp-long v0, v2, v0

    if-eqz v0, :cond_e

    .line 368
    :try_start_7
    iget-object v0, p0, Lezk;->m:Lizu;

    invoke-virtual {v0}, Lizu;->g()Ljac;

    move-result-object v0

    sget-object v1, Ljac;->b:Ljac;

    if-ne v0, v1, :cond_11

    const/4 v0, 0x1

    move v1, v0

    .line 369
    :goto_d
    const/4 v0, 0x0

    .line 370
    invoke-virtual {v4}, Lhyx;->moveToFirst()Z

    .line 371
    :goto_e
    invoke-virtual {v4}, Lhyx;->isAfterLast()Z

    move-result v7

    if-nez v7, :cond_e

    .line 372
    iget-object v7, p0, Lezk;->o:[Z

    invoke-virtual {v4}, Lhyx;->a()I

    move-result v8

    aget-boolean v7, v7, v8

    if-ne v7, v1, :cond_12

    const/4 v7, 0x0

    .line 373
    invoke-virtual {v4, v7}, Lhyx;->getLong(I)J

    move-result-wide v8

    cmp-long v7, v8, v2

    if-nez v7, :cond_12

    .line 374
    const-string v1, "start_position"

    invoke-virtual {v5, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 383
    :cond_e
    const-string v0, "LocalCollectionLoader"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 384
    invoke-virtual {v4}, Lhyx;->getCount()I

    move-result v0

    .line 385
    invoke-virtual {v6}, Lfvg;->b()J

    move-result-wide v2

    .line 386
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x51

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "esLoadInBackground: END totalRows="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ", msec="

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", thread="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 384
    :cond_f
    return-object v4

    .line 349
    :cond_10
    sget-object v0, Lezk;->c:[Ljava/lang/String;

    goto/16 :goto_b

    .line 363
    :catch_1
    move-exception v2

    sget-object v2, Lezk;->b:Lloy;

    move-wide v2, v0

    goto/16 :goto_c

    .line 368
    :cond_11
    const/4 v0, 0x0

    move v1, v0

    goto :goto_d

    .line 377
    :cond_12
    add-int/lit8 v0, v0, 0x1

    .line 378
    invoke-virtual {v4}, Lhyx;->moveToNext()Z
    :try_end_7
    .catch Ljava/lang/RuntimeException; {:try_start_7 .. :try_end_7} :catch_0

    goto :goto_e

    .line 395
    :cond_13
    throw v0

    .line 342
    :catchall_1
    move-exception v0

    goto/16 :goto_9

    :cond_14
    move-object v0, v7

    goto/16 :goto_7

    :cond_15
    move-object v0, v7

    goto/16 :goto_8

    :cond_16
    move-object v6, v0

    goto/16 :goto_0
.end method

.method public a(Landroid/database/Cursor;I)V
    .locals 1

    .prologue
    .line 428
    invoke-interface {p1, p2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 429
    invoke-virtual {p0}, Lezk;->n()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lezk;->a(Landroid/content/Context;Landroid/database/Cursor;)Lizu;

    move-result-object v0

    iput-object v0, p0, Lezk;->m:Lizu;

    .line 430
    return-void
.end method

.method public a(Lizu;)V
    .locals 0

    .prologue
    .line 434
    iput-object p1, p0, Lezk;->m:Lizu;

    .line 435
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;I)V
    .locals 0

    .prologue
    .line 38
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lezk;->a(Landroid/database/Cursor;I)V

    return-void
.end method

.method protected g()V
    .locals 5

    .prologue
    .line 204
    invoke-super {p0}, Lhye;->g()V

    .line 205
    iget-boolean v0, p0, Lezk;->k:Z

    if-nez v0, :cond_1

    .line 206
    iget-object v0, p0, Lezk;->n:[Landroid/net/Uri;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    .line 207
    iget-object v1, p0, Lezk;->n:[Landroid/net/Uri;

    aget-object v1, v1, v0

    .line 208
    invoke-virtual {p0}, Lezk;->n()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    iget-object v4, p0, Lezk;->l:Ldp;

    invoke-virtual {v2, v1, v3, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 206
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 211
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lezk;->k:Z

    .line 213
    :cond_1
    return-void
.end method

.method protected w()V
    .locals 2

    .prologue
    .line 217
    iget-boolean v0, p0, Lezk;->k:Z

    if-eqz v0, :cond_0

    .line 218
    invoke-virtual {p0}, Lezk;->n()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lezk;->l:Ldp;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 219
    const/4 v0, 0x0

    iput-boolean v0, p0, Lezk;->k:Z

    .line 221
    :cond_0
    return-void
.end method
