.class public final Lnuh;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnua;

.field public b:Lnub;

.field public c:Lnuk;

.field private d:Lnty;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 13
    iput-object v0, p0, Lnuh;->a:Lnua;

    .line 16
    iput-object v0, p0, Lnuh;->b:Lnub;

    .line 19
    iput-object v0, p0, Lnuh;->c:Lnuk;

    .line 22
    iput-object v0, p0, Lnuh;->d:Lnty;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 45
    const/4 v0, 0x0

    .line 46
    iget-object v1, p0, Lnuh;->a:Lnua;

    if-eqz v1, :cond_0

    .line 47
    const/4 v0, 0x1

    iget-object v1, p0, Lnuh;->a:Lnua;

    .line 48
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 50
    :cond_0
    iget-object v1, p0, Lnuh;->b:Lnub;

    if-eqz v1, :cond_1

    .line 51
    const/4 v1, 0x2

    iget-object v2, p0, Lnuh;->b:Lnub;

    .line 52
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 54
    :cond_1
    iget-object v1, p0, Lnuh;->c:Lnuk;

    if-eqz v1, :cond_2

    .line 55
    const/4 v1, 0x3

    iget-object v2, p0, Lnuh;->c:Lnuk;

    .line 56
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 58
    :cond_2
    iget-object v1, p0, Lnuh;->d:Lnty;

    if-eqz v1, :cond_3

    .line 59
    const/4 v1, 0x4

    iget-object v2, p0, Lnuh;->d:Lnty;

    .line 60
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 62
    :cond_3
    iget-object v1, p0, Lnuh;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 63
    iput v0, p0, Lnuh;->ai:I

    .line 64
    return v0
.end method

.method public a(Loxn;)Lnuh;
    .locals 2

    .prologue
    .line 72
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 73
    sparse-switch v0, :sswitch_data_0

    .line 77
    iget-object v1, p0, Lnuh;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 78
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnuh;->ah:Ljava/util/List;

    .line 81
    :cond_1
    iget-object v1, p0, Lnuh;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 83
    :sswitch_0
    return-object p0

    .line 88
    :sswitch_1
    iget-object v0, p0, Lnuh;->a:Lnua;

    if-nez v0, :cond_2

    .line 89
    new-instance v0, Lnua;

    invoke-direct {v0}, Lnua;-><init>()V

    iput-object v0, p0, Lnuh;->a:Lnua;

    .line 91
    :cond_2
    iget-object v0, p0, Lnuh;->a:Lnua;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 95
    :sswitch_2
    iget-object v0, p0, Lnuh;->b:Lnub;

    if-nez v0, :cond_3

    .line 96
    new-instance v0, Lnub;

    invoke-direct {v0}, Lnub;-><init>()V

    iput-object v0, p0, Lnuh;->b:Lnub;

    .line 98
    :cond_3
    iget-object v0, p0, Lnuh;->b:Lnub;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 102
    :sswitch_3
    iget-object v0, p0, Lnuh;->c:Lnuk;

    if-nez v0, :cond_4

    .line 103
    new-instance v0, Lnuk;

    invoke-direct {v0}, Lnuk;-><init>()V

    iput-object v0, p0, Lnuh;->c:Lnuk;

    .line 105
    :cond_4
    iget-object v0, p0, Lnuh;->c:Lnuk;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 109
    :sswitch_4
    iget-object v0, p0, Lnuh;->d:Lnty;

    if-nez v0, :cond_5

    .line 110
    new-instance v0, Lnty;

    invoke-direct {v0}, Lnty;-><init>()V

    iput-object v0, p0, Lnuh;->d:Lnty;

    .line 112
    :cond_5
    iget-object v0, p0, Lnuh;->d:Lnty;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 73
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 27
    iget-object v0, p0, Lnuh;->a:Lnua;

    if-eqz v0, :cond_0

    .line 28
    const/4 v0, 0x1

    iget-object v1, p0, Lnuh;->a:Lnua;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 30
    :cond_0
    iget-object v0, p0, Lnuh;->b:Lnub;

    if-eqz v0, :cond_1

    .line 31
    const/4 v0, 0x2

    iget-object v1, p0, Lnuh;->b:Lnub;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 33
    :cond_1
    iget-object v0, p0, Lnuh;->c:Lnuk;

    if-eqz v0, :cond_2

    .line 34
    const/4 v0, 0x3

    iget-object v1, p0, Lnuh;->c:Lnuk;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 36
    :cond_2
    iget-object v0, p0, Lnuh;->d:Lnty;

    if-eqz v0, :cond_3

    .line 37
    const/4 v0, 0x4

    iget-object v1, p0, Lnuh;->d:Lnty;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 39
    :cond_3
    iget-object v0, p0, Lnuh;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 41
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lnuh;->a(Loxn;)Lnuh;

    move-result-object v0

    return-object v0
.end method
