.class public final Ldkt;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmds;",
        "Lmdt;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/Integer;

.field private final b:Ljava/lang/String;

.field private p:Lodt;


# direct methods
.method public constructor <init>(Landroid/content/Context;IILjava/lang/String;)V
    .locals 6

    .prologue
    .line 48
    const-string v3, "getvolumecontrols"

    new-instance v4, Lmds;

    invoke-direct {v4}, Lmds;-><init>()V

    new-instance v5, Lmdt;

    invoke-direct {v5}, Lmdt;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 51
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldkt;->a:Ljava/lang/Integer;

    .line 54
    iput-object p4, p0, Ldkt;->b:Ljava/lang/String;

    .line 55
    return-void
.end method


# virtual methods
.method protected a(Lmds;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 59
    iget-object v0, p0, Ldkt;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 60
    new-instance v0, Lods;

    invoke-direct {v0}, Lods;-><init>()V

    .line 61
    iget-object v1, p0, Ldkt;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v0, Lods;->b:I

    .line 62
    iget-object v1, p0, Ldkt;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v1, v3, :cond_2

    .line 63
    iget-object v1, p0, Ldkt;->b:Ljava/lang/String;

    iput-object v1, v0, Lods;->c:Ljava/lang/String;

    .line 70
    :cond_0
    :goto_0
    new-instance v1, Lnvj;

    invoke-direct {v1}, Lnvj;-><init>()V

    iput-object v1, p1, Lmds;->a:Lnvj;

    .line 71
    iget-object v1, p1, Lmds;->a:Lnvj;

    new-array v2, v3, [Lods;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    iput-object v2, v1, Lnvj;->a:[Lods;

    .line 73
    :cond_1
    return-void

    .line 64
    :cond_2
    iget-object v1, p0, Ldkt;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_3

    .line 65
    iget-object v1, p0, Ldkt;->b:Ljava/lang/String;

    iput-object v1, v0, Lods;->d:Ljava/lang/String;

    goto :goto_0

    .line 66
    :cond_3
    iget-object v1, p0, Ldkt;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    .line 67
    new-instance v0, Ljava/lang/IllegalArgumentException;

    iget-object v1, p0, Ldkt;->a:Ljava/lang/Integer;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1d

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Invalid volume control type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected a(Lmdt;)V
    .locals 7

    .prologue
    .line 77
    iget-object v0, p1, Lmdt;->a:Lnwd;

    iget-object v0, v0, Lnwd;->a:Lodt;

    iput-object v0, p0, Ldkt;->p:Lodt;

    .line 79
    iget-object v0, p0, Ldkt;->b:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ldkt;->p:Lodt;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ldkt;->p:Lodt;

    iget-object v0, v0, Lodt;->a:[Lodv;

    if-eqz v0, :cond_3

    .line 80
    iget-object v0, p0, Ldkt;->p:Lodt;

    iget-object v2, v0, Lodt;->a:[Lodv;

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_3

    aget-object v0, v2, v1

    .line 81
    iget-object v4, v0, Lodv;->b:Lods;

    .line 82
    if-eqz v4, :cond_0

    iget-object v5, v0, Lodv;->c:Lodw;

    if-eqz v5, :cond_0

    .line 83
    iget-object v5, v0, Lodv;->c:Lodw;

    .line 86
    iget v0, v4, Lods;->b:I

    const/4 v6, 0x4

    if-ne v0, v6, :cond_1

    iget-object v0, p0, Ldkt;->b:Ljava/lang/String;

    iget-object v6, v4, Lods;->d:Ljava/lang/String;

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 88
    iget-object v0, p0, Ldkt;->f:Landroid/content/Context;

    const-class v4, Lktq;

    invoke-static {v0, v4}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lktq;

    iget v4, p0, Ldkt;->c:I

    iget-object v6, p0, Ldkt;->b:Ljava/lang/String;

    .line 89
    invoke-interface {v0, v4, v6, v5}, Lktq;->a(ILjava/lang/String;Lodw;)V

    .line 80
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 90
    :cond_1
    iget v0, v4, Lods;->b:I

    const/4 v6, 0x1

    if-ne v0, v6, :cond_2

    iget-object v0, p0, Ldkt;->b:Ljava/lang/String;

    iget-object v6, v4, Lods;->c:Ljava/lang/String;

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 91
    iget-object v0, p0, Ldkt;->f:Landroid/content/Context;

    iget v4, p0, Ldkt;->c:I

    iget-object v6, p0, Ldkt;->b:Ljava/lang/String;

    invoke-static {v0, v4, v6, v5}, Ldsm;->a(Landroid/content/Context;ILjava/lang/String;Lodw;)V

    goto :goto_1

    .line 92
    :cond_2
    iget v0, v4, Lods;->b:I

    const/4 v4, 0x2

    if-ne v0, v4, :cond_0

    .line 93
    iget-object v0, p0, Ldkt;->f:Landroid/content/Context;

    iget v4, p0, Ldkt;->c:I

    const-string v6, "v.whatshot"

    invoke-static {v0, v4, v6, v5}, Ldsm;->a(Landroid/content/Context;ILjava/lang/String;Lodw;)V

    goto :goto_1

    .line 98
    :cond_3
    return-void
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 22
    check-cast p1, Lmds;

    invoke-virtual {p0, p1}, Ldkt;->a(Lmds;)V

    return-void
.end method

.method protected synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 22
    check-cast p1, Lmdt;

    invoke-virtual {p0, p1}, Ldkt;->a(Lmdt;)V

    return-void
.end method
