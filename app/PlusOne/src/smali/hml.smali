.class public final Lhml;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lhmk;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lhml;->a:Ljava/util/List;

    .line 27
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)Lhml;
    .locals 4

    .prologue
    .line 45
    const-class v0, Lhmm;

    invoke-static {p1, v0}, Llnh;->d(Landroid/content/Context;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v2

    .line 47
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 48
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhmm;

    invoke-interface {v0}, Lhmm;->ac_()Lhmk;

    move-result-object v0

    .line 49
    iget-object v3, p0, Lhml;->a:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 47
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 51
    :cond_0
    return-object p0
.end method

.method public a(Landroid/view/View;)Lhml;
    .locals 2

    .prologue
    .line 64
    invoke-virtual {p0, p1}, Lhml;->b(Landroid/view/View;)V

    .line 67
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 68
    :goto_0
    if-eqz v1, :cond_1

    .line 69
    instance-of v0, v1, Landroid/view/View;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 70
    check-cast v0, Landroid/view/View;

    invoke-virtual {p0, v0}, Lhml;->b(Landroid/view/View;)V

    .line 72
    :cond_0
    invoke-interface {v1}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    goto :goto_0

    .line 75
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0, v0}, Lhml;->a(Landroid/content/Context;)Lhml;

    .line 76
    return-object p0
.end method

.method public a(Lhmk;)Lhml;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lhml;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 34
    return-object p0
.end method

.method public a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lhmk;",
            ">;"
        }
    .end annotation

    .prologue
    .line 89
    iget-object v0, p0, Lhml;->a:Ljava/util/List;

    return-object v0
.end method

.method public a(Lhmp;)V
    .locals 0

    .prologue
    .line 93
    invoke-interface {p1, p0}, Lhmp;->a(Lhml;)V

    .line 94
    return-void
.end method

.method b(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 80
    instance-of v0, p1, Lhmm;

    if-eqz v0, :cond_1

    check-cast p1, Lhmm;

    .line 81
    invoke-interface {p1}, Lhmm;->ac_()Lhmk;

    move-result-object v0

    .line 83
    :goto_0
    if-eqz v0, :cond_0

    .line 84
    iget-object v1, p0, Lhml;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 86
    :cond_0
    return-void

    .line 82
    :cond_1
    invoke-static {p1}, Lhmo;->a(Landroid/view/View;)Lhmk;

    move-result-object v0

    goto :goto_0
.end method
