.class public abstract Lfwo;
.super Landroid/view/View;
.source "PG"

# interfaces
.implements Lkdd;
.implements Lljh;


# static fields
.field public static a:I

.field public static b:I

.field public static c:I

.field public static d:I

.field public static e:I

.field private static g:Z

.field private static h:Landroid/graphics/drawable/NinePatchDrawable;

.field private static i:Landroid/graphics/drawable/Drawable;

.field private static j:I

.field private static k:I

.field private static l:I


# instance fields
.field public f:Landroid/graphics/Rect;

.field private final m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Llip;",
            ">;"
        }
    .end annotation
.end field

.field private n:Llip;

.field private o:Landroid/view/View$OnClickListener;

.field private p:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 57
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 90
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 59
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfwo;->m:Ljava/util/List;

    .line 66
    iput-boolean v2, p0, Lfwo;->p:Z

    .line 92
    sget-boolean v0, Lfwo;->g:Z

    if-nez v0, :cond_0

    .line 93
    sput-boolean v2, Lfwo;->g:Z

    .line 95
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 97
    const v0, 0x7f020065

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/NinePatchDrawable;

    sput-object v0, Lfwo;->h:Landroid/graphics/drawable/NinePatchDrawable;

    .line 98
    const v0, 0x7f020415

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Lfwo;->i:Landroid/graphics/drawable/Drawable;

    .line 100
    const v0, 0x7f0d0246

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lfwo;->a:I

    .line 101
    const v0, 0x7f0d0247

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lfwo;->b:I

    .line 102
    const v0, 0x7f0d0248

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lfwo;->c:I

    .line 103
    const v0, 0x7f0d0249

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lfwo;->d:I

    .line 104
    const v0, 0x7f0d0244

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 106
    sput v0, Lfwo;->j:I

    shl-int/lit8 v0, v0, 0x1

    sput v0, Lfwo;->k:I

    .line 107
    const v0, 0x7f0d0245

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 109
    sput v0, Lfwo;->e:I

    shl-int/lit8 v0, v0, 0x1

    sput v0, Lfwo;->l:I

    .line 110
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 113
    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lfwo;->f:Landroid/graphics/Rect;

    .line 114
    invoke-virtual {p0, v2}, Lfwo;->setClickable(Z)V

    .line 115
    invoke-virtual {p0, v2}, Lfwo;->setFocusable(Z)V

    .line 116
    return-void
.end method


# virtual methods
.method protected abstract a(IIII)I
.end method

.method protected abstract a(Landroid/graphics/Canvas;II)I
.end method

.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 167
    invoke-virtual {p0}, Lfwo;->e()V

    .line 168
    iget-object v0, p0, Lfwo;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 169
    iput-object v1, p0, Lfwo;->n:Llip;

    .line 170
    iput-object v1, p0, Lfwo;->o:Landroid/view/View$OnClickListener;

    .line 171
    iget-object v0, p0, Lfwo;->f:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 173
    invoke-virtual {p0}, Lfwo;->clearAnimation()V

    .line 174
    return-void
.end method

.method public a(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 124
    iput-object p1, p0, Lfwo;->o:Landroid/view/View$OnClickListener;

    .line 125
    return-void
.end method

.method public a(Lkda;)V
    .locals 0

    .prologue
    .line 156
    invoke-virtual {p0}, Lfwo;->invalidate()V

    .line 157
    return-void
.end method

.method public a(Llip;)V
    .locals 1

    .prologue
    .line 280
    if-nez p1, :cond_0

    .line 285
    :goto_0
    return-void

    .line 283
    :cond_0
    iget-object v0, p0, Lfwo;->m:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 284
    iget-object v0, p0, Lfwo;->m:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 119
    iput-boolean p1, p0, Lfwo;->p:Z

    .line 120
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 130
    invoke-static {p0}, Llii;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 131
    invoke-virtual {p0}, Lfwo;->d()V

    .line 133
    :cond_0
    return-void
.end method

.method public b(Llip;)V
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Lfwo;->m:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 293
    return-void
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 137
    invoke-virtual {p0}, Lfwo;->e()V

    .line 138
    return-void
.end method

.method protected d()V
    .locals 0

    .prologue
    .line 145
    return-void
.end method

.method protected drawableStateChanged()V
    .locals 0

    .prologue
    .line 239
    invoke-super {p0}, Landroid/view/View;->drawableStateChanged()V

    .line 240
    invoke-virtual {p0}, Lfwo;->invalidate()V

    .line 241
    return-void
.end method

.method protected e()V
    .locals 0

    .prologue
    .line 152
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 0

    .prologue
    .line 245
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    .line 246
    invoke-virtual {p0}, Lfwo;->b()V

    .line 247
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 252
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 253
    invoke-virtual {p0}, Lfwo;->e()V

    .line 254
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 206
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 208
    invoke-virtual {p0}, Lfwo;->getWidth()I

    move-result v3

    .line 209
    invoke-virtual {p0}, Lfwo;->getHeight()I

    move-result v4

    .line 212
    iget-boolean v0, p0, Lfwo;->p:Z

    if-eqz v0, :cond_2

    .line 213
    sget v0, Lfwo;->j:I

    .line 214
    sget v2, Lfwo;->e:I

    .line 215
    sget v0, Lfwo;->k:I

    .line 216
    sget v0, Lfwo;->l:I

    .line 224
    :goto_0
    sget-object v5, Lfwo;->h:Landroid/graphics/drawable/NinePatchDrawable;

    iget-object v6, p0, Lfwo;->f:Landroid/graphics/Rect;

    invoke-virtual {v5, v6}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 225
    sget-object v5, Lfwo;->h:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual {v5, p1}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 227
    sget v5, Lfwo;->a:I

    sget v5, Lfwo;->c:I

    add-int/2addr v2, v5

    sget v5, Lfwo;->a:I

    sget v5, Lfwo;->b:I

    sget v5, Lfwo;->c:I

    add-int/2addr v0, v5

    sget v5, Lfwo;->d:I

    add-int/2addr v0, v5

    sub-int v0, v4, v0

    invoke-virtual {p0, p1, v2, v0}, Lfwo;->a(Landroid/graphics/Canvas;II)I

    .line 231
    invoke-virtual {p0}, Lfwo;->isPressed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lfwo;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lfwo;->n:Llip;

    if-nez v0, :cond_1

    .line 232
    sget-object v0, Lfwo;->i:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1, v1, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 233
    sget-object v0, Lfwo;->i:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 235
    :cond_1
    return-void

    :cond_2
    move v0, v1

    move v2, v1

    .line 220
    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 178
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 179
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 180
    if-gtz v0, :cond_0

    move v0, v1

    .line 183
    :cond_0
    iget-boolean v2, p0, Lfwo;->p:Z

    if-eqz v2, :cond_1

    .line 184
    sget v6, Lfwo;->j:I

    .line 185
    sget v5, Lfwo;->e:I

    .line 186
    sget v4, Lfwo;->k:I

    .line 187
    sget v2, Lfwo;->l:I

    .line 195
    :goto_0
    invoke-virtual {p0, v1, v0}, Lfwo;->setMeasuredDimension(II)V

    .line 197
    iget-object v7, p0, Lfwo;->f:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lfwo;->getMeasuredWidth()I

    move-result v8

    invoke-virtual {p0}, Lfwo;->getMeasuredHeight()I

    move-result v9

    invoke-virtual {v7, v3, v3, v8, v9}, Landroid/graphics/Rect;->set(IIII)V

    .line 199
    sget v3, Lfwo;->a:I

    add-int/2addr v3, v6

    sget v6, Lfwo;->c:I

    add-int/2addr v5, v6

    sget v6, Lfwo;->a:I

    add-int/2addr v4, v6

    sget v6, Lfwo;->b:I

    add-int/2addr v4, v6

    sub-int/2addr v1, v4

    sget v4, Lfwo;->c:I

    add-int/2addr v2, v4

    sget v4, Lfwo;->d:I

    add-int/2addr v2, v4

    sub-int/2addr v0, v2

    invoke-virtual {p0, v3, v5, v1, v0}, Lfwo;->a(IIII)I

    .line 202
    return-void

    :cond_1
    move v2, v3

    move v4, v3

    move v5, v3

    move v6, v3

    .line 192
    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 297
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v3, v0

    .line 298
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v4, v0

    .line 300
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 340
    :cond_0
    :goto_0
    :pswitch_0
    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    .line 302
    :pswitch_1
    iget-object v0, p0, Lfwo;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_1
    if-ltz v2, :cond_1

    .line 303
    iget-object v0, p0, Lfwo;->m:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llip;

    .line 304
    invoke-interface {v0, v3, v4, v1}, Llip;->a(III)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 305
    iput-object v0, p0, Lfwo;->n:Llip;

    .line 309
    :cond_1
    invoke-virtual {p0}, Lfwo;->invalidate()V

    goto :goto_0

    .line 302
    :cond_2
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_1

    .line 314
    :pswitch_2
    iget-object v0, p0, Lfwo;->n:Llip;

    if-eqz v0, :cond_3

    .line 315
    iput-object v2, p0, Lfwo;->n:Llip;

    .line 316
    invoke-virtual {p0, v1}, Lfwo;->setPressed(Z)V

    .line 317
    invoke-virtual {p0}, Lfwo;->invalidate()V

    .line 320
    :cond_3
    iget-object v0, p0, Lfwo;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v1

    move v1, v0

    :goto_2
    if-ltz v1, :cond_4

    .line 321
    iget-object v0, p0, Lfwo;->m:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llip;

    .line 322
    const/4 v5, 0x1

    invoke-interface {v0, v3, v4, v5}, Llip;->a(III)Z

    move-result v0

    or-int/2addr v2, v0

    .line 320
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_2

    .line 324
    :cond_4
    if-nez v2, :cond_0

    iget-object v0, p0, Lfwo;->o:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    .line 325
    iget-object v0, p0, Lfwo;->o:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    goto :goto_0

    .line 331
    :pswitch_3
    iget-object v0, p0, Lfwo;->n:Llip;

    if-eqz v0, :cond_0

    .line 332
    iget-object v0, p0, Lfwo;->n:Llip;

    const/4 v1, 0x3

    invoke-interface {v0, v3, v4, v1}, Llip;->a(III)Z

    .line 333
    iput-object v2, p0, Lfwo;->n:Llip;

    .line 334
    invoke-virtual {p0}, Lfwo;->invalidate()V

    goto :goto_0

    .line 300
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
