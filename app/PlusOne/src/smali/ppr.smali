.class public final Lppr;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lppr;


# instance fields
.field public b:Lppt;

.field public c:[Lpps;

.field private d:I

.field private e:Lpps;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/Integer;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/Integer;

.field private m:Lpow;

.field private n:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Lppr;

    sput-object v0, Lppr;->a:[Lppr;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 134
    const/high16 v0, -0x80000000

    iput v0, p0, Lppr;->d:I

    .line 137
    iput-object v1, p0, Lppr;->e:Lpps;

    .line 154
    iput-object v1, p0, Lppr;->b:Lppt;

    .line 157
    sget-object v0, Lpps;->a:[Lpps;

    iput-object v0, p0, Lppr;->c:[Lpps;

    .line 160
    iput-object v1, p0, Lppr;->m:Lpow;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 216
    .line 217
    iget v0, p0, Lppr;->d:I

    const/high16 v2, -0x80000000

    if-eq v0, v2, :cond_d

    .line 218
    const/4 v0, 0x1

    iget v2, p0, Lppr;->d:I

    .line 219
    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 221
    :goto_0
    iget-object v2, p0, Lppr;->e:Lpps;

    if-eqz v2, :cond_0

    .line 222
    const/4 v2, 0x2

    iget-object v3, p0, Lppr;->e:Lpps;

    .line 223
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 225
    :cond_0
    iget-object v2, p0, Lppr;->f:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 226
    const/4 v2, 0x3

    iget-object v3, p0, Lppr;->f:Ljava/lang/String;

    .line 227
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 229
    :cond_1
    iget-object v2, p0, Lppr;->g:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 230
    const/4 v2, 0x4

    iget-object v3, p0, Lppr;->g:Ljava/lang/String;

    .line 231
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 233
    :cond_2
    iget-object v2, p0, Lppr;->i:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 234
    const/4 v2, 0x5

    iget-object v3, p0, Lppr;->i:Ljava/lang/String;

    .line 235
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 237
    :cond_3
    iget-object v2, p0, Lppr;->k:Ljava/lang/String;

    if-eqz v2, :cond_4

    .line 238
    const/4 v2, 0x6

    iget-object v3, p0, Lppr;->k:Ljava/lang/String;

    .line 239
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 241
    :cond_4
    iget-object v2, p0, Lppr;->l:Ljava/lang/Integer;

    if-eqz v2, :cond_5

    .line 242
    const/4 v2, 0x7

    iget-object v3, p0, Lppr;->l:Ljava/lang/Integer;

    .line 243
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 245
    :cond_5
    iget-object v2, p0, Lppr;->b:Lppt;

    if-eqz v2, :cond_6

    .line 246
    const/16 v2, 0x8

    iget-object v3, p0, Lppr;->b:Lppt;

    .line 247
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 249
    :cond_6
    iget-object v2, p0, Lppr;->j:Ljava/lang/Integer;

    if-eqz v2, :cond_7

    .line 250
    const/16 v2, 0x9

    iget-object v3, p0, Lppr;->j:Ljava/lang/Integer;

    .line 251
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 253
    :cond_7
    iget-object v2, p0, Lppr;->c:[Lpps;

    if-eqz v2, :cond_9

    .line 254
    iget-object v2, p0, Lppr;->c:[Lpps;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_9

    aget-object v4, v2, v1

    .line 255
    if-eqz v4, :cond_8

    .line 256
    const/16 v5, 0xa

    .line 257
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 254
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 261
    :cond_9
    iget-object v1, p0, Lppr;->m:Lpow;

    if-eqz v1, :cond_a

    .line 262
    const/16 v1, 0xb

    iget-object v2, p0, Lppr;->m:Lpow;

    .line 263
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 265
    :cond_a
    iget-object v1, p0, Lppr;->h:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 266
    const/16 v1, 0xc

    iget-object v2, p0, Lppr;->h:Ljava/lang/String;

    .line 267
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 269
    :cond_b
    iget-object v1, p0, Lppr;->n:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 270
    const/16 v1, 0xd

    iget-object v2, p0, Lppr;->n:Ljava/lang/String;

    .line 271
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 273
    :cond_c
    iget-object v1, p0, Lppr;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 274
    iput v0, p0, Lppr;->ai:I

    .line 275
    return v0

    :cond_d
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lppr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 283
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 284
    sparse-switch v0, :sswitch_data_0

    .line 288
    iget-object v2, p0, Lppr;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 289
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lppr;->ah:Ljava/util/List;

    .line 292
    :cond_1
    iget-object v2, p0, Lppr;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 294
    :sswitch_0
    return-object p0

    .line 299
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 300
    if-eqz v0, :cond_2

    const/4 v2, 0x1

    if-ne v0, v2, :cond_3

    .line 302
    :cond_2
    iput v0, p0, Lppr;->d:I

    goto :goto_0

    .line 304
    :cond_3
    iput v1, p0, Lppr;->d:I

    goto :goto_0

    .line 309
    :sswitch_2
    iget-object v0, p0, Lppr;->e:Lpps;

    if-nez v0, :cond_4

    .line 310
    new-instance v0, Lpps;

    invoke-direct {v0}, Lpps;-><init>()V

    iput-object v0, p0, Lppr;->e:Lpps;

    .line 312
    :cond_4
    iget-object v0, p0, Lppr;->e:Lpps;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 316
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lppr;->f:Ljava/lang/String;

    goto :goto_0

    .line 320
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lppr;->g:Ljava/lang/String;

    goto :goto_0

    .line 324
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lppr;->i:Ljava/lang/String;

    goto :goto_0

    .line 328
    :sswitch_6
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lppr;->k:Ljava/lang/String;

    goto :goto_0

    .line 332
    :sswitch_7
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lppr;->l:Ljava/lang/Integer;

    goto :goto_0

    .line 336
    :sswitch_8
    iget-object v0, p0, Lppr;->b:Lppt;

    if-nez v0, :cond_5

    .line 337
    new-instance v0, Lppt;

    invoke-direct {v0}, Lppt;-><init>()V

    iput-object v0, p0, Lppr;->b:Lppt;

    .line 339
    :cond_5
    iget-object v0, p0, Lppr;->b:Lppt;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 343
    :sswitch_9
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lppr;->j:Ljava/lang/Integer;

    goto :goto_0

    .line 347
    :sswitch_a
    const/16 v0, 0x52

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 348
    iget-object v0, p0, Lppr;->c:[Lpps;

    if-nez v0, :cond_7

    move v0, v1

    .line 349
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lpps;

    .line 350
    iget-object v3, p0, Lppr;->c:[Lpps;

    if-eqz v3, :cond_6

    .line 351
    iget-object v3, p0, Lppr;->c:[Lpps;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 353
    :cond_6
    iput-object v2, p0, Lppr;->c:[Lpps;

    .line 354
    :goto_2
    iget-object v2, p0, Lppr;->c:[Lpps;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_8

    .line 355
    iget-object v2, p0, Lppr;->c:[Lpps;

    new-instance v3, Lpps;

    invoke-direct {v3}, Lpps;-><init>()V

    aput-object v3, v2, v0

    .line 356
    iget-object v2, p0, Lppr;->c:[Lpps;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 357
    invoke-virtual {p1}, Loxn;->a()I

    .line 354
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 348
    :cond_7
    iget-object v0, p0, Lppr;->c:[Lpps;

    array-length v0, v0

    goto :goto_1

    .line 360
    :cond_8
    iget-object v2, p0, Lppr;->c:[Lpps;

    new-instance v3, Lpps;

    invoke-direct {v3}, Lpps;-><init>()V

    aput-object v3, v2, v0

    .line 361
    iget-object v2, p0, Lppr;->c:[Lpps;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 365
    :sswitch_b
    iget-object v0, p0, Lppr;->m:Lpow;

    if-nez v0, :cond_9

    .line 366
    new-instance v0, Lpow;

    invoke-direct {v0}, Lpow;-><init>()V

    iput-object v0, p0, Lppr;->m:Lpow;

    .line 368
    :cond_9
    iget-object v0, p0, Lppr;->m:Lpow;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 372
    :sswitch_c
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lppr;->h:Ljava/lang/String;

    goto/16 :goto_0

    .line 376
    :sswitch_d
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lppr;->n:Ljava/lang/String;

    goto/16 :goto_0

    .line 284
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 167
    iget v0, p0, Lppr;->d:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 168
    const/4 v0, 0x1

    iget v1, p0, Lppr;->d:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 170
    :cond_0
    iget-object v0, p0, Lppr;->e:Lpps;

    if-eqz v0, :cond_1

    .line 171
    const/4 v0, 0x2

    iget-object v1, p0, Lppr;->e:Lpps;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 173
    :cond_1
    iget-object v0, p0, Lppr;->f:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 174
    const/4 v0, 0x3

    iget-object v1, p0, Lppr;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 176
    :cond_2
    iget-object v0, p0, Lppr;->g:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 177
    const/4 v0, 0x4

    iget-object v1, p0, Lppr;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 179
    :cond_3
    iget-object v0, p0, Lppr;->i:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 180
    const/4 v0, 0x5

    iget-object v1, p0, Lppr;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 182
    :cond_4
    iget-object v0, p0, Lppr;->k:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 183
    const/4 v0, 0x6

    iget-object v1, p0, Lppr;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 185
    :cond_5
    iget-object v0, p0, Lppr;->l:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 186
    const/4 v0, 0x7

    iget-object v1, p0, Lppr;->l:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 188
    :cond_6
    iget-object v0, p0, Lppr;->b:Lppt;

    if-eqz v0, :cond_7

    .line 189
    const/16 v0, 0x8

    iget-object v1, p0, Lppr;->b:Lppt;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 191
    :cond_7
    iget-object v0, p0, Lppr;->j:Ljava/lang/Integer;

    if-eqz v0, :cond_8

    .line 192
    const/16 v0, 0x9

    iget-object v1, p0, Lppr;->j:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 194
    :cond_8
    iget-object v0, p0, Lppr;->c:[Lpps;

    if-eqz v0, :cond_a

    .line 195
    iget-object v1, p0, Lppr;->c:[Lpps;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_a

    aget-object v3, v1, v0

    .line 196
    if-eqz v3, :cond_9

    .line 197
    const/16 v4, 0xa

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 195
    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 201
    :cond_a
    iget-object v0, p0, Lppr;->m:Lpow;

    if-eqz v0, :cond_b

    .line 202
    const/16 v0, 0xb

    iget-object v1, p0, Lppr;->m:Lpow;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 204
    :cond_b
    iget-object v0, p0, Lppr;->h:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 205
    const/16 v0, 0xc

    iget-object v1, p0, Lppr;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 207
    :cond_c
    iget-object v0, p0, Lppr;->n:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 208
    const/16 v0, 0xd

    iget-object v1, p0, Lppr;->n:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 210
    :cond_d
    iget-object v0, p0, Lppr;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 212
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lppr;->a(Loxn;)Lppr;

    move-result-object v0

    return-object v0
.end method
