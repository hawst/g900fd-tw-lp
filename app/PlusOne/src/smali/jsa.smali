.class public final Ljsa;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljqh;
.implements Ljrl;


# instance fields
.field private final a:Ljrm;

.field private final b:Ljpa;

.field private final c:I

.field private final d:Ljqc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljqc",
            "<",
            "Ljod;",
            ">;"
        }
    .end annotation
.end field

.field private e:Z

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljod;",
            "Ljava/util/List",
            "<",
            "Ljpv;",
            ">;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljrm;Ljpa;ILjqc;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljrm;",
            "Ljpa;",
            "I",
            "Ljqc",
            "<",
            "Ljod;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Ljsa;->a:Ljrm;

    .line 31
    iput-object p2, p0, Ljsa;->b:Ljpa;

    .line 32
    iput p3, p0, Ljsa;->c:I

    .line 33
    iput-object p4, p0, Ljsa;->d:Ljqc;

    .line 34
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljsa;->e:Z

    .line 35
    return-void
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljod;",
            "Ljava/util/List",
            "<",
            "Ljpv;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 51
    iget-object v0, p0, Ljsa;->f:Ljava/util/List;

    return-object v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Ljsa;->a:Ljrm;

    invoke-virtual {v0, p0}, Ljrm;->b(Ljrl;)V

    .line 57
    return-void
.end method

.method public c()V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 39
    iget-object v0, p0, Ljsa;->a:Ljrm;

    invoke-virtual {v0}, Ljrm;->a()Ljava/util/List;

    move-result-object v5

    iget-object v0, p0, Ljsa;->a:Ljrm;

    invoke-virtual {v0}, Ljrm;->d()Ljava/util/HashMap;

    move-result-object v6

    if-eqz v5, :cond_0

    if-nez v6, :cond_1

    .line 41
    :cond_0
    iget-boolean v0, p0, Ljsa;->e:Z

    if-eqz v0, :cond_3

    .line 42
    iput-boolean v3, p0, Ljsa;->e:Z

    .line 43
    iget-object v0, p0, Ljsa;->b:Ljpa;

    invoke-interface {v0, p0}, Ljpa;->a(Ljqh;)V

    .line 47
    :goto_0
    return-void

    .line 39
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ljsa;->f:Ljava/util/List;

    move v2, v3

    :goto_1
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljod;

    iget-object v1, p0, Ljsa;->d:Ljqc;

    invoke-interface {v1, v0}, Ljqc;->a(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljod;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    if-eqz v1, :cond_4

    iget v4, p0, Ljsa;->c:I

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v7

    invoke-static {v4, v7}, Ljava/lang/Math;->min(II)I

    move-result v4

    invoke-interface {v1, v3, v4}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v1

    :goto_2
    iget-object v4, p0, Ljsa;->f:Ljava/util/List;

    new-instance v7, Landroid/util/Pair;

    invoke-direct {v7, v0, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 45
    :cond_3
    iget-object v0, p0, Ljsa;->b:Ljpa;

    goto :goto_0

    :cond_4
    move-object v1, v4

    goto :goto_2
.end method
