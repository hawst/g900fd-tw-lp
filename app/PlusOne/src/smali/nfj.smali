.class public final Lnfj;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnzx;

.field private b:Lnym;

.field private c:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 106
    invoke-direct {p0}, Loxq;-><init>()V

    .line 109
    iput-object v0, p0, Lnfj;->b:Lnym;

    .line 112
    iput-object v0, p0, Lnfj;->a:Lnzx;

    .line 106
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 134
    const/4 v0, 0x0

    .line 135
    iget-object v1, p0, Lnfj;->b:Lnym;

    if-eqz v1, :cond_0

    .line 136
    const/4 v0, 0x1

    iget-object v1, p0, Lnfj;->b:Lnym;

    .line 137
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 139
    :cond_0
    iget-object v1, p0, Lnfj;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 140
    const/4 v1, 0x2

    iget-object v2, p0, Lnfj;->c:Ljava/lang/Boolean;

    .line 141
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 143
    :cond_1
    iget-object v1, p0, Lnfj;->a:Lnzx;

    if-eqz v1, :cond_2

    .line 144
    const/4 v1, 0x3

    iget-object v2, p0, Lnfj;->a:Lnzx;

    .line 145
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 147
    :cond_2
    iget-object v1, p0, Lnfj;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 148
    iput v0, p0, Lnfj;->ai:I

    .line 149
    return v0
.end method

.method public a(Loxn;)Lnfj;
    .locals 2

    .prologue
    .line 157
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 158
    sparse-switch v0, :sswitch_data_0

    .line 162
    iget-object v1, p0, Lnfj;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 163
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnfj;->ah:Ljava/util/List;

    .line 166
    :cond_1
    iget-object v1, p0, Lnfj;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 168
    :sswitch_0
    return-object p0

    .line 173
    :sswitch_1
    iget-object v0, p0, Lnfj;->b:Lnym;

    if-nez v0, :cond_2

    .line 174
    new-instance v0, Lnym;

    invoke-direct {v0}, Lnym;-><init>()V

    iput-object v0, p0, Lnfj;->b:Lnym;

    .line 176
    :cond_2
    iget-object v0, p0, Lnfj;->b:Lnym;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 180
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnfj;->c:Ljava/lang/Boolean;

    goto :goto_0

    .line 184
    :sswitch_3
    iget-object v0, p0, Lnfj;->a:Lnzx;

    if-nez v0, :cond_3

    .line 185
    new-instance v0, Lnzx;

    invoke-direct {v0}, Lnzx;-><init>()V

    iput-object v0, p0, Lnfj;->a:Lnzx;

    .line 187
    :cond_3
    iget-object v0, p0, Lnfj;->a:Lnzx;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 158
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 119
    iget-object v0, p0, Lnfj;->b:Lnym;

    if-eqz v0, :cond_0

    .line 120
    const/4 v0, 0x1

    iget-object v1, p0, Lnfj;->b:Lnym;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 122
    :cond_0
    iget-object v0, p0, Lnfj;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 123
    const/4 v0, 0x2

    iget-object v1, p0, Lnfj;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 125
    :cond_1
    iget-object v0, p0, Lnfj;->a:Lnzx;

    if-eqz v0, :cond_2

    .line 126
    const/4 v0, 0x3

    iget-object v1, p0, Lnfj;->a:Lnzx;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 128
    :cond_2
    iget-object v0, p0, Lnfj;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 130
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 102
    invoke-virtual {p0, p1}, Lnfj;->a(Loxn;)Lnfj;

    move-result-object v0

    return-object v0
.end method
