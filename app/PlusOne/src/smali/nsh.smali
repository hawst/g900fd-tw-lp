.class public final Lnsh;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnsh;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:I

.field public f:Lnsa;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/Boolean;

.field private i:Ljava/lang/Boolean;

.field private j:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 820
    const/4 v0, 0x0

    new-array v0, v0, [Lnsh;

    sput-object v0, Lnsh;->a:[Lnsh;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 821
    invoke-direct {p0}, Loxq;-><init>()V

    .line 832
    const/high16 v0, -0x80000000

    iput v0, p0, Lnsh;->e:I

    .line 839
    const/4 v0, 0x0

    iput-object v0, p0, Lnsh;->f:Lnsa;

    .line 821
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 879
    const/4 v0, 0x0

    .line 880
    iget-object v1, p0, Lnsh;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 881
    const/4 v0, 0x1

    iget-object v1, p0, Lnsh;->b:Ljava/lang/String;

    .line 882
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 884
    :cond_0
    iget-object v1, p0, Lnsh;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 885
    const/4 v1, 0x2

    iget-object v2, p0, Lnsh;->c:Ljava/lang/String;

    .line 886
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 888
    :cond_1
    iget-object v1, p0, Lnsh;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 889
    const/4 v1, 0x3

    iget-object v2, p0, Lnsh;->d:Ljava/lang/String;

    .line 890
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 892
    :cond_2
    iget-object v1, p0, Lnsh;->g:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 893
    const/4 v1, 0x4

    iget-object v2, p0, Lnsh;->g:Ljava/lang/String;

    .line 894
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 896
    :cond_3
    iget v1, p0, Lnsh;->e:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_4

    .line 897
    const/4 v1, 0x5

    iget v2, p0, Lnsh;->e:I

    .line 898
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 900
    :cond_4
    iget-object v1, p0, Lnsh;->h:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    .line 901
    const/4 v1, 0x7

    iget-object v2, p0, Lnsh;->h:Ljava/lang/Boolean;

    .line 902
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 904
    :cond_5
    iget-object v1, p0, Lnsh;->i:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    .line 905
    const/16 v1, 0x8

    iget-object v2, p0, Lnsh;->i:Ljava/lang/Boolean;

    .line 906
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 908
    :cond_6
    iget-object v1, p0, Lnsh;->f:Lnsa;

    if-eqz v1, :cond_7

    .line 909
    const/16 v1, 0x9

    iget-object v2, p0, Lnsh;->f:Lnsa;

    .line 910
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 912
    :cond_7
    iget-object v1, p0, Lnsh;->j:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    .line 913
    const/16 v1, 0xa

    iget-object v2, p0, Lnsh;->j:Ljava/lang/Boolean;

    .line 914
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 916
    :cond_8
    iget-object v1, p0, Lnsh;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 917
    iput v0, p0, Lnsh;->ai:I

    .line 918
    return v0
.end method

.method public a(Loxn;)Lnsh;
    .locals 2

    .prologue
    .line 926
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 927
    sparse-switch v0, :sswitch_data_0

    .line 931
    iget-object v1, p0, Lnsh;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 932
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnsh;->ah:Ljava/util/List;

    .line 935
    :cond_1
    iget-object v1, p0, Lnsh;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 937
    :sswitch_0
    return-object p0

    .line 942
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnsh;->b:Ljava/lang/String;

    goto :goto_0

    .line 946
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnsh;->c:Ljava/lang/String;

    goto :goto_0

    .line 950
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnsh;->d:Ljava/lang/String;

    goto :goto_0

    .line 954
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnsh;->g:Ljava/lang/String;

    goto :goto_0

    .line 958
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 959
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-ne v0, v1, :cond_3

    .line 967
    :cond_2
    iput v0, p0, Lnsh;->e:I

    goto :goto_0

    .line 969
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lnsh;->e:I

    goto :goto_0

    .line 974
    :sswitch_6
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnsh;->h:Ljava/lang/Boolean;

    goto :goto_0

    .line 978
    :sswitch_7
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnsh;->i:Ljava/lang/Boolean;

    goto :goto_0

    .line 982
    :sswitch_8
    iget-object v0, p0, Lnsh;->f:Lnsa;

    if-nez v0, :cond_4

    .line 983
    new-instance v0, Lnsa;

    invoke-direct {v0}, Lnsa;-><init>()V

    iput-object v0, p0, Lnsh;->f:Lnsa;

    .line 985
    :cond_4
    iget-object v0, p0, Lnsh;->f:Lnsa;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 989
    :sswitch_9
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnsh;->j:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 927
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x38 -> :sswitch_6
        0x40 -> :sswitch_7
        0x4a -> :sswitch_8
        0x50 -> :sswitch_9
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 846
    iget-object v0, p0, Lnsh;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 847
    const/4 v0, 0x1

    iget-object v1, p0, Lnsh;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 849
    :cond_0
    iget-object v0, p0, Lnsh;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 850
    const/4 v0, 0x2

    iget-object v1, p0, Lnsh;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 852
    :cond_1
    iget-object v0, p0, Lnsh;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 853
    const/4 v0, 0x3

    iget-object v1, p0, Lnsh;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 855
    :cond_2
    iget-object v0, p0, Lnsh;->g:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 856
    const/4 v0, 0x4

    iget-object v1, p0, Lnsh;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 858
    :cond_3
    iget v0, p0, Lnsh;->e:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_4

    .line 859
    const/4 v0, 0x5

    iget v1, p0, Lnsh;->e:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 861
    :cond_4
    iget-object v0, p0, Lnsh;->h:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 862
    const/4 v0, 0x7

    iget-object v1, p0, Lnsh;->h:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 864
    :cond_5
    iget-object v0, p0, Lnsh;->i:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    .line 865
    const/16 v0, 0x8

    iget-object v1, p0, Lnsh;->i:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 867
    :cond_6
    iget-object v0, p0, Lnsh;->f:Lnsa;

    if-eqz v0, :cond_7

    .line 868
    const/16 v0, 0x9

    iget-object v1, p0, Lnsh;->f:Lnsa;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 870
    :cond_7
    iget-object v0, p0, Lnsh;->j:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    .line 871
    const/16 v0, 0xa

    iget-object v1, p0, Lnsh;->j:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 873
    :cond_8
    iget-object v0, p0, Lnsh;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 875
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 817
    invoke-virtual {p0, p1}, Lnsh;->a(Loxn;)Lnsh;

    move-result-object v0

    return-object v0
.end method
