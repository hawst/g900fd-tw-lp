.class public final Lewf;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhtl;
.implements Llnx;
.implements Llrg;


# instance fields
.field private a:Landroid/content/Context;

.field private final b:Lae;

.field private c:Lhee;


# direct methods
.method constructor <init>(Landroid/app/Activity;Llqr;)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    check-cast p1, Lz;

    invoke-virtual {p1}, Lz;->f()Lae;

    move-result-object v0

    iput-object v0, p0, Lewf;->b:Lae;

    .line 40
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 41
    return-void
.end method

.method constructor <init>(Lu;Llqr;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    invoke-virtual {p1}, Lu;->p()Lae;

    move-result-object v0

    iput-object v0, p0, Lewf;->b:Lae;

    .line 35
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 36
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Llnh;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 45
    const-class v0, Lhee;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Lewf;->c:Lhee;

    .line 46
    iput-object p1, p0, Lewf;->a:Landroid/content/Context;

    .line 47
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLhtm;[B)V
    .locals 5

    .prologue
    .line 53
    invoke-static {p1}, Lcom/google/android/apps/plus/service/EsService;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 54
    iget-object v0, p0, Lewf;->c:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v1

    .line 55
    if-eqz p5, :cond_1

    .line 56
    iget-object v0, p0, Lewf;->a:Landroid/content/Context;

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/plus/service/EsService;->d(Landroid/content/Context;ILjava/lang/String;)I

    .line 72
    :cond_0
    :goto_0
    return-void

    .line 59
    :cond_1
    if-eqz p6, :cond_3

    .line 60
    iget-object v0, p0, Lewf;->a:Landroid/content/Context;

    iget-object v2, p0, Lewf;->c:Lhee;

    .line 61
    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    .line 60
    if-eqz p6, :cond_2

    invoke-static {v0, p4, v2}, Lfva;->a(Landroid/content/Context;ZI)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p6}, Lhtm;->m()V

    const/4 v0, 0x1

    .line 66
    :goto_1
    iget-object v2, p0, Lewf;->a:Landroid/content/Context;

    invoke-static {v2, v1, p1, v0, p7}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILjava/lang/String;Z[B)I

    .line 68
    iget-object v0, p0, Lewf;->a:Landroid/content/Context;

    iget-object v1, p0, Lewf;->c:Lhee;

    .line 69
    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    .line 68
    invoke-static {v0, v1, p2, p3}, Lcom/google/android/apps/plus/phone/AddProfilePhotoSpringboardActivity;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 60
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 63
    :cond_3
    iget-object v0, p0, Lewf;->a:Landroid/content/Context;

    iget-object v2, p0, Lewf;->b:Lae;

    iget-object v3, p0, Lewf;->c:Lhee;

    .line 64
    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    const-string v4, "plus_one_promo"

    .line 63
    invoke-static {v0, v2, v3, p4, v4}, Lfva;->a(Landroid/content/Context;Lae;IZLjava/lang/String;)Z

    move-result v0

    goto :goto_1
.end method
