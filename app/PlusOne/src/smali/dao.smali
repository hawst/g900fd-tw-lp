.class final Ldao;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ldax;


# instance fields
.field final synthetic a:Ldac;


# direct methods
.method constructor <init>(Ldac;)V
    .locals 0

    .prologue
    .line 279
    iput-object p1, p0, Ldao;->a:Ldac;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 282
    iget-object v0, p0, Ldao;->a:Ldac;

    invoke-static {v0}, Ldac;->f(Ldac;)Ldgr;

    move-result-object v0

    sget-object v1, Ldgx;->N:Ldgo;

    invoke-virtual {v0, v1}, Ldgr;->d(Ldgu;)V

    .line 283
    return-void
.end method

.method public a(Ljava/lang/String;I)V
    .locals 4

    .prologue
    .line 308
    new-instance v0, Lemv;

    invoke-direct {v0}, Lemv;-><init>()V

    .line 309
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 310
    iget-object v2, p0, Ldao;->a:Ldac;

    invoke-static {v2}, Ldac;->i(Ldac;)Lhee;

    move-result-object v2

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    .line 311
    const-string v3, "account_id"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 312
    const-string v2, "plus_one_id"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    const-string v2, "total_plus_ones"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 314
    invoke-virtual {v0, v1}, Lemv;->f(Landroid/os/Bundle;)V

    .line 315
    iget-object v1, p0, Ldao;->a:Ldac;

    invoke-virtual {v1}, Ldac;->p()Lae;

    move-result-object v1

    const-string v2, "comments_dialog_plus_ones"

    invoke-virtual {v0, v1, v2}, Lemv;->a(Lae;Ljava/lang/String;)V

    .line 316
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 287
    iget-object v0, p0, Ldao;->a:Ldac;

    invoke-static {v0}, Ldac;->f(Ldac;)Ldgr;

    move-result-object v0

    sget-object v1, Ldgx;->O:Ldgo;

    invoke-virtual {v0, v1}, Ldgr;->d(Ldgu;)V

    .line 288
    return-void
.end method

.method public c()V
    .locals 4

    .prologue
    .line 292
    iget-object v0, p0, Ldao;->a:Ldac;

    invoke-static {v0}, Ldac;->b(Ldac;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 304
    :goto_0
    return-void

    .line 296
    :cond_0
    iget-object v0, p0, Ldao;->a:Ldac;

    invoke-static {v0}, Ldac;->g(Ldac;)Ldeo;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ldeo;->c(Z)V

    .line 297
    invoke-static {}, Llsx;->d()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Ldap;

    invoke-direct {v1, p0}, Ldap;-><init>(Ldao;)V

    const-wide/16 v2, 0x96

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
