.class public final Lnsa;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnsa;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3185
    const/4 v0, 0x0

    new-array v0, v0, [Lnsa;

    sput-object v0, Lnsa;->a:[Lnsa;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3186
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 3207
    const/4 v0, 0x0

    .line 3208
    iget-object v1, p0, Lnsa;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3209
    const/4 v0, 0x1

    iget-object v1, p0, Lnsa;->b:Ljava/lang/String;

    .line 3210
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3212
    :cond_0
    iget-object v1, p0, Lnsa;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 3213
    const/4 v1, 0x2

    iget-object v2, p0, Lnsa;->c:Ljava/lang/String;

    .line 3214
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3216
    :cond_1
    iget-object v1, p0, Lnsa;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3217
    iput v0, p0, Lnsa;->ai:I

    .line 3218
    return v0
.end method

.method public a(Loxn;)Lnsa;
    .locals 2

    .prologue
    .line 3226
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3227
    sparse-switch v0, :sswitch_data_0

    .line 3231
    iget-object v1, p0, Lnsa;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 3232
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnsa;->ah:Ljava/util/List;

    .line 3235
    :cond_1
    iget-object v1, p0, Lnsa;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3237
    :sswitch_0
    return-object p0

    .line 3242
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnsa;->b:Ljava/lang/String;

    goto :goto_0

    .line 3246
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnsa;->c:Ljava/lang/String;

    goto :goto_0

    .line 3227
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 3195
    iget-object v0, p0, Lnsa;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 3196
    const/4 v0, 0x1

    iget-object v1, p0, Lnsa;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3198
    :cond_0
    iget-object v0, p0, Lnsa;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 3199
    const/4 v0, 0x2

    iget-object v1, p0, Lnsa;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3201
    :cond_1
    iget-object v0, p0, Lnsa;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3203
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3182
    invoke-virtual {p0, p1}, Lnsa;->a(Loxn;)Lnsa;

    move-result-object v0

    return-object v0
.end method
