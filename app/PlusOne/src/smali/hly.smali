.class public Lhly;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhlv;


# instance fields
.field private a:I

.field private b:Lhml;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILhml;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput p1, p0, Lhly;->a:I

    .line 30
    iput-object p2, p0, Lhly;->b:Lhml;

    .line 31
    return-void
.end method

.method public static a(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 39
    new-instance v0, Lhml;

    invoke-direct {v0}, Lhml;-><init>()V

    invoke-virtual {v0, p0}, Lhml;->a(Landroid/content/Context;)Lhml;

    move-result-object v0

    invoke-static {p0, p1, v0}, Lhly;->a(Landroid/content/Context;ILhml;)V

    .line 40
    return-void
.end method

.method public static a(Landroid/content/Context;ILhml;)V
    .locals 1

    .prologue
    .line 60
    new-instance v0, Lhly;

    invoke-direct {v0, p1, p2}, Lhly;-><init>(ILhml;)V

    invoke-virtual {v0, p0}, Lhly;->a(Landroid/content/Context;)V

    .line 61
    return-void
.end method

.method public static a(Landroid/view/View;I)V
    .locals 2

    .prologue
    .line 48
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Lhml;

    invoke-direct {v1}, Lhml;-><init>()V

    invoke-virtual {v1, p0}, Lhml;->a(Landroid/view/View;)Lhml;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lhly;->a(Landroid/content/Context;ILhml;)V

    .line 49
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 86
    iget v0, p0, Lhly;->a:I

    return v0
.end method

.method public a(Ljava/lang/String;)Lhly;
    .locals 0

    .prologue
    .line 81
    iput-object p1, p0, Lhly;->c:Ljava/lang/String;

    .line 82
    return-object p0
.end method

.method public a(Landroid/content/Context;Lhlx;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lhly;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhly;->c:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p2, p1}, Lhlx;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 67
    const-class v0, Lhlw;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhlw;

    invoke-interface {v0, p1, p0}, Lhlw;->a(Landroid/content/Context;Lhlv;)V

    .line 68
    return-void
.end method

.method public a(Lhmp;)V
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lhly;->b:Lhml;

    invoke-virtual {v0, p1}, Lhml;->a(Lhmp;)V

    .line 73
    return-void
.end method

.method public b()Lhml;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lhly;->b:Lhml;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 95
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    const-string v1, "UserEvent action: %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lhly;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
