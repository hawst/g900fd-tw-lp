.class final Leeh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private synthetic a:Leef;


# direct methods
.method constructor <init>(Leef;)V
    .locals 0

    .prologue
    .line 299
    iput-object p1, p0, Leeh;->a:Leef;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 303
    const v0, 0x7f10006d

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 304
    iget-object v1, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 305
    new-instance v2, Landroid/content/ComponentName;

    iget-object v3, v1, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v1, v1, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v2, v3, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    iget-object v1, p0, Leeh;->a:Leef;

    invoke-static {v1, v0}, Leef;->a(Leef;Landroid/content/pm/ResolveInfo;)Z

    move-result v1

    .line 308
    iget-object v3, p0, Leeh;->a:Leef;

    invoke-virtual {v3}, Leef;->n()Lz;

    move-result-object v3

    .line 309
    if-eqz v1, :cond_1

    iget-object v4, p0, Leeh;->a:Leef;

    invoke-static {v4}, Leef;->g(Leef;)Lhee;

    move-result-object v4

    invoke-interface {v4}, Lhee;->f()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Leeh;->a:Leef;

    .line 310
    invoke-static {v4}, Leef;->g(Leef;)Lhee;

    move-result-object v4

    invoke-interface {v4}, Lhee;->g()Lhej;

    move-result-object v4

    const-string v5, "is_google_plus"

    invoke-interface {v4, v5}, Lhej;->c(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 311
    :cond_0
    iget-object v0, p0, Leeh;->a:Leef;

    new-instance v1, Livy;

    iget-object v2, p0, Leeh;->a:Leef;

    invoke-static {v2}, Leef;->i(Leef;)Llnl;

    move-result-object v2

    invoke-direct {v1, v2}, Livy;-><init>(Landroid/content/Context;)V

    new-instance v2, Liwg;

    invoke-direct {v2}, Liwg;-><init>()V

    const-class v4, Liwl;

    .line 313
    invoke-virtual {v2, v4}, Liwg;->b(Ljava/lang/Class;)Liwg;

    move-result-object v2

    const-class v4, Lixj;

    .line 314
    invoke-virtual {v2, v4}, Liwg;->a(Ljava/lang/Class;)Liwg;

    move-result-object v2

    .line 312
    invoke-virtual {v1, v2}, Livy;->a(Liwg;)Livy;

    move-result-object v1

    iget-object v2, p0, Leeh;->a:Leef;

    .line 315
    invoke-static {v2, v3}, Leef;->a(Leef;Landroid/app/Activity;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Livy;->a(Landroid/content/Intent;)Livy;

    move-result-object v1

    .line 316
    invoke-virtual {v1}, Livy;->a()Landroid/content/Intent;

    move-result-object v1

    .line 311
    invoke-static {v0, v1}, Leef;->a(Leef;Landroid/content/Intent;)Landroid/content/Intent;

    .line 317
    iget-object v0, p0, Leeh;->a:Leef;

    invoke-static {v0}, Leef;->c(Leef;)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Leeh;->a:Leef;

    invoke-static {v1}, Leef;->b(Leef;)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 366
    :goto_0
    return-void

    .line 320
    :cond_1
    if-eqz v1, :cond_2

    .line 321
    iget-object v0, p0, Leeh;->a:Leef;

    invoke-static {v0, v6}, Leef;->a(Leef;Lhgw;)V

    goto :goto_0

    .line 322
    :cond_2
    iget-object v1, p0, Leeh;->a:Leef;

    invoke-static {v1, v2}, Leef;->a(Leef;Landroid/content/ComponentName;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 323
    iget-object v1, p0, Leeh;->a:Leef;

    new-instance v2, Leer;

    iget-object v3, p0, Leeh;->a:Leef;

    invoke-direct {v2}, Leer;-><init>()V

    invoke-static {v1, v2}, Leef;->a(Leef;Leer;)Leer;

    .line 324
    iget-object v1, p0, Leeh;->a:Leef;

    invoke-static {v1}, Leef;->j(Leef;)Ljava/util/ArrayList;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljuf;

    .line 325
    iget-object v2, p0, Leeh;->a:Leef;

    invoke-static {v2}, Leef;->k(Leef;)Leer;

    move-result-object v2

    .line 326
    invoke-interface {v1}, Ljuf;->f()Lizu;

    move-result-object v1

    invoke-virtual {v1}, Lizu;->c()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v2, v1}, Leer;->a(Ljava/lang/Long;)Leer;

    move-result-object v1

    .line 327
    invoke-virtual {v1, p1}, Leer;->a(Landroid/view/View;)Leer;

    move-result-object v1

    .line 328
    invoke-virtual {v1, v0}, Leer;->a(Landroid/content/pm/ResolveInfo;)Leer;

    .line 330
    iget-object v0, p0, Leeh;->a:Leef;

    invoke-static {v0}, Leef;->k(Leef;)Leer;

    move-result-object v0

    invoke-virtual {v0}, Leer;->a()Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Leeh;->a:Leef;

    invoke-static {v0}, Leef;->k(Leef;)Leer;

    move-result-object v0

    invoke-virtual {v0}, Leer;->a()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_4

    .line 332
    :cond_3
    iget-object v0, p0, Leeh;->a:Leef;

    invoke-virtual {v0}, Leef;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x4

    iget-object v2, p0, Leeh;->a:Leef;

    invoke-static {v2}, Leef;->l(Leef;)Lbc;

    move-result-object v2

    invoke-virtual {v0, v1, v6, v2}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    goto :goto_0

    .line 336
    :cond_4
    iget-object v0, p0, Leeh;->a:Leef;

    invoke-static {v0}, Leef;->m(Leef;)V

    goto :goto_0

    .line 339
    :cond_5
    iget-object v1, p0, Leeh;->a:Leef;

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Leef;->a(Leef;Ljava/lang/String;)V

    .line 340
    const v1, 0x7f10006c

    invoke-virtual {p1, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    .line 342
    iget-object v4, p0, Leeh;->a:Leef;

    invoke-static {v4, v0}, Leef;->b(Leef;Landroid/content/pm/ResolveInfo;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Leeh;->a:Leef;

    invoke-static {v0}, Leef;->g(Leef;)Lhee;

    move-result-object v0

    invoke-interface {v0}, Lhee;->f()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 343
    iget-object v0, p0, Leeh;->a:Leef;

    invoke-static {v0}, Leef;->n(Leef;)Llnh;

    move-result-object v0

    const-class v4, Lhxp;

    invoke-virtual {v0, v4}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhxp;

    .line 344
    if-eqz v0, :cond_6

    .line 345
    invoke-interface {v0}, Lhxp;->d()Landroid/content/Intent;

    move-result-object v0

    .line 346
    const-string v1, "account_id"

    iget-object v2, p0, Leeh;->a:Leef;

    .line 347
    invoke-static {v2}, Leef;->g(Leef;)Lhee;

    move-result-object v2

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    .line 346
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 348
    iget-object v1, p0, Leeh;->a:Leef;

    invoke-virtual {v1, v0}, Leef;->a(Landroid/content/Intent;)V

    .line 349
    invoke-virtual {v3}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    .line 354
    :cond_6
    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 355
    const/high16 v0, 0x10000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 357
    iget-object v0, p0, Leeh;->a:Leef;

    invoke-static {v0, v1}, Leef;->a(Leef;Landroid/content/Intent;)Landroid/content/Intent;

    .line 359
    iget-object v0, p0, Leeh;->a:Leef;

    invoke-static {v0}, Leef;->o(Leef;)Lfan;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Leeh;->a:Leef;

    invoke-static {v0}, Leef;->o(Leef;)Lfan;

    move-result-object v0

    invoke-virtual {v0, v1}, Lfan;->c(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 360
    iget-object v0, p0, Leeh;->a:Leef;

    invoke-virtual {v0}, Leef;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x3

    iget-object v2, p0, Leeh;->a:Leef;

    invoke-static {v2}, Leef;->p(Leef;)Lbc;

    move-result-object v2

    invoke-virtual {v0, v1, v6, v2}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 361
    iget-object v0, p0, Leeh;->a:Leef;

    iget-object v1, p0, Leeh;->a:Leef;

    const v2, 0x7f0a0a02

    invoke-virtual {v1, v2}, Leef;->e_(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Leef;->b(Leef;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 363
    :cond_7
    iget-object v0, p0, Leeh;->a:Leef;

    invoke-static {v0}, Leef;->c(Leef;)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Leeh;->a:Leef;

    invoke-static {v1}, Leef;->b(Leef;)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto/16 :goto_0
.end method
