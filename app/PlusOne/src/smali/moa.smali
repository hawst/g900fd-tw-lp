.class public final Lmoa;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Float;

.field public b:Ljava/lang/Float;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3073
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 3094
    const/4 v0, 0x0

    .line 3095
    iget-object v1, p0, Lmoa;->a:Ljava/lang/Float;

    if-eqz v1, :cond_0

    .line 3096
    const/4 v0, 0x1

    iget-object v1, p0, Lmoa;->a:Ljava/lang/Float;

    .line 3097
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x0

    .line 3099
    :cond_0
    iget-object v1, p0, Lmoa;->b:Ljava/lang/Float;

    if-eqz v1, :cond_1

    .line 3100
    const/4 v1, 0x2

    iget-object v2, p0, Lmoa;->b:Ljava/lang/Float;

    .line 3101
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 3103
    :cond_1
    iget-object v1, p0, Lmoa;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3104
    iput v0, p0, Lmoa;->ai:I

    .line 3105
    return v0
.end method

.method public a(Loxn;)Lmoa;
    .locals 2

    .prologue
    .line 3113
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3114
    sparse-switch v0, :sswitch_data_0

    .line 3118
    iget-object v1, p0, Lmoa;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 3119
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmoa;->ah:Ljava/util/List;

    .line 3122
    :cond_1
    iget-object v1, p0, Lmoa;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3124
    :sswitch_0
    return-object p0

    .line 3129
    :sswitch_1
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lmoa;->a:Ljava/lang/Float;

    goto :goto_0

    .line 3133
    :sswitch_2
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lmoa;->b:Ljava/lang/Float;

    goto :goto_0

    .line 3114
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x15 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 3082
    iget-object v0, p0, Lmoa;->a:Ljava/lang/Float;

    if-eqz v0, :cond_0

    .line 3083
    const/4 v0, 0x1

    iget-object v1, p0, Lmoa;->a:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 3085
    :cond_0
    iget-object v0, p0, Lmoa;->b:Ljava/lang/Float;

    if-eqz v0, :cond_1

    .line 3086
    const/4 v0, 0x2

    iget-object v1, p0, Lmoa;->b:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 3088
    :cond_1
    iget-object v0, p0, Lmoa;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3090
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3069
    invoke-virtual {p0, p1}, Lmoa;->a(Loxn;)Lmoa;

    move-result-object v0

    return-object v0
.end method
