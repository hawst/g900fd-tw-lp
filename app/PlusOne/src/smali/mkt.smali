.class public final Lmkt;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnfy;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 25108
    invoke-direct {p0}, Loxq;-><init>()V

    .line 25111
    iput-object v0, p0, Lmkt;->apiHeader:Llyr;

    .line 25114
    iput-object v0, p0, Lmkt;->a:Lnfy;

    .line 25108
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 25131
    const/4 v0, 0x0

    .line 25132
    iget-object v1, p0, Lmkt;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 25133
    const/4 v0, 0x1

    iget-object v1, p0, Lmkt;->apiHeader:Llyr;

    .line 25134
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 25136
    :cond_0
    iget-object v1, p0, Lmkt;->a:Lnfy;

    if-eqz v1, :cond_1

    .line 25137
    const/4 v1, 0x2

    iget-object v2, p0, Lmkt;->a:Lnfy;

    .line 25138
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 25140
    :cond_1
    iget-object v1, p0, Lmkt;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 25141
    iput v0, p0, Lmkt;->ai:I

    .line 25142
    return v0
.end method

.method public a(Loxn;)Lmkt;
    .locals 2

    .prologue
    .line 25150
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 25151
    sparse-switch v0, :sswitch_data_0

    .line 25155
    iget-object v1, p0, Lmkt;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 25156
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmkt;->ah:Ljava/util/List;

    .line 25159
    :cond_1
    iget-object v1, p0, Lmkt;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 25161
    :sswitch_0
    return-object p0

    .line 25166
    :sswitch_1
    iget-object v0, p0, Lmkt;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 25167
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmkt;->apiHeader:Llyr;

    .line 25169
    :cond_2
    iget-object v0, p0, Lmkt;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 25173
    :sswitch_2
    iget-object v0, p0, Lmkt;->a:Lnfy;

    if-nez v0, :cond_3

    .line 25174
    new-instance v0, Lnfy;

    invoke-direct {v0}, Lnfy;-><init>()V

    iput-object v0, p0, Lmkt;->a:Lnfy;

    .line 25176
    :cond_3
    iget-object v0, p0, Lmkt;->a:Lnfy;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 25151
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 25119
    iget-object v0, p0, Lmkt;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 25120
    const/4 v0, 0x1

    iget-object v1, p0, Lmkt;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 25122
    :cond_0
    iget-object v0, p0, Lmkt;->a:Lnfy;

    if-eqz v0, :cond_1

    .line 25123
    const/4 v0, 0x2

    iget-object v1, p0, Lmkt;->a:Lnfy;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 25125
    :cond_1
    iget-object v0, p0, Lmkt;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 25127
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 25104
    invoke-virtual {p0, p1}, Lmkt;->a(Loxn;)Lmkt;

    move-result-object v0

    return-object v0
.end method
