.class public final Lois;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lois;


# instance fields
.field public b:I

.field public c:Ljava/lang/String;

.field public d:[Loiu;

.field private e:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4497
    const/4 v0, 0x0

    new-array v0, v0, [Lois;

    sput-object v0, Lois;->a:[Lois;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 4498
    invoke-direct {p0}, Loxq;-><init>()V

    .line 4501
    const/high16 v0, -0x80000000

    iput v0, p0, Lois;->b:I

    .line 4506
    sget-object v0, Loiu;->a:[Loiu;

    iput-object v0, p0, Lois;->d:[Loiu;

    .line 4498
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 4535
    .line 4536
    iget v0, p0, Lois;->b:I

    const/high16 v2, -0x80000000

    if-eq v0, v2, :cond_4

    .line 4537
    const/4 v0, 0x1

    iget v2, p0, Lois;->b:I

    .line 4538
    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 4540
    :goto_0
    iget-object v2, p0, Lois;->d:[Loiu;

    if-eqz v2, :cond_1

    .line 4541
    iget-object v2, p0, Lois;->d:[Loiu;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 4542
    if-eqz v4, :cond_0

    .line 4543
    const/4 v5, 0x2

    .line 4544
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 4541
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 4548
    :cond_1
    iget-object v1, p0, Lois;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 4549
    const/4 v1, 0x3

    iget-object v2, p0, Lois;->e:Ljava/lang/Integer;

    .line 4550
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4552
    :cond_2
    iget-object v1, p0, Lois;->c:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 4553
    const/4 v1, 0x4

    iget-object v2, p0, Lois;->c:Ljava/lang/String;

    .line 4554
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4556
    :cond_3
    iget-object v1, p0, Lois;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4557
    iput v0, p0, Lois;->ai:I

    .line 4558
    return v0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lois;
    .locals 6

    .prologue
    const/16 v5, 0x12

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 4566
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 4567
    sparse-switch v0, :sswitch_data_0

    .line 4571
    iget-object v2, p0, Lois;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 4572
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lois;->ah:Ljava/util/List;

    .line 4575
    :cond_1
    iget-object v2, p0, Lois;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4577
    :sswitch_0
    return-object p0

    .line 4582
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 4583
    if-eq v0, v4, :cond_2

    const/4 v2, 0x2

    if-eq v0, v2, :cond_2

    const/4 v2, 0x3

    if-eq v0, v2, :cond_2

    const/4 v2, 0x4

    if-eq v0, v2, :cond_2

    const/4 v2, 0x5

    if-eq v0, v2, :cond_2

    const/4 v2, 0x6

    if-eq v0, v2, :cond_2

    const/4 v2, 0x7

    if-eq v0, v2, :cond_2

    const/16 v2, 0x8

    if-eq v0, v2, :cond_2

    const/16 v2, 0x9

    if-eq v0, v2, :cond_2

    const/16 v2, 0xa

    if-eq v0, v2, :cond_2

    const/16 v2, 0xb

    if-eq v0, v2, :cond_2

    const/16 v2, 0xc

    if-eq v0, v2, :cond_2

    const/16 v2, 0xd

    if-eq v0, v2, :cond_2

    const/16 v2, 0xe

    if-eq v0, v2, :cond_2

    const/16 v2, 0xf

    if-eq v0, v2, :cond_2

    const/16 v2, 0x10

    if-eq v0, v2, :cond_2

    const/16 v2, 0x11

    if-eq v0, v2, :cond_2

    if-eq v0, v5, :cond_2

    const/16 v2, 0x13

    if-eq v0, v2, :cond_2

    const/16 v2, 0x14

    if-eq v0, v2, :cond_2

    const/16 v2, 0x16

    if-eq v0, v2, :cond_2

    const/16 v2, 0x17

    if-eq v0, v2, :cond_2

    const/16 v2, 0x18

    if-eq v0, v2, :cond_2

    const/16 v2, 0x19

    if-eq v0, v2, :cond_2

    const/16 v2, 0x1a

    if-eq v0, v2, :cond_2

    const/16 v2, 0x1b

    if-eq v0, v2, :cond_2

    const/16 v2, 0x1c

    if-eq v0, v2, :cond_2

    const/16 v2, 0x1d

    if-eq v0, v2, :cond_2

    const/16 v2, 0x1f

    if-eq v0, v2, :cond_2

    const/16 v2, 0x20

    if-eq v0, v2, :cond_2

    const/16 v2, 0x21

    if-eq v0, v2, :cond_2

    const/16 v2, 0x22

    if-eq v0, v2, :cond_2

    const/16 v2, 0x23

    if-eq v0, v2, :cond_2

    const/16 v2, 0x24

    if-eq v0, v2, :cond_2

    const/16 v2, 0x25

    if-eq v0, v2, :cond_2

    const/16 v2, 0x26

    if-eq v0, v2, :cond_2

    const/16 v2, 0x27

    if-eq v0, v2, :cond_2

    const/16 v2, 0x28

    if-eq v0, v2, :cond_2

    const/16 v2, 0x1e

    if-eq v0, v2, :cond_2

    const/16 v2, 0x15

    if-ne v0, v2, :cond_3

    .line 4623
    :cond_2
    iput v0, p0, Lois;->b:I

    goto/16 :goto_0

    .line 4625
    :cond_3
    iput v4, p0, Lois;->b:I

    goto/16 :goto_0

    .line 4630
    :sswitch_2
    invoke-static {p1, v5}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 4631
    iget-object v0, p0, Lois;->d:[Loiu;

    if-nez v0, :cond_5

    move v0, v1

    .line 4632
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Loiu;

    .line 4633
    iget-object v3, p0, Lois;->d:[Loiu;

    if-eqz v3, :cond_4

    .line 4634
    iget-object v3, p0, Lois;->d:[Loiu;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4636
    :cond_4
    iput-object v2, p0, Lois;->d:[Loiu;

    .line 4637
    :goto_2
    iget-object v2, p0, Lois;->d:[Loiu;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 4638
    iget-object v2, p0, Lois;->d:[Loiu;

    new-instance v3, Loiu;

    invoke-direct {v3}, Loiu;-><init>()V

    aput-object v3, v2, v0

    .line 4639
    iget-object v2, p0, Lois;->d:[Loiu;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 4640
    invoke-virtual {p1}, Loxn;->a()I

    .line 4637
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 4631
    :cond_5
    iget-object v0, p0, Lois;->d:[Loiu;

    array-length v0, v0

    goto :goto_1

    .line 4643
    :cond_6
    iget-object v2, p0, Lois;->d:[Loiu;

    new-instance v3, Loiu;

    invoke-direct {v3}, Loiu;-><init>()V

    aput-object v3, v2, v0

    .line 4644
    iget-object v2, p0, Lois;->d:[Loiu;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 4648
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lois;->e:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 4652
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lois;->c:Ljava/lang/String;

    goto/16 :goto_0

    .line 4567
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 4513
    iget v0, p0, Lois;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 4514
    const/4 v0, 0x1

    iget v1, p0, Lois;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 4516
    :cond_0
    iget-object v0, p0, Lois;->d:[Loiu;

    if-eqz v0, :cond_2

    .line 4517
    iget-object v1, p0, Lois;->d:[Loiu;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 4518
    if-eqz v3, :cond_1

    .line 4519
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 4517
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 4523
    :cond_2
    iget-object v0, p0, Lois;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 4524
    const/4 v0, 0x3

    iget-object v1, p0, Lois;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 4526
    :cond_3
    iget-object v0, p0, Lois;->c:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 4527
    const/4 v0, 0x4

    iget-object v1, p0, Lois;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 4529
    :cond_4
    iget-object v0, p0, Lois;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 4531
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 4494
    invoke-virtual {p0, p1}, Lois;->a(Loxn;)Lois;

    move-result-object v0

    return-object v0
.end method
