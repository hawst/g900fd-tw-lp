.class public final Loyy;
.super Loxq;
.source "PG"


# static fields
.field public static final a:Loxr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Loxr",
            "<",
            "Loyy;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:Ljava/lang/Long;

.field public c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Lpdi;

.field private j:Ljava/lang/String;

.field private k:[Loya;

.field private l:Loya;

.field private m:[Loya;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/Long;

.field private q:Loya;

.field private r:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 9
    const v0, 0x294bd3f

    new-instance v1, Loyz;

    invoke-direct {v1}, Loyz;-><init>()V

    .line 14
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    move-result-object v0

    sput-object v0, Loyy;->a:Loxr;

    .line 13
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 27
    iput-object v1, p0, Loyy;->i:Lpdi;

    .line 32
    sget-object v0, Loya;->a:[Loya;

    iput-object v0, p0, Loyy;->k:[Loya;

    .line 35
    iput-object v1, p0, Loyy;->l:Loya;

    .line 38
    sget-object v0, Loya;->a:[Loya;

    iput-object v0, p0, Loyy;->m:[Loya;

    .line 51
    iput-object v1, p0, Loyy;->q:Loya;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 123
    .line 124
    iget-object v0, p0, Loyy;->d:Ljava/lang/String;

    if-eqz v0, :cond_12

    .line 125
    const/4 v0, 0x1

    iget-object v2, p0, Loyy;->d:Ljava/lang/String;

    .line 126
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 128
    :goto_0
    iget-object v2, p0, Loyy;->e:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 129
    const/4 v2, 0x2

    iget-object v3, p0, Loyy;->e:Ljava/lang/String;

    .line 130
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 132
    :cond_0
    iget-object v2, p0, Loyy;->f:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 133
    const/4 v2, 0x3

    iget-object v3, p0, Loyy;->f:Ljava/lang/String;

    .line 134
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 136
    :cond_1
    iget-object v2, p0, Loyy;->g:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 137
    const/4 v2, 0x4

    iget-object v3, p0, Loyy;->g:Ljava/lang/String;

    .line 138
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 140
    :cond_2
    iget-object v2, p0, Loyy;->h:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 141
    const/4 v2, 0x5

    iget-object v3, p0, Loyy;->h:Ljava/lang/String;

    .line 142
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 144
    :cond_3
    iget-object v2, p0, Loyy;->i:Lpdi;

    if-eqz v2, :cond_4

    .line 145
    const/4 v2, 0x6

    iget-object v3, p0, Loyy;->i:Lpdi;

    .line 146
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 148
    :cond_4
    iget-object v2, p0, Loyy;->j:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 149
    const/4 v2, 0x7

    iget-object v3, p0, Loyy;->j:Ljava/lang/String;

    .line 150
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 152
    :cond_5
    iget-object v2, p0, Loyy;->k:[Loya;

    if-eqz v2, :cond_7

    .line 153
    iget-object v3, p0, Loyy;->k:[Loya;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_7

    aget-object v5, v3, v2

    .line 154
    if-eqz v5, :cond_6

    .line 155
    const/16 v6, 0x8

    .line 156
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 153
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 160
    :cond_7
    iget-object v2, p0, Loyy;->l:Loya;

    if-eqz v2, :cond_8

    .line 161
    const/16 v2, 0x9

    iget-object v3, p0, Loyy;->l:Loya;

    .line 162
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 164
    :cond_8
    iget-object v2, p0, Loyy;->m:[Loya;

    if-eqz v2, :cond_a

    .line 165
    iget-object v2, p0, Loyy;->m:[Loya;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_a

    aget-object v4, v2, v1

    .line 166
    if-eqz v4, :cond_9

    .line 167
    const/16 v5, 0xb

    .line 168
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 165
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 172
    :cond_a
    iget-object v1, p0, Loyy;->n:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 173
    const/16 v1, 0xc

    iget-object v2, p0, Loyy;->n:Ljava/lang/String;

    .line 174
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 176
    :cond_b
    iget-object v1, p0, Loyy;->o:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 177
    const/16 v1, 0x4b

    iget-object v2, p0, Loyy;->o:Ljava/lang/String;

    .line 178
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 180
    :cond_c
    iget-object v1, p0, Loyy;->b:Ljava/lang/Long;

    if-eqz v1, :cond_d

    .line 181
    const/16 v1, 0xb4

    iget-object v2, p0, Loyy;->b:Ljava/lang/Long;

    .line 182
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 184
    :cond_d
    iget-object v1, p0, Loyy;->c:Ljava/lang/String;

    if-eqz v1, :cond_e

    .line 185
    const/16 v1, 0xb5

    iget-object v2, p0, Loyy;->c:Ljava/lang/String;

    .line 186
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 188
    :cond_e
    iget-object v1, p0, Loyy;->p:Ljava/lang/Long;

    if-eqz v1, :cond_f

    .line 189
    const/16 v1, 0xb6

    iget-object v2, p0, Loyy;->p:Ljava/lang/Long;

    .line 190
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 192
    :cond_f
    iget-object v1, p0, Loyy;->q:Loya;

    if-eqz v1, :cond_10

    .line 193
    const/16 v1, 0xb9

    iget-object v2, p0, Loyy;->q:Loya;

    .line 194
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 196
    :cond_10
    iget-object v1, p0, Loyy;->r:Ljava/lang/String;

    if-eqz v1, :cond_11

    .line 197
    const/16 v1, 0xfe

    iget-object v2, p0, Loyy;->r:Ljava/lang/String;

    .line 198
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 200
    :cond_11
    iget-object v1, p0, Loyy;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 201
    iput v0, p0, Loyy;->ai:I

    .line 202
    return v0

    :cond_12
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Loyy;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 210
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 211
    sparse-switch v0, :sswitch_data_0

    .line 215
    iget-object v2, p0, Loyy;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 216
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Loyy;->ah:Ljava/util/List;

    .line 219
    :cond_1
    iget-object v2, p0, Loyy;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 221
    :sswitch_0
    return-object p0

    .line 226
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyy;->d:Ljava/lang/String;

    goto :goto_0

    .line 230
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyy;->e:Ljava/lang/String;

    goto :goto_0

    .line 234
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyy;->f:Ljava/lang/String;

    goto :goto_0

    .line 238
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyy;->g:Ljava/lang/String;

    goto :goto_0

    .line 242
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyy;->h:Ljava/lang/String;

    goto :goto_0

    .line 246
    :sswitch_6
    iget-object v0, p0, Loyy;->i:Lpdi;

    if-nez v0, :cond_2

    .line 247
    new-instance v0, Lpdi;

    invoke-direct {v0}, Lpdi;-><init>()V

    iput-object v0, p0, Loyy;->i:Lpdi;

    .line 249
    :cond_2
    iget-object v0, p0, Loyy;->i:Lpdi;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 253
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyy;->j:Ljava/lang/String;

    goto :goto_0

    .line 257
    :sswitch_8
    const/16 v0, 0x42

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 258
    iget-object v0, p0, Loyy;->k:[Loya;

    if-nez v0, :cond_4

    move v0, v1

    .line 259
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Loya;

    .line 260
    iget-object v3, p0, Loyy;->k:[Loya;

    if-eqz v3, :cond_3

    .line 261
    iget-object v3, p0, Loyy;->k:[Loya;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 263
    :cond_3
    iput-object v2, p0, Loyy;->k:[Loya;

    .line 264
    :goto_2
    iget-object v2, p0, Loyy;->k:[Loya;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 265
    iget-object v2, p0, Loyy;->k:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 266
    iget-object v2, p0, Loyy;->k:[Loya;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 267
    invoke-virtual {p1}, Loxn;->a()I

    .line 264
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 258
    :cond_4
    iget-object v0, p0, Loyy;->k:[Loya;

    array-length v0, v0

    goto :goto_1

    .line 270
    :cond_5
    iget-object v2, p0, Loyy;->k:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 271
    iget-object v2, p0, Loyy;->k:[Loya;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 275
    :sswitch_9
    iget-object v0, p0, Loyy;->l:Loya;

    if-nez v0, :cond_6

    .line 276
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    iput-object v0, p0, Loyy;->l:Loya;

    .line 278
    :cond_6
    iget-object v0, p0, Loyy;->l:Loya;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 282
    :sswitch_a
    const/16 v0, 0x5a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 283
    iget-object v0, p0, Loyy;->m:[Loya;

    if-nez v0, :cond_8

    move v0, v1

    .line 284
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Loya;

    .line 285
    iget-object v3, p0, Loyy;->m:[Loya;

    if-eqz v3, :cond_7

    .line 286
    iget-object v3, p0, Loyy;->m:[Loya;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 288
    :cond_7
    iput-object v2, p0, Loyy;->m:[Loya;

    .line 289
    :goto_4
    iget-object v2, p0, Loyy;->m:[Loya;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    .line 290
    iget-object v2, p0, Loyy;->m:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 291
    iget-object v2, p0, Loyy;->m:[Loya;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 292
    invoke-virtual {p1}, Loxn;->a()I

    .line 289
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 283
    :cond_8
    iget-object v0, p0, Loyy;->m:[Loya;

    array-length v0, v0

    goto :goto_3

    .line 295
    :cond_9
    iget-object v2, p0, Loyy;->m:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 296
    iget-object v2, p0, Loyy;->m:[Loya;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 300
    :sswitch_b
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyy;->n:Ljava/lang/String;

    goto/16 :goto_0

    .line 304
    :sswitch_c
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyy;->o:Ljava/lang/String;

    goto/16 :goto_0

    .line 308
    :sswitch_d
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Loyy;->b:Ljava/lang/Long;

    goto/16 :goto_0

    .line 312
    :sswitch_e
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyy;->c:Ljava/lang/String;

    goto/16 :goto_0

    .line 316
    :sswitch_f
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Loyy;->p:Ljava/lang/Long;

    goto/16 :goto_0

    .line 320
    :sswitch_10
    iget-object v0, p0, Loyy;->q:Loya;

    if-nez v0, :cond_a

    .line 321
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    iput-object v0, p0, Loyy;->q:Loya;

    .line 323
    :cond_a
    iget-object v0, p0, Loyy;->q:Loya;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 327
    :sswitch_11
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyy;->r:Ljava/lang/String;

    goto/16 :goto_0

    .line 211
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x5a -> :sswitch_a
        0x62 -> :sswitch_b
        0x25a -> :sswitch_c
        0x5a0 -> :sswitch_d
        0x5aa -> :sswitch_e
        0x5b0 -> :sswitch_f
        0x5ca -> :sswitch_10
        0x7f2 -> :sswitch_11
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 58
    iget-object v1, p0, Loyy;->d:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 59
    const/4 v1, 0x1

    iget-object v2, p0, Loyy;->d:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 61
    :cond_0
    iget-object v1, p0, Loyy;->e:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 62
    const/4 v1, 0x2

    iget-object v2, p0, Loyy;->e:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 64
    :cond_1
    iget-object v1, p0, Loyy;->f:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 65
    const/4 v1, 0x3

    iget-object v2, p0, Loyy;->f:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 67
    :cond_2
    iget-object v1, p0, Loyy;->g:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 68
    const/4 v1, 0x4

    iget-object v2, p0, Loyy;->g:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 70
    :cond_3
    iget-object v1, p0, Loyy;->h:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 71
    const/4 v1, 0x5

    iget-object v2, p0, Loyy;->h:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 73
    :cond_4
    iget-object v1, p0, Loyy;->i:Lpdi;

    if-eqz v1, :cond_5

    .line 74
    const/4 v1, 0x6

    iget-object v2, p0, Loyy;->i:Lpdi;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 76
    :cond_5
    iget-object v1, p0, Loyy;->j:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 77
    const/4 v1, 0x7

    iget-object v2, p0, Loyy;->j:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 79
    :cond_6
    iget-object v1, p0, Loyy;->k:[Loya;

    if-eqz v1, :cond_8

    .line 80
    iget-object v2, p0, Loyy;->k:[Loya;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_8

    aget-object v4, v2, v1

    .line 81
    if-eqz v4, :cond_7

    .line 82
    const/16 v5, 0x8

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 80
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 86
    :cond_8
    iget-object v1, p0, Loyy;->l:Loya;

    if-eqz v1, :cond_9

    .line 87
    const/16 v1, 0x9

    iget-object v2, p0, Loyy;->l:Loya;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 89
    :cond_9
    iget-object v1, p0, Loyy;->m:[Loya;

    if-eqz v1, :cond_b

    .line 90
    iget-object v1, p0, Loyy;->m:[Loya;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_b

    aget-object v3, v1, v0

    .line 91
    if-eqz v3, :cond_a

    .line 92
    const/16 v4, 0xb

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 90
    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 96
    :cond_b
    iget-object v0, p0, Loyy;->n:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 97
    const/16 v0, 0xc

    iget-object v1, p0, Loyy;->n:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 99
    :cond_c
    iget-object v0, p0, Loyy;->o:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 100
    const/16 v0, 0x4b

    iget-object v1, p0, Loyy;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 102
    :cond_d
    iget-object v0, p0, Loyy;->b:Ljava/lang/Long;

    if-eqz v0, :cond_e

    .line 103
    const/16 v0, 0xb4

    iget-object v1, p0, Loyy;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 105
    :cond_e
    iget-object v0, p0, Loyy;->c:Ljava/lang/String;

    if-eqz v0, :cond_f

    .line 106
    const/16 v0, 0xb5

    iget-object v1, p0, Loyy;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 108
    :cond_f
    iget-object v0, p0, Loyy;->p:Ljava/lang/Long;

    if-eqz v0, :cond_10

    .line 109
    const/16 v0, 0xb6

    iget-object v1, p0, Loyy;->p:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 111
    :cond_10
    iget-object v0, p0, Loyy;->q:Loya;

    if-eqz v0, :cond_11

    .line 112
    const/16 v0, 0xb9

    iget-object v1, p0, Loyy;->q:Loya;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 114
    :cond_11
    iget-object v0, p0, Loyy;->r:Ljava/lang/String;

    if-eqz v0, :cond_12

    .line 115
    const/16 v0, 0xfe

    iget-object v1, p0, Loyy;->r:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 117
    :cond_12
    iget-object v0, p0, Loyy;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 119
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Loyy;->a(Loxn;)Loyy;

    move-result-object v0

    return-object v0
.end method
