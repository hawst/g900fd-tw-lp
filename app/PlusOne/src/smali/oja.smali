.class public final Loja;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Loja;


# instance fields
.field public b:I

.field public c:Lohx;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 5933
    const/4 v0, 0x0

    new-array v0, v0, [Loja;

    sput-object v0, Loja;->a:[Loja;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5934
    invoke-direct {p0}, Loxq;-><init>()V

    .line 5947
    const/high16 v0, -0x80000000

    iput v0, p0, Loja;->b:I

    .line 5950
    const/4 v0, 0x0

    iput-object v0, p0, Loja;->c:Lohx;

    .line 5934
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 5982
    const/4 v0, 0x0

    .line 5983
    iget v1, p0, Loja;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 5984
    const/4 v0, 0x1

    iget v1, p0, Loja;->b:I

    .line 5985
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 5987
    :cond_0
    iget-object v1, p0, Loja;->c:Lohx;

    if-eqz v1, :cond_1

    .line 5988
    const/4 v1, 0x2

    iget-object v2, p0, Loja;->c:Lohx;

    .line 5989
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5991
    :cond_1
    iget-object v1, p0, Loja;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 5992
    const/4 v1, 0x3

    iget-object v2, p0, Loja;->d:Ljava/lang/String;

    .line 5993
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5995
    :cond_2
    iget-object v1, p0, Loja;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 5996
    const/4 v1, 0x4

    iget-object v2, p0, Loja;->e:Ljava/lang/String;

    .line 5997
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5999
    :cond_3
    iget-object v1, p0, Loja;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 6000
    const/4 v1, 0x5

    iget-object v2, p0, Loja;->f:Ljava/lang/Integer;

    .line 6001
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6003
    :cond_4
    iget-object v1, p0, Loja;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6004
    iput v0, p0, Loja;->ai:I

    .line 6005
    return v0
.end method

.method public a(Loxn;)Loja;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 6013
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 6014
    sparse-switch v0, :sswitch_data_0

    .line 6018
    iget-object v1, p0, Loja;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 6019
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Loja;->ah:Ljava/util/List;

    .line 6022
    :cond_1
    iget-object v1, p0, Loja;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6024
    :sswitch_0
    return-object p0

    .line 6029
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 6030
    if-eq v0, v2, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-ne v0, v1, :cond_3

    .line 6037
    :cond_2
    iput v0, p0, Loja;->b:I

    goto :goto_0

    .line 6039
    :cond_3
    iput v2, p0, Loja;->b:I

    goto :goto_0

    .line 6044
    :sswitch_2
    iget-object v0, p0, Loja;->c:Lohx;

    if-nez v0, :cond_4

    .line 6045
    new-instance v0, Lohx;

    invoke-direct {v0}, Lohx;-><init>()V

    iput-object v0, p0, Loja;->c:Lohx;

    .line 6047
    :cond_4
    iget-object v0, p0, Loja;->c:Lohx;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 6051
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loja;->d:Ljava/lang/String;

    goto :goto_0

    .line 6055
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loja;->e:Ljava/lang/String;

    goto :goto_0

    .line 6059
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Loja;->f:Ljava/lang/Integer;

    goto :goto_0

    .line 6014
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 5961
    iget v0, p0, Loja;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 5962
    const/4 v0, 0x1

    iget v1, p0, Loja;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 5964
    :cond_0
    iget-object v0, p0, Loja;->c:Lohx;

    if-eqz v0, :cond_1

    .line 5965
    const/4 v0, 0x2

    iget-object v1, p0, Loja;->c:Lohx;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 5967
    :cond_1
    iget-object v0, p0, Loja;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 5968
    const/4 v0, 0x3

    iget-object v1, p0, Loja;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 5970
    :cond_2
    iget-object v0, p0, Loja;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 5971
    const/4 v0, 0x4

    iget-object v1, p0, Loja;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 5973
    :cond_3
    iget-object v0, p0, Loja;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 5974
    const/4 v0, 0x5

    iget-object v1, p0, Loja;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 5976
    :cond_4
    iget-object v0, p0, Loja;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 5978
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5930
    invoke-virtual {p0, p1}, Loja;->a(Loxn;)Loja;

    move-result-object v0

    return-object v0
.end method
