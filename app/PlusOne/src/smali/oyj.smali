.class public final Loyj;
.super Loxq;
.source "PG"


# static fields
.field public static final a:Loxr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Loxr",
            "<",
            "Loyj;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private A:Loya;

.field private B:Ljava/lang/String;

.field private C:Ljava/lang/String;

.field private D:Ljava/lang/String;

.field private E:Ljava/lang/String;

.field private F:Ljava/lang/String;

.field public b:Loya;

.field public c:Loyo;

.field public d:Loyh;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Lpdi;

.field private k:Ljava/lang/String;

.field private l:[Loya;

.field private m:Ljava/lang/String;

.field private n:[Loya;

.field private o:Ljava/lang/String;

.field private p:Loya;

.field private q:[Loya;

.field private r:Ljava/lang/Boolean;

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/Boolean;

.field private u:Loya;

.field private v:[Loya;

.field private w:I

.field private x:Loya;

.field private y:Ljava/lang/String;

.field private z:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 9
    const v0, 0x27a2bee

    new-instance v1, Loyk;

    invoke-direct {v1}, Loyk;-><init>()V

    .line 14
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    move-result-object v0

    sput-object v0, Loyj;->a:Loxr;

    .line 13
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 27
    iput-object v1, p0, Loyj;->j:Lpdi;

    .line 32
    sget-object v0, Loya;->a:[Loya;

    iput-object v0, p0, Loyj;->l:[Loya;

    .line 35
    iput-object v1, p0, Loyj;->b:Loya;

    .line 40
    sget-object v0, Loya;->a:[Loya;

    iput-object v0, p0, Loyj;->n:[Loya;

    .line 45
    iput-object v1, p0, Loyj;->p:Loya;

    .line 48
    sget-object v0, Loya;->a:[Loya;

    iput-object v0, p0, Loyj;->q:[Loya;

    .line 55
    iput-object v1, p0, Loyj;->c:Loyo;

    .line 60
    iput-object v1, p0, Loyj;->d:Loyh;

    .line 63
    iput-object v1, p0, Loyj;->u:Loya;

    .line 66
    sget-object v0, Loya;->a:[Loya;

    iput-object v0, p0, Loyj;->v:[Loya;

    .line 69
    const/high16 v0, -0x80000000

    iput v0, p0, Loyj;->w:I

    .line 72
    iput-object v1, p0, Loyj;->x:Loya;

    .line 79
    iput-object v1, p0, Loyj;->A:Loya;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 209
    .line 210
    iget-object v0, p0, Loyj;->e:Ljava/lang/String;

    if-eqz v0, :cond_22

    .line 211
    const/4 v0, 0x1

    iget-object v2, p0, Loyj;->e:Ljava/lang/String;

    .line 212
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 214
    :goto_0
    iget-object v2, p0, Loyj;->f:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 215
    const/4 v2, 0x2

    iget-object v3, p0, Loyj;->f:Ljava/lang/String;

    .line 216
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 218
    :cond_0
    iget-object v2, p0, Loyj;->g:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 219
    const/4 v2, 0x3

    iget-object v3, p0, Loyj;->g:Ljava/lang/String;

    .line 220
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 222
    :cond_1
    iget-object v2, p0, Loyj;->h:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 223
    const/4 v2, 0x4

    iget-object v3, p0, Loyj;->h:Ljava/lang/String;

    .line 224
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 226
    :cond_2
    iget-object v2, p0, Loyj;->i:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 227
    const/4 v2, 0x5

    iget-object v3, p0, Loyj;->i:Ljava/lang/String;

    .line 228
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 230
    :cond_3
    iget-object v2, p0, Loyj;->j:Lpdi;

    if-eqz v2, :cond_4

    .line 231
    const/4 v2, 0x6

    iget-object v3, p0, Loyj;->j:Lpdi;

    .line 232
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 234
    :cond_4
    iget-object v2, p0, Loyj;->k:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 235
    const/4 v2, 0x7

    iget-object v3, p0, Loyj;->k:Ljava/lang/String;

    .line 236
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 238
    :cond_5
    iget-object v2, p0, Loyj;->l:[Loya;

    if-eqz v2, :cond_7

    .line 239
    iget-object v3, p0, Loyj;->l:[Loya;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_7

    aget-object v5, v3, v2

    .line 240
    if-eqz v5, :cond_6

    .line 241
    const/16 v6, 0x8

    .line 242
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 239
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 246
    :cond_7
    iget-object v2, p0, Loyj;->b:Loya;

    if-eqz v2, :cond_8

    .line 247
    const/16 v2, 0x9

    iget-object v3, p0, Loyj;->b:Loya;

    .line 248
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 250
    :cond_8
    iget-object v2, p0, Loyj;->m:Ljava/lang/String;

    if-eqz v2, :cond_9

    .line 251
    const/16 v2, 0xa

    iget-object v3, p0, Loyj;->m:Ljava/lang/String;

    .line 252
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 254
    :cond_9
    iget-object v2, p0, Loyj;->n:[Loya;

    if-eqz v2, :cond_b

    .line 255
    iget-object v3, p0, Loyj;->n:[Loya;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_b

    aget-object v5, v3, v2

    .line 256
    if-eqz v5, :cond_a

    .line 257
    const/16 v6, 0xb

    .line 258
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 255
    :cond_a
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 262
    :cond_b
    iget-object v2, p0, Loyj;->o:Ljava/lang/String;

    if-eqz v2, :cond_c

    .line 263
    const/16 v2, 0xc

    iget-object v3, p0, Loyj;->o:Ljava/lang/String;

    .line 264
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 266
    :cond_c
    iget-object v2, p0, Loyj;->p:Loya;

    if-eqz v2, :cond_d

    .line 267
    const/16 v2, 0x12

    iget-object v3, p0, Loyj;->p:Loya;

    .line 268
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 270
    :cond_d
    iget-object v2, p0, Loyj;->q:[Loya;

    if-eqz v2, :cond_f

    .line 271
    iget-object v3, p0, Loyj;->q:[Loya;

    array-length v4, v3

    move v2, v1

    :goto_3
    if-ge v2, v4, :cond_f

    aget-object v5, v3, v2

    .line 272
    if-eqz v5, :cond_e

    .line 273
    const/16 v6, 0x2a

    .line 274
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 271
    :cond_e
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 278
    :cond_f
    iget-object v2, p0, Loyj;->r:Ljava/lang/Boolean;

    if-eqz v2, :cond_10

    .line 279
    const/16 v2, 0x41

    iget-object v3, p0, Loyj;->r:Ljava/lang/Boolean;

    .line 280
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 282
    :cond_10
    iget-object v2, p0, Loyj;->s:Ljava/lang/String;

    if-eqz v2, :cond_11

    .line 283
    const/16 v2, 0x4b

    iget-object v3, p0, Loyj;->s:Ljava/lang/String;

    .line 284
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 286
    :cond_11
    iget-object v2, p0, Loyj;->c:Loyo;

    if-eqz v2, :cond_12

    .line 287
    const/16 v2, 0x4c

    iget-object v3, p0, Loyj;->c:Loyo;

    .line 288
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 290
    :cond_12
    iget-object v2, p0, Loyj;->t:Ljava/lang/Boolean;

    if-eqz v2, :cond_13

    .line 291
    const/16 v2, 0x4d

    iget-object v3, p0, Loyj;->t:Ljava/lang/Boolean;

    .line 292
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 294
    :cond_13
    iget-object v2, p0, Loyj;->d:Loyh;

    if-eqz v2, :cond_14

    .line 295
    const/16 v2, 0x4e

    iget-object v3, p0, Loyj;->d:Loyh;

    .line 296
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 298
    :cond_14
    iget-object v2, p0, Loyj;->u:Loya;

    if-eqz v2, :cond_15

    .line 299
    const/16 v2, 0x52

    iget-object v3, p0, Loyj;->u:Loya;

    .line 300
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 302
    :cond_15
    iget-object v2, p0, Loyj;->v:[Loya;

    if-eqz v2, :cond_17

    .line 303
    iget-object v2, p0, Loyj;->v:[Loya;

    array-length v3, v2

    :goto_4
    if-ge v1, v3, :cond_17

    aget-object v4, v2, v1

    .line 304
    if-eqz v4, :cond_16

    .line 305
    const/16 v5, 0x53

    .line 306
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 303
    :cond_16
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 310
    :cond_17
    iget v1, p0, Loyj;->w:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_18

    .line 311
    const/16 v1, 0x5a

    iget v2, p0, Loyj;->w:I

    .line 312
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 314
    :cond_18
    iget-object v1, p0, Loyj;->x:Loya;

    if-eqz v1, :cond_19

    .line 315
    const/16 v1, 0x60

    iget-object v2, p0, Loyj;->x:Loya;

    .line 316
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 318
    :cond_19
    iget-object v1, p0, Loyj;->y:Ljava/lang/String;

    if-eqz v1, :cond_1a

    .line 319
    const/16 v1, 0x6f

    iget-object v2, p0, Loyj;->y:Ljava/lang/String;

    .line 320
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 322
    :cond_1a
    iget-object v1, p0, Loyj;->z:Ljava/lang/String;

    if-eqz v1, :cond_1b

    .line 323
    const/16 v1, 0x70

    iget-object v2, p0, Loyj;->z:Ljava/lang/String;

    .line 324
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 326
    :cond_1b
    iget-object v1, p0, Loyj;->A:Loya;

    if-eqz v1, :cond_1c

    .line 327
    const/16 v1, 0xb9

    iget-object v2, p0, Loyj;->A:Loya;

    .line 328
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 330
    :cond_1c
    iget-object v1, p0, Loyj;->B:Ljava/lang/String;

    if-eqz v1, :cond_1d

    .line 331
    const/16 v1, 0xbc

    iget-object v2, p0, Loyj;->B:Ljava/lang/String;

    .line 332
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 334
    :cond_1d
    iget-object v1, p0, Loyj;->C:Ljava/lang/String;

    if-eqz v1, :cond_1e

    .line 335
    const/16 v1, 0xbd

    iget-object v2, p0, Loyj;->C:Ljava/lang/String;

    .line 336
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 338
    :cond_1e
    iget-object v1, p0, Loyj;->D:Ljava/lang/String;

    if-eqz v1, :cond_1f

    .line 339
    const/16 v1, 0xbe

    iget-object v2, p0, Loyj;->D:Ljava/lang/String;

    .line 340
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 342
    :cond_1f
    iget-object v1, p0, Loyj;->E:Ljava/lang/String;

    if-eqz v1, :cond_20

    .line 343
    const/16 v1, 0xbf

    iget-object v2, p0, Loyj;->E:Ljava/lang/String;

    .line 344
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 346
    :cond_20
    iget-object v1, p0, Loyj;->F:Ljava/lang/String;

    if-eqz v1, :cond_21

    .line 347
    const/16 v1, 0xfe

    iget-object v2, p0, Loyj;->F:Ljava/lang/String;

    .line 348
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 350
    :cond_21
    iget-object v1, p0, Loyj;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 351
    iput v0, p0, Loyj;->ai:I

    .line 352
    return v0

    :cond_22
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Loyj;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 360
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 361
    sparse-switch v0, :sswitch_data_0

    .line 365
    iget-object v2, p0, Loyj;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 366
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Loyj;->ah:Ljava/util/List;

    .line 369
    :cond_1
    iget-object v2, p0, Loyj;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 371
    :sswitch_0
    return-object p0

    .line 376
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyj;->e:Ljava/lang/String;

    goto :goto_0

    .line 380
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyj;->f:Ljava/lang/String;

    goto :goto_0

    .line 384
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyj;->g:Ljava/lang/String;

    goto :goto_0

    .line 388
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyj;->h:Ljava/lang/String;

    goto :goto_0

    .line 392
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyj;->i:Ljava/lang/String;

    goto :goto_0

    .line 396
    :sswitch_6
    iget-object v0, p0, Loyj;->j:Lpdi;

    if-nez v0, :cond_2

    .line 397
    new-instance v0, Lpdi;

    invoke-direct {v0}, Lpdi;-><init>()V

    iput-object v0, p0, Loyj;->j:Lpdi;

    .line 399
    :cond_2
    iget-object v0, p0, Loyj;->j:Lpdi;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 403
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyj;->k:Ljava/lang/String;

    goto :goto_0

    .line 407
    :sswitch_8
    const/16 v0, 0x42

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 408
    iget-object v0, p0, Loyj;->l:[Loya;

    if-nez v0, :cond_4

    move v0, v1

    .line 409
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Loya;

    .line 410
    iget-object v3, p0, Loyj;->l:[Loya;

    if-eqz v3, :cond_3

    .line 411
    iget-object v3, p0, Loyj;->l:[Loya;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 413
    :cond_3
    iput-object v2, p0, Loyj;->l:[Loya;

    .line 414
    :goto_2
    iget-object v2, p0, Loyj;->l:[Loya;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 415
    iget-object v2, p0, Loyj;->l:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 416
    iget-object v2, p0, Loyj;->l:[Loya;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 417
    invoke-virtual {p1}, Loxn;->a()I

    .line 414
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 408
    :cond_4
    iget-object v0, p0, Loyj;->l:[Loya;

    array-length v0, v0

    goto :goto_1

    .line 420
    :cond_5
    iget-object v2, p0, Loyj;->l:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 421
    iget-object v2, p0, Loyj;->l:[Loya;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 425
    :sswitch_9
    iget-object v0, p0, Loyj;->b:Loya;

    if-nez v0, :cond_6

    .line 426
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    iput-object v0, p0, Loyj;->b:Loya;

    .line 428
    :cond_6
    iget-object v0, p0, Loyj;->b:Loya;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 432
    :sswitch_a
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyj;->m:Ljava/lang/String;

    goto/16 :goto_0

    .line 436
    :sswitch_b
    const/16 v0, 0x5a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 437
    iget-object v0, p0, Loyj;->n:[Loya;

    if-nez v0, :cond_8

    move v0, v1

    .line 438
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Loya;

    .line 439
    iget-object v3, p0, Loyj;->n:[Loya;

    if-eqz v3, :cond_7

    .line 440
    iget-object v3, p0, Loyj;->n:[Loya;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 442
    :cond_7
    iput-object v2, p0, Loyj;->n:[Loya;

    .line 443
    :goto_4
    iget-object v2, p0, Loyj;->n:[Loya;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    .line 444
    iget-object v2, p0, Loyj;->n:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 445
    iget-object v2, p0, Loyj;->n:[Loya;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 446
    invoke-virtual {p1}, Loxn;->a()I

    .line 443
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 437
    :cond_8
    iget-object v0, p0, Loyj;->n:[Loya;

    array-length v0, v0

    goto :goto_3

    .line 449
    :cond_9
    iget-object v2, p0, Loyj;->n:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 450
    iget-object v2, p0, Loyj;->n:[Loya;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 454
    :sswitch_c
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyj;->o:Ljava/lang/String;

    goto/16 :goto_0

    .line 458
    :sswitch_d
    iget-object v0, p0, Loyj;->p:Loya;

    if-nez v0, :cond_a

    .line 459
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    iput-object v0, p0, Loyj;->p:Loya;

    .line 461
    :cond_a
    iget-object v0, p0, Loyj;->p:Loya;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 465
    :sswitch_e
    const/16 v0, 0x152

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 466
    iget-object v0, p0, Loyj;->q:[Loya;

    if-nez v0, :cond_c

    move v0, v1

    .line 467
    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Loya;

    .line 468
    iget-object v3, p0, Loyj;->q:[Loya;

    if-eqz v3, :cond_b

    .line 469
    iget-object v3, p0, Loyj;->q:[Loya;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 471
    :cond_b
    iput-object v2, p0, Loyj;->q:[Loya;

    .line 472
    :goto_6
    iget-object v2, p0, Loyj;->q:[Loya;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_d

    .line 473
    iget-object v2, p0, Loyj;->q:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 474
    iget-object v2, p0, Loyj;->q:[Loya;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 475
    invoke-virtual {p1}, Loxn;->a()I

    .line 472
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 466
    :cond_c
    iget-object v0, p0, Loyj;->q:[Loya;

    array-length v0, v0

    goto :goto_5

    .line 478
    :cond_d
    iget-object v2, p0, Loyj;->q:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 479
    iget-object v2, p0, Loyj;->q:[Loya;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 483
    :sswitch_f
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Loyj;->r:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 487
    :sswitch_10
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyj;->s:Ljava/lang/String;

    goto/16 :goto_0

    .line 491
    :sswitch_11
    iget-object v0, p0, Loyj;->c:Loyo;

    if-nez v0, :cond_e

    .line 492
    new-instance v0, Loyo;

    invoke-direct {v0}, Loyo;-><init>()V

    iput-object v0, p0, Loyj;->c:Loyo;

    .line 494
    :cond_e
    iget-object v0, p0, Loyj;->c:Loyo;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 498
    :sswitch_12
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Loyj;->t:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 502
    :sswitch_13
    iget-object v0, p0, Loyj;->d:Loyh;

    if-nez v0, :cond_f

    .line 503
    new-instance v0, Loyh;

    invoke-direct {v0}, Loyh;-><init>()V

    iput-object v0, p0, Loyj;->d:Loyh;

    .line 505
    :cond_f
    iget-object v0, p0, Loyj;->d:Loyh;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 509
    :sswitch_14
    iget-object v0, p0, Loyj;->u:Loya;

    if-nez v0, :cond_10

    .line 510
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    iput-object v0, p0, Loyj;->u:Loya;

    .line 512
    :cond_10
    iget-object v0, p0, Loyj;->u:Loya;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 516
    :sswitch_15
    const/16 v0, 0x29a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 517
    iget-object v0, p0, Loyj;->v:[Loya;

    if-nez v0, :cond_12

    move v0, v1

    .line 518
    :goto_7
    add-int/2addr v2, v0

    new-array v2, v2, [Loya;

    .line 519
    iget-object v3, p0, Loyj;->v:[Loya;

    if-eqz v3, :cond_11

    .line 520
    iget-object v3, p0, Loyj;->v:[Loya;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 522
    :cond_11
    iput-object v2, p0, Loyj;->v:[Loya;

    .line 523
    :goto_8
    iget-object v2, p0, Loyj;->v:[Loya;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_13

    .line 524
    iget-object v2, p0, Loyj;->v:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 525
    iget-object v2, p0, Loyj;->v:[Loya;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 526
    invoke-virtual {p1}, Loxn;->a()I

    .line 523
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 517
    :cond_12
    iget-object v0, p0, Loyj;->v:[Loya;

    array-length v0, v0

    goto :goto_7

    .line 529
    :cond_13
    iget-object v2, p0, Loyj;->v:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 530
    iget-object v2, p0, Loyj;->v:[Loya;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 534
    :sswitch_16
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 535
    if-eqz v0, :cond_14

    const/4 v2, 0x1

    if-ne v0, v2, :cond_15

    .line 537
    :cond_14
    iput v0, p0, Loyj;->w:I

    goto/16 :goto_0

    .line 539
    :cond_15
    iput v1, p0, Loyj;->w:I

    goto/16 :goto_0

    .line 544
    :sswitch_17
    iget-object v0, p0, Loyj;->x:Loya;

    if-nez v0, :cond_16

    .line 545
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    iput-object v0, p0, Loyj;->x:Loya;

    .line 547
    :cond_16
    iget-object v0, p0, Loyj;->x:Loya;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 551
    :sswitch_18
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyj;->y:Ljava/lang/String;

    goto/16 :goto_0

    .line 555
    :sswitch_19
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyj;->z:Ljava/lang/String;

    goto/16 :goto_0

    .line 559
    :sswitch_1a
    iget-object v0, p0, Loyj;->A:Loya;

    if-nez v0, :cond_17

    .line 560
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    iput-object v0, p0, Loyj;->A:Loya;

    .line 562
    :cond_17
    iget-object v0, p0, Loyj;->A:Loya;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 566
    :sswitch_1b
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyj;->B:Ljava/lang/String;

    goto/16 :goto_0

    .line 570
    :sswitch_1c
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyj;->C:Ljava/lang/String;

    goto/16 :goto_0

    .line 574
    :sswitch_1d
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyj;->D:Ljava/lang/String;

    goto/16 :goto_0

    .line 578
    :sswitch_1e
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyj;->E:Ljava/lang/String;

    goto/16 :goto_0

    .line 582
    :sswitch_1f
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyj;->F:Ljava/lang/String;

    goto/16 :goto_0

    .line 361
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x92 -> :sswitch_d
        0x152 -> :sswitch_e
        0x208 -> :sswitch_f
        0x25a -> :sswitch_10
        0x262 -> :sswitch_11
        0x268 -> :sswitch_12
        0x272 -> :sswitch_13
        0x292 -> :sswitch_14
        0x29a -> :sswitch_15
        0x2d0 -> :sswitch_16
        0x302 -> :sswitch_17
        0x37a -> :sswitch_18
        0x382 -> :sswitch_19
        0x5ca -> :sswitch_1a
        0x5e2 -> :sswitch_1b
        0x5ea -> :sswitch_1c
        0x5f2 -> :sswitch_1d
        0x5fa -> :sswitch_1e
        0x7f2 -> :sswitch_1f
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 94
    iget-object v1, p0, Loyj;->e:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 95
    const/4 v1, 0x1

    iget-object v2, p0, Loyj;->e:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 97
    :cond_0
    iget-object v1, p0, Loyj;->f:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 98
    const/4 v1, 0x2

    iget-object v2, p0, Loyj;->f:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 100
    :cond_1
    iget-object v1, p0, Loyj;->g:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 101
    const/4 v1, 0x3

    iget-object v2, p0, Loyj;->g:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 103
    :cond_2
    iget-object v1, p0, Loyj;->h:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 104
    const/4 v1, 0x4

    iget-object v2, p0, Loyj;->h:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 106
    :cond_3
    iget-object v1, p0, Loyj;->i:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 107
    const/4 v1, 0x5

    iget-object v2, p0, Loyj;->i:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 109
    :cond_4
    iget-object v1, p0, Loyj;->j:Lpdi;

    if-eqz v1, :cond_5

    .line 110
    const/4 v1, 0x6

    iget-object v2, p0, Loyj;->j:Lpdi;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 112
    :cond_5
    iget-object v1, p0, Loyj;->k:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 113
    const/4 v1, 0x7

    iget-object v2, p0, Loyj;->k:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 115
    :cond_6
    iget-object v1, p0, Loyj;->l:[Loya;

    if-eqz v1, :cond_8

    .line 116
    iget-object v2, p0, Loyj;->l:[Loya;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_8

    aget-object v4, v2, v1

    .line 117
    if-eqz v4, :cond_7

    .line 118
    const/16 v5, 0x8

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 116
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 122
    :cond_8
    iget-object v1, p0, Loyj;->b:Loya;

    if-eqz v1, :cond_9

    .line 123
    const/16 v1, 0x9

    iget-object v2, p0, Loyj;->b:Loya;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 125
    :cond_9
    iget-object v1, p0, Loyj;->m:Ljava/lang/String;

    if-eqz v1, :cond_a

    .line 126
    const/16 v1, 0xa

    iget-object v2, p0, Loyj;->m:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 128
    :cond_a
    iget-object v1, p0, Loyj;->n:[Loya;

    if-eqz v1, :cond_c

    .line 129
    iget-object v2, p0, Loyj;->n:[Loya;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_c

    aget-object v4, v2, v1

    .line 130
    if-eqz v4, :cond_b

    .line 131
    const/16 v5, 0xb

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 129
    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 135
    :cond_c
    iget-object v1, p0, Loyj;->o:Ljava/lang/String;

    if-eqz v1, :cond_d

    .line 136
    const/16 v1, 0xc

    iget-object v2, p0, Loyj;->o:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 138
    :cond_d
    iget-object v1, p0, Loyj;->p:Loya;

    if-eqz v1, :cond_e

    .line 139
    const/16 v1, 0x12

    iget-object v2, p0, Loyj;->p:Loya;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 141
    :cond_e
    iget-object v1, p0, Loyj;->q:[Loya;

    if-eqz v1, :cond_10

    .line 142
    iget-object v2, p0, Loyj;->q:[Loya;

    array-length v3, v2

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_10

    aget-object v4, v2, v1

    .line 143
    if-eqz v4, :cond_f

    .line 144
    const/16 v5, 0x2a

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 142
    :cond_f
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 148
    :cond_10
    iget-object v1, p0, Loyj;->r:Ljava/lang/Boolean;

    if-eqz v1, :cond_11

    .line 149
    const/16 v1, 0x41

    iget-object v2, p0, Loyj;->r:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IZ)V

    .line 151
    :cond_11
    iget-object v1, p0, Loyj;->s:Ljava/lang/String;

    if-eqz v1, :cond_12

    .line 152
    const/16 v1, 0x4b

    iget-object v2, p0, Loyj;->s:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 154
    :cond_12
    iget-object v1, p0, Loyj;->c:Loyo;

    if-eqz v1, :cond_13

    .line 155
    const/16 v1, 0x4c

    iget-object v2, p0, Loyj;->c:Loyo;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 157
    :cond_13
    iget-object v1, p0, Loyj;->t:Ljava/lang/Boolean;

    if-eqz v1, :cond_14

    .line 158
    const/16 v1, 0x4d

    iget-object v2, p0, Loyj;->t:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IZ)V

    .line 160
    :cond_14
    iget-object v1, p0, Loyj;->d:Loyh;

    if-eqz v1, :cond_15

    .line 161
    const/16 v1, 0x4e

    iget-object v2, p0, Loyj;->d:Loyh;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 163
    :cond_15
    iget-object v1, p0, Loyj;->u:Loya;

    if-eqz v1, :cond_16

    .line 164
    const/16 v1, 0x52

    iget-object v2, p0, Loyj;->u:Loya;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 166
    :cond_16
    iget-object v1, p0, Loyj;->v:[Loya;

    if-eqz v1, :cond_18

    .line 167
    iget-object v1, p0, Loyj;->v:[Loya;

    array-length v2, v1

    :goto_3
    if-ge v0, v2, :cond_18

    aget-object v3, v1, v0

    .line 168
    if-eqz v3, :cond_17

    .line 169
    const/16 v4, 0x53

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 167
    :cond_17
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 173
    :cond_18
    iget v0, p0, Loyj;->w:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_19

    .line 174
    const/16 v0, 0x5a

    iget v1, p0, Loyj;->w:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 176
    :cond_19
    iget-object v0, p0, Loyj;->x:Loya;

    if-eqz v0, :cond_1a

    .line 177
    const/16 v0, 0x60

    iget-object v1, p0, Loyj;->x:Loya;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 179
    :cond_1a
    iget-object v0, p0, Loyj;->y:Ljava/lang/String;

    if-eqz v0, :cond_1b

    .line 180
    const/16 v0, 0x6f

    iget-object v1, p0, Loyj;->y:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 182
    :cond_1b
    iget-object v0, p0, Loyj;->z:Ljava/lang/String;

    if-eqz v0, :cond_1c

    .line 183
    const/16 v0, 0x70

    iget-object v1, p0, Loyj;->z:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 185
    :cond_1c
    iget-object v0, p0, Loyj;->A:Loya;

    if-eqz v0, :cond_1d

    .line 186
    const/16 v0, 0xb9

    iget-object v1, p0, Loyj;->A:Loya;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 188
    :cond_1d
    iget-object v0, p0, Loyj;->B:Ljava/lang/String;

    if-eqz v0, :cond_1e

    .line 189
    const/16 v0, 0xbc

    iget-object v1, p0, Loyj;->B:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 191
    :cond_1e
    iget-object v0, p0, Loyj;->C:Ljava/lang/String;

    if-eqz v0, :cond_1f

    .line 192
    const/16 v0, 0xbd

    iget-object v1, p0, Loyj;->C:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 194
    :cond_1f
    iget-object v0, p0, Loyj;->D:Ljava/lang/String;

    if-eqz v0, :cond_20

    .line 195
    const/16 v0, 0xbe

    iget-object v1, p0, Loyj;->D:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 197
    :cond_20
    iget-object v0, p0, Loyj;->E:Ljava/lang/String;

    if-eqz v0, :cond_21

    .line 198
    const/16 v0, 0xbf

    iget-object v1, p0, Loyj;->E:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 200
    :cond_21
    iget-object v0, p0, Loyj;->F:Ljava/lang/String;

    if-eqz v0, :cond_22

    .line 201
    const/16 v0, 0xfe

    iget-object v1, p0, Loyj;->F:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 203
    :cond_22
    iget-object v0, p0, Loyj;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 205
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Loyj;->a(Loxn;)Loyj;

    move-result-object v0

    return-object v0
.end method
