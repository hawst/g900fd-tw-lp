.class public final Lddm;
.super Lddr;
.source "PG"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lddm;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final D:Lddq;

.field private final E:Lddl;

.field private final F:Lddl;

.field private final G:J

.field private final H:I

.field private final I:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    new-instance v0, Lddn;

    invoke-direct {v0}, Lddn;-><init>()V

    sput-object v0, Lddm;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 145
    invoke-direct {p0, p1}, Lddr;-><init>(Landroid/os/Parcel;)V

    .line 146
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lddm;->G:J

    .line 147
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lddm;->H:I

    .line 148
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lddm;->I:I

    .line 150
    iget-object v0, p0, Lddm;->a:Landroid/os/Bundle;

    if-nez v0, :cond_0

    .line 151
    iput-object v2, p0, Lddm;->D:Lddq;

    .line 152
    iput-object v2, p0, Lddm;->F:Lddl;

    .line 153
    iput-object v2, p0, Lddm;->E:Lddl;

    .line 159
    :goto_0
    return-void

    .line 155
    :cond_0
    iget-object v1, p0, Lddm;->a:Landroid/os/Bundle;

    iget-object v0, p0, Lddm;->c:Ljava/util/EnumMap;

    sget-object v2, Lddt;->a:Lddt;

    invoke-virtual {v0, v2}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizu;

    invoke-static {v1, v0}, Lddq;->a(Landroid/os/Bundle;Lizu;)Lddq;

    move-result-object v0

    iput-object v0, p0, Lddm;->D:Lddq;

    .line 156
    iget-object v0, p0, Lddm;->D:Lddq;

    invoke-direct {p0, v0}, Lddm;->a(Lddq;)Lddl;

    move-result-object v0

    iput-object v0, p0, Lddm;->F:Lddl;

    .line 157
    iget-object v0, p0, Lddm;->D:Lddq;

    invoke-direct {p0, v0}, Lddm;->b(Lddq;)Lddl;

    move-result-object v0

    iput-object v0, p0, Lddm;->E:Lddl;

    goto :goto_0
.end method

.method protected constructor <init>(Ldds;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 122
    invoke-direct {p0, p1}, Lddr;-><init>(Ldds;)V

    .line 123
    iget-object v1, p1, Ldds;->d:Landroid/os/Bundle;

    iget-object v0, p1, Ldds;->f:Ljava/util/EnumMap;

    sget-object v2, Lddt;->a:Lddt;

    .line 124
    invoke-virtual {v0, v2}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizu;

    .line 123
    invoke-static {v1, v0}, Lddq;->a(Landroid/os/Bundle;Lizu;)Lddq;

    move-result-object v0

    iput-object v0, p0, Lddm;->D:Lddq;

    .line 125
    instance-of v0, p1, Lddp;

    if-eqz v0, :cond_1

    .line 126
    check-cast p1, Lddp;

    .line 127
    iget-wide v0, p1, Lddp;->a:J

    iput-wide v0, p0, Lddm;->G:J

    .line 128
    iget v0, p1, Lddp;->b:I

    iput v0, p0, Lddm;->H:I

    .line 129
    iget v0, p1, Lddp;->c:I

    iput v0, p0, Lddm;->I:I

    .line 136
    :goto_0
    const-string v0, "OneUpProxyAllPhotos"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 137
    const-string v0, "Creating proxy with type: "

    iget-object v1, p0, Lddm;->D:Lddq;

    invoke-virtual {v1}, Lddq;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 140
    :cond_0
    :goto_1
    iget-object v0, p0, Lddm;->D:Lddq;

    invoke-direct {p0, v0}, Lddm;->a(Lddq;)Lddl;

    move-result-object v0

    iput-object v0, p0, Lddm;->F:Lddl;

    .line 141
    iget-object v0, p0, Lddm;->D:Lddq;

    invoke-direct {p0, v0}, Lddm;->b(Lddq;)Lddl;

    move-result-object v0

    iput-object v0, p0, Lddm;->E:Lddl;

    .line 142
    return-void

    .line 131
    :cond_1
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lddm;->G:J

    .line 132
    iput v3, p0, Lddm;->H:I

    .line 133
    iput v3, p0, Lddm;->I:I

    goto :goto_0

    .line 137
    :cond_2
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private Z()Lddl;
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lddm;->E:Lddl;

    if-eqz v0, :cond_0

    .line 204
    iget-object v0, p0, Lddm;->E:Lddl;

    .line 206
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lddm;->F:Lddl;

    goto :goto_0
.end method

.method static synthetic a(Lddm;)J
    .locals 2

    .prologue
    .line 24
    iget-wide v0, p0, Lddm;->G:J

    return-wide v0
.end method

.method private a(Lddq;)Lddl;
    .locals 2

    .prologue
    .line 162
    sget-object v0, Lddo;->a:[I

    invoke-virtual {p1}, Lddq;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 173
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown ProxyType"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 165
    :pswitch_0
    new-instance v0, Lddw;

    invoke-direct {v0}, Lddw;-><init>()V

    .line 166
    invoke-virtual {v0, p0}, Lddw;->a(Lddl;)Ldds;

    .line 167
    iget v1, p0, Lddm;->H:I

    invoke-virtual {v0, v1}, Lddw;->a(I)Lddw;

    .line 168
    iget v1, p0, Lddm;->I:I

    invoke-virtual {v0, v1}, Lddw;->b(I)Lddw;

    .line 169
    invoke-virtual {v0}, Lddw;->a()Lddl;

    move-result-object v0

    .line 171
    :goto_0
    return-object v0

    :pswitch_1
    const/4 v0, 0x0

    goto :goto_0

    .line 162
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic b(Lddm;)I
    .locals 1

    .prologue
    .line 24
    iget v0, p0, Lddm;->H:I

    return v0
.end method

.method private b(Lddq;)Lddl;
    .locals 2

    .prologue
    .line 178
    sget-object v0, Lddo;->a:[I

    invoke-virtual {p1}, Lddq;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 187
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown ProxyType"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 181
    :pswitch_0
    new-instance v0, Lddz;

    invoke-direct {v0}, Lddz;-><init>()V

    .line 182
    invoke-virtual {v0, p0}, Lddz;->a(Lddl;)Ldds;

    move-result-object v0

    .line 183
    invoke-virtual {v0}, Ldds;->a()Lddl;

    move-result-object v0

    .line 185
    :goto_0
    return-object v0

    :pswitch_1
    const/4 v0, 0x0

    goto :goto_0

    .line 178
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic c(Lddm;)I
    .locals 1

    .prologue
    .line 24
    iget v0, p0, Lddm;->I:I

    return v0
.end method


# virtual methods
.method public A()Z
    .locals 1

    .prologue
    .line 259
    invoke-direct {p0}, Lddm;->Z()Lddl;

    move-result-object v0

    invoke-interface {v0}, Lddl;->A()Z

    move-result v0

    return v0
.end method

.method public B()Z
    .locals 1

    .prologue
    .line 269
    const/4 v0, 0x0

    return v0
.end method

.method public C()Z
    .locals 1

    .prologue
    .line 289
    invoke-direct {p0}, Lddm;->Z()Lddl;

    move-result-object v0

    invoke-interface {v0}, Lddl;->C()Z

    move-result v0

    return v0
.end method

.method public E()Z
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lddm;->E:Lddl;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public L()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 314
    iget-object v1, p0, Lddm;->C:Lctq;

    invoke-virtual {v1}, Lctq;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lddm;->a:Landroid/os/Bundle;

    const-string v2, "selected_only"

    .line 315
    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public M()Z
    .locals 1

    .prologue
    .line 238
    invoke-virtual {p0}, Lddm;->e()Lnzi;

    move-result-object v0

    invoke-virtual {p0, v0}, Lddm;->c(Lnzi;)Z

    move-result v0

    return v0
.end method

.method public N()Z
    .locals 1

    .prologue
    .line 264
    const/4 v0, 0x1

    return v0
.end method

.method public P()Z
    .locals 2

    .prologue
    .line 319
    invoke-super {p0}, Lddr;->P()Z

    move-result v0

    .line 321
    if-nez v0, :cond_0

    .line 322
    iget-object v1, p0, Lddm;->F:Lddl;

    if-eqz v1, :cond_0

    .line 323
    iget-object v0, p0, Lddm;->F:Lddl;

    invoke-interface {v0}, Lddl;->P()Z

    move-result v0

    .line 326
    :cond_0
    return v0
.end method

.method public S()I
    .locals 1

    .prologue
    .line 299
    invoke-direct {p0}, Lddm;->Z()Lddl;

    move-result-object v0

    invoke-interface {v0}, Lddl;->S()I

    move-result v0

    return v0
.end method

.method public T()I
    .locals 1

    .prologue
    .line 304
    invoke-direct {p0}, Lddm;->Z()Lddl;

    move-result-object v0

    invoke-interface {v0}, Lddl;->T()I

    move-result v0

    return v0
.end method

.method public V()Ldds;
    .locals 1

    .prologue
    .line 212
    new-instance v0, Lddp;

    invoke-direct {v0}, Lddp;-><init>()V

    return-object v0
.end method

.method public a(Landroid/content/Context;I)Landroid/content/Intent;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 217
    invoke-direct {p0}, Lddm;->Z()Lddl;

    move-result-object v1

    iget-object v2, p0, Lddm;->E:Lddl;

    if-ne v1, v2, :cond_1

    .line 218
    iget-object v1, p0, Lddm;->C:Lctq;

    invoke-virtual {v1}, Lctq;->d()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 219
    :cond_0
    invoke-virtual {p0}, Lddm;->e()Lnzi;

    move-result-object v1

    invoke-virtual {p0, p1, p2, v1, v0}, Lddm;->a(Landroid/content/Context;ILnzi;Z)Landroid/content/Intent;

    move-result-object v0

    .line 221
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lddm;->e()Lnzi;

    move-result-object v1

    invoke-virtual {p0, p1, p2, v1, v0}, Lddm;->a(Landroid/content/Context;ILnzi;Z)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method public b(Landroid/content/Context;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 227
    invoke-direct {p0}, Lddm;->Z()Lddl;

    move-result-object v0

    invoke-interface {v0, p1}, Lddl;->b(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljuf;
    .locals 6

    .prologue
    .line 232
    iget-object v0, p0, Lddm;->B:Lctz;

    invoke-virtual {v0}, Lctz;->a()Ljcn;

    move-result-object v0

    sget-object v1, Ldqo;->a:Ldqo;

    new-instance v2, Ldqp;

    iget-wide v4, p0, Lddm;->G:J

    invoke-direct {v2, v4, v5}, Ldqp;-><init>(J)V

    invoke-virtual {v0, v1, v2}, Ljcn;->a(Ljcj;Ljcm;)Ljcl;

    move-result-object v0

    check-cast v0, Ljuf;

    return-object v0
.end method

.method public d(Landroid/content/Context;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 331
    invoke-super {p0}, Lddr;->P()Z

    move-result v0

    .line 333
    if-nez v0, :cond_0

    .line 334
    iget-object v0, p0, Lddm;->F:Lddl;

    if-eqz v0, :cond_0

    .line 335
    iget-object v0, p0, Lddm;->F:Lddl;

    invoke-interface {v0, p1}, Lddl;->d(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 338
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lddr;->d(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method public d()Ljuf;
    .locals 8

    .prologue
    .line 243
    new-instance v0, Ldqm;

    iget-wide v1, p0, Lddm;->G:J

    .line 244
    invoke-virtual {p0}, Lddm;->a()Lizu;

    move-result-object v3

    invoke-virtual {p0}, Lddm;->p()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lddm;->f()Lnzi;

    move-result-object v5

    iget-wide v6, p0, Lddm;->w:J

    invoke-direct/range {v0 .. v7}, Ldqm;-><init>(JLizu;Ljava/lang/String;Lnzi;J)V

    return-object v0
.end method

.method public v()Z
    .locals 1

    .prologue
    .line 294
    invoke-direct {p0}, Lddm;->Z()Lddl;

    move-result-object v0

    invoke-interface {v0}, Lddl;->v()Z

    move-result v0

    return v0
.end method

.method public w()Z
    .locals 1

    .prologue
    .line 279
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 193
    invoke-super {p0, p1, p2}, Lddr;->writeToParcel(Landroid/os/Parcel;I)V

    .line 194
    iget-wide v0, p0, Lddm;->G:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 195
    iget v0, p0, Lddm;->H:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 196
    iget v0, p0, Lddm;->I:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 197
    return-void
.end method

.method public x()Z
    .locals 1

    .prologue
    .line 284
    const/4 v0, 0x0

    return v0
.end method

.method public y()Z
    .locals 1

    .prologue
    .line 309
    invoke-direct {p0}, Lddm;->Z()Lddl;

    move-result-object v0

    invoke-interface {v0}, Lddl;->y()Z

    move-result v0

    return v0
.end method

.method public z()Z
    .locals 1

    .prologue
    .line 274
    const/4 v0, 0x0

    return v0
.end method
