.class public final Lmae;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnva;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 4615
    invoke-direct {p0}, Loxq;-><init>()V

    .line 4618
    iput-object v0, p0, Lmae;->apiHeader:Llyq;

    .line 4621
    iput-object v0, p0, Lmae;->a:Lnva;

    .line 4615
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 4638
    const/4 v0, 0x0

    .line 4639
    iget-object v1, p0, Lmae;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 4640
    const/4 v0, 0x1

    iget-object v1, p0, Lmae;->apiHeader:Llyq;

    .line 4641
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 4643
    :cond_0
    iget-object v1, p0, Lmae;->a:Lnva;

    if-eqz v1, :cond_1

    .line 4644
    const/4 v1, 0x2

    iget-object v2, p0, Lmae;->a:Lnva;

    .line 4645
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4647
    :cond_1
    iget-object v1, p0, Lmae;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4648
    iput v0, p0, Lmae;->ai:I

    .line 4649
    return v0
.end method

.method public a(Loxn;)Lmae;
    .locals 2

    .prologue
    .line 4657
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 4658
    sparse-switch v0, :sswitch_data_0

    .line 4662
    iget-object v1, p0, Lmae;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 4663
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmae;->ah:Ljava/util/List;

    .line 4666
    :cond_1
    iget-object v1, p0, Lmae;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4668
    :sswitch_0
    return-object p0

    .line 4673
    :sswitch_1
    iget-object v0, p0, Lmae;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 4674
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmae;->apiHeader:Llyq;

    .line 4676
    :cond_2
    iget-object v0, p0, Lmae;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 4680
    :sswitch_2
    iget-object v0, p0, Lmae;->a:Lnva;

    if-nez v0, :cond_3

    .line 4681
    new-instance v0, Lnva;

    invoke-direct {v0}, Lnva;-><init>()V

    iput-object v0, p0, Lmae;->a:Lnva;

    .line 4683
    :cond_3
    iget-object v0, p0, Lmae;->a:Lnva;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 4658
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 4626
    iget-object v0, p0, Lmae;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 4627
    const/4 v0, 0x1

    iget-object v1, p0, Lmae;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 4629
    :cond_0
    iget-object v0, p0, Lmae;->a:Lnva;

    if-eqz v0, :cond_1

    .line 4630
    const/4 v0, 0x2

    iget-object v1, p0, Lmae;->a:Lnva;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 4632
    :cond_1
    iget-object v0, p0, Lmae;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 4634
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 4611
    invoke-virtual {p0, p1}, Lmae;->a(Loxn;)Lmae;

    move-result-object v0

    return-object v0
.end method
