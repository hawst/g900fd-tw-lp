.class public final Lmbs;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnvi;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 8179
    invoke-direct {p0}, Loxq;-><init>()V

    .line 8182
    iput-object v0, p0, Lmbs;->apiHeader:Llyq;

    .line 8185
    iput-object v0, p0, Lmbs;->a:Lnvi;

    .line 8179
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 8202
    const/4 v0, 0x0

    .line 8203
    iget-object v1, p0, Lmbs;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 8204
    const/4 v0, 0x1

    iget-object v1, p0, Lmbs;->apiHeader:Llyq;

    .line 8205
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 8207
    :cond_0
    iget-object v1, p0, Lmbs;->a:Lnvi;

    if-eqz v1, :cond_1

    .line 8208
    const/4 v1, 0x2

    iget-object v2, p0, Lmbs;->a:Lnvi;

    .line 8209
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8211
    :cond_1
    iget-object v1, p0, Lmbs;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8212
    iput v0, p0, Lmbs;->ai:I

    .line 8213
    return v0
.end method

.method public a(Loxn;)Lmbs;
    .locals 2

    .prologue
    .line 8221
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 8222
    sparse-switch v0, :sswitch_data_0

    .line 8226
    iget-object v1, p0, Lmbs;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 8227
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmbs;->ah:Ljava/util/List;

    .line 8230
    :cond_1
    iget-object v1, p0, Lmbs;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 8232
    :sswitch_0
    return-object p0

    .line 8237
    :sswitch_1
    iget-object v0, p0, Lmbs;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 8238
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmbs;->apiHeader:Llyq;

    .line 8240
    :cond_2
    iget-object v0, p0, Lmbs;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 8244
    :sswitch_2
    iget-object v0, p0, Lmbs;->a:Lnvi;

    if-nez v0, :cond_3

    .line 8245
    new-instance v0, Lnvi;

    invoke-direct {v0}, Lnvi;-><init>()V

    iput-object v0, p0, Lmbs;->a:Lnvi;

    .line 8247
    :cond_3
    iget-object v0, p0, Lmbs;->a:Lnvi;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 8222
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 8190
    iget-object v0, p0, Lmbs;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 8191
    const/4 v0, 0x1

    iget-object v1, p0, Lmbs;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 8193
    :cond_0
    iget-object v0, p0, Lmbs;->a:Lnvi;

    if-eqz v0, :cond_1

    .line 8194
    const/4 v0, 0x2

    iget-object v1, p0, Lmbs;->a:Lnvi;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 8196
    :cond_1
    iget-object v0, p0, Lmbs;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 8198
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 8175
    invoke-virtual {p0, p1}, Lmbs;->a(Loxn;)Lmbs;

    move-result-object v0

    return-object v0
.end method
