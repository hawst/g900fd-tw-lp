.class public final Lnhl;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lnij;

.field public b:Ljava/lang/Boolean;

.field public c:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 162
    invoke-direct {p0}, Loxq;-><init>()V

    .line 165
    sget-object v0, Lnij;->a:[Lnij;

    iput-object v0, p0, Lnhl;->a:[Lnij;

    .line 162
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 193
    .line 194
    iget-object v1, p0, Lnhl;->a:[Lnij;

    if-eqz v1, :cond_1

    .line 195
    iget-object v2, p0, Lnhl;->a:[Lnij;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 196
    if-eqz v4, :cond_0

    .line 197
    const/4 v5, 0x1

    .line 198
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 195
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 202
    :cond_1
    iget-object v1, p0, Lnhl;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 203
    const/4 v1, 0x2

    iget-object v2, p0, Lnhl;->b:Ljava/lang/Boolean;

    .line 204
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 206
    :cond_2
    iget-object v1, p0, Lnhl;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 207
    const/4 v1, 0x3

    iget-object v2, p0, Lnhl;->c:Ljava/lang/Boolean;

    .line 208
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 210
    :cond_3
    iget-object v1, p0, Lnhl;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 211
    iput v0, p0, Lnhl;->ai:I

    .line 212
    return v0
.end method

.method public a(Loxn;)Lnhl;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 220
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 221
    sparse-switch v0, :sswitch_data_0

    .line 225
    iget-object v2, p0, Lnhl;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 226
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnhl;->ah:Ljava/util/List;

    .line 229
    :cond_1
    iget-object v2, p0, Lnhl;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 231
    :sswitch_0
    return-object p0

    .line 236
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 237
    iget-object v0, p0, Lnhl;->a:[Lnij;

    if-nez v0, :cond_3

    move v0, v1

    .line 238
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnij;

    .line 239
    iget-object v3, p0, Lnhl;->a:[Lnij;

    if-eqz v3, :cond_2

    .line 240
    iget-object v3, p0, Lnhl;->a:[Lnij;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 242
    :cond_2
    iput-object v2, p0, Lnhl;->a:[Lnij;

    .line 243
    :goto_2
    iget-object v2, p0, Lnhl;->a:[Lnij;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 244
    iget-object v2, p0, Lnhl;->a:[Lnij;

    new-instance v3, Lnij;

    invoke-direct {v3}, Lnij;-><init>()V

    aput-object v3, v2, v0

    .line 245
    iget-object v2, p0, Lnhl;->a:[Lnij;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 246
    invoke-virtual {p1}, Loxn;->a()I

    .line 243
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 237
    :cond_3
    iget-object v0, p0, Lnhl;->a:[Lnij;

    array-length v0, v0

    goto :goto_1

    .line 249
    :cond_4
    iget-object v2, p0, Lnhl;->a:[Lnij;

    new-instance v3, Lnij;

    invoke-direct {v3}, Lnij;-><init>()V

    aput-object v3, v2, v0

    .line 250
    iget-object v2, p0, Lnhl;->a:[Lnij;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 254
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnhl;->b:Ljava/lang/Boolean;

    goto :goto_0

    .line 258
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnhl;->c:Ljava/lang/Boolean;

    goto :goto_0

    .line 221
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 174
    iget-object v0, p0, Lnhl;->a:[Lnij;

    if-eqz v0, :cond_1

    .line 175
    iget-object v1, p0, Lnhl;->a:[Lnij;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 176
    if-eqz v3, :cond_0

    .line 177
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 175
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 181
    :cond_1
    iget-object v0, p0, Lnhl;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 182
    const/4 v0, 0x2

    iget-object v1, p0, Lnhl;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 184
    :cond_2
    iget-object v0, p0, Lnhl;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 185
    const/4 v0, 0x3

    iget-object v1, p0, Lnhl;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 187
    :cond_3
    iget-object v0, p0, Lnhl;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 189
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 158
    invoke-virtual {p0, p1}, Lnhl;->a(Loxn;)Lnhl;

    move-result-object v0

    return-object v0
.end method
