.class public final Ldex;
.super Legi;
.source "PG"

# interfaces
.implements Lbc;
.implements Lcsn;
.implements Ldyh;
.implements Lfzz;
.implements Lgak;
.implements Lhyi;
.implements Lkc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Legi;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lcsn;",
        "Ldyh;",
        "Lfzz;",
        "Lgak;",
        "Lhyi;",
        "Lkc;"
    }
.end annotation


# instance fields
.field private final N:Ldfl;

.field private final aA:Ldcm;

.field private final aB:Lcyw;

.field private final aC:Ldfs;

.field private aD:Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;

.field private aE:Ljava/lang/String;

.field private aF:Ljava/lang/String;

.field private aG:I

.field private aH:Lder;

.field private final aI:Lczs;

.field private aJ:Z

.field private aK:Z

.field private aL:Z

.field private aM:Landroid/content/BroadcastReceiver;

.field private aN:I

.field private aO:Ldfj;

.field private aP:Landroid/view/View;

.field private aQ:Z

.field private aR:Landroid/animation/ObjectAnimator;

.field private aS:Ljava/lang/Runnable;

.field private aT:I

.field private aU:Z

.field private aV:Ljava/lang/Runnable;

.field private ah:Ljava/lang/Integer;

.field private ai:I

.field private aj:Ljava/lang/String;

.field private ak:Lcom/google/android/apps/plus/views/PhotoViewPager;

.field private al:Lhyj;

.field private am:I

.field private an:Lizu;

.field private ao:Z

.field private ap:Z

.field private aq:Lely;

.field private ar:Lely;

.field private final as:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ldfu;",
            ">;"
        }
    .end annotation
.end field

.field private final aw:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lejn;",
            ">;"
        }
    .end annotation
.end field

.field private final ax:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final ay:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ldyi;",
            ">;"
        }
    .end annotation
.end field

.field private final az:Ldbh;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 109
    invoke-direct {p0}, Legi;-><init>()V

    .line 157
    new-instance v0, Ldfl;

    invoke-direct {v0, p0}, Ldfl;-><init>(Ldex;)V

    iput-object v0, p0, Ldex;->N:Ldfl;

    .line 162
    iput v2, p0, Ldex;->ai:I

    .line 188
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Ldex;->as:Ljava/util/Set;

    .line 190
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ldex;->aw:Ljava/util/List;

    .line 191
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 193
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Ldex;->ax:Ljava/util/Set;

    .line 195
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Ldex;->ay:Ljava/util/Set;

    .line 198
    new-instance v0, Ldfi;

    invoke-direct {v0}, Ldfi;-><init>()V

    iput-object v0, p0, Ldex;->az:Ldbh;

    .line 199
    new-instance v0, Ldfm;

    invoke-direct {v0, p0}, Ldfm;-><init>(Ldex;)V

    iput-object v0, p0, Ldex;->aA:Ldcm;

    .line 200
    new-instance v0, Ldfn;

    invoke-direct {v0, p0}, Ldfn;-><init>(Ldex;)V

    iput-object v0, p0, Ldex;->aB:Lcyw;

    .line 201
    new-instance v0, Ldfh;

    invoke-direct {v0}, Ldfh;-><init>()V

    iput-object v0, p0, Ldex;->aC:Ldfs;

    .line 209
    new-instance v0, Lder;

    invoke-direct {v0}, Lder;-><init>()V

    iput-object v0, p0, Ldex;->aH:Lder;

    .line 211
    new-instance v0, Lczs;

    invoke-direct {v0}, Lczs;-><init>()V

    iput-object v0, p0, Ldex;->aI:Lczs;

    .line 216
    new-instance v0, Ldey;

    invoke-direct {v0, p0}, Ldey;-><init>(Ldex;)V

    iput-object v0, p0, Ldex;->aM:Landroid/content/BroadcastReceiver;

    .line 241
    const/4 v0, -0x1

    iput v0, p0, Ldex;->aN:I

    .line 247
    const/4 v0, 0x0

    iput-object v0, p0, Ldex;->aS:Ljava/lang/Runnable;

    .line 256
    new-instance v0, Ldez;

    invoke-direct {v0, p0}, Ldez;-><init>(Ldex;)V

    iput-object v0, p0, Ldex;->aV:Ljava/lang/Runnable;

    .line 270
    new-instance v0, Lcsc;

    iget-object v1, p0, Ldex;->av:Llqm;

    invoke-direct {v0, p0, v1, v2}, Lcsc;-><init>(Lu;Llqr;I)V

    .line 1583
    return-void
.end method

.method static synthetic a(Ldex;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Ldex;->ah:Ljava/lang/Integer;

    return-object p1
.end method

.method static synthetic a(Ldex;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Ldex;->aS:Ljava/lang/Runnable;

    return-object p1
.end method

.method static synthetic a(Ldex;)Llnh;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Ldex;->au:Llnh;

    return-object v0
.end method

.method static synthetic a(Ldex;ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 109
    iput p1, p0, Ldex;->ai:I

    iget-object v0, p0, Ldex;->T:Lcmz;

    invoke-virtual {v0, p2}, Lcmz;->a(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Ldex;Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 109
    iget-object v0, p0, Ldex;->al:Lhyj;

    invoke-virtual {v0}, Lhyj;->g()Lhyo;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Ldex;->a(ZLandroid/view/View;)V

    :cond_0
    iget-object v0, p0, Ldex;->aD:Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldex;->aD:Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->setVisibility(I)V

    :cond_1
    iget-object v0, p0, Ldex;->al:Lhyj;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lhyj;->b(I)V

    iget-object v0, p0, Ldex;->ak:Lcom/google/android/apps/plus/views/PhotoViewPager;

    iget v1, p0, Ldex;->aN:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/views/PhotoViewPager;->a(IZ)V

    return-void
.end method

.method static synthetic a(Ldex;ZIILhyo;)V
    .locals 0

    .prologue
    .line 109
    invoke-direct {p0, p1, p2, p3, p4}, Ldex;->a(ZIILhyo;)V

    return-void
.end method

.method private a(Ldo;I)V
    .locals 1

    .prologue
    .line 904
    if-ltz p2, :cond_0

    instance-of v0, p1, Lfdi;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldex;->al:Lhyj;

    .line 906
    invoke-virtual {v0}, Lhyj;->g()Lhyo;

    move-result-object v0

    invoke-interface {v0}, Lhyo;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 907
    iget-object v0, p0, Ldex;->al:Lhyj;

    invoke-virtual {v0}, Lhyj;->g()Lhyo;

    move-result-object v0

    invoke-interface {v0}, Lhyo;->d()Ljava/lang/Object;

    move-result-object v0

    .line 908
    check-cast p1, Lfdi;

    invoke-interface {p1, v0, p2}, Lfdi;->a(Ljava/lang/Object;I)V

    .line 910
    :cond_0
    return-void
.end method

.method private a(ZIILhyo;)V
    .locals 9

    .prologue
    const/16 v3, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 775
    if-nez p1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Ldex;->aJ:Z

    .line 776
    iget-boolean v0, p0, Ldex;->ap:Z

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    .line 779
    invoke-virtual {p0}, Ldex;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->finish()V

    .line 824
    :goto_1
    return-void

    :cond_0
    move v0, v2

    .line 775
    goto :goto_0

    .line 783
    :cond_1
    invoke-virtual {p0}, Ldex;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v4, "refresh_collection"

    invoke-virtual {v0, v4, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 784
    iget-boolean v4, p0, Ldex;->ao:Z

    if-nez v4, :cond_3

    if-nez p1, :cond_2

    if-eqz v0, :cond_3

    .line 785
    :cond_2
    iput-boolean v1, p0, Ldex;->ao:Z

    .line 787
    iget-object v4, p0, Ldex;->P:Lhee;

    invoke-interface {v4}, Lhee;->f()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 788
    invoke-virtual {p0}, Ldex;->n()Lz;

    move-result-object v4

    iget-object v5, p0, Ldex;->P:Lhee;

    invoke-interface {v5}, Lhee;->d()I

    move-result v5

    .line 789
    invoke-virtual {p0}, Ldex;->k()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "view_id"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Ldex;->aE:Ljava/lang/String;

    iget-object v8, p0, Ldex;->aF:Ljava/lang/String;

    .line 788
    invoke-static {v4, v5, v6, v7, v8}, Ldff;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 793
    :cond_3
    if-eqz p1, :cond_8

    .line 794
    invoke-virtual {p0}, Ldex;->x()Landroid/view/View;

    move-result-object v4

    invoke-direct {p0, v0, v4}, Ldex;->a(ZLandroid/view/View;)V

    .line 795
    if-nez v0, :cond_6

    :goto_2
    invoke-virtual {p0}, Ldex;->x()Landroid/view/View;

    move-result-object v4

    const v0, 0x7f100317

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    if-eqz v1, :cond_7

    move v0, v2

    :goto_3
    invoke-virtual {v5, v0}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f100318

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 796
    iput v2, p0, Ldex;->aN:I

    .line 798
    iput v2, p0, Ldex;->aT:I

    .line 805
    :goto_4
    iget-boolean v0, p0, Ldex;->aQ:Z

    if-eqz v0, :cond_4

    .line 808
    iget-object v0, p0, Ldex;->al:Lhyj;

    iget v1, p0, Ldex;->aN:I

    invoke-virtual {v0, v1}, Lhyj;->b(I)V

    .line 811
    :cond_4
    iget v0, p0, Ldex;->am:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_9

    iget v0, p0, Ldex;->am:I

    .line 812
    :goto_5
    iget-object v1, p0, Ldex;->al:Lhyj;

    invoke-virtual {v1, p4, v0}, Lhyj;->a(Lhyo;I)Lhyo;

    .line 817
    iget-object v1, p0, Ldex;->ak:Lcom/google/android/apps/plus/views/PhotoViewPager;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/plus/views/PhotoViewPager;->a(IZ)V

    .line 818
    if-nez v0, :cond_5

    if-nez p1, :cond_5

    .line 819
    invoke-virtual {p0, v2}, Ldex;->g_(I)V

    .line 822
    :cond_5
    const/4 v0, 0x0

    iput-object v0, p0, Ldex;->an:Lizu;

    .line 823
    iget-object v0, p0, Ldex;->O:Lhje;

    invoke-virtual {v0}, Lhje;->a()V

    goto/16 :goto_1

    :cond_6
    move v1, v2

    .line 795
    goto :goto_2

    :cond_7
    move v0, v3

    goto :goto_3

    .line 800
    :cond_8
    iput-boolean v1, p0, Ldex;->ap:Z

    .line 801
    iput p2, p0, Ldex;->aN:I

    .line 802
    iput p3, p0, Ldex;->aT:I

    goto :goto_4

    .line 811
    :cond_9
    iget v0, p0, Ldex;->aN:I

    goto :goto_5
.end method

.method private a(ZLandroid/view/View;)V
    .locals 3

    .prologue
    const/16 v1, 0x8

    .line 952
    const v0, 0x7f100318

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 953
    const v0, 0x7f100317

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 954
    return-void

    :cond_0
    move v0, v1

    .line 952
    goto :goto_0
.end method

.method static synthetic a(Ldex;Z)Z
    .locals 0

    .prologue
    .line 109
    iput-boolean p1, p0, Ldex;->aK:Z

    return p1
.end method

.method private ac()Z
    .locals 2

    .prologue
    .line 446
    invoke-virtual {p0}, Ldex;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "all_photos_row_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private ad()V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1085
    iget-object v0, p0, Ldex;->aV:Ljava/lang/Runnable;

    invoke-static {}, Llsx;->d()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1086
    const/4 v0, 0x0

    iput-object v0, p0, Ldex;->aR:Landroid/animation/ObjectAnimator;

    .line 1087
    iput-boolean v2, p0, Ldex;->aQ:Z

    .line 1088
    invoke-virtual {p0, v2}, Ldex;->a(Z)V

    .line 1089
    invoke-virtual {p0}, Ldex;->x()Landroid/view/View;

    move-result-object v0

    .line 1090
    if-nez v0, :cond_0

    .line 1102
    :goto_0
    return-void

    .line 1093
    :cond_0
    iget-object v1, p0, Ldex;->aP:Landroid/view/View;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Landroid/view/View;->setAlpha(F)V

    .line 1100
    new-instance v1, Ldfk;

    invoke-direct {v1, p0, v0}, Ldfk;-><init>(Ldex;Landroid/view/View;)V

    iput-object v1, p0, Ldex;->aS:Ljava/lang/Runnable;

    .line 1101
    iget-object v0, p0, Ldex;->aS:Ljava/lang/Runnable;

    invoke-static {v0}, Llsx;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method static synthetic b(Ldex;)I
    .locals 1

    .prologue
    .line 109
    iget v0, p0, Ldex;->am:I

    return v0
.end method

.method static synthetic b(Ldex;Landroid/view/View;)V
    .locals 4

    .prologue
    .line 109
    iget-object v0, p0, Ldex;->aV:Ljava/lang/Runnable;

    invoke-static {}, Llsx;->d()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Ldex;->aO:Ldfj;

    new-instance v1, Ldfb;

    const-class v2, Ljava/lang/Float;

    const-string v3, "animationPosition"

    invoke-direct {v1, v2, v3}, Ldfb;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    const/4 v2, 0x2

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Ldex;->aR:Landroid/animation/ObjectAnimator;

    iget-object v0, p0, Ldex;->aR:Landroid/animation/ObjectAnimator;

    new-instance v1, Ldfc;

    invoke-direct {v1, p1}, Ldfc;-><init>(Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    iget-object v0, p0, Ldex;->aR:Landroid/animation/ObjectAnimator;

    new-instance v1, Ldfd;

    invoke-direct {v1, p0}, Ldfd;-><init>(Ldex;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v0, p0, Ldex;->aR:Landroid/animation/ObjectAnimator;

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    iget-object v0, p0, Ldex;->aR:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method static synthetic b(Ldex;Z)Z
    .locals 0

    .prologue
    .line 109
    iput-boolean p1, p0, Ldex;->aL:Z

    return p1
.end method

.method static synthetic c(Ldex;)Lcom/google/android/apps/plus/views/PhotoViewPager;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Ldex;->ak:Lcom/google/android/apps/plus/views/PhotoViewPager;

    return-object v0
.end method

.method static synthetic c(Ldex;Z)Z
    .locals 0

    .prologue
    .line 109
    iput-boolean p1, p0, Ldex;->aQ:Z

    return p1
.end method

.method static synthetic d(Ldex;)V
    .locals 0

    .prologue
    .line 109
    invoke-direct {p0}, Ldex;->ad()V

    return-void
.end method

.method private d(I)Z
    .locals 1

    .prologue
    .line 440
    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    const/4 v0, 0x3

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic e(Ldex;)Llnl;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Ldex;->at:Llnl;

    return-object v0
.end method

.method static synthetic f(Ldex;)Lhee;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Ldex;->P:Lhee;

    return-object v0
.end method

.method static synthetic g(Ldex;)I
    .locals 1

    .prologue
    .line 109
    iget v0, p0, Ldex;->aT:I

    return v0
.end method

.method static synthetic h(Ldex;)Lely;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Ldex;->aq:Lely;

    return-object v0
.end method

.method static synthetic i(Ldex;)Lcsg;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Ldex;->U:Lcsg;

    return-object v0
.end method

.method static synthetic j(Ldex;)Llnl;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Ldex;->at:Llnl;

    return-object v0
.end method

.method static synthetic k(Ldex;)Lhee;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Ldex;->P:Lhee;

    return-object v0
.end method

.method static synthetic l(Ldex;)Landroid/view/View;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Ldex;->aP:Landroid/view/View;

    return-object v0
.end method

.method static synthetic m(Ldex;)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Ldex;->ah:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic n(Ldex;)Lcmz;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Ldex;->T:Lcmz;

    return-object v0
.end method

.method static synthetic o(Ldex;)I
    .locals 1

    .prologue
    .line 109
    iget v0, p0, Ldex;->ai:I

    return v0
.end method

.method static synthetic p(Ldex;)Llnl;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Ldex;->at:Llnl;

    return-object v0
.end method

.method static synthetic q(Ldex;)Z
    .locals 1

    .prologue
    .line 109
    iget-boolean v0, p0, Ldex;->aJ:Z

    return v0
.end method

.method static synthetic r(Ldex;)Z
    .locals 1

    .prologue
    .line 109
    iget-boolean v0, p0, Ldex;->aK:Z

    return v0
.end method

.method static synthetic s(Ldex;)Llnh;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Ldex;->au:Llnh;

    return-object v0
.end method

.method static synthetic t(Ldex;)Llnl;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Ldex;->at:Llnl;

    return-object v0
.end method

.method static synthetic u(Ldex;)Llnl;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Ldex;->at:Llnl;

    return-object v0
.end method


# virtual methods
.method public A()V
    .locals 2

    .prologue
    .line 543
    iget-object v0, p0, Ldex;->aD:Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->b()V

    .line 544
    const/4 v0, 0x0

    iput-object v0, p0, Ldex;->aD:Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;

    .line 547
    iget-object v0, p0, Ldex;->al:Lhyj;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lhyj;->d(I)V

    .line 548
    invoke-super {p0}, Legi;->A()V

    .line 549
    return-void
.end method

.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 610
    sget-object v0, Lhmw;->ah:Lhmw;

    return-object v0
.end method

.method public G_()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 920
    iget-object v0, p0, Ldex;->aB:Lcyw;

    invoke-interface {v0, v2}, Lcyw;->a(Z)V

    move v1, v2

    move v3, v2

    .line 925
    :goto_0
    iget-object v0, p0, Ldex;->aw:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 926
    iget-object v0, p0, Ldex;->aw:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lejn;

    .line 928
    invoke-interface {v0}, Lejn;->a()Z

    move-result v0

    or-int/2addr v3, v0

    .line 925
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 931
    :cond_0
    iget-object v0, p0, Ldex;->as:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldfu;

    .line 932
    invoke-interface {v0}, Ldfu;->a()Z

    move-result v0

    or-int/2addr v3, v0

    .line 933
    goto :goto_1

    .line 935
    :cond_1
    iget-object v0, p0, Ldex;->aA:Ldcm;

    invoke-interface {v0}, Ldcm;->b()Z

    move-result v0

    if-nez v0, :cond_2

    if-eqz v3, :cond_3

    :cond_2
    const/4 v2, 0x1

    :cond_3
    return v2
.end method

.method protected U()Z
    .locals 1

    .prologue
    .line 451
    const/4 v0, 0x0

    return v0
.end method

.method public V()Z
    .locals 1

    .prologue
    .line 624
    iget-boolean v0, p0, Ldex;->ac:Z

    if-eqz v0, :cond_0

    .line 625
    invoke-super {p0}, Legi;->a()Z

    move-result v0

    .line 627
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Legi;->V()Z

    move-result v0

    goto :goto_0
.end method

.method public W()Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;
    .locals 1

    .prologue
    .line 632
    iget-object v0, p0, Ldex;->aD:Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;

    return-object v0
.end method

.method public X()Z
    .locals 1

    .prologue
    .line 680
    iget-object v0, p0, Ldex;->aC:Ldfs;

    invoke-interface {v0}, Ldfs;->a()Z

    move-result v0

    return v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 25

    .prologue
    .line 294
    const v4, 0x7f0400dd

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    invoke-super {v0, v1, v2, v3, v4}, Legi;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;

    move-result-object v24

    .line 297
    invoke-virtual/range {p0 .. p0}, Ldex;->k()Landroid/os/Bundle;

    move-result-object v22

    .line 298
    move-object/from16 v0, p0

    iget-object v4, v0, Ldex;->P:Lhee;

    invoke-interface {v4}, Lhee;->d()I

    move-result v16

    .line 300
    if-eqz p3, :cond_6

    .line 301
    move-object/from16 v0, p0

    iget-object v4, v0, Ldex;->az:Ldbh;

    const-string v5, "show_hashtags"

    move-object/from16 v0, p3

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    invoke-interface {v4, v5}, Ldbh;->a(Z)V

    .line 302
    move-object/from16 v0, p0

    iget-object v4, v0, Ldex;->aA:Ldcm;

    const-string v5, "show_shapes"

    move-object/from16 v0, p3

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    invoke-interface {v4, v5}, Ldcm;->a(Z)V

    .line 303
    const-string v4, "current_item"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Ldex;->am:I

    .line 304
    move-object/from16 v0, p0

    iget-object v4, v0, Ldex;->aC:Ldfs;

    const-string v5, "full_screen"

    move-object/from16 v0, p3

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    invoke-interface {v4, v5}, Ldfs;->a(Z)V

    .line 305
    const-string v4, "pending_request_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 306
    const-string v4, "pending_request_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Ldex;->ah:Ljava/lang/Integer;

    .line 308
    :cond_0
    const-string v4, "operation_type"

    const/4 v5, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Ldex;->ai:I

    .line 309
    const-string v4, "collection_refreshed"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    move-object/from16 v0, p0

    iput-boolean v4, v0, Ldex;->ao:Z

    .line 310
    const-string v4, "loaded_not_empty"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    move-object/from16 v0, p0

    iput-boolean v4, v0, Ldex;->ap:Z

    .line 311
    const-string v4, "slideshow_enabled"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    move-object/from16 v0, p0

    iput-boolean v4, v0, Ldex;->aK:Z

    .line 312
    const-string v4, "local_slideshow"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    move-object/from16 v0, p0

    iput-boolean v4, v0, Ldex;->aL:Z

    .line 313
    const-string v4, "performed_in_animation"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    move-object/from16 v0, p0

    iput-boolean v4, v0, Ldex;->aU:Z

    .line 314
    const-string v4, "view_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Ldex;->aj:Ljava/lang/String;

    .line 315
    const-string v4, "all_photos_offset"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Ldex;->aT:I

    .line 325
    :goto_0
    const-string v4, "com.google.android.libraries.social.notifications.FROM_NOTIFICATION"

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 326
    move-object/from16 v0, p0

    iget-object v4, v0, Ldex;->at:Llnl;

    invoke-static {v4}, Lcom/google/android/apps/plus/service/SlideshowService;->a(Landroid/content/Context;)V

    .line 329
    :cond_1
    const v4, 0x7f100315

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;

    move-object/from16 v0, p0

    iput-object v4, v0, Ldex;->aD:Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;

    .line 330
    const v4, 0x7f100314

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Ldex;->aP:Landroid/view/View;

    .line 332
    const-string v4, "thumbnail_view_bounds"

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Landroid/graphics/RectF;

    .line 333
    if-nez v4, :cond_7

    .line 334
    move-object/from16 v0, p0

    iget-boolean v4, v0, Ldex;->aU:Z

    if-nez v4, :cond_2

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Ldex;->aQ:Z

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Ldex;->aU:Z

    move-object/from16 v0, p0

    iget-object v4, v0, Ldex;->aD:Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->setVisibility(I)V

    new-instance v4, Ldfa;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Ldfa;-><init>(Ldex;)V

    const-wide/16 v6, 0x1f4

    invoke-static {v4, v6, v7}, Llsx;->a(Ljava/lang/Runnable;J)V

    .line 339
    :cond_2
    :goto_1
    const-string v4, "auth_key"

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Ldex;->aE:Ljava/lang/String;

    .line 340
    const-string v4, "event_id"

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Ldex;->aF:Ljava/lang/String;

    .line 342
    move-object/from16 v0, p0

    iget-object v9, v0, Ldex;->aj:Ljava/lang/String;

    .line 343
    const-string v4, "disable_photo_comments"

    const/4 v5, 0x0

    .line 344
    move-object/from16 v0, v22

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v17

    .line 345
    const/4 v4, -0x1

    move/from16 v0, v16

    if-eq v0, v4, :cond_8

    move-object/from16 v0, p0

    iget-object v4, v0, Ldex;->at:Llnl;

    .line 346
    move/from16 v0, v16

    invoke-static {v4, v0}, Ljvj;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    const/4 v11, 0x1

    .line 348
    :goto_2
    const-string v4, "photo_ref"

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v7

    check-cast v7, Lizu;

    .line 350
    const-string v4, "picker_mode"

    const/4 v5, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v5

    .line 351
    if-eqz v7, :cond_9

    const/4 v4, 0x3

    if-ne v5, v4, :cond_9

    const/4 v4, 0x1

    move/from16 v23, v4

    .line 354
    :goto_3
    if-eqz v23, :cond_a

    .line 355
    new-instance v4, Lfde;

    move-object/from16 v0, p0

    iget-object v5, v0, Ldex;->at:Llnl;

    invoke-virtual/range {p0 .. p0}, Ldex;->q()Lae;

    move-result-object v6

    const-string v8, "prevent_edit"

    const/4 v9, 0x0

    .line 356
    move-object/from16 v0, v22

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    const-string v9, "prevent_share"

    const/4 v10, 0x0

    .line 357
    move-object/from16 v0, v22

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    const-string v10, "prevent_delete"

    const/4 v11, 0x0

    .line 358
    move-object/from16 v0, v22

    invoke-virtual {v0, v10, v11}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v10

    invoke-direct/range {v4 .. v10}, Lfde;-><init>(Landroid/content/Context;Lae;Lizu;ZZZ)V

    move-object/from16 v0, p0

    iput-object v4, v0, Ldex;->al:Lhyj;

    .line 390
    :goto_4
    move-object/from16 v0, p0

    iget-object v4, v0, Ldex;->al:Lhyj;

    move-object/from16 v0, p0

    invoke-virtual {v4, v0}, Lhyj;->a(Lhyi;)V

    .line 391
    const v4, 0x7f100316

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/views/PhotoViewPager;

    move-object/from16 v0, p0

    iput-object v4, v0, Ldex;->ak:Lcom/google/android/apps/plus/views/PhotoViewPager;

    .line 392
    move-object/from16 v0, p0

    iget-object v4, v0, Ldex;->ak:Lcom/google/android/apps/plus/views/PhotoViewPager;

    move-object/from16 v0, p0

    iget-object v5, v0, Ldex;->al:Lhyj;

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/views/PhotoViewPager;->a(Lip;)V

    .line 393
    move-object/from16 v0, p0

    iget-object v4, v0, Ldex;->ak:Lcom/google/android/apps/plus/views/PhotoViewPager;

    move-object/from16 v0, p0

    invoke-virtual {v4, v0}, Lcom/google/android/apps/plus/views/PhotoViewPager;->a(Lgak;)V

    .line 394
    move-object/from16 v0, p0

    iget-object v4, v0, Ldex;->ak:Lcom/google/android/apps/plus/views/PhotoViewPager;

    move-object/from16 v0, p0

    invoke-virtual {v4, v0}, Lcom/google/android/apps/plus/views/PhotoViewPager;->a(Lkc;)V

    .line 397
    move-object/from16 v0, p0

    iget-boolean v4, v0, Ldex;->aQ:Z

    if-nez v4, :cond_e

    const/4 v4, 0x1

    :goto_5
    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-direct {v0, v4, v1}, Ldex;->a(ZLandroid/view/View;)V

    .line 399
    if-nez v23, :cond_3

    .line 400
    invoke-direct/range {p0 .. p0}, Ldex;->ac()Z

    move-result v4

    if-eqz v4, :cond_f

    .line 401
    invoke-virtual/range {p0 .. p0}, Ldex;->w()Lbb;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    new-instance v7, Ldfe;

    move-object/from16 v0, p0

    invoke-direct {v7, v0}, Ldfe;-><init>(Ldex;)V

    invoke-virtual {v4, v5, v6, v7}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 407
    :cond_3
    :goto_6
    move-object/from16 v0, p0

    iget-object v4, v0, Ldex;->P:Lhee;

    invoke-interface {v4}, Lhee;->f()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 408
    new-instance v13, Landroid/view/ContextThemeWrapper;

    move-object/from16 v0, p0

    iget-object v4, v0, Ldex;->at:Llnl;

    const v5, 0x7f0901dd

    invoke-direct {v13, v4, v5}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 410
    new-instance v12, Lely;

    .line 411
    invoke-virtual/range {p0 .. p0}, Ldex;->p()Lae;

    move-result-object v14

    invoke-virtual/range {p0 .. p0}, Ldex;->w()Lbb;

    move-result-object v15

    const/16 v17, 0x0

    invoke-direct/range {v12 .. v17}, Lely;-><init>(Landroid/content/Context;Lae;Lbb;II)V

    move-object/from16 v0, p0

    iput-object v12, v0, Ldex;->aq:Lely;

    .line 412
    move-object/from16 v0, p0

    iget-object v4, v0, Ldex;->aq:Lely;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lely;->g(Z)V

    .line 413
    move-object/from16 v0, p0

    iget-object v4, v0, Ldex;->aq:Lely;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lely;->a(Z)V

    .line 414
    move-object/from16 v0, p0

    iget-object v4, v0, Ldex;->aq:Lely;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lely;->e(Z)V

    .line 415
    move-object/from16 v0, p0

    iget-object v4, v0, Ldex;->aq:Lely;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lely;->b(Z)V

    .line 416
    move-object/from16 v0, p0

    iget-object v4, v0, Ldex;->aq:Lely;

    const/4 v5, -0x1

    invoke-virtual {v4, v5}, Lely;->j_(I)V

    .line 417
    move-object/from16 v0, p0

    iget-object v4, v0, Ldex;->aq:Lely;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lely;->d(Z)V

    .line 418
    move-object/from16 v0, p0

    iget-object v4, v0, Ldex;->aq:Lely;

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Lely;->a(Landroid/os/Bundle;)V

    .line 420
    new-instance v12, Lely;

    .line 421
    invoke-virtual/range {p0 .. p0}, Ldex;->p()Lae;

    move-result-object v14

    invoke-virtual/range {p0 .. p0}, Ldex;->w()Lbb;

    move-result-object v15

    const/16 v17, 0x1

    invoke-direct/range {v12 .. v17}, Lely;-><init>(Landroid/content/Context;Lae;Lbb;II)V

    move-object/from16 v0, p0

    iput-object v12, v0, Ldex;->ar:Lely;

    .line 422
    move-object/from16 v0, p0

    iget-object v4, v0, Ldex;->ar:Lely;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lely;->e(Z)V

    .line 423
    move-object/from16 v0, p0

    iget-object v4, v0, Ldex;->ar:Lely;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lely;->g(Z)V

    .line 426
    :cond_4
    if-eqz p3, :cond_5

    .line 427
    const-string v4, "auto_switch_ref"

    .line 428
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lizu;

    move-object/from16 v0, p0

    iput-object v4, v0, Ldex;->an:Lizu;

    .line 429
    const-string v4, "reset_pager_after_zoom"

    .line 430
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    .line 431
    if-eqz v4, :cond_5

    .line 432
    new-instance v4, Ldfk;

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-direct {v4, v0, v1}, Ldfk;-><init>(Ldex;Landroid/view/View;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Ldex;->aS:Ljava/lang/Runnable;

    .line 436
    :cond_5
    return-object v24

    .line 317
    :cond_6
    const/4 v4, -0x1

    move-object/from16 v0, p0

    iput v4, v0, Ldex;->am:I

    .line 318
    move-object/from16 v0, p0

    iget-object v4, v0, Ldex;->az:Ldbh;

    const/4 v5, 0x0

    invoke-interface {v4, v5}, Ldbh;->a(Z)V

    .line 319
    move-object/from16 v0, p0

    iget-object v4, v0, Ldex;->aA:Ldcm;

    const/4 v5, 0x0

    invoke-interface {v4, v5}, Ldcm;->a(Z)V

    .line 320
    move-object/from16 v0, p0

    iget-object v4, v0, Ldex;->aC:Ldfs;

    const/4 v5, 0x0

    invoke-interface {v4, v5}, Ldfs;->a(Z)V

    .line 321
    const-string v4, "view_id"

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Ldex;->aj:Ljava/lang/String;

    .line 322
    const-string v4, "all_photos_offset"

    const/4 v5, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Ldex;->aT:I

    goto/16 :goto_0

    .line 336
    :cond_7
    if-eqz v4, :cond_2

    move-object/from16 v0, p0

    iget-boolean v5, v0, Ldex;->aU:Z

    if-nez v5, :cond_2

    invoke-static {}, Llsj;->c()Z

    move-result v5

    if-eqz v5, :cond_2

    new-instance v5, Ldfj;

    move-object/from16 v0, p0

    invoke-direct {v5, v0, v4}, Ldfj;-><init>(Ldex;Landroid/graphics/RectF;)V

    move-object/from16 v0, p0

    iput-object v5, v0, Ldex;->aO:Ldfj;

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Ldex;->aQ:Z

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Ldex;->aU:Z

    move-object/from16 v0, p0

    iget-object v4, v0, Ldex;->aD:Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->setVisibility(I)V

    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Ldex;->a(Z)V

    move-object/from16 v0, p0

    iget-object v4, v0, Ldex;->aP:Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/View;->setAlpha(F)V

    move-object/from16 v0, p0

    iget-object v4, v0, Ldex;->aV:Ljava/lang/Runnable;

    const-wide/16 v6, 0x1f4

    invoke-static {v4, v6, v7}, Llsx;->a(Ljava/lang/Runnable;J)V

    goto/16 :goto_1

    .line 346
    :cond_8
    const/4 v11, 0x0

    goto/16 :goto_2

    .line 351
    :cond_9
    const/4 v4, 0x0

    move/from16 v23, v4

    goto/16 :goto_3

    .line 359
    :cond_a
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Ldex;->c(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 360
    new-instance v4, Lezw;

    move-object/from16 v0, p0

    iget-object v5, v0, Ldex;->at:Llnl;

    .line 361
    invoke-virtual/range {p0 .. p0}, Ldex;->q()Lae;

    move-result-object v6

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Ldex;->P:Lhee;

    invoke-interface {v8}, Lhee;->d()I

    move-result v8

    const-string v10, "force_return_edit_list"

    const/4 v12, 0x0

    .line 362
    move-object/from16 v0, v22

    invoke-virtual {v0, v10, v12}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v10

    const-string v12, "prevent_edit"

    const/4 v13, 0x0

    .line 363
    move-object/from16 v0, v22

    invoke-virtual {v0, v12, v13}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v12

    const-string v13, "prevent_share"

    const/4 v14, 0x0

    .line 364
    move-object/from16 v0, v22

    invoke-virtual {v0, v13, v14}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v13

    const-string v14, "prevent_delete"

    const/4 v15, 0x0

    .line 365
    move-object/from16 v0, v22

    invoke-virtual {v0, v14, v15}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v14

    move-object/from16 v0, p0

    iget-boolean v15, v0, Ldex;->ag:Z

    invoke-direct/range {v4 .. v15}, Lezw;-><init>(Landroid/content/Context;Lae;Landroid/database/Cursor;ILjava/lang/String;ZZZZZZ)V

    move-object/from16 v0, p0

    iput-object v4, v0, Ldex;->al:Lhyj;

    goto/16 :goto_4

    .line 367
    :cond_b
    invoke-direct/range {p0 .. p0}, Ldex;->ac()Z

    move-result v4

    if-eqz v4, :cond_c

    .line 368
    const-string v4, "photo_ref"

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v13

    check-cast v13, Lizu;

    .line 369
    new-instance v5, Levb;

    move-object/from16 v0, p0

    iget-object v6, v0, Ldex;->at:Llnl;

    invoke-virtual/range {p0 .. p0}, Ldex;->q()Lae;

    move-result-object v7

    move-object/from16 v0, p0

    iget-boolean v9, v0, Ldex;->ag:Z

    const-string v4, "all_photos_row_id"

    .line 371
    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v10

    const-string v4, "force_return_edit_list"

    const/4 v8, 0x0

    .line 372
    move-object/from16 v0, v22

    invoke-virtual {v0, v4, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v12

    const-string v4, "all_photos_offset"

    const/4 v8, 0x0

    .line 373
    move-object/from16 v0, v22

    invoke-virtual {v0, v4, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v14

    move/from16 v8, v16

    invoke-direct/range {v5 .. v14}, Levb;-><init>(Landroid/content/Context;Lae;IZJZLizu;I)V

    move-object/from16 v0, p0

    iput-object v5, v0, Ldex;->al:Lhyj;

    goto/16 :goto_4

    .line 374
    :cond_c
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Ldex;->d(I)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 375
    new-instance v12, Lfbw;

    move-object/from16 v0, p0

    iget-object v13, v0, Ldex;->at:Llnl;

    .line 376
    invoke-virtual/range {p0 .. p0}, Ldex;->q()Lae;

    move-result-object v14

    const/4 v15, 0x0

    const-string v4, "prevent_edit"

    const/4 v5, 0x0

    .line 377
    move-object/from16 v0, v22

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v17

    const-string v4, "prevent_share"

    const/4 v5, 0x0

    .line 378
    move-object/from16 v0, v22

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v18

    const-string v4, "prevent_delete"

    const/4 v5, 0x0

    .line 379
    move-object/from16 v0, v22

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v19

    const-string v4, "force_return_edit_list"

    const/4 v5, 0x0

    .line 380
    move-object/from16 v0, v22

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v20

    const-string v4, "selected_only"

    const/4 v5, 0x0

    .line 381
    move-object/from16 v0, v22

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v21

    move/from16 v22, v11

    invoke-direct/range {v12 .. v22}, Lfbw;-><init>(Landroid/content/Context;Lae;Landroid/database/Cursor;IZZZZZZ)V

    move-object/from16 v0, p0

    iput-object v12, v0, Ldex;->al:Lhyj;

    goto/16 :goto_4

    .line 384
    :cond_d
    new-instance v11, Levd;

    move-object/from16 v0, p0

    iget-object v12, v0, Ldex;->at:Llnl;

    invoke-virtual/range {p0 .. p0}, Ldex;->q()Lae;

    move-result-object v13

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Ldex;->aE:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ldex;->aF:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Ldex;->ag:Z

    move/from16 v20, v0

    const-string v4, "show_oob_tile"

    const/4 v5, 0x0

    .line 386
    move-object/from16 v0, v22

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v21

    const-string v4, "launch_comments_at_start"

    const/4 v5, 0x0

    .line 387
    move-object/from16 v0, v22

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v22

    move-object v15, v9

    invoke-direct/range {v11 .. v22}, Levd;-><init>(Landroid/content/Context;Lae;Landroid/database/Cursor;Ljava/lang/String;IZLjava/lang/String;Ljava/lang/String;ZZZ)V

    move-object/from16 v0, p0

    iput-object v11, v0, Ldex;->al:Lhyj;

    goto/16 :goto_4

    .line 397
    :cond_e
    const/4 v4, 0x0

    goto/16 :goto_5

    .line 403
    :cond_f
    invoke-virtual/range {p0 .. p0}, Ldex;->w()Lbb;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v4, v5, v6, v0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    goto/16 :goto_6
.end method

.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 709
    invoke-virtual/range {p0 .. p0}, Ldex;->k()Landroid/os/Bundle;

    move-result-object v2

    const-string v1, "tile_id"

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    const-string v1, "photo_id"

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    const-string v1, "oob_only"

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v14

    const-string v1, "show_oob_tile"

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    const/4 v7, 0x0

    const-string v1, "shareables"

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "shareables"

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v7

    :cond_0
    const-string v1, "selected_only"

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "photo_picker_selected"

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Ljcn;

    if-eqz v5, :cond_2

    invoke-virtual {v5}, Ljcn;->k()I

    move-result v1

    if-lez v1, :cond_2

    const-string v1, "photo_ref"

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v6

    check-cast v6, Lizu;

    new-instance v1, Lfbv;

    move-object/from16 v0, p0

    iget-object v2, v0, Ldex;->at:Llnl;

    move-object/from16 v0, p0

    iget-object v3, v0, Ldex;->P:Lhee;

    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    const/4 v4, 0x0

    const-class v8, Ljuf;

    invoke-virtual {v5, v8}, Ljcn;->a(Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v5

    move-object/from16 v0, p0

    iget v8, v0, Ldex;->aa:I

    invoke-direct/range {v1 .. v8}, Lfbv;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/util/List;Lizu;Ljava/util/List;I)V

    move-object v2, v1

    .line 711
    :goto_0
    instance-of v1, v2, Lfdi;

    if-eqz v1, :cond_1

    .line 712
    move-object/from16 v0, p0

    iget-object v1, v0, Ldex;->an:Lizu;

    if-eqz v1, :cond_6

    move-object v1, v2

    .line 713
    check-cast v1, Lfdi;

    move-object/from16 v0, p0

    iget-object v3, v0, Ldex;->an:Lizu;

    invoke-interface {v1, v3}, Lfdi;->a(Lizu;)V

    .line 719
    :cond_1
    :goto_1
    return-object v2

    .line 709
    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Ldex;->aj:Ljava/lang/String;

    invoke-static {v1}, Ljvj;->m(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "photo_ref"

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lizu;

    new-instance v5, Lezk;

    move-object/from16 v0, p0

    iget-object v6, v0, Ldex;->at:Llnl;

    const-string v3, "filter"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v8

    move-object/from16 v0, p0

    iget-object v2, v0, Ldex;->an:Lizu;

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget-object v9, v0, Ldex;->an:Lizu;

    :goto_2
    move-object/from16 v0, p0

    iget-object v10, v0, Ldex;->aj:Ljava/lang/String;

    const/4 v11, 0x0

    invoke-direct/range {v5 .. v11}, Lezk;-><init>(Landroid/content/Context;Ljava/util/List;ILizu;Ljava/lang/String;Landroid/net/Uri;)V

    move-object v2, v5

    goto :goto_0

    :cond_3
    move-object v9, v1

    goto :goto_2

    :cond_4
    const-string v1, "picker_mode"

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Ldex;->d(I)Z

    move-result v1

    if-eqz v1, :cond_5

    new-instance v8, Lfbv;

    move-object/from16 v0, p0

    iget-object v9, v0, Ldex;->at:Llnl;

    move-object/from16 v0, p0

    iget-object v1, v0, Ldex;->P:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v10

    move-object/from16 v0, p0

    iget-object v11, v0, Ldex;->aj:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v0, v0, Ldex;->aa:I

    move/from16 v16, v0

    move-object v15, v7

    invoke-direct/range {v8 .. v16}, Lfbv;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/util/List;I)V

    move-object v2, v8

    goto :goto_0

    :cond_5
    new-instance v1, Lfbo;

    move-object/from16 v0, p0

    iget-object v2, v0, Ldex;->at:Llnl;

    move-object/from16 v0, p0

    iget-object v3, v0, Ldex;->P:Lhee;

    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Ldex;->aj:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v9, v0, Ldex;->aa:I

    move-object v5, v12

    move-object v6, v13

    move v7, v8

    move v8, v14

    invoke-direct/range {v1 .. v9}, Lfbo;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZI)V

    move-object v2, v1

    goto/16 :goto_0

    .line 714
    :cond_6
    move-object/from16 v0, p0

    iget v1, v0, Ldex;->am:I

    const/4 v3, -0x1

    if-eq v1, v3, :cond_1

    .line 715
    move-object/from16 v0, p0

    iget v1, v0, Ldex;->am:I

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v1}, Ldex;->a(Ldo;I)V

    goto/16 :goto_1
.end method

.method public a(IFI)V
    .locals 0

    .prologue
    .line 886
    return-void
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v0, -0x1

    .line 765
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    const/4 v1, 0x1

    .line 766
    :goto_0
    if-nez v1, :cond_1

    .line 767
    invoke-interface {p1}, Landroid/database/Cursor;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "start_position"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    :cond_1
    iget v2, p0, Ldex;->aT:I

    new-instance v3, Lhyf;

    invoke-direct {v3, p1}, Lhyf;-><init>(Landroid/database/Cursor;)V

    .line 766
    invoke-direct {p0, v1, v0, v2, v3}, Ldex;->a(ZIILhyo;)V

    .line 771
    return-void

    .line 765
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public a(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1156
    iget-object v0, p0, Ldex;->aO:Ldfj;

    if-eqz v0, :cond_0

    .line 1157
    iget-object v0, p0, Ldex;->aO:Ldfj;

    invoke-virtual {v0, p1}, Ldfj;->a(Landroid/graphics/Canvas;)V

    .line 1159
    :cond_0
    return-void
.end method

.method public a(Landroid/view/View;Landroid/graphics/drawable/Drawable;Landroid/graphics/RectF;)V
    .locals 1

    .prologue
    .line 1149
    iget-object v0, p0, Ldex;->aO:Ldfj;

    if-eqz v0, :cond_0

    .line 1150
    iget-object v0, p0, Ldex;->aO:Ldfj;

    invoke-virtual {v0, p1, p2, p3}, Ldfj;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;Landroid/graphics/RectF;)V

    .line 1152
    :cond_0
    return-void
.end method

.method public a(Ldfu;)V
    .locals 1

    .prologue
    .line 553
    iget-object v0, p0, Ldex;->as:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 554
    return-void
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 833
    return-void
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 109
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p2}, Ldex;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method public a(Ldyi;)V
    .locals 1

    .prologue
    .line 866
    iget-object v0, p0, Ldex;->ay:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 867
    iget v0, p0, Ldex;->aG:I

    invoke-interface {p1, v0}, Ldyi;->a(I)V

    .line 868
    return-void
.end method

.method public a(Lizu;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 637
    iput-object p1, p0, Ldex;->an:Lizu;

    .line 639
    invoke-virtual {p0}, Ldex;->w()Lbb;

    move-result-object v0

    invoke-virtual {v0, v3}, Lbb;->b(I)Ldo;

    move-result-object v0

    .line 640
    instance-of v1, v0, Lfdi;

    if-eqz v1, :cond_0

    .line 641
    check-cast v0, Lfdi;

    invoke-interface {v0, p1}, Lfdi;->a(Lizu;)V

    .line 644
    :cond_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 645
    iget-object v0, p0, Ldex;->aj:Ljava/lang/String;

    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 646
    iput-object p2, p0, Ldex;->aj:Ljava/lang/String;

    .line 647
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 648
    const-string v1, "view_id"

    iget-object v2, p0, Ldex;->aj:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 649
    invoke-virtual {p0}, Ldex;->n()Lz;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Lz;->setResult(ILandroid/content/Intent;)V

    .line 653
    :cond_1
    invoke-virtual {p0}, Ldex;->u()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 654
    invoke-direct {p0}, Ldex;->ac()Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Ldfe;

    invoke-direct {v0, p0}, Ldfe;-><init>(Ldex;)V

    .line 655
    :goto_0
    invoke-virtual {p0}, Ldex;->w()Lbb;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v3, v2, v0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 657
    :cond_2
    return-void

    :cond_3
    move-object v0, p0

    .line 654
    goto :goto_0
.end method

.method public a(Ljava/util/List;Lduo;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Lduo;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1122
    if-eqz p2, :cond_0

    invoke-interface {p2}, Lduo;->a()I

    move-result v0

    if-eq v0, v3, :cond_1

    .line 1145
    :cond_0
    :goto_0
    return-void

    .line 1126
    :cond_1
    invoke-interface {p2}, Lduo;->b()Ljava/util/ArrayList;

    move-result-object v0

    .line 1128
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v1, v3, :cond_0

    .line 1132
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizu;

    .line 1133
    invoke-virtual {v0}, Lizu;->i()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1134
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    move-object v7, v1

    .line 1136
    :goto_1
    invoke-virtual {p0}, Ldex;->n()Lz;

    move-result-object v1

    .line 1137
    invoke-virtual {v0}, Lizu;->a()Ljava/lang/String;

    move-result-object v2

    .line 1138
    invoke-virtual {v0}, Lizu;->b()Ljava/lang/String;

    move-result-object v3

    .line 1139
    invoke-virtual {v0}, Lizu;->c()J

    move-result-wide v4

    .line 1140
    invoke-virtual {v0}, Lizu;->d()Ljava/lang/String;

    move-result-object v6

    .line 1142
    invoke-virtual {v0}, Lizu;->g()Ljac;

    move-result-object v8

    .line 1136
    invoke-static/range {v1 .. v8}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Ljac;)Lizu;

    move-result-object v0

    .line 1144
    iget-object v1, p0, Ldex;->aj:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Ldex;->a(Lizu;Ljava/lang/String;)V

    goto :goto_0

    .line 1134
    :cond_2
    invoke-virtual {v0}, Lizu;->e()Landroid/net/Uri;

    move-result-object v7

    goto :goto_1
.end method

.method public a(Loo;)V
    .locals 2

    .prologue
    .line 969
    invoke-virtual {p0}, Ldex;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "disable_up_button"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 970
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Loo;->c(Z)V

    .line 972
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 685
    iget-object v0, p0, Ldex;->aC:Ldfs;

    invoke-interface {v0}, Ldfs;->a()Z

    move-result v0

    if-ne v0, p1, :cond_1

    .line 705
    :cond_0
    return-void

    .line 689
    :cond_1
    invoke-virtual {p0}, Ldex;->n()Lz;

    move-result-object v0

    .line 690
    instance-of v1, v0, Los;

    if-eqz v1, :cond_3

    check-cast v0, Los;

    .line 691
    invoke-virtual {v0}, Los;->h()Loo;

    move-result-object v0

    .line 692
    :goto_0
    if-eqz v0, :cond_2

    .line 693
    if-eqz p1, :cond_4

    .line 694
    invoke-virtual {v0}, Loo;->f()V

    .line 700
    :cond_2
    :goto_1
    iget-object v0, p0, Ldex;->aC:Ldfs;

    invoke-interface {v0, p1}, Ldfs;->a(Z)V

    .line 702
    iget-object v0, p0, Ldex;->ax:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_2

    .line 691
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 696
    :cond_4
    invoke-virtual {v0}, Loo;->e()V

    goto :goto_1
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 615
    iget-boolean v0, p0, Ldex;->ac:Z

    if-eqz v0, :cond_0

    .line 616
    invoke-super {p0}, Legi;->a()Z

    move-result v0

    .line 619
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aO_()V
    .locals 4

    .prologue
    .line 456
    invoke-super {p0}, Legi;->aO_()V

    .line 457
    iget-object v0, p0, Ldex;->V:Lcsm;

    invoke-interface {v0, p0}, Lcsm;->a(Lcsn;)V

    .line 458
    iget-object v0, p0, Ldex;->aD:Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->d()V

    .line 459
    iget-object v0, p0, Ldex;->N:Ldfl;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(Lfhh;)V

    .line 461
    iget-object v0, p0, Ldex;->ah:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 462
    iget-object v0, p0, Ldex;->ah:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 463
    iget-object v0, p0, Ldex;->ah:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(I)Lfib;

    move-result-object v0

    .line 464
    iget-object v1, p0, Ldex;->N:Ldfl;

    iget-object v2, p0, Ldex;->ah:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2, v0}, Ldfl;->k(ILfib;)Z

    .line 468
    :cond_0
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 469
    const-string v1, "com.google.android.apps.photos.SLIDESHOW_STATE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 470
    iget-object v1, p0, Ldex;->at:Llnl;

    invoke-static {v1}, Ldr;->a(Landroid/content/Context;)Ldr;

    move-result-object v1

    iget-object v2, p0, Ldex;->aM:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Ldr;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 472
    iget-object v0, p0, Ldex;->at:Llnl;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/SlideshowService;->d(Landroid/content/Context;)V

    .line 474
    iget-boolean v0, p0, Ldex;->aL:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Ldex;->aK:Z

    if-eqz v0, :cond_1

    .line 477
    iget-object v0, p0, Ldex;->at:Llnl;

    invoke-virtual {p0}, Ldex;->k()Landroid/os/Bundle;

    move-result-object v1

    iget v2, p0, Ldex;->am:I

    add-int/lit8 v2, v2, -0x1

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/service/SlideshowService;->a(Landroid/content/Context;Landroid/os/Bundle;I)V

    .line 480
    :cond_1
    iget-object v0, p0, Ldex;->an:Lizu;

    if-eqz v0, :cond_2

    .line 481
    invoke-direct {p0}, Ldex;->ac()Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Ldfe;

    invoke-direct {v0, p0}, Ldfe;-><init>(Ldex;)V

    .line 482
    :goto_0
    invoke-virtual {p0}, Ldex;->w()Lbb;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 485
    :cond_2
    iget-object v0, p0, Ldex;->aS:Ljava/lang/Runnable;

    if-eqz v0, :cond_3

    .line 486
    iget-object v0, p0, Ldex;->aS:Ljava/lang/Runnable;

    invoke-static {v0}, Llsx;->a(Ljava/lang/Runnable;)V

    .line 488
    :cond_3
    return-void

    :cond_4
    move-object v0, p0

    .line 481
    goto :goto_0
.end method

.method public b(I)V
    .locals 2

    .prologue
    .line 877
    iput p1, p0, Ldex;->aG:I

    .line 878
    iget-object v0, p0, Ldex;->ay:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldyi;

    .line 879
    invoke-interface {v0, p1}, Ldyi;->a(I)V

    goto :goto_0

    .line 881
    :cond_0
    return-void
.end method

.method public b(Ldfu;)V
    .locals 1

    .prologue
    .line 558
    iget-object v0, p0, Ldex;->as:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 559
    return-void
.end method

.method public b(Ldyi;)V
    .locals 1

    .prologue
    .line 872
    iget-object v0, p0, Ldex;->ay:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 873
    return-void
.end method

.method protected b(Lhjk;)V
    .locals 2

    .prologue
    .line 976
    invoke-super {p0, p1}, Legi;->b(Lhjk;)V

    .line 979
    const v0, 0x7f100696

    invoke-interface {p1, v0}, Lhjk;->c(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 981
    invoke-virtual {p0, p1}, Ldex;->e(Lhjk;)V

    .line 982
    return-void
.end method

.method public b(Lu;I)V
    .locals 3

    .prologue
    .line 837
    move-object v0, p1

    check-cast v0, Ldfv;

    .line 840
    iget-object v1, p0, Ldex;->aI:Lczs;

    invoke-virtual {v1, p2}, Lczs;->a(I)V

    .line 843
    iget-boolean v1, p0, Ldex;->aQ:Z

    if-nez v1, :cond_1

    if-nez p1, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {p0}, Ldex;->x()Landroid/view/View;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Ldex;->a(ZLandroid/view/View;)V

    .line 845
    if-eqz v0, :cond_0

    .line 846
    iget-object v1, p0, Ldex;->aH:Lder;

    invoke-virtual {v0}, Ldfv;->c()Ldeo;

    move-result-object v2

    invoke-virtual {v1, v2}, Lder;->a(Ldeo;)V

    .line 848
    iget-boolean v1, p0, Ldex;->aQ:Z

    if-nez v1, :cond_0

    .line 851
    invoke-virtual {v0}, Ldfv;->a()V

    .line 855
    :cond_0
    iget-object v0, p0, Ldex;->as:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldfu;

    .line 856
    invoke-interface {v0, p1}, Ldfu;->a(Lu;)V

    goto :goto_1

    .line 843
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 859
    :cond_2
    iget-object v0, p0, Ldex;->ax:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_2

    .line 862
    :cond_3
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 275
    invoke-super {p0, p1}, Legi;->c(Landroid/os/Bundle;)V

    .line 277
    iget-object v0, p0, Ldex;->au:Llnh;

    const-class v1, Ldyh;

    .line 278
    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Lcvh;

    new-instance v2, Ldfg;

    invoke-direct {v2, p0}, Ldfg;-><init>(Ldex;)V

    .line 279
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Ldbh;

    iget-object v2, p0, Ldex;->az:Ldbh;

    .line 280
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Ldcm;

    iget-object v2, p0, Ldex;->aA:Ldcm;

    .line 281
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Ldfs;

    iget-object v2, p0, Ldex;->aC:Ldfs;

    .line 282
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Lczs;

    iget-object v2, p0, Ldex;->aI:Lczs;

    .line 283
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Lcyw;

    iget-object v2, p0, Ldex;->aB:Lcyw;

    .line 284
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Lfzz;

    .line 285
    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 288
    iget-object v0, p0, Ldex;->au:Llnh;

    const-class v1, Lder;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lder;

    iput-object v0, p0, Ldex;->aH:Lder;

    .line 289
    return-void
.end method

.method protected c(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 964
    const/4 v0, 0x0

    return v0
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 585
    invoke-super {p0, p1}, Legi;->e(Landroid/os/Bundle;)V

    .line 586
    const-string v0, "show_hashtags"

    iget-object v1, p0, Ldex;->az:Ldbh;

    invoke-interface {v1}, Ldbh;->a()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 587
    const-string v0, "show_shapes"

    iget-object v1, p0, Ldex;->aA:Ldcm;

    invoke-interface {v1}, Ldcm;->a()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 588
    const-string v0, "current_item"

    iget-object v1, p0, Ldex;->ak:Lcom/google/android/apps/plus/views/PhotoViewPager;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PhotoViewPager;->c()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 589
    const-string v0, "full_screen"

    iget-object v1, p0, Ldex;->aC:Ldfs;

    invoke-interface {v1}, Ldfs;->a()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 590
    const-string v0, "operation_type"

    iget v1, p0, Ldex;->ai:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 591
    const-string v0, "collection_refreshed"

    iget-boolean v1, p0, Ldex;->ao:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 592
    const-string v0, "loaded_not_empty"

    iget-boolean v1, p0, Ldex;->ap:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 593
    const-string v0, "auto_switch_ref"

    iget-object v1, p0, Ldex;->an:Lizu;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 594
    const-string v0, "slideshow_enabled"

    iget-boolean v1, p0, Ldex;->aK:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 595
    const-string v0, "local_slideshow"

    iget-boolean v1, p0, Ldex;->aL:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 596
    const-string v0, "performed_in_animation"

    iget-boolean v1, p0, Ldex;->aU:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 597
    const-string v1, "reset_pager_after_zoom"

    iget-object v0, p0, Ldex;->aS:Ljava/lang/Runnable;

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 598
    const-string v0, "view_id"

    iget-object v1, p0, Ldex;->aj:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 599
    const-string v0, "all_photos_offset"

    iget v1, p0, Ldex;->aT:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 600
    iget-object v0, p0, Ldex;->ah:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 601
    const-string v0, "pending_request_id"

    iget-object v1, p0, Ldex;->ah:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 603
    :cond_0
    iget-object v0, p0, Ldex;->aq:Lely;

    if-eqz v0, :cond_1

    .line 604
    iget-object v0, p0, Ldex;->aq:Lely;

    invoke-virtual {v0, p1}, Lely;->b(Landroid/os/Bundle;)V

    .line 606
    :cond_1
    return-void

    .line 597
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()V
    .locals 1

    .prologue
    .line 517
    invoke-super {p0}, Legi;->g()V

    .line 518
    iget-object v0, p0, Ldex;->P:Lhee;

    invoke-interface {v0}, Lhee;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 519
    iget-object v0, p0, Ldex;->aq:Lely;

    if-eqz v0, :cond_0

    .line 520
    iget-object v0, p0, Ldex;->aq:Lely;

    invoke-virtual {v0}, Lely;->f()V

    .line 522
    :cond_0
    iget-object v0, p0, Ldex;->ar:Lely;

    if-eqz v0, :cond_1

    .line 523
    iget-object v0, p0, Ldex;->ar:Lely;

    invoke-virtual {v0}, Lely;->f()V

    .line 526
    :cond_1
    return-void
.end method

.method public g_(I)V
    .locals 2

    .prologue
    .line 890
    iput p1, p0, Ldex;->am:I

    .line 891
    iget-object v0, p0, Ldex;->al:Lhyj;

    invoke-virtual {v0, p1}, Lhyj;->d(I)V

    .line 892
    invoke-virtual {p0}, Ldex;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 897
    invoke-virtual {p0}, Ldex;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbb;->b(I)Ldo;

    move-result-object v0

    iget v1, p0, Ldex;->am:I

    .line 896
    invoke-direct {p0, v0, v1}, Ldex;->a(Ldo;I)V

    .line 899
    :cond_0
    return-void
.end method

.method public h()V
    .locals 1

    .prologue
    .line 530
    invoke-super {p0}, Legi;->h()V

    .line 531
    iget-object v0, p0, Ldex;->P:Lhee;

    invoke-interface {v0}, Lhee;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 532
    iget-object v0, p0, Ldex;->aq:Lely;

    if-eqz v0, :cond_0

    .line 533
    iget-object v0, p0, Ldex;->aq:Lely;

    invoke-virtual {v0}, Lely;->g()V

    .line 535
    :cond_0
    iget-object v0, p0, Ldex;->ar:Lely;

    if-eqz v0, :cond_1

    .line 536
    iget-object v0, p0, Ldex;->ar:Lely;

    invoke-virtual {v0}, Lely;->g()V

    .line 539
    :cond_1
    return-void
.end method

.method public z()V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 493
    invoke-super {p0}, Legi;->z()V

    .line 494
    iget-object v0, p0, Ldex;->V:Lcsm;

    invoke-interface {v0, p0}, Lcsm;->b(Lcsn;)V

    .line 495
    iget-object v0, p0, Ldex;->aD:Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->c()V

    .line 496
    iget-object v0, p0, Ldex;->N:Ldfl;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(Lfhh;)V

    .line 498
    iget-object v0, p0, Ldex;->aM:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 499
    iget-object v0, p0, Ldex;->at:Llnl;

    invoke-static {v0}, Ldr;->a(Landroid/content/Context;)Ldr;

    move-result-object v0

    iget-object v1, p0, Ldex;->aM:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Ldr;->a(Landroid/content/BroadcastReceiver;)V

    .line 502
    :cond_0
    iget-object v0, p0, Ldex;->au:Llnh;

    const-class v1, Ljuk;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljuk;

    invoke-virtual {v0}, Ljuk;->b()Z

    move-result v0

    .line 503
    if-nez v0, :cond_1

    .line 504
    iget-object v0, p0, Ldex;->at:Llnl;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/SlideshowService;->a(Landroid/content/Context;)V

    .line 506
    :cond_1
    iget-object v0, p0, Ldex;->aR:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_2

    .line 507
    iget-object v0, p0, Ldex;->aR:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 508
    invoke-direct {p0}, Ldex;->ad()V

    .line 510
    :cond_2
    iget-object v0, p0, Ldex;->aS:Ljava/lang/Runnable;

    if-eqz v0, :cond_3

    .line 511
    iget-object v0, p0, Ldex;->aS:Ljava/lang/Runnable;

    invoke-static {}, Llsx;->d()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 513
    :cond_3
    return-void
.end method
