.class public final Lpli;
.super Loxq;
.source "PG"


# static fields
.field public static final a:Loxr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Loxr",
            "<",
            "Lpli;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:Lplh;

.field private c:Lplh;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 86
    const v0, 0x338c8a0

    new-instance v1, Lplj;

    invoke-direct {v1}, Lplj;-><init>()V

    .line 91
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    move-result-object v0

    sput-object v0, Lpli;->a:Loxr;

    .line 90
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 114
    const/4 v0, 0x0

    .line 115
    iget-object v1, p0, Lpli;->b:Lplh;

    if-eqz v1, :cond_0

    .line 116
    const/4 v0, 0x1

    iget-object v1, p0, Lpli;->b:Lplh;

    .line 117
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 119
    :cond_0
    iget-object v1, p0, Lpli;->c:Lplh;

    if-eqz v1, :cond_1

    .line 120
    const/4 v1, 0x2

    iget-object v2, p0, Lpli;->c:Lplh;

    .line 121
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 123
    :cond_1
    iget-object v1, p0, Lpli;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 124
    iput v0, p0, Lpli;->ai:I

    .line 125
    return v0
.end method

.method public a(Loxn;)Lpli;
    .locals 2

    .prologue
    .line 133
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 134
    sparse-switch v0, :sswitch_data_0

    .line 138
    iget-object v1, p0, Lpli;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 139
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpli;->ah:Ljava/util/List;

    .line 142
    :cond_1
    iget-object v1, p0, Lpli;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 144
    :sswitch_0
    return-object p0

    .line 149
    :sswitch_1
    iget-object v0, p0, Lpli;->b:Lplh;

    if-nez v0, :cond_2

    .line 150
    new-instance v0, Lplh;

    invoke-direct {v0}, Lplh;-><init>()V

    iput-object v0, p0, Lpli;->b:Lplh;

    .line 152
    :cond_2
    iget-object v0, p0, Lpli;->b:Lplh;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 156
    :sswitch_2
    iget-object v0, p0, Lpli;->c:Lplh;

    if-nez v0, :cond_3

    .line 157
    new-instance v0, Lplh;

    invoke-direct {v0}, Lplh;-><init>()V

    iput-object v0, p0, Lpli;->c:Lplh;

    .line 159
    :cond_3
    iget-object v0, p0, Lpli;->c:Lplh;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 134
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 102
    iget-object v0, p0, Lpli;->b:Lplh;

    if-eqz v0, :cond_0

    .line 103
    const/4 v0, 0x1

    iget-object v1, p0, Lpli;->b:Lplh;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 105
    :cond_0
    iget-object v0, p0, Lpli;->c:Lplh;

    if-eqz v0, :cond_1

    .line 106
    const/4 v0, 0x2

    iget-object v1, p0, Lpli;->c:Lplh;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 108
    :cond_1
    iget-object v0, p0, Lpli;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 110
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 83
    invoke-virtual {p0, p1}, Lpli;->a(Loxn;)Lpli;

    move-result-object v0

    return-object v0
.end method
