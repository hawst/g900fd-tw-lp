.class public final Ldwb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Liah;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;ILjava/lang/Object;)V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v0, 0x0

    const/4 v9, 0x3

    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 21
    instance-of v1, p3, Lhtc;

    if-eqz v1, :cond_5

    .line 22
    check-cast p3, Lhtc;

    .line 23
    iget-object v1, p3, Lhtc;->d:Ljava/lang/String;

    iget-object v3, p3, Lhtc;->e:Ljava/lang/String;

    const-string v4, "ALBUM"

    invoke-static {v0, v1, v3, v4}, Ljvj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-array v3, v8, [Ljava/lang/String;

    aput-object v1, v3, v2

    invoke-static {v9, v3}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 25
    invoke-static {p1, p2}, Lhzt;->a(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 26
    if-eqz p3, :cond_0

    iget-object v4, p3, Lhtc;->e:Ljava/lang/String;

    if-eqz v4, :cond_0

    iget-wide v4, p3, Lhtc;->f:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_0

    iget-object v4, p3, Lhtc;->d:Ljava/lang/String;

    if-eqz v4, :cond_0

    iget-object v4, p3, Lhtc;->c:Ljava/lang/String;

    if-nez v4, :cond_2

    :cond_0
    const-string v2, "EsTileData"

    const-string v3, "Invalid DbEmbedMedia object; cannot insert into the database"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 27
    :goto_0
    iget-object v2, p3, Lhtc;->a:Ljava/lang/String;

    invoke-static {p1, p2, v1, v0, v2}, Ljvj;->a(Landroid/content/Context;ILjava/lang/String;[Lnzx;Ljava/lang/String;)V

    .line 39
    :cond_1
    :goto_1
    return-void

    .line 26
    :cond_2
    new-instance v0, Lnyz;

    invoke-direct {v0}, Lnyz;-><init>()V

    iget-object v4, p3, Lhtc;->d:Ljava/lang/String;

    iput-object v4, v0, Lnyz;->c:Ljava/lang/String;

    new-instance v4, Lnyb;

    invoke-direct {v4}, Lnyb;-><init>()V

    iget-object v5, p3, Lhtc;->e:Ljava/lang/String;

    iput-object v5, v4, Lnyb;->d:Ljava/lang/String;

    iput-object v0, v4, Lnyb;->f:Lnyz;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v4, Lnyb;->c:Ljava/lang/Integer;

    iput v9, v4, Lnyb;->e:I

    const-string v0, ""

    iput-object v0, v4, Lnyb;->b:Ljava/lang/String;

    new-instance v0, Lnzo;

    invoke-direct {v0}, Lnzo;-><init>()V

    iput-object v4, v0, Lnzo;->b:Lnyb;

    new-instance v5, Lnzx;

    invoke-direct {v5}, Lnzx;-><init>()V

    iput v10, v5, Lnzx;->k:I

    sget-object v6, Lnzo;->a:Loxr;

    invoke-virtual {v5, v6, v0}, Lnzx;->a(Loxr;Ljava/lang/Object;)V

    invoke-static {v3, v5}, Ldtc;->a(Landroid/database/sqlite/SQLiteDatabase;Lnzx;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    invoke-static {v5}, Ljvj;->a(Lnzx;)Ljava/lang/String;

    move-result-object v0

    :cond_3
    iput-object v0, v5, Lnzx;->b:Ljava/lang/String;

    new-array v0, v8, [Lnzx;

    invoke-static {v3, p3, v4}, Ldtc;->a(Landroid/database/sqlite/SQLiteDatabase;Lhtc;Lnyb;)Lnzx;

    move-result-object v3

    if-eqz v3, :cond_4

    aput-object v3, v0, v2

    :cond_4
    iput-object v0, v5, Lnzx;->j:[Lnzx;

    new-array v0, v8, [Lnzx;

    aput-object v5, v0, v2

    goto :goto_0

    .line 29
    :cond_5
    instance-of v1, p3, Lhsz;

    if-eqz v1, :cond_1

    .line 30
    check-cast p3, Lhsz;

    .line 31
    iget-object v1, p3, Lhsz;->d:Ljava/lang/String;

    iget-object v3, p3, Lhsz;->e:Ljava/lang/String;

    const-string v4, "ALBUM"

    invoke-static {v0, v1, v3, v4}, Ljvj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-array v3, v8, [Ljava/lang/String;

    aput-object v1, v3, v2

    invoke-static {v9, v3}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 33
    if-eqz v3, :cond_1

    .line 34
    invoke-static {p1, p2}, Lhzt;->a(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 35
    if-eqz p3, :cond_6

    iget-object v1, p3, Lhsz;->e:Ljava/lang/String;

    if-eqz v1, :cond_6

    iget-object v1, p3, Lhsz;->d:Ljava/lang/String;

    if-eqz v1, :cond_6

    iget v1, p3, Lhsz;->b:I

    if-nez v1, :cond_7

    :cond_6
    const-string v1, "EsTileData"

    const-string v2, "Invalid DbEmbedAlbum object; cannot insert into the database"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 36
    :goto_2
    iget-object v1, p3, Lhsz;->a:Ljava/lang/String;

    invoke-static {p1, p2, v3, v0, v1}, Ljvj;->a(Landroid/content/Context;ILjava/lang/String;[Lnzx;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 35
    :cond_7
    iget v5, p3, Lhsz;->b:I

    new-instance v0, Lnyz;

    invoke-direct {v0}, Lnyz;-><init>()V

    iget-object v1, p3, Lhsz;->d:Ljava/lang/String;

    iput-object v1, v0, Lnyz;->c:Ljava/lang/String;

    new-instance v6, Lnyb;

    invoke-direct {v6}, Lnyb;-><init>()V

    iget-object v1, p3, Lhsz;->e:Ljava/lang/String;

    iput-object v1, v6, Lnyb;->d:Ljava/lang/String;

    iput-object v0, v6, Lnyb;->f:Lnyz;

    iget-object v1, p3, Lhsz;->f:Ljava/lang/String;

    if-nez v1, :cond_a

    const-string v0, ""

    :goto_3
    iput-object v0, v6, Lnyb;->b:Ljava/lang/String;

    iget v0, p3, Lhsz;->c:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v6, Lnyb;->c:Ljava/lang/Integer;

    iput v9, v6, Lnyb;->e:I

    new-instance v0, Lnzo;

    invoke-direct {v0}, Lnzo;-><init>()V

    iput-object v6, v0, Lnzo;->b:Lnyb;

    new-instance v7, Lnzx;

    invoke-direct {v7}, Lnzx;-><init>()V

    iput v10, v7, Lnzx;->k:I

    iput-object v1, v7, Lnzx;->c:Ljava/lang/String;

    sget-object v1, Lnzo;->a:Loxr;

    invoke-virtual {v7, v1, v0}, Lnzx;->a(Loxr;Ljava/lang/Object;)V

    invoke-static {v4, v7}, Ldtc;->a(Landroid/database/sqlite/SQLiteDatabase;Lnzx;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_8

    invoke-static {v7}, Ljvj;->a(Lnzx;)Ljava/lang/String;

    move-result-object v0

    :cond_8
    iput-object v0, v7, Lnzx;->b:Ljava/lang/String;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v5}, Ljava/util/ArrayList;-><init>(I)V

    move v0, v2

    :goto_4
    iget v5, p3, Lhsz;->b:I

    if-ge v0, v5, :cond_b

    iget-object v5, p3, Lhsz;->g:[Lhtc;

    aget-object v5, v5, v0

    invoke-static {v4, v5, v6}, Ldtc;->a(Landroid/database/sqlite/SQLiteDatabase;Lhtc;Lnyb;)Lnzx;

    move-result-object v5

    if-eqz v5, :cond_9

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_a
    move-object v0, v1

    goto :goto_3

    :cond_b
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lnzx;

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lnzx;

    iput-object v0, v7, Lnzx;->j:[Lnzx;

    new-array v0, v8, [Lnzx;

    aput-object v7, v0, v2

    goto :goto_2
.end method
