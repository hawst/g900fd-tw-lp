.class public final Lnkk;
.super Loxq;
.source "PG"


# instance fields
.field public a:I

.field private b:Ljava/lang/Integer;

.field private c:I

.field private d:Ljava/lang/Long;

.field private e:[I

.field private f:Lnkj;

.field private g:Ljava/lang/Boolean;

.field private h:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/high16 v0, -0x80000000

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 48
    iput v0, p0, Lnkk;->a:I

    .line 53
    iput v0, p0, Lnkk;->c:I

    .line 58
    sget-object v0, Loxx;->a:[I

    iput-object v0, p0, Lnkk;->e:[I

    .line 61
    const/4 v0, 0x0

    iput-object v0, p0, Lnkk;->f:Lnkj;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/high16 v4, -0x80000000

    const/4 v1, 0x0

    .line 102
    .line 103
    iget v0, p0, Lnkk;->a:I

    if-eq v0, v4, :cond_8

    .line 104
    const/4 v0, 0x1

    iget v2, p0, Lnkk;->a:I

    .line 105
    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 107
    :goto_0
    iget-object v2, p0, Lnkk;->b:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    .line 108
    const/4 v2, 0x3

    iget-object v3, p0, Lnkk;->b:Ljava/lang/Integer;

    .line 109
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 111
    :cond_0
    iget v2, p0, Lnkk;->c:I

    if-eq v2, v4, :cond_1

    .line 112
    const/4 v2, 0x4

    iget v3, p0, Lnkk;->c:I

    .line 113
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 115
    :cond_1
    iget-object v2, p0, Lnkk;->d:Ljava/lang/Long;

    if-eqz v2, :cond_2

    .line 116
    const/4 v2, 0x5

    iget-object v3, p0, Lnkk;->d:Ljava/lang/Long;

    .line 117
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Loxo;->f(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 119
    :cond_2
    iget-object v2, p0, Lnkk;->e:[I

    if-eqz v2, :cond_4

    iget-object v2, p0, Lnkk;->e:[I

    array-length v2, v2

    if-lez v2, :cond_4

    .line 121
    iget-object v3, p0, Lnkk;->e:[I

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_3

    aget v5, v3, v1

    .line 123
    invoke-static {v5}, Loxo;->i(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 121
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 125
    :cond_3
    add-int/2addr v0, v2

    .line 126
    iget-object v1, p0, Lnkk;->e:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 128
    :cond_4
    iget-object v1, p0, Lnkk;->f:Lnkj;

    if-eqz v1, :cond_5

    .line 129
    const/16 v1, 0x8

    iget-object v2, p0, Lnkk;->f:Lnkj;

    .line 130
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 132
    :cond_5
    iget-object v1, p0, Lnkk;->g:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    .line 133
    const/16 v1, 0x9

    iget-object v2, p0, Lnkk;->g:Ljava/lang/Boolean;

    .line 134
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 136
    :cond_6
    iget-object v1, p0, Lnkk;->h:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    .line 137
    const/16 v1, 0xa

    iget-object v2, p0, Lnkk;->h:Ljava/lang/Boolean;

    .line 138
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 140
    :cond_7
    iget-object v1, p0, Lnkk;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 141
    iput v0, p0, Lnkk;->ai:I

    .line 142
    return v0

    :cond_8
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lnkk;
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 150
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 151
    sparse-switch v0, :sswitch_data_0

    .line 155
    iget-object v1, p0, Lnkk;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 156
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnkk;->ah:Ljava/util/List;

    .line 159
    :cond_1
    iget-object v1, p0, Lnkk;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 161
    :sswitch_0
    return-object p0

    .line 166
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 167
    if-eq v0, v4, :cond_2

    if-eq v0, v5, :cond_2

    if-eq v0, v6, :cond_2

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    .line 171
    :cond_2
    iput v0, p0, Lnkk;->a:I

    goto :goto_0

    .line 173
    :cond_3
    iput v4, p0, Lnkk;->a:I

    goto :goto_0

    .line 178
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnkk;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 182
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 183
    if-eqz v0, :cond_4

    if-eq v0, v4, :cond_4

    if-eq v0, v5, :cond_4

    if-ne v0, v6, :cond_5

    .line 187
    :cond_4
    iput v0, p0, Lnkk;->c:I

    goto :goto_0

    .line 189
    :cond_5
    iput v3, p0, Lnkk;->c:I

    goto :goto_0

    .line 194
    :sswitch_4
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lnkk;->d:Ljava/lang/Long;

    goto :goto_0

    .line 198
    :sswitch_5
    const/16 v0, 0x30

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 199
    iget-object v0, p0, Lnkk;->e:[I

    array-length v0, v0

    .line 200
    add-int/2addr v1, v0

    new-array v1, v1, [I

    .line 201
    iget-object v2, p0, Lnkk;->e:[I

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 202
    iput-object v1, p0, Lnkk;->e:[I

    .line 203
    :goto_1
    iget-object v1, p0, Lnkk;->e:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_6

    .line 204
    iget-object v1, p0, Lnkk;->e:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    .line 205
    invoke-virtual {p1}, Loxn;->a()I

    .line 203
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 208
    :cond_6
    iget-object v1, p0, Lnkk;->e:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    goto/16 :goto_0

    .line 212
    :sswitch_6
    iget-object v0, p0, Lnkk;->f:Lnkj;

    if-nez v0, :cond_7

    .line 213
    new-instance v0, Lnkj;

    invoke-direct {v0}, Lnkj;-><init>()V

    iput-object v0, p0, Lnkk;->f:Lnkj;

    .line 215
    :cond_7
    iget-object v0, p0, Lnkk;->f:Lnkj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 219
    :sswitch_7
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnkk;->g:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 223
    :sswitch_8
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnkk;->h:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 151
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x18 -> :sswitch_2
        0x20 -> :sswitch_3
        0x28 -> :sswitch_4
        0x30 -> :sswitch_5
        0x42 -> :sswitch_6
        0x48 -> :sswitch_7
        0x50 -> :sswitch_8
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    const/high16 v2, -0x80000000

    .line 70
    iget v0, p0, Lnkk;->a:I

    if-eq v0, v2, :cond_0

    .line 71
    const/4 v0, 0x1

    iget v1, p0, Lnkk;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 73
    :cond_0
    iget-object v0, p0, Lnkk;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 74
    const/4 v0, 0x3

    iget-object v1, p0, Lnkk;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 76
    :cond_1
    iget v0, p0, Lnkk;->c:I

    if-eq v0, v2, :cond_2

    .line 77
    const/4 v0, 0x4

    iget v1, p0, Lnkk;->c:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 79
    :cond_2
    iget-object v0, p0, Lnkk;->d:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 80
    const/4 v0, 0x5

    iget-object v1, p0, Lnkk;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 82
    :cond_3
    iget-object v0, p0, Lnkk;->e:[I

    if-eqz v0, :cond_4

    iget-object v0, p0, Lnkk;->e:[I

    array-length v0, v0

    if-lez v0, :cond_4

    .line 83
    iget-object v1, p0, Lnkk;->e:[I

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_4

    aget v3, v1, v0

    .line 84
    const/4 v4, 0x6

    invoke-virtual {p1, v4, v3}, Loxo;->a(II)V

    .line 83
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 87
    :cond_4
    iget-object v0, p0, Lnkk;->f:Lnkj;

    if-eqz v0, :cond_5

    .line 88
    const/16 v0, 0x8

    iget-object v1, p0, Lnkk;->f:Lnkj;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 90
    :cond_5
    iget-object v0, p0, Lnkk;->g:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    .line 91
    const/16 v0, 0x9

    iget-object v1, p0, Lnkk;->g:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 93
    :cond_6
    iget-object v0, p0, Lnkk;->h:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    .line 94
    const/16 v0, 0xa

    iget-object v1, p0, Lnkk;->h:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 96
    :cond_7
    iget-object v0, p0, Lnkk;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 98
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lnkk;->a(Loxn;)Lnkk;

    move-result-object v0

    return-object v0
.end method
