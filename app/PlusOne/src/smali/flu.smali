.class public final Lflu;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhob;
.implements Llnx;
.implements Llqi;
.implements Llrg;


# instance fields
.field a:Landroid/content/Context;

.field b:Lhee;

.field c:Landroid/app/ProgressDialog;

.field d:Landroid/app/AlertDialog;

.field e:Landroid/content/DialogInterface$OnClickListener;

.field private f:Landroid/app/Activity;

.field private g:Lhoc;

.field private h:Lfly;

.field private i:Z


# direct methods
.method public constructor <init>(Llqr;)V
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 199
    new-instance v0, Lflx;

    invoke-direct {v0, p0}, Lflx;-><init>(Lflu;)V

    iput-object v0, p0, Lflu;->e:Landroid/content/DialogInterface$OnClickListener;

    .line 67
    invoke-virtual {p1, p0}, Llqr;->a(Llrg;)Llrg;

    .line 68
    return-void
.end method

.method public constructor <init>(Llqr;Lfly;)V
    .locals 1

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 199
    new-instance v0, Lflx;

    invoke-direct {v0, p0}, Lflx;-><init>(Lflu;)V

    iput-object v0, p0, Lflu;->e:Landroid/content/DialogInterface$OnClickListener;

    .line 77
    iput-object p2, p0, Lflu;->h:Lfly;

    .line 78
    invoke-virtual {p1, p0}, Llqr;->a(Llrg;)Llrg;

    .line 79
    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 96
    iget-object v0, p0, Lflu;->b:Lhee;

    invoke-interface {v0}, Lhee;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 137
    :goto_0
    return-void

    .line 100
    :cond_0
    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lflu;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lflu;->c:Landroid/app/ProgressDialog;

    .line 101
    iget-object v0, p0, Lflu;->c:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lflu;->a:Landroid/content/Context;

    const v2, 0x7f0a0768

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 102
    iget-object v0, p0, Lflu;->c:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 104
    new-instance v0, Lflv;

    invoke-direct {v0, p0}, Lflv;-><init>(Lflu;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    .line 136
    invoke-virtual {v0, v1}, Lflv;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public a(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 92
    iput-object p1, p0, Lflu;->f:Landroid/app/Activity;

    .line 93
    return-void
.end method

.method public a(Landroid/content/Context;Llnh;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 83
    iput-object p1, p0, Lflu;->a:Landroid/content/Context;

    .line 84
    const-class v0, Lhee;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Lflu;->b:Lhee;

    .line 85
    const-class v0, Lhoc;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    iput-object v0, p0, Lflu;->g:Lhoc;

    .line 87
    iget-object v0, p0, Lflu;->g:Lhoc;

    invoke-virtual {v0, p0}, Lhoc;->a(Lhob;)Lhoc;

    .line 88
    return-void
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 2

    .prologue
    .line 190
    iget-boolean v0, p0, Lflu;->i:Z

    if-eqz v0, :cond_1

    const-string v0, "RemoveAccountTask"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 191
    iget-object v0, p0, Lflu;->h:Lfly;

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Lflu;->h:Lfly;

    iget-object v1, p0, Lflu;->a:Landroid/content/Context;

    invoke-interface {v0, v1}, Lfly;->a(Landroid/content/Context;)V

    .line 194
    :cond_0
    iget-object v0, p0, Lflu;->f:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 196
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflu;->i:Z

    .line 197
    return-void
.end method

.method b()V
    .locals 4

    .prologue
    .line 175
    iget-object v0, p0, Lflu;->b:Lhee;

    invoke-interface {v0}, Lhee;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 185
    :goto_0
    return-void

    .line 179
    :cond_0
    iget-object v0, p0, Lflu;->a:Landroid/content/Context;

    const-class v1, Lhms;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Lflu;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->g:Lhmv;

    .line 180
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 179
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 182
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflu;->i:Z

    .line 183
    iget-object v0, p0, Lflu;->g:Lhoc;

    new-instance v1, Ldpv;

    iget-object v2, p0, Lflu;->a:Landroid/content/Context;

    iget-object v3, p0, Lflu;->b:Lhee;

    .line 184
    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    invoke-direct {v1, v2, v3}, Ldpv;-><init>(Landroid/content/Context;I)V

    .line 183
    invoke-virtual {v0, v1}, Lhoc;->b(Lhny;)V

    goto :goto_0
.end method
