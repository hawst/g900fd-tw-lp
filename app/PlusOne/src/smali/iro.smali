.class public final Liro;
.super Lpyq;
.source "PG"


# static fields
.field private static final b:Landroid/graphics/Paint;


# instance fields
.field private c:Landroid/graphics/Bitmap;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 19
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    .line 20
    sput-object v0, Liro;->b:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 21
    sget-object v0, Liro;->b:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->DST_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 22
    return-void
.end method

.method public constructor <init>(Lirp;Landroid/graphics/Bitmap$Config;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Lpyq;-><init>(Lpys;Landroid/graphics/Bitmap$Config;)V

    .line 28
    return-void
.end method


# virtual methods
.method public a(Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 34
    iput-object p1, p0, Liro;->c:Landroid/graphics/Bitmap;

    .line 35
    return-void
.end method

.method protected b(Landroid/graphics/Bitmap;)V
    .locals 6

    .prologue
    .line 39
    iget-object v0, p0, Liro;->c:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 40
    sget-object v1, Liro;->b:Landroid/graphics/Paint;

    monitor-enter v1

    .line 41
    :try_start_0
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, p1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iget-object v2, p0, Liro;->c:Landroid/graphics/Bitmap;

    const/4 v3, 0x0

    const/4 v4, 0x0

    sget-object v5, Liro;->b:Landroid/graphics/Paint;

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 42
    monitor-exit v1

    .line 44
    :cond_0
    return-void

    .line 42
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
