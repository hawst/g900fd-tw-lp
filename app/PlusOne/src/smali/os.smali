.class public Los;
.super Lz;
.source "PG"

# interfaces
.implements Ldb;
.implements Lpp;


# instance fields
.field private e:Lot;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Lz;-><init>()V

    return-void
.end method

.method private l()Lot;
    .locals 2

    .prologue
    .line 556
    iget-object v0, p0, Los;->e:Lot;

    if-nez v0, :cond_0

    .line 557
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1

    new-instance v0, Lpf;

    invoke-direct {v0, p0}, Lpf;-><init>(Los;)V

    :goto_0
    iput-object v0, p0, Los;->e:Lot;

    .line 559
    :cond_0
    iget-object v0, p0, Los;->e:Lot;

    return-object v0

    .line 557
    :cond_1
    new-instance v0, Low;

    invoke-direct {v0, p0}, Low;-><init>(Los;)V

    goto :goto_0
.end method


# virtual methods
.method public a()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 466
    invoke-static {p0}, Lbf;->a(Landroid/app/Activity;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public a(Lxo;)Lxn;
    .locals 1

    .prologue
    .line 228
    invoke-direct {p0}, Los;->l()Lot;

    move-result-object v0

    invoke-virtual {v0, p1}, Lot;->a(Lxo;)Lxn;

    move-result-object v0

    return-object v0
.end method

.method public a(Lda;)V
    .locals 0

    .prologue
    .line 385
    invoke-virtual {p1, p0}, Lda;->a(Landroid/app/Activity;)Lda;

    .line 386
    return-void
.end method

.method public a(Lxn;)V
    .locals 0

    .prologue
    .line 216
    return-void
.end method

.method a(ILandroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 276
    invoke-super {p0, p1, p2}, Lz;->onCreatePanelMenu(ILandroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method a(ILandroid/view/View;Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 280
    invoke-super {p0, p1, p2, p3}, Lz;->onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public a(Landroid/content/Intent;)Z
    .locals 1

    .prologue
    .line 483
    invoke-static {p0, p1}, Lbf;->a(Landroid/app/Activity;Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method

.method protected a(Landroid/view/View;Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 256
    invoke-direct {p0}, Los;->l()Lot;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lot;->a(Landroid/view/View;Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public a_(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 499
    invoke-static {p0, p1}, Lbf;->b(Landroid/app/Activity;Landroid/content/Intent;)V

    .line 500
    return-void
.end method

.method public addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .prologue
    .line 117
    invoke-direct {p0}, Los;->l()Lot;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lot;->b(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 118
    return-void
.end method

.method public az_()V
    .locals 1

    .prologue
    .line 199
    invoke-direct {p0}, Los;->l()Lot;

    move-result-object v0

    invoke-virtual {v0}, Lot;->g()V

    .line 200
    return-void
.end method

.method b(ILandroid/view/Menu;)V
    .locals 0

    .prologue
    .line 288
    invoke-super {p0, p1, p2}, Lz;->onPanelClosed(ILandroid/view/Menu;)V

    .line 289
    return-void
.end method

.method public b(Lda;)V
    .locals 0

    .prologue
    .line 404
    return-void
.end method

.method public b(Lxn;)V
    .locals 0

    .prologue
    .line 225
    return-void
.end method

.method b(Landroid/view/View;Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 284
    invoke-super {p0, p1, p2}, Lz;->a(Landroid/view/View;Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method c(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 264
    invoke-super {p0, p1}, Lz;->setContentView(Landroid/view/View;)V

    .line 265
    return-void
.end method

.method c(ILandroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 292
    invoke-super {p0, p1, p2}, Lz;->onMenuOpened(ILandroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public getMenuInflater()Landroid/view/MenuInflater;
    .locals 1

    .prologue
    .line 97
    invoke-direct {p0}, Los;->l()Lot;

    move-result-object v0

    invoke-virtual {v0}, Lot;->c()Landroid/view/MenuInflater;

    move-result-object v0

    return-object v0
.end method

.method public h()Loo;
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0}, Los;->l()Lot;

    move-result-object v0

    invoke-virtual {v0}, Lot;->b()Loo;

    move-result-object v0

    return-object v0
.end method

.method public i()Z
    .locals 2

    .prologue
    .line 430
    invoke-virtual {p0}, Los;->a()Landroid/content/Intent;

    move-result-object v0

    .line 432
    if-eqz v0, :cond_1

    .line 433
    invoke-virtual {p0, v0}, Los;->a(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 434
    invoke-static {p0}, Lda;->a(Landroid/content/Context;)Lda;

    move-result-object v0

    .line 435
    invoke-virtual {p0, v0}, Los;->a(Lda;)V

    .line 436
    invoke-virtual {p0, v0}, Los;->b(Lda;)V

    .line 437
    invoke-virtual {v0}, Lda;->b()V

    .line 440
    :try_start_0
    invoke-static {p0}, Lh;->a(Landroid/app/Activity;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 451
    :goto_0
    const/4 v0, 0x1

    .line 453
    :goto_1
    return v0

    .line 444
    :catch_0
    move-exception v0

    invoke-virtual {p0}, Los;->finish()V

    goto :goto_0

    .line 449
    :cond_0
    invoke-virtual {p0, v0}, Los;->a_(Landroid/content/Intent;)V

    goto :goto_0

    .line 453
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public invalidateOptionsMenu()V
    .locals 1

    .prologue
    .line 206
    invoke-direct {p0}, Los;->l()Lot;

    move-result-object v0

    invoke-virtual {v0}, Lot;->g()V

    .line 207
    return-void
.end method

.method public j()Lpi;
    .locals 2

    .prologue
    .line 513
    invoke-direct {p0}, Los;->l()Lot;

    move-result-object v0

    new-instance v1, Lov;

    invoke-direct {v1, v0}, Lov;-><init>(Lot;)V

    return-object v1
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 297
    invoke-direct {p0}, Los;->l()Lot;

    move-result-object v0

    invoke-virtual {v0}, Lot;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 298
    invoke-super {p0}, Lz;->onBackPressed()V

    .line 300
    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 128
    invoke-super {p0, p1}, Lz;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 129
    invoke-direct {p0}, Los;->l()Lot;

    move-result-object v0

    invoke-virtual {v0}, Lot;->d()V

    .line 130
    return-void
.end method

.method public final onContentChanged()V
    .locals 0

    .prologue
    .line 534
    invoke-direct {p0}, Los;->l()Lot;

    .line 535
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 122
    invoke-super {p0, p1}, Lz;->onCreate(Landroid/os/Bundle;)V

    .line 123
    invoke-direct {p0}, Los;->l()Lot;

    move-result-object v0

    invoke-virtual {v0, p1}, Lot;->a(Landroid/os/Bundle;)V

    .line 124
    return-void
.end method

.method public onCreatePanelMenu(ILandroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 233
    invoke-direct {p0}, Los;->l()Lot;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lot;->c(ILandroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onCreatePanelView(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 146
    if-nez p1, :cond_0

    .line 147
    invoke-direct {p0}, Los;->l()Lot;

    move-result-object v0

    invoke-virtual {v0, p1}, Lot;->b(I)Landroid/view/View;

    move-result-object v0

    .line 149
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lz;->onCreatePanelView(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public onCreateView(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
    .locals 1

    .prologue
    .line 547
    invoke-super {p0, p1, p2, p3}, Lz;->onCreateView(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;

    move-result-object v0

    .line 548
    if-eqz v0, :cond_0

    .line 552
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Los;->l()Lot;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lot;->a(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 169
    invoke-super {p0}, Lz;->onDestroy()V

    .line 170
    invoke-direct {p0}, Los;->l()Lot;

    move-result-object v0

    const/4 v1, 0x1

    iput-boolean v1, v0, Lot;->h:Z

    .line 171
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 524
    invoke-super {p0, p1, p2}, Lz;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 525
    const/4 v0, 0x1

    .line 527
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Los;->l()Lot;

    move-result-object v0

    invoke-virtual {v0, p2}, Lot;->a(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyShortcut(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 518
    invoke-direct {p0}, Los;->l()Lot;

    move-result-object v0

    invoke-virtual {v0, p2}, Lot;->b(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public final onMenuItemSelected(ILandroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 155
    invoke-super {p0, p1, p2}, Lz;->onMenuItemSelected(ILandroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 156
    const/4 v0, 0x1

    .line 164
    :goto_0
    return v0

    .line 159
    :cond_0
    invoke-virtual {p0}, Los;->h()Loo;

    move-result-object v0

    .line 160
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x102002c

    if-ne v1, v2, :cond_1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Loo;->b()I

    move-result v0

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_1

    .line 162
    invoke-virtual {p0}, Los;->i()Z

    move-result v0

    goto :goto_0

    .line 164
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onMenuOpened(ILandroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 248
    invoke-direct {p0}, Los;->l()Lot;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lot;->b(ILandroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onPanelClosed(ILandroid/view/Menu;)V
    .locals 1

    .prologue
    .line 243
    invoke-direct {p0}, Los;->l()Lot;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lot;->a(ILandroid/view/Menu;)V

    .line 244
    return-void
.end method

.method public onPostResume()V
    .locals 1

    .prologue
    .line 140
    invoke-super {p0}, Lz;->onPostResume()V

    .line 141
    invoke-direct {p0}, Los;->l()Lot;

    move-result-object v0

    invoke-virtual {v0}, Lot;->f()V

    .line 142
    return-void
.end method

.method public onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 238
    invoke-direct {p0}, Los;->l()Lot;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lot;->a(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 134
    invoke-super {p0}, Lz;->onStop()V

    .line 135
    invoke-direct {p0}, Los;->l()Lot;

    move-result-object v0

    invoke-virtual {v0}, Lot;->e()V

    .line 136
    return-void
.end method

.method protected onTitleChanged(Ljava/lang/CharSequence;I)V
    .locals 1

    .prologue
    .line 175
    invoke-super {p0, p1, p2}, Lz;->onTitleChanged(Ljava/lang/CharSequence;I)V

    .line 176
    invoke-direct {p0}, Los;->l()Lot;

    move-result-object v0

    invoke-virtual {v0, p1}, Lot;->a(Ljava/lang/CharSequence;)V

    .line 177
    return-void
.end method

.method public setContentView(I)V
    .locals 1

    .prologue
    .line 102
    invoke-direct {p0}, Los;->l()Lot;

    move-result-object v0

    invoke-virtual {v0, p1}, Lot;->a(I)V

    .line 103
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 107
    invoke-direct {p0}, Los;->l()Lot;

    move-result-object v0

    invoke-virtual {v0, p1}, Lot;->a(Landroid/view/View;)V

    .line 108
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .prologue
    .line 112
    invoke-direct {p0}, Los;->l()Lot;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lot;->a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 113
    return-void
.end method
