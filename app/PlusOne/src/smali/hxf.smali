.class public final Lhxf;
.super Llin;
.source "PG"


# static fields
.field private static final D:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field private static E:Landroid/graphics/drawable/Drawable;


# instance fields
.field private A:Z

.field private B:Z

.field private C:Z

.field private final f:I

.field private final g:I

.field private final h:I

.field private final i:I

.field private final j:I

.field private final k:I

.field private final l:I

.field private final m:I

.field private final n:I

.field private final o:Landroid/widget/TextView;

.field private p:Landroid/widget/TextView;

.field private q:Landroid/graphics/drawable/Drawable;

.field private final r:Landroid/graphics/Rect;

.field private final s:I

.field private final t:I

.field private u:Ljava/lang/String;

.field private v:Ljava/lang/String;

.field private w:Ljava/lang/String;

.field private final x:Landroid/text/SpannableStringBuilder;

.field private y:I

.field private z:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lhxf;->D:Landroid/util/SparseArray;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 63
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lhxf;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 64
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, -0x2

    .line 67
    invoke-direct {p0, p1, p2}, Llin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 50
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    iput-object v0, p0, Lhxf;->x:Landroid/text/SpannableStringBuilder;

    .line 54
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhxf;->B:Z

    .line 55
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhxf;->C:Z

    .line 69
    invoke-virtual {p0}, Lhxf;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 70
    const v1, 0x7f0d0215

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lhxf;->f:I

    .line 72
    const v1, 0x7f0d0216

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, p0, Lhxf;->g:I

    .line 74
    const v1, 0x7f0d0217

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, p0, Lhxf;->h:I

    .line 76
    const v1, 0x7f0d0218

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lhxf;->i:I

    .line 78
    const v1, 0x7f0d0219

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, p0, Lhxf;->j:I

    .line 80
    const v1, 0x7f0c006b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lhxf;->t:I

    .line 82
    const v1, 0x7f0d021c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, p0, Lhxf;->l:I

    .line 85
    const v1, 0x7f0d021a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, p0, Lhxf;->k:I

    .line 87
    const v1, 0x7f0d021b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lhxf;->s:I

    .line 89
    const v1, 0x7f0d021d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, p0, Lhxf;->m:I

    .line 91
    const v1, 0x7f0d021e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, p0, Lhxf;->n:I

    .line 94
    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lhxf;->o:Landroid/widget/TextView;

    .line 95
    iget-object v1, p0, Lhxf;->o:Landroid/widget/TextView;

    iget v2, p0, Lhxf;->t:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 96
    iget-object v1, p0, Lhxf;->o:Landroid/widget/TextView;

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 97
    iget-object v1, p0, Lhxf;->o:Landroid/widget/TextView;

    const/16 v2, 0x1b

    invoke-static {p1, v1, v2}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 102
    iget-object v1, p0, Lhxf;->o:Landroid/widget/TextView;

    const/16 v2, 0x10

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 103
    iget-object v1, p0, Lhxf;->o:Landroid/widget/TextView;

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 105
    iget-object v1, p0, Lhxf;->o:Landroid/widget/TextView;

    invoke-virtual {p0, v1}, Lhxf;->addView(Landroid/view/View;)V

    .line 107
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lhxf;->r:Landroid/graphics/Rect;

    .line 109
    sget-object v1, Lhxf;->E:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_0

    .line 110
    const v1, 0x7f02030f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Lhxf;->E:Landroid/graphics/drawable/Drawable;

    .line 113
    :cond_0
    sget-object v0, Lhxf;->E:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lhxf;->q:Landroid/graphics/drawable/Drawable;

    .line 114
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lhxf;->u:Ljava/lang/String;

    return-object v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, -0x2

    .line 201
    iget-object v0, p0, Lhxf;->p:Landroid/widget/TextView;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lhxf;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lhxf;->p:Landroid/widget/TextView;

    iget-object v1, p0, Lhxf;->p:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setSingleLine(Z)V

    iget-object v1, p0, Lhxf;->p:Landroid/widget/TextView;

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    iget-object v1, p0, Lhxf;->p:Landroid/widget/TextView;

    const/16 v2, 0x10

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v1, p0, Lhxf;->p:Landroid/widget/TextView;

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lhxf;->p:Landroid/widget/TextView;

    const/16 v2, 0xc

    invoke-static {v0, v1, v2}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    iget-object v0, p0, Lhxf;->p:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lhxf;->addView(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lhxf;->p:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 202
    iget-object v0, p0, Lhxf;->p:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 203
    iput-boolean v4, p0, Lhxf;->C:Z

    .line 204
    return-void
.end method

.method public a(Ljava/lang/String;ILjava/lang/String;IZ)V
    .locals 9

    .prologue
    const v8, 0x7f020180

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 118
    iput-object p1, p0, Lhxf;->u:Ljava/lang/String;

    .line 119
    iput p2, p0, Lhxf;->y:I

    .line 120
    iput p4, p0, Lhxf;->z:I

    .line 121
    iget-boolean v0, p0, Lhxf;->B:Z

    if-eqz v0, :cond_5

    if-eq p2, v6, :cond_0

    const/16 v0, 0xa

    if-ne p2, v0, :cond_5

    :cond_0
    move v0, v6

    :goto_0
    iput-boolean v0, p0, Lhxf;->A:Z

    .line 125
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    const-string v0, "v.whatshot"

    .line 126
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 127
    const/4 p2, -0x2

    .line 129
    :cond_1
    sget-object v0, Lhxf;->D:Landroid/util/SparseArray;

    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lhxf;->q:Landroid/graphics/drawable/Drawable;

    .line 131
    sparse-switch p2, :sswitch_data_0

    .line 145
    iput-object p3, p0, Lhxf;->v:Ljava/lang/String;

    .line 148
    :goto_1
    iget-object v0, p0, Lhxf;->o:Landroid/widget/TextView;

    iget-object v1, p0, Lhxf;->v:Ljava/lang/String;

    iget-object v2, p0, Lhxf;->x:Landroid/text/SpannableStringBuilder;

    iget-object v3, p0, Lhxf;->w:Ljava/lang/String;

    sget-object v4, Llin;->d:Landroid/text/style/StyleSpan;

    sget-object v5, Llin;->e:Landroid/text/style/ForegroundColorSpan;

    invoke-static/range {v0 .. v5}, Llhv;->a(Landroid/widget/TextView;Ljava/lang/String;Landroid/text/SpannableStringBuilder;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 151
    iget-object v0, p0, Lhxf;->q:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_2

    .line 153
    sparse-switch p2, :sswitch_data_1

    move v0, v8

    .line 180
    :goto_2
    invoke-virtual {p0}, Lhxf;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lhxf;->q:Landroid/graphics/drawable/Drawable;

    .line 182
    sget-object v0, Lhxf;->D:Landroid/util/SparseArray;

    iget-object v1, p0, Lhxf;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 185
    :cond_2
    iget-boolean v0, p0, Lhxf;->A:Z

    if-eqz v0, :cond_8

    .line 186
    invoke-virtual {p0}, Lhxf;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 187
    const v1, 0x7f11002c

    new-array v2, v6, [Ljava/lang/Object;

    .line 188
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    .line 187
    invoke-virtual {v0, v1, p4, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 189
    invoke-virtual {p0, v0}, Lhxf;->a(Ljava/lang/String;)V

    .line 197
    :cond_3
    :goto_3
    iget-object v0, p0, Lhxf;->v:Ljava/lang/String;

    if-nez v0, :cond_4

    const-string v0, "CircleListItemView"

    const-string v1, "Circle name unavailable for content description."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    iget-boolean v0, p0, Lhxf;->A:Z

    if-eqz v0, :cond_9

    invoke-virtual {p0}, Lhxf;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f11002d

    iget v2, p0, Lhxf;->z:I

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lhxf;->v:Ljava/lang/String;

    aput-object v4, v3, v7

    iget v4, p0, Lhxf;->z:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lhxf;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 198
    :goto_4
    return-void

    :cond_5
    move v0, v7

    .line 121
    goto/16 :goto_0

    .line 133
    :sswitch_0
    invoke-virtual {p0}, Lhxf;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a04a3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhxf;->v:Ljava/lang/String;

    goto/16 :goto_1

    .line 136
    :sswitch_1
    invoke-virtual {p0}, Lhxf;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a04a4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhxf;->v:Ljava/lang/String;

    goto/16 :goto_1

    .line 139
    :sswitch_2
    invoke-virtual {p0}, Lhxf;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a04a5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhxf;->v:Ljava/lang/String;

    goto/16 :goto_1

    .line 142
    :sswitch_3
    invoke-virtual {p0}, Lhxf;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0529

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhxf;->v:Ljava/lang/String;

    goto/16 :goto_1

    .line 155
    :sswitch_4
    if-eqz p5, :cond_6

    const v0, 0x7f020313

    goto/16 :goto_2

    :cond_6
    const v0, 0x7f02030f

    goto/16 :goto_2

    .line 159
    :sswitch_5
    const v0, 0x7f0201aa

    .line 160
    goto/16 :goto_2

    .line 162
    :sswitch_6
    if-eqz p5, :cond_7

    const v0, 0x7f0201cd

    goto/16 :goto_2

    :cond_7
    const v0, 0x7f0201ca

    goto/16 :goto_2

    .line 166
    :sswitch_7
    const v0, 0x7f020411

    .line 167
    goto/16 :goto_2

    :sswitch_8
    move v0, v8

    .line 170
    goto/16 :goto_2

    .line 172
    :sswitch_9
    const v0, 0x7f02030a

    .line 173
    goto/16 :goto_2

    .line 175
    :sswitch_a
    const v0, 0x7f020418

    .line 176
    goto/16 :goto_2

    .line 191
    :cond_8
    iput-boolean v7, p0, Lhxf;->C:Z

    .line 192
    iget-object v0, p0, Lhxf;->p:Landroid/widget/TextView;

    if-eqz v0, :cond_3

    .line 193
    iget-object v0, p0, Lhxf;->p:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_3

    .line 197
    :cond_9
    iget-object v0, p0, Lhxf;->v:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lhxf;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 131
    :sswitch_data_0
    .sparse-switch
        -0x2 -> :sswitch_3
        0x5 -> :sswitch_2
        0x7 -> :sswitch_1
        0x9 -> :sswitch_0
    .end sparse-switch

    .line 153
    :sswitch_data_1
    .sparse-switch
        -0x2 -> :sswitch_a
        0x5 -> :sswitch_8
        0x7 -> :sswitch_6
        0x8 -> :sswitch_5
        0x9 -> :sswitch_4
        0xa -> :sswitch_7
        0x65 -> :sswitch_9
    .end sparse-switch
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 223
    iget-object v0, p0, Lhxf;->u:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 224
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Call setMemberCountVisible() before calling setCircle()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 227
    :cond_0
    iput-boolean p1, p0, Lhxf;->B:Z

    .line 228
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lhxf;->v:Ljava/lang/String;

    return-object v0
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 231
    if-nez p1, :cond_0

    .line 232
    const/4 v0, 0x0

    iput-object v0, p0, Lhxf;->w:Ljava/lang/String;

    .line 236
    :goto_0
    return-void

    .line 234
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhxf;->w:Ljava/lang/String;

    goto :goto_0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 215
    iget v0, p0, Lhxf;->y:I

    return v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 219
    iget v0, p0, Lhxf;->z:I

    return v0
.end method

.method public dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 331
    invoke-virtual {p0}, Lhxf;->c()I

    move-result v0

    const/4 v1, -0x3

    if-eq v0, v1, :cond_0

    .line 332
    iget-object v0, p0, Lhxf;->q:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lhxf;->r:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 333
    iget-object v0, p0, Lhxf;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 335
    :cond_0
    invoke-super {p0, p1}, Llin;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 336
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 11

    .prologue
    .line 287
    iget v2, p0, Lhxf;->i:I

    .line 289
    iget-object v0, p0, Lhxf;->o:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v1

    .line 292
    iget-boolean v0, p0, Lhxf;->C:Z

    if-eqz v0, :cond_2

    .line 293
    iget-object v0, p0, Lhxf;->p:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v0

    iget v3, p0, Lhxf;->n:I

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    .line 296
    :goto_0
    sub-int v3, p5, p3

    iget v4, p0, Lhxf;->s:I

    sub-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    .line 297
    sub-int v4, p5, p3

    sub-int/2addr v4, v0

    div-int/lit8 v4, v4, 0x2

    .line 298
    iget-object v5, p0, Lhxf;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    .line 299
    iget-object v6, p0, Lhxf;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v6

    .line 300
    iget v7, p0, Lhxf;->s:I

    sub-int/2addr v7, v6

    div-int/lit8 v7, v7, 0x2

    add-int/2addr v3, v7

    .line 301
    iget v7, p0, Lhxf;->s:I

    sub-int/2addr v7, v5

    div-int/lit8 v7, v7, 0x2

    add-int/2addr v7, v2

    .line 303
    iget-object v8, p0, Lhxf;->r:Landroid/graphics/Rect;

    add-int/2addr v5, v7

    add-int/2addr v6, v3

    invoke-virtual {v8, v7, v3, v5, v6}, Landroid/graphics/Rect;->set(IIII)V

    .line 306
    iget v3, p0, Lhxf;->s:I

    iget v5, p0, Lhxf;->k:I

    add-int/2addr v3, v5

    add-int/2addr v3, v2

    .line 307
    iget v2, p0, Lhxf;->j:I

    sub-int v2, p4, v2

    .line 308
    iget-boolean v5, p0, Lhxf;->b:Z

    if-eqz v5, :cond_0

    .line 309
    iget-object v5, p0, Lhxf;->c:Landroid/widget/CheckBox;

    invoke-virtual {v5}, Landroid/widget/CheckBox;->getMeasuredWidth()I

    move-result v5

    .line 310
    iget-object v6, p0, Lhxf;->c:Landroid/widget/CheckBox;

    invoke-virtual {v6}, Landroid/widget/CheckBox;->getMeasuredHeight()I

    move-result v6

    .line 311
    sub-int v7, p5, p3

    sub-int/2addr v7, v6

    div-int/lit8 v7, v7, 0x2

    .line 312
    iget-object v8, p0, Lhxf;->c:Landroid/widget/CheckBox;

    sub-int v9, v2, v5

    sget v10, Llin;->a:I

    sub-int/2addr v9, v10

    sget v10, Llin;->a:I

    sub-int v10, v2, v10

    add-int/2addr v6, v7

    invoke-virtual {v8, v9, v7, v10, v6}, Landroid/widget/CheckBox;->layout(IIII)V

    .line 316
    iget v6, p0, Lhxf;->m:I

    add-int/2addr v5, v6

    sub-int/2addr v2, v5

    .line 319
    :cond_0
    iget-object v5, p0, Lhxf;->o:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v5

    sub-int v6, v2, v3

    iget v7, p0, Lhxf;->l:I

    sub-int/2addr v6, v7

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 321
    iget-object v6, p0, Lhxf;->o:Landroid/widget/TextView;

    add-int/2addr v5, v3

    add-int v7, v4, v1

    invoke-virtual {v6, v3, v4, v5, v7}, Landroid/widget/TextView;->layout(IIII)V

    .line 323
    iget-boolean v5, p0, Lhxf;->C:Z

    if-eqz v5, :cond_1

    .line 324
    add-int/2addr v1, v4

    iget v5, p0, Lhxf;->n:I

    add-int/2addr v1, v5

    .line 325
    iget-object v5, p0, Lhxf;->p:Landroid/widget/TextView;

    add-int/2addr v0, v4

    invoke-virtual {v5, v3, v1, v2, v0}, Landroid/widget/TextView;->layout(IIII)V

    .line 327
    :cond_1
    return-void

    :cond_2
    move v0, v1

    goto/16 :goto_0
.end method

.method protected onMeasure(II)V
    .locals 8

    .prologue
    const/high16 v7, 0x40000000    # 2.0f

    const/4 v2, 0x0

    .line 240
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    .line 243
    iget v0, p0, Lhxf;->i:I

    sub-int v0, v4, v0

    iget v1, p0, Lhxf;->j:I

    sub-int/2addr v0, v1

    iget v1, p0, Lhxf;->s:I

    sub-int/2addr v0, v1

    iget v1, p0, Lhxf;->k:I

    sub-int/2addr v0, v1

    .line 246
    iget-boolean v1, p0, Lhxf;->b:Z

    if-eqz v1, :cond_1

    .line 247
    iget-object v1, p0, Lhxf;->c:Landroid/widget/CheckBox;

    invoke-virtual {v1, v2, p2}, Landroid/widget/CheckBox;->measure(II)V

    .line 248
    iget-object v1, p0, Lhxf;->c:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->getMeasuredHeight()I

    move-result v1

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 249
    iget-object v3, p0, Lhxf;->c:Landroid/widget/CheckBox;

    invoke-virtual {v3}, Landroid/widget/CheckBox;->getMeasuredWidth()I

    move-result v3

    iget v5, p0, Lhxf;->m:I

    add-int/2addr v3, v5

    sub-int/2addr v0, v3

    .line 253
    :goto_0
    iget-object v3, p0, Lhxf;->o:Landroid/widget/TextView;

    invoke-virtual {v3, v2, v2}, Landroid/widget/TextView;->measure(II)V

    .line 255
    iget-object v3, p0, Lhxf;->o:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v3

    .line 257
    iget-boolean v5, p0, Lhxf;->C:Z

    if-eqz v5, :cond_0

    .line 258
    iget-object v5, p0, Lhxf;->p:Landroid/widget/TextView;

    invoke-virtual {v5, v2, v2}, Landroid/widget/TextView;->measure(II)V

    .line 259
    iget-object v2, p0, Lhxf;->p:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v2

    iget v5, p0, Lhxf;->n:I

    add-int/2addr v2, v5

    add-int/2addr v2, v3

    .line 260
    iget-object v3, p0, Lhxf;->p:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v3

    invoke-static {v3, v0}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 262
    iget-object v5, p0, Lhxf;->p:Landroid/widget/TextView;

    .line 263
    invoke-static {v3, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    iget-object v6, p0, Lhxf;->p:Landroid/widget/TextView;

    .line 264
    invoke-virtual {v6}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v6

    invoke-static {v6, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    .line 262
    invoke-virtual {v5, v3, v6}, Landroid/widget/TextView;->measure(II)V

    .line 268
    :goto_1
    iget-object v3, p0, Lhxf;->o:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v3

    invoke-static {v3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 270
    iget-object v3, p0, Lhxf;->o:Landroid/widget/TextView;

    .line 271
    invoke-static {v0, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    iget v5, p0, Lhxf;->t:I

    mul-int/2addr v5, v2

    const/high16 v6, -0x80000000

    .line 273
    invoke-static {v5, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 270
    invoke-virtual {v3, v0, v5}, Landroid/widget/TextView;->measure(II)V

    .line 276
    iget-object v0, p0, Lhxf;->o:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLineCount()I

    move-result v0

    mul-int/2addr v0, v2

    .line 278
    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget v1, p0, Lhxf;->s:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 279
    iget v1, p0, Lhxf;->g:I

    iget v2, p0, Lhxf;->h:I

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 280
    iget v1, p0, Lhxf;->f:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 282
    invoke-virtual {p0, v4, v0}, Lhxf;->setMeasuredDimension(II)V

    .line 283
    return-void

    :cond_0
    move v2, v3

    goto :goto_1

    :cond_1
    move v1, v2

    goto :goto_0
.end method
