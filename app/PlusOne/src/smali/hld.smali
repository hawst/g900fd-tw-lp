.class public final enum Lhld;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lhld;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lhld;

.field public static final enum b:Lhld;

.field public static final enum c:Lhld;

.field public static final enum d:Lhld;

.field public static final enum e:Lhld;

.field public static final enum f:Lhld;

.field private static g:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lhld;",
            ">;"
        }
    .end annotation
.end field

.field private static final synthetic i:[Lhld;


# instance fields
.field private final h:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 10
    new-instance v1, Lhld;

    const-string v2, "Queued"

    const-string v3, "Queued"

    invoke-direct {v1, v2, v0, v3}, Lhld;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lhld;->a:Lhld;

    .line 12
    new-instance v1, Lhld;

    const-string v2, "InProgress"

    const-string v3, "InProgress"

    invoke-direct {v1, v2, v5, v3}, Lhld;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lhld;->b:Lhld;

    .line 14
    new-instance v1, Lhld;

    const-string v2, "Complete"

    const-string v3, "Complete"

    invoke-direct {v1, v2, v6, v3}, Lhld;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lhld;->c:Lhld;

    .line 16
    new-instance v1, Lhld;

    const-string v2, "Error"

    const-string v3, "Error"

    invoke-direct {v1, v2, v7, v3}, Lhld;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lhld;->d:Lhld;

    .line 18
    new-instance v1, Lhld;

    const-string v2, "Failed"

    const-string v3, "Failed"

    invoke-direct {v1, v2, v8, v3}, Lhld;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lhld;->e:Lhld;

    .line 20
    new-instance v1, Lhld;

    const-string v2, "Cancelled"

    const/4 v3, 0x5

    const-string v4, "Cancelled"

    invoke-direct {v1, v2, v3, v4}, Lhld;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lhld;->f:Lhld;

    .line 8
    const/4 v1, 0x6

    new-array v1, v1, [Lhld;

    sget-object v2, Lhld;->a:Lhld;

    aput-object v2, v1, v0

    sget-object v2, Lhld;->b:Lhld;

    aput-object v2, v1, v5

    sget-object v2, Lhld;->c:Lhld;

    aput-object v2, v1, v6

    sget-object v2, Lhld;->d:Lhld;

    aput-object v2, v1, v7

    sget-object v2, Lhld;->e:Lhld;

    aput-object v2, v1, v8

    const/4 v2, 0x5

    sget-object v3, Lhld;->f:Lhld;

    aput-object v3, v1, v2

    sput-object v1, Lhld;->i:[Lhld;

    .line 24
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sput-object v1, Lhld;->g:Ljava/util/HashMap;

    .line 25
    invoke-static {}, Lhld;->values()[Lhld;

    move-result-object v1

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 26
    sget-object v4, Lhld;->g:Ljava/util/HashMap;

    invoke-virtual {v3}, Lhld;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 25
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 28
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 38
    iput-object p3, p0, Lhld;->h:Ljava/lang/String;

    .line 39
    return-void
.end method

.method public static a(Ljava/lang/String;)Lhld;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lhld;->g:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhld;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lhld;
    .locals 1

    .prologue
    .line 8
    const-class v0, Lhld;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lhld;

    return-object v0
.end method

.method public static values()[Lhld;
    .locals 1

    .prologue
    .line 8
    sget-object v0, Lhld;->i:[Lhld;

    invoke-virtual {v0}, [Lhld;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhld;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lhld;->h:Ljava/lang/String;

    return-object v0
.end method
