.class public final Lpgx;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lpgx;


# instance fields
.field public b:Lppf;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Lpgx;

    sput-object v0, Lpgx;->a:[Lpgx;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 18
    const/4 v0, 0x0

    iput-object v0, p0, Lpgx;->b:Lppf;

    .line 27
    const/high16 v0, -0x80000000

    iput v0, p0, Lpgx;->f:I

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 53
    const/4 v0, 0x0

    .line 54
    iget-object v1, p0, Lpgx;->b:Lppf;

    if-eqz v1, :cond_0

    .line 55
    const/4 v0, 0x1

    iget-object v1, p0, Lpgx;->b:Lppf;

    .line 56
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 58
    :cond_0
    iget-object v1, p0, Lpgx;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 59
    const/4 v1, 0x2

    iget-object v2, p0, Lpgx;->c:Ljava/lang/String;

    .line 60
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 62
    :cond_1
    iget-object v1, p0, Lpgx;->e:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 63
    const/4 v1, 0x3

    iget-object v2, p0, Lpgx;->e:Ljava/lang/String;

    .line 64
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 66
    :cond_2
    iget-object v1, p0, Lpgx;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 67
    const/4 v1, 0x4

    iget-object v2, p0, Lpgx;->d:Ljava/lang/String;

    .line 68
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 70
    :cond_3
    iget v1, p0, Lpgx;->f:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_4

    .line 71
    const/4 v1, 0x5

    iget v2, p0, Lpgx;->f:I

    .line 72
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 74
    :cond_4
    iget-object v1, p0, Lpgx;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 75
    iput v0, p0, Lpgx;->ai:I

    .line 76
    return v0
.end method

.method public a(Loxn;)Lpgx;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 84
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 85
    sparse-switch v0, :sswitch_data_0

    .line 89
    iget-object v1, p0, Lpgx;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 90
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpgx;->ah:Ljava/util/List;

    .line 93
    :cond_1
    iget-object v1, p0, Lpgx;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 95
    :sswitch_0
    return-object p0

    .line 100
    :sswitch_1
    iget-object v0, p0, Lpgx;->b:Lppf;

    if-nez v0, :cond_2

    .line 101
    new-instance v0, Lppf;

    invoke-direct {v0}, Lppf;-><init>()V

    iput-object v0, p0, Lpgx;->b:Lppf;

    .line 103
    :cond_2
    iget-object v0, p0, Lpgx;->b:Lppf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 107
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpgx;->c:Ljava/lang/String;

    goto :goto_0

    .line 111
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpgx;->e:Ljava/lang/String;

    goto :goto_0

    .line 115
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpgx;->d:Ljava/lang/String;

    goto :goto_0

    .line 119
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 120
    if-eq v0, v2, :cond_3

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    .line 122
    :cond_3
    iput v0, p0, Lpgx;->f:I

    goto :goto_0

    .line 124
    :cond_4
    iput v2, p0, Lpgx;->f:I

    goto :goto_0

    .line 85
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 32
    iget-object v0, p0, Lpgx;->b:Lppf;

    if-eqz v0, :cond_0

    .line 33
    const/4 v0, 0x1

    iget-object v1, p0, Lpgx;->b:Lppf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 35
    :cond_0
    iget-object v0, p0, Lpgx;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 36
    const/4 v0, 0x2

    iget-object v1, p0, Lpgx;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 38
    :cond_1
    iget-object v0, p0, Lpgx;->e:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 39
    const/4 v0, 0x3

    iget-object v1, p0, Lpgx;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 41
    :cond_2
    iget-object v0, p0, Lpgx;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 42
    const/4 v0, 0x4

    iget-object v1, p0, Lpgx;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 44
    :cond_3
    iget v0, p0, Lpgx;->f:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_4

    .line 45
    const/4 v0, 0x5

    iget v1, p0, Lpgx;->f:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 47
    :cond_4
    iget-object v0, p0, Lpgx;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 49
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lpgx;->a(Loxn;)Lpgx;

    move-result-object v0

    return-object v0
.end method
