.class public final Locf;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Locg;

.field public b:[Locb;

.field public c:[Locm;

.field public d:Locd;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1441
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1444
    sget-object v0, Locg;->a:[Locg;

    iput-object v0, p0, Locf;->a:[Locg;

    .line 1447
    sget-object v0, Locb;->a:[Locb;

    iput-object v0, p0, Locf;->b:[Locb;

    .line 1450
    sget-object v0, Locm;->a:[Locm;

    iput-object v0, p0, Locf;->c:[Locm;

    .line 1453
    const/4 v0, 0x0

    iput-object v0, p0, Locf;->d:Locd;

    .line 1441
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1488
    .line 1489
    iget-object v0, p0, Locf;->a:[Locg;

    if-eqz v0, :cond_1

    .line 1490
    iget-object v3, p0, Locf;->a:[Locg;

    array-length v4, v3

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 1491
    if-eqz v5, :cond_0

    .line 1492
    const/4 v6, 0x1

    .line 1493
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 1490
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 1497
    :cond_2
    iget-object v2, p0, Locf;->b:[Locb;

    if-eqz v2, :cond_4

    .line 1498
    iget-object v3, p0, Locf;->b:[Locb;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_4

    aget-object v5, v3, v2

    .line 1499
    if-eqz v5, :cond_3

    .line 1500
    const/4 v6, 0x2

    .line 1501
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 1498
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1505
    :cond_4
    iget-object v2, p0, Locf;->c:[Locm;

    if-eqz v2, :cond_6

    .line 1506
    iget-object v2, p0, Locf;->c:[Locm;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    .line 1507
    if-eqz v4, :cond_5

    .line 1508
    const/4 v5, 0x3

    .line 1509
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 1506
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1513
    :cond_6
    iget-object v1, p0, Locf;->d:Locd;

    if-eqz v1, :cond_7

    .line 1514
    const/4 v1, 0x4

    iget-object v2, p0, Locf;->d:Locd;

    .line 1515
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1517
    :cond_7
    iget-object v1, p0, Locf;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1518
    iput v0, p0, Locf;->ai:I

    .line 1519
    return v0
.end method

.method public a(Loxn;)Locf;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1527
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1528
    sparse-switch v0, :sswitch_data_0

    .line 1532
    iget-object v2, p0, Locf;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 1533
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Locf;->ah:Ljava/util/List;

    .line 1536
    :cond_1
    iget-object v2, p0, Locf;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1538
    :sswitch_0
    return-object p0

    .line 1543
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1544
    iget-object v0, p0, Locf;->a:[Locg;

    if-nez v0, :cond_3

    move v0, v1

    .line 1545
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Locg;

    .line 1546
    iget-object v3, p0, Locf;->a:[Locg;

    if-eqz v3, :cond_2

    .line 1547
    iget-object v3, p0, Locf;->a:[Locg;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1549
    :cond_2
    iput-object v2, p0, Locf;->a:[Locg;

    .line 1550
    :goto_2
    iget-object v2, p0, Locf;->a:[Locg;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 1551
    iget-object v2, p0, Locf;->a:[Locg;

    new-instance v3, Locg;

    invoke-direct {v3}, Locg;-><init>()V

    aput-object v3, v2, v0

    .line 1552
    iget-object v2, p0, Locf;->a:[Locg;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1553
    invoke-virtual {p1}, Loxn;->a()I

    .line 1550
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1544
    :cond_3
    iget-object v0, p0, Locf;->a:[Locg;

    array-length v0, v0

    goto :goto_1

    .line 1556
    :cond_4
    iget-object v2, p0, Locf;->a:[Locg;

    new-instance v3, Locg;

    invoke-direct {v3}, Locg;-><init>()V

    aput-object v3, v2, v0

    .line 1557
    iget-object v2, p0, Locf;->a:[Locg;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1561
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1562
    iget-object v0, p0, Locf;->b:[Locb;

    if-nez v0, :cond_6

    move v0, v1

    .line 1563
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Locb;

    .line 1564
    iget-object v3, p0, Locf;->b:[Locb;

    if-eqz v3, :cond_5

    .line 1565
    iget-object v3, p0, Locf;->b:[Locb;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1567
    :cond_5
    iput-object v2, p0, Locf;->b:[Locb;

    .line 1568
    :goto_4
    iget-object v2, p0, Locf;->b:[Locb;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    .line 1569
    iget-object v2, p0, Locf;->b:[Locb;

    new-instance v3, Locb;

    invoke-direct {v3}, Locb;-><init>()V

    aput-object v3, v2, v0

    .line 1570
    iget-object v2, p0, Locf;->b:[Locb;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1571
    invoke-virtual {p1}, Loxn;->a()I

    .line 1568
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 1562
    :cond_6
    iget-object v0, p0, Locf;->b:[Locb;

    array-length v0, v0

    goto :goto_3

    .line 1574
    :cond_7
    iget-object v2, p0, Locf;->b:[Locb;

    new-instance v3, Locb;

    invoke-direct {v3}, Locb;-><init>()V

    aput-object v3, v2, v0

    .line 1575
    iget-object v2, p0, Locf;->b:[Locb;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1579
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1580
    iget-object v0, p0, Locf;->c:[Locm;

    if-nez v0, :cond_9

    move v0, v1

    .line 1581
    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Locm;

    .line 1582
    iget-object v3, p0, Locf;->c:[Locm;

    if-eqz v3, :cond_8

    .line 1583
    iget-object v3, p0, Locf;->c:[Locm;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1585
    :cond_8
    iput-object v2, p0, Locf;->c:[Locm;

    .line 1586
    :goto_6
    iget-object v2, p0, Locf;->c:[Locm;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_a

    .line 1587
    iget-object v2, p0, Locf;->c:[Locm;

    new-instance v3, Locm;

    invoke-direct {v3}, Locm;-><init>()V

    aput-object v3, v2, v0

    .line 1588
    iget-object v2, p0, Locf;->c:[Locm;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1589
    invoke-virtual {p1}, Loxn;->a()I

    .line 1586
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 1580
    :cond_9
    iget-object v0, p0, Locf;->c:[Locm;

    array-length v0, v0

    goto :goto_5

    .line 1592
    :cond_a
    iget-object v2, p0, Locf;->c:[Locm;

    new-instance v3, Locm;

    invoke-direct {v3}, Locm;-><init>()V

    aput-object v3, v2, v0

    .line 1593
    iget-object v2, p0, Locf;->c:[Locm;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1597
    :sswitch_4
    iget-object v0, p0, Locf;->d:Locd;

    if-nez v0, :cond_b

    .line 1598
    new-instance v0, Locd;

    invoke-direct {v0}, Locd;-><init>()V

    iput-object v0, p0, Locf;->d:Locd;

    .line 1600
    :cond_b
    iget-object v0, p0, Locf;->d:Locd;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1528
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1458
    iget-object v1, p0, Locf;->a:[Locg;

    if-eqz v1, :cond_1

    .line 1459
    iget-object v2, p0, Locf;->a:[Locg;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 1460
    if-eqz v4, :cond_0

    .line 1461
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 1459
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1465
    :cond_1
    iget-object v1, p0, Locf;->b:[Locb;

    if-eqz v1, :cond_3

    .line 1466
    iget-object v2, p0, Locf;->b:[Locb;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 1467
    if-eqz v4, :cond_2

    .line 1468
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 1466
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1472
    :cond_3
    iget-object v1, p0, Locf;->c:[Locm;

    if-eqz v1, :cond_5

    .line 1473
    iget-object v1, p0, Locf;->c:[Locm;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    .line 1474
    if-eqz v3, :cond_4

    .line 1475
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 1473
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1479
    :cond_5
    iget-object v0, p0, Locf;->d:Locd;

    if-eqz v0, :cond_6

    .line 1480
    const/4 v0, 0x4

    iget-object v1, p0, Locf;->d:Locd;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1482
    :cond_6
    iget-object v0, p0, Locf;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1484
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1437
    invoke-virtual {p0, p1}, Locf;->a(Loxn;)Locf;

    move-result-object v0

    return-object v0
.end method
