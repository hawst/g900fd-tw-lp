.class public final Lnyu;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Boolean;

.field public b:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5904
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 5925
    const/4 v0, 0x0

    .line 5926
    iget-object v1, p0, Lnyu;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 5927
    const/4 v0, 0x1

    iget-object v1, p0, Lnyu;->a:Ljava/lang/Boolean;

    .line 5928
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 5930
    :cond_0
    iget-object v1, p0, Lnyu;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 5931
    const/4 v1, 0x2

    iget-object v2, p0, Lnyu;->b:Ljava/lang/Boolean;

    .line 5932
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 5934
    :cond_1
    iget-object v1, p0, Lnyu;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5935
    iput v0, p0, Lnyu;->ai:I

    .line 5936
    return v0
.end method

.method public a(Loxn;)Lnyu;
    .locals 2

    .prologue
    .line 5944
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 5945
    sparse-switch v0, :sswitch_data_0

    .line 5949
    iget-object v1, p0, Lnyu;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 5950
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnyu;->ah:Ljava/util/List;

    .line 5953
    :cond_1
    iget-object v1, p0, Lnyu;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5955
    :sswitch_0
    return-object p0

    .line 5960
    :sswitch_1
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnyu;->a:Ljava/lang/Boolean;

    goto :goto_0

    .line 5964
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnyu;->b:Ljava/lang/Boolean;

    goto :goto_0

    .line 5945
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 5913
    iget-object v0, p0, Lnyu;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 5914
    const/4 v0, 0x1

    iget-object v1, p0, Lnyu;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 5916
    :cond_0
    iget-object v0, p0, Lnyu;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 5917
    const/4 v0, 0x2

    iget-object v1, p0, Lnyu;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 5919
    :cond_1
    iget-object v0, p0, Lnyu;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 5921
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5900
    invoke-virtual {p0, p1}, Lnyu;->a(Loxn;)Lnyu;

    move-result-object v0

    return-object v0
.end method
