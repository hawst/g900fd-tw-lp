.class public final Ldtu;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lnyq;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Ldtu;->a:Ljava/util/HashMap;

    return-void
.end method

.method public static a(Landroid/content/Context;ILdts;)I
    .locals 3

    .prologue
    const/16 v0, 0x28

    .line 116
    .line 119
    invoke-static {p0, p1}, Lcuj;->b(Landroid/content/Context;I)Lnyj;

    move-result-object v1

    .line 120
    if-eqz v1, :cond_0

    iget-object v2, v1, Lnyj;->a:Ljava/lang/Integer;

    .line 121
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ge v2, v0, :cond_0

    .line 122
    iget-object v0, v1, Lnyj;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 127
    :cond_0
    if-nez v0, :cond_2

    .line 128
    sget-object v1, Ldts;->c:Ldts;

    if-eq p2, v1, :cond_1

    sget-object v1, Ldts;->b:Ldts;

    if-ne p2, v1, :cond_2

    .line 130
    :cond_1
    const/4 v0, 0x2

    .line 131
    :cond_2
    return v0
.end method

.method public static a(Landroid/content/Context;I)Lnyq;
    .locals 5

    .prologue
    .line 61
    sget-object v1, Ldtu;->a:Ljava/util/HashMap;

    monitor-enter v1

    .line 62
    :try_start_0
    sget-object v0, Ldtu;->a:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnyq;

    .line 63
    if-eqz v0, :cond_0

    iget-object v2, v0, Lnyq;->k:Lnyj;

    if-eqz v2, :cond_0

    iget-object v0, v0, Lnyq;->l:Lnyc;

    if-nez v0, :cond_1

    .line 65
    :cond_0
    new-instance v2, Lnyr;

    invoke-direct {v2}, Lnyr;-><init>()V

    .line 66
    invoke-static {v2}, Ldtu;->a(Lnyr;)V

    .line 68
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 69
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v3, "gaia_id"

    .line 70
    invoke-interface {v0, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 71
    new-instance v3, Lhqc;

    new-instance v4, Lkfo;

    invoke-direct {v4, p0, p1}, Lkfo;-><init>(Landroid/content/Context;I)V

    invoke-direct {v3, p0, v4, v0, v2}, Lhqc;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Lnyr;)V

    .line 74
    invoke-virtual {v3}, Lhqc;->l()V

    .line 75
    invoke-virtual {v3}, Lhqc;->t()Z

    move-result v0

    if-nez v0, :cond_2

    .line 76
    sget-object v0, Ldtu;->a:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v3}, Lhqc;->i()Lnyq;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    :cond_1
    :goto_0
    sget-object v0, Ldtu;->a:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnyq;

    monitor-exit v1

    return-object v0

    .line 78
    :cond_2
    const-string v0, "EsTileSyncSettings"

    invoke-virtual {v3, v0}, Lhqc;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 82
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static a(I)V
    .locals 3

    .prologue
    .line 44
    sget-object v1, Ldtu;->a:Ljava/util/HashMap;

    monitor-enter v1

    .line 45
    :try_start_0
    sget-object v0, Ldtu;->a:Ljava/util/HashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static a(ILnyq;)V
    .locals 3

    .prologue
    .line 50
    sget-object v1, Ldtu;->a:Ljava/util/HashMap;

    monitor-enter v1

    .line 51
    :try_start_0
    sget-object v0, Ldtu;->a:Ljava/util/HashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static a(Lnyr;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 102
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnyr;->i:Ljava/lang/Boolean;

    .line 103
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnyr;->j:Ljava/lang/Boolean;

    .line 104
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnyr;->d:Ljava/lang/Boolean;

    .line 105
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnyr;->g:Ljava/lang/Boolean;

    .line 106
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnyr;->b:Ljava/lang/Boolean;

    .line 107
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnyr;->f:Ljava/lang/Boolean;

    .line 108
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnyr;->k:Ljava/lang/Boolean;

    .line 109
    return-void
.end method

.method public static b(Landroid/content/Context;I)J
    .locals 2

    .prologue
    .line 91
    .line 92
    invoke-static {p0, p1}, Lcuj;->b(Landroid/content/Context;I)Lnyj;

    move-result-object v0

    .line 93
    if-eqz v0, :cond_0

    iget-object v0, v0, Lnyj;->b:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method
