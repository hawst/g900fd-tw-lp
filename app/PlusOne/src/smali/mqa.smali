.class public final Lmqa;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lmpm;

.field public c:I

.field public d:[Lpaf;

.field private e:[Lpcu;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lmqa;->b:Lmpm;

    .line 23
    const/high16 v0, -0x80000000

    iput v0, p0, Lmqa;->c:I

    .line 26
    sget-object v0, Lpaf;->a:[Lpaf;

    iput-object v0, p0, Lmqa;->d:[Lpaf;

    .line 29
    sget-object v0, Lpcu;->a:[Lpcu;

    iput-object v0, p0, Lmqa;->e:[Lpcu;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 63
    .line 64
    iget-object v0, p0, Lmqa;->a:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 65
    const/4 v0, 0x1

    iget-object v2, p0, Lmqa;->a:Ljava/lang/String;

    .line 66
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 68
    :goto_0
    iget v2, p0, Lmqa;->c:I

    const/high16 v3, -0x80000000

    if-eq v2, v3, :cond_0

    .line 69
    const/4 v2, 0x2

    iget v3, p0, Lmqa;->c:I

    .line 70
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 72
    :cond_0
    iget-object v2, p0, Lmqa;->d:[Lpaf;

    if-eqz v2, :cond_2

    .line 73
    iget-object v3, p0, Lmqa;->d:[Lpaf;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 74
    if-eqz v5, :cond_1

    .line 75
    const/4 v6, 0x3

    .line 76
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 73
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 80
    :cond_2
    iget-object v2, p0, Lmqa;->b:Lmpm;

    if-eqz v2, :cond_3

    .line 81
    const/4 v2, 0x4

    iget-object v3, p0, Lmqa;->b:Lmpm;

    .line 82
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 84
    :cond_3
    iget-object v2, p0, Lmqa;->e:[Lpcu;

    if-eqz v2, :cond_5

    .line 85
    iget-object v2, p0, Lmqa;->e:[Lpcu;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    .line 86
    if-eqz v4, :cond_4

    .line 87
    const/4 v5, 0x5

    .line 88
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 85
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 92
    :cond_5
    iget-object v1, p0, Lmqa;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 93
    iput v0, p0, Lmqa;->ai:I

    .line 94
    return v0

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lmqa;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 102
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 103
    sparse-switch v0, :sswitch_data_0

    .line 107
    iget-object v2, p0, Lmqa;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 108
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmqa;->ah:Ljava/util/List;

    .line 111
    :cond_1
    iget-object v2, p0, Lmqa;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 113
    :sswitch_0
    return-object p0

    .line 118
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmqa;->a:Ljava/lang/String;

    goto :goto_0

    .line 122
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 123
    if-eq v0, v4, :cond_2

    const/4 v2, 0x2

    if-ne v0, v2, :cond_3

    .line 125
    :cond_2
    iput v0, p0, Lmqa;->c:I

    goto :goto_0

    .line 127
    :cond_3
    iput v4, p0, Lmqa;->c:I

    goto :goto_0

    .line 132
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 133
    iget-object v0, p0, Lmqa;->d:[Lpaf;

    if-nez v0, :cond_5

    move v0, v1

    .line 134
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lpaf;

    .line 135
    iget-object v3, p0, Lmqa;->d:[Lpaf;

    if-eqz v3, :cond_4

    .line 136
    iget-object v3, p0, Lmqa;->d:[Lpaf;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 138
    :cond_4
    iput-object v2, p0, Lmqa;->d:[Lpaf;

    .line 139
    :goto_2
    iget-object v2, p0, Lmqa;->d:[Lpaf;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 140
    iget-object v2, p0, Lmqa;->d:[Lpaf;

    new-instance v3, Lpaf;

    invoke-direct {v3}, Lpaf;-><init>()V

    aput-object v3, v2, v0

    .line 141
    iget-object v2, p0, Lmqa;->d:[Lpaf;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 142
    invoke-virtual {p1}, Loxn;->a()I

    .line 139
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 133
    :cond_5
    iget-object v0, p0, Lmqa;->d:[Lpaf;

    array-length v0, v0

    goto :goto_1

    .line 145
    :cond_6
    iget-object v2, p0, Lmqa;->d:[Lpaf;

    new-instance v3, Lpaf;

    invoke-direct {v3}, Lpaf;-><init>()V

    aput-object v3, v2, v0

    .line 146
    iget-object v2, p0, Lmqa;->d:[Lpaf;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 150
    :sswitch_4
    iget-object v0, p0, Lmqa;->b:Lmpm;

    if-nez v0, :cond_7

    .line 151
    new-instance v0, Lmpm;

    invoke-direct {v0}, Lmpm;-><init>()V

    iput-object v0, p0, Lmqa;->b:Lmpm;

    .line 153
    :cond_7
    iget-object v0, p0, Lmqa;->b:Lmpm;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 157
    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 158
    iget-object v0, p0, Lmqa;->e:[Lpcu;

    if-nez v0, :cond_9

    move v0, v1

    .line 159
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lpcu;

    .line 160
    iget-object v3, p0, Lmqa;->e:[Lpcu;

    if-eqz v3, :cond_8

    .line 161
    iget-object v3, p0, Lmqa;->e:[Lpcu;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 163
    :cond_8
    iput-object v2, p0, Lmqa;->e:[Lpcu;

    .line 164
    :goto_4
    iget-object v2, p0, Lmqa;->e:[Lpcu;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_a

    .line 165
    iget-object v2, p0, Lmqa;->e:[Lpcu;

    new-instance v3, Lpcu;

    invoke-direct {v3}, Lpcu;-><init>()V

    aput-object v3, v2, v0

    .line 166
    iget-object v2, p0, Lmqa;->e:[Lpcu;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 167
    invoke-virtual {p1}, Loxn;->a()I

    .line 164
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 158
    :cond_9
    iget-object v0, p0, Lmqa;->e:[Lpcu;

    array-length v0, v0

    goto :goto_3

    .line 170
    :cond_a
    iget-object v2, p0, Lmqa;->e:[Lpcu;

    new-instance v3, Lpcu;

    invoke-direct {v3}, Lpcu;-><init>()V

    aput-object v3, v2, v0

    .line 171
    iget-object v2, p0, Lmqa;->e:[Lpcu;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 103
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 34
    iget-object v1, p0, Lmqa;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 35
    const/4 v1, 0x1

    iget-object v2, p0, Lmqa;->a:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 37
    :cond_0
    iget v1, p0, Lmqa;->c:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_1

    .line 38
    const/4 v1, 0x2

    iget v2, p0, Lmqa;->c:I

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 40
    :cond_1
    iget-object v1, p0, Lmqa;->d:[Lpaf;

    if-eqz v1, :cond_3

    .line 41
    iget-object v2, p0, Lmqa;->d:[Lpaf;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 42
    if-eqz v4, :cond_2

    .line 43
    const/4 v5, 0x3

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 41
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 47
    :cond_3
    iget-object v1, p0, Lmqa;->b:Lmpm;

    if-eqz v1, :cond_4

    .line 48
    const/4 v1, 0x4

    iget-object v2, p0, Lmqa;->b:Lmpm;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 50
    :cond_4
    iget-object v1, p0, Lmqa;->e:[Lpcu;

    if-eqz v1, :cond_6

    .line 51
    iget-object v1, p0, Lmqa;->e:[Lpcu;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_6

    aget-object v3, v1, v0

    .line 52
    if-eqz v3, :cond_5

    .line 53
    const/4 v4, 0x5

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 51
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 57
    :cond_6
    iget-object v0, p0, Lmqa;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 59
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lmqa;->a(Loxn;)Lmqa;

    move-result-object v0

    return-object v0
.end method
