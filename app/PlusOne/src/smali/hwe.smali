.class public final Lhwe;
.super Landroid/view/ViewGroup;
.source "PG"

# interfaces
.implements Lljh;


# instance fields
.field private final a:Lhvx;

.field private b:Z

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/graphics/Rect;

.field private h:Z

.field private i:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 49
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 64
    invoke-virtual {p0}, Lhwe;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lhvx;->a(Landroid/content/Context;)Lhvx;

    move-result-object v0

    iput-object v0, p0, Lhwe;->a:Lhvx;

    .line 66
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lhwe;->i:Landroid/graphics/Rect;

    .line 67
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lhwe;->g:Landroid/graphics/Rect;

    .line 68
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhwe;->h:Z

    .line 69
    iput-boolean v1, p0, Lhwe;->b:Z

    .line 71
    invoke-virtual {p0, v1}, Lhwe;->setWillNotDraw(Z)V

    .line 50
    return-void
.end method

.method private a(Landroid/widget/TextView;IILjava/lang/String;)Landroid/widget/TextView;
    .locals 3

    .prologue
    .line 193
    if-nez p1, :cond_0

    .line 194
    invoke-virtual {p0}, Lhwe;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, v2, p2}, Llhx;->a(Landroid/content/Context;Landroid/util/AttributeSet;II)Landroid/widget/TextView;

    move-result-object p1

    .line 196
    invoke-virtual {p1, p3}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 198
    :cond_0
    invoke-virtual {p1, p4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 199
    invoke-virtual {p1, p4}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 200
    invoke-virtual {p0, p1}, Lhwe;->addView(Landroid/view/View;)V

    .line 201
    return-object p1
.end method

.method private a(Landroid/widget/TextView;)V
    .locals 1

    .prologue
    .line 205
    if-eqz p1, :cond_0

    .line 206
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 207
    const-string v0, ""

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 208
    invoke-virtual {p0, p1}, Lhwe;->removeView(Landroid/view/View;)V

    .line 210
    :cond_0
    return-void
.end method

.method private a(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 213
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-ne v0, p0, :cond_0

    .line 214
    const/4 v0, 0x1

    .line 216
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 225
    invoke-virtual {p0}, Lhwe;->b()V

    .line 226
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhwe;->b:Z

    .line 227
    return-void
.end method

.method public a(Lhur;)V
    .locals 4

    .prologue
    .line 93
    invoke-virtual {p0}, Lhwe;->b()V

    .line 95
    invoke-virtual {p1}, Lhur;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhwe;->c:Ljava/lang/String;

    .line 96
    invoke-virtual {p1}, Lhur;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhwe;->d:Ljava/lang/String;

    .line 98
    iget-object v0, p0, Lhwe;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lhwe;->e:Landroid/widget/TextView;

    const/16 v1, 0x27

    iget-object v2, p0, Lhwe;->a:Lhvx;

    iget v2, v2, Lhvx;->m:I

    iget-object v3, p0, Lhwe;->c:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2, v3}, Lhwe;->a(Landroid/widget/TextView;IILjava/lang/String;)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Lhwe;->e:Landroid/widget/TextView;

    :cond_0
    iget-object v0, p0, Lhwe;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lhwe;->f:Landroid/widget/TextView;

    const/16 v1, 0x24

    iget-object v2, p0, Lhwe;->a:Lhvx;

    iget v2, v2, Lhvx;->n:I

    iget-object v3, p0, Lhwe;->d:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2, v3}, Lhwe;->a(Landroid/widget/TextView;IILjava/lang/String;)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Lhwe;->f:Landroid/widget/TextView;

    .line 99
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhwe;->b:Z

    .line 100
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 173
    iget-boolean v0, p0, Lhwe;->h:Z

    if-eq v0, p1, :cond_0

    .line 174
    iput-boolean p1, p0, Lhwe;->h:Z

    .line 175
    invoke-virtual {p0}, Lhwe;->invalidate()V

    .line 177
    :cond_0
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 180
    iget-object v0, p0, Lhwe;->e:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lhwe;->a(Landroid/widget/TextView;)V

    .line 181
    iget-object v0, p0, Lhwe;->f:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lhwe;->a(Landroid/widget/TextView;)V

    .line 183
    iget-object v0, p0, Lhwe;->i:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 184
    iget-object v0, p0, Lhwe;->g:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 186
    iput-object v1, p0, Lhwe;->c:Ljava/lang/String;

    .line 187
    iput-object v1, p0, Lhwe;->d:Ljava/lang/String;

    .line 188
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhwe;->h:Z

    .line 189
    return-void
.end method

.method public c()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lhwe;->g:Landroid/graphics/Rect;

    return-object v0
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 146
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onDraw(Landroid/graphics/Canvas;)V

    .line 148
    iget-boolean v0, p0, Lhwe;->h:Z

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lhwe;->i:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lhwe;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lhwe;->getHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 150
    iget-object v0, p0, Lhwe;->a:Lhvx;

    iget-object v0, v0, Lhvx;->o:Landroid/graphics/drawable/NinePatchDrawable;

    iget-object v1, p0, Lhwe;->i:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 151
    iget-object v0, p0, Lhwe;->a:Lhvx;

    iget-object v0, v0, Lhvx;->o:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 153
    :cond_0
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 5

    .prologue
    .line 157
    iget-object v0, p0, Lhwe;->a:Lhvx;

    iget v1, v0, Lhvx;->d:I

    .line 158
    iget-object v0, p0, Lhwe;->a:Lhvx;

    iget v0, v0, Lhvx;->d:I

    mul-int/lit8 v0, v0, 0x2

    .line 160
    iget-object v2, p0, Lhwe;->e:Landroid/widget/TextView;

    invoke-direct {p0, v2}, Lhwe;->a(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 161
    iget-object v2, p0, Lhwe;->e:Landroid/widget/TextView;

    iget-object v3, p0, Lhwe;->e:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, v1

    iget-object v4, p0, Lhwe;->e:Landroid/widget/TextView;

    .line 162
    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v0

    .line 161
    invoke-virtual {v2, v1, v0, v3, v4}, Landroid/widget/TextView;->layout(IIII)V

    .line 163
    iget-object v2, p0, Lhwe;->e:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v0, v2

    .line 166
    :cond_0
    iget-object v2, p0, Lhwe;->f:Landroid/widget/TextView;

    invoke-direct {p0, v2}, Lhwe;->a(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 167
    iget-object v2, p0, Lhwe;->f:Landroid/widget/TextView;

    iget-object v3, p0, Lhwe;->f:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, v1

    iget-object v4, p0, Lhwe;->f:Landroid/widget/TextView;

    .line 168
    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v0

    .line 167
    invoke-virtual {v2, v1, v0, v3, v4}, Landroid/widget/TextView;->layout(IIII)V

    .line 170
    :cond_1
    return-void
.end method

.method public onMeasure(II)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 104
    iget-boolean v0, p0, Lhwe;->b:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x40

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " expected to have been bound with valid data. Was bind() called?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 106
    :cond_0
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    .line 107
    iget-object v0, p0, Lhwe;->a:Lhvx;

    iget v0, v0, Lhvx;->d:I

    mul-int/lit8 v2, v0, 0x2

    .line 109
    iget-object v0, p0, Lhwe;->a:Lhvx;

    iget v0, v0, Lhvx;->d:I

    mul-int/lit8 v0, v0, 0x2

    sub-int v5, v4, v0

    .line 113
    iget-object v0, p0, Lhwe;->e:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lhwe;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 114
    iget-object v0, p0, Lhwe;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1, p2}, Landroid/widget/TextView;->measure(II)V

    .line 115
    iget-object v0, p0, Lhwe;->e:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v0

    .line 116
    if-lez v0, :cond_3

    .line 119
    :goto_0
    iget-object v3, p0, Lhwe;->e:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getHeight()I

    move-result v3

    add-int/2addr v2, v3

    .line 122
    :goto_1
    iget-object v3, p0, Lhwe;->f:Landroid/widget/TextView;

    invoke-direct {p0, v3}, Lhwe;->a(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 123
    iget-object v3, p0, Lhwe;->f:Landroid/widget/TextView;

    invoke-virtual {v3, p1, p2}, Landroid/widget/TextView;->measure(II)V

    .line 124
    iget-object v3, p0, Lhwe;->f:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v3

    .line 125
    if-le v3, v0, :cond_1

    move v0, v3

    .line 128
    :cond_1
    iget-object v3, p0, Lhwe;->f:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getHeight()I

    move-result v3

    add-int/2addr v2, v3

    .line 133
    :cond_2
    iget-object v3, p0, Lhwe;->g:Landroid/graphics/Rect;

    iget-object v6, p0, Lhwe;->a:Lhvx;

    iget v6, v6, Lhvx;->d:I

    iput v6, v3, Landroid/graphics/Rect;->top:I

    .line 134
    iget-object v3, p0, Lhwe;->g:Landroid/graphics/Rect;

    iput v1, v3, Landroid/graphics/Rect;->left:I

    .line 135
    iget-object v1, p0, Lhwe;->g:Landroid/graphics/Rect;

    iget-object v3, p0, Lhwe;->g:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    invoke-static {v0, v5}, Ljava/lang/Math;->min(II)I

    move-result v0

    add-int/2addr v0, v3

    iget-object v3, p0, Lhwe;->a:Lhvx;

    iget v3, v3, Lhvx;->d:I

    mul-int/lit8 v3, v3, 0x2

    add-int/2addr v0, v3

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 138
    iget-object v0, p0, Lhwe;->a:Lhvx;

    iget v0, v0, Lhvx;->d:I

    add-int/2addr v0, v2

    .line 139
    iget-object v1, p0, Lhwe;->g:Landroid/graphics/Rect;

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 141
    invoke-virtual {p0, v4, v0}, Lhwe;->setMeasuredDimension(II)V

    .line 142
    return-void

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method
