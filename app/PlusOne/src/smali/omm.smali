.class public final Lomm;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Float;

.field private b:Lomo;

.field private c:Ljava/lang/Float;

.field private d:Ljava/lang/Float;

.field private e:[Lomo;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 499
    invoke-direct {p0}, Loxq;-><init>()V

    .line 504
    const/4 v0, 0x0

    iput-object v0, p0, Lomm;->b:Lomo;

    .line 511
    sget-object v0, Lomo;->a:[Lomo;

    iput-object v0, p0, Lomm;->e:[Lomo;

    .line 499
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 541
    .line 542
    iget-object v0, p0, Lomm;->a:Ljava/lang/Float;

    if-eqz v0, :cond_5

    .line 543
    const/4 v0, 0x1

    iget-object v2, p0, Lomm;->a:Ljava/lang/Float;

    .line 544
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x0

    .line 546
    :goto_0
    iget-object v2, p0, Lomm;->b:Lomo;

    if-eqz v2, :cond_0

    .line 547
    const/4 v2, 0x2

    iget-object v3, p0, Lomm;->b:Lomo;

    .line 548
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 550
    :cond_0
    iget-object v2, p0, Lomm;->c:Ljava/lang/Float;

    if-eqz v2, :cond_1

    .line 551
    const/4 v2, 0x3

    iget-object v3, p0, Lomm;->c:Ljava/lang/Float;

    .line 552
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 554
    :cond_1
    iget-object v2, p0, Lomm;->d:Ljava/lang/Float;

    if-eqz v2, :cond_2

    .line 555
    const/4 v2, 0x4

    iget-object v3, p0, Lomm;->d:Ljava/lang/Float;

    .line 556
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 558
    :cond_2
    iget-object v2, p0, Lomm;->e:[Lomo;

    if-eqz v2, :cond_4

    .line 559
    iget-object v2, p0, Lomm;->e:[Lomo;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 560
    if-eqz v4, :cond_3

    .line 561
    const/4 v5, 0x5

    .line 562
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 559
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 566
    :cond_4
    iget-object v1, p0, Lomm;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 567
    iput v0, p0, Lomm;->ai:I

    .line 568
    return v0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lomm;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 576
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 577
    sparse-switch v0, :sswitch_data_0

    .line 581
    iget-object v2, p0, Lomm;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 582
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lomm;->ah:Ljava/util/List;

    .line 585
    :cond_1
    iget-object v2, p0, Lomm;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 587
    :sswitch_0
    return-object p0

    .line 592
    :sswitch_1
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lomm;->a:Ljava/lang/Float;

    goto :goto_0

    .line 596
    :sswitch_2
    iget-object v0, p0, Lomm;->b:Lomo;

    if-nez v0, :cond_2

    .line 597
    new-instance v0, Lomo;

    invoke-direct {v0}, Lomo;-><init>()V

    iput-object v0, p0, Lomm;->b:Lomo;

    .line 599
    :cond_2
    iget-object v0, p0, Lomm;->b:Lomo;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 603
    :sswitch_3
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lomm;->c:Ljava/lang/Float;

    goto :goto_0

    .line 607
    :sswitch_4
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lomm;->d:Ljava/lang/Float;

    goto :goto_0

    .line 611
    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 612
    iget-object v0, p0, Lomm;->e:[Lomo;

    if-nez v0, :cond_4

    move v0, v1

    .line 613
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lomo;

    .line 614
    iget-object v3, p0, Lomm;->e:[Lomo;

    if-eqz v3, :cond_3

    .line 615
    iget-object v3, p0, Lomm;->e:[Lomo;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 617
    :cond_3
    iput-object v2, p0, Lomm;->e:[Lomo;

    .line 618
    :goto_2
    iget-object v2, p0, Lomm;->e:[Lomo;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 619
    iget-object v2, p0, Lomm;->e:[Lomo;

    new-instance v3, Lomo;

    invoke-direct {v3}, Lomo;-><init>()V

    aput-object v3, v2, v0

    .line 620
    iget-object v2, p0, Lomm;->e:[Lomo;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 621
    invoke-virtual {p1}, Loxn;->a()I

    .line 618
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 612
    :cond_4
    iget-object v0, p0, Lomm;->e:[Lomo;

    array-length v0, v0

    goto :goto_1

    .line 624
    :cond_5
    iget-object v2, p0, Lomm;->e:[Lomo;

    new-instance v3, Lomo;

    invoke-direct {v3}, Lomo;-><init>()V

    aput-object v3, v2, v0

    .line 625
    iget-object v2, p0, Lomm;->e:[Lomo;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 577
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x12 -> :sswitch_2
        0x1d -> :sswitch_3
        0x25 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 516
    iget-object v0, p0, Lomm;->a:Ljava/lang/Float;

    if-eqz v0, :cond_0

    .line 517
    const/4 v0, 0x1

    iget-object v1, p0, Lomm;->a:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 519
    :cond_0
    iget-object v0, p0, Lomm;->b:Lomo;

    if-eqz v0, :cond_1

    .line 520
    const/4 v0, 0x2

    iget-object v1, p0, Lomm;->b:Lomo;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 522
    :cond_1
    iget-object v0, p0, Lomm;->c:Ljava/lang/Float;

    if-eqz v0, :cond_2

    .line 523
    const/4 v0, 0x3

    iget-object v1, p0, Lomm;->c:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 525
    :cond_2
    iget-object v0, p0, Lomm;->d:Ljava/lang/Float;

    if-eqz v0, :cond_3

    .line 526
    const/4 v0, 0x4

    iget-object v1, p0, Lomm;->d:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 528
    :cond_3
    iget-object v0, p0, Lomm;->e:[Lomo;

    if-eqz v0, :cond_5

    .line 529
    iget-object v1, p0, Lomm;->e:[Lomo;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    .line 530
    if-eqz v3, :cond_4

    .line 531
    const/4 v4, 0x5

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 529
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 535
    :cond_5
    iget-object v0, p0, Lomm;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 537
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 495
    invoke-virtual {p0, p1}, Lomm;->a(Loxn;)Lomm;

    move-result-object v0

    return-object v0
.end method
