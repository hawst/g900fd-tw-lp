.class public final Lmvf;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmvf;


# instance fields
.field private b:Ljava/lang/Integer;

.field private c:Ljava/lang/Integer;

.field private d:[Ljava/lang/Long;

.field private e:[Lmud;

.field private f:[Lmue;

.field private g:Lmvs;

.field private h:[Ljava/lang/Integer;

.field private i:[Lmvm;

.field private j:Ljava/lang/String;

.field private k:Lmvx;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9748
    const/4 v0, 0x0

    new-array v0, v0, [Lmvf;

    sput-object v0, Lmvf;->a:[Lmvf;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 9749
    invoke-direct {p0}, Loxq;-><init>()V

    .line 9756
    sget-object v0, Loxx;->h:[Ljava/lang/Long;

    iput-object v0, p0, Lmvf;->d:[Ljava/lang/Long;

    .line 9759
    sget-object v0, Lmud;->a:[Lmud;

    iput-object v0, p0, Lmvf;->e:[Lmud;

    .line 9762
    sget-object v0, Lmue;->a:[Lmue;

    iput-object v0, p0, Lmvf;->f:[Lmue;

    .line 9765
    iput-object v1, p0, Lmvf;->g:Lmvs;

    .line 9768
    sget-object v0, Loxx;->g:[Ljava/lang/Integer;

    iput-object v0, p0, Lmvf;->h:[Ljava/lang/Integer;

    .line 9771
    sget-object v0, Lmvm;->a:[Lmvm;

    iput-object v0, p0, Lmvf;->i:[Lmvm;

    .line 9776
    iput-object v1, p0, Lmvf;->k:Lmvx;

    .line 9749
    return-void
.end method


# virtual methods
.method public a()I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 9833
    .line 9834
    iget-object v0, p0, Lmvf;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_e

    .line 9835
    const/4 v0, 0x1

    iget-object v2, p0, Lmvf;->b:Ljava/lang/Integer;

    .line 9836
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 9838
    :goto_0
    iget-object v2, p0, Lmvf;->c:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    .line 9839
    const/4 v2, 0x2

    iget-object v3, p0, Lmvf;->c:Ljava/lang/Integer;

    .line 9840
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 9842
    :cond_0
    iget-object v2, p0, Lmvf;->d:[Ljava/lang/Long;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lmvf;->d:[Ljava/lang/Long;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 9844
    iget-object v4, p0, Lmvf;->d:[Ljava/lang/Long;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_1
    if-ge v2, v5, :cond_1

    aget-object v6, v4, v2

    .line 9846
    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Loxo;->f(J)I

    move-result v6

    add-int/2addr v3, v6

    .line 9844
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 9848
    :cond_1
    add-int/2addr v0, v3

    .line 9849
    iget-object v2, p0, Lmvf;->d:[Ljava/lang/Long;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 9851
    :cond_2
    iget-object v2, p0, Lmvf;->e:[Lmud;

    if-eqz v2, :cond_4

    .line 9852
    iget-object v3, p0, Lmvf;->e:[Lmud;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_4

    aget-object v5, v3, v2

    .line 9853
    if-eqz v5, :cond_3

    .line 9854
    const/4 v6, 0x4

    .line 9855
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 9852
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 9859
    :cond_4
    iget-object v2, p0, Lmvf;->f:[Lmue;

    if-eqz v2, :cond_6

    .line 9860
    iget-object v3, p0, Lmvf;->f:[Lmue;

    array-length v4, v3

    move v2, v1

    :goto_3
    if-ge v2, v4, :cond_6

    aget-object v5, v3, v2

    .line 9861
    if-eqz v5, :cond_5

    .line 9862
    const/4 v6, 0x5

    .line 9863
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 9860
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 9867
    :cond_6
    iget-object v2, p0, Lmvf;->g:Lmvs;

    if-eqz v2, :cond_7

    .line 9868
    const/4 v2, 0x6

    iget-object v3, p0, Lmvf;->g:Lmvs;

    .line 9869
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 9871
    :cond_7
    iget-object v2, p0, Lmvf;->h:[Ljava/lang/Integer;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lmvf;->h:[Ljava/lang/Integer;

    array-length v2, v2

    if-lez v2, :cond_9

    .line 9873
    iget-object v4, p0, Lmvf;->h:[Ljava/lang/Integer;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_4
    if-ge v2, v5, :cond_8

    aget-object v6, v4, v2

    .line 9875
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-static {v6}, Loxo;->i(I)I

    move-result v6

    add-int/2addr v3, v6

    .line 9873
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 9877
    :cond_8
    add-int/2addr v0, v3

    .line 9878
    iget-object v2, p0, Lmvf;->h:[Ljava/lang/Integer;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 9880
    :cond_9
    iget-object v2, p0, Lmvf;->i:[Lmvm;

    if-eqz v2, :cond_b

    .line 9881
    iget-object v2, p0, Lmvf;->i:[Lmvm;

    array-length v3, v2

    :goto_5
    if-ge v1, v3, :cond_b

    aget-object v4, v2, v1

    .line 9882
    if-eqz v4, :cond_a

    .line 9883
    const/16 v5, 0x9

    .line 9884
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 9881
    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 9888
    :cond_b
    iget-object v1, p0, Lmvf;->j:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 9889
    const/16 v1, 0xa

    iget-object v2, p0, Lmvf;->j:Ljava/lang/String;

    .line 9890
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9892
    :cond_c
    iget-object v1, p0, Lmvf;->k:Lmvx;

    if-eqz v1, :cond_d

    .line 9893
    const/16 v1, 0xb

    iget-object v2, p0, Lmvf;->k:Lmvx;

    .line 9894
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9896
    :cond_d
    iget-object v1, p0, Lmvf;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9897
    iput v0, p0, Lmvf;->ai:I

    .line 9898
    return v0

    :cond_e
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lmvf;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 9906
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 9907
    sparse-switch v0, :sswitch_data_0

    .line 9911
    iget-object v2, p0, Lmvf;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 9912
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmvf;->ah:Ljava/util/List;

    .line 9915
    :cond_1
    iget-object v2, p0, Lmvf;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 9917
    :sswitch_0
    return-object p0

    .line 9922
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmvf;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 9926
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmvf;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 9930
    :sswitch_3
    const/16 v0, 0x18

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 9931
    iget-object v0, p0, Lmvf;->d:[Ljava/lang/Long;

    array-length v0, v0

    .line 9932
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/Long;

    .line 9933
    iget-object v3, p0, Lmvf;->d:[Ljava/lang/Long;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 9934
    iput-object v2, p0, Lmvf;->d:[Ljava/lang/Long;

    .line 9935
    :goto_1
    iget-object v2, p0, Lmvf;->d:[Ljava/lang/Long;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_2

    .line 9936
    iget-object v2, p0, Lmvf;->d:[Ljava/lang/Long;

    invoke-virtual {p1}, Loxn;->e()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v0

    .line 9937
    invoke-virtual {p1}, Loxn;->a()I

    .line 9935
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 9940
    :cond_2
    iget-object v2, p0, Lmvf;->d:[Ljava/lang/Long;

    invoke-virtual {p1}, Loxn;->e()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v0

    goto :goto_0

    .line 9944
    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 9945
    iget-object v0, p0, Lmvf;->e:[Lmud;

    if-nez v0, :cond_4

    move v0, v1

    .line 9946
    :goto_2
    add-int/2addr v2, v0

    new-array v2, v2, [Lmud;

    .line 9947
    iget-object v3, p0, Lmvf;->e:[Lmud;

    if-eqz v3, :cond_3

    .line 9948
    iget-object v3, p0, Lmvf;->e:[Lmud;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 9950
    :cond_3
    iput-object v2, p0, Lmvf;->e:[Lmud;

    .line 9951
    :goto_3
    iget-object v2, p0, Lmvf;->e:[Lmud;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 9952
    iget-object v2, p0, Lmvf;->e:[Lmud;

    new-instance v3, Lmud;

    invoke-direct {v3}, Lmud;-><init>()V

    aput-object v3, v2, v0

    .line 9953
    iget-object v2, p0, Lmvf;->e:[Lmud;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 9954
    invoke-virtual {p1}, Loxn;->a()I

    .line 9951
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 9945
    :cond_4
    iget-object v0, p0, Lmvf;->e:[Lmud;

    array-length v0, v0

    goto :goto_2

    .line 9957
    :cond_5
    iget-object v2, p0, Lmvf;->e:[Lmud;

    new-instance v3, Lmud;

    invoke-direct {v3}, Lmud;-><init>()V

    aput-object v3, v2, v0

    .line 9958
    iget-object v2, p0, Lmvf;->e:[Lmud;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 9962
    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 9963
    iget-object v0, p0, Lmvf;->f:[Lmue;

    if-nez v0, :cond_7

    move v0, v1

    .line 9964
    :goto_4
    add-int/2addr v2, v0

    new-array v2, v2, [Lmue;

    .line 9965
    iget-object v3, p0, Lmvf;->f:[Lmue;

    if-eqz v3, :cond_6

    .line 9966
    iget-object v3, p0, Lmvf;->f:[Lmue;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 9968
    :cond_6
    iput-object v2, p0, Lmvf;->f:[Lmue;

    .line 9969
    :goto_5
    iget-object v2, p0, Lmvf;->f:[Lmue;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_8

    .line 9970
    iget-object v2, p0, Lmvf;->f:[Lmue;

    new-instance v3, Lmue;

    invoke-direct {v3}, Lmue;-><init>()V

    aput-object v3, v2, v0

    .line 9971
    iget-object v2, p0, Lmvf;->f:[Lmue;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 9972
    invoke-virtual {p1}, Loxn;->a()I

    .line 9969
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 9963
    :cond_7
    iget-object v0, p0, Lmvf;->f:[Lmue;

    array-length v0, v0

    goto :goto_4

    .line 9975
    :cond_8
    iget-object v2, p0, Lmvf;->f:[Lmue;

    new-instance v3, Lmue;

    invoke-direct {v3}, Lmue;-><init>()V

    aput-object v3, v2, v0

    .line 9976
    iget-object v2, p0, Lmvf;->f:[Lmue;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 9980
    :sswitch_6
    iget-object v0, p0, Lmvf;->g:Lmvs;

    if-nez v0, :cond_9

    .line 9981
    new-instance v0, Lmvs;

    invoke-direct {v0}, Lmvs;-><init>()V

    iput-object v0, p0, Lmvf;->g:Lmvs;

    .line 9983
    :cond_9
    iget-object v0, p0, Lmvf;->g:Lmvs;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 9987
    :sswitch_7
    const/16 v0, 0x38

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 9988
    iget-object v0, p0, Lmvf;->h:[Ljava/lang/Integer;

    array-length v0, v0

    .line 9989
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/Integer;

    .line 9990
    iget-object v3, p0, Lmvf;->h:[Ljava/lang/Integer;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 9991
    iput-object v2, p0, Lmvf;->h:[Ljava/lang/Integer;

    .line 9992
    :goto_6
    iget-object v2, p0, Lmvf;->h:[Ljava/lang/Integer;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_a

    .line 9993
    iget-object v2, p0, Lmvf;->h:[Ljava/lang/Integer;

    invoke-virtual {p1}, Loxn;->g()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    .line 9994
    invoke-virtual {p1}, Loxn;->a()I

    .line 9992
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 9997
    :cond_a
    iget-object v2, p0, Lmvf;->h:[Ljava/lang/Integer;

    invoke-virtual {p1}, Loxn;->g()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    .line 10001
    :sswitch_8
    const/16 v0, 0x4a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 10002
    iget-object v0, p0, Lmvf;->i:[Lmvm;

    if-nez v0, :cond_c

    move v0, v1

    .line 10003
    :goto_7
    add-int/2addr v2, v0

    new-array v2, v2, [Lmvm;

    .line 10004
    iget-object v3, p0, Lmvf;->i:[Lmvm;

    if-eqz v3, :cond_b

    .line 10005
    iget-object v3, p0, Lmvf;->i:[Lmvm;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 10007
    :cond_b
    iput-object v2, p0, Lmvf;->i:[Lmvm;

    .line 10008
    :goto_8
    iget-object v2, p0, Lmvf;->i:[Lmvm;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_d

    .line 10009
    iget-object v2, p0, Lmvf;->i:[Lmvm;

    new-instance v3, Lmvm;

    invoke-direct {v3}, Lmvm;-><init>()V

    aput-object v3, v2, v0

    .line 10010
    iget-object v2, p0, Lmvf;->i:[Lmvm;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 10011
    invoke-virtual {p1}, Loxn;->a()I

    .line 10008
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 10002
    :cond_c
    iget-object v0, p0, Lmvf;->i:[Lmvm;

    array-length v0, v0

    goto :goto_7

    .line 10014
    :cond_d
    iget-object v2, p0, Lmvf;->i:[Lmvm;

    new-instance v3, Lmvm;

    invoke-direct {v3}, Lmvm;-><init>()V

    aput-object v3, v2, v0

    .line 10015
    iget-object v2, p0, Lmvf;->i:[Lmvm;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 10019
    :sswitch_9
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmvf;->j:Ljava/lang/String;

    goto/16 :goto_0

    .line 10023
    :sswitch_a
    iget-object v0, p0, Lmvf;->k:Lmvx;

    if-nez v0, :cond_e

    .line 10024
    new-instance v0, Lmvx;

    invoke-direct {v0}, Lmvx;-><init>()V

    iput-object v0, p0, Lmvf;->k:Lmvx;

    .line 10026
    :cond_e
    iget-object v0, p0, Lmvf;->k:Lmvx;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 9907
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x4a -> :sswitch_8
        0x52 -> :sswitch_9
        0x5a -> :sswitch_a
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 9781
    iget-object v1, p0, Lmvf;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 9782
    const/4 v1, 0x1

    iget-object v2, p0, Lmvf;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 9784
    :cond_0
    iget-object v1, p0, Lmvf;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 9785
    const/4 v1, 0x2

    iget-object v2, p0, Lmvf;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 9787
    :cond_1
    iget-object v1, p0, Lmvf;->d:[Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 9788
    iget-object v2, p0, Lmvf;->d:[Ljava/lang/Long;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 9789
    const/4 v5, 0x3

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {p1, v5, v6, v7}, Loxo;->a(IJ)V

    .line 9788
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 9792
    :cond_2
    iget-object v1, p0, Lmvf;->e:[Lmud;

    if-eqz v1, :cond_4

    .line 9793
    iget-object v2, p0, Lmvf;->e:[Lmud;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 9794
    if-eqz v4, :cond_3

    .line 9795
    const/4 v5, 0x4

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 9793
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 9799
    :cond_4
    iget-object v1, p0, Lmvf;->f:[Lmue;

    if-eqz v1, :cond_6

    .line 9800
    iget-object v2, p0, Lmvf;->f:[Lmue;

    array-length v3, v2

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    .line 9801
    if-eqz v4, :cond_5

    .line 9802
    const/4 v5, 0x5

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 9800
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 9806
    :cond_6
    iget-object v1, p0, Lmvf;->g:Lmvs;

    if-eqz v1, :cond_7

    .line 9807
    const/4 v1, 0x6

    iget-object v2, p0, Lmvf;->g:Lmvs;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 9809
    :cond_7
    iget-object v1, p0, Lmvf;->h:[Ljava/lang/Integer;

    if-eqz v1, :cond_8

    .line 9810
    iget-object v2, p0, Lmvf;->h:[Ljava/lang/Integer;

    array-length v3, v2

    move v1, v0

    :goto_3
    if-ge v1, v3, :cond_8

    aget-object v4, v2, v1

    .line 9811
    const/4 v5, 0x7

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {p1, v5, v4}, Loxo;->a(II)V

    .line 9810
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 9814
    :cond_8
    iget-object v1, p0, Lmvf;->i:[Lmvm;

    if-eqz v1, :cond_a

    .line 9815
    iget-object v1, p0, Lmvf;->i:[Lmvm;

    array-length v2, v1

    :goto_4
    if-ge v0, v2, :cond_a

    aget-object v3, v1, v0

    .line 9816
    if-eqz v3, :cond_9

    .line 9817
    const/16 v4, 0x9

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 9815
    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 9821
    :cond_a
    iget-object v0, p0, Lmvf;->j:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 9822
    const/16 v0, 0xa

    iget-object v1, p0, Lmvf;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 9824
    :cond_b
    iget-object v0, p0, Lmvf;->k:Lmvx;

    if-eqz v0, :cond_c

    .line 9825
    const/16 v0, 0xb

    iget-object v1, p0, Lmvf;->k:Lmvx;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 9827
    :cond_c
    iget-object v0, p0, Lmvf;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 9829
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 9745
    invoke-virtual {p0, p1}, Lmvf;->a(Loxn;)Lmvf;

    move-result-object v0

    return-object v0
.end method
