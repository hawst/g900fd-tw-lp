.class public final Lnbm;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnbm;


# instance fields
.field private b:I

.field private c:Lnbn;

.field private d:Lnbv;

.field private e:Lnbj;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1000
    const/4 v0, 0x0

    new-array v0, v0, [Lnbm;

    sput-object v0, Lnbm;->a:[Lnbm;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1001
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1011
    const/high16 v0, -0x80000000

    iput v0, p0, Lnbm;->b:I

    .line 1014
    iput-object v1, p0, Lnbm;->c:Lnbn;

    .line 1017
    iput-object v1, p0, Lnbm;->d:Lnbv;

    .line 1020
    iput-object v1, p0, Lnbm;->e:Lnbj;

    .line 1001
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1043
    const/4 v0, 0x0

    .line 1044
    iget v1, p0, Lnbm;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 1045
    const/4 v0, 0x1

    iget v1, p0, Lnbm;->b:I

    .line 1046
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1048
    :cond_0
    iget-object v1, p0, Lnbm;->c:Lnbn;

    if-eqz v1, :cond_1

    .line 1049
    const/4 v1, 0x2

    iget-object v2, p0, Lnbm;->c:Lnbn;

    .line 1050
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1052
    :cond_1
    iget-object v1, p0, Lnbm;->d:Lnbv;

    if-eqz v1, :cond_2

    .line 1053
    const/4 v1, 0x3

    iget-object v2, p0, Lnbm;->d:Lnbv;

    .line 1054
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1056
    :cond_2
    iget-object v1, p0, Lnbm;->e:Lnbj;

    if-eqz v1, :cond_3

    .line 1057
    const/4 v1, 0x4

    iget-object v2, p0, Lnbm;->e:Lnbj;

    .line 1058
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1060
    :cond_3
    iget-object v1, p0, Lnbm;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1061
    iput v0, p0, Lnbm;->ai:I

    .line 1062
    return v0
.end method

.method public a(Loxn;)Lnbm;
    .locals 2

    .prologue
    .line 1070
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1071
    sparse-switch v0, :sswitch_data_0

    .line 1075
    iget-object v1, p0, Lnbm;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1076
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnbm;->ah:Ljava/util/List;

    .line 1079
    :cond_1
    iget-object v1, p0, Lnbm;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1081
    :sswitch_0
    return-object p0

    .line 1086
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1087
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 1091
    :cond_2
    iput v0, p0, Lnbm;->b:I

    goto :goto_0

    .line 1093
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lnbm;->b:I

    goto :goto_0

    .line 1098
    :sswitch_2
    iget-object v0, p0, Lnbm;->c:Lnbn;

    if-nez v0, :cond_4

    .line 1099
    new-instance v0, Lnbn;

    invoke-direct {v0}, Lnbn;-><init>()V

    iput-object v0, p0, Lnbm;->c:Lnbn;

    .line 1101
    :cond_4
    iget-object v0, p0, Lnbm;->c:Lnbn;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1105
    :sswitch_3
    iget-object v0, p0, Lnbm;->d:Lnbv;

    if-nez v0, :cond_5

    .line 1106
    new-instance v0, Lnbv;

    invoke-direct {v0}, Lnbv;-><init>()V

    iput-object v0, p0, Lnbm;->d:Lnbv;

    .line 1108
    :cond_5
    iget-object v0, p0, Lnbm;->d:Lnbv;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1112
    :sswitch_4
    iget-object v0, p0, Lnbm;->e:Lnbj;

    if-nez v0, :cond_6

    .line 1113
    new-instance v0, Lnbj;

    invoke-direct {v0}, Lnbj;-><init>()V

    iput-object v0, p0, Lnbm;->e:Lnbj;

    .line 1115
    :cond_6
    iget-object v0, p0, Lnbm;->e:Lnbj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1071
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1025
    iget v0, p0, Lnbm;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 1026
    const/4 v0, 0x1

    iget v1, p0, Lnbm;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1028
    :cond_0
    iget-object v0, p0, Lnbm;->c:Lnbn;

    if-eqz v0, :cond_1

    .line 1029
    const/4 v0, 0x2

    iget-object v1, p0, Lnbm;->c:Lnbn;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1031
    :cond_1
    iget-object v0, p0, Lnbm;->d:Lnbv;

    if-eqz v0, :cond_2

    .line 1032
    const/4 v0, 0x3

    iget-object v1, p0, Lnbm;->d:Lnbv;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1034
    :cond_2
    iget-object v0, p0, Lnbm;->e:Lnbj;

    if-eqz v0, :cond_3

    .line 1035
    const/4 v0, 0x4

    iget-object v1, p0, Lnbm;->e:Lnbj;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1037
    :cond_3
    iget-object v0, p0, Lnbm;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1039
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 997
    invoke-virtual {p0, p1}, Lnbm;->a(Loxn;)Lnbm;

    move-result-object v0

    return-object v0
.end method
