.class public final Lgbh;
.super Landroid/widget/LinearLayout;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static a:Llct;


# instance fields
.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/TextView;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:I

.field private g:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, -0x1

    const/4 v6, -0x2

    const/4 v5, 0x0

    .line 45
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 37
    new-instance v0, Lgbi;

    invoke-direct {v0, p0}, Lgbi;-><init>(Lgbh;)V

    iput-object v0, p0, Lgbh;->g:Ljava/lang/Runnable;

    .line 46
    sget-object v0, Lgbh;->a:Llct;

    if-nez v0, :cond_0

    invoke-static {p1}, Llct;->a(Landroid/content/Context;)Llct;

    move-result-object v0

    sput-object v0, Lgbh;->a:Llct;

    :cond_0
    sget-object v0, Lgbh;->a:Llct;

    iget-object v0, v0, Llct;->al:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual {p0, v0}, Lgbh;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lgbh;->setOrientation(I)V

    sget-object v0, Lgbh;->a:Llct;

    iget v0, v0, Llct;->m:I

    sget-object v1, Lgbh;->a:Llct;

    iget v1, v1, Llct;->m:I

    sget-object v2, Lgbh;->a:Llct;

    iget v2, v2, Llct;->m:I

    sget-object v3, Lgbh;->a:Llct;

    iget v3, v3, Llct;->m:I

    invoke-virtual {p0, v0, v1, v2, v3}, Lgbh;->setPadding(IIII)V

    const/16 v0, 0x1f

    invoke-static {p1, v5, v8, v0}, Llhx;->a(Landroid/content/Context;Landroid/util/AttributeSet;II)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Lgbh;->b:Landroid/widget/TextView;

    iget-object v0, p0, Lgbh;->b:Landroid/widget/TextView;

    sget-object v1, Lgbh;->a:Llct;

    iget v1, v1, Llct;->aC:I

    sget-object v2, Lgbh;->a:Llct;

    iget v2, v2, Llct;->aC:I

    sget-object v3, Lgbh;->a:Llct;

    iget v3, v3, Llct;->aC:I

    sget-object v4, Lgbh;->a:Llct;

    iget v4, v4, Llct;->aC:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    iget-object v0, p0, Lgbh;->b:Landroid/widget/TextView;

    invoke-virtual {p0}, Lgbh;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0acf

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lgbh;->b:Landroid/widget/TextView;

    invoke-virtual {p0}, Lgbh;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02026f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v5, v5, v1, v5}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    const/16 v0, 0x25

    invoke-static {p1, v5, v8, v0}, Llhx;->a(Landroid/content/Context;Landroid/util/AttributeSet;II)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Lgbh;->c:Landroid/widget/TextView;

    iget-object v0, p0, Lgbh;->b:Landroid/widget/TextView;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v7, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Lgbh;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lgbh;->c:Landroid/widget/TextView;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v7, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Lgbh;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0, p0}, Lgbh;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 47
    return-void
.end method

.method static synthetic a(Lgbh;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x12c

    const/4 v3, 0x1

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 26
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lgbh;->setClickable(Z)V

    invoke-static {}, Llsj;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, v2}, Lgbh;->setAlpha(F)V

    invoke-virtual {p0}, Lgbh;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    sget-object v1, Lgbh;->a:Llct;

    iget-object v1, v1, Llct;->b:Landroid/view/animation/AccelerateInterpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-static {}, Llsj;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    :cond_0
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    :goto_0
    return-void

    :cond_1
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v2, v1}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    invoke-virtual {v0, v4, v5}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    sget-object v1, Lgbh;->a:Llct;

    iget-object v1, v1, Llct;->b:Landroid/view/animation/AccelerateInterpolator;

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    invoke-virtual {v0, v3}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    invoke-virtual {v0, v3}, Landroid/view/animation/AlphaAnimation;->setFillEnabled(Z)V

    invoke-virtual {p0, v0}, Lgbh;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method


# virtual methods
.method public a(ILjava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 89
    iput-object p2, p0, Lgbh;->d:Ljava/lang/String;

    .line 90
    iput-object p3, p0, Lgbh;->e:Ljava/lang/String;

    .line 91
    iput p1, p0, Lgbh;->f:I

    .line 93
    iget-object v1, p0, Lgbh;->c:Landroid/widget/TextView;

    invoke-virtual {p0}, Lgbh;->getContext()Landroid/content/Context;

    move-result-object v2

    const-class v0, Lhei;

    invoke-static {v2, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iget v3, p0, Lgbh;->f:I

    invoke-interface {v0, v3}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v3, "is_default_restricted"

    invoke-interface {v0, v3}, Lhej;->c(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    const v0, 0x7f0a0ad5

    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 95
    iget-object v0, p0, Lgbh;->g:Ljava/lang/Runnable;

    invoke-static {}, Llsx;->d()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 96
    iget-object v0, p0, Lgbh;->g:Ljava/lang/Runnable;

    const-wide/16 v2, 0x2710

    invoke-static {v0, v2, v3}, Llsx;->a(Ljava/lang/Runnable;J)V

    .line 97
    return-void

    .line 93
    :cond_0
    const-string v3, "is_child"

    invoke-interface {v0, v3}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f0a0ad4

    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const v0, 0x7f0a0ad3

    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 135
    invoke-virtual {p0}, Lgbh;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 136
    iget v1, p0, Lgbh;->f:I

    .line 137
    const-string v2, "plusone_posts"

    .line 138
    const-string v3, "https://support.google.com/plus/?hl=%locale%"

    invoke-static {v0, v2, v3}, Litk;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lgbh;->d:Ljava/lang/String;

    iget-object v4, p0, Lgbh;->e:Ljava/lang/String;

    .line 137
    invoke-static {v0, v1, v2, v3, v4}, Litm;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    return-void
.end method
