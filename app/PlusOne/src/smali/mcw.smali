.class public final Lmcw;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmov;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 30535
    invoke-direct {p0}, Loxq;-><init>()V

    .line 30538
    iput-object v0, p0, Lmcw;->apiHeader:Llyq;

    .line 30541
    iput-object v0, p0, Lmcw;->a:Lmov;

    .line 30535
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 30558
    const/4 v0, 0x0

    .line 30559
    iget-object v1, p0, Lmcw;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 30560
    const/4 v0, 0x1

    iget-object v1, p0, Lmcw;->apiHeader:Llyq;

    .line 30561
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 30563
    :cond_0
    iget-object v1, p0, Lmcw;->a:Lmov;

    if-eqz v1, :cond_1

    .line 30564
    const/4 v1, 0x2

    iget-object v2, p0, Lmcw;->a:Lmov;

    .line 30565
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 30567
    :cond_1
    iget-object v1, p0, Lmcw;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 30568
    iput v0, p0, Lmcw;->ai:I

    .line 30569
    return v0
.end method

.method public a(Loxn;)Lmcw;
    .locals 2

    .prologue
    .line 30577
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 30578
    sparse-switch v0, :sswitch_data_0

    .line 30582
    iget-object v1, p0, Lmcw;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 30583
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmcw;->ah:Ljava/util/List;

    .line 30586
    :cond_1
    iget-object v1, p0, Lmcw;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 30588
    :sswitch_0
    return-object p0

    .line 30593
    :sswitch_1
    iget-object v0, p0, Lmcw;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 30594
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmcw;->apiHeader:Llyq;

    .line 30596
    :cond_2
    iget-object v0, p0, Lmcw;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 30600
    :sswitch_2
    iget-object v0, p0, Lmcw;->a:Lmov;

    if-nez v0, :cond_3

    .line 30601
    new-instance v0, Lmov;

    invoke-direct {v0}, Lmov;-><init>()V

    iput-object v0, p0, Lmcw;->a:Lmov;

    .line 30603
    :cond_3
    iget-object v0, p0, Lmcw;->a:Lmov;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 30578
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 30546
    iget-object v0, p0, Lmcw;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 30547
    const/4 v0, 0x1

    iget-object v1, p0, Lmcw;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 30549
    :cond_0
    iget-object v0, p0, Lmcw;->a:Lmov;

    if-eqz v0, :cond_1

    .line 30550
    const/4 v0, 0x2

    iget-object v1, p0, Lmcw;->a:Lmov;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 30552
    :cond_1
    iget-object v0, p0, Lmcw;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 30554
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 30531
    invoke-virtual {p0, p1}, Lmcw;->a(Loxn;)Lmcw;

    move-result-object v0

    return-object v0
.end method
