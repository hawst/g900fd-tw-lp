.class public Llku;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Llkt;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 945
    const/4 v0, 0x3

    new-array v0, v0, [Llkt;

    const/4 v1, 0x0

    sget-object v2, Llkt;->b:Llkt;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Llkt;->c:Llkt;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Llkt;->d:Llkt;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Llku;-><init>(Ljava/util/List;)V

    .line 947
    return-void
.end method

.method protected constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Llkt;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 939
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 940
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 941
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Llku;->a:Ljava/util/List;

    .line 942
    return-void
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Llkt;",
            ">;"
        }
    .end annotation

    .prologue
    .line 955
    iget-object v0, p0, Llku;->a:Ljava/util/List;

    return-object v0
.end method

.method public a(Llkt;)Llkt;
    .locals 1

    .prologue
    .line 965
    iget-object v0, p1, Llkt;->f:Llkt;

    invoke-virtual {p0, v0}, Llku;->c(Llkt;)Llkt;

    move-result-object v0

    return-object v0
.end method

.method public b(Llkt;)Llkt;
    .locals 1

    .prologue
    .line 975
    iget-object v0, p1, Llkt;->e:Llkt;

    return-object v0
.end method

.method public c(Llkt;)Llkt;
    .locals 0

    .prologue
    .line 987
    return-object p1
.end method
