.class public final Lkyl;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Landroid/view/View;

.field public b:Landroid/view/View;

.field public c:Landroid/view/View;

.field public d:Lcom/google/android/libraries/social/media/ui/MediaView;

.field public e:Landroid/widget/TextView;

.field public f:Landroid/widget/TextView;

.field public g:Landroid/widget/ImageView;

.field public h:Landroid/widget/ImageButton;

.field public i:Landroid/widget/LinearLayout;

.field public j:Landroid/view/ViewGroup;

.field public k:Landroid/widget/TextView;

.field public l:Lcom/google/android/libraries/social/squares/membership/JoinButton;

.field public m:Lcom/google/android/libraries/social/squares/stream/RelatedLinksView;

.field public n:Landroid/widget/TextView;

.field public o:Landroid/widget/Button;

.field public p:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 3

    .prologue
    const v2, 0x7f020197

    .line 159
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 160
    const v0, 0x7f1001e6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lkyl;->a:Landroid/view/View;

    .line 161
    const v0, 0x7f1005b6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lkyl;->b:Landroid/view/View;

    .line 162
    const v0, 0x7f1005b4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lkyl;->c:Landroid/view/View;

    .line 164
    iget-object v0, p0, Lkyl;->a:Landroid/view/View;

    const v1, 0x7f1005bc

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    iput-object v0, p0, Lkyl;->d:Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 165
    iget-object v0, p0, Lkyl;->d:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->f(I)V

    .line 166
    iget-object v0, p0, Lkyl;->d:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->g(I)V

    .line 167
    iget-object v0, p0, Lkyl;->a:Landroid/view/View;

    const v1, 0x7f1005b3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lkyl;->e:Landroid/widget/TextView;

    .line 168
    iget-object v0, p0, Lkyl;->a:Landroid/view/View;

    const v1, 0x7f1005bf

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lkyl;->f:Landroid/widget/TextView;

    .line 170
    iget-object v0, p0, Lkyl;->b:Landroid/view/View;

    const v1, 0x7f1005b7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lkyl;->j:Landroid/view/ViewGroup;

    .line 171
    iget-object v0, p0, Lkyl;->b:Landroid/view/View;

    const v1, 0x7f100245

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lkyl;->k:Landroid/widget/TextView;

    .line 172
    iget-object v0, p0, Lkyl;->b:Landroid/view/View;

    const v1, 0x7f1005b9

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/squares/membership/JoinButton;

    iput-object v0, p0, Lkyl;->l:Lcom/google/android/libraries/social/squares/membership/JoinButton;

    .line 173
    iget-object v0, p0, Lkyl;->b:Landroid/view/View;

    const v1, 0x7f1005ba

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lkyl;->o:Landroid/widget/Button;

    .line 174
    iget-object v0, p0, Lkyl;->b:Landroid/view/View;

    const v1, 0x7f1005b8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/squares/stream/RelatedLinksView;

    iput-object v0, p0, Lkyl;->m:Lcom/google/android/libraries/social/squares/stream/RelatedLinksView;

    .line 175
    iget-object v0, p0, Lkyl;->b:Landroid/view/View;

    const v1, 0x7f1002b1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lkyl;->n:Landroid/widget/TextView;

    .line 176
    iget-object v0, p0, Lkyl;->b:Landroid/view/View;

    const v1, 0x7f1005bb

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lkyl;->p:Landroid/widget/TextView;

    .line 178
    iget-object v0, p0, Lkyl;->c:Landroid/view/View;

    const v1, 0x7f1005b5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lkyl;->i:Landroid/widget/LinearLayout;

    .line 180
    const v0, 0x7f1005c3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lkyl;->g:Landroid/widget/ImageView;

    .line 181
    const v0, 0x7f100406

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lkyl;->h:Landroid/widget/ImageButton;

    .line 183
    iget-object v0, p0, Lkyl;->h:Landroid/widget/ImageButton;

    new-instance v1, Lhmk;

    sget-object v2, Lomv;->z:Lhmn;

    invoke-direct {v1, v2}, Lhmk;-><init>(Lhmn;)V

    invoke-static {v0, v1}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    .line 185
    iget-object v0, p0, Lkyl;->o:Landroid/widget/Button;

    new-instance v1, Lhmk;

    sget-object v2, Lomv;->F:Lhmn;

    invoke-direct {v1, v2}, Lhmk;-><init>(Lhmn;)V

    invoke-static {v0, v1}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    .line 187
    return-void
.end method
