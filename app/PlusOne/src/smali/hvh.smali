.class public Lhvh;
.super Landroid/view/View;
.source "PG"

# interfaces
.implements Lljh;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static l:Lhwd;


# instance fields
.field public a:[Ljava/lang/String;

.field public b:[Landroid/graphics/Rect;

.field public c:[Landroid/graphics/Rect;

.field public d:I

.field public e:I

.field public f:J

.field public g:F

.field public h:I

.field public i:I

.field public j:Z

.field public k:Ljava/lang/Runnable;

.field private m:Lhua;

.field private n:[Landroid/graphics/Paint;

.field private o:Landroid/graphics/Rect;

.field private p:I

.field private q:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 128
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 54
    const/4 v0, -0x1

    iput v0, p0, Lhvh;->e:I

    .line 56
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lhvh;->g:F

    .line 60
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhvh;->j:Z

    .line 61
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhvh;->q:Z

    .line 63
    new-instance v0, Lhvi;

    invoke-direct {v0, p0}, Lhvi;-><init>(Lhvh;)V

    iput-object v0, p0, Lhvh;->k:Ljava/lang/Runnable;

    .line 129
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lhvh;->o:Landroid/graphics/Rect;

    .line 130
    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 311
    invoke-virtual {p0}, Lhvh;->e()V

    .line 312
    invoke-virtual {p0, v0}, Lhvh;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 314
    iput-object v0, p0, Lhvh;->m:Lhua;

    .line 316
    iput-object v0, p0, Lhvh;->a:[Ljava/lang/String;

    .line 317
    iput-object v0, p0, Lhvh;->n:[Landroid/graphics/Paint;

    .line 318
    iput-object v0, p0, Lhvh;->b:[Landroid/graphics/Rect;

    .line 319
    iput-object v0, p0, Lhvh;->c:[Landroid/graphics/Rect;

    .line 321
    iget-object v0, p0, Lhvh;->o:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 323
    iput v2, p0, Lhvh;->p:I

    .line 324
    iput v2, p0, Lhvh;->d:I

    .line 326
    const/4 v0, -0x1

    iput v0, p0, Lhvh;->e:I

    .line 327
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lhvh;->f:J

    .line 328
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lhvh;->g:F

    .line 329
    iput v2, p0, Lhvh;->h:I

    .line 330
    iput v2, p0, Lhvh;->i:I

    .line 331
    iput-boolean v2, p0, Lhvh;->q:Z

    .line 332
    return-void
.end method

.method public a(Lhua;ILandroid/view/View$OnClickListener;Z)V
    .locals 1

    .prologue
    .line 134
    sget-object v0, Lhvh;->l:Lhwd;

    if-nez v0, :cond_0

    .line 135
    invoke-virtual {p0}, Lhvh;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lhwd;->a(Landroid/content/Context;)Lhwd;

    move-result-object v0

    sput-object v0, Lhvh;->l:Lhwd;

    .line 138
    :cond_0
    invoke-virtual {p0}, Lhvh;->a()V

    .line 140
    invoke-virtual {p0, p3}, Lhvh;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 141
    iput-object p1, p0, Lhvh;->m:Lhua;

    .line 142
    iput p2, p0, Lhvh;->p:I

    .line 144
    iput-boolean p4, p0, Lhvh;->q:Z

    .line 145
    iget-boolean v0, p0, Lhvh;->q:Z

    if-eqz v0, :cond_1

    .line 146
    invoke-virtual {p0}, Lhvh;->d()V

    .line 148
    :cond_1
    invoke-virtual {p0}, Lhvh;->requestLayout()V

    .line 150
    invoke-static {p0}, Llhn;->a(Landroid/view/View;)V

    .line 151
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 356
    iput-boolean p1, p0, Lhvh;->q:Z

    .line 357
    iget-boolean v0, p0, Lhvh;->q:Z

    if-eqz v0, :cond_0

    .line 358
    invoke-virtual {p0}, Lhvh;->d()V

    .line 362
    :goto_0
    return-void

    .line 360
    :cond_0
    invoke-virtual {p0}, Lhvh;->e()V

    goto :goto_0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 342
    iget-object v0, p0, Lhvh;->a:[Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhvh;->a:[Ljava/lang/String;

    array-length v0, v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 346
    iget v0, p0, Lhvh;->h:I

    return v0
.end method

.method public d()V
    .locals 4

    .prologue
    .line 365
    iget-boolean v0, p0, Lhvh;->q:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lhvh;->j:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lhvh;->e:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 366
    invoke-static {p0}, Llii;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 367
    const/4 v0, 0x0

    iput v0, p0, Lhvh;->e:I

    .line 368
    invoke-static {}, Llsx;->d()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lhvh;->k:Ljava/lang/Runnable;

    const-wide/16 v2, 0x10

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 370
    :cond_0
    return-void
.end method

.method protected drawableStateChanged()V
    .locals 0

    .prologue
    .line 351
    invoke-virtual {p0}, Lhvh;->invalidate()V

    .line 352
    invoke-super {p0}, Landroid/view/View;->drawableStateChanged()V

    .line 353
    return-void
.end method

.method public e()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 373
    iget v0, p0, Lhvh;->e:I

    if-eq v0, v2, :cond_0

    .line 374
    invoke-static {}, Llsx;->d()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lhvh;->k:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 375
    iput v2, p0, Lhvh;->e:I

    .line 377
    :cond_0
    return-void
.end method

.method public getContentDescription()Ljava/lang/CharSequence;
    .locals 6
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "accessibility"
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 156
    invoke-static {}, Llsu;->a()Ljava/lang/StringBuilder;

    move-result-object v2

    .line 157
    iget-object v0, p0, Lhvh;->m:Lhua;

    invoke-interface {v0}, Lhua;->a()I

    move-result v3

    move v0, v1

    .line 158
    :goto_0
    if-ge v0, v3, :cond_0

    .line 159
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/CharSequence;

    iget-object v5, p0, Lhvh;->m:Lhua;

    invoke-interface {v5, v0}, Lhua;->a(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v2, v4}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 158
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 161
    :cond_0
    invoke-static {v2}, Llsu;->a(Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 0

    .prologue
    .line 166
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    .line 167
    invoke-virtual {p0}, Lhvh;->d()V

    .line 168
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 172
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 173
    invoke-virtual {p0}, Lhvh;->e()V

    .line 174
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/high16 v9, 0x437f0000    # 255.0f

    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 243
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 246
    invoke-virtual {p0}, Lhvh;->isFocused()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lhvh;->isPressed()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 247
    :cond_0
    sget-object v0, Lhvh;->l:Lhwd;

    iget-object v0, v0, Lhwd;->y:Landroid/graphics/drawable/NinePatchDrawable;

    .line 252
    :goto_0
    iget-object v2, p0, Lhvh;->o:Landroid/graphics/Rect;

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 253
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 256
    invoke-virtual {p0}, Lhvh;->getContext()Landroid/content/Context;

    move-result-object v0

    const/16 v2, 0xa

    invoke-static {v0, v2}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v2

    .line 258
    iget v0, p0, Lhvh;->e:I

    if-ne v0, v10, :cond_5

    .line 259
    iget v0, p0, Lhvh;->i:I

    iget v3, p0, Lhvh;->h:I

    if-ge v0, v3, :cond_4

    .line 260
    iget v0, p0, Lhvh;->g:F

    sget-object v3, Lhvh;->l:Lhwd;

    iget v3, v3, Lhwd;->j:I

    neg-int v3, v3

    int-to-float v3, v3

    .line 261
    invoke-virtual {v2}, Landroid/text/TextPaint;->ascent()F

    move-result v4

    add-float/2addr v3, v4

    mul-float/2addr v0, v3

    float-to-int v0, v0

    .line 270
    :goto_1
    iget-object v3, p0, Lhvh;->a:[Ljava/lang/String;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lhvh;->a:[Ljava/lang/String;

    array-length v3, v3

    if-lez v3, :cond_2

    .line 271
    invoke-virtual {v2}, Landroid/text/TextPaint;->getAlpha()I

    move-result v3

    .line 273
    iget v4, p0, Lhvh;->g:F

    mul-float/2addr v4, v9

    float-to-int v4, v4

    invoke-virtual {v2, v4}, Landroid/text/TextPaint;->setAlpha(I)V

    .line 274
    iget-object v4, p0, Lhvh;->a:[Ljava/lang/String;

    iget v5, p0, Lhvh;->h:I

    aget-object v4, v4, v5

    sget-object v5, Lhvh;->l:Lhwd;

    iget v5, v5, Lhwd;->i:I

    int-to-float v5, v5

    sget-object v6, Lhvh;->l:Lhwd;

    iget v6, v6, Lhwd;->j:I

    int-to-float v6, v6

    .line 275
    invoke-virtual {v2}, Landroid/text/TextPaint;->ascent()F

    move-result v7

    sub-float/2addr v6, v7

    .line 274
    invoke-virtual {p1, v4, v5, v6, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 277
    iget v4, p0, Lhvh;->e:I

    if-ne v4, v10, :cond_1

    .line 278
    iget v4, p0, Lhvh;->g:F

    mul-float/2addr v4, v9

    float-to-int v4, v4

    rsub-int v4, v4, 0xff

    invoke-virtual {v2, v4}, Landroid/text/TextPaint;->setAlpha(I)V

    .line 279
    iget-object v4, p0, Lhvh;->a:[Ljava/lang/String;

    iget v5, p0, Lhvh;->i:I

    aget-object v4, v4, v5

    sget-object v5, Lhvh;->l:Lhwd;

    iget v5, v5, Lhwd;->i:I

    int-to-float v5, v5

    sget-object v6, Lhvh;->l:Lhwd;

    iget v6, v6, Lhwd;->j:I

    int-to-float v6, v6

    .line 280
    invoke-virtual {v2}, Landroid/text/TextPaint;->ascent()F

    move-result v7

    sub-float/2addr v6, v7

    int-to-float v0, v0

    add-float/2addr v0, v6

    .line 279
    invoke-virtual {p1, v4, v5, v0, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 284
    :cond_1
    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setAlpha(I)V

    .line 287
    :cond_2
    iget-object v0, p0, Lhvh;->b:[Landroid/graphics/Rect;

    if-eqz v0, :cond_6

    .line 288
    iget-object v0, p0, Lhvh;->b:[Landroid/graphics/Rect;

    aget-object v0, v0, v1

    iget v0, v0, Landroid/graphics/Rect;->left:I

    .line 289
    iget-object v2, p0, Lhvh;->b:[Landroid/graphics/Rect;

    aget-object v2, v2, v1

    iget v2, v2, Landroid/graphics/Rect;->right:I

    .line 290
    :goto_2
    iget-object v3, p0, Lhvh;->b:[Landroid/graphics/Rect;

    array-length v3, v3

    if-ge v1, v3, :cond_6

    .line 291
    sget-object v3, Lhvh;->l:Lhwd;

    iget-object v3, v3, Lhwd;->l:Landroid/graphics/Rect;

    .line 292
    iput v0, v3, Landroid/graphics/Rect;->left:I

    .line 293
    iput v2, v3, Landroid/graphics/Rect;->right:I

    .line 295
    iget-object v4, p0, Lhvh;->b:[Landroid/graphics/Rect;

    aget-object v4, v4, v1

    iget v4, v4, Landroid/graphics/Rect;->top:I

    .line 296
    iget-object v5, p0, Lhvh;->c:[Landroid/graphics/Rect;

    aget-object v5, v5, v1

    iget v5, v5, Landroid/graphics/Rect;->top:I

    .line 297
    iget v6, p0, Lhvh;->g:F

    int-to-float v4, v4

    mul-float/2addr v4, v6

    iget v6, p0, Lhvh;->g:F

    sub-float v6, v8, v6

    int-to-float v5, v5

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    float-to-int v4, v4

    iput v4, v3, Landroid/graphics/Rect;->top:I

    .line 300
    iget-object v4, p0, Lhvh;->b:[Landroid/graphics/Rect;

    aget-object v4, v4, v1

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    .line 301
    iget-object v5, p0, Lhvh;->c:[Landroid/graphics/Rect;

    aget-object v5, v5, v1

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    .line 302
    iget v6, p0, Lhvh;->g:F

    int-to-float v4, v4

    mul-float/2addr v4, v6

    iget v6, p0, Lhvh;->g:F

    sub-float v6, v8, v6

    int-to-float v5, v5

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    float-to-int v4, v4

    iput v4, v3, Landroid/graphics/Rect;->bottom:I

    .line 304
    iget-object v4, p0, Lhvh;->n:[Landroid/graphics/Paint;

    aget-object v4, v4, v1

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 290
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 249
    :cond_3
    sget-object v0, Lhvh;->l:Lhwd;

    iget-object v0, v0, Lhwd;->x:Landroid/graphics/drawable/NinePatchDrawable;

    goto/16 :goto_0

    .line 263
    :cond_4
    iget v0, p0, Lhvh;->g:F

    sget-object v3, Lhvh;->l:Lhwd;

    iget v3, v3, Lhwd;->j:I

    int-to-float v3, v3

    .line 264
    invoke-virtual {v2}, Landroid/text/TextPaint;->descent()F

    move-result v4

    add-float/2addr v3, v4

    mul-float/2addr v0, v3

    float-to-int v0, v0

    goto/16 :goto_1

    :cond_5
    move v0, v1

    .line 267
    goto/16 :goto_1

    .line 307
    :cond_6
    return-void
.end method

.method protected onMeasure(II)V
    .locals 15

    .prologue
    .line 178
    iget-object v0, p0, Lhvh;->m:Lhua;

    invoke-interface {v0}, Lhua;->a()I

    move-result v6

    .line 180
    invoke-virtual {p0}, Lhvh;->getContext()Landroid/content/Context;

    move-result-object v0

    const/16 v1, 0xa

    invoke-static {v0, v1}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v7

    .line 182
    const/high16 v1, -0x80000000

    .line 183
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v6, :cond_0

    .line 184
    iget-object v2, p0, Lhvh;->m:Lhua;

    .line 185
    invoke-interface {v2, v0}, Lhua;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v7, v2}, Llib;->a(Landroid/text/TextPaint;Ljava/lang/String;)I

    move-result v2

    .line 184
    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 183
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 187
    :cond_0
    sget-object v0, Lhvh;->l:Lhwd;

    iget v0, v0, Lhwd;->i:I

    mul-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v1

    .line 189
    iget v1, p0, Lhvh;->p:I

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v8

    .line 190
    const/4 v2, 0x0

    .line 191
    const/4 v4, 0x0

    .line 192
    const/4 v3, 0x0

    .line 194
    invoke-static {v7}, Llib;->a(Landroid/text/TextPaint;)I

    move-result v0

    sget-object v1, Lhvh;->l:Lhwd;

    iget v1, v1, Lhwd;->j:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iput v0, p0, Lhvh;->d:I

    .line 198
    new-array v9, v6, [Ljava/lang/String;

    .line 199
    new-array v10, v6, [I

    .line 200
    const/4 v1, 0x0

    .line 201
    const/4 v0, 0x0

    move v5, v0

    :goto_1
    if-ge v5, v6, :cond_3

    .line 202
    iget-object v0, p0, Lhvh;->m:Lhua;

    invoke-interface {v0, v5}, Lhua;->a(I)Ljava/lang/String;

    move-result-object v11

    .line 203
    sget-object v0, Lhvh;->l:Lhwd;

    iget v0, v0, Lhwd;->i:I

    mul-int/lit8 v0, v0, 0x2

    sub-int v0, v8, v0

    sget-object v12, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    const/4 v13, 0x0

    invoke-static {v11, v7, v0, v12, v13}, Llib;->a(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/TextUtils$TruncateAt;Landroid/text/TextUtils$EllipsizeCallback;)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 209
    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/lit8 v11, v11, -0x1

    int-to-float v11, v11

    const/high16 v12, 0x3f000000    # 0.5f

    mul-float/2addr v11, v12

    float-to-double v12, v11

    invoke-static {v12, v13}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v12

    double-to-int v11, v12

    add-int/lit8 v11, v11, 0x2

    .line 210
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v12

    if-nez v12, :cond_1

    const-string v12, "\u2026"

    invoke-virtual {v0, v12}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 211
    :cond_1
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v12

    if-lt v12, v11, :cond_8

    .line 212
    :cond_2
    aput-object v0, v9, v1

    .line 213
    aput v5, v10, v1

    .line 214
    add-int/lit8 v0, v1, 0x1

    .line 201
    :goto_2
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    move v1, v0

    goto :goto_1

    .line 218
    :cond_3
    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lhvh;->a:[Ljava/lang/String;

    .line 219
    new-array v0, v1, [Landroid/graphics/Paint;

    iput-object v0, p0, Lhvh;->n:[Landroid/graphics/Paint;

    .line 220
    new-array v0, v1, [Landroid/graphics/Rect;

    iput-object v0, p0, Lhvh;->b:[Landroid/graphics/Rect;

    .line 221
    new-array v0, v1, [Landroid/graphics/Rect;

    iput-object v0, p0, Lhvh;->c:[Landroid/graphics/Rect;

    .line 222
    const/4 v0, 0x0

    move v5, v0

    move v6, v2

    :goto_3
    if-ge v5, v1, :cond_6

    .line 223
    iget-object v0, p0, Lhvh;->a:[Ljava/lang/String;

    aget-object v2, v9, v5

    aput-object v2, v0, v5

    .line 225
    iget-object v7, p0, Lhvh;->n:[Landroid/graphics/Paint;

    iget-object v0, p0, Lhvh;->m:Lhua;

    aget v2, v10, v5

    invoke-interface {v0, v2}, Lhua;->b(I)Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Lhvh;->l:Lhwd;

    iget-object v0, v0, Lhwd;->v:[Landroid/graphics/Paint;

    add-int/lit8 v2, v3, 0x1

    aget-object v0, v0, v3

    move v3, v4

    :goto_4
    aput-object v0, v7, v5

    .line 229
    iget-object v4, p0, Lhvh;->b:[Landroid/graphics/Rect;

    new-instance v7, Landroid/graphics/Rect;

    sget-object v0, Lhvh;->l:Lhwd;

    iget v0, v0, Lhwd;->z:I

    sub-int v11, v8, v0

    if-nez v5, :cond_5

    iget v0, p0, Lhvh;->d:I

    :goto_5
    add-int/2addr v0, v6

    invoke-direct {v7, v11, v6, v8, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v7, v4, v5

    .line 231
    iget-object v0, p0, Lhvh;->c:[Landroid/graphics/Rect;

    new-instance v4, Landroid/graphics/Rect;

    iget-object v6, p0, Lhvh;->b:[Landroid/graphics/Rect;

    aget-object v6, v6, v5

    invoke-direct {v4, v6}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    aput-object v4, v0, v5

    .line 232
    iget-object v0, p0, Lhvh;->b:[Landroid/graphics/Rect;

    aget-object v0, v0, v5

    iget v4, v0, Landroid/graphics/Rect;->bottom:I

    .line 222
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    move v6, v4

    move v4, v3

    move v3, v2

    goto :goto_3

    .line 225
    :cond_4
    sget-object v0, Lhvh;->l:Lhwd;

    iget-object v0, v0, Lhwd;->w:[Landroid/graphics/Paint;

    add-int/lit8 v2, v4, 0x1

    aget-object v0, v0, v4

    move v14, v3

    move v3, v2

    move v2, v14

    goto :goto_4

    .line 229
    :cond_5
    iget v0, p0, Lhvh;->d:I

    div-int/lit8 v0, v0, 0x4

    goto :goto_5

    .line 235
    :cond_6
    iget-object v0, p0, Lhvh;->o:Landroid/graphics/Rect;

    const/4 v2, 0x0

    const/4 v3, 0x0

    iget v4, p0, Lhvh;->d:I

    invoke-virtual {v0, v2, v3, v8, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 236
    if-lez v1, :cond_7

    iget-object v0, p0, Lhvh;->b:[Landroid/graphics/Rect;

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    .line 238
    :goto_6
    invoke-virtual {p0, v8, v0}, Lhvh;->setMeasuredDimension(II)V

    .line 239
    return-void

    .line 236
    :cond_7
    const/4 v0, 0x0

    goto :goto_6

    :cond_8
    move v0, v1

    goto/16 :goto_2
.end method
