.class public final Lpkl;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lpkl;


# instance fields
.field private b:Lppf;

.field private c:Lppf;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 502
    const/4 v0, 0x0

    new-array v0, v0, [Lpkl;

    sput-object v0, Lpkl;->a:[Lpkl;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 503
    invoke-direct {p0}, Loxq;-><init>()V

    .line 506
    iput-object v0, p0, Lpkl;->b:Lppf;

    .line 509
    iput-object v0, p0, Lpkl;->c:Lppf;

    .line 503
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 526
    const/4 v0, 0x0

    .line 527
    iget-object v1, p0, Lpkl;->b:Lppf;

    if-eqz v1, :cond_0

    .line 528
    const/4 v0, 0x1

    iget-object v1, p0, Lpkl;->b:Lppf;

    .line 529
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 531
    :cond_0
    iget-object v1, p0, Lpkl;->c:Lppf;

    if-eqz v1, :cond_1

    .line 532
    const/4 v1, 0x2

    iget-object v2, p0, Lpkl;->c:Lppf;

    .line 533
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 535
    :cond_1
    iget-object v1, p0, Lpkl;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 536
    iput v0, p0, Lpkl;->ai:I

    .line 537
    return v0
.end method

.method public a(Loxn;)Lpkl;
    .locals 2

    .prologue
    .line 545
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 546
    sparse-switch v0, :sswitch_data_0

    .line 550
    iget-object v1, p0, Lpkl;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 551
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpkl;->ah:Ljava/util/List;

    .line 554
    :cond_1
    iget-object v1, p0, Lpkl;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 556
    :sswitch_0
    return-object p0

    .line 561
    :sswitch_1
    iget-object v0, p0, Lpkl;->b:Lppf;

    if-nez v0, :cond_2

    .line 562
    new-instance v0, Lppf;

    invoke-direct {v0}, Lppf;-><init>()V

    iput-object v0, p0, Lpkl;->b:Lppf;

    .line 564
    :cond_2
    iget-object v0, p0, Lpkl;->b:Lppf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 568
    :sswitch_2
    iget-object v0, p0, Lpkl;->c:Lppf;

    if-nez v0, :cond_3

    .line 569
    new-instance v0, Lppf;

    invoke-direct {v0}, Lppf;-><init>()V

    iput-object v0, p0, Lpkl;->c:Lppf;

    .line 571
    :cond_3
    iget-object v0, p0, Lpkl;->c:Lppf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 546
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 514
    iget-object v0, p0, Lpkl;->b:Lppf;

    if-eqz v0, :cond_0

    .line 515
    const/4 v0, 0x1

    iget-object v1, p0, Lpkl;->b:Lppf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 517
    :cond_0
    iget-object v0, p0, Lpkl;->c:Lppf;

    if-eqz v0, :cond_1

    .line 518
    const/4 v0, 0x2

    iget-object v1, p0, Lpkl;->c:Lppf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 520
    :cond_1
    iget-object v0, p0, Lpkl;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 522
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 499
    invoke-virtual {p0, p1}, Lpkl;->a(Loxn;)Lpkl;

    move-result-object v0

    return-object v0
.end method
