.class public final Loep;
.super Loxq;
.source "PG"


# instance fields
.field private a:[Loeu;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 93
    invoke-direct {p0}, Loxq;-><init>()V

    .line 96
    sget-object v0, Loeu;->a:[Loeu;

    iput-object v0, p0, Loep;->a:[Loeu;

    .line 93
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 114
    .line 115
    iget-object v1, p0, Loep;->a:[Loeu;

    if-eqz v1, :cond_1

    .line 116
    iget-object v2, p0, Loep;->a:[Loeu;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 117
    if-eqz v4, :cond_0

    .line 118
    const/4 v5, 0x1

    .line 119
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 116
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 123
    :cond_1
    iget-object v1, p0, Loep;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 124
    iput v0, p0, Loep;->ai:I

    .line 125
    return v0
.end method

.method public a(Loxn;)Loep;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 133
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 134
    sparse-switch v0, :sswitch_data_0

    .line 138
    iget-object v2, p0, Loep;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 139
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Loep;->ah:Ljava/util/List;

    .line 142
    :cond_1
    iget-object v2, p0, Loep;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 144
    :sswitch_0
    return-object p0

    .line 149
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 150
    iget-object v0, p0, Loep;->a:[Loeu;

    if-nez v0, :cond_3

    move v0, v1

    .line 151
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Loeu;

    .line 152
    iget-object v3, p0, Loep;->a:[Loeu;

    if-eqz v3, :cond_2

    .line 153
    iget-object v3, p0, Loep;->a:[Loeu;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 155
    :cond_2
    iput-object v2, p0, Loep;->a:[Loeu;

    .line 156
    :goto_2
    iget-object v2, p0, Loep;->a:[Loeu;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 157
    iget-object v2, p0, Loep;->a:[Loeu;

    new-instance v3, Loeu;

    invoke-direct {v3}, Loeu;-><init>()V

    aput-object v3, v2, v0

    .line 158
    iget-object v2, p0, Loep;->a:[Loeu;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 159
    invoke-virtual {p1}, Loxn;->a()I

    .line 156
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 150
    :cond_3
    iget-object v0, p0, Loep;->a:[Loeu;

    array-length v0, v0

    goto :goto_1

    .line 162
    :cond_4
    iget-object v2, p0, Loep;->a:[Loeu;

    new-instance v3, Loeu;

    invoke-direct {v3}, Loeu;-><init>()V

    aput-object v3, v2, v0

    .line 163
    iget-object v2, p0, Loep;->a:[Loeu;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 134
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 101
    iget-object v0, p0, Loep;->a:[Loeu;

    if-eqz v0, :cond_1

    .line 102
    iget-object v1, p0, Loep;->a:[Loeu;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 103
    if-eqz v3, :cond_0

    .line 104
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 102
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 108
    :cond_1
    iget-object v0, p0, Loep;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 110
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 89
    invoke-virtual {p0, p1}, Loep;->a(Loxn;)Loep;

    move-result-object v0

    return-object v0
.end method
