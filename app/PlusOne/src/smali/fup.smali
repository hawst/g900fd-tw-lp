.class public final Lfup;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Litj;


# instance fields
.field private a:I

.field private b:Lu;

.field private final c:Landroid/content/Context;

.field private d:Ljava/lang/String;

.field private final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lu;Landroid/content/Context;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput p3, p0, Lfup;->a:I

    .line 57
    iput-object p1, p0, Lfup;->b:Lu;

    .line 58
    iput-object p2, p0, Lfup;->c:Landroid/content/Context;

    .line 59
    iput-object p4, p0, Lfup;->e:Ljava/lang/String;

    .line 60
    return-void
.end method

.method static synthetic a(Lfup;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lfup;->c:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 66
    iput-object p2, p0, Lfup;->d:Ljava/lang/String;

    .line 67
    const-string v0, "extra_creation_source_id"

    iget-object v1, p0, Lfup;->d:Ljava/lang/String;

    .line 68
    invoke-static {v0, v1}, Lhmt;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v7

    .line 70
    iget-object v0, p0, Lfup;->c:Landroid/content/Context;

    const-class v1, Lhee;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v2

    .line 71
    iget-object v0, p0, Lfup;->c:Landroid/content/Context;

    invoke-static {v0, p3}, Leyq;->a(Landroid/content/Context;Ljava/util/List;)Ljava/lang/String;

    move-result-object v3

    .line 72
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 74
    iget-object v0, p0, Lfup;->c:Landroid/content/Context;

    invoke-static {v0, v3, p4, p7}, Leyq;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v4

    .line 76
    if-eqz v4, :cond_2

    .line 78
    :try_start_0
    iget-object v0, p0, Lfup;->b:Lu;

    invoke-virtual {v0, v4}, Lu;->a(Landroid/content/Intent;)V

    .line 79
    iget-object v0, p0, Lfup;->c:Landroid/content/Context;

    const-class v1, Lhms;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v5, Lhmr;

    iget-object v1, p0, Lfup;->c:Landroid/content/Context;

    invoke-direct {v5, v1, v2}, Lhmr;-><init>(Landroid/content/Context;I)V

    if-eqz p7, :cond_1

    sget-object v1, Lhmv;->bp:Lhmv;

    .line 81
    :goto_0
    invoke-virtual {v5, v1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 84
    invoke-virtual {v1, v7}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    .line 79
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 155
    :cond_0
    :goto_1
    return-void

    .line 79
    :cond_1
    sget-object v1, Lhmv;->bi:Lhmv;
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 88
    :catch_0
    move-exception v0

    iget-object v0, p0, Lfup;->e:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 89
    iget-object v0, p0, Lfup;->e:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x2a

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Failed to start deep linked Activity with "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 127
    :cond_2
    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 128
    iget-object v0, p0, Lfup;->c:Landroid/content/Context;

    iget-object v1, p0, Lfup;->d:Ljava/lang/String;

    invoke-static {v0, v2, p5, v1, p1}, Litm;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 133
    iget-object v0, p0, Lfup;->c:Landroid/content/Context;

    const-class v1, Lhms;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v3, Lhmr;

    iget-object v1, p0, Lfup;->c:Landroid/content/Context;

    invoke-direct {v3, v1, v2}, Lhmr;-><init>(Landroid/content/Context;I)V

    if-eqz p7, :cond_6

    sget-object v1, Lhmv;->bq:Lhmv;

    .line 135
    :goto_2
    invoke-virtual {v3, v1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 138
    invoke-virtual {v1, v7}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    .line 133
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    goto :goto_1

    .line 95
    :cond_3
    invoke-static {p3}, Leyq;->a(Ljava/util/List;)Landroid/content/Intent;

    move-result-object v1

    .line 96
    iget-object v3, p0, Lfup;->b:Lu;

    iget v4, p0, Lfup;->a:I

    if-eqz p7, :cond_4

    const/4 v0, 0x2

    :goto_3
    add-int/2addr v0, v4

    invoke-virtual {v3, v1, v0}, Lu;->a(Landroid/content/Intent;I)V

    .line 101
    invoke-static {p3}, Leyq;->b(Ljava/util/List;)Ljava/lang/String;

    move-result-object v3

    .line 103
    new-instance v0, Lfuq;

    move-object v1, p0

    move-object v4, p1

    move-object v5, p6

    move v6, p7

    invoke-direct/range {v0 .. v6}, Lfuq;-><init>(Lfup;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    .line 113
    invoke-virtual {v0, v1}, Lfuq;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 115
    iget-object v0, p0, Lfup;->c:Landroid/content/Context;

    const-class v1, Lhms;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v3, Lhmr;

    iget-object v1, p0, Lfup;->c:Landroid/content/Context;

    invoke-direct {v3, v1, v2}, Lhmr;-><init>(Landroid/content/Context;I)V

    if-eqz p7, :cond_5

    sget-object v1, Lhmv;->bn:Lhmv;

    .line 117
    :goto_4
    invoke-virtual {v3, v1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 120
    invoke-virtual {v1, v7}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    .line 115
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    goto/16 :goto_1

    .line 96
    :cond_4
    const/4 v0, 0x1

    goto :goto_3

    .line 115
    :cond_5
    sget-object v1, Lhmv;->bg:Lhmv;

    goto :goto_4

    .line 133
    :cond_6
    sget-object v1, Lhmv;->bj:Lhmv;

    goto :goto_2

    .line 143
    :cond_7
    iget-object v0, p0, Lfup;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 144
    invoke-static {v0}, Lfuz;->a(Landroid/content/pm/PackageManager;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 145
    iget-object v0, p0, Lfup;->c:Landroid/content/Context;

    const-class v1, Lhms;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v3, Lhmr;

    iget-object v1, p0, Lfup;->c:Landroid/content/Context;

    invoke-direct {v3, v1, v2}, Lhmr;-><init>(Landroid/content/Context;I)V

    if-eqz p7, :cond_8

    sget-object v1, Lhmv;->bo:Lhmv;

    .line 147
    :goto_5
    invoke-virtual {v3, v1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 150
    invoke-virtual {v1, v7}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    .line 145
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    goto/16 :goto_1

    :cond_8
    sget-object v1, Lhmv;->bh:Lhmv;

    goto :goto_5
.end method

.method public a(II)Z
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 159
    iget v0, p0, Lfup;->a:I

    sub-int v3, p1, v0

    .line 160
    if-eq v3, v1, :cond_0

    if-ne v3, v6, :cond_2

    .line 162
    :cond_0
    const-string v0, "extra_creation_source_id"

    iget-object v4, p0, Lfup;->d:Ljava/lang/String;

    .line 163
    invoke-static {v0, v4}, Lhmt;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v4

    .line 165
    iget-object v0, p0, Lfup;->c:Landroid/content/Context;

    const-class v5, Lhee;

    invoke-static {v0, v5}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v5

    .line 166
    if-ne v3, v6, :cond_3

    move v0, v1

    .line 167
    :goto_0
    iget-object v2, p0, Lfup;->b:Lu;

    invoke-virtual {v2}, Lu;->n()Lz;

    move-result-object v2

    .line 168
    if-eqz v2, :cond_1

    .line 170
    const/4 v2, -0x1

    if-ne p2, v2, :cond_5

    .line 171
    if-eqz v0, :cond_4

    sget-object v0, Lhmv;->br:Lhmv;

    :goto_1
    move-object v2, v0

    .line 179
    :goto_2
    iget-object v0, p0, Lfup;->c:Landroid/content/Context;

    const-class v3, Lhms;

    invoke-static {v0, v3}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v3, Lhmr;

    iget-object v6, p0, Lfup;->c:Landroid/content/Context;

    invoke-direct {v3, v6, v5}, Lhmr;-><init>(Landroid/content/Context;I)V

    .line 181
    invoke-virtual {v3, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v2

    .line 182
    invoke-virtual {v2, v4}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v2

    .line 179
    invoke-interface {v0, v2}, Lhms;->a(Lhmr;)V

    :cond_1
    move v2, v1

    .line 187
    :cond_2
    return v2

    :cond_3
    move v0, v2

    .line 166
    goto :goto_0

    .line 171
    :cond_4
    sget-object v0, Lhmv;->bk:Lhmv;

    goto :goto_1

    .line 175
    :cond_5
    if-eqz v0, :cond_6

    sget-object v0, Lhmv;->bs:Lhmv;

    :goto_3
    move-object v2, v0

    goto :goto_2

    :cond_6
    sget-object v0, Lhmv;->bl:Lhmv;

    goto :goto_3
.end method
