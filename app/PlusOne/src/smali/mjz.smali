.class public final Lmjz;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lnqo;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 33856
    invoke-direct {p0}, Loxq;-><init>()V

    .line 33859
    iput-object v0, p0, Lmjz;->apiHeader:Llyr;

    .line 33862
    iput-object v0, p0, Lmjz;->a:Lnqo;

    .line 33856
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 33879
    const/4 v0, 0x0

    .line 33880
    iget-object v1, p0, Lmjz;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 33881
    const/4 v0, 0x1

    iget-object v1, p0, Lmjz;->apiHeader:Llyr;

    .line 33882
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 33884
    :cond_0
    iget-object v1, p0, Lmjz;->a:Lnqo;

    if-eqz v1, :cond_1

    .line 33885
    const/4 v1, 0x2

    iget-object v2, p0, Lmjz;->a:Lnqo;

    .line 33886
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 33888
    :cond_1
    iget-object v1, p0, Lmjz;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 33889
    iput v0, p0, Lmjz;->ai:I

    .line 33890
    return v0
.end method

.method public a(Loxn;)Lmjz;
    .locals 2

    .prologue
    .line 33898
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 33899
    sparse-switch v0, :sswitch_data_0

    .line 33903
    iget-object v1, p0, Lmjz;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 33904
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmjz;->ah:Ljava/util/List;

    .line 33907
    :cond_1
    iget-object v1, p0, Lmjz;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 33909
    :sswitch_0
    return-object p0

    .line 33914
    :sswitch_1
    iget-object v0, p0, Lmjz;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 33915
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmjz;->apiHeader:Llyr;

    .line 33917
    :cond_2
    iget-object v0, p0, Lmjz;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 33921
    :sswitch_2
    iget-object v0, p0, Lmjz;->a:Lnqo;

    if-nez v0, :cond_3

    .line 33922
    new-instance v0, Lnqo;

    invoke-direct {v0}, Lnqo;-><init>()V

    iput-object v0, p0, Lmjz;->a:Lnqo;

    .line 33924
    :cond_3
    iget-object v0, p0, Lmjz;->a:Lnqo;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 33899
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 33867
    iget-object v0, p0, Lmjz;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 33868
    const/4 v0, 0x1

    iget-object v1, p0, Lmjz;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 33870
    :cond_0
    iget-object v0, p0, Lmjz;->a:Lnqo;

    if-eqz v0, :cond_1

    .line 33871
    const/4 v0, 0x2

    iget-object v1, p0, Lmjz;->a:Lnqo;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 33873
    :cond_1
    iget-object v0, p0, Lmjz;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 33875
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 33852
    invoke-virtual {p0, p1}, Lmjz;->a(Loxn;)Lmjz;

    move-result-object v0

    return-object v0
.end method
