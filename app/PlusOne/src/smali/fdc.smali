.class final Lfdc;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:I

.field private final c:Lfdb;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;ILfdb;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 489
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 490
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lfdc;->a:Landroid/content/Context;

    .line 491
    iput-object p3, p0, Lfdc;->c:Lfdb;

    .line 492
    iput-object p4, p0, Lfdc;->d:Ljava/lang/String;

    .line 493
    iput p2, p0, Lfdc;->b:I

    .line 494
    iput-object p5, p0, Lfdc;->e:Ljava/lang/String;

    .line 495
    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 499
    if-eqz p1, :cond_0

    array-length v0, p1

    if-nez v0, :cond_1

    .line 500
    :cond_0
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 509
    :goto_0
    return-object v0

    .line 503
    :cond_1
    aget-object v5, p1, v6

    .line 504
    iget-object v0, p0, Lfdc;->d:Ljava/lang/String;

    invoke-static {v0}, Ljvj;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 505
    iget-object v0, p0, Lfdc;->a:Landroid/content/Context;

    iget v1, p0, Lfdc;->b:I

    iget-object v3, p0, Lfdc;->d:Ljava/lang/String;

    iget-object v4, p0, Lfdc;->e:Ljava/lang/String;

    invoke-static/range {v0 .. v6}, Ldig;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lkff;

    move-result-object v0

    .line 507
    invoke-virtual {v0}, Lkff;->l()V

    .line 509
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lkff;->t()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    move v6, v7

    :cond_3
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method protected a(Ljava/lang/Boolean;)V
    .locals 2

    .prologue
    .line 514
    iget-object v0, p0, Lfdc;->c:Lfdb;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Lfdb;->a(Lfdb;Z)V

    .line 515
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 481
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lfdc;->a([Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 481
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lfdc;->a(Ljava/lang/Boolean;)V

    return-void
.end method
