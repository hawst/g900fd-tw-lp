.class final Lfl;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Landroid/net/Uri;",
        "Ljava/lang/Boolean;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lfk;

.field private synthetic b:Landroid/os/CancellationSignal;

.field private synthetic c:Landroid/print/PrintAttributes;

.field private synthetic d:Landroid/print/PrintAttributes;

.field private synthetic e:Landroid/print/PrintDocumentAdapter$LayoutResultCallback;


# direct methods
.method constructor <init>(Lfk;Landroid/os/CancellationSignal;Landroid/print/PrintAttributes;Landroid/print/PrintAttributes;Landroid/print/PrintDocumentAdapter$LayoutResultCallback;)V
    .locals 0

    .prologue
    .line 330
    iput-object p1, p0, Lfl;->a:Lfk;

    iput-object p2, p0, Lfl;->b:Landroid/os/CancellationSignal;

    iput-object p3, p0, Lfl;->c:Landroid/print/PrintAttributes;

    iput-object p4, p0, Lfl;->d:Landroid/print/PrintAttributes;

    iput-object p5, p0, Lfl;->e:Landroid/print/PrintDocumentAdapter$LayoutResultCallback;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs a()Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 348
    :try_start_0
    iget-object v0, p0, Lfl;->a:Lfk;

    iget-object v0, v0, Lfk;->d:Lfi;

    iget-object v1, p0, Lfl;->a:Lfk;

    iget-object v1, v1, Lfk;->c:Landroid/net/Uri;

    const/16 v2, 0xdac

    invoke-virtual {v0, v1, v2}, Lfi;->a(Landroid/net/Uri;I)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 352
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Landroid/graphics/Bitmap;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 357
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 358
    iget-object v1, p0, Lfl;->a:Lfk;

    iput-object p1, v1, Lfk;->a:Landroid/graphics/Bitmap;

    .line 359
    if-eqz p1, :cond_1

    .line 360
    new-instance v1, Landroid/print/PrintDocumentInfo$Builder;

    iget-object v2, p0, Lfl;->a:Lfk;

    iget-object v2, v2, Lfk;->b:Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/print/PrintDocumentInfo$Builder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/print/PrintDocumentInfo$Builder;->setContentType(I)Landroid/print/PrintDocumentInfo$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/print/PrintDocumentInfo$Builder;->setPageCount(I)Landroid/print/PrintDocumentInfo$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/print/PrintDocumentInfo$Builder;->build()Landroid/print/PrintDocumentInfo;

    move-result-object v1

    .line 364
    iget-object v2, p0, Lfl;->c:Landroid/print/PrintAttributes;

    iget-object v3, p0, Lfl;->d:Landroid/print/PrintAttributes;

    invoke-virtual {v2, v3}, Landroid/print/PrintAttributes;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 366
    :goto_0
    iget-object v2, p0, Lfl;->e:Landroid/print/PrintDocumentAdapter$LayoutResultCallback;

    invoke-virtual {v2, v1, v0}, Landroid/print/PrintDocumentAdapter$LayoutResultCallback;->onLayoutFinished(Landroid/print/PrintDocumentInfo;Z)V

    .line 371
    :goto_1
    return-void

    .line 364
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 369
    :cond_1
    iget-object v0, p0, Lfl;->e:Landroid/print/PrintDocumentAdapter$LayoutResultCallback;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/print/PrintDocumentAdapter$LayoutResultCallback;->onLayoutFailed(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method protected b()V
    .locals 1

    .prologue
    .line 376
    iget-object v0, p0, Lfl;->e:Landroid/print/PrintDocumentAdapter$LayoutResultCallback;

    invoke-virtual {v0}, Landroid/print/PrintDocumentAdapter$LayoutResultCallback;->onLayoutCancelled()V

    .line 377
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 330
    invoke-virtual {p0}, Lfl;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onCancelled(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 330
    invoke-virtual {p0}, Lfl;->b()V

    return-void
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 330
    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lfl;->a(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 335
    iget-object v0, p0, Lfl;->b:Landroid/os/CancellationSignal;

    new-instance v1, Lfm;

    invoke-direct {v1, p0}, Lfm;-><init>(Lfl;)V

    invoke-virtual {v0, v1}, Landroid/os/CancellationSignal;->setOnCancelListener(Landroid/os/CancellationSignal$OnCancelListener;)V

    .line 343
    return-void
.end method
