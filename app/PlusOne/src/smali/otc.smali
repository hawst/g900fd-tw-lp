.class public final Lotc;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field private b:Lotd;

.field private c:Lote;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 83
    invoke-direct {p0}, Loxq;-><init>()V

    .line 272
    iput-object v0, p0, Lotc;->b:Lotd;

    .line 275
    iput-object v0, p0, Lotc;->c:Lote;

    .line 83
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 295
    const/4 v0, 0x0

    .line 296
    iget-object v1, p0, Lotc;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 297
    const/4 v0, 0x1

    iget-object v1, p0, Lotc;->a:Ljava/lang/String;

    .line 298
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 300
    :cond_0
    iget-object v1, p0, Lotc;->b:Lotd;

    if-eqz v1, :cond_1

    .line 301
    const/4 v1, 0x2

    iget-object v2, p0, Lotc;->b:Lotd;

    .line 302
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 304
    :cond_1
    iget-object v1, p0, Lotc;->c:Lote;

    if-eqz v1, :cond_2

    .line 305
    const/4 v1, 0x3

    iget-object v2, p0, Lotc;->c:Lote;

    .line 306
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 308
    :cond_2
    iget-object v1, p0, Lotc;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 309
    iput v0, p0, Lotc;->ai:I

    .line 310
    return v0
.end method

.method public a(Loxn;)Lotc;
    .locals 2

    .prologue
    .line 318
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 319
    sparse-switch v0, :sswitch_data_0

    .line 323
    iget-object v1, p0, Lotc;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 324
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lotc;->ah:Ljava/util/List;

    .line 327
    :cond_1
    iget-object v1, p0, Lotc;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 329
    :sswitch_0
    return-object p0

    .line 334
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lotc;->a:Ljava/lang/String;

    goto :goto_0

    .line 338
    :sswitch_2
    iget-object v0, p0, Lotc;->b:Lotd;

    if-nez v0, :cond_2

    .line 339
    new-instance v0, Lotd;

    invoke-direct {v0}, Lotd;-><init>()V

    iput-object v0, p0, Lotc;->b:Lotd;

    .line 341
    :cond_2
    iget-object v0, p0, Lotc;->b:Lotd;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 345
    :sswitch_3
    iget-object v0, p0, Lotc;->c:Lote;

    if-nez v0, :cond_3

    .line 346
    new-instance v0, Lote;

    invoke-direct {v0}, Lote;-><init>()V

    iput-object v0, p0, Lotc;->c:Lote;

    .line 348
    :cond_3
    iget-object v0, p0, Lotc;->c:Lote;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 319
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 280
    iget-object v0, p0, Lotc;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 281
    const/4 v0, 0x1

    iget-object v1, p0, Lotc;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 283
    :cond_0
    iget-object v0, p0, Lotc;->b:Lotd;

    if-eqz v0, :cond_1

    .line 284
    const/4 v0, 0x2

    iget-object v1, p0, Lotc;->b:Lotd;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 286
    :cond_1
    iget-object v0, p0, Lotc;->c:Lote;

    if-eqz v0, :cond_2

    .line 287
    const/4 v0, 0x3

    iget-object v1, p0, Lotc;->c:Lote;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 289
    :cond_2
    iget-object v0, p0, Lotc;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 291
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 79
    invoke-virtual {p0, p1}, Lotc;->a(Loxn;)Lotc;

    move-result-object v0

    return-object v0
.end method
