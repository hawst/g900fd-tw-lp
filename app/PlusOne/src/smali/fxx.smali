.class public final Lfxx;
.super Lfye;
.source "PG"


# static fields
.field private static a:Z

.field private static b:F

.field private static c:I

.field private static d:Ljava/lang/String;


# instance fields
.field private e:Lfwe;

.field private f:Landroid/widget/TextView;

.field private g:I

.field private h:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 48
    invoke-direct {p0, p1, p2, p3}, Lfye;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 49
    sget-boolean v0, Lfxx;->a:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a073d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lfxx;->d:Ljava/lang/String;

    const v1, 0x7f0d024c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lfxx;->b:F

    const v1, 0x7f0b013d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lfxx;->c:I

    sput-boolean v6, Lfxx;->a:Z

    :cond_0
    sget v3, Lfxx;->b:F

    sget v4, Lfxx;->c:I

    move-object v0, p1

    move-object v1, p2

    move v2, p3

    invoke-static/range {v0 .. v6}, Llif;->a(Landroid/content/Context;Landroid/util/AttributeSet;IFIZZ)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Lfxx;->f:Landroid/widget/TextView;

    iget-object v0, p0, Lfxx;->f:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lfxx;->addView(Landroid/view/View;)V

    new-instance v0, Lfwe;

    invoke-direct {v0, p1, p2, p3}, Lfwe;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lfxx;->e:Lfwe;

    iget-object v0, p0, Lfxx;->e:Lfwe;

    invoke-virtual {p0, v0}, Lfxx;->addView(Landroid/view/View;)V

    iput v5, p0, Lfxx;->g:I

    iput v5, p0, Lfxx;->h:I

    .line 50
    return-void
.end method

.method private a([Lpay;ILjava/util/ArrayList;)I
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lpay;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Ldrv;",
            ">;)I"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 99
    .line 100
    invoke-static {p1, p2}, Ldrm;->a([Lpay;I)Lpay;

    move-result-object v1

    .line 101
    if-eqz v1, :cond_2

    .line 102
    iget-object v2, v1, Lpay;->d:[Lpax;

    if-eqz v2, :cond_1

    .line 103
    iget-object v2, v1, Lpay;->d:[Lpax;

    array-length v3, v2

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 104
    iget-object v5, v4, Lpax;->b:Lpaf;

    if-eqz v5, :cond_0

    .line 105
    iget-object v5, v4, Lpax;->b:Lpaf;

    iget-object v5, v5, Lpaf;->c:Ljava/lang/String;

    .line 106
    iget-object v6, v4, Lpax;->b:Lpaf;

    iget-object v6, v6, Lpaf;->b:Ljava/lang/String;

    .line 107
    iget-object v4, v4, Lpax;->b:Lpaf;

    iget-object v4, v4, Lpaf;->d:Ljava/lang/String;

    .line 109
    if-eqz v5, :cond_0

    .line 110
    new-instance v7, Ldrv;

    invoke-direct {v7}, Ldrv;-><init>()V

    .line 111
    iput-object v5, v7, Ldrv;->gaiaId:Ljava/lang/String;

    .line 112
    iput-object v6, v7, Ldrv;->name:Ljava/lang/String;

    .line 113
    iput-object v4, v7, Ldrv;->avatarUrl:Ljava/lang/String;

    .line 114
    invoke-virtual {p3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 103
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 120
    :cond_1
    iget-object v0, v1, Lpay;->c:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 123
    :cond_2
    return v0
.end method


# virtual methods
.method public a(Lfxf;Ljava/util/ArrayList;I)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lfxf;",
            "Ljava/util/ArrayList",
            "<",
            "Ldrv;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 150
    iput p3, p0, Lfxx;->h:I

    .line 151
    iget-object v0, p0, Lfxx;->e:Lfwe;

    iget v1, p0, Lfxx;->h:I

    invoke-virtual {v0, p2, p1, v1}, Lfwe;->a(Ljava/util/ArrayList;Lfxf;I)V

    .line 152
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Lfxx;->g:I

    .line 153
    iget-object v0, p0, Lfxx;->f:Landroid/widget/TextView;

    sget-object v1, Lfxx;->d:Ljava/lang/String;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lfxx;->h:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 154
    invoke-virtual {p0}, Lfxx;->requestLayout()V

    .line 155
    return-void
.end method

.method public a([Lpay;Lfxf;)V
    .locals 3

    .prologue
    .line 130
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 132
    const/4 v1, 0x1

    invoke-direct {p0, p1, v1, v0}, Lfxx;->a([Lpay;ILjava/util/ArrayList;)I

    move-result v1

    add-int/lit8 v1, v1, 0x0

    .line 133
    const/4 v2, 0x6

    invoke-direct {p0, p1, v2, v0}, Lfxx;->a([Lpay;ILjava/util/ArrayList;)I

    move-result v2

    add-int/2addr v1, v2

    .line 134
    const/4 v2, 0x5

    invoke-direct {p0, p1, v2, v0}, Lfxx;->a([Lpay;ILjava/util/ArrayList;)I

    move-result v2

    add-int/2addr v1, v2

    .line 136
    invoke-virtual {p0, p2, v0, v1}, Lfxx;->a(Lfxf;Ljava/util/ArrayList;I)V

    .line 137
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 162
    iget v0, p0, Lfxx;->h:I

    return v0
.end method

.method public c()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 166
    iput v0, p0, Lfxx;->g:I

    .line 167
    iput v0, p0, Lfxx;->h:I

    .line 168
    return-void
.end method

.method protected measureChildren(II)V
    .locals 5

    .prologue
    const/high16 v4, -0x80000000

    const/4 v3, 0x0

    .line 75
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 76
    iget-object v1, p0, Lfxx;->f:Landroid/widget/TextView;

    invoke-static {v1, v0, v4, v3, v3}, Lfxx;->a(Landroid/view/View;IIII)V

    .line 79
    iget-object v1, p0, Lfxx;->f:Landroid/widget/TextView;

    invoke-static {v1, v3, v3}, Lfxx;->a(Landroid/view/View;II)V

    .line 80
    iget-object v1, p0, Lfxx;->f:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v1

    add-int/lit8 v1, v1, 0x0

    .line 82
    iget v2, p0, Lfxx;->g:I

    if-lez v2, :cond_0

    .line 83
    iget-object v2, p0, Lfxx;->e:Lfwe;

    invoke-static {v2, v0, v4, v3, v3}, Lfxx;->a(Landroid/view/View;IIII)V

    .line 84
    iget-object v0, p0, Lfxx;->e:Lfwe;

    invoke-static {v0, v3, v1}, Lfxx;->a(Landroid/view/View;II)V

    .line 85
    iget-object v0, p0, Lfxx;->e:Lfwe;

    invoke-virtual {v0}, Lfwe;->getMeasuredHeight()I

    .line 87
    :cond_0
    return-void
.end method
