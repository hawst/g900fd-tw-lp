.class public final enum Lfvc;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lfvc;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lfvc;

.field public static final enum b:Lfvc;

.field public static final enum c:Lfvc;

.field public static final enum d:Lfvc;

.field public static final enum e:Lfvc;

.field public static final enum f:Lfvc;

.field public static final enum g:Lfvc;

.field public static final enum h:Lfvc;

.field public static final enum i:Lfvc;

.field public static final enum j:Lfvc;

.field public static final enum k:Lfvc;

.field public static final enum l:Lfvc;

.field public static final enum m:Lfvc;

.field public static final enum n:Lfvc;

.field private static enum o:Lfvc;

.field private static enum p:Lfvc;

.field private static enum q:Lfvc;

.field private static enum r:Lfvc;

.field private static enum s:Lfvc;

.field private static enum t:Lfvc;

.field private static final u:Lloy;

.field private static final synthetic y:[Lfvc;


# instance fields
.field private final v:Ljava/lang/String;

.field private final w:Ljava/lang/String;

.field private final x:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    .line 22
    new-instance v0, Lfvc;

    const-string v1, "PLUS_STATICMAPS_API_KEY"

    const/4 v2, 0x0

    const-string v3, "debug.plus.staticmaps.api_key"

    const-string v4, "AIzaSyAYfoSs86LzFMXNWJhyeGtZp0ijdZb_uGU"

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lfvc;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lfvc;->a:Lfvc;

    .line 26
    new-instance v0, Lfvc;

    const-string v1, "LOCATION_DEBUGGING"

    const/4 v2, 0x1

    const-string v3, "debug.plus.location.toast"

    const-string v4, "FALSE"

    invoke-direct {v0, v1, v2, v3, v4}, Lfvc;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lfvc;->b:Lfvc;

    .line 29
    new-instance v0, Lfvc;

    const-string v1, "EMOTISHARE_GEN1_DATE"

    const/4 v2, 0x2

    const-string v3, "debug.plus.emotishare.gen1"

    const-string v4, "0"

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lfvc;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lfvc;->c:Lfvc;

    .line 30
    new-instance v0, Lfvc;

    const-string v1, "EMOTISHARE_GEN2_DATE"

    const/4 v2, 0x3

    const-string v3, "debug.plus.emotishare.gen2"

    new-instance v4, Ljava/util/GregorianCalendar;

    const/16 v5, 0x7dc

    const/16 v6, 0xb

    const/16 v7, 0x1b

    invoke-direct {v4, v5, v6, v7}, Ljava/util/GregorianCalendar;-><init>(III)V

    .line 31
    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lfvc;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lfvc;->d:Lfvc;

    .line 33
    new-instance v0, Lfvc;

    const-string v1, "EMOTISHARE_GEN3_DATE"

    const/4 v2, 0x4

    const-string v3, "debug.plus.emotishare.gen3"

    new-instance v4, Ljava/util/GregorianCalendar;

    const/16 v5, 0x7dd

    const/4 v6, 0x1

    const/4 v7, 0x1

    invoke-direct {v4, v5, v6, v7}, Ljava/util/GregorianCalendar;-><init>(III)V

    .line 34
    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lfvc;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lfvc;->e:Lfvc;

    .line 38
    new-instance v0, Lfvc;

    const-string v1, "ENABLE_STREAM_GIF_ANIMATION"

    const/4 v2, 0x5

    const-string v3, "debug.plus.enable_stream_gif"

    const-string v4, "TRUE"

    invoke-direct {v0, v1, v2, v3, v4}, Lfvc;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lfvc;->f:Lfvc;

    .line 41
    new-instance v0, Lfvc;

    const-string v1, "IS_AUTOMATION_BUILD"

    const/4 v2, 0x6

    const-string v3, "debug.plus.testing.automation"

    const-string v4, "FALSE"

    const/4 v5, 0x3

    invoke-direct/range {v0 .. v5}, Lfvc;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lfvc;->g:Lfvc;

    .line 44
    new-instance v0, Lfvc;

    const-string v1, "IS_MONKEY_BUILD"

    const/4 v2, 0x7

    const-string v3, "debug.plus.testing.monkey"

    const-string v4, "FALSE"

    const/4 v5, 0x3

    invoke-direct/range {v0 .. v5}, Lfvc;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lfvc;->o:Lfvc;

    .line 47
    new-instance v0, Lfvc;

    const-string v1, "ENABLE_NATIVE_NETWORKING"

    const/16 v2, 0x8

    const-string v3, "debug.plus.native.http"

    const-string v4, "true"

    const/4 v5, 0x3

    invoke-direct/range {v0 .. v5}, Lfvc;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lfvc;->p:Lfvc;

    .line 50
    new-instance v0, Lfvc;

    const-string v1, "ENABLE_DEBUG_STREAM"

    const/16 v2, 0x9

    const-string v3, "debug.plus.stream"

    const-string v4, "false"

    const/4 v5, 0x3

    invoke-direct/range {v0 .. v5}, Lfvc;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lfvc;->q:Lfvc;

    .line 53
    new-instance v0, Lfvc;

    const-string v1, "GUNS_DEBUG_MODE"

    const/16 v2, 0xa

    const-string v3, "debug.plus.guns_debug_mode"

    const-string v4, ""

    const/4 v5, 0x3

    invoke-direct/range {v0 .. v5}, Lfvc;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lfvc;->h:Lfvc;

    .line 58
    new-instance v0, Lfvc;

    const-string v1, "ENABLE_AUTO_SIGNIN"

    const/16 v2, 0xb

    const-string v3, "debug.plus.autosignin"

    const-string v4, "false"

    const/4 v5, 0x3

    invoke-direct/range {v0 .. v5}, Lfvc;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lfvc;->r:Lfvc;

    .line 60
    new-instance v0, Lfvc;

    const-string v1, "ENABLE_ADAPTIVE_IMAGE_DOWNLOADS"

    const/16 v2, 0xc

    const-string v3, "debug.plus.adaptive.down"

    const-string v4, "false"

    const/4 v5, 0x3

    invoke-direct/range {v0 .. v5}, Lfvc;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lfvc;->s:Lfvc;

    .line 63
    new-instance v0, Lfvc;

    const-string v1, "ENABLE_NEW_FACE_TAGS"

    const/16 v2, 0xd

    const-string v3, "debug.plus.new_face_tags"

    const-string v4, "false"

    const/4 v5, 0x3

    invoke-direct/range {v0 .. v5}, Lfvc;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lfvc;->t:Lfvc;

    .line 71
    new-instance v0, Lfvc;

    const-string v1, "ENABLE_PHOTOS_SYNC_ON_REFRESH"

    const/16 v2, 0xe

    const-string v3, "debug.plus.photo_refresh_syncs"

    const-string v4, "true"

    const/4 v5, 0x3

    invoke-direct/range {v0 .. v5}, Lfvc;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lfvc;->i:Lfvc;

    .line 77
    new-instance v0, Lfvc;

    const-string v1, "ENABLE_ANDROID_CONTACTS_SYNC"

    const/16 v2, 0xf

    const-string v3, "debug.plus.synctocp2"

    const-string v4, "true"

    const/4 v5, 0x3

    invoke-direct/range {v0 .. v5}, Lfvc;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lfvc;->j:Lfvc;

    .line 82
    new-instance v0, Lfvc;

    const-string v1, "SHOW_PHOTO_TILE_DEBUG_INFO"

    const/16 v2, 0x10

    const-string v3, "debug.plus.debug_overlay"

    const-string v4, "false"

    invoke-direct {v0, v1, v2, v3, v4}, Lfvc;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lfvc;->k:Lfvc;

    .line 87
    new-instance v0, Lfvc;

    const-string v1, "ENABLE_ALBUM_VISIBILITY"

    const/16 v2, 0x11

    const-string v3, "debug.plus.album_visibility"

    const-string v4, "false"

    const/4 v5, 0x3

    invoke-direct/range {v0 .. v5}, Lfvc;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lfvc;->l:Lfvc;

    .line 93
    new-instance v0, Lfvc;

    const-string v1, "MOVIEMAKER_FORCE_ENABLE"

    const/16 v2, 0x12

    const-string v3, "debug.plus.moviemaker_force"

    const-string v4, "false"

    invoke-direct {v0, v1, v2, v3, v4}, Lfvc;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lfvc;->m:Lfvc;

    .line 99
    new-instance v0, Lfvc;

    const-string v1, "SWIPE_TO_DISMISS_AB_STATUS_BAR"

    const/16 v2, 0x13

    const-string v3, "debug.photos.dismiss_ab_bar"

    const-string v4, "false"

    invoke-direct {v0, v1, v2, v3, v4}, Lfvc;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lfvc;->n:Lfvc;

    .line 16
    const/16 v0, 0x14

    new-array v0, v0, [Lfvc;

    const/4 v1, 0x0

    sget-object v2, Lfvc;->a:Lfvc;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lfvc;->b:Lfvc;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lfvc;->c:Lfvc;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lfvc;->d:Lfvc;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lfvc;->e:Lfvc;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lfvc;->f:Lfvc;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lfvc;->g:Lfvc;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lfvc;->o:Lfvc;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lfvc;->p:Lfvc;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lfvc;->q:Lfvc;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lfvc;->h:Lfvc;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lfvc;->r:Lfvc;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lfvc;->s:Lfvc;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lfvc;->t:Lfvc;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lfvc;->i:Lfvc;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lfvc;->j:Lfvc;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lfvc;->k:Lfvc;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lfvc;->l:Lfvc;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lfvc;->m:Lfvc;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lfvc;->n:Lfvc;

    aput-object v2, v0, v1

    sput-object v0, Lfvc;->y:[Lfvc;

    .line 111
    new-instance v0, Lloy;

    const-string v1, "debug.plus.dogfood"

    invoke-direct {v0, v1}, Lloy;-><init>(Ljava/lang/String;)V

    sput-object v0, Lfvc;->u:Lloy;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 122
    const/4 v5, 0x2

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lfvc;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 123
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 125
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 126
    iput-object p3, p0, Lfvc;->v:Ljava/lang/String;

    .line 127
    iput-object p4, p0, Lfvc;->w:Ljava/lang/String;

    .line 128
    iput p5, p0, Lfvc;->x:I

    .line 129
    return-void
.end method

.method public static c()Z
    .locals 1

    .prologue
    .line 166
    sget-object v0, Lfvc;->o:Lfvc;

    invoke-virtual {v0}, Lfvc;->b()Z

    move-result v0

    return v0
.end method

.method public static d()Z
    .locals 1

    .prologue
    .line 170
    sget-object v0, Lfvc;->u:Lloy;

    const/4 v0, 0x0

    return v0
.end method

.method public static valueOf(Ljava/lang/String;)Lfvc;
    .locals 1

    .prologue
    .line 16
    const-class v0, Lfvc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lfvc;

    return-object v0
.end method

.method public static values()[Lfvc;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lfvc;->y:[Lfvc;

    invoke-virtual {v0}, [Lfvc;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lfvc;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 132
    iget-object v0, p0, Lfvc;->w:Ljava/lang/String;

    .line 133
    iget v1, p0, Lfvc;->x:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_2

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    sget-object v1, Lfvc;->u:Lloy;

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    .line 134
    iget-object v0, p0, Lfvc;->v:Ljava/lang/String;

    iget-object v1, p0, Lfvc;->w:Ljava/lang/String;

    invoke-static {v0, v1}, Lfvr;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 136
    :cond_1
    return-object v0

    .line 133
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public b()Z
    .locals 2

    .prologue
    .line 140
    const-string v0, "TRUE"

    invoke-virtual {p0}, Lfvc;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
