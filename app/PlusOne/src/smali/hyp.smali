.class public Lhyp;
.super Landroid/widget/BaseAdapter;
.source "PG"


# instance fields
.field private a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/Adapter;",
            ">;"
        }
    .end annotation
.end field

.field private b:[I

.field private c:[I

.field private d:Z

.field private e:Z

.field private f:I

.field private g:I

.field private h:[I

.field private final i:Landroid/database/DataSetObserver;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 15
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 17
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lhyp;->a:Ljava/util/ArrayList;

    .line 20
    iput-boolean v1, p0, Lhyp;->d:Z

    .line 21
    iput-boolean v1, p0, Lhyp;->e:Z

    .line 22
    iput v1, p0, Lhyp;->f:I

    .line 23
    const/4 v0, -0x1

    iput v0, p0, Lhyp;->g:I

    .line 28
    new-instance v0, Lhyq;

    invoke-direct {v0, p0}, Lhyq;-><init>(Lhyp;)V

    iput-object v0, p0, Lhyp;->i:Landroid/database/DataSetObserver;

    return-void
.end method

.method static synthetic a(Lhyp;)V
    .locals 1

    .prologue
    .line 15
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhyp;->d:Z

    return-void
.end method

.method private b()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 200
    iget-boolean v1, p0, Lhyp;->d:Z

    if-eqz v1, :cond_0

    .line 229
    :goto_0
    return-void

    .line 205
    :cond_0
    iget-object v1, p0, Lhyp;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [I

    iput-object v1, p0, Lhyp;->b:[I

    .line 206
    iput v0, p0, Lhyp;->f:I

    .line 209
    iget-object v1, p0, Lhyp;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, 0x2

    new-array v1, v1, [I

    iput-object v1, p0, Lhyp;->c:[I

    .line 210
    iget-object v1, p0, Lhyp;->c:[I

    aput v0, v1, v0

    .line 213
    iput v0, p0, Lhyp;->g:I

    .line 214
    iget-object v1, p0, Lhyp;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    new-array v1, v1, [I

    iput-object v1, p0, Lhyp;->h:[I

    .line 215
    iget-object v1, p0, Lhyp;->h:[I

    aput v0, v1, v0

    .line 217
    :goto_1
    iget-object v1, p0, Lhyp;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 218
    iget-object v1, p0, Lhyp;->b:[I

    invoke-direct {p0, v0}, Lhyp;->c(I)Landroid/widget/Adapter;

    move-result-object v2

    invoke-interface {v2}, Landroid/widget/Adapter;->getCount()I

    move-result v2

    aput v2, v1, v0

    .line 219
    iget v1, p0, Lhyp;->f:I

    iget-object v2, p0, Lhyp;->b:[I

    aget v2, v2, v0

    add-int/2addr v1, v2

    iput v1, p0, Lhyp;->f:I

    .line 221
    iget-object v1, p0, Lhyp;->c:[I

    add-int/lit8 v2, v0, 0x1

    iget-object v3, p0, Lhyp;->c:[I

    aget v3, v3, v0

    iget-object v4, p0, Lhyp;->b:[I

    aget v4, v4, v0

    add-int/2addr v3, v4

    aput v3, v1, v2

    .line 223
    invoke-direct {p0, v0}, Lhyp;->c(I)Landroid/widget/Adapter;

    move-result-object v1

    invoke-interface {v1}, Landroid/widget/Adapter;->getViewTypeCount()I

    move-result v1

    .line 224
    iget-object v2, p0, Lhyp;->h:[I

    add-int/lit8 v3, v0, 0x1

    iget-object v4, p0, Lhyp;->h:[I

    aget v4, v4, v0

    add-int/2addr v4, v1

    aput v4, v2, v3

    .line 225
    iget v2, p0, Lhyp;->g:I

    add-int/2addr v1, v2

    iput v1, p0, Lhyp;->g:I

    .line 217
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 227
    :cond_1
    iget-object v0, p0, Lhyp;->c:[I

    iget-object v1, p0, Lhyp;->c:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    const v2, 0x7fffffff

    aput v2, v0, v1

    .line 228
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhyp;->d:Z

    goto/16 :goto_0
.end method

.method private c(I)Landroid/widget/Adapter;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lhyp;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Adapter;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lhyp;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public a(I)I
    .locals 2

    .prologue
    .line 87
    invoke-direct {p0}, Lhyp;->b()V

    .line 88
    const/4 v0, 0x1

    .line 89
    :goto_0
    iget-object v1, p0, Lhyp;->c:[I

    aget v1, v1, v0

    if-lt p1, v1, :cond_0

    .line 90
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 92
    :cond_0
    iget-object v1, p0, Lhyp;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-le v0, v1, :cond_1

    .line 93
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0, p1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(I)V

    throw v0

    .line 95
    :cond_1
    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public a(Landroid/widget/Adapter;)V
    .locals 2

    .prologue
    .line 49
    iget-boolean v0, p0, Lhyp;->e:Z

    if-eqz v0, :cond_0

    .line 50
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "has been set on ListView"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 52
    :cond_0
    iget-object v0, p0, Lhyp;->i:Landroid/database/DataSetObserver;

    invoke-interface {p1, v0}, Landroid/widget/Adapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 53
    iget-object v0, p0, Lhyp;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 54
    return-void
.end method

.method public b(I)I
    .locals 2

    .prologue
    .line 113
    invoke-direct {p0}, Lhyp;->b()V

    .line 114
    invoke-virtual {p0, p1}, Lhyp;->a(I)I

    move-result v0

    .line 115
    iget-object v1, p0, Lhyp;->c:[I

    aget v0, v1, v0

    sub-int v0, p1, v0

    return v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 159
    invoke-direct {p0}, Lhyp;->b()V

    .line 160
    iget v0, p0, Lhyp;->f:I

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 145
    invoke-direct {p0}, Lhyp;->b()V

    .line 146
    invoke-virtual {p0, p1}, Lhyp;->a(I)I

    move-result v0

    invoke-direct {p0, v0}, Lhyp;->c(I)Landroid/widget/Adapter;

    move-result-object v0

    .line 147
    invoke-virtual {p0, p1}, Lhyp;->b(I)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 152
    invoke-direct {p0}, Lhyp;->b()V

    .line 153
    invoke-virtual {p0, p1}, Lhyp;->a(I)I

    move-result v0

    invoke-direct {p0, v0}, Lhyp;->c(I)Landroid/widget/Adapter;

    move-result-object v0

    .line 154
    invoke-virtual {p0, p1}, Lhyp;->b(I)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/widget/Adapter;->getItemId(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 3

    .prologue
    .line 131
    invoke-direct {p0}, Lhyp;->b()V

    .line 132
    invoke-virtual {p0, p1}, Lhyp;->a(I)I

    move-result v0

    .line 133
    iget-object v1, p0, Lhyp;->c:[I

    aget v1, v1, v0

    sub-int v1, p1, v1

    iget-object v2, p0, Lhyp;->h:[I

    aget v2, v2, v0

    invoke-direct {p0, v0}, Lhyp;->c(I)Landroid/widget/Adapter;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/widget/Adapter;->getItemViewType(I)I

    move-result v0

    add-int/2addr v0, v2

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 138
    invoke-direct {p0}, Lhyp;->b()V

    .line 139
    invoke-virtual {p0, p1}, Lhyp;->a(I)I

    move-result v0

    invoke-direct {p0, v0}, Lhyp;->c(I)Landroid/widget/Adapter;

    move-result-object v0

    .line 140
    invoke-virtual {p0, p1}, Lhyp;->b(I)I

    move-result v1

    invoke-interface {v0, v1, p2, p3}, Landroid/widget/Adapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 165
    invoke-direct {p0}, Lhyp;->b()V

    .line 166
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhyp;->e:Z

    .line 167
    iget v0, p0, Lhyp;->g:I

    return v0
.end method
