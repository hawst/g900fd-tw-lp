.class public final Lndo;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lndo;


# instance fields
.field public b:Lotc;

.field public c:I

.field public d:Ljava/lang/Integer;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 109
    const/4 v0, 0x0

    new-array v0, v0, [Lndo;

    sput-object v0, Lndo;->a:[Lndo;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 110
    invoke-direct {p0}, Loxq;-><init>()V

    .line 119
    const/4 v0, 0x0

    iput-object v0, p0, Lndo;->b:Lotc;

    .line 122
    const/high16 v0, -0x80000000

    iput v0, p0, Lndo;->c:I

    .line 110
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 154
    const/4 v0, 0x0

    .line 155
    iget-object v1, p0, Lndo;->b:Lotc;

    if-eqz v1, :cond_0

    .line 156
    const/4 v0, 0x1

    iget-object v1, p0, Lndo;->b:Lotc;

    .line 157
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 159
    :cond_0
    iget v1, p0, Lndo;->c:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_1

    .line 160
    const/4 v1, 0x2

    iget v2, p0, Lndo;->c:I

    .line 161
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 163
    :cond_1
    iget-object v1, p0, Lndo;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 164
    const/4 v1, 0x3

    iget-object v2, p0, Lndo;->d:Ljava/lang/Integer;

    .line 165
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 167
    :cond_2
    iget-object v1, p0, Lndo;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 168
    const/4 v1, 0x4

    iget-object v2, p0, Lndo;->e:Ljava/lang/String;

    .line 169
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 171
    :cond_3
    iget-object v1, p0, Lndo;->f:Ljava/lang/Long;

    if-eqz v1, :cond_4

    .line 172
    const/4 v1, 0x5

    iget-object v2, p0, Lndo;->f:Ljava/lang/Long;

    .line 173
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 175
    :cond_4
    iget-object v1, p0, Lndo;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 176
    iput v0, p0, Lndo;->ai:I

    .line 177
    return v0
.end method

.method public a(Loxn;)Lndo;
    .locals 2

    .prologue
    .line 185
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 186
    sparse-switch v0, :sswitch_data_0

    .line 190
    iget-object v1, p0, Lndo;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 191
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lndo;->ah:Ljava/util/List;

    .line 194
    :cond_1
    iget-object v1, p0, Lndo;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 196
    :sswitch_0
    return-object p0

    .line 201
    :sswitch_1
    iget-object v0, p0, Lndo;->b:Lotc;

    if-nez v0, :cond_2

    .line 202
    new-instance v0, Lotc;

    invoke-direct {v0}, Lotc;-><init>()V

    iput-object v0, p0, Lndo;->b:Lotc;

    .line 204
    :cond_2
    iget-object v0, p0, Lndo;->b:Lotc;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 208
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 209
    if-eqz v0, :cond_3

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    .line 212
    :cond_3
    iput v0, p0, Lndo;->c:I

    goto :goto_0

    .line 214
    :cond_4
    const/4 v0, 0x0

    iput v0, p0, Lndo;->c:I

    goto :goto_0

    .line 219
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lndo;->d:Ljava/lang/Integer;

    goto :goto_0

    .line 223
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lndo;->e:Ljava/lang/String;

    goto :goto_0

    .line 227
    :sswitch_5
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lndo;->f:Ljava/lang/Long;

    goto :goto_0

    .line 186
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 133
    iget-object v0, p0, Lndo;->b:Lotc;

    if-eqz v0, :cond_0

    .line 134
    const/4 v0, 0x1

    iget-object v1, p0, Lndo;->b:Lotc;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 136
    :cond_0
    iget v0, p0, Lndo;->c:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 137
    const/4 v0, 0x2

    iget v1, p0, Lndo;->c:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 139
    :cond_1
    iget-object v0, p0, Lndo;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 140
    const/4 v0, 0x3

    iget-object v1, p0, Lndo;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 142
    :cond_2
    iget-object v0, p0, Lndo;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 143
    const/4 v0, 0x4

    iget-object v1, p0, Lndo;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 145
    :cond_3
    iget-object v0, p0, Lndo;->f:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 146
    const/4 v0, 0x5

    iget-object v1, p0, Lndo;->f:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 148
    :cond_4
    iget-object v0, p0, Lndo;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 150
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 106
    invoke-virtual {p0, p1}, Lndo;->a(Loxn;)Lndo;

    move-result-object v0

    return-object v0
.end method
