.class public final Lnhy;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lnja;

.field private b:[Lnhx;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5833
    invoke-direct {p0}, Loxq;-><init>()V

    .line 5836
    const/4 v0, 0x0

    iput-object v0, p0, Lnhy;->a:Lnja;

    .line 5839
    sget-object v0, Lnhx;->a:[Lnhx;

    iput-object v0, p0, Lnhy;->b:[Lnhx;

    .line 5833
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 5860
    .line 5861
    iget-object v0, p0, Lnhy;->a:Lnja;

    if-eqz v0, :cond_2

    .line 5862
    const/4 v0, 0x1

    iget-object v2, p0, Lnhy;->a:Lnja;

    .line 5863
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 5865
    :goto_0
    iget-object v2, p0, Lnhy;->b:[Lnhx;

    if-eqz v2, :cond_1

    .line 5866
    iget-object v2, p0, Lnhy;->b:[Lnhx;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 5867
    if-eqz v4, :cond_0

    .line 5868
    const/4 v5, 0x2

    .line 5869
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 5866
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 5873
    :cond_1
    iget-object v1, p0, Lnhy;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5874
    iput v0, p0, Lnhy;->ai:I

    .line 5875
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lnhy;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 5883
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 5884
    sparse-switch v0, :sswitch_data_0

    .line 5888
    iget-object v2, p0, Lnhy;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 5889
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnhy;->ah:Ljava/util/List;

    .line 5892
    :cond_1
    iget-object v2, p0, Lnhy;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5894
    :sswitch_0
    return-object p0

    .line 5899
    :sswitch_1
    iget-object v0, p0, Lnhy;->a:Lnja;

    if-nez v0, :cond_2

    .line 5900
    new-instance v0, Lnja;

    invoke-direct {v0}, Lnja;-><init>()V

    iput-object v0, p0, Lnhy;->a:Lnja;

    .line 5902
    :cond_2
    iget-object v0, p0, Lnhy;->a:Lnja;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 5906
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 5907
    iget-object v0, p0, Lnhy;->b:[Lnhx;

    if-nez v0, :cond_4

    move v0, v1

    .line 5908
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnhx;

    .line 5909
    iget-object v3, p0, Lnhy;->b:[Lnhx;

    if-eqz v3, :cond_3

    .line 5910
    iget-object v3, p0, Lnhy;->b:[Lnhx;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 5912
    :cond_3
    iput-object v2, p0, Lnhy;->b:[Lnhx;

    .line 5913
    :goto_2
    iget-object v2, p0, Lnhy;->b:[Lnhx;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 5914
    iget-object v2, p0, Lnhy;->b:[Lnhx;

    new-instance v3, Lnhx;

    invoke-direct {v3}, Lnhx;-><init>()V

    aput-object v3, v2, v0

    .line 5915
    iget-object v2, p0, Lnhy;->b:[Lnhx;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 5916
    invoke-virtual {p1}, Loxn;->a()I

    .line 5913
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 5907
    :cond_4
    iget-object v0, p0, Lnhy;->b:[Lnhx;

    array-length v0, v0

    goto :goto_1

    .line 5919
    :cond_5
    iget-object v2, p0, Lnhy;->b:[Lnhx;

    new-instance v3, Lnhx;

    invoke-direct {v3}, Lnhx;-><init>()V

    aput-object v3, v2, v0

    .line 5920
    iget-object v2, p0, Lnhy;->b:[Lnhx;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 5884
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 5844
    iget-object v0, p0, Lnhy;->a:Lnja;

    if-eqz v0, :cond_0

    .line 5845
    const/4 v0, 0x1

    iget-object v1, p0, Lnhy;->a:Lnja;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 5847
    :cond_0
    iget-object v0, p0, Lnhy;->b:[Lnhx;

    if-eqz v0, :cond_2

    .line 5848
    iget-object v1, p0, Lnhy;->b:[Lnhx;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 5849
    if-eqz v3, :cond_1

    .line 5850
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 5848
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 5854
    :cond_2
    iget-object v0, p0, Lnhy;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 5856
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5829
    invoke-virtual {p0, p1}, Lnhy;->a(Loxn;)Lnhy;

    move-result-object v0

    return-object v0
.end method
