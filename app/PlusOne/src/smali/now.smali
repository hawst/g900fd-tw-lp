.class public final Lnow;
.super Loxq;
.source "PG"


# instance fields
.field private a:I

.field private b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/high16 v0, -0x80000000

    .line 214
    invoke-direct {p0}, Loxq;-><init>()V

    .line 244
    iput v0, p0, Lnow;->a:I

    .line 247
    iput v0, p0, Lnow;->b:I

    .line 214
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 264
    const/4 v0, 0x0

    .line 265
    iget v1, p0, Lnow;->a:I

    if-eq v1, v2, :cond_0

    .line 266
    const/4 v0, 0x1

    iget v1, p0, Lnow;->a:I

    .line 267
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 269
    :cond_0
    iget v1, p0, Lnow;->b:I

    if-eq v1, v2, :cond_1

    .line 270
    const/4 v1, 0x2

    iget v2, p0, Lnow;->b:I

    .line 271
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 273
    :cond_1
    iget-object v1, p0, Lnow;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 274
    iput v0, p0, Lnow;->ai:I

    .line 275
    return v0
.end method

.method public a(Loxn;)Lnow;
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 283
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 284
    sparse-switch v0, :sswitch_data_0

    .line 288
    iget-object v1, p0, Lnow;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 289
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnow;->ah:Ljava/util/List;

    .line 292
    :cond_1
    iget-object v1, p0, Lnow;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 294
    :sswitch_0
    return-object p0

    .line 299
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 300
    if-eqz v0, :cond_2

    if-eq v0, v3, :cond_2

    if-eq v0, v4, :cond_2

    if-eq v0, v5, :cond_2

    if-eq v0, v6, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa

    if-eq v0, v1, :cond_2

    const/16 v1, 0xb

    if-eq v0, v1, :cond_2

    const/16 v1, 0xc

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd

    if-eq v0, v1, :cond_2

    const/16 v1, 0xe

    if-eq v0, v1, :cond_2

    const/16 v1, 0xf

    if-eq v0, v1, :cond_2

    const/16 v1, 0x10

    if-eq v0, v1, :cond_2

    const/16 v1, 0x11

    if-eq v0, v1, :cond_2

    const/16 v1, 0x12

    if-eq v0, v1, :cond_2

    const/16 v1, 0x13

    if-eq v0, v1, :cond_2

    const/16 v1, 0x14

    if-eq v0, v1, :cond_2

    const/16 v1, 0x15

    if-eq v0, v1, :cond_2

    const/16 v1, 0x16

    if-eq v0, v1, :cond_2

    const/16 v1, 0x17

    if-ne v0, v1, :cond_3

    .line 324
    :cond_2
    iput v0, p0, Lnow;->a:I

    goto :goto_0

    .line 326
    :cond_3
    iput v2, p0, Lnow;->a:I

    goto :goto_0

    .line 331
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 332
    if-eqz v0, :cond_4

    if-eq v0, v3, :cond_4

    if-eq v0, v4, :cond_4

    if-eq v0, v5, :cond_4

    if-eq v0, v6, :cond_4

    const/4 v1, 0x5

    if-eq v0, v1, :cond_4

    const/4 v1, 0x6

    if-eq v0, v1, :cond_4

    const/4 v1, 0x7

    if-eq v0, v1, :cond_4

    const/16 v1, 0x8

    if-eq v0, v1, :cond_4

    const/16 v1, 0x9

    if-eq v0, v1, :cond_4

    const/16 v1, 0xa

    if-eq v0, v1, :cond_4

    const/16 v1, 0xb

    if-eq v0, v1, :cond_4

    const/16 v1, 0xc

    if-eq v0, v1, :cond_4

    const/16 v1, 0xd

    if-eq v0, v1, :cond_4

    const/16 v1, 0xe

    if-eq v0, v1, :cond_4

    const/16 v1, 0xf

    if-eq v0, v1, :cond_4

    const/16 v1, 0x10

    if-eq v0, v1, :cond_4

    const/16 v1, 0x11

    if-eq v0, v1, :cond_4

    const/16 v1, 0x12

    if-eq v0, v1, :cond_4

    const/16 v1, 0x13

    if-eq v0, v1, :cond_4

    const/16 v1, 0x14

    if-eq v0, v1, :cond_4

    const/16 v1, 0x15

    if-eq v0, v1, :cond_4

    const/16 v1, 0x16

    if-eq v0, v1, :cond_4

    const/16 v1, 0x17

    if-ne v0, v1, :cond_5

    .line 356
    :cond_4
    iput v0, p0, Lnow;->b:I

    goto/16 :goto_0

    .line 358
    :cond_5
    iput v2, p0, Lnow;->b:I

    goto/16 :goto_0

    .line 284
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 252
    iget v0, p0, Lnow;->a:I

    if-eq v0, v2, :cond_0

    .line 253
    const/4 v0, 0x1

    iget v1, p0, Lnow;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 255
    :cond_0
    iget v0, p0, Lnow;->b:I

    if-eq v0, v2, :cond_1

    .line 256
    const/4 v0, 0x2

    iget v1, p0, Lnow;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 258
    :cond_1
    iget-object v0, p0, Lnow;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 260
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 210
    invoke-virtual {p0, p1}, Lnow;->a(Loxn;)Lnow;

    move-result-object v0

    return-object v0
.end method
