.class public final Ldhg;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lejk;


# instance fields
.field private final a:Lepz;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Lepz;

    invoke-direct {v0}, Lepz;-><init>()V

    iput-object v0, p0, Ldhg;->a:Lepz;

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)[Landroid/net/Uri;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 26
    iget-object v0, p0, Ldhg;->a:Lepz;

    invoke-virtual {v0, p1}, Lepz;->a(Landroid/content/Context;)[Landroid/net/Uri;

    move-result-object v0

    .line 27
    if-eqz v0, :cond_0

    .line 52
    :goto_0
    return-object v0

    .line 31
    :cond_0
    const-class v0, Lder;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lder;

    invoke-virtual {v0}, Lder;->a()Ldeo;

    move-result-object v0

    .line 32
    if-eqz v0, :cond_1

    .line 33
    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 34
    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v1

    invoke-interface {v1}, Lddl;->a()Lizu;

    move-result-object v1

    if-nez v1, :cond_2

    .line 35
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 38
    :cond_2
    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    invoke-interface {v0}, Lddl;->a()Lizu;

    move-result-object v1

    .line 39
    new-array v0, v4, [Landroid/net/Uri;

    .line 40
    invoke-virtual {v1}, Lizu;->i()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 41
    invoke-virtual {v1}, Lizu;->e()Landroid/net/Uri;

    move-result-object v1

    aput-object v1, v0, v3

    goto :goto_0

    .line 43
    :cond_3
    invoke-virtual {v1}, Lizu;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lizu;->g()Ljac;

    move-result-object v1

    invoke-static {p1, v2, v1}, Lcom/google/android/apps/photos/content/GooglePhotosImageProvider;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Landroid/net/Uri;

    move-result-object v1

    aput-object v1, v0, v3

    .line 48
    const-string v1, "com.android.bluetooth"

    aget-object v2, v0, v3

    invoke-virtual {p1, v1, v2, v4}, Landroid/content/Context;->grantUriPermission(Ljava/lang/String;Landroid/net/Uri;I)V

    goto :goto_0
.end method
