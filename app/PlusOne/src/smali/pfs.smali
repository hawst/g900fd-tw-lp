.class public final Lpfs;
.super Loxq;
.source "PG"


# static fields
.field public static final a:Loxr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Loxr",
            "<",
            "Lpfs;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:Lpfr;

.field private c:Lpfr;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 108
    const v0, 0x2e5737b

    new-instance v1, Lpft;

    invoke-direct {v1}, Lpft;-><init>()V

    .line 113
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    move-result-object v0

    sput-object v0, Lpfs;->a:Loxr;

    .line 112
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 136
    const/4 v0, 0x0

    .line 137
    iget-object v1, p0, Lpfs;->b:Lpfr;

    if-eqz v1, :cond_0

    .line 138
    const/4 v0, 0x1

    iget-object v1, p0, Lpfs;->b:Lpfr;

    .line 139
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 141
    :cond_0
    iget-object v1, p0, Lpfs;->c:Lpfr;

    if-eqz v1, :cond_1

    .line 142
    const/4 v1, 0x2

    iget-object v2, p0, Lpfs;->c:Lpfr;

    .line 143
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 145
    :cond_1
    iget-object v1, p0, Lpfs;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 146
    iput v0, p0, Lpfs;->ai:I

    .line 147
    return v0
.end method

.method public a(Loxn;)Lpfs;
    .locals 2

    .prologue
    .line 155
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 156
    sparse-switch v0, :sswitch_data_0

    .line 160
    iget-object v1, p0, Lpfs;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 161
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpfs;->ah:Ljava/util/List;

    .line 164
    :cond_1
    iget-object v1, p0, Lpfs;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 166
    :sswitch_0
    return-object p0

    .line 171
    :sswitch_1
    iget-object v0, p0, Lpfs;->b:Lpfr;

    if-nez v0, :cond_2

    .line 172
    new-instance v0, Lpfr;

    invoke-direct {v0}, Lpfr;-><init>()V

    iput-object v0, p0, Lpfs;->b:Lpfr;

    .line 174
    :cond_2
    iget-object v0, p0, Lpfs;->b:Lpfr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 178
    :sswitch_2
    iget-object v0, p0, Lpfs;->c:Lpfr;

    if-nez v0, :cond_3

    .line 179
    new-instance v0, Lpfr;

    invoke-direct {v0}, Lpfr;-><init>()V

    iput-object v0, p0, Lpfs;->c:Lpfr;

    .line 181
    :cond_3
    iget-object v0, p0, Lpfs;->c:Lpfr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 156
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 124
    iget-object v0, p0, Lpfs;->b:Lpfr;

    if-eqz v0, :cond_0

    .line 125
    const/4 v0, 0x1

    iget-object v1, p0, Lpfs;->b:Lpfr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 127
    :cond_0
    iget-object v0, p0, Lpfs;->c:Lpfr;

    if-eqz v0, :cond_1

    .line 128
    const/4 v0, 0x2

    iget-object v1, p0, Lpfs;->c:Lpfr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 130
    :cond_1
    iget-object v0, p0, Lpfs;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 132
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 105
    invoke-virtual {p0, p1}, Lpfs;->a(Loxn;)Lpfs;

    move-result-object v0

    return-object v0
.end method
