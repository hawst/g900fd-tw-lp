.class public final Loeu;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Loeu;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Loeq;

.field private d:I

.field private e:Loes;

.field private f:Loev;

.field private g:Loer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 175
    const/4 v0, 0x0

    new-array v0, v0, [Loeu;

    sput-object v0, Loeu;->a:[Loeu;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 176
    invoke-direct {p0}, Loxq;-><init>()V

    .line 189
    iput-object v1, p0, Loeu;->c:Loeq;

    .line 192
    const/high16 v0, -0x80000000

    iput v0, p0, Loeu;->d:I

    .line 195
    iput-object v1, p0, Loeu;->e:Loes;

    .line 198
    iput-object v1, p0, Loeu;->f:Loev;

    .line 201
    iput-object v1, p0, Loeu;->g:Loer;

    .line 176
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 230
    const/4 v0, 0x0

    .line 231
    iget-object v1, p0, Loeu;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 232
    const/4 v0, 0x1

    iget-object v1, p0, Loeu;->b:Ljava/lang/String;

    .line 233
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 235
    :cond_0
    iget-object v1, p0, Loeu;->c:Loeq;

    if-eqz v1, :cond_1

    .line 236
    const/4 v1, 0x2

    iget-object v2, p0, Loeu;->c:Loeq;

    .line 237
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 239
    :cond_1
    iget v1, p0, Loeu;->d:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_2

    .line 240
    const/4 v1, 0x3

    iget v2, p0, Loeu;->d:I

    .line 241
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 243
    :cond_2
    iget-object v1, p0, Loeu;->e:Loes;

    if-eqz v1, :cond_3

    .line 244
    const/4 v1, 0x4

    iget-object v2, p0, Loeu;->e:Loes;

    .line 245
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 247
    :cond_3
    iget-object v1, p0, Loeu;->f:Loev;

    if-eqz v1, :cond_4

    .line 248
    const/4 v1, 0x5

    iget-object v2, p0, Loeu;->f:Loev;

    .line 249
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 251
    :cond_4
    iget-object v1, p0, Loeu;->g:Loer;

    if-eqz v1, :cond_5

    .line 252
    const/4 v1, 0x6

    iget-object v2, p0, Loeu;->g:Loer;

    .line 253
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 255
    :cond_5
    iget-object v1, p0, Loeu;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 256
    iput v0, p0, Loeu;->ai:I

    .line 257
    return v0
.end method

.method public a(Loxn;)Loeu;
    .locals 2

    .prologue
    .line 265
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 266
    sparse-switch v0, :sswitch_data_0

    .line 270
    iget-object v1, p0, Loeu;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 271
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Loeu;->ah:Ljava/util/List;

    .line 274
    :cond_1
    iget-object v1, p0, Loeu;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 276
    :sswitch_0
    return-object p0

    .line 281
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loeu;->b:Ljava/lang/String;

    goto :goto_0

    .line 285
    :sswitch_2
    iget-object v0, p0, Loeu;->c:Loeq;

    if-nez v0, :cond_2

    .line 286
    new-instance v0, Loeq;

    invoke-direct {v0}, Loeq;-><init>()V

    iput-object v0, p0, Loeu;->c:Loeq;

    .line 288
    :cond_2
    iget-object v0, p0, Loeu;->c:Loeq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 292
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 293
    if-eqz v0, :cond_3

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    const/4 v1, 0x4

    if-ne v0, v1, :cond_4

    .line 298
    :cond_3
    iput v0, p0, Loeu;->d:I

    goto :goto_0

    .line 300
    :cond_4
    const/4 v0, 0x0

    iput v0, p0, Loeu;->d:I

    goto :goto_0

    .line 305
    :sswitch_4
    iget-object v0, p0, Loeu;->e:Loes;

    if-nez v0, :cond_5

    .line 306
    new-instance v0, Loes;

    invoke-direct {v0}, Loes;-><init>()V

    iput-object v0, p0, Loeu;->e:Loes;

    .line 308
    :cond_5
    iget-object v0, p0, Loeu;->e:Loes;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 312
    :sswitch_5
    iget-object v0, p0, Loeu;->f:Loev;

    if-nez v0, :cond_6

    .line 313
    new-instance v0, Loev;

    invoke-direct {v0}, Loev;-><init>()V

    iput-object v0, p0, Loeu;->f:Loev;

    .line 315
    :cond_6
    iget-object v0, p0, Loeu;->f:Loev;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 319
    :sswitch_6
    iget-object v0, p0, Loeu;->g:Loer;

    if-nez v0, :cond_7

    .line 320
    new-instance v0, Loer;

    invoke-direct {v0}, Loer;-><init>()V

    iput-object v0, p0, Loeu;->g:Loer;

    .line 322
    :cond_7
    iget-object v0, p0, Loeu;->g:Loer;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 266
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 206
    iget-object v0, p0, Loeu;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 207
    const/4 v0, 0x1

    iget-object v1, p0, Loeu;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 209
    :cond_0
    iget-object v0, p0, Loeu;->c:Loeq;

    if-eqz v0, :cond_1

    .line 210
    const/4 v0, 0x2

    iget-object v1, p0, Loeu;->c:Loeq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 212
    :cond_1
    iget v0, p0, Loeu;->d:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_2

    .line 213
    const/4 v0, 0x3

    iget v1, p0, Loeu;->d:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 215
    :cond_2
    iget-object v0, p0, Loeu;->e:Loes;

    if-eqz v0, :cond_3

    .line 216
    const/4 v0, 0x4

    iget-object v1, p0, Loeu;->e:Loes;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 218
    :cond_3
    iget-object v0, p0, Loeu;->f:Loev;

    if-eqz v0, :cond_4

    .line 219
    const/4 v0, 0x5

    iget-object v1, p0, Loeu;->f:Loev;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 221
    :cond_4
    iget-object v0, p0, Loeu;->g:Loer;

    if-eqz v0, :cond_5

    .line 222
    const/4 v0, 0x6

    iget-object v1, p0, Loeu;->g:Loer;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 224
    :cond_5
    iget-object v0, p0, Loeu;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 226
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 172
    invoke-virtual {p0, p1}, Loeu;->a(Loxn;)Loeu;

    move-result-object v0

    return-object v0
.end method
