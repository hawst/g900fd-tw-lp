.class public final Lmhd;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lngy;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 33370
    invoke-direct {p0}, Loxq;-><init>()V

    .line 33373
    iput-object v0, p0, Lmhd;->apiHeader:Llyr;

    .line 33376
    iput-object v0, p0, Lmhd;->a:Lngy;

    .line 33370
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 33393
    const/4 v0, 0x0

    .line 33394
    iget-object v1, p0, Lmhd;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 33395
    const/4 v0, 0x1

    iget-object v1, p0, Lmhd;->apiHeader:Llyr;

    .line 33396
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 33398
    :cond_0
    iget-object v1, p0, Lmhd;->a:Lngy;

    if-eqz v1, :cond_1

    .line 33399
    const/4 v1, 0x2

    iget-object v2, p0, Lmhd;->a:Lngy;

    .line 33400
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 33402
    :cond_1
    iget-object v1, p0, Lmhd;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 33403
    iput v0, p0, Lmhd;->ai:I

    .line 33404
    return v0
.end method

.method public a(Loxn;)Lmhd;
    .locals 2

    .prologue
    .line 33412
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 33413
    sparse-switch v0, :sswitch_data_0

    .line 33417
    iget-object v1, p0, Lmhd;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 33418
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmhd;->ah:Ljava/util/List;

    .line 33421
    :cond_1
    iget-object v1, p0, Lmhd;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 33423
    :sswitch_0
    return-object p0

    .line 33428
    :sswitch_1
    iget-object v0, p0, Lmhd;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 33429
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmhd;->apiHeader:Llyr;

    .line 33431
    :cond_2
    iget-object v0, p0, Lmhd;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 33435
    :sswitch_2
    iget-object v0, p0, Lmhd;->a:Lngy;

    if-nez v0, :cond_3

    .line 33436
    new-instance v0, Lngy;

    invoke-direct {v0}, Lngy;-><init>()V

    iput-object v0, p0, Lmhd;->a:Lngy;

    .line 33438
    :cond_3
    iget-object v0, p0, Lmhd;->a:Lngy;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 33413
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 33381
    iget-object v0, p0, Lmhd;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 33382
    const/4 v0, 0x1

    iget-object v1, p0, Lmhd;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 33384
    :cond_0
    iget-object v0, p0, Lmhd;->a:Lngy;

    if-eqz v0, :cond_1

    .line 33385
    const/4 v0, 0x2

    iget-object v1, p0, Lmhd;->a:Lngy;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 33387
    :cond_1
    iget-object v0, p0, Lmhd;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 33389
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 33366
    invoke-virtual {p0, p1}, Lmhd;->a(Loxn;)Lmhd;

    move-result-object v0

    return-object v0
.end method
