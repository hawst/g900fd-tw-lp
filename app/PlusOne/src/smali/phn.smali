.class public final Lphn;
.super Loxq;
.source "PG"


# static fields
.field public static final a:Loxr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Loxr",
            "<",
            "Lphn;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:Ljava/lang/Float;

.field public c:Ljava/lang/Float;

.field public d:Ljava/lang/Integer;

.field public e:Lpxa;

.field public f:Ljava/lang/Float;

.field public g:Ljava/lang/Integer;

.field public h:Ljava/lang/Float;

.field public i:Ljava/lang/Float;

.field public j:Ljava/lang/Float;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 9
    const v0, 0x439a19a

    new-instance v1, Lpho;

    invoke-direct {v1}, Lpho;-><init>()V

    .line 14
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    move-result-object v0

    sput-object v0, Lphn;->a:Loxr;

    .line 13
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 23
    const/4 v0, 0x0

    iput-object v0, p0, Lphn;->e:Lpxa;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 71
    const/4 v0, 0x0

    .line 72
    iget-object v1, p0, Lphn;->b:Ljava/lang/Float;

    if-eqz v1, :cond_0

    .line 73
    const/4 v0, 0x1

    iget-object v1, p0, Lphn;->b:Ljava/lang/Float;

    .line 74
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x0

    .line 76
    :cond_0
    iget-object v1, p0, Lphn;->c:Ljava/lang/Float;

    if-eqz v1, :cond_1

    .line 77
    const/4 v1, 0x2

    iget-object v2, p0, Lphn;->c:Ljava/lang/Float;

    .line 78
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 80
    :cond_1
    iget-object v1, p0, Lphn;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 81
    const/4 v1, 0x3

    iget-object v2, p0, Lphn;->d:Ljava/lang/Integer;

    .line 82
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 84
    :cond_2
    iget-object v1, p0, Lphn;->e:Lpxa;

    if-eqz v1, :cond_3

    .line 85
    const/4 v1, 0x4

    iget-object v2, p0, Lphn;->e:Lpxa;

    .line 86
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 88
    :cond_3
    iget-object v1, p0, Lphn;->f:Ljava/lang/Float;

    if-eqz v1, :cond_4

    .line 89
    const/4 v1, 0x5

    iget-object v2, p0, Lphn;->f:Ljava/lang/Float;

    .line 90
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 92
    :cond_4
    iget-object v1, p0, Lphn;->g:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 93
    const/4 v1, 0x6

    iget-object v2, p0, Lphn;->g:Ljava/lang/Integer;

    .line 94
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 96
    :cond_5
    iget-object v1, p0, Lphn;->h:Ljava/lang/Float;

    if-eqz v1, :cond_6

    .line 97
    const/4 v1, 0x7

    iget-object v2, p0, Lphn;->h:Ljava/lang/Float;

    .line 98
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 100
    :cond_6
    iget-object v1, p0, Lphn;->i:Ljava/lang/Float;

    if-eqz v1, :cond_7

    .line 101
    const/16 v1, 0x8

    iget-object v2, p0, Lphn;->i:Ljava/lang/Float;

    .line 102
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 104
    :cond_7
    iget-object v1, p0, Lphn;->j:Ljava/lang/Float;

    if-eqz v1, :cond_8

    .line 105
    const/16 v1, 0x9

    iget-object v2, p0, Lphn;->j:Ljava/lang/Float;

    .line 106
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 108
    :cond_8
    iget-object v1, p0, Lphn;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 109
    iput v0, p0, Lphn;->ai:I

    .line 110
    return v0
.end method

.method public a(Loxn;)Lphn;
    .locals 2

    .prologue
    .line 118
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 119
    sparse-switch v0, :sswitch_data_0

    .line 123
    iget-object v1, p0, Lphn;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 124
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lphn;->ah:Ljava/util/List;

    .line 127
    :cond_1
    iget-object v1, p0, Lphn;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 129
    :sswitch_0
    return-object p0

    .line 134
    :sswitch_1
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lphn;->b:Ljava/lang/Float;

    goto :goto_0

    .line 138
    :sswitch_2
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lphn;->c:Ljava/lang/Float;

    goto :goto_0

    .line 142
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lphn;->d:Ljava/lang/Integer;

    goto :goto_0

    .line 146
    :sswitch_4
    iget-object v0, p0, Lphn;->e:Lpxa;

    if-nez v0, :cond_2

    .line 147
    new-instance v0, Lpxa;

    invoke-direct {v0}, Lpxa;-><init>()V

    iput-object v0, p0, Lphn;->e:Lpxa;

    .line 149
    :cond_2
    iget-object v0, p0, Lphn;->e:Lpxa;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 153
    :sswitch_5
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lphn;->f:Ljava/lang/Float;

    goto :goto_0

    .line 157
    :sswitch_6
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lphn;->g:Ljava/lang/Integer;

    goto :goto_0

    .line 161
    :sswitch_7
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lphn;->h:Ljava/lang/Float;

    goto :goto_0

    .line 165
    :sswitch_8
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lphn;->i:Ljava/lang/Float;

    goto :goto_0

    .line 169
    :sswitch_9
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lphn;->j:Ljava/lang/Float;

    goto/16 :goto_0

    .line 119
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x15 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2d -> :sswitch_5
        0x30 -> :sswitch_6
        0x3d -> :sswitch_7
        0x45 -> :sswitch_8
        0x4d -> :sswitch_9
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 38
    iget-object v0, p0, Lphn;->b:Ljava/lang/Float;

    if-eqz v0, :cond_0

    .line 39
    const/4 v0, 0x1

    iget-object v1, p0, Lphn;->b:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 41
    :cond_0
    iget-object v0, p0, Lphn;->c:Ljava/lang/Float;

    if-eqz v0, :cond_1

    .line 42
    const/4 v0, 0x2

    iget-object v1, p0, Lphn;->c:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 44
    :cond_1
    iget-object v0, p0, Lphn;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 45
    const/4 v0, 0x3

    iget-object v1, p0, Lphn;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 47
    :cond_2
    iget-object v0, p0, Lphn;->e:Lpxa;

    if-eqz v0, :cond_3

    .line 48
    const/4 v0, 0x4

    iget-object v1, p0, Lphn;->e:Lpxa;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 50
    :cond_3
    iget-object v0, p0, Lphn;->f:Ljava/lang/Float;

    if-eqz v0, :cond_4

    .line 51
    const/4 v0, 0x5

    iget-object v1, p0, Lphn;->f:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 53
    :cond_4
    iget-object v0, p0, Lphn;->g:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 54
    const/4 v0, 0x6

    iget-object v1, p0, Lphn;->g:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 56
    :cond_5
    iget-object v0, p0, Lphn;->h:Ljava/lang/Float;

    if-eqz v0, :cond_6

    .line 57
    const/4 v0, 0x7

    iget-object v1, p0, Lphn;->h:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 59
    :cond_6
    iget-object v0, p0, Lphn;->i:Ljava/lang/Float;

    if-eqz v0, :cond_7

    .line 60
    const/16 v0, 0x8

    iget-object v1, p0, Lphn;->i:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 62
    :cond_7
    iget-object v0, p0, Lphn;->j:Ljava/lang/Float;

    if-eqz v0, :cond_8

    .line 63
    const/16 v0, 0x9

    iget-object v1, p0, Lphn;->j:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 65
    :cond_8
    iget-object v0, p0, Lphn;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 67
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lphn;->a(Loxn;)Lphn;

    move-result-object v0

    return-object v0
.end method
