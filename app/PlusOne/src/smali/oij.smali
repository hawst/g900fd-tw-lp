.class public final Loij;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Loij;


# instance fields
.field public b:Lohn;

.field public c:Ljava/lang/Boolean;

.field private d:I

.field private e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2339
    const/4 v0, 0x0

    new-array v0, v0, [Loij;

    sput-object v0, Loij;->a:[Loij;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2340
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2343
    const/4 v0, 0x0

    iput-object v0, p0, Loij;->b:Lohn;

    .line 2346
    const/high16 v0, -0x80000000

    iput v0, p0, Loij;->d:I

    .line 2340
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 2373
    const/4 v0, 0x0

    .line 2374
    iget v1, p0, Loij;->d:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 2375
    const/4 v0, 0x1

    iget v1, p0, Loij;->d:I

    .line 2376
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2378
    :cond_0
    iget-object v1, p0, Loij;->e:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 2379
    const/4 v1, 0x2

    iget-object v2, p0, Loij;->e:Ljava/lang/String;

    .line 2380
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2382
    :cond_1
    iget-object v1, p0, Loij;->b:Lohn;

    if-eqz v1, :cond_2

    .line 2383
    const/4 v1, 0x3

    iget-object v2, p0, Loij;->b:Lohn;

    .line 2384
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2386
    :cond_2
    iget-object v1, p0, Loij;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 2387
    const/4 v1, 0x4

    iget-object v2, p0, Loij;->c:Ljava/lang/Boolean;

    .line 2388
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2390
    :cond_3
    iget-object v1, p0, Loij;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2391
    iput v0, p0, Loij;->ai:I

    .line 2392
    return v0
.end method

.method public a(Loxn;)Loij;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2400
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2401
    sparse-switch v0, :sswitch_data_0

    .line 2405
    iget-object v1, p0, Loij;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 2406
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Loij;->ah:Ljava/util/List;

    .line 2409
    :cond_1
    iget-object v1, p0, Loij;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2411
    :sswitch_0
    return-object p0

    .line 2416
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 2417
    if-eq v0, v2, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-ne v0, v1, :cond_3

    .line 2422
    :cond_2
    iput v0, p0, Loij;->d:I

    goto :goto_0

    .line 2424
    :cond_3
    iput v2, p0, Loij;->d:I

    goto :goto_0

    .line 2429
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loij;->e:Ljava/lang/String;

    goto :goto_0

    .line 2433
    :sswitch_3
    iget-object v0, p0, Loij;->b:Lohn;

    if-nez v0, :cond_4

    .line 2434
    new-instance v0, Lohn;

    invoke-direct {v0}, Lohn;-><init>()V

    iput-object v0, p0, Loij;->b:Lohn;

    .line 2436
    :cond_4
    iget-object v0, p0, Loij;->b:Lohn;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2440
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Loij;->c:Ljava/lang/Boolean;

    goto :goto_0

    .line 2401
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 2355
    iget v0, p0, Loij;->d:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 2356
    const/4 v0, 0x1

    iget v1, p0, Loij;->d:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2358
    :cond_0
    iget-object v0, p0, Loij;->e:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2359
    const/4 v0, 0x2

    iget-object v1, p0, Loij;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 2361
    :cond_1
    iget-object v0, p0, Loij;->b:Lohn;

    if-eqz v0, :cond_2

    .line 2362
    const/4 v0, 0x3

    iget-object v1, p0, Loij;->b:Lohn;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2364
    :cond_2
    iget-object v0, p0, Loij;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 2365
    const/4 v0, 0x4

    iget-object v1, p0, Loij;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 2367
    :cond_3
    iget-object v0, p0, Loij;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2369
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2336
    invoke-virtual {p0, p1}, Loij;->a(Loxn;)Loij;

    move-result-object v0

    return-object v0
.end method
