.class public final Ldxr;
.super Lepp;
.source "PG"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field private final d:Lepn;

.field private e:Z

.field private f:Z

.field private g:Lu;

.field private h:Ljava/lang/String;

.field private i:Landroid/widget/RadioButton;

.field private j:Landroid/widget/RadioButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILept;Lu;)V
    .locals 6

    .prologue
    .line 51
    sget-object v5, Lepn;->d:Lepn;

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Ldxr;-><init>(Landroid/content/Context;ILept;Lu;Lepn;)V

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILept;Lu;Lepn;)V
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lepp;-><init>(Landroid/content/Context;ILept;Z)V

    .line 44
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldxr;->f:Z

    .line 57
    iput-object p4, p0, Ldxr;->g:Lu;

    .line 58
    iput-object p5, p0, Ldxr;->d:Lepn;

    .line 59
    return-void
.end method

.method static synthetic a(Ldxr;Lhmn;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1}, Ldxr;->a(Lhmn;)V

    return-void
.end method

.method private a(Lhmn;)V
    .locals 5

    .prologue
    .line 99
    iget-object v0, p0, Ldxr;->c:Landroid/content/Context;

    const/4 v1, 0x4

    new-instance v2, Lhml;

    invoke-direct {v2}, Lhml;-><init>()V

    new-instance v3, Lhmk;

    invoke-direct {v3, p1}, Lhmk;-><init>(Lhmn;)V

    .line 100
    invoke-virtual {v2, v3}, Lhml;->a(Lhmk;)Lhml;

    move-result-object v2

    new-instance v3, Lhmk;

    sget-object v4, Lomt;->c:Lhmn;

    invoke-direct {v3, v4}, Lhmk;-><init>(Lhmn;)V

    .line 101
    invoke-virtual {v2, v3}, Lhml;->a(Lhmk;)Lhml;

    move-result-object v2

    new-instance v3, Lhmk;

    sget-object v4, Lond;->a:Lhmn;

    invoke-direct {v3, v4}, Lhmk;-><init>(Lhmn;)V

    .line 102
    invoke-virtual {v2, v3}, Lhml;->a(Lhmk;)Lhml;

    move-result-object v2

    .line 99
    invoke-static {v0, v1, v2}, Lhly;->a(Landroid/content/Context;ILhml;)V

    .line 103
    return-void
.end method

.method static synthetic a(Ldxr;)Z
    .locals 1

    .prologue
    .line 38
    iget-boolean v0, p0, Ldxr;->e:Z

    return v0
.end method

.method static synthetic b(Ldxr;)Z
    .locals 1

    .prologue
    .line 38
    iget-boolean v0, p0, Ldxr;->f:Z

    return v0
.end method

.method static synthetic c(Ldxr;)Landroid/widget/RadioButton;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Ldxr;->j:Landroid/widget/RadioButton;

    return-object v0
.end method

.method static synthetic d(Ldxr;)Landroid/widget/RadioButton;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Ldxr;->i:Landroid/widget/RadioButton;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ldxr;
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Ldxr;->h:Ljava/lang/String;

    .line 63
    return-object p0
.end method

.method public a()V
    .locals 7

    .prologue
    .line 74
    new-instance v0, Ldxv;

    iget-object v1, p0, Ldxr;->g:Lu;

    iget v2, p0, Ldxr;->b:I

    const-string v3, "dialog_sync_disabled"

    iget-boolean v4, p0, Ldxr;->e:Z

    iget-boolean v5, p0, Ldxr;->f:Z

    new-instance v6, Ldxs;

    invoke-direct {v6, p0}, Ldxs;-><init>(Ldxr;)V

    invoke-direct/range {v0 .. v6}, Ldxv;-><init>(Lu;ILjava/lang/String;ZZLdxy;)V

    .line 89
    invoke-virtual {v0}, Ldxv;->a()V

    .line 90
    return-void
.end method

.method protected a(Landroid/view/View;)V
    .locals 10

    .prologue
    const v9, 0x7f1001b7

    const v8, 0x7f1001b6

    const/16 v7, 0x8

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 117
    iget-object v0, p0, Ldxr;->c:Landroid/content/Context;

    const v1, 0x7f0a060f

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Ldxr;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 118
    const v0, 0x7f0a0619

    new-array v1, v2, [Ljava/lang/Object;

    iget-object v4, p0, Ldxr;->c:Landroid/content/Context;

    const-string v5, "auto_backup"

    .line 120
    const-string v6, "https://support.google.com/plus/?hl=%locale%"

    invoke-static {v4, v5, v6}, Litk;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    .line 118
    invoke-virtual {p0, p1, v0, v1}, Ldxr;->a(Landroid/view/View;I[Ljava/lang/Object;)V

    .line 123
    const v0, 0x7f1001b3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 124
    iget-object v1, p0, Ldxr;->h:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 125
    iget-object v1, p0, Ldxr;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 128
    :cond_0
    iget-object v0, p0, Ldxr;->c:Landroid/content/Context;

    const-class v1, Ljgn;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljgn;

    .line 129
    invoke-interface {v0}, Ljgn;->e()Z

    move-result v0

    if-nez v0, :cond_7

    move v4, v2

    .line 130
    :goto_0
    if-nez v4, :cond_8

    iget-object v0, p0, Ldxr;->c:Landroid/content/Context;

    const-class v1, Lieh;

    .line 131
    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    sget-object v1, Ldxd;->v:Lief;

    iget v5, p0, Ldxr;->b:I

    invoke-interface {v0, v1, v5}, Lieh;->b(Lief;I)Z

    move-result v0

    if-eqz v0, :cond_8

    move v1, v2

    .line 134
    :goto_1
    invoke-virtual {p1, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Ldxr;->i:Landroid/widget/RadioButton;

    .line 135
    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Ldxr;->j:Landroid/widget/RadioButton;

    .line 137
    if-nez v4, :cond_1

    if-eqz v1, :cond_2

    .line 139
    :cond_1
    iget-object v0, p0, Ldxr;->i:Landroid/widget/RadioButton;

    invoke-virtual {v0, v7}, Landroid/widget/RadioButton;->setVisibility(I)V

    .line 140
    iget-object v0, p0, Ldxr;->j:Landroid/widget/RadioButton;

    invoke-virtual {v0, v7}, Landroid/widget/RadioButton;->setVisibility(I)V

    .line 143
    :cond_2
    if-nez v4, :cond_3

    if-nez v1, :cond_4

    .line 146
    :cond_3
    const v0, 0x7f1001b5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v5, 0x7f0a0604

    .line 147
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(I)V

    .line 151
    :cond_4
    if-nez v1, :cond_9

    if-nez v4, :cond_5

    iget-boolean v0, p0, Ldxr;->e:Z

    if-eqz v0, :cond_9

    .line 152
    :cond_5
    iget-object v0, p0, Ldxr;->i:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 158
    :goto_2
    iget-object v0, p0, Ldxr;->i:Landroid/widget/RadioButton;

    new-instance v1, Ldxt;

    invoke-direct {v1, p0}, Ldxt;-><init>(Ldxr;)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 167
    iget-object v0, p0, Ldxr;->j:Landroid/widget/RadioButton;

    new-instance v1, Ldxu;

    invoke-direct {v1, p0}, Ldxu;-><init>(Ldxr;)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 176
    const v0, 0x7f1001b9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 178
    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 179
    iget-boolean v1, p0, Ldxr;->f:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 181
    invoke-virtual {p0, p1, v9}, Ldxr;->a(Landroid/view/View;I)V

    .line 182
    invoke-virtual {p0, p1, v8}, Ldxr;->a(Landroid/view/View;I)V

    .line 186
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x11

    if-lt v1, v2, :cond_6

    .line 187
    iget-object v1, p0, Ldxr;->c:Landroid/content/Context;

    .line 188
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0371

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 189
    invoke-virtual {v0, v1, v3, v1, v3}, Landroid/widget/CheckBox;->setPadding(IIII)V

    .line 190
    iget-object v0, p0, Ldxr;->i:Landroid/widget/RadioButton;

    invoke-virtual {v0, v1, v3, v1, v3}, Landroid/widget/RadioButton;->setPadding(IIII)V

    .line 191
    iget-object v0, p0, Ldxr;->j:Landroid/widget/RadioButton;

    invoke-virtual {v0, v1, v3, v1, v3}, Landroid/widget/RadioButton;->setPadding(IIII)V

    .line 193
    :cond_6
    return-void

    :cond_7
    move v4, v3

    .line 129
    goto/16 :goto_0

    :cond_8
    move v1, v3

    .line 131
    goto/16 :goto_1

    .line 154
    :cond_9
    iget-object v0, p0, Ldxr;->j:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_2
.end method

.method public a(Lnyq;)Z
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Ldxr;->c:Landroid/content/Context;

    invoke-static {v0}, Lhqd;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 69
    invoke-super {p0, p1}, Lepp;->a(Lnyq;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ldxr;->g()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 94
    invoke-virtual {p0}, Ldxr;->f()V

    .line 95
    sget-object v0, Lomt;->a:Lhmn;

    invoke-direct {p0, v0}, Ldxr;->a(Lhmn;)V

    .line 96
    return-void
.end method

.method public d()I
    .locals 1

    .prologue
    .line 197
    const v0, 0x7f040052

    return v0
.end method

.method public e()Lepn;
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Ldxr;->d:Lepn;

    return-object v0
.end method

.method protected f()V
    .locals 4

    .prologue
    .line 108
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 109
    invoke-static {}, Lhsb;->a()J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 110
    iget-object v2, p0, Ldxr;->c:Landroid/content/Context;

    invoke-static {v2, v0, v1}, Lhsb;->a(Landroid/content/Context;J)V

    .line 112
    invoke-super {p0}, Lepp;->f()V

    .line 113
    return-void
.end method

.method public g()Z
    .locals 3

    .prologue
    .line 227
    iget-object v0, p0, Ldxr;->c:Landroid/content/Context;

    const-class v1, Lhei;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iget v1, p0, Ldxr;->b:I

    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 228
    const-string v1, "is_plus_page"

    invoke-interface {v0, v1}, Lhej;->c(Ljava/lang/String;)Z

    move-result v1

    .line 229
    iget-object v0, p0, Ldxr;->c:Landroid/content/Context;

    const-class v2, Lhpu;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhpu;

    .line 232
    if-nez v1, :cond_0

    invoke-virtual {v0}, Lhpu;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()V
    .locals 3

    .prologue
    .line 239
    iget-object v0, p0, Ldxr;->c:Landroid/content/Context;

    iget v1, p0, Ldxr;->b:I

    sget-object v2, Lepn;->j:Lepn;

    invoke-static {v0, v1, v2}, Ldhv;->c(Landroid/content/Context;ILepn;)V

    .line 240
    return-void
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .prologue
    .line 219
    iput-boolean p2, p0, Ldxr;->f:Z

    .line 220
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 207
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f1001b6

    if-ne v0, v1, :cond_1

    .line 208
    iget-object v0, p0, Ldxr;->j:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Ldxr;->e:Z

    .line 214
    :goto_1
    return-void

    .line 208
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 209
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f1001b7

    if-ne v0, v1, :cond_2

    .line 210
    iget-object v0, p0, Ldxr;->i:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v0

    iput-boolean v0, p0, Ldxr;->e:Z

    goto :goto_1

    .line 212
    :cond_2
    invoke-super {p0, p1}, Lepp;->onClick(Landroid/view/View;)V

    goto :goto_1
.end method
