.class public final Lpbb;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lpbb;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1229
    const/4 v0, 0x0

    new-array v0, v0, [Lpbb;

    sput-object v0, Lpbb;->a:[Lpbb;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1230
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1251
    const/4 v0, 0x0

    .line 1252
    iget-object v1, p0, Lpbb;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1253
    const/4 v0, 0x1

    iget-object v1, p0, Lpbb;->b:Ljava/lang/String;

    .line 1254
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1256
    :cond_0
    iget-object v1, p0, Lpbb;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 1257
    const/4 v1, 0x2

    iget-object v2, p0, Lpbb;->c:Ljava/lang/Integer;

    .line 1258
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1260
    :cond_1
    iget-object v1, p0, Lpbb;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1261
    iput v0, p0, Lpbb;->ai:I

    .line 1262
    return v0
.end method

.method public a(Loxn;)Lpbb;
    .locals 2

    .prologue
    .line 1270
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1271
    sparse-switch v0, :sswitch_data_0

    .line 1275
    iget-object v1, p0, Lpbb;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1276
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpbb;->ah:Ljava/util/List;

    .line 1279
    :cond_1
    iget-object v1, p0, Lpbb;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1281
    :sswitch_0
    return-object p0

    .line 1286
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpbb;->b:Ljava/lang/String;

    goto :goto_0

    .line 1290
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lpbb;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 1271
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1239
    iget-object v0, p0, Lpbb;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1240
    const/4 v0, 0x1

    iget-object v1, p0, Lpbb;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1242
    :cond_0
    iget-object v0, p0, Lpbb;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 1243
    const/4 v0, 0x2

    iget-object v1, p0, Lpbb;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1245
    :cond_1
    iget-object v0, p0, Lpbb;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1247
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1226
    invoke-virtual {p0, p1}, Lpbb;->a(Loxn;)Lpbb;

    move-result-object v0

    return-object v0
.end method
