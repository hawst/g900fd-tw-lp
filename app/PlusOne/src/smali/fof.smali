.class public final Lfof;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lfof;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Ljava/lang/String;

.field private b:I

.field private c:I

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Z

.field private l:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 193
    new-instance v0, Lfog;

    invoke-direct {v0}, Lfog;-><init>()V

    sput-object v0, Lfof;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 178
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 179
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfof;->a:Ljava/lang/String;

    .line 180
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lfof;->b:I

    .line 181
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lfof;->c:I

    .line 182
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfof;->d:Ljava/lang/String;

    .line 183
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfof;->e:Ljava/lang/String;

    .line 184
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfof;->f:Ljava/lang/String;

    .line 185
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfof;->g:Ljava/lang/String;

    .line 186
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfof;->h:Ljava/lang/String;

    .line 187
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfof;->i:Ljava/lang/String;

    .line 188
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfof;->j:Ljava/lang/String;

    .line 189
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lfof;->k:Z

    .line 190
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-lez v0, :cond_1

    :goto_1
    iput-boolean v1, p0, Lfof;->l:Z

    .line 191
    return-void

    :cond_0
    move v0, v2

    .line 189
    goto :goto_0

    :cond_1
    move v1, v2

    .line 190
    goto :goto_1
.end method

.method public constructor <init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lfof;->a:Ljava/lang/String;

    .line 27
    iput p2, p0, Lfof;->b:I

    .line 28
    iput p3, p0, Lfof;->c:I

    .line 29
    iput-object p4, p0, Lfof;->d:Ljava/lang/String;

    .line 30
    iput-object p5, p0, Lfof;->e:Ljava/lang/String;

    .line 31
    iput-object p6, p0, Lfof;->f:Ljava/lang/String;

    .line 32
    iput-object p7, p0, Lfof;->g:Ljava/lang/String;

    .line 33
    iput-object p8, p0, Lfof;->h:Ljava/lang/String;

    .line 34
    iput-object p9, p0, Lfof;->i:Ljava/lang/String;

    .line 35
    iput-object p10, p0, Lfof;->j:Ljava/lang/String;

    .line 36
    iput-boolean p11, p0, Lfof;->k:Z

    .line 37
    iput-boolean p12, p0, Lfof;->l:Z

    .line 38
    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 207
    if-nez p0, :cond_1

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lfof;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 45
    iget v0, p0, Lfof;->b:I

    return v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lfof;->c:I

    return v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lfof;->d:Ljava/lang/String;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x0

    return v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lfof;->e:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 126
    instance-of v1, p1, Lfof;

    if-nez v1, :cond_1

    .line 141
    :cond_0
    :goto_0
    return v0

    .line 129
    :cond_1
    check-cast p1, Lfof;

    .line 130
    iget-object v1, p0, Lfof;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lfof;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lfof;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lfof;->b:I

    .line 131
    invoke-virtual {p1}, Lfof;->b()I

    move-result v2

    if-ne v1, v2, :cond_0

    iget v1, p0, Lfof;->c:I

    .line 132
    invoke-virtual {p1}, Lfof;->c()I

    move-result v2

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lfof;->d:Ljava/lang/String;

    .line 133
    invoke-virtual {p1}, Lfof;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lfof;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lfof;->e:Ljava/lang/String;

    .line 134
    invoke-virtual {p1}, Lfof;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lfof;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lfof;->f:Ljava/lang/String;

    .line 135
    invoke-virtual {p1}, Lfof;->f()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lfof;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lfof;->g:Ljava/lang/String;

    .line 136
    invoke-virtual {p1}, Lfof;->g()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lfof;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lfof;->h:Ljava/lang/String;

    .line 137
    invoke-virtual {p1}, Lfof;->h()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lfof;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lfof;->i:Ljava/lang/String;

    .line 138
    invoke-virtual {p1}, Lfof;->i()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lfof;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lfof;->j:Ljava/lang/String;

    .line 139
    invoke-virtual {p1}, Lfof;->j()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lfof;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lfof;->k:Z

    .line 140
    invoke-virtual {p1}, Lfof;->k()Z

    move-result v2

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lfof;->l:Z

    .line 141
    invoke-virtual {p1}, Lfof;->l()Z

    move-result v2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lfof;->f:Ljava/lang/String;

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lfof;->g:Ljava/lang/String;

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lfof;->h:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 146
    const/16 v0, 0x11

    .line 147
    iget-object v3, p0, Lfof;->a:Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 148
    iget-object v0, p0, Lfof;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 150
    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lfof;->b:I

    add-int/2addr v0, v3

    .line 151
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lfof;->c:I

    add-int/2addr v0, v3

    .line 152
    iget-object v3, p0, Lfof;->d:Ljava/lang/String;

    if-eqz v3, :cond_1

    .line 153
    mul-int/lit8 v0, v0, 0x1f

    iget-object v3, p0, Lfof;->d:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    add-int/2addr v0, v3

    .line 155
    :cond_1
    iget-object v3, p0, Lfof;->e:Ljava/lang/String;

    if-eqz v3, :cond_2

    .line 156
    mul-int/lit8 v0, v0, 0x1f

    iget-object v3, p0, Lfof;->e:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    add-int/2addr v0, v3

    .line 158
    :cond_2
    iget-object v3, p0, Lfof;->f:Ljava/lang/String;

    if-eqz v3, :cond_3

    .line 159
    mul-int/lit8 v0, v0, 0x1f

    iget-object v3, p0, Lfof;->f:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    add-int/2addr v0, v3

    .line 161
    :cond_3
    iget-object v3, p0, Lfof;->g:Ljava/lang/String;

    if-eqz v3, :cond_4

    .line 162
    mul-int/lit8 v0, v0, 0x1f

    iget-object v3, p0, Lfof;->g:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    add-int/2addr v0, v3

    .line 164
    :cond_4
    iget-object v3, p0, Lfof;->h:Ljava/lang/String;

    if-eqz v3, :cond_5

    .line 165
    mul-int/lit8 v0, v0, 0x1f

    iget-object v3, p0, Lfof;->h:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    add-int/2addr v0, v3

    .line 167
    :cond_5
    iget-object v3, p0, Lfof;->i:Ljava/lang/String;

    if-eqz v3, :cond_6

    .line 168
    mul-int/lit8 v0, v0, 0x1f

    iget-object v3, p0, Lfof;->i:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    add-int/2addr v0, v3

    .line 170
    :cond_6
    iget-object v3, p0, Lfof;->j:Ljava/lang/String;

    if-eqz v3, :cond_7

    .line 171
    mul-int/lit8 v0, v0, 0x1f

    iget-object v3, p0, Lfof;->j:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    add-int/2addr v0, v3

    .line 173
    :cond_7
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lfof;->k:Z

    if-eqz v0, :cond_8

    move v0, v1

    :goto_0
    add-int/2addr v0, v3

    .line 174
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v3, p0, Lfof;->l:Z

    if-eqz v3, :cond_9

    :goto_1
    add-int/2addr v0, v1

    .line 175
    return v0

    :cond_8
    move v0, v2

    .line 173
    goto :goto_0

    :cond_9
    move v1, v2

    .line 174
    goto :goto_1
.end method

.method public i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lfof;->i:Ljava/lang/String;

    return-object v0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lfof;->j:Ljava/lang/String;

    return-object v0
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 81
    iget-boolean v0, p0, Lfof;->k:Z

    return v0
.end method

.method public l()Z
    .locals 1

    .prologue
    .line 85
    iget-boolean v0, p0, Lfof;->l:Z

    return v0
.end method

.method public m()Z
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lfof;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfof;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public n()Z
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lfof;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfof;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public o()Z
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lfof;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 110
    iget-object v0, p0, Lfof;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 111
    iget v0, p0, Lfof;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 112
    iget v0, p0, Lfof;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 113
    iget-object v0, p0, Lfof;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 114
    iget-object v0, p0, Lfof;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 115
    iget-object v0, p0, Lfof;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 116
    iget-object v0, p0, Lfof;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 117
    iget-object v0, p0, Lfof;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 118
    iget-object v0, p0, Lfof;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 119
    iget-object v0, p0, Lfof;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 120
    iget-boolean v0, p0, Lfof;->k:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 121
    iget-boolean v0, p0, Lfof;->l:Z

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 122
    return-void

    :cond_0
    move v0, v2

    .line 120
    goto :goto_0

    :cond_1
    move v1, v2

    .line 121
    goto :goto_1
.end method
