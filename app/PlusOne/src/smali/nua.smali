.class public final Lnua;
.super Loxq;
.source "PG"


# instance fields
.field public a:[I

.field private b:Logu;

.field private c:Lnuf;

.field private d:Lntz;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 125
    invoke-direct {p0}, Loxq;-><init>()V

    .line 128
    sget-object v0, Loxx;->a:[I

    iput-object v0, p0, Lnua;->a:[I

    .line 131
    iput-object v1, p0, Lnua;->b:Logu;

    .line 134
    iput-object v1, p0, Lnua;->c:Lnuf;

    .line 137
    iput-object v1, p0, Lnua;->d:Lntz;

    .line 125
    return-void
.end method


# virtual methods
.method public a()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 162
    .line 163
    iget-object v1, p0, Lnua;->a:[I

    if-eqz v1, :cond_1

    iget-object v1, p0, Lnua;->a:[I

    array-length v1, v1

    if-lez v1, :cond_1

    .line 165
    iget-object v2, p0, Lnua;->a:[I

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v0, v3, :cond_0

    aget v4, v2, v0

    .line 167
    invoke-static {v4}, Loxo;->i(I)I

    move-result v4

    add-int/2addr v1, v4

    .line 165
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 170
    :cond_0
    iget-object v0, p0, Lnua;->a:[I

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v1

    .line 172
    :cond_1
    iget-object v1, p0, Lnua;->b:Logu;

    if-eqz v1, :cond_2

    .line 173
    const/4 v1, 0x2

    iget-object v2, p0, Lnua;->b:Logu;

    .line 174
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 176
    :cond_2
    iget-object v1, p0, Lnua;->c:Lnuf;

    if-eqz v1, :cond_3

    .line 177
    const/4 v1, 0x3

    iget-object v2, p0, Lnua;->c:Lnuf;

    .line 178
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 180
    :cond_3
    iget-object v1, p0, Lnua;->d:Lntz;

    if-eqz v1, :cond_4

    .line 181
    const/4 v1, 0x4

    iget-object v2, p0, Lnua;->d:Lntz;

    .line 182
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 184
    :cond_4
    iget-object v1, p0, Lnua;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 185
    iput v0, p0, Lnua;->ai:I

    .line 186
    return v0
.end method

.method public a(Loxn;)Lnua;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 194
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 195
    sparse-switch v0, :sswitch_data_0

    .line 199
    iget-object v1, p0, Lnua;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 200
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnua;->ah:Ljava/util/List;

    .line 203
    :cond_1
    iget-object v1, p0, Lnua;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 205
    :sswitch_0
    return-object p0

    .line 210
    :sswitch_1
    const/16 v0, 0x8

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 211
    iget-object v0, p0, Lnua;->a:[I

    array-length v0, v0

    .line 212
    add-int/2addr v1, v0

    new-array v1, v1, [I

    .line 213
    iget-object v2, p0, Lnua;->a:[I

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 214
    iput-object v1, p0, Lnua;->a:[I

    .line 215
    :goto_1
    iget-object v1, p0, Lnua;->a:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 216
    iget-object v1, p0, Lnua;->a:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    .line 217
    invoke-virtual {p1}, Loxn;->a()I

    .line 215
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 220
    :cond_2
    iget-object v1, p0, Lnua;->a:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    goto :goto_0

    .line 224
    :sswitch_2
    iget-object v0, p0, Lnua;->b:Logu;

    if-nez v0, :cond_3

    .line 225
    new-instance v0, Logu;

    invoke-direct {v0}, Logu;-><init>()V

    iput-object v0, p0, Lnua;->b:Logu;

    .line 227
    :cond_3
    iget-object v0, p0, Lnua;->b:Logu;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 231
    :sswitch_3
    iget-object v0, p0, Lnua;->c:Lnuf;

    if-nez v0, :cond_4

    .line 232
    new-instance v0, Lnuf;

    invoke-direct {v0}, Lnuf;-><init>()V

    iput-object v0, p0, Lnua;->c:Lnuf;

    .line 234
    :cond_4
    iget-object v0, p0, Lnua;->c:Lnuf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 238
    :sswitch_4
    iget-object v0, p0, Lnua;->d:Lntz;

    if-nez v0, :cond_5

    .line 239
    new-instance v0, Lntz;

    invoke-direct {v0}, Lntz;-><init>()V

    iput-object v0, p0, Lnua;->d:Lntz;

    .line 241
    :cond_5
    iget-object v0, p0, Lnua;->d:Lntz;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 195
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 142
    iget-object v0, p0, Lnua;->a:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lnua;->a:[I

    array-length v0, v0

    if-lez v0, :cond_0

    .line 143
    iget-object v1, p0, Lnua;->a:[I

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget v3, v1, v0

    .line 144
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->a(II)V

    .line 143
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 147
    :cond_0
    iget-object v0, p0, Lnua;->b:Logu;

    if-eqz v0, :cond_1

    .line 148
    const/4 v0, 0x2

    iget-object v1, p0, Lnua;->b:Logu;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 150
    :cond_1
    iget-object v0, p0, Lnua;->c:Lnuf;

    if-eqz v0, :cond_2

    .line 151
    const/4 v0, 0x3

    iget-object v1, p0, Lnua;->c:Lnuf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 153
    :cond_2
    iget-object v0, p0, Lnua;->d:Lntz;

    if-eqz v0, :cond_3

    .line 154
    const/4 v0, 0x4

    iget-object v1, p0, Lnua;->d:Lntz;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 156
    :cond_3
    iget-object v0, p0, Lnua;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 158
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 121
    invoke-virtual {p0, p1}, Lnua;->a(Loxn;)Lnua;

    move-result-object v0

    return-object v0
.end method
