.class public final Lhuy;
.super Landroid/view/ViewGroup;
.source "PG"

# interfaces
.implements Lljh;


# instance fields
.field private final a:Lhvx;

.field private b:Z

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field private f:Lcom/google/android/libraries/social/media/ui/MediaView;

.field private final g:I

.field private h:Lhuz;

.field private i:I

.field private j:I

.field private k:Landroid/widget/Button;

.field private l:Landroid/widget/Button;

.field private m:Lhuz;

.field private n:Lhuz;

.field private o:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 87
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 102
    invoke-virtual {p0}, Lhuy;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 104
    invoke-static {v0}, Lhvx;->a(Landroid/content/Context;)Lhvx;

    move-result-object v1

    iput-object v1, p0, Lhuy;->a:Lhvx;

    .line 106
    const/4 v1, 0x0

    iput-boolean v1, p0, Lhuy;->b:Z

    .line 108
    const/16 v1, 0xa

    .line 109
    invoke-static {v0, v1}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v0

    .line 110
    invoke-static {v0}, Llib;->a(Landroid/text/TextPaint;)I

    move-result v0

    iput v0, p0, Lhuy;->g:I

    .line 112
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lhuy;->setWillNotDraw(Z)V

    .line 114
    invoke-virtual {p0}, Lhuy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020071

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 115
    invoke-virtual {p0}, Lhuy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f020073

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 123
    invoke-static {}, Llsj;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lhuy;->a:Lhvx;

    iget v2, v2, Lhvx;->f:I

    .line 124
    invoke-static {v1, v2, v0}, Lkct;->a(Landroid/graphics/drawable/Drawable;ILandroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 123
    :cond_0
    invoke-virtual {p0, v0}, Lhuy;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 88
    return-void
.end method

.method private a(Landroid/view/View;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 645
    if-nez p1, :cond_1

    .line 648
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-ne v1, p0, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method protected a(I)I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 412
    invoke-static {v0, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 413
    const/high16 v1, -0x80000000

    invoke-static {p1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 415
    iget-object v1, p0, Lhuy;->k:Landroid/widget/Button;

    invoke-direct {p0, v1}, Lhuy;->a(Landroid/view/View;)Z

    move-result v4

    .line 416
    iget-object v1, p0, Lhuy;->l:Landroid/widget/Button;

    invoke-direct {p0, v1}, Lhuy;->a(Landroid/view/View;)Z

    move-result v5

    .line 418
    if-nez v4, :cond_0

    if-eqz v5, :cond_3

    .line 421
    :cond_0
    if-eqz v4, :cond_5

    .line 422
    iget-object v1, p0, Lhuy;->k:Landroid/widget/Button;

    invoke-virtual {v1, v3, v2}, Landroid/widget/Button;->measure(II)V

    .line 423
    iget-object v1, p0, Lhuy;->k:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getMeasuredHeight()I

    move-result v1

    add-int/lit8 v1, v1, 0x0

    .line 426
    :goto_0
    if-eqz v5, :cond_4

    .line 427
    iget-object v5, p0, Lhuy;->l:Landroid/widget/Button;

    invoke-virtual {v5, v3, v2}, Landroid/widget/Button;->measure(II)V

    .line 429
    if-eqz v4, :cond_2

    .line 430
    iget-object v2, p0, Lhuy;->k:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/Button;->getMeasuredWidth()I

    move-result v2

    sub-int v2, p1, v2

    iget-object v3, p0, Lhuy;->l:Landroid/widget/Button;

    .line 431
    invoke-virtual {v3}, Landroid/widget/Button;->getMeasuredWidth()I

    move-result v3

    if-lt v2, v3, :cond_1

    const/4 v0, 0x1

    :cond_1
    iput-boolean v0, p0, Lhuy;->o:Z

    .line 435
    iget-boolean v0, p0, Lhuy;->o:Z

    if-nez v0, :cond_4

    .line 436
    :cond_2
    iget-object v0, p0, Lhuy;->l:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v0, v1

    .line 445
    :goto_1
    iget-object v1, p0, Lhuy;->a:Lhvx;

    iget v1, v1, Lhvx;->c:I

    add-int/2addr v0, v1

    .line 448
    :cond_3
    return v0

    :cond_4
    move v0, v1

    goto :goto_1

    :cond_5
    move v1, v0

    goto :goto_0
.end method

.method protected a(II)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 351
    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 352
    const/high16 v0, 0x40000000    # 2.0f

    invoke-static {p1, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 353
    iget-object v0, p0, Lhuy;->a:Lhvx;

    iget v4, v0, Lhvx;->i:I

    .line 356
    if-lez p2, :cond_4

    const/4 v0, 0x1

    .line 358
    :goto_0
    iget-object v5, p0, Lhuy;->c:Landroid/widget/TextView;

    invoke-direct {p0, v5}, Lhuy;->a(Landroid/view/View;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 359
    iget-object v1, p0, Lhuy;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v3, v2}, Landroid/widget/TextView;->measure(II)V

    .line 360
    iget-object v1, p0, Lhuy;->c:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v1, v4

    add-int/lit8 v1, v1, 0x0

    .line 362
    sub-int/2addr p2, v1

    .line 365
    :cond_0
    iget-object v5, p0, Lhuy;->e:Landroid/widget/TextView;

    invoke-direct {p0, v5}, Lhuy;->a(Landroid/view/View;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 366
    iget-object v5, p0, Lhuy;->e:Landroid/widget/TextView;

    invoke-virtual {v5, v3, v2}, Landroid/widget/TextView;->measure(II)V

    .line 370
    if-eqz v0, :cond_5

    iget-object v5, p0, Lhuy;->e:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v5

    if-ge p2, v5, :cond_5

    .line 371
    iget-object v5, p0, Lhuy;->e:Landroid/widget/TextView;

    invoke-virtual {p0, v5}, Lhuy;->removeView(Landroid/view/View;)V

    .line 381
    :cond_1
    :goto_1
    iget-object v5, p0, Lhuy;->d:Landroid/widget/TextView;

    invoke-direct {p0, v5}, Lhuy;->a(Landroid/view/View;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 384
    if-eqz v0, :cond_2

    .line 385
    iget v0, p0, Lhuy;->g:I

    div-int v0, p2, v0

    .line 386
    if-lez v0, :cond_6

    .line 387
    iget-object v5, p0, Lhuy;->d:Landroid/widget/TextView;

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 394
    :cond_2
    iget-object v0, p0, Lhuy;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v3, v2}, Landroid/widget/TextView;->measure(II)V

    .line 395
    iget-object v0, p0, Lhuy;->d:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v1

    .line 399
    :goto_2
    if-lez v0, :cond_3

    .line 400
    iget-object v1, p0, Lhuy;->a:Lhvx;

    iget v1, v1, Lhvx;->v:I

    add-int/2addr v0, v1

    :cond_3
    move v1, v0

    .line 403
    :goto_3
    return v1

    :cond_4
    move v0, v1

    .line 356
    goto :goto_0

    .line 375
    :cond_5
    iget-object v5, p0, Lhuy;->e:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v4

    add-int/2addr v1, v5

    .line 377
    sub-int/2addr p2, v1

    goto :goto_1

    .line 389
    :cond_6
    iget-object v0, p0, Lhuy;->d:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lhuy;->removeView(Landroid/view/View;)V

    goto :goto_3

    :cond_7
    move v0, v1

    goto :goto_2
.end method

.method public a()V
    .locals 1

    .prologue
    .line 633
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhuy;->b:Z

    .line 635
    iget-object v0, p0, Lhuy;->f:Lcom/google/android/libraries/social/media/ui/MediaView;

    if-eqz v0, :cond_0

    .line 636
    iget-object v0, p0, Lhuy;->f:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->a()V

    .line 638
    :cond_0
    return-void
.end method

.method public a(Lhui;)V
    .locals 12

    .prologue
    const/4 v3, 0x1

    const-wide/high16 v10, 0x3fe4000000000000L    # 0.625

    const/4 v8, 0x0

    const/4 v2, 0x0

    .line 141
    iget-object v0, p0, Lhuy;->h:Lhuz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhuy;->h:Lhuz;

    invoke-virtual {v0, v2}, Lhuz;->a(Lhjy;)V

    :cond_0
    iget-object v0, p0, Lhuy;->c:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhuy;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lhuy;->c:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lhuy;->c:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lhuy;->removeView(Landroid/view/View;)V

    :cond_1
    iget-object v0, p0, Lhuy;->d:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhuy;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lhuy;->d:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lhuy;->d:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lhuy;->removeView(Landroid/view/View;)V

    :cond_2
    iget-object v0, p0, Lhuy;->e:Landroid/widget/TextView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lhuy;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lhuy;->e:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lhuy;->e:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lhuy;->removeView(Landroid/view/View;)V

    :cond_3
    iget-object v0, p0, Lhuy;->k:Landroid/widget/Button;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lhuy;->k:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lhuy;->k:Landroid/widget/Button;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lhuy;->k:Landroid/widget/Button;

    invoke-virtual {p0, v0}, Lhuy;->removeView(Landroid/view/View;)V

    :cond_4
    iget-object v0, p0, Lhuy;->m:Lhuz;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lhuy;->m:Lhuz;

    invoke-virtual {v0, v2}, Lhuz;->a(Lhjy;)V

    :cond_5
    iget-object v0, p0, Lhuy;->l:Landroid/widget/Button;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lhuy;->l:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lhuy;->l:Landroid/widget/Button;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lhuy;->l:Landroid/widget/Button;

    invoke-virtual {p0, v0}, Lhuy;->removeView(Landroid/view/View;)V

    :cond_6
    iget-object v0, p0, Lhuy;->n:Lhuz;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lhuy;->n:Lhuz;

    invoke-virtual {v0, v2}, Lhuz;->a(Lhjy;)V

    :cond_7
    iput-boolean v3, p0, Lhuy;->o:Z

    iget-object v0, p0, Lhuy;->f:Lcom/google/android/libraries/social/media/ui/MediaView;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lhuy;->f:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;)V

    iget-object v0, p0, Lhuy;->f:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {p0, v0}, Lhuy;->removeView(Landroid/view/View;)V

    :cond_8
    iput v8, p0, Lhuy;->i:I

    iput v8, p0, Lhuy;->j:I

    .line 143
    if-nez p1, :cond_a

    .line 208
    :cond_9
    :goto_0
    return-void

    .line 146
    :cond_a
    iput-boolean v3, p0, Lhuy;->b:Z

    .line 148
    invoke-virtual {p0, p1}, Lhuy;->c(Lhui;)V

    .line 149
    invoke-virtual {p0, p1}, Lhuy;->b(Lhui;)V

    .line 151
    invoke-virtual {p1}, Lhui;->j()Lhjy;

    move-result-object v0

    .line 153
    if-eqz v0, :cond_c

    .line 154
    iget-object v1, p0, Lhuy;->h:Lhuz;

    if-nez v1, :cond_b

    .line 155
    new-instance v1, Lhuz;

    invoke-direct {v1, p0}, Lhuz;-><init>(Landroid/view/View;)V

    iput-object v1, p0, Lhuy;->h:Lhuz;

    .line 158
    :cond_b
    iget-object v1, p0, Lhuy;->h:Lhuz;

    invoke-virtual {v1, v0}, Lhuz;->a(Lhjy;)V

    .line 161
    :cond_c
    invoke-virtual {p1}, Lhui;->d()Ljava/lang/String;

    move-result-object v0

    .line 163
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 164
    invoke-virtual {p1}, Lhui;->f()I

    move-result v1

    .line 165
    invoke-virtual {p1}, Lhui;->g()I

    move-result v2

    .line 169
    if-eqz v1, :cond_d

    if-nez v2, :cond_e

    .line 170
    :cond_d
    iput v8, p0, Lhuy;->i:I

    .line 171
    iput v8, p0, Lhuy;->j:I

    goto :goto_0

    .line 175
    :cond_e
    int-to-double v4, v1

    int-to-double v6, v2

    div-double/2addr v4, v6

    .line 179
    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    cmpl-double v3, v4, v6

    if-ltz v3, :cond_11

    .line 180
    iget-object v1, p0, Lhuy;->a:Lhvx;

    iget v1, v1, Lhvx;->b:I

    iput v1, p0, Lhuy;->i:I

    .line 181
    iget v1, p0, Lhuy;->i:I

    iput v1, p0, Lhuy;->j:I

    .line 194
    :cond_f
    :goto_1
    invoke-virtual {p0}, Lhuy;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 195
    invoke-virtual {p0}, Lhuy;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Ljac;->a:Ljac;

    invoke-static {v2, v0, v3}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v0

    .line 197
    if-eqz v0, :cond_9

    .line 198
    iget-object v2, p0, Lhuy;->f:Lcom/google/android/libraries/social/media/ui/MediaView;

    if-nez v2, :cond_10

    .line 199
    new-instance v2, Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-direct {v2, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lhuy;->f:Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 200
    iget-object v1, p0, Lhuy;->f:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v1, v8}, Lcom/google/android/libraries/social/media/ui/MediaView;->b(I)V

    .line 203
    :cond_10
    iget-object v1, p0, Lhuy;->f:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;)V

    .line 204
    iget-object v0, p0, Lhuy;->f:Lcom/google/android/libraries/social/media/ui/MediaView;

    iget v1, p0, Lhuy;->i:I

    iget v2, p0, Lhuy;->j:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(II)V

    .line 205
    iget-object v0, p0, Lhuy;->f:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {p0, v0}, Lhuy;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 183
    :cond_11
    iget-object v3, p0, Lhuy;->a:Lhvx;

    iget v3, v3, Lhvx;->a:I

    iput v3, p0, Lhuy;->i:I

    .line 184
    iget v3, p0, Lhuy;->i:I

    if-eq v3, v1, :cond_12

    .line 185
    iget v3, p0, Lhuy;->i:I

    int-to-double v6, v3

    int-to-double v2, v2

    mul-double/2addr v2, v6

    int-to-double v6, v1

    div-double/2addr v2, v6

    double-to-int v1, v2

    iput v1, p0, Lhuy;->j:I

    .line 189
    :goto_2
    cmpg-double v1, v4, v10

    if-gez v1, :cond_f

    .line 190
    iget v1, p0, Lhuy;->i:I

    int-to-double v2, v1

    div-double/2addr v2, v10

    double-to-int v1, v2

    iput v1, p0, Lhuy;->j:I

    goto :goto_1

    .line 187
    :cond_12
    iput v2, p0, Lhuy;->j:I

    goto :goto_2
.end method

.method protected b(Lhui;)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x5

    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 215
    iget-object v0, p0, Lhuy;->a:Lhvx;

    iget v4, v0, Lhvx;->d:I

    .line 216
    iget-object v0, p0, Lhuy;->a:Lhvx;

    iget v5, v0, Lhvx;->c:I

    .line 218
    invoke-virtual {p1}, Lhui;->h()Lhuj;

    move-result-object v6

    .line 219
    if-eqz v6, :cond_2

    .line 220
    invoke-virtual {v6}, Lhuj;->a()Ljava/lang/String;

    move-result-object v7

    .line 221
    if-eqz v7, :cond_2

    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 222
    iget-object v0, p0, Lhuy;->k:Landroid/widget/Button;

    if-nez v0, :cond_0

    .line 223
    invoke-virtual {p0}, Lhuy;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static/range {v0 .. v5}, Lkcr;->a(Landroid/content/Context;Landroid/util/AttributeSet;IIII)Landroid/widget/Button;

    move-result-object v0

    iput-object v0, p0, Lhuy;->k:Landroid/widget/Button;

    .line 226
    iget-object v0, p0, Lhuy;->k:Landroid/widget/Button;

    invoke-virtual {v0, v9}, Landroid/widget/Button;->setMaxLines(I)V

    .line 227
    iget-object v0, p0, Lhuy;->k:Landroid/widget/Button;

    sget-object v8, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v8}, Landroid/widget/Button;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 229
    :cond_0
    iget-object v0, p0, Lhuy;->k:Landroid/widget/Button;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 230
    iget-object v0, p0, Lhuy;->k:Landroid/widget/Button;

    invoke-virtual {p0, v0}, Lhuy;->addView(Landroid/view/View;)V

    .line 232
    invoke-virtual {v6}, Lhuj;->b()Lhjy;

    move-result-object v0

    .line 234
    if-eqz v0, :cond_2

    .line 235
    iget-object v6, p0, Lhuy;->m:Lhuz;

    if-nez v6, :cond_1

    .line 236
    new-instance v6, Lhuz;

    iget-object v7, p0, Lhuy;->k:Landroid/widget/Button;

    invoke-direct {v6, v7}, Lhuz;-><init>(Landroid/view/View;)V

    iput-object v6, p0, Lhuy;->m:Lhuz;

    .line 239
    :cond_1
    iget-object v6, p0, Lhuy;->m:Lhuz;

    invoke-virtual {v6, v0}, Lhuz;->a(Lhjy;)V

    .line 244
    :cond_2
    invoke-virtual {p1}, Lhui;->i()Lhuj;

    move-result-object v6

    .line 245
    if-eqz v6, :cond_5

    .line 246
    invoke-virtual {v6}, Lhuj;->a()Ljava/lang/String;

    move-result-object v7

    .line 247
    if-eqz v7, :cond_5

    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 248
    iget-object v0, p0, Lhuy;->l:Landroid/widget/Button;

    if-nez v0, :cond_3

    .line 249
    invoke-virtual {p0}, Lhuy;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static/range {v0 .. v5}, Lkcr;->a(Landroid/content/Context;Landroid/util/AttributeSet;IIII)Landroid/widget/Button;

    move-result-object v0

    iput-object v0, p0, Lhuy;->l:Landroid/widget/Button;

    .line 252
    iget-object v0, p0, Lhuy;->l:Landroid/widget/Button;

    invoke-virtual {v0, v9}, Landroid/widget/Button;->setMaxLines(I)V

    .line 253
    iget-object v0, p0, Lhuy;->l:Landroid/widget/Button;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 255
    :cond_3
    iget-object v0, p0, Lhuy;->l:Landroid/widget/Button;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v7, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 256
    iget-object v0, p0, Lhuy;->l:Landroid/widget/Button;

    invoke-virtual {p0, v0}, Lhuy;->addView(Landroid/view/View;)V

    .line 258
    invoke-virtual {v6}, Lhuj;->b()Lhjy;

    move-result-object v0

    .line 260
    if-eqz v0, :cond_5

    .line 261
    iget-object v1, p0, Lhuy;->n:Lhuz;

    if-nez v1, :cond_4

    .line 262
    new-instance v1, Lhuz;

    iget-object v2, p0, Lhuy;->l:Landroid/widget/Button;

    invoke-direct {v1, v2}, Lhuz;-><init>(Landroid/view/View;)V

    iput-object v1, p0, Lhuy;->n:Lhuz;

    .line 265
    :cond_4
    iget-object v1, p0, Lhuy;->n:Lhuz;

    invoke-virtual {v1, v0}, Lhuz;->a(Lhjy;)V

    .line 269
    :cond_5
    return-void
.end method

.method protected c(Lhui;)V
    .locals 11

    .prologue
    const/16 v10, 0xa

    const/4 v9, 0x0

    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 277
    invoke-virtual {p0}, Lhuy;->getContext()Landroid/content/Context;

    move-result-object v6

    .line 279
    iget-object v0, p0, Lhuy;->a:Lhvx;

    iget v1, v0, Lhvx;->q:I

    .line 281
    invoke-virtual {p1}, Lhui;->e()Ljava/lang/CharSequence;

    move-result-object v3

    .line 282
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v2

    .line 284
    :goto_0
    if-eqz v0, :cond_1

    .line 285
    iget-object v4, p0, Lhuy;->e:Landroid/widget/TextView;

    if-nez v4, :cond_0

    .line 286
    invoke-static {v6, v9, v5, v10}, Llhx;->a(Landroid/content/Context;Landroid/util/AttributeSet;II)Landroid/widget/TextView;

    move-result-object v4

    iput-object v4, p0, Lhuy;->e:Landroid/widget/TextView;

    .line 288
    iget-object v4, p0, Lhuy;->e:Landroid/widget/TextView;

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 289
    iget-object v4, p0, Lhuy;->e:Landroid/widget/TextView;

    sget-object v7, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 291
    :cond_0
    iget-object v4, p0, Lhuy;->e:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 292
    iget-object v4, p0, Lhuy;->e:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 293
    iget-object v3, p0, Lhuy;->e:Landroid/widget/TextView;

    invoke-virtual {p0, v3}, Lhuy;->addView(Landroid/view/View;)V

    .line 296
    :cond_1
    invoke-virtual {p1}, Lhui;->b()Ljava/lang/CharSequence;

    move-result-object v7

    .line 297
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_8

    move v4, v2

    .line 299
    :goto_1
    if-eqz v4, :cond_3

    .line 300
    iget-object v3, p0, Lhuy;->c:Landroid/widget/TextView;

    if-nez v3, :cond_2

    .line 301
    const/16 v3, 0x1a

    invoke-static {v6, v9, v5, v3}, Llhx;->a(Landroid/content/Context;Landroid/util/AttributeSet;II)Landroid/widget/TextView;

    move-result-object v3

    iput-object v3, p0, Lhuy;->c:Landroid/widget/TextView;

    .line 303
    iget-object v3, p0, Lhuy;->c:Landroid/widget/TextView;

    sget-object v8, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 308
    :cond_2
    invoke-virtual {p1}, Lhui;->d()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 309
    iget-object v0, p0, Lhuy;->c:Landroid/widget/TextView;

    move-object v3, v0

    move v0, v1

    .line 311
    :goto_2
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 314
    iget-object v0, p0, Lhuy;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 315
    iget-object v0, p0, Lhuy;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 316
    iget-object v0, p0, Lhuy;->c:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lhuy;->addView(Landroid/view/View;)V

    .line 319
    :cond_3
    iget-object v0, p0, Lhuy;->a:Lhvx;

    iget v0, v0, Lhvx;->s:I

    .line 323
    if-eqz v4, :cond_4

    iget-object v3, p0, Lhuy;->c:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getLineCount()I

    move-result v3

    if-ne v1, v3, :cond_4

    .line 324
    add-int/lit8 v0, v0, -0x1

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 327
    :cond_4
    invoke-virtual {p1}, Lhui;->c()Ljava/lang/CharSequence;

    move-result-object v1

    .line 329
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 330
    iget-object v2, p0, Lhuy;->d:Landroid/widget/TextView;

    if-nez v2, :cond_5

    .line 331
    invoke-static {v6, v9, v5, v10}, Llhx;->a(Landroid/content/Context;Landroid/util/AttributeSet;II)Landroid/widget/TextView;

    move-result-object v2

    iput-object v2, p0, Lhuy;->d:Landroid/widget/TextView;

    .line 333
    iget-object v2, p0, Lhuy;->d:Landroid/widget/TextView;

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 335
    :cond_5
    iget-object v2, p0, Lhuy;->d:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 336
    iget-object v0, p0, Lhuy;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 337
    iget-object v0, p0, Lhuy;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 338
    iget-object v0, p0, Lhuy;->d:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lhuy;->addView(Landroid/view/View;)V

    .line 340
    :cond_6
    return-void

    :cond_7
    move v0, v5

    .line 282
    goto/16 :goto_0

    :cond_8
    move v4, v5

    .line 297
    goto :goto_1

    .line 311
    :cond_9
    iget-object v3, p0, Lhuy;->c:Landroid/widget/TextView;

    if-eqz v0, :cond_a

    move v0, v2

    goto :goto_2

    :cond_a
    const/4 v0, 0x2

    goto :goto_2
.end method

.method protected onLayout(ZIIII)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 501
    iget-object v1, p0, Lhuy;->a:Lhvx;

    iget v2, v1, Lhvx;->d:I

    .line 502
    iget-object v1, p0, Lhuy;->a:Lhvx;

    iget v3, v1, Lhvx;->i:I

    .line 503
    iget-object v1, p0, Lhuy;->a:Lhvx;

    iget v1, v1, Lhvx;->v:I

    .line 506
    iget-object v4, p0, Lhuy;->f:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-direct {p0, v4}, Lhuy;->a(Landroid/view/View;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 507
    iget-object v4, p0, Lhuy;->f:Lcom/google/android/libraries/social/media/ui/MediaView;

    iget-object v5, p0, Lhuy;->f:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v5}, Lcom/google/android/libraries/social/media/ui/MediaView;->getMeasuredWidth()I

    move-result v5

    iget-object v6, p0, Lhuy;->f:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v6}, Lcom/google/android/libraries/social/media/ui/MediaView;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v6, v2

    invoke-virtual {v4, v0, v0, v5, v6}, Lcom/google/android/libraries/social/media/ui/MediaView;->layout(IIII)V

    .line 508
    iget-object v0, p0, Lhuy;->f:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getMeasuredWidth()I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 511
    :cond_0
    add-int v4, v0, v2

    .line 513
    iget-object v0, p0, Lhuy;->c:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lhuy;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 514
    iget-object v0, p0, Lhuy;->c:Landroid/widget/TextView;

    iget-object v5, p0, Lhuy;->c:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v5

    add-int/2addr v5, v4

    iget-object v6, p0, Lhuy;->c:Landroid/widget/TextView;

    .line 515
    invoke-virtual {v6}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v6, v1

    .line 514
    invoke-virtual {v0, v4, v1, v5, v6}, Landroid/widget/TextView;->layout(IIII)V

    .line 517
    iget-object v0, p0, Lhuy;->c:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    .line 520
    :goto_0
    iget-object v1, p0, Lhuy;->d:Landroid/widget/TextView;

    invoke-direct {p0, v1}, Lhuy;->a(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 521
    iget-object v1, p0, Lhuy;->d:Landroid/widget/TextView;

    iget-object v5, p0, Lhuy;->d:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v5

    add-int/2addr v5, v4

    iget-object v6, p0, Lhuy;->d:Landroid/widget/TextView;

    .line 522
    invoke-virtual {v6}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v6, v0

    .line 521
    invoke-virtual {v1, v4, v0, v5, v6}, Landroid/widget/TextView;->layout(IIII)V

    .line 524
    iget-object v1, p0, Lhuy;->d:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 527
    :cond_1
    iget-object v1, p0, Lhuy;->e:Landroid/widget/TextView;

    invoke-direct {p0, v1}, Lhuy;->a(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 528
    iget-object v1, p0, Lhuy;->e:Landroid/widget/TextView;

    iget-object v3, p0, Lhuy;->e:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, v4

    iget-object v5, p0, Lhuy;->e:Landroid/widget/TextView;

    .line 529
    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v0

    .line 528
    invoke-virtual {v1, v4, v0, v3, v5}, Landroid/widget/TextView;->layout(IIII)V

    .line 533
    :cond_2
    sub-int v0, v4, v2

    .line 535
    invoke-virtual {p0}, Lhuy;->getMeasuredHeight()I

    move-result v1

    iget-object v2, p0, Lhuy;->a:Lhvx;

    iget v2, v2, Lhvx;->c:I

    sub-int/2addr v1, v2

    .line 537
    iget-boolean v2, p0, Lhuy;->o:Z

    if-eqz v2, :cond_5

    .line 538
    iget-object v2, p0, Lhuy;->k:Landroid/widget/Button;

    invoke-direct {p0, v2}, Lhuy;->a(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 539
    iget-object v2, p0, Lhuy;->k:Landroid/widget/Button;

    iget-object v3, p0, Lhuy;->k:Landroid/widget/Button;

    invoke-virtual {v3}, Landroid/widget/Button;->getMeasuredHeight()I

    move-result v3

    sub-int v3, v1, v3

    iget-object v4, p0, Lhuy;->k:Landroid/widget/Button;

    .line 540
    invoke-virtual {v4}, Landroid/widget/Button;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v0

    .line 539
    invoke-virtual {v2, v0, v3, v4, v1}, Landroid/widget/Button;->layout(IIII)V

    .line 541
    iget-object v2, p0, Lhuy;->k:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/Button;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v0, v2

    .line 544
    :cond_3
    iget-object v2, p0, Lhuy;->l:Landroid/widget/Button;

    invoke-direct {p0, v2}, Lhuy;->a(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 545
    iget-object v2, p0, Lhuy;->l:Landroid/widget/Button;

    iget-object v3, p0, Lhuy;->l:Landroid/widget/Button;

    invoke-virtual {v3}, Landroid/widget/Button;->getMeasuredHeight()I

    move-result v3

    sub-int v3, v1, v3

    iget-object v4, p0, Lhuy;->l:Landroid/widget/Button;

    .line 546
    invoke-virtual {v4}, Landroid/widget/Button;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v0

    .line 545
    invoke-virtual {v2, v0, v3, v4, v1}, Landroid/widget/Button;->layout(IIII)V

    .line 557
    :cond_4
    :goto_1
    return-void

    .line 549
    :cond_5
    iget-object v2, p0, Lhuy;->l:Landroid/widget/Button;

    iget-object v3, p0, Lhuy;->l:Landroid/widget/Button;

    invoke-virtual {v3}, Landroid/widget/Button;->getMeasuredHeight()I

    move-result v3

    sub-int v3, v1, v3

    iget-object v4, p0, Lhuy;->l:Landroid/widget/Button;

    .line 550
    invoke-virtual {v4}, Landroid/widget/Button;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v0

    .line 549
    invoke-virtual {v2, v0, v3, v4, v1}, Landroid/widget/Button;->layout(IIII)V

    .line 552
    iget-object v2, p0, Lhuy;->l:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/Button;->getMeasuredHeight()I

    move-result v2

    sub-int/2addr v1, v2

    .line 554
    iget-object v2, p0, Lhuy;->k:Landroid/widget/Button;

    iget-object v3, p0, Lhuy;->k:Landroid/widget/Button;

    invoke-virtual {v3}, Landroid/widget/Button;->getMeasuredHeight()I

    move-result v3

    sub-int v3, v1, v3

    iget-object v4, p0, Lhuy;->k:Landroid/widget/Button;

    .line 555
    invoke-virtual {v4}, Landroid/widget/Button;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v0

    .line 554
    invoke-virtual {v2, v0, v3, v4, v1}, Landroid/widget/Button;->layout(IIII)V

    goto :goto_1

    :cond_6
    move v0, v1

    goto/16 :goto_0
.end method

.method protected onMeasure(II)V
    .locals 7

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    const/4 v1, 0x0

    .line 460
    iget-boolean v0, p0, Lhuy;->b:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x40

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " expected to have been bound with valid data. Was bind() called?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 462
    :cond_0
    invoke-virtual {p0}, Lhuy;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lhvj;->a(Landroid/content/Context;I)I

    move-result v3

    .line 465
    iget-object v0, p0, Lhuy;->a:Lhvx;

    iget v0, v0, Lhvx;->d:I

    mul-int/lit8 v0, v0, 0x2

    sub-int v0, v3, v0

    .line 467
    iget-object v2, p0, Lhuy;->f:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-direct {p0, v2}, Lhuy;->a(Landroid/view/View;)Z

    move-result v4

    .line 469
    if-eqz v4, :cond_3

    .line 470
    iget v2, p0, Lhuy;->i:I

    invoke-static {v2, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 471
    iget v5, p0, Lhuy;->j:I

    invoke-static {v5, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 472
    iget-object v6, p0, Lhuy;->f:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v6, v2, v5}, Lcom/google/android/libraries/social/media/ui/MediaView;->measure(II)V

    .line 474
    iget-object v2, p0, Lhuy;->f:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->getMeasuredWidth()I

    move-result v2

    sub-int v2, v0, v2

    .line 475
    iget-object v0, p0, Lhuy;->f:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getMeasuredHeight()I

    move-result v0

    .line 478
    :goto_0
    invoke-virtual {p0, v2}, Lhuy;->a(I)I

    move-result v5

    .line 479
    sub-int v6, v0, v5

    invoke-virtual {p0, v2, v6}, Lhuy;->a(II)I

    move-result v2

    .line 481
    if-nez v4, :cond_1

    .line 482
    add-int v0, v2, v5

    .line 487
    :cond_1
    if-nez v0, :cond_2

    .line 488
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lhuy;->setVisibility(I)V

    .line 489
    invoke-virtual {p0, v1, v1}, Lhuy;->setMeasuredDimension(II)V

    .line 494
    :goto_1
    return-void

    .line 493
    :cond_2
    invoke-virtual {p0, v3, v0}, Lhuy;->setMeasuredDimension(II)V

    goto :goto_1

    :cond_3
    move v2, v0

    move v0, v1

    goto :goto_0
.end method
