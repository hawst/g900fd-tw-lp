.class public final Ldio;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmbe;",
        "Lmbf;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lkzl;

.field private final b:I

.field private final p:Ljava/lang/String;

.field private final q:Ljava/lang/String;

.field private final r:Ljava/lang/String;

.field private final s:I

.field private t:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 39
    const-string v3, "getactivities"

    new-instance v4, Lmbe;

    invoke-direct {v4}, Lmbe;-><init>()V

    new-instance v5, Lmbf;

    invoke-direct {v5}, Lmbf;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 41
    iget-object v0, p0, Ldio;->f:Landroid/content/Context;

    const-class v1, Lkzl;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkzl;

    iput-object v0, p0, Ldio;->a:Lkzl;

    .line 42
    iput p3, p0, Ldio;->b:I

    .line 43
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "f."

    invoke-virtual {p4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44
    const/4 v0, 0x2

    invoke-virtual {p4, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p4

    .line 46
    :cond_0
    iput-object p4, p0, Ldio;->p:Ljava/lang/String;

    .line 47
    iput-object p5, p0, Ldio;->q:Ljava/lang/String;

    .line 48
    iput-object p6, p0, Ldio;->r:Ljava/lang/String;

    .line 49
    new-instance v0, Llcr;

    invoke-direct {v0, p1}, Llcr;-><init>(Landroid/content/Context;)V

    iget v0, v0, Llcr;->a:I

    iput v0, p0, Ldio;->s:I

    .line 50
    return-void
.end method


# virtual methods
.method protected a(Lmbe;)V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v4, 0x2

    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 54
    new-instance v0, Lnve;

    invoke-direct {v0}, Lnve;-><init>()V

    iput-object v0, p1, Lmbe;->a:Lnve;

    .line 55
    iget-object v2, p1, Lmbe;->a:Lnve;

    .line 57
    new-instance v0, Logk;

    invoke-direct {v0}, Logk;-><init>()V

    iput-object v0, v2, Lnve;->a:Logk;

    .line 58
    iget-object v3, v2, Lnve;->a:Logk;

    iget v0, p0, Ldio;->b:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    move v0, v1

    :goto_0
    iput v0, v3, Logk;->a:I

    .line 59
    iget v0, p0, Ldio;->b:I

    if-eq v0, v6, :cond_0

    iget v0, p0, Ldio;->b:I

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Ldio;->p:Ljava/lang/String;

    if-nez v0, :cond_1

    iget-object v0, p0, Ldio;->q:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 61
    :cond_1
    iget-object v0, v2, Lnve;->a:Logk;

    iput v4, v0, Logk;->b:I

    .line 65
    :goto_1
    iget-object v0, v2, Lnve;->a:Logk;

    iget-object v3, p0, Ldio;->p:Ljava/lang/String;

    iput-object v3, v0, Logk;->d:Ljava/lang/String;

    .line 66
    iget-object v0, v2, Lnve;->a:Logk;

    iget-object v3, p0, Ldio;->q:Ljava/lang/String;

    iput-object v3, v0, Logk;->c:Ljava/lang/String;

    .line 67
    iget-object v0, v2, Lnve;->a:Logk;

    iget-object v3, p0, Ldio;->r:Ljava/lang/String;

    iput-object v3, v0, Logk;->e:Ljava/lang/String;

    .line 68
    const/4 v0, 0x0

    iput-object v0, v2, Lnve;->b:Ljava/lang/String;

    .line 69
    iget-object v0, v2, Lnve;->a:Logk;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Logk;->f:Ljava/lang/Integer;

    .line 71
    iget-object v0, v2, Lnve;->a:Logk;

    iput v6, v0, Logk;->g:I

    .line 72
    iget-object v0, v2, Lnve;->a:Logk;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Logk;->i:Ljava/lang/Integer;

    .line 73
    iget-object v0, v2, Lnve;->a:Logk;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Logk;->m:Ljava/lang/Integer;

    .line 74
    iget-object v0, v2, Lnve;->a:Logk;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Logk;->j:Ljava/lang/Integer;

    .line 76
    iget-object v0, v2, Lnve;->a:Logk;

    new-instance v1, Lofl;

    invoke-direct {v1}, Lofl;-><init>()V

    iput-object v1, v0, Logk;->l:Lofl;

    .line 77
    iget-object v0, v2, Lnve;->a:Logk;

    iget-object v0, v0, Logk;->l:Lofl;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v1, v0, Lofl;->e:Ljava/lang/Boolean;

    .line 78
    iget-object v0, v2, Lnve;->a:Logk;

    iget-object v0, v0, Logk;->l:Lofl;

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    iput-object v1, v0, Lofl;->b:Ljava/lang/Boolean;

    .line 79
    iget-object v0, v2, Lnve;->a:Logk;

    iget-object v0, v0, Logk;->l:Lofl;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v1, v0, Lofl;->c:Ljava/lang/Boolean;

    .line 80
    iget-object v0, v2, Lnve;->a:Logk;

    new-instance v1, Logu;

    invoke-direct {v1}, Logu;-><init>()V

    iput-object v1, v0, Logk;->n:Logu;

    .line 81
    iget-object v0, v2, Lnve;->a:Logk;

    iget-object v0, v0, Logk;->n:Logu;

    iget-object v1, p0, Ldio;->a:Lkzl;

    iget v3, p0, Ldio;->c:I

    .line 82
    invoke-interface {v1, v5}, Lkzl;->a(Z)[I

    move-result-object v1

    iput-object v1, v0, Logu;->a:[I

    .line 83
    iget-object v0, v2, Lnve;->a:Logk;

    iput v4, v0, Logk;->h:I

    .line 85
    new-instance v0, Loxz;

    invoke-direct {v0}, Loxz;-><init>()V

    iput-object v0, v2, Lnve;->d:Loxz;

    .line 86
    iget-object v0, v2, Lnve;->d:Loxz;

    iget-object v1, p0, Ldio;->a:Lkzl;

    iget-object v3, p0, Ldio;->f:Landroid/content/Context;

    iget v4, p0, Ldio;->c:I

    invoke-interface {v1, v3, v4}, Lkzl;->a(Landroid/content/Context;I)[I

    move-result-object v1

    iput-object v1, v0, Loxz;->a:[I

    .line 88
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v2, Lnve;->e:Ljava/lang/Boolean;

    .line 90
    new-instance v0, Locx;

    invoke-direct {v0}, Locx;-><init>()V

    iput-object v0, v2, Lnve;->f:Locx;

    .line 91
    iget-object v0, v2, Lnve;->f:Locx;

    iget v1, p0, Ldio;->s:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Locx;->a:Ljava/lang/Integer;

    .line 92
    return-void

    .line 58
    :pswitch_1
    const/4 v0, 0x5

    goto/16 :goto_0

    :pswitch_2
    const/16 v0, 0x10

    goto/16 :goto_0

    :pswitch_3
    const/16 v0, 0x12

    goto/16 :goto_0

    .line 63
    :cond_2
    iget-object v0, v2, Lnve;->a:Logk;

    iput v1, v0, Logk;->b:I

    goto/16 :goto_1

    .line 58
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method protected a(Lmbf;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 96
    iget-object v1, p1, Lmbf;->a:Loda;

    .line 101
    iget v0, p0, Ldio;->b:I

    const/4 v2, 0x4

    if-ne v0, v2, :cond_0

    .line 102
    iget-object v0, p0, Ldio;->q:Ljava/lang/String;

    iget-object v2, p0, Ldio;->r:Ljava/lang/String;

    invoke-static {v0, v2, v5}, Llbc;->a(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 108
    :goto_0
    iget-object v2, p0, Ldio;->f:Landroid/content/Context;

    iget v3, p0, Ldio;->c:I

    iget-object v1, v1, Loda;->a:Logi;

    iget-object v1, v1, Logi;->a:[Logr;

    invoke-static {v2, v3, v0, v1}, Llap;->a(Landroid/content/Context;ILjava/lang/String;[Logr;)Z

    move-result v0

    iput-boolean v0, p0, Ldio;->t:Z

    .line 110
    return-void

    .line 104
    :cond_0
    iget-object v0, p0, Ldio;->q:Ljava/lang/String;

    iget-object v2, p0, Ldio;->p:Ljava/lang/String;

    const/4 v3, 0x0

    iget v4, p0, Ldio;->b:I

    invoke-static {v0, v2, v3, v5, v4}, Llbc;->a(Ljava/lang/String;Ljava/lang/String;Llae;ZI)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 26
    check-cast p1, Lmbe;

    invoke-virtual {p0, p1}, Ldio;->a(Lmbe;)V

    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 137
    iget-boolean v0, p0, Ldio;->t:Z

    return v0
.end method

.method protected synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 26
    check-cast p1, Lmbf;

    invoke-virtual {p0, p1}, Ldio;->a(Lmbf;)V

    return-void
.end method
