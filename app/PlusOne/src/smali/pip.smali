.class public final Lpip;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lpip;


# instance fields
.field private b:Lpir;

.field private c:Lpis;

.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 417
    const/4 v0, 0x0

    new-array v0, v0, [Lpip;

    sput-object v0, Lpip;->a:[Lpip;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 418
    invoke-direct {p0}, Loxq;-><init>()V

    .line 444
    iput-object v0, p0, Lpip;->b:Lpir;

    .line 447
    iput-object v0, p0, Lpip;->c:Lpis;

    .line 450
    const/high16 v0, -0x80000000

    iput v0, p0, Lpip;->d:I

    .line 418
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 470
    const/4 v0, 0x0

    .line 471
    iget-object v1, p0, Lpip;->b:Lpir;

    if-eqz v1, :cond_0

    .line 472
    const/4 v0, 0x1

    iget-object v1, p0, Lpip;->b:Lpir;

    .line 473
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 475
    :cond_0
    iget v1, p0, Lpip;->d:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_1

    .line 476
    const/4 v1, 0x2

    iget v2, p0, Lpip;->d:I

    .line 477
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 479
    :cond_1
    iget-object v1, p0, Lpip;->c:Lpis;

    if-eqz v1, :cond_2

    .line 480
    const/4 v1, 0x3

    iget-object v2, p0, Lpip;->c:Lpis;

    .line 481
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 483
    :cond_2
    iget-object v1, p0, Lpip;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 484
    iput v0, p0, Lpip;->ai:I

    .line 485
    return v0
.end method

.method public a(Loxn;)Lpip;
    .locals 3

    .prologue
    const/16 v2, 0x2b

    .line 493
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 494
    sparse-switch v0, :sswitch_data_0

    .line 498
    iget-object v1, p0, Lpip;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 499
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpip;->ah:Ljava/util/List;

    .line 502
    :cond_1
    iget-object v1, p0, Lpip;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 504
    :sswitch_0
    return-object p0

    .line 509
    :sswitch_1
    iget-object v0, p0, Lpip;->b:Lpir;

    if-nez v0, :cond_2

    .line 510
    new-instance v0, Lpir;

    invoke-direct {v0}, Lpir;-><init>()V

    iput-object v0, p0, Lpip;->b:Lpir;

    .line 512
    :cond_2
    iget-object v0, p0, Lpip;->b:Lpir;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 516
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 517
    if-eq v0, v2, :cond_3

    const/16 v1, 0x2c

    if-eq v0, v1, :cond_3

    const/16 v1, 0xc8

    if-eq v0, v1, :cond_3

    const/16 v1, 0xdc

    if-eq v0, v1, :cond_3

    const/16 v1, 0xdd

    if-eq v0, v1, :cond_3

    const/16 v1, 0xde

    if-eq v0, v1, :cond_3

    const/16 v1, 0xdf

    if-eq v0, v1, :cond_3

    const/16 v1, 0xe0

    if-eq v0, v1, :cond_3

    const/16 v1, 0xe1

    if-eq v0, v1, :cond_3

    const/16 v1, 0xe2

    if-eq v0, v1, :cond_3

    const/16 v1, 0xe3

    if-eq v0, v1, :cond_3

    const/16 v1, 0x12c

    if-eq v0, v1, :cond_3

    const/16 v1, 0x12e

    if-eq v0, v1, :cond_3

    const/16 v1, 0xf0

    if-eq v0, v1, :cond_3

    const/16 v1, 0xf1

    if-eq v0, v1, :cond_3

    const/16 v1, 0x138

    if-eq v0, v1, :cond_3

    const/16 v1, 0x13a

    if-eq v0, v1, :cond_3

    const/16 v1, 0x13b

    if-eq v0, v1, :cond_3

    const/16 v1, 0x13c

    if-eq v0, v1, :cond_3

    const/16 v1, 0x3a98

    if-ne v0, v1, :cond_4

    .line 537
    :cond_3
    iput v0, p0, Lpip;->d:I

    goto :goto_0

    .line 539
    :cond_4
    iput v2, p0, Lpip;->d:I

    goto/16 :goto_0

    .line 544
    :sswitch_3
    iget-object v0, p0, Lpip;->c:Lpis;

    if-nez v0, :cond_5

    .line 545
    new-instance v0, Lpis;

    invoke-direct {v0}, Lpis;-><init>()V

    iput-object v0, p0, Lpip;->c:Lpis;

    .line 547
    :cond_5
    iget-object v0, p0, Lpip;->c:Lpis;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 494
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 455
    iget-object v0, p0, Lpip;->b:Lpir;

    if-eqz v0, :cond_0

    .line 456
    const/4 v0, 0x1

    iget-object v1, p0, Lpip;->b:Lpir;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 458
    :cond_0
    iget v0, p0, Lpip;->d:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 459
    const/4 v0, 0x2

    iget v1, p0, Lpip;->d:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 461
    :cond_1
    iget-object v0, p0, Lpip;->c:Lpis;

    if-eqz v0, :cond_2

    .line 462
    const/4 v0, 0x3

    iget-object v1, p0, Lpip;->c:Lpis;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 464
    :cond_2
    iget-object v0, p0, Lpip;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 466
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 414
    invoke-virtual {p0, p1}, Lpip;->a(Loxn;)Lpip;

    move-result-object v0

    return-object v0
.end method
