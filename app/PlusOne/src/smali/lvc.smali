.class public final Llvc;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lluz;

.field private b:Llve;

.field private c:Llvj;

.field private d:Llvq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 9
    invoke-direct {p0}, Loxq;-><init>()V

    .line 12
    iput-object v0, p0, Llvc;->b:Llve;

    .line 15
    iput-object v0, p0, Llvc;->c:Llvj;

    .line 18
    iput-object v0, p0, Llvc;->d:Llvq;

    .line 21
    iput-object v0, p0, Llvc;->a:Lluz;

    .line 9
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 44
    const/4 v0, 0x0

    .line 45
    iget-object v1, p0, Llvc;->b:Llve;

    if-eqz v1, :cond_0

    .line 46
    const/4 v0, 0x1

    iget-object v1, p0, Llvc;->b:Llve;

    .line 47
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 49
    :cond_0
    iget-object v1, p0, Llvc;->c:Llvj;

    if-eqz v1, :cond_1

    .line 50
    const/4 v1, 0x2

    iget-object v2, p0, Llvc;->c:Llvj;

    .line 51
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 53
    :cond_1
    iget-object v1, p0, Llvc;->d:Llvq;

    if-eqz v1, :cond_2

    .line 54
    const/4 v1, 0x3

    iget-object v2, p0, Llvc;->d:Llvq;

    .line 55
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 57
    :cond_2
    iget-object v1, p0, Llvc;->a:Lluz;

    if-eqz v1, :cond_3

    .line 58
    const/4 v1, 0x4

    iget-object v2, p0, Llvc;->a:Lluz;

    .line 59
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 61
    :cond_3
    iget-object v1, p0, Llvc;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 62
    iput v0, p0, Llvc;->ai:I

    .line 63
    return v0
.end method

.method public a(Loxn;)Llvc;
    .locals 2

    .prologue
    .line 71
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 72
    sparse-switch v0, :sswitch_data_0

    .line 76
    iget-object v1, p0, Llvc;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 77
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llvc;->ah:Ljava/util/List;

    .line 80
    :cond_1
    iget-object v1, p0, Llvc;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 82
    :sswitch_0
    return-object p0

    .line 87
    :sswitch_1
    iget-object v0, p0, Llvc;->b:Llve;

    if-nez v0, :cond_2

    .line 88
    new-instance v0, Llve;

    invoke-direct {v0}, Llve;-><init>()V

    iput-object v0, p0, Llvc;->b:Llve;

    .line 90
    :cond_2
    iget-object v0, p0, Llvc;->b:Llve;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 94
    :sswitch_2
    iget-object v0, p0, Llvc;->c:Llvj;

    if-nez v0, :cond_3

    .line 95
    new-instance v0, Llvj;

    invoke-direct {v0}, Llvj;-><init>()V

    iput-object v0, p0, Llvc;->c:Llvj;

    .line 97
    :cond_3
    iget-object v0, p0, Llvc;->c:Llvj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 101
    :sswitch_3
    iget-object v0, p0, Llvc;->d:Llvq;

    if-nez v0, :cond_4

    .line 102
    new-instance v0, Llvq;

    invoke-direct {v0}, Llvq;-><init>()V

    iput-object v0, p0, Llvc;->d:Llvq;

    .line 104
    :cond_4
    iget-object v0, p0, Llvc;->d:Llvq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 108
    :sswitch_4
    iget-object v0, p0, Llvc;->a:Lluz;

    if-nez v0, :cond_5

    .line 109
    new-instance v0, Lluz;

    invoke-direct {v0}, Lluz;-><init>()V

    iput-object v0, p0, Llvc;->a:Lluz;

    .line 111
    :cond_5
    iget-object v0, p0, Llvc;->a:Lluz;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 72
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 26
    iget-object v0, p0, Llvc;->b:Llve;

    if-eqz v0, :cond_0

    .line 27
    const/4 v0, 0x1

    iget-object v1, p0, Llvc;->b:Llve;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 29
    :cond_0
    iget-object v0, p0, Llvc;->c:Llvj;

    if-eqz v0, :cond_1

    .line 30
    const/4 v0, 0x2

    iget-object v1, p0, Llvc;->c:Llvj;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 32
    :cond_1
    iget-object v0, p0, Llvc;->d:Llvq;

    if-eqz v0, :cond_2

    .line 33
    const/4 v0, 0x3

    iget-object v1, p0, Llvc;->d:Llvq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 35
    :cond_2
    iget-object v0, p0, Llvc;->a:Lluz;

    if-eqz v0, :cond_3

    .line 36
    const/4 v0, 0x4

    iget-object v1, p0, Llvc;->a:Lluz;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 38
    :cond_3
    iget-object v0, p0, Llvc;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 40
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Llvc;->a(Loxn;)Llvc;

    move-result-object v0

    return-object v0
.end method
