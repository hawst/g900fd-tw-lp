.class public final Lnqp;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnqp;


# instance fields
.field public b:Lnwr;

.field public c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Lnqp;

    sput-object v0, Lnqp;->a:[Lnqp;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 13
    const/4 v0, 0x0

    iput-object v0, p0, Lnqp;->b:Lnwr;

    .line 16
    const/high16 v0, -0x80000000

    iput v0, p0, Lnqp;->c:I

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 33
    const/4 v0, 0x0

    .line 34
    iget-object v1, p0, Lnqp;->b:Lnwr;

    if-eqz v1, :cond_0

    .line 35
    const/4 v0, 0x1

    iget-object v1, p0, Lnqp;->b:Lnwr;

    .line 36
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 38
    :cond_0
    iget v1, p0, Lnqp;->c:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_1

    .line 39
    const/4 v1, 0x2

    iget v2, p0, Lnqp;->c:I

    .line 40
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 42
    :cond_1
    iget-object v1, p0, Lnqp;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 43
    iput v0, p0, Lnqp;->ai:I

    .line 44
    return v0
.end method

.method public a(Loxn;)Lnqp;
    .locals 2

    .prologue
    .line 52
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 53
    sparse-switch v0, :sswitch_data_0

    .line 57
    iget-object v1, p0, Lnqp;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 58
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnqp;->ah:Ljava/util/List;

    .line 61
    :cond_1
    iget-object v1, p0, Lnqp;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 63
    :sswitch_0
    return-object p0

    .line 68
    :sswitch_1
    iget-object v0, p0, Lnqp;->b:Lnwr;

    if-nez v0, :cond_2

    .line 69
    new-instance v0, Lnwr;

    invoke-direct {v0}, Lnwr;-><init>()V

    iput-object v0, p0, Lnqp;->b:Lnwr;

    .line 71
    :cond_2
    iget-object v0, p0, Lnqp;->b:Lnwr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 75
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 76
    if-eqz v0, :cond_3

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    const/4 v1, 0x4

    if-eq v0, v1, :cond_3

    const/16 v1, 0x11

    if-eq v0, v1, :cond_3

    const/4 v1, 0x5

    if-eq v0, v1, :cond_3

    const/16 v1, 0x12

    if-eq v0, v1, :cond_3

    const/4 v1, 0x6

    if-eq v0, v1, :cond_3

    const/16 v1, 0x17

    if-eq v0, v1, :cond_3

    const/4 v1, 0x7

    if-eq v0, v1, :cond_3

    const/16 v1, 0x16

    if-eq v0, v1, :cond_3

    const/16 v1, 0x15

    if-eq v0, v1, :cond_3

    const/16 v1, 0x14

    if-eq v0, v1, :cond_3

    const/16 v1, 0x8

    if-eq v0, v1, :cond_3

    const/16 v1, 0x9

    if-eq v0, v1, :cond_3

    const/16 v1, 0xa

    if-eq v0, v1, :cond_3

    const/16 v1, 0x19

    if-eq v0, v1, :cond_3

    const/16 v1, 0xc

    if-eq v0, v1, :cond_3

    const/16 v1, 0x10

    if-eq v0, v1, :cond_3

    const/16 v1, 0xd

    if-eq v0, v1, :cond_3

    const/16 v1, 0xe

    if-eq v0, v1, :cond_3

    const/16 v1, 0xf

    if-eq v0, v1, :cond_3

    const/16 v1, 0xb

    if-eq v0, v1, :cond_3

    const/16 v1, 0x18

    if-ne v0, v1, :cond_4

    .line 101
    :cond_3
    iput v0, p0, Lnqp;->c:I

    goto/16 :goto_0

    .line 103
    :cond_4
    const/4 v0, 0x0

    iput v0, p0, Lnqp;->c:I

    goto/16 :goto_0

    .line 53
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 21
    iget-object v0, p0, Lnqp;->b:Lnwr;

    if-eqz v0, :cond_0

    .line 22
    const/4 v0, 0x1

    iget-object v1, p0, Lnqp;->b:Lnwr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 24
    :cond_0
    iget v0, p0, Lnqp;->c:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 25
    const/4 v0, 0x2

    iget v1, p0, Lnqp;->c:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 27
    :cond_1
    iget-object v0, p0, Lnqp;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 29
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lnqp;->a(Loxn;)Lnqp;

    move-result-object v0

    return-object v0
.end method
