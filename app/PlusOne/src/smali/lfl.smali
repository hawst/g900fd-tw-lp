.class public final Llfl;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/opengl/GLSurfaceView$Renderer;


# instance fields
.field private a:Lify;

.field private synthetic b:Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;)V
    .locals 0

    .prologue
    .line 267
    iput-object p1, p0, Llfl;->b:Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 6

    .prologue
    .line 286
    iget-object v0, p0, Llfl;->a:Lify;

    invoke-virtual {v0}, Lify;->c()V

    .line 288
    iget-object v0, p0, Llfl;->b:Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;

    invoke-static {v0}, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->c(Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 289
    :try_start_0
    iget-object v0, p0, Llfl;->b:Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;

    invoke-static {v0}, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->b(Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;)Llfk;

    move-result-object v0

    iget-object v0, v0, Llfk;->f:Ljava/lang/Runnable;

    .line 290
    iget-object v2, p0, Llfl;->b:Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;

    invoke-static {v2}, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->b(Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;)Llfk;

    move-result-object v2

    iget-object v2, v2, Llfk;->g:Llfe;

    iget-object v3, p0, Llfl;->b:Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;

    invoke-static {v3}, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->b(Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;)Llfk;

    move-result-object v3

    iget-object v3, v3, Llfk;->e:Llfi;

    iget-object v4, p0, Llfl;->b:Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;

    invoke-static {v4}, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->b(Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;)Llfk;

    move-result-object v4

    iget v4, v4, Llfk;->d:I

    invoke-virtual {v2, v3, v4}, Llfe;->a(Llfi;I)V

    .line 291
    iget-object v2, p0, Llfl;->b:Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;

    invoke-static {v2}, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->b(Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;)Llfk;

    move-result-object v2

    iget-object v2, v2, Llfk;->g:Llfe;

    iget-object v3, p0, Llfl;->b:Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;

    invoke-static {v3}, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->b(Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;)Llfk;

    move-result-object v3

    iget v3, v3, Llfk;->b:I

    iget-object v4, p0, Llfl;->b:Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;

    invoke-static {v4}, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->b(Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;)Llfk;

    move-result-object v4

    iget v4, v4, Llfk;->c:I

    iget-object v5, p0, Llfl;->b:Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;

    .line 292
    invoke-static {v5}, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->b(Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;)Llfk;

    move-result-object v5

    iget v5, v5, Llfk;->a:F

    .line 291
    invoke-virtual {v2, v3, v4, v5}, Llfe;->a(IIF)V

    .line 293
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 294
    iget-object v1, p0, Llfl;->b:Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;

    invoke-static {v1}, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->b(Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;)Llfk;

    move-result-object v1

    iget-object v1, v1, Llfk;->g:Llfe;

    iget-object v2, p0, Llfl;->a:Lify;

    invoke-virtual {v1, v2}, Llfe;->a(Lifx;)Z

    move-result v1

    .line 295
    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    .line 296
    iget-object v1, p0, Llfl;->b:Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;

    invoke-static {v1}, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->c(Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 299
    :try_start_1
    iget-object v2, p0, Llfl;->b:Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;

    invoke-static {v2}, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->b(Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;)Llfk;

    move-result-object v2

    iget-object v2, v2, Llfk;->f:Ljava/lang/Runnable;

    if-ne v2, v0, :cond_0

    .line 300
    iget-object v2, p0, Llfl;->b:Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;

    invoke-static {v2}, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->b(Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;)Llfk;

    move-result-object v2

    const/4 v3, 0x0

    iput-object v3, v2, Llfk;->f:Ljava/lang/Runnable;

    .line 302
    :cond_0
    iget-object v2, p0, Llfl;->b:Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;

    invoke-virtual {v2, v0}, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->post(Ljava/lang/Runnable;)Z

    .line 303
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 305
    :cond_1
    return-void

    .line 293
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 303
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Llfl;->a:Lify;

    invoke-virtual {v0, p2, p3}, Lify;->a(II)V

    .line 281
    iget-object v0, p0, Llfl;->b:Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;

    invoke-static {v0}, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->b(Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;)Llfk;

    move-result-object v0

    iget-object v0, v0, Llfk;->g:Llfe;

    invoke-virtual {v0, p2, p3}, Llfe;->a(II)V

    .line 282
    return-void
.end method

.method public onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V
    .locals 3

    .prologue
    .line 273
    new-instance v0, Lify;

    invoke-direct {v0}, Lify;-><init>()V

    iput-object v0, p0, Llfl;->a:Lify;

    .line 274
    invoke-static {}, Lifv;->k()V

    .line 275
    iget-object v0, p0, Llfl;->b:Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;

    invoke-static {v0}, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->b(Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;)Llfk;

    move-result-object v0

    iget-object v0, v0, Llfk;->g:Llfe;

    iget-object v1, p0, Llfl;->b:Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;

    invoke-static {v1}, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->b(Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;)Llfk;

    move-result-object v1

    iget-object v1, v1, Llfk;->e:Llfi;

    iget-object v2, p0, Llfl;->b:Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;

    invoke-static {v2}, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->b(Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;)Llfk;

    move-result-object v2

    iget v2, v2, Llfk;->d:I

    invoke-virtual {v0, v1, v2}, Llfe;->a(Llfi;I)V

    .line 276
    return-void
.end method
