.class public final Llzo;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmpo;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 17899
    invoke-direct {p0}, Loxq;-><init>()V

    .line 17902
    iput-object v0, p0, Llzo;->apiHeader:Llyq;

    .line 17905
    iput-object v0, p0, Llzo;->a:Lmpo;

    .line 17899
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 17922
    const/4 v0, 0x0

    .line 17923
    iget-object v1, p0, Llzo;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 17924
    const/4 v0, 0x1

    iget-object v1, p0, Llzo;->apiHeader:Llyq;

    .line 17925
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 17927
    :cond_0
    iget-object v1, p0, Llzo;->a:Lmpo;

    if-eqz v1, :cond_1

    .line 17928
    const/4 v1, 0x2

    iget-object v2, p0, Llzo;->a:Lmpo;

    .line 17929
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 17931
    :cond_1
    iget-object v1, p0, Llzo;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 17932
    iput v0, p0, Llzo;->ai:I

    .line 17933
    return v0
.end method

.method public a(Loxn;)Llzo;
    .locals 2

    .prologue
    .line 17941
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 17942
    sparse-switch v0, :sswitch_data_0

    .line 17946
    iget-object v1, p0, Llzo;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 17947
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llzo;->ah:Ljava/util/List;

    .line 17950
    :cond_1
    iget-object v1, p0, Llzo;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 17952
    :sswitch_0
    return-object p0

    .line 17957
    :sswitch_1
    iget-object v0, p0, Llzo;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 17958
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Llzo;->apiHeader:Llyq;

    .line 17960
    :cond_2
    iget-object v0, p0, Llzo;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 17964
    :sswitch_2
    iget-object v0, p0, Llzo;->a:Lmpo;

    if-nez v0, :cond_3

    .line 17965
    new-instance v0, Lmpo;

    invoke-direct {v0}, Lmpo;-><init>()V

    iput-object v0, p0, Llzo;->a:Lmpo;

    .line 17967
    :cond_3
    iget-object v0, p0, Llzo;->a:Lmpo;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 17942
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 17910
    iget-object v0, p0, Llzo;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 17911
    const/4 v0, 0x1

    iget-object v1, p0, Llzo;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 17913
    :cond_0
    iget-object v0, p0, Llzo;->a:Lmpo;

    if-eqz v0, :cond_1

    .line 17914
    const/4 v0, 0x2

    iget-object v1, p0, Llzo;->a:Lmpo;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 17916
    :cond_1
    iget-object v0, p0, Llzo;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 17918
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 17895
    invoke-virtual {p0, p1}, Llzo;->a(Loxn;)Llzo;

    move-result-object v0

    return-object v0
.end method
