.class final Lcxx;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ldgv;


# instance fields
.field final a:Lice;

.field private synthetic b:Lcxu;


# direct methods
.method constructor <init>(Lcxu;)V
    .locals 3

    .prologue
    .line 104
    iput-object p1, p0, Lcxx;->b:Lcxu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    new-instance v0, Lice;

    iget-object v1, p0, Lcxx;->b:Lcxu;

    iget-object v1, p0, Lcxx;->b:Lcxu;

    .line 106
    invoke-virtual {v1}, Lcxu;->z_()Llqr;

    move-result-object v1

    new-instance v2, Lcxy;

    invoke-direct {v2, p0}, Lcxy;-><init>(Lcxx;)V

    invoke-direct {v0, v1, v2}, Lice;-><init>(Llqr;Lici;)V

    iput-object v0, p0, Lcxx;->a:Lice;

    .line 105
    return-void
.end method


# virtual methods
.method public a()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 116
    iget-object v0, p0, Lcxx;->b:Lcxu;

    invoke-virtual {v0}, Lcxu;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    .line 117
    const-string v0, "connectivity"

    .line 118
    invoke-virtual {v4, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 120
    invoke-static {v4}, Llsa;->a(Landroid/content/Context;)Z

    move-result v5

    .line 121
    if-eqz v5, :cond_1

    .line 122
    invoke-static {v0}, Ler;->a(Landroid/net/ConnectivityManager;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v3, v1

    .line 123
    :goto_0
    if-eqz v5, :cond_2

    if-nez v3, :cond_2

    move v0, v1

    .line 126
    :goto_1
    invoke-static {v4}, Libq;->b(Landroid/content/Context;)Z

    move-result v2

    .line 128
    if-nez v2, :cond_0

    if-eqz v0, :cond_0

    .line 129
    invoke-static {v4}, Libq;->a(Landroid/content/Context;)V

    .line 132
    :cond_0
    invoke-static {v4}, Libq;->c(Landroid/content/Context;)Z

    move-result v0

    .line 133
    if-eqz v0, :cond_3

    .line 134
    iget-object v0, p0, Lcxx;->a:Lice;

    invoke-virtual {v0}, Lice;->b()V

    .line 143
    :goto_2
    return-void

    :cond_1
    move v3, v2

    .line 122
    goto :goto_0

    :cond_2
    move v0, v2

    .line 123
    goto :goto_1

    .line 138
    :cond_3
    if-nez v2, :cond_4

    if-nez v3, :cond_5

    .line 139
    :cond_4
    invoke-virtual {p0}, Lcxx;->b()V

    goto :goto_2

    .line 141
    :cond_5
    new-instance v0, Lcxz;

    invoke-direct {v0, p0}, Lcxz;-><init>(Lcxx;)V

    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcxx;->b:Lcxu;

    invoke-virtual {v3}, Lcxu;->n()Lz;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0a0417

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f0a0418

    invoke-virtual {v2, v1, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f0a0419

    invoke-virtual {v2, v1, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_2
.end method

.method b()V
    .locals 4

    .prologue
    .line 168
    iget-object v0, p0, Lcxx;->b:Lcxu;

    invoke-virtual {v0}, Lcxu;->n()Lz;

    move-result-object v0

    .line 169
    iget-object v1, p0, Lcxx;->b:Lcxu;

    invoke-virtual {v1}, Lcxu;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "account_id"

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 171
    if-eqz v0, :cond_0

    .line 172
    iget-object v2, p0, Lcxx;->b:Lcxu;

    invoke-static {v2}, Lcxu;->b(Lcxu;)Ldeo;

    move-result-object v2

    invoke-virtual {v2}, Ldeo;->a()Lddl;

    move-result-object v2

    .line 173
    invoke-interface {v2, v0, v1}, Lddl;->a(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v0

    .line 174
    invoke-interface {v2}, Lddl;->U()Z

    move-result v1

    if-nez v1, :cond_1

    .line 175
    iget-object v1, p0, Lcxx;->b:Lcxu;

    invoke-static {v1}, Lcxu;->e(Lcxu;)Lhke;

    move-result-object v1

    const v2, 0x7f1000bb

    invoke-virtual {v1, v2, v0}, Lhke;->a(ILandroid/content/Intent;)V

    .line 180
    :cond_0
    :goto_0
    return-void

    .line 177
    :cond_1
    iget-object v1, p0, Lcxx;->b:Lcxu;

    invoke-static {v1}, Lcxu;->e(Lcxu;)Lhke;

    move-result-object v1

    const v2, 0x7f1000bc

    invoke-virtual {v1, v2, v0}, Lhke;->a(ILandroid/content/Intent;)V

    goto :goto_0
.end method
