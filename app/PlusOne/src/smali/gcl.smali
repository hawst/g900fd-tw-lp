.class public final Lgcl;
.super Lhya;
.source "PG"


# instance fields
.field private synthetic a:Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 129
    iput-object p1, p0, Lgcl;->a:Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;

    .line 130
    invoke-direct {p0, p2}, Lhya;-><init>(Landroid/content/Context;)V

    .line 133
    invoke-virtual {p0, v0, v1}, Lgcl;->b(ZZ)V

    .line 134
    invoke-virtual {p0, v0, v1}, Lgcl;->b(ZZ)V

    .line 135
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 139
    const/4 v0, 0x2

    return v0
.end method

.method protected a(II)I
    .locals 0

    .prologue
    .line 144
    return p1
.end method

.method protected a(Landroid/content/Context;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 171
    packed-switch p2, :pswitch_data_0

    .line 178
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 173
    :pswitch_0
    new-instance v0, Lhxf;

    invoke-direct {v0, p1}, Lhxf;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 176
    :pswitch_1
    invoke-static {p1}, Ljpg;->a(Landroid/content/Context;)Ljpg;

    move-result-object v0

    goto :goto_0

    .line 171
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected a(Landroid/content/Context;ILandroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 150
    iget-object v0, p0, Lgcl;->a:Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400fc

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected a(Landroid/view/View;ILandroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 155
    check-cast p1, Landroid/widget/TextView;

    .line 156
    packed-switch p2, :pswitch_data_0

    .line 166
    :goto_0
    return-void

    .line 158
    :pswitch_0
    const v0, 0x7f0a082b

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 162
    :pswitch_1
    const v0, 0x7f0a082c

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 156
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected a(Landroid/view/View;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)V
    .locals 9

    .prologue
    const/4 v3, 0x2

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 184
    packed-switch p2, :pswitch_data_0

    .line 217
    :goto_0
    return-void

    :pswitch_0
    move-object v0, p1

    .line 186
    check-cast v0, Lhxf;

    .line 187
    const/4 v1, 0x4

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 189
    invoke-interface {p3, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 191
    invoke-interface {p3, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x3

    .line 192
    invoke-interface {p3, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iget-object v5, p0, Lgcl;->a:Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;

    iget-object v6, p0, Lgcl;->a:Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;

    .line 194
    invoke-static {v6}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->a(Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;)Livx;

    move-result-object v6

    invoke-virtual {v6}, Livx;->d()I

    move-result v6

    .line 193
    invoke-static {v5, v6, v2}, Lhxm;->a(Landroid/content/Context;II)Z

    move-result v5

    .line 188
    invoke-virtual/range {v0 .. v5}, Lhxf;->a(Ljava/lang/String;ILjava/lang/String;IZ)V

    .line 196
    iget-object v1, p0, Lgcl;->a:Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;

    invoke-static {v1}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->b(Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;)Ljava/util/HashMap;

    move-result-object v1

    .line 197
    invoke-interface {p3, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 196
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 199
    iget-object v2, p0, Lgcl;->a:Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetConfigurationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 201
    if-nez v1, :cond_0

    .line 202
    const v1, 0x7f0a0693

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 208
    :goto_1
    invoke-virtual {v0, v1}, Lhxf;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 204
    :cond_0
    const v3, 0x7f11005d

    new-array v4, v7, [Ljava/lang/Object;

    .line 206
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    .line 204
    invoke-virtual {v2, v3, v1, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 212
    :pswitch_1
    check-cast p1, Ljpg;

    .line 213
    invoke-interface {p3, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 214
    invoke-interface {p3, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 215
    invoke-static {v1}, Llsy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Ljpg;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    invoke-interface {p3, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljpg;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 184
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
