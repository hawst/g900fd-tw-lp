.class public final Lktr;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lktq;


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:[Ljava/lang/String;

.field private static final c:[Ljava/lang/String;

.field private static final d:[Ljava/lang/String;

.field private static f:Landroid/util/SparseIntArray;


# instance fields
.field private e:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 64
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "post_visibility"

    aput-object v1, v0, v2

    const-string v1, "joinability"

    aput-object v1, v0, v3

    const-string v1, "square_streams"

    aput-object v1, v0, v4

    sput-object v0, Lktr;->a:[Ljava/lang/String;

    .line 74
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "auto_subscribe"

    aput-object v1, v0, v2

    sput-object v0, Lktr;->b:[Ljava/lang/String;

    .line 80
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "token"

    aput-object v1, v0, v2

    const-string v1, "member_count"

    aput-object v1, v0, v3

    sput-object v0, Lktr;->c:[Ljava/lang/String;

    .line 85
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "qualified_id"

    aput-object v1, v0, v2

    const-string v1, "membership_status"

    aput-object v1, v0, v3

    sput-object v0, Lktr;->d:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106
    iput-object p1, p0, Lktr;->e:Landroid/content/Context;

    .line 107
    return-void
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/util/Collection;)I
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Lnsh;",
            ">;)I"
        }
    .end annotation

    .prologue
    const/4 v8, 0x5

    .line 923
    const/4 v0, 0x0

    .line 924
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3, v8}, Landroid/content/ContentValues;-><init>(I)V

    .line 925
    invoke-interface {p3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnsh;

    .line 926
    const-string v2, "link_square_id"

    invoke-virtual {v3, v2, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 927
    iget-object v2, v0, Lnsh;->b:Ljava/lang/String;

    if-nez v2, :cond_2

    .line 928
    const-string v5, "qualified_id"

    const-string v2, "t:"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    iget-object v2, v0, Lnsh;->f:Lnsa;

    iget-object v2, v2, Lnsa;->b:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {v6, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_1
    invoke-virtual {v3, v5, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 930
    const-string v2, "name"

    iget-object v5, v0, Lnsh;->f:Lnsa;

    iget-object v5, v5, Lnsa;->c:Ljava/lang/String;

    invoke-virtual {v3, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 931
    const-string v2, "avatar"

    const-string v5, ""

    invoke-virtual {v3, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 939
    :goto_2
    const-string v2, "membership_status"

    iget v5, v0, Lnsh;->e:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 940
    const-string v2, "square_contact"

    const/4 v5, 0x0

    invoke-virtual {p1, v2, v5, v3, v8}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    .line 942
    add-int/lit8 v1, v1, 0x1

    .line 944
    const-string v2, "SquaresDataServiceImpl"

    const/4 v5, 0x3

    invoke-static {v2, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 945
    iget-object v2, v0, Lnsh;->b:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, v0, Lnsh;->d:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x16

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Insert user: id="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " name="

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 928
    :cond_1
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 933
    :cond_2
    const-string v5, "qualified_id"

    const-string v2, "g:"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    iget-object v2, v0, Lnsh;->b:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_3

    invoke-virtual {v6, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_3
    invoke-virtual {v3, v5, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 935
    const-string v2, "name"

    iget-object v5, v0, Lnsh;->d:Ljava/lang/String;

    invoke-virtual {v3, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 936
    iget-object v2, v0, Lnsh;->c:Ljava/lang/String;

    invoke-static {v2}, Lhst;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 937
    const-string v5, "avatar"

    invoke-virtual {v3, v5, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 933
    :cond_3
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3

    .line 948
    :cond_4
    return v1
.end method

.method private static a(Lkts;)Landroid/content/ContentValues;
    .locals 3

    .prologue
    .line 1249
    iget-object v0, p0, Lkts;->a:Lnsr;

    invoke-static {v0}, Lktr;->a(Lnsr;)Landroid/content/ContentValues;

    move-result-object v0

    .line 1251
    const-string v1, "inviter_gaia_id"

    invoke-virtual {p0}, Lkts;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1252
    const-string v1, "inviter_name"

    invoke-virtual {p0}, Lkts;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1253
    const-string v1, "inviter_photo_url"

    .line 1254
    invoke-virtual {p0}, Lkts;->c()Ljava/lang/String;

    move-result-object v2

    .line 1253
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1255
    const-string v1, "list_category"

    iget v2, p0, Lkts;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1256
    const-string v1, "sort_index"

    iget v2, p0, Lkts;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1258
    iget-object v1, p0, Lkts;->d:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1259
    const-string v1, "suggestion_id"

    iget-object v2, p0, Lkts;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1262
    :cond_0
    return-object v0
.end method

.method public static a(Lnsr;)Landroid/content/ContentValues;
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1276
    iget-object v0, p0, Lnsr;->b:Lnsb;

    iget-object v3, v0, Lnsb;->b:Lnsc;

    .line 1277
    iget-object v4, p0, Lnsr;->h:Lnst;

    .line 1279
    iget v0, p0, Lnsr;->f:I

    .line 1280
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 1281
    const-string v6, "square_id"

    iget-object v7, p0, Lnsr;->b:Lnsb;

    iget-object v7, v7, Lnsb;->a:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1282
    const-string v6, "square_name"

    iget-object v7, v3, Lnsc;->a:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1283
    const-string v6, "tagline"

    iget-object v7, v3, Lnsc;->b:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1284
    const-string v6, "photo_url"

    iget-object v7, v3, Lnsc;->c:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1285
    const-string v6, "about_text"

    iget-object v7, v3, Lnsc;->e:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1286
    const-string v6, "joinability"

    iget-object v7, p0, Lnsr;->b:Lnsb;

    iget v7, v7, Lnsb;->d:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1287
    const-string v6, "membership_status"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1288
    const-string v6, "restricted_domain"

    iget-object v7, v3, Lnsc;->g:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1290
    iget-object v6, p0, Lnsr;->b:Lnsb;

    iget-object v6, v6, Lnsb;->c:Lnsf;

    if-eqz v6, :cond_0

    .line 1291
    const-string v6, "post_visibility"

    iget-object v7, p0, Lnsr;->b:Lnsb;

    iget-object v7, v7, Lnsb;->c:Lnsf;

    iget v7, v7, Lnsf;->a:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1294
    :cond_0
    if-eqz v4, :cond_9

    .line 1295
    const-string v6, "is_member"

    iget-object v0, v4, Lnst;->a:Ljava/lang/Boolean;

    .line 1296
    invoke-static {v0}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 1295
    invoke-virtual {v5, v6, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1297
    const-string v6, "can_see_members"

    iget-object v0, v4, Lnst;->b:Ljava/lang/Boolean;

    .line 1298
    invoke-static {v0}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 1297
    invoke-virtual {v5, v6, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1299
    const-string v6, "can_see_posts"

    iget-object v0, v4, Lnst;->c:Ljava/lang/Boolean;

    .line 1300
    invoke-static {v0}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 1299
    invoke-virtual {v5, v6, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1301
    const-string v6, "can_join"

    iget-object v0, v4, Lnst;->d:Ljava/lang/Boolean;

    .line 1302
    invoke-static {v0}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    :goto_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 1301
    invoke-virtual {v5, v6, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1303
    const-string v6, "can_request_to_join"

    iget-object v0, v4, Lnst;->e:Ljava/lang/Boolean;

    .line 1304
    invoke-static {v0}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v1

    :goto_4
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 1303
    invoke-virtual {v5, v6, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1305
    const-string v6, "can_share"

    iget-object v0, v4, Lnst;->f:Ljava/lang/Boolean;

    .line 1306
    invoke-static {v0}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v1

    :goto_5
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 1305
    invoke-virtual {v5, v6, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1307
    const-string v6, "can_invite"

    iget-object v0, v4, Lnst;->g:Ljava/lang/Boolean;

    .line 1308
    invoke-static {v0}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v0

    if-eqz v0, :cond_8

    move v0, v1

    :goto_6
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 1307
    invoke-virtual {v5, v6, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1316
    :goto_7
    iget-object v0, p0, Lnsr;->d:Lnsv;

    if-eqz v0, :cond_1

    .line 1317
    const-string v0, "member_count"

    iget-object v4, p0, Lnsr;->d:Lnsv;

    iget-object v4, v4, Lnsv;->a:Ljava/lang/Integer;

    .line 1318
    invoke-static {v4}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 1317
    invoke-virtual {v5, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1321
    :cond_1
    const-string v4, "notifications_enabled"

    iget v0, p0, Lnsr;->g:I

    if-ne v0, v1, :cond_c

    move v0, v1

    .line 1322
    :goto_8
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 1321
    invoke-virtual {v5, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1324
    iget-object v0, p0, Lnsr;->c:Lnsw;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lnsr;->c:Lnsw;

    iget-object v0, v0, Lnsw;->a:[Lnsj;

    invoke-static {v0}, Llrv;->a([Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 1325
    iget-object v0, p0, Lnsr;->c:Lnsw;

    iget-object v4, v0, Lnsw;->a:[Lnsj;

    .line 1326
    array-length v0, v4

    new-array v6, v0, [Lktg;

    move v0, v2

    .line 1327
    :goto_9
    array-length v7, v6

    if-ge v0, v7, :cond_d

    .line 1328
    aget-object v7, v4, v0

    .line 1329
    new-instance v8, Lktg;

    iget-object v9, v7, Lnsj;->b:Ljava/lang/String;

    iget-object v10, v7, Lnsj;->c:Ljava/lang/String;

    iget-object v7, v7, Lnsj;->d:Ljava/lang/String;

    invoke-direct {v8, v9, v10, v7}, Lktg;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v8, v6, v0

    .line 1327
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    :cond_2
    move v0, v2

    .line 1296
    goto/16 :goto_0

    :cond_3
    move v0, v2

    .line 1298
    goto/16 :goto_1

    :cond_4
    move v0, v2

    .line 1300
    goto/16 :goto_2

    :cond_5
    move v0, v2

    .line 1302
    goto/16 :goto_3

    :cond_6
    move v0, v2

    .line 1304
    goto/16 :goto_4

    :cond_7
    move v0, v2

    .line 1306
    goto :goto_5

    :cond_8
    move v0, v2

    .line 1308
    goto :goto_6

    .line 1310
    :cond_9
    const-string v4, "is_member"

    const/4 v6, 0x3

    if-eq v0, v6, :cond_a

    const/4 v6, 0x2

    if-eq v0, v6, :cond_a

    if-ne v0, v1, :cond_b

    :cond_a
    move v0, v1

    .line 1311
    :goto_a
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 1310
    invoke-virtual {v5, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_7

    :cond_b
    move v0, v2

    goto :goto_a

    :cond_c
    move v0, v2

    .line 1321
    goto :goto_8

    .line 1331
    :cond_d
    const-string v0, "square_streams"

    .line 1332
    invoke-static {v6}, Lktg;->a([Lktg;)[B

    move-result-object v4

    .line 1331
    invoke-virtual {v5, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1335
    :cond_e
    iget-object v4, p0, Lnsr;->i:Lnsi;

    .line 1336
    if-eqz v4, :cond_f

    .line 1337
    const-string v6, "auto_subscribe"

    iget-object v0, v4, Lnsi;->b:Ljava/lang/Boolean;

    .line 1338
    invoke-static {v0}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v0

    if-eqz v0, :cond_11

    move v0, v1

    :goto_b
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 1337
    invoke-virtual {v5, v6, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1339
    const-string v0, "disable_subscription"

    iget-object v4, v4, Lnsi;->a:Ljava/lang/Boolean;

    .line 1340
    invoke-static {v4}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v4

    if-eqz v4, :cond_12

    :goto_c
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 1339
    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1343
    :cond_f
    iget-object v0, p0, Lnsr;->e:Lnsu;

    .line 1344
    if-eqz v0, :cond_10

    .line 1345
    const-string v1, "unread_count"

    iget-object v0, v0, Lnsu;->a:Ljava/lang/Integer;

    .line 1346
    invoke-static {v0}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 1345
    invoke-virtual {v5, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1349
    :cond_10
    const-string v0, "related_links"

    iget-object v1, v3, Lnsc;->f:Lnse;

    .line 1350
    invoke-static {v1}, Lktf;->a(Lnse;)[B

    move-result-object v1

    .line 1349
    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1352
    iget-object v0, v3, Lnsc;->d:Lpdt;

    .line 1353
    if-eqz v0, :cond_13

    .line 1354
    const-string v1, "location"

    const-class v2, Lpdt;

    .line 1355
    invoke-static {v0, v2}, Lhyr;->a(Loxu;Ljava/lang/Class;)[B

    move-result-object v0

    .line 1354
    invoke-virtual {v5, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1360
    :goto_d
    return-object v5

    :cond_11
    move v0, v2

    .line 1338
    goto :goto_b

    :cond_12
    move v1, v2

    .line 1340
    goto :goto_c

    .line 1357
    :cond_13
    const-string v0, "location"

    invoke-virtual {v5, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_d
.end method

.method private a(I[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 10

    .prologue
    .line 895
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 897
    const-string v1, "squares"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 898
    sget-object v1, Lktv;->a:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 900
    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "sort_index"

    move-object v9, v1

    .line 904
    :goto_0
    const-string v1, "SquaresDataServiceImpl"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 905
    const-string v8, "QUERY: "

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v6, p5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildQuery([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v8, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 909
    :cond_0
    :goto_1
    iget-object v1, p0, Lktr;->e:Landroid/content/Context;

    invoke-static {v1, p1}, Lhzt;->b(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v7, v9

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 912
    const-string v1, "SquaresDataServiceImpl"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 913
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x1a

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "QUERY results: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 916
    :cond_1
    iget-object v1, p0, Lktr;->e:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lktp;->a:Landroid/net/Uri;

    invoke-interface {v0, v1, v2}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 918
    return-object v0

    :cond_2
    move-object v9, p5

    .line 900
    goto :goto_0

    .line 905
    :cond_3
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v8}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private a(ILandroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;II)V
    .locals 6

    .prologue
    .line 953
    const/4 v0, 0x1

    if-ne p4, v0, :cond_1

    .line 955
    const/4 v4, 0x2

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lktr;->a(ILandroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;II)V

    .line 963
    :cond_0
    :goto_0
    packed-switch p4, :pswitch_data_0

    .line 976
    :goto_1
    return-void

    .line 957
    :cond_1
    const/4 v0, 0x2

    if-ne p4, v0, :cond_0

    .line 959
    const/4 v4, 0x3

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lktr;->a(ILandroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;II)V

    goto :goto_0

    .line 969
    :pswitch_0
    const-string v0, "UPDATE square_member_status SET member_count = member_count + ? WHERE square_id=? AND membership_status=?"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    int-to-long v4, p5

    .line 970
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p3, v1, v2

    const/4 v2, 0x2

    int-to-long v4, p4

    .line 972
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    .line 969
    invoke-virtual {p2, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 963
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private a(ILjava/lang/String;[Lktg;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1007
    if-eqz p3, :cond_0

    .line 1009
    array-length v2, p3

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_0

    .line 1010
    aget-object v3, p3, v0

    .line 1011
    invoke-virtual {v3}, Lktg;->a()Ljava/lang/String;

    move-result-object v3

    .line 1012
    invoke-static {p2, v3, v1}, Llbc;->a(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    .line 1013
    iget-object v4, p0, Lktr;->e:Landroid/content/Context;

    invoke-static {v4, p1, v3}, Llap;->f(Landroid/content/Context;ILjava/lang/String;)V

    .line 1009
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1017
    :cond_0
    const/4 v0, 0x0

    invoke-static {p2, v0, v1}, Llbc;->a(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 1018
    iget-object v1, p0, Lktr;->e:Landroid/content/Context;

    invoke-static {v1, p1, v0}, Llap;->f(Landroid/content/Context;ILjava/lang/String;)V

    .line 1019
    return-void
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Lnry;)V
    .locals 9

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 981
    array-length v5, p3

    move v4, v1

    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v6, p3, v4

    .line 982
    iget-object v2, v6, Lnry;->e:[Lnsh;

    .line 985
    if-eqz v2, :cond_2

    .line 986
    iget-object v0, v6, Lnry;->c:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 987
    array-length v2, v2

    iget-object v7, v6, Lnry;->c:Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    if-ge v2, v7, :cond_1

    .line 988
    iget-object v2, v6, Lnry;->d:Ljava/lang/String;

    .line 993
    :goto_1
    iget v6, v6, Lnry;->b:I

    .line 995
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 996
    const-string v8, "square_id"

    invoke-virtual {v7, v8, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 997
    const-string v8, "membership_status"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v7, v8, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 998
    const-string v6, "token"

    invoke-virtual {v7, v6, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 999
    const-string v2, "member_count"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v7, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1001
    const-string v0, "square_member_status"

    const/4 v2, 0x5

    invoke-virtual {p1, v0, v3, v7, v2}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    .line 981
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 1004
    :cond_0
    return-void

    :cond_1
    move-object v2, v3

    goto :goto_1

    :cond_2
    move v0, v1

    move-object v2, v3

    goto :goto_1
.end method

.method private a(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 1393
    iget-object v0, p0, Lktr;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1394
    return-void
.end method

.method private static a(Landroid/database/Cursor;Lnsr;)Z
    .locals 27

    .prologue
    .line 1141
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 1142
    const/4 v2, 0x2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 1143
    const/4 v2, 0x3

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 1144
    const/4 v2, 0x4

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 1145
    const/16 v2, 0x1e

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 1146
    const/4 v2, 0x5

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    .line 1147
    const/4 v2, 0x6

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v19

    .line 1148
    const/4 v2, 0x7

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v20

    .line 1149
    const/16 v2, 0xa

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v21

    .line 1150
    const/16 v2, 0x1b

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v22

    .line 1151
    const/16 v2, 0x8

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    move v3, v2

    .line 1152
    :goto_0
    const/16 v2, 0xb

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    move v4, v2

    .line 1153
    :goto_1
    const/16 v2, 0xc

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    move v5, v2

    .line 1154
    :goto_2
    const/16 v2, 0xd

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    move v6, v2

    .line 1155
    :goto_3
    const/16 v2, 0xe

    .line 1156
    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x1

    move v7, v2

    .line 1157
    :goto_4
    const/16 v2, 0xf

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x1

    move v8, v2

    .line 1158
    :goto_5
    const/16 v2, 0x10

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_7

    const/4 v2, 0x1

    move v9, v2

    .line 1159
    :goto_6
    const/16 v2, 0x11

    .line 1160
    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_8

    const/4 v2, 0x1

    move v10, v2

    .line 1161
    :goto_7
    const/16 v2, 0x19

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_9

    const/4 v2, 0x1

    move v11, v2

    .line 1162
    :goto_8
    const/16 v2, 0x1a

    .line 1163
    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_a

    const/4 v2, 0x1

    move v12, v2

    .line 1164
    :goto_9
    const/16 v2, 0x12

    .line 1165
    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    invoke-static {v2}, Lktg;->a([B)[Lktg;

    move-result-object v23

    .line 1166
    const/16 v2, 0x1f

    .line 1167
    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    invoke-static {v2}, Lktf;->a([B)Lnse;

    move-result-object v24

    .line 1168
    new-instance v2, Lpdt;

    invoke-direct {v2}, Lpdt;-><init>()V

    const/16 v25, 0x20

    .line 1169
    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v25

    .line 1168
    move-object/from16 v0, v25

    invoke-static {v2, v0}, Lhys;->a(Loxu;[B)Loxu;

    move-result-object v2

    check-cast v2, Lpdt;

    .line 1172
    move-object/from16 v0, p1

    iget-object v0, v0, Lnsr;->b:Lnsb;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lnsb;->b:Lnsc;

    move-object/from16 v25, v0

    .line 1173
    move-object/from16 v0, v25

    iget-object v0, v0, Lnsc;->a:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-static {v13, v0}, Llsu;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_0

    move-object/from16 v0, v25

    iget-object v13, v0, Lnsc;->b:Ljava/lang/String;

    .line 1174
    invoke-static {v14, v13}, Llsu;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_0

    move-object/from16 v0, v25

    iget-object v13, v0, Lnsc;->c:Ljava/lang/String;

    .line 1175
    invoke-static {v15, v13}, Llsu;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_0

    move-object/from16 v0, v25

    iget-object v13, v0, Lnsc;->e:Ljava/lang/String;

    .line 1176
    move-object/from16 v0, v16

    invoke-static {v0, v13}, Llsu;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_0

    move-object/from16 v0, p1

    iget v13, v0, Lnsr;->f:I

    move/from16 v0, v20

    if-ne v0, v13, :cond_0

    move-object/from16 v0, p1

    iget-object v13, v0, Lnsr;->b:Lnsb;

    iget v13, v13, Lnsb;->d:I

    move/from16 v0, v18

    if-ne v0, v13, :cond_0

    move-object/from16 v0, v25

    iget-object v13, v0, Lnsc;->g:Ljava/lang/String;

    .line 1179
    move-object/from16 v0, v17

    invoke-static {v0, v13}, Llsu;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_b

    .line 1180
    :cond_0
    const/4 v2, 0x1

    .line 1235
    :goto_a
    return v2

    .line 1151
    :cond_1
    const/4 v2, 0x0

    move v3, v2

    goto/16 :goto_0

    .line 1152
    :cond_2
    const/4 v2, 0x0

    move v4, v2

    goto/16 :goto_1

    .line 1153
    :cond_3
    const/4 v2, 0x0

    move v5, v2

    goto/16 :goto_2

    .line 1154
    :cond_4
    const/4 v2, 0x0

    move v6, v2

    goto/16 :goto_3

    .line 1156
    :cond_5
    const/4 v2, 0x0

    move v7, v2

    goto/16 :goto_4

    .line 1157
    :cond_6
    const/4 v2, 0x0

    move v8, v2

    goto/16 :goto_5

    .line 1158
    :cond_7
    const/4 v2, 0x0

    move v9, v2

    goto/16 :goto_6

    .line 1160
    :cond_8
    const/4 v2, 0x0

    move v10, v2

    goto/16 :goto_7

    .line 1161
    :cond_9
    const/4 v2, 0x0

    move v11, v2

    goto/16 :goto_8

    .line 1163
    :cond_a
    const/4 v2, 0x0

    move v12, v2

    goto/16 :goto_9

    .line 1183
    :cond_b
    move-object/from16 v0, p1

    iget-object v13, v0, Lnsr;->h:Lnst;

    .line 1184
    if-eqz v13, :cond_d

    iget-object v14, v13, Lnst;->a:Ljava/lang/Boolean;

    .line 1185
    invoke-static {v14}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v14

    if-ne v3, v14, :cond_c

    iget-object v3, v13, Lnst;->b:Ljava/lang/Boolean;

    .line 1186
    invoke-static {v3}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v3

    if-ne v4, v3, :cond_c

    iget-object v3, v13, Lnst;->c:Ljava/lang/Boolean;

    .line 1187
    invoke-static {v3}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v3

    if-ne v5, v3, :cond_c

    iget-object v3, v13, Lnst;->d:Ljava/lang/Boolean;

    .line 1188
    invoke-static {v3}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v3

    if-ne v6, v3, :cond_c

    iget-object v3, v13, Lnst;->e:Ljava/lang/Boolean;

    .line 1189
    invoke-static {v3}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v3

    if-ne v7, v3, :cond_c

    iget-object v3, v13, Lnst;->f:Ljava/lang/Boolean;

    .line 1190
    invoke-static {v3}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v3

    if-ne v8, v3, :cond_c

    iget-object v3, v13, Lnst;->g:Ljava/lang/Boolean;

    .line 1191
    invoke-static {v3}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v3

    if-eq v9, v3, :cond_d

    .line 1192
    :cond_c
    const/4 v2, 0x1

    goto :goto_a

    .line 1195
    :cond_d
    move-object/from16 v0, p1

    iget-object v3, v0, Lnsr;->b:Lnsb;

    iget-object v3, v3, Lnsb;->c:Lnsf;

    if-eqz v3, :cond_e

    move-object/from16 v0, p1

    iget-object v3, v0, Lnsr;->b:Lnsb;

    iget-object v3, v3, Lnsb;->c:Lnsf;

    iget v3, v3, Lnsf;->a:I

    move/from16 v0, v21

    if-eq v0, v3, :cond_e

    .line 1197
    const/4 v2, 0x1

    goto :goto_a

    .line 1200
    :cond_e
    move-object/from16 v0, p1

    iget v3, v0, Lnsr;->g:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_f

    const/4 v3, 0x1

    :goto_b
    if-eq v10, v3, :cond_10

    .line 1202
    const/4 v2, 0x1

    goto/16 :goto_a

    .line 1200
    :cond_f
    const/4 v3, 0x0

    goto :goto_b

    .line 1205
    :cond_10
    move-object/from16 v0, p1

    iget-object v3, v0, Lnsr;->d:Lnsv;

    if-eqz v3, :cond_11

    move-object/from16 v0, p1

    iget-object v3, v0, Lnsr;->d:Lnsv;

    iget-object v3, v3, Lnsv;->a:Ljava/lang/Integer;

    .line 1206
    invoke-static {v3}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v3

    move/from16 v0, v19

    if-eq v0, v3, :cond_11

    .line 1207
    const/4 v2, 0x1

    goto/16 :goto_a

    .line 1210
    :cond_11
    move-object/from16 v0, p1

    iget-object v3, v0, Lnsr;->c:Lnsw;

    if-eqz v3, :cond_19

    move-object/from16 v0, p1

    iget-object v3, v0, Lnsr;->c:Lnsw;

    if-eqz v3, :cond_12

    iget-object v4, v3, Lnsw;->a:[Lnsj;

    invoke-static {v4}, Llrv;->a([Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_13

    :cond_12
    invoke-static/range {v23 .. v23}, Llrv;->a([Ljava/lang/Object;)Z

    move-result v3

    :goto_c
    if-nez v3, :cond_19

    .line 1211
    const/4 v2, 0x1

    goto/16 :goto_a

    .line 1210
    :cond_13
    if-nez v23, :cond_14

    const/4 v3, 0x0

    goto :goto_c

    :cond_14
    iget-object v4, v3, Lnsw;->a:[Lnsj;

    array-length v3, v4

    move-object/from16 v0, v23

    array-length v5, v0

    if-eq v3, v5, :cond_15

    const/4 v3, 0x0

    goto :goto_c

    :cond_15
    const/4 v3, 0x0

    move-object/from16 v0, v23

    array-length v5, v0

    :goto_d
    if-ge v3, v5, :cond_18

    aget-object v6, v23, v3

    aget-object v7, v4, v3

    invoke-virtual {v6}, Lktg;->a()Ljava/lang/String;

    move-result-object v8

    iget-object v9, v7, Lnsj;->b:Ljava/lang/String;

    invoke-static {v8, v9}, Llsu;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_16

    invoke-virtual {v6}, Lktg;->b()Ljava/lang/String;

    move-result-object v8

    iget-object v9, v7, Lnsj;->c:Ljava/lang/String;

    invoke-static {v8, v9}, Llsu;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_16

    invoke-virtual {v6}, Lktg;->c()Ljava/lang/String;

    move-result-object v6

    iget-object v7, v7, Lnsj;->d:Ljava/lang/String;

    invoke-static {v6, v7}, Llsu;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_17

    :cond_16
    const/4 v3, 0x0

    goto :goto_c

    :cond_17
    add-int/lit8 v3, v3, 0x1

    goto :goto_d

    :cond_18
    const/4 v3, 0x1

    goto :goto_c

    .line 1214
    :cond_19
    move-object/from16 v0, p1

    iget-object v3, v0, Lnsr;->i:Lnsi;

    .line 1215
    if-eqz v3, :cond_1b

    iget-object v4, v3, Lnsi;->b:Ljava/lang/Boolean;

    .line 1216
    invoke-static {v4}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v4

    if-ne v11, v4, :cond_1a

    iget-object v3, v3, Lnsi;->a:Ljava/lang/Boolean;

    .line 1217
    invoke-static {v3}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v3

    if-eq v12, v3, :cond_1b

    .line 1218
    :cond_1a
    const/4 v2, 0x1

    goto/16 :goto_a

    .line 1221
    :cond_1b
    move-object/from16 v0, p1

    iget-object v3, v0, Lnsr;->e:Lnsu;

    .line 1222
    if-eqz v3, :cond_1c

    iget-object v3, v3, Lnsu;->a:Ljava/lang/Integer;

    .line 1223
    invoke-static {v3}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v3

    move/from16 v0, v22

    if-eq v0, v3, :cond_1c

    .line 1224
    const/4 v2, 0x1

    goto/16 :goto_a

    .line 1227
    :cond_1c
    move-object/from16 v0, p1

    iget-object v3, v0, Lnsr;->b:Lnsb;

    iget-object v3, v3, Lnsb;->b:Lnsc;

    iget-object v4, v3, Lnsc;->f:Lnse;

    if-nez v24, :cond_1d

    if-eqz v4, :cond_23

    :cond_1d
    if-eqz v24, :cond_1e

    if-nez v4, :cond_1f

    :cond_1e
    const/4 v3, 0x0

    :goto_e
    if-nez v3, :cond_24

    .line 1228
    const/4 v2, 0x1

    goto/16 :goto_a

    .line 1227
    :cond_1f
    move-object/from16 v0, v24

    iget-object v3, v0, Lnse;->a:[Lnsd;

    array-length v3, v3

    iget-object v5, v4, Lnse;->a:[Lnsd;

    array-length v5, v5

    if-eq v3, v5, :cond_20

    const/4 v3, 0x0

    goto :goto_e

    :cond_20
    const/4 v3, 0x0

    :goto_f
    move-object/from16 v0, v24

    iget-object v5, v0, Lnse;->a:[Lnsd;

    array-length v5, v5

    if-ge v3, v5, :cond_23

    move-object/from16 v0, v24

    iget-object v5, v0, Lnse;->a:[Lnsd;

    aget-object v5, v5, v3

    iget-object v6, v4, Lnse;->a:[Lnsd;

    aget-object v6, v6, v3

    iget-object v7, v5, Lnsd;->b:Ljava/lang/String;

    iget-object v8, v6, Lnsd;->b:Ljava/lang/String;

    invoke-static {v7, v8}, Llsu;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_21

    iget-object v7, v5, Lnsd;->c:Ljava/lang/String;

    iget-object v8, v6, Lnsd;->c:Ljava/lang/String;

    invoke-static {v7, v8}, Llsu;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_21

    iget-object v5, v5, Lnsd;->d:Ljava/lang/String;

    iget-object v6, v6, Lnsd;->d:Ljava/lang/String;

    invoke-static {v5, v6}, Llsu;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_22

    :cond_21
    const/4 v3, 0x0

    goto :goto_e

    :cond_22
    add-int/lit8 v3, v3, 0x1

    goto :goto_f

    :cond_23
    const/4 v3, 0x1

    goto :goto_e

    .line 1231
    :cond_24
    move-object/from16 v0, p1

    iget-object v3, v0, Lnsr;->b:Lnsb;

    iget-object v3, v3, Lnsb;->b:Lnsc;

    iget-object v3, v3, Lnsc;->d:Lpdt;

    if-nez v2, :cond_25

    if-eqz v3, :cond_28

    :cond_25
    if-eqz v2, :cond_26

    if-nez v3, :cond_27

    :cond_26
    const/4 v2, 0x0

    :goto_10
    if-nez v2, :cond_29

    .line 1232
    const/4 v2, 0x1

    goto/16 :goto_a

    .line 1231
    :cond_27
    iget-object v2, v2, Lpdt;->a:Ljava/lang/String;

    iget-object v3, v3, Lpdt;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_28

    const/4 v2, 0x0

    goto :goto_10

    :cond_28
    const/4 v2, 0x1

    goto :goto_10

    .line 1235
    :cond_29
    const/4 v2, 0x0

    goto/16 :goto_a
.end method

.method private static b(Lnsr;)Z
    .locals 2

    .prologue
    .line 1028
    if-eqz p0, :cond_0

    iget-object v0, p0, Lnsr;->b:Lnsb;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lnsr;->b:Lnsb;

    iget-object v0, v0, Lnsb;->b:Lnsc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lnsr;->b:Lnsb;

    iget-object v0, v0, Lnsb;->a:Ljava/lang/String;

    .line 1031
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1032
    const/4 v0, 0x1

    .line 1037
    :goto_0
    return v0

    .line 1034
    :cond_0
    const-string v0, "SquaresDataServiceImpl"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1035
    const-string v0, "SquaresDataServiceImpl"

    const-string v1, "Invalid ViewerSquare"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1037
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(ILjava/lang/String;[Lnry;)I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 543
    if-nez p3, :cond_0

    .line 570
    :goto_0
    return v0

    .line 548
    :cond_0
    iget-object v1, p0, Lktr;->e:Landroid/content/Context;

    invoke-static {v1, p1}, Lhzt;->a(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 549
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 552
    :try_start_0
    invoke-direct {p0, v2, p2, p3}, Lktr;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Lnry;)V

    .line 555
    array-length v3, p3

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, p3, v1

    .line 556
    iget-object v5, v4, Lnry;->e:[Lnsh;

    if-eqz v5, :cond_1

    .line 557
    iget-object v4, v4, Lnry;->e:[Lnsh;

    .line 558
    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    .line 557
    invoke-direct {p0, v2, p2, v4}, Lktr;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/util/Collection;)I

    move-result v4

    add-int/2addr v0, v4

    .line 555
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 562
    :cond_2
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 564
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 568
    invoke-static {p2}, Lktp;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {p0, v1}, Lktr;->a(Landroid/net/Uri;)V

    goto :goto_0

    .line 564
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method public a(I[Lnru;[Lnrx;[Lnsq;)I
    .locals 19

    .prologue
    .line 198
    new-instance v12, Ljava/util/HashMap;

    invoke-direct {v12}, Ljava/util/HashMap;-><init>()V

    .line 200
    if-eqz p4, :cond_1

    move-object/from16 v0, p4

    array-length v2, v0

    if-lez v2, :cond_1

    const/4 v2, 0x1

    move v11, v2

    .line 201
    :goto_0
    if-eqz v11, :cond_2

    .line 202
    const/4 v6, 0x0

    move-object/from16 v0, p4

    array-length v8, v0

    :goto_1
    if-ge v6, v8, :cond_2

    .line 203
    aget-object v7, p4, v6

    .line 204
    iget-object v2, v7, Lnsq;->b:Lnsr;

    invoke-static {v2}, Lktr;->b(Lnsr;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 205
    iget-object v2, v7, Lnsq;->b:Lnsr;

    iget-object v2, v2, Lnsr;->b:Lnsb;

    iget-object v9, v2, Lnsb;->a:Ljava/lang/String;

    new-instance v2, Lkts;

    iget-object v3, v7, Lnsq;->b:Lnsr;

    const/4 v4, 0x3

    const/4 v5, 0x0

    iget-object v7, v7, Lnsq;->c:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Lkts;-><init>(Lnsr;ILnsh;ILjava/lang/String;)V

    invoke-virtual {v12, v9, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 202
    :cond_0
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 200
    :cond_1
    const/4 v2, 0x0

    move v11, v2

    goto :goto_0

    .line 212
    :cond_2
    if-eqz p2, :cond_5

    .line 215
    const/4 v6, 0x0

    move-object/from16 v0, p2

    array-length v8, v0

    :goto_2
    if-ge v6, v8, :cond_5

    .line 216
    aget-object v3, p2, v6

    .line 217
    iget-object v2, v3, Lnru;->c:[Lnsh;

    invoke-static {v2}, Llrv;->a([Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v5, 0x0

    .line 218
    :goto_3
    iget-object v2, v3, Lnru;->b:Lnsr;

    invoke-static {v2}, Lktr;->b(Lnsr;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 219
    iget-object v2, v3, Lnru;->b:Lnsr;

    iget-object v2, v2, Lnsr;->b:Lnsb;

    iget-object v9, v2, Lnsb;->a:Ljava/lang/String;

    new-instance v2, Lkts;

    iget-object v3, v3, Lnru;->b:Lnsr;

    const/4 v4, 0x1

    const/4 v7, 0x0

    invoke-direct/range {v2 .. v7}, Lkts;-><init>(Lnsr;ILnsh;ILjava/lang/String;)V

    invoke-virtual {v12, v9, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215
    :cond_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 217
    :cond_4
    iget-object v2, v3, Lnru;->c:[Lnsh;

    const/4 v4, 0x0

    aget-object v5, v2, v4

    goto :goto_3

    .line 226
    :cond_5
    if-eqz p3, :cond_7

    .line 227
    const/4 v6, 0x0

    move-object/from16 v0, p3

    array-length v8, v0

    :goto_4
    if-ge v6, v8, :cond_7

    .line 228
    aget-object v3, p3, v6

    .line 229
    iget-object v2, v3, Lnrx;->b:Lnsr;

    invoke-static {v2}, Lktr;->b(Lnsr;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 230
    iget-object v2, v3, Lnrx;->b:Lnsr;

    iget-object v2, v2, Lnsr;->b:Lnsb;

    iget-object v9, v2, Lnsb;->a:Ljava/lang/String;

    new-instance v2, Lkts;

    iget-object v3, v3, Lnrx;->b:Lnsr;

    const/4 v4, 0x2

    const/4 v5, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v2 .. v7}, Lkts;-><init>(Lnsr;ILnsh;ILjava/lang/String;)V

    invoke-virtual {v12, v9, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 227
    :cond_6
    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    .line 237
    :cond_7
    const/4 v10, 0x0

    .line 238
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    .line 239
    move-object/from16 v0, p0

    iget-object v2, v0, Lktr;->e:Landroid/content/Context;

    move/from16 v0, p1

    invoke-static {v2, v0}, Lhzt;->a(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 240
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 242
    :try_start_0
    new-instance v13, Ljava/util/HashMap;

    invoke-direct {v13}, Ljava/util/HashMap;-><init>()V

    .line 243
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 247
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 248
    if-eqz p3, :cond_8

    .line 249
    const-string v3, "is_member!=0 OR "

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 251
    :cond_8
    if-eqz p2, :cond_9

    .line 252
    const-string v3, "membership_status = "

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 253
    const-string v3, "5 OR "

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 255
    :cond_9
    if-eqz v11, :cond_a

    .line 256
    const-string v3, "list_category = "

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 257
    const-string v3, "3 OR "

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 261
    :cond_a
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x4

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 263
    const-string v3, "squares"

    sget-object v4, Lktv;->c:[Ljava/lang/String;

    .line 264
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 263
    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v5

    .line 267
    :goto_5
    :try_start_1
    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_11

    .line 268
    const/4 v3, 0x0

    invoke-interface {v5, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 269
    invoke-virtual {v12, v6}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lkts;

    .line 270
    if-nez v3, :cond_b

    .line 271
    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_5

    .line 288
    :catchall_0
    move-exception v3

    :try_start_2
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    throw v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 335
    :catchall_1
    move-exception v3

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v3

    .line 274
    :cond_b
    :try_start_3
    iget-object v4, v3, Lkts;->a:Lnsr;

    invoke-static {v5, v4}, Lktr;->a(Landroid/database/Cursor;Lnsr;)Z

    move-result v4

    if-eqz v4, :cond_d

    const/4 v4, 0x1

    :goto_6
    if-eqz v4, :cond_10

    .line 275
    invoke-static {v3}, Lktr;->a(Lkts;)Landroid/content/ContentValues;

    move-result-object v3

    .line 276
    const-string v4, "SquaresDataServiceImpl"

    const/4 v7, 0x3

    invoke-static {v4, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 277
    const-string v4, "square_name"

    .line 278
    invoke-virtual {v3, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0x18

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v8, "Update square: id="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " name="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 284
    :cond_c
    :goto_7
    invoke-virtual {v13, v6, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5

    .line 274
    :cond_d
    const/16 v4, 0x9

    invoke-interface {v5, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    const/16 v7, 0x14

    invoke-interface {v5, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/16 v8, 0x15

    invoke-interface {v5, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/16 v9, 0x16

    invoke-interface {v5, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/16 v17, 0x1d

    move/from16 v0, v17

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    iget v0, v3, Lkts;->b:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-ne v4, v0, :cond_e

    invoke-virtual {v3}, Lkts;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v7, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_e

    invoke-virtual {v3}, Lkts;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v8, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_e

    invoke-virtual {v3}, Lkts;->c()Ljava/lang/String;

    move-result-object v4

    invoke-static {v9, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_e

    iget-object v4, v3, Lkts;->d:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-static {v0, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_f

    :cond_e
    const/4 v4, 0x1

    goto/16 :goto_6

    :cond_f
    const/4 v4, 0x0

    goto/16 :goto_6

    .line 281
    :cond_10
    new-instance v4, Landroid/content/ContentValues;

    const/4 v7, 0x2

    invoke-direct {v4, v7}, Landroid/content/ContentValues;-><init>(I)V

    .line 282
    const-string v7, "sort_index"

    iget v3, v3, Lkts;->c:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v4, v7, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-object v3, v4

    goto :goto_7

    .line 288
    :cond_11
    :try_start_4
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    .line 292
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1b

    .line 293
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 294
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 295
    new-array v7, v5, [Ljava/lang/String;

    .line 296
    const-string v3, "square_id IN ("

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 297
    const/4 v3, 0x0

    move v4, v3

    :goto_8
    if-ge v4, v5, :cond_12

    .line 298
    const-string v3, "?,"

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 299
    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    aput-object v3, v7, v4

    .line 297
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_8

    .line 301
    :cond_12
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 302
    const-string v3, ")"

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 303
    const-string v3, "squares"

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4, v7}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 306
    const-string v3, "SquaresDataServiceImpl"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_13

    .line 307
    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x1a

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Delete "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " squares"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 312
    :cond_13
    :goto_9
    invoke-virtual {v13}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_a
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_14

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 313
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 314
    const-string v7, "squares"

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/ContentValues;

    const-string v8, "square_id=?"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    aput-object v4, v9, v10

    invoke-virtual {v2, v7, v3, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 317
    add-int/lit8 v5, v5, 0x1

    .line 318
    goto :goto_a

    .line 321
    :cond_14
    invoke-virtual {v12}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v4, v5

    :cond_15
    :goto_b
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_16

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lkts;

    .line 322
    const-string v5, "squares"

    const/4 v7, 0x0

    .line 323
    invoke-static {v3}, Lktr;->a(Lkts;)Landroid/content/ContentValues;

    move-result-object v8

    const/4 v9, 0x5

    .line 322
    invoke-virtual {v2, v5, v7, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    .line 324
    add-int/lit8 v4, v4, 0x1

    .line 326
    const-string v5, "SquaresDataServiceImpl"

    const/4 v7, 0x3

    invoke-static {v5, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_15

    .line 327
    iget-object v5, v3, Lkts;->a:Lnsr;

    iget-object v5, v5, Lnsr;->b:Lnsb;

    iget-object v5, v5, Lnsb;->a:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    iget-object v3, v3, Lkts;->a:Lnsr;

    iget-object v3, v3, Lnsr;->b:Lnsb;

    iget-object v3, v3, Lnsb;->b:Lnsc;

    iget-object v3, v3, Lnsc;->a:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0x18

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v8, "Insert square: id="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " name="

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_b

    .line 333
    :cond_16
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 335
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 338
    if-nez p2, :cond_17

    if-eqz p3, :cond_18

    .line 339
    :cond_17
    move-object/from16 v0, p0

    iget-object v2, v0, Lktr;->e:Landroid/content/Context;

    const-class v3, Lktw;

    invoke-static {v2, v3}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lktw;

    move/from16 v0, p1

    invoke-virtual {v2, v0, v14, v15}, Lktw;->a(IJ)V

    .line 342
    :cond_18
    if-eqz v11, :cond_19

    .line 343
    move-object/from16 v0, p0

    iget-object v2, v0, Lktr;->e:Landroid/content/Context;

    const-class v3, Lktw;

    invoke-static {v2, v3}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lktw;

    move/from16 v0, p1

    invoke-virtual {v2, v0, v14, v15}, Lktw;->b(IJ)V

    .line 346
    :cond_19
    if-eqz v4, :cond_1a

    .line 347
    sget-object v2, Lktp;->a:Landroid/net/Uri;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lktr;->a(Landroid/net/Uri;)V

    .line 350
    move-object/from16 v0, p0

    iget-object v2, v0, Lktr;->e:Landroid/content/Context;

    const-class v3, Lkxp;

    invoke-static {v2, v3}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lkxp;

    const/4 v3, 0x0

    move/from16 v0, p1

    invoke-virtual {v2, v0, v3}, Lkxp;->a(ILnto;)V

    .line 353
    :cond_1a
    return v4

    :cond_1b
    move v5, v10

    goto/16 :goto_9
.end method

.method public a(ILjava/lang/String;I[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 13

    .prologue
    .line 682
    packed-switch p3, :pswitch_data_0

    .line 707
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 708
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const/4 v2, 0x2

    .line 709
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const/4 v2, 0x3

    .line 710
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    move-object v1, v0

    .line 715
    :goto_0
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 716
    const-string v0, "membership_status IN ("

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 718
    const/4 v0, 0x0

    :goto_1
    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 719
    const-string v2, "?,"

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 718
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 684
    :pswitch_0
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    move-object v1, v0

    .line 685
    goto :goto_0

    .line 689
    :pswitch_1
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    move-object v1, v0

    .line 690
    goto :goto_0

    .line 694
    :pswitch_2
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    move-object v1, v0

    .line 695
    goto :goto_0

    .line 699
    :pswitch_3
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 700
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const/4 v2, 0x2

    .line 701
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    move-object v1, v0

    .line 702
    goto :goto_0

    .line 721
    :cond_0
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 722
    const-string v0, ")"

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 724
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 727
    const-string v2, "square_contact"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 728
    const-string v2, "link_square_id"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 729
    const-string v2, "=?"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 730
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-static {v1, v2}, Llrv;->a([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 732
    sget-object v1, Lktv;->b:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 733
    const-string v11, "square_contact.membership_status, name"

    .line 737
    const-string v1, "SquaresDataServiceImpl"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 738
    const-string v9, "QUERY: "

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object/from16 v1, p4

    move-object/from16 v6, p5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildQuery([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v9, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 742
    :cond_1
    :goto_2
    iget-object v1, p0, Lktr;->e:Landroid/content/Context;

    invoke-static {v1, p1}, Lhzt;->b(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    .line 743
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v12, 0x0

    move-object v4, v0

    move-object/from16 v6, p4

    move-object v8, v3

    .line 742
    invoke-virtual/range {v4 .. v12}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 745
    const-string v1, "SquaresDataServiceImpl"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 746
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x1a

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "QUERY results: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 749
    :cond_2
    iget-object v1, p0, Lktr;->e:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {p2}, Lktp;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 751
    return-object v0

    .line 738
    :cond_3
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v9}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 682
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(ILjava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 862
    iget-object v0, p0, Lktr;->e:Landroid/content/Context;

    invoke-static {v0, p1}, Lhzt;->b(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 863
    const-string v1, "squares"

    const-string v3, "square_id=?"

    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p2, v4, v2

    move-object v2, p3

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 866
    iget-object v1, p0, Lktr;->e:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {p2}, Lktp;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 867
    return-object v0
.end method

.method public a(I[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 112
    const-string v5, "sort_index"

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v4, v3

    invoke-direct/range {v0 .. v5}, Lktr;->a(I[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public a(I[Ljava/lang/String;Ljava/lang/String;Z)Landroid/database/Cursor;
    .locals 6
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 120
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 121
    const-string v1, "is_member!=0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 122
    if-eqz p4, :cond_0

    .line 123
    const-string v1, " AND restricted_domain!=\'\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 125
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lktr;->a(I[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public a(ILjava/lang/String;)V
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 756
    iget-object v0, p0, Lktr;->e:Landroid/content/Context;

    invoke-static {v0, p1}, Lhzt;->a(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 758
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 759
    const-string v2, "invitation_dismissed"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 760
    const-string v2, "squares"

    const-string v3, "square_id=?"

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p2, v4, v5

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 763
    if-lez v0, :cond_0

    .line 764
    sget-object v0, Lktp;->a:Landroid/net/Uri;

    invoke-direct {p0, v0}, Lktr;->a(Landroid/net/Uri;)V

    .line 766
    :cond_0
    return-void
.end method

.method public a(ILjava/lang/String;I)V
    .locals 12

    .prologue
    .line 358
    const/4 v1, 0x0

    .line 359
    iget-object v0, p0, Lktr;->e:Landroid/content/Context;

    invoke-static {v0, p1}, Lhzt;->a(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 360
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 362
    :try_start_0
    new-instance v11, Landroid/content/ContentValues;

    invoke-direct {v11}, Landroid/content/ContentValues;-><init>()V

    .line 363
    const/4 v2, 0x3

    if-eq p3, v2, :cond_0

    const/4 v2, 0x2

    if-ne p3, v2, :cond_5

    .line 364
    :cond_0
    const-string v1, "membership_status"

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v11, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 365
    const-string v1, "is_member"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v11, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 366
    const-string v1, "list_category"

    const/4 v2, 0x2

    .line 367
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 366
    invoke-virtual {v11, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 368
    const-string v1, "can_see_members"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v11, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 369
    const-string v1, "can_see_posts"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v11, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 370
    const-string v1, "can_join"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v11, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 371
    const-string v1, "can_request_to_join"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v11, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 375
    const/4 v8, 0x0

    .line 376
    const-string v1, "squares"

    sget-object v2, Lktr;->b:[Ljava/lang/String;

    const-string v3, "square_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p2, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 380
    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_12

    .line 381
    const/4 v1, 0x0

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    .line 384
    :goto_0
    :try_start_2
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 386
    const-string v2, "notifications_enabled"

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v11, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 387
    const/4 v1, 0x1

    .line 452
    :cond_1
    :goto_1
    if-eqz v1, :cond_2

    .line 453
    const-string v2, "squares"

    const-string v3, "square_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p2, v4, v5

    invoke-virtual {v0, v2, v11, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 456
    :cond_2
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 458
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 461
    if-eqz v1, :cond_3

    .line 462
    sget-object v0, Lktp;->a:Landroid/net/Uri;

    invoke-direct {p0, v0}, Lktr;->a(Landroid/net/Uri;)V

    .line 465
    iget-object v0, p0, Lktr;->e:Landroid/content/Context;

    const-class v1, Lkxp;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkxp;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lkxp;->a(ILnto;)V

    .line 467
    :cond_3
    return-void

    .line 381
    :cond_4
    const/4 v1, 0x0

    goto :goto_0

    .line 384
    :catchall_0
    move-exception v1

    :try_start_3
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 458
    :catchall_1
    move-exception v1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v1

    .line 389
    :cond_5
    if-nez p3, :cond_6

    .line 390
    :try_start_4
    const-string v1, "membership_status"

    const/4 v2, 0x4

    .line 391
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 390
    invoke-virtual {v11, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 392
    const-string v1, "can_request_to_join"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v11, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 393
    const/4 v1, 0x1

    goto :goto_1

    .line 395
    :cond_6
    const/16 v2, 0x14

    if-ne p3, v2, :cond_7

    .line 396
    const-string v1, "membership_status"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v11, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 397
    const-string v1, "can_request_to_join"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v11, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 398
    const/4 v1, 0x1

    goto :goto_1

    .line 400
    :cond_7
    const/16 v2, 0xf

    if-ne p3, v2, :cond_8

    .line 401
    const-string v1, "notifications_enabled"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v11, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 402
    const/4 v1, 0x1

    goto :goto_1

    .line 404
    :cond_8
    const/16 v2, 0x10

    if-ne p3, v2, :cond_9

    .line 405
    const-string v1, "notifications_enabled"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v11, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 406
    const/4 v1, 0x1

    goto/16 :goto_1

    .line 408
    :cond_9
    const/4 v2, 0x5

    if-eq p3, v2, :cond_a

    const/16 v2, 0x15

    if-ne p3, v2, :cond_1

    .line 410
    :cond_a
    const/4 v10, -0x1

    .line 411
    const/4 v9, 0x0

    .line 412
    const/4 v8, 0x0

    .line 416
    const-string v1, "squares"

    sget-object v2, Lktr;->a:[Ljava/lang/String;

    const-string v3, "square_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p2, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result-object v5

    .line 420
    :try_start_5
    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_11

    .line 421
    const/4 v1, 0x0

    invoke-interface {v5, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-nez v1, :cond_c

    const/4 v2, 0x1

    .line 423
    :goto_2
    const/4 v1, 0x1

    invoke-interface {v5, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 424
    const/4 v1, 0x2

    invoke-interface {v5, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    .line 425
    invoke-static {v1}, Lktg;->a([B)[Lktg;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    move-result-object v1

    move v4, v3

    move v3, v2

    move-object v2, v1

    .line 428
    :goto_3
    :try_start_6
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    .line 431
    const-string v1, "membership_status"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v11, v1, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 432
    const-string v1, "is_member"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v11, v1, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 433
    const-string v5, "can_see_members"

    if-eqz v3, :cond_d

    const/4 v1, 0x1

    :goto_4
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v11, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 434
    const-string v5, "can_see_posts"

    if-eqz v3, :cond_e

    const/4 v1, 0x1

    :goto_5
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v11, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 435
    const-string v5, "can_join"

    if-nez v4, :cond_f

    const/4 v1, 0x1

    .line 436
    :goto_6
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 435
    invoke-virtual {v11, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 437
    const-string v5, "can_request_to_join"

    const/4 v1, 0x1

    if-ne v4, v1, :cond_10

    const/4 v1, 0x1

    .line 438
    :goto_7
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 437
    invoke-virtual {v11, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 439
    const-string v1, "can_share"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v11, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 440
    const-string v1, "can_invite"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v11, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 441
    if-nez v3, :cond_b

    const/4 v1, 0x5

    if-ne p3, v1, :cond_b

    .line 442
    invoke-direct {p0, p1, p2, v2}, Lktr;->a(ILjava/lang/String;[Lktg;)V

    .line 444
    const-string v1, "square_streams"

    invoke-virtual {v11, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 445
    const-string v1, "related_links"

    invoke-virtual {v11, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 447
    :cond_b
    const-string v1, "list_category"

    const/4 v2, 0x0

    .line 448
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 447
    invoke-virtual {v11, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 449
    const/4 v1, 0x1

    goto/16 :goto_1

    .line 421
    :cond_c
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 428
    :catchall_2
    move-exception v1

    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    throw v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 433
    :cond_d
    const/4 v1, 0x0

    goto :goto_4

    .line 434
    :cond_e
    const/4 v1, 0x0

    goto :goto_5

    .line 435
    :cond_f
    const/4 v1, 0x0

    goto :goto_6

    .line 437
    :cond_10
    const/4 v1, 0x0

    goto :goto_7

    :cond_11
    move-object v2, v8

    move v3, v9

    move v4, v10

    goto/16 :goto_3

    :cond_12
    move v1, v8

    goto/16 :goto_0
.end method

.method public a(ILjava/lang/String;Ljava/lang/String;I)V
    .locals 11

    .prologue
    .line 475
    sget-object v0, Lktr;->f:Landroid/util/SparseIntArray;

    if-nez v0, :cond_0

    .line 476
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    .line 477
    const/4 v1, 0x1

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 478
    const/4 v1, 0x4

    const/4 v2, 0x7

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 479
    const/4 v1, 0x6

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 480
    const/4 v1, 0x7

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 481
    const/16 v1, 0x8

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 482
    const/16 v1, 0x9

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 483
    const/16 v1, 0xa

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 484
    const/16 v1, 0xc

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 485
    const/16 v1, 0xd

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 486
    const/16 v1, 0xe

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 487
    const/16 v1, 0x16

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 488
    sput-object v0, Lktr;->f:Landroid/util/SparseIntArray;

    .line 491
    :cond_0
    sget-object v0, Lktr;->f:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p4}, Landroid/util/SparseIntArray;->get(I)I

    move-result v10

    .line 492
    const/4 v8, 0x0

    .line 493
    const/4 v9, -0x1

    .line 495
    iget-object v0, p0, Lktr;->e:Landroid/content/Context;

    invoke-static {v0, p1}, Lhzt;->a(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 496
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 498
    :try_start_0
    const-string v3, "link_square_id=? AND qualified_id=?"

    .line 500
    const/4 v1, 0x2

    new-array v4, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p2, v4, v1

    const/4 v1, 0x1

    aput-object p3, v4, v1

    .line 502
    const-string v1, "square_contact"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "membership_status"

    aput-object v6, v2, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 505
    if-eqz v1, :cond_3

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 506
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 509
    :goto_0
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 510
    const-string v1, "membership_status"

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v2, v1, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 512
    const-string v1, "square_contact"

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 514
    iget-object v1, p0, Lktr;->e:Landroid/content/Context;

    const-class v3, Lhei;

    invoke-static {v1, v3}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhei;

    .line 515
    invoke-interface {v1, p1}, Lhei;->a(I)Lhej;

    move-result-object v1

    const-string v3, "gaia_id"

    invoke-interface {v1, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 516
    invoke-static {p3}, Lkto;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 518
    invoke-virtual {v2}, Landroid/content/ContentValues;->clear()V

    .line 519
    const-string v1, "membership_status"

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 521
    const-string v1, "squares"

    const-string v3, "square_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p2, v4, v6

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 524
    const/4 v1, 0x1

    move v7, v1

    .line 527
    :goto_1
    const/4 v6, -0x1

    move-object v1, p0

    move v2, p1

    move-object v3, v0

    move-object v4, p2

    invoke-direct/range {v1 .. v6}, Lktr;->a(ILandroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;II)V

    .line 528
    const/4 v6, 0x1

    move-object v1, p0

    move v2, p1

    move-object v3, v0

    move-object v4, p2

    move v5, v10

    invoke-direct/range {v1 .. v6}, Lktr;->a(ILandroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;II)V

    .line 529
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 531
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 534
    invoke-static {p2}, Lktp;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0}, Lktr;->a(Landroid/net/Uri;)V

    .line 536
    if-eqz v7, :cond_1

    .line 537
    sget-object v0, Lktp;->a:Landroid/net/Uri;

    invoke-direct {p0, v0}, Lktr;->a(Landroid/net/Uri;)V

    .line 539
    :cond_1
    return-void

    .line 531
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v1

    :cond_2
    move v7, v8

    goto :goto_1

    :cond_3
    move v5, v9

    goto :goto_0
.end method

.method public a(ILjava/lang/String;Lodw;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/high16 v7, -0x80000000

    .line 770
    iget-object v0, p0, Lktr;->e:Landroid/content/Context;

    invoke-static {v0, p1}, Lhzt;->a(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 772
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 773
    iget v0, p3, Lodw;->b:I

    if-eq v0, v7, :cond_0

    .line 774
    const-string v5, "notifications_enabled"

    iget v0, p3, Lodw;->b:I

    const/4 v6, 0x2

    if-ne v0, v6, :cond_3

    move v0, v1

    .line 775
    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 774
    invoke-virtual {v4, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 777
    :cond_0
    iget v0, p3, Lodw;->a:I

    if-eq v0, v7, :cond_1

    .line 778
    const-string v0, "volume"

    iget v5, p3, Lodw;->a:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 779
    const-string v0, "last_volume_sync"

    .line 780
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    .line 779
    invoke-virtual {v4, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 783
    :cond_1
    invoke-virtual {v4}, Landroid/content/ContentValues;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 784
    const-string v0, "squares"

    const-string v5, "square_id=?"

    new-array v1, v1, [Ljava/lang/String;

    aput-object p2, v1, v2

    invoke-virtual {v3, v0, v4, v5, v1}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 787
    if-lez v0, :cond_2

    .line 788
    sget-object v0, Lktp;->a:Landroid/net/Uri;

    invoke-direct {p0, v0}, Lktr;->a(Landroid/net/Uri;)V

    .line 791
    :cond_2
    return-void

    :cond_3
    move v0, v2

    .line 774
    goto :goto_0
.end method

.method public a(ILnsr;)Z
    .locals 16

    .prologue
    .line 130
    invoke-static/range {p2 .. p2}, Lktr;->b(Lnsr;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 131
    const/4 v2, 0x0

    .line 192
    :goto_0
    return v2

    .line 134
    :cond_0
    const/4 v11, 0x0

    .line 135
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    .line 136
    move-object/from16 v0, p2

    iget-object v2, v0, Lnsr;->b:Lnsb;

    iget-object v14, v2, Lnsb;->a:Ljava/lang/String;

    .line 138
    move-object/from16 v0, p0

    iget-object v2, v0, Lktr;->e:Landroid/content/Context;

    move/from16 v0, p1

    invoke-static {v2, v0}, Lhzt;->a(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 139
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 141
    const/4 v10, 0x0

    .line 142
    :try_start_0
    const-string v3, "squares"

    sget-object v4, Lktv;->c:[Ljava/lang/String;

    const-string v5, "square_id=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v14, v6, v7

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v5

    .line 146
    :try_start_1
    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 147
    move-object/from16 v0, p2

    invoke-static {v5, v0}, Lktr;->a(Landroid/database/Cursor;Lnsr;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 148
    invoke-static/range {p2 .. p2}, Lktr;->a(Lnsr;)Landroid/content/ContentValues;

    move-result-object v3

    .line 149
    const/4 v4, 0x1

    .line 151
    const-string v6, "SquaresDataServiceImpl"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 152
    move-object/from16 v0, p2

    iget-object v6, v0, Lnsr;->b:Lnsb;

    iget-object v6, v6, Lnsb;->b:Lnsc;

    iget-object v6, v6, Lnsc;->a:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v14}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0x18

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v8, "Update square: id="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " name="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 158
    :cond_1
    :goto_1
    const-string v6, "last_sync"

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 160
    const-string v6, "unread_count"

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v15, v3

    move v3, v4

    move-object v4, v15

    .line 163
    :goto_2
    :try_start_2
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    .line 166
    if-eqz v4, :cond_4

    .line 167
    const-string v5, "squares"

    const-string v6, "square_id=?"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object v14, v7, v8

    invoke-virtual {v2, v5, v4, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 180
    :cond_2
    :goto_3
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 182
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 187
    sget-object v2, Lktp;->a:Landroid/net/Uri;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lktr;->a(Landroid/net/Uri;)V

    .line 190
    move-object/from16 v0, p0

    iget-object v2, v0, Lktr;->e:Landroid/content/Context;

    const-class v4, Lkxp;

    invoke-static {v2, v4}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lkxp;

    const/4 v4, 0x0

    move/from16 v0, p1

    invoke-virtual {v2, v0, v4}, Lkxp;->a(ILnto;)V

    move v2, v3

    .line 192
    goto/16 :goto_0

    .line 156
    :cond_3
    :try_start_3
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v4, v11

    goto :goto_1

    .line 163
    :catchall_0
    move-exception v3

    :try_start_4
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    throw v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 182
    :catchall_1
    move-exception v3

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v3

    .line 170
    :cond_4
    :try_start_5
    invoke-static/range {p2 .. p2}, Lktr;->a(Lnsr;)Landroid/content/ContentValues;

    move-result-object v3

    .line 171
    const-string v4, "last_sync"

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 173
    const-string v4, "squares"

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5, v3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 174
    const/4 v3, 0x1

    .line 176
    const-string v4, "SquaresDataServiceImpl"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 177
    move-object/from16 v0, p2

    iget-object v4, v0, Lnsr;->b:Lnsb;

    iget-object v4, v4, Lnsb;->b:Lnsc;

    iget-object v4, v4, Lnsc;->a:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v14}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x18

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Insert square: id="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " name="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto/16 :goto_3

    :cond_5
    move-object v4, v10

    move v3, v11

    goto/16 :goto_2
.end method

.method public b(ILjava/lang/String;[Lnry;)I
    .locals 18

    .prologue
    .line 575
    if-nez p3, :cond_0

    .line 576
    const/4 v4, 0x0

    .line 674
    :goto_0
    return v4

    .line 579
    :cond_0
    new-instance v14, Ljava/util/HashMap;

    invoke-direct {v14}, Ljava/util/HashMap;-><init>()V

    .line 580
    move-object/from16 v0, p3

    array-length v7, v0

    const/4 v4, 0x0

    move v6, v4

    :goto_1
    if-ge v6, v7, :cond_6

    aget-object v4, p3, v6

    .line 581
    iget-object v8, v4, Lnry;->e:[Lnsh;

    .line 582
    if-eqz v8, :cond_5

    .line 583
    array-length v9, v8

    const/4 v4, 0x0

    move v5, v4

    :goto_2
    if-ge v5, v9, :cond_5

    aget-object v10, v8, v5

    .line 586
    iget-object v4, v10, Lnsh;->b:Ljava/lang/String;

    if-eqz v4, :cond_3

    .line 587
    const-string v4, "g:"

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    iget-object v4, v10, Lnsh;->b:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v12

    if-eqz v12, :cond_2

    invoke-virtual {v11, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :goto_3
    invoke-virtual {v14, v4, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 585
    :cond_1
    :goto_4
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_2

    .line 587
    :cond_2
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v11}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3

    .line 588
    :cond_3
    iget-object v4, v10, Lnsh;->f:Lnsa;

    if-eqz v4, :cond_1

    .line 589
    const-string v4, "t:"

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    iget-object v4, v10, Lnsh;->f:Lnsa;

    iget-object v4, v4, Lnsa;->b:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v12

    if-eqz v12, :cond_4

    invoke-virtual {v11, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :goto_5
    invoke-virtual {v14, v4, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    :cond_4
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v11}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_5

    .line 580
    :cond_5
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto :goto_1

    .line 594
    :cond_6
    const/4 v13, 0x0

    .line 595
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    .line 596
    move-object/from16 v0, p0

    iget-object v4, v0, Lktr;->e:Landroid/content/Context;

    move/from16 v0, p1

    invoke-static {v4, v0}, Lhzt;->a(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 597
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 599
    :try_start_0
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 602
    const-string v5, "square_contact"

    sget-object v6, Lktr;->d:[Ljava/lang/String;

    const-string v7, "link_square_id=?"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    aput-object p2, v8, v9

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/16 v12, 0xc9

    .line 605
    invoke-static {v12}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v12

    .line 602
    invoke-virtual/range {v4 .. v12}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 608
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v5

    const/16 v7, 0xc8

    if-le v5, v7, :cond_8

    .line 610
    const-string v5, "square_contact"

    const-string v6, "link_square_id=?"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object p2, v7, v8

    invoke-virtual {v4, v5, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v5

    add-int/lit8 v5, v5, 0x0

    .line 655
    :cond_7
    :goto_6
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-direct {v0, v4, v1, v2}, Lktr;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Lnry;)V

    .line 658
    invoke-virtual {v14}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v6

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v4, v1, v6}, Lktr;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/util/Collection;)I

    move-result v6

    add-int/2addr v5, v6

    .line 661
    new-instance v6, Landroid/content/ContentValues;

    const/4 v7, 0x1

    invoke-direct {v6, v7}, Landroid/content/ContentValues;-><init>(I)V

    .line 662
    const-string v7, "last_members_sync"

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 663
    const-string v7, "squares"

    const-string v8, "square_id=?"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    aput-object p2, v9, v10

    invoke-virtual {v4, v7, v6, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 666
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 668
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 672
    invoke-static/range {p2 .. p2}, Lktp;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lktr;->a(Landroid/net/Uri;)V

    move v4, v5

    .line 674
    goto/16 :goto_0

    .line 614
    :cond_8
    :goto_7
    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_a

    .line 615
    const/4 v5, 0x0

    invoke-interface {v6, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 616
    invoke-virtual {v14, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lnsh;

    .line 617
    if-nez v5, :cond_9

    .line 618
    invoke-virtual {v15, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 619
    invoke-virtual {v14, v7}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_7

    .line 630
    :catchall_0
    move-exception v5

    :try_start_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 668
    :catchall_1
    move-exception v5

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v5

    .line 621
    :cond_9
    const/4 v8, 0x1

    .line 622
    :try_start_3
    invoke-interface {v6, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 623
    iget v5, v5, Lnsh;->e:I

    if-ne v8, v5, :cond_8

    .line 625
    invoke-virtual {v14, v7}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_7

    .line 630
    :cond_a
    :try_start_4
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 633
    invoke-virtual {v15}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_c

    .line 634
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 635
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v8

    .line 636
    add-int/lit8 v5, v8, 0x1

    new-array v9, v5, [Ljava/lang/String;

    .line 637
    const-string v5, "link_square_id=? AND "

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 638
    const-string v5, "qualified_id IN ("

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 639
    const/4 v5, 0x0

    aput-object p2, v9, v5

    .line 640
    const/4 v5, 0x0

    move v6, v5

    :goto_8
    if-ge v6, v8, :cond_b

    .line 641
    const-string v5, "?,"

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 642
    add-int/lit8 v10, v6, 0x1

    invoke-virtual {v15, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    aput-object v5, v9, v10

    .line 640
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_8

    .line 644
    :cond_b
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 645
    const-string v5, ")"

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 646
    const-string v5, "square_contact"

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6, v9}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v5

    add-int/lit8 v5, v5, 0x0

    .line 648
    const-string v6, "SquaresDataServiceImpl"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 649
    new-instance v6, Ljava/lang/StringBuilder;

    const/16 v7, 0x21

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "Delete "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " square members"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/16 :goto_6

    :cond_c
    move v5, v13

    goto/16 :goto_6
.end method

.method public b(ILjava/lang/String;)J
    .locals 4

    .prologue
    .line 795
    iget-object v0, p0, Lktr;->e:Landroid/content/Context;

    invoke-static {v0, p1}, Lhzt;->b(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 797
    :try_start_0
    const-string v1, "SELECT last_members_sync  FROM squares WHERE square_id=?"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-static {v0, v1, v2}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 804
    :goto_0
    return-wide v0

    :catch_0
    move-exception v0

    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public b(ILjava/lang/String;I)Landroid/database/Cursor;
    .locals 8

    .prologue
    const/4 v4, 0x2

    const/4 v5, 0x0

    .line 826
    packed-switch p3, :pswitch_data_0

    .line 849
    const/4 v0, 0x3

    move v6, v0

    .line 854
    :goto_0
    iget-object v0, p0, Lktr;->e:Landroid/content/Context;

    invoke-static {v0, p1}, Lhzt;->b(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 855
    const-string v1, "square_member_status"

    sget-object v2, Lktr;->c:[Ljava/lang/String;

    const-string v3, "square_id=? AND membership_status=?"

    new-array v4, v4, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object p2, v4, v7

    const/4 v7, 0x1

    .line 858
    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v7

    move-object v6, v5

    move-object v7, v5

    .line 855
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0

    .line 828
    :pswitch_0
    const/4 v0, 0x6

    move v6, v0

    .line 829
    goto :goto_0

    .line 833
    :pswitch_1
    const/4 v0, 0x5

    move v6, v0

    .line 834
    goto :goto_0

    .line 838
    :pswitch_2
    const/4 v0, 0x4

    move v6, v0

    .line 839
    goto :goto_0

    :pswitch_3
    move v6, v4

    .line 844
    goto :goto_0

    .line 826
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public c(ILjava/lang/String;)J
    .locals 4

    .prologue
    .line 810
    iget-object v0, p0, Lktr;->e:Landroid/content/Context;

    invoke-static {v0, p1}, Lhzt;->b(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 812
    :try_start_0
    const-string v1, "SELECT last_volume_sync  FROM squares WHERE square_id=?"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-static {v0, v1, v2}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 819
    :goto_0
    return-wide v0

    :catch_0
    move-exception v0

    const-wide/16 v0, -0x1

    goto :goto_0
.end method
