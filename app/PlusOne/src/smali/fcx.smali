.class final Lfcx;
.super Ljava/lang/Thread;
.source "PG"


# instance fields
.field final synthetic a:Landroid/location/Location;

.field final synthetic b:Lfcw;


# direct methods
.method constructor <init>(Lfcw;Landroid/location/Location;)V
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lfcx;->b:Lfcw;

    iput-object p2, p0, Lfcx;->a:Landroid/location/Location;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 51
    new-instance v0, Ldnp;

    iget-object v1, p0, Lfcx;->b:Lfcw;

    .line 52
    iget-object v1, v1, Lfcw;->a:Landroid/content/Context;

    iget-object v2, p0, Lfcx;->b:Lfcw;

    iget v2, v2, Lfcw;->b:I

    new-instance v3, Liuh;

    iget-object v5, p0, Lfcx;->a:Landroid/location/Location;

    invoke-direct {v3, v5, v4}, Liuh;-><init>(Landroid/location/Location;Ljava/lang/String;)V

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Ldnp;-><init>(Landroid/content/Context;ILiuh;Llae;Z)V

    .line 55
    invoke-virtual {v0}, Ldnp;->l()V

    .line 56
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 57
    iget-object v2, p0, Lfcx;->b:Lfcw;

    iget-boolean v2, v2, Lfcw;->e:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lfcx;->a:Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 58
    const-string v2, "location_source"

    iget-object v3, p0, Lfcx;->a:Landroid/location/Location;

    .line 59
    invoke-virtual {v3}, Landroid/location/Location;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v5, "location_source"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 58
    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    :cond_0
    invoke-virtual {v0}, Ldnp;->d()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 66
    invoke-virtual {v0}, Ldnp;->e()Llae;

    move-result-object v0

    .line 67
    const-string v4, "finest_location"

    .line 76
    :goto_0
    if-eqz v0, :cond_1

    .line 77
    invoke-virtual {v1, v4, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 78
    iget-object v2, p0, Lfcx;->b:Lfcw;

    iget-object v2, v2, Lfcw;->a:Landroid/content/Context;

    invoke-virtual {v0, v2}, Llae;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 79
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 80
    const-string v2, "location_description"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    :cond_1
    iget-object v0, p0, Lfcx;->a:Landroid/location/Location;

    invoke-virtual {v0, v1}, Landroid/location/Location;->setExtras(Landroid/os/Bundle;)V

    .line 86
    iget-object v0, p0, Lfcx;->b:Lfcw;

    iget-object v0, v0, Lfcw;->d:Landroid/os/Handler;

    new-instance v1, Lfcy;

    invoke-direct {v1, p0}, Lfcy;-><init>(Lfcx;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 100
    return-void

    .line 68
    :cond_2
    invoke-virtual {v0}, Ldnp;->S_()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 69
    invoke-virtual {v0}, Ldnp;->i()Llae;

    move-result-object v0

    .line 70
    const-string v4, "finest_location"

    goto :goto_0

    .line 71
    :cond_3
    invoke-virtual {v0}, Ldnp;->f()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 72
    invoke-virtual {v0}, Ldnp;->g()Llae;

    move-result-object v0

    .line 73
    const-string v4, "coarse_location"

    goto :goto_0

    :cond_4
    move-object v0, v4

    goto :goto_0
.end method
