.class public final Lpyk;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lpyk;


# instance fields
.field public b:I

.field public c:Lpym;

.field public d:Lpxv;

.field private e:Lpoy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Lpyk;

    sput-object v0, Lpyk;->a:[Lpyk;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 13
    const/high16 v0, -0x80000000

    iput v0, p0, Lpyk;->b:I

    .line 16
    iput-object v1, p0, Lpyk;->c:Lpym;

    .line 19
    iput-object v1, p0, Lpyk;->d:Lpxv;

    .line 22
    iput-object v1, p0, Lpyk;->e:Lpoy;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 45
    const/4 v0, 0x0

    .line 46
    iget v1, p0, Lpyk;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 47
    const/4 v0, 0x1

    iget v1, p0, Lpyk;->b:I

    .line 48
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 50
    :cond_0
    iget-object v1, p0, Lpyk;->d:Lpxv;

    if-eqz v1, :cond_1

    .line 51
    const/4 v1, 0x3

    iget-object v2, p0, Lpyk;->d:Lpxv;

    .line 52
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 54
    :cond_1
    iget-object v1, p0, Lpyk;->e:Lpoy;

    if-eqz v1, :cond_2

    .line 55
    const/4 v1, 0x4

    iget-object v2, p0, Lpyk;->e:Lpoy;

    .line 56
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 58
    :cond_2
    iget-object v1, p0, Lpyk;->c:Lpym;

    if-eqz v1, :cond_3

    .line 59
    const/4 v1, 0x6

    iget-object v2, p0, Lpyk;->c:Lpym;

    .line 60
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 62
    :cond_3
    iget-object v1, p0, Lpyk;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 63
    iput v0, p0, Lpyk;->ai:I

    .line 64
    return v0
.end method

.method public a(Loxn;)Lpyk;
    .locals 2

    .prologue
    .line 72
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 73
    sparse-switch v0, :sswitch_data_0

    .line 77
    iget-object v1, p0, Lpyk;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 78
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpyk;->ah:Ljava/util/List;

    .line 81
    :cond_1
    iget-object v1, p0, Lpyk;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 83
    :sswitch_0
    return-object p0

    .line 88
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 89
    if-eqz v0, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8

    if-eq v0, v1, :cond_2

    const/16 v1, 0x3e9

    if-eq v0, v1, :cond_2

    const/16 v1, 0x3ea

    if-eq v0, v1, :cond_2

    const/16 v1, 0x3eb

    if-eq v0, v1, :cond_2

    const/16 v1, 0x3ec

    if-ne v0, v1, :cond_3

    .line 100
    :cond_2
    iput v0, p0, Lpyk;->b:I

    goto :goto_0

    .line 102
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lpyk;->b:I

    goto :goto_0

    .line 107
    :sswitch_2
    iget-object v0, p0, Lpyk;->d:Lpxv;

    if-nez v0, :cond_4

    .line 108
    new-instance v0, Lpxv;

    invoke-direct {v0}, Lpxv;-><init>()V

    iput-object v0, p0, Lpyk;->d:Lpxv;

    .line 110
    :cond_4
    iget-object v0, p0, Lpyk;->d:Lpxv;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 114
    :sswitch_3
    iget-object v0, p0, Lpyk;->e:Lpoy;

    if-nez v0, :cond_5

    .line 115
    new-instance v0, Lpoy;

    invoke-direct {v0}, Lpoy;-><init>()V

    iput-object v0, p0, Lpyk;->e:Lpoy;

    .line 117
    :cond_5
    iget-object v0, p0, Lpyk;->e:Lpoy;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 121
    :sswitch_4
    iget-object v0, p0, Lpyk;->c:Lpym;

    if-nez v0, :cond_6

    .line 122
    new-instance v0, Lpym;

    invoke-direct {v0}, Lpym;-><init>()V

    iput-object v0, p0, Lpyk;->c:Lpym;

    .line 124
    :cond_6
    iget-object v0, p0, Lpyk;->c:Lpym;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 73
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x32 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 27
    iget v0, p0, Lpyk;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 28
    const/4 v0, 0x1

    iget v1, p0, Lpyk;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 30
    :cond_0
    iget-object v0, p0, Lpyk;->d:Lpxv;

    if-eqz v0, :cond_1

    .line 31
    const/4 v0, 0x3

    iget-object v1, p0, Lpyk;->d:Lpxv;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 33
    :cond_1
    iget-object v0, p0, Lpyk;->e:Lpoy;

    if-eqz v0, :cond_2

    .line 34
    const/4 v0, 0x4

    iget-object v1, p0, Lpyk;->e:Lpoy;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 36
    :cond_2
    iget-object v0, p0, Lpyk;->c:Lpym;

    if-eqz v0, :cond_3

    .line 37
    const/4 v0, 0x6

    iget-object v1, p0, Lpyk;->c:Lpym;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 39
    :cond_3
    iget-object v0, p0, Lpyk;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 41
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lpyk;->a(Loxn;)Lpyk;

    move-result-object v0

    return-object v0
.end method
