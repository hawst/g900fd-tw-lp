.class public final Lnft;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Boolean;

.field public b:Lnzi;

.field public c:Lnym;

.field private d:Lnzx;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2273
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2278
    iput-object v0, p0, Lnft;->b:Lnzi;

    .line 2281
    iput-object v0, p0, Lnft;->c:Lnym;

    .line 2284
    iput-object v0, p0, Lnft;->d:Lnzx;

    .line 2273
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 2307
    const/4 v0, 0x0

    .line 2308
    iget-object v1, p0, Lnft;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 2309
    const/4 v0, 0x1

    iget-object v1, p0, Lnft;->a:Ljava/lang/Boolean;

    .line 2310
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 2312
    :cond_0
    iget-object v1, p0, Lnft;->b:Lnzi;

    if-eqz v1, :cond_1

    .line 2313
    const/4 v1, 0x2

    iget-object v2, p0, Lnft;->b:Lnzi;

    .line 2314
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2316
    :cond_1
    iget-object v1, p0, Lnft;->c:Lnym;

    if-eqz v1, :cond_2

    .line 2317
    const/4 v1, 0x3

    iget-object v2, p0, Lnft;->c:Lnym;

    .line 2318
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2320
    :cond_2
    iget-object v1, p0, Lnft;->d:Lnzx;

    if-eqz v1, :cond_3

    .line 2321
    const/4 v1, 0x4

    iget-object v2, p0, Lnft;->d:Lnzx;

    .line 2322
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2324
    :cond_3
    iget-object v1, p0, Lnft;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2325
    iput v0, p0, Lnft;->ai:I

    .line 2326
    return v0
.end method

.method public a(Loxn;)Lnft;
    .locals 2

    .prologue
    .line 2334
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2335
    sparse-switch v0, :sswitch_data_0

    .line 2339
    iget-object v1, p0, Lnft;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 2340
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnft;->ah:Ljava/util/List;

    .line 2343
    :cond_1
    iget-object v1, p0, Lnft;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2345
    :sswitch_0
    return-object p0

    .line 2350
    :sswitch_1
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnft;->a:Ljava/lang/Boolean;

    goto :goto_0

    .line 2354
    :sswitch_2
    iget-object v0, p0, Lnft;->b:Lnzi;

    if-nez v0, :cond_2

    .line 2355
    new-instance v0, Lnzi;

    invoke-direct {v0}, Lnzi;-><init>()V

    iput-object v0, p0, Lnft;->b:Lnzi;

    .line 2357
    :cond_2
    iget-object v0, p0, Lnft;->b:Lnzi;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2361
    :sswitch_3
    iget-object v0, p0, Lnft;->c:Lnym;

    if-nez v0, :cond_3

    .line 2362
    new-instance v0, Lnym;

    invoke-direct {v0}, Lnym;-><init>()V

    iput-object v0, p0, Lnft;->c:Lnym;

    .line 2364
    :cond_3
    iget-object v0, p0, Lnft;->c:Lnym;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2368
    :sswitch_4
    iget-object v0, p0, Lnft;->d:Lnzx;

    if-nez v0, :cond_4

    .line 2369
    new-instance v0, Lnzx;

    invoke-direct {v0}, Lnzx;-><init>()V

    iput-object v0, p0, Lnft;->d:Lnzx;

    .line 2371
    :cond_4
    iget-object v0, p0, Lnft;->d:Lnzx;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2335
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 2289
    iget-object v0, p0, Lnft;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 2290
    const/4 v0, 0x1

    iget-object v1, p0, Lnft;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 2292
    :cond_0
    iget-object v0, p0, Lnft;->b:Lnzi;

    if-eqz v0, :cond_1

    .line 2293
    const/4 v0, 0x2

    iget-object v1, p0, Lnft;->b:Lnzi;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2295
    :cond_1
    iget-object v0, p0, Lnft;->c:Lnym;

    if-eqz v0, :cond_2

    .line 2296
    const/4 v0, 0x3

    iget-object v1, p0, Lnft;->c:Lnym;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2298
    :cond_2
    iget-object v0, p0, Lnft;->d:Lnzx;

    if-eqz v0, :cond_3

    .line 2299
    const/4 v0, 0x4

    iget-object v1, p0, Lnft;->d:Lnzx;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2301
    :cond_3
    iget-object v0, p0, Lnft;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2303
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2269
    invoke-virtual {p0, p1}, Lnft;->a(Loxn;)Lnft;

    move-result-object v0

    return-object v0
.end method
