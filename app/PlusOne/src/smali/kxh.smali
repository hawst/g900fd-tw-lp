.class public abstract Lkxh;
.super Lhya;
.source "PG"

# interfaces
.implements Lbc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lhya;",
        "Lbc",
        "<",
        "Lkxl;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:I

.field public b:Ljava/lang/String;

.field private final c:Lbb;

.field private d:Z

.field private e:Z

.field private f:Ljava/lang/Exception;

.field private g:Z

.field private h:Z

.field private final i:I

.field private j:Lkxm;

.field private l:Z

.field private final m:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lae;Lbb;II)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 124
    invoke-direct {p0, p1, v1}, Lhya;-><init>(Landroid/content/Context;B)V

    .line 81
    iput-boolean v3, p0, Lkxh;->h:Z

    .line 87
    new-instance v0, Lkxm;

    invoke-virtual {p0}, Lkxh;->b()[Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Lkxm;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lkxh;->j:Lkxm;

    .line 114
    new-instance v0, Lkxj;

    invoke-direct {v0, p0}, Lkxj;-><init>(Lkxh;)V

    iput-object v0, p0, Lkxh;->m:Landroid/os/Handler;

    move v0, v1

    .line 125
    :goto_0
    const/4 v2, 0x2

    if-ge v0, v2, :cond_0

    .line 126
    invoke-virtual {p0, v1, v1}, Lkxh;->b(ZZ)V

    .line 125
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 129
    :cond_0
    mul-int/lit8 v0, p5, 0xa

    add-int/lit16 v0, v0, 0x400

    iput v0, p0, Lkxh;->i:I

    .line 131
    const-string v0, "search_results_fragment"

    .line 132
    invoke-virtual {p2, v0}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lkxo;

    .line 133
    if-nez v0, :cond_2

    .line 134
    new-instance v0, Lkxo;

    invoke-direct {v0}, Lkxo;-><init>()V

    .line 135
    invoke-virtual {p2}, Lae;->a()Lat;

    move-result-object v1

    const-string v2, "search_results_fragment"

    invoke-virtual {v1, v0, v2}, Lat;->a(Lu;Ljava/lang/String;)Lat;

    move-result-object v1

    .line 136
    invoke-virtual {v1}, Lat;->c()I

    .line 146
    :cond_1
    :goto_1
    iget-object v1, p0, Lkxh;->j:Lkxm;

    invoke-virtual {v0, v1}, Lkxo;->a(Lkxm;)V

    .line 148
    iput-object p3, p0, Lkxh;->c:Lbb;

    .line 149
    iput p4, p0, Lkxh;->a:I

    .line 150
    return-void

    .line 138
    :cond_2
    invoke-virtual {v0}, Lkxo;->a()Lkxm;

    move-result-object v1

    .line 139
    if-eqz v1, :cond_1

    .line 140
    iput-object v1, p0, Lkxh;->j:Lkxm;

    .line 141
    iget-object v1, p0, Lkxh;->j:Lkxm;

    invoke-virtual {v1}, Lkxm;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lkxh;->b:Ljava/lang/String;

    .line 142
    iput-boolean v3, p0, Lkxh;->l:Z

    goto :goto_1
.end method

.method static synthetic a(Lkxh;)V
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x1

    iput-boolean v0, p0, Lkxh;->d:Z

    invoke-direct {p0}, Lkxh;->j()V

    return-void
.end method

.method static synthetic b(Lkxh;)V
    .locals 3

    .prologue
    .line 29
    iget-object v0, p0, Lkxh;->c:Lbb;

    iget v1, p0, Lkxh;->i:I

    invoke-virtual {v0, v1}, Lbb;->b(I)Ldo;

    move-result-object v0

    check-cast v0, Lkxk;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lkxk;->p()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lkxk;->m()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lkxh;->j:Lkxm;

    invoke-virtual {v1}, Lkxm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lkxh;->d:Z

    iget-object v0, p0, Lkxh;->c:Lbb;

    iget v1, p0, Lkxh;->i:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    invoke-direct {p0}, Lkxh;->j()V

    :cond_0
    return-void
.end method

.method private j()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 406
    new-instance v0, Lhym;

    new-array v1, v3, [Ljava/lang/String;

    const-string v2, "_id"

    aput-object v2, v1, v4

    invoke-direct {v0, v1}, Lhym;-><init>([Ljava/lang/String;)V

    .line 407
    iget-object v1, p0, Lkxh;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 408
    iget-object v1, p0, Lkxh;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p0}, Lkxh;->c()I

    move-result v2

    if-lt v1, v2, :cond_5

    .line 409
    iget-boolean v1, p0, Lkxh;->e:Z

    if-eqz v1, :cond_2

    .line 410
    new-array v1, v3, [Ljava/lang/Object;

    iget-object v2, p0, Lkxh;->f:Ljava/lang/Exception;

    invoke-virtual {p0, v2}, Lkxh;->a(Ljava/lang/Exception;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lhym;->a([Ljava/lang/Object;)V

    .line 422
    :cond_0
    :goto_0
    invoke-virtual {v0}, Lhym;->getCount()I

    move-result v1

    if-eqz v1, :cond_1

    .line 423
    invoke-virtual {p0}, Lkxh;->f()V

    .line 426
    :cond_1
    invoke-virtual {p0, v3, v0}, Lkxh;->a(ILandroid/database/Cursor;)V

    .line 427
    return-void

    .line 411
    :cond_2
    iget-boolean v1, p0, Lkxh;->g:Z

    if-eqz v1, :cond_3

    .line 412
    new-array v1, v3, [Ljava/lang/Object;

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lhym;->a([Ljava/lang/Object;)V

    goto :goto_0

    .line 413
    :cond_3
    iget-boolean v1, p0, Lkxh;->d:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lkxh;->h:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lkxh;->j:Lkxm;

    .line 414
    invoke-virtual {v1}, Lkxm;->f()I

    move-result v1

    if-lez v1, :cond_0

    .line 415
    :cond_4
    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lhym;->a([Ljava/lang/Object;)V

    goto :goto_0

    .line 417
    :cond_5
    iget-object v1, p0, Lkxh;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p0}, Lkxh;->c()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 418
    new-array v1, v3, [Ljava/lang/Object;

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lhym;->a([Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 266
    const/4 v0, 0x2

    return v0
.end method

.method protected a(II)I
    .locals 0

    .prologue
    .line 271
    return p1
.end method

.method public a(Ljava/lang/Exception;)I
    .locals 1

    .prologue
    .line 430
    const/4 v0, 0x3

    return v0
.end method

.method protected a(Landroid/content/Context;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 464
    packed-switch p2, :pswitch_data_0

    .line 470
    invoke-virtual {p0, p1, p5}, Lkxh;->b(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    .line 466
    :pswitch_0
    invoke-virtual {p0, p1, p5}, Lkxh;->a(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 464
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public abstract a(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
.end method

.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Lkxl;",
            ">;"
        }
    .end annotation

    .prologue
    .line 324
    iget v0, p0, Lkxh;->i:I

    if-ne p1, v0, :cond_0

    .line 325
    iget-object v0, p0, Lkxh;->j:Lkxm;

    invoke-virtual {v0}, Lkxm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkxh;->a(Ljava/lang/String;)Lkxk;

    move-result-object v0

    .line 327
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract a(Ljava/lang/String;)Lkxk;
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 275
    if-eqz p1, :cond_0

    .line 276
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 277
    const-string v0, "search_list_adapter.query"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkxh;->b:Ljava/lang/String;

    .line 278
    const-string v0, "square_search_list_adapter.error"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lkxh;->e:Z

    .line 279
    const-string v0, "square+search_list_adapter.loading"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lkxh;->d:Z

    .line 280
    const-string v0, "square_search_list_adapter.not_found"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lkxh;->g:Z

    .line 282
    const-string v0, "search_list_adapter.results"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lkxh;->l:Z

    if-nez v0, :cond_0

    .line 283
    const-string v0, "search_list_adapter.results"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lkxm;

    iput-object v0, p0, Lkxh;->j:Lkxm;

    .line 286
    :cond_0
    return-void
.end method

.method public abstract a(Landroid/view/View;I)V
.end method

.method protected a(Landroid/view/View;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 478
    packed-switch p2, :pswitch_data_0

    .line 492
    :cond_0
    :goto_0
    return-void

    .line 480
    :pswitch_0
    invoke-virtual {p0, p1, p3}, Lkxh;->a(Landroid/view/View;Landroid/database/Cursor;)V

    .line 481
    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-virtual {p0}, Lkxh;->h()I

    move-result v1

    sub-int/2addr v0, v1

    if-lt p4, v0, :cond_0

    .line 482
    invoke-virtual {p0}, Lkxh;->g()V

    goto :goto_0

    .line 488
    :pswitch_1
    const/4 v0, 0x0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lkxh;->a(Landroid/view/View;I)V

    goto :goto_0

    .line 478
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public abstract a(Landroid/view/View;Landroid/database/Cursor;)V
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Lkxl;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 400
    return-void
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 29
    check-cast p2, Lkxl;

    invoke-virtual {p0, p1, p2}, Lkxh;->a(Ldo;Lkxl;)V

    return-void
.end method

.method public a(Ldo;Lkxl;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Lkxl;",
            ">;",
            "Lkxl;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 332
    invoke-virtual {p1}, Ldo;->o()I

    move-result v0

    .line 333
    iget v3, p0, Lkxh;->i:I

    if-ne v0, v3, :cond_4

    .line 334
    sget-object v0, Lkxk;->b:Lkxl;

    if-eq p2, v0, :cond_3

    .line 335
    iget-object v0, p0, Lkxh;->m:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 336
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lkxl;->e()Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_0
    move v0, v2

    :goto_0
    iput-boolean v0, p0, Lkxh;->e:Z

    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lkxl;->d()Ljava/lang/Exception;

    move-result-object v0

    iput-object v0, p0, Lkxh;->f:Ljava/lang/Exception;

    :cond_1
    iget-boolean v0, p0, Lkxh;->e:Z

    if-eqz v0, :cond_6

    iput-boolean v1, p0, Lkxh;->d:Z

    .line 337
    :cond_2
    :goto_1
    iget-object v0, p0, Lkxh;->m:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lkxh;->j:Lkxm;

    invoke-virtual {v0}, Lkxm;->h()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-nez v2, :cond_a

    iget-object v0, p0, Lkxh;->m:Landroid/os/Handler;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 339
    :cond_3
    :goto_2
    invoke-direct {p0}, Lkxh;->j()V

    .line 341
    :cond_4
    return-void

    :cond_5
    move v0, v1

    .line 336
    goto :goto_0

    :cond_6
    invoke-virtual {p2}, Lkxl;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lkxh;->j:Lkxm;

    invoke-virtual {v3}, Lkxm;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lkxh;->j:Lkxm;

    invoke-virtual {v0}, Lkxm;->e()Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lkxh;->j:Lkxm;

    invoke-virtual {v0}, Lkxm;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_7
    iput-boolean v1, p0, Lkxh;->d:Z

    invoke-virtual {p2}, Lkxl;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lkxh;->j:Lkxm;

    invoke-virtual {p2}, Lkxl;->c()Lhym;

    move-result-object v4

    invoke-virtual {v4}, Lhym;->getCount()I

    move-result v4

    if-nez v4, :cond_8

    const/4 v0, 0x0

    :cond_8
    invoke-virtual {v3, v0}, Lkxm;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lkxh;->j:Lkxm;

    invoke-virtual {p2}, Lkxl;->c()Lhym;

    move-result-object v3

    invoke-virtual {v0, v3}, Lkxm;->a(Lhym;)V

    iget-object v0, p0, Lkxh;->j:Lkxm;

    invoke-virtual {v0}, Lkxm;->f()I

    move-result v0

    if-nez v0, :cond_9

    :goto_3
    iput-boolean v2, p0, Lkxh;->g:Z

    goto :goto_1

    :cond_9
    move v2, v1

    goto :goto_3

    .line 337
    :cond_a
    invoke-virtual {p0, v1, v0}, Lkxh;->a(ILandroid/database/Cursor;)V

    goto :goto_2
.end method

.method public abstract b(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 292
    const-string v0, "search_list_adapter.query"

    iget-object v1, p0, Lkxh;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    const-string v0, "square_search_list_adapter.error"

    iget-boolean v1, p0, Lkxh;->e:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 294
    const-string v0, "square+search_list_adapter.loading"

    iget-boolean v1, p0, Lkxh;->d:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 295
    const-string v0, "square_search_list_adapter.not_found"

    iget-boolean v1, p0, Lkxh;->g:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 297
    iget-object v0, p0, Lkxh;->j:Lkxm;

    invoke-virtual {v0}, Lkxm;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 298
    const-string v0, "search_list_adapter.results"

    iget-object v1, p0, Lkxh;->j:Lkxm;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 300
    :cond_0
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 220
    iget-object v0, p0, Lkxh;->b:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 252
    :goto_0
    return-void

    .line 224
    :cond_0
    iget-object v0, p0, Lkxh;->j:Lkxm;

    invoke-virtual {v0, p1}, Lkxm;->b(Ljava/lang/String;)V

    .line 225
    iget-object v0, p0, Lkxh;->m:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 226
    iget-object v0, p0, Lkxh;->m:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 228
    iput-object p1, p0, Lkxh;->b:Ljava/lang/String;

    .line 229
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 230
    iget-object v0, p0, Lkxh;->c:Lbb;

    iget v1, p0, Lkxh;->i:I

    invoke-virtual {v0, v1}, Lbb;->a(I)V

    .line 231
    invoke-virtual {p0}, Lkxh;->at()V

    goto :goto_0

    .line 233
    :cond_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 234
    const-string v1, "query"

    iget-object v2, p0, Lkxh;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    iput-boolean v3, p0, Lkxh;->e:Z

    .line 237
    const/4 v1, 0x0

    iput-object v1, p0, Lkxh;->f:Ljava/lang/Exception;

    .line 238
    iput-boolean v3, p0, Lkxh;->g:Z

    .line 239
    iput-boolean v3, p0, Lkxh;->d:Z

    .line 241
    iget-object v1, p0, Lkxh;->m:Landroid/os/Handler;

    const-wide/16 v2, 0x12c

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 247
    iget-object v1, p0, Lkxh;->c:Lbb;

    iget v2, p0, Lkxh;->i:I

    invoke-virtual {v1, v2}, Lbb;->a(I)V

    .line 248
    iget-object v1, p0, Lkxh;->c:Lbb;

    iget v2, p0, Lkxh;->i:I

    invoke-virtual {v1, v2, v0, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 250
    invoke-direct {p0}, Lkxh;->j()V

    goto :goto_0
.end method

.method protected b(II)Z
    .locals 1

    .prologue
    .line 503
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 504
    const/4 v0, 0x0

    .line 506
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lhya;->b(II)Z

    move-result v0

    goto :goto_0
.end method

.method public abstract b()[Ljava/lang/String;
.end method

.method public c()I
    .locals 1

    .prologue
    .line 207
    const/4 v0, 0x2

    return v0
.end method

.method public d()V
    .locals 3

    .prologue
    .line 303
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 304
    const-string v1, "query"

    iget-object v2, p0, Lkxh;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 305
    iget-object v1, p0, Lkxh;->c:Lbb;

    iget v2, p0, Lkxh;->i:I

    invoke-virtual {v1, v2, v0, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 306
    invoke-direct {p0}, Lkxh;->j()V

    .line 307
    return-void
.end method

.method public e()V
    .locals 2

    .prologue
    .line 310
    iget-object v0, p0, Lkxh;->m:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 311
    return-void
.end method

.method protected f()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 391
    iget-object v0, p0, Lkxh;->m:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 392
    iget-object v0, p0, Lkxh;->j:Lkxm;

    invoke-virtual {v0}, Lkxm;->h()Landroid/database/Cursor;

    move-result-object v0

    .line 393
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_0

    .line 394
    invoke-virtual {p0, v2, v0}, Lkxh;->a(ILandroid/database/Cursor;)V

    .line 396
    :cond_0
    return-void
.end method

.method protected g()V
    .locals 2

    .prologue
    .line 437
    iget-object v0, p0, Lkxh;->j:Lkxm;

    invoke-virtual {v0}, Lkxm;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 448
    :goto_0
    return-void

    .line 441
    :cond_0
    iget-object v0, p0, Lkxh;->m:Landroid/os/Handler;

    new-instance v1, Lkxi;

    invoke-direct {v1, p0}, Lkxi;-><init>(Lkxh;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method protected h()I
    .locals 1

    .prologue
    .line 498
    const/4 v0, 0x4

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lkxh;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method
