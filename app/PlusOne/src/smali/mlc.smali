.class public final Lmlc;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnew;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 24541
    invoke-direct {p0}, Loxq;-><init>()V

    .line 24544
    iput-object v0, p0, Lmlc;->apiHeader:Llyq;

    .line 24547
    iput-object v0, p0, Lmlc;->a:Lnew;

    .line 24541
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 24564
    const/4 v0, 0x0

    .line 24565
    iget-object v1, p0, Lmlc;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 24566
    const/4 v0, 0x1

    iget-object v1, p0, Lmlc;->apiHeader:Llyq;

    .line 24567
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 24569
    :cond_0
    iget-object v1, p0, Lmlc;->a:Lnew;

    if-eqz v1, :cond_1

    .line 24570
    const/4 v1, 0x2

    iget-object v2, p0, Lmlc;->a:Lnew;

    .line 24571
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 24573
    :cond_1
    iget-object v1, p0, Lmlc;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 24574
    iput v0, p0, Lmlc;->ai:I

    .line 24575
    return v0
.end method

.method public a(Loxn;)Lmlc;
    .locals 2

    .prologue
    .line 24583
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 24584
    sparse-switch v0, :sswitch_data_0

    .line 24588
    iget-object v1, p0, Lmlc;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 24589
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmlc;->ah:Ljava/util/List;

    .line 24592
    :cond_1
    iget-object v1, p0, Lmlc;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 24594
    :sswitch_0
    return-object p0

    .line 24599
    :sswitch_1
    iget-object v0, p0, Lmlc;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 24600
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmlc;->apiHeader:Llyq;

    .line 24602
    :cond_2
    iget-object v0, p0, Lmlc;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 24606
    :sswitch_2
    iget-object v0, p0, Lmlc;->a:Lnew;

    if-nez v0, :cond_3

    .line 24607
    new-instance v0, Lnew;

    invoke-direct {v0}, Lnew;-><init>()V

    iput-object v0, p0, Lmlc;->a:Lnew;

    .line 24609
    :cond_3
    iget-object v0, p0, Lmlc;->a:Lnew;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 24584
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 24552
    iget-object v0, p0, Lmlc;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 24553
    const/4 v0, 0x1

    iget-object v1, p0, Lmlc;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 24555
    :cond_0
    iget-object v0, p0, Lmlc;->a:Lnew;

    if-eqz v0, :cond_1

    .line 24556
    const/4 v0, 0x2

    iget-object v1, p0, Lmlc;->a:Lnew;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 24558
    :cond_1
    iget-object v0, p0, Lmlc;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 24560
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 24537
    invoke-virtual {p0, p1}, Lmlc;->a(Loxn;)Lmlc;

    move-result-object v0

    return-object v0
.end method
