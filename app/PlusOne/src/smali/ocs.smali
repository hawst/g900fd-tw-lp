.class public final Locs;
.super Loxq;
.source "PG"


# instance fields
.field private a:[Loct;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 149
    invoke-direct {p0}, Loxq;-><init>()V

    .line 266
    sget-object v0, Loct;->a:[Loct;

    iput-object v0, p0, Locs;->a:[Loct;

    .line 149
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 284
    .line 285
    iget-object v1, p0, Locs;->a:[Loct;

    if-eqz v1, :cond_1

    .line 286
    iget-object v2, p0, Locs;->a:[Loct;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 287
    if-eqz v4, :cond_0

    .line 288
    const/4 v5, 0x1

    .line 289
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 286
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 293
    :cond_1
    iget-object v1, p0, Locs;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 294
    iput v0, p0, Locs;->ai:I

    .line 295
    return v0
.end method

.method public a(Loxn;)Locs;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 303
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 304
    sparse-switch v0, :sswitch_data_0

    .line 308
    iget-object v2, p0, Locs;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 309
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Locs;->ah:Ljava/util/List;

    .line 312
    :cond_1
    iget-object v2, p0, Locs;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 314
    :sswitch_0
    return-object p0

    .line 319
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 320
    iget-object v0, p0, Locs;->a:[Loct;

    if-nez v0, :cond_3

    move v0, v1

    .line 321
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Loct;

    .line 322
    iget-object v3, p0, Locs;->a:[Loct;

    if-eqz v3, :cond_2

    .line 323
    iget-object v3, p0, Locs;->a:[Loct;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 325
    :cond_2
    iput-object v2, p0, Locs;->a:[Loct;

    .line 326
    :goto_2
    iget-object v2, p0, Locs;->a:[Loct;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 327
    iget-object v2, p0, Locs;->a:[Loct;

    new-instance v3, Loct;

    invoke-direct {v3}, Loct;-><init>()V

    aput-object v3, v2, v0

    .line 328
    iget-object v2, p0, Locs;->a:[Loct;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 329
    invoke-virtual {p1}, Loxn;->a()I

    .line 326
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 320
    :cond_3
    iget-object v0, p0, Locs;->a:[Loct;

    array-length v0, v0

    goto :goto_1

    .line 332
    :cond_4
    iget-object v2, p0, Locs;->a:[Loct;

    new-instance v3, Loct;

    invoke-direct {v3}, Loct;-><init>()V

    aput-object v3, v2, v0

    .line 333
    iget-object v2, p0, Locs;->a:[Loct;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 304
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 271
    iget-object v0, p0, Locs;->a:[Loct;

    if-eqz v0, :cond_1

    .line 272
    iget-object v1, p0, Locs;->a:[Loct;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 273
    if-eqz v3, :cond_0

    .line 274
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 272
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 278
    :cond_1
    iget-object v0, p0, Locs;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 280
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 145
    invoke-virtual {p0, p1}, Locs;->a(Loxn;)Locs;

    move-result-object v0

    return-object v0
.end method
