.class public final Ldvz;
.super Lkfp;
.source "PG"


# instance fields
.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:I

.field private l:I

.field private m:I

.field private n:Lhmv;

.field private o:Lhmw;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lkfp;-><init>()V

    return-void
.end method


# virtual methods
.method a()Landroid/os/Bundle;
    .locals 6

    .prologue
    .line 117
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 118
    iget v1, p0, Ldvz;->d:I

    if-lez v1, :cond_0

    .line 119
    const-string v1, "extra_photo_count"

    iget v2, p0, Ldvz;->d:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 121
    :cond_0
    iget v1, p0, Ldvz;->c:I

    if-lez v1, :cond_1

    .line 122
    const-string v1, "extra_video_count"

    iget v2, p0, Ldvz;->c:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 124
    :cond_1
    iget-wide v2, p0, Ldvz;->a:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_2

    .line 125
    const-string v1, "extra_total_bytes"

    iget-wide v2, p0, Ldvz;->a:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 127
    :cond_2
    iget v1, p0, Ldvz;->e:I

    if-lez v1, :cond_3

    .line 128
    const-string v1, "extra_thumbnail_count"

    iget v2, p0, Ldvz;->e:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 130
    :cond_3
    iget v1, p0, Ldvz;->f:I

    if-lez v1, :cond_4

    .line 131
    const-string v1, "extra_max_highlight_item_count"

    iget v2, p0, Ldvz;->f:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 133
    :cond_4
    iget v1, p0, Ldvz;->h:I

    if-lez v1, :cond_5

    .line 134
    const-string v1, "extra_max_highlight_page_count"

    iget v2, p0, Ldvz;->h:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 136
    :cond_5
    iget v1, p0, Ldvz;->g:I

    if-lez v1, :cond_6

    .line 137
    const-string v1, "extra_highlight_item_count"

    iget v2, p0, Ldvz;->g:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 139
    :cond_6
    iget v1, p0, Ldvz;->i:I

    if-lez v1, :cond_7

    .line 140
    const-string v1, "extra_highlight_page_count"

    iget v2, p0, Ldvz;->i:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 142
    :cond_7
    iget v1, p0, Ldvz;->j:I

    if-lez v1, :cond_8

    .line 143
    const-string v1, "extra_all_photos_item_count"

    iget v2, p0, Ldvz;->j:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 145
    :cond_8
    iget v1, p0, Ldvz;->l:I

    if-lez v1, :cond_9

    .line 146
    const-string v1, "extra_max_all_photos_item_count"

    iget v2, p0, Ldvz;->l:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 148
    :cond_9
    iget v1, p0, Ldvz;->k:I

    if-lez v1, :cond_a

    .line 149
    const-string v1, "extra_all_photos_page_count"

    iget v2, p0, Ldvz;->k:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 151
    :cond_a
    iget v1, p0, Ldvz;->m:I

    if-lez v1, :cond_b

    .line 152
    const-string v1, "extra_cache_evictions"

    iget v2, p0, Ldvz;->m:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 154
    :cond_b
    return-object v0
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 72
    iget v0, p0, Ldvz;->c:I

    add-int/2addr v0, p1

    iput v0, p0, Ldvz;->c:I

    .line 73
    return-void
.end method

.method public declared-synchronized a(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 45
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Ldvz;->f()V

    .line 46
    invoke-static {}, Llsx;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    invoke-virtual {p0, p1, p2}, Ldvz;->b(Landroid/content/Context;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 56
    :goto_0
    monitor-exit p0

    return-void

    .line 49
    :cond_0
    :try_start_1
    new-instance v0, Ldwa;

    invoke-direct {v0, p0, p1, p2}, Ldwa;-><init>(Ldvz;Landroid/content/Context;I)V

    invoke-static {v0}, Llsx;->a(Ljava/lang/Runnable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 45
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Ljava/lang/String;Lhmw;Lhmv;)V
    .locals 1

    .prologue
    .line 36
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Ldvz;->c(Ljava/lang/String;)V

    .line 37
    iput-object p2, p0, Ldvz;->o:Lhmw;

    .line 38
    iput-object p3, p0, Ldvz;->n:Lhmv;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 39
    monitor-exit p0

    return-void

    .line 36
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 76
    iget v0, p0, Ldvz;->d:I

    add-int/2addr v0, p1

    iput v0, p0, Ldvz;->d:I

    .line 77
    return-void
.end method

.method b(Landroid/content/Context;I)V
    .locals 4

    .prologue
    .line 59
    invoke-virtual {p0}, Ldvz;->a()Landroid/os/Bundle;

    move-result-object v1

    .line 60
    iget-object v0, p0, Ldvz;->o:Lhmw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldvz;->n:Lhmv;

    if-nez v0, :cond_1

    .line 69
    :cond_0
    :goto_0
    return-void

    .line 63
    :cond_1
    const-class v0, Lhms;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    invoke-direct {v2, p1, p2}, Lhmr;-><init>(Landroid/content/Context;I)V

    iget-object v3, p0, Ldvz;->n:Lhmv;

    .line 65
    invoke-virtual {v2, v3}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v2

    iget-object v3, p0, Ldvz;->o:Lhmw;

    .line 66
    invoke-virtual {v2, v3}, Lhmr;->a(Lhmw;)Lhmr;

    move-result-object v2

    .line 67
    invoke-virtual {v2, v1}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    .line 63
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    goto :goto_0
.end method

.method public c(I)V
    .locals 1

    .prologue
    .line 80
    iget v0, p0, Ldvz;->e:I

    add-int/2addr v0, p1

    iput v0, p0, Ldvz;->e:I

    .line 81
    return-void
.end method

.method public d(I)V
    .locals 1

    .prologue
    .line 84
    iget v0, p0, Ldvz;->f:I

    add-int/2addr v0, p1

    iput v0, p0, Ldvz;->f:I

    .line 85
    return-void
.end method

.method public e(I)V
    .locals 1

    .prologue
    .line 88
    iget v0, p0, Ldvz;->h:I

    add-int/2addr v0, p1

    iput v0, p0, Ldvz;->h:I

    .line 89
    return-void
.end method

.method public f(I)V
    .locals 1

    .prologue
    .line 92
    iget v0, p0, Ldvz;->g:I

    add-int/2addr v0, p1

    iput v0, p0, Ldvz;->g:I

    .line 93
    return-void
.end method

.method public g(I)V
    .locals 1

    .prologue
    .line 96
    iget v0, p0, Ldvz;->i:I

    add-int/2addr v0, p1

    iput v0, p0, Ldvz;->i:I

    .line 97
    return-void
.end method

.method public h(I)V
    .locals 1

    .prologue
    .line 100
    iget v0, p0, Ldvz;->j:I

    add-int/2addr v0, p1

    iput v0, p0, Ldvz;->j:I

    .line 101
    return-void
.end method

.method public i(I)V
    .locals 1

    .prologue
    .line 104
    iget v0, p0, Ldvz;->l:I

    add-int/2addr v0, p1

    iput v0, p0, Ldvz;->l:I

    .line 105
    return-void
.end method

.method public j(I)V
    .locals 1

    .prologue
    .line 108
    iget v0, p0, Ldvz;->k:I

    add-int/2addr v0, p1

    iput v0, p0, Ldvz;->k:I

    .line 109
    return-void
.end method

.method public k(I)V
    .locals 1

    .prologue
    .line 112
    iget v0, p0, Ldvz;->m:I

    add-int/2addr v0, p1

    iput v0, p0, Ldvz;->m:I

    .line 113
    return-void
.end method
