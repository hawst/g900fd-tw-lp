.class final Lcwm;
.super Lhxz;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lhxz",
        "<",
        "Lcwk;",
        ">;"
    }
.end annotation


# static fields
.field private static final b:Ljava/text/SimpleDateFormat;

.field private static final c:[Ljava/lang/String;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InlinedApi"
        }
    .end annotation
.end field

.field private static final d:[Ljava/lang/String;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InlinedApi"
        }
    .end annotation
.end field

.field private static final e:[Ljava/lang/String;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InlinedApi"
        }
    .end annotation
.end field

.field private static final f:[Ljava/lang/String;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InlinedApi"
        }
    .end annotation
.end field


# instance fields
.field private final g:[Ljava/lang/String;

.field private final h:Landroid/net/Uri;

.field private final i:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 474
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy:MM:dd kk:mm:ss"

    .line 475
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcwm;->b:Ljava/text/SimpleDateFormat;

    .line 478
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_data"

    aput-object v1, v0, v3

    const-string v1, "datetaken"

    aput-object v1, v0, v4

    const-string v1, "orientation"

    aput-object v1, v0, v5

    const-string v1, "title"

    aput-object v1, v0, v6

    const-string v1, "_size"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "latitude"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "longitude"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "null AS duration"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "null AS width"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "null AS height"

    aput-object v2, v0, v1

    sput-object v0, Lcwm;->c:[Ljava/lang/String;

    .line 491
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_data"

    aput-object v1, v0, v3

    const-string v1, "datetaken"

    aput-object v1, v0, v4

    const-string v1, "orientation"

    aput-object v1, v0, v5

    const-string v1, "title"

    aput-object v1, v0, v6

    const-string v1, "_size"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "latitude"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "longitude"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "null AS duration"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "width"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "height"

    aput-object v2, v0, v1

    sput-object v0, Lcwm;->d:[Ljava/lang/String;

    .line 504
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_data"

    aput-object v1, v0, v3

    const-string v1, "datetaken"

    aput-object v1, v0, v4

    const-string v1, "null AS orientation"

    aput-object v1, v0, v5

    const-string v1, "title"

    aput-object v1, v0, v6

    const-string v1, "_size"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "latitude"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "longitude"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "null AS width"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "null AS height"

    aput-object v2, v0, v1

    sput-object v0, Lcwm;->e:[Ljava/lang/String;

    .line 517
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_data"

    aput-object v1, v0, v3

    const-string v1, "datetaken"

    aput-object v1, v0, v4

    const-string v1, "null AS orientation"

    aput-object v1, v0, v5

    const-string v1, "title"

    aput-object v1, v0, v6

    const-string v1, "_size"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "latitude"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "longitude"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "width"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "height"

    aput-object v2, v0, v1

    sput-object v0, Lcwm;->f:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;I)V
    .locals 2

    .prologue
    const/16 v1, 0x10

    .line 545
    invoke-direct {p0, p1}, Lhxz;-><init>(Landroid/content/Context;)V

    .line 546
    iput-object p2, p0, Lcwm;->h:Landroid/net/Uri;

    .line 547
    iput p3, p0, Lcwm;->i:I

    .line 548
    if-nez p3, :cond_1

    .line 549
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v1, :cond_0

    .line 550
    sget-object v0, Lcwm;->d:[Ljava/lang/String;

    iput-object v0, p0, Lcwm;->g:[Ljava/lang/String;

    .line 558
    :goto_0
    return-void

    .line 552
    :cond_0
    sget-object v0, Lcwm;->c:[Ljava/lang/String;

    iput-object v0, p0, Lcwm;->g:[Ljava/lang/String;

    goto :goto_0

    .line 554
    :cond_1
    const/4 v0, 0x1

    if-ne p3, v0, :cond_3

    .line 555
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v1, :cond_2

    .line 556
    sget-object v0, Lcwm;->f:[Ljava/lang/String;

    iput-object v0, p0, Lcwm;->g:[Ljava/lang/String;

    goto :goto_0

    .line 558
    :cond_2
    sget-object v0, Lcwm;->e:[Ljava/lang/String;

    iput-object v0, p0, Lcwm;->g:[Ljava/lang/String;

    goto :goto_0

    .line 561
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "You must specify a valid media type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public f()Lcwk;
    .locals 14

    .prologue
    const-wide/16 v12, 0x0

    const/4 v10, 0x1

    const/4 v7, 0x0

    const-wide/16 v8, 0x0

    const/4 v3, 0x0

    .line 631
    invoke-virtual {p0}, Lcwm;->n()Landroid/content/Context;

    move-result-object v0

    .line 632
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 633
    new-instance v6, Lcwk;

    invoke-direct {v6}, Lcwk;-><init>()V

    .line 636
    iget-object v1, p0, Lcwm;->h:Landroid/net/Uri;

    invoke-static {v1}, Llsb;->b(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_1b

    .line 637
    iget-object v1, p0, Lcwm;->h:Landroid/net/Uri;

    iget-object v2, p0, Lcwm;->g:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 640
    :goto_0
    if-eqz v1, :cond_a

    .line 642
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 643
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 644
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v6, Lcwk;->h:Ljava/lang/String;

    .line 646
    :cond_0
    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 647
    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v6, Lcwk;->c:Ljava/lang/Long;

    .line 649
    :cond_1
    const/4 v2, 0x2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_2

    .line 650
    const/4 v2, 0x2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v6, Lcwk;->f:Ljava/lang/Integer;

    .line 652
    :cond_2
    const/4 v2, 0x3

    invoke-interface {v1, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_3

    .line 653
    const/4 v2, 0x3

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v6, Lcwk;->g:Ljava/lang/String;

    .line 655
    :cond_3
    const/4 v2, 0x4

    invoke-interface {v1, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_4

    .line 656
    const/4 v2, 0x4

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v6, Lcwk;->i:Ljava/lang/Long;

    .line 658
    :cond_4
    const/4 v2, 0x5

    invoke-interface {v1, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_5

    .line 659
    const/4 v2, 0x5

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    iput-object v2, v6, Lcwk;->a:Ljava/lang/Double;

    .line 661
    :cond_5
    const/4 v2, 0x6

    invoke-interface {v1, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_6

    .line 662
    const/4 v2, 0x6

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    iput-object v2, v6, Lcwk;->b:Ljava/lang/Double;

    .line 664
    :cond_6
    const/4 v2, 0x7

    invoke-interface {v1, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_7

    .line 665
    const/4 v2, 0x7

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v6, Lcwk;->q:Ljava/lang/Long;

    .line 667
    :cond_7
    const/16 v2, 0x8

    invoke-interface {v1, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_8

    .line 668
    const/16 v2, 0x8

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-long v4, v2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v6, Lcwk;->d:Ljava/lang/Long;

    .line 670
    :cond_8
    const/16 v2, 0x9

    invoke-interface {v1, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_9

    .line 671
    const/16 v2, 0x9

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-long v4, v2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v6, Lcwk;->e:Ljava/lang/Long;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 675
    :cond_9
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 679
    :cond_a
    iget v1, p0, Lcwm;->i:I

    if-nez v1, :cond_15

    .line 680
    new-instance v2, Lidp;

    invoke-direct {v2}, Lidp;-><init>()V

    :try_start_1
    iget-object v1, p0, Lcwm;->h:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v1

    invoke-virtual {v2, v1}, Lidp;->a(Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :goto_1
    iget-object v1, v6, Lcwk;->c:Ljava/lang/Long;

    invoke-static {v1}, Llsl;->a(Ljava/lang/Long;)J

    move-result-wide v4

    cmp-long v1, v4, v8

    if-nez v1, :cond_b

    sget v1, Lidp;->i:I

    invoke-virtual {v2, v1}, Lidp;->b(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_b

    :try_start_2
    sget-object v4, Lcwm;->b:Ljava/text/SimpleDateFormat;

    invoke-virtual {v4, v1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v6, Lcwk;->c:Ljava/lang/Long;
    :try_end_2
    .catch Ljava/text/ParseException; {:try_start_2 .. :try_end_2} :catch_1

    :cond_b
    :goto_2
    iget-object v1, v6, Lcwk;->d:Ljava/lang/Long;

    invoke-static {v1}, Llsl;->a(Ljava/lang/Long;)J

    move-result-wide v4

    cmp-long v1, v4, v8

    if-nez v1, :cond_c

    sget v1, Lidp;->a:I

    invoke-virtual {v2, v1}, Lidp;->c(I)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v6, Lcwk;->d:Ljava/lang/Long;

    :cond_c
    iget-object v1, v6, Lcwk;->e:Ljava/lang/Long;

    invoke-static {v1}, Llsl;->a(Ljava/lang/Long;)J

    move-result-wide v4

    cmp-long v1, v4, v8

    if-nez v1, :cond_d

    sget v1, Lidp;->b:I

    invoke-virtual {v2, v1}, Lidp;->c(I)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v6, Lcwk;->e:Ljava/lang/Long;

    :cond_d
    iget-object v1, v6, Lcwk;->f:Ljava/lang/Integer;

    invoke-static {v1}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v1

    if-nez v1, :cond_e

    sget v1, Lidp;->f:I

    invoke-virtual {v2, v1}, Lidp;->e(I)Ljava/lang/Byte;

    move-result-object v1

    if-nez v1, :cond_18

    move-object v1, v3

    :goto_3
    iput-object v1, v6, Lcwk;->f:Ljava/lang/Integer;

    :cond_e
    iget-object v1, v6, Lcwk;->p:Ljava/lang/Integer;

    invoke-static {v1}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v1

    if-nez v1, :cond_f

    sget v1, Lidp;->p:I

    invoke-virtual {v2, v1}, Lidp;->d(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v6, Lcwk;->p:Ljava/lang/Integer;

    :cond_f
    iget-object v1, v6, Lcwk;->j:Ljava/lang/Float;

    if-nez v1, :cond_10

    sget v1, Lidp;->q:I

    invoke-virtual {v2, v1}, Lidp;->f(I)Liee;

    move-result-object v1

    if-nez v1, :cond_19

    move-object v1, v3

    :goto_4
    iput-object v1, v6, Lcwk;->j:Ljava/lang/Float;

    :cond_10
    iget-object v1, v6, Lcwk;->k:Ljava/lang/Float;

    if-nez v1, :cond_11

    sget v1, Lidp;->o:I

    invoke-virtual {v2, v1}, Lidp;->f(I)Liee;

    move-result-object v1

    if-nez v1, :cond_1a

    :goto_5
    iput-object v3, v6, Lcwk;->k:Ljava/lang/Float;

    :cond_11
    iget-object v1, v6, Lcwk;->a:Ljava/lang/Double;

    invoke-static {v1}, Llsl;->a(Ljava/lang/Double;)D

    move-result-wide v4

    cmpl-double v1, v4, v12

    if-eqz v1, :cond_12

    iget-object v1, v6, Lcwk;->b:Ljava/lang/Double;

    invoke-static {v1}, Llsl;->a(Ljava/lang/Double;)D

    move-result-wide v4

    cmpl-double v1, v4, v12

    if-nez v1, :cond_13

    :cond_12
    invoke-virtual {v2}, Lidp;->c()[D

    move-result-object v1

    if-eqz v1, :cond_13

    aget-wide v2, v1, v7

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    iput-object v2, v6, Lcwk;->a:Ljava/lang/Double;

    aget-wide v2, v1, v10

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    iput-object v1, v6, Lcwk;->b:Ljava/lang/Double;

    :cond_13
    iget-object v1, v6, Lcwk;->d:Ljava/lang/Long;

    invoke-static {v1}, Llsl;->a(Ljava/lang/Long;)J

    move-result-wide v2

    cmp-long v1, v2, v8

    if-eqz v1, :cond_14

    iget-object v1, v6, Lcwk;->e:Ljava/lang/Long;

    invoke-static {v1}, Llsl;->a(Ljava/lang/Long;)J

    move-result-wide v2

    cmp-long v1, v2, v8

    if-nez v1, :cond_15

    :cond_14
    :try_start_3
    iget-object v1, p0, Lcwm;->h:Landroid/net/Uri;

    invoke-static {v0, v1}, Ljcq;->a(Landroid/content/ContentResolver;Landroid/net/Uri;)Landroid/graphics/Point;

    move-result-object v0

    iget v1, v0, Landroid/graphics/Point;->x:I

    int-to-long v2, v1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v6, Lcwk;->d:Ljava/lang/Long;

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-long v0, v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v6, Lcwk;->e:Ljava/lang/Long;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 682
    :cond_15
    :goto_6
    iget-object v0, v6, Lcwk;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_16

    iget-object v0, v6, Lcwk;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_16

    .line 683
    new-instance v0, Ljava/io/File;

    iget-object v1, v6, Lcwk;->h:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v6, Lcwk;->g:Ljava/lang/String;

    .line 685
    :cond_16
    iget-object v0, v6, Lcwk;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 686
    iget-object v0, p0, Lcwm;->h:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v6, Lcwk;->h:Ljava/lang/String;

    .line 689
    :cond_17
    return-object v6

    .line 675
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 680
    :catch_0
    move-exception v1

    const-string v4, "ExifInfoDialogFragment"

    const-string v5, "failed to read exif data"

    invoke-static {v4, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_1

    :catch_1
    move-exception v1

    const-string v4, "ExifInfoDialogFragment"

    const-string v5, "failed to parse exif timestamp"

    invoke-static {v4, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_2

    :cond_18
    invoke-virtual {v1}, Ljava/lang/Byte;->byteValue()B

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/16 :goto_3

    :cond_19
    invoke-virtual {v1}, Liee;->c()D

    move-result-wide v4

    double-to-float v1, v4

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    goto/16 :goto_4

    :cond_1a
    invoke-virtual {v1}, Liee;->c()D

    move-result-wide v4

    double-to-float v1, v4

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    goto/16 :goto_5

    :catch_2
    move-exception v0

    goto :goto_6

    :cond_1b
    move-object v1, v3

    goto/16 :goto_0
.end method

.method public synthetic j()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 471
    invoke-virtual {p0}, Lcwm;->f()Lcwk;

    move-result-object v0

    return-object v0
.end method
