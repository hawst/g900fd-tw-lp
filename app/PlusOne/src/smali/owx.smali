.class public final Lowx;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lowx;


# instance fields
.field private b:Losf;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 620
    const/4 v0, 0x0

    new-array v0, v0, [Lowx;

    sput-object v0, Lowx;->a:[Lowx;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 621
    invoke-direct {p0}, Loxq;-><init>()V

    .line 624
    const/4 v0, 0x0

    iput-object v0, p0, Lowx;->b:Losf;

    .line 621
    return-void
.end method


# virtual methods
.method public a()I
    .locals 2

    .prologue
    .line 638
    const/4 v0, 0x0

    .line 639
    iget-object v1, p0, Lowx;->b:Losf;

    if-eqz v1, :cond_0

    .line 640
    const/4 v0, 0x1

    iget-object v1, p0, Lowx;->b:Losf;

    .line 641
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 643
    :cond_0
    iget-object v1, p0, Lowx;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 644
    iput v0, p0, Lowx;->ai:I

    .line 645
    return v0
.end method

.method public a(Loxn;)Lowx;
    .locals 2

    .prologue
    .line 653
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 654
    sparse-switch v0, :sswitch_data_0

    .line 658
    iget-object v1, p0, Lowx;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 659
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lowx;->ah:Ljava/util/List;

    .line 662
    :cond_1
    iget-object v1, p0, Lowx;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 664
    :sswitch_0
    return-object p0

    .line 669
    :sswitch_1
    iget-object v0, p0, Lowx;->b:Losf;

    if-nez v0, :cond_2

    .line 670
    new-instance v0, Losf;

    invoke-direct {v0}, Losf;-><init>()V

    iput-object v0, p0, Lowx;->b:Losf;

    .line 672
    :cond_2
    iget-object v0, p0, Lowx;->b:Losf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 654
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 629
    iget-object v0, p0, Lowx;->b:Losf;

    if-eqz v0, :cond_0

    .line 630
    const/4 v0, 0x1

    iget-object v1, p0, Lowx;->b:Losf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 632
    :cond_0
    iget-object v0, p0, Lowx;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 634
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 617
    invoke-virtual {p0, p1}, Lowx;->a(Loxn;)Lowx;

    move-result-object v0

    return-object v0
.end method
