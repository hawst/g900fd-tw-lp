.class public final Lmlh;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lmli;

.field public b:Ljava/lang/Boolean;

.field private c:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 106
    sget-object v0, Lmli;->a:[Lmli;

    iput-object v0, p0, Lmlh;->a:[Lmli;

    .line 109
    const/high16 v0, -0x80000000

    iput v0, p0, Lmlh;->c:I

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 135
    .line 136
    iget-object v1, p0, Lmlh;->a:[Lmli;

    if-eqz v1, :cond_1

    .line 137
    iget-object v2, p0, Lmlh;->a:[Lmli;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 138
    if-eqz v4, :cond_0

    .line 139
    const/4 v5, 0x3

    .line 140
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 137
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 144
    :cond_1
    iget v1, p0, Lmlh;->c:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_2

    .line 145
    const/4 v1, 0x4

    iget v2, p0, Lmlh;->c:I

    .line 146
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 148
    :cond_2
    iget-object v1, p0, Lmlh;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 149
    const/4 v1, 0x5

    iget-object v2, p0, Lmlh;->b:Ljava/lang/Boolean;

    .line 150
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 152
    :cond_3
    iget-object v1, p0, Lmlh;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 153
    iput v0, p0, Lmlh;->ai:I

    .line 154
    return v0
.end method

.method public a(Loxn;)Lmlh;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 162
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 163
    sparse-switch v0, :sswitch_data_0

    .line 167
    iget-object v2, p0, Lmlh;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 168
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmlh;->ah:Ljava/util/List;

    .line 171
    :cond_1
    iget-object v2, p0, Lmlh;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 173
    :sswitch_0
    return-object p0

    .line 178
    :sswitch_1
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 179
    iget-object v0, p0, Lmlh;->a:[Lmli;

    if-nez v0, :cond_3

    move v0, v1

    .line 180
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lmli;

    .line 181
    iget-object v3, p0, Lmlh;->a:[Lmli;

    if-eqz v3, :cond_2

    .line 182
    iget-object v3, p0, Lmlh;->a:[Lmli;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 184
    :cond_2
    iput-object v2, p0, Lmlh;->a:[Lmli;

    .line 185
    :goto_2
    iget-object v2, p0, Lmlh;->a:[Lmli;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 186
    iget-object v2, p0, Lmlh;->a:[Lmli;

    new-instance v3, Lmli;

    invoke-direct {v3}, Lmli;-><init>()V

    aput-object v3, v2, v0

    .line 187
    iget-object v2, p0, Lmlh;->a:[Lmli;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 188
    invoke-virtual {p1}, Loxn;->a()I

    .line 185
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 179
    :cond_3
    iget-object v0, p0, Lmlh;->a:[Lmli;

    array-length v0, v0

    goto :goto_1

    .line 191
    :cond_4
    iget-object v2, p0, Lmlh;->a:[Lmli;

    new-instance v3, Lmli;

    invoke-direct {v3}, Lmli;-><init>()V

    aput-object v3, v2, v0

    .line 192
    iget-object v2, p0, Lmlh;->a:[Lmli;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 196
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 197
    if-eqz v0, :cond_5

    const/4 v2, 0x1

    if-eq v0, v2, :cond_5

    const/4 v2, 0x2

    if-eq v0, v2, :cond_5

    const/4 v2, 0x3

    if-ne v0, v2, :cond_6

    .line 201
    :cond_5
    iput v0, p0, Lmlh;->c:I

    goto :goto_0

    .line 203
    :cond_6
    iput v1, p0, Lmlh;->c:I

    goto :goto_0

    .line 208
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmlh;->b:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 163
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1a -> :sswitch_1
        0x20 -> :sswitch_2
        0x28 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 116
    iget-object v0, p0, Lmlh;->a:[Lmli;

    if-eqz v0, :cond_1

    .line 117
    iget-object v1, p0, Lmlh;->a:[Lmli;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 118
    if-eqz v3, :cond_0

    .line 119
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 117
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 123
    :cond_1
    iget v0, p0, Lmlh;->c:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_2

    .line 124
    const/4 v0, 0x4

    iget v1, p0, Lmlh;->c:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 126
    :cond_2
    iget-object v0, p0, Lmlh;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 127
    const/4 v0, 0x5

    iget-object v1, p0, Lmlh;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 129
    :cond_3
    iget-object v0, p0, Lmlh;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 131
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lmlh;->a(Loxn;)Lmlh;

    move-result-object v0

    return-object v0
.end method
