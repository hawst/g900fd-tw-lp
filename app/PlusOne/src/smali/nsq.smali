.class public final Lnsq;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnsq;


# instance fields
.field public b:Lnsr;

.field public c:Ljava/lang/String;

.field private d:Ljava/lang/Double;

.field private e:Ljava/lang/Integer;

.field private f:[Lnsh;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2737
    const/4 v0, 0x0

    new-array v0, v0, [Lnsq;

    sput-object v0, Lnsq;->a:[Lnsq;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2738
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2741
    const/4 v0, 0x0

    iput-object v0, p0, Lnsq;->b:Lnsr;

    .line 2748
    sget-object v0, Lnsh;->a:[Lnsh;

    iput-object v0, p0, Lnsq;->f:[Lnsh;

    .line 2738
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2780
    .line 2781
    iget-object v0, p0, Lnsq;->b:Lnsr;

    if-eqz v0, :cond_5

    .line 2782
    const/4 v0, 0x1

    iget-object v2, p0, Lnsq;->b:Lnsr;

    .line 2783
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2785
    :goto_0
    iget-object v2, p0, Lnsq;->d:Ljava/lang/Double;

    if-eqz v2, :cond_0

    .line 2786
    const/4 v2, 0x2

    iget-object v3, p0, Lnsq;->d:Ljava/lang/Double;

    .line 2787
    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x8

    add-int/2addr v0, v2

    .line 2789
    :cond_0
    iget-object v2, p0, Lnsq;->e:Ljava/lang/Integer;

    if-eqz v2, :cond_1

    .line 2790
    const/4 v2, 0x3

    iget-object v3, p0, Lnsq;->e:Ljava/lang/Integer;

    .line 2791
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 2793
    :cond_1
    iget-object v2, p0, Lnsq;->f:[Lnsh;

    if-eqz v2, :cond_3

    .line 2794
    iget-object v2, p0, Lnsq;->f:[Lnsh;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 2795
    if-eqz v4, :cond_2

    .line 2796
    const/4 v5, 0x4

    .line 2797
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 2794
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2801
    :cond_3
    iget-object v1, p0, Lnsq;->c:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 2802
    const/4 v1, 0x5

    iget-object v2, p0, Lnsq;->c:Ljava/lang/String;

    .line 2803
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2805
    :cond_4
    iget-object v1, p0, Lnsq;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2806
    iput v0, p0, Lnsq;->ai:I

    .line 2807
    return v0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lnsq;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2815
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2816
    sparse-switch v0, :sswitch_data_0

    .line 2820
    iget-object v2, p0, Lnsq;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 2821
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnsq;->ah:Ljava/util/List;

    .line 2824
    :cond_1
    iget-object v2, p0, Lnsq;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2826
    :sswitch_0
    return-object p0

    .line 2831
    :sswitch_1
    iget-object v0, p0, Lnsq;->b:Lnsr;

    if-nez v0, :cond_2

    .line 2832
    new-instance v0, Lnsr;

    invoke-direct {v0}, Lnsr;-><init>()V

    iput-object v0, p0, Lnsq;->b:Lnsr;

    .line 2834
    :cond_2
    iget-object v0, p0, Lnsq;->b:Lnsr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2838
    :sswitch_2
    invoke-virtual {p1}, Loxn;->c()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lnsq;->d:Ljava/lang/Double;

    goto :goto_0

    .line 2842
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnsq;->e:Ljava/lang/Integer;

    goto :goto_0

    .line 2846
    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 2847
    iget-object v0, p0, Lnsq;->f:[Lnsh;

    if-nez v0, :cond_4

    move v0, v1

    .line 2848
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnsh;

    .line 2849
    iget-object v3, p0, Lnsq;->f:[Lnsh;

    if-eqz v3, :cond_3

    .line 2850
    iget-object v3, p0, Lnsq;->f:[Lnsh;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2852
    :cond_3
    iput-object v2, p0, Lnsq;->f:[Lnsh;

    .line 2853
    :goto_2
    iget-object v2, p0, Lnsq;->f:[Lnsh;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 2854
    iget-object v2, p0, Lnsq;->f:[Lnsh;

    new-instance v3, Lnsh;

    invoke-direct {v3}, Lnsh;-><init>()V

    aput-object v3, v2, v0

    .line 2855
    iget-object v2, p0, Lnsq;->f:[Lnsh;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 2856
    invoke-virtual {p1}, Loxn;->a()I

    .line 2853
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2847
    :cond_4
    iget-object v0, p0, Lnsq;->f:[Lnsh;

    array-length v0, v0

    goto :goto_1

    .line 2859
    :cond_5
    iget-object v2, p0, Lnsq;->f:[Lnsh;

    new-instance v3, Lnsh;

    invoke-direct {v3}, Lnsh;-><init>()V

    aput-object v3, v2, v0

    .line 2860
    iget-object v2, p0, Lnsq;->f:[Lnsh;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 2864
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnsq;->c:Ljava/lang/String;

    goto/16 :goto_0

    .line 2816
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x11 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 2755
    iget-object v0, p0, Lnsq;->b:Lnsr;

    if-eqz v0, :cond_0

    .line 2756
    const/4 v0, 0x1

    iget-object v1, p0, Lnsq;->b:Lnsr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2758
    :cond_0
    iget-object v0, p0, Lnsq;->d:Ljava/lang/Double;

    if-eqz v0, :cond_1

    .line 2759
    const/4 v0, 0x2

    iget-object v1, p0, Lnsq;->d:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(ID)V

    .line 2761
    :cond_1
    iget-object v0, p0, Lnsq;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 2762
    const/4 v0, 0x3

    iget-object v1, p0, Lnsq;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2764
    :cond_2
    iget-object v0, p0, Lnsq;->f:[Lnsh;

    if-eqz v0, :cond_4

    .line 2765
    iget-object v1, p0, Lnsq;->f:[Lnsh;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 2766
    if-eqz v3, :cond_3

    .line 2767
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 2765
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2771
    :cond_4
    iget-object v0, p0, Lnsq;->c:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 2772
    const/4 v0, 0x5

    iget-object v1, p0, Lnsq;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 2774
    :cond_5
    iget-object v0, p0, Lnsq;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2776
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2734
    invoke-virtual {p0, p1}, Lnsq;->a(Loxn;)Lnsq;

    move-result-object v0

    return-object v0
.end method
