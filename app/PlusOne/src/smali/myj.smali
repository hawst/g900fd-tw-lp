.class public final Lmyj;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmyk;

.field public b:Lmyi;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1564
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1567
    iput-object v0, p0, Lmyj;->a:Lmyk;

    .line 1570
    iput-object v0, p0, Lmyj;->b:Lmyi;

    .line 1564
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1587
    const/4 v0, 0x0

    .line 1588
    iget-object v1, p0, Lmyj;->a:Lmyk;

    if-eqz v1, :cond_0

    .line 1589
    const/4 v0, 0x1

    iget-object v1, p0, Lmyj;->a:Lmyk;

    .line 1590
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1592
    :cond_0
    iget-object v1, p0, Lmyj;->b:Lmyi;

    if-eqz v1, :cond_1

    .line 1593
    const/4 v1, 0x2

    iget-object v2, p0, Lmyj;->b:Lmyi;

    .line 1594
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1596
    :cond_1
    iget-object v1, p0, Lmyj;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1597
    iput v0, p0, Lmyj;->ai:I

    .line 1598
    return v0
.end method

.method public a(Loxn;)Lmyj;
    .locals 2

    .prologue
    .line 1606
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1607
    sparse-switch v0, :sswitch_data_0

    .line 1611
    iget-object v1, p0, Lmyj;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1612
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmyj;->ah:Ljava/util/List;

    .line 1615
    :cond_1
    iget-object v1, p0, Lmyj;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1617
    :sswitch_0
    return-object p0

    .line 1622
    :sswitch_1
    iget-object v0, p0, Lmyj;->a:Lmyk;

    if-nez v0, :cond_2

    .line 1623
    new-instance v0, Lmyk;

    invoke-direct {v0}, Lmyk;-><init>()V

    iput-object v0, p0, Lmyj;->a:Lmyk;

    .line 1625
    :cond_2
    iget-object v0, p0, Lmyj;->a:Lmyk;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1629
    :sswitch_2
    iget-object v0, p0, Lmyj;->b:Lmyi;

    if-nez v0, :cond_3

    .line 1630
    new-instance v0, Lmyi;

    invoke-direct {v0}, Lmyi;-><init>()V

    iput-object v0, p0, Lmyj;->b:Lmyi;

    .line 1632
    :cond_3
    iget-object v0, p0, Lmyj;->b:Lmyi;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1607
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1575
    iget-object v0, p0, Lmyj;->a:Lmyk;

    if-eqz v0, :cond_0

    .line 1576
    const/4 v0, 0x1

    iget-object v1, p0, Lmyj;->a:Lmyk;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1578
    :cond_0
    iget-object v0, p0, Lmyj;->b:Lmyi;

    if-eqz v0, :cond_1

    .line 1579
    const/4 v0, 0x2

    iget-object v1, p0, Lmyj;->b:Lmyi;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1581
    :cond_1
    iget-object v0, p0, Lmyj;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1583
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1560
    invoke-virtual {p0, p1}, Lmyj;->a(Loxn;)Lmyj;

    move-result-object v0

    return-object v0
.end method
