.class public Ljcs;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljdh;I)Lncm;
    .locals 6

    .prologue
    .line 22
    new-instance v0, Lncm;

    invoke-direct {v0}, Lncm;-><init>()V

    .line 23
    invoke-virtual {p5}, Ljdh;->h()Ljbh;

    move-result-object v1

    invoke-virtual {v1}, Ljbh;->a()Ljava/lang/String;

    move-result-object v1

    .line 24
    iput-object p2, v0, Lncm;->a:Ljava/lang/String;

    .line 25
    iput-object p3, v0, Lncm;->b:Ljava/lang/String;

    .line 26
    iput-object p4, v0, Lncm;->h:Ljava/lang/String;

    .line 27
    iput-object v1, v0, Lncm;->l:Ljava/lang/String;

    .line 28
    invoke-static {v1}, Ljbh;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 29
    iput-object v1, v0, Lncm;->g:Ljava/lang/String;

    .line 30
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    iput-object v2, v0, Lncm;->n:[Ljava/lang/String;

    .line 31
    invoke-virtual {p5}, Ljdh;->j()Ljava/lang/String;

    move-result-object v1

    .line 32
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 33
    iput-object v1, v0, Lncm;->o:Ljava/lang/String;

    .line 35
    :cond_0
    invoke-virtual {p5}, Ljdh;->b()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lncm;->e:Ljava/lang/Boolean;

    .line 36
    new-instance v1, Lncr;

    invoke-direct {v1}, Lncr;-><init>()V

    iput-object v1, v0, Lncm;->m:Lncr;

    .line 37
    iget-object v1, v0, Lncm;->m:Lncr;

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lncr;->a:Ljava/lang/Integer;

    .line 38
    invoke-virtual {p5}, Ljdh;->f()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lncm;->j:Ljava/lang/Long;

    .line 40
    invoke-virtual {p5}, Ljdh;->l()Lopf;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 41
    new-instance v1, Lnbi;

    invoke-direct {v1}, Lnbi;-><init>()V

    iput-object v1, v0, Lncm;->k:Lnbi;

    .line 42
    iget-object v1, v0, Lncm;->k:Lnbi;

    new-instance v2, Lnbu;

    invoke-direct {v2}, Lnbu;-><init>()V

    iput-object v2, v1, Lnbi;->b:Lnbu;

    .line 43
    iget-object v1, v0, Lncm;->k:Lnbi;

    iget-object v1, v1, Lnbi;->b:Lnbu;

    invoke-virtual {p5}, Ljdh;->l()Lopf;

    move-result-object v2

    iput-object v2, v1, Lnbu;->a:Lopf;

    .line 51
    :cond_1
    :goto_0
    invoke-virtual {p5}, Ljdh;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lncm;->f:Ljava/lang/String;

    .line 52
    return-object v0

    .line 44
    :cond_2
    invoke-virtual {p5}, Ljdh;->g()Landroid/net/Uri;

    move-result-object v1

    invoke-static {p1, v1}, Ljdv;->d(Landroid/content/Context;Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 45
    new-instance v1, Lnbi;

    invoke-direct {v1}, Lnbi;-><init>()V

    iput-object v1, v0, Lncm;->k:Lnbi;

    .line 46
    iget-object v1, v0, Lncm;->k:Lnbi;

    new-instance v2, Lnbu;

    invoke-direct {v2}, Lnbu;-><init>()V

    iput-object v2, v1, Lnbi;->b:Lnbu;

    .line 47
    iget-object v1, v0, Lncm;->k:Lnbi;

    iget-object v1, v1, Lnbi;->b:Lnbu;

    new-instance v2, Lopf;

    invoke-direct {v2}, Lopf;-><init>()V

    iput-object v2, v1, Lnbu;->a:Lopf;

    .line 48
    iget-object v1, v0, Lncm;->k:Lnbi;

    iget-object v1, v1, Lnbi;->b:Lnbu;

    iget-object v1, v1, Lnbu;->a:Lopf;

    const/16 v2, 0x8

    iput v2, v1, Lopf;->b:I

    goto :goto_0
.end method
