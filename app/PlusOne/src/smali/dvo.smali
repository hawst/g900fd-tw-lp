.class public final Ldvo;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Ldvo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:[Lnoy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 98
    new-instance v0, Ldvp;

    invoke-direct {v0}, Ldvp;-><init>()V

    sput-object v0, Ldvo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldvo;->a:Ljava/lang/String;

    .line 42
    const/4 v1, 0x0

    .line 44
    :try_start_0
    new-instance v0, Lnox;

    invoke-direct {v0}, Lnox;-><init>()V

    .line 45
    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v2

    .line 44
    invoke-static {v0, v2}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lnox;

    iget-object v0, v0, Lnox;->a:[Lnoy;
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    .line 50
    :goto_0
    iput-object v0, p0, Ldvo;->b:[Lnoy;

    .line 51
    return-void

    .line 47
    :catch_0
    move-exception v0

    const-string v0, "NotifSettingsCategory"

    const-string v2, "Unable to deserialize NotificationSettings. This should not happen since was serialized earlier by writeToParcel."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;[Lnoy;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Ldvo;->a:Ljava/lang/String;

    .line 37
    iput-object p2, p0, Ldvo;->b:[Lnoy;

    .line 38
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Ldvo;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a(I)Lnoy;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Ldvo;->b:[Lnoy;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Ldvo;->b:[Lnoy;

    array-length v0, v0

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 84
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Category: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 85
    iget-object v1, p0, Ldvo;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Settings: "

    .line 86
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ldvo;->b:[Lnoy;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 87
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 55
    iget-object v0, p0, Ldvo;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 56
    new-instance v0, Lnox;

    invoke-direct {v0}, Lnox;-><init>()V

    .line 57
    iget-object v1, p0, Ldvo;->b:[Lnoy;

    iput-object v1, v0, Lnox;->a:[Lnoy;

    .line 58
    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 59
    return-void
.end method
