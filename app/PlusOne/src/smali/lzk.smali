.class public final Llzk;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lojg;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10285
    invoke-direct {p0}, Loxq;-><init>()V

    .line 10288
    iput-object v0, p0, Llzk;->apiHeader:Llyq;

    .line 10291
    iput-object v0, p0, Llzk;->a:Lojg;

    .line 10285
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 10308
    const/4 v0, 0x0

    .line 10309
    iget-object v1, p0, Llzk;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 10310
    const/4 v0, 0x1

    iget-object v1, p0, Llzk;->apiHeader:Llyq;

    .line 10311
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 10313
    :cond_0
    iget-object v1, p0, Llzk;->a:Lojg;

    if-eqz v1, :cond_1

    .line 10314
    const/4 v1, 0x2

    iget-object v2, p0, Llzk;->a:Lojg;

    .line 10315
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10317
    :cond_1
    iget-object v1, p0, Llzk;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10318
    iput v0, p0, Llzk;->ai:I

    .line 10319
    return v0
.end method

.method public a(Loxn;)Llzk;
    .locals 2

    .prologue
    .line 10327
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 10328
    sparse-switch v0, :sswitch_data_0

    .line 10332
    iget-object v1, p0, Llzk;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 10333
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llzk;->ah:Ljava/util/List;

    .line 10336
    :cond_1
    iget-object v1, p0, Llzk;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 10338
    :sswitch_0
    return-object p0

    .line 10343
    :sswitch_1
    iget-object v0, p0, Llzk;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 10344
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Llzk;->apiHeader:Llyq;

    .line 10346
    :cond_2
    iget-object v0, p0, Llzk;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 10350
    :sswitch_2
    iget-object v0, p0, Llzk;->a:Lojg;

    if-nez v0, :cond_3

    .line 10351
    new-instance v0, Lojg;

    invoke-direct {v0}, Lojg;-><init>()V

    iput-object v0, p0, Llzk;->a:Lojg;

    .line 10353
    :cond_3
    iget-object v0, p0, Llzk;->a:Lojg;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 10328
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 10296
    iget-object v0, p0, Llzk;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 10297
    const/4 v0, 0x1

    iget-object v1, p0, Llzk;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 10299
    :cond_0
    iget-object v0, p0, Llzk;->a:Lojg;

    if-eqz v0, :cond_1

    .line 10300
    const/4 v0, 0x2

    iget-object v1, p0, Llzk;->a:Lojg;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 10302
    :cond_1
    iget-object v0, p0, Llzk;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 10304
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 10281
    invoke-virtual {p0, p1}, Llzk;->a(Loxn;)Llzk;

    move-result-object v0

    return-object v0
.end method
