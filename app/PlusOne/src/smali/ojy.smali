.class public final Lojy;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lohn;

.field public b:Ljava/lang/String;

.field private c:[Lohv;

.field private d:Loin;

.field private e:Ljava/lang/Integer;

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 196
    invoke-direct {p0}, Loxq;-><init>()V

    .line 199
    iput-object v1, p0, Lojy;->a:Lohn;

    .line 204
    sget-object v0, Lohv;->a:[Lohv;

    iput-object v0, p0, Lojy;->c:[Lohv;

    .line 207
    iput-object v1, p0, Lojy;->d:Loin;

    .line 196
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 244
    .line 245
    iget-object v0, p0, Lojy;->a:Lohn;

    if-eqz v0, :cond_6

    .line 246
    const/4 v0, 0x1

    iget-object v2, p0, Lojy;->a:Lohn;

    .line 247
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 249
    :goto_0
    iget-object v2, p0, Lojy;->b:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 250
    const/4 v2, 0x2

    iget-object v3, p0, Lojy;->b:Ljava/lang/String;

    .line 251
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 253
    :cond_0
    iget-object v2, p0, Lojy;->c:[Lohv;

    if-eqz v2, :cond_2

    .line 254
    iget-object v2, p0, Lojy;->c:[Lohv;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 255
    if-eqz v4, :cond_1

    .line 256
    const/4 v5, 0x3

    .line 257
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 254
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 261
    :cond_2
    iget-object v1, p0, Lojy;->d:Loin;

    if-eqz v1, :cond_3

    .line 262
    const/4 v1, 0x4

    iget-object v2, p0, Lojy;->d:Loin;

    .line 263
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 265
    :cond_3
    iget-object v1, p0, Lojy;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 266
    const/4 v1, 0x5

    iget-object v2, p0, Lojy;->e:Ljava/lang/Integer;

    .line 267
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 269
    :cond_4
    iget-object v1, p0, Lojy;->f:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 270
    const/4 v1, 0x6

    iget-object v2, p0, Lojy;->f:Ljava/lang/String;

    .line 271
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 273
    :cond_5
    iget-object v1, p0, Lojy;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 274
    iput v0, p0, Lojy;->ai:I

    .line 275
    return v0

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lojy;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 283
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 284
    sparse-switch v0, :sswitch_data_0

    .line 288
    iget-object v2, p0, Lojy;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 289
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lojy;->ah:Ljava/util/List;

    .line 292
    :cond_1
    iget-object v2, p0, Lojy;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 294
    :sswitch_0
    return-object p0

    .line 299
    :sswitch_1
    iget-object v0, p0, Lojy;->a:Lohn;

    if-nez v0, :cond_2

    .line 300
    new-instance v0, Lohn;

    invoke-direct {v0}, Lohn;-><init>()V

    iput-object v0, p0, Lojy;->a:Lohn;

    .line 302
    :cond_2
    iget-object v0, p0, Lojy;->a:Lohn;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 306
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lojy;->b:Ljava/lang/String;

    goto :goto_0

    .line 310
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 311
    iget-object v0, p0, Lojy;->c:[Lohv;

    if-nez v0, :cond_4

    move v0, v1

    .line 312
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lohv;

    .line 313
    iget-object v3, p0, Lojy;->c:[Lohv;

    if-eqz v3, :cond_3

    .line 314
    iget-object v3, p0, Lojy;->c:[Lohv;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 316
    :cond_3
    iput-object v2, p0, Lojy;->c:[Lohv;

    .line 317
    :goto_2
    iget-object v2, p0, Lojy;->c:[Lohv;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 318
    iget-object v2, p0, Lojy;->c:[Lohv;

    new-instance v3, Lohv;

    invoke-direct {v3}, Lohv;-><init>()V

    aput-object v3, v2, v0

    .line 319
    iget-object v2, p0, Lojy;->c:[Lohv;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 320
    invoke-virtual {p1}, Loxn;->a()I

    .line 317
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 311
    :cond_4
    iget-object v0, p0, Lojy;->c:[Lohv;

    array-length v0, v0

    goto :goto_1

    .line 323
    :cond_5
    iget-object v2, p0, Lojy;->c:[Lohv;

    new-instance v3, Lohv;

    invoke-direct {v3}, Lohv;-><init>()V

    aput-object v3, v2, v0

    .line 324
    iget-object v2, p0, Lojy;->c:[Lohv;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 328
    :sswitch_4
    iget-object v0, p0, Lojy;->d:Loin;

    if-nez v0, :cond_6

    .line 329
    new-instance v0, Loin;

    invoke-direct {v0}, Loin;-><init>()V

    iput-object v0, p0, Lojy;->d:Loin;

    .line 331
    :cond_6
    iget-object v0, p0, Lojy;->d:Loin;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 335
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lojy;->e:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 339
    :sswitch_6
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lojy;->f:Ljava/lang/String;

    goto/16 :goto_0

    .line 284
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 216
    iget-object v0, p0, Lojy;->a:Lohn;

    if-eqz v0, :cond_0

    .line 217
    const/4 v0, 0x1

    iget-object v1, p0, Lojy;->a:Lohn;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 219
    :cond_0
    iget-object v0, p0, Lojy;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 220
    const/4 v0, 0x2

    iget-object v1, p0, Lojy;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 222
    :cond_1
    iget-object v0, p0, Lojy;->c:[Lohv;

    if-eqz v0, :cond_3

    .line 223
    iget-object v1, p0, Lojy;->c:[Lohv;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 224
    if-eqz v3, :cond_2

    .line 225
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 223
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 229
    :cond_3
    iget-object v0, p0, Lojy;->d:Loin;

    if-eqz v0, :cond_4

    .line 230
    const/4 v0, 0x4

    iget-object v1, p0, Lojy;->d:Loin;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 232
    :cond_4
    iget-object v0, p0, Lojy;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 233
    const/4 v0, 0x5

    iget-object v1, p0, Lojy;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 235
    :cond_5
    iget-object v0, p0, Lojy;->f:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 236
    const/4 v0, 0x6

    iget-object v1, p0, Lojy;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 238
    :cond_6
    iget-object v0, p0, Lojy;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 240
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 192
    invoke-virtual {p0, p1}, Lojy;->a(Loxn;)Lojy;

    move-result-object v0

    return-object v0
.end method
