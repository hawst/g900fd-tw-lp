.class public final Ldvq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Ldvq;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:I

.field private final c:[Ldvo;

.field private d:Lhgw;

.field private final e:[I

.field private final f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 282
    new-instance v0, Ldvr;

    invoke-direct {v0}, Ldvr;-><init>()V

    sput-object v0, Ldvq;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 141
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 142
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldvq;->a:Ljava/lang/String;

    .line 143
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Ldvq;->b:I

    .line 144
    sget-object v0, Ldvo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldvo;

    iput-object v0, p0, Ldvq;->c:[Ldvo;

    .line 145
    const-class v0, Lhgw;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lhgw;

    iput-object v0, p0, Ldvq;->d:Lhgw;

    .line 146
    invoke-virtual {p1}, Landroid/os/Parcel;->createIntArray()[I

    move-result-object v0

    iput-object v0, p0, Ldvq;->e:[I

    .line 147
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldvq;->f:Ljava/lang/String;

    .line 148
    return-void
.end method

.method constructor <init>(Ljava/lang/String;I[Ldvo;Lhgw;[ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 133
    iput-object p1, p0, Ldvq;->a:Ljava/lang/String;

    .line 134
    iput p2, p0, Ldvq;->b:I

    .line 135
    iput-object p3, p0, Ldvq;->c:[Ldvo;

    .line 136
    iput-object p4, p0, Ldvq;->d:Lhgw;

    .line 137
    iput-object p5, p0, Ldvq;->e:[I

    .line 138
    iput-object p6, p0, Ldvq;->f:Ljava/lang/String;

    .line 139
    return-void
.end method

.method static synthetic a(Landroid/content/Context;ILocf;)Lhgw;
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 25
    if-nez p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0, p1, p2, v0, v0}, Lhft;->a(Landroid/content/Context;ILocf;ZZ)Lhgw;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Locf;)[I
    .locals 6

    .prologue
    .line 25
    if-eqz p0, :cond_1

    iget-object v1, p0, Locf;->b:[Locb;

    if-eqz v1, :cond_1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    iget v4, v3, Locb;->b:I

    const/4 v5, 0x7

    if-ne v4, v5, :cond_0

    iget-object v0, v3, Locb;->c:[I

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public a(I)Ldvo;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Ldvq;->c:[Ldvo;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Ldvq;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 171
    iget v0, p0, Ldvq;->b:I

    return v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Ldvq;->c:[Ldvo;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Ldvq;->c:[Ldvo;

    array-length v0, v0

    goto :goto_0
.end method

.method public d()[Ldvo;
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Ldvq;->c:[Ldvo;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 230
    const/4 v0, 0x0

    return v0
.end method

.method public e()Lhgw;
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Ldvq;->d:Lhgw;

    return-object v0
.end method

.method public f()[I
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Ldvq;->e:[I

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Ldvq;->f:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Ldvq;->c:[Ldvo;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 152
    iget-object v0, p0, Ldvq;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 153
    iget v0, p0, Ldvq;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 154
    iget-object v0, p0, Ldvq;->c:[Ldvo;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    .line 155
    iget-object v0, p0, Ldvq;->d:Lhgw;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 156
    iget-object v0, p0, Ldvq;->e:[I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeIntArray([I)V

    .line 157
    iget-object v0, p0, Ldvq;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 158
    return-void
.end method
