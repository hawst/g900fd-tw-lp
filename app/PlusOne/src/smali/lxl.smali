.class public final Llxl;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Llxl;


# instance fields
.field private b:Llxp;

.field private c:Ljava/lang/String;

.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3029
    const/4 v0, 0x0

    new-array v0, v0, [Llxl;

    sput-object v0, Llxl;->a:[Llxl;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3030
    invoke-direct {p0}, Loxq;-><init>()V

    .line 3043
    const/4 v0, 0x0

    iput-object v0, p0, Llxl;->b:Llxp;

    .line 3048
    const/high16 v0, -0x80000000

    iput v0, p0, Llxl;->d:I

    .line 3030
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 3068
    const/4 v0, 0x0

    .line 3069
    iget-object v1, p0, Llxl;->b:Llxp;

    if-eqz v1, :cond_0

    .line 3070
    const/4 v0, 0x1

    iget-object v1, p0, Llxl;->b:Llxp;

    .line 3071
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3073
    :cond_0
    iget-object v1, p0, Llxl;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 3074
    const/4 v1, 0x2

    iget-object v2, p0, Llxl;->c:Ljava/lang/String;

    .line 3075
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3077
    :cond_1
    iget v1, p0, Llxl;->d:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_2

    .line 3078
    const/4 v1, 0x3

    iget v2, p0, Llxl;->d:I

    .line 3079
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3081
    :cond_2
    iget-object v1, p0, Llxl;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3082
    iput v0, p0, Llxl;->ai:I

    .line 3083
    return v0
.end method

.method public a(Loxn;)Llxl;
    .locals 3

    .prologue
    const/4 v2, 0x6

    .line 3091
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3092
    sparse-switch v0, :sswitch_data_0

    .line 3096
    iget-object v1, p0, Llxl;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 3097
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llxl;->ah:Ljava/util/List;

    .line 3100
    :cond_1
    iget-object v1, p0, Llxl;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3102
    :sswitch_0
    return-object p0

    .line 3107
    :sswitch_1
    iget-object v0, p0, Llxl;->b:Llxp;

    if-nez v0, :cond_2

    .line 3108
    new-instance v0, Llxp;

    invoke-direct {v0}, Llxp;-><init>()V

    iput-object v0, p0, Llxl;->b:Llxp;

    .line 3110
    :cond_2
    iget-object v0, p0, Llxl;->b:Llxp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3114
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llxl;->c:Ljava/lang/String;

    goto :goto_0

    .line 3118
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 3119
    if-eq v0, v2, :cond_3

    if-eqz v0, :cond_3

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    const/4 v1, 0x4

    if-eq v0, v1, :cond_3

    const/4 v1, 0x5

    if-ne v0, v1, :cond_4

    .line 3126
    :cond_3
    iput v0, p0, Llxl;->d:I

    goto :goto_0

    .line 3128
    :cond_4
    iput v2, p0, Llxl;->d:I

    goto :goto_0

    .line 3092
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 3053
    iget-object v0, p0, Llxl;->b:Llxp;

    if-eqz v0, :cond_0

    .line 3054
    const/4 v0, 0x1

    iget-object v1, p0, Llxl;->b:Llxp;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 3056
    :cond_0
    iget-object v0, p0, Llxl;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 3057
    const/4 v0, 0x2

    iget-object v1, p0, Llxl;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3059
    :cond_1
    iget v0, p0, Llxl;->d:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_2

    .line 3060
    const/4 v0, 0x3

    iget v1, p0, Llxl;->d:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 3062
    :cond_2
    iget-object v0, p0, Llxl;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3064
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3026
    invoke-virtual {p0, p1}, Llxl;->a(Loxn;)Llxl;

    move-result-object v0

    return-object v0
.end method
