.class public final Lluk;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lluk;


# instance fields
.field public b:Llus;

.field public c:Ljava/lang/Long;

.field public d:Llul;

.field private e:[Lluu;

.field private f:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const/4 v0, 0x0

    new-array v0, v0, [Lluk;

    sput-object v0, Lluk;->a:[Lluk;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 9
    invoke-direct {p0}, Loxq;-><init>()V

    .line 12
    iput-object v0, p0, Lluk;->b:Llus;

    .line 17
    iput-object v0, p0, Lluk;->d:Llul;

    .line 20
    sget-object v0, Lluu;->a:[Lluu;

    iput-object v0, p0, Lluk;->e:[Lluu;

    .line 23
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lluk;->f:[Ljava/lang/String;

    .line 9
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 55
    .line 56
    iget-object v0, p0, Lluk;->b:Llus;

    if-eqz v0, :cond_6

    .line 57
    const/4 v0, 0x1

    iget-object v2, p0, Lluk;->b:Llus;

    .line 58
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 60
    :goto_0
    iget-object v2, p0, Lluk;->c:Ljava/lang/Long;

    if-eqz v2, :cond_0

    .line 61
    const/4 v2, 0x2

    iget-object v3, p0, Lluk;->c:Ljava/lang/Long;

    .line 62
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Loxo;->e(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 64
    :cond_0
    iget-object v2, p0, Lluk;->d:Llul;

    if-eqz v2, :cond_1

    .line 65
    const/4 v2, 0x3

    iget-object v3, p0, Lluk;->d:Llul;

    .line 66
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 68
    :cond_1
    iget-object v2, p0, Lluk;->e:[Lluu;

    if-eqz v2, :cond_3

    .line 69
    iget-object v3, p0, Lluk;->e:[Lluu;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_3

    aget-object v5, v3, v2

    .line 70
    if-eqz v5, :cond_2

    .line 71
    const/4 v6, 0x4

    .line 72
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 69
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 76
    :cond_3
    iget-object v2, p0, Lluk;->f:[Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lluk;->f:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_5

    .line 78
    iget-object v3, p0, Lluk;->f:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v1, v4, :cond_4

    aget-object v5, v3, v1

    .line 80
    invoke-static {v5}, Loxo;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 78
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 82
    :cond_4
    add-int/2addr v0, v2

    .line 83
    iget-object v1, p0, Lluk;->f:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 85
    :cond_5
    iget-object v1, p0, Lluk;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 86
    iput v0, p0, Lluk;->ai:I

    .line 87
    return v0

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lluk;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 95
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 96
    sparse-switch v0, :sswitch_data_0

    .line 100
    iget-object v2, p0, Lluk;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 101
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lluk;->ah:Ljava/util/List;

    .line 104
    :cond_1
    iget-object v2, p0, Lluk;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 106
    :sswitch_0
    return-object p0

    .line 111
    :sswitch_1
    iget-object v0, p0, Lluk;->b:Llus;

    if-nez v0, :cond_2

    .line 112
    new-instance v0, Llus;

    invoke-direct {v0}, Llus;-><init>()V

    iput-object v0, p0, Lluk;->b:Llus;

    .line 114
    :cond_2
    iget-object v0, p0, Lluk;->b:Llus;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 118
    :sswitch_2
    invoke-virtual {p1}, Loxn;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lluk;->c:Ljava/lang/Long;

    goto :goto_0

    .line 122
    :sswitch_3
    iget-object v0, p0, Lluk;->d:Llul;

    if-nez v0, :cond_3

    .line 123
    new-instance v0, Llul;

    invoke-direct {v0}, Llul;-><init>()V

    iput-object v0, p0, Lluk;->d:Llul;

    .line 125
    :cond_3
    iget-object v0, p0, Lluk;->d:Llul;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 129
    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 130
    iget-object v0, p0, Lluk;->e:[Lluu;

    if-nez v0, :cond_5

    move v0, v1

    .line 131
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lluu;

    .line 132
    iget-object v3, p0, Lluk;->e:[Lluu;

    if-eqz v3, :cond_4

    .line 133
    iget-object v3, p0, Lluk;->e:[Lluu;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 135
    :cond_4
    iput-object v2, p0, Lluk;->e:[Lluu;

    .line 136
    :goto_2
    iget-object v2, p0, Lluk;->e:[Lluu;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 137
    iget-object v2, p0, Lluk;->e:[Lluu;

    new-instance v3, Lluu;

    invoke-direct {v3}, Lluu;-><init>()V

    aput-object v3, v2, v0

    .line 138
    iget-object v2, p0, Lluk;->e:[Lluu;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 139
    invoke-virtual {p1}, Loxn;->a()I

    .line 136
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 130
    :cond_5
    iget-object v0, p0, Lluk;->e:[Lluu;

    array-length v0, v0

    goto :goto_1

    .line 142
    :cond_6
    iget-object v2, p0, Lluk;->e:[Lluu;

    new-instance v3, Lluu;

    invoke-direct {v3}, Lluu;-><init>()V

    aput-object v3, v2, v0

    .line 143
    iget-object v2, p0, Lluk;->e:[Lluu;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 147
    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 148
    iget-object v0, p0, Lluk;->f:[Ljava/lang/String;

    array-length v0, v0

    .line 149
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 150
    iget-object v3, p0, Lluk;->f:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 151
    iput-object v2, p0, Lluk;->f:[Ljava/lang/String;

    .line 152
    :goto_3
    iget-object v2, p0, Lluk;->f:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    .line 153
    iget-object v2, p0, Lluk;->f:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 154
    invoke-virtual {p1}, Loxn;->a()I

    .line 152
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 157
    :cond_7
    iget-object v2, p0, Lluk;->f:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    .line 96
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 28
    iget-object v1, p0, Lluk;->b:Llus;

    if-eqz v1, :cond_0

    .line 29
    const/4 v1, 0x1

    iget-object v2, p0, Lluk;->b:Llus;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 31
    :cond_0
    iget-object v1, p0, Lluk;->c:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 32
    const/4 v1, 0x2

    iget-object v2, p0, Lluk;->c:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Loxo;->a(IJ)V

    .line 34
    :cond_1
    iget-object v1, p0, Lluk;->d:Llul;

    if-eqz v1, :cond_2

    .line 35
    const/4 v1, 0x3

    iget-object v2, p0, Lluk;->d:Llul;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 37
    :cond_2
    iget-object v1, p0, Lluk;->e:[Lluu;

    if-eqz v1, :cond_4

    .line 38
    iget-object v2, p0, Lluk;->e:[Lluu;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 39
    if-eqz v4, :cond_3

    .line 40
    const/4 v5, 0x4

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 38
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 44
    :cond_4
    iget-object v1, p0, Lluk;->f:[Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 45
    iget-object v1, p0, Lluk;->f:[Ljava/lang/String;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    .line 46
    const/4 v4, 0x5

    invoke-virtual {p1, v4, v3}, Loxo;->a(ILjava/lang/String;)V

    .line 45
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 49
    :cond_5
    iget-object v0, p0, Lluk;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 51
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Lluk;->a(Loxn;)Lluk;

    move-result-object v0

    return-object v0
.end method
