.class public final Lpcd;
.super Loxq;
.source "PG"


# static fields
.field public static final a:Loxr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Loxr",
            "<",
            "Lpcd;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:[Loya;

.field public f:Ljava/lang/Long;

.field public g:Ljava/lang/Boolean;

.field public h:Ljava/lang/String;

.field public i:Loya;

.field public j:I

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Lpdi;

.field private p:Ljava/lang/String;

.field private q:[Loya;

.field private r:Loya;

.field private s:[Loya;

.field private t:Ljava/lang/String;

.field private u:[Loya;

.field private v:Ljava/lang/String;

.field private w:Loya;

.field private x:Ljava/lang/String;

.field private y:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 9
    const v0, 0x2d49205

    new-instance v1, Lpce;

    invoke-direct {v1}, Lpce;-><init>()V

    .line 14
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    move-result-object v0

    sput-object v0, Lpcd;->a:Loxr;

    .line 13
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 27
    iput-object v1, p0, Lpcd;->o:Lpdi;

    .line 32
    sget-object v0, Loya;->a:[Loya;

    iput-object v0, p0, Lpcd;->q:[Loya;

    .line 35
    iput-object v1, p0, Lpcd;->r:Loya;

    .line 38
    sget-object v0, Loya;->a:[Loya;

    iput-object v0, p0, Lpcd;->s:[Loya;

    .line 45
    sget-object v0, Loya;->a:[Loya;

    iput-object v0, p0, Lpcd;->u:[Loya;

    .line 52
    iput-object v1, p0, Lpcd;->w:Loya;

    .line 55
    sget-object v0, Loya;->a:[Loya;

    iput-object v0, p0, Lpcd;->e:[Loya;

    .line 66
    iput-object v1, p0, Lpcd;->i:Loya;

    .line 69
    const/high16 v0, -0x80000000

    iput v0, p0, Lpcd;->j:I

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 170
    .line 171
    iget-object v0, p0, Lpcd;->k:Ljava/lang/String;

    if-eqz v0, :cond_1b

    .line 172
    const/4 v0, 0x1

    iget-object v2, p0, Lpcd;->k:Ljava/lang/String;

    .line 173
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 175
    :goto_0
    iget-object v2, p0, Lpcd;->l:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 176
    const/4 v2, 0x2

    iget-object v3, p0, Lpcd;->l:Ljava/lang/String;

    .line 177
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 179
    :cond_0
    iget-object v2, p0, Lpcd;->b:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 180
    const/4 v2, 0x3

    iget-object v3, p0, Lpcd;->b:Ljava/lang/String;

    .line 181
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 183
    :cond_1
    iget-object v2, p0, Lpcd;->m:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 184
    const/4 v2, 0x4

    iget-object v3, p0, Lpcd;->m:Ljava/lang/String;

    .line 185
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 187
    :cond_2
    iget-object v2, p0, Lpcd;->n:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 188
    const/4 v2, 0x5

    iget-object v3, p0, Lpcd;->n:Ljava/lang/String;

    .line 189
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 191
    :cond_3
    iget-object v2, p0, Lpcd;->o:Lpdi;

    if-eqz v2, :cond_4

    .line 192
    const/4 v2, 0x6

    iget-object v3, p0, Lpcd;->o:Lpdi;

    .line 193
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 195
    :cond_4
    iget-object v2, p0, Lpcd;->p:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 196
    const/4 v2, 0x7

    iget-object v3, p0, Lpcd;->p:Ljava/lang/String;

    .line 197
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 199
    :cond_5
    iget-object v2, p0, Lpcd;->q:[Loya;

    if-eqz v2, :cond_7

    .line 200
    iget-object v3, p0, Lpcd;->q:[Loya;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_7

    aget-object v5, v3, v2

    .line 201
    if-eqz v5, :cond_6

    .line 202
    const/16 v6, 0x8

    .line 203
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 200
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 207
    :cond_7
    iget-object v2, p0, Lpcd;->r:Loya;

    if-eqz v2, :cond_8

    .line 208
    const/16 v2, 0x9

    iget-object v3, p0, Lpcd;->r:Loya;

    .line 209
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 211
    :cond_8
    iget-object v2, p0, Lpcd;->s:[Loya;

    if-eqz v2, :cond_a

    .line 212
    iget-object v3, p0, Lpcd;->s:[Loya;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_a

    aget-object v5, v3, v2

    .line 213
    if-eqz v5, :cond_9

    .line 214
    const/16 v6, 0xb

    .line 215
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 212
    :cond_9
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 219
    :cond_a
    iget-object v2, p0, Lpcd;->t:Ljava/lang/String;

    if-eqz v2, :cond_b

    .line 220
    const/16 v2, 0xc

    iget-object v3, p0, Lpcd;->t:Ljava/lang/String;

    .line 221
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 223
    :cond_b
    iget-object v2, p0, Lpcd;->c:Ljava/lang/String;

    if-eqz v2, :cond_c

    .line 224
    const/16 v2, 0x1b

    iget-object v3, p0, Lpcd;->c:Ljava/lang/String;

    .line 225
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 227
    :cond_c
    iget-object v2, p0, Lpcd;->u:[Loya;

    if-eqz v2, :cond_e

    .line 228
    iget-object v3, p0, Lpcd;->u:[Loya;

    array-length v4, v3

    move v2, v1

    :goto_3
    if-ge v2, v4, :cond_e

    aget-object v5, v3, v2

    .line 229
    if-eqz v5, :cond_d

    .line 230
    const/16 v6, 0x2a

    .line 231
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 228
    :cond_d
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 235
    :cond_e
    iget-object v2, p0, Lpcd;->v:Ljava/lang/String;

    if-eqz v2, :cond_f

    .line 236
    const/16 v2, 0x4b

    iget-object v3, p0, Lpcd;->v:Ljava/lang/String;

    .line 237
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 239
    :cond_f
    iget-object v2, p0, Lpcd;->d:Ljava/lang/String;

    if-eqz v2, :cond_10

    .line 240
    const/16 v2, 0x6a

    iget-object v3, p0, Lpcd;->d:Ljava/lang/String;

    .line 241
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 243
    :cond_10
    iget-object v2, p0, Lpcd;->w:Loya;

    if-eqz v2, :cond_11

    .line 244
    const/16 v2, 0xb9

    iget-object v3, p0, Lpcd;->w:Loya;

    .line 245
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 247
    :cond_11
    iget-object v2, p0, Lpcd;->e:[Loya;

    if-eqz v2, :cond_13

    .line 248
    iget-object v2, p0, Lpcd;->e:[Loya;

    array-length v3, v2

    :goto_4
    if-ge v1, v3, :cond_13

    aget-object v4, v2, v1

    .line 249
    if-eqz v4, :cond_12

    .line 250
    const/16 v5, 0xc8

    .line 251
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 248
    :cond_12
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 255
    :cond_13
    iget-object v1, p0, Lpcd;->f:Ljava/lang/Long;

    if-eqz v1, :cond_14

    .line 256
    const/16 v1, 0xcb

    iget-object v2, p0, Lpcd;->f:Ljava/lang/Long;

    .line 257
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 259
    :cond_14
    iget-object v1, p0, Lpcd;->g:Ljava/lang/Boolean;

    if-eqz v1, :cond_15

    .line 260
    const/16 v1, 0xd0

    iget-object v2, p0, Lpcd;->g:Ljava/lang/Boolean;

    .line 261
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 263
    :cond_15
    iget-object v1, p0, Lpcd;->x:Ljava/lang/String;

    if-eqz v1, :cond_16

    .line 264
    const/16 v1, 0xfe

    iget-object v2, p0, Lpcd;->x:Ljava/lang/String;

    .line 265
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 267
    :cond_16
    iget-object v1, p0, Lpcd;->h:Ljava/lang/String;

    if-eqz v1, :cond_17

    .line 268
    const/16 v1, 0x100

    iget-object v2, p0, Lpcd;->h:Ljava/lang/String;

    .line 269
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 271
    :cond_17
    iget-object v1, p0, Lpcd;->i:Loya;

    if-eqz v1, :cond_18

    .line 272
    const/16 v1, 0x10c

    iget-object v2, p0, Lpcd;->i:Loya;

    .line 273
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 275
    :cond_18
    iget v1, p0, Lpcd;->j:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_19

    .line 276
    const/16 v1, 0x10d

    iget v2, p0, Lpcd;->j:I

    .line 277
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 279
    :cond_19
    iget-object v1, p0, Lpcd;->y:Ljava/lang/String;

    if-eqz v1, :cond_1a

    .line 280
    const/16 v1, 0x10e

    iget-object v2, p0, Lpcd;->y:Ljava/lang/String;

    .line 281
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 283
    :cond_1a
    iget-object v1, p0, Lpcd;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 284
    iput v0, p0, Lpcd;->ai:I

    .line 285
    return v0

    :cond_1b
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lpcd;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 293
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 294
    sparse-switch v0, :sswitch_data_0

    .line 298
    iget-object v2, p0, Lpcd;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 299
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lpcd;->ah:Ljava/util/List;

    .line 302
    :cond_1
    iget-object v2, p0, Lpcd;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 304
    :sswitch_0
    return-object p0

    .line 309
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpcd;->k:Ljava/lang/String;

    goto :goto_0

    .line 313
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpcd;->l:Ljava/lang/String;

    goto :goto_0

    .line 317
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpcd;->b:Ljava/lang/String;

    goto :goto_0

    .line 321
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpcd;->m:Ljava/lang/String;

    goto :goto_0

    .line 325
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpcd;->n:Ljava/lang/String;

    goto :goto_0

    .line 329
    :sswitch_6
    iget-object v0, p0, Lpcd;->o:Lpdi;

    if-nez v0, :cond_2

    .line 330
    new-instance v0, Lpdi;

    invoke-direct {v0}, Lpdi;-><init>()V

    iput-object v0, p0, Lpcd;->o:Lpdi;

    .line 332
    :cond_2
    iget-object v0, p0, Lpcd;->o:Lpdi;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 336
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpcd;->p:Ljava/lang/String;

    goto :goto_0

    .line 340
    :sswitch_8
    const/16 v0, 0x42

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 341
    iget-object v0, p0, Lpcd;->q:[Loya;

    if-nez v0, :cond_4

    move v0, v1

    .line 342
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Loya;

    .line 343
    iget-object v3, p0, Lpcd;->q:[Loya;

    if-eqz v3, :cond_3

    .line 344
    iget-object v3, p0, Lpcd;->q:[Loya;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 346
    :cond_3
    iput-object v2, p0, Lpcd;->q:[Loya;

    .line 347
    :goto_2
    iget-object v2, p0, Lpcd;->q:[Loya;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 348
    iget-object v2, p0, Lpcd;->q:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 349
    iget-object v2, p0, Lpcd;->q:[Loya;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 350
    invoke-virtual {p1}, Loxn;->a()I

    .line 347
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 341
    :cond_4
    iget-object v0, p0, Lpcd;->q:[Loya;

    array-length v0, v0

    goto :goto_1

    .line 353
    :cond_5
    iget-object v2, p0, Lpcd;->q:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 354
    iget-object v2, p0, Lpcd;->q:[Loya;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 358
    :sswitch_9
    iget-object v0, p0, Lpcd;->r:Loya;

    if-nez v0, :cond_6

    .line 359
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    iput-object v0, p0, Lpcd;->r:Loya;

    .line 361
    :cond_6
    iget-object v0, p0, Lpcd;->r:Loya;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 365
    :sswitch_a
    const/16 v0, 0x5a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 366
    iget-object v0, p0, Lpcd;->s:[Loya;

    if-nez v0, :cond_8

    move v0, v1

    .line 367
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Loya;

    .line 368
    iget-object v3, p0, Lpcd;->s:[Loya;

    if-eqz v3, :cond_7

    .line 369
    iget-object v3, p0, Lpcd;->s:[Loya;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 371
    :cond_7
    iput-object v2, p0, Lpcd;->s:[Loya;

    .line 372
    :goto_4
    iget-object v2, p0, Lpcd;->s:[Loya;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    .line 373
    iget-object v2, p0, Lpcd;->s:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 374
    iget-object v2, p0, Lpcd;->s:[Loya;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 375
    invoke-virtual {p1}, Loxn;->a()I

    .line 372
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 366
    :cond_8
    iget-object v0, p0, Lpcd;->s:[Loya;

    array-length v0, v0

    goto :goto_3

    .line 378
    :cond_9
    iget-object v2, p0, Lpcd;->s:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 379
    iget-object v2, p0, Lpcd;->s:[Loya;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 383
    :sswitch_b
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpcd;->t:Ljava/lang/String;

    goto/16 :goto_0

    .line 387
    :sswitch_c
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpcd;->c:Ljava/lang/String;

    goto/16 :goto_0

    .line 391
    :sswitch_d
    const/16 v0, 0x152

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 392
    iget-object v0, p0, Lpcd;->u:[Loya;

    if-nez v0, :cond_b

    move v0, v1

    .line 393
    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Loya;

    .line 394
    iget-object v3, p0, Lpcd;->u:[Loya;

    if-eqz v3, :cond_a

    .line 395
    iget-object v3, p0, Lpcd;->u:[Loya;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 397
    :cond_a
    iput-object v2, p0, Lpcd;->u:[Loya;

    .line 398
    :goto_6
    iget-object v2, p0, Lpcd;->u:[Loya;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_c

    .line 399
    iget-object v2, p0, Lpcd;->u:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 400
    iget-object v2, p0, Lpcd;->u:[Loya;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 401
    invoke-virtual {p1}, Loxn;->a()I

    .line 398
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 392
    :cond_b
    iget-object v0, p0, Lpcd;->u:[Loya;

    array-length v0, v0

    goto :goto_5

    .line 404
    :cond_c
    iget-object v2, p0, Lpcd;->u:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 405
    iget-object v2, p0, Lpcd;->u:[Loya;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 409
    :sswitch_e
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpcd;->v:Ljava/lang/String;

    goto/16 :goto_0

    .line 413
    :sswitch_f
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpcd;->d:Ljava/lang/String;

    goto/16 :goto_0

    .line 417
    :sswitch_10
    iget-object v0, p0, Lpcd;->w:Loya;

    if-nez v0, :cond_d

    .line 418
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    iput-object v0, p0, Lpcd;->w:Loya;

    .line 420
    :cond_d
    iget-object v0, p0, Lpcd;->w:Loya;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 424
    :sswitch_11
    const/16 v0, 0x642

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 425
    iget-object v0, p0, Lpcd;->e:[Loya;

    if-nez v0, :cond_f

    move v0, v1

    .line 426
    :goto_7
    add-int/2addr v2, v0

    new-array v2, v2, [Loya;

    .line 427
    iget-object v3, p0, Lpcd;->e:[Loya;

    if-eqz v3, :cond_e

    .line 428
    iget-object v3, p0, Lpcd;->e:[Loya;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 430
    :cond_e
    iput-object v2, p0, Lpcd;->e:[Loya;

    .line 431
    :goto_8
    iget-object v2, p0, Lpcd;->e:[Loya;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_10

    .line 432
    iget-object v2, p0, Lpcd;->e:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 433
    iget-object v2, p0, Lpcd;->e:[Loya;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 434
    invoke-virtual {p1}, Loxn;->a()I

    .line 431
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 425
    :cond_f
    iget-object v0, p0, Lpcd;->e:[Loya;

    array-length v0, v0

    goto :goto_7

    .line 437
    :cond_10
    iget-object v2, p0, Lpcd;->e:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 438
    iget-object v2, p0, Lpcd;->e:[Loya;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 442
    :sswitch_12
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lpcd;->f:Ljava/lang/Long;

    goto/16 :goto_0

    .line 446
    :sswitch_13
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lpcd;->g:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 450
    :sswitch_14
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpcd;->x:Ljava/lang/String;

    goto/16 :goto_0

    .line 454
    :sswitch_15
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpcd;->h:Ljava/lang/String;

    goto/16 :goto_0

    .line 458
    :sswitch_16
    iget-object v0, p0, Lpcd;->i:Loya;

    if-nez v0, :cond_11

    .line 459
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    iput-object v0, p0, Lpcd;->i:Loya;

    .line 461
    :cond_11
    iget-object v0, p0, Lpcd;->i:Loya;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 465
    :sswitch_17
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 466
    if-eqz v0, :cond_12

    const/4 v2, 0x1

    if-eq v0, v2, :cond_12

    const/4 v2, 0x2

    if-ne v0, v2, :cond_13

    .line 469
    :cond_12
    iput v0, p0, Lpcd;->j:I

    goto/16 :goto_0

    .line 471
    :cond_13
    iput v1, p0, Lpcd;->j:I

    goto/16 :goto_0

    .line 476
    :sswitch_18
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpcd;->y:Ljava/lang/String;

    goto/16 :goto_0

    .line 294
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x5a -> :sswitch_a
        0x62 -> :sswitch_b
        0xda -> :sswitch_c
        0x152 -> :sswitch_d
        0x25a -> :sswitch_e
        0x352 -> :sswitch_f
        0x5ca -> :sswitch_10
        0x642 -> :sswitch_11
        0x658 -> :sswitch_12
        0x680 -> :sswitch_13
        0x7f2 -> :sswitch_14
        0x802 -> :sswitch_15
        0x862 -> :sswitch_16
        0x868 -> :sswitch_17
        0x872 -> :sswitch_18
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 76
    iget-object v1, p0, Lpcd;->k:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 77
    const/4 v1, 0x1

    iget-object v2, p0, Lpcd;->k:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 79
    :cond_0
    iget-object v1, p0, Lpcd;->l:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 80
    const/4 v1, 0x2

    iget-object v2, p0, Lpcd;->l:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 82
    :cond_1
    iget-object v1, p0, Lpcd;->b:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 83
    const/4 v1, 0x3

    iget-object v2, p0, Lpcd;->b:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 85
    :cond_2
    iget-object v1, p0, Lpcd;->m:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 86
    const/4 v1, 0x4

    iget-object v2, p0, Lpcd;->m:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 88
    :cond_3
    iget-object v1, p0, Lpcd;->n:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 89
    const/4 v1, 0x5

    iget-object v2, p0, Lpcd;->n:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 91
    :cond_4
    iget-object v1, p0, Lpcd;->o:Lpdi;

    if-eqz v1, :cond_5

    .line 92
    const/4 v1, 0x6

    iget-object v2, p0, Lpcd;->o:Lpdi;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 94
    :cond_5
    iget-object v1, p0, Lpcd;->p:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 95
    const/4 v1, 0x7

    iget-object v2, p0, Lpcd;->p:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 97
    :cond_6
    iget-object v1, p0, Lpcd;->q:[Loya;

    if-eqz v1, :cond_8

    .line 98
    iget-object v2, p0, Lpcd;->q:[Loya;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_8

    aget-object v4, v2, v1

    .line 99
    if-eqz v4, :cond_7

    .line 100
    const/16 v5, 0x8

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 98
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 104
    :cond_8
    iget-object v1, p0, Lpcd;->r:Loya;

    if-eqz v1, :cond_9

    .line 105
    const/16 v1, 0x9

    iget-object v2, p0, Lpcd;->r:Loya;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 107
    :cond_9
    iget-object v1, p0, Lpcd;->s:[Loya;

    if-eqz v1, :cond_b

    .line 108
    iget-object v2, p0, Lpcd;->s:[Loya;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_b

    aget-object v4, v2, v1

    .line 109
    if-eqz v4, :cond_a

    .line 110
    const/16 v5, 0xb

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 108
    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 114
    :cond_b
    iget-object v1, p0, Lpcd;->t:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 115
    const/16 v1, 0xc

    iget-object v2, p0, Lpcd;->t:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 117
    :cond_c
    iget-object v1, p0, Lpcd;->c:Ljava/lang/String;

    if-eqz v1, :cond_d

    .line 118
    const/16 v1, 0x1b

    iget-object v2, p0, Lpcd;->c:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 120
    :cond_d
    iget-object v1, p0, Lpcd;->u:[Loya;

    if-eqz v1, :cond_f

    .line 121
    iget-object v2, p0, Lpcd;->u:[Loya;

    array-length v3, v2

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_f

    aget-object v4, v2, v1

    .line 122
    if-eqz v4, :cond_e

    .line 123
    const/16 v5, 0x2a

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 121
    :cond_e
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 127
    :cond_f
    iget-object v1, p0, Lpcd;->v:Ljava/lang/String;

    if-eqz v1, :cond_10

    .line 128
    const/16 v1, 0x4b

    iget-object v2, p0, Lpcd;->v:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 130
    :cond_10
    iget-object v1, p0, Lpcd;->d:Ljava/lang/String;

    if-eqz v1, :cond_11

    .line 131
    const/16 v1, 0x6a

    iget-object v2, p0, Lpcd;->d:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 133
    :cond_11
    iget-object v1, p0, Lpcd;->w:Loya;

    if-eqz v1, :cond_12

    .line 134
    const/16 v1, 0xb9

    iget-object v2, p0, Lpcd;->w:Loya;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 136
    :cond_12
    iget-object v1, p0, Lpcd;->e:[Loya;

    if-eqz v1, :cond_14

    .line 137
    iget-object v1, p0, Lpcd;->e:[Loya;

    array-length v2, v1

    :goto_3
    if-ge v0, v2, :cond_14

    aget-object v3, v1, v0

    .line 138
    if-eqz v3, :cond_13

    .line 139
    const/16 v4, 0xc8

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 137
    :cond_13
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 143
    :cond_14
    iget-object v0, p0, Lpcd;->f:Ljava/lang/Long;

    if-eqz v0, :cond_15

    .line 144
    const/16 v0, 0xcb

    iget-object v1, p0, Lpcd;->f:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 146
    :cond_15
    iget-object v0, p0, Lpcd;->g:Ljava/lang/Boolean;

    if-eqz v0, :cond_16

    .line 147
    const/16 v0, 0xd0

    iget-object v1, p0, Lpcd;->g:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 149
    :cond_16
    iget-object v0, p0, Lpcd;->x:Ljava/lang/String;

    if-eqz v0, :cond_17

    .line 150
    const/16 v0, 0xfe

    iget-object v1, p0, Lpcd;->x:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 152
    :cond_17
    iget-object v0, p0, Lpcd;->h:Ljava/lang/String;

    if-eqz v0, :cond_18

    .line 153
    const/16 v0, 0x100

    iget-object v1, p0, Lpcd;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 155
    :cond_18
    iget-object v0, p0, Lpcd;->i:Loya;

    if-eqz v0, :cond_19

    .line 156
    const/16 v0, 0x10c

    iget-object v1, p0, Lpcd;->i:Loya;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 158
    :cond_19
    iget v0, p0, Lpcd;->j:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1a

    .line 159
    const/16 v0, 0x10d

    iget v1, p0, Lpcd;->j:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 161
    :cond_1a
    iget-object v0, p0, Lpcd;->y:Ljava/lang/String;

    if-eqz v0, :cond_1b

    .line 162
    const/16 v0, 0x10e

    iget-object v1, p0, Lpcd;->y:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 164
    :cond_1b
    iget-object v0, p0, Lpcd;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 166
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lpcd;->a(Loxn;)Lpcd;

    move-result-object v0

    return-object v0
.end method
