.class public final Lera;
.super Leak;
.source "PG"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/media/MediaPlayer$OnErrorListener;
.implements Landroid/media/MediaPlayer$OnInfoListener;
.implements Landroid/media/MediaPlayer$OnPreparedListener;
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private N:Landroid/content/Intent;

.field private O:Landroid/net/Uri;

.field private P:Ljava/lang/String;

.field private Q:Landroid/net/Uri;

.field private R:Z

.field private S:I

.field private T:I

.field private U:Landroid/widget/MediaController;

.field private V:Landroid/widget/VideoView;

.field private W:Z

.field private X:Z

.field private Y:Z

.field private Z:Z

.field private aa:Landroid/media/audiofx/Virtualizer;

.field private ab:Llmt;

.field private ac:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 118
    invoke-direct {p0}, Leak;-><init>()V

    .line 89
    const/4 v0, 0x0

    iput v0, p0, Lera;->S:I

    .line 110
    new-instance v0, Lerb;

    invoke-direct {v0, p0}, Lerb;-><init>(Lera;)V

    iput-object v0, p0, Lera;->ac:Ljava/lang/Runnable;

    .line 119
    return-void
.end method

.method private U()V
    .locals 1

    .prologue
    .line 290
    iget-object v0, p0, Lera;->V:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->stopPlayback()V

    .line 291
    iget-object v0, p0, Lera;->ab:Llmt;

    if-eqz v0, :cond_0

    .line 292
    iget-object v0, p0, Lera;->ab:Llmt;

    invoke-virtual {v0}, Llmt;->b()V

    .line 293
    const/4 v0, 0x0

    iput-object v0, p0, Lera;->ab:Llmt;

    .line 295
    :cond_0
    return-void
.end method

.method private V()V
    .locals 2

    .prologue
    .line 420
    iget-object v0, p0, Lera;->O:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    .line 421
    const-string v1, "https"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "http"

    .line 422
    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "rtsp"

    .line 423
    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 425
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lera;->W:Z

    .line 429
    :goto_0
    return-void

    .line 427
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lera;->W:Z

    goto :goto_0
.end method

.method static synthetic a(Lera;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lera;->O:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic b(Lera;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lera;->Q:Landroid/net/Uri;

    return-object v0
.end method

.method private c(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 534
    iget v0, p0, Lera;->S:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget v0, p0, Lera;->S:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 536
    :goto_0
    iget-boolean v1, p0, Lera;->W:Z

    if-eqz v1, :cond_2

    .line 537
    invoke-virtual {p0, p1}, Lera;->d(Landroid/view/View;)V

    .line 545
    :goto_1
    return-void

    .line 534
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 539
    :cond_2
    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lera;->X:Z

    if-nez v0, :cond_3

    .line 540
    invoke-virtual {p0, p1}, Lera;->h(Landroid/view/View;)V

    goto :goto_1

    .line 542
    :cond_3
    invoke-virtual {p0, p1}, Lera;->g(Landroid/view/View;)V

    goto :goto_1
.end method

.method static synthetic c(Lera;)Z
    .locals 1

    .prologue
    .line 52
    iget-boolean v0, p0, Lera;->R:Z

    return v0
.end method

.method private e()V
    .locals 3

    .prologue
    .line 224
    iget-object v0, p0, Lera;->O:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lera;->Q:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lera;->R:Z

    if-eqz v0, :cond_0

    .line 225
    iget-object v0, p0, Lera;->V:Landroid/widget/VideoView;

    iget-object v1, p0, Lera;->Q:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/widget/VideoView;->setVideoURI(Landroid/net/Uri;)V

    .line 239
    :goto_0
    return-void

    .line 226
    :cond_0
    iget-object v0, p0, Lera;->P:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 227
    iget-object v0, p0, Lera;->P:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/libraries/social/media/MediaResource;->isPartialDownloadFileName(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lera;->O:Landroid/net/Uri;

    .line 228
    :goto_1
    invoke-virtual {p0}, Lera;->n()Lz;

    move-result-object v1

    iget-object v2, p0, Lera;->P:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Llmt;->a(Landroid/content/Context;Ljava/lang/String;Landroid/net/Uri;)Llmt;

    move-result-object v0

    iput-object v0, p0, Lera;->ab:Llmt;

    .line 230
    :try_start_0
    iget-object v0, p0, Lera;->ab:Llmt;

    invoke-virtual {v0}, Llmt;->a()Landroid/net/Uri;

    move-result-object v0

    .line 231
    iget-object v1, p0, Lera;->V:Landroid/widget/VideoView;

    invoke-virtual {v1, v0}, Landroid/widget/VideoView;->setVideoURI(Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 232
    :catch_0
    move-exception v0

    .line 233
    const-string v1, "VideoViewFragment"

    const-string v2, "Couldn\'t start video server"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 234
    iget-object v0, p0, Lera;->V:Landroid/widget/VideoView;

    iget-object v1, p0, Lera;->O:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/widget/VideoView;->setVideoURI(Landroid/net/Uri;)V

    goto :goto_0

    .line 227
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 237
    :cond_2
    iget-object v0, p0, Lera;->V:Landroid/widget/VideoView;

    iget-object v1, p0, Lera;->O:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/widget/VideoView;->setVideoURI(Landroid/net/Uri;)V

    goto :goto_0
.end method


# virtual methods
.method public A()V
    .locals 1

    .prologue
    .line 281
    iget-object v0, p0, Lera;->aa:Landroid/media/audiofx/Virtualizer;

    if-eqz v0, :cond_0

    .line 282
    iget-object v0, p0, Lera;->aa:Landroid/media/audiofx/Virtualizer;

    invoke-virtual {v0}, Landroid/media/audiofx/Virtualizer;->release()V

    .line 283
    const/4 v0, 0x0

    iput-object v0, p0, Lera;->aa:Landroid/media/audiofx/Virtualizer;

    .line 285
    :cond_0
    invoke-direct {p0}, Lera;->U()V

    .line 286
    invoke-super {p0}, Leak;->A()V

    .line 287
    return-void
.end method

.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 565
    sget-object v0, Lhmw;->ak:Lhmw;

    return-object v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 169
    const v0, 0x7f040230

    invoke-super {p0, p1, p2, p3, v0}, Leak;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;

    move-result-object v3

    .line 172
    new-instance v0, Lerd;

    invoke-virtual {p0}, Lera;->n()Lz;

    move-result-object v4

    invoke-direct {v0, p0, v4}, Lerd;-><init>(Lera;Landroid/content/Context;)V

    iput-object v0, p0, Lera;->U:Landroid/widget/MediaController;

    .line 174
    const v0, 0x7f100625

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 175
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 177
    iget-object v4, p0, Lera;->U:Landroid/widget/MediaController;

    invoke-virtual {v4, v0}, Landroid/widget/MediaController;->setAnchorView(Landroid/view/View;)V

    .line 178
    iget-object v0, p0, Lera;->U:Landroid/widget/MediaController;

    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v4, v5, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v4}, Landroid/widget/MediaController;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 181
    const v0, 0x7f100626

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/VideoView;

    iput-object v0, p0, Lera;->V:Landroid/widget/VideoView;

    .line 182
    iget-object v0, p0, Lera;->V:Landroid/widget/VideoView;

    iget-object v4, p0, Lera;->U:Landroid/widget/MediaController;

    invoke-virtual {v0, v4}, Landroid/widget/VideoView;->setMediaController(Landroid/widget/MediaController;)V

    .line 183
    iget-object v0, p0, Lera;->V:Landroid/widget/VideoView;

    invoke-virtual {v0, p0}, Landroid/widget/VideoView;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 184
    iget-object v0, p0, Lera;->V:Landroid/widget/VideoView;

    invoke-virtual {v0, p0}, Landroid/widget/VideoView;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 185
    iget-object v0, p0, Lera;->V:Landroid/widget/VideoView;

    invoke-virtual {v0, p0}, Landroid/widget/VideoView;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 187
    invoke-direct {p0}, Lera;->e()V

    .line 192
    if-eqz p3, :cond_0

    iget-object v0, p0, Lera;->O:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 193
    iget-object v0, p0, Lera;->V:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->start()V

    .line 194
    iget-object v0, p0, Lera;->V:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->suspend()V

    .line 195
    iput-boolean v1, p0, Lera;->Y:Z

    .line 198
    :cond_0
    const v0, 0x7f0a0650

    invoke-virtual {p0, v3, v0}, Lera;->a(Landroid/view/View;I)V

    .line 200
    iget-object v0, p0, Lera;->N:Landroid/content/Intent;

    const-string v4, "virtualize"

    invoke-virtual {v0, v4, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 201
    if-eqz v0, :cond_1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x12

    if-lt v0, v4, :cond_3

    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    .line 202
    iget-object v0, p0, Lera;->V:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->getAudioSessionId()I

    move-result v0

    .line 203
    if-eqz v0, :cond_1

    .line 204
    new-instance v4, Landroid/media/audiofx/Virtualizer;

    invoke-direct {v4, v2, v0}, Landroid/media/audiofx/Virtualizer;-><init>(II)V

    iput-object v4, p0, Lera;->aa:Landroid/media/audiofx/Virtualizer;

    .line 205
    iget-object v0, p0, Lera;->aa:Landroid/media/audiofx/Virtualizer;

    invoke-virtual {v0, v1}, Landroid/media/audiofx/Virtualizer;->setEnabled(Z)I

    .line 207
    :cond_1
    invoke-static {}, Llsj;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 212
    new-instance v0, Lerc;

    invoke-direct {v0, p0}, Lerc;-><init>(Lera;)V

    invoke-virtual {v3, v0}, Landroid/view/View;->setOnSystemUiVisibilityChangeListener(Landroid/view/View$OnSystemUiVisibilityChangeListener;)V

    .line 220
    :cond_2
    return-object v3

    :cond_3
    move v0, v2

    .line 201
    goto :goto_0
.end method

.method public a()V
    .locals 4

    .prologue
    .line 555
    iget-object v0, p0, Lera;->U:Landroid/widget/MediaController;

    invoke-virtual {v0}, Landroid/widget/MediaController;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 556
    invoke-virtual {p0}, Lera;->n()Lz;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Llii;->a(Landroid/app/Activity;Z)V

    .line 561
    :goto_0
    return-void

    .line 558
    :cond_0
    iget-object v0, p0, Lera;->ac:Ljava/lang/Runnable;

    const-wide/16 v2, 0x64

    invoke-static {v0, v2, v3}, Llsx;->a(Ljava/lang/Runnable;J)V

    goto :goto_0
.end method

.method public a(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 123
    invoke-super {p0, p1}, Leak;->a(Landroid/app/Activity;)V

    .line 124
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lera;->N:Landroid/content/Intent;

    .line 125
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 11

    .prologue
    const/16 v10, 0x280

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 129
    invoke-super {p0, p1}, Leak;->a(Landroid/os/Bundle;)V

    .line 131
    if-nez p1, :cond_c

    .line 132
    iget-object v0, p0, Lera;->N:Landroid/content/Intent;

    const-string v1, "video_data"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 134
    :try_start_0
    iget-object v0, p0, Lera;->N:Landroid/content/Intent;

    const-string v1, "video_data"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    .line 135
    new-instance v1, Lnzb;

    invoke-direct {v1}, Lnzb;-><init>()V

    invoke-static {v1, v0}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lnzb;

    .line 136
    iget-object v6, v0, Lnzb;->c:[Lnzc;

    if-eqz v6, :cond_0

    array-length v1, v6

    if-nez v1, :cond_2

    .line 137
    :cond_0
    :goto_0
    iget v0, v0, Lnzb;->b:I

    iput v0, p0, Lera;->S:I

    .line 138
    const-string v0, "VideoViewFragment"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 139
    iget-object v0, p0, Lera;->O:Landroid/net/Uri;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lera;->S:I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x27

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Video stream uri: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", status: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    .line 158
    :cond_1
    :goto_1
    iget-object v0, p0, Lera;->N:Landroid/content/Intent;

    const-string v1, "android.intent.extra.finishOnCompletion"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lera;->Z:Z

    .line 163
    return-void

    .line 136
    :cond_2
    const/4 v1, 0x0

    :try_start_1
    iput-object v1, p0, Lera;->P:Ljava/lang/String;

    array-length v2, v6

    move v1, v4

    :goto_2
    if-ge v1, v2, :cond_f

    aget-object v5, v6, v1

    iget-object v7, v5, Lnzc;->d:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_6

    iget-object v7, v5, Lnzc;->d:Ljava/lang/String;

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v7}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v8

    const-string v9, "content"

    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_3

    const-string v9, "file"

    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    :cond_3
    iput-object v7, p0, Lera;->O:Landroid/net/Uri;
    :try_end_1
    .catch Loxt; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 141
    :catch_0
    move-exception v0

    .line 142
    const-string v1, "VideoViewFragment"

    const-string v2, "Unable to parse Video from byte array."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 136
    :cond_4
    :try_start_2
    invoke-virtual {p0}, Lera;->n()Lz;

    move-result-object v8

    iget-object v9, v5, Lnzc;->d:Ljava/lang/String;

    invoke-static {v8, v9}, Lcom/google/android/libraries/social/media/MediaResource;->getCachedVideoFile(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v8

    if-eqz v8, :cond_6

    invoke-virtual {v8}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lera;->P:Ljava/lang/String;

    iput-object v7, p0, Lera;->O:Landroid/net/Uri;

    iget-object v1, p0, Lera;->P:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/libraries/social/media/MediaResource;->isPartialDownloadFileName(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    move v1, v3

    :goto_3
    iget-object v2, v5, Lnzc;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-le v2, v10, :cond_0

    :goto_4
    if-nez v1, :cond_7

    array-length v7, v6

    move v5, v4

    move v2, v4

    :goto_5
    if-ge v5, v7, :cond_7

    aget-object v8, v6, v5

    iget-object v1, v8, Lnzc;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Llmz;->a(I)Z

    move-result v1

    if-eqz v1, :cond_e

    iget-object v1, v8, Lnzc;->d:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_e

    iget-object v1, v8, Lnzc;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-gt v1, v10, :cond_e

    if-le v1, v2, :cond_e

    iget-object v2, v8, Lnzc;->d:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iput-object v2, p0, Lera;->Q:Landroid/net/Uri;

    :goto_6
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    move v2, v1

    goto :goto_5

    :cond_5
    move v1, v4

    goto :goto_3

    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_2

    :cond_7
    iget-object v1, p0, Lera;->P:Ljava/lang/String;

    if-nez v1, :cond_0

    array-length v7, v6

    move v5, v4

    move v2, v4

    :goto_7
    if-ge v5, v7, :cond_8

    aget-object v8, v6, v5

    iget-object v1, v8, Lnzc;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Llmz;->a(I)Z

    move-result v1

    if-eqz v1, :cond_d

    iget-object v1, v8, Lnzc;->d:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_d

    iget-object v1, v8, Lnzc;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-le v1, v10, :cond_d

    if-le v1, v2, :cond_d

    iget-object v2, v8, Lnzc;->d:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iput-object v2, p0, Lera;->O:Landroid/net/Uri;

    :goto_8
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    move v2, v1

    goto :goto_7

    :cond_8
    iget-object v1, p0, Lera;->O:Landroid/net/Uri;

    if-nez v1, :cond_9

    iget-object v1, p0, Lera;->Q:Landroid/net/Uri;

    iput-object v1, p0, Lera;->O:Landroid/net/Uri;

    const/4 v1, 0x0

    iput-object v1, p0, Lera;->Q:Landroid/net/Uri;

    :cond_9
    iget-object v1, p0, Lera;->Q:Landroid/net/Uri;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lera;->n()Lz;

    move-result-object v1

    const-class v2, Ljgn;

    invoke-static {v1, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljgn;

    invoke-interface {v1}, Ljgn;->b()Z

    move-result v1

    if-nez v1, :cond_a

    move v1, v3

    :goto_9
    iput-boolean v1, p0, Lera;->R:Z
    :try_end_2
    .catch Loxt; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    :cond_a
    move v1, v4

    goto :goto_9

    .line 146
    :cond_b
    iget-object v0, p0, Lera;->N:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lera;->O:Landroid/net/Uri;

    .line 147
    const/4 v0, 0x2

    iput v0, p0, Lera;->S:I

    goto/16 :goto_1

    .line 150
    :cond_c
    const-string v0, "uri"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lera;->O:Landroid/net/Uri;

    .line 151
    const-string v0, "cacheFile"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lera;->P:Ljava/lang/String;

    .line 152
    const-string v0, "status"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lera;->S:I

    .line 153
    const-string v0, "position"

    invoke-virtual {p1, v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lera;->T:I

    .line 154
    const-string v0, "play_sd"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lera;->R:Z

    .line 155
    const-string v0, "sd_uri"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lera;->Q:Landroid/net/Uri;

    goto/16 :goto_1

    :cond_d
    move v1, v2

    goto :goto_8

    :cond_e
    move v1, v2

    goto/16 :goto_6

    :cond_f
    move v1, v4

    goto/16 :goto_4
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 516
    iget-boolean v0, p0, Lera;->R:Z

    if-eq v0, p1, :cond_0

    .line 517
    iput-boolean p1, p0, Lera;->R:Z

    .line 518
    const/4 v0, 0x1

    iput-boolean v0, p0, Lera;->W:Z

    .line 519
    invoke-virtual {p0}, Lera;->x()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lera;->c(Landroid/view/View;)V

    .line 520
    iget-object v0, p0, Lera;->U:Landroid/widget/MediaController;

    invoke-virtual {v0}, Landroid/widget/MediaController;->hide()V

    .line 521
    iget-object v0, p0, Lera;->V:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->getCurrentPosition()I

    move-result v0

    .line 522
    invoke-direct {p0}, Lera;->U()V

    .line 523
    invoke-direct {p0}, Lera;->e()V

    .line 524
    iget-object v1, p0, Lera;->V:Landroid/widget/VideoView;

    invoke-virtual {v1, v0}, Landroid/widget/VideoView;->seekTo(I)V

    .line 525
    iget-object v0, p0, Lera;->V:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->start()V

    .line 527
    :cond_0
    return-void
.end method

.method public aO_()V
    .locals 3

    .prologue
    .line 243
    invoke-super {p0}, Leak;->aO_()V

    .line 244
    iget-boolean v0, p0, Lera;->Y:Z

    if-eqz v0, :cond_0

    .line 245
    iget-object v0, p0, Lera;->V:Landroid/widget/VideoView;

    iget v1, p0, Lera;->T:I

    invoke-virtual {v0, v1}, Landroid/widget/VideoView;->seekTo(I)V

    .line 246
    iget-object v0, p0, Lera;->V:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->resume()V

    .line 247
    invoke-direct {p0}, Lera;->V()V

    .line 248
    invoke-virtual {p0}, Lera;->x()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lera;->c(Landroid/view/View;)V

    .line 252
    :goto_0
    return-void

    .line 250
    :cond_0
    invoke-virtual {p0}, Lera;->x()Landroid/view/View;

    move-result-object v0

    iget v1, p0, Lera;->S:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    iget v1, p0, Lera;->S:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_3

    :cond_1
    iget-object v1, p0, Lera;->O:Landroid/net/Uri;

    if-eqz v1, :cond_2

    invoke-direct {p0}, Lera;->V()V

    iget-object v1, p0, Lera;->V:Landroid/widget/VideoView;

    invoke-virtual {v1}, Landroid/widget/VideoView;->start()V

    :goto_1
    invoke-direct {p0, v0}, Lera;->c(Landroid/view/View;)V

    goto :goto_0

    :cond_2
    const/4 v1, 0x1

    iput-boolean v1, p0, Lera;->X:Z

    const v1, 0x7f0a0652

    invoke-virtual {p0, v0, v1}, Lera;->a(Landroid/view/View;I)V

    goto :goto_1

    :cond_3
    iget v1, p0, Lera;->S:I

    if-nez v1, :cond_4

    const v1, 0x7f0a0651

    invoke-virtual {p0, v0, v1}, Lera;->a(Landroid/view/View;I)V

    goto :goto_1

    :cond_4
    const v1, 0x7f0a0650

    invoke-virtual {p0, v0, v1}, Lera;->a(Landroid/view/View;I)V

    goto :goto_1
.end method

.method protected c(I)V
    .locals 4

    .prologue
    .line 548
    and-int/lit8 v0, p1, 0x2

    if-nez v0, :cond_1

    .line 549
    :try_start_0
    iget-object v0, p0, Lera;->U:Landroid/widget/MediaController;

    invoke-virtual {v0}, Landroid/widget/MediaController;->show()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 550
    :cond_0
    :goto_0
    iget-object v0, p0, Lera;->ac:Ljava/lang/Runnable;

    const-wide/16 v2, 0xbb8

    invoke-static {v0, v2, v3}, Llsx;->a(Ljava/lang/Runnable;J)V

    .line 552
    :cond_1
    return-void

    .line 549
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lera;->W:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lera;->X:Z

    invoke-virtual {p0}, Lera;->x()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    const v1, 0x7f0a0652

    invoke-virtual {p0, v0, v1}, Lera;->a(Landroid/view/View;I)V

    invoke-direct {p0, v0}, Lera;->c(Landroid/view/View;)V

    goto :goto_0
.end method

.method protected d()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 299
    iget v2, p0, Lera;->S:I

    const/4 v3, 0x2

    if-eq v2, v3, :cond_0

    iget v2, p0, Lera;->S:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_3

    :cond_0
    move v2, v1

    .line 300
    :goto_0
    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lera;->X:Z

    if-nez v2, :cond_1

    iget-boolean v2, p0, Lera;->W:Z

    if-eqz v2, :cond_2

    :cond_1
    move v0, v1

    .line 301
    :cond_2
    return v0

    :cond_3
    move v2, v0

    .line 299
    goto :goto_0
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 270
    invoke-super {p0, p1}, Leak;->e(Landroid/os/Bundle;)V

    .line 271
    const-string v0, "position"

    iget v1, p0, Lera;->T:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 272
    const-string v0, "uri"

    iget-object v1, p0, Lera;->O:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 273
    const-string v0, "cacheFile"

    iget-object v1, p0, Lera;->P:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    const-string v0, "status"

    iget v1, p0, Lera;->S:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 275
    const-string v0, "play_sd"

    iget-boolean v1, p0, Lera;->R:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 276
    const-string v0, "sd_uri"

    iget-object v1, p0, Lera;->Q:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 277
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 306
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f100625

    if-ne v0, v1, :cond_0

    .line 307
    iget-object v0, p0, Lera;->U:Landroid/widget/MediaController;

    invoke-virtual {v0}, Landroid/widget/MediaController;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 308
    invoke-virtual {p0}, Lera;->a()V

    .line 311
    :cond_0
    return-void
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 1

    .prologue
    .line 355
    iget-boolean v0, p0, Lera;->Z:Z

    if-eqz v0, :cond_0

    .line 356
    invoke-virtual {p0}, Lera;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->finish()V

    .line 358
    :cond_0
    return-void
.end method

.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 375
    iput-boolean v2, p0, Lera;->X:Z

    .line 376
    const/4 v0, 0x0

    iput-boolean v0, p0, Lera;->W:Z

    .line 378
    invoke-virtual {p0}, Lera;->x()Landroid/view/View;

    move-result-object v0

    .line 379
    if-eqz v0, :cond_0

    .line 380
    const v1, 0x7f0a0652

    invoke-virtual {p0, v0, v1}, Lera;->a(Landroid/view/View;I)V

    .line 381
    invoke-direct {p0, v0}, Lera;->c(Landroid/view/View;)V

    .line 384
    :cond_0
    return v2
.end method

.method public onInfo(Landroid/media/MediaPlayer;II)Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 330
    sparse-switch p2, :sswitch_data_0

    .line 346
    :goto_0
    invoke-virtual {p0}, Lera;->x()Landroid/view/View;

    move-result-object v0

    .line 347
    if-eqz v0, :cond_0

    .line 348
    invoke-direct {p0, v0}, Lera;->c(Landroid/view/View;)V

    .line 350
    :cond_0
    return v1

    .line 332
    :sswitch_0
    iput-boolean v1, p0, Lera;->W:Z

    .line 333
    iput-boolean v0, p0, Lera;->X:Z

    goto :goto_0

    .line 336
    :sswitch_1
    iput-boolean v0, p0, Lera;->W:Z

    .line 337
    iput-boolean v0, p0, Lera;->X:Z

    goto :goto_0

    .line 342
    :sswitch_2
    iput-boolean v0, p0, Lera;->W:Z

    .line 343
    iput-boolean v1, p0, Lera;->X:Z

    goto :goto_0

    .line 330
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_2
        0x64 -> :sswitch_2
        0xc8 -> :sswitch_2
        0x2bd -> :sswitch_0
        0x2be -> :sswitch_1
    .end sparse-switch
.end method

.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 1

    .prologue
    .line 389
    const/4 v0, 0x0

    iput-boolean v0, p0, Lera;->W:Z

    .line 390
    invoke-virtual {p1, p0}, Landroid/media/MediaPlayer;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    .line 391
    invoke-virtual {p0}, Lera;->x()Landroid/view/View;

    move-result-object v0

    .line 392
    if-eqz v0, :cond_0

    .line 393
    invoke-direct {p0, v0}, Lera;->c(Landroid/view/View;)V

    .line 395
    :cond_0
    return-void
.end method

.method public z()V
    .locals 2

    .prologue
    const/16 v0, 0x7d0

    .line 256
    invoke-super {p0}, Leak;->z()V

    .line 257
    iget-object v1, p0, Lera;->V:Landroid/widget/VideoView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lera;->O:Landroid/net/Uri;

    if-eqz v1, :cond_0

    .line 258
    iget-object v1, p0, Lera;->V:Landroid/widget/VideoView;

    invoke-virtual {v1}, Landroid/widget/VideoView;->getCurrentPosition()I

    move-result v1

    iput v1, p0, Lera;->T:I

    .line 259
    iget v1, p0, Lera;->T:I

    if-le v1, v0, :cond_1

    .line 261
    :goto_0
    iget v1, p0, Lera;->T:I

    sub-int v0, v1, v0

    iput v0, p0, Lera;->T:I

    .line 263
    iget-object v0, p0, Lera;->V:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->suspend()V

    .line 264
    const/4 v0, 0x1

    iput-boolean v0, p0, Lera;->Y:Z

    .line 266
    :cond_0
    return-void

    .line 259
    :cond_1
    iget v0, p0, Lera;->T:I

    goto :goto_0
.end method
