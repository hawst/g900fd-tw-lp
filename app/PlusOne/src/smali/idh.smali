.class public final Lidh;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:Lpbl;

.field private b:Lozp;


# direct methods
.method public constructor <init>(Lozp;)V
    .locals 2

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    if-nez p1, :cond_0

    .line 37
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "HoaPlusEventV2 cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 39
    :cond_0
    iput-object p1, p0, Lidh;->b:Lozp;

    .line 40
    return-void
.end method

.method public constructor <init>(Lpbl;)V
    .locals 2

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    if-nez p1, :cond_0

    .line 30
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "PlusEvent cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 32
    :cond_0
    iput-object p1, p0, Lidh;->a:Lpbl;

    .line 33
    return-void
.end method


# virtual methods
.method public a()Lpbl;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lidh;->a:Lpbl;

    return-object v0
.end method

.method public a(Lidh;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 189
    iget-object v0, p0, Lidh;->a:Lpbl;

    if-eqz v0, :cond_4

    .line 190
    iget-object v0, p0, Lidh;->a:Lpbl;

    invoke-virtual {p1}, Lidh;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lpbl;->b:Ljava/lang/String;

    .line 191
    iget-object v0, p0, Lidh;->a:Lpbl;

    invoke-virtual {p1}, Lidh;->p()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lpbl;->c:Ljava/lang/String;

    .line 192
    iget-object v0, p0, Lidh;->a:Lpbl;

    iget-object v0, v0, Lpbl;->h:Loya;

    sget-object v1, Loyy;->a:Loxr;

    .line 193
    invoke-virtual {p1}, Lidh;->l()Loyy;

    move-result-object v2

    .line 192
    invoke-virtual {v0, v1, v2}, Loya;->a(Loxr;Ljava/lang/Object;)V

    .line 195
    invoke-virtual {p1}, Lidh;->m()Loyy;

    move-result-object v0

    .line 197
    if-eqz v0, :cond_2

    .line 198
    iget-object v1, p0, Lidh;->a:Lpbl;

    iget-object v1, v1, Lpbl;->i:Loya;

    if-nez v1, :cond_0

    .line 199
    iget-object v1, p0, Lidh;->a:Lpbl;

    new-instance v2, Loya;

    invoke-direct {v2}, Loya;-><init>()V

    iput-object v2, v1, Lpbl;->i:Loya;

    .line 202
    :cond_0
    iget-object v1, p0, Lidh;->a:Lpbl;

    iget-object v1, v1, Lpbl;->i:Loya;

    sget-object v2, Loyy;->a:Loxr;

    invoke-virtual {v1, v2, v0}, Loya;->a(Loxr;Ljava/lang/Object;)V

    .line 207
    :goto_0
    invoke-virtual {p1}, Lidh;->o()Lpao;

    move-result-object v0

    .line 209
    if-eqz v0, :cond_3

    .line 210
    iget-object v1, p0, Lidh;->a:Lpbl;

    iget-object v1, v1, Lpbl;->f:Loya;

    if-nez v1, :cond_1

    .line 211
    iget-object v1, p0, Lidh;->a:Lpbl;

    new-instance v2, Loya;

    invoke-direct {v2}, Loya;-><init>()V

    iput-object v2, v1, Lpbl;->f:Loya;

    .line 214
    :cond_1
    iget-object v1, p0, Lidh;->a:Lpbl;

    iget-object v1, v1, Lpbl;->f:Loya;

    sget-object v2, Lpao;->a:Loxr;

    invoke-virtual {v1, v2, v0}, Loya;->a(Loxr;Ljava/lang/Object;)V

    .line 219
    :goto_1
    iget-object v0, p0, Lidh;->a:Lpbl;

    invoke-virtual {p1}, Lidh;->i()Lpbj;

    move-result-object v1

    iput-object v1, v0, Lpbl;->l:Lpbj;

    .line 252
    :goto_2
    return-void

    .line 204
    :cond_2
    iget-object v0, p0, Lidh;->a:Lpbl;

    iput-object v3, v0, Lpbl;->i:Loya;

    goto :goto_0

    .line 216
    :cond_3
    iget-object v0, p0, Lidh;->a:Lpbl;

    iput-object v3, v0, Lpbl;->f:Loya;

    goto :goto_1

    .line 221
    :cond_4
    iget-object v0, p0, Lidh;->b:Lozp;

    invoke-virtual {p1}, Lidh;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lozp;->b:Ljava/lang/String;

    .line 222
    iget-object v0, p0, Lidh;->b:Lozp;

    invoke-virtual {p1}, Lidh;->p()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lozp;->c:Ljava/lang/String;

    .line 223
    iget-object v0, p0, Lidh;->b:Lozp;

    iget-object v0, v0, Lozp;->h:Loya;

    sget-object v1, Loyy;->a:Loxr;

    .line 224
    invoke-virtual {p1}, Lidh;->l()Loyy;

    move-result-object v2

    .line 223
    invoke-virtual {v0, v1, v2}, Loya;->a(Loxr;Ljava/lang/Object;)V

    .line 226
    invoke-virtual {p1}, Lidh;->m()Loyy;

    move-result-object v0

    .line 228
    if-eqz v0, :cond_7

    .line 229
    iget-object v1, p0, Lidh;->b:Lozp;

    iget-object v1, v1, Lozp;->i:Loya;

    if-nez v1, :cond_5

    .line 230
    iget-object v1, p0, Lidh;->b:Lozp;

    new-instance v2, Loya;

    invoke-direct {v2}, Loya;-><init>()V

    iput-object v2, v1, Lozp;->i:Loya;

    .line 233
    :cond_5
    iget-object v1, p0, Lidh;->b:Lozp;

    iget-object v1, v1, Lozp;->i:Loya;

    sget-object v2, Loyy;->a:Loxr;

    invoke-virtual {v1, v2, v0}, Loya;->a(Loxr;Ljava/lang/Object;)V

    .line 238
    :goto_3
    invoke-virtual {p1}, Lidh;->o()Lpao;

    move-result-object v0

    .line 240
    if-eqz v0, :cond_8

    .line 241
    iget-object v1, p0, Lidh;->b:Lozp;

    iget-object v1, v1, Lozp;->f:Loya;

    if-nez v1, :cond_6

    .line 242
    iget-object v1, p0, Lidh;->b:Lozp;

    new-instance v2, Loya;

    invoke-direct {v2}, Loya;-><init>()V

    iput-object v2, v1, Lozp;->f:Loya;

    .line 245
    :cond_6
    iget-object v1, p0, Lidh;->b:Lozp;

    iget-object v1, v1, Lozp;->f:Loya;

    sget-object v2, Lpao;->a:Loxr;

    invoke-virtual {v1, v2, v0}, Loya;->a(Loxr;Ljava/lang/Object;)V

    .line 250
    :goto_4
    iget-object v0, p0, Lidh;->b:Lozp;

    invoke-virtual {p1}, Lidh;->i()Lpbj;

    move-result-object v1

    iput-object v1, v0, Lozp;->l:Lpbj;

    goto :goto_2

    .line 235
    :cond_7
    iget-object v0, p0, Lidh;->b:Lozp;

    iput-object v3, v0, Lozp;->i:Loya;

    goto :goto_3

    .line 247
    :cond_8
    iget-object v0, p0, Lidh;->b:Lozp;

    iput-object v3, v0, Lozp;->f:Loya;

    goto :goto_4
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 264
    iget-object v0, p0, Lidh;->a:Lpbl;

    if-eqz v0, :cond_0

    .line 265
    iget-object v0, p0, Lidh;->a:Lpbl;

    iput-object p1, v0, Lpbl;->b:Ljava/lang/String;

    .line 269
    :goto_0
    return-void

    .line 267
    :cond_0
    iget-object v0, p0, Lidh;->b:Lozp;

    iput-object p1, v0, Lozp;->b:Ljava/lang/String;

    goto :goto_0
.end method

.method public a(Loyy;)V
    .locals 5

    .prologue
    const/16 v4, 0x196

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 280
    iget-object v0, p0, Lidh;->a:Lpbl;

    if-eqz v0, :cond_1

    .line 281
    iget-object v0, p0, Lidh;->a:Lpbl;

    iget-object v0, v0, Lpbl;->h:Loya;

    if-nez v0, :cond_0

    .line 282
    iget-object v0, p0, Lidh;->a:Lpbl;

    new-instance v1, Loya;

    invoke-direct {v1}, Loya;-><init>()V

    iput-object v1, v0, Lpbl;->h:Loya;

    .line 283
    iget-object v0, p0, Lidh;->a:Lpbl;

    iget-object v0, v0, Lpbl;->h:Loya;

    new-array v1, v3, [I

    aput v4, v1, v2

    iput-object v1, v0, Loya;->b:[I

    .line 285
    :cond_0
    iget-object v0, p0, Lidh;->a:Lpbl;

    iget-object v0, v0, Lpbl;->h:Loya;

    sget-object v1, Loyy;->a:Loxr;

    invoke-virtual {v0, v1, p1}, Loya;->a(Loxr;Ljava/lang/Object;)V

    .line 293
    :goto_0
    return-void

    .line 287
    :cond_1
    iget-object v0, p0, Lidh;->b:Lozp;

    iget-object v0, v0, Lozp;->h:Loya;

    if-nez v0, :cond_2

    .line 288
    iget-object v0, p0, Lidh;->b:Lozp;

    new-instance v1, Loya;

    invoke-direct {v1}, Loya;-><init>()V

    iput-object v1, v0, Lozp;->h:Loya;

    .line 289
    iget-object v0, p0, Lidh;->b:Lozp;

    iget-object v0, v0, Lozp;->h:Loya;

    new-array v1, v3, [I

    aput v4, v1, v2

    iput-object v1, v0, Loya;->b:[I

    .line 291
    :cond_2
    iget-object v0, p0, Lidh;->b:Lozp;

    iget-object v0, v0, Lozp;->h:Loya;

    sget-object v1, Loyy;->a:Loxr;

    invoke-virtual {v0, v1, p1}, Loya;->a(Loxr;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public b()Lozp;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lidh;->b:Lozp;

    return-object v0
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lidh;->a:Lpbl;

    if-eqz v0, :cond_0

    .line 273
    iget-object v0, p0, Lidh;->a:Lpbl;

    iput-object p1, v0, Lpbl;->c:Ljava/lang/String;

    .line 277
    :goto_0
    return-void

    .line 275
    :cond_0
    iget-object v0, p0, Lidh;->b:Lozp;

    iput-object p1, v0, Lozp;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method public b(Loyy;)V
    .locals 5

    .prologue
    const/16 v4, 0x196

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 296
    iget-object v0, p0, Lidh;->a:Lpbl;

    if-eqz v0, :cond_1

    .line 297
    iget-object v0, p0, Lidh;->a:Lpbl;

    iget-object v0, v0, Lpbl;->i:Loya;

    if-nez v0, :cond_0

    .line 298
    iget-object v0, p0, Lidh;->a:Lpbl;

    new-instance v1, Loya;

    invoke-direct {v1}, Loya;-><init>()V

    iput-object v1, v0, Lpbl;->i:Loya;

    .line 299
    iget-object v0, p0, Lidh;->a:Lpbl;

    iget-object v0, v0, Lpbl;->i:Loya;

    new-array v1, v3, [I

    aput v4, v1, v2

    iput-object v1, v0, Loya;->b:[I

    .line 301
    :cond_0
    iget-object v0, p0, Lidh;->a:Lpbl;

    iget-object v0, v0, Lpbl;->i:Loya;

    sget-object v1, Loyy;->a:Loxr;

    invoke-virtual {v0, v1, p1}, Loya;->a(Loxr;Ljava/lang/Object;)V

    .line 309
    :goto_0
    return-void

    .line 303
    :cond_1
    iget-object v0, p0, Lidh;->b:Lozp;

    iget-object v0, v0, Lozp;->i:Loya;

    if-nez v0, :cond_2

    .line 304
    iget-object v0, p0, Lidh;->b:Lozp;

    new-instance v1, Loya;

    invoke-direct {v1}, Loya;-><init>()V

    iput-object v1, v0, Lozp;->i:Loya;

    .line 305
    iget-object v0, p0, Lidh;->b:Lozp;

    iget-object v0, v0, Lozp;->i:Loya;

    new-array v1, v3, [I

    aput v4, v1, v2

    iput-object v1, v0, Loya;->b:[I

    .line 307
    :cond_2
    iget-object v0, p0, Lidh;->b:Lozp;

    iget-object v0, v0, Lozp;->i:Loya;

    sget-object v1, Loyy;->a:Loxr;

    invoke-virtual {v0, v1, p1}, Loya;->a(Loxr;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lidh;->a:Lpbl;

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lidh;->a:Lpbl;

    iget-object v0, v0, Lpbl;->g:Ljava/lang/String;

    .line 54
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lidh;->b:Lozp;

    iget-object v0, v0, Lozp;->g:Ljava/lang/String;

    goto :goto_0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lidh;->a:Lpbl;

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lidh;->a:Lpbl;

    iget-object v0, v0, Lpbl;->b:Ljava/lang/String;

    .line 62
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lidh;->b:Lozp;

    iget-object v0, v0, Lozp;->b:Ljava/lang/String;

    goto :goto_0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lidh;->a:Lpbl;

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lidh;->a:Lpbl;

    iget-object v0, v0, Lpbl;->d:Ljava/lang/String;

    .line 70
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lidh;->b:Lozp;

    iget-object v0, v0, Lozp;->d:Ljava/lang/String;

    goto :goto_0
.end method

.method public f()[B
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lidh;->a:Lpbl;

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lidh;->a:Lpbl;

    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    .line 78
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lidh;->b:Lozp;

    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    goto :goto_0
.end method

.method public g()I
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lidh;->a:Lpbl;

    if-eqz v0, :cond_0

    .line 84
    const/4 v0, 0x0

    .line 86
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lidh;->a:Lpbl;

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lidh;->a:Lpbl;

    iget-object v0, v0, Lpbl;->e:Ljava/lang/String;

    .line 94
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lidh;->b:Lozp;

    iget-object v0, v0, Lozp;->e:Ljava/lang/String;

    goto :goto_0
.end method

.method public i()Lpbj;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lidh;->a:Lpbl;

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lidh;->a:Lpbl;

    iget-object v0, v0, Lpbl;->l:Lpbj;

    .line 102
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lidh;->b:Lozp;

    iget-object v0, v0, Lozp;->l:Lpbj;

    goto :goto_0
.end method

.method public j()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 107
    iget-object v2, p0, Lidh;->a:Lpbl;

    if-eqz v2, :cond_2

    .line 108
    iget-object v2, p0, Lidh;->a:Lpbl;

    iget-object v2, v2, Lpbl;->k:Ljava/lang/Boolean;

    invoke-static {v2}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 110
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 108
    goto :goto_0

    .line 110
    :cond_2
    iget-object v2, p0, Lidh;->b:Lozp;

    iget-object v2, v2, Lozp;->k:Ljava/lang/Boolean;

    invoke-static {v2}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public k()Lpai;
    .locals 2

    .prologue
    .line 115
    iget-object v0, p0, Lidh;->a:Lpbl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lidh;->a:Lpbl;

    iget-object v0, v0, Lpbl;->j:Loya;

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lidh;->a:Lpbl;

    iget-object v0, v0, Lpbl;->j:Loya;

    sget-object v1, Lpai;->a:Loxr;

    invoke-virtual {v0, v1}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpai;

    .line 120
    :goto_0
    return-object v0

    .line 117
    :cond_0
    iget-object v0, p0, Lidh;->b:Lozp;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lidh;->b:Lozp;

    iget-object v0, v0, Lozp;->j:Loya;

    if-eqz v0, :cond_1

    .line 118
    iget-object v0, p0, Lidh;->b:Lozp;

    iget-object v0, v0, Lozp;->j:Loya;

    sget-object v1, Lpai;->a:Loxr;

    invoke-virtual {v0, v1}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpai;

    goto :goto_0

    .line 120
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l()Loyy;
    .locals 2

    .prologue
    .line 124
    iget-object v0, p0, Lidh;->a:Lpbl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lidh;->a:Lpbl;

    iget-object v0, v0, Lpbl;->h:Loya;

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lidh;->a:Lpbl;

    iget-object v0, v0, Lpbl;->h:Loya;

    sget-object v1, Loyy;->a:Loxr;

    invoke-virtual {v0, v1}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Loyy;

    .line 129
    :goto_0
    return-object v0

    .line 126
    :cond_0
    iget-object v0, p0, Lidh;->b:Lozp;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lidh;->b:Lozp;

    iget-object v0, v0, Lozp;->h:Loya;

    if-eqz v0, :cond_1

    .line 127
    iget-object v0, p0, Lidh;->b:Lozp;

    iget-object v0, v0, Lozp;->h:Loya;

    sget-object v1, Loyy;->a:Loxr;

    invoke-virtual {v0, v1}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Loyy;

    goto :goto_0

    .line 129
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public m()Loyy;
    .locals 2

    .prologue
    .line 133
    iget-object v0, p0, Lidh;->a:Lpbl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lidh;->a:Lpbl;

    iget-object v0, v0, Lpbl;->i:Loya;

    if-eqz v0, :cond_0

    .line 134
    iget-object v0, p0, Lidh;->a:Lpbl;

    iget-object v0, v0, Lpbl;->i:Loya;

    sget-object v1, Loyy;->a:Loxr;

    invoke-virtual {v0, v1}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Loyy;

    .line 138
    :goto_0
    return-object v0

    .line 135
    :cond_0
    iget-object v0, p0, Lidh;->b:Lozp;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lidh;->b:Lozp;

    iget-object v0, v0, Lozp;->i:Loya;

    if-eqz v0, :cond_1

    .line 136
    iget-object v0, p0, Lidh;->b:Lozp;

    iget-object v0, v0, Lozp;->i:Loya;

    sget-object v1, Loyy;->a:Loxr;

    invoke-virtual {v0, v1}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Loyy;

    goto :goto_0

    .line 138
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public n()J
    .locals 4

    .prologue
    .line 142
    invoke-virtual {p0}, Lidh;->m()Loyy;

    move-result-object v0

    .line 143
    if-eqz v0, :cond_0

    iget-object v1, v0, Loyy;->b:Ljava/lang/Long;

    if-nez v1, :cond_1

    .line 144
    :cond_0
    invoke-virtual {p0}, Lidh;->l()Loyy;

    move-result-object v0

    iget-object v0, v0, Loyy;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/32 v2, 0x6ddd00

    add-long/2addr v0, v2

    .line 145
    :goto_0
    return-wide v0

    .line 144
    :cond_1
    iget-object v0, v0, Loyy;->b:Ljava/lang/Long;

    .line 145
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_0
.end method

.method public o()Lpao;
    .locals 2

    .prologue
    .line 149
    iget-object v0, p0, Lidh;->a:Lpbl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lidh;->a:Lpbl;

    iget-object v0, v0, Lpbl;->f:Loya;

    if-eqz v0, :cond_0

    .line 150
    iget-object v0, p0, Lidh;->a:Lpbl;

    iget-object v0, v0, Lpbl;->f:Loya;

    sget-object v1, Lpao;->a:Loxr;

    invoke-virtual {v0, v1}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpao;

    .line 154
    :goto_0
    return-object v0

    .line 151
    :cond_0
    iget-object v0, p0, Lidh;->b:Lozp;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lidh;->b:Lozp;

    iget-object v0, v0, Lozp;->f:Loya;

    if-eqz v0, :cond_1

    .line 152
    iget-object v0, p0, Lidh;->b:Lozp;

    iget-object v0, v0, Lozp;->f:Loya;

    sget-object v1, Lpao;->a:Loxr;

    invoke-virtual {v0, v1}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpao;

    goto :goto_0

    .line 154
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lidh;->a:Lpbl;

    if-eqz v0, :cond_0

    .line 159
    iget-object v0, p0, Lidh;->a:Lpbl;

    iget-object v0, v0, Lpbl;->c:Ljava/lang/String;

    .line 161
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lidh;->b:Lozp;

    iget-object v0, v0, Lozp;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method public q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lidh;->a:Lpbl;

    if-eqz v0, :cond_0

    .line 167
    const/4 v0, 0x0

    .line 169
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lidh;->b:Lozp;

    iget-object v0, v0, Lozp;->p:Ljava/lang/String;

    goto :goto_0
.end method

.method public r()Loya;
    .locals 3

    .prologue
    .line 174
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    .line 175
    iget-object v1, p0, Lidh;->a:Lpbl;

    if-eqz v1, :cond_0

    .line 176
    const/4 v1, 0x3

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    iput-object v1, v0, Loya;->b:[I

    .line 178
    sget-object v1, Lpbl;->a:Loxr;

    iget-object v2, p0, Lidh;->a:Lpbl;

    invoke-virtual {v0, v1, v2}, Loya;->a(Loxr;Ljava/lang/Object;)V

    .line 184
    :goto_0
    return-object v0

    .line 180
    :cond_0
    const/4 v1, 0x4

    new-array v1, v1, [I

    fill-array-data v1, :array_1

    iput-object v1, v0, Loya;->b:[I

    .line 182
    sget-object v1, Lozp;->a:Loxr;

    iget-object v2, p0, Lidh;->b:Lozp;

    invoke-virtual {v0, v1, v2}, Loya;->a(Loxr;Ljava/lang/Object;)V

    goto :goto_0

    .line 176
    nop

    :array_0
    .array-data 4
        0x194
        0x160
        0x14f
    .end array-data

    .line 180
    :array_1
    .array-data 4
        0x19a
        0x194
        0x160
        0x14f
    .end array-data
.end method

.method public s()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 312
    iget-object v0, p0, Lidh;->a:Lpbl;

    if-eqz v0, :cond_0

    .line 313
    iget-object v0, p0, Lidh;->a:Lpbl;

    iput-object v1, v0, Lpbl;->i:Loya;

    .line 317
    :goto_0
    return-void

    .line 315
    :cond_0
    iget-object v0, p0, Lidh;->b:Lozp;

    iput-object v1, v0, Lozp;->i:Loya;

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Lidh;->a:Lpbl;

    if-eqz v0, :cond_0

    .line 257
    iget-object v0, p0, Lidh;->a:Lpbl;

    invoke-virtual {v0}, Lpbl;->toString()Ljava/lang/String;

    move-result-object v0

    .line 259
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lidh;->b:Lozp;

    invoke-virtual {v0}, Lozp;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
