.class public final Lgar;
.super Lgbz;
.source "PG"


# instance fields
.field private A:Ljxy;

.field private B:Lkzw;

.field private C:Z

.field private y:Ljxx;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lgbz;-><init>(Landroid/content/Context;)V

    .line 36
    const-class v0, Ljxy;

    invoke-static {p1, v0}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljxy;

    iput-object v0, p0, Lgar;->A:Ljxy;

    iget-object v0, p0, Lgar;->A:Ljxy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgar;->A:Ljxy;

    invoke-interface {v0, p1}, Ljxy;->a(Landroid/content/Context;)Ljxx;

    move-result-object v0

    iput-object v0, p0, Lgar;->y:Ljxx;

    .line 37
    :cond_0
    return-void
.end method

.method private D()Z
    .locals 4

    .prologue
    .line 205
    iget-object v0, p0, Lgar;->B:Lkzw;

    invoke-virtual {v0}, Lkzw;->p()Z

    move-result v0

    if-nez v0, :cond_1

    .line 206
    const-string v0, "PollCardViewGroup"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 207
    iget-object v0, p0, Lgar;->B:Lkzw;

    invoke-virtual {v0}, Lkzw;->b()I

    move-result v0

    iget-object v1, p0, Lgar;->B:Lkzw;

    .line 208
    invoke-virtual {v1}, Lkzw;->a()I

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x51

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Invalid poll : pollOptionsCount "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " pollOptionsWithMediaCount "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0}, Ljava/lang/Exception;-><init>()V

    .line 207
    :cond_0
    const/4 v0, 0x0

    .line 213
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method protected a(III)I
    .locals 4

    .prologue
    const/4 v3, -0x2

    .line 123
    iget-object v0, p0, Lgar;->y:Ljxx;

    if-nez v0, :cond_0

    .line 124
    invoke-super {p0, p1, p2, p3}, Lgbz;->a(III)I

    move-result v0

    .line 155
    :goto_0
    return v0

    .line 126
    :cond_0
    iget-boolean v0, p0, Lgar;->C:Z

    if-eqz v0, :cond_1

    .line 128
    iget-object v0, p0, Lgar;->y:Ljxx;

    invoke-interface {v0}, Ljxx;->d()Landroid/view/View;

    move-result-object v0

    .line 129
    invoke-virtual {v0, v3, v3}, Landroid/view/View;->measure(II)V

    .line 130
    sget-object v1, Lgar;->z:Llct;

    iget v1, v1, Llct;->m:I

    add-int/2addr v1, p1

    sget-object v2, Lgar;->z:Llct;

    iget v2, v2, Llct;->m:I

    add-int/2addr v2, p1

    .line 132
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v2, v3

    .line 133
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v3, p2

    .line 130
    invoke-virtual {v0, v1, p2, v2, v3}, Landroid/view/View;->layout(IIII)V

    .line 134
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    sget-object v1, Lgar;->z:Llct;

    iget v1, v1, Llct;->m:I

    add-int/2addr v0, v1

    add-int/2addr v0, p2

    .line 135
    goto :goto_0

    .line 137
    :cond_1
    invoke-direct {p0}, Lgar;->D()Z

    move-result v0

    if-nez v0, :cond_2

    .line 138
    invoke-super {p0, p1, p2, p3}, Lgbz;->a(III)I

    move-result v0

    goto :goto_0

    .line 141
    :cond_2
    iget-object v0, p0, Lgar;->y:Ljxx;

    invoke-interface {v0}, Ljxx;->b()Ljxw;

    move-result-object v0

    .line 142
    invoke-virtual {v0, p3}, Ljxw;->a(I)V

    .line 143
    invoke-virtual {v0, v3, v3}, Ljxw;->measure(II)V

    .line 144
    invoke-virtual {v0}, Ljxw;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v1, p1

    .line 145
    invoke-virtual {v0}, Ljxw;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v2, p2

    .line 144
    invoke-virtual {v0, p1, p2, v1, v2}, Ljxw;->layout(IIII)V

    .line 147
    invoke-virtual {v0}, Ljxw;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v0, p2

    .line 148
    iget-object v1, p0, Lgar;->y:Ljxx;

    invoke-interface {v1}, Ljxx;->c()Ljxw;

    move-result-object v1

    .line 149
    invoke-virtual {v1, p3}, Ljxw;->a(I)V

    .line 150
    invoke-virtual {v1, v3, v3}, Ljxw;->measure(II)V

    .line 152
    invoke-virtual {v1}, Ljxw;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v2, p1

    .line 153
    invoke-virtual {v1}, Ljxw;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v3, v0

    .line 151
    invoke-virtual {v1, p1, v0, v2, v3}, Ljxw;->layout(IIII)V

    .line 154
    invoke-virtual {v1}, Ljxw;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 155
    goto :goto_0
.end method

.method protected a(Landroid/graphics/Canvas;I)I
    .locals 2

    .prologue
    .line 176
    iget-object v0, p0, Lgar;->y:Ljxx;

    if-nez v0, :cond_1

    .line 187
    :cond_0
    :goto_0
    return p2

    .line 179
    :cond_1
    iget-boolean v0, p0, Lgar;->C:Z

    if-eqz v0, :cond_2

    .line 180
    iget-object v0, p0, Lgar;->y:Ljxx;

    invoke-interface {v0}, Ljxx;->d()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v0, p2

    sget-object v1, Lgar;->z:Llct;

    iget v1, v1, Llct;->m:I

    add-int p2, v0, v1

    goto :goto_0

    .line 183
    :cond_2
    invoke-direct {p0}, Lgar;->D()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 186
    iget-object v0, p0, Lgar;->y:Ljxx;

    invoke-interface {v0}, Ljxx;->b()Ljxw;

    move-result-object v0

    invoke-virtual {v0}, Ljxw;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v0, p2

    iget-object v1, p0, Lgar;->y:Ljxx;

    .line 187
    invoke-interface {v1}, Ljxx;->c()Ljxw;

    move-result-object v1

    invoke-virtual {v1}, Ljxw;->getMeasuredHeight()I

    move-result v1

    add-int p2, v0, v1

    goto :goto_0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 197
    invoke-super {p0}, Lgbz;->a()V

    .line 198
    iget-object v0, p0, Lgar;->y:Ljxx;

    if-eqz v0, :cond_0

    .line 199
    iget-object v0, p0, Lgar;->y:Ljxx;

    invoke-interface {v0}, Ljxx;->a()V

    .line 201
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lgar;->B:Lkzw;

    .line 202
    return-void
.end method

.method protected a(Landroid/database/Cursor;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 69
    iget-object v0, p0, Lgar;->y:Ljxx;

    if-nez v0, :cond_1

    .line 83
    :cond_0
    :goto_0
    return-void

    .line 73
    :cond_1
    iget-object v0, p0, Lgar;->A:Ljxy;

    .line 74
    invoke-virtual {p0}, Lgar;->t()Ljava/lang/String;

    move-result-object v3

    .line 73
    invoke-interface {v0, v3}, Ljxy;->a(Ljava/lang/String;)[B

    move-result-object v0

    .line 75
    if-eqz v0, :cond_2

    .line 77
    :goto_1
    if-eqz v0, :cond_0

    .line 80
    invoke-static {v0}, Lkzw;->a([B)Lkzw;

    move-result-object v0

    iput-object v0, p0, Lgar;->B:Lkzw;

    .line 81
    iget-object v0, p0, Lgar;->B:Lkzw;

    invoke-virtual {v0}, Lkzw;->q()I

    move-result v0

    if-eq v0, v1, :cond_3

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lgar;->C:Z

    .line 82
    iget-object v0, p0, Lgar;->y:Ljxx;

    iget-object v3, p0, Lgar;->B:Lkzw;

    invoke-virtual {p0}, Lgar;->t()Ljava/lang/String;

    move-result-object v4

    iget v5, p0, Lgar;->w:I

    if-eq v5, v1, :cond_4

    :goto_3
    invoke-interface {v0, v3, v4, v1}, Ljxx;->a(Lkzw;Ljava/lang/String;Z)V

    goto :goto_0

    .line 75
    :cond_2
    const/16 v0, 0x17

    .line 76
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    goto :goto_1

    :cond_3
    move v0, v2

    .line 81
    goto :goto_2

    :cond_4
    move v1, v2

    .line 82
    goto :goto_3
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    .line 218
    iget-object v0, p0, Lgar;->B:Lkzw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgar;->y:Ljxx;

    if-nez v0, :cond_1

    .line 226
    :cond_0
    :goto_0
    return-void

    .line 221
    :cond_1
    invoke-virtual {p0}, Lgar;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lhtp;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhtp;

    .line 222
    iget-object v1, p0, Lgar;->r:Ljava/lang/String;

    iget-object v2, p0, Lgar;->c:Ljava/lang/String;

    iget-boolean v3, p0, Lgar;->j:Z

    iget-boolean v4, p0, Lgar;->g:Z

    iget-object v5, p0, Lgar;->l:[B

    iget-object v6, p0, Lgar;->B:Lkzw;

    .line 223
    invoke-virtual {v6}, Lkzw;->r()I

    move-result v7

    const/4 v8, 0x1

    iget-object v6, p0, Lgar;->y:Ljxx;

    .line 225
    invoke-interface {v6}, Ljxx;->h()Ljava/lang/String;

    move-result-object v9

    move-object v6, p1

    .line 222
    invoke-interface/range {v0 .. v9}, Lhtp;->a(Ljava/lang/String;Ljava/lang/String;ZZ[BLandroid/os/Bundle;IZLjava/lang/String;)V

    goto :goto_0
.end method

.method protected aD_()Z
    .locals 1

    .prologue
    .line 192
    const/4 v0, 0x1

    return v0
.end method

.method protected a_(Landroid/database/Cursor;Llcr;I)V
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lgar;->y:Ljxx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgar;->B:Lkzw;

    if-nez v0, :cond_1

    .line 111
    :cond_0
    :goto_0
    return-void

    .line 97
    :cond_1
    iget-boolean v0, p0, Lgar;->C:Z

    if-eqz v0, :cond_2

    .line 98
    iget-object v0, p0, Lgar;->y:Ljxx;

    invoke-interface {v0}, Ljxx;->d()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgar;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 101
    :cond_2
    invoke-direct {p0}, Lgar;->D()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lgar;->y:Ljxx;

    invoke-interface {v0}, Ljxx;->b()Ljxw;

    move-result-object v0

    .line 105
    invoke-virtual {v0}, Ljxw;->b()V

    .line 106
    invoke-virtual {p0, v0}, Lgar;->addView(Landroid/view/View;)V

    .line 108
    iget-object v0, p0, Lgar;->y:Ljxx;

    invoke-interface {v0}, Ljxx;->c()Ljxw;

    move-result-object v0

    .line 109
    invoke-virtual {v0}, Ljxw;->b()V

    .line 110
    invoke-virtual {p0, v0}, Lgar;->addView(Landroid/view/View;)V

    goto :goto_0
.end method
