.class public final Lntd;
.super Loxq;
.source "PG"


# instance fields
.field public a:[I

.field public b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1489
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1492
    sget-object v0, Loxx;->a:[I

    iput-object v0, p0, Lntd;->a:[I

    .line 1489
    return-void
.end method


# virtual methods
.method public a()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1513
    .line 1514
    iget-object v1, p0, Lntd;->a:[I

    if-eqz v1, :cond_1

    iget-object v1, p0, Lntd;->a:[I

    array-length v1, v1

    if-lez v1, :cond_1

    .line 1516
    iget-object v2, p0, Lntd;->a:[I

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v0, v3, :cond_0

    aget v4, v2, v0

    .line 1518
    invoke-static {v4}, Loxo;->i(I)I

    move-result v4

    add-int/2addr v1, v4

    .line 1516
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1521
    :cond_0
    iget-object v0, p0, Lntd;->a:[I

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v1

    .line 1523
    :cond_1
    iget-object v1, p0, Lntd;->b:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1524
    const/4 v1, 0x2

    iget-object v2, p0, Lntd;->b:Ljava/lang/String;

    .line 1525
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1527
    :cond_2
    iget-object v1, p0, Lntd;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1528
    iput v0, p0, Lntd;->ai:I

    .line 1529
    return v0
.end method

.method public a(Loxn;)Lntd;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1537
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1538
    sparse-switch v0, :sswitch_data_0

    .line 1542
    iget-object v1, p0, Lntd;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1543
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lntd;->ah:Ljava/util/List;

    .line 1546
    :cond_1
    iget-object v1, p0, Lntd;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1548
    :sswitch_0
    return-object p0

    .line 1553
    :sswitch_1
    const/16 v0, 0x8

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 1554
    iget-object v0, p0, Lntd;->a:[I

    array-length v0, v0

    .line 1555
    add-int/2addr v1, v0

    new-array v1, v1, [I

    .line 1556
    iget-object v2, p0, Lntd;->a:[I

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1557
    iput-object v1, p0, Lntd;->a:[I

    .line 1558
    :goto_1
    iget-object v1, p0, Lntd;->a:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 1559
    iget-object v1, p0, Lntd;->a:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    .line 1560
    invoke-virtual {p1}, Loxn;->a()I

    .line 1558
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1563
    :cond_2
    iget-object v1, p0, Lntd;->a:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    goto :goto_0

    .line 1567
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lntd;->b:Ljava/lang/String;

    goto :goto_0

    .line 1538
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 1499
    iget-object v0, p0, Lntd;->a:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lntd;->a:[I

    array-length v0, v0

    if-lez v0, :cond_0

    .line 1500
    iget-object v1, p0, Lntd;->a:[I

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget v3, v1, v0

    .line 1501
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->a(II)V

    .line 1500
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1504
    :cond_0
    iget-object v0, p0, Lntd;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1505
    const/4 v0, 0x2

    iget-object v1, p0, Lntd;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1507
    :cond_1
    iget-object v0, p0, Lntd;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1509
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1485
    invoke-virtual {p0, p1}, Lntd;->a(Loxn;)Lntd;

    move-result-object v0

    return-object v0
.end method
