.class final Leeu;
.super Lfhh;
.source "PG"


# instance fields
.field private synthetic a:Leet;


# direct methods
.method constructor <init>(Leet;)V
    .locals 0

    .prologue
    .line 341
    iput-object p1, p0, Leeu;->a:Leet;

    invoke-direct {p0}, Lfhh;-><init>()V

    return-void
.end method


# virtual methods
.method public A(ILfib;)V
    .locals 2

    .prologue
    .line 417
    const-string v0, "HostedProfileFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 418
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x34

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "onGetBestPhotoTilesComplete(); requestId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 420
    :cond_0
    iget-object v0, p0, Leeu;->a:Leet;

    invoke-static {v0, p1, p2}, Leet;->b(Leet;ILfib;)V

    .line 421
    return-void
.end method

.method public L(ILfib;)V
    .locals 1

    .prologue
    .line 366
    iget-object v0, p0, Leeu;->a:Leet;

    invoke-virtual {v0, p1, p2}, Leet;->b(ILfib;)V

    .line 367
    return-void
.end method

.method public M(ILfib;)V
    .locals 2

    .prologue
    .line 389
    const-string v0, "HostedProfileFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 390
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x30

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "onSetCoverPhotoComplete(); requestId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 393
    :cond_0
    iget-object v0, p0, Leeu;->a:Leet;

    invoke-static {v0, p1, p2}, Leet;->a(Leet;ILfib;)V

    .line 394
    return-void
.end method

.method public N(ILfib;)V
    .locals 2

    .prologue
    .line 399
    const-string v0, "HostedProfileFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 400
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x30

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "onSetCoverPhotoComplete(); requestId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 403
    :cond_0
    iget-object v0, p0, Leeu;->a:Leet;

    invoke-static {v0, p1, p2}, Leet;->a(Leet;ILfib;)V

    .line 404
    return-void
.end method

.method public a(IILfib;)V
    .locals 5

    .prologue
    .line 372
    const-string v0, "HostedProfileFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 373
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x35

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "onUploadProfilePhotoComplete(); requestId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 376
    :cond_0
    iget-object v0, p0, Leeu;->a:Leet;

    invoke-virtual {v0, p1, p3}, Leet;->a(ILfib;)V

    .line 378
    if-eqz p3, :cond_1

    invoke-virtual {p3}, Lfib;->e()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p3}, Lfib;->d()Ljava/lang/Exception;

    move-result-object v0

    if-nez v0, :cond_1

    .line 380
    iget-object v0, p0, Leeu;->a:Leet;

    iget-object v1, p0, Leeu;->a:Leet;

    invoke-virtual {v1}, Leet;->n()Lz;

    move-result-object v1

    iget-object v2, p0, Leeu;->a:Leet;

    iget-object v2, v2, Leet;->R:Lhee;

    .line 381
    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    iget-object v3, p0, Leeu;->a:Leet;

    invoke-static {v3}, Leet;->a(Leet;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    .line 380
    invoke-static {v1, v2, v3, v4}, Lcom/google/android/apps/plus/service/EsService;->f(Landroid/content/Context;ILjava/lang/String;Z)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Leet;->a(Leet;Ljava/lang/Integer;)Ljava/lang/Integer;

    .line 382
    iget-object v0, p0, Leeu;->a:Leet;

    invoke-virtual {v0}, Leet;->ay()V

    .line 384
    :cond_1
    return-void
.end method

.method public a(ILjava/lang/String;ZLfib;)V
    .locals 1

    .prologue
    .line 360
    iget-object v0, p0, Leeu;->a:Leet;

    invoke-virtual {v0, p1, p3, p4}, Leet;->b(IZLfib;)V

    .line 361
    return-void
.end method

.method public a(IZLfib;)V
    .locals 1

    .prologue
    .line 354
    iget-object v0, p0, Leeu;->a:Leet;

    invoke-virtual {v0, p1, p2, p3}, Leet;->a(IZLfib;)V

    .line 355
    return-void
.end method

.method public l(ILfib;)V
    .locals 2

    .prologue
    .line 345
    const-string v0, "HostedProfileFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 346
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x37

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "onGetProfileAndContactComplete(); requestId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 348
    :cond_0
    iget-object v0, p0, Leeu;->a:Leet;

    invoke-virtual {v0, p1, p2}, Leet;->a(ILfib;)V

    .line 349
    return-void
.end method

.method public u(ILfib;)V
    .locals 2

    .prologue
    .line 409
    const-string v0, "HostedProfileFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 410
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x2f

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "onGetAlbumListComplete(); requestId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 412
    :cond_0
    iget-object v0, p0, Leeu;->a:Leet;

    invoke-static {v0, p1, p2}, Leet;->b(Leet;ILfib;)V

    .line 413
    return-void
.end method
