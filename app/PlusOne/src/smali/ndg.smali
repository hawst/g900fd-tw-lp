.class public final Lndg;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Long;

.field public b:Ljava/lang/Long;

.field public c:Ljava/lang/Integer;

.field public d:[Lndc;

.field public e:[Lndc;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 417
    invoke-direct {p0}, Loxq;-><init>()V

    .line 426
    sget-object v0, Lndc;->a:[Lndc;

    iput-object v0, p0, Lndg;->d:[Lndc;

    .line 429
    sget-object v0, Lndc;->a:[Lndc;

    iput-object v0, p0, Lndg;->e:[Lndc;

    .line 417
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 463
    .line 464
    iget-object v0, p0, Lndg;->a:Ljava/lang/Long;

    if-eqz v0, :cond_6

    .line 465
    const/4 v0, 0x1

    iget-object v2, p0, Lndg;->a:Ljava/lang/Long;

    .line 466
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Loxo;->f(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 468
    :goto_0
    iget-object v2, p0, Lndg;->b:Ljava/lang/Long;

    if-eqz v2, :cond_0

    .line 469
    const/4 v2, 0x2

    iget-object v3, p0, Lndg;->b:Ljava/lang/Long;

    .line 470
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Loxo;->f(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 472
    :cond_0
    iget-object v2, p0, Lndg;->c:Ljava/lang/Integer;

    if-eqz v2, :cond_1

    .line 473
    const/4 v2, 0x3

    iget-object v3, p0, Lndg;->c:Ljava/lang/Integer;

    .line 474
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 476
    :cond_1
    iget-object v2, p0, Lndg;->d:[Lndc;

    if-eqz v2, :cond_3

    .line 477
    iget-object v3, p0, Lndg;->d:[Lndc;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_3

    aget-object v5, v3, v2

    .line 478
    if-eqz v5, :cond_2

    .line 479
    const/4 v6, 0x4

    .line 480
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 477
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 484
    :cond_3
    iget-object v2, p0, Lndg;->e:[Lndc;

    if-eqz v2, :cond_5

    .line 485
    iget-object v2, p0, Lndg;->e:[Lndc;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    .line 486
    if-eqz v4, :cond_4

    .line 487
    const/4 v5, 0x5

    .line 488
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 485
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 492
    :cond_5
    iget-object v1, p0, Lndg;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 493
    iput v0, p0, Lndg;->ai:I

    .line 494
    return v0

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lndg;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 502
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 503
    sparse-switch v0, :sswitch_data_0

    .line 507
    iget-object v2, p0, Lndg;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 508
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lndg;->ah:Ljava/util/List;

    .line 511
    :cond_1
    iget-object v2, p0, Lndg;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 513
    :sswitch_0
    return-object p0

    .line 518
    :sswitch_1
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lndg;->a:Ljava/lang/Long;

    goto :goto_0

    .line 522
    :sswitch_2
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lndg;->b:Ljava/lang/Long;

    goto :goto_0

    .line 526
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lndg;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 530
    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 531
    iget-object v0, p0, Lndg;->d:[Lndc;

    if-nez v0, :cond_3

    move v0, v1

    .line 532
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lndc;

    .line 533
    iget-object v3, p0, Lndg;->d:[Lndc;

    if-eqz v3, :cond_2

    .line 534
    iget-object v3, p0, Lndg;->d:[Lndc;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 536
    :cond_2
    iput-object v2, p0, Lndg;->d:[Lndc;

    .line 537
    :goto_2
    iget-object v2, p0, Lndg;->d:[Lndc;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 538
    iget-object v2, p0, Lndg;->d:[Lndc;

    new-instance v3, Lndc;

    invoke-direct {v3}, Lndc;-><init>()V

    aput-object v3, v2, v0

    .line 539
    iget-object v2, p0, Lndg;->d:[Lndc;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 540
    invoke-virtual {p1}, Loxn;->a()I

    .line 537
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 531
    :cond_3
    iget-object v0, p0, Lndg;->d:[Lndc;

    array-length v0, v0

    goto :goto_1

    .line 543
    :cond_4
    iget-object v2, p0, Lndg;->d:[Lndc;

    new-instance v3, Lndc;

    invoke-direct {v3}, Lndc;-><init>()V

    aput-object v3, v2, v0

    .line 544
    iget-object v2, p0, Lndg;->d:[Lndc;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 548
    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 549
    iget-object v0, p0, Lndg;->e:[Lndc;

    if-nez v0, :cond_6

    move v0, v1

    .line 550
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lndc;

    .line 551
    iget-object v3, p0, Lndg;->e:[Lndc;

    if-eqz v3, :cond_5

    .line 552
    iget-object v3, p0, Lndg;->e:[Lndc;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 554
    :cond_5
    iput-object v2, p0, Lndg;->e:[Lndc;

    .line 555
    :goto_4
    iget-object v2, p0, Lndg;->e:[Lndc;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    .line 556
    iget-object v2, p0, Lndg;->e:[Lndc;

    new-instance v3, Lndc;

    invoke-direct {v3}, Lndc;-><init>()V

    aput-object v3, v2, v0

    .line 557
    iget-object v2, p0, Lndg;->e:[Lndc;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 558
    invoke-virtual {p1}, Loxn;->a()I

    .line 555
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 549
    :cond_6
    iget-object v0, p0, Lndg;->e:[Lndc;

    array-length v0, v0

    goto :goto_3

    .line 561
    :cond_7
    iget-object v2, p0, Lndg;->e:[Lndc;

    new-instance v3, Lndc;

    invoke-direct {v3}, Lndc;-><init>()V

    aput-object v3, v2, v0

    .line 562
    iget-object v2, p0, Lndg;->e:[Lndc;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 503
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 434
    iget-object v1, p0, Lndg;->a:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 435
    const/4 v1, 0x1

    iget-object v2, p0, Lndg;->a:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Loxo;->b(IJ)V

    .line 437
    :cond_0
    iget-object v1, p0, Lndg;->b:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 438
    const/4 v1, 0x2

    iget-object v2, p0, Lndg;->b:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Loxo;->b(IJ)V

    .line 440
    :cond_1
    iget-object v1, p0, Lndg;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 441
    const/4 v1, 0x3

    iget-object v2, p0, Lndg;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 443
    :cond_2
    iget-object v1, p0, Lndg;->d:[Lndc;

    if-eqz v1, :cond_4

    .line 444
    iget-object v2, p0, Lndg;->d:[Lndc;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 445
    if-eqz v4, :cond_3

    .line 446
    const/4 v5, 0x4

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 444
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 450
    :cond_4
    iget-object v1, p0, Lndg;->e:[Lndc;

    if-eqz v1, :cond_6

    .line 451
    iget-object v1, p0, Lndg;->e:[Lndc;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_6

    aget-object v3, v1, v0

    .line 452
    if-eqz v3, :cond_5

    .line 453
    const/4 v4, 0x5

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 451
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 457
    :cond_6
    iget-object v0, p0, Lndg;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 459
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 413
    invoke-virtual {p0, p1}, Lndg;->a(Loxn;)Lndg;

    move-result-object v0

    return-object v0
.end method
