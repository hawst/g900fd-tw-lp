.class public final Lmtc;
.super Loxq;
.source "PG"


# instance fields
.field public a:I

.field private b:Ljava/lang/Integer;

.field private c:Ljava/lang/Integer;

.field private d:Ljava/lang/Boolean;

.field private e:Ljava/lang/Boolean;

.field private f:Ljava/lang/Boolean;

.field private g:[I

.field private h:Ljava/lang/Integer;

.field private i:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3651
    invoke-direct {p0}, Loxq;-><init>()V

    .line 3662
    const/high16 v0, -0x80000000

    iput v0, p0, Lmtc;->a:I

    .line 3667
    sget-object v0, Loxx;->a:[I

    iput-object v0, p0, Lmtc;->g:[I

    .line 3651
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 3711
    .line 3712
    iget-object v0, p0, Lmtc;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_9

    .line 3713
    const/4 v0, 0x1

    iget-object v2, p0, Lmtc;->b:Ljava/lang/Integer;

    .line 3714
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3716
    :goto_0
    iget-object v2, p0, Lmtc;->c:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    .line 3717
    const/4 v2, 0x2

    iget-object v3, p0, Lmtc;->c:Ljava/lang/Integer;

    .line 3718
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 3720
    :cond_0
    iget-object v2, p0, Lmtc;->d:Ljava/lang/Boolean;

    if-eqz v2, :cond_1

    .line 3721
    const/4 v2, 0x3

    iget-object v3, p0, Lmtc;->d:Ljava/lang/Boolean;

    .line 3722
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 3724
    :cond_1
    iget-object v2, p0, Lmtc;->e:Ljava/lang/Boolean;

    if-eqz v2, :cond_2

    .line 3725
    const/4 v2, 0x4

    iget-object v3, p0, Lmtc;->e:Ljava/lang/Boolean;

    .line 3726
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 3728
    :cond_2
    iget v2, p0, Lmtc;->a:I

    const/high16 v3, -0x80000000

    if-eq v2, v3, :cond_3

    .line 3729
    const/4 v2, 0x5

    iget v3, p0, Lmtc;->a:I

    .line 3730
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 3732
    :cond_3
    iget-object v2, p0, Lmtc;->f:Ljava/lang/Boolean;

    if-eqz v2, :cond_4

    .line 3733
    const/4 v2, 0x6

    iget-object v3, p0, Lmtc;->f:Ljava/lang/Boolean;

    .line 3734
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 3736
    :cond_4
    iget-object v2, p0, Lmtc;->g:[I

    if-eqz v2, :cond_6

    iget-object v2, p0, Lmtc;->g:[I

    array-length v2, v2

    if-lez v2, :cond_6

    .line 3738
    iget-object v3, p0, Lmtc;->g:[I

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_5

    aget v5, v3, v1

    .line 3740
    invoke-static {v5}, Loxo;->i(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 3738
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 3742
    :cond_5
    add-int/2addr v0, v2

    .line 3743
    iget-object v1, p0, Lmtc;->g:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3745
    :cond_6
    iget-object v1, p0, Lmtc;->h:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    .line 3746
    const/16 v1, 0x8

    iget-object v2, p0, Lmtc;->h:Ljava/lang/Integer;

    .line 3747
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3749
    :cond_7
    iget-object v1, p0, Lmtc;->i:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    .line 3750
    const/16 v1, 0x9

    iget-object v2, p0, Lmtc;->i:Ljava/lang/Boolean;

    .line 3751
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3753
    :cond_8
    iget-object v1, p0, Lmtc;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3754
    iput v0, p0, Lmtc;->ai:I

    .line 3755
    return v0

    :cond_9
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lmtc;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 3763
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3764
    sparse-switch v0, :sswitch_data_0

    .line 3768
    iget-object v1, p0, Lmtc;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 3769
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmtc;->ah:Ljava/util/List;

    .line 3772
    :cond_1
    iget-object v1, p0, Lmtc;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3774
    :sswitch_0
    return-object p0

    .line 3779
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmtc;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 3783
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmtc;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 3787
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmtc;->d:Ljava/lang/Boolean;

    goto :goto_0

    .line 3791
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmtc;->e:Ljava/lang/Boolean;

    goto :goto_0

    .line 3795
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 3796
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa

    if-eq v0, v1, :cond_2

    const/16 v1, 0xb

    if-eq v0, v1, :cond_2

    const/16 v1, 0xc

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd

    if-eq v0, v1, :cond_2

    const/16 v1, 0xe

    if-eq v0, v1, :cond_2

    const/16 v1, 0xf

    if-eq v0, v1, :cond_2

    const/16 v1, 0x10

    if-eq v0, v1, :cond_2

    const/16 v1, 0x11

    if-eq v0, v1, :cond_2

    const/16 v1, 0x12

    if-eq v0, v1, :cond_2

    const/16 v1, 0x13

    if-eq v0, v1, :cond_2

    const/16 v1, 0x14

    if-eq v0, v1, :cond_2

    const/16 v1, 0x15

    if-ne v0, v1, :cond_3

    .line 3818
    :cond_2
    iput v0, p0, Lmtc;->a:I

    goto/16 :goto_0

    .line 3820
    :cond_3
    iput v3, p0, Lmtc;->a:I

    goto/16 :goto_0

    .line 3825
    :sswitch_6
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmtc;->f:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 3829
    :sswitch_7
    const/16 v0, 0x38

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 3830
    iget-object v0, p0, Lmtc;->g:[I

    array-length v0, v0

    .line 3831
    add-int/2addr v1, v0

    new-array v1, v1, [I

    .line 3832
    iget-object v2, p0, Lmtc;->g:[I

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3833
    iput-object v1, p0, Lmtc;->g:[I

    .line 3834
    :goto_1
    iget-object v1, p0, Lmtc;->g:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_4

    .line 3835
    iget-object v1, p0, Lmtc;->g:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    .line 3836
    invoke-virtual {p1}, Loxn;->a()I

    .line 3834
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 3839
    :cond_4
    iget-object v1, p0, Lmtc;->g:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    goto/16 :goto_0

    .line 3843
    :sswitch_8
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmtc;->h:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 3847
    :sswitch_9
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmtc;->i:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 3764
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 3676
    iget-object v0, p0, Lmtc;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 3677
    const/4 v0, 0x1

    iget-object v1, p0, Lmtc;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 3679
    :cond_0
    iget-object v0, p0, Lmtc;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 3680
    const/4 v0, 0x2

    iget-object v1, p0, Lmtc;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 3682
    :cond_1
    iget-object v0, p0, Lmtc;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 3683
    const/4 v0, 0x3

    iget-object v1, p0, Lmtc;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 3685
    :cond_2
    iget-object v0, p0, Lmtc;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 3686
    const/4 v0, 0x4

    iget-object v1, p0, Lmtc;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 3688
    :cond_3
    iget v0, p0, Lmtc;->a:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_4

    .line 3689
    const/4 v0, 0x5

    iget v1, p0, Lmtc;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 3691
    :cond_4
    iget-object v0, p0, Lmtc;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 3692
    const/4 v0, 0x6

    iget-object v1, p0, Lmtc;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 3694
    :cond_5
    iget-object v0, p0, Lmtc;->g:[I

    if-eqz v0, :cond_6

    iget-object v0, p0, Lmtc;->g:[I

    array-length v0, v0

    if-lez v0, :cond_6

    .line 3695
    iget-object v1, p0, Lmtc;->g:[I

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_6

    aget v3, v1, v0

    .line 3696
    const/4 v4, 0x7

    invoke-virtual {p1, v4, v3}, Loxo;->a(II)V

    .line 3695
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3699
    :cond_6
    iget-object v0, p0, Lmtc;->h:Ljava/lang/Integer;

    if-eqz v0, :cond_7

    .line 3700
    const/16 v0, 0x8

    iget-object v1, p0, Lmtc;->h:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 3702
    :cond_7
    iget-object v0, p0, Lmtc;->i:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    .line 3703
    const/16 v0, 0x9

    iget-object v1, p0, Lmtc;->i:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 3705
    :cond_8
    iget-object v0, p0, Lmtc;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3707
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3647
    invoke-virtual {p0, p1}, Lmtc;->a(Loxn;)Lmtc;

    move-result-object v0

    return-object v0
.end method
