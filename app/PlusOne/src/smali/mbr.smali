.class public final Lmbr;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnhr;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 29644
    invoke-direct {p0}, Loxq;-><init>()V

    .line 29647
    iput-object v0, p0, Lmbr;->apiHeader:Llyr;

    .line 29650
    iput-object v0, p0, Lmbr;->a:Lnhr;

    .line 29644
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 29667
    const/4 v0, 0x0

    .line 29668
    iget-object v1, p0, Lmbr;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 29669
    const/4 v0, 0x1

    iget-object v1, p0, Lmbr;->apiHeader:Llyr;

    .line 29670
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 29672
    :cond_0
    iget-object v1, p0, Lmbr;->a:Lnhr;

    if-eqz v1, :cond_1

    .line 29673
    const/4 v1, 0x2

    iget-object v2, p0, Lmbr;->a:Lnhr;

    .line 29674
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 29676
    :cond_1
    iget-object v1, p0, Lmbr;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 29677
    iput v0, p0, Lmbr;->ai:I

    .line 29678
    return v0
.end method

.method public a(Loxn;)Lmbr;
    .locals 2

    .prologue
    .line 29686
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 29687
    sparse-switch v0, :sswitch_data_0

    .line 29691
    iget-object v1, p0, Lmbr;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 29692
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmbr;->ah:Ljava/util/List;

    .line 29695
    :cond_1
    iget-object v1, p0, Lmbr;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 29697
    :sswitch_0
    return-object p0

    .line 29702
    :sswitch_1
    iget-object v0, p0, Lmbr;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 29703
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmbr;->apiHeader:Llyr;

    .line 29705
    :cond_2
    iget-object v0, p0, Lmbr;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 29709
    :sswitch_2
    iget-object v0, p0, Lmbr;->a:Lnhr;

    if-nez v0, :cond_3

    .line 29710
    new-instance v0, Lnhr;

    invoke-direct {v0}, Lnhr;-><init>()V

    iput-object v0, p0, Lmbr;->a:Lnhr;

    .line 29712
    :cond_3
    iget-object v0, p0, Lmbr;->a:Lnhr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 29687
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 29655
    iget-object v0, p0, Lmbr;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 29656
    const/4 v0, 0x1

    iget-object v1, p0, Lmbr;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 29658
    :cond_0
    iget-object v0, p0, Lmbr;->a:Lnhr;

    if-eqz v0, :cond_1

    .line 29659
    const/4 v0, 0x2

    iget-object v1, p0, Lmbr;->a:Lnhr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 29661
    :cond_1
    iget-object v0, p0, Lmbr;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 29663
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 29640
    invoke-virtual {p0, p1}, Lmbr;->a(Loxn;)Lmbr;

    move-result-object v0

    return-object v0
.end method
