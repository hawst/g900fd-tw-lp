.class public final Lhwd;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static K:Lhwd;


# instance fields
.field public final A:Landroid/graphics/Paint;

.field public final B:Landroid/graphics/Paint;

.field public final C:Landroid/graphics/Bitmap;

.field public final D:Landroid/graphics/Bitmap;

.field public final E:Landroid/graphics/Bitmap;

.field public final F:Landroid/graphics/Bitmap;

.field public final G:I

.field public final H:I

.field public final I:I

.field public final J:I

.field public final a:Lfo;

.field public final b:Landroid/text/style/StyleSpan;

.field public final c:I

.field public final d:I

.field public final e:I

.field public final f:I

.field public final g:I

.field public final h:I

.field public final i:I

.field public final j:I

.field public final k:I

.field public final l:Landroid/graphics/Rect;

.field public final m:I

.field public final n:I

.field public final o:I

.field public final p:I

.field public final q:I

.field public final r:I

.field public final s:I

.field public final t:I

.field public final u:Landroid/graphics/Rect;

.field public final v:[Landroid/graphics/Paint;

.field public final w:[Landroid/graphics/Paint;

.field public final x:Landroid/graphics/drawable/NinePatchDrawable;

.field public final y:Landroid/graphics/drawable/NinePatchDrawable;

.field public final z:I


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x0

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 132
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 134
    invoke-static {}, Lfo;->a()Lfo;

    move-result-object v0

    iput-object v0, p0, Lhwd;->a:Lfo;

    .line 136
    const v0, 0x7f0c0045

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    .line 139
    new-instance v0, Landroid/text/style/StyleSpan;

    invoke-direct {v0, v3}, Landroid/text/style/StyleSpan;-><init>(I)V

    iput-object v0, p0, Lhwd;->b:Landroid/text/style/StyleSpan;

    .line 141
    const v0, 0x7f0d0189

    .line 142
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    .line 144
    const v0, 0x7f0b0149

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lhwd;->c:I

    .line 146
    const v0, 0x7f0d0187

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lhwd;->d:I

    .line 147
    const v0, 0x7f0d0188

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lhwd;->e:I

    .line 149
    const v0, 0x7f0d018a

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lhwd;->f:I

    .line 152
    const v0, 0x7f0d018b

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lhwd;->g:I

    .line 153
    const v0, 0x7f0d018c

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lhwd;->h:I

    .line 155
    const v0, 0x7f0d0199

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lhwd;->i:I

    .line 156
    const v0, 0x7f0d019a

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lhwd;->j:I

    .line 158
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lhwd;->l:Landroid/graphics/Rect;

    .line 160
    const v0, 0x7f0d019b

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lhwd;->k:I

    .line 161
    const v0, 0x7f0d0195

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lhwd;->m:I

    .line 163
    const v0, 0x7f0d01a2

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lhwd;->n:I

    .line 164
    const v0, 0x7f0d01a3

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lhwd;->o:I

    .line 165
    const v0, 0x7f0d01a4

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lhwd;->p:I

    .line 167
    const v0, 0x7f0c004f

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lhwd;->q:I

    .line 168
    const v0, 0x7f0c004e

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lhwd;->r:I

    .line 169
    const v0, 0x7f0c004d

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lhwd;->s:I

    .line 171
    const v0, 0x106000b

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lhwd;->t:I

    .line 172
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lhwd;->u:Landroid/graphics/Rect;

    .line 174
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v4}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lhwd;->A:Landroid/graphics/Paint;

    .line 176
    new-array v0, v6, [Landroid/graphics/Paint;

    iput-object v0, p0, Lhwd;->w:[Landroid/graphics/Paint;

    .line 177
    iget-object v0, p0, Lhwd;->w:[Landroid/graphics/Paint;

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    aput-object v2, v0, v5

    .line 178
    iget-object v0, p0, Lhwd;->w:[Landroid/graphics/Paint;

    aget-object v0, v0, v5

    const v2, 0x7f0b0104

    .line 179
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 178
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 180
    iget-object v0, p0, Lhwd;->w:[Landroid/graphics/Paint;

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    aput-object v2, v0, v3

    .line 181
    iget-object v0, p0, Lhwd;->w:[Landroid/graphics/Paint;

    aget-object v0, v0, v3

    const v2, 0x7f0b0105

    .line 182
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 181
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 183
    iget-object v0, p0, Lhwd;->w:[Landroid/graphics/Paint;

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    aput-object v2, v0, v4

    .line 184
    iget-object v0, p0, Lhwd;->w:[Landroid/graphics/Paint;

    aget-object v0, v0, v4

    const v2, 0x7f0b0106

    .line 185
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 184
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 187
    new-array v0, v6, [Landroid/graphics/Paint;

    iput-object v0, p0, Lhwd;->v:[Landroid/graphics/Paint;

    .line 188
    iget-object v0, p0, Lhwd;->v:[Landroid/graphics/Paint;

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    aput-object v2, v0, v5

    .line 189
    iget-object v0, p0, Lhwd;->v:[Landroid/graphics/Paint;

    aget-object v0, v0, v5

    const v2, 0x7f0b0107

    .line 190
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 189
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 191
    iget-object v0, p0, Lhwd;->v:[Landroid/graphics/Paint;

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    aput-object v2, v0, v3

    .line 192
    iget-object v0, p0, Lhwd;->v:[Landroid/graphics/Paint;

    aget-object v0, v0, v3

    const v2, 0x7f0b0108

    .line 193
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 192
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 194
    iget-object v0, p0, Lhwd;->v:[Landroid/graphics/Paint;

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    aput-object v2, v0, v4

    .line 195
    iget-object v0, p0, Lhwd;->v:[Landroid/graphics/Paint;

    aget-object v0, v0, v4

    const v2, 0x7f0b0109

    .line 196
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 195
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 198
    const v0, 0x7f020079

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/NinePatchDrawable;

    iput-object v0, p0, Lhwd;->x:Landroid/graphics/drawable/NinePatchDrawable;

    .line 199
    const v0, 0x7f02007a

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/NinePatchDrawable;

    iput-object v0, p0, Lhwd;->y:Landroid/graphics/drawable/NinePatchDrawable;

    .line 202
    const v0, 0x7f0d019d

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lhwd;->z:I

    .line 204
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lhwd;->B:Landroid/graphics/Paint;

    .line 205
    iget-object v0, p0, Lhwd;->B:Landroid/graphics/Paint;

    const v2, 0x7f0b0101

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 206
    iget-object v0, p0, Lhwd;->B:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 207
    iget-object v0, p0, Lhwd;->B:Landroid/graphics/Paint;

    const v2, 0x7f0d019c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 209
    const v0, 0x7f0d0245

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    .line 213
    const v0, 0x7f0203eb

    invoke-static {v1, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lhwd;->C:Landroid/graphics/Bitmap;

    .line 214
    const v0, 0x7f0203e3

    invoke-static {v1, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lhwd;->D:Landroid/graphics/Bitmap;

    .line 215
    const v0, 0x7f0203f7

    invoke-static {v1, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lhwd;->E:Landroid/graphics/Bitmap;

    .line 216
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v2, "ic_clx_16"

    const-string v3, "drawable"

    .line 217
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    .line 216
    invoke-virtual {v0, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 218
    if-eqz v0, :cond_0

    .line 220
    invoke-static {v1, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lhwd;->F:Landroid/graphics/Bitmap;

    .line 224
    :goto_0
    const v0, 0x7f0203ec

    invoke-static {v1, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    .line 227
    const v0, 0x7f0204d1

    iput v0, p0, Lhwd;->G:I

    .line 228
    const v0, 0x7f0204da

    iput v0, p0, Lhwd;->H:I

    .line 229
    const v0, 0x7f0204ea

    iput v0, p0, Lhwd;->I:I

    .line 231
    const v0, 0x7f0d0196

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lhwd;->J:I

    .line 232
    return-void

    .line 222
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lhwd;->F:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)Lhwd;
    .locals 1

    .prologue
    .line 235
    sget-object v0, Lhwd;->K:Lhwd;

    if-nez v0, :cond_0

    .line 236
    invoke-static {}, Llsx;->b()V

    .line 237
    new-instance v0, Lhwd;

    invoke-direct {v0, p0}, Lhwd;-><init>(Landroid/content/Context;)V

    sput-object v0, Lhwd;->K:Lhwd;

    .line 239
    :cond_0
    sget-object v0, Lhwd;->K:Lhwd;

    return-object v0
.end method
