.class public final Lodp;
.super Loxq;
.source "PG"


# instance fields
.field private a:I

.field private b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/high16 v0, -0x80000000

    .line 155
    invoke-direct {p0}, Loxq;-><init>()V

    .line 165
    iput v0, p0, Lodp;->a:I

    .line 168
    iput v0, p0, Lodp;->b:I

    .line 155
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 185
    const/4 v0, 0x0

    .line 186
    iget v1, p0, Lodp;->a:I

    if-eq v1, v2, :cond_0

    .line 187
    const/4 v0, 0x1

    iget v1, p0, Lodp;->a:I

    .line 188
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 190
    :cond_0
    iget v1, p0, Lodp;->b:I

    if-eq v1, v2, :cond_1

    .line 191
    const/4 v1, 0x2

    iget v2, p0, Lodp;->b:I

    .line 192
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 194
    :cond_1
    iget-object v1, p0, Lodp;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 195
    iput v0, p0, Lodp;->ai:I

    .line 196
    return v0
.end method

.method public a(Loxn;)Lodp;
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 204
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 205
    sparse-switch v0, :sswitch_data_0

    .line 209
    iget-object v1, p0, Lodp;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 210
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lodp;->ah:Ljava/util/List;

    .line 213
    :cond_1
    iget-object v1, p0, Lodp;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 215
    :sswitch_0
    return-object p0

    .line 220
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 221
    if-eqz v0, :cond_2

    if-eq v0, v3, :cond_2

    if-eq v0, v4, :cond_2

    if-ne v0, v5, :cond_3

    .line 225
    :cond_2
    iput v0, p0, Lodp;->a:I

    goto :goto_0

    .line 227
    :cond_3
    iput v2, p0, Lodp;->a:I

    goto :goto_0

    .line 232
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 233
    if-eqz v0, :cond_4

    if-eq v0, v3, :cond_4

    if-eq v0, v4, :cond_4

    if-ne v0, v5, :cond_5

    .line 237
    :cond_4
    iput v0, p0, Lodp;->b:I

    goto :goto_0

    .line 239
    :cond_5
    iput v2, p0, Lodp;->b:I

    goto :goto_0

    .line 205
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 173
    iget v0, p0, Lodp;->a:I

    if-eq v0, v2, :cond_0

    .line 174
    const/4 v0, 0x1

    iget v1, p0, Lodp;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 176
    :cond_0
    iget v0, p0, Lodp;->b:I

    if-eq v0, v2, :cond_1

    .line 177
    const/4 v0, 0x2

    iget v1, p0, Lodp;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 179
    :cond_1
    iget-object v0, p0, Lodp;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 181
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 151
    invoke-virtual {p0, p1}, Lodp;->a(Loxn;)Lodp;

    move-result-object v0

    return-object v0
.end method
