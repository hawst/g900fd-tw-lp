.class public final Ldqt;
.super Lllq;
.source "PG"


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:F

.field public final c:F

.field public final d:F

.field public final e:F

.field public final f:I


# direct methods
.method private constructor <init>(Ljava/lang/String;FFFFI)V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0}, Lllq;-><init>()V

    .line 74
    iput-object p1, p0, Ldqt;->a:Ljava/lang/String;

    .line 75
    iput p2, p0, Ldqt;->b:F

    .line 76
    iput p3, p0, Ldqt;->c:F

    .line 77
    iput p4, p0, Ldqt;->d:F

    .line 78
    iput p5, p0, Ldqt;->e:F

    .line 79
    iput p6, p0, Ldqt;->f:I

    .line 80
    return-void
.end method

.method public static a([B)Ldqt;
    .locals 8

    .prologue
    .line 49
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, p0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 50
    new-instance v7, Ljava/io/DataInputStream;

    invoke-direct {v7, v0}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 52
    :try_start_0
    invoke-virtual {v7}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v1

    .line 53
    invoke-virtual {v7}, Ljava/io/DataInputStream;->readFloat()F

    move-result v2

    .line 54
    invoke-virtual {v7}, Ljava/io/DataInputStream;->readFloat()F

    move-result v3

    .line 55
    invoke-virtual {v7}, Ljava/io/DataInputStream;->readFloat()F

    move-result v4

    .line 56
    invoke-virtual {v7}, Ljava/io/DataInputStream;->readFloat()F

    move-result v5

    .line 57
    invoke-virtual {v7}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    .line 58
    new-instance v0, Ldqt;

    invoke-direct/range {v0 .. v6}, Ldqt;-><init>(Ljava/lang/String;FFFFI)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 63
    :try_start_1
    invoke-virtual {v7}, Ljava/io/DataInputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 69
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    .line 63
    :try_start_2
    invoke-virtual {v7}, Ljava/io/DataInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 69
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 62
    :catchall_0
    move-exception v0

    .line 63
    :try_start_3
    invoke-virtual {v7}, Ljava/io/DataInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 66
    :goto_2
    throw v0

    :catch_1
    move-exception v1

    goto :goto_0

    .line 67
    :catch_2
    move-exception v0

    goto :goto_1

    :catch_3
    move-exception v1

    goto :goto_2
.end method

.method public static a(Ljava/lang/String;FFFFI)[B
    .locals 3

    .prologue
    .line 24
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 25
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 27
    :try_start_0
    invoke-virtual {v1, p0}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 28
    invoke-virtual {v1, p1}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 29
    invoke-virtual {v1, p2}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 30
    invoke-virtual {v1, p3}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 31
    invoke-virtual {v1, p4}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 32
    invoke-virtual {v1, p5}, Ljava/io/DataOutputStream;->writeInt(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 37
    :try_start_1
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 42
    :goto_0
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v2

    .line 37
    :try_start_2
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 41
    :catch_1
    move-exception v1

    goto :goto_0

    .line 36
    :catchall_0
    move-exception v0

    .line 37
    :try_start_3
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 40
    :goto_1
    throw v0

    .line 41
    :catch_2
    move-exception v1

    goto :goto_0

    :catch_3
    move-exception v1

    goto :goto_1
.end method
