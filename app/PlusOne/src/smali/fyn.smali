.class public final Lfyn;
.super Lgbz;
.source "PG"

# interfaces
.implements Lled;


# instance fields
.field private A:I

.field private B:Lkzs;

.field private C:Lkzs;

.field private D:Lkzv;

.field private y:Llec;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lfyn;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lfyn;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3}, Lgbz;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    new-instance v0, Llec;

    invoke-direct {v0, p1, p2, p3}, Llec;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lfyn;->y:Llec;

    .line 45
    return-void
.end method


# virtual methods
.method protected a(III)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 109
    iput p2, p0, Lfyn;->A:I

    .line 110
    iget-object v0, p0, Lfyn;->y:Llec;

    invoke-virtual {v0}, Llec;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-ne v0, p0, :cond_0

    .line 111
    iget-object v0, p0, Lfyn;->y:Llec;

    const/high16 v1, 0x40000000    # 2.0f

    .line 112
    invoke-static {p3, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 113
    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 111
    invoke-virtual {v0, v1, v2}, Llec;->measure(II)V

    .line 114
    iget-object v0, p0, Lfyn;->y:Llec;

    invoke-virtual {v0}, Llec;->getMeasuredHeight()I

    move-result v0

    add-int/2addr p2, v0

    .line 116
    :cond_0
    return p2
.end method

.method protected a(Landroid/graphics/Canvas;I)I
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lfyn;->y:Llec;

    invoke-virtual {v0}, Llec;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-ne v0, p0, :cond_0

    .line 122
    iget-object v0, p0, Lfyn;->y:Llec;

    invoke-virtual {v0}, Llec;->getHeight()I

    move-result v0

    add-int/2addr p2, v0

    .line 124
    :cond_0
    return p2
.end method

.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 49
    invoke-super {p0}, Lgbz;->a()V

    .line 51
    iget-object v0, p0, Lfyn;->y:Llec;

    invoke-virtual {v0}, Llec;->a()V

    .line 52
    const/4 v0, 0x0

    iput v0, p0, Lfyn;->A:I

    .line 53
    iput-object v1, p0, Lfyn;->B:Lkzs;

    .line 54
    iput-object v1, p0, Lfyn;->C:Lkzs;

    .line 55
    iput-object v1, p0, Lfyn;->D:Lkzv;

    .line 56
    return-void
.end method

.method protected a(Landroid/database/Cursor;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 60
    const/16 v0, 0xa

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 62
    const/16 v2, 0x17

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    .line 63
    if-eqz v2, :cond_0

    .line 64
    invoke-static {v2}, Lkzv;->a([B)Lkzv;

    move-result-object v2

    iput-object v2, p0, Lfyn;->D:Lkzv;

    .line 67
    :cond_0
    iget-object v2, p0, Lfyn;->D:Lkzv;

    if-nez v2, :cond_2

    .line 86
    :cond_1
    :goto_0
    return-void

    .line 71
    :cond_2
    const-wide/16 v2, 0x2000

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    .line 72
    const/16 v2, 0x19

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    .line 74
    if-eqz v2, :cond_3

    .line 75
    invoke-static {v2}, Lkzs;->a([B)Lkzs;

    move-result-object v2

    iput-object v2, p0, Lfyn;->B:Lkzs;

    .line 79
    :cond_3
    const-wide/16 v2, 0x800

    and-long/2addr v0, v2

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    .line 80
    const/16 v0, 0x18

    .line 81
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 82
    if-eqz v0, :cond_1

    .line 83
    invoke-static {v0}, Lkzs;->a([B)Lkzs;

    move-result-object v0

    iput-object v0, p0, Lfyn;->C:Lkzs;

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Lkzs;)V
    .locals 6

    .prologue
    .line 178
    invoke-virtual {p0}, Lfyn;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lkzh;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkzh;

    .line 179
    iget-object v1, p0, Lfyn;->r:Ljava/lang/String;

    iget-object v2, p0, Lfyn;->b:Ljava/lang/String;

    iget-object v3, p0, Lfyn;->c:Ljava/lang/String;

    move-object v4, p1

    move-object v5, p2

    invoke-interface/range {v0 .. v5}, Lkzh;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lkzs;)V

    .line 181
    return-void
.end method

.method public a(Lkzs;)V
    .locals 4

    .prologue
    .line 185
    invoke-virtual {p0}, Lfyn;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lkzh;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkzh;

    .line 186
    iget-object v1, p0, Lfyn;->r:Ljava/lang/String;

    iget-object v2, p0, Lfyn;->b:Ljava/lang/String;

    iget-object v3, p0, Lfyn;->c:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3, p1}, Lkzh;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lkzs;)V

    .line 188
    return-void
.end method

.method protected a(ZIIII)V
    .locals 6

    .prologue
    .line 129
    invoke-super/range {p0 .. p5}, Lgbz;->a(ZIIII)V

    .line 130
    iget-object v0, p0, Lfyn;->y:Llec;

    invoke-virtual {v0}, Llec;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-ne v0, p0, :cond_0

    .line 131
    iget-object v0, p0, Lfyn;->y:Llec;

    iget-object v1, p0, Lfyn;->q:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget v2, p0, Lfyn;->A:I

    iget-object v3, p0, Lfyn;->q:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget-object v4, p0, Lfyn;->y:Llec;

    .line 132
    invoke-virtual {v4}, Llec;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v3, v4

    iget v4, p0, Lfyn;->A:I

    iget-object v5, p0, Lfyn;->y:Llec;

    .line 133
    invoke-virtual {v5}, Llec;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v4, v5

    .line 131
    invoke-virtual {v0, v1, v2, v3, v4}, Llec;->layout(IIII)V

    .line 135
    :cond_0
    return-void
.end method

.method protected aD_()Z
    .locals 1

    .prologue
    .line 104
    const/4 v0, 0x1

    return v0
.end method

.method protected a_(Landroid/database/Cursor;Llcr;I)V
    .locals 10

    .prologue
    .line 91
    iget-object v0, p0, Lfyn;->y:Llec;

    invoke-virtual {p0, v0}, Lfyn;->removeView(Landroid/view/View;)V

    .line 92
    iget-object v0, p0, Lfyn;->D:Lkzv;

    if-nez v0, :cond_0

    .line 100
    :goto_0
    return-void

    .line 95
    :cond_0
    iget-object v0, p0, Lfyn;->y:Llec;

    iget-object v1, p0, Lfyn;->D:Lkzv;

    iget-object v2, p0, Lfyn;->B:Lkzs;

    iget-object v3, p0, Lfyn;->C:Lkzs;

    iget-object v4, p0, Lfyn;->b:Ljava/lang/String;

    iget-object v5, p0, Lfyn;->a:Ljava/lang/String;

    iget v6, p0, Lfyn;->o:I

    .line 97
    invoke-virtual {p2, v6}, Llcr;->a(I)I

    move-result v6

    iget v7, p0, Lfyn;->o:I

    const/4 v8, 0x0

    move-object v9, p0

    .line 95
    invoke-virtual/range {v0 .. v9}, Llec;->a(Lkzv;Lkzs;Lkzs;Ljava/lang/String;Ljava/lang/String;IIZLled;)V

    .line 99
    iget-object v0, p0, Lfyn;->y:Llec;

    invoke-virtual {p0, v0}, Lfyn;->addView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public b(Z)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 169
    invoke-super {p0, p1}, Lgbz;->b(Z)Landroid/content/Intent;

    move-result-object v0

    .line 170
    if-eqz v0, :cond_0

    .line 171
    iget-object v1, p0, Lfyn;->y:Llec;

    invoke-virtual {v1, v0}, Llec;->a(Landroid/content/Intent;)V

    .line 173
    :cond_0
    return-object v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 151
    invoke-super {p0}, Lgbz;->b()V

    .line 152
    invoke-static {p0}, Llii;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lfyn;->y:Llec;

    if-eqz v0, :cond_0

    .line 154
    iget-object v0, p0, Lfyn;->y:Llec;

    invoke-virtual {v0}, Llec;->b()V

    .line 157
    :cond_0
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lfyn;->y:Llec;

    if-eqz v0, :cond_0

    .line 162
    iget-object v0, p0, Lfyn;->y:Llec;

    invoke-virtual {v0}, Llec;->c()V

    .line 164
    :cond_0
    invoke-super {p0}, Lgbz;->c()V

    .line 165
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 0

    .prologue
    .line 139
    invoke-super {p0}, Lgbz;->onAttachedToWindow()V

    .line 140
    invoke-virtual {p0}, Lfyn;->b()V

    .line 141
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 145
    invoke-super {p0}, Lgbz;->onDetachedFromWindow()V

    .line 146
    invoke-virtual {p0}, Lfyn;->c()V

    .line 147
    return-void
.end method
