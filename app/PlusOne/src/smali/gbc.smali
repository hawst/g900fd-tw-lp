.class public final Lgbc;
.super Landroid/view/View;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lfin;
.implements Lljh;


# static fields
.field private static a:Z

.field private static b:Landroid/graphics/Bitmap;

.field private static c:Landroid/graphics/Bitmap;

.field private static d:Landroid/graphics/Bitmap;

.field private static e:Landroid/graphics/Paint;

.field private static f:Landroid/text/TextPaint;

.field private static g:Llct;


# instance fields
.field private h:Ljava/lang/String;

.field private i:Landroid/graphics/Rect;

.field private j:Landroid/graphics/Point;

.field private k:Landroid/graphics/Rect;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Landroid/graphics/Bitmap;

.field private p:Landroid/graphics/Point;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lgbc;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lgbc;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 66
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const v4, 0x7f0d024b

    const/4 v3, 0x1

    .line 70
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 72
    sget-boolean v0, Lgbc;->a:Z

    if-nez v0, :cond_0

    .line 73
    invoke-virtual {p0}, Lgbc;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 74
    sput-boolean v3, Lgbc;->a:Z

    .line 75
    const v1, 0x7f02012f

    invoke-static {v0, v1}, Lfus;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lgbc;->b:Landroid/graphics/Bitmap;

    .line 76
    const v1, 0x7f0202ff

    .line 77
    invoke-static {v0, v1}, Lfus;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sget-object v2, Lgbc;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    .line 76
    invoke-static {v1, v2}, Llrw;->a(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lgbc;->c:Landroid/graphics/Bitmap;

    .line 78
    const v1, 0x7f020369

    .line 79
    invoke-static {v0, v1}, Lfus;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sget-object v2, Lgbc;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    .line 78
    invoke-static {v1, v2}, Llrw;->a(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lgbc;->d:Landroid/graphics/Bitmap;

    .line 81
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 82
    sput-object v1, Lgbc;->e:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 83
    sget-object v1, Lgbc;->e:Landroid/graphics/Paint;

    const v2, 0x7f0b0301

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 85
    sget-object v1, Lgbc;->e:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 87
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 88
    sput-object v1, Lgbc;->f:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 89
    sget-object v1, Lgbc;->f:Landroid/text/TextPaint;

    const v2, 0x7f0b0141

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 90
    sget-object v1, Lgbc;->f:Landroid/text/TextPaint;

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-virtual {v1, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 92
    sget-object v0, Lgbc;->f:Landroid/text/TextPaint;

    invoke-static {v0, v4}, Llib;->a(Landroid/text/TextPaint;I)V

    .line 94
    invoke-static {p1}, Llct;->a(Landroid/content/Context;)Llct;

    move-result-object v0

    sput-object v0, Lgbc;->g:Llct;

    .line 96
    :cond_0
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lgbc;->j:Landroid/graphics/Point;

    .line 97
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lgbc;->p:Landroid/graphics/Point;

    .line 98
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lgbc;->i:Landroid/graphics/Rect;

    .line 99
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lgbc;->k:Landroid/graphics/Rect;

    .line 100
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 244
    iput-object v0, p0, Lgbc;->h:Ljava/lang/String;

    .line 245
    iput-object v0, p0, Lgbc;->l:Ljava/lang/String;

    .line 246
    iput-object v0, p0, Lgbc;->m:Ljava/lang/String;

    .line 247
    iput-object v0, p0, Lgbc;->n:Ljava/lang/String;

    .line 248
    iget-object v0, p0, Lgbc;->j:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v1}, Landroid/graphics/Point;->set(II)V

    .line 249
    iget-object v0, p0, Lgbc;->p:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v1}, Landroid/graphics/Point;->set(II)V

    .line 250
    iget-object v0, p0, Lgbc;->i:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 251
    iget-object v0, p0, Lgbc;->k:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 252
    return-void
.end method

.method public a(Ljava/lang/String;ZLjava/lang/String;)V
    .locals 3

    .prologue
    .line 190
    iget-object v1, p0, Lgbc;->o:Landroid/graphics/Bitmap;

    .line 191
    if-eqz p2, :cond_3

    if-eqz p1, :cond_3

    iget-object v0, p0, Lgbc;->l:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 192
    sget-object v0, Lgbc;->d:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lgbc;->o:Landroid/graphics/Bitmap;

    .line 197
    :goto_0
    iget-object v0, p0, Lgbc;->l:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lgbc;->m:Ljava/lang/String;

    .line 198
    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x1

    .line 199
    :goto_1
    if-eqz v0, :cond_0

    .line 200
    iput-object p3, p0, Lgbc;->m:Ljava/lang/String;

    .line 203
    :cond_0
    iget-object v2, p0, Lgbc;->o:Landroid/graphics/Bitmap;

    if-ne v1, v2, :cond_1

    if-eqz v0, :cond_2

    .line 204
    :cond_1
    invoke-virtual {p0}, Lgbc;->invalidate()V

    .line 206
    :cond_2
    return-void

    .line 194
    :cond_3
    sget-object v0, Lgbc;->c:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lgbc;->o:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 198
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a(Lkzy;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 103
    iput-object p2, p0, Lgbc;->h:Ljava/lang/String;

    .line 105
    invoke-virtual {p1}, Lkzy;->e()Ljava/lang/String;

    move-result-object v1

    .line 106
    invoke-virtual {p1}, Lkzy;->f()Ljava/lang/String;

    move-result-object v2

    .line 107
    invoke-virtual {p1}, Lkzy;->g()Z

    move-result v3

    .line 109
    const-string v0, "https://"

    invoke-virtual {v1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 110
    if-gez v0, :cond_0

    .line 111
    const-string v0, "http://"

    invoke-virtual {v1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 114
    :cond_0
    if-ltz v0, :cond_6

    .line 115
    if-eqz v3, :cond_3

    .line 116
    invoke-virtual {v1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, p0

    .line 122
    :goto_0
    iput-object v0, v1, Lgbc;->l:Ljava/lang/String;

    .line 125
    invoke-virtual {p0}, Lgbc;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lgbc;->l:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbc;->m:Ljava/lang/String;

    .line 127
    invoke-virtual {p1}, Lkzy;->c()Ljava/lang/String;

    move-result-object v0

    .line 128
    invoke-virtual {p1}, Lkzy;->a()Ljava/lang/String;

    move-result-object v1

    .line 130
    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x100

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 131
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 132
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0xa

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 133
    iput-object v0, p0, Lgbc;->n:Ljava/lang/String;

    .line 135
    :cond_1
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 136
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 137
    iput-object v1, p0, Lgbc;->n:Ljava/lang/String;

    .line 139
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbc;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 144
    :goto_1
    invoke-virtual {p0, p0}, Lgbc;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 145
    return-void

    .line 118
    :cond_3
    invoke-virtual {v2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbc;->l:Ljava/lang/String;

    .line 119
    iget-object v0, p0, Lgbc;->l:Ljava/lang/String;

    const-string v1, "mode=inline"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 120
    iget-object v0, p0, Lgbc;->l:Ljava/lang/String;

    const-string v1, "mode=inline"

    const-string v2, "mode=streaming"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    move-object v1, p0

    goto :goto_0

    .line 122
    :cond_4
    iget-object v0, p0, Lgbc;->l:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v0, "&mode=streaming"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v1, p0

    goto :goto_0

    :cond_5
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    move-object v1, p0

    goto :goto_0

    .line 141
    :cond_6
    const-string v0, ""

    iput-object v0, p0, Lgbc;->m:Ljava/lang/String;

    goto :goto_1
.end method

.method protected onAttachedToWindow()V
    .locals 0

    .prologue
    .line 210
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    .line 211
    invoke-static {p0}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->a(Lfin;)V

    .line 212
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 222
    invoke-virtual {p0}, Lgbc;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 223
    const-class v0, Lhee;

    invoke-static {v1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    .line 224
    invoke-interface {v0}, Lhee;->d()I

    move-result v2

    .line 226
    iget-object v0, p0, Lgbc;->l:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 227
    :goto_0
    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;

    invoke-direct {v3, v1, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 228
    if-eqz v0, :cond_1

    const-string v0, "com.google.android.apps.plus.service.SkyjamPlaybackService.PLAY"

    :goto_1
    invoke-virtual {v3, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 229
    const-string v0, "account_id"

    invoke-virtual {v3, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 230
    const-string v0, "music_url"

    iget-object v2, p0, Lgbc;->l:Ljava/lang/String;

    invoke-virtual {v3, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 231
    const-string v0, "song"

    iget-object v2, p0, Lgbc;->n:Ljava/lang/String;

    invoke-virtual {v3, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 232
    const-string v0, "activity_id"

    iget-object v2, p0, Lgbc;->h:Ljava/lang/String;

    invoke-virtual {v3, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 233
    invoke-virtual {v1, v3}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 234
    return-void

    .line 226
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 228
    :cond_1
    const-string v0, "com.google.android.apps.plus.service.SkyjamPlaybackService.STOP"

    goto :goto_1
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 216
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 217
    invoke-static {p0}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->b(Lfin;)V

    .line 218
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 179
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 181
    iget-object v0, p0, Lgbc;->i:Landroid/graphics/Rect;

    sget-object v1, Lgbc;->e:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 182
    iget-object v0, p0, Lgbc;->m:Ljava/lang/String;

    iget-object v1, p0, Lgbc;->j:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    iget-object v2, p0, Lgbc;->j:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    sget-object v3, Lgbc;->f:Landroid/text/TextPaint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 184
    iget-object v0, p0, Lgbc;->o:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lgbc;->p:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    iget-object v2, p0, Lgbc;->p:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    invoke-virtual {p1, v0, v1, v2, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 185
    sget-object v0, Lgbc;->b:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lgbc;->k:Landroid/graphics/Rect;

    invoke-virtual {p1, v0, v4, v1, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 186
    return-void
.end method

.method protected onMeasure(II)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 149
    iget-object v0, p0, Lgbc;->l:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lgbc;->d:Landroid/graphics/Bitmap;

    :goto_0
    iput-object v0, p0, Lgbc;->o:Landroid/graphics/Bitmap;

    .line 150
    iget-object v0, p0, Lgbc;->o:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    .line 151
    sget-object v1, Lgbc;->f:Landroid/text/TextPaint;

    invoke-static {v1}, Llib;->a(Landroid/text/TextPaint;)I

    move-result v1

    .line 152
    sget-object v2, Lgbc;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    .line 153
    sget-object v3, Lgbc;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    .line 155
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v4

    invoke-static {v4, v3}, Ljava/lang/Math;->max(II)I

    move-result v4

    sget-object v5, Lgbc;->g:Llct;

    iget v5, v5, Llct;->m:I

    mul-int/lit8 v5, v5, 0x2

    add-int/2addr v4, v5

    .line 157
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v5

    .line 159
    iget-object v6, p0, Lgbc;->i:Landroid/graphics/Rect;

    invoke-virtual {v6, v7, v7, v5, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 161
    iget-object v6, p0, Lgbc;->i:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->left:I

    sget-object v7, Lgbc;->g:Llct;

    iget v7, v7, Llct;->m:I

    add-int/2addr v6, v7

    .line 162
    iget-object v7, p0, Lgbc;->i:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->top:I

    sub-int v0, v4, v0

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v7

    .line 163
    iget-object v7, p0, Lgbc;->p:Landroid/graphics/Point;

    invoke-virtual {v7, v6, v0}, Landroid/graphics/Point;->set(II)V

    .line 165
    iget-object v0, p0, Lgbc;->o:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    sget-object v7, Lgbc;->g:Llct;

    iget v7, v7, Llct;->m:I

    add-int/2addr v0, v7

    add-int/2addr v0, v6

    .line 166
    iget-object v6, p0, Lgbc;->i:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    sub-int v1, v4, v1

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v6

    sget-object v6, Lgbc;->f:Landroid/text/TextPaint;

    .line 167
    invoke-virtual {v6}, Landroid/text/TextPaint;->ascent()F

    move-result v6

    float-to-int v6, v6

    sub-int/2addr v1, v6

    .line 168
    iget-object v6, p0, Lgbc;->j:Landroid/graphics/Point;

    invoke-virtual {v6, v0, v1}, Landroid/graphics/Point;->set(II)V

    .line 170
    iget-object v0, p0, Lgbc;->i:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    sub-int/2addr v0, v2

    .line 171
    iget-object v1, p0, Lgbc;->i:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    sub-int v6, v4, v3

    div-int/lit8 v6, v6, 0x2

    add-int/2addr v1, v6

    .line 172
    iget-object v6, p0, Lgbc;->k:Landroid/graphics/Rect;

    add-int/2addr v2, v0

    add-int/2addr v3, v1

    invoke-virtual {v6, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 174
    invoke-virtual {p0, v5, v4}, Lgbc;->setMeasuredDimension(II)V

    .line 175
    return-void

    .line 149
    :cond_0
    sget-object v0, Lgbc;->c:Landroid/graphics/Bitmap;

    goto/16 :goto_0
.end method
