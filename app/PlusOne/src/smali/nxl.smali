.class public final Lnxl;
.super Loxq;
.source "PG"


# instance fields
.field public a:I

.field public b:Lnxm;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 95
    const/high16 v0, -0x80000000

    iput v0, p0, Lnxl;->a:I

    .line 98
    const/4 v0, 0x0

    iput-object v0, p0, Lnxl;->b:Lnxm;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 115
    const/4 v0, 0x0

    .line 116
    iget v1, p0, Lnxl;->a:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 117
    const/4 v0, 0x1

    iget v1, p0, Lnxl;->a:I

    .line 118
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 120
    :cond_0
    iget-object v1, p0, Lnxl;->b:Lnxm;

    if-eqz v1, :cond_1

    .line 121
    const/4 v1, 0x2

    iget-object v2, p0, Lnxl;->b:Lnxm;

    .line 122
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 124
    :cond_1
    iget-object v1, p0, Lnxl;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 125
    iput v0, p0, Lnxl;->ai:I

    .line 126
    return v0
.end method

.method public a(Loxn;)Lnxl;
    .locals 2

    .prologue
    .line 134
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 135
    sparse-switch v0, :sswitch_data_0

    .line 139
    iget-object v1, p0, Lnxl;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 140
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnxl;->ah:Ljava/util/List;

    .line 143
    :cond_1
    iget-object v1, p0, Lnxl;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 145
    :sswitch_0
    return-object p0

    .line 150
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 151
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-ne v0, v1, :cond_3

    .line 157
    :cond_2
    iput v0, p0, Lnxl;->a:I

    goto :goto_0

    .line 159
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lnxl;->a:I

    goto :goto_0

    .line 164
    :sswitch_2
    iget-object v0, p0, Lnxl;->b:Lnxm;

    if-nez v0, :cond_4

    .line 165
    new-instance v0, Lnxm;

    invoke-direct {v0}, Lnxm;-><init>()V

    iput-object v0, p0, Lnxl;->b:Lnxm;

    .line 167
    :cond_4
    iget-object v0, p0, Lnxl;->b:Lnxm;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 135
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 103
    iget v0, p0, Lnxl;->a:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 104
    const/4 v0, 0x1

    iget v1, p0, Lnxl;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 106
    :cond_0
    iget-object v0, p0, Lnxl;->b:Lnxm;

    if-eqz v0, :cond_1

    .line 107
    const/4 v0, 0x2

    iget-object v1, p0, Lnxl;->b:Lnxm;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 109
    :cond_1
    iget-object v0, p0, Lnxl;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 111
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lnxl;->a(Loxn;)Lnxl;

    move-result-object v0

    return-object v0
.end method
