.class public final Loch;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Loch;


# instance fields
.field private b:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1015
    const/4 v0, 0x0

    new-array v0, v0, [Loch;

    sput-object v0, Loch;->a:[Loch;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1016
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 2

    .prologue
    .line 1032
    const/4 v0, 0x0

    .line 1033
    iget-object v1, p0, Loch;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 1034
    const/4 v0, 0x1

    iget-object v1, p0, Loch;->b:Ljava/lang/Integer;

    .line 1035
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1037
    :cond_0
    iget-object v1, p0, Loch;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1038
    iput v0, p0, Loch;->ai:I

    .line 1039
    return v0
.end method

.method public a(Loxn;)Loch;
    .locals 2

    .prologue
    .line 1047
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1048
    sparse-switch v0, :sswitch_data_0

    .line 1052
    iget-object v1, p0, Loch;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1053
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Loch;->ah:Ljava/util/List;

    .line 1056
    :cond_1
    iget-object v1, p0, Loch;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1058
    :sswitch_0
    return-object p0

    .line 1063
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Loch;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 1048
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1023
    iget-object v0, p0, Loch;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 1024
    const/4 v0, 0x1

    iget-object v1, p0, Loch;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1026
    :cond_0
    iget-object v0, p0, Loch;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1028
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1012
    invoke-virtual {p0, p1}, Loch;->a(Loxn;)Loch;

    move-result-object v0

    return-object v0
.end method
