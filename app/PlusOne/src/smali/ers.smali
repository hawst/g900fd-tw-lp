.class public final Lers;
.super Llol;
.source "PG"

# interfaces
.implements Landroid/widget/AbsListView$RecyclerListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lbc;
.implements Lezf;
.implements Lfaw;
.implements Lfvq;
.implements Lgau;
.implements Ljit;
.implements Lkch;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Llol;",
        "Landroid/widget/AbsListView$RecyclerListener;",
        "Landroid/widget/AdapterView$OnItemClickListener;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lezf;",
        "Lfaw;",
        "Lfvq;",
        "Lgau;",
        "Ljit;",
        "Lkch;"
    }
.end annotation


# instance fields
.field private N:Lesa;

.field private O:Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;

.field private P:Lesb;

.field private Q:Lcom/google/android/apps/plus/views/UnreadNotificationListView;

.field private R:Lerj;

.field private S:Lcom/google/android/apps/plus/views/ReadNotificationListView;

.field private T:Lerj;

.field private U:Lgau;

.field private V:Lkci;

.field private W:Lhee;

.field private X:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ldsk;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private Y:I

.field private Z:J

.field private aa:Z

.field private ab:Z

.field private ac:Z

.field private ad:Lfhh;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 66
    invoke-direct {p0}, Llol;-><init>()V

    .line 124
    new-instance v0, Lkci;

    iget-object v1, p0, Lers;->av:Llqm;

    const v2, 0x7f10005b

    invoke-direct {v0, p0, v1, v2, p0}, Lkci;-><init>(Lu;Llqr;ILkch;)V

    iput-object v0, p0, Lers;->V:Lkci;

    .line 130
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lers;->X:Ljava/util/concurrent/ConcurrentHashMap;

    .line 150
    new-instance v0, Lert;

    invoke-direct {v0, p0}, Lert;-><init>(Lers;)V

    iput-object v0, p0, Lers;->ad:Lfhh;

    return-void
.end method

.method static synthetic a(Lers;)Llnl;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lers;->at:Llnl;

    return-object v0
.end method

.method private a(ILfib;)V
    .locals 3

    .prologue
    .line 542
    iget-object v0, p0, Lers;->X:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 543
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-static {v1}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v1

    if-ne v1, p1, :cond_0

    .line 544
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldsk;

    .line 545
    iget-object v1, p0, Lers;->X:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 547
    sget-object v1, Lery;->a:[I

    invoke-virtual {v0}, Ldsk;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 551
    :pswitch_0
    invoke-direct {p0}, Lers;->ac()V

    goto :goto_0

    .line 556
    :pswitch_1
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lfib;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 557
    iget-object v0, p0, Lers;->T:Lerj;

    invoke-virtual {v0}, Lerj;->f()V

    goto :goto_0

    .line 564
    :pswitch_2
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lfib;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 565
    iget-object v0, p0, Lers;->R:Lerj;

    invoke-virtual {v0}, Lerj;->f()V

    goto :goto_0

    .line 572
    :cond_1
    iget-object v0, p0, Lers;->V:Lkci;

    if-eqz v0, :cond_2

    .line 573
    iget-object v0, p0, Lers;->V:Lkci;

    invoke-virtual {v0}, Lkci;->e()V

    .line 575
    :cond_2
    iget-object v0, p0, Lers;->N:Lesa;

    invoke-interface {v0}, Lesa;->ax_()V

    .line 576
    return-void

    .line 547
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method private a(Ldsk;Ljava/lang/Integer;)V
    .locals 1

    .prologue
    .line 392
    if-eqz p2, :cond_0

    .line 393
    iget-object v0, p0, Lers;->X:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 395
    :cond_0
    return-void
.end method

.method static synthetic a(Lers;ILfib;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Lers;->a(ILfib;)V

    return-void
.end method

.method static synthetic a(Lers;Ldsk;Ljava/lang/Integer;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Lers;->a(Ldsk;Ljava/lang/Integer;)V

    return-void
.end method

.method private a(Ldsk;)Z
    .locals 1

    .prologue
    .line 383
    iget-object v0, p0, Lers;->X:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lers;->X:Ljava/util/concurrent/ConcurrentHashMap;

    .line 384
    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lers;Ldsk;)Z
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lers;->a(Ldsk;)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lers;Z)Z
    .locals 0

    .prologue
    .line 66
    iput-boolean p1, p0, Lers;->ab:Z

    return p1
.end method

.method private ac()V
    .locals 3

    .prologue
    .line 398
    iget-object v0, p0, Lers;->T:Lerj;

    sget-object v1, Ldsk;->g:Ldsk;

    .line 399
    invoke-direct {p0, v1}, Lers;->a(Ldsk;)Z

    move-result v1

    sget-object v2, Ldsk;->h:Ldsk;

    .line 400
    invoke-direct {p0, v2}, Lers;->a(Ldsk;)Z

    move-result v2

    .line 398
    invoke-virtual {v0, v1, v2}, Lerj;->a(ZZ)V

    .line 402
    iget-object v0, p0, Lers;->R:Lerj;

    sget-object v1, Ldsk;->e:Ldsk;

    .line 403
    invoke-direct {p0, v1}, Lers;->a(Ldsk;)Z

    move-result v1

    sget-object v2, Ldsk;->f:Ldsk;

    .line 404
    invoke-direct {p0, v2}, Lers;->a(Ldsk;)Z

    move-result v2

    .line 402
    invoke-virtual {v0, v1, v2}, Lerj;->a(ZZ)V

    .line 405
    return-void
.end method

.method private ad()I
    .locals 2

    .prologue
    .line 609
    iget-object v0, p0, Lers;->at:Llnl;

    invoke-virtual {v0}, Llnl;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d013e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method static synthetic b(Lers;)Lhee;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lers;->W:Lhee;

    return-object v0
.end method

.method static synthetic c(Lers;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Lers;->ac()V

    return-void
.end method

.method private c(Z)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    .line 432
    const/4 v0, 0x0

    .line 433
    sget-object v1, Ldsk;->e:Ldsk;

    invoke-direct {p0, v1}, Lers;->a(Ldsk;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 434
    if-eqz p1, :cond_0

    .line 435
    iget-object v0, p0, Lers;->R:Lerj;

    invoke-virtual {v0}, Lerj;->b()V

    .line 437
    :cond_0
    sget-object v6, Ldsk;->e:Ldsk;

    .line 438
    invoke-virtual {p0}, Lers;->n()Lz;

    move-result-object v0

    iget-object v1, p0, Lers;->W:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    const/4 v3, 0x4

    const/4 v4, 0x0

    move v5, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;III[BZ)Ljava/lang/Integer;

    move-result-object v0

    .line 437
    invoke-direct {p0, v6, v0}, Lers;->a(Ldsk;Ljava/lang/Integer;)V

    .line 447
    :goto_0
    if-eqz v2, :cond_2

    .line 448
    iget-object v0, p0, Lers;->V:Lkci;

    invoke-virtual {v0}, Lkci;->f()Z

    move-result v0

    if-nez v0, :cond_1

    .line 449
    iget-object v0, p0, Lers;->V:Lkci;

    invoke-virtual {v0}, Lkci;->d()V

    .line 451
    :cond_1
    iget-object v0, p0, Lers;->V:Lkci;

    invoke-virtual {v0}, Lkci;->e()V

    .line 452
    iget-object v0, p0, Lers;->N:Lesa;

    invoke-interface {v0}, Lesa;->ax_()V

    .line 454
    :cond_2
    return-void

    :cond_3
    move v2, v0

    goto :goto_0
.end method

.method static synthetic d(Lers;)Llnl;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lers;->at:Llnl;

    return-object v0
.end method

.method static synthetic e(Lers;)Lcom/google/android/apps/plus/views/ReadNotificationListView;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lers;->S:Lcom/google/android/apps/plus/views/ReadNotificationListView;

    return-object v0
.end method

.method static synthetic f(Lers;)Lcom/google/android/apps/plus/views/UnreadNotificationListView;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lers;->Q:Lcom/google/android/apps/plus/views/UnreadNotificationListView;

    return-object v0
.end method

.method static synthetic g(Lers;)Lerj;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lers;->T:Lerj;

    return-object v0
.end method

.method static synthetic h(Lers;)Lgau;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lers;->U:Lgau;

    return-object v0
.end method


# virtual methods
.method public K_()V
    .locals 1

    .prologue
    .line 917
    invoke-virtual {p0}, Lers;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 918
    iget-object v0, p0, Lers;->V:Lkci;

    invoke-virtual {v0}, Lkci;->c()V

    .line 919
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lers;->c(Z)V

    .line 921
    :cond_0
    return-void
.end method

.method public U()I
    .locals 1

    .prologue
    .line 580
    iget-object v0, p0, Lers;->R:Lerj;

    invoke-virtual {v0}, Lerj;->h()I

    move-result v0

    return v0
.end method

.method public V()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 585
    new-instance v0, Ldoq;

    iget-object v1, p0, Lers;->at:Llnl;

    iget-object v2, p0, Lers;->W:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    invoke-direct {v0, v1, v2}, Ldoq;-><init>(Landroid/content/Context;I)V

    .line 586
    invoke-virtual {p0}, Lers;->n()Lz;

    move-result-object v1

    invoke-static {v1, v0}, Lhoc;->a(Landroid/content/Context;Lhny;)V

    .line 587
    iget-object v0, p0, Lers;->Q:Lcom/google/android/apps/plus/views/UnreadNotificationListView;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/UnreadNotificationListView;->setSelection(I)V

    .line 588
    iget-object v0, p0, Lers;->S:Lcom/google/android/apps/plus/views/ReadNotificationListView;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/ReadNotificationListView;->setSelection(I)V

    .line 589
    return-void
.end method

.method public W()V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v5, 0x0

    .line 715
    invoke-static {v0, v0, v5}, Lhmv;->a(ZZZ)Lhmv;

    move-result-object v1

    .line 717
    iget-object v0, p0, Lers;->W:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v2

    .line 718
    iget-object v0, p0, Lers;->au:Llnh;

    const-class v3, Lhms;

    invoke-virtual {v0, v3}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v3, Lhmr;

    iget-object v4, p0, Lers;->at:Llnl;

    invoke-direct {v3, v4, v2}, Lhmr;-><init>(Landroid/content/Context;I)V

    .line 720
    invoke-virtual {v3, v1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 718
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 722
    iput-boolean v5, p0, Lers;->ab:Z

    .line 723
    return-void
.end method

.method public X()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 753
    iget-object v0, p0, Lers;->S:Lcom/google/android/apps/plus/views/ReadNotificationListView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ReadNotificationListView;->a()V

    .line 754
    iget-object v0, p0, Lers;->S:Lcom/google/android/apps/plus/views/ReadNotificationListView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ReadNotificationListView;->setVisibility(I)V

    .line 755
    iget-object v0, p0, Lers;->Q:Lcom/google/android/apps/plus/views/UnreadNotificationListView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/UnreadNotificationListView;->setVisibility(I)V

    .line 756
    iget-object v0, p0, Lers;->Q:Lcom/google/android/apps/plus/views/UnreadNotificationListView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/UnreadNotificationListView;->a()V

    .line 757
    iput-boolean v2, p0, Lers;->ab:Z

    .line 758
    return-void
.end method

.method public Y()Z
    .locals 1

    .prologue
    .line 912
    iget-object v0, p0, Lers;->Q:Lcom/google/android/apps/plus/views/UnreadNotificationListView;

    invoke-static {v0}, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;->a(Landroid/widget/AbsListView;)Z

    move-result v0

    return v0
.end method

.method public Z()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 906
    iget-object v0, p0, Lers;->S:Lcom/google/android/apps/plus/views/ReadNotificationListView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ReadNotificationListView;->getVisibility()I

    move-result v0

    const/16 v3, 0x8

    if-ne v0, v3, :cond_0

    move v0, v1

    .line 907
    :goto_0
    invoke-virtual {p0}, Lers;->aQ_()Z

    move-result v3

    if-nez v3, :cond_1

    if-eqz v0, :cond_1

    :goto_1
    return v1

    :cond_0
    move v0, v2

    .line 906
    goto :goto_0

    :cond_1
    move v1, v2

    .line 907
    goto :goto_1
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    .line 193
    const v0, 0x7f04012b

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    .line 195
    const v0, 0x7f10005b

    .line 196
    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;

    iput-object v0, p0, Lers;->O:Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;

    .line 198
    const v0, 0x7f1003bb

    .line 199
    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/UnreadNotificationListView;

    iput-object v0, p0, Lers;->Q:Lcom/google/android/apps/plus/views/UnreadNotificationListView;

    .line 200
    const v0, 0x7f1003bc

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/ReadNotificationListView;

    iput-object v0, p0, Lers;->S:Lcom/google/android/apps/plus/views/ReadNotificationListView;

    .line 202
    iget-object v0, p0, Lers;->S:Lcom/google/android/apps/plus/views/ReadNotificationListView;

    iget-object v1, p0, Lers;->U:Lgau;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ReadNotificationListView;->a(Lgau;)V

    .line 204
    iget-object v0, p0, Lers;->Q:Lcom/google/android/apps/plus/views/UnreadNotificationListView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/UnreadNotificationListView;->setRecyclerListener(Landroid/widget/AbsListView$RecyclerListener;)V

    .line 205
    iget-object v0, p0, Lers;->S:Lcom/google/android/apps/plus/views/ReadNotificationListView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/ReadNotificationListView;->setRecyclerListener(Landroid/widget/AbsListView$RecyclerListener;)V

    .line 207
    invoke-virtual {p0}, Lers;->o()Landroid/content/res/Resources;

    move-result-object v0

    .line 209
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-ge v1, v2, :cond_0

    .line 210
    const v1, 0x7f0b0305

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 211
    iget-object v1, p0, Lers;->Q:Lcom/google/android/apps/plus/views/UnreadNotificationListView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/views/UnreadNotificationListView;->setCacheColorHint(I)V

    .line 212
    iget-object v1, p0, Lers;->S:Lcom/google/android/apps/plus/views/ReadNotificationListView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/views/ReadNotificationListView;->setCacheColorHint(I)V

    .line 215
    :cond_0
    new-instance v0, Lesb;

    invoke-virtual {p0}, Lers;->n()Lz;

    move-result-object v1

    iget-object v2, p0, Lers;->Q:Lcom/google/android/apps/plus/views/UnreadNotificationListView;

    invoke-direct {v0, v1, v2, p0}, Lesb;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;Lfvq;)V

    iput-object v0, p0, Lers;->P:Lesb;

    .line 218
    new-instance v0, Lerj;

    invoke-virtual {p0}, Lers;->n()Lz;

    move-result-object v1

    iget-object v2, p0, Lers;->P:Lesb;

    .line 219
    invoke-virtual {v2}, Lesb;->b()Landroid/view/View$OnTouchListener;

    move-result-object v3

    const/4 v5, 0x1

    move-object v2, p0

    move-object v4, p0

    invoke-direct/range {v0 .. v5}, Lerj;-><init>(Landroid/content/Context;Lezf;Landroid/view/View$OnTouchListener;Lfaw;I)V

    iput-object v0, p0, Lers;->R:Lerj;

    .line 221
    new-instance v0, Lerj;

    invoke-virtual {p0}, Lers;->n()Lz;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v5, 0x2

    move-object v2, p0

    move-object v4, p0

    invoke-direct/range {v0 .. v5}, Lerj;-><init>(Landroid/content/Context;Lezf;Landroid/view/View$OnTouchListener;Lfaw;I)V

    iput-object v0, p0, Lers;->T:Lerj;

    .line 223
    iget-object v0, p0, Lers;->Q:Lcom/google/android/apps/plus/views/UnreadNotificationListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/UnreadNotificationListView;->setVisibility(I)V

    iget-boolean v0, p0, Lers;->ab:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lers;->S:Lcom/google/android/apps/plus/views/ReadNotificationListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ReadNotificationListView;->setVisibility(I)V

    .line 225
    :cond_1
    iget-object v0, p0, Lers;->Q:Lcom/google/android/apps/plus/views/UnreadNotificationListView;

    iget-object v1, p0, Lers;->R:Lerj;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/UnreadNotificationListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 226
    iget-object v0, p0, Lers;->Q:Lcom/google/android/apps/plus/views/UnreadNotificationListView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/UnreadNotificationListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 227
    iget-object v0, p0, Lers;->P:Lesb;

    iget-object v1, p0, Lers;->R:Lerj;

    invoke-virtual {v0, v1}, Lesb;->a(Lerj;)V

    .line 229
    iget-object v0, p0, Lers;->S:Lcom/google/android/apps/plus/views/ReadNotificationListView;

    iget-object v1, p0, Lers;->T:Lerj;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ReadNotificationListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 230
    iget-object v0, p0, Lers;->S:Lcom/google/android/apps/plus/views/ReadNotificationListView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/ReadNotificationListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 231
    iget-object v0, p0, Lers;->S:Lcom/google/android/apps/plus/views/ReadNotificationListView;

    iget-object v1, p0, Lers;->Q:Lcom/google/android/apps/plus/views/UnreadNotificationListView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ReadNotificationListView;->a(Landroid/view/View;)V

    .line 236
    iget-object v0, p0, Lers;->R:Lerj;

    iget-wide v2, p0, Lers;->Z:J

    invoke-virtual {v0, v2, v3}, Lerj;->a(J)V

    .line 238
    iget-object v0, p0, Lers;->V:Lkci;

    iget-boolean v1, p0, Lers;->aa:Z

    invoke-virtual {v0, v1}, Lkci;->b(Z)V

    .line 239
    invoke-virtual {p0}, Lers;->n()Lz;

    move-result-object v0

    const-class v1, Lkcj;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    .line 241
    iget-object v0, p0, Lers;->O:Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const/4 v1, -0x1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 243
    if-eqz p3, :cond_8

    .line 244
    const-string v0, "read_notification_bar_visible"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 245
    iget-object v0, p0, Lers;->S:Lcom/google/android/apps/plus/views/ReadNotificationListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ReadNotificationListView;->setVisibility(I)V

    .line 246
    const/4 v0, 0x1

    iput-boolean v0, p0, Lers;->ab:Z

    .line 249
    :cond_2
    const-string v0, "low_pri_read_expanded"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 250
    iget-object v0, p0, Lers;->T:Lerj;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lerj;->a(Z)V

    .line 253
    :cond_3
    const-string v0, "low_pri_read_expanded_ever"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 254
    iget-object v0, p0, Lers;->T:Lerj;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lerj;->b(Z)V

    .line 257
    :cond_4
    const-string v0, "low_pri_unread_expanded"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 258
    iget-object v0, p0, Lers;->R:Lerj;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lerj;->a(Z)V

    .line 261
    :cond_5
    const-string v0, "low_pri_unread_expanded_ever"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 262
    iget-object v0, p0, Lers;->R:Lerj;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lerj;->b(Z)V

    .line 265
    :cond_6
    const-string v0, "low_pri_read_summary"

    .line 266
    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    .line 267
    if-eqz v0, :cond_7

    array-length v1, v0

    if-lez v1, :cond_7

    .line 269
    :try_start_0
    iget-object v1, p0, Lers;->T:Lerj;

    new-instance v2, Lmyx;

    invoke-direct {v2}, Lmyx;-><init>()V

    invoke-static {v2, v0}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lmyx;

    invoke-virtual {v1, v0}, Lerj;->a(Lmyx;)V
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    .line 276
    :cond_7
    :goto_0
    const-string v0, "low_pri_unread_summary"

    .line 277
    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    .line 278
    if-eqz v0, :cond_8

    array-length v1, v0

    if-lez v1, :cond_8

    .line 280
    :try_start_1
    iget-object v1, p0, Lers;->R:Lerj;

    new-instance v2, Lmyx;

    invoke-direct {v2}, Lmyx;-><init>()V

    invoke-static {v2, v0}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lmyx;

    invoke-virtual {v1, v0}, Lerj;->a(Lmyx;)V
    :try_end_1
    .catch Loxt; {:try_start_1 .. :try_end_1} :catch_1

    .line 288
    :cond_8
    :goto_1
    return-object v6

    .line 271
    :catch_0
    move-exception v0

    .line 272
    const-string v1, "NotificationFragmentLeg"

    const-string v2, "Failed to deserialize the low-pri read notification summary"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 282
    :catch_1
    move-exception v0

    .line 283
    const-string v1, "NotificationFragmentLeg"

    const-string v2, "Failed to deserialize the low-pri unread notification summary"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 783
    packed-switch p1, :pswitch_data_0

    move-object v0, v4

    .line 798
    :goto_0
    return-object v0

    .line 785
    :pswitch_0
    new-instance v0, Lhye;

    invoke-virtual {p0}, Lers;->n()Lz;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->a:Landroid/net/Uri;

    iget-object v3, p0, Lers;->W:Lhee;

    .line 786
    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    .line 785
    invoke-static {v2, v3}, Lcom/google/android/apps/plus/content/EsProvider;->a(Landroid/net/Uri;I)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lerz;->a:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Lhye;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 790
    :pswitch_1
    new-instance v0, Leri;

    iget-object v1, p0, Lers;->at:Llnl;

    iget-object v2, p0, Lers;->W:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    invoke-direct {v0, v1, v2}, Leri;-><init>(Landroid/content/Context;I)V

    goto :goto_0

    .line 793
    :pswitch_2
    new-instance v0, Leri;

    iget-object v1, p0, Lers;->at:Llnl;

    iget-object v2, p0, Lers;->W:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    const/4 v3, 0x1

    const/4 v4, 0x4

    invoke-direct {v0, v1, v2, v3, v4}, Leri;-><init>(Landroid/content/Context;III)V

    goto :goto_0

    .line 783
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public a(II[B)V
    .locals 10

    .prologue
    const-wide/16 v8, 0xfa

    const v7, 0x7f050006

    const/4 v3, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 494
    invoke-virtual {p0}, Lers;->s()Z

    move-result v0

    if-nez v0, :cond_1

    .line 534
    :cond_0
    :goto_0
    return-void

    .line 498
    :cond_1
    const/4 v0, 0x4

    if-ne p2, v0, :cond_3

    .line 499
    if-ne p1, v3, :cond_2

    .line 500
    invoke-virtual {p0}, Lers;->n()Lz;

    move-result-object v0

    iget-object v1, p0, Lers;->S:Lcom/google/android/apps/plus/views/ReadNotificationListView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/ReadNotificationListView;->setSelection(I)V

    new-instance v1, Landroid/view/animation/TranslateAnimation;

    iget-object v2, p0, Lers;->Q:Lcom/google/android/apps/plus/views/UnreadNotificationListView;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/UnreadNotificationListView;->getHeight()I

    move-result v2

    invoke-direct {p0}, Lers;->ad()I

    move-result v3

    sub-int/2addr v2, v3

    int-to-float v2, v2

    invoke-direct {v1, v5, v5, v5, v2}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    invoke-virtual {v1, v0, v7}, Landroid/view/animation/TranslateAnimation;->setInterpolator(Landroid/content/Context;I)V

    invoke-virtual {v1, v8, v9}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    invoke-virtual {v1, v6}, Landroid/view/animation/TranslateAnimation;->setFillAfter(Z)V

    new-instance v2, Lerw;

    invoke-direct {v2, p0}, Lerw;-><init>(Lers;)V

    invoke-virtual {v1, v2}, Landroid/view/animation/TranslateAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v2, p0, Lers;->S:Lcom/google/android/apps/plus/views/ReadNotificationListView;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/plus/views/ReadNotificationListView;->startAnimation(Landroid/view/animation/Animation;)V

    new-instance v1, Landroid/view/animation/AlphaAnimation;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v1, v5, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    invoke-virtual {v1, v0, v7}, Landroid/view/animation/AlphaAnimation;->setInterpolator(Landroid/content/Context;I)V

    invoke-virtual {v1, v8, v9}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    invoke-virtual {v1, v6}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    new-instance v0, Lerx;

    invoke-direct {v0, p0}, Lerx;-><init>(Lers;)V

    invoke-virtual {v1, v0}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v0, p0, Lers;->Q:Lcom/google/android/apps/plus/views/UnreadNotificationListView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/UnreadNotificationListView;->clearAnimation()V

    iget-object v0, p0, Lers;->Q:Lcom/google/android/apps/plus/views/UnreadNotificationListView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/UnreadNotificationListView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 502
    :cond_2
    new-instance v0, Leru;

    invoke-direct {v0, p0}, Leru;-><init>(Lers;)V

    .line 518
    invoke-virtual {p0}, Lers;->n()Lz;

    move-result-object v1

    .line 502
    new-instance v2, Landroid/view/animation/TranslateAnimation;

    iget-object v3, p0, Lers;->Q:Lcom/google/android/apps/plus/views/UnreadNotificationListView;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/UnreadNotificationListView;->getHeight()I

    move-result v3

    invoke-direct {p0}, Lers;->ad()I

    move-result v4

    sub-int/2addr v3, v4

    int-to-float v3, v3

    invoke-direct {v2, v5, v5, v3, v5}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    invoke-virtual {v2, v1, v7}, Landroid/view/animation/TranslateAnimation;->setInterpolator(Landroid/content/Context;I)V

    invoke-virtual {v2, v8, v9}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    invoke-virtual {v2, v6}, Landroid/view/animation/TranslateAnimation;->setFillAfter(Z)V

    new-instance v3, Lerv;

    invoke-direct {v3, p0, v0}, Lerv;-><init>(Lers;Ljava/lang/Runnable;)V

    invoke-virtual {v2, v3}, Landroid/view/animation/TranslateAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v0, p0, Lers;->S:Lcom/google/android/apps/plus/views/ReadNotificationListView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ReadNotificationListView;->a()V

    iget-object v0, p0, Lers;->S:Lcom/google/android/apps/plus/views/ReadNotificationListView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/ReadNotificationListView;->startAnimation(Landroid/view/animation/Animation;)V

    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v2, v5}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    invoke-virtual {v0, v1, v7}, Landroid/view/animation/AlphaAnimation;->setInterpolator(Landroid/content/Context;I)V

    invoke-virtual {v0, v8, v9}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    invoke-virtual {v0, v6}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    iget-object v1, p0, Lers;->Q:Lcom/google/android/apps/plus/views/UnreadNotificationListView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/UnreadNotificationListView;->clearAnimation()V

    iget-object v1, p0, Lers;->Q:Lcom/google/android/apps/plus/views/UnreadNotificationListView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/views/UnreadNotificationListView;->startAnimation(Landroid/view/animation/Animation;)V

    goto/16 :goto_0

    .line 520
    :cond_3
    if-ne p2, v3, :cond_0

    .line 522
    sget-object v0, Ldsk;->f:Ldsk;

    .line 523
    if-ne p1, v3, :cond_4

    .line 524
    sget-object v0, Ldsk;->h:Ldsk;

    move-object v6, v0

    .line 526
    :goto_1
    iget-object v0, p0, Lers;->at:Llnl;

    iget-object v1, p0, Lers;->W:Lhee;

    .line 527
    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    const/4 v5, 0x0

    move v2, p1

    move-object v4, p3

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;III[BZ)Ljava/lang/Integer;

    move-result-object v0

    .line 526
    invoke-direct {p0, v6, v0}, Lers;->a(Ldsk;Ljava/lang/Integer;)V

    .line 532
    invoke-direct {p0}, Lers;->ac()V

    goto/16 :goto_0

    :cond_4
    move-object v6, v0

    goto :goto_1
.end method

.method public a(Landroid/app/Activity;)V
    .locals 4

    .prologue
    .line 329
    invoke-super {p0, p1}, Llol;->a(Landroid/app/Activity;)V

    .line 331
    :try_start_0
    move-object v0, p1

    check-cast v0, Lesa;

    move-object v1, v0

    iput-object v1, p0, Lers;->N:Lesa;

    .line 332
    iput-object p0, p0, Lers;->U:Lgau;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 336
    return-void

    .line 334
    :catch_0
    move-exception v1

    new-instance v1, Ljava/lang/ClassCastException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, " must implement all appropriate Listeners"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x0

    .line 172
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 174
    if-eqz p1, :cond_2

    .line 176
    invoke-static {}, Ldsk;->values()[Ldsk;

    move-result-object v2

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 177
    invoke-virtual {v4}, Ldsk;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 178
    invoke-virtual {v4}, Ldsk;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {p0, v4, v5}, Lers;->a(Ldsk;Ljava/lang/Integer;)V

    .line 176
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 181
    :cond_1
    const-string v0, "new_notifications_count"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lers;->Y:I

    .line 182
    const-string v0, "last_viewed_notification_version"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lers;->Z:J

    .line 184
    const-string v0, "is_active"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lers;->aa:Z

    .line 185
    const-string v0, "is_showing_read"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lers;->ab:Z

    .line 187
    :cond_2
    invoke-virtual {p0}, Lers;->w()Lbb;

    move-result-object v0

    invoke-virtual {v0, v1, v6, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v6, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v6, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 188
    return-void
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 804
    return-void
.end method

.method public a(Ldo;Landroid/database/Cursor;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v8, 0x4

    const/4 v5, 0x1

    const/4 v7, 0x0

    const/4 v3, 0x2

    .line 808
    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 849
    :cond_0
    :goto_0
    return-void

    .line 812
    :cond_1
    invoke-virtual {p1}, Ldo;->o()I

    move-result v0

    .line 813
    if-nez v0, :cond_3

    .line 814
    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lers;->Y:I

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lers;->Z:J

    iget-object v2, p0, Lers;->R:Lerj;

    invoke-virtual {v2, v0, v1}, Lerj;->a(J)V

    iget-boolean v0, p0, Lers;->aa:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lers;->Y:I

    if-lez v0, :cond_2

    iput v7, p0, Lers;->Y:I

    iget-object v0, p0, Lers;->at:Llnl;

    iget-object v1, p0, Lers;->W:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/service/EsService;->b(Landroid/content/Context;I)Ljava/lang/Integer;

    :cond_2
    iget-object v0, p0, Lers;->N:Lesa;

    invoke-interface {v0}, Lesa;->ax_()V

    goto :goto_0

    .line 817
    :cond_3
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 827
    :pswitch_0
    iget-object v1, p0, Lers;->R:Lerj;

    iget-wide v4, p0, Lers;->Z:J

    .line 829
    invoke-interface {p2}, Landroid/database/Cursor;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    move-object v2, p2

    move v3, v8

    .line 827
    invoke-virtual/range {v1 .. v6}, Lerj;->a(Landroid/database/Cursor;IJLandroid/os/Bundle;)V

    .line 830
    iget-object v0, p0, Lers;->P:Lesb;

    invoke-virtual {v0}, Lesb;->c()V

    .line 831
    iget-object v0, p0, Lers;->N:Lesa;

    invoke-interface {v0}, Lesa;->ax_()V

    .line 835
    iget-boolean v0, p0, Lers;->aa:Z

    if-nez v0, :cond_4

    .line 836
    iget-object v0, p0, Lers;->at:Llnl;

    iget-object v1, p0, Lers;->W:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;I)Ljava/lang/Integer;

    goto :goto_0

    .line 819
    :pswitch_1
    new-instance v0, Lexk;

    invoke-direct {v0, p2, v3, v8}, Lexk;-><init>(Landroid/database/Cursor;II)V

    new-instance v1, Lexk;

    invoke-direct {v1, p2, v3, v3}, Lexk;-><init>(Landroid/database/Cursor;II)V

    iget-object v2, p0, Lers;->T:Lerj;

    invoke-interface {p2}, Landroid/database/Cursor;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {v2, v0, v1, v4}, Lerj;->a(Landroid/database/Cursor;Landroid/database/Cursor;Landroid/os/Bundle;)V

    new-instance v2, Lexk;

    invoke-direct {v2, p2, v5, v3}, Lexk;-><init>(Landroid/database/Cursor;II)V

    iget-object v1, p0, Lers;->R:Lerj;

    iget-wide v4, p0, Lers;->Z:J

    invoke-interface {p2}, Landroid/database/Cursor;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    invoke-virtual/range {v1 .. v6}, Lerj;->a(Landroid/database/Cursor;IJLandroid/os/Bundle;)V

    iget-object v0, p0, Lers;->T:Lerj;

    invoke-virtual {v0}, Lerj;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lers;->T:Lerj;

    invoke-virtual {v0}, Lerj;->e()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lers;->S:Lcom/google/android/apps/plus/views/ReadNotificationListView;

    iget-object v1, p0, Lers;->T:Lerj;

    invoke-virtual {v1}, Lerj;->d()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ReadNotificationListView;->setSelection(I)V

    iget-object v0, p0, Lers;->T:Lerj;

    invoke-virtual {v0}, Lerj;->k()V

    goto/16 :goto_0

    .line 837
    :cond_4
    iget-boolean v0, p0, Lers;->ac:Z

    if-eqz v0, :cond_0

    .line 838
    iget-object v0, p0, Lers;->P:Lesb;

    invoke-virtual {v0}, Lesb;->a()V

    .line 839
    iput-boolean v7, p0, Lers;->ac:Z

    goto/16 :goto_0

    .line 817
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 66
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lers;->a(Ldo;Landroid/database/Cursor;)V

    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 538
    iget-object v0, p0, Lers;->O:Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;->a(Z)V

    .line 539
    return-void
.end method

.method public a([BII)V
    .locals 3

    .prologue
    const/4 v2, 0x4

    .line 467
    iget-object v0, p0, Lers;->at:Llnl;

    iget-object v1, p0, Lers;->W:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    invoke-static {v0, v1, p2, p3, p1}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;III[B)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 469
    const/4 v1, 0x2

    if-ne p2, v1, :cond_1

    .line 470
    if-ne p3, v2, :cond_0

    .line 471
    sget-object v1, Ldsk;->c:Ldsk;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lers;->a(Ldsk;Ljava/lang/Integer;)V

    .line 486
    :goto_0
    iget-object v0, p0, Lers;->V:Lkci;

    invoke-virtual {v0}, Lkci;->e()V

    .line 487
    iget-object v0, p0, Lers;->N:Lesa;

    invoke-interface {v0}, Lesa;->ax_()V

    .line 488
    return-void

    .line 473
    :cond_0
    sget-object v1, Ldsk;->d:Ldsk;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lers;->a(Ldsk;Ljava/lang/Integer;)V

    goto :goto_0

    .line 476
    :cond_1
    if-ne p3, v2, :cond_2

    .line 477
    sget-object v1, Ldsk;->a:Ldsk;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lers;->a(Ldsk;Ljava/lang/Integer;)V

    goto :goto_0

    .line 479
    :cond_2
    sget-object v1, Ldsk;->b:Ldsk;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lers;->a(Ldsk;Ljava/lang/Integer;)V

    goto :goto_0
.end method

.method public aO_()V
    .locals 3

    .prologue
    .line 341
    invoke-super {p0}, Llol;->aO_()V

    .line 342
    iget-object v0, p0, Lers;->ad:Lfhh;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(Lfhh;)V

    .line 343
    iget-object v0, p0, Lers;->X:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/apps/plus/service/EsService;->a(I)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(I)Lfib;

    move-result-object v0

    invoke-direct {p0, v2, v0}, Lers;->a(ILfib;)V

    goto :goto_0

    .line 344
    :cond_1
    iget-object v0, p0, Lers;->V:Lkci;

    if-eqz v0, :cond_2

    .line 345
    iget-object v0, p0, Lers;->V:Lkci;

    invoke-virtual {v0}, Lkci;->e()V

    .line 347
    :cond_2
    iget-boolean v0, p0, Lers;->aa:Z

    if-eqz v0, :cond_3

    .line 348
    const/4 v0, 0x1

    iput-boolean v0, p0, Lers;->ac:Z

    .line 349
    iget-object v0, p0, Lers;->P:Lesb;

    invoke-virtual {v0}, Lesb;->a()V

    .line 351
    :cond_3
    return-void
.end method

.method public aQ_()Z
    .locals 1

    .prologue
    .line 940
    sget-object v0, Ldsk;->e:Ldsk;

    invoke-direct {p0, v0}, Lers;->a(Ldsk;)Z

    move-result v0

    return v0
.end method

.method public aa()I
    .locals 1

    .prologue
    .line 875
    iget v0, p0, Lers;->Y:I

    return v0
.end method

.method public ab()Z
    .locals 1

    .prologue
    .line 928
    sget-object v0, Ldsk;->e:Ldsk;

    invoke-direct {p0, v0}, Lers;->a(Ldsk;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Ldsk;->a:Ldsk;

    .line 929
    invoke-direct {p0, v0}, Lers;->a(Ldsk;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Ldsk;->c:Ldsk;

    .line 930
    invoke-direct {p0, v0}, Lers;->a(Ldsk;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Ldsk;->b:Ldsk;

    .line 931
    invoke-direct {p0, v0}, Lers;->a(Ldsk;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Ldsk;->b:Ldsk;

    .line 932
    invoke-direct {p0, v0}, Lers;->a(Ldsk;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 933
    :cond_0
    const/4 v0, 0x1

    .line 935
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Z)V
    .locals 4

    .prologue
    .line 369
    iput-boolean p1, p0, Lers;->aa:Z

    .line 370
    iget-object v0, p0, Lers;->V:Lkci;

    invoke-virtual {v0, p1}, Lkci;->b(Z)V

    .line 372
    if-eqz p1, :cond_0

    .line 373
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lers;->c(Z)V

    .line 380
    :goto_0
    return-void

    .line 375
    :cond_0
    iget-object v0, p0, Lers;->R:Lerj;

    invoke-virtual {v0}, Lerj;->g()J

    move-result-wide v0

    iget-wide v2, p0, Lers;->Z:J

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    iget-object v2, p0, Lers;->at:Llnl;

    iget-object v3, p0, Lers;->W:Lhee;

    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    invoke-static {v2, v3, v0, v1}, Lcom/google/android/apps/plus/service/EsService;->b(Landroid/content/Context;IJ)Ljava/lang/Integer;

    .line 376
    :cond_1
    invoke-virtual {p0}, Lers;->X()V

    .line 377
    iget-object v0, p0, Lers;->T:Lerj;

    invoke-virtual {v0}, Lerj;->c()V

    .line 378
    iget-object v0, p0, Lers;->R:Lerj;

    invoke-virtual {v0}, Lerj;->c()V

    goto :goto_0
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 166
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 167
    iget-object v0, p0, Lers;->au:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Lers;->W:Lhee;

    .line 168
    return-void
.end method

.method public c(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 412
    iget-object v0, p0, Lers;->P:Lesb;

    invoke-virtual {v0, p1}, Lesb;->c(Landroid/view/View;)V

    .line 413
    return-void
.end method

.method public e()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 420
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lers;->c(Z)V

    .line 423
    invoke-virtual {p0}, Lers;->X()V

    .line 424
    iget-object v0, p0, Lers;->S:Lcom/google/android/apps/plus/views/ReadNotificationListView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ReadNotificationListView;->setSelection(I)V

    .line 425
    iget-object v0, p0, Lers;->Q:Lcom/google/android/apps/plus/views/UnreadNotificationListView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/UnreadNotificationListView;->setSelection(I)V

    .line 426
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 293
    iget-object v0, p0, Lers;->X:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 294
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldsk;

    invoke-virtual {v1}, Ldsk;->name()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0

    .line 296
    :cond_0
    const-string v0, "new_notifications_count"

    iget v1, p0, Lers;->Y:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 297
    const-string v0, "last_viewed_notification_version"

    iget-wide v2, p0, Lers;->Z:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 298
    const-string v0, "is_active"

    iget-boolean v1, p0, Lers;->aa:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 299
    const-string v0, "is_showing_read"

    iget-boolean v1, p0, Lers;->ab:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 301
    const-string v1, "read_notification_bar_visible"

    iget-object v0, p0, Lers;->S:Lcom/google/android/apps/plus/views/ReadNotificationListView;

    .line 302
    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ReadNotificationListView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    .line 301
    :goto_1
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 303
    const-string v0, "low_pri_read_expanded"

    iget-object v1, p0, Lers;->T:Lerj;

    .line 304
    invoke-virtual {v1}, Lerj;->l()Z

    move-result v1

    .line 303
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 305
    const-string v0, "low_pri_read_expanded_ever"

    iget-object v1, p0, Lers;->T:Lerj;

    .line 306
    invoke-virtual {v1}, Lerj;->m()Z

    move-result v1

    .line 305
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 307
    const-string v0, "low_pri_unread_expanded"

    iget-object v1, p0, Lers;->R:Lerj;

    .line 308
    invoke-virtual {v1}, Lerj;->l()Z

    move-result v1

    .line 307
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 309
    const-string v0, "low_pri_unread_expanded_ever"

    iget-object v1, p0, Lers;->R:Lerj;

    .line 310
    invoke-virtual {v1}, Lerj;->m()Z

    move-result v1

    .line 309
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 312
    iget-object v0, p0, Lers;->T:Lerj;

    .line 313
    invoke-virtual {v0}, Lerj;->n()Lmyx;

    move-result-object v0

    .line 314
    if-eqz v0, :cond_1

    .line 315
    const-string v1, "low_pri_read_summary"

    .line 316
    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    .line 315
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 319
    :cond_1
    iget-object v0, p0, Lers;->R:Lerj;

    .line 320
    invoke-virtual {v0}, Lerj;->n()Lmyx;

    move-result-object v0

    .line 321
    if-eqz v0, :cond_2

    .line 322
    const-string v1, "low_pri_unread_summary"

    .line 323
    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    .line 322
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 325
    :cond_2
    return-void

    .line 302
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 593
    iget-object v0, p0, Lers;->Q:Lcom/google/android/apps/plus/views/UnreadNotificationListView;

    if-ne p1, v0, :cond_1

    .line 594
    iget-object v0, p0, Lers;->R:Lerj;

    iget-object v1, p0, Lers;->at:Llnl;

    iget-object v2, p0, Lers;->W:Lhee;

    .line 595
    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    .line 594
    invoke-virtual {v0, v1, v2, p3}, Lerj;->a(Landroid/content/Context;II)V

    .line 600
    :cond_0
    :goto_0
    return-void

    .line 596
    :cond_1
    iget-object v0, p0, Lers;->S:Lcom/google/android/apps/plus/views/ReadNotificationListView;

    if-ne p1, v0, :cond_0

    .line 597
    iget-object v0, p0, Lers;->T:Lerj;

    iget-object v1, p0, Lers;->at:Llnl;

    iget-object v2, p0, Lers;->W:Lhee;

    .line 598
    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    .line 597
    invoke-virtual {v0, v1, v2, p3}, Lerj;->a(Landroid/content/Context;II)V

    goto :goto_0
.end method

.method public onMovedToScrapHeap(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 605
    invoke-static {p1}, Llii;->f(Landroid/view/View;)V

    .line 606
    return-void
.end method

.method public z()V
    .locals 1

    .prologue
    .line 355
    invoke-super {p0}, Llol;->z()V

    .line 356
    iget-object v0, p0, Lers;->ad:Lfhh;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(Lfhh;)V

    .line 357
    return-void
.end method
