.class public final Lpob;
.super Loxq;
.source "PG"


# instance fields
.field private a:[Lpoc;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Loxq;-><init>()V

    .line 94
    sget-object v0, Lpoc;->a:[Lpoc;

    iput-object v0, p0, Lpob;->a:[Lpoc;

    .line 14
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 112
    .line 113
    iget-object v1, p0, Lpob;->a:[Lpoc;

    if-eqz v1, :cond_1

    .line 114
    iget-object v2, p0, Lpob;->a:[Lpoc;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 115
    if-eqz v4, :cond_0

    .line 116
    const/4 v5, 0x1

    .line 117
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 114
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 121
    :cond_1
    iget-object v1, p0, Lpob;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 122
    iput v0, p0, Lpob;->ai:I

    .line 123
    return v0
.end method

.method public a(Loxn;)Lpob;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 131
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 132
    sparse-switch v0, :sswitch_data_0

    .line 136
    iget-object v2, p0, Lpob;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 137
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lpob;->ah:Ljava/util/List;

    .line 140
    :cond_1
    iget-object v2, p0, Lpob;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 142
    :sswitch_0
    return-object p0

    .line 147
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 148
    iget-object v0, p0, Lpob;->a:[Lpoc;

    if-nez v0, :cond_3

    move v0, v1

    .line 149
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lpoc;

    .line 150
    iget-object v3, p0, Lpob;->a:[Lpoc;

    if-eqz v3, :cond_2

    .line 151
    iget-object v3, p0, Lpob;->a:[Lpoc;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 153
    :cond_2
    iput-object v2, p0, Lpob;->a:[Lpoc;

    .line 154
    :goto_2
    iget-object v2, p0, Lpob;->a:[Lpoc;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 155
    iget-object v2, p0, Lpob;->a:[Lpoc;

    new-instance v3, Lpoc;

    invoke-direct {v3}, Lpoc;-><init>()V

    aput-object v3, v2, v0

    .line 156
    iget-object v2, p0, Lpob;->a:[Lpoc;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 157
    invoke-virtual {p1}, Loxn;->a()I

    .line 154
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 148
    :cond_3
    iget-object v0, p0, Lpob;->a:[Lpoc;

    array-length v0, v0

    goto :goto_1

    .line 160
    :cond_4
    iget-object v2, p0, Lpob;->a:[Lpoc;

    new-instance v3, Lpoc;

    invoke-direct {v3}, Lpoc;-><init>()V

    aput-object v3, v2, v0

    .line 161
    iget-object v2, p0, Lpob;->a:[Lpoc;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 132
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 99
    iget-object v0, p0, Lpob;->a:[Lpoc;

    if-eqz v0, :cond_1

    .line 100
    iget-object v1, p0, Lpob;->a:[Lpoc;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 101
    if-eqz v3, :cond_0

    .line 102
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 100
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 106
    :cond_1
    iget-object v0, p0, Lpob;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 108
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 10
    invoke-virtual {p0, p1}, Lpob;->a(Loxn;)Lpob;

    move-result-object v0

    return-object v0
.end method
