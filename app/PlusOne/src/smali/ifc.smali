.class public final Lifc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lifc;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/io/File;

.field public b:J

.field public c:J

.field public d:Z


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 2

    .prologue
    .line 392
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 393
    iput-object p1, p0, Lifc;->a:Ljava/io/File;

    .line 394
    invoke-virtual {p1}, Ljava/io/File;->lastModified()J

    move-result-wide v0

    iput-wide v0, p0, Lifc;->b:J

    .line 395
    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v0

    iput-wide v0, p0, Lifc;->c:J

    .line 396
    return-void
.end method


# virtual methods
.method public a(Lifc;)I
    .locals 4

    .prologue
    .line 400
    iget-boolean v0, p0, Lifc;->d:Z

    if-eqz v0, :cond_1

    .line 401
    iget-boolean v0, p1, Lifc;->d:Z

    if-nez v0, :cond_0

    .line 402
    const/4 v0, 0x1

    .line 413
    :goto_0
    return v0

    .line 406
    :cond_0
    iget-wide v0, p0, Lifc;->b:J

    iget-wide v2, p1, Lifc;->b:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    goto :goto_0

    .line 408
    :cond_1
    iget-boolean v0, p1, Lifc;->d:Z

    if-eqz v0, :cond_2

    .line 409
    const/4 v0, -0x1

    goto :goto_0

    .line 413
    :cond_2
    iget-wide v0, p1, Lifc;->c:J

    iget-wide v2, p0, Lifc;->c:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    goto :goto_0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 386
    check-cast p1, Lifc;

    invoke-virtual {p0, p1}, Lifc;->a(Lifc;)I

    move-result v0

    return v0
.end method
