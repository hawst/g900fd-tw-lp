.class public final Lmmi;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lmmk;

.field private b:Lmmg;

.field private c:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 426
    invoke-direct {p0}, Loxq;-><init>()V

    .line 429
    sget-object v0, Lmmk;->a:[Lmmk;

    iput-object v0, p0, Lmmi;->a:[Lmmk;

    .line 432
    const/4 v0, 0x0

    iput-object v0, p0, Lmmi;->b:Lmmg;

    .line 426
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 458
    .line 459
    iget-object v1, p0, Lmmi;->a:[Lmmk;

    if-eqz v1, :cond_1

    .line 460
    iget-object v2, p0, Lmmi;->a:[Lmmk;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 461
    if-eqz v4, :cond_0

    .line 462
    const/4 v5, 0x1

    .line 463
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 460
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 467
    :cond_1
    iget-object v1, p0, Lmmi;->b:Lmmg;

    if-eqz v1, :cond_2

    .line 468
    const/4 v1, 0x2

    iget-object v2, p0, Lmmi;->b:Lmmg;

    .line 469
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 471
    :cond_2
    iget-object v1, p0, Lmmi;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 472
    const/4 v1, 0x3

    iget-object v2, p0, Lmmi;->c:Ljava/lang/Boolean;

    .line 473
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 475
    :cond_3
    iget-object v1, p0, Lmmi;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 476
    iput v0, p0, Lmmi;->ai:I

    .line 477
    return v0
.end method

.method public a(Loxn;)Lmmi;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 485
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 486
    sparse-switch v0, :sswitch_data_0

    .line 490
    iget-object v2, p0, Lmmi;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 491
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmmi;->ah:Ljava/util/List;

    .line 494
    :cond_1
    iget-object v2, p0, Lmmi;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 496
    :sswitch_0
    return-object p0

    .line 501
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 502
    iget-object v0, p0, Lmmi;->a:[Lmmk;

    if-nez v0, :cond_3

    move v0, v1

    .line 503
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lmmk;

    .line 504
    iget-object v3, p0, Lmmi;->a:[Lmmk;

    if-eqz v3, :cond_2

    .line 505
    iget-object v3, p0, Lmmi;->a:[Lmmk;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 507
    :cond_2
    iput-object v2, p0, Lmmi;->a:[Lmmk;

    .line 508
    :goto_2
    iget-object v2, p0, Lmmi;->a:[Lmmk;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 509
    iget-object v2, p0, Lmmi;->a:[Lmmk;

    new-instance v3, Lmmk;

    invoke-direct {v3}, Lmmk;-><init>()V

    aput-object v3, v2, v0

    .line 510
    iget-object v2, p0, Lmmi;->a:[Lmmk;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 511
    invoke-virtual {p1}, Loxn;->a()I

    .line 508
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 502
    :cond_3
    iget-object v0, p0, Lmmi;->a:[Lmmk;

    array-length v0, v0

    goto :goto_1

    .line 514
    :cond_4
    iget-object v2, p0, Lmmi;->a:[Lmmk;

    new-instance v3, Lmmk;

    invoke-direct {v3}, Lmmk;-><init>()V

    aput-object v3, v2, v0

    .line 515
    iget-object v2, p0, Lmmi;->a:[Lmmk;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 519
    :sswitch_2
    iget-object v0, p0, Lmmi;->b:Lmmg;

    if-nez v0, :cond_5

    .line 520
    new-instance v0, Lmmg;

    invoke-direct {v0}, Lmmg;-><init>()V

    iput-object v0, p0, Lmmi;->b:Lmmg;

    .line 522
    :cond_5
    iget-object v0, p0, Lmmi;->b:Lmmg;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 526
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmmi;->c:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 486
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 439
    iget-object v0, p0, Lmmi;->a:[Lmmk;

    if-eqz v0, :cond_1

    .line 440
    iget-object v1, p0, Lmmi;->a:[Lmmk;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 441
    if-eqz v3, :cond_0

    .line 442
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 440
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 446
    :cond_1
    iget-object v0, p0, Lmmi;->b:Lmmg;

    if-eqz v0, :cond_2

    .line 447
    const/4 v0, 0x2

    iget-object v1, p0, Lmmi;->b:Lmmg;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 449
    :cond_2
    iget-object v0, p0, Lmmi;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 450
    const/4 v0, 0x3

    iget-object v1, p0, Lmmi;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 452
    :cond_3
    iget-object v0, p0, Lmmi;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 454
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 422
    invoke-virtual {p0, p1}, Lmmi;->a(Loxn;)Lmmi;

    move-result-object v0

    return-object v0
.end method
