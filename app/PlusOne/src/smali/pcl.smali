.class public final Lpcl;
.super Loxq;
.source "PG"


# static fields
.field public static final a:Loxr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Loxr",
            "<",
            "Lpcl;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Lpcj;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Lpdi;

.field private j:Ljava/lang/String;

.field private k:[Loya;

.field private l:Loya;

.field private m:[Loya;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;

.field private s:Loya;

.field private t:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 9
    const v0, 0x275347f

    new-instance v1, Lpcm;

    invoke-direct {v1}, Lpcm;-><init>()V

    .line 14
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    move-result-object v0

    sput-object v0, Lpcl;->a:Loxr;

    .line 13
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 27
    iput-object v1, p0, Lpcl;->i:Lpdi;

    .line 32
    sget-object v0, Loya;->a:[Loya;

    iput-object v0, p0, Lpcl;->k:[Loya;

    .line 35
    iput-object v1, p0, Lpcl;->l:Loya;

    .line 38
    sget-object v0, Loya;->a:[Loya;

    iput-object v0, p0, Lpcl;->m:[Loya;

    .line 53
    iput-object v1, p0, Lpcl;->d:Lpcj;

    .line 56
    iput-object v1, p0, Lpcl;->s:Loya;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 134
    .line 135
    iget-object v0, p0, Lpcl;->e:Ljava/lang/String;

    if-eqz v0, :cond_14

    .line 136
    const/4 v0, 0x1

    iget-object v2, p0, Lpcl;->e:Ljava/lang/String;

    .line 137
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 139
    :goto_0
    iget-object v2, p0, Lpcl;->f:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 140
    const/4 v2, 0x2

    iget-object v3, p0, Lpcl;->f:Ljava/lang/String;

    .line 141
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 143
    :cond_0
    iget-object v2, p0, Lpcl;->b:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 144
    const/4 v2, 0x3

    iget-object v3, p0, Lpcl;->b:Ljava/lang/String;

    .line 145
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 147
    :cond_1
    iget-object v2, p0, Lpcl;->g:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 148
    const/4 v2, 0x4

    iget-object v3, p0, Lpcl;->g:Ljava/lang/String;

    .line 149
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 151
    :cond_2
    iget-object v2, p0, Lpcl;->h:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 152
    const/4 v2, 0x5

    iget-object v3, p0, Lpcl;->h:Ljava/lang/String;

    .line 153
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 155
    :cond_3
    iget-object v2, p0, Lpcl;->i:Lpdi;

    if-eqz v2, :cond_4

    .line 156
    const/4 v2, 0x6

    iget-object v3, p0, Lpcl;->i:Lpdi;

    .line 157
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 159
    :cond_4
    iget-object v2, p0, Lpcl;->j:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 160
    const/4 v2, 0x7

    iget-object v3, p0, Lpcl;->j:Ljava/lang/String;

    .line 161
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 163
    :cond_5
    iget-object v2, p0, Lpcl;->k:[Loya;

    if-eqz v2, :cond_7

    .line 164
    iget-object v3, p0, Lpcl;->k:[Loya;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_7

    aget-object v5, v3, v2

    .line 165
    if-eqz v5, :cond_6

    .line 166
    const/16 v6, 0x8

    .line 167
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 164
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 171
    :cond_7
    iget-object v2, p0, Lpcl;->l:Loya;

    if-eqz v2, :cond_8

    .line 172
    const/16 v2, 0x9

    iget-object v3, p0, Lpcl;->l:Loya;

    .line 173
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 175
    :cond_8
    iget-object v2, p0, Lpcl;->m:[Loya;

    if-eqz v2, :cond_a

    .line 176
    iget-object v2, p0, Lpcl;->m:[Loya;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_a

    aget-object v4, v2, v1

    .line 177
    if-eqz v4, :cond_9

    .line 178
    const/16 v5, 0xb

    .line 179
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 176
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 183
    :cond_a
    iget-object v1, p0, Lpcl;->n:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 184
    const/16 v1, 0xc

    iget-object v2, p0, Lpcl;->n:Ljava/lang/String;

    .line 185
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 187
    :cond_b
    iget-object v1, p0, Lpcl;->o:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 188
    const/16 v1, 0x4b

    iget-object v2, p0, Lpcl;->o:Ljava/lang/String;

    .line 189
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 191
    :cond_c
    iget-object v1, p0, Lpcl;->c:Ljava/lang/String;

    if-eqz v1, :cond_d

    .line 192
    const/16 v1, 0x5b

    iget-object v2, p0, Lpcl;->c:Ljava/lang/String;

    .line 193
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 195
    :cond_d
    iget-object v1, p0, Lpcl;->p:Ljava/lang/String;

    if-eqz v1, :cond_e

    .line 196
    const/16 v1, 0x5c

    iget-object v2, p0, Lpcl;->p:Ljava/lang/String;

    .line 197
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 199
    :cond_e
    iget-object v1, p0, Lpcl;->q:Ljava/lang/String;

    if-eqz v1, :cond_f

    .line 200
    const/16 v1, 0x5d

    iget-object v2, p0, Lpcl;->q:Ljava/lang/String;

    .line 201
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 203
    :cond_f
    iget-object v1, p0, Lpcl;->r:Ljava/lang/String;

    if-eqz v1, :cond_10

    .line 204
    const/16 v1, 0x5e

    iget-object v2, p0, Lpcl;->r:Ljava/lang/String;

    .line 205
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 207
    :cond_10
    iget-object v1, p0, Lpcl;->d:Lpcj;

    if-eqz v1, :cond_11

    .line 208
    const/16 v1, 0x5f

    iget-object v2, p0, Lpcl;->d:Lpcj;

    .line 209
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 211
    :cond_11
    iget-object v1, p0, Lpcl;->s:Loya;

    if-eqz v1, :cond_12

    .line 212
    const/16 v1, 0xb9

    iget-object v2, p0, Lpcl;->s:Loya;

    .line 213
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 215
    :cond_12
    iget-object v1, p0, Lpcl;->t:Ljava/lang/String;

    if-eqz v1, :cond_13

    .line 216
    const/16 v1, 0xfe

    iget-object v2, p0, Lpcl;->t:Ljava/lang/String;

    .line 217
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 219
    :cond_13
    iget-object v1, p0, Lpcl;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 220
    iput v0, p0, Lpcl;->ai:I

    .line 221
    return v0

    :cond_14
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lpcl;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 229
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 230
    sparse-switch v0, :sswitch_data_0

    .line 234
    iget-object v2, p0, Lpcl;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 235
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lpcl;->ah:Ljava/util/List;

    .line 238
    :cond_1
    iget-object v2, p0, Lpcl;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 240
    :sswitch_0
    return-object p0

    .line 245
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpcl;->e:Ljava/lang/String;

    goto :goto_0

    .line 249
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpcl;->f:Ljava/lang/String;

    goto :goto_0

    .line 253
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpcl;->b:Ljava/lang/String;

    goto :goto_0

    .line 257
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpcl;->g:Ljava/lang/String;

    goto :goto_0

    .line 261
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpcl;->h:Ljava/lang/String;

    goto :goto_0

    .line 265
    :sswitch_6
    iget-object v0, p0, Lpcl;->i:Lpdi;

    if-nez v0, :cond_2

    .line 266
    new-instance v0, Lpdi;

    invoke-direct {v0}, Lpdi;-><init>()V

    iput-object v0, p0, Lpcl;->i:Lpdi;

    .line 268
    :cond_2
    iget-object v0, p0, Lpcl;->i:Lpdi;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 272
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpcl;->j:Ljava/lang/String;

    goto :goto_0

    .line 276
    :sswitch_8
    const/16 v0, 0x42

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 277
    iget-object v0, p0, Lpcl;->k:[Loya;

    if-nez v0, :cond_4

    move v0, v1

    .line 278
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Loya;

    .line 279
    iget-object v3, p0, Lpcl;->k:[Loya;

    if-eqz v3, :cond_3

    .line 280
    iget-object v3, p0, Lpcl;->k:[Loya;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 282
    :cond_3
    iput-object v2, p0, Lpcl;->k:[Loya;

    .line 283
    :goto_2
    iget-object v2, p0, Lpcl;->k:[Loya;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 284
    iget-object v2, p0, Lpcl;->k:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 285
    iget-object v2, p0, Lpcl;->k:[Loya;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 286
    invoke-virtual {p1}, Loxn;->a()I

    .line 283
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 277
    :cond_4
    iget-object v0, p0, Lpcl;->k:[Loya;

    array-length v0, v0

    goto :goto_1

    .line 289
    :cond_5
    iget-object v2, p0, Lpcl;->k:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 290
    iget-object v2, p0, Lpcl;->k:[Loya;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 294
    :sswitch_9
    iget-object v0, p0, Lpcl;->l:Loya;

    if-nez v0, :cond_6

    .line 295
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    iput-object v0, p0, Lpcl;->l:Loya;

    .line 297
    :cond_6
    iget-object v0, p0, Lpcl;->l:Loya;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 301
    :sswitch_a
    const/16 v0, 0x5a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 302
    iget-object v0, p0, Lpcl;->m:[Loya;

    if-nez v0, :cond_8

    move v0, v1

    .line 303
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Loya;

    .line 304
    iget-object v3, p0, Lpcl;->m:[Loya;

    if-eqz v3, :cond_7

    .line 305
    iget-object v3, p0, Lpcl;->m:[Loya;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 307
    :cond_7
    iput-object v2, p0, Lpcl;->m:[Loya;

    .line 308
    :goto_4
    iget-object v2, p0, Lpcl;->m:[Loya;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    .line 309
    iget-object v2, p0, Lpcl;->m:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 310
    iget-object v2, p0, Lpcl;->m:[Loya;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 311
    invoke-virtual {p1}, Loxn;->a()I

    .line 308
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 302
    :cond_8
    iget-object v0, p0, Lpcl;->m:[Loya;

    array-length v0, v0

    goto :goto_3

    .line 314
    :cond_9
    iget-object v2, p0, Lpcl;->m:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 315
    iget-object v2, p0, Lpcl;->m:[Loya;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 319
    :sswitch_b
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpcl;->n:Ljava/lang/String;

    goto/16 :goto_0

    .line 323
    :sswitch_c
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpcl;->o:Ljava/lang/String;

    goto/16 :goto_0

    .line 327
    :sswitch_d
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpcl;->c:Ljava/lang/String;

    goto/16 :goto_0

    .line 331
    :sswitch_e
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpcl;->p:Ljava/lang/String;

    goto/16 :goto_0

    .line 335
    :sswitch_f
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpcl;->q:Ljava/lang/String;

    goto/16 :goto_0

    .line 339
    :sswitch_10
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpcl;->r:Ljava/lang/String;

    goto/16 :goto_0

    .line 343
    :sswitch_11
    iget-object v0, p0, Lpcl;->d:Lpcj;

    if-nez v0, :cond_a

    .line 344
    new-instance v0, Lpcj;

    invoke-direct {v0}, Lpcj;-><init>()V

    iput-object v0, p0, Lpcl;->d:Lpcj;

    .line 346
    :cond_a
    iget-object v0, p0, Lpcl;->d:Lpcj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 350
    :sswitch_12
    iget-object v0, p0, Lpcl;->s:Loya;

    if-nez v0, :cond_b

    .line 351
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    iput-object v0, p0, Lpcl;->s:Loya;

    .line 353
    :cond_b
    iget-object v0, p0, Lpcl;->s:Loya;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 357
    :sswitch_13
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpcl;->t:Ljava/lang/String;

    goto/16 :goto_0

    .line 230
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x5a -> :sswitch_a
        0x62 -> :sswitch_b
        0x25a -> :sswitch_c
        0x2da -> :sswitch_d
        0x2e2 -> :sswitch_e
        0x2ea -> :sswitch_f
        0x2f2 -> :sswitch_10
        0x2fa -> :sswitch_11
        0x5ca -> :sswitch_12
        0x7f2 -> :sswitch_13
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 63
    iget-object v1, p0, Lpcl;->e:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 64
    const/4 v1, 0x1

    iget-object v2, p0, Lpcl;->e:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 66
    :cond_0
    iget-object v1, p0, Lpcl;->f:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 67
    const/4 v1, 0x2

    iget-object v2, p0, Lpcl;->f:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 69
    :cond_1
    iget-object v1, p0, Lpcl;->b:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 70
    const/4 v1, 0x3

    iget-object v2, p0, Lpcl;->b:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 72
    :cond_2
    iget-object v1, p0, Lpcl;->g:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 73
    const/4 v1, 0x4

    iget-object v2, p0, Lpcl;->g:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 75
    :cond_3
    iget-object v1, p0, Lpcl;->h:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 76
    const/4 v1, 0x5

    iget-object v2, p0, Lpcl;->h:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 78
    :cond_4
    iget-object v1, p0, Lpcl;->i:Lpdi;

    if-eqz v1, :cond_5

    .line 79
    const/4 v1, 0x6

    iget-object v2, p0, Lpcl;->i:Lpdi;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 81
    :cond_5
    iget-object v1, p0, Lpcl;->j:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 82
    const/4 v1, 0x7

    iget-object v2, p0, Lpcl;->j:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 84
    :cond_6
    iget-object v1, p0, Lpcl;->k:[Loya;

    if-eqz v1, :cond_8

    .line 85
    iget-object v2, p0, Lpcl;->k:[Loya;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_8

    aget-object v4, v2, v1

    .line 86
    if-eqz v4, :cond_7

    .line 87
    const/16 v5, 0x8

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 85
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 91
    :cond_8
    iget-object v1, p0, Lpcl;->l:Loya;

    if-eqz v1, :cond_9

    .line 92
    const/16 v1, 0x9

    iget-object v2, p0, Lpcl;->l:Loya;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 94
    :cond_9
    iget-object v1, p0, Lpcl;->m:[Loya;

    if-eqz v1, :cond_b

    .line 95
    iget-object v1, p0, Lpcl;->m:[Loya;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_b

    aget-object v3, v1, v0

    .line 96
    if-eqz v3, :cond_a

    .line 97
    const/16 v4, 0xb

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 95
    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 101
    :cond_b
    iget-object v0, p0, Lpcl;->n:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 102
    const/16 v0, 0xc

    iget-object v1, p0, Lpcl;->n:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 104
    :cond_c
    iget-object v0, p0, Lpcl;->o:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 105
    const/16 v0, 0x4b

    iget-object v1, p0, Lpcl;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 107
    :cond_d
    iget-object v0, p0, Lpcl;->c:Ljava/lang/String;

    if-eqz v0, :cond_e

    .line 108
    const/16 v0, 0x5b

    iget-object v1, p0, Lpcl;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 110
    :cond_e
    iget-object v0, p0, Lpcl;->p:Ljava/lang/String;

    if-eqz v0, :cond_f

    .line 111
    const/16 v0, 0x5c

    iget-object v1, p0, Lpcl;->p:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 113
    :cond_f
    iget-object v0, p0, Lpcl;->q:Ljava/lang/String;

    if-eqz v0, :cond_10

    .line 114
    const/16 v0, 0x5d

    iget-object v1, p0, Lpcl;->q:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 116
    :cond_10
    iget-object v0, p0, Lpcl;->r:Ljava/lang/String;

    if-eqz v0, :cond_11

    .line 117
    const/16 v0, 0x5e

    iget-object v1, p0, Lpcl;->r:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 119
    :cond_11
    iget-object v0, p0, Lpcl;->d:Lpcj;

    if-eqz v0, :cond_12

    .line 120
    const/16 v0, 0x5f

    iget-object v1, p0, Lpcl;->d:Lpcj;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 122
    :cond_12
    iget-object v0, p0, Lpcl;->s:Loya;

    if-eqz v0, :cond_13

    .line 123
    const/16 v0, 0xb9

    iget-object v1, p0, Lpcl;->s:Loya;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 125
    :cond_13
    iget-object v0, p0, Lpcl;->t:Ljava/lang/String;

    if-eqz v0, :cond_14

    .line 126
    const/16 v0, 0xfe

    iget-object v1, p0, Lpcl;->t:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 128
    :cond_14
    iget-object v0, p0, Lpcl;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 130
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lpcl;->a(Loxn;)Lpcl;

    move-result-object v0

    return-object v0
.end method
