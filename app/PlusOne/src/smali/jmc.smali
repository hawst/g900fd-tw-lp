.class public final Ljmc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llnx;
.implements Llrg;


# instance fields
.field private final a:Lz;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljmb;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lhee;

.field private d:Lkbz;

.field private e:Ljme;

.field private f:Z


# direct methods
.method public constructor <init>(Lz;Llqr;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Ljmc;->a:Lz;

    .line 34
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 35
    return-void
.end method


# virtual methods
.method public a()V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 46
    iget-object v0, p0, Ljmc;->c:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v3

    .line 47
    const/4 v0, -0x1

    if-ne v3, v0, :cond_1

    .line 65
    :cond_0
    :goto_0
    return-void

    .line 50
    :cond_1
    iget-object v0, p0, Ljmc;->e:Ljme;

    iget-object v4, p0, Ljmc;->a:Lz;

    invoke-virtual {v0, v4, v3}, Ljme;->b(Landroid/content/Context;I)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Ljmc;->f:Z

    .line 52
    iget-object v0, p0, Ljmc;->d:Lkbz;

    iget-object v4, p0, Ljmc;->a:Lz;

    invoke-virtual {v0, v4, v3}, Lkbz;->a(Landroid/content/Context;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 55
    iget-object v0, p0, Ljmc;->a:Lz;

    invoke-virtual {v0}, Lz;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "skip_interstitials"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 58
    iget-object v0, p0, Ljmc;->c:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v4

    iget-object v0, p0, Ljmc;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    move v3, v2

    :goto_2
    if-ge v3, v5, :cond_4

    iget-object v0, p0, Ljmc;->b:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljmb;

    iget-object v6, p0, Ljmc;->a:Lz;

    iget-boolean v7, p0, Ljmc;->f:Z

    invoke-interface {v0, v6, v4, v7}, Ljmb;->a(Landroid/content/Context;IZ)I

    move-result v0

    if-ne v0, v1, :cond_3

    :goto_3
    if-eqz v1, :cond_0

    .line 62
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Ljmc;->a:Lz;

    const-class v2, Lcom/google/android/libraries/social/onboarding/InterstitialsActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 63
    const-string v1, "account_id"

    iget-object v2, p0, Ljmc;->c:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 64
    iget-object v1, p0, Ljmc;->a:Lz;

    invoke-virtual {v1, v0}, Lz;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_2
    move v0, v2

    .line 50
    goto :goto_1

    .line 58
    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    :cond_4
    move v1, v2

    goto :goto_3
.end method

.method public a(Landroid/content/Context;Llnh;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 39
    const-class v0, Ljmb;

    invoke-virtual {p2, v0}, Llnh;->c(Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Ljmc;->b:Ljava/util/List;

    .line 40
    const-class v0, Lhee;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Ljmc;->c:Lhee;

    .line 41
    const-class v0, Lkbz;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkbz;

    iput-object v0, p0, Ljmc;->d:Lkbz;

    .line 42
    const-class v0, Ljme;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljme;

    iput-object v0, p0, Ljmc;->e:Ljme;

    .line 43
    return-void
.end method
