.class public final Lmku;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lovv;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 20977
    invoke-direct {p0}, Loxq;-><init>()V

    .line 20980
    iput-object v0, p0, Lmku;->apiHeader:Llyq;

    .line 20983
    iput-object v0, p0, Lmku;->a:Lovv;

    .line 20977
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 21000
    const/4 v0, 0x0

    .line 21001
    iget-object v1, p0, Lmku;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 21002
    const/4 v0, 0x1

    iget-object v1, p0, Lmku;->apiHeader:Llyq;

    .line 21003
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 21005
    :cond_0
    iget-object v1, p0, Lmku;->a:Lovv;

    if-eqz v1, :cond_1

    .line 21006
    const/4 v1, 0x2

    iget-object v2, p0, Lmku;->a:Lovv;

    .line 21007
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 21009
    :cond_1
    iget-object v1, p0, Lmku;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 21010
    iput v0, p0, Lmku;->ai:I

    .line 21011
    return v0
.end method

.method public a(Loxn;)Lmku;
    .locals 2

    .prologue
    .line 21019
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 21020
    sparse-switch v0, :sswitch_data_0

    .line 21024
    iget-object v1, p0, Lmku;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 21025
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmku;->ah:Ljava/util/List;

    .line 21028
    :cond_1
    iget-object v1, p0, Lmku;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 21030
    :sswitch_0
    return-object p0

    .line 21035
    :sswitch_1
    iget-object v0, p0, Lmku;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 21036
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmku;->apiHeader:Llyq;

    .line 21038
    :cond_2
    iget-object v0, p0, Lmku;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 21042
    :sswitch_2
    iget-object v0, p0, Lmku;->a:Lovv;

    if-nez v0, :cond_3

    .line 21043
    new-instance v0, Lovv;

    invoke-direct {v0}, Lovv;-><init>()V

    iput-object v0, p0, Lmku;->a:Lovv;

    .line 21045
    :cond_3
    iget-object v0, p0, Lmku;->a:Lovv;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 21020
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 20988
    iget-object v0, p0, Lmku;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 20989
    const/4 v0, 0x1

    iget-object v1, p0, Lmku;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 20991
    :cond_0
    iget-object v0, p0, Lmku;->a:Lovv;

    if-eqz v0, :cond_1

    .line 20992
    const/4 v0, 0x2

    iget-object v1, p0, Lmku;->a:Lovv;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 20994
    :cond_1
    iget-object v0, p0, Lmku;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 20996
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 20973
    invoke-virtual {p0, p1}, Lmku;->a(Loxn;)Lmku;

    move-result-object v0

    return-object v0
.end method
