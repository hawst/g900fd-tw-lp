.class public final Ldmd;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmgo;",
        "Lmgp;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:J

.field private final p:J

.field private final q:Ljava/lang/String;

.field private final r:Z

.field private final s:Ljava/lang/String;

.field private final t:Ljava/lang/String;

.field private final u:Z

.field private final v:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;JJZLjava/lang/String;ZLjava/lang/String;)V
    .locals 10

    .prologue
    .line 46
    const-string v5, "photosnametagapproval"

    new-instance v6, Lmgo;

    invoke-direct {v6}, Lmgo;-><init>()V

    new-instance v7, Lmgp;

    invoke-direct {v7}, Lmgp;-><init>()V

    move-object v2, p0

    move-object v3, p1

    move v4, p2

    invoke-direct/range {v2 .. v7}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 49
    iput-object p3, p0, Ldmd;->a:Ljava/lang/String;

    .line 50
    move-wide/from16 v0, p6

    iput-wide v0, p0, Ldmd;->b:J

    .line 51
    move-wide/from16 v0, p8

    iput-wide v0, p0, Ldmd;->p:J

    .line 52
    move/from16 v0, p10

    iput-boolean v0, p0, Ldmd;->r:Z

    .line 53
    iput-object p4, p0, Ldmd;->s:Ljava/lang/String;

    .line 54
    iput-object p5, p0, Ldmd;->t:Ljava/lang/String;

    .line 55
    move-object/from16 v0, p11

    iput-object v0, p0, Ldmd;->q:Ljava/lang/String;

    .line 56
    move/from16 v0, p12

    iput-boolean v0, p0, Ldmd;->u:Z

    .line 57
    move-object/from16 v0, p13

    iput-object v0, p0, Ldmd;->v:Ljava/lang/String;

    .line 58
    return-void
.end method


# virtual methods
.method protected a(Lmgo;)V
    .locals 4

    .prologue
    .line 62
    new-instance v0, Lnao;

    invoke-direct {v0}, Lnao;-><init>()V

    iput-object v0, p1, Lmgo;->a:Lnao;

    .line 63
    iget-object v0, p1, Lmgo;->a:Lnao;

    .line 64
    iget-object v1, p0, Ldmd;->a:Ljava/lang/String;

    iput-object v1, v0, Lnao;->a:Ljava/lang/String;

    .line 65
    iget-wide v2, p0, Ldmd;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lnao;->b:Ljava/lang/Long;

    .line 66
    iget-wide v2, p0, Ldmd;->p:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lnao;->d:Ljava/lang/Long;

    .line 67
    iget-boolean v1, p0, Ldmd;->r:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lnao;->c:Ljava/lang/Boolean;

    .line 68
    iget-object v1, p0, Ldmd;->q:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 69
    iget-object v1, p0, Ldmd;->q:Ljava/lang/String;

    iput-object v1, v0, Lnao;->e:Ljava/lang/String;

    .line 71
    :cond_0
    return-void
.end method

.method protected a(Lmgp;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v7, 0x0

    .line 75
    iget-object v0, p1, Lmgp;->a:Lnap;

    .line 76
    iget-object v0, v0, Lnap;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 126
    :cond_0
    :goto_0
    return-void

    .line 79
    :cond_1
    iget-object v0, p0, Ldmd;->f:Landroid/content/Context;

    iget v2, p0, Ldmd;->c:I

    iget-object v3, p0, Ldmd;->s:Ljava/lang/String;

    iget-object v4, p0, Ldmd;->t:Ljava/lang/String;

    invoke-static {v0, v2, v3, v4, v1}, Ljvj;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)[B

    move-result-object v0

    .line 81
    if-eqz v0, :cond_0

    .line 84
    :try_start_0
    new-instance v2, Lnym;

    invoke-direct {v2}, Lnym;-><init>()V

    invoke-static {v2, v0}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lnym;
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    .line 91
    iget-object v3, v0, Lnym;->g:[Lnys;

    .line 92
    if-eqz v3, :cond_0

    array-length v2, v3

    if-eqz v2, :cond_0

    .line 99
    iget-wide v4, p0, Ldmd;->p:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    .line 100
    iget-object v2, v0, Lnym;->g:[Lnys;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    :goto_1
    if-ltz v2, :cond_3

    .line 101
    aget-object v5, v3, v2

    .line 102
    iget-object v6, v5, Lnys;->b:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 103
    iget-boolean v2, p0, Ldmd;->r:Z

    if-eqz v2, :cond_2

    iget v2, v5, Lnys;->d:I

    invoke-static {v2}, Lfve;->b(I)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, v5, Lnys;->h:[Lnyz;

    if-eqz v2, :cond_2

    iget-object v2, v5, Lnys;->h:[Lnyz;

    array-length v2, v2

    if-eqz v2, :cond_2

    .line 105
    iget-object v2, v5, Lnys;->h:[Lnyz;

    aget-object v2, v2, v7

    iput-object v2, v5, Lnys;->c:Lnyz;

    .line 107
    :cond_2
    iget-boolean v2, p0, Ldmd;->r:Z

    if-eqz v2, :cond_4

    :goto_2
    iput v1, v5, Lnys;->d:I

    .line 116
    :cond_3
    new-instance v1, Lnzx;

    invoke-direct {v1}, Lnzx;-><init>()V

    .line 117
    iget-object v2, p0, Ldmd;->s:Ljava/lang/String;

    iput-object v2, v1, Lnzx;->b:Ljava/lang/String;

    .line 118
    new-instance v2, Lnzu;

    invoke-direct {v2}, Lnzu;-><init>()V

    .line 119
    iput-object v0, v2, Lnzu;->b:Lnym;

    .line 120
    sget-object v0, Lnzu;->a:Loxr;

    invoke-virtual {v1, v0, v2}, Lnzx;->a(Loxr;Ljava/lang/Object;)V

    .line 121
    iget-object v0, p0, Ldmd;->f:Landroid/content/Context;

    iget v2, p0, Ldmd;->c:I

    invoke-static {v0, v2, v1, v7}, Ljvj;->a(Landroid/content/Context;ILnzx;I)V

    .line 123
    iget-object v0, p0, Ldmd;->v:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Ldmd;->f:Landroid/content/Context;

    iget v1, p0, Ldmd;->c:I

    iget-object v2, p0, Ldmd;->v:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Ljvd;->a(Landroid/content/Context;ILjava/lang/String;)V

    goto/16 :goto_0

    .line 87
    :catch_0
    move-exception v0

    .line 88
    const-string v1, "HttpOperation"

    const-string v2, "Unable to parse Photo from byte array."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    .line 107
    :cond_4
    iget-boolean v1, p0, Ldmd;->u:Z

    if-eqz v1, :cond_5

    const/4 v1, 0x4

    goto :goto_2

    :cond_5
    const/4 v1, 0x2

    goto :goto_2

    .line 100
    :cond_6
    add-int/lit8 v2, v2, -0x1

    goto :goto_1
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 28
    check-cast p1, Lmgo;

    invoke-virtual {p0, p1}, Ldmd;->a(Lmgo;)V

    return-void
.end method

.method protected synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 28
    check-cast p1, Lmgp;

    invoke-virtual {p0, p1}, Ldmd;->a(Lmgp;)V

    return-void
.end method
