.class public final Llbb;
.super Lhye;
.source "PG"


# instance fields
.field private final b:I

.field private final c:Ljava/lang/String;

.field private final d:Z

.field private final e:Ljava/lang/String;

.field private final f:Lkzl;

.field private final g:Ldp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">.dp;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;I[Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lhye;-><init>(Landroid/content/Context;)V

    .line 21
    new-instance v0, Ldp;

    invoke-direct {v0, p0}, Ldp;-><init>(Ldo;)V

    iput-object v0, p0, Llbb;->g:Ldp;

    .line 26
    invoke-virtual {p0, p3}, Llbb;->a([Ljava/lang/String;)V

    .line 28
    iput p2, p0, Llbb;->b:I

    .line 29
    iput-object p4, p0, Llbb;->c:Ljava/lang/String;

    .line 30
    iput-boolean p5, p0, Llbb;->d:Z

    .line 31
    iput-object p6, p0, Llbb;->e:Ljava/lang/String;

    .line 32
    const-class v0, Lkzl;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkzl;

    iput-object v0, p0, Llbb;->f:Lkzl;

    .line 33
    return-void
.end method


# virtual methods
.method public C()Landroid/database/Cursor;
    .locals 7

    .prologue
    .line 37
    iget-object v0, p0, Llbb;->f:Lkzl;

    iget v1, p0, Llbb;->b:I

    invoke-virtual {p0}, Llbb;->j()[Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Llbb;->c:Ljava/lang/String;

    iget-boolean v4, p0, Llbb;->d:Z

    const/4 v5, 0x0

    iget-object v6, p0, Llbb;->e:Ljava/lang/String;

    invoke-interface/range {v0 .. v6}, Lkzl;->a(I[Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 40
    if-eqz v0, :cond_0

    .line 41
    iget-object v1, p0, Llbb;->g:Ldp;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    .line 44
    :cond_0
    return-object v0
.end method
