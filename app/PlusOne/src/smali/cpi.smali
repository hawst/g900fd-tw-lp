.class public Lcpi;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcpi;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumMap",
            "<",
            "Lcph;",
            "Lgl",
            "<",
            "Ljava/lang/Long;",
            "Lcpg;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcpi;->a:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Ljava/util/EnumMap;

    const-class v1, Lcph;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcpi;->b:Ljava/util/EnumMap;

    .line 41
    iget-object v0, p0, Lcpi;->b:Ljava/util/EnumMap;

    sget-object v1, Lcph;->a:Lcph;

    new-instance v2, Lgl;

    const/16 v3, 0x1f4

    invoke-direct {v2, v3}, Lgl;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    iget-object v0, p0, Lcpi;->b:Ljava/util/EnumMap;

    sget-object v1, Lcph;->b:Lcph;

    new-instance v2, Lgl;

    const/16 v3, 0xa

    invoke-direct {v2, v3}, Lgl;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    return-void
.end method

.method public static declared-synchronized a(I)Lcpi;
    .locals 4

    .prologue
    .line 29
    const-class v1, Lcpi;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcpi;->a:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcpi;

    .line 30
    if-nez v0, :cond_0

    .line 31
    new-instance v0, Lcpi;

    invoke-direct {v0}, Lcpi;-><init>()V

    .line 32
    sget-object v2, Lcpi;->a:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 34
    :cond_0
    monitor-exit v1

    return-object v0

    .line 29
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public a(JLcph;)Lcpg;
    .locals 3

    .prologue
    .line 53
    iget-object v0, p0, Lcpi;->b:Ljava/util/EnumMap;

    invoke-virtual {v0, p3}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgl;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgl;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcpg;

    return-object v0
.end method

.method public a(Landroid/content/Context;IJLcph;)Lcpg;
    .locals 1

    .prologue
    .line 119
    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {p1, p2, v0, p5}, Lcpe;->a(Landroid/content/Context;ILjava/lang/Long;Lcph;)Lcpg;

    move-result-object v0

    .line 120
    invoke-virtual {p0, v0}, Lcpi;->a(Lcpg;)V

    .line 121
    return-object v0
.end method

.method public a(Landroid/content/Context;ILjava/util/List;Lcph;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lcph;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcpg;",
            ">;"
        }
    .end annotation

    .prologue
    .line 101
    invoke-static {p1, p2, p3, p4}, Lcpe;->a(Landroid/content/Context;ILjava/util/List;Lcph;)Ljava/util/List;

    move-result-object v1

    .line 104
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcpg;

    .line 105
    invoke-virtual {p0, v0}, Lcpi;->a(Lcpg;)V

    goto :goto_0

    .line 108
    :cond_0
    return-object v1
.end method

.method public a(Lcpg;)V
    .locals 11

    .prologue
    .line 61
    if-nez p1, :cond_0

    .line 74
    :goto_0
    return-void

    .line 66
    :cond_0
    iget-object v0, p1, Lcpg;->a:Lcph;

    sget-object v1, Lcph;->b:Lcph;

    if-ne v0, v1, :cond_1

    .line 67
    new-instance v0, Lcpg;

    sget-object v1, Lcph;->a:Lcph;

    iget-wide v2, p1, Lcpg;->b:J

    iget-wide v4, p1, Lcpg;->c:J

    iget-wide v6, p1, Lcpg;->d:J

    iget-object v8, p1, Lcpg;->e:Ljava/lang/String;

    iget-object v9, p1, Lcpg;->f:Lizu;

    const/4 v10, 0x0

    invoke-direct/range {v0 .. v10}, Lcpg;-><init>(Lcph;JJJLjava/lang/String;Lizu;Lnym;)V

    .line 70
    iget-object v1, p0, Lcpi;->b:Ljava/util/EnumMap;

    sget-object v2, Lcph;->a:Lcph;

    invoke-virtual {v1, v2}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lgl;

    iget-wide v2, v0, Lcpg;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lgl;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    :cond_1
    iget-object v0, p0, Lcpi;->b:Ljava/util/EnumMap;

    iget-object v1, p1, Lcpg;->a:Lcph;

    invoke-virtual {v0, v1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgl;

    iget-wide v2, p1, Lcpg;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lgl;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
