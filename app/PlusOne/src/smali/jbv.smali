.class public final Ljbv;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lizr;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Ljbv;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Lizu;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 85
    new-instance v0, Ljbw;

    invoke-direct {v0}, Ljbw;-><init>()V

    sput-object v0, Ljbv;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;Ljac;)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    invoke-static {p1, p2, p3}, Lizu;->a(Landroid/content/Context;Landroid/net/Uri;Ljac;)Lizu;

    move-result-object v0

    iput-object v0, p0, Ljbv;->a:Lizu;

    .line 38
    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    const-class v0, Lizu;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lizu;

    iput-object v0, p0, Ljbv;->a:Lizu;

    .line 66
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;Ljac;)Ljac;
    .locals 2

    .prologue
    .line 21
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 22
    if-nez v0, :cond_1

    .line 29
    :cond_0
    :goto_0
    return-object p2

    .line 24
    :cond_1
    const-string v1, "video"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 25
    sget-object p2, Ljac;->b:Ljac;

    goto :goto_0

    .line 26
    :cond_2
    const-string v1, "image"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 27
    sget-object p2, Ljac;->a:Ljac;

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;)Ljbv;
    .locals 2

    .prologue
    .line 33
    new-instance v0, Ljbv;

    sget-object v1, Ljac;->a:Ljac;

    invoke-static {p0, p1, v1}, Ljbv;->a(Landroid/content/Context;Landroid/net/Uri;Ljac;)Ljac;

    move-result-object v1

    invoke-direct {v0, p0, p1, v1}, Ljbv;-><init>(Landroid/content/Context;Landroid/net/Uri;Ljac;)V

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 70
    instance-of v1, p1, Ljbv;

    if-nez v1, :cond_1

    .line 79
    :cond_0
    :goto_0
    return v0

    .line 73
    :cond_1
    check-cast p1, Lizr;

    invoke-interface {p1}, Lizr;->f()Lizu;

    move-result-object v1

    .line 74
    if-nez v1, :cond_2

    iget-object v2, p0, Ljbv;->a:Lizu;

    if-nez v2, :cond_2

    .line 75
    const/4 v0, 0x1

    goto :goto_0

    .line 76
    :cond_2
    if-eqz v1, :cond_0

    iget-object v2, p0, Ljbv;->a:Lizu;

    if-eqz v2, :cond_0

    .line 79
    iget-object v0, p0, Ljbv;->a:Lizu;

    invoke-virtual {v1, v0}, Lizu;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public f()Lizu;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Ljbv;->a:Lizu;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, Ljbv;->a:Lizu;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 62
    return-void
.end method
