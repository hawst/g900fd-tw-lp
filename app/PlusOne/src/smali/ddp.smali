.class public final Lddp;
.super Ldds;
.source "PG"


# instance fields
.field a:J

.field b:I

.field c:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ldds;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lddl;
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 87
    invoke-virtual {p0, v0}, Lddp;->e(Z)Ldds;

    .line 88
    invoke-virtual {p0, v0}, Lddp;->c(Z)Ldds;

    .line 90
    new-instance v0, Lddm;

    invoke-direct {v0, p0}, Lddm;-><init>(Ldds;)V

    return-object v0
.end method

.method public a(I)Lddp;
    .locals 0

    .prologue
    .line 69
    iput p1, p0, Lddp;->b:I

    .line 70
    return-object p0
.end method

.method public a(J)Lddp;
    .locals 1

    .prologue
    .line 64
    iput-wide p1, p0, Lddp;->a:J

    .line 65
    return-object p0
.end method

.method public a(Landroid/content/Context;Landroid/os/Bundle;)Ldds;
    .locals 2

    .prologue
    .line 80
    invoke-super {p0, p1, p2}, Ldds;->a(Landroid/content/Context;Landroid/os/Bundle;)Ldds;

    .line 81
    const-string v0, "all_photos_row_id"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lddp;->a(J)Lddp;

    .line 82
    return-object p0
.end method

.method public a(Lddl;)Ldds;
    .locals 2

    .prologue
    .line 53
    invoke-super {p0, p1}, Ldds;->a(Lddl;)Ldds;

    .line 54
    instance-of v0, p1, Lddm;

    if-eqz v0, :cond_0

    .line 55
    check-cast p1, Lddm;

    .line 56
    invoke-static {p1}, Lddm;->a(Lddm;)J

    move-result-wide v0

    iput-wide v0, p0, Lddp;->a:J

    .line 57
    invoke-static {p1}, Lddm;->b(Lddm;)I

    move-result v0

    iput v0, p0, Lddp;->b:I

    .line 58
    invoke-static {p1}, Lddm;->c(Lddm;)I

    move-result v0

    iput v0, p0, Lddp;->c:I

    .line 60
    :cond_0
    return-object p0
.end method

.method public b(I)Lddp;
    .locals 0

    .prologue
    .line 74
    iput p1, p0, Lddp;->c:I

    .line 75
    return-object p0
.end method
