.class public final Lmfw;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmyj;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2995
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2998
    iput-object v0, p0, Lmfw;->apiHeader:Llyq;

    .line 3001
    iput-object v0, p0, Lmfw;->a:Lmyj;

    .line 2995
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 3018
    const/4 v0, 0x0

    .line 3019
    iget-object v1, p0, Lmfw;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 3020
    const/4 v0, 0x1

    iget-object v1, p0, Lmfw;->apiHeader:Llyq;

    .line 3021
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3023
    :cond_0
    iget-object v1, p0, Lmfw;->a:Lmyj;

    if-eqz v1, :cond_1

    .line 3024
    const/4 v1, 0x2

    iget-object v2, p0, Lmfw;->a:Lmyj;

    .line 3025
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3027
    :cond_1
    iget-object v1, p0, Lmfw;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3028
    iput v0, p0, Lmfw;->ai:I

    .line 3029
    return v0
.end method

.method public a(Loxn;)Lmfw;
    .locals 2

    .prologue
    .line 3037
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3038
    sparse-switch v0, :sswitch_data_0

    .line 3042
    iget-object v1, p0, Lmfw;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 3043
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmfw;->ah:Ljava/util/List;

    .line 3046
    :cond_1
    iget-object v1, p0, Lmfw;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3048
    :sswitch_0
    return-object p0

    .line 3053
    :sswitch_1
    iget-object v0, p0, Lmfw;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 3054
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmfw;->apiHeader:Llyq;

    .line 3056
    :cond_2
    iget-object v0, p0, Lmfw;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3060
    :sswitch_2
    iget-object v0, p0, Lmfw;->a:Lmyj;

    if-nez v0, :cond_3

    .line 3061
    new-instance v0, Lmyj;

    invoke-direct {v0}, Lmyj;-><init>()V

    iput-object v0, p0, Lmfw;->a:Lmyj;

    .line 3063
    :cond_3
    iget-object v0, p0, Lmfw;->a:Lmyj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3038
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 3006
    iget-object v0, p0, Lmfw;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 3007
    const/4 v0, 0x1

    iget-object v1, p0, Lmfw;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 3009
    :cond_0
    iget-object v0, p0, Lmfw;->a:Lmyj;

    if-eqz v0, :cond_1

    .line 3010
    const/4 v0, 0x2

    iget-object v1, p0, Lmfw;->a:Lmyj;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 3012
    :cond_1
    iget-object v0, p0, Lmfw;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3014
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2991
    invoke-virtual {p0, p1}, Lmfw;->a(Loxn;)Lmfw;

    move-result-object v0

    return-object v0
.end method
