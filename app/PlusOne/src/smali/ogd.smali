.class public final Logd;
.super Loxq;
.source "PG"


# instance fields
.field public a:I

.field private b:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9034
    invoke-direct {p0}, Loxq;-><init>()V

    .line 9043
    const/high16 v0, -0x80000000

    iput v0, p0, Logd;->a:I

    .line 9034
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 9062
    const/4 v0, 0x0

    .line 9063
    iget v1, p0, Logd;->a:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 9064
    const/4 v0, 0x1

    iget v1, p0, Logd;->a:I

    .line 9065
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 9067
    :cond_0
    iget-object v1, p0, Logd;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 9068
    const/4 v1, 0x2

    iget-object v2, p0, Logd;->b:Ljava/lang/Integer;

    .line 9069
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 9071
    :cond_1
    iget-object v1, p0, Logd;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9072
    iput v0, p0, Logd;->ai:I

    .line 9073
    return v0
.end method

.method public a(Loxn;)Logd;
    .locals 2

    .prologue
    .line 9081
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 9082
    sparse-switch v0, :sswitch_data_0

    .line 9086
    iget-object v1, p0, Logd;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 9087
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Logd;->ah:Ljava/util/List;

    .line 9090
    :cond_1
    iget-object v1, p0, Logd;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 9092
    :sswitch_0
    return-object p0

    .line 9097
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 9098
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 9101
    :cond_2
    iput v0, p0, Logd;->a:I

    goto :goto_0

    .line 9103
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Logd;->a:I

    goto :goto_0

    .line 9108
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Logd;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 9082
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 9050
    iget v0, p0, Logd;->a:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 9051
    const/4 v0, 0x1

    iget v1, p0, Logd;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 9053
    :cond_0
    iget-object v0, p0, Logd;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 9054
    const/4 v0, 0x2

    iget-object v1, p0, Logd;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 9056
    :cond_1
    iget-object v0, p0, Logd;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 9058
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 9030
    invoke-virtual {p0, p1}, Logd;->a(Loxn;)Logd;

    move-result-object v0

    return-object v0
.end method
