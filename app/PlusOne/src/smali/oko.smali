.class public final Loko;
.super Loxq;
.source "PG"


# instance fields
.field private a:[Loki;

.field private b:[Ljava/lang/Long;

.field private c:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2346
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2349
    sget-object v0, Loki;->a:[Loki;

    iput-object v0, p0, Loko;->a:[Loki;

    .line 2352
    sget-object v0, Loxx;->h:[Ljava/lang/Long;

    iput-object v0, p0, Loko;->b:[Ljava/lang/Long;

    .line 2355
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Loko;->c:[Ljava/lang/String;

    .line 2346
    return-void
.end method


# virtual methods
.method public a()I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 2383
    .line 2384
    iget-object v0, p0, Loko;->a:[Loki;

    if-eqz v0, :cond_1

    .line 2385
    iget-object v3, p0, Loko;->a:[Loki;

    array-length v4, v3

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 2386
    if-eqz v5, :cond_0

    .line 2387
    const/4 v6, 0x1

    .line 2388
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 2385
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 2392
    :cond_2
    iget-object v2, p0, Loko;->b:[Ljava/lang/Long;

    if-eqz v2, :cond_4

    iget-object v2, p0, Loko;->b:[Ljava/lang/Long;

    array-length v2, v2

    if-lez v2, :cond_4

    .line 2394
    iget-object v4, p0, Loko;->b:[Ljava/lang/Long;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_1
    if-ge v2, v5, :cond_3

    aget-object v6, v4, v2

    .line 2396
    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Loxo;->f(J)I

    move-result v6

    add-int/2addr v3, v6

    .line 2394
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2398
    :cond_3
    add-int/2addr v0, v3

    .line 2399
    iget-object v2, p0, Loko;->b:[Ljava/lang/Long;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 2401
    :cond_4
    iget-object v2, p0, Loko;->c:[Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v2, p0, Loko;->c:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_6

    .line 2403
    iget-object v3, p0, Loko;->c:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v1, v4, :cond_5

    aget-object v5, v3, v1

    .line 2405
    invoke-static {v5}, Loxo;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 2403
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2407
    :cond_5
    add-int/2addr v0, v2

    .line 2408
    iget-object v1, p0, Loko;->c:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2410
    :cond_6
    iget-object v1, p0, Loko;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2411
    iput v0, p0, Loko;->ai:I

    .line 2412
    return v0
.end method

.method public a(Loxn;)Loko;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2420
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2421
    sparse-switch v0, :sswitch_data_0

    .line 2425
    iget-object v2, p0, Loko;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 2426
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Loko;->ah:Ljava/util/List;

    .line 2429
    :cond_1
    iget-object v2, p0, Loko;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2431
    :sswitch_0
    return-object p0

    .line 2436
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 2437
    iget-object v0, p0, Loko;->a:[Loki;

    if-nez v0, :cond_3

    move v0, v1

    .line 2438
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Loki;

    .line 2439
    iget-object v3, p0, Loko;->a:[Loki;

    if-eqz v3, :cond_2

    .line 2440
    iget-object v3, p0, Loko;->a:[Loki;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2442
    :cond_2
    iput-object v2, p0, Loko;->a:[Loki;

    .line 2443
    :goto_2
    iget-object v2, p0, Loko;->a:[Loki;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 2444
    iget-object v2, p0, Loko;->a:[Loki;

    new-instance v3, Loki;

    invoke-direct {v3}, Loki;-><init>()V

    aput-object v3, v2, v0

    .line 2445
    iget-object v2, p0, Loko;->a:[Loki;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 2446
    invoke-virtual {p1}, Loxn;->a()I

    .line 2443
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2437
    :cond_3
    iget-object v0, p0, Loko;->a:[Loki;

    array-length v0, v0

    goto :goto_1

    .line 2449
    :cond_4
    iget-object v2, p0, Loko;->a:[Loki;

    new-instance v3, Loki;

    invoke-direct {v3}, Loki;-><init>()V

    aput-object v3, v2, v0

    .line 2450
    iget-object v2, p0, Loko;->a:[Loki;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2454
    :sswitch_2
    const/16 v0, 0x10

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 2455
    iget-object v0, p0, Loko;->b:[Ljava/lang/Long;

    array-length v0, v0

    .line 2456
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/Long;

    .line 2457
    iget-object v3, p0, Loko;->b:[Ljava/lang/Long;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2458
    iput-object v2, p0, Loko;->b:[Ljava/lang/Long;

    .line 2459
    :goto_3
    iget-object v2, p0, Loko;->b:[Ljava/lang/Long;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 2460
    iget-object v2, p0, Loko;->b:[Ljava/lang/Long;

    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v0

    .line 2461
    invoke-virtual {p1}, Loxn;->a()I

    .line 2459
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 2464
    :cond_5
    iget-object v2, p0, Loko;->b:[Ljava/lang/Long;

    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    .line 2468
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 2469
    iget-object v0, p0, Loko;->c:[Ljava/lang/String;

    array-length v0, v0

    .line 2470
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 2471
    iget-object v3, p0, Loko;->c:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2472
    iput-object v2, p0, Loko;->c:[Ljava/lang/String;

    .line 2473
    :goto_4
    iget-object v2, p0, Loko;->c:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 2474
    iget-object v2, p0, Loko;->c:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 2475
    invoke-virtual {p1}, Loxn;->a()I

    .line 2473
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 2478
    :cond_6
    iget-object v2, p0, Loko;->c:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    .line 2421
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 2360
    iget-object v1, p0, Loko;->a:[Loki;

    if-eqz v1, :cond_1

    .line 2361
    iget-object v2, p0, Loko;->a:[Loki;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 2362
    if-eqz v4, :cond_0

    .line 2363
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 2361
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2367
    :cond_1
    iget-object v1, p0, Loko;->b:[Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 2368
    iget-object v2, p0, Loko;->b:[Ljava/lang/Long;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 2369
    const/4 v5, 0x2

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {p1, v5, v6, v7}, Loxo;->b(IJ)V

    .line 2368
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2372
    :cond_2
    iget-object v1, p0, Loko;->c:[Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 2373
    iget-object v1, p0, Loko;->c:[Ljava/lang/String;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 2374
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->a(ILjava/lang/String;)V

    .line 2373
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2377
    :cond_3
    iget-object v0, p0, Loko;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2379
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2342
    invoke-virtual {p0, p1}, Loko;->a(Loxn;)Loko;

    move-result-object v0

    return-object v0
.end method
