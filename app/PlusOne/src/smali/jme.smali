.class public Ljme;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lheo;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;I)V
    .locals 3

    .prologue
    .line 26
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 27
    invoke-interface {v0, p2}, Lhei;->b(I)Lhek;

    move-result-object v0

    const-string v1, "ONBOARDING_COMPLETED"

    const/4 v2, 0x1

    .line 28
    invoke-interface {v0, v1, v2}, Lhek;->b(Ljava/lang/String;Z)Lhek;

    move-result-object v0

    .line 29
    invoke-interface {v0}, Lhek;->c()I

    .line 30
    return-void
.end method

.method public a(Landroid/content/Context;Lhem;)V
    .locals 0

    .prologue
    .line 41
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lheq;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 45
    new-instance v0, Ljmf;

    invoke-direct {v0}, Ljmf;-><init>()V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 46
    return-void
.end method

.method public b(Landroid/content/Context;I)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 33
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 34
    invoke-interface {v0, p2}, Lhei;->d(I)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 37
    :goto_0
    return v0

    :cond_0
    invoke-interface {v0, p2}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v2, "ONBOARDING_COMPLETED"

    invoke-interface {v0, v2, v1}, Lhej;->a(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method
