.class public final Leju;
.super Leku;
.source "PG"


# instance fields
.field private N:Lejw;

.field private O:I

.field private ad:Ljava/lang/String;

.field private ae:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final af:Lbc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbc",
            "<",
            "Lojw;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 37
    invoke-direct {p0}, Leku;-><init>()V

    .line 45
    new-instance v0, Lejv;

    invoke-direct {v0, p0}, Lejv;-><init>(Leju;)V

    iput-object v0, p0, Leju;->af:Lbc;

    .line 69
    new-instance v0, Lhnw;

    new-instance v1, Lejx;

    invoke-direct {v1, p0}, Lejx;-><init>(Leju;)V

    invoke-direct {v0, p0, v1}, Lhnw;-><init>(Llrn;Lhnv;)V

    .line 80
    return-void
.end method

.method static synthetic a(Leju;)I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Leju;->O:I

    return v0
.end method

.method static synthetic a(Leju;I)I
    .locals 0

    .prologue
    .line 37
    iput p1, p0, Leju;->O:I

    return p1
.end method

.method static synthetic a(Leju;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Leju;->ad:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Leju;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Leju;->ae:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic b(Leju;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Leju;->ae:Ljava/util/ArrayList;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 210
    const/16 v0, 0x2c

    return v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 173
    invoke-super {p0, p1, p2, p3}, Leku;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 174
    iget-object v1, p0, Leju;->N:Lejw;

    invoke-virtual {p0, v1}, Leju;->a(Landroid/widget/ListAdapter;)V

    .line 175
    return-object v0
.end method

.method protected a(II)Ldid;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 216
    .line 217
    invoke-virtual {p0, p1}, Leju;->e(I)I

    move-result v0

    .line 216
    invoke-static {v0, v1, v1}, Ldib;->a(ILjava/lang/Integer;Ljava/lang/Integer;)Ldid;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 166
    invoke-super {p0, p1}, Leku;->a(Landroid/os/Bundle;)V

    .line 167
    new-instance v0, Lejw;

    invoke-direct {v0, p0}, Lejw;-><init>(Leju;)V

    iput-object v0, p0, Leju;->N:Lejw;

    .line 168
    return-void
.end method

.method public a(Lhjk;)V
    .locals 1

    .prologue
    .line 192
    invoke-super {p0, p1}, Leku;->a(Lhjk;)V

    .line 193
    iget-object v0, p0, Leju;->ad:Ljava/lang/String;

    invoke-interface {p1, v0}, Lhjk;->a(Ljava/lang/CharSequence;)V

    .line 194
    return-void
.end method

.method public a([Lois;)V
    .locals 2

    .prologue
    .line 184
    iget-object v0, p0, Leju;->N:Lejw;

    iget v1, p0, Leju;->O:I

    invoke-virtual {v0, p1, v1}, Lejw;->a([Lois;I)V

    .line 185
    iget-object v0, p0, Leju;->N:Lejw;

    invoke-virtual {v0}, Lejw;->notifyDataSetChanged()V

    .line 186
    iget-object v0, p0, Leju;->Q:Lhje;

    invoke-virtual {v0}, Lhje;->a()V

    .line 187
    invoke-virtual {p0}, Leju;->x()Landroid/view/View;

    invoke-virtual {p0}, Leju;->aa()V

    .line 188
    return-void
.end method

.method public aR_()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 227
    const/4 v0, 0x4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public b()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 222
    const/16 v0, 0x69

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected d()V
    .locals 4

    .prologue
    .line 198
    const/4 v0, 0x1

    iput-boolean v0, p0, Leju;->R:Z

    .line 199
    invoke-virtual {p0}, Leju;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    iget-object v3, p0, Leju;->af:Lbc;

    invoke-virtual {v0, v1, v2, v3}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 200
    return-void
.end method

.method protected e()V
    .locals 4

    .prologue
    .line 204
    const/4 v0, 0x1

    iput-boolean v0, p0, Leju;->R:Z

    .line 205
    invoke-virtual {p0}, Leju;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    iget-object v3, p0, Leju;->af:Lbc;

    invoke-virtual {v0, v1, v2, v3}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 206
    return-void
.end method

.method public z()V
    .locals 0

    .prologue
    .line 180
    invoke-super {p0}, Leku;->z()V

    .line 181
    return-void
.end method
