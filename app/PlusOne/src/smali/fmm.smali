.class public final Lfmm;
.super Llol;
.source "PG"

# interfaces
.implements Leqi;
.implements Lhob;
.implements Lkgr;


# instance fields
.field private N:Lfmh;

.field private O:Z

.field private P:I

.field private Q:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

.field private R:Lkhl;

.field private final S:Lhkd;

.field private final T:Lhke;

.field private U:Lkgp;

.field private final V:Lhoc;

.field private W:Lkhr;

.field private X:Lbc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbc",
            "<",
            "Lfmh;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 114
    invoke-direct {p0}, Llol;-><init>()V

    .line 65
    new-instance v0, Lfmn;

    invoke-direct {v0, p0}, Lfmn;-><init>(Lfmm;)V

    iput-object v0, p0, Lfmm;->S:Lhkd;

    .line 77
    new-instance v0, Lhke;

    iget-object v1, p0, Lfmm;->av:Llqm;

    invoke-direct {v0, v1}, Lhke;-><init>(Llqr;)V

    iget-object v1, p0, Lfmm;->au:Llnh;

    .line 79
    invoke-virtual {v0, v1}, Lhke;->a(Llnh;)Lhke;

    move-result-object v0

    const v1, 0x7f1000c6

    iget-object v2, p0, Lfmm;->S:Lhkd;

    .line 80
    invoke-virtual {v0, v1, v2}, Lhke;->a(ILhkd;)Lhke;

    move-result-object v0

    iput-object v0, p0, Lfmm;->T:Lhke;

    .line 83
    new-instance v0, Lkgp;

    iget-object v1, p0, Lfmm;->av:Llqm;

    invoke-direct {v0, p0, v1}, Lkgp;-><init>(Lkgr;Llqr;)V

    iput-object v0, p0, Lfmm;->U:Lkgp;

    .line 85
    new-instance v0, Lhoc;

    iget-object v1, p0, Lfmm;->av:Llqm;

    invoke-direct {v0, p0, v1}, Lhoc;-><init>(Lu;Llqr;)V

    .line 86
    invoke-virtual {v0, p0}, Lhoc;->a(Lhob;)Lhoc;

    move-result-object v0

    iput-object v0, p0, Lfmm;->V:Lhoc;

    .line 89
    new-instance v0, Lfmo;

    iget-object v1, p0, Lfmm;->U:Lkgp;

    iget-object v2, p0, Lfmm;->av:Llqm;

    invoke-direct {v0, p0, p0, v1, v2}, Lfmo;-><init>(Lfmm;Lkgr;Lkgp;Llqr;)V

    iput-object v0, p0, Lfmm;->X:Lbc;

    .line 114
    return-void
.end method

.method private U()V
    .locals 4

    .prologue
    .line 224
    iget-object v0, p0, Lfmm;->N:Lfmh;

    iget-object v0, v0, Lfmh;->b:Lhgw;

    if-eqz v0, :cond_0

    .line 226
    iget-object v0, p0, Lfmm;->N:Lfmh;

    iget-object v0, v0, Lfmh;->b:Lhgw;

    invoke-virtual {v0}, Lhgw;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 228
    new-instance v0, Lock;

    invoke-direct {v0}, Lock;-><init>()V

    .line 232
    :goto_0
    new-instance v1, Lfmv;

    iget-object v2, p0, Lfmm;->at:Llnl;

    iget v3, p0, Lfmm;->P:I

    .line 233
    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    invoke-direct {v1, v2, v3, v0}, Lfmv;-><init>(Landroid/content/Context;I[B)V

    .line 234
    iget-object v0, p0, Lfmm;->V:Lhoc;

    invoke-virtual {v0, v1}, Lhoc;->c(Lhny;)V

    .line 236
    :cond_0
    return-void

    .line 230
    :cond_1
    iget-object v0, p0, Lfmm;->N:Lfmh;

    iget-object v0, v0, Lfmh;->b:Lhgw;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lhft;->a(Lhgw;Lhgw;)Lock;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lfmm;)Lfmh;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lfmm;->N:Lfmh;

    return-object v0
.end method

.method static synthetic a(Lfmm;Lfmh;)Lfmh;
    .locals 0

    .prologue
    .line 47
    iput-object p1, p0, Lfmm;->N:Lfmh;

    return-object p1
.end method

.method static synthetic b(Lfmm;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Lfmm;->U()V

    return-void
.end method

.method static synthetic c(Lfmm;)Llnl;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lfmm;->at:Llnl;

    return-object v0
.end method

.method static synthetic d(Lfmm;)I
    .locals 1

    .prologue
    .line 47
    iget v0, p0, Lfmm;->P:I

    return v0
.end method

.method static synthetic e(Lfmm;)Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lfmm;->O:Z

    return v0
.end method

.method static synthetic f(Lfmm;)V
    .locals 5

    .prologue
    .line 47
    iget-object v0, p0, Lfmm;->Q:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfmm;->Q:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    iget-object v1, p0, Lfmm;->N:Lfmh;

    iget-boolean v1, v1, Lfmh;->a:Z

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->a(Z)V

    :cond_0
    iget-object v0, p0, Lfmm;->R:Lkhl;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfmm;->R:Lkhl;

    iget-object v1, p0, Lfmm;->N:Lfmh;

    iget-object v1, v1, Lfmh;->b:Lhgw;

    invoke-static {v1}, Lfuk;->a(Lhgw;)I

    move-result v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lfmm;->N:Lfmh;

    iget-object v4, v4, Lfmh;->c:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lfmm;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkhl;->d(Ljava/lang/CharSequence;)V

    :cond_1
    return-void
.end method

.method static synthetic g(Lfmm;)Lcom/google/android/libraries/social/settings/CheckBoxPreference;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lfmm;->Q:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    return-object v0
.end method

.method static synthetic h(Lfmm;)Llnl;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lfmm;->at:Llnl;

    return-object v0
.end method

.method static synthetic i(Lfmm;)Lhoc;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lfmm;->V:Lhoc;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 151
    invoke-virtual {p0}, Lfmm;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v3, p0, Lfmm;->X:Lbc;

    invoke-virtual {v0, v1, v2, v3}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 152
    return-void
.end method

.method public a(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 118
    invoke-super {p0, p1}, Llol;->a(Landroid/app/Activity;)V

    .line 119
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account_id"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lfmm;->P:I

    .line 120
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 124
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 125
    if-eqz p1, :cond_0

    .line 126
    const-string v0, "sharing_settings"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 127
    const-string v0, "sharing_settings"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lfmh;

    iput-object v0, p0, Lfmm;->N:Lfmh;

    .line 131
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 241
    const-string v2, "SetAutoTaggingSettingsTask"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 242
    invoke-virtual {p2}, Lhoz;->f()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 244
    iget-object v2, p0, Lfmm;->N:Lfmh;

    iget-object v3, p0, Lfmm;->N:Lfmh;

    iget-boolean v3, v3, Lfmh;->a:Z

    if-nez v3, :cond_1

    :goto_0
    iput-boolean v0, v2, Lfmh;->a:Z

    .line 245
    iget-object v0, p0, Lfmm;->Q:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    if-eqz v0, :cond_0

    .line 246
    iget-object v0, p0, Lfmm;->Q:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    iget-object v1, p0, Lfmm;->N:Lfmh;

    iget-boolean v1, v1, Lfmh;->a:Z

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->a(Z)V

    .line 255
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 244
    goto :goto_0

    .line 249
    :cond_2
    const-string v2, "UpdateSourceBackgroundTask"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 250
    invoke-virtual {p2}, Lhoz;->f()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lfmm;->R:Lkhl;

    if-eqz v2, :cond_0

    .line 251
    iget-object v2, p0, Lfmm;->R:Lkhl;

    iget-object v3, p0, Lfmm;->N:Lfmh;

    iget-object v3, v3, Lfmh;->b:Lhgw;

    invoke-static {v3}, Lfuk;->a(Lhgw;)I

    move-result v3

    new-array v0, v0, [Ljava/lang/Object;

    iget-object v4, p0, Lfmm;->N:Lfmh;

    iget-object v4, v4, Lfmh;->c:Ljava/lang/String;

    aput-object v4, v0, v1

    invoke-virtual {p0, v3, v0}, Lfmm;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lkhl;->d(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public aj()V
    .locals 4

    .prologue
    .line 269
    invoke-virtual {p0}, Lfmm;->n()Lz;

    move-result-object v0

    iget v1, p0, Lfmm;->P:I

    const v2, 0x7f0a0377

    iget-object v3, p0, Lfmm;->N:Lfmh;

    iget-object v3, v3, Lfmh;->b:Lhgw;

    invoke-static {v0, v1, v2, v3}, Lfuk;->a(Landroid/app/Activity;IILhgw;)Landroid/content/Intent;

    move-result-object v0

    .line 271
    iget-object v1, p0, Lfmm;->T:Lhke;

    const v2, 0x7f1000c6

    invoke-virtual {v1, v2, v0}, Lhke;->a(ILandroid/content/Intent;)V

    .line 272
    return-void
.end method

.method public b(Ljava/lang/String;ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 259
    new-instance v0, Lhgw;

    new-instance v1, Lhxc;

    const/4 v2, 0x1

    invoke-direct {v1, p1, p2, p3, v2}, Lhxc;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    invoke-direct {v0, v1}, Lhgw;-><init>(Lhxc;)V

    .line 261
    iget-object v1, p0, Lfmm;->at:Llnl;

    iget-object v2, p0, Lfmm;->N:Lfmh;

    iget-object v2, v2, Lfmh;->b:Lhgw;

    invoke-static {v1, v0, v2}, Lfuk;->a(Landroid/content/Context;Lhgw;Lhgw;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 262
    iget-object v1, p0, Lfmm;->N:Lfmh;

    iput-object v0, v1, Lfmh;->b:Lhgw;

    .line 263
    invoke-direct {p0}, Lfmm;->U()V

    .line 265
    :cond_0
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 143
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 144
    iget-object v0, p0, Lfmm;->au:Llnh;

    const-class v1, Lieh;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    .line 145
    sget-object v1, Ldxd;->e:Lief;

    iget v2, p0, Lfmm;->P:I

    invoke-interface {v0, v1, v2}, Lieh;->b(Lief;I)Z

    move-result v0

    iput-boolean v0, p0, Lfmm;->O:Z

    .line 147
    return-void
.end method

.method public d()V
    .locals 4

    .prologue
    .line 156
    invoke-virtual {p0}, Lfmm;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v3, p0, Lfmm;->X:Lbc;

    invoke-virtual {v0, v1, v2, v3}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 157
    return-void
.end method

.method public e()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 161
    new-instance v0, Lkhr;

    iget-object v1, p0, Lfmm;->at:Llnl;

    invoke-direct {v0, v1}, Lkhr;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lfmm;->W:Lkhr;

    .line 163
    iget-object v0, p0, Lfmm;->W:Lkhr;

    const v1, 0x7f0a088f

    .line 164
    invoke-virtual {p0, v1}, Lfmm;->e_(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0a0890

    .line 165
    invoke-virtual {p0, v2}, Lfmm;->e_(I)Ljava/lang/String;

    move-result-object v2

    .line 163
    invoke-virtual {v0, v1, v2}, Lkhr;->d(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    move-result-object v0

    iput-object v0, p0, Lfmm;->Q:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    .line 166
    iget-object v0, p0, Lfmm;->Q:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    new-instance v1, Lfmp;

    invoke-direct {v1, p0}, Lfmp;-><init>(Lfmm;)V

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->a(Lkhp;)V

    .line 177
    iget-object v0, p0, Lfmm;->U:Lkgp;

    iget-object v1, p0, Lfmm;->Q:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    invoke-virtual {v0, v1}, Lkgp;->a(Lkhl;)Lkhl;

    .line 179
    iget-boolean v0, p0, Lfmm;->O:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfmm;->N:Lfmh;

    iget-object v0, v0, Lfmh;->b:Lhgw;

    if-eqz v0, :cond_0

    .line 180
    iget-object v0, p0, Lfmm;->W:Lkhr;

    const v1, 0x7f0a0892

    invoke-virtual {v0, v1}, Lkhr;->a(I)Lcom/google/android/libraries/social/settings/PreferenceCategory;

    move-result-object v0

    .line 183
    iget-object v1, p0, Lfmm;->W:Lkhr;

    const v2, 0x7f0a0893

    .line 184
    invoke-virtual {p0, v2}, Lfmm;->e_(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lfmm;->N:Lfmh;

    iget-object v3, v3, Lfmh;->b:Lhgw;

    .line 185
    invoke-static {v3}, Lfuk;->a(Lhgw;)I

    move-result v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lfmm;->N:Lfmh;

    iget-object v5, v5, Lfmh;->c:Ljava/lang/String;

    aput-object v5, v4, v6

    .line 184
    invoke-virtual {p0, v3, v4}, Lfmm;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 183
    invoke-virtual {v1, v2, v3}, Lkhr;->a(Ljava/lang/String;Ljava/lang/String;)Lkhl;

    move-result-object v1

    iput-object v1, p0, Lfmm;->R:Lkhl;

    .line 187
    iget-object v1, p0, Lfmm;->R:Lkhl;

    new-instance v2, Lfmq;

    invoke-direct {v2, p0}, Lfmq;-><init>(Lfmm;)V

    invoke-virtual {v1, v2}, Lkhl;->a(Lkhq;)V

    .line 197
    iget-object v1, p0, Lfmm;->R:Lkhl;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/settings/PreferenceCategory;->c(Lkhl;)Z

    .line 199
    iget-object v1, p0, Lfmm;->W:Lkhr;

    const/4 v2, 0x0

    const v3, 0x7f0a06f2

    .line 200
    invoke-virtual {p0, v3}, Lfmm;->e_(I)Ljava/lang/String;

    move-result-object v3

    .line 199
    invoke-virtual {v1, v2, v3}, Lkhr;->b(Ljava/lang/String;Ljava/lang/String;)Lkhl;

    move-result-object v1

    .line 201
    invoke-virtual {v1, v6}, Lkhl;->c(Z)V

    .line 202
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/settings/PreferenceCategory;->c(Lkhl;)Z

    .line 204
    new-instance v1, Lkgz;

    iget-object v2, p0, Lfmm;->at:Llnl;

    invoke-direct {v1, v2}, Lkgz;-><init>(Landroid/content/Context;)V

    .line 205
    invoke-virtual {v1, v6}, Lkgz;->c(Z)V

    .line 206
    iget-object v2, p0, Lfmm;->at:Llnl;

    const-string v3, "plusone_posts"

    const-string v4, "https://support.google.com/plus/?hl=%locale%"

    invoke-static {v2, v3, v4}, Litk;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 207
    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-direct {v3, v4, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v1, v3}, Lkgz;->a(Landroid/content/Intent;)V

    .line 208
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/settings/PreferenceCategory;->c(Lkhl;)Z

    .line 210
    :cond_0
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 135
    invoke-super {p0, p1}, Llol;->e(Landroid/os/Bundle;)V

    .line 136
    iget-object v0, p0, Lfmm;->N:Lfmh;

    if-eqz v0, :cond_0

    .line 137
    const-string v0, "sharing_settings"

    iget-object v1, p0, Lfmm;->N:Lfmh;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 139
    :cond_0
    return-void
.end method
