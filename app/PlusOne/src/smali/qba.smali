.class public final Lqba;
.super Loxq;
.source "PG"


# instance fields
.field public a:I

.field public b:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1505
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1508
    const/high16 v0, -0x80000000

    iput v0, p0, Lqba;->a:I

    .line 1505
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1527
    const/4 v0, 0x0

    .line 1528
    iget v1, p0, Lqba;->a:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 1529
    const/4 v0, 0x1

    iget v1, p0, Lqba;->a:I

    .line 1530
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1532
    :cond_0
    iget-object v1, p0, Lqba;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 1533
    const/4 v1, 0x2

    iget-object v2, p0, Lqba;->b:Ljava/lang/Integer;

    .line 1534
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1536
    :cond_1
    iget-object v1, p0, Lqba;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1537
    iput v0, p0, Lqba;->ai:I

    .line 1538
    return v0
.end method

.method public a(Loxn;)Lqba;
    .locals 2

    .prologue
    .line 1546
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1547
    sparse-switch v0, :sswitch_data_0

    .line 1551
    iget-object v1, p0, Lqba;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1552
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lqba;->ah:Ljava/util/List;

    .line 1555
    :cond_1
    iget-object v1, p0, Lqba;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1557
    :sswitch_0
    return-object p0

    .line 1562
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1563
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa

    if-eq v0, v1, :cond_2

    const/16 v1, 0xb

    if-eq v0, v1, :cond_2

    const/16 v1, 0xc

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd

    if-eq v0, v1, :cond_2

    const/16 v1, 0xe

    if-eq v0, v1, :cond_2

    const/16 v1, 0xf

    if-eq v0, v1, :cond_2

    const/16 v1, 0x10

    if-eq v0, v1, :cond_2

    const/16 v1, 0x11

    if-eq v0, v1, :cond_2

    const/16 v1, 0x12

    if-eq v0, v1, :cond_2

    const/16 v1, 0x13

    if-eq v0, v1, :cond_2

    const/16 v1, 0x14

    if-eq v0, v1, :cond_2

    const/16 v1, 0x15

    if-eq v0, v1, :cond_2

    const/16 v1, 0x16

    if-eq v0, v1, :cond_2

    const/16 v1, 0x17

    if-eq v0, v1, :cond_2

    const/16 v1, 0x18

    if-eq v0, v1, :cond_2

    const/16 v1, 0x19

    if-ne v0, v1, :cond_3

    .line 1589
    :cond_2
    iput v0, p0, Lqba;->a:I

    goto :goto_0

    .line 1591
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lqba;->a:I

    goto/16 :goto_0

    .line 1596
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lqba;->b:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 1547
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1515
    iget v0, p0, Lqba;->a:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 1516
    const/4 v0, 0x1

    iget v1, p0, Lqba;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1518
    :cond_0
    iget-object v0, p0, Lqba;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 1519
    const/4 v0, 0x2

    iget-object v1, p0, Lqba;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1521
    :cond_1
    iget-object v0, p0, Lqba;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1523
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1501
    invoke-virtual {p0, p1}, Lqba;->a(Loxn;)Lqba;

    move-result-object v0

    return-object v0
.end method
