.class final Lena;
.super Landroid/widget/ArrayAdapter;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lofi;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 144
    const v0, 0x7f040093

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {p0, p1, v0, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 146
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v3, -0x1

    const/4 v4, 0x0

    .line 150
    .line 151
    if-nez p2, :cond_0

    .line 152
    invoke-virtual {p0}, Lena;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 153
    const v1, 0x7f040093

    const/4 v5, 0x0

    invoke-virtual {v0, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 155
    :cond_0
    invoke-virtual {p0, p1}, Lena;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lofi;

    .line 157
    const v1, 0x7f100174

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    .line 158
    invoke-virtual {v1, v4}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setVisibility(I)V

    .line 159
    iget-object v5, v0, Lofi;->b:Lofv;

    iget-object v5, v5, Lofv;->c:Ljava/lang/String;

    iget-object v6, v0, Lofi;->b:Lofv;

    iget-object v6, v6, Lofv;->d:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    const v1, 0x7f10013d

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 162
    iget-object v5, v0, Lofi;->b:Lofv;

    iget-object v5, v5, Lofv;->b:Ljava/lang/String;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 164
    iget-object v1, v0, Lofi;->c:[Lltf;

    if-eqz v1, :cond_4

    .line 167
    iget-object v5, v0, Lofi;->c:[Lltf;

    array-length v6, v5

    move v0, v4

    move v1, v4

    :goto_0
    if-ge v4, v6, :cond_3

    aget-object v7, v5, v4

    .line 168
    iget v8, v7, Lltf;->b:I

    const/4 v9, 0x2

    if-ne v8, v9, :cond_2

    move v0, v2

    .line 167
    :cond_1
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 170
    :cond_2
    iget v7, v7, Lltf;->b:I

    const/4 v8, 0x3

    if-ne v7, v8, :cond_1

    move v1, v2

    .line 171
    goto :goto_1

    .line 175
    :cond_3
    if-eqz v0, :cond_5

    if-eqz v1, :cond_5

    .line 176
    const v0, 0x7f0a0af4

    move v1, v0

    .line 182
    :goto_2
    if-eq v1, v3, :cond_4

    .line 183
    const v0, 0x7f10027a

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 184
    invoke-virtual {p2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 188
    :cond_4
    return-object p2

    .line 177
    :cond_5
    if-eqz v0, :cond_6

    .line 178
    const v0, 0x7f0a0af2

    move v1, v0

    goto :goto_2

    .line 179
    :cond_6
    if-eqz v1, :cond_7

    .line 180
    const v0, 0x7f0a0af3

    move v1, v0

    goto :goto_2

    :cond_7
    move v1, v3

    goto :goto_2
.end method
