.class public final Llhg;
.super Llhb;
.source "PG"


# instance fields
.field private final g:Loo;

.field private final h:I

.field private i:Llhc;

.field private j:Llhh;

.field private k:Z


# direct methods
.method public constructor <init>(Loo;Llhc;)V
    .locals 3

    .prologue
    .line 34
    invoke-virtual {p1}, Loo;->i()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Loo;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    :goto_0
    const/4 v2, 0x1

    invoke-direct {p0, v1, v0, v2}, Llhb;-><init>(Landroid/content/Context;IZ)V

    .line 37
    iput-object p1, p0, Llhg;->g:Loo;

    .line 38
    iput-object p2, p0, Llhg;->i:Llhc;

    .line 39
    iget-object v0, p0, Llhg;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 40
    const v1, 0x7f0d0243

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Llhg;->h:I

    .line 42
    const v1, 0x7f0d0242

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Llhg;->f:I

    .line 44
    return-void

    .line 34
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 66
    invoke-super {p0, p1, p2}, Llhb;->a(Landroid/view/View;I)V

    .line 67
    iget-object v0, p0, Llhg;->i:Llhc;

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Llhg;->i:Llhc;

    invoke-interface {v0, p1, p2}, Llhc;->a(Landroid/view/View;I)V

    .line 70
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Llhg;->k:Z

    .line 71
    return-void
.end method

.method public a(Landroid/view/View;III)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 75
    iget-object v0, p0, Llhg;->g:Loo;

    invoke-virtual {v0}, Loo;->g()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x2

    :goto_0
    iput v0, p0, Llhg;->c:I

    .line 77
    if-gez p3, :cond_0

    iget v0, p0, Llhg;->e:I

    if-gtz v0, :cond_1

    :cond_0
    if-lez p3, :cond_6

    iget v0, p0, Llhg;->e:I

    if-gez v0, :cond_6

    :cond_1
    move v0, v2

    .line 79
    :goto_1
    if-eqz v0, :cond_2

    .line 80
    iput-boolean v1, p0, Llhg;->k:Z

    .line 83
    :cond_2
    invoke-super {p0, p1, p2, p3, p4}, Llhb;->a(Landroid/view/View;III)V

    .line 85
    iget-object v0, p0, Llhg;->i:Llhc;

    if-eqz v0, :cond_4

    if-gez p3, :cond_7

    move v6, v2

    :goto_2
    if-lez p3, :cond_8

    move v5, v2

    :goto_3
    if-nez p2, :cond_9

    if-nez p3, :cond_9

    if-nez p4, :cond_9

    move v4, v2

    :goto_4
    if-nez p2, :cond_b

    iget v3, p0, Llhg;->f:I

    iget-boolean v0, p0, Llhg;->b:Z

    if-eqz v0, :cond_a

    neg-int v0, p4

    :goto_5
    if-lt v3, v0, :cond_b

    move v3, v2

    :goto_6
    if-eqz v6, :cond_c

    if-nez v3, :cond_3

    move v1, v2

    :cond_3
    :goto_7
    if-eqz v1, :cond_4

    .line 86
    iget-object v0, p0, Llhg;->i:Llhc;

    iget-boolean v1, p0, Llhg;->k:Z

    if-eqz v1, :cond_10

    :goto_8
    invoke-interface {v0, p1, p2, p3, p4}, Llhc;->a(Landroid/view/View;III)V

    .line 88
    iput-boolean v2, p0, Llhg;->k:Z

    .line 90
    :cond_4
    return-void

    :cond_5
    move v0, v1

    .line 75
    goto :goto_0

    :cond_6
    move v0, v1

    .line 77
    goto :goto_1

    :cond_7
    move v6, v1

    .line 85
    goto :goto_2

    :cond_8
    move v5, v1

    goto :goto_3

    :cond_9
    move v4, v1

    goto :goto_4

    :cond_a
    move v0, p4

    goto :goto_5

    :cond_b
    move v3, v1

    goto :goto_6

    :cond_c
    if-nez v5, :cond_d

    if-eqz v4, :cond_3

    :cond_d
    iget v0, p0, Llhg;->e:I

    iget v4, p0, Llhg;->h:I

    if-le v0, v4, :cond_f

    move v0, v2

    :goto_9
    if-nez v3, :cond_e

    if-eqz v0, :cond_3

    :cond_e
    move v1, v2

    goto :goto_7

    :cond_f
    move v0, v1

    goto :goto_9

    .line 86
    :cond_10
    iget p3, p0, Llhg;->e:I

    goto :goto_8
.end method

.method public a(Llhh;)V
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Llhg;->j:Llhh;

    .line 62
    return-void
.end method

.method protected a(Z)V
    .locals 1

    .prologue
    .line 170
    invoke-virtual {p0, p1}, Llhg;->b(Z)F

    move-result v0

    iput v0, p0, Llhg;->d:F

    .line 171
    return-void
.end method

.method protected b(Z)F
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 122
    if-eqz p1, :cond_2

    .line 123
    iget-object v2, p0, Llhg;->g:Loo;

    invoke-virtual {v2}, Loo;->g()Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    if-eqz v0, :cond_5

    .line 124
    const/4 v0, 0x0

    .line 131
    :goto_1
    return v0

    .line 123
    :cond_0
    iget-object v0, p0, Llhg;->g:Loo;

    invoke-virtual {v0}, Loo;->e()V

    const/4 v0, 0x2

    iput v0, p0, Llhg;->c:I

    iget-object v0, p0, Llhg;->j:Llhh;

    if-eqz v0, :cond_1

    iget-object v0, p0, Llhg;->j:Llhh;

    invoke-interface {v0, v1}, Llhh;->a(Z)V

    :cond_1
    move v0, v1

    goto :goto_0

    .line 127
    :cond_2
    iget-object v2, p0, Llhg;->g:Loo;

    invoke-virtual {v2}, Loo;->g()Z

    move-result v2

    if-nez v2, :cond_3

    :goto_2
    if-eqz v0, :cond_5

    .line 128
    iget-object v0, p0, Llhg;->g:Loo;

    invoke-virtual {v0}, Loo;->d()I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    goto :goto_1

    .line 127
    :cond_3
    iget-object v2, p0, Llhg;->j:Llhh;

    if-eqz v2, :cond_4

    iget-object v2, p0, Llhg;->j:Llhh;

    invoke-interface {v2, v0}, Llhh;->a(Z)V

    :cond_4
    iget-object v2, p0, Llhg;->g:Loo;

    invoke-virtual {v2}, Loo;->f()V

    iput v0, p0, Llhg;->c:I

    move v0, v1

    goto :goto_2

    .line 131
    :cond_5
    iget v0, p0, Llhg;->d:F

    goto :goto_1
.end method

.method protected e()V
    .locals 0

    .prologue
    .line 118
    return-void
.end method
