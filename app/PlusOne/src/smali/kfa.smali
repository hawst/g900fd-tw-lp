.class public abstract Lkfa;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lkfn;


# static fields
.field private static final a:Llrq;

.field private static final b:Lloy;

.field private static final c:Llrq;

.field private static final d:Llrq;

.field private static final e:Llrq;

.field private static final f:Llrq;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 19
    new-instance v0, Llrq;

    const-string v1, "debug.plus.apiary_token"

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Llrq;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lkfa;->a:Llrq;

    .line 25
    new-instance v0, Llrq;

    const-string v1, "debug.plus.backend.url"

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Llrq;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    new-instance v0, Lloy;

    const-string v1, "debug.plus.tracing_enabled"

    invoke-direct {v0, v1}, Lloy;-><init>(Ljava/lang/String;)V

    sput-object v0, Lkfa;->b:Lloy;

    .line 38
    new-instance v0, Llrq;

    const-string v1, "debug.plus.tracing_token"

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Llrq;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lkfa;->c:Llrq;

    .line 44
    new-instance v0, Llrq;

    const-string v1, "debug.plus.tracing_path"

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Llrq;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lkfa;->d:Llrq;

    .line 52
    new-instance v0, Llrq;

    const-string v1, "debug.plus.tracing_level"

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Llrq;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lkfa;->e:Llrq;

    .line 58
    new-instance v0, Llrq;

    const-string v1, "debug.plus.experiment_override"

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Llrq;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lkfa;->f:Llrq;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    sget-object v0, Lkfa;->e:Llrq;

    invoke-virtual {v0}, Llrq;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lkfa;->a:Llrq;

    invoke-virtual {v0}, Llrq;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 73
    sget-object v0, Lkfa;->b:Lloy;

    const/4 v0, 0x0

    return v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    sget-object v0, Lkfa;->c:Llrq;

    invoke-virtual {v0}, Llrq;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 83
    sget-object v0, Lkfa;->d:Llrq;

    invoke-virtual {v0}, Llrq;->a()Ljava/lang/String;

    move-result-object v0

    .line 84
    if-eqz v0, :cond_0

    .line 85
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 87
    :cond_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 88
    const-string v0, ".*"

    .line 90
    :cond_1
    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 100
    sget-object v0, Lkfa;->f:Llrq;

    invoke-virtual {v0}, Llrq;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
