.class public final Loif;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Boolean;

.field public b:Ljava/lang/Boolean;

.field private c:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6401
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 6427
    const/4 v0, 0x0

    .line 6428
    iget-object v1, p0, Loif;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 6429
    const/4 v0, 0x1

    iget-object v1, p0, Loif;->a:Ljava/lang/Boolean;

    .line 6430
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 6432
    :cond_0
    iget-object v1, p0, Loif;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 6433
    const/4 v1, 0x3

    iget-object v2, p0, Loif;->c:Ljava/lang/Boolean;

    .line 6434
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 6436
    :cond_1
    iget-object v1, p0, Loif;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 6437
    const/4 v1, 0x4

    iget-object v2, p0, Loif;->b:Ljava/lang/Boolean;

    .line 6438
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 6440
    :cond_2
    iget-object v1, p0, Loif;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6441
    iput v0, p0, Loif;->ai:I

    .line 6442
    return v0
.end method

.method public a(Loxn;)Loif;
    .locals 2

    .prologue
    .line 6450
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 6451
    sparse-switch v0, :sswitch_data_0

    .line 6455
    iget-object v1, p0, Loif;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 6456
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Loif;->ah:Ljava/util/List;

    .line 6459
    :cond_1
    iget-object v1, p0, Loif;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6461
    :sswitch_0
    return-object p0

    .line 6466
    :sswitch_1
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Loif;->a:Ljava/lang/Boolean;

    goto :goto_0

    .line 6470
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Loif;->c:Ljava/lang/Boolean;

    goto :goto_0

    .line 6474
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Loif;->b:Ljava/lang/Boolean;

    goto :goto_0

    .line 6451
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x18 -> :sswitch_2
        0x20 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 6412
    iget-object v0, p0, Loif;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 6413
    const/4 v0, 0x1

    iget-object v1, p0, Loif;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 6415
    :cond_0
    iget-object v0, p0, Loif;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 6416
    const/4 v0, 0x3

    iget-object v1, p0, Loif;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 6418
    :cond_1
    iget-object v0, p0, Loif;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 6419
    const/4 v0, 0x4

    iget-object v1, p0, Loif;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 6421
    :cond_2
    iget-object v0, p0, Loif;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 6423
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6397
    invoke-virtual {p0, p1}, Loif;->a(Loxn;)Loif;

    move-result-object v0

    return-object v0
.end method
