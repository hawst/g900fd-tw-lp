.class public final Lmwu;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Integer;

.field private b:Ljava/lang/Integer;

.field private c:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11770
    invoke-direct {p0}, Loxq;-><init>()V

    .line 11777
    const/high16 v0, -0x80000000

    iput v0, p0, Lmwu;->c:I

    .line 11770
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 11797
    const/4 v0, 0x0

    .line 11798
    iget-object v1, p0, Lmwu;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 11799
    const/4 v0, 0x1

    iget-object v1, p0, Lmwu;->a:Ljava/lang/Integer;

    .line 11800
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 11802
    :cond_0
    iget-object v1, p0, Lmwu;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 11803
    const/4 v1, 0x2

    iget-object v2, p0, Lmwu;->b:Ljava/lang/Integer;

    .line 11804
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 11806
    :cond_1
    iget v1, p0, Lmwu;->c:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_2

    .line 11807
    const/4 v1, 0x3

    iget v2, p0, Lmwu;->c:I

    .line 11808
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 11810
    :cond_2
    iget-object v1, p0, Lmwu;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11811
    iput v0, p0, Lmwu;->ai:I

    .line 11812
    return v0
.end method

.method public a(Loxn;)Lmwu;
    .locals 2

    .prologue
    .line 11820
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 11821
    sparse-switch v0, :sswitch_data_0

    .line 11825
    iget-object v1, p0, Lmwu;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 11826
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmwu;->ah:Ljava/util/List;

    .line 11829
    :cond_1
    iget-object v1, p0, Lmwu;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 11831
    :sswitch_0
    return-object p0

    .line 11836
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmwu;->a:Ljava/lang/Integer;

    goto :goto_0

    .line 11840
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmwu;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 11844
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 11845
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 11849
    :cond_2
    iput v0, p0, Lmwu;->c:I

    goto :goto_0

    .line 11851
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lmwu;->c:I

    goto :goto_0

    .line 11821
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 11782
    iget-object v0, p0, Lmwu;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 11783
    const/4 v0, 0x1

    iget-object v1, p0, Lmwu;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 11785
    :cond_0
    iget-object v0, p0, Lmwu;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 11786
    const/4 v0, 0x2

    iget-object v1, p0, Lmwu;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 11788
    :cond_1
    iget v0, p0, Lmwu;->c:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_2

    .line 11789
    const/4 v0, 0x3

    iget v1, p0, Lmwu;->c:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 11791
    :cond_2
    iget-object v0, p0, Lmwu;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 11793
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 11766
    invoke-virtual {p0, p1}, Lmwu;->a(Loxn;)Lmwu;

    move-result-object v0

    return-object v0
.end method
