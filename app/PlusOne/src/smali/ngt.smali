.class public final Lngt;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lngl;

.field public c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 489
    const v0, 0x39526cd

    new-instance v1, Lngu;

    invoke-direct {v1}, Lngu;-><init>()V

    .line 494
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    .line 493
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 490
    invoke-direct {p0}, Loxq;-><init>()V

    .line 499
    const/4 v0, 0x0

    iput-object v0, p0, Lngt;->b:Lngl;

    .line 490
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 521
    const/4 v0, 0x0

    .line 522
    iget-object v1, p0, Lngt;->b:Lngl;

    if-eqz v1, :cond_0

    .line 523
    const/4 v0, 0x2

    iget-object v1, p0, Lngt;->b:Lngl;

    .line 524
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 526
    :cond_0
    iget-object v1, p0, Lngt;->a:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 527
    const/4 v1, 0x4

    iget-object v2, p0, Lngt;->a:Ljava/lang/String;

    .line 528
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 530
    :cond_1
    iget-object v1, p0, Lngt;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 531
    const/4 v1, 0x5

    iget-object v2, p0, Lngt;->c:Ljava/lang/String;

    .line 532
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 534
    :cond_2
    iget-object v1, p0, Lngt;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 535
    iput v0, p0, Lngt;->ai:I

    .line 536
    return v0
.end method

.method public a(Loxn;)Lngt;
    .locals 2

    .prologue
    .line 544
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 545
    sparse-switch v0, :sswitch_data_0

    .line 549
    iget-object v1, p0, Lngt;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 550
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lngt;->ah:Ljava/util/List;

    .line 553
    :cond_1
    iget-object v1, p0, Lngt;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 555
    :sswitch_0
    return-object p0

    .line 560
    :sswitch_1
    iget-object v0, p0, Lngt;->b:Lngl;

    if-nez v0, :cond_2

    .line 561
    new-instance v0, Lngl;

    invoke-direct {v0}, Lngl;-><init>()V

    iput-object v0, p0, Lngt;->b:Lngl;

    .line 563
    :cond_2
    iget-object v0, p0, Lngt;->b:Lngl;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 567
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lngt;->a:Ljava/lang/String;

    goto :goto_0

    .line 571
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lngt;->c:Ljava/lang/String;

    goto :goto_0

    .line 545
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x22 -> :sswitch_2
        0x2a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 506
    iget-object v0, p0, Lngt;->b:Lngl;

    if-eqz v0, :cond_0

    .line 507
    const/4 v0, 0x2

    iget-object v1, p0, Lngt;->b:Lngl;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 509
    :cond_0
    iget-object v0, p0, Lngt;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 510
    const/4 v0, 0x4

    iget-object v1, p0, Lngt;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 512
    :cond_1
    iget-object v0, p0, Lngt;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 513
    const/4 v0, 0x5

    iget-object v1, p0, Lngt;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 515
    :cond_2
    iget-object v0, p0, Lngt;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 517
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 486
    invoke-virtual {p0, p1}, Lngt;->a(Loxn;)Lngt;

    move-result-object v0

    return-object v0
.end method
