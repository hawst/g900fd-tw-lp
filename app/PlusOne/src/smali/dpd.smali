.class public final Ldpd;
.super Lhny;
.source "PG"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:I

.field private final c:I

.field private final d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ldli;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lhxb;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;ILjava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Ldli;",
            ">;I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 44
    const-string v0, "GroupModifyCircleMembershipsTask"

    invoke-direct {p0, p1, v0}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 45
    iput-object p1, p0, Ldpd;->a:Landroid/content/Context;

    .line 46
    iput p2, p0, Ldpd;->b:I

    .line 47
    iput p4, p0, Ldpd;->c:I

    .line 48
    iput-object p3, p0, Ldpd;->d:Ljava/util/ArrayList;

    .line 49
    iput-object p5, p0, Ldpd;->e:Ljava/util/ArrayList;

    .line 50
    iput-object p6, p0, Ldpd;->f:Ljava/util/ArrayList;

    .line 52
    const-class v0, Lhxb;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhxb;

    iput-object v0, p0, Ldpd;->h:Lhxb;

    .line 53
    return-void
.end method

.method private a(Loju;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 131
    iget-object v0, p1, Loju;->a:[Lohv;

    if-nez v0, :cond_1

    .line 152
    :cond_0
    return-void

    :cond_1
    move v6, v7

    .line 135
    :goto_0
    iget-object v0, p1, Loju;->a:[Lohv;

    array-length v0, v0

    if-ge v6, v0, :cond_0

    .line 136
    iget-object v0, p1, Loju;->a:[Lohv;

    aget-object v3, v0, v6

    .line 137
    iget-object v0, v3, Lohv;->b:Lohp;

    invoke-static {v0}, Ldsm;->a(Lohp;)Ljava/lang/String;

    move-result-object v2

    .line 138
    if-eqz v3, :cond_2

    .line 140
    iget-object v0, v3, Lohv;->d:[Loij;

    if-nez v0, :cond_2

    .line 141
    iget-object v0, p0, Ldpd;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Loij;

    iput-object v0, v3, Lohv;->d:[Loij;

    move v1, v7

    .line 142
    :goto_1
    iget-object v0, p0, Ldpd;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 143
    new-instance v4, Loij;

    invoke-direct {v4}, Loij;-><init>()V

    .line 144
    iget-object v0, p0, Ldpd;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lhxe;->b(Ljava/lang/String;)Lohn;

    move-result-object v0

    iput-object v0, v4, Loij;->b:Lohn;

    .line 145
    iget-object v0, v3, Lohv;->d:[Loij;

    aput-object v4, v0, v1

    .line 142
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 149
    :cond_2
    invoke-virtual {p0}, Ldpd;->f()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Ldpd;->b:I

    iget-object v4, p0, Ldpd;->e:Ljava/util/ArrayList;

    iget-object v5, p0, Ldpd;->f:Ljava/util/ArrayList;

    invoke-static/range {v0 .. v5}, Ldsm;->a(Landroid/content/Context;ILjava/lang/String;Lohv;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 135
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0
.end method

.method private a(Z)V
    .locals 8

    .prologue
    .line 193
    iget-object v0, p0, Ldpd;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    .line 194
    const/4 v0, 0x0

    move v6, v0

    :goto_0
    if-ge v6, v7, :cond_0

    .line 195
    iget-object v0, p0, Ldpd;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Ldli;

    .line 196
    iget-object v0, p0, Ldpd;->h:Lhxb;

    invoke-virtual {p0}, Ldpd;->f()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Ldpd;->b:I

    .line 197
    invoke-virtual {v4}, Ldli;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Ldli;->b()Ljava/lang/String;

    move-result-object v4

    move v5, p1

    .line 196
    invoke-interface/range {v0 .. v5}, Lhxb;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)V

    .line 194
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 199
    :cond_0
    return-void
.end method

.method private d()Lhoz;
    .locals 9
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 88
    const/4 v8, 0x0

    .line 91
    :try_start_0
    iget-object v0, p0, Ldpd;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v7

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, Ldpd;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldli;

    iget-object v3, p0, Ldpd;->h:Lhxb;

    invoke-virtual {p0}, Ldpd;->f()Landroid/content/Context;

    move-result-object v4

    iget v5, p0, Ldpd;->b:I

    invoke-virtual {v0}, Ldli;->a()Ljava/lang/String;

    move-result-object v0

    const/4 v6, 0x0

    invoke-interface {v3, v4, v5, v0, v6}, Lhxb;->a(Landroid/content/Context;ILjava/lang/String;Z)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 93
    :cond_0
    new-instance v0, Ldlh;

    iget-object v1, p0, Ldpd;->a:Landroid/content/Context;

    new-instance v2, Lkfo;

    iget-object v3, p0, Ldpd;->a:Landroid/content/Context;

    iget v4, p0, Ldpd;->b:I

    invoke-direct {v2, v3, v4}, Lkfo;-><init>(Landroid/content/Context;I)V

    iget v3, p0, Ldpd;->b:I

    iget-object v3, p0, Ldpd;->d:Ljava/util/ArrayList;

    iget-object v4, p0, Ldpd;->e:Ljava/util/ArrayList;

    iget-object v5, p0, Ldpd;->f:Ljava/util/ArrayList;

    iget v6, p0, Ldpd;->c:I

    invoke-direct/range {v0 .. v6}, Ldlh;-><init>(Landroid/content/Context;Lkfo;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;I)V

    .line 96
    invoke-virtual {v0}, Ldlh;->l()V

    .line 97
    invoke-virtual {v0}, Ldlh;->t()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 98
    invoke-virtual {p0}, Ldpd;->f()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a0592

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 99
    const-string v2, "ModifyCircleMemberships"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 100
    const-string v2, "GroupModifyCircleMembershipsTask failed with "

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget v3, v0, Lkff;->i:I

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0xb

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v2, v1

    move v1, v7

    .line 106
    :goto_1
    :try_start_1
    new-instance v3, Lhoz;

    iget v4, v0, Lkff;->i:I

    iget-object v0, v0, Lkff;->k:Ljava/lang/Exception;

    invoke-direct {v3, v4, v0, v2}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 109
    invoke-direct {p0, v1}, Ldpd;->a(Z)V

    return-object v3

    .line 103
    :cond_1
    const/4 v2, 0x1

    .line 104
    :try_start_2
    invoke-virtual {v0}, Ldlh;->D()Loxu;

    move-result-object v1

    check-cast v1, Lmff;

    iget-object v1, v1, Lmff;->a:Loju;

    iget-object v3, v1, Loju;->a:[Lohv;

    if-eqz v3, :cond_3

    iget-object v3, p0, Ldpd;->e:Ljava/util/ArrayList;

    if-eqz v3, :cond_2

    iget-object v3, p0, Ldpd;->e:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_2

    invoke-direct {p0, v1}, Ldpd;->a(Loju;)V

    move v1, v2

    move-object v2, v8

    goto :goto_1

    :cond_2
    invoke-direct {p0}, Ldpd;->e()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :cond_3
    move v1, v2

    move-object v2, v8

    goto :goto_1

    .line 109
    :catchall_0
    move-exception v0

    move v1, v7

    :goto_2
    invoke-direct {p0, v1}, Ldpd;->a(Z)V

    throw v0

    :catchall_1
    move-exception v0

    move v1, v2

    goto :goto_2

    :catchall_2
    move-exception v0

    goto :goto_2

    :cond_4
    move-object v2, v1

    move v1, v7

    goto :goto_1
.end method

.method private e()V
    .locals 8

    .prologue
    .line 160
    iget-object v0, p0, Ldpd;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    .line 161
    const/4 v0, 0x0

    move v6, v0

    :goto_0
    if-ge v6, v7, :cond_0

    .line 162
    iget-object v0, p0, Ldpd;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldli;

    .line 163
    invoke-virtual {v0}, Ldli;->a()Ljava/lang/String;

    move-result-object v2

    .line 164
    invoke-virtual {p0}, Ldpd;->f()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Ldpd;->b:I

    const/4 v3, 0x0

    iget-object v4, p0, Ldpd;->e:Ljava/util/ArrayList;

    iget-object v5, p0, Ldpd;->f:Ljava/util/ArrayList;

    invoke-static/range {v0 .. v5}, Ldsm;->a(Landroid/content/Context;ILjava/lang/String;Lohv;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 161
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 167
    :cond_0
    return-void
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Ldpd;->d()Lhoz;

    move-result-object v0

    return-object v0
.end method
