.class public final Lffl;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static a:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 113
    const/4 v0, -0x1

    sput v0, Lffl;->a:I

    return-void
.end method

.method private static a(I)I
    .locals 1

    .prologue
    .line 1516
    const v0, 0x7f1000a2

    if-ne p0, v0, :cond_0

    const v0, 0x7f020368

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f020363

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;Landroid/database/Cursor;I)I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1526
    const-class v0, Lieh;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    .line 1527
    sget-object v2, Ldxd;->C:Lief;

    invoke-interface {v0, v2, p2}, Lieh;->b(Lief;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1544
    :cond_0
    :goto_0
    return v1

    .line 1535
    :cond_1
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 1539
    :cond_2
    const/16 v2, 0xa

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 1540
    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1541
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1543
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1544
    const/4 v2, 0x3

    if-ne v0, v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;II)Landroid/app/Notification;
    .locals 7
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 928
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 929
    if-le p2, v4, :cond_0

    .line 930
    invoke-static {p1}, Lffl;->b(I)I

    move-result v0

    .line 932
    :goto_0
    new-instance v2, Lbs;

    invoke-direct {v2, p0}, Lbs;-><init>(Landroid/content/Context;)V

    .line 936
    const v3, 0x7f0a024d

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lbs;->a(Ljava/lang/CharSequence;)Lbs;

    .line 937
    const v3, 0x7f11007b

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 938
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    .line 937
    invoke-virtual {v1, v3, p2, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 939
    invoke-virtual {v2, v3}, Lbs;->b(Ljava/lang/CharSequence;)Lbs;

    .line 940
    invoke-virtual {v2, v0}, Lbs;->a(I)Lbs;

    .line 941
    const v0, 0x7f0b01a1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v2, v0}, Lbs;->d(I)Lbs;

    .line 942
    invoke-virtual {v2}, Lbs;->c()Landroid/app/Notification;

    move-result-object v0

    return-object v0

    .line 931
    :cond_0
    invoke-static {p1}, Lffl;->a(I)I

    move-result v0

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;ILandroid/database/Cursor;Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 6

    .prologue
    .line 1158
    if-nez p2, :cond_0

    .line 1159
    const/4 v0, 0x0

    .line 1171
    :goto_0
    return-object v0

    .line 1161
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1163
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    new-array v3, v0, [J

    .line 1164
    const/4 v0, 0x0

    .line 1165
    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1167
    :goto_1
    const/16 v1, 0x9

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1168
    add-int/lit8 v1, v0, 0x1

    const/16 v4, 0x14

    invoke-interface {p2, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    aput-wide v4, v3, v0

    .line 1169
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1171
    :cond_1
    invoke-static {p0, p1, v2, v3, p3}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILjava/util/ArrayList;[JLjava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;ILandroid/database/Cursor;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1179
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {p0, p1, p2, v0, v1}, Lffl;->a(Landroid/content/Context;ILandroid/database/Cursor;Lda;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/Context;ILandroid/database/Cursor;Lda;Z)Landroid/content/Intent;
    .locals 23

    .prologue
    .line 1202
    const/16 v2, 0xd

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 1203
    const/16 v2, 0x9

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 1204
    const/16 v2, 0xc

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 1205
    const/16 v2, 0x14

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 1206
    const/16 v2, 0xb

    .line 1207
    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    move/from16 v20, v2

    .line 1208
    :goto_0
    const/16 v2, 0x10

    .line 1209
    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1210
    const/16 v2, 0x1b

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v11

    .line 1211
    const/16 v2, 0xf

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    .line 1212
    if-nez v2, :cond_2

    const/4 v2, 0x0

    move-object v10, v2

    .line 1215
    :goto_1
    const/4 v2, 0x0

    .line 1217
    const/4 v3, 0x3

    if-ne v6, v3, :cond_3

    const/4 v3, 0x1

    move/from16 v22, v3

    .line 1218
    :goto_2
    const/16 v21, 0x0

    .line 1220
    packed-switch v6, :pswitch_data_0

    .line 1330
    :pswitch_0
    const/16 v3, 0x3f

    if-ne v7, v3, :cond_c

    .line 1331
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_c

    .line 1332
    const/4 v10, 0x1

    const/4 v11, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    invoke-static/range {v2 .. v11}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;IIJZZ)Landroid/content/Intent;

    move-result-object v2

    move/from16 v12, v21

    .line 1350
    :goto_3
    if-nez v2, :cond_0

    .line 1351
    const/4 v3, 0x2

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1352
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1353
    new-instance v2, Landroid/content/Intent;

    const-class v4, Lcom/google/android/libraries/social/gateway/GatewayActivity;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const-string v3, "account_id"

    move/from16 v0, p1

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1356
    :cond_0
    new-instance v14, Ljava/util/ArrayList;

    const/4 v3, 0x1

    invoke-direct {v14, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 1357
    invoke-virtual {v14, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1360
    invoke-static/range {p2 .. p2}, Lffl;->b(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v15

    move-object/from16 v3, p0

    move/from16 v4, p1

    move-object v5, v2

    move-object/from16 v6, p3

    move/from16 v10, v20

    move/from16 v11, v22

    move/from16 v13, p4

    .line 1358
    invoke-static/range {v3 .. v15}, Lfhu;->a(Landroid/content/Context;ILandroid/content/Intent;Lda;IJZZZZLjava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 1362
    :goto_4
    return-object v2

    .line 1207
    :cond_1
    const/4 v2, 0x0

    move/from16 v20, v2

    goto/16 :goto_0

    .line 1213
    :cond_2
    invoke-static {v2}, Ldqu;->a([B)Ljava/util/List;

    move-result-object v2

    move-object v10, v2

    goto :goto_1

    .line 1217
    :cond_3
    const/4 v3, 0x0

    move/from16 v22, v3

    goto :goto_2

    .line 1222
    :pswitch_1
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_e

    .line 1223
    const/4 v10, 0x1

    const/4 v11, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    invoke-static/range {v2 .. v11}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;IIJZZ)Landroid/content/Intent;

    move-result-object v2

    move/from16 v12, v21

    goto :goto_3

    .line 1231
    :pswitch_2
    const/16 v3, 0x11

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 1232
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_e

    const/16 v3, 0x4c

    if-eq v7, v3, :cond_e

    .line 1234
    const/4 v14, 0x0

    const/4 v15, 0x0

    const/high16 v17, -0x80000000

    const/16 v18, 0x0

    const/16 v19, 0x1

    move-object/from16 v10, p0

    move/from16 v11, p1

    move v13, v7

    move-object/from16 v16, v5

    invoke-static/range {v10 .. v19}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Z)Landroid/content/Intent;

    move-result-object v2

    move/from16 v12, v21

    goto/16 :goto_3

    .line 1243
    :pswitch_3
    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-static {v0, v1, v10, v5}, Lffl;->a(Landroid/content/Context;ILjava/util/List;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    move/from16 v12, v21

    .line 1244
    goto/16 :goto_3

    .line 1248
    :pswitch_4
    const/16 v3, 0x12

    if-ne v7, v3, :cond_4

    .line 1250
    const/4 v2, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-static {v0, v1, v2}, Leyq;->a(Landroid/content/Context;ILandroid/net/Uri;)Landroid/content/Intent;

    move-result-object v2

    .line 1251
    const-string v3, "starting_tab_index"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move/from16 v12, v21

    goto/16 :goto_3

    .line 1257
    :cond_4
    const/16 v3, 0x17

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_e

    .line 1258
    const/4 v2, 0x6

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v5, v3, v4

    .line 1259
    invoke-static {v2, v3}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1261
    new-instance v3, Ldew;

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v3, v0, v1}, Ldew;-><init>(Landroid/content/Context;I)V

    .line 1262
    move-object/from16 v0, p0

    invoke-virtual {v3, v0}, Ldew;->a(Landroid/content/Context;)Ldew;

    move-result-object v3

    .line 1263
    invoke-virtual {v3, v2}, Ldew;->d(Ljava/lang/String;)Ldew;

    move-result-object v2

    .line 1265
    const/16 v3, 0x61

    if-eq v7, v3, :cond_5

    const/16 v3, 0x6f

    if-ne v7, v3, :cond_6

    .line 1269
    :cond_5
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ldew;->k(Z)Ldew;

    move-result-object v2

    .line 1272
    :cond_6
    invoke-virtual {v2}, Ldew;->a()Landroid/content/Intent;

    move-result-object v2

    .line 1273
    const/4 v12, 0x1

    .line 1274
    goto/16 :goto_3

    .line 1280
    :pswitch_5
    const/16 v3, 0x13

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 1281
    const/16 v3, 0x31

    if-ne v7, v3, :cond_7

    .line 1282
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 1283
    const/4 v10, 0x1

    const/4 v11, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    invoke-static/range {v2 .. v11}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;IIJZZ)Landroid/content/Intent;

    move-result-object v2

    move/from16 v12, v21

    goto/16 :goto_3

    .line 1286
    :cond_7
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_e

    .line 1287
    const/16 v2, 0x34

    if-ne v7, v2, :cond_8

    .line 1288
    const-class v2, Lkvh;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lkvh;

    const/4 v13, -0x1

    const/4 v14, -0x1

    const/4 v2, 0x3

    .line 1292
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    move/from16 v11, p1

    .line 1288
    invoke-interface/range {v10 .. v15}, Lkvh;->a(ILjava/lang/String;IILjava/lang/Integer;)Landroid/content/Intent;

    move-result-object v2

    move/from16 v12, v21

    goto/16 :goto_3

    .line 1293
    :cond_8
    const/16 v2, 0x30

    if-ne v7, v2, :cond_9

    .line 1294
    const/4 v13, 0x0

    move-object/from16 v10, p0

    move/from16 v11, p1

    move-object v14, v5

    move-wide v15, v8

    invoke-static/range {v10 .. v16}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v2

    move/from16 v12, v21

    goto/16 :goto_3

    .line 1297
    :cond_9
    const/4 v13, 0x0

    move-object/from16 v10, p0

    move/from16 v11, p1

    move-object v14, v5

    move-wide v15, v8

    invoke-static/range {v10 .. v16}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v2

    move/from16 v12, v21

    goto/16 :goto_3

    .line 1305
    :pswitch_6
    const/16 v3, 0x21

    if-ne v7, v3, :cond_a

    .line 1306
    const/4 v2, 0x2

    .line 1307
    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1308
    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-static {v0, v1, v2}, Leyq;->g(Landroid/content/Context;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 1310
    :cond_a
    if-nez v2, :cond_e

    .line 1313
    invoke-static {}, Leyq;->a()Landroid/content/Intent;

    move-result-object v2

    goto/16 :goto_4

    .line 1319
    :pswitch_7
    const/4 v3, 0x2

    .line 1320
    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1321
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_e

    .line 1323
    invoke-static {v3}, Ldqs;->a(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 1324
    invoke-static/range {p0 .. p1}, Leyq;->b(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v2

    const-string v4, "circle_info"

    new-instance v11, Levm;

    const v6, 0x7f0a06fa

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v13

    const-string v14, "v.all.circles"

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const-wide/16 v18, 0x0

    move-object/from16 v12, p0

    invoke-direct/range {v11 .. v19}, Levm;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IZIJ)V

    invoke-virtual {v2, v4, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    if-eqz v3, :cond_b

    array-length v4, v3

    if-eqz v4, :cond_b

    const-string v4, "pinned_activity_ids"

    invoke-virtual {v2, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    :cond_b
    move/from16 v12, v21

    .line 1325
    goto/16 :goto_3

    .line 1335
    :cond_c
    const/16 v3, 0x69

    if-eq v7, v3, :cond_d

    const/16 v3, 0x6c

    if-ne v7, v3, :cond_e

    .line 1337
    :cond_d
    if-eqz v10, :cond_e

    invoke-interface {v10}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_e

    .line 1338
    const/4 v2, 0x0

    .line 1340
    invoke-interface {v10, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ldqv;

    iget-object v2, v2, Ldqv;->c:Ljava/lang/String;

    .line 1339
    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-static {v0, v1, v2}, Leyq;->f(Landroid/content/Context;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 1341
    const-string v3, "num_coalesced_notifs"

    .line 1342
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1343
    const-string v3, "notification_payload"

    invoke-virtual {v2, v3, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    :cond_e
    move/from16 v12, v21

    goto/16 :goto_3

    .line 1220
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_2
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_7
    .end packed-switch
.end method

.method private static a(Landroid/content/Context;ILjava/util/List;Ljava/lang/String;)Landroid/content/Intent;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Ldqv;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 1367
    if-eqz p2, :cond_3

    .line 1369
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 1370
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "gaia_id"

    .line 1371
    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1372
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1373
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldqv;

    .line 1374
    iget-object v4, v0, Ldqv;->c:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1375
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1381
    :cond_1
    :try_start_0
    invoke-static {v2}, Ldqu;->a(Ljava/util/List;)[B

    move-result-object v1

    .line 1384
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1385
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    .line 1386
    if-ne v0, v5, :cond_2

    .line 1387
    const/4 v0, 0x0

    .line 1388
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldqv;

    iget-object v0, v0, Ldqv;->c:Ljava/lang/String;

    const/4 v1, 0x1

    .line 1387
    invoke-static {p0, p1, v0, p3, v1}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 1399
    :goto_1
    return-object v0

    .line 1390
    :cond_2
    if-le v0, v5, :cond_3

    .line 1391
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/google/android/apps/plus/phone/PeopleListActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "android.intent.action.VIEW"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "account_id"

    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v2, "circle_actor_data"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    const-string v1, "com.google.android.libraries.social.notifications.notif_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "people_view_type"

    const/16 v2, 0xb

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    .line 1399
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static a(Landroid/content/Context;IILandroid/database/Cursor;I)Lffm;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1026
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1027
    const v0, 0x7f110068

    .line 1028
    invoke-virtual {v1, v0, p2}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v2

    .line 1029
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 1030
    new-instance v3, Landroid/app/Notification;

    .line 1031
    invoke-static {p4}, Lffl;->a(I)I

    move-result v0

    invoke-direct {v3, v0, v2, v4, v5}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    .line 1033
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 1034
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v4, "account_name"

    invoke-interface {v0, v4}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1037
    const v4, 0x7f110069

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v0, v5, v6

    .line 1039
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v7

    .line 1038
    invoke-virtual {v1, v4, p2, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1041
    invoke-static {p0, p1, p3}, Lffl;->c(Landroid/content/Context;ILandroid/database/Cursor;)Landroid/content/Intent;

    move-result-object v4

    .line 1042
    const/high16 v5, 0x14000000

    invoke-virtual {v4, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1043
    const-string v5, "com.google.android.libraries.social.notifications.FROM_ANDROID_NOTIFICATION"

    invoke-virtual {v4, v5, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1046
    invoke-static {}, Lfhu;->a()I

    move-result v5

    .line 1047
    invoke-static {p0, p1, v4}, Lfhu;->a(Landroid/content/Context;ILandroid/content/Intent;)Landroid/content/Intent;

    move-result-object v4

    .line 1046
    invoke-static {p0, v5, v4, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    .line 1048
    invoke-virtual {v3, p0, v2, v0, v4}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 1049
    const-string v0, "AST"

    invoke-static {p0, p1, p3, v0}, Lffl;->a(Landroid/content/Context;ILandroid/database/Cursor;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, v3, Landroid/app/Notification;->deleteIntent:Landroid/app/PendingIntent;

    .line 1052
    invoke-static {}, Llsj;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1053
    const v0, 0x7f0b01a1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, v3, Landroid/app/Notification;->color:I

    .line 1055
    :cond_0
    new-instance v0, Lffm;

    invoke-direct {v0, v3, v6}, Lffm;-><init>(Landroid/app/Notification;Z)V

    return-object v0
.end method

.method private static a(Landroid/content/Context;ILandroid/database/Cursor;I)Lffm;
    .locals 5

    .prologue
    .line 251
    sget v0, Lffl;->a:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lffl;->a:I

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 252
    const/4 v0, 0x0

    sput v0, Lffl;->a:I

    .line 255
    :cond_0
    sget-object v0, Lfvc;->h:Lfvc;

    invoke-virtual {v0}, Lfvc;->a()Ljava/lang/String;

    move-result-object v0

    .line 256
    const-string v1, "AndroidNotification"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 257
    sget v1, Lffl;->a:I

    .line 258
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x3b

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Debug mode="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Showing notification "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " of "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 257
    :cond_1
    const-string v1, "rich"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 262
    sget v0, Lffl;->a:I

    invoke-interface {p2, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 263
    invoke-static {p0, p1, p2, p3}, Lffl;->e(Landroid/content/Context;ILandroid/database/Cursor;I)Lffm;

    move-result-object v0

    .line 274
    :goto_0
    return-object v0

    .line 264
    :cond_2
    const-string v1, "single"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 265
    sget v0, Lffl;->a:I

    invoke-interface {p2, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 266
    invoke-static {p0, p1, p2, p3}, Lffl;->c(Landroid/content/Context;ILandroid/database/Cursor;I)Lffm;

    move-result-object v0

    goto :goto_0

    .line 267
    :cond_3
    const-string v1, "digest"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 268
    invoke-static {p0, p1, p2, p3}, Lffl;->d(Landroid/content/Context;ILandroid/database/Cursor;I)Lffm;

    move-result-object v0

    goto :goto_0

    .line 269
    :cond_4
    const-string v1, "coalesced"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 270
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-static {p0, p1, v0, p2, p3}, Lffl;->a(Landroid/content/Context;IILandroid/database/Cursor;I)Lffm;

    move-result-object v0

    goto :goto_0

    .line 274
    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;Ljava/util/List;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Ldqv;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1063
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1064
    if-eqz p1, :cond_2

    .line 1065
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldqv;

    .line 1066
    if-eqz v0, :cond_0

    .line 1068
    iget-object v1, v0, Ldqv;->d:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1069
    :try_start_0
    const-class v1, Lhso;

    invoke-static {p0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhso;

    .line 1072
    iget-object v0, v0, Ldqv;->d:Ljava/lang/String;

    .line 1073
    invoke-static {v0}, Lhst;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x2

    .line 1075
    invoke-static {}, Llsj;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 1072
    :goto_1
    invoke-interface {v1, v4, v5, v0}, Lhso;->a(Ljava/lang/String;II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 1077
    if-eqz v0, :cond_0

    .line 1078
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lkdn; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lkde; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 1080
    :catch_0
    move-exception v0

    .line 1081
    const-string v1, "AndroidNotification"

    const-string v4, "Cannot download square avatar"

    invoke-static {v1, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 1075
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 1082
    :catch_1
    move-exception v0

    .line 1083
    const-string v1, "AndroidNotification"

    const-string v4, "Canceled"

    invoke-static {v1, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 1089
    :cond_2
    return-object v2
.end method

.method private static a(Landroid/content/Context;Llun;I)Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Llun;",
            "I)",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 971
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 972
    const v3, 0x7f0d014f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    .line 973
    const v4, 0x7f0d0150

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v4, v2

    .line 976
    iget-object v2, p1, Llun;->a:Llut;

    if-eqz v2, :cond_4

    .line 977
    iget-object v2, p1, Llun;->a:Llut;

    iget-object v2, v2, Llut;->b:[Llup;

    move-object v8, v2

    .line 981
    :goto_0
    if-eqz v8, :cond_3

    array-length v2, v8

    if-eqz v2, :cond_3

    .line 982
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 983
    array-length v9, v8

    move v7, v1

    :goto_1
    if-ge v7, v9, :cond_2

    aget-object v0, v8, v7

    .line 984
    if-eqz v0, :cond_1

    iget-object v1, v0, Llup;->b:Lluo;

    if-eqz v1, :cond_1

    .line 985
    iget-object v0, v0, Llup;->b:Lluo;

    iget-object v0, v0, Lluo;->a:Ljava/lang/String;

    sget-object v1, Ljac;->a:Ljac;

    invoke-static {p0, v0, v1}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v1

    .line 990
    :try_start_0
    const-class v0, Lizs;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizs;

    .line 991
    const/4 v2, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lizs;->a(Lizu;IIII)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 993
    if-eqz v0, :cond_0

    .line 994
    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lkdn; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lkde; {:try_start_0 .. :try_end_0} :catch_1

    .line 1002
    :cond_0
    :goto_2
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    if-ge v0, p2, :cond_2

    .line 1003
    :cond_1
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_1

    .line 996
    :catch_0
    move-exception v0

    .line 997
    const-string v2, "AndroidNotification"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, 0x19

    invoke-direct {v5, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v10, "Could not download image "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .line 998
    :catch_1
    move-exception v0

    .line 999
    const-string v2, "AndroidNotification"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, 0x9

    invoke-direct {v5, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v10, "Canceled "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    :cond_2
    move-object v0, v6

    .line 1007
    :cond_3
    return-object v0

    :cond_4
    move-object v8, v0

    goto/16 :goto_0
.end method

.method private static a(Landroid/database/Cursor;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 352
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 353
    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    .line 354
    if-lez v1, :cond_2

    invoke-interface {p0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 356
    :cond_0
    const/16 v1, 0x1c

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    .line 357
    invoke-static {v1}, Lffl;->a([B)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 358
    const/16 v1, 0x9

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 360
    :cond_1
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 362
    :cond_2
    return-object v0
.end method

.method public static declared-synchronized a(Landroid/content/Context;I)V
    .locals 11

    .prologue
    const/4 v2, 0x3

    const/4 v3, 0x2

    const v1, 0x7f1000a2

    const/4 v0, 0x0

    const/4 v10, 0x1

    .line 150
    const-class v4, Lffl;

    monitor-enter v4

    :try_start_0
    invoke-static {p0, p1}, Leum;->a(Landroid/content/Context;I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v5

    if-nez v5, :cond_1

    .line 182
    :cond_0
    monitor-exit v4

    return-void

    .line 154
    :cond_1
    :try_start_1
    invoke-static {p0, p1}, Lffl;->b(Landroid/content/Context;I)Z

    move-result v5

    if-eqz v5, :cond_2

    move v3, v2

    .line 157
    :cond_2
    new-array v5, v3, [Lffm;

    .line 158
    invoke-static {}, Lffl;->a()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 159
    invoke-static {p0, p1}, Ldsf;->c(Landroid/content/Context;I)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v2

    if-eqz v2, :cond_4

    :try_start_2
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v6

    if-lez v6, :cond_3

    const/4 v6, 0x0

    const v7, 0x7f10009e

    invoke-static {p0, p1, v2, v7}, Lffl;->a(Landroid/content/Context;ILandroid/database/Cursor;I)Lffm;

    move-result-object v7

    aput-object v7, v5, v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_3
    :try_start_3
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_4
    const/4 v2, 0x1

    invoke-static {p0, p1, v2}, Ldsf;->b(Landroid/content/Context;IZ)Landroid/database/Cursor;

    move-result-object v2

    if-eqz v2, :cond_8

    invoke-static {p0, p1}, Lffl;->b(Landroid/content/Context;I)Z

    move-result v6

    if-eqz v6, :cond_9

    invoke-static {v2}, Lffl;->a(Landroid/database/Cursor;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    const/4 v2, 0x1

    const/4 v7, 0x0

    invoke-static {p0, p1, v2, v6, v7}, Ldsf;->a(Landroid/content/Context;IZLjava/util/List;Z)Landroid/database/Cursor;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v2

    if-eqz v2, :cond_6

    :try_start_4
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v7

    if-lez v7, :cond_5

    const/4 v7, 0x2

    const v8, 0x7f1000c9

    invoke-static {p0, p1, v2, v8}, Lffl;->a(Landroid/content/Context;ILandroid/database/Cursor;I)Lffm;

    move-result-object v8

    aput-object v8, v5, v7
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :cond_5
    :try_start_5
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_6
    const/4 v2, 0x1

    const/4 v7, 0x1

    invoke-static {p0, p1, v2, v6, v7}, Ldsf;->a(Landroid/content/Context;IZLjava/util/List;Z)Landroid/database/Cursor;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result-object v2

    if-eqz v2, :cond_8

    :try_start_6
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v6

    if-lez v6, :cond_7

    const/4 v6, 0x1

    const v7, 0x7f1000a2

    invoke-static {p0, p1, v2, v7}, Lffl;->a(Landroid/content/Context;ILandroid/database/Cursor;I)Lffm;

    move-result-object v7

    aput-object v7, v5, v6
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    :cond_7
    :try_start_7
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_8
    :goto_0
    move v2, v0

    .line 164
    :goto_1
    if-ge v2, v3, :cond_0

    .line 166
    if-nez v2, :cond_13

    .line 167
    const v0, 0x7f10009e

    .line 172
    :goto_2
    aget-object v6, v5, v2

    if-nez v6, :cond_15

    .line 174
    invoke-static {p0, p1, v0}, Lfhu;->a(Landroid/content/Context;II)V

    .line 164
    :goto_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 159
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 150
    :catchall_1
    move-exception v0

    monitor-exit v4

    throw v0

    .line 159
    :catchall_2
    move-exception v0

    :try_start_8
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    :catchall_3
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :cond_9
    :try_start_9
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v6

    if-lez v6, :cond_a

    const/4 v6, 0x1

    const v7, 0x7f1000a2

    invoke-static {p0, p1, v2, v7}, Lffl;->a(Landroid/content/Context;ILandroid/database/Cursor;I)Lffm;

    move-result-object v7

    aput-object v7, v5, v6
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    :cond_a
    :try_start_a
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_4
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    .line 161
    :cond_b
    invoke-static {p0, p1}, Ldsf;->c(Landroid/content/Context;I)Landroid/database/Cursor;
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    move-result-object v2

    if-eqz v2, :cond_c

    const/4 v6, 0x0

    const v7, 0x7f10009e

    :try_start_b
    invoke-static {p0, p1, v2, v7}, Lffl;->b(Landroid/content/Context;ILandroid/database/Cursor;I)Lffm;

    move-result-object v7

    aput-object v7, v5, v6
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_5

    :try_start_c
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_c
    const/4 v2, 0x0

    invoke-static {p0, p1, v2}, Ldsf;->b(Landroid/content/Context;IZ)Landroid/database/Cursor;

    move-result-object v2

    if-eqz v2, :cond_8

    invoke-static {p0, p1}, Lffl;->b(Landroid/content/Context;I)Z

    move-result v6

    if-eqz v6, :cond_12

    invoke-static {v2}, Lffl;->a(Landroid/database/Cursor;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    const/4 v2, 0x0

    const/4 v7, 0x0

    invoke-static {p0, p1, v2, v6, v7}, Ldsf;->a(Landroid/content/Context;IZLjava/util/List;Z)Landroid/database/Cursor;

    move-result-object v2

    if-eqz v2, :cond_e

    const-string v7, "AndroidNotification"

    const/4 v8, 0x3

    invoke-static {v7, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_d

    const-string v7, "moviesCursor size: "

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v9

    if-eqz v9, :cond_10

    invoke-virtual {v7, v8}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    :cond_d
    :goto_4
    const/4 v7, 0x2

    const v8, 0x7f1000c9

    :try_start_d
    invoke-static {p0, p1, v2, v8}, Lffl;->b(Landroid/content/Context;ILandroid/database/Cursor;I)Lffm;

    move-result-object v8

    aput-object v8, v5, v7
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_6

    :try_start_e
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_e
    const/4 v2, 0x0

    const/4 v7, 0x1

    invoke-static {p0, p1, v2, v6, v7}, Ldsf;->a(Landroid/content/Context;IZLjava/util/List;Z)Landroid/database/Cursor;

    move-result-object v2

    if-eqz v2, :cond_8

    const-string v6, "AndroidNotification"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_f

    const-string v6, "photosCursor size: "

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v8

    if-eqz v8, :cond_11

    invoke-virtual {v6, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    :cond_f
    :goto_5
    const/4 v6, 0x1

    const v7, 0x7f1000a2

    :try_start_f
    invoke-static {p0, p1, v2, v7}, Lffl;->b(Landroid/content/Context;ILandroid/database/Cursor;I)Lffm;

    move-result-object v7

    aput-object v7, v5, v6
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_7

    :try_start_10
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    :catchall_5
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_10
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v7}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_4

    :catchall_6
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_11
    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_5

    :catchall_7
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    :cond_12
    const/4 v6, 0x1

    const v7, 0x7f1000a2

    :try_start_11
    invoke-static {p0, p1, v2, v7}, Lffl;->b(Landroid/content/Context;ILandroid/database/Cursor;I)Lffm;

    move-result-object v7

    aput-object v7, v5, v6
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_8

    :try_start_12
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    :catchall_8
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    .line 169
    :cond_13
    if-ne v2, v10, :cond_14

    move v0, v1

    goto/16 :goto_2

    :cond_14
    const v0, 0x7f1000c9

    goto/16 :goto_2

    .line 178
    :cond_15
    aget-object v6, v5, v2

    iget-object v6, v6, Lffm;->a:Landroid/app/Notification;

    .line 179
    aget-object v7, v5, v2

    iget-boolean v7, v7, Lffm;->b:Z

    invoke-static {p0, p1, v6, v0, v7}, Lfhu;->a(Landroid/content/Context;ILandroid/app/Notification;IZ)V
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_1

    goto/16 :goto_3
.end method

.method private static a(Landroid/content/Context;Lbs;ILjava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lbs;",
            "I",
            "Ljava/util/List",
            "<",
            "Ldqv;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1552
    if-nez p3, :cond_1

    .line 1566
    :cond_0
    return-void

    .line 1556
    :cond_1
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 1557
    invoke-interface {v0, p2}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "account_name"

    .line 1558
    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1559
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldqv;

    .line 1560
    const-class v1, Linw;

    invoke-static {p0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Linw;

    iget-object v0, v0, Ldqv;->c:Ljava/lang/String;

    .line 1561
    invoke-interface {v1, v2, v0}, Linw;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1562
    if-eqz v0, :cond_2

    .line 1563
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lbs;->b(Ljava/lang/String;)Lbs;

    goto :goto_0
.end method

.method public static a()Z
    .locals 1

    .prologue
    .line 1490
    sget-object v0, Lfvc;->h:Lfvc;

    invoke-virtual {v0}, Lfvc;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;)Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1511
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    new-array v3, v1, [Ljava/lang/String;

    const-string v4, "logged_in"

    aput-object v4, v3, v2

    .line 1512
    invoke-interface {v0, v3}, Lhei;->a([Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method private static a([B)Z
    .locals 4

    .prologue
    const/4 v3, 0x6

    const/4 v1, 0x0

    .line 1569
    if-eqz p0, :cond_1

    .line 1571
    :try_start_0
    new-instance v0, Llva;

    invoke-direct {v0}, Llva;-><init>()V

    .line 1572
    invoke-static {v0, p0}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Llva;

    .line 1573
    sget-object v2, Llwf;->a:Loxr;

    invoke-virtual {v0, v2}, Llva;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llwf;

    .line 1575
    if-eqz v0, :cond_1

    .line 1577
    iget-object v2, v0, Llwf;->b:Llwh;

    if-eqz v2, :cond_2

    .line 1578
    iget-object v0, v0, Llwf;->b:Llwh;

    iget v0, v0, Llwh;->a:I
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    .line 1580
    :goto_0
    const/4 v2, 0x5

    if-eq v0, v2, :cond_0

    if-ne v0, v3, :cond_1

    .line 1581
    :cond_0
    const/4 v0, 0x1

    .line 1590
    :goto_1
    return v0

    .line 1584
    :catch_0
    move-exception v0

    .line 1585
    const-string v2, "AndroidNotification"

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1586
    const-string v2, "AndroidNotification"

    const-string v3, "Unable to parse AppPayload proto"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_1
    move v0, v1

    .line 1590
    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method private static b(I)I
    .locals 1

    .prologue
    .line 1521
    const v0, 0x7f1000a2

    if-ne p0, v0, :cond_0

    const v0, 0x7f020368

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f02059d

    goto :goto_0
.end method

.method private static b(Landroid/content/Context;ILandroid/database/Cursor;)Lda;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1108
    .line 1109
    invoke-static {p0}, Lda;->a(Landroid/content/Context;)Lda;

    move-result-object v0

    invoke-static {p0, p1, p2, v0, v3}, Lffl;->a(Landroid/content/Context;ILandroid/database/Cursor;Lda;Z)Landroid/content/Intent;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    .line 1110
    :cond_0
    if-eqz v0, :cond_1

    .line 1112
    invoke-virtual {v0}, Lda;->a()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Lda;->a(I)Landroid/content/Intent;

    move-result-object v1

    .line 1113
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1126
    :goto_0
    return-object v0

    .line 1120
    :cond_1
    invoke-static {p0, p1, p2}, Lffl;->c(Landroid/content/Context;ILandroid/database/Cursor;)Landroid/content/Intent;

    move-result-object v1

    .line 1121
    const-string v0, "com.google.android.libraries.social.notifications.FROM_ANDROID_NOTIFICATION"

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1123
    invoke-static {p0}, Lda;->a(Landroid/content/Context;)Lda;

    move-result-object v0

    .line 1124
    invoke-static {p0, p1, v1}, Lfhu;->a(Landroid/content/Context;ILandroid/content/Intent;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lda;->a(Landroid/content/Intent;)Lda;

    goto :goto_0
.end method

.method private static b(Landroid/content/Context;ILandroid/database/Cursor;I)Lffm;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 370
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 371
    if-lez v0, :cond_3

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 372
    if-ne v0, v2, :cond_1

    .line 373
    invoke-static {}, Lffl;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 374
    invoke-static {p0, p1, p2, p3}, Lffl;->e(Landroid/content/Context;ILandroid/database/Cursor;I)Lffm;

    move-result-object v0

    .line 388
    :goto_0
    return-object v0

    .line 376
    :cond_0
    invoke-static {p0, p1, p2, p3}, Lffl;->c(Landroid/content/Context;ILandroid/database/Cursor;I)Lffm;

    move-result-object v0

    goto :goto_0

    .line 378
    :cond_1
    if-le v0, v2, :cond_3

    .line 379
    invoke-static {}, Lffl;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 380
    invoke-static {p0, p1, p2, p3}, Lffl;->d(Landroid/content/Context;ILandroid/database/Cursor;I)Lffm;

    move-result-object v0

    goto :goto_0

    .line 383
    :cond_2
    invoke-static {p0, p1, v0, p2, p3}, Lffl;->a(Landroid/content/Context;IILandroid/database/Cursor;I)Lffm;

    move-result-object v0

    goto :goto_0

    .line 388
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Landroid/database/Cursor;)Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1594
    const/16 v0, 0x1d

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 1595
    if-eqz v0, :cond_0

    .line 1596
    :try_start_0
    new-instance v1, Llua;

    invoke-direct {v1}, Llua;-><init>()V

    invoke-static {v1, v0}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Llua;

    .line 1599
    iget-object v1, v0, Llua;->a:[Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1600
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, v0, Llua;->a:[Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 1607
    :goto_0
    return-object v0

    .line 1602
    :catch_0
    move-exception v0

    .line 1604
    const-string v1, "AndroidNotification"

    const-string v2, "Unable to parse AnalytisData proto"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1607
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    goto :goto_0
.end method

.method private static b()Z
    .locals 2

    .prologue
    .line 135
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Landroid/content/Context;I)Z
    .locals 2

    .prologue
    .line 142
    const-class v0, Lieh;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    .line 143
    sget-object v1, Ldxd;->D:Lief;

    invoke-interface {v0, v1, p1}, Lieh;->b(Lief;I)Z

    move-result v0

    return v0
.end method

.method private static b(Landroid/content/Context;II)Z
    .locals 2

    .prologue
    .line 948
    const-class v0, Lieh;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    .line 949
    sget-object v1, Ldxd;->A:Lief;

    invoke-interface {v0, v1, p1}, Lieh;->b(Lief;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x12

    if-ne p2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(Landroid/content/Context;ILandroid/database/Cursor;)Landroid/content/Intent;
    .locals 6

    .prologue
    .line 1132
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1133
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1135
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1136
    if-eqz p2, :cond_1

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1138
    :cond_0
    const/16 v3, 0xc

    invoke-interface {p2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 1140
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1141
    invoke-static {p2}, Lffl;->b(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v3

    .line 1142
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v5, 0x5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 1144
    const/4 v5, 0x0

    invoke-virtual {v3, v5, v4}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1146
    const/16 v3, 0x9

    invoke-interface {p2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1148
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1149
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1152
    :cond_1
    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/google/android/apps/plus/phone/HomeActivity;

    invoke-direct {v3, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v4, "android.intent.action.VIEW"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "account_id"

    invoke-virtual {v3, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v4, "com.google.android.libraries.social.notifications.show_notifications"

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "com.google.android.libraries.social.notifications.notif_types"

    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putIntegerArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    :cond_2
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "com.google.android.libraries.social.notifications.coalescing_codes"

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    :cond_3
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "com.google.android.libraries.social.notifications.ext_ids"

    invoke-virtual {v3, v0, v2}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    :cond_4
    return-object v3
.end method

.method private static c(Landroid/content/Context;ILandroid/database/Cursor;I)Lffm;
    .locals 10

    .prologue
    .line 396
    const/16 v0, 0xd

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 397
    const/16 v1, 0xc

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 398
    const/4 v1, 0x3

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 399
    const/4 v1, 0x1

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 400
    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "\n"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 401
    const/16 v5, 0x1c

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v5

    .line 402
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 404
    invoke-static {p0, p1, p2}, Lffl;->b(Landroid/content/Context;ILandroid/database/Cursor;)Lda;

    move-result-object v7

    .line 407
    const/16 v8, 0x8

    if-ne v0, v8, :cond_0

    .line 408
    const/4 v0, 0x0

    .line 447
    :goto_0
    return-object v0

    .line 412
    :cond_0
    const/4 v0, 0x6

    if-eq v2, v0, :cond_1

    const/16 v0, 0x27

    if-ne v2, v0, :cond_4

    .line 414
    :cond_1
    const v0, 0x7f020362

    .line 425
    :goto_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 426
    new-instance v5, Landroid/app/Notification;

    invoke-direct {v5, v0, v4, v8, v9}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    .line 428
    invoke-static {}, Lfhu;->a()I

    move-result v0

    const/high16 v4, 0x8000000

    .line 427
    invoke-virtual {v7, v0, v4}, Lda;->b(II)Landroid/app/PendingIntent;

    move-result-object v4

    .line 431
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 432
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v7, "account_name"

    .line 433
    invoke-interface {v0, v7}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 434
    invoke-static {p0}, Lffl;->a(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 435
    :goto_2
    invoke-virtual {v5, p0, v3, v0, v4}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 437
    const-string v0, "AST"

    invoke-static {p0, p1, p2, v0}, Lffl;->a(Landroid/content/Context;ILandroid/database/Cursor;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v0

    .line 439
    if-eqz v0, :cond_2

    .line 440
    iput-object v0, v5, Landroid/app/Notification;->deleteIntent:Landroid/app/PendingIntent;

    .line 443
    :cond_2
    invoke-static {}, Llsj;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 444
    const v0, 0x7f0b01a1

    invoke-virtual {v6, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, v5, Landroid/app/Notification;->color:I

    .line 446
    :cond_3
    new-instance v0, Lffm;

    .line 447
    invoke-static {p0, p1, v2}, Lffl;->b(Landroid/content/Context;II)Z

    move-result v1

    invoke-direct {v0, v5, v1}, Lffm;-><init>(Landroid/app/Notification;Z)V

    goto :goto_0

    .line 415
    :cond_4
    const/16 v0, 0x12

    if-ne v2, v0, :cond_5

    .line 416
    const v0, 0x7f020365

    goto :goto_1

    .line 417
    :cond_5
    const/16 v0, 0x61

    if-eq v2, v0, :cond_6

    const/16 v0, 0x6f

    if-eq v2, v0, :cond_6

    .line 419
    invoke-static {v5}, Lffl;->a([B)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 420
    :cond_6
    const v0, 0x7f020524

    goto :goto_1

    .line 422
    :cond_7
    invoke-static {p3}, Lffl;->a(I)I

    move-result v0

    goto :goto_1

    :cond_8
    move-object v0, v1

    .line 434
    goto :goto_2
.end method

.method private static d(Landroid/content/Context;ILandroid/database/Cursor;I)Lffm;
    .locals 20
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 459
    :cond_0
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_1

    const/16 v4, 0xd

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    const/16 v5, 0x8

    if-eq v4, v5, :cond_0

    const/4 v4, 0x0

    :goto_0
    if-eqz v4, :cond_2

    .line 460
    const/4 v4, 0x0

    .line 565
    :goto_1
    return-object v4

    .line 459
    :cond_1
    const/4 v4, 0x1

    goto :goto_0

    .line 463
    :cond_2
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-nez v4, :cond_3

    .line 464
    const/4 v4, 0x0

    goto :goto_1

    .line 466
    :cond_3
    const-wide v10, 0x7fffffffffffffffL

    .line 467
    const/4 v8, 0x0

    .line 468
    new-instance v18, Lbu;

    invoke-direct/range {v18 .. v18}, Lbu;-><init>()V

    .line 469
    const/4 v6, 0x0

    .line 470
    const/4 v7, 0x1

    .line 471
    const/4 v13, 0x0

    .line 472
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 473
    const-class v4, Ldhu;

    move-object/from16 v0, p0

    invoke-static {v0, v4}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ldhu;

    .line 475
    :goto_2
    const/16 v5, 0xd

    move-object/from16 v0, p2

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 476
    const/16 v5, 0x8

    if-eq v9, v5, :cond_b

    .line 477
    const/4 v5, 0x3

    .line 478
    move-object/from16 v0, p2

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 479
    const/4 v12, 0x1

    .line 480
    move-object/from16 v0, p2

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 481
    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v15

    add-int/lit8 v15, v15, 0x2

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v16

    add-int v15, v15, v16

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v14, ": "

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 482
    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Lbu;->c(Ljava/lang/CharSequence;)Lbu;

    .line 483
    if-nez v6, :cond_a

    move-object v12, v5

    .line 488
    :goto_3
    const/4 v5, 0x6

    .line 489
    move-object/from16 v0, p2

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 490
    const-wide/16 v16, 0x3e8

    div-long v14, v14, v16

    invoke-static {v10, v11, v14, v15}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v16

    .line 491
    add-int/lit8 v15, v8, 0x1

    .line 492
    const/16 v5, 0xc

    move-object/from16 v0, p2

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-static {v0, v1, v5}, Lffl;->b(Landroid/content/Context;II)Z

    move-result v5

    and-int v14, v7, v5

    .line 494
    const/16 v5, 0xf

    move-object/from16 v0, p2

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v5

    .line 495
    if-eqz v5, :cond_4

    .line 496
    invoke-static {v5}, Ldqu;->a([B)Ljava/util/List;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-interface {v0, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 499
    :cond_4
    const/16 v5, 0xd

    if-ne v9, v5, :cond_d

    .line 501
    const/4 v5, 0x2

    move-object/from16 v0, p2

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 503
    if-eqz v4, :cond_d

    const/4 v5, 0x3

    if-ge v13, v5, :cond_d

    .line 504
    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x1

    const/4 v11, 0x0

    move-object/from16 v5, p0

    move/from16 v6, p1

    invoke-virtual/range {v4 .. v11}, Ldhu;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 507
    add-int/lit8 v5, v13, 0x1

    move v6, v14

    move v7, v15

    move-wide/from16 v8, v16

    .line 511
    :goto_4
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v10

    if-nez v10, :cond_c

    .line 513
    if-nez v7, :cond_5

    .line 514
    const/4 v4, 0x0

    goto/16 :goto_1

    .line 516
    :cond_5
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 517
    const v4, 0x7f110068

    .line 518
    invoke-virtual {v5, v4, v7}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v4

    .line 521
    invoke-static/range {p0 .. p2}, Lffl;->c(Landroid/content/Context;ILandroid/database/Cursor;)Landroid/content/Intent;

    move-result-object v10

    .line 522
    const/high16 v11, 0x14000000

    invoke-virtual {v10, v11}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 523
    const-string v11, "com.google.android.libraries.social.notifications.FROM_ANDROID_NOTIFICATION"

    const/4 v13, 0x1

    invoke-virtual {v10, v11, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 525
    new-instance v11, Lbs;

    move-object/from16 v0, p0

    invoke-direct {v11, v0}, Lbs;-><init>(Landroid/content/Context;)V

    .line 527
    invoke-static {}, Lfhu;->a()I

    move-result v13

    .line 528
    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-static {v0, v1, v10}, Lfhu;->a(Landroid/content/Context;ILandroid/content/Intent;)Landroid/content/Intent;

    move-result-object v10

    const/4 v14, 0x0

    .line 527
    move-object/from16 v0, p0

    invoke-static {v0, v13, v10, v14}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v10

    .line 529
    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Lbu;->a(Ljava/lang/CharSequence;)Lbu;

    .line 530
    invoke-virtual {v11, v4}, Lbs;->e(Ljava/lang/CharSequence;)Lbs;

    move-result-object v13

    .line 531
    invoke-virtual {v13, v4}, Lbs;->a(Ljava/lang/CharSequence;)Lbs;

    move-result-object v4

    .line 532
    invoke-virtual {v4, v8, v9}, Lbs;->a(J)Lbs;

    move-result-object v4

    .line 533
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, p1

    invoke-static {v0, v1, v2}, Lffl;->a(Landroid/content/Context;Landroid/database/Cursor;I)I

    move-result v8

    invoke-virtual {v4, v8}, Lbs;->c(I)Lbs;

    move-result-object v4

    .line 534
    invoke-virtual {v4, v7}, Lbs;->b(I)Lbs;

    move-result-object v4

    .line 535
    invoke-virtual {v4, v10}, Lbs;->a(Landroid/app/PendingIntent;)Lbs;

    move-result-object v4

    const-string v8, "AST"

    .line 536
    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, p2

    invoke-static {v0, v1, v2, v8}, Lffl;->a(Landroid/content/Context;ILandroid/database/Cursor;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v8

    invoke-virtual {v4, v8}, Lbs;->b(Landroid/app/PendingIntent;)Lbs;

    move-result-object v4

    .line 538
    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Lbs;->a(Lce;)Lbs;

    .line 540
    invoke-static {}, Llsj;->a()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 541
    invoke-static/range {p3 .. p3}, Lffl;->b(I)I

    move-result v4

    invoke-virtual {v11, v4}, Lbs;->a(I)Lbs;

    .line 548
    :goto_5
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 549
    invoke-virtual {v11, v12}, Lbs;->b(Ljava/lang/CharSequence;)Lbs;

    .line 552
    :cond_6
    invoke-static/range {p0 .. p0}, Lffl;->a(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 553
    const-class v4, Lhei;

    move-object/from16 v0, p0

    invoke-static {v0, v4}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lhei;

    .line 554
    move/from16 v0, p1

    invoke-interface {v4, v0}, Lhei;->a(I)Lhej;

    move-result-object v4

    const-string v8, "account_name"

    invoke-interface {v4, v8}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 555
    invoke-virtual {v11, v4}, Lbs;->c(Ljava/lang/CharSequence;)Lbs;

    .line 557
    :cond_7
    invoke-static {}, Llsj;->a()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 558
    const/4 v4, 0x0

    invoke-virtual {v11, v4}, Lbs;->e(I)Lbs;

    move-result-object v4

    .line 559
    move-object/from16 v0, p0

    move/from16 v1, p3

    invoke-static {v0, v1, v7}, Lffl;->a(Landroid/content/Context;II)Landroid/app/Notification;

    move-result-object v7

    invoke-virtual {v4, v7}, Lbs;->a(Landroid/app/Notification;)Lbs;

    move-result-object v4

    const-string v7, "social"

    .line 560
    invoke-virtual {v4, v7}, Lbs;->a(Ljava/lang/String;)Lbs;

    move-result-object v4

    const v7, 0x7f0b01a1

    .line 561
    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v4, v5}, Lbs;->d(I)Lbs;

    .line 562
    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, v19

    invoke-static {v0, v11, v1, v2}, Lffl;->a(Landroid/content/Context;Lbs;ILjava/util/List;)V

    .line 565
    :cond_8
    new-instance v4, Lffm;

    invoke-virtual {v11}, Lbs;->c()Landroid/app/Notification;

    move-result-object v5

    invoke-direct {v4, v5, v6}, Lffm;-><init>(Landroid/app/Notification;Z)V

    goto/16 :goto_1

    .line 543
    :cond_9
    invoke-static/range {p3 .. p3}, Lffl;->a(I)I

    move-result v4

    invoke-virtual {v11, v4}, Lbs;->a(I)Lbs;

    move-result-object v4

    .line 545
    invoke-static/range {p3 .. p3}, Lffl;->b(I)I

    move-result v8

    .line 544
    invoke-static {v5, v8}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v8

    invoke-virtual {v4, v8}, Lbs;->a(Landroid/graphics/Bitmap;)Lbs;

    goto :goto_5

    :cond_a
    move-object v12, v6

    goto/16 :goto_3

    :cond_b
    move v5, v13

    move-object v12, v6

    move v6, v7

    move v7, v8

    move-wide v8, v10

    goto/16 :goto_4

    :cond_c
    move v13, v5

    move-wide v10, v8

    move v8, v7

    move v7, v6

    move-object v6, v12

    goto/16 :goto_2

    :cond_d
    move v5, v13

    move v6, v14

    move v7, v15

    move-wide/from16 v8, v16

    goto/16 :goto_4
.end method

.method private static e(Landroid/content/Context;ILandroid/database/Cursor;I)Lffm;
    .locals 43
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 575
    const/16 v4, 0xd

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 579
    const/16 v4, 0x8

    if-ne v8, v4, :cond_0

    .line 580
    const/4 v4, 0x0

    .line 922
    :goto_0
    return-object v4

    .line 583
    :cond_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v36

    .line 585
    const/16 v4, 0xc

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 586
    const/16 v4, 0x9

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 587
    const/16 v4, 0x14

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 590
    const/4 v4, 0x3

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v37

    .line 591
    const/4 v4, 0x1

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v38

    .line 592
    const/4 v4, 0x4

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v31

    .line 593
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static/range {v37 .. v37}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-static/range {v38 .. v38}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    move-object/from16 v0, v37

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v38

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    .line 594
    const/4 v4, 0x6

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const-wide/16 v12, 0x3e8

    div-long v40, v4, v12

    .line 596
    const/16 v4, 0x10

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 598
    const/16 v20, 0x0

    .line 599
    const/16 v19, 0x0

    .line 600
    const/16 v18, 0x0

    .line 602
    const/16 v17, 0x0

    .line 603
    const/16 v16, 0x0

    .line 604
    const/4 v13, 0x0

    .line 605
    const/4 v12, -0x1

    .line 607
    const/16 v30, 0x0

    .line 608
    const/16 v29, 0x0

    .line 609
    const/16 v28, 0x0

    .line 612
    invoke-static/range {p3 .. p3}, Lffl;->a(I)I

    move-result v15

    .line 614
    const/16 v4, 0xf

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v4

    .line 616
    if-eqz v4, :cond_d

    .line 617
    invoke-static {v4}, Ldqu;->a([B)Ljava/util/List;

    move-result-object v4

    move-object/from16 v26, v4

    .line 621
    :goto_1
    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-static {v0, v1}, Lffl;->a(Landroid/content/Context;Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    .line 622
    invoke-static {v4}, Lhss;->a(Ljava/util/List;)Landroid/graphics/Bitmap;

    move-result-object v22

    .line 624
    const/16 v4, 0x16

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v4

    .line 625
    const/4 v5, 0x0

    .line 627
    :try_start_0
    new-instance v14, Llun;

    invoke-direct {v14}, Llun;-><init>()V

    invoke-static {v14, v4}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v4

    check-cast v4, Llun;
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    move-object/from16 v27, v4

    .line 633
    :goto_2
    const/4 v14, 0x0

    .line 634
    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-static {v0, v1, v6}, Llap;->d(Landroid/content/Context;ILjava/lang/String;)Z

    move-result v21

    .line 637
    sparse-switch v8, :sswitch_data_0

    :cond_1
    move-object v4, v14

    move v5, v15

    move/from16 v32, v12

    move-object/from16 v33, v13

    move-object/from16 v34, v16

    move/from16 v35, v17

    .line 677
    :goto_3
    sparse-switch v9, :sswitch_data_1

    :cond_2
    move v4, v5

    move-object/from16 v6, v28

    move-object/from16 v7, v29

    move-object/from16 v8, v30

    move-object/from16 v5, v22

    .line 812
    :goto_4
    invoke-static/range {p0 .. p2}, Lffl;->b(Landroid/content/Context;ILandroid/database/Cursor;)Lda;

    move-result-object v10

    .line 813
    invoke-static {}, Lfhu;->a()I

    move-result v11

    const/high16 v12, 0x8000000

    invoke-virtual {v10, v11, v12}, Lda;->b(II)Landroid/app/PendingIntent;

    move-result-object v10

    .line 816
    new-instance v11, Lbs;

    move-object/from16 v0, p0

    invoke-direct {v11, v0}, Lbs;-><init>(Landroid/content/Context;)V

    .line 817
    move-object/from16 v0, v39

    invoke-virtual {v11, v0}, Lbs;->e(Ljava/lang/CharSequence;)Lbs;

    move-result-object v12

    .line 818
    move-object/from16 v0, v37

    invoke-virtual {v12, v0}, Lbs;->a(Ljava/lang/CharSequence;)Lbs;

    move-result-object v12

    .line 819
    move-object/from16 v0, v38

    invoke-virtual {v12, v0}, Lbs;->b(Ljava/lang/CharSequence;)Lbs;

    move-result-object v12

    .line 820
    move-wide/from16 v0, v40

    invoke-virtual {v12, v0, v1}, Lbs;->a(J)Lbs;

    move-result-object v12

    .line 821
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, p1

    invoke-static {v0, v1, v2}, Lffl;->a(Landroid/content/Context;Landroid/database/Cursor;I)I

    move-result v13

    invoke-virtual {v12, v13}, Lbs;->c(I)Lbs;

    move-result-object v12

    .line 822
    invoke-virtual {v12, v4}, Lbs;->a(I)Lbs;

    move-result-object v4

    .line 823
    invoke-virtual {v4, v10}, Lbs;->a(Landroid/app/PendingIntent;)Lbs;

    move-result-object v4

    const-string v10, "AST"

    .line 824
    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, p2

    invoke-static {v0, v1, v2, v10}, Lffl;->a(Landroid/content/Context;ILandroid/database/Cursor;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v10

    invoke-virtual {v4, v10}, Lbs;->b(Landroid/app/PendingIntent;)Lbs;

    .line 827
    if-eqz v8, :cond_19

    .line 828
    new-instance v4, Lbq;

    invoke-direct {v4}, Lbq;-><init>()V

    .line 829
    invoke-virtual {v4, v8}, Lbq;->a(Landroid/graphics/Bitmap;)Lbq;

    .line 830
    if-eqz v7, :cond_3

    .line 831
    invoke-virtual {v4, v7}, Lbq;->b(Landroid/graphics/Bitmap;)Lbq;

    .line 833
    :cond_3
    invoke-static/range {p0 .. p0}, Lffl;->a(Landroid/content/Context;)Z

    move-result v7

    if-nez v7, :cond_4

    .line 838
    if-eqz v6, :cond_18

    .line 839
    invoke-virtual {v4, v6}, Lbq;->a(Ljava/lang/CharSequence;)Lbq;

    .line 844
    :cond_4
    :goto_5
    invoke-virtual {v11, v4}, Lbs;->a(Lce;)Lbs;

    .line 875
    :cond_5
    :goto_6
    if-eqz v5, :cond_6

    .line 876
    invoke-virtual {v11, v5}, Lbs;->a(Landroid/graphics/Bitmap;)Lbs;

    .line 879
    :cond_6
    invoke-static/range {p0 .. p1}, Ldhv;->a(Landroid/content/Context;I)Z

    move-result v4

    if-nez v4, :cond_9

    .line 880
    if-eqz v20, :cond_7

    .line 881
    new-instance v4, Lbp;

    move/from16 v0, v20

    move-object/from16 v1, v19

    move-object/from16 v2, v18

    invoke-direct {v4, v0, v1, v2}, Lbp;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 882
    invoke-virtual {v4}, Lbp;->a()Lbn;

    move-result-object v4

    .line 883
    new-instance v5, Lcf;

    invoke-direct {v5}, Lcf;-><init>()V

    invoke-virtual {v5, v4}, Lcf;->a(Lbn;)Lcf;

    move-result-object v4

    invoke-virtual {v11, v4}, Lbs;->a(Lbt;)Lbs;

    .line 886
    move/from16 v0, v20

    move-object/from16 v1, v19

    move-object/from16 v2, v18

    invoke-virtual {v11, v0, v1, v2}, Lbs;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Lbs;

    .line 891
    :cond_7
    if-eqz v35, :cond_9

    .line 892
    const/4 v4, 0x1

    move/from16 v0, v32

    if-eq v0, v4, :cond_8

    .line 894
    new-instance v4, Lbp;

    move/from16 v0, v35

    move-object/from16 v1, v34

    move-object/from16 v2, v33

    invoke-direct {v4, v0, v1, v2}, Lbp;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 895
    invoke-virtual {v4}, Lbp;->a()Lbn;

    move-result-object v4

    .line 896
    new-instance v5, Lcf;

    invoke-direct {v5}, Lcf;-><init>()V

    invoke-virtual {v5, v4}, Lcf;->a(Lbn;)Lcf;

    move-result-object v4

    invoke-virtual {v11, v4}, Lbs;->a(Lbt;)Lbs;

    .line 899
    :cond_8
    move/from16 v0, v35

    move-object/from16 v1, v34

    move-object/from16 v2, v33

    invoke-virtual {v11, v0, v1, v2}, Lbs;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Lbs;

    .line 905
    :cond_9
    invoke-static/range {p0 .. p0}, Lffl;->a(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 906
    const-class v4, Lhei;

    move-object/from16 v0, p0

    invoke-static {v0, v4}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lhei;

    .line 907
    move/from16 v0, p1

    invoke-interface {v4, v0}, Lhei;->a(I)Lhej;

    move-result-object v4

    const-string v5, "account_name"

    invoke-interface {v4, v5}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 908
    invoke-virtual {v11, v4}, Lbs;->c(Ljava/lang/CharSequence;)Lbs;

    .line 911
    :cond_a
    invoke-static {}, Llsj;->a()Z

    move-result v4

    if-eqz v4, :cond_b

    .line 912
    const/4 v4, 0x0

    invoke-virtual {v11, v4}, Lbs;->e(I)Lbs;

    move-result-object v4

    const/4 v5, 0x1

    .line 913
    move-object/from16 v0, p0

    move/from16 v1, p3

    invoke-static {v0, v1, v5}, Lffl;->a(Landroid/content/Context;II)Landroid/app/Notification;

    move-result-object v5

    invoke-virtual {v4, v5}, Lbs;->a(Landroid/app/Notification;)Lbs;

    move-result-object v4

    const-string v5, "social"

    .line 914
    invoke-virtual {v4, v5}, Lbs;->a(Ljava/lang/String;)Lbs;

    .line 915
    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, v26

    invoke-static {v0, v11, v1, v2}, Lffl;->a(Landroid/content/Context;Lbs;ILjava/util/List;)V

    .line 917
    :cond_b
    invoke-static {}, Llsj;->a()Z

    move-result v4

    if-eqz v4, :cond_c

    .line 918
    const v4, 0x7f0b01a1

    move-object/from16 v0, v36

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v11, v4}, Lbs;->d(I)Lbs;

    .line 921
    :cond_c
    new-instance v4, Lffm;

    invoke-virtual {v11}, Lbs;->c()Landroid/app/Notification;

    move-result-object v5

    .line 922
    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-static {v0, v1, v9}, Lffl;->b(Landroid/content/Context;II)Z

    move-result v6

    invoke-direct {v4, v5, v6}, Lffm;-><init>(Landroid/app/Notification;Z)V

    goto/16 :goto_0

    .line 619
    :cond_d
    const/4 v4, 0x0

    move-object/from16 v26, v4

    goto/16 :goto_1

    .line 628
    :catch_0
    move-exception v4

    .line 630
    const-string v14, "AndroidNotification"

    const-string v21, "Unable to parse ExpandedInfo proto"

    move-object/from16 v0, v21

    invoke-static {v14, v0, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object/from16 v27, v5

    goto/16 :goto_2

    .line 639
    :sswitch_0
    const v5, 0x7f020198

    move-object v4, v14

    move/from16 v32, v12

    move-object/from16 v33, v13

    move-object/from16 v34, v16

    move/from16 v35, v17

    .line 640
    goto/16 :goto_3

    .line 645
    :sswitch_1
    const/4 v4, 0x3

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-static {v0, v1, v4}, Lffl;->a(Landroid/content/Context;Llun;I)Ljava/util/List;

    move-result-object v4

    .line 648
    const v5, 0x7f0d014f

    move-object/from16 v0, v36

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    .line 649
    const v8, 0x7f0d0150

    move-object/from16 v0, v36

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    float-to-int v8, v8

    .line 650
    invoke-static {v4, v5, v8}, Lfus;->a(Ljava/util/List;II)Landroid/graphics/Bitmap;

    move-result-object v30

    .line 652
    const v5, 0x7f020368

    .line 653
    const v8, 0x7f020368

    move-object/from16 v0, v36

    invoke-static {v0, v8}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v22

    move/from16 v32, v12

    move-object/from16 v33, v13

    move-object/from16 v34, v16

    move/from16 v35, v17

    .line 654
    goto/16 :goto_3

    .line 659
    :sswitch_2
    if-nez v21, :cond_1

    .line 660
    const v17, 0x7f02059c

    .line 661
    const v4, 0x7f0a06a4

    move-object/from16 v0, v36

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 663
    const/4 v12, 0x1

    const/4 v13, 0x1

    move-object/from16 v4, p0

    move/from16 v5, p1

    invoke-static/range {v4 .. v13}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;IIJZZ)Landroid/content/Intent;

    move-result-object v4

    if-eqz v4, :cond_e

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v5

    if-eqz v5, :cond_e

    const-string v5, "com.google.android.libraries.social.notifications.FROM_ANDROID_NOTIFICATION"

    const/4 v8, 0x1

    invoke-virtual {v4, v5, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {}, Lfhu;->a()I

    move-result v5

    const/4 v8, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v5, v4, v8}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    .line 666
    :goto_7
    const/4 v5, 0x1

    move/from16 v32, v5

    move-object/from16 v33, v4

    move-object/from16 v34, v16

    move/from16 v35, v17

    move-object v4, v14

    move v5, v15

    goto/16 :goto_3

    .line 663
    :cond_e
    const/4 v4, 0x0

    goto :goto_7

    .line 672
    :sswitch_3
    const v5, 0x7f02036d

    move-object v4, v14

    move/from16 v32, v12

    move-object/from16 v33, v13

    move-object/from16 v34, v16

    move/from16 v35, v17

    goto/16 :goto_3

    .line 680
    :sswitch_4
    const v8, 0x7f02059b

    .line 682
    if-eqz v26, :cond_22

    invoke-interface/range {v26 .. v26}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_22

    .line 683
    const/16 v4, 0x27

    if-ne v9, v4, :cond_f

    const/4 v4, 0x1

    .line 685
    :goto_8
    if-eqz v4, :cond_21

    invoke-interface/range {v26 .. v26}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_21

    .line 687
    const/4 v4, 0x0

    move-object/from16 v0, v26

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ldqv;

    .line 688
    if-eqz v4, :cond_10

    const/4 v5, 0x1

    :goto_9
    iget-object v6, v4, Ldqv;->c:Ljava/lang/String;

    if-eqz v6, :cond_11

    const/4 v6, 0x1

    :goto_a
    and-int/2addr v5, v6

    if-eqz v5, :cond_21

    .line 689
    const v7, 0x7f02059c

    .line 690
    const v5, 0x7f0a06a6

    move-object/from16 v0, v36

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 693
    if-nez v4, :cond_12

    const/4 v4, 0x0

    :goto_b
    const/4 v5, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-static {v0, v1, v5, v4}, Leyq;->a(Landroid/content/Context;ILjava/util/ArrayList;Lhgw;)Landroid/content/Intent;

    move-result-object v4

    invoke-static {}, Lfhu;->a()I

    move-result v5

    const/4 v10, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v5, v4, v10}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    move-object v5, v6

    move v6, v7

    :goto_c
    move-object/from16 v7, v29

    move-object/from16 v18, v4

    move-object/from16 v19, v5

    move/from16 v20, v6

    move-object/from16 v5, v22

    move-object/from16 v6, v28

    move v4, v8

    move-object/from16 v8, v30

    .line 697
    goto/16 :goto_4

    .line 683
    :cond_f
    const/4 v4, 0x0

    goto :goto_8

    .line 688
    :cond_10
    const/4 v5, 0x0

    goto :goto_9

    :cond_11
    const/4 v6, 0x0

    goto :goto_a

    .line 693
    :cond_12
    new-instance v5, Lhgw;

    new-instance v10, Ljqs;

    iget-object v11, v4, Ldqv;->c:Ljava/lang/String;

    iget-object v4, v4, Ldqv;->b:Ljava/lang/String;

    const/4 v12, 0x0

    invoke-direct {v10, v11, v4, v12}, Ljqs;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v5, v10}, Lhgw;-><init>(Ljqs;)V

    move-object v4, v5

    goto :goto_b

    .line 703
    :sswitch_5
    const/16 v6, 0x1c

    move-object/from16 v0, p2

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v6

    .line 704
    invoke-static {v6}, Lffl;->a([B)Z

    move-result v6

    if-eqz v6, :cond_20

    .line 705
    const v19, 0x7f020524

    .line 708
    :goto_d
    if-eqz v4, :cond_13

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_13

    .line 709
    const v5, 0x7f0d028b

    move-object/from16 v0, v36

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    .line 710
    const/4 v6, 0x0

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Bitmap;

    int-to-float v6, v5

    int-to-float v5, v5

    invoke-static {v4, v6, v5}, Lfus;->a(Landroid/graphics/Bitmap;FF)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 716
    :goto_e
    const v21, 0x7f0205a0

    .line 717
    const v5, 0x7f0a0780

    move-object/from16 v0, v36

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 720
    invoke-static/range {p2 .. p2}, Ldsf;->a(Landroid/database/Cursor;)I

    move-result v5

    .line 718
    const/4 v6, 0x1

    if-ne v5, v6, :cond_14

    const/4 v5, 0x6

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object v7, v6, v8

    invoke-static {v5, v6}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ldew;

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v6, v0, v1}, Ldew;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v6, v5}, Ldew;->d(Ljava/lang/String;)Ldew;

    move-result-object v5

    invoke-virtual {v5}, Ldew;->a()Landroid/content/Intent;

    move-result-object v5

    move-object/from16 v18, v5

    :goto_f
    new-instance v16, Ljava/util/ArrayList;

    const/4 v5, 0x1

    move-object/from16 v0, v16

    invoke-direct {v0, v5}, Ljava/util/ArrayList;-><init>(I)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v8, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x1

    const/4 v14, 0x0

    const/4 v15, 0x1

    invoke-static/range {p2 .. p2}, Lffl;->b(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v17

    move-object/from16 v5, p0

    move/from16 v6, p1

    move-object/from16 v7, v18

    invoke-static/range {v5 .. v17}, Lfhu;->a(Landroid/content/Context;ILandroid/content/Intent;Lda;IJZZZZLjava/util/ArrayList;Ljava/util/ArrayList;)V

    invoke-static {}, Lfhu;->a()I

    move-result v5

    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, v18

    invoke-static {v0, v1, v2}, Lfhu;->a(Landroid/content/Context;ILandroid/content/Intent;)Landroid/content/Intent;

    move-result-object v6

    const/4 v7, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v5, v6, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v18

    move-object v5, v4

    move-object/from16 v6, v28

    move-object/from16 v7, v29

    move-object/from16 v8, v30

    move/from16 v4, v19

    move-object/from16 v19, v20

    move/from16 v20, v21

    .line 721
    goto/16 :goto_4

    .line 712
    :cond_13
    const v4, 0x7f020368

    move-object/from16 v0, v36

    invoke-static {v0, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v4

    goto :goto_e

    .line 718
    :cond_14
    const/4 v5, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-static {v0, v1, v5}, Leyq;->a(Landroid/content/Context;ILandroid/net/Uri;)Landroid/content/Intent;

    move-result-object v5

    const-string v6, "destination"

    const/4 v8, 0x3

    invoke-virtual {v5, v6, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v5

    move-object/from16 v18, v5

    goto :goto_f

    .line 725
    :sswitch_6
    const v5, 0x7f020367

    .line 726
    const v6, 0x7f020367

    .line 727
    move-object/from16 v0, v36

    invoke-static {v0, v6}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    .line 730
    if-eqz v4, :cond_15

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_15

    .line 731
    const v6, 0x7f0d028b

    move-object/from16 v0, v36

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v6, v6

    .line 732
    const/4 v8, 0x0

    invoke-interface {v4, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Bitmap;

    int-to-float v8, v6

    int-to-float v6, v6

    invoke-static {v4, v8, v6}, Lfus;->a(Landroid/graphics/Bitmap;FF)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 739
    :goto_10
    const v8, 0x7f0205a0

    .line 740
    const v6, 0x7f0a0780

    move-object/from16 v0, v36

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 741
    invoke-static/range {p0 .. p1}, Leyq;->f(Landroid/content/Context;I)Leyt;

    move-result-object v12

    const/4 v13, 0x2

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v12, v13}, Leyt;->a(Ljava/lang/Integer;)Leyt;

    move-result-object v12

    const/16 v13, 0x1e

    invoke-virtual {v12, v13}, Leyt;->a(I)Leyt;

    move-result-object v12

    invoke-virtual {v12}, Leyt;->a()Landroid/content/Intent;

    move-result-object v15

    new-instance v24, Ljava/util/ArrayList;

    invoke-direct/range {v24 .. v24}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v24

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v16, 0x0

    const/16 v17, 0x12

    const/16 v20, 0x0

    const/16 v21, 0x1

    const/16 v22, 0x0

    const/16 v23, 0x1

    invoke-static/range {p2 .. p2}, Lffl;->b(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v25

    move-object/from16 v13, p0

    move/from16 v14, p1

    move-wide/from16 v18, v10

    invoke-static/range {v13 .. v25}, Lfhu;->a(Landroid/content/Context;ILandroid/content/Intent;Lda;IJZZZZLjava/util/ArrayList;Ljava/util/ArrayList;)V

    invoke-static {}, Lfhu;->a()I

    move-result v7

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-static {v0, v1, v15}, Lfhu;->a(Landroid/content/Context;ILandroid/content/Intent;)Landroid/content/Intent;

    move-result-object v10

    const/4 v11, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v7, v10, v11}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v18

    move-object/from16 v7, v29

    move-object/from16 v19, v6

    move/from16 v20, v8

    move-object/from16 v8, v30

    move-object/from16 v6, v28

    move-object/from16 v42, v4

    move v4, v5

    move-object/from16 v5, v42

    .line 744
    goto/16 :goto_4

    .line 734
    :cond_15
    const v4, 0x7f020367

    move-object/from16 v0, v36

    invoke-static {v0, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v4

    goto :goto_10

    .line 750
    :sswitch_7
    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-static {v0, v1, v6}, Llap;->c(Landroid/content/Context;ILjava/lang/String;)Z

    move-result v4

    .line 753
    if-eqz v21, :cond_16

    .line 754
    const v8, 0x7f0201c5

    .line 755
    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-static {v0, v1, v6}, Llap;->e(Landroid/content/Context;ILjava/lang/String;)Lkzs;

    move-result-object v16

    .line 757
    invoke-virtual/range {v16 .. v16}, Lkzs;->d()Ljava/lang/String;

    move-result-object v4

    .line 760
    invoke-static/range {p2 .. p2}, Lffl;->b(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v21

    move-object/from16 v13, p0

    move/from16 v14, p1

    move-object v15, v6

    move/from16 v17, v9

    move-wide/from16 v18, v10

    move-object/from16 v20, v7

    .line 758
    invoke-static/range {v13 .. v21}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILjava/lang/String;Lkzs;IJLjava/lang/String;Ljava/util/ArrayList;)Landroid/app/PendingIntent;

    move-result-object v18

    move-object/from16 v6, v28

    move-object/from16 v7, v29

    move-object/from16 v19, v4

    move/from16 v20, v8

    move-object/from16 v8, v30

    move v4, v5

    move-object/from16 v5, v22

    .line 761
    goto/16 :goto_4

    :cond_16
    if-nez v4, :cond_2

    .line 762
    const v20, 0x7f02059e

    .line 763
    const v4, 0x7f0a06a5

    move-object/from16 v0, v36

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v12, p0

    move/from16 v13, p1

    move-object v14, v6

    move-object v15, v7

    move-wide/from16 v16, v10

    .line 764
    invoke-static/range {v12 .. v17}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;J)Landroid/app/PendingIntent;

    move-result-object v18

    move v4, v5

    move-object/from16 v6, v28

    move-object/from16 v7, v29

    move-object/from16 v8, v30

    move-object/from16 v5, v22

    goto/16 :goto_4

    .line 772
    :sswitch_8
    const/4 v4, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-static {v0, v1, v4}, Lffl;->a(Landroid/content/Context;Llun;I)Ljava/util/List;

    move-result-object v6

    .line 773
    const/4 v4, 0x0

    .line 774
    if-eqz v6, :cond_17

    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_17

    .line 775
    const/4 v4, 0x0

    invoke-interface {v6, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Bitmap;

    .line 778
    :cond_17
    const v6, 0x7f0d014f

    move-object/from16 v0, v36

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v6, v6

    .line 779
    const v7, 0x7f0d0150

    move-object/from16 v0, v36

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    float-to-int v7, v7

    .line 780
    int-to-float v6, v6

    int-to-float v7, v7

    invoke-static {v4, v6, v7}, Lfus;->a(Landroid/graphics/Bitmap;FF)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 783
    const v6, 0x7f0d028b

    move-object/from16 v0, v36

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v6, v6

    .line 784
    int-to-float v7, v6

    int-to-float v6, v6

    invoke-static {v4, v7, v6}, Lfus;->a(Landroid/graphics/Bitmap;FF)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 787
    const v6, 0x7f02036d

    move-object/from16 v0, v36

    invoke-static {v0, v6}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 790
    move-object/from16 v0, v27

    iget-object v6, v0, Llun;->a:Llut;

    iget-object v6, v6, Llut;->a:Ljava/lang/String;

    .line 793
    const/4 v10, 0x2

    move-object/from16 v0, p2

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 795
    const-class v10, Ldhu;

    move-object/from16 v0, p0

    invoke-static {v0, v10}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ldhu;

    .line 796
    if-eqz v10, :cond_1f

    .line 797
    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x1

    const/16 v17, 0x0

    move-object/from16 v11, p0

    move/from16 v12, p1

    invoke-virtual/range {v10 .. v17}, Ldhu;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    move/from16 v42, v5

    move-object v5, v4

    move/from16 v4, v42

    goto/16 :goto_4

    .line 806
    :sswitch_9
    const v4, 0x7f02027f

    move-object/from16 v5, v22

    move-object/from16 v6, v28

    move-object/from16 v7, v29

    move-object/from16 v8, v30

    goto/16 :goto_4

    .line 841
    :cond_18
    move-object/from16 v0, v38

    invoke-virtual {v4, v0}, Lbq;->a(Ljava/lang/CharSequence;)Lbq;

    goto/16 :goto_5

    .line 845
    :cond_19
    if-eqz v27, :cond_5

    move-object/from16 v0, v27

    iget-object v4, v0, Llun;->b:[Lluk;

    if-eqz v4, :cond_5

    move-object/from16 v0, v27

    iget-object v4, v0, Llun;->b:[Lluk;

    array-length v4, v4

    if-eqz v4, :cond_5

    .line 847
    move-object/from16 v0, v27

    iget-object v7, v0, Llun;->b:[Lluk;

    .line 848
    array-length v4, v7

    const/4 v6, 0x1

    if-ne v4, v6, :cond_1b

    .line 849
    invoke-static/range {v31 .. v31}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 850
    invoke-virtual/range {v31 .. v31}, Ljava/lang/String;->length()I

    move-result v4

    const/16 v6, 0xb4

    if-ge v4, v6, :cond_1a

    move-object/from16 v4, v31

    :goto_11
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static/range {v38 .. v38}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x2

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    move-object/from16 v0, v38

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 851
    new-instance v6, Lbr;

    invoke-direct {v6}, Lbr;-><init>()V

    invoke-virtual {v6, v4}, Lbr;->b(Ljava/lang/CharSequence;)Lbr;

    move-result-object v4

    invoke-virtual {v11, v4}, Lbs;->a(Lce;)Lbs;

    goto/16 :goto_6

    .line 850
    :cond_1a
    const/4 v4, 0x0

    const/16 v6, 0xb4

    move-object/from16 v0, v31

    invoke-virtual {v0, v4, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "\u2026"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_11

    .line 853
    :cond_1b
    sparse-switch v9, :sswitch_data_2

    const/4 v4, 0x1

    :goto_12
    if-eqz v4, :cond_5

    .line 854
    new-instance v4, Lbu;

    invoke-direct {v4, v11}, Lbu;-><init>(Lbs;)V

    .line 855
    move-object/from16 v0, v37

    invoke-virtual {v4, v0}, Lbu;->a(Ljava/lang/CharSequence;)Lbu;

    move-result-object v8

    .line 856
    if-nez v20, :cond_1c

    if-nez v35, :cond_1c

    .line 859
    const v4, 0x7f0a024d

    move-object/from16 v0, v36

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, v4}, Lbu;->b(Ljava/lang/CharSequence;)Lbu;

    .line 861
    :cond_1c
    array-length v10, v7

    const/4 v4, 0x0

    move v6, v4

    :goto_13
    if-ge v6, v10, :cond_1e

    aget-object v12, v7, v6

    .line 862
    iget-object v4, v12, Lluk;->b:Llus;

    iget-object v4, v4, Llus;->c:Ljava/lang/String;

    .line 863
    iget-object v13, v12, Lluk;->b:Llus;

    iget-object v13, v13, Llus;->d:Ljava/lang/String;

    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_1d

    .line 864
    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iget-object v12, v12, Lluk;->b:Llus;

    iget-object v12, v12, Llus;->d:Ljava/lang/String;

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v14

    add-int/lit8 v14, v14, 0x1

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v15

    add-int/2addr v14, v15

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v13, " "

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 866
    :cond_1d
    invoke-virtual {v8, v4}, Lbu;->c(Ljava/lang/CharSequence;)Lbu;

    .line 861
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto :goto_13

    .line 853
    :sswitch_a
    const/4 v4, 0x0

    goto :goto_12

    .line 868
    :cond_1e
    invoke-static/range {v31 .. v31}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 869
    const-string v4, " "

    invoke-virtual {v8, v4}, Lbu;->c(Ljava/lang/CharSequence;)Lbu;

    .line 870
    move-object/from16 v0, v31

    invoke-virtual {v8, v0}, Lbu;->c(Ljava/lang/CharSequence;)Lbu;

    goto/16 :goto_6

    :cond_1f
    move/from16 v42, v5

    move-object v5, v4

    move/from16 v4, v42

    goto/16 :goto_4

    :cond_20
    move/from16 v19, v5

    goto/16 :goto_d

    :cond_21
    move-object/from16 v4, v18

    move-object/from16 v5, v19

    move/from16 v6, v20

    goto/16 :goto_c

    :cond_22
    move v4, v8

    move-object/from16 v5, v22

    move-object/from16 v6, v28

    move-object/from16 v7, v29

    move-object/from16 v8, v30

    goto/16 :goto_4

    .line 637
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_2
        0x3 -> :sswitch_1
        0xb -> :sswitch_0
        0xd -> :sswitch_3
    .end sparse-switch

    .line 677
    :sswitch_data_1
    .sparse-switch
        0x6 -> :sswitch_4
        0x10 -> :sswitch_7
        0x12 -> :sswitch_6
        0x18 -> :sswitch_7
        0x27 -> :sswitch_4
        0x61 -> :sswitch_5
        0x63 -> :sswitch_8
        0x69 -> :sswitch_9
        0x6f -> :sswitch_5
    .end sparse-switch

    .line 853
    :sswitch_data_2
    .sparse-switch
        0x6 -> :sswitch_a
        0x27 -> :sswitch_a
    .end sparse-switch
.end method
