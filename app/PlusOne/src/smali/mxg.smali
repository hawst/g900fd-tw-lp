.class public final Lmxg;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmxg;


# instance fields
.field public b:I

.field public c:Ljava/lang/Boolean;

.field private d:Ljava/lang/Boolean;

.field private e:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 116
    const/4 v0, 0x0

    new-array v0, v0, [Lmxg;

    sput-object v0, Lmxg;->a:[Lmxg;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 117
    invoke-direct {p0}, Loxq;-><init>()V

    .line 120
    const/high16 v0, -0x80000000

    iput v0, p0, Lmxg;->b:I

    .line 117
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 149
    const/4 v0, 0x0

    .line 150
    iget v1, p0, Lmxg;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 151
    const/4 v0, 0x1

    iget v1, p0, Lmxg;->b:I

    .line 152
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 154
    :cond_0
    iget-object v1, p0, Lmxg;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 155
    const/4 v1, 0x2

    iget-object v2, p0, Lmxg;->c:Ljava/lang/Boolean;

    .line 156
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 158
    :cond_1
    iget-object v1, p0, Lmxg;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 159
    const/4 v1, 0x3

    iget-object v2, p0, Lmxg;->d:Ljava/lang/Boolean;

    .line 160
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 162
    :cond_2
    iget-object v1, p0, Lmxg;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 163
    const/4 v1, 0x4

    iget-object v2, p0, Lmxg;->e:Ljava/lang/Boolean;

    .line 164
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 166
    :cond_3
    iget-object v1, p0, Lmxg;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 167
    iput v0, p0, Lmxg;->ai:I

    .line 168
    return v0
.end method

.method public a(Loxn;)Lmxg;
    .locals 3

    .prologue
    const/16 v2, 0x163

    .line 176
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 177
    sparse-switch v0, :sswitch_data_0

    .line 181
    iget-object v1, p0, Lmxg;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 182
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmxg;->ah:Ljava/util/List;

    .line 185
    :cond_1
    iget-object v1, p0, Lmxg;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 187
    :sswitch_0
    return-object p0

    .line 192
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 193
    if-eq v0, v2, :cond_2

    const/16 v1, 0x18d

    if-eq v0, v1, :cond_2

    const/16 v1, 0x1e5

    if-ne v0, v1, :cond_3

    .line 196
    :cond_2
    iput v0, p0, Lmxg;->b:I

    goto :goto_0

    .line 198
    :cond_3
    iput v2, p0, Lmxg;->b:I

    goto :goto_0

    .line 203
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmxg;->c:Ljava/lang/Boolean;

    goto :goto_0

    .line 207
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmxg;->d:Ljava/lang/Boolean;

    goto :goto_0

    .line 211
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmxg;->e:Ljava/lang/Boolean;

    goto :goto_0

    .line 177
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 131
    iget v0, p0, Lmxg;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 132
    const/4 v0, 0x1

    iget v1, p0, Lmxg;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 134
    :cond_0
    iget-object v0, p0, Lmxg;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 135
    const/4 v0, 0x2

    iget-object v1, p0, Lmxg;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 137
    :cond_1
    iget-object v0, p0, Lmxg;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 138
    const/4 v0, 0x3

    iget-object v1, p0, Lmxg;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 140
    :cond_2
    iget-object v0, p0, Lmxg;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 141
    const/4 v0, 0x4

    iget-object v1, p0, Lmxg;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 143
    :cond_3
    iget-object v0, p0, Lmxg;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 145
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 113
    invoke-virtual {p0, p1}, Lmxg;->a(Loxn;)Lmxg;

    move-result-object v0

    return-object v0
.end method
