.class public final Lelz;
.super Leku;
.source "PG"

# interfaces
.implements Lzd;
.implements Lze;


# static fields
.field private static N:I

.field private static O:I


# instance fields
.field private ad:Lelw;

.field private ae:Ljava/lang/String;

.field private af:Ljava/lang/String;

.field private ag:Z

.field private ah:Landroid/widget/TextView;

.field private ai:Landroid/support/v7/widget/SearchView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Leku;-><init>()V

    .line 62
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Leku;-><init>()V

    .line 65
    iput-object p1, p0, Lelz;->ae:Ljava/lang/String;

    .line 66
    iput-boolean p2, p0, Lelz;->ag:Z

    .line 67
    return-void
.end method

.method static synthetic a(Lelz;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lelz;->ah:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic b(Lelz;)Landroid/support/v7/widget/SearchView;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lelz;->ai:Landroid/support/v7/widget/SearchView;

    return-object v0
.end method

.method static synthetic c(Lelz;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lelz;->ae:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 317
    const/16 v0, 0x46

    return v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, -0x2

    const/4 v5, 0x0

    .line 111
    invoke-super {p0, p1, p2, p3}, Leku;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 112
    iget-object v0, p0, Lelz;->ad:Lelw;

    invoke-virtual {p0, v0}, Lelz;->a(Landroid/widget/ListAdapter;)V

    move-object v0, v1

    .line 114
    check-cast v0, Landroid/widget/RelativeLayout;

    .line 116
    new-instance v2, Landroid/widget/TextView;

    invoke-virtual {p0}, Lelz;->n()Lz;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lelz;->ah:Landroid/widget/TextView;

    .line 117
    iget-object v2, p0, Lelz;->ah:Landroid/widget/TextView;

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 118
    iget-object v2, p0, Lelz;->ah:Landroid/widget/TextView;

    sget v3, Lelz;->N:I

    sget v4, Lelz;->N:I

    invoke-virtual {v2, v3, v5, v4, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 119
    iget-object v2, p0, Lelz;->at:Llnl;

    iget-object v3, p0, Lelz;->ah:Landroid/widget/TextView;

    invoke-static {v2, v3, v7}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 121
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 123
    sget v3, Lelz;->O:I

    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 124
    const/16 v3, 0xe

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 125
    iget-object v3, p0, Lelz;->ah:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 127
    iget-boolean v2, p0, Lelz;->ag:Z

    if-eqz v2, :cond_0

    .line 128
    iget-object v2, p0, Lelz;->ah:Landroid/widget/TextView;

    const v3, 0x7f0a0814

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 133
    :goto_0
    iget-object v2, p0, Lelz;->ah:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 134
    invoke-virtual {p0}, Lelz;->aa()V

    .line 136
    iget-object v0, p0, Lelz;->ab:Licq;

    invoke-virtual {v0, v5}, Licq;->a(Z)Licq;

    move-result-object v0

    new-instance v2, Lema;

    invoke-direct {v2, p0}, Lema;-><init>(Lelz;)V

    .line 137
    invoke-virtual {v0, v2}, Licq;->a(Lico;)Licq;

    .line 151
    return-object v1

    .line 130
    :cond_0
    iget-object v2, p0, Lelz;->ah:Landroid/widget/TextView;

    const v3, 0x7f0a0817

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method protected a(II)Ldid;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 323
    .line 324
    invoke-virtual {p0, p1}, Lelz;->e(I)I

    move-result v0

    .line 323
    invoke-static {v0, v1, v1}, Ldib;->a(ILjava/lang/Integer;Ljava/lang/Integer;)Ldid;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 71
    invoke-super {p0, p1}, Leku;->a(Landroid/os/Bundle;)V

    .line 73
    sget v0, Lelz;->N:I

    if-nez v0, :cond_0

    .line 74
    invoke-virtual {p0}, Lelz;->o()Landroid/content/res/Resources;

    move-result-object v0

    .line 75
    const v1, 0x7f0d0349

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lelz;->N:I

    .line 77
    const v1, 0x7f0d034a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lelz;->O:I

    .line 81
    :cond_0
    if-eqz p1, :cond_3

    .line 82
    const-string v0, "query"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lelz;->ae:Ljava/lang/String;

    .line 83
    const-string v0, "delayed_query"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lelz;->af:Ljava/lang/String;

    .line 84
    const-string v0, "show_unified_search"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lelz;->ag:Z

    .line 85
    const-string v0, "unblock_request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 86
    const-string v0, "unblock_request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lelz;->V:Ljava/lang/Integer;

    .line 92
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lelz;->w()Lbb;

    move-result-object v3

    .line 93
    new-instance v0, Lelw;

    .line 94
    invoke-virtual {p0}, Lelz;->n()Lz;

    move-result-object v1

    invoke-virtual {p0}, Lelz;->p()Lae;

    move-result-object v2

    iget-object v4, p0, Lelz;->Z:Lhee;

    invoke-interface {v4}, Lhee;->d()I

    move-result v4

    iget-object v5, p0, Lelz;->Y:Lfuw;

    invoke-direct/range {v0 .. v5}, Lelw;-><init>(Landroid/content/Context;Lae;Lbb;ILfuw;)V

    iput-object v0, p0, Lelz;->ad:Lelw;

    .line 96
    iget-object v0, p0, Lelz;->ad:Lelw;

    invoke-virtual {v0, p1}, Lelw;->a(Landroid/os/Bundle;)V

    .line 97
    iget-object v0, p0, Lelz;->ad:Lelw;

    invoke-virtual {v0, p0}, Lelw;->a(Lfze;)V

    .line 98
    iget-object v0, p0, Lelz;->ad:Lelw;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lelw;->j_(I)V

    .line 99
    iget-object v0, p0, Lelz;->ad:Lelw;

    invoke-virtual {v0, v6}, Lelw;->e(Z)V

    .line 100
    iget-object v0, p0, Lelz;->ad:Lelw;

    invoke-virtual {v0, v6}, Lelw;->h(Z)V

    .line 101
    iget-object v0, p0, Lelz;->ad:Lelw;

    invoke-virtual {v0, v6}, Lelw;->g(Z)V

    .line 102
    iget-object v0, p0, Lelz;->ad:Lelw;

    invoke-virtual {v0, v6}, Lelw;->c(Z)V

    .line 103
    iget-object v0, p0, Lelz;->ae:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 104
    iget-object v0, p0, Lelz;->ad:Lelw;

    iget-boolean v1, p0, Lelz;->ag:Z

    invoke-virtual {v0, v1}, Lelw;->j(Z)V

    .line 106
    :cond_2
    return-void

    .line 89
    :cond_3
    invoke-virtual {p0}, Lelz;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "query"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lelz;->af:Ljava/lang/String;

    goto :goto_0
.end method

.method public a(Loo;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 206
    invoke-super {p0, p1}, Leku;->a(Loo;)V

    .line 208
    invoke-virtual {p0}, Lelz;->n()Lz;

    move-result-object v0

    .line 209
    new-instance v1, Landroid/support/v7/widget/SearchView;

    invoke-virtual {p1}, Loo;->i()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/support/v7/widget/SearchView;-><init>(Landroid/content/Context;)V

    .line 210
    invoke-static {v0, v1}, Lley;->a(Landroid/content/Context;Landroid/support/v7/widget/SearchView;)V

    .line 211
    invoke-virtual {p0}, Lelz;->o()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a0813

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/SearchView;->a(Ljava/lang/CharSequence;)V

    .line 212
    invoke-virtual {v1, v3}, Landroid/support/v7/widget/SearchView;->a(Z)V

    .line 213
    invoke-virtual {p1, v1}, Loo;->a(Landroid/view/View;)V

    .line 214
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Loo;->e(Z)V

    .line 215
    invoke-virtual {p1, v3}, Loo;->d(Z)V

    .line 217
    iget-object v0, p0, Lelz;->ae:Ljava/lang/String;

    invoke-virtual {v1, v0, v3}, Landroid/support/v7/widget/SearchView;->a(Ljava/lang/CharSequence;Z)V

    .line 218
    invoke-virtual {v1, p0}, Landroid/support/v7/widget/SearchView;->a(Lze;)V

    .line 219
    invoke-virtual {v1, p0}, Landroid/support/v7/widget/SearchView;->a(Lzd;)V

    .line 221
    invoke-virtual {v1}, Landroid/support/v7/widget/SearchView;->requestFocus()Z

    .line 223
    new-instance v0, Lemc;

    invoke-direct {v0, p0, v1}, Lemc;-><init>(Lelz;Landroid/support/v7/widget/SearchView;)V

    const-wide/16 v2, 0x32

    invoke-virtual {v1, v0, v2, v3}, Landroid/support/v7/widget/SearchView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 235
    iput-object v1, p0, Lelz;->ai:Landroid/support/v7/widget/SearchView;

    .line 236
    return-void
.end method

.method public a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 292
    invoke-virtual {p0}, Lelz;->x()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Llsn;->b(Landroid/view/View;)V

    .line 293
    iget-object v0, p0, Lelz;->ai:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView;->clearFocus()V

    .line 294
    iget-boolean v0, p0, Lelz;->ag:Z

    if-eqz v0, :cond_0

    .line 295
    invoke-virtual {p0}, Lelz;->ad()V

    .line 297
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public aO_()V
    .locals 4

    .prologue
    .line 178
    invoke-super {p0}, Leku;->aO_()V

    .line 179
    iget-object v0, p0, Lelz;->af:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 180
    iget-object v0, p0, Lelz;->af:Ljava/lang/String;

    iput-object v0, p0, Lelz;->ae:Ljava/lang/String;

    .line 181
    const/4 v0, 0x0

    iput-object v0, p0, Lelz;->af:Ljava/lang/String;

    .line 182
    iget-object v0, p0, Lelz;->ad:Lelw;

    iget-object v1, p0, Lelz;->ae:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lelw;->b(Ljava/lang/String;)V

    .line 183
    iget-object v0, p0, Lelz;->Q:Lhje;

    invoke-virtual {v0}, Lhje;->a()V

    .line 186
    :cond_0
    iget-object v0, p0, Lelz;->ai:Landroid/support/v7/widget/SearchView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lelz;->ae:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 187
    iget-object v0, p0, Lelz;->ai:Landroid/support/v7/widget/SearchView;

    new-instance v1, Lemb;

    invoke-direct {v1, p0}, Lemb;-><init>(Lelz;)V

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v7/widget/SearchView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 197
    :cond_1
    return-void
.end method

.method public aR_()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 335
    const/4 v0, 0x4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected ab()Z
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lelz;->ad:Lelw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lelz;->ae:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ad()V
    .locals 4

    .prologue
    .line 308
    iget-object v0, p0, Lelz;->Z:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    .line 310
    invoke-virtual {p0}, Lelz;->n()Lz;

    move-result-object v1

    iget-object v2, p0, Lelz;->ae:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v1, v0, v2, v3}, Leyq;->e(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 311
    invoke-virtual {p0, v0}, Lelz;->a(Landroid/content/Intent;)V

    .line 312
    invoke-virtual {p0}, Lelz;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->finish()V

    .line 313
    return-void
.end method

.method public ao_()Z
    .locals 2

    .prologue
    .line 302
    iget-object v0, p0, Lelz;->ad:Lelw;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lelw;->b(Ljava/lang/String;)V

    .line 303
    const/4 v0, 0x1

    return v0
.end method

.method public b()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 330
    const/16 v0, 0x68

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public b(Loo;)V
    .locals 1

    .prologue
    .line 240
    invoke-super {p0, p1}, Leku;->b(Loo;)V

    .line 242
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Loo;->a(Landroid/view/View;)V

    .line 243
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Loo;->e(Z)V

    .line 244
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Loo;->d(Z)V

    .line 245
    return-void
.end method

.method public b(Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 261
    if-nez p1, :cond_2

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lelz;->ae:Ljava/lang/String;

    .line 263
    invoke-static {}, Lfvc;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 264
    invoke-virtual {p0}, Lelz;->n()Lz;

    move-result-object v0

    invoke-static {v0, p1}, Lewm;->a(Landroid/content/Context;Ljava/lang/CharSequence;)V

    .line 267
    :cond_0
    iget-object v0, p0, Lelz;->Z:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v2

    .line 269
    iget-object v0, p0, Lelz;->ad:Lelw;

    if-eqz v0, :cond_1

    .line 270
    iget-object v0, p0, Lelz;->ad:Lelw;

    iget-object v3, p0, Lelz;->ae:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lelw;->b(Ljava/lang/String;)V

    .line 272
    iget-object v0, p0, Lelz;->ae:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 273
    const-string v0, "extra_search_type"

    .line 274
    invoke-static {v0, v1}, Lhmt;->a(Ljava/lang/String;I)Landroid/os/Bundle;

    move-result-object v3

    .line 276
    iget-object v0, p0, Lelz;->au:Llnh;

    const-class v4, Lhms;

    invoke-virtual {v0, v4}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v4, Lhmr;

    iget-object v5, p0, Lelz;->at:Llnl;

    invoke-direct {v4, v5, v2}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v2, Lhmv;->eU:Lhmv;

    .line 278
    invoke-virtual {v4, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v2

    .line 279
    invoke-virtual {v2, v3}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v2

    .line 276
    invoke-interface {v0, v2}, Lhms;->a(Lhmr;)V

    .line 284
    :cond_1
    invoke-virtual {p0}, Lelz;->x()Landroid/view/View;

    invoke-virtual {p0}, Lelz;->aa()V

    .line 285
    iget-object v2, p0, Lelz;->ad:Lelw;

    iget-boolean v0, p0, Lelz;->ag:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lelz;->ae:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    invoke-virtual {v2, v0}, Lelw;->j(Z)V

    .line 287
    return v1

    .line 261
    :cond_2
    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 285
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected d()V
    .locals 0

    .prologue
    .line 342
    return-void
.end method

.method protected e()V
    .locals 0

    .prologue
    .line 346
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 156
    invoke-super {p0, p1}, Leku;->e(Landroid/os/Bundle;)V

    .line 157
    iget-object v0, p0, Lelz;->ad:Lelw;

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lelz;->ad:Lelw;

    invoke-virtual {v0, p1}, Lelw;->b(Landroid/os/Bundle;)V

    .line 160
    :cond_0
    const-string v0, "query"

    iget-object v1, p0, Lelz;->ae:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    iget-object v0, p0, Lelz;->af:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 162
    const-string v0, "delayed_query"

    iget-object v1, p0, Lelz;->af:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    :cond_1
    const-string v0, "show_unified_search"

    iget-boolean v1, p0, Lelz;->ag:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 165
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 249
    invoke-super {p0}, Leku;->g()V

    .line 250
    iget-object v0, p0, Lelz;->ad:Lelw;

    invoke-virtual {v0}, Lelw;->f()V

    .line 251
    return-void
.end method

.method public h()V
    .locals 1

    .prologue
    .line 255
    invoke-super {p0}, Leku;->g()V

    .line 256
    iget-object v0, p0, Lelz;->ad:Lelw;

    invoke-virtual {v0}, Lelw;->g()V

    .line 257
    return-void
.end method

.method public z()V
    .locals 1

    .prologue
    .line 169
    invoke-super {p0}, Leku;->z()V

    .line 171
    iget-object v0, p0, Lelz;->ai:Landroid/support/v7/widget/SearchView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lelz;->ai:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView;->findFocus()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 172
    iget-object v0, p0, Lelz;->ai:Landroid/support/v7/widget/SearchView;

    invoke-static {v0}, Llsn;->b(Landroid/view/View;)V

    .line 174
    :cond_0
    return-void
.end method
