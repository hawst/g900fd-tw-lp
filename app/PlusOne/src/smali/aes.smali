.class public final Laes;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Integer;

.field private b:Ljava/lang/Integer;

.field private c:[Ljava/lang/Integer;

.field private d:I

.field private e:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 651
    invoke-direct {p0}, Loxq;-><init>()V

    .line 658
    sget-object v0, Loxx;->g:[Ljava/lang/Integer;

    iput-object v0, p0, Laes;->c:[Ljava/lang/Integer;

    .line 661
    const/high16 v0, -0x80000000

    iput v0, p0, Laes;->d:I

    .line 651
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 691
    .line 692
    iget-object v0, p0, Laes;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 693
    const/4 v0, 0x1

    iget-object v2, p0, Laes;->a:Ljava/lang/Integer;

    .line 694
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 696
    :goto_0
    iget-object v2, p0, Laes;->b:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    .line 697
    const/4 v2, 0x2

    iget-object v3, p0, Laes;->b:Ljava/lang/Integer;

    .line 698
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 700
    :cond_0
    iget-object v2, p0, Laes;->c:[Ljava/lang/Integer;

    if-eqz v2, :cond_2

    iget-object v2, p0, Laes;->c:[Ljava/lang/Integer;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 702
    iget-object v3, p0, Laes;->c:[Ljava/lang/Integer;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_1

    aget-object v5, v3, v1

    .line 704
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-static {v5}, Loxo;->i(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 702
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 706
    :cond_1
    add-int/2addr v0, v2

    .line 707
    iget-object v1, p0, Laes;->c:[Ljava/lang/Integer;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 709
    :cond_2
    iget v1, p0, Laes;->d:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_3

    .line 710
    const/4 v1, 0x4

    iget v2, p0, Laes;->d:I

    .line 711
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 713
    :cond_3
    iget-object v1, p0, Laes;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 714
    const/4 v1, 0x5

    iget-object v2, p0, Laes;->e:Ljava/lang/Boolean;

    .line 715
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 717
    :cond_4
    iget-object v1, p0, Laes;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 718
    iput v0, p0, Laes;->ai:I

    .line 719
    return v0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Laes;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 727
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 728
    sparse-switch v0, :sswitch_data_0

    .line 732
    iget-object v1, p0, Laes;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 733
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Laes;->ah:Ljava/util/List;

    .line 736
    :cond_1
    iget-object v1, p0, Laes;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 738
    :sswitch_0
    return-object p0

    .line 743
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Laes;->a:Ljava/lang/Integer;

    goto :goto_0

    .line 747
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Laes;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 751
    :sswitch_3
    const/16 v0, 0x18

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 752
    iget-object v0, p0, Laes;->c:[Ljava/lang/Integer;

    array-length v0, v0

    .line 753
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/Integer;

    .line 754
    iget-object v2, p0, Laes;->c:[Ljava/lang/Integer;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 755
    iput-object v1, p0, Laes;->c:[Ljava/lang/Integer;

    .line 756
    :goto_1
    iget-object v1, p0, Laes;->c:[Ljava/lang/Integer;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 757
    iget-object v1, p0, Laes;->c:[Ljava/lang/Integer;

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    .line 758
    invoke-virtual {p1}, Loxn;->a()I

    .line 756
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 761
    :cond_2
    iget-object v1, p0, Laes;->c:[Ljava/lang/Integer;

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    .line 765
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 766
    if-eq v0, v4, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    const/4 v1, 0x4

    if-eq v0, v1, :cond_3

    const/4 v1, 0x5

    if-eq v0, v1, :cond_3

    const/4 v1, 0x6

    if-eq v0, v1, :cond_3

    const/4 v1, 0x7

    if-eq v0, v1, :cond_3

    const/16 v1, 0x8

    if-eq v0, v1, :cond_3

    const/16 v1, 0x9

    if-eq v0, v1, :cond_3

    const/16 v1, 0xa

    if-eq v0, v1, :cond_3

    const/16 v1, 0xb

    if-eq v0, v1, :cond_3

    const/16 v1, 0xc

    if-eq v0, v1, :cond_3

    const/16 v1, 0xd

    if-eq v0, v1, :cond_3

    const/16 v1, 0xe

    if-eq v0, v1, :cond_3

    const/16 v1, 0xf

    if-eq v0, v1, :cond_3

    const/16 v1, 0x10

    if-eq v0, v1, :cond_3

    const/16 v1, 0x11

    if-ne v0, v1, :cond_4

    .line 783
    :cond_3
    iput v0, p0, Laes;->d:I

    goto/16 :goto_0

    .line 785
    :cond_4
    iput v4, p0, Laes;->d:I

    goto/16 :goto_0

    .line 790
    :sswitch_5
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Laes;->e:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 728
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 668
    iget-object v0, p0, Laes;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 669
    const/4 v0, 0x1

    iget-object v1, p0, Laes;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 671
    :cond_0
    iget-object v0, p0, Laes;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 672
    const/4 v0, 0x2

    iget-object v1, p0, Laes;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 674
    :cond_1
    iget-object v0, p0, Laes;->c:[Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 675
    iget-object v1, p0, Laes;->c:[Ljava/lang/Integer;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 676
    const/4 v4, 0x3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p1, v4, v3}, Loxo;->a(II)V

    .line 675
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 679
    :cond_2
    iget v0, p0, Laes;->d:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_3

    .line 680
    const/4 v0, 0x4

    iget v1, p0, Laes;->d:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 682
    :cond_3
    iget-object v0, p0, Laes;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 683
    const/4 v0, 0x5

    iget-object v1, p0, Laes;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 685
    :cond_4
    iget-object v0, p0, Laes;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 687
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 647
    invoke-virtual {p0, p1}, Laes;->a(Loxn;)Laes;

    move-result-object v0

    return-object v0
.end method
