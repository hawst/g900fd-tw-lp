.class public final Llyv;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lojv;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10042
    invoke-direct {p0}, Loxq;-><init>()V

    .line 10045
    iput-object v0, p0, Llyv;->apiHeader:Llyr;

    .line 10048
    iput-object v0, p0, Llyv;->a:Lojv;

    .line 10042
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 10065
    const/4 v0, 0x0

    .line 10066
    iget-object v1, p0, Llyv;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 10067
    const/4 v0, 0x1

    iget-object v1, p0, Llyv;->apiHeader:Llyr;

    .line 10068
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 10070
    :cond_0
    iget-object v1, p0, Llyv;->a:Lojv;

    if-eqz v1, :cond_1

    .line 10071
    const/4 v1, 0x2

    iget-object v2, p0, Llyv;->a:Lojv;

    .line 10072
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10074
    :cond_1
    iget-object v1, p0, Llyv;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10075
    iput v0, p0, Llyv;->ai:I

    .line 10076
    return v0
.end method

.method public a(Loxn;)Llyv;
    .locals 2

    .prologue
    .line 10084
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 10085
    sparse-switch v0, :sswitch_data_0

    .line 10089
    iget-object v1, p0, Llyv;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 10090
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llyv;->ah:Ljava/util/List;

    .line 10093
    :cond_1
    iget-object v1, p0, Llyv;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 10095
    :sswitch_0
    return-object p0

    .line 10100
    :sswitch_1
    iget-object v0, p0, Llyv;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 10101
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Llyv;->apiHeader:Llyr;

    .line 10103
    :cond_2
    iget-object v0, p0, Llyv;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 10107
    :sswitch_2
    iget-object v0, p0, Llyv;->a:Lojv;

    if-nez v0, :cond_3

    .line 10108
    new-instance v0, Lojv;

    invoke-direct {v0}, Lojv;-><init>()V

    iput-object v0, p0, Llyv;->a:Lojv;

    .line 10110
    :cond_3
    iget-object v0, p0, Llyv;->a:Lojv;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 10085
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 10053
    iget-object v0, p0, Llyv;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 10054
    const/4 v0, 0x1

    iget-object v1, p0, Llyv;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 10056
    :cond_0
    iget-object v0, p0, Llyv;->a:Lojv;

    if-eqz v0, :cond_1

    .line 10057
    const/4 v0, 0x2

    iget-object v1, p0, Llyv;->a:Lojv;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 10059
    :cond_1
    iget-object v0, p0, Llyv;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 10061
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 10038
    invoke-virtual {p0, p1}, Llyv;->a(Loxn;)Llyv;

    move-result-object v0

    return-object v0
.end method
