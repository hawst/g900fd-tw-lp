.class public final Losg;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Losg;


# instance fields
.field private b:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1331
    const/4 v0, 0x0

    new-array v0, v0, [Losg;

    sput-object v0, Losg;->a:[Losg;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1332
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1357
    const/high16 v0, -0x80000000

    iput v0, p0, Losg;->b:I

    .line 1332
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1371
    const/4 v0, 0x0

    .line 1372
    iget v1, p0, Losg;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 1373
    const/4 v0, 0x1

    iget v1, p0, Losg;->b:I

    .line 1374
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1376
    :cond_0
    iget-object v1, p0, Losg;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1377
    iput v0, p0, Losg;->ai:I

    .line 1378
    return v0
.end method

.method public a(Loxn;)Losg;
    .locals 2

    .prologue
    .line 1386
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1387
    sparse-switch v0, :sswitch_data_0

    .line 1391
    iget-object v1, p0, Losg;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1392
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Losg;->ah:Ljava/util/List;

    .line 1395
    :cond_1
    iget-object v1, p0, Losg;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1397
    :sswitch_0
    return-object p0

    .line 1402
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1403
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa

    if-eq v0, v1, :cond_2

    const/16 v1, 0xb

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd

    if-eq v0, v1, :cond_2

    const/16 v1, 0xc

    if-eq v0, v1, :cond_2

    const/16 v1, 0xe

    if-eq v0, v1, :cond_2

    const/16 v1, 0xf

    if-eq v0, v1, :cond_2

    const/16 v1, 0x10

    if-eq v0, v1, :cond_2

    const/16 v1, 0x11

    if-eq v0, v1, :cond_2

    const/16 v1, 0x12

    if-ne v0, v1, :cond_3

    .line 1422
    :cond_2
    iput v0, p0, Losg;->b:I

    goto :goto_0

    .line 1424
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Losg;->b:I

    goto :goto_0

    .line 1387
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1362
    iget v0, p0, Losg;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 1363
    const/4 v0, 0x1

    iget v1, p0, Losg;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1365
    :cond_0
    iget-object v0, p0, Losg;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1367
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1328
    invoke-virtual {p0, p1}, Losg;->a(Loxn;)Losg;

    move-result-object v0

    return-object v0
.end method
