.class public final Loom;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Long;

.field public b:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 495
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 516
    const/4 v0, 0x0

    .line 517
    iget-object v1, p0, Loom;->a:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 518
    const/4 v0, 0x1

    iget-object v1, p0, Loom;->a:Ljava/lang/Long;

    .line 519
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Loxo;->f(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 521
    :cond_0
    iget-object v1, p0, Loom;->b:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 522
    const/4 v1, 0x2

    iget-object v2, p0, Loom;->b:Ljava/lang/Long;

    .line 523
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 525
    :cond_1
    iget-object v1, p0, Loom;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 526
    iput v0, p0, Loom;->ai:I

    .line 527
    return v0
.end method

.method public a(Loxn;)Loom;
    .locals 2

    .prologue
    .line 535
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 536
    sparse-switch v0, :sswitch_data_0

    .line 540
    iget-object v1, p0, Loom;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 541
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Loom;->ah:Ljava/util/List;

    .line 544
    :cond_1
    iget-object v1, p0, Loom;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 546
    :sswitch_0
    return-object p0

    .line 551
    :sswitch_1
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Loom;->a:Ljava/lang/Long;

    goto :goto_0

    .line 555
    :sswitch_2
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Loom;->b:Ljava/lang/Long;

    goto :goto_0

    .line 536
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 504
    iget-object v0, p0, Loom;->a:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 505
    const/4 v0, 0x1

    iget-object v1, p0, Loom;->a:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 507
    :cond_0
    iget-object v0, p0, Loom;->b:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 508
    const/4 v0, 0x2

    iget-object v1, p0, Loom;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 510
    :cond_1
    iget-object v0, p0, Loom;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 512
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 491
    invoke-virtual {p0, p1}, Loom;->a(Loxn;)Loom;

    move-result-object v0

    return-object v0
.end method
