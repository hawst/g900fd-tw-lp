.class public Ldes;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljlw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljlw",
        "<",
        "Ldes;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljlx;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljlx",
            "<",
            "Ldes;",
            ">;"
        }
    .end annotation
.end field

.field private b:I

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lhov;)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ldes;->c:Ljava/util/List;

    .line 39
    new-instance v0, Ljls;

    invoke-direct {v0, p0, p1}, Ljls;-><init>(Ljava/lang/Object;Lhov;)V

    iput-object v0, p0, Ldes;->a:Ljlx;

    .line 40
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Ldes;->b:I

    return v0
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 58
    iput p1, p0, Ldes;->b:I

    .line 59
    invoke-virtual {p0}, Ldes;->b()Ljlx;

    move-result-object v0

    invoke-interface {v0}, Ljlx;->a()V

    .line 60
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Ldes;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 82
    invoke-virtual {p0}, Ldes;->b()Ljlx;

    move-result-object v0

    invoke-interface {v0}, Ljlx;->a()V

    .line 83
    return-void
.end method

.method public b()Ljlx;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljlx",
            "<",
            "Ldes;",
            ">;"
        }
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Ldes;->a:Ljlx;

    return-object v0
.end method

.method public c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 66
    iget-object v0, p0, Ldes;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Ldes;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 74
    invoke-virtual {p0}, Ldes;->b()Ljlx;

    move-result-object v0

    invoke-interface {v0}, Ljlx;->a()V

    .line 75
    return-void
.end method
