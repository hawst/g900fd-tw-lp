.class public final Lmzv;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnxr;

.field public b:Ljava/lang/String;

.field public c:[Lnzx;

.field public d:Lnzx;

.field public e:[Logr;

.field private f:Lmzp;

.field private g:Lnxo;

.field private h:Lnxh;

.field private i:Lmzw;

.field private j:[Lnym;

.field private k:Lnyz;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 448
    invoke-direct {p0}, Loxq;-><init>()V

    .line 451
    iput-object v1, p0, Lmzv;->f:Lmzp;

    .line 454
    iput-object v1, p0, Lmzv;->a:Lnxr;

    .line 459
    sget-object v0, Lnzx;->a:[Lnzx;

    iput-object v0, p0, Lmzv;->c:[Lnzx;

    .line 462
    iput-object v1, p0, Lmzv;->g:Lnxo;

    .line 465
    iput-object v1, p0, Lmzv;->h:Lnxh;

    .line 468
    iput-object v1, p0, Lmzv;->i:Lmzw;

    .line 471
    iput-object v1, p0, Lmzv;->d:Lnzx;

    .line 474
    sget-object v0, Logr;->a:[Logr;

    iput-object v0, p0, Lmzv;->e:[Logr;

    .line 477
    sget-object v0, Lnym;->a:[Lnym;

    iput-object v0, p0, Lmzv;->j:[Lnym;

    .line 480
    iput-object v1, p0, Lmzv;->k:Lnyz;

    .line 448
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 536
    .line 537
    iget-object v0, p0, Lmzv;->f:Lmzp;

    if-eqz v0, :cond_d

    .line 538
    const/4 v0, 0x1

    iget-object v2, p0, Lmzv;->f:Lmzp;

    .line 539
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 541
    :goto_0
    iget-object v2, p0, Lmzv;->a:Lnxr;

    if-eqz v2, :cond_0

    .line 542
    const/4 v2, 0x2

    iget-object v3, p0, Lmzv;->a:Lnxr;

    .line 543
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 545
    :cond_0
    iget-object v2, p0, Lmzv;->j:[Lnym;

    if-eqz v2, :cond_2

    .line 546
    iget-object v3, p0, Lmzv;->j:[Lnym;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 547
    if-eqz v5, :cond_1

    .line 548
    const/4 v6, 0x3

    .line 549
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 546
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 553
    :cond_2
    iget-object v2, p0, Lmzv;->b:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 554
    const/4 v2, 0x4

    iget-object v3, p0, Lmzv;->b:Ljava/lang/String;

    .line 555
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 557
    :cond_3
    iget-object v2, p0, Lmzv;->c:[Lnzx;

    if-eqz v2, :cond_5

    .line 558
    iget-object v3, p0, Lmzv;->c:[Lnzx;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_5

    aget-object v5, v3, v2

    .line 559
    if-eqz v5, :cond_4

    .line 560
    const/4 v6, 0x5

    .line 561
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 558
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 565
    :cond_5
    iget-object v2, p0, Lmzv;->g:Lnxo;

    if-eqz v2, :cond_6

    .line 566
    const/4 v2, 0x6

    iget-object v3, p0, Lmzv;->g:Lnxo;

    .line 567
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 569
    :cond_6
    iget-object v2, p0, Lmzv;->i:Lmzw;

    if-eqz v2, :cond_7

    .line 570
    const/4 v2, 0x7

    iget-object v3, p0, Lmzv;->i:Lmzw;

    .line 571
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 573
    :cond_7
    iget-object v2, p0, Lmzv;->h:Lnxh;

    if-eqz v2, :cond_8

    .line 574
    const/16 v2, 0x9

    iget-object v3, p0, Lmzv;->h:Lnxh;

    .line 575
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 577
    :cond_8
    iget-object v2, p0, Lmzv;->d:Lnzx;

    if-eqz v2, :cond_9

    .line 578
    const/16 v2, 0xa

    iget-object v3, p0, Lmzv;->d:Lnzx;

    .line 579
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 581
    :cond_9
    iget-object v2, p0, Lmzv;->e:[Logr;

    if-eqz v2, :cond_b

    .line 582
    iget-object v2, p0, Lmzv;->e:[Logr;

    array-length v3, v2

    :goto_3
    if-ge v1, v3, :cond_b

    aget-object v4, v2, v1

    .line 583
    if-eqz v4, :cond_a

    .line 584
    const/16 v5, 0xb

    .line 585
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 582
    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 589
    :cond_b
    iget-object v1, p0, Lmzv;->k:Lnyz;

    if-eqz v1, :cond_c

    .line 590
    const/16 v1, 0xc

    iget-object v2, p0, Lmzv;->k:Lnyz;

    .line 591
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 593
    :cond_c
    iget-object v1, p0, Lmzv;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 594
    iput v0, p0, Lmzv;->ai:I

    .line 595
    return v0

    :cond_d
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lmzv;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 603
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 604
    sparse-switch v0, :sswitch_data_0

    .line 608
    iget-object v2, p0, Lmzv;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 609
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmzv;->ah:Ljava/util/List;

    .line 612
    :cond_1
    iget-object v2, p0, Lmzv;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 614
    :sswitch_0
    return-object p0

    .line 619
    :sswitch_1
    iget-object v0, p0, Lmzv;->f:Lmzp;

    if-nez v0, :cond_2

    .line 620
    new-instance v0, Lmzp;

    invoke-direct {v0}, Lmzp;-><init>()V

    iput-object v0, p0, Lmzv;->f:Lmzp;

    .line 622
    :cond_2
    iget-object v0, p0, Lmzv;->f:Lmzp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 626
    :sswitch_2
    iget-object v0, p0, Lmzv;->a:Lnxr;

    if-nez v0, :cond_3

    .line 627
    new-instance v0, Lnxr;

    invoke-direct {v0}, Lnxr;-><init>()V

    iput-object v0, p0, Lmzv;->a:Lnxr;

    .line 629
    :cond_3
    iget-object v0, p0, Lmzv;->a:Lnxr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 633
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 634
    iget-object v0, p0, Lmzv;->j:[Lnym;

    if-nez v0, :cond_5

    move v0, v1

    .line 635
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnym;

    .line 636
    iget-object v3, p0, Lmzv;->j:[Lnym;

    if-eqz v3, :cond_4

    .line 637
    iget-object v3, p0, Lmzv;->j:[Lnym;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 639
    :cond_4
    iput-object v2, p0, Lmzv;->j:[Lnym;

    .line 640
    :goto_2
    iget-object v2, p0, Lmzv;->j:[Lnym;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 641
    iget-object v2, p0, Lmzv;->j:[Lnym;

    new-instance v3, Lnym;

    invoke-direct {v3}, Lnym;-><init>()V

    aput-object v3, v2, v0

    .line 642
    iget-object v2, p0, Lmzv;->j:[Lnym;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 643
    invoke-virtual {p1}, Loxn;->a()I

    .line 640
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 634
    :cond_5
    iget-object v0, p0, Lmzv;->j:[Lnym;

    array-length v0, v0

    goto :goto_1

    .line 646
    :cond_6
    iget-object v2, p0, Lmzv;->j:[Lnym;

    new-instance v3, Lnym;

    invoke-direct {v3}, Lnym;-><init>()V

    aput-object v3, v2, v0

    .line 647
    iget-object v2, p0, Lmzv;->j:[Lnym;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 651
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmzv;->b:Ljava/lang/String;

    goto/16 :goto_0

    .line 655
    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 656
    iget-object v0, p0, Lmzv;->c:[Lnzx;

    if-nez v0, :cond_8

    move v0, v1

    .line 657
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lnzx;

    .line 658
    iget-object v3, p0, Lmzv;->c:[Lnzx;

    if-eqz v3, :cond_7

    .line 659
    iget-object v3, p0, Lmzv;->c:[Lnzx;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 661
    :cond_7
    iput-object v2, p0, Lmzv;->c:[Lnzx;

    .line 662
    :goto_4
    iget-object v2, p0, Lmzv;->c:[Lnzx;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    .line 663
    iget-object v2, p0, Lmzv;->c:[Lnzx;

    new-instance v3, Lnzx;

    invoke-direct {v3}, Lnzx;-><init>()V

    aput-object v3, v2, v0

    .line 664
    iget-object v2, p0, Lmzv;->c:[Lnzx;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 665
    invoke-virtual {p1}, Loxn;->a()I

    .line 662
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 656
    :cond_8
    iget-object v0, p0, Lmzv;->c:[Lnzx;

    array-length v0, v0

    goto :goto_3

    .line 668
    :cond_9
    iget-object v2, p0, Lmzv;->c:[Lnzx;

    new-instance v3, Lnzx;

    invoke-direct {v3}, Lnzx;-><init>()V

    aput-object v3, v2, v0

    .line 669
    iget-object v2, p0, Lmzv;->c:[Lnzx;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 673
    :sswitch_6
    iget-object v0, p0, Lmzv;->g:Lnxo;

    if-nez v0, :cond_a

    .line 674
    new-instance v0, Lnxo;

    invoke-direct {v0}, Lnxo;-><init>()V

    iput-object v0, p0, Lmzv;->g:Lnxo;

    .line 676
    :cond_a
    iget-object v0, p0, Lmzv;->g:Lnxo;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 680
    :sswitch_7
    iget-object v0, p0, Lmzv;->i:Lmzw;

    if-nez v0, :cond_b

    .line 681
    new-instance v0, Lmzw;

    invoke-direct {v0}, Lmzw;-><init>()V

    iput-object v0, p0, Lmzv;->i:Lmzw;

    .line 683
    :cond_b
    iget-object v0, p0, Lmzv;->i:Lmzw;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 687
    :sswitch_8
    iget-object v0, p0, Lmzv;->h:Lnxh;

    if-nez v0, :cond_c

    .line 688
    new-instance v0, Lnxh;

    invoke-direct {v0}, Lnxh;-><init>()V

    iput-object v0, p0, Lmzv;->h:Lnxh;

    .line 690
    :cond_c
    iget-object v0, p0, Lmzv;->h:Lnxh;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 694
    :sswitch_9
    iget-object v0, p0, Lmzv;->d:Lnzx;

    if-nez v0, :cond_d

    .line 695
    new-instance v0, Lnzx;

    invoke-direct {v0}, Lnzx;-><init>()V

    iput-object v0, p0, Lmzv;->d:Lnzx;

    .line 697
    :cond_d
    iget-object v0, p0, Lmzv;->d:Lnzx;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 701
    :sswitch_a
    const/16 v0, 0x5a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 702
    iget-object v0, p0, Lmzv;->e:[Logr;

    if-nez v0, :cond_f

    move v0, v1

    .line 703
    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Logr;

    .line 704
    iget-object v3, p0, Lmzv;->e:[Logr;

    if-eqz v3, :cond_e

    .line 705
    iget-object v3, p0, Lmzv;->e:[Logr;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 707
    :cond_e
    iput-object v2, p0, Lmzv;->e:[Logr;

    .line 708
    :goto_6
    iget-object v2, p0, Lmzv;->e:[Logr;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_10

    .line 709
    iget-object v2, p0, Lmzv;->e:[Logr;

    new-instance v3, Logr;

    invoke-direct {v3}, Logr;-><init>()V

    aput-object v3, v2, v0

    .line 710
    iget-object v2, p0, Lmzv;->e:[Logr;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 711
    invoke-virtual {p1}, Loxn;->a()I

    .line 708
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 702
    :cond_f
    iget-object v0, p0, Lmzv;->e:[Logr;

    array-length v0, v0

    goto :goto_5

    .line 714
    :cond_10
    iget-object v2, p0, Lmzv;->e:[Logr;

    new-instance v3, Logr;

    invoke-direct {v3}, Logr;-><init>()V

    aput-object v3, v2, v0

    .line 715
    iget-object v2, p0, Lmzv;->e:[Logr;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 719
    :sswitch_b
    iget-object v0, p0, Lmzv;->k:Lnyz;

    if-nez v0, :cond_11

    .line 720
    new-instance v0, Lnyz;

    invoke-direct {v0}, Lnyz;-><init>()V

    iput-object v0, p0, Lmzv;->k:Lnyz;

    .line 722
    :cond_11
    iget-object v0, p0, Lmzv;->k:Lnyz;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 604
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x4a -> :sswitch_8
        0x52 -> :sswitch_9
        0x5a -> :sswitch_a
        0x62 -> :sswitch_b
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 485
    iget-object v1, p0, Lmzv;->f:Lmzp;

    if-eqz v1, :cond_0

    .line 486
    const/4 v1, 0x1

    iget-object v2, p0, Lmzv;->f:Lmzp;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 488
    :cond_0
    iget-object v1, p0, Lmzv;->a:Lnxr;

    if-eqz v1, :cond_1

    .line 489
    const/4 v1, 0x2

    iget-object v2, p0, Lmzv;->a:Lnxr;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 491
    :cond_1
    iget-object v1, p0, Lmzv;->j:[Lnym;

    if-eqz v1, :cond_3

    .line 492
    iget-object v2, p0, Lmzv;->j:[Lnym;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 493
    if-eqz v4, :cond_2

    .line 494
    const/4 v5, 0x3

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 492
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 498
    :cond_3
    iget-object v1, p0, Lmzv;->b:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 499
    const/4 v1, 0x4

    iget-object v2, p0, Lmzv;->b:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 501
    :cond_4
    iget-object v1, p0, Lmzv;->c:[Lnzx;

    if-eqz v1, :cond_6

    .line 502
    iget-object v2, p0, Lmzv;->c:[Lnzx;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    .line 503
    if-eqz v4, :cond_5

    .line 504
    const/4 v5, 0x5

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 502
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 508
    :cond_6
    iget-object v1, p0, Lmzv;->g:Lnxo;

    if-eqz v1, :cond_7

    .line 509
    const/4 v1, 0x6

    iget-object v2, p0, Lmzv;->g:Lnxo;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 511
    :cond_7
    iget-object v1, p0, Lmzv;->i:Lmzw;

    if-eqz v1, :cond_8

    .line 512
    const/4 v1, 0x7

    iget-object v2, p0, Lmzv;->i:Lmzw;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 514
    :cond_8
    iget-object v1, p0, Lmzv;->h:Lnxh;

    if-eqz v1, :cond_9

    .line 515
    const/16 v1, 0x9

    iget-object v2, p0, Lmzv;->h:Lnxh;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 517
    :cond_9
    iget-object v1, p0, Lmzv;->d:Lnzx;

    if-eqz v1, :cond_a

    .line 518
    const/16 v1, 0xa

    iget-object v2, p0, Lmzv;->d:Lnzx;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 520
    :cond_a
    iget-object v1, p0, Lmzv;->e:[Logr;

    if-eqz v1, :cond_c

    .line 521
    iget-object v1, p0, Lmzv;->e:[Logr;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_c

    aget-object v3, v1, v0

    .line 522
    if-eqz v3, :cond_b

    .line 523
    const/16 v4, 0xb

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 521
    :cond_b
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 527
    :cond_c
    iget-object v0, p0, Lmzv;->k:Lnyz;

    if-eqz v0, :cond_d

    .line 528
    const/16 v0, 0xc

    iget-object v1, p0, Lmzv;->k:Lnyz;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 530
    :cond_d
    iget-object v0, p0, Lmzv;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 532
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 444
    invoke-virtual {p0, p1}, Lmzv;->a(Loxn;)Lmzv;

    move-result-object v0

    return-object v0
.end method
