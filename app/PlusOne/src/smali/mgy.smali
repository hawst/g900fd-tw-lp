.class public final Lmgy;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnga;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 9475
    invoke-direct {p0}, Loxq;-><init>()V

    .line 9478
    iput-object v0, p0, Lmgy;->apiHeader:Llyq;

    .line 9481
    iput-object v0, p0, Lmgy;->a:Lnga;

    .line 9475
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 9498
    const/4 v0, 0x0

    .line 9499
    iget-object v1, p0, Lmgy;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 9500
    const/4 v0, 0x1

    iget-object v1, p0, Lmgy;->apiHeader:Llyq;

    .line 9501
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 9503
    :cond_0
    iget-object v1, p0, Lmgy;->a:Lnga;

    if-eqz v1, :cond_1

    .line 9504
    const/4 v1, 0x2

    iget-object v2, p0, Lmgy;->a:Lnga;

    .line 9505
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9507
    :cond_1
    iget-object v1, p0, Lmgy;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9508
    iput v0, p0, Lmgy;->ai:I

    .line 9509
    return v0
.end method

.method public a(Loxn;)Lmgy;
    .locals 2

    .prologue
    .line 9517
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 9518
    sparse-switch v0, :sswitch_data_0

    .line 9522
    iget-object v1, p0, Lmgy;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 9523
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmgy;->ah:Ljava/util/List;

    .line 9526
    :cond_1
    iget-object v1, p0, Lmgy;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 9528
    :sswitch_0
    return-object p0

    .line 9533
    :sswitch_1
    iget-object v0, p0, Lmgy;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 9534
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmgy;->apiHeader:Llyq;

    .line 9536
    :cond_2
    iget-object v0, p0, Lmgy;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 9540
    :sswitch_2
    iget-object v0, p0, Lmgy;->a:Lnga;

    if-nez v0, :cond_3

    .line 9541
    new-instance v0, Lnga;

    invoke-direct {v0}, Lnga;-><init>()V

    iput-object v0, p0, Lmgy;->a:Lnga;

    .line 9543
    :cond_3
    iget-object v0, p0, Lmgy;->a:Lnga;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 9518
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 9486
    iget-object v0, p0, Lmgy;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 9487
    const/4 v0, 0x1

    iget-object v1, p0, Lmgy;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 9489
    :cond_0
    iget-object v0, p0, Lmgy;->a:Lnga;

    if-eqz v0, :cond_1

    .line 9490
    const/4 v0, 0x2

    iget-object v1, p0, Lmgy;->a:Lnga;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 9492
    :cond_1
    iget-object v0, p0, Lmgy;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 9494
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 9471
    invoke-virtual {p0, p1}, Lmgy;->a(Loxn;)Lmgy;

    move-result-object v0

    return-object v0
.end method
