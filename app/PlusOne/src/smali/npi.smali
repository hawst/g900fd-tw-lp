.class public final Lnpi;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Locf;

.field private c:Lock;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 910
    invoke-direct {p0}, Loxq;-><init>()V

    .line 915
    iput-object v0, p0, Lnpi;->b:Locf;

    .line 918
    iput-object v0, p0, Lnpi;->c:Lock;

    .line 910
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 938
    const/4 v0, 0x0

    .line 939
    iget-object v1, p0, Lnpi;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 940
    const/4 v0, 0x1

    iget-object v1, p0, Lnpi;->a:Ljava/lang/String;

    .line 941
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 943
    :cond_0
    iget-object v1, p0, Lnpi;->b:Locf;

    if-eqz v1, :cond_1

    .line 944
    const/4 v1, 0x2

    iget-object v2, p0, Lnpi;->b:Locf;

    .line 945
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 947
    :cond_1
    iget-object v1, p0, Lnpi;->c:Lock;

    if-eqz v1, :cond_2

    .line 948
    const/4 v1, 0x3

    iget-object v2, p0, Lnpi;->c:Lock;

    .line 949
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 951
    :cond_2
    iget-object v1, p0, Lnpi;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 952
    iput v0, p0, Lnpi;->ai:I

    .line 953
    return v0
.end method

.method public a(Loxn;)Lnpi;
    .locals 2

    .prologue
    .line 961
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 962
    sparse-switch v0, :sswitch_data_0

    .line 966
    iget-object v1, p0, Lnpi;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 967
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnpi;->ah:Ljava/util/List;

    .line 970
    :cond_1
    iget-object v1, p0, Lnpi;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 972
    :sswitch_0
    return-object p0

    .line 977
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnpi;->a:Ljava/lang/String;

    goto :goto_0

    .line 981
    :sswitch_2
    iget-object v0, p0, Lnpi;->b:Locf;

    if-nez v0, :cond_2

    .line 982
    new-instance v0, Locf;

    invoke-direct {v0}, Locf;-><init>()V

    iput-object v0, p0, Lnpi;->b:Locf;

    .line 984
    :cond_2
    iget-object v0, p0, Lnpi;->b:Locf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 988
    :sswitch_3
    iget-object v0, p0, Lnpi;->c:Lock;

    if-nez v0, :cond_3

    .line 989
    new-instance v0, Lock;

    invoke-direct {v0}, Lock;-><init>()V

    iput-object v0, p0, Lnpi;->c:Lock;

    .line 991
    :cond_3
    iget-object v0, p0, Lnpi;->c:Lock;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 962
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 923
    iget-object v0, p0, Lnpi;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 924
    const/4 v0, 0x1

    iget-object v1, p0, Lnpi;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 926
    :cond_0
    iget-object v0, p0, Lnpi;->b:Locf;

    if-eqz v0, :cond_1

    .line 927
    const/4 v0, 0x2

    iget-object v1, p0, Lnpi;->b:Locf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 929
    :cond_1
    iget-object v0, p0, Lnpi;->c:Lock;

    if-eqz v0, :cond_2

    .line 930
    const/4 v0, 0x3

    iget-object v1, p0, Lnpi;->c:Lock;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 932
    :cond_2
    iget-object v0, p0, Lnpi;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 934
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 906
    invoke-virtual {p0, p1}, Lnpi;->a(Loxn;)Lnpi;

    move-result-object v0

    return-object v0
.end method
