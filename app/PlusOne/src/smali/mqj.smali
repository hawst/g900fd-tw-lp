.class public final Lmqj;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmpm;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:[Lmqd;

.field public e:Ljava/lang/String;

.field public f:Loxz;

.field private g:Ljava/lang/Boolean;

.field private h:I

.field private i:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 13
    iput-object v1, p0, Lmqj;->a:Lmpm;

    .line 22
    sget-object v0, Lmqd;->a:[Lmqd;

    iput-object v0, p0, Lmqj;->d:[Lmqd;

    .line 25
    const/high16 v0, -0x80000000

    iput v0, p0, Lmqj;->h:I

    .line 32
    iput-object v1, p0, Lmqj;->f:Loxz;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 74
    .line 75
    iget-object v0, p0, Lmqj;->a:Lmpm;

    if-eqz v0, :cond_9

    .line 76
    const/4 v0, 0x1

    iget-object v2, p0, Lmqj;->a:Lmpm;

    .line 77
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 79
    :goto_0
    iget-object v2, p0, Lmqj;->b:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 80
    const/4 v2, 0x2

    iget-object v3, p0, Lmqj;->b:Ljava/lang/String;

    .line 81
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 83
    :cond_0
    iget-object v2, p0, Lmqj;->c:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 84
    const/4 v2, 0x3

    iget-object v3, p0, Lmqj;->c:Ljava/lang/String;

    .line 85
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 87
    :cond_1
    iget-object v2, p0, Lmqj;->d:[Lmqd;

    if-eqz v2, :cond_3

    .line 88
    iget-object v2, p0, Lmqj;->d:[Lmqd;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 89
    if-eqz v4, :cond_2

    .line 90
    const/4 v5, 0x4

    .line 91
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 88
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 95
    :cond_3
    iget v1, p0, Lmqj;->h:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_4

    .line 96
    const/4 v1, 0x5

    iget v2, p0, Lmqj;->h:I

    .line 97
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 99
    :cond_4
    iget-object v1, p0, Lmqj;->e:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 100
    const/4 v1, 0x6

    iget-object v2, p0, Lmqj;->e:Ljava/lang/String;

    .line 101
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 103
    :cond_5
    iget-object v1, p0, Lmqj;->i:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 104
    const/4 v1, 0x7

    iget-object v2, p0, Lmqj;->i:Ljava/lang/String;

    .line 105
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 107
    :cond_6
    iget-object v1, p0, Lmqj;->g:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    .line 108
    const/16 v1, 0x8

    iget-object v2, p0, Lmqj;->g:Ljava/lang/Boolean;

    .line 109
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 111
    :cond_7
    iget-object v1, p0, Lmqj;->f:Loxz;

    if-eqz v1, :cond_8

    .line 112
    const/16 v1, 0x9

    iget-object v2, p0, Lmqj;->f:Loxz;

    .line 113
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 115
    :cond_8
    iget-object v1, p0, Lmqj;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 116
    iput v0, p0, Lmqj;->ai:I

    .line 117
    return v0

    :cond_9
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lmqj;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 125
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 126
    sparse-switch v0, :sswitch_data_0

    .line 130
    iget-object v2, p0, Lmqj;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 131
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmqj;->ah:Ljava/util/List;

    .line 134
    :cond_1
    iget-object v2, p0, Lmqj;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 136
    :sswitch_0
    return-object p0

    .line 141
    :sswitch_1
    iget-object v0, p0, Lmqj;->a:Lmpm;

    if-nez v0, :cond_2

    .line 142
    new-instance v0, Lmpm;

    invoke-direct {v0}, Lmpm;-><init>()V

    iput-object v0, p0, Lmqj;->a:Lmpm;

    .line 144
    :cond_2
    iget-object v0, p0, Lmqj;->a:Lmpm;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 148
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmqj;->b:Ljava/lang/String;

    goto :goto_0

    .line 152
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmqj;->c:Ljava/lang/String;

    goto :goto_0

    .line 156
    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 157
    iget-object v0, p0, Lmqj;->d:[Lmqd;

    if-nez v0, :cond_4

    move v0, v1

    .line 158
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lmqd;

    .line 159
    iget-object v3, p0, Lmqj;->d:[Lmqd;

    if-eqz v3, :cond_3

    .line 160
    iget-object v3, p0, Lmqj;->d:[Lmqd;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 162
    :cond_3
    iput-object v2, p0, Lmqj;->d:[Lmqd;

    .line 163
    :goto_2
    iget-object v2, p0, Lmqj;->d:[Lmqd;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 164
    iget-object v2, p0, Lmqj;->d:[Lmqd;

    new-instance v3, Lmqd;

    invoke-direct {v3}, Lmqd;-><init>()V

    aput-object v3, v2, v0

    .line 165
    iget-object v2, p0, Lmqj;->d:[Lmqd;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 166
    invoke-virtual {p1}, Loxn;->a()I

    .line 163
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 157
    :cond_4
    iget-object v0, p0, Lmqj;->d:[Lmqd;

    array-length v0, v0

    goto :goto_1

    .line 169
    :cond_5
    iget-object v2, p0, Lmqj;->d:[Lmqd;

    new-instance v3, Lmqd;

    invoke-direct {v3}, Lmqd;-><init>()V

    aput-object v3, v2, v0

    .line 170
    iget-object v2, p0, Lmqj;->d:[Lmqd;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 174
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 175
    if-eqz v0, :cond_6

    const/4 v2, 0x1

    if-eq v0, v2, :cond_6

    const/4 v2, 0x2

    if-ne v0, v2, :cond_7

    .line 178
    :cond_6
    iput v0, p0, Lmqj;->h:I

    goto/16 :goto_0

    .line 180
    :cond_7
    iput v1, p0, Lmqj;->h:I

    goto/16 :goto_0

    .line 185
    :sswitch_6
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmqj;->e:Ljava/lang/String;

    goto/16 :goto_0

    .line 189
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmqj;->i:Ljava/lang/String;

    goto/16 :goto_0

    .line 193
    :sswitch_8
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmqj;->g:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 197
    :sswitch_9
    iget-object v0, p0, Lmqj;->f:Loxz;

    if-nez v0, :cond_8

    .line 198
    new-instance v0, Loxz;

    invoke-direct {v0}, Loxz;-><init>()V

    iput-object v0, p0, Lmqj;->f:Loxz;

    .line 200
    :cond_8
    iget-object v0, p0, Lmqj;->f:Loxz;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 126
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 37
    iget-object v0, p0, Lmqj;->a:Lmpm;

    if-eqz v0, :cond_0

    .line 38
    const/4 v0, 0x1

    iget-object v1, p0, Lmqj;->a:Lmpm;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 40
    :cond_0
    iget-object v0, p0, Lmqj;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 41
    const/4 v0, 0x2

    iget-object v1, p0, Lmqj;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 43
    :cond_1
    iget-object v0, p0, Lmqj;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 44
    const/4 v0, 0x3

    iget-object v1, p0, Lmqj;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 46
    :cond_2
    iget-object v0, p0, Lmqj;->d:[Lmqd;

    if-eqz v0, :cond_4

    .line 47
    iget-object v1, p0, Lmqj;->d:[Lmqd;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 48
    if-eqz v3, :cond_3

    .line 49
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 47
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 53
    :cond_4
    iget v0, p0, Lmqj;->h:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_5

    .line 54
    const/4 v0, 0x5

    iget v1, p0, Lmqj;->h:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 56
    :cond_5
    iget-object v0, p0, Lmqj;->e:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 57
    const/4 v0, 0x6

    iget-object v1, p0, Lmqj;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 59
    :cond_6
    iget-object v0, p0, Lmqj;->i:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 60
    const/4 v0, 0x7

    iget-object v1, p0, Lmqj;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 62
    :cond_7
    iget-object v0, p0, Lmqj;->g:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    .line 63
    const/16 v0, 0x8

    iget-object v1, p0, Lmqj;->g:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 65
    :cond_8
    iget-object v0, p0, Lmqj;->f:Loxz;

    if-eqz v0, :cond_9

    .line 66
    const/16 v0, 0x9

    iget-object v1, p0, Lmqj;->f:Loxz;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 68
    :cond_9
    iget-object v0, p0, Lmqj;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 70
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lmqj;->a(Loxn;)Lmqj;

    move-result-object v0

    return-object v0
.end method
