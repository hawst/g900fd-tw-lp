.class public Laoe;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljed;",
        "Ljava/lang/Void;",
        "Laoc;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljfb;

.field private final c:Lajl;

.field private final d:Laoi;

.field private final e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Laoe;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Laoe;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljfb;Lajl;Laoi;Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 73
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 74
    const-string v0, "plusDataProvider"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljfb;

    iput-object v0, p0, Laoe;->b:Ljfb;

    .line 75
    const-string v0, "metricsStore"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lajl;

    iput-object v0, p0, Laoe;->c:Lajl;

    .line 76
    const-string v0, "listener"

    invoke-static {p3, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoi;

    iput-object v0, p0, Laoe;->d:Laoi;

    .line 77
    iput-boolean p4, p0, Laoe;->e:Z

    .line 78
    return-void
.end method

.method public static a(Ljfb;Lajl;)Laoh;
    .locals 1

    .prologue
    .line 55
    new-instance v0, Laof;

    invoke-direct {v0, p0, p1}, Laof;-><init>(Ljfb;Lajl;)V

    return-object v0
.end method


# virtual methods
.method protected varargs a([Ljed;)Laoc;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 82
    aget-object v3, p1, v0

    .line 85
    iget-object v1, p0, Laoe;->b:Ljfb;

    iget-object v2, v3, Ljed;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljfb;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    .line 86
    const/4 v2, 0x0

    move v1, v0

    .line 87
    :goto_0
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 88
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljeb;

    iget-object v0, v0, Ljeb;->a:Ljed;

    invoke-virtual {v0, v3}, Ljed;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 89
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljeb;

    move-object v2, v0

    .line 94
    :cond_0
    if-nez v2, :cond_2

    .line 95
    new-instance v0, Laoc;

    sget-object v1, Laod;->b:Laod;

    invoke-direct {v0, v1}, Laoc;-><init>(Laod;)V

    .line 132
    :goto_1
    return-object v0

    .line 87
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 100
    :cond_2
    iget-boolean v0, p0, Laoe;->e:Z

    if-eqz v0, :cond_5

    .line 102
    iget-object v0, p0, Laoe;->b:Ljfb;

    invoke-virtual {v0, v3}, Ljfb;->b(Ljed;)Ljava/util/List;

    move-result-object v0

    move-object v1, v0

    .line 109
    :goto_2
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 110
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljej;

    .line 111
    sget-object v5, Laog;->a:[I

    iget-object v6, v0, Ljej;->b:Ljel;

    invoke-virtual {v6}, Ljel;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    goto :goto_3

    .line 113
    :pswitch_0
    iget-boolean v5, p0, Laoe;->e:Z

    if-nez v5, :cond_4

    iget-object v5, p0, Laoe;->c:Lajl;

    iget-object v6, v0, Ljej;->a:Landroid/net/Uri;

    .line 114
    invoke-interface {v5, v6}, Lajl;->f(Landroid/net/Uri;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 115
    :cond_4
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 105
    :cond_5
    iget-object v0, p0, Laoe;->b:Ljfb;

    invoke-virtual {v0, v3}, Ljfb;->a(Ljed;)Ljava/util/List;

    move-result-object v0

    move-object v1, v0

    goto :goto_2

    .line 119
    :pswitch_1
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 127
    :cond_6
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_7

    .line 128
    sget-object v0, Laoe;->a:Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v4, 0x59

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Had "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " before filtering, but none after. This cluster should have been unmarked."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 132
    :cond_7
    new-instance v0, Laoc;

    invoke-direct {v0, v2, v3}, Laoc;-><init>(Ljeb;Ljava/util/List;)V

    goto :goto_1

    .line 111
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected a(Laoc;)V
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Laoe;->d:Laoi;

    invoke-interface {v0, p0, p1}, Laoi;->a(Laoe;Laoc;)V

    .line 138
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 19
    check-cast p1, [Ljed;

    invoke-virtual {p0, p1}, Laoe;->a([Ljed;)Laoc;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 19
    check-cast p1, Laoc;

    invoke-virtual {p0, p1}, Laoe;->a(Laoc;)V

    return-void
.end method
