.class public final Lnzu;
.super Loxq;
.source "PG"


# static fields
.field public static final a:Loxr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Loxr",
            "<",
            "Lnzu;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:Lnym;

.field private c:Loud;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 9
    const v0, 0x284879c

    new-instance v1, Lnzv;

    invoke-direct {v1}, Lnzv;-><init>()V

    .line 14
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    move-result-object v0

    sput-object v0, Lnzu;->a:Loxr;

    .line 13
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 17
    iput-object v0, p0, Lnzu;->b:Lnym;

    .line 20
    iput-object v0, p0, Lnzu;->c:Loud;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 37
    const/4 v0, 0x0

    .line 38
    iget-object v1, p0, Lnzu;->b:Lnym;

    if-eqz v1, :cond_0

    .line 39
    const/4 v0, 0x1

    iget-object v1, p0, Lnzu;->b:Lnym;

    .line 40
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 42
    :cond_0
    iget-object v1, p0, Lnzu;->c:Loud;

    if-eqz v1, :cond_1

    .line 43
    const/4 v1, 0x2

    iget-object v2, p0, Lnzu;->c:Loud;

    .line 44
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 46
    :cond_1
    iget-object v1, p0, Lnzu;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 47
    iput v0, p0, Lnzu;->ai:I

    .line 48
    return v0
.end method

.method public a(Loxn;)Lnzu;
    .locals 2

    .prologue
    .line 56
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 57
    sparse-switch v0, :sswitch_data_0

    .line 61
    iget-object v1, p0, Lnzu;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 62
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnzu;->ah:Ljava/util/List;

    .line 65
    :cond_1
    iget-object v1, p0, Lnzu;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 67
    :sswitch_0
    return-object p0

    .line 72
    :sswitch_1
    iget-object v0, p0, Lnzu;->b:Lnym;

    if-nez v0, :cond_2

    .line 73
    new-instance v0, Lnym;

    invoke-direct {v0}, Lnym;-><init>()V

    iput-object v0, p0, Lnzu;->b:Lnym;

    .line 75
    :cond_2
    iget-object v0, p0, Lnzu;->b:Lnym;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 79
    :sswitch_2
    iget-object v0, p0, Lnzu;->c:Loud;

    if-nez v0, :cond_3

    .line 80
    new-instance v0, Loud;

    invoke-direct {v0}, Loud;-><init>()V

    iput-object v0, p0, Lnzu;->c:Loud;

    .line 82
    :cond_3
    iget-object v0, p0, Lnzu;->c:Loud;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 57
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 25
    iget-object v0, p0, Lnzu;->b:Lnym;

    if-eqz v0, :cond_0

    .line 26
    const/4 v0, 0x1

    iget-object v1, p0, Lnzu;->b:Lnym;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 28
    :cond_0
    iget-object v0, p0, Lnzu;->c:Loud;

    if-eqz v0, :cond_1

    .line 29
    const/4 v0, 0x2

    iget-object v1, p0, Lnzu;->c:Loud;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 31
    :cond_1
    iget-object v0, p0, Lnzu;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 33
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lnzu;->a(Loxn;)Lnzu;

    move-result-object v0

    return-object v0
.end method
