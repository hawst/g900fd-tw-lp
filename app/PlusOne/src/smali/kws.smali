.class public final Lkws;
.super Llol;
.source "PG"

# interfaces
.implements Lbc;
.implements Lhio;
.implements Lhyz;
.implements Lhzq;
.implements Lksv;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Llol;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lhio;",
        "Lhyz;",
        "Lhzq;",
        "Lksv;"
    }
.end annotation


# static fields
.field private static final N:[Ljava/lang/String;


# instance fields
.field private O:Landroid/database/Cursor;

.field private P:I

.field private Q:Lhzb;

.field private R:Lhzl;

.field private S:Z

.field private T:Z

.field private U:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lkwt;",
            ">;"
        }
    .end annotation
.end field

.field private V:Lksu;

.field private W:Lhie;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 44
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "square_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "square_name"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "photo_url"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "restricted_domain"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "square_streams"

    aput-object v2, v0, v1

    sput-object v0, Lkws;->N:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Llol;-><init>()V

    .line 64
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lkws;->U:Landroid/util/SparseArray;

    return-void
.end method


# virtual methods
.method public U()V
    .locals 3

    .prologue
    .line 260
    iget-boolean v0, p0, Lkws;->T:Z

    if-eqz v0, :cond_0

    .line 261
    invoke-virtual {p0}, Lkws;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 263
    :cond_0
    return-void
.end method

.method public a(Landroid/content/Context;)I
    .locals 1

    .prologue
    .line 167
    invoke-static {p1}, Lhiu;->a(Landroid/content/Context;)I

    move-result v0

    return v0
.end method

.method public a(I)Landroid/os/Parcelable;
    .locals 10

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 220
    iget-object v0, p0, Lkws;->O:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 222
    iget-object v0, p0, Lkws;->U:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkwt;

    .line 223
    if-nez v0, :cond_0

    .line 226
    iget-object v0, p0, Lkws;->O:Landroid/database/Cursor;

    const/4 v1, 0x4

    .line 227
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Lktg;->a([B)[Lktg;

    move-result-object v0

    .line 230
    if-eqz v0, :cond_2

    array-length v1, v0

    if-ne v1, v5, :cond_2

    .line 231
    aget-object v1, v0, v6

    invoke-virtual {v1}, Lktg;->a()Ljava/lang/String;

    move-result-object v3

    .line 232
    aget-object v0, v0, v6

    invoke-virtual {v0}, Lktg;->b()Ljava/lang/String;

    move-result-object v4

    .line 234
    :goto_0
    invoke-static {}, Lkwt;->a()Lkwv;

    move-result-object v0

    iget-object v1, p0, Lkws;->O:Landroid/database/Cursor;

    const/4 v2, 0x2

    .line 235
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkwv;->a(Ljava/lang/String;)Lkwv;

    move-result-object v7

    new-instance v0, Lkxr;

    iget-object v1, p0, Lkws;->O:Landroid/database/Cursor;

    .line 237
    invoke-interface {v1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lkws;->O:Landroid/database/Cursor;

    .line 238
    invoke-interface {v2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v8, p0, Lkws;->O:Landroid/database/Cursor;

    const/4 v9, 0x3

    .line 241
    invoke-interface {v8, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_1

    :goto_1
    invoke-direct/range {v0 .. v5}, Lkxr;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 236
    invoke-virtual {v7, v0}, Lkwv;->a(Lkxr;)Lkwv;

    move-result-object v0

    .line 242
    invoke-virtual {v0}, Lkwv;->a()Lkwt;

    move-result-object v0

    .line 243
    iget-object v1, p0, Lkws;->U:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 245
    :cond_0
    return-object v0

    :cond_1
    move v5, v6

    .line 241
    goto :goto_1

    :cond_2
    move-object v3, v4

    goto :goto_0
.end method

.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 138
    new-instance v0, Lktu;

    iget-object v1, p0, Lkws;->at:Llnl;

    iget v2, p0, Lkws;->P:I

    sget-object v3, Lkws;->N:[Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lktu;-><init>(Landroid/content/Context;I[Ljava/lang/String;)V

    .line 139
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lktu;->a(I)Lktu;

    .line 140
    iget-boolean v1, p0, Lkws;->S:Z

    invoke-virtual {v0, v1}, Lktu;->a(Z)Lktu;

    .line 141
    return-object v0
.end method

.method public a(ILandroid/view/View;)V
    .locals 3

    .prologue
    .line 210
    iget-object v0, p0, Lkws;->O:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 211
    check-cast p2, Lhiu;

    .line 212
    iget-object v0, p0, Lkws;->O:Landroid/database/Cursor;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lhiu;->a(Ljava/lang/String;)V

    .line 213
    invoke-virtual {p2}, Lhiu;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;

    iget-object v1, p0, Lkws;->O:Landroid/database/Cursor;

    const/4 v2, 0x2

    .line 214
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->a(Ljava/lang/String;)Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;

    move-result-object v0

    .line 215
    invoke-virtual {v0}, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->a()Lhis;

    move-result-object v0

    invoke-virtual {p0, p1}, Lkws;->a(I)Landroid/os/Parcelable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhis;->a(Landroid/os/Parcelable;)V

    .line 216
    return-void
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 146
    iput-object p1, p0, Lkws;->O:Landroid/database/Cursor;

    .line 147
    iget-object v0, p0, Lkws;->U:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 148
    iget-object v0, p0, Lkws;->Q:Lhzb;

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lkws;->Q:Lhzb;

    invoke-interface {v0}, Lhzb;->e()V

    .line 151
    :cond_0
    return-void
.end method

.method public a(Landroid/os/Parcelable;)V
    .locals 3

    .prologue
    .line 93
    iget-object v0, p0, Lkws;->R:Lhzl;

    if-eqz v0, :cond_0

    instance-of v0, p1, Lkwt;

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lkws;->R:Lhzl;

    invoke-interface {v0, p1}, Lhzl;->c(Landroid/os/Parcelable;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 95
    iget-object v0, p0, Lkws;->R:Lhzl;

    invoke-interface {v0, p1}, Lhzl;->b(Landroid/os/Parcelable;)Z

    .line 107
    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v0, p1

    .line 97
    check-cast v0, Lkwt;

    .line 98
    invoke-virtual {v0}, Lkwt;->b()Lkxr;

    move-result-object v1

    invoke-virtual {v1}, Lkxr;->c()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    .line 99
    iget-object v1, p0, Lkws;->V:Lksu;

    invoke-virtual {v0}, Lkwt;->b()Lkxr;

    move-result-object v0

    invoke-interface {v1, v0}, Lksu;->a(Lkxr;)Lt;

    move-result-object v0

    invoke-virtual {p0}, Lkws;->q()Lae;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lt;->a(Lae;Ljava/lang/String;)V

    goto :goto_0

    .line 101
    :cond_2
    iget-object v0, p0, Lkws;->R:Lhzl;

    invoke-interface {v0}, Lhzl;->a()V

    .line 102
    iget-object v0, p0, Lkws;->R:Lhzl;

    invoke-interface {v0, p1}, Lhzl;->a(Landroid/os/Parcelable;)Z

    .line 103
    iget-object v0, p0, Lkws;->W:Lhie;

    const/4 v1, -0x1

    invoke-interface {v0, v1}, Lhie;->a(I)V

    goto :goto_0
.end method

.method public a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 190
    return-void
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 156
    return-void
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 38
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p2}, Lkws;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method public a(Lhzb;)V
    .locals 0

    .prologue
    .line 255
    iput-object p1, p0, Lkws;->Q:Lhzb;

    .line 256
    return-void
.end method

.method public a(Lkxr;)V
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Lkws;->R:Lhzl;

    invoke-interface {v0}, Lhzl;->a()V

    .line 112
    iget-object v0, p0, Lkws;->R:Lhzl;

    invoke-static {}, Lkwt;->a()Lkwv;

    move-result-object v1

    .line 113
    invoke-virtual {v1, p1}, Lkwv;->a(Lkxr;)Lkwv;

    move-result-object v1

    .line 114
    invoke-virtual {v1}, Lkwv;->a()Lkwt;

    move-result-object v1

    .line 112
    invoke-interface {v0, v1}, Lhzl;->a(Landroid/os/Parcelable;)Z

    .line 115
    iget-object v0, p0, Lkws;->W:Lhie;

    const/4 v1, -0x1

    invoke-interface {v0, v1}, Lhie;->a(I)V

    .line 116
    return-void
.end method

.method public a(Z)V
    .locals 4

    .prologue
    .line 76
    iput-boolean p1, p0, Lkws;->S:Z

    .line 77
    iget-object v0, p0, Lkws;->R:Lhzl;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lkws;->S:Z

    if-eqz v0, :cond_1

    .line 78
    iget-object v0, p0, Lkws;->R:Lhzl;

    invoke-interface {v0}, Lhzl;->c()Ljava/util/List;

    move-result-object v2

    .line 79
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 80
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lkwt;

    if-eqz v0, :cond_0

    .line 81
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkwt;

    .line 82
    invoke-virtual {v0}, Lkwt;->b()Lkxr;

    move-result-object v3

    invoke-virtual {v3}, Lkxr;->e()Z

    move-result v3

    if-nez v3, :cond_0

    .line 83
    iget-object v3, p0, Lkws;->R:Lhzl;

    invoke-interface {v3, v0}, Lhzl;->b(Landroid/os/Parcelable;)Z

    .line 79
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 88
    :cond_1
    invoke-virtual {p0}, Lkws;->U()V

    .line 89
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 194
    const/4 v0, 0x0

    return v0
.end method

.method public aO_()V
    .locals 3

    .prologue
    .line 160
    invoke-super {p0}, Llol;->aO_()V

    .line 161
    const/4 v0, 0x1

    iput-boolean v0, p0, Lkws;->T:Z

    .line 162
    invoke-virtual {p0}, Lkws;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 163
    return-void
.end method

.method public b()Landroid/view/View;
    .locals 1

    .prologue
    .line 200
    const/4 v0, 0x0

    return-object v0
.end method

.method public b(Landroid/content/Context;)Landroid/view/View;
    .locals 2

    .prologue
    .line 172
    new-instance v0, Lhiu;

    invoke-direct {v0, p1}, Lhiu;-><init>(Landroid/content/Context;)V

    .line 173
    new-instance v1, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;

    invoke-direct {v1, p1}, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lhiu;->a(Landroid/view/View;)V

    .line 174
    return-object v0
.end method

.method public c(Landroid/content/Context;)Landroid/view/View;
    .locals 1

    .prologue
    .line 185
    const/4 v0, 0x0

    return-object v0
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 125
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 126
    iget-object v0, p0, Lkws;->au:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    iput v0, p0, Lkws;->P:I

    .line 127
    iget-object v0, p0, Lkws;->au:Llnh;

    const-class v1, Lhzl;

    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhzl;

    iput-object v0, p0, Lkws;->R:Lhzl;

    .line 128
    iget-object v0, p0, Lkws;->au:Llnh;

    const-class v1, Lhin;

    .line 129
    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhin;

    .line 130
    invoke-virtual {v0, p0}, Lhin;->a(Lhio;)V

    .line 131
    iget-object v0, p0, Lkws;->au:Llnh;

    const-class v1, Lksv;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 132
    iget-object v0, p0, Lkws;->au:Llnh;

    const-class v1, Lksu;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lksu;

    iput-object v0, p0, Lkws;->V:Lksu;

    .line 133
    iget-object v0, p0, Lkws;->au:Llnh;

    const-class v1, Lhie;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhie;

    iput-object v0, p0, Lkws;->W:Lhie;

    .line 134
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 179
    const/4 v0, 0x0

    return v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lkws;->O:Landroid/database/Cursor;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lkws;->O:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    goto :goto_0
.end method

.method public e()V
    .locals 0

    .prologue
    .line 121
    return-void
.end method

.method public g()V
    .locals 0

    .prologue
    .line 70
    invoke-super {p0}, Llol;->g()V

    .line 71
    invoke-virtual {p0}, Lkws;->U()V

    .line 72
    return-void
.end method
