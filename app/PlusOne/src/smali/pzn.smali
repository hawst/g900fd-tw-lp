.class public final Lpzn;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lpzq;

.field private b:I

.field private c:Lpzu;

.field private d:Lpzt;

.field private e:Lpzp;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 524
    const/high16 v0, -0x80000000

    iput v0, p0, Lpzn;->b:I

    .line 527
    sget-object v0, Lpzq;->a:[Lpzq;

    iput-object v0, p0, Lpzn;->a:[Lpzq;

    .line 530
    iput-object v1, p0, Lpzn;->c:Lpzu;

    .line 533
    iput-object v1, p0, Lpzn;->d:Lpzt;

    .line 536
    iput-object v1, p0, Lpzn;->e:Lpzp;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 566
    .line 567
    iget v0, p0, Lpzn;->b:I

    const/high16 v2, -0x80000000

    if-eq v0, v2, :cond_5

    .line 568
    const/4 v0, 0x1

    iget v2, p0, Lpzn;->b:I

    .line 569
    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 571
    :goto_0
    iget-object v2, p0, Lpzn;->a:[Lpzq;

    if-eqz v2, :cond_1

    .line 572
    iget-object v2, p0, Lpzn;->a:[Lpzq;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 573
    if-eqz v4, :cond_0

    .line 574
    const/4 v5, 0x2

    .line 575
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 572
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 579
    :cond_1
    iget-object v1, p0, Lpzn;->c:Lpzu;

    if-eqz v1, :cond_2

    .line 580
    const/4 v1, 0x3

    iget-object v2, p0, Lpzn;->c:Lpzu;

    .line 581
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 583
    :cond_2
    iget-object v1, p0, Lpzn;->d:Lpzt;

    if-eqz v1, :cond_3

    .line 584
    const/4 v1, 0x4

    iget-object v2, p0, Lpzn;->d:Lpzt;

    .line 585
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 587
    :cond_3
    iget-object v1, p0, Lpzn;->e:Lpzp;

    if-eqz v1, :cond_4

    .line 588
    const/4 v1, 0x5

    iget-object v2, p0, Lpzn;->e:Lpzp;

    .line 589
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 591
    :cond_4
    iget-object v1, p0, Lpzn;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 592
    iput v0, p0, Lpzn;->ai:I

    .line 593
    return v0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lpzn;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 601
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 602
    sparse-switch v0, :sswitch_data_0

    .line 606
    iget-object v2, p0, Lpzn;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 607
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lpzn;->ah:Ljava/util/List;

    .line 610
    :cond_1
    iget-object v2, p0, Lpzn;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 612
    :sswitch_0
    return-object p0

    .line 617
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 618
    if-eqz v0, :cond_2

    const/4 v2, 0x1

    if-eq v0, v2, :cond_2

    const/4 v2, 0x2

    if-eq v0, v2, :cond_2

    const/4 v2, 0x3

    if-eq v0, v2, :cond_2

    const/4 v2, 0x4

    if-eq v0, v2, :cond_2

    const/4 v2, 0x5

    if-eq v0, v2, :cond_2

    const/4 v2, 0x6

    if-ne v0, v2, :cond_3

    .line 625
    :cond_2
    iput v0, p0, Lpzn;->b:I

    goto :goto_0

    .line 627
    :cond_3
    iput v1, p0, Lpzn;->b:I

    goto :goto_0

    .line 632
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 633
    iget-object v0, p0, Lpzn;->a:[Lpzq;

    if-nez v0, :cond_5

    move v0, v1

    .line 634
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lpzq;

    .line 635
    iget-object v3, p0, Lpzn;->a:[Lpzq;

    if-eqz v3, :cond_4

    .line 636
    iget-object v3, p0, Lpzn;->a:[Lpzq;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 638
    :cond_4
    iput-object v2, p0, Lpzn;->a:[Lpzq;

    .line 639
    :goto_2
    iget-object v2, p0, Lpzn;->a:[Lpzq;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 640
    iget-object v2, p0, Lpzn;->a:[Lpzq;

    new-instance v3, Lpzq;

    invoke-direct {v3}, Lpzq;-><init>()V

    aput-object v3, v2, v0

    .line 641
    iget-object v2, p0, Lpzn;->a:[Lpzq;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 642
    invoke-virtual {p1}, Loxn;->a()I

    .line 639
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 633
    :cond_5
    iget-object v0, p0, Lpzn;->a:[Lpzq;

    array-length v0, v0

    goto :goto_1

    .line 645
    :cond_6
    iget-object v2, p0, Lpzn;->a:[Lpzq;

    new-instance v3, Lpzq;

    invoke-direct {v3}, Lpzq;-><init>()V

    aput-object v3, v2, v0

    .line 646
    iget-object v2, p0, Lpzn;->a:[Lpzq;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 650
    :sswitch_3
    iget-object v0, p0, Lpzn;->c:Lpzu;

    if-nez v0, :cond_7

    .line 651
    new-instance v0, Lpzu;

    invoke-direct {v0}, Lpzu;-><init>()V

    iput-object v0, p0, Lpzn;->c:Lpzu;

    .line 653
    :cond_7
    iget-object v0, p0, Lpzn;->c:Lpzu;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 657
    :sswitch_4
    iget-object v0, p0, Lpzn;->d:Lpzt;

    if-nez v0, :cond_8

    .line 658
    new-instance v0, Lpzt;

    invoke-direct {v0}, Lpzt;-><init>()V

    iput-object v0, p0, Lpzn;->d:Lpzt;

    .line 660
    :cond_8
    iget-object v0, p0, Lpzn;->d:Lpzt;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 664
    :sswitch_5
    iget-object v0, p0, Lpzn;->e:Lpzp;

    if-nez v0, :cond_9

    .line 665
    new-instance v0, Lpzp;

    invoke-direct {v0}, Lpzp;-><init>()V

    iput-object v0, p0, Lpzn;->e:Lpzp;

    .line 667
    :cond_9
    iget-object v0, p0, Lpzn;->e:Lpzp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 602
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 541
    iget v0, p0, Lpzn;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 542
    const/4 v0, 0x1

    iget v1, p0, Lpzn;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 544
    :cond_0
    iget-object v0, p0, Lpzn;->a:[Lpzq;

    if-eqz v0, :cond_2

    .line 545
    iget-object v1, p0, Lpzn;->a:[Lpzq;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 546
    if-eqz v3, :cond_1

    .line 547
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 545
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 551
    :cond_2
    iget-object v0, p0, Lpzn;->c:Lpzu;

    if-eqz v0, :cond_3

    .line 552
    const/4 v0, 0x3

    iget-object v1, p0, Lpzn;->c:Lpzu;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 554
    :cond_3
    iget-object v0, p0, Lpzn;->d:Lpzt;

    if-eqz v0, :cond_4

    .line 555
    const/4 v0, 0x4

    iget-object v1, p0, Lpzn;->d:Lpzt;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 557
    :cond_4
    iget-object v0, p0, Lpzn;->e:Lpzp;

    if-eqz v0, :cond_5

    .line 558
    const/4 v0, 0x5

    iget-object v1, p0, Lpzn;->e:Lpzp;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 560
    :cond_5
    iget-object v0, p0, Lpzn;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 562
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lpzn;->a(Loxn;)Lpzn;

    move-result-object v0

    return-object v0
.end method
