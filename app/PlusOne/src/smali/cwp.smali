.class final Lcwp;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lbc",
        "<",
        "Lcwk;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Landroid/content/Context;

.field private synthetic b:Lcwd;


# direct methods
.method constructor <init>(Lcwd;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 696
    iput-object p1, p0, Lcwp;->b:Lcwd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 697
    iput-object p2, p0, Lcwp;->a:Landroid/content/Context;

    .line 698
    return-void
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Lcwk;",
            ">;"
        }
    .end annotation

    .prologue
    .line 702
    const-string v0, "local_uri"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 703
    iget-object v1, p0, Lcwp;->b:Lcwd;

    .line 704
    invoke-virtual {v1}, Lcwd;->n()Lz;

    move-result-object v1

    invoke-virtual {v1}, Lz;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1, v0}, Llsb;->b(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 705
    invoke-static {v1}, Llsb;->c(Ljava/lang/String;)Z

    move-result v1

    .line 706
    new-instance v2, Lcwm;

    iget-object v3, p0, Lcwp;->b:Lcwd;

    invoke-virtual {v3}, Lcwd;->n()Lz;

    move-result-object v3

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-direct {v2, v3, v0, v1}, Lcwm;-><init>(Landroid/content/Context;Landroid/net/Uri;I)V

    return-object v2

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public a(Lcwk;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcwk;",
            ")V"
        }
    .end annotation

    .prologue
    .line 712
    iget-object v0, p0, Lcwp;->b:Lcwd;

    invoke-static {v0}, Lcwd;->a(Lcwd;)Lcwl;

    move-result-object v0

    if-nez v0, :cond_0

    .line 716
    :goto_0
    return-void

    .line 715
    :cond_0
    iget-object v0, p0, Lcwp;->b:Lcwd;

    invoke-static {v0}, Lcwd;->a(Lcwd;)Lcwl;

    move-result-object v0

    iget-object v1, p0, Lcwp;->a:Landroid/content/Context;

    invoke-static {v1, p1}, Lcwd;->a(Landroid/content/Context;Lcwk;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcwl;->a(Ljava/util/List;)V

    goto :goto_0
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Lcwk;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 719
    return-void
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 693
    check-cast p2, Lcwk;

    invoke-virtual {p0, p2}, Lcwp;->a(Lcwk;)V

    return-void
.end method
