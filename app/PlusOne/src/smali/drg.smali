.class public final Ldrg;
.super Liab;
.source "PG"


# static fields
.field private static a:J

.field private static b:Z

.field private static final e:[Ljava/lang/String;

.field private static final f:[Landroid/net/Uri;


# instance fields
.field private final c:Landroid/content/Context;

.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 4564
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "local_media_inserted"

    aput-object v1, v0, v3

    const-string v1, "fetched_all_remote_photos"

    aput-object v1, v0, v4

    const-string v1, "have_fingerprints_been_generated"

    aput-object v1, v0, v5

    const-string v1, "received_tickle_since_last_sync"

    aput-object v1, v0, v6

    const-string v1, "synced_initial_max"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "synced_server_max"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "initial_sync_complete"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "pressed_load_more"

    aput-object v2, v0, v1

    sput-object v0, Ldrg;->e:[Ljava/lang/String;

    .line 4575
    new-array v0, v7, [Landroid/net/Uri;

    const-string v1, "content://media/external/images/media"

    .line 4576
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    aput-object v1, v0, v3

    const-string v1, "content://media/phoneStorage/images/media"

    .line 4577
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    aput-object v1, v0, v4

    const-string v1, "content://media/external/video/media"

    .line 4578
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    aput-object v1, v0, v5

    const-string v1, "content://media/phoneStorage/video/media"

    .line 4579
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    aput-object v1, v0, v6

    sput-object v0, Ldrg;->f:[Landroid/net/Uri;

    .line 4575
    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 144
    const-string v0, "es"

    invoke-direct {p0, p1, v0, p2}, Ldrg;-><init>(Landroid/content/Context;Ljava/lang/String;I)V

    .line 145
    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 6

    .prologue
    .line 148
    const/4 v4, 0x0

    const/16 v5, 0x6ab

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v5}, Liab;-><init>(Landroid/content/Context;Ljava/lang/String;ILandroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 150
    if-nez p1, :cond_0

    .line 151
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Context is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 153
    :cond_0
    if-gez p3, :cond_1

    .line 154
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x1f

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Invalid account id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 157
    :cond_1
    iput-object p1, p0, Ldrg;->c:Landroid/content/Context;

    .line 158
    iput p3, p0, Ldrg;->d:I

    .line 160
    new-instance v0, Ldrh;

    invoke-direct {v0, p1, p3}, Ldrh;-><init>(Landroid/content/Context;I)V

    invoke-static {v0}, Llsx;->a(Ljava/lang/Runnable;)V

    .line 166
    return-void
.end method

.method private A(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 2955
    const-string v0, "DELETE FROM tile_requests WHERE view_id NOT IN ( SELECT DISTINCT view_id FROM all_tiles WHERE media_attr & 512 == 0 )"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2957
    return-void
.end method

.method private B(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3

    .prologue
    .line 2960
    iget-object v0, p0, Ldrg;->c:Landroid/content/Context;

    const v1, 0x7f0a06fa

    .line 2961
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x40

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "UPDATE circles SET circle_name = \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' WHERE type = \'v.all.circles\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2960
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2962
    return-void
.end method

.method private C(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 2965
    const-string v0, "ALTER TABLE account_status ADD COLUMN next_notifications_fetch_param BLOB"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2966
    const-string v0, "UPDATE account_status SET last_viewed_notification_version=0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2967
    const-string v0, "DELETE FROM guns"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2968
    return-void
.end method

.method private D(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 2971
    const-string v0, "DROP TABLE IF EXISTS guns;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2972
    const-string v0, "CREATE TABLE guns ( _id INTEGER, key TEXT UNIQUE NOT NULL, creation_time INT NOT NULL, collapsed_description TEXT, collapsed_destination TEXT, collapsed_heading TEXT, collapsed_annotation TEXT, collapsed_icon TEXT, entity_reference TEXT, entity_reference_type TEXT, priority TEXT, read_state INT NOT NULL DEFAULT(0), type INT NOT NULL DEFAULT(0), category INT NOT NULL DEFAULT(0), seen INT NOT NULL DEFAULT(0), actors BLOB, activity_id TEXT, event_id TEXT, album_id TEXT, community_id TEXT, display_index INT NOT NULL DEFAULT(0), updated_version INT NOT NULL DEFAULT(0), push_enabled INT NOT NULL DEFAULT(0), expanded_info BLOB );"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2997
    return-void
.end method

.method private E(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3000
    const-string v0, "UPDATE account_status SET last_notification_sync_version=0, unviewed_notifications_count=0, has_unread_notifications=0, last_viewed_notification_version=0, next_notifications_fetch_param=NULL"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3006
    return-void
.end method

.method private F(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3009
    const-string v0, "ALTER TABLE guns ADD COLUMN photos BLOB"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3010
    return-void
.end method

.method private G(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3013
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3014
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3015
    const-string v0, "DROP TABLE IF EXISTS activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3016
    const-string v0, "CREATE TABLE activities (_id INTEGER PRIMARY KEY, activity_id TEXT UNIQUE NOT NULL, data_state INT NOT NULL DEFAULT (0), author_id TEXT NOT NULL, source_id TEXT, source_name TEXT, total_comment_count INT NOT NULL, plus_one_data BLOB, acl_display TEXT, loc BLOB, created INT NOT NULL, modified INT NOT NULL, whats_hot BLOB, social_friends_plus_oned BLOB, content_flags INT NOT NULL DEFAULT(0), activity_flags INT NOT NULL DEFAULT(0), annotation TEXT, annotation_plaintext TEXT, title TEXT, title_plaintext TEXT, original_author_id TEXT, original_author_name TEXT, original_author_avatar_url TEXT, comment BLOB, permalink TEXT, event_id TEXT, photo_collection BLOB, square_update BLOB, square_reshare_update BLOB, relateds BLOB, num_reshares INT NOT NULL DEFAULT(0), embed_deep_link BLOB, album_id TEXT, embed_media BLOB, embed_photo_album BLOB, embed_checkin BLOB, embed_place BLOB, embed_place_review BLOB, embed_skyjam BLOB, embed_appinvite BLOB, embed_hangout BLOB, embed_square BLOB, embed_emotishare BLOB, embed_google_offer_v2 BLOB, promo BLOB);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3062
    return-void
.end method

.method private H(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3065
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3066
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3067
    const-string v0, "DROP TABLE IF EXISTS activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3068
    const-string v0, "CREATE TABLE activities (_id INTEGER PRIMARY KEY, activity_id TEXT UNIQUE NOT NULL, data_state INT NOT NULL DEFAULT (0), author_id TEXT NOT NULL, source_id TEXT, source_name TEXT, total_comment_count INT NOT NULL, plus_one_data BLOB, acl_display TEXT, loc BLOB, created INT NOT NULL, modified INT NOT NULL, whats_hot BLOB, social_friends_plus_oned BLOB, content_flags INT NOT NULL DEFAULT(0), activity_flags INT NOT NULL DEFAULT(0), annotation TEXT, annotation_plaintext TEXT, title TEXT, title_plaintext TEXT, original_author_id TEXT, original_author_name TEXT, original_author_avatar_url TEXT, comment BLOB, permalink TEXT, event_id TEXT, photo_collection BLOB, album_id TEXT, square_update BLOB, square_reshare_update BLOB, relateds BLOB, num_reshares INT NOT NULL DEFAULT(0), embed BLOB, embed_deep_link BLOB, embed_appinvite BLOB, promo BLOB);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3105
    return-void
.end method

.method private I(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3108
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3109
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3110
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3111
    const-string v0, "ALTER TABLE activities ADD COLUMN domain TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3112
    return-void
.end method

.method private J(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3115
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3116
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3117
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3118
    return-void
.end method

.method private K(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3121
    const-string v0, "ALTER TABLE circles ADD COLUMN for_sharing INT DEFAULT(0)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3123
    const-string v0, "UPDATE circles SET for_sharing=1 WHERE semantic_hints & 64 != 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3124
    return-void
.end method

.method private L(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3127
    const-string v0, "ALTER TABLE account_status ADD COLUMN circle_settings_sync_time INT DEFAULT(-1)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3129
    return-void
.end method

.method private M(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3132
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3133
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3134
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3135
    return-void
.end method

.method private N(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3138
    const-string v0, "DELETE FROM network_data_transactions"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3139
    const-string v0, "ALTER TABLE network_data_transactions ADD COLUMN log_file TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3140
    return-void
.end method

.method private O(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3143
    const-string v0, "ALTER TABLE guns ADD COLUMN pending_read INT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3144
    const-string v0, "ALTER TABLE guns ADD COLUMN pending_display_index INT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3145
    return-void
.end method

.method private P(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3148
    const-string v0, "UPDATE account_status SET people_sync_time=-1"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3149
    const-string v0, "UPDATE account_status SET circle_sync_time=-1"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3150
    const-string v0, "ALTER TABLE contacts ADD COLUMN in_same_visibility_group INT DEFAULT(0)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3152
    return-void
.end method

.method private Q(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3155
    const-string v0, "DROP TABLE IF EXISTS realtimechat_metadata"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3156
    const-string v0, "DROP TABLE IF EXISTS conversations"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3157
    const-string v0, "DROP TABLE IF EXISTS participants"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3158
    const-string v0, "DROP TABLE IF EXISTS messages"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3159
    const-string v0, "DROP TABLE IF EXISTS conversation_participants"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3160
    const-string v0, "DROP TABLE IF EXISTS messenger_suggestions"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3161
    const-string v0, "DROP TABLE IF EXISTS hangout_suggestions"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3162
    return-void
.end method

.method private R(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3165
    const-string v0, "DELETE FROM all_tiles"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3166
    const-string v0, "DELETE FROM tile_requests"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3167
    const-string v0, "DELETE FROM photo_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3168
    return-void
.end method

.method private S(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3171
    const-string v0, "UPDATE account_status SET audience_data=NULL"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3172
    const-string v0, "UPDATE account_status SET audience_history=NULL"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3173
    return-void
.end method

.method private T(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3176
    const-string v0, "DELETE FROM profiles"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3177
    const-string v0, "ALTER TABLE profiles ADD COLUMN videos_data_proto BLOB"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3178
    const-string v0, "ALTER TABLE profiles ADD COLUMN reviews_data_proto BLOB"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3179
    return-void
.end method

.method private U(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3182
    const-string v0, "ALTER TABLE account_status ADD COLUMN circle_fingerprint INT DEFAULT(-1)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3184
    const-string v0, "ALTER TABLE account_status ADD COLUMN people_fingerprint INT DEFAULT(-1)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3186
    return-void
.end method

.method private V(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3189
    const-string v0, "DROP TABLE IF EXISTS notifications"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3190
    const-string v0, "DROP TABLE IF EXISTS circle_action"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3191
    const-string v0, "CREATE TABLE circled_me_users (gaia_id TEXT NOT NULL,notification_key INT NOT NULL, UNIQUE (gaia_id, notification_key), FOREIGN KEY (notification_key) REFERENCES guns (key) ON DELETE CASCADE);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3196
    return-void
.end method

.method private W(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3199
    const-string v0, "UPDATE guns SET pending_read=0 WHERE pending_read IS NULL"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3200
    return-void
.end method

.method private X(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3203
    const-string v0, "DELETE FROM events"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3204
    const-string v0, "DELETE FROM event_activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3205
    const-string v0, "DELETE FROM event_people"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3206
    const-string v0, "ALTER TABLE events ADD COLUMN creator_gaia_id INT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3207
    return-void
.end method

.method private Y(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3210
    const-string v0, "ALTER TABLE account_status ADD COLUMN cover_photo_spec BLOB"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3212
    const-string v0, "DELETE FROM profiles"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3213
    return-void
.end method

.method private Z(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3216
    const-string v0, "DELETE FROM all_tiles"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3217
    const-string v0, "DELETE FROM tile_requests"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3218
    const-string v0, "DELETE FROM photo_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3219
    return-void
.end method

.method static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)J
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 1730
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "COUNT(*)"

    aput-object v0, v2, v1

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 1735
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1736
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 1741
    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 1744
    return-wide v0

    .line 1738
    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0

    .line 1741
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static a(Landroid/content/Context;I)Ldrg;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 139
    const-class v0, Lhzr;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhzr;

    .line 140
    invoke-virtual {v0, p0, p1}, Lhzr;->b(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteOpenHelper;

    move-result-object v0

    check-cast v0, Ldrg;

    return-object v0
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I[Ljava/lang/String;[Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 4796
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3, v7}, Landroid/content/ContentValues;-><init>(I)V

    .line 4798
    if-eqz p4, :cond_0

    .line 4799
    const-string v2, "partition_name"

    invoke-virtual {v3, v2, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4800
    array-length v4, p4

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v5, p4, v2

    .line 4801
    const-string v6, "table_name"

    invoke-virtual {v3, v6, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4802
    const-string v5, "partition_tables"

    invoke-virtual {p1, v5, v1, v3}, Landroid/database/sqlite/SQLiteDatabase;->replace(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 4800
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 4806
    :cond_0
    if-eqz p5, :cond_1

    .line 4807
    invoke-virtual {v3}, Landroid/content/ContentValues;->clear()V

    .line 4808
    const-string v2, "partition_name"

    invoke-virtual {v3, v2, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4809
    array-length v2, p5

    :goto_1
    if-ge v0, v2, :cond_1

    aget-object v4, p5, v0

    .line 4810
    const-string v5, "view_name"

    invoke-virtual {v3, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4811
    const-string v4, "partition_views"

    invoke-virtual {p1, v4, v1, v3}, Landroid/database/sqlite/SQLiteDatabase;->replace(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 4809
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 4816
    :cond_1
    invoke-virtual {v3}, Landroid/content/ContentValues;->clear()V

    .line 4817
    const-string v0, "partition_name"

    invoke-virtual {v3, v0, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4818
    const-string v0, "version"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v3, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4819
    const-string v0, "partition_versions"

    invoke-virtual {p1, v0, v1, v3}, Landroid/database/sqlite/SQLiteDatabase;->replace(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 4821
    const-string v0, "EsDatabaseHelper"

    invoke-static {v0, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 4822
    if-eqz p4, :cond_4

    .line 4824
    invoke-static {p4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    :goto_2
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    if-eqz p5, :cond_2

    .line 4825
    invoke-static {p5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    :cond_2
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x39

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "insertPartition "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", version: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", tables: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", views: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4822
    :cond_3
    return-void

    :cond_4
    move-object v0, v1

    .line 4824
    goto :goto_2
.end method

.method private aA(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3478
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3479
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3480
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3481
    return-void
.end method

.method private aB(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3484
    const-string v0, "UPDATE account_status SET people_sync_time=-1, people_last_update_token=null"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3485
    const-string v0, "UPDATE contacts SET last_updated_time=-1"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3486
    return-void
.end method

.method private aC(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3490
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3491
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3492
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3493
    return-void
.end method

.method private aD(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3497
    const-string v0, "DELETE FROM profiles"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3498
    const-string v0, "UPDATE account_status SET people_sync_time=-1, people_last_update_token=null"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3499
    const-string v0, "UPDATE contacts SET last_updated_time=-1"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3500
    return-void
.end method

.method private aE(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3505
    const-string v0, "DROP TABLE IF EXISTS suggestion_events"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3506
    const-string v0, "CREATE TABLE suggestion_events (action_type INT, person_id BLOB, suggestion_id BLOB, suggestion_ui INT, timestamp INT)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3512
    return-void
.end method

.method private aF(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3516
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3517
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3518
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3519
    return-void
.end method

.method private aG(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3523
    const-string v0, "DELETE FROM profiles"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3524
    const-string v0, "UPDATE account_status SET people_sync_time=-1, people_last_update_token=null"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3525
    const-string v0, "UPDATE contacts SET last_updated_time=-1"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3526
    return-void
.end method

.method private aH(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3530
    const-string v0, "DELETE FROM profiles"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3531
    const-string v0, "UPDATE account_status SET people_sync_time=-1, people_last_update_token=null"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3532
    const-string v0, "UPDATE contacts SET last_updated_time=-1"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3533
    return-void
.end method

.method private aI(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3537
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3538
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3539
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3540
    return-void
.end method

.method private aJ(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3543
    const-string v0, "DROP INDEX IF EXISTS tile_idx"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3544
    const-string v0, "CREATE INDEX tile_idx ON all_tiles ( view_id, view_order, type, cluster_id, tile_id, title, subtitle, image_url, image_width, image_height, cluster_count, comment_count, plusone_count, acl, user_actions, media_attr, timestamp, last_refresh_time, parent_id );"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3566
    return-void
.end method

.method private aK(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3570
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3571
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3572
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3573
    return-void
.end method

.method private aL(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3577
    const-string v0, "ALTER TABLE activities ADD COLUMN author_annotation BLOB"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3578
    return-void
.end method

.method private aM(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3581
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3582
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3583
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3584
    return-void
.end method

.method private aN(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3588
    const-string v0, "ALTER TABLE activity_comments ADD COLUMN flagged BOOLEAN NOT NULL DEFAULT(0)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3590
    const-string v0, "ALTER TABLE photo_comments ADD COLUMN flagged BOOLEAN NOT NULL DEFAULT(0)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3592
    return-void
.end method

.method private aO(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3595
    const-string v0, "ALTER TABLE circles ADD COLUMN last_volume_sync INT NOT NULL DEFAULT(-1)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3596
    const-string v0, "ALTER TABLE squares ADD COLUMN last_volume_sync INT NOT NULL DEFAULT(-1)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3597
    return-void
.end method

.method private aP(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3601
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3602
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3603
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3604
    return-void
.end method

.method private aQ(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3607
    const-string v0, "DROP TABLE IF EXISTS guns;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3608
    const-string v0, "CREATE TABLE guns ( _id INTEGER, key TEXT UNIQUE NOT NULL, creation_time INT NOT NULL, collapsed_description TEXT, collapsed_destination TEXT, collapsed_heading TEXT, collapsed_annotation TEXT, collapsed_icon TEXT, entity_reference TEXT, entity_reference_type TEXT, priority TEXT, read_state INT NOT NULL DEFAULT(0), type INT NOT NULL DEFAULT(0), category INT NOT NULL DEFAULT(0), seen INT NOT NULL DEFAULT(0), actors BLOB, activity_id TEXT, event_id TEXT, album_id TEXT, community_id TEXT, updated_version INT NOT NULL DEFAULT(0), push_enabled INT NOT NULL DEFAULT(0), expanded_info BLOB,photos BLOB,pending_read INT NOT NULL DEFAULT(0));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3636
    const-string v0, "UPDATE account_status SET last_notification_sync_version=0, unviewed_notifications_count=0, has_unread_notifications=0, last_viewed_notification_version=0, next_notifications_fetch_param=NULL"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3642
    return-void
.end method

.method private aR(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3646
    const-string v0, "DELETE FROM analytics_events"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3647
    return-void
.end method

.method private aS(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3650
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3651
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3652
    const-string v0, "DROP TABLE IF EXISTS activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3653
    const-string v0, "CREATE TABLE activities (_id INTEGER PRIMARY KEY,activity_id TEXT UNIQUE NOT NULL,data_state INT NOT NULL DEFAULT (0),author_id TEXT NOT NULL,source_id TEXT,source_name TEXT,total_comment_count INT NOT NULL,plus_one_data BLOB,acl_display TEXT,loc BLOB,created INT NOT NULL,modified INT NOT NULL,whats_hot BLOB,social_friends_plus_oned BLOB,content_flags INT NOT NULL DEFAULT(0),activity_flags INT NOT NULL DEFAULT(0),annotation BLOB,annotation_plaintext TEXT,title BLOB,title_plaintext TEXT,original_author_id TEXT,original_author_name TEXT,original_author_avatar_url TEXT,comment BLOB,permalink TEXT,event_id TEXT,album_id TEXT,square_update BLOB,square_reshare_update BLOB,relateds BLOB,num_reshares INT NOT NULL DEFAULT(0),embed BLOB,embed_deep_link BLOB,embed_appinvite BLOB,promo BLOB,domain TEXT,explanation_activity_id TEXT,birthday BLOB,author_annotation BLOB);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3692
    return-void
.end method

.method private aT(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3696
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3697
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3698
    const-string v0, "DROP TABLE IF EXISTS activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3699
    const-string v0, "CREATE TABLE activities (_id INTEGER PRIMARY KEY,activity_id TEXT UNIQUE NOT NULL,data_state INT NOT NULL DEFAULT (0),author_id TEXT NOT NULL,source_id TEXT,source_name TEXT,total_comment_count INT NOT NULL,plus_one_data BLOB,acl_display TEXT,loc BLOB,created INT NOT NULL,modified INT NOT NULL,whats_hot BLOB,social_friends_plus_oned BLOB,content_flags INT NOT NULL DEFAULT(0),activity_flags INT NOT NULL DEFAULT(0),annotation BLOB,title BLOB,original_author_id TEXT,original_author_name TEXT,original_author_avatar_url TEXT,comment BLOB,permalink TEXT,event_id TEXT,album_id TEXT,square_update BLOB,square_reshare_update BLOB,relateds BLOB,num_reshares INT NOT NULL DEFAULT(0),embed BLOB,embed_deep_link BLOB,embed_appinvite BLOB,promo BLOB,domain TEXT,explanation_activity_id TEXT,birthday BLOB,author_annotation BLOB);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3736
    return-void
.end method

.method private aU(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3739
    const-string v0, "ALTER TABLE activities ADD COLUMN original_activity_url TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3740
    return-void
.end method

.method private aV(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3744
    const-string v0, "DELETE FROM events"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3745
    const-string v0, "DELETE FROM event_activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3746
    const-string v0, "DELETE FROM event_people"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3747
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3748
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3749
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3750
    return-void
.end method

.method private aW(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3754
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3755
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3756
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3758
    const-string v0, "DROP TABLE IF EXISTS emotishare_data"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3759
    const-string v0, "CREATE TABLE emotishare_data (_id INTEGER PRIMARY KEY AUTOINCREMENT,type INTEGER UNIQUE ON CONFLICT REPLACE,data BLOB,generation INT DEFAULT(-1));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3765
    return-void
.end method

.method private aX(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4

    .prologue
    .line 3768
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 3770
    :try_start_0
    const-string v0, "ALTER TABLE account_status RENAME TO tmp_table"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3771
    const-string v0, "CREATE TABLE account_status(user_id TEXT,last_sync_time INT DEFAULT(-1),last_stats_sync_time INT DEFAULT(-1),last_contacted_time INT DEFAULT(-1),wipeout_stats INT DEFAULT(0),circle_sync_time INT DEFAULT(-1),circle_fingerprint INT DEFAULT(-1),circle_settings_sync_time INT DEFAULT(-1),people_sync_time INT DEFAULT(-1),people_fingerprint INT DEFAULT(-1),people_last_update_token TEXT,suggested_people_sync_time INT DEFAULT(-1),blocked_people_sync_time INT DEFAULT(-1),event_list_sync_time INT DEFAULT(-1),event_themes_sync_time INT DEFAULT(-1),cover_photo_spec BLOB,audience_data BLOB,audience_history BLOB,contacts_sync_version INT DEFAULT(0),push_notifications INT DEFAULT(0),last_analytics_sync_time INT DEFAULT(-1),last_settings_sync_time INT DEFAULT(-1),last_squares_sync_time INT DEFAULT(-1),last_emotishare_sync_time INT DEFAULT(-1),last_notification_sync_version INT DEFAULT(0), unviewed_notifications_count INT DEFAULT(0), has_unread_notifications INT DEFAULT(0), last_viewed_notification_version INT DEFAULT(0), next_read_notifications_fetch_param BLOB, next_unread_notifications_fetch_param BLOB, last_read_notifications_sync_time INT DEFAULT(-1));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3804
    const-string v0, "user_id,last_sync_time,last_stats_sync_time,last_contacted_time,wipeout_stats,circle_sync_time,circle_fingerprint,circle_settings_sync_time,people_sync_time,people_fingerprint,people_last_update_token,suggested_people_sync_time,blocked_people_sync_time,event_list_sync_time,event_themes_sync_time,cover_photo_spec,audience_data,audience_history,contacts_sync_version,push_notifications,last_analytics_sync_time,last_settings_sync_time,last_squares_sync_time,last_emotishare_sync_time,last_notification_sync_version, unviewed_notifications_count, has_unread_notifications, last_viewed_notification_version"

    .line 3832
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x35

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "INSERT INTO account_status ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") SELECT "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " FROM tmp_table;"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3834
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3836
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 3837
    return-void

    .line 3836
    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method private aY(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3841
    const-string v0, "DROP TABLE IF EXISTS squares"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3842
    const-string v0, "CREATE TABLE squares (_id INTEGER PRIMARY KEY,square_id TEXT UNIQUE NOT NULL,square_name TEXT,tagline TEXT,photo_url TEXT,about_text TEXT,joinability INT NOT NULL DEFAULT(0),member_count INT NOT NULL DEFAULT(0),membership_status INT NOT NULL DEFAULT(0),is_member INT NOT NULL DEFAULT(0),list_category INT NOT NULL DEFAULT(0),post_visibility INT NOT NULL DEFAULT(-1),can_see_members INT NOT NULL DEFAULT(0),can_see_posts INT NOT NULL DEFAULT(0),can_join INT NOT NULL DEFAULT(0),can_request_to_join INT NOT NULL DEFAULT(0),can_share INT NOT NULL DEFAULT(0),can_invite INT NOT NULL DEFAULT(0),notifications_enabled INT NOT NULL DEFAULT(0),square_streams BLOB,inviter_gaia_id TEXT,sort_index INT NOT NULL DEFAULT(0),last_sync INT DEFAULT(-1),last_members_sync INT DEFAULT(-1),invitation_dismissed INT NOT NULL DEFAULT(0),auto_subscribe INT NOT NULL DEFAULT(0),disable_subscription INT NOT NULL DEFAULT(0),unread_count INT NOT NULL DEFAULT(0),volume INT,suggestion_id TEXT,last_volume_sync INT NOT NULL DEFAULT(-1),restricted_domain TEXT  );"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3875
    const-string v0, "UPDATE account_status SET last_squares_sync_time=-1"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3876
    return-void
.end method

.method private aZ(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3879
    const-string v0, "ALTER TABLE guns ADD COLUMN pending_delete INT NOT NULL DEFAULT(0)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3880
    return-void
.end method

.method private aa(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3222
    const-string v0, "DELETE FROM all_tiles"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3223
    const-string v0, "DELETE FROM tile_requests"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3224
    const-string v0, "DELETE FROM photo_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3225
    return-void
.end method

.method private ab(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3228
    const-string v0, "CREATE TABLE sync_status (sync_data_kind INTEGER PRIMARY KEY, last_sync INT NOT NULL);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3231
    return-void
.end method

.method private ac(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3234
    const-string v0, "CREATE TABLE shared_collections (_id TEXT UNIQUE NOT NULL);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3235
    return-void
.end method

.method private ad(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3238
    const-string v0, "DELETE FROM all_tiles"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3239
    const-string v0, "DELETE FROM tile_requests"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3240
    const-string v0, "DELETE FROM photo_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3241
    return-void
.end method

.method private ae(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3244
    const-string v0, "DROP TABLE IF EXISTS events"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3245
    const-string v0, "DELETE FROM event_activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3246
    const-string v0, "DELETE FROM event_people"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3247
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3248
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3249
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3250
    const-string v0, "CREATE TABLE events (_id INTEGER PRIMARY KEY AUTOINCREMENT, event_id TEXT UNIQUE NOT NULL, activity_id TEXT UNIQUE, name TEXT, source INT, creator_gaia_id TEXT, update_timestamp INT, refresh_timestamp INT, activity_refresh_timestamp INT, invitee_roster_timestamp INT, fingerprint INT NOT NULL DEFAULT(0), start_time INT NOT NULL, end_time INT NOT NULL, instant_share_end_time INT, can_invite_people INT DEFAULT (0), can_post_photos INT DEFAULT (0), can_comment INT DEFAULT(0) NOT NULL, mine INT DEFAULT (0) NOT NULL, polling_token TEXT,resume_token TEXT,display_time INT DEFAULT (0),event_data BLOB, plus_one_data BLOB, invitee_roster BLOB, deleted INT DEFAULT (0) );"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3277
    return-void
.end method

.method private af(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3

    .prologue
    .line 3283
    :try_start_0
    const-string v0, "ALTER TABLE account_status ADD COLUMN circle_sync_time INT DEFAULT(-1)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3290
    :goto_0
    :try_start_1
    const-string v0, "ALTER TABLE account_status ADD COLUMN people_sync_time INT DEFAULT(-1)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 3295
    :goto_1
    return-void

    .line 3285
    :catch_0
    move-exception v0

    .line 3286
    const-string v1, "EsDatabaseHelper"

    const-string v2, "Failed to add circle_sync_time column to account_status table."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 3292
    :catch_1
    move-exception v0

    .line 3293
    const-string v1, "EsDatabaseHelper"

    const-string v2, "Failed to add people_sync_time column to account_status table."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method private ag(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3298
    const-string v0, "ALTER TABLE contacts ADD COLUMN verified INT DEFAULT(0)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3300
    return-void
.end method

.method private ah(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3303
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3304
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3305
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3306
    return-void
.end method

.method private ai(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3309
    const-string v0, "UPDATE account_status SET people_sync_time=-1, people_last_update_token=null"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3310
    const-string v0, "UPDATE contacts SET last_updated_time=-1"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3311
    return-void
.end method

.method private aj(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3314
    const-string v0, "DELETE FROM all_tiles"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3315
    const-string v0, "DELETE FROM tile_requests"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3316
    const-string v0, "DELETE FROM photo_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3317
    return-void
.end method

.method private ak(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 3323
    .line 3325
    :try_start_0
    const-string v1, "guns"

    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "key"

    aput-object v3, v2, v0

    const/4 v0, 0x1

    const-string v3, "actors"

    aput-object v3, v2, v0

    const-string v3, "type=6"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 3333
    if-eqz v1, :cond_2

    .line 3334
    :cond_0
    :goto_0
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3335
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 3336
    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    .line 3337
    invoke-static {v2}, Ldqu;->a([B)Ljava/util/List;

    move-result-object v2

    .line 3338
    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 3339
    invoke-static {p1, v0, v2}, Ldsm;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/util/List;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 3343
    :catch_0
    move-exception v0

    .line 3344
    :goto_1
    :try_start_2
    const-string v2, "EsDatabaseHelper"

    const-string v3, "Failed to add actors from existing circle add notifications to the contacts table"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 3347
    if-eqz v1, :cond_1

    .line 3348
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 3351
    :cond_1
    :goto_2
    return-void

    .line 3347
    :cond_2
    if-eqz v1, :cond_1

    .line 3348
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 3347
    :catchall_0
    move-exception v0

    move-object v1, v8

    :goto_3
    if-eqz v1, :cond_3

    .line 3348
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 3347
    :catchall_1
    move-exception v0

    goto :goto_3

    .line 3343
    :catch_1
    move-exception v0

    move-object v1, v8

    goto :goto_1
.end method

.method private al(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3354
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3355
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3356
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3357
    return-void
.end method

.method private am(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3360
    const-string v0, "UPDATE account_status SET last_squares_sync_time=-1"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3361
    const-string v0, "DELETE FROM squares"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3362
    const-string v0, "ALTER TABLE squares ADD COLUMN is_restricted BOOLEAN DEFAULT \'0\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3363
    return-void
.end method

.method private an(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3366
    const-string v0, "ALTER TABLE activities ADD COLUMN explanation_activity_id TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3367
    return-void
.end method

.method private ao(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3370
    const-string v0, "UPDATE account_status SET audience_data=null"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3371
    return-void
.end method

.method private ap(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3374
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3375
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3376
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3377
    return-void
.end method

.method private aq(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3380
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3381
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3382
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3383
    return-void
.end method

.method private ar(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3386
    const-string v0, "UPDATE account_status SET last_squares_sync_time=-1"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3387
    const-string v0, "DROP TABLE IF EXISTS squares"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3388
    const-string v0, "CREATE TABLE squares (_id INTEGER PRIMARY KEY,square_id TEXT UNIQUE NOT NULL,square_name TEXT,tagline TEXT,photo_url TEXT,about_text TEXT,joinability INT NOT NULL DEFAULT(0),member_count INT NOT NULL DEFAULT(0),membership_status INT NOT NULL DEFAULT(0),is_member INT NOT NULL DEFAULT(0),list_category INT NOT NULL DEFAULT(0),post_visibility INT NOT NULL DEFAULT(-1),can_see_members INT NOT NULL DEFAULT(0),can_see_posts INT NOT NULL DEFAULT(0),can_join INT NOT NULL DEFAULT(0),can_request_to_join INT NOT NULL DEFAULT(0),can_share INT NOT NULL DEFAULT(0),can_invite INT NOT NULL DEFAULT(0),notifications_enabled INT NOT NULL DEFAULT(0),square_streams BLOB,inviter_gaia_id TEXT,sort_index INT NOT NULL DEFAULT(0),last_sync INT DEFAULT(-1),last_members_sync INT DEFAULT(-1),invitation_dismissed INT NOT NULL DEFAULT(0),auto_subscribe INT NOT NULL DEFAULT(0),disable_subscription INT NOT NULL DEFAULT(0),unread_count INT NOT NULL DEFAULT(0),volume INT,is_restricted BOOLEAN DEFAULT \'0\');"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3419
    return-void
.end method

.method private as(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3422
    const-string v0, "UPDATE account_status SET audience_history=null"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3423
    return-void
.end method

.method private at(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3426
    const-string v0, "ALTER TABLE people_suggestion_events RENAME TO suggestion_events"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3428
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3429
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3430
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3431
    return-void
.end method

.method private au(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3434
    const-string v0, "ALTER TABLE squares ADD COLUMN suggestion_id TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3435
    return-void
.end method

.method private av(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3438
    const-string v0, "ALTER TABLE activities ADD COLUMN birthday BLOB"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3439
    return-void
.end method

.method private aw(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3442
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3443
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3444
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3445
    return-void
.end method

.method private ax(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3448
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3449
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3450
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3451
    return-void
.end method

.method private ay(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3456
    const-string v0, "DELETE FROM events"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3457
    const-string v0, "DELETE FROM event_activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3458
    const-string v0, "DELETE FROM event_people"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3462
    const-string v0, "DELETE FROM guns"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3466
    const-string v0, "DELETE FROM profiles"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3467
    const-string v0, "UPDATE account_status SET people_sync_time=-1, people_last_update_token=null"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3468
    const-string v0, "UPDATE contacts SET last_updated_time=-1"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3469
    return-void
.end method

.method private az(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3472
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3473
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3474
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3475
    return-void
.end method

.method static synthetic b(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 57
    sget-boolean v0, Ldrg;->b:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->b(Landroid/content/Context;)V

    invoke-static {p0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->a(Landroid/content/Context;)V

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->b(Landroid/content/Context;I)V

    invoke-static {p0}, Lcom/google/android/libraries/social/autobackup/AutoBackupSyncService;->a(Landroid/content/Context;)V

    invoke-static {p0}, Lcom/google/android/apps/photos/service/GooglePhotoDownsyncService;->a(Landroid/content/Context;)V

    const/4 v0, 0x1

    sput-boolean v0, Ldrg;->b:Z

    :cond_0
    return-void
.end method

.method public static b()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 192
    sget-wide v2, Ldrg;->a:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 196
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sget-wide v4, Ldrg;->a:J

    sub-long/2addr v2, v4

    const-wide/32 v4, 0xea60

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private bA(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 4142
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4143
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4144
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4145
    return-void
.end method

.method private bB(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 4149
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4150
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4151
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4152
    return-void
.end method

.method private bC(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 4155
    const-string v0, "ALTER TABLE account_status ADD COLUMN last_notification_heavy_tickle_version INT DEFAULT(0)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4157
    const-string v0, "ALTER TABLE account_status ADD COLUMN gcm_push_notifications INT DEFAULT(0)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4159
    return-void
.end method

.method private bD(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 4162
    const-string v0, "ALTER TABLE all_tiles ADD COLUMN content_url"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4163
    const-string v0, "DROP INDEX IF EXISTS tile_idx"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4164
    const-string v0, "CREATE INDEX tile_idx ON all_tiles ( view_id, view_order, type, cluster_id, tile_id, title, subtitle, image_url, image_width, image_height, cluster_count, comment_count, plusone_count, acl, user_actions, media_attr, timestamp, last_refresh_time, parent_id, photographer_gaia_id, photographer_avatar_url, content_url);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4191
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "photo_id"

    aput-object v0, v2, v1

    const-string v0, "data"

    aput-object v0, v2, v3

    .line 4196
    const-string v3, "media_attr & 32 > 0"

    .line 4197
    const-string v1, "all_tiles"

    move-object v0, p1

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 4200
    :try_start_0
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 4201
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 4202
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 4203
    new-instance v0, Lnym;

    invoke-direct {v0}, Lnym;-><init>()V

    const/4 v3, 0x1

    .line 4205
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    .line 4204
    invoke-static {v0, v3}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lnym;

    .line 4206
    invoke-static {v0}, Llmz;->a(Lnym;)Ljava/lang/String;

    move-result-object v0

    .line 4208
    invoke-virtual {v2}, Landroid/content/ContentValues;->clear()V

    .line 4209
    if-eqz v0, :cond_0

    .line 4210
    const-string v3, "content_url"

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4220
    const-string v0, "all_tiles"

    const-string v3, "photo_id = ?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    .line 4221
    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v6, v7

    .line 4220
    invoke-virtual {p1, v0, v2, v3, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 4224
    :catch_0
    move-exception v0

    :try_start_1
    invoke-direct {p0, p1}, Ldrg;->bE(Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4229
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 4230
    :goto_1
    return-void

    .line 4212
    :cond_0
    :try_start_2
    const-string v0, "EsDatabaseHelper"

    const/4 v2, 0x5

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4213
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v2, 0x3b

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "No video url for something that should "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 4217
    :cond_1
    invoke-direct {p0, p1}, Ldrg;->bE(Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_2
    .catch Loxt; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 4229
    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private bE(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 4234
    const-string v0, "DELETE FROM all_tiles"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4235
    const-string v0, "DELETE FROM tile_requests"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4236
    const-string v0, "DELETE FROM photo_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4237
    return-void
.end method

.method private bF(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 4241
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4242
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4243
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4244
    return-void
.end method

.method private bG(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 4251
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4252
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4253
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4254
    const-string v0, "ALTER TABLE activity_streams ADD COLUMN featured_update BLOB"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4255
    return-void
.end method

.method private bH(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 4271
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4272
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4273
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4274
    return-void
.end method

.method private bI(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 4277
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4278
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4279
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4280
    return-void
.end method

.method private bJ(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 4284
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4285
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4286
    const-string v0, "DROP TABLE IF EXISTS activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4287
    const-string v0, "CREATE TABLE activities (_id INTEGER PRIMARY KEY,activity_id TEXT UNIQUE NOT NULL,data_state INT NOT NULL DEFAULT (0),author_id TEXT NOT NULL,source_id TEXT,source_name TEXT,total_comment_count INT NOT NULL,plus_one_data BLOB,acl_display TEXT,loc BLOB,created INT NOT NULL,modified INT NOT NULL,whats_hot BLOB,social_friends_plus_oned BLOB,content_flags INT NOT NULL DEFAULT(0),activity_flags INT NOT NULL DEFAULT(0),annotation BLOB,title BLOB,original_author_id TEXT,original_author_name TEXT,original_author_avatar_url TEXT,comment BLOB,permalink TEXT,event_id TEXT,square_update BLOB,square_reshare_update BLOB,relateds BLOB,num_reshares INT NOT NULL DEFAULT(0),embed BLOB,embed_deep_link BLOB,embed_appinvite BLOB,promo BLOB,domain TEXT,explanation_activity_id TEXT,birthday BLOB,original_activity_url TEXT,author_annotation BLOB);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4324
    return-void
.end method

.method private bK(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 4327
    const-string v0, "CREATE TABLE all_photos (_id INTEGER PRIMARY KEY AUTOINCREMENT, photo_id INTEGER, image_url TEXT, is_from_autobackup BOOLEAN DEFAULT \'0\', comment_count INTEGER, plusone_count INTEGER, data BLOB, local_file_path TEXT, local_content_uri TEXT, fingerprint TEXT, timestamp INTEGER NOT NULL DEFAULT \'0\', media_attr INTEGER NOT NULL DEFAULT \'0\', user_actions INTEGER NOT NULL DEFAULT \'0\');"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4341
    const-string v0, "CREATE INDEX remote_photos_idx ON all_photos(is_from_autobackup, timestamp, fingerprint, local_content_uri, photo_id, image_url, comment_count, plusone_count, media_attr );"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4352
    return-void
.end method

.method private bL(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 4355
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4356
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4357
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4358
    return-void
.end method

.method private bM(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 4363
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4364
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4365
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4366
    const-string v0, "DROP TABLE IF EXISTS activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4367
    const-string v0, "CREATE TABLE activity_comments  (_id INTEGER PRIMARY KEY,activity_id TEXT NOT NULL,comment_id TEXT UNIQUE NOT NULL,author_id TEXT NOT NULL,content BLOB,created INT NOT NULL,plus_one_data BLOB,comment_flags INT NOT NULL DEFAULT(0), FOREIGN KEY (activity_id) REFERENCES activities(activity_id) ON DELETE CASCADE);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4378
    return-void
.end method

.method private bN(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 4381
    const-string v0, "ALTER TABLE activities ADD COLUMN promoted_post_data BLOB"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4382
    return-void
.end method

.method private bO(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 4385
    const-string v0, "CREATE TABLE photo_requests (token TEXT, token_type INT NOT NULL);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4389
    return-void
.end method

.method private bP(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 4392
    const-string v0, "DROP TABLE IF EXISTS all_photos"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4393
    const-string v0, "CREATE TABLE all_photos (_id INTEGER PRIMARY KEY AUTOINCREMENT, photo_id INTEGER, image_url TEXT, is_primary BOOLEAN DEFAULT \'0\', comment_count INTEGER, plusone_count INTEGER, data BLOB, local_file_path TEXT, local_content_uri TEXT, fingerprint TEXT, timestamp INTEGER NOT NULL DEFAULT \'0\', media_attr INTEGER NOT NULL DEFAULT \'0\', user_actions INTEGER NOT NULL DEFAULT \'0\');"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4408
    const-string v0, "CREATE INDEX remote_photos_idx ON all_photos(is_primary, timestamp, fingerprint, local_content_uri, photo_id, image_url, comment_count, plusone_count, media_attr );"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4419
    return-void
.end method

.method private bQ(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 4422
    const-string v0, "CREATE TABLE table_versions (table_name TEXT PRIMARY KEY,version INT NOT NULL DEFAULT(0));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4426
    return-void
.end method

.method private bR(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 4429
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4430
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4431
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4432
    return-void
.end method

.method private bS(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 4435
    const-string v0, "ALTER TABLE account_status ADD COLUMN people_view_suggestions BLOB"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4437
    return-void
.end method

.method private bT(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 4440
    const-string v0, "DROP INDEX IF EXISTS remote_photos_idx"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4441
    const-string v0, "CREATE INDEX all_photos_photo_id_idx ON all_photos(photo_id);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4444
    const-string v0, "CREATE INDEX all_photos_ui_idx ON all_photos(is_primary, local_content_uri, timestamp, _id);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4450
    const-string v0, "CREATE INDEX all_photos_is_primary_idx ON all_photos(is_primary, fingerprint, photo_id);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4455
    const-string v0, "CREATE INDEX all_photos_local_only_by_fingerprint_idx ON all_photos(fingerprint, photo_id);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4459
    const-string v0, "CREATE INDEX all_photos_local_only_by_content_uri_idx ON all_photos(local_content_uri, photo_id);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4463
    return-void
.end method

.method private bU(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 4466
    const-string v0, "DELETE FROM profiles"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4467
    const-string v0, "ALTER TABLE profiles ADD COLUMN local_reviews_data_proto BLOB"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4468
    const-string v0, "ALTER TABLE profiles ADD COLUMN self_local_reviews_data_proto BLOB"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4470
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4471
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4472
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4473
    return-void
.end method

.method private bV(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 4476
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4477
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4478
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4479
    return-void
.end method

.method private bW(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 4482
    const-string v0, "DROP TABLE IF EXISTS all_photos"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4483
    const-string v0, "CREATE TABLE all_photos (_id INTEGER PRIMARY KEY AUTOINCREMENT, photo_id INTEGER, image_url TEXT, is_primary BOOLEAN DEFAULT \'0\', data BLOB, local_file_path TEXT, local_content_uri TEXT, fingerprint TEXT, timestamp INTEGER NOT NULL DEFAULT \'0\', media_attr INTEGER NOT NULL DEFAULT \'0\', user_actions INTEGER NOT NULL DEFAULT \'0\');"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4496
    const-string v0, "CREATE INDEX all_photos_photo_id_idx ON all_photos(photo_id);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4499
    const-string v0, "CREATE INDEX all_photos_ui_idx ON all_photos(is_primary, local_content_uri, timestamp, _id);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4505
    const-string v0, "CREATE INDEX all_photos_is_primary_idx ON all_photos(is_primary, fingerprint, photo_id);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4510
    const-string v0, "CREATE INDEX all_photos_local_only_by_fingerprint_idx ON all_photos(fingerprint, photo_id);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4514
    const-string v0, "CREATE INDEX all_photos_local_only_by_content_uri_idx ON all_photos(local_content_uri, photo_id);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4518
    return-void
.end method

.method private bX(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 4521
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4522
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4523
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4524
    return-void
.end method

.method private bY(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 4527
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4528
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4529
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4530
    return-void
.end method

.method private bZ(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 4533
    const-string v0, "ALTER TABLE all_photos ADD COLUMN has_edit_list BOOLEAN DEFAULT \'0\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4534
    return-void
.end method

.method private ba(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3883
    const-string v0, "ALTER TABLE account_status ADD COLUMN notification_poll_interval INT DEFAULT(-1)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3885
    return-void
.end method

.method private bb(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3888
    const-string v0, "ALTER TABLE all_tiles ADD COLUMN equivalence_token TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3889
    return-void
.end method

.method private bc(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3893
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3894
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3895
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3896
    return-void
.end method

.method private bd(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3899
    const-string v0, "DELETE FROM all_tiles where view_id=\'best\' AND view_order < 50100"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3900
    return-void
.end method

.method private be(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3904
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3905
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3906
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3907
    return-void
.end method

.method private bf(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3911
    const-string v0, "DELETE FROM events"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3912
    const-string v0, "DELETE FROM event_activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3913
    const-string v0, "DELETE FROM event_people"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3914
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3915
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3916
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3917
    const-string v0, "DELETE FROM guns"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3919
    const-string v0, "DROP TABLE IF EXISTS all_tiles"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3920
    const-string v0, "CREATE TABLE all_tiles (_id INTEGER PRIMARY KEY AUTOINCREMENT, view_id TEXT NOT NULL, view_order INTEGER NOT NULL, type INTEGER NOT NULL, cluster_id TEXT, tile_id TEXT NOT NULL, title TEXT, subtitle TEXT, image_url TEXT, image_width INTEGER, image_height INTEGER, cluster_count INTEGER, comment_count INTEGER, plusone_count INTEGER, acl INTEGER, user_actions INTEGER NOT NULL DEFAULT \'0\', media_attr INTEGER NOT NULL DEFAULT \'0\', timestamp INTEGER NOT NULL DEFAULT \'0\', data BLOB, parent_id TEXT, photo_id INTEGER, owner_id TEXT, last_refresh_time INTEGER NOT NULL DEFAULT \'0\', equivalence_token TEXT);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3946
    return-void
.end method

.method private bg(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3949
    const-string v0, "CREATE TABLE media_cache (filename TEXT PRIMARY KEY,image_url TEXT,size INT NOT NULL DEFAULT(0),http_status INT NOT NULL DEFAULT(0),representation_type INT NOT NULL DEFAULT(0))"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3953
    return-void
.end method

.method private bh(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v3, 0x0

    .line 3957
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "circle_id"

    aput-object v0, v2, v8

    const-string v0, "type"

    aput-object v0, v2, v9

    .line 3959
    const-string v1, "circles"

    move-object v0, p1

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    .line 3960
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 3962
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3963
    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 3964
    invoke-interface {v0, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 3965
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 3966
    const-string v4, "circle_id"

    invoke-virtual {v3, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3967
    const-string v1, "show_order"

    invoke-static {v2}, Ldsm;->a(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v3, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3968
    const-string v1, "circles"

    const-string v2, "circle_id=?"

    new-array v4, v9, [Ljava/lang/String;

    const-string v5, "circle_id"

    .line 3970
    invoke-virtual {v3, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    .line 3968
    invoke-virtual {p1, v1, v3, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0

    .line 3972
    :cond_0
    return-void
.end method

.method private bi(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3975
    const-string v0, "ALTER TABLE events ADD COLUMN event_type INT DEFAULT(0)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3976
    return-void
.end method

.method private bj(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3980
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3981
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3982
    const-string v0, "DROP TABLE IF EXISTS activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3983
    const-string v0, "CREATE TABLE activity_comments (_id INTEGER PRIMARY KEY,activity_id TEXT NOT NULL,comment_id TEXT UNIQUE NOT NULL,author_id TEXT NOT NULL,content TEXT,created INT NOT NULL,plus_one_data BLOB,comment_flags INT NOT NULL DEFAULT(0), FOREIGN KEY (activity_id) REFERENCES activities(activity_id) ON DELETE CASCADE);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3993
    return-void
.end method

.method private bk(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 3996
    const-string v0, "ALTER TABLE all_tiles ADD COLUMN media_key TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3997
    return-void
.end method

.method private bl(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 4001
    const-string v0, "UPDATE account_status SET audience_history=NULL"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4002
    return-void
.end method

.method private bm(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 4005
    const-string v0, "ALTER TABLE account_status ADD COLUMN next_read_low_notifications_fetch_param BLOB"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4007
    const-string v0, "ALTER TABLE account_status ADD COLUMN next_unread_low_notifications_fetch_param BLOB"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4009
    const-string v0, "ALTER TABLE account_status ADD COLUMN read_low_notifications_summary BLOB"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4011
    const-string v0, "ALTER TABLE account_status ADD COLUMN unread_low_notifications_summary BLOB"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4013
    return-void
.end method

.method private bn(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 4016
    const-string v0, "CREATE INDEX media_cache_idx ON media_cache ( image_url, http_status, representation_type )"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4018
    return-void
.end method

.method private bo(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 4021
    const-string v0, "ALTER TABLE guns ADD COLUMN display_index INT NOT NULL DEFAULT(0)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4022
    return-void
.end method

.method private bp(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 4025
    const-string v0, "UPDATE circles SET show_order = 0 WHERE circle_id = \'v.all.circles\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4031
    const-string v0, "UPDATE circles SET show_order = 10000 WHERE circle_id = \'v.nearby\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4037
    const-string v0, "UPDATE circles SET show_order = 10 WHERE circle_id = \'v.whatshot\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4042
    return-void
.end method

.method private bq(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 4045
    const-string v0, "ALTER TABLE guns ADD COLUMN creators BLOB"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4046
    return-void
.end method

.method private br(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 4049
    const-string v0, "ALTER TABLE account_status ADD COLUMN people_view_notification_count INT DEFAULT(0)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4051
    const-string v0, "ALTER TABLE account_status ADD COLUMN people_view_notification_poll_interval INT DEFAULT(-1)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4053
    return-void
.end method

.method private bs(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 4056
    const-string v0, "ALTER TABLE account_status ADD COLUMN last_lowpri_read_notifications_sync_time INT DEFAULT(-1)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4058
    const-string v0, "ALTER TABLE account_status ADD COLUMN last_lowpri_unread_notifications_sync_time INT DEFAULT(-1)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4060
    return-void
.end method

.method private bt(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 4067
    const-string v0, "ALTER TABLE account_status ADD COLUMN has_synced_photo_uploads INT DEFAULT(0)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4069
    return-void
.end method

.method private bu(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 4072
    const-string v0, "ALTER TABLE all_tiles ADD COLUMN photographer_gaia_id TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4073
    const-string v0, "ALTER TABLE all_tiles ADD COLUMN photographer_avatar_url TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4074
    const-string v0, "DROP INDEX IF EXISTS tile_idx"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4075
    const-string v0, "CREATE INDEX tile_idx ON all_tiles ( view_id, view_order, type, cluster_id, tile_id, title, subtitle, image_url, image_width, image_height, cluster_count, comment_count, plusone_count, acl, user_actions, media_attr, timestamp, last_refresh_time, parent_id, photographer_gaia_id, photographer_avatar_url);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4099
    return-void
.end method

.method private bv(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 4102
    const-string v0, "UPDATE sync_status SET last_sync = 0 WHERE sync_data_kind = 13"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4103
    const-string v0, "UPDATE account_status SET people_view_notification_poll_interval = 1"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4104
    return-void
.end method

.method private bw(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 4108
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4109
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4110
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4111
    return-void
.end method

.method private bx(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 4115
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4116
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4117
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4118
    const-string v0, "DELETE FROM events"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4119
    const-string v0, "DELETE FROM guns"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4120
    const-string v0, "UPDATE account_status SET last_notification_sync_version=0, unviewed_notifications_count=0, has_unread_notifications=0, last_viewed_notification_version=0, next_unread_notifications_fetch_param=NULL"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4126
    return-void
.end method

.method private by(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 4130
    const-string v0, "UPDATE profiles SET contact_proto=NULL"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4131
    return-void
.end method

.method private bz(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 4134
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4135
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4136
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4137
    const-string v0, "DELETE FROM events"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4138
    return-void
.end method

.method private cA(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 5037
    const-string v0, "DROP TABLE IF EXISTS manual_autoawesome"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 5038
    const-string v0, "CREATE TABLE manual_autoawesome (_id INTEGER PRIMARY KEY AUTOINCREMENT,render_type INT NOT NULL DEFAULT(0),icon_url TEXT NOT NULL,short_name TEXT NOT NULL,min_num_photos INT NOT NULL DEFAULT(1),max_num_photos INT NOT NULL DEFAULT(1),disallow_animated_inputs INT NOT NULL DEFAULT(0),render_failed_message TEXT NOT NULL);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 5048
    return-void
.end method

.method private cB(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 5051
    const-string v0, "ALTER TABLE guns ADD COLUMN payload BLOB"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 5052
    return-void
.end method

.method private cC(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 5056
    new-array v4, v3, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "analytics_events"

    aput-object v1, v4, v0

    .line 5058
    const-string v2, "analytics_legacy"

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Ldrg;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I[Ljava/lang/String;[Ljava/lang/String;)V

    .line 5059
    return-void
.end method

.method private cD(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 5062
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 5063
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 5064
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 5065
    return-void
.end method

.method private cE(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 5068
    const-string v0, "UPDATE all_tiles SET media_attr = media_attr | 16777216 WHERE parent_id IS NULL AND (acl IS NULL OR acl IN (-1, 2))"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 5071
    const-string v0, "UPDATE all_tiles SET media_attr = media_attr | 16777216 WHERE parent_id IN (SELECT cluster_id FROM all_tiles WHERE parent_id IS NULL AND (acl IS NULL OR acl IN (-1, 2)))"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 5074
    return-void
.end method

.method private cF(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 5077
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 5078
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 5079
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 5081
    const-string v0, "CREATE TABLE activity_contacts (gaia_id TEXT PRIMARY KEY, avatar_url TEXT, name TEXT)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 5083
    return-void
.end method

.method private cG(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 8

    .prologue
    const/4 v5, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 5087
    const/4 v0, 0x6

    new-array v4, v0, [Ljava/lang/String;

    const-string v0, "activity_streams"

    aput-object v0, v4, v2

    const-string v0, "activities"

    aput-object v0, v4, v3

    const-string v0, "activity_contacts"

    aput-object v0, v4, v6

    const-string v0, "activity_comments"

    aput-object v0, v4, v7

    const-string v0, "search"

    aput-object v0, v4, v5

    const/4 v0, 0x5

    const-string v1, "deep_link_installs"

    aput-object v1, v4, v0

    .line 5096
    new-array v5, v5, [Ljava/lang/String;

    const-string v0, "activities_stream_view"

    aput-object v0, v5, v2

    const-string v0, "activity_view"

    aput-object v0, v5, v3

    const-string v0, "comments_view"

    aput-object v0, v5, v6

    const-string v0, "deep_link_installs_view"

    aput-object v0, v5, v7

    .line 5103
    const-string v2, "stream"

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Ldrg;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I[Ljava/lang/String;[Ljava/lang/String;)V

    .line 5104
    return-void
.end method

.method private cH(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 12

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v10, 0x0

    .line 5107
    .line 5110
    :try_start_0
    const-string v1, "account_status"

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "has_synced_photo_uploads"

    aput-object v3, v2, v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 5112
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5113
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-eqz v0, :cond_2

    move v0, v8

    :goto_0
    move v9, v0

    .line 5116
    :cond_0
    if-eqz v1, :cond_1

    .line 5117
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 5119
    :cond_1
    iget-object v0, p0, Ldrg;->c:Landroid/content/Context;

    const-class v1, Lhrb;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhrb;

    iget-object v1, p0, Ldrg;->c:Landroid/content/Context;

    iget v2, p0, Ldrg;->d:I

    .line 5120
    invoke-virtual {v0, v1, v2, v9}, Lhrb;->a(Landroid/content/Context;IZ)V

    .line 5121
    return-void

    :cond_2
    move v0, v9

    .line 5113
    goto :goto_0

    .line 5116
    :catchall_0
    move-exception v0

    move-object v1, v0

    move-object v0, v10

    :goto_1
    if-eqz v0, :cond_3

    .line 5117
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 5119
    :cond_3
    iget-object v0, p0, Ldrg;->c:Landroid/content/Context;

    const-class v2, Lhrb;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhrb;

    iget-object v2, p0, Ldrg;->c:Landroid/content/Context;

    iget v3, p0, Ldrg;->d:I

    .line 5120
    invoke-virtual {v0, v2, v3, v9}, Lhrb;->a(Landroid/content/Context;IZ)V

    throw v1

    .line 5116
    :catchall_1
    move-exception v0

    move-object v11, v0

    move-object v0, v1

    move-object v1, v11

    goto :goto_1
.end method

.method private cI(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 5127
    const-string v0, "CREATE INDEX all_photos_image_url_index ON all_photos(image_url)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 5128
    return-void
.end method

.method private cJ(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 5131
    const-string v0, "ALTER TABLE suggestion_events ADD COLUMN action_source INT DEFAULT(0)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 5132
    return-void
.end method

.method private cK(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 5136
    const-string v0, "ALTER TABLE profiles ADD COLUMN profile_squares_proto BLOB"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 5137
    return-void
.end method

.method private cL(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 5141
    const/4 v0, 0x7

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "all_photos"

    aput-object v1, v4, v0

    const-string v0, "all_tiles"

    aput-object v0, v4, v3

    const/4 v0, 0x2

    const-string v1, "tile_requests"

    aput-object v1, v4, v0

    const/4 v0, 0x3

    const-string v1, "photo_comments"

    aput-object v1, v4, v0

    const/4 v0, 0x4

    const-string v1, "shared_collections"

    aput-object v1, v4, v0

    const/4 v0, 0x5

    const-string v1, "all_photos_local_sync"

    aput-object v1, v4, v0

    const/4 v0, 0x6

    const-string v1, "photo_requests"

    aput-object v1, v4, v0

    .line 5151
    const-string v2, "PhotosDbPartition"

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Ldrg;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I[Ljava/lang/String;[Ljava/lang/String;)V

    .line 5152
    return-void
.end method

.method private cM(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 5156
    const-string v0, "UPDATE account_status SET audience_data=NULL"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 5157
    const-string v0, "UPDATE account_status SET audience_history=NULL"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 5158
    return-void
.end method

.method private cN(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 5161
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 5162
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 5163
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 5164
    return-void
.end method

.method private cO(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 5167
    const-string v0, "ALTER TABLE network_data_transactions ADD COLUMN negotiated_protocol TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 5168
    return-void
.end method

.method private cP(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 5172
    new-array v4, v3, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "suggestion_events"

    aput-object v1, v4, v0

    .line 5176
    const-string v2, "discovery"

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Ldrg;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I[Ljava/lang/String;[Ljava/lang/String;)V

    .line 5177
    return-void
.end method

.method private cQ(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 5181
    const-string v0, "DROP TABLE suggested_people"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 5182
    return-void
.end method

.method private cR(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 5185
    const-string v0, "ALTER TABLE guns ADD COLUMN app_payload BLOB"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 5186
    return-void
.end method

.method private cS(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 5189
    const-string v0, "ALTER TABLE guns ADD COLUMN analytics_data BLOB"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 5190
    return-void
.end method

.method private ca(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 4537
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4538
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4539
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4540
    return-void
.end method

.method private cb(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 4543
    const-string v0, "ALTER TABLE all_photos ADD COLUMN signature TEXT DEFAULT NULL"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4544
    return-void
.end method

.method private cc(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 4547
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4548
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4549
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4550
    return-void
.end method

.method private cd(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 4553
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4554
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4555
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4556
    return-void
.end method

.method private ce(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 4559
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4560
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4561
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4562
    return-void
.end method

.method private cf(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 4583
    const-string v0, "DELETE FROM all_photos"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4584
    const-string v0, "DELETE FROM photo_requests"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4585
    iget-object v0, p0, Ldrg;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 4586
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 4587
    invoke-interface {v3}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 4588
    sget-object v6, Ldrg;->e:[Ljava/lang/String;

    move v2, v1

    :goto_1
    const/16 v7, 0x8

    if-ge v2, v7, :cond_0

    aget-object v7, v6, v2

    .line 4589
    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_1

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    const-string v8, ":"

    invoke-virtual {v7, v8}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 4590
    :cond_1
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 4588
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 4595
    :cond_3
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 4596
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 4597
    invoke-interface {v2, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_2

    .line 4600
    :cond_4
    sget-object v3, Ldrg;->f:[Landroid/net/Uri;

    :goto_3
    const/4 v0, 0x4

    if-ge v1, v0, :cond_5

    aget-object v4, v3, v1

    .line 4601
    iget-object v0, p0, Ldrg;->c:Landroid/content/Context;

    const-class v5, Lhei;

    invoke-static {v0, v5}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iget v5, p0, Ldrg;->d:I

    .line 4602
    invoke-interface {v0, v5}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v5, "gaia_id"

    invoke-interface {v0, v5}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 4603
    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 4604
    const/4 v4, 0x0

    invoke-interface {v2, v0, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 4600
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 4607
    :cond_5
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 4608
    return-void
.end method

.method private cg(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 4611
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4612
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4613
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4614
    return-void
.end method

.method private ch(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 4617
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4618
    const-string v0, "DROP TABLE IF EXISTS activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4619
    const-string v0, "DROP TABLE IF EXISTS activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4620
    const-string v0, "CREATE TABLE activities (_id INTEGER PRIMARY KEY,activity_id TEXT UNIQUE NOT NULL,data_state INT NOT NULL DEFAULT (0),author_id TEXT NOT NULL,source_id TEXT,source_name TEXT,total_comment_count INT NOT NULL,plus_one_data BLOB,acl_display TEXT,loc BLOB,created INT NOT NULL,modified INT NOT NULL,content_flags INT NOT NULL DEFAULT(0),activity_flags INT NOT NULL DEFAULT(0),annotation BLOB,title BLOB,original_author_id TEXT,original_author_name TEXT,original_author_avatar_url TEXT,comment BLOB,permalink TEXT,event_id TEXT,square_update BLOB,square_reshare_update BLOB,relateds BLOB,num_reshares INT NOT NULL DEFAULT(0),embed BLOB,embed_deep_link BLOB,embed_appinvite BLOB,promo BLOB,domain TEXT,birthday BLOB,author_annotation BLOB,original_activity_url TEXT,promoted_post_data BLOB);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4656
    const-string v0, "CREATE TABLE activity_streams (stream_key TEXT NOT NULL,activity_id TEXT NOT NULL,sort_index INT NOT NULL,last_activity INT,token TEXT,context_specific_data BLOB,stream_token TEXT,PRIMARY KEY (stream_key, activity_id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4665
    return-void
.end method

.method private ci(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 4668
    const-string v0, "DROP INDEX all_photos_ui_idx"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4669
    const-string v0, "CREATE INDEX all_photos_ui_idx on all_photos (is_primary, timestamp, _id, local_content_uri);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4675
    return-void
.end method

.method private cj(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 4678
    const-string v0, "CREATE TABLE all_photos_local_sync (media_store_uri TEXT UNIQUE NOT NULL, media_store_token TEXT);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4682
    return-void
.end method

.method private ck(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 4685
    const-string v0, "ALTER TABLE squares ADD COLUMN related_links BLOB"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4686
    return-void
.end method

.method private cl(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 4689
    const-string v0, "DELETE FROM profiles"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4690
    const-string v0, "ALTER TABLE profiles ADD COLUMN profile_stats_proto BLOB"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4691
    return-void
.end method

.method private cm(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 4700
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4701
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4702
    const-string v0, "DROP TABLE IF EXISTS activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4703
    const-string v0, "CREATE TABLE activities (_id INTEGER PRIMARY KEY,activity_id TEXT UNIQUE NOT NULL,data_state INT NOT NULL DEFAULT (0),author_id TEXT NOT NULL,source_id TEXT,source_name TEXT,total_comment_count INT NOT NULL,plus_one_data BLOB,acl_display TEXT,loc BLOB,created INT NOT NULL,modified INT NOT NULL,content_flags INT NOT NULL DEFAULT(0),activity_flags INT NOT NULL DEFAULT(0),annotation BLOB,title BLOB,original_author_id TEXT,original_author_name TEXT,original_author_avatar_url TEXT,comment BLOB,permalink TEXT,event_id TEXT,square_update BLOB,square_reshare_update BLOB,relateds BLOB,num_reshares INT NOT NULL DEFAULT(0),embed BLOB,embed_deep_link BLOB,embed_appinvite BLOB,payload BLOB,domain TEXT,birthday BLOB,author_annotation BLOB,original_activity_url TEXT,promoted_post_data BLOB);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4739
    return-void
.end method

.method private cn(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 11

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 4746
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 4749
    const-string v1, "all_photos"

    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "fingerprint"

    aput-object v0, v2, v3

    const-string v0, "SUM(is_primary) as total_primaries"

    aput-object v0, v2, v5

    const-string v3, "is_primary = 1 AND fingerprint IS NOT NULL"

    const-string v5, "fingerprint"

    const-string v6, "total_primaries > 1"

    move-object v0, p1

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 4755
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 4757
    :cond_0
    :goto_0
    :try_start_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 4758
    const-string v0, "fingerprint"

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 4759
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 4760
    const-string v1, "is_primary"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v9, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4765
    const-string v1, "all_photos"

    const-string v2, "fingerprint = ?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {p1, v1, v9, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 4768
    const-string v1, "all_photos"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const-string v3, "fingerprint = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "timestamp DESC"

    const-string v8, "1"

    move-object v0, p1

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 4771
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4772
    const-string v0, "_id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 4773
    const-string v0, "is_primary"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v9, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4774
    const-string v0, "all_photos"

    const-string v4, "_id = ?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v6

    invoke-virtual {p1, v0, v9, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 4777
    :cond_1
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 4782
    :catchall_0
    move-exception v0

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 4783
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0

    .line 4777
    :catchall_1
    move-exception v0

    :try_start_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 4779
    :cond_2
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 4782
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 4783
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 4784
    return-void
.end method

.method private co(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 4788
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4789
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4790
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4791
    return-void
.end method

.method private cp(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 4831
    const-string v0, "CREATE TABLE partition_versions (partition_name TEXT NOT NULL PRIMARY KEY,version INT NOT NULL DEFAULT(0));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4834
    const-string v0, "CREATE TABLE partition_tables (partition_name TEXT NOT NULL,table_name TEXT NOT NULL,UNIQUE (partition_name,table_name));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4838
    const-string v0, "CREATE TABLE partition_views (partition_name TEXT NOT NULL,view_name TEXT NOT NULL,UNIQUE (partition_name,view_name));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4843
    const/4 v0, 0x3

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "partition_versions"

    aput-object v1, v4, v0

    const-string v0, "partition_tables"

    aput-object v0, v4, v3

    const/4 v0, 0x2

    const-string v1, "partition_views"

    aput-object v1, v4, v0

    .line 4849
    const-string v2, "__master_partition__"

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Ldrg;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I[Ljava/lang/String;[Ljava/lang/String;)V

    .line 4850
    return-void
.end method

.method private cq(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 4854
    const/4 v0, 0x3

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "squares"

    aput-object v1, v4, v0

    const-string v0, "square_contact"

    aput-object v0, v4, v3

    const/4 v0, 0x2

    const-string v1, "square_member_status"

    aput-object v1, v4, v0

    .line 4860
    const-string v2, "squares"

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Ldrg;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I[Ljava/lang/String;[Ljava/lang/String;)V

    .line 4861
    return-void
.end method

.method private cr(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 4866
    const-string v0, "UPDATE account_status SET last_squares_sync_time=-1"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4867
    return-void
.end method

.method private cs(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 4871
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v1

    const-string v0, "data"

    aput-object v0, v2, v3

    .line 4876
    const-string v3, "type = 2"

    .line 4877
    const-string v1, "all_tiles"

    move-object v0, p1

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 4880
    :try_start_0
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 4882
    :cond_0
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4883
    invoke-virtual {v2}, Landroid/content/ContentValues;->clear()V

    .line 4884
    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Ljux;->a([B)Ljux;

    move-result-object v0

    .line 4885
    invoke-virtual {v0}, Ljux;->c()Lnzx;

    move-result-object v0

    .line 4887
    sget-object v3, Lnzr;->a:Loxr;

    invoke-virtual {v0, v3}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 4888
    sget-object v3, Lnzr;->a:Loxr;

    .line 4889
    invoke-virtual {v0, v3}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnzr;

    iget-object v0, v0, Lnzr;->b:Lnxr;

    .line 4891
    iget-object v3, v0, Lnxr;->m:Lnzf;

    if-eqz v3, :cond_0

    iget-object v3, v0, Lnxr;->m:Lnzf;

    iget-object v3, v3, Lnzf;->a:Ljava/lang/Long;

    if-eqz v3, :cond_0

    .line 4892
    const-string v3, "timestamp"

    iget-object v0, v0, Lnxr;->m:Lnzf;

    iget-object v0, v0, Lnzf;->a:Ljava/lang/Long;

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 4893
    const-string v0, "all_tiles"

    const-string v3, "_id = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 4894
    invoke-interface {v1, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    .line 4893
    invoke-virtual {p1, v0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 4899
    :catch_0
    move-exception v0

    :try_start_1
    const-string v0, "DELETE FROM all_tiles"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4900
    const-string v0, "DELETE FROM tile_requests"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4901
    const-string v0, "DELETE FROM photo_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4903
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 4904
    :goto_1
    return-void

    .line 4903
    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private ct(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 4908
    const-string v0, "ALTER TABLE all_tiles ADD COLUMN duration_days INTEGER NOT NULL DEFAULT \'0\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4909
    return-void
.end method

.method private cu(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3

    .prologue
    .line 4912
    iget-object v0, p0, Ldrg;->c:Landroid/content/Context;

    const-class v1, Lhei;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iget v1, p0, Ldrg;->d:I

    .line 4913
    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 4914
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x3d

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "UPDATE all_tiles SET view_id = \'best:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' WHERE view_id = \'best\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4916
    return-void
.end method

.method private cv(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v5, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 4921
    new-array v2, v10, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v9

    const-string v0, "type"

    aput-object v0, v2, v8

    .line 4925
    const-string v1, "all_tiles"

    const-string v3, "view_id = ?"

    new-array v4, v8, [Ljava/lang/String;

    const-string v0, "best"

    aput-object v0, v4, v9

    const-string v7, "view_order ASC"

    move-object v0, p1

    move-object v6, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    move-object v1, v5

    .line 4929
    :goto_0
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 4930
    const/4 v0, 0x1

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v10, :cond_1

    move v0, v8

    .line 4931
    :goto_1
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 4933
    const-string v3, "all_tiles"

    const-string v4, "_id = ?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v1, v6, v7

    invoke-virtual {p1, v3, v4, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 4935
    :cond_0
    if-eqz v0, :cond_2

    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_2
    move-object v1, v0

    .line 4936
    goto :goto_0

    :cond_1
    move v0, v9

    .line 4930
    goto :goto_1

    :cond_2
    move-object v0, v5

    .line 4935
    goto :goto_2

    .line 4937
    :cond_3
    if-eqz v1, :cond_4

    .line 4939
    const-string v0, "all_tiles"

    const-string v3, "_id = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-virtual {p1, v0, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4945
    :cond_4
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 4946
    :goto_3
    return-void

    .line 4942
    :catch_0
    move-exception v0

    :try_start_1
    const-string v0, "DELETE FROM all_tiles"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4943
    const-string v0, "DELETE FROM tile_requests"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4945
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_3

    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private cw(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 4950
    const-string v0, "DROP TABLE IF EXISTS activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4951
    const-string v0, "DROP TABLE IF EXISTS activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4952
    const-string v0, "DROP TABLE IF EXISTS activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4953
    const-string v0, "CREATE TABLE activities (_id INTEGER PRIMARY KEY,unique_activity_id TEXT UNIQUE NOT NULL,activity_id TEXT NOT NULL,data_state INT NOT NULL DEFAULT (0),author_id TEXT NOT NULL,source_id TEXT,source_name TEXT,total_comment_count INT NOT NULL,plus_one_data BLOB,acl_display TEXT,loc BLOB,created INT NOT NULL,modified INT NOT NULL,content_flags INT NOT NULL DEFAULT(0),activity_flags INT NOT NULL DEFAULT(0),annotation BLOB,title BLOB,original_author_id TEXT,original_author_name TEXT,original_author_avatar_url TEXT,comment BLOB,permalink TEXT,event_id TEXT,square_update BLOB,square_reshare_update BLOB,relateds BLOB,num_reshares INT NOT NULL DEFAULT(0),embed BLOB,embed_deep_link BLOB,embed_appinvite BLOB,payload BLOB,domain TEXT,birthday BLOB,author_annotation BLOB,original_activity_url TEXT,promoted_post_data BLOB);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4990
    const-string v0, "CREATE TABLE activity_streams (stream_key TEXT NOT NULL,unique_activity_id TEXT NOT NULL,sort_index INT NOT NULL,last_activity INT,token TEXT,context_specific_data BLOB,stream_token TEXT,PRIMARY KEY (stream_key, unique_activity_id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4999
    const-string v0, "CREATE TABLE activity_comments (_id INTEGER PRIMARY KEY,activity_id TEXT NOT NULL,comment_id TEXT UNIQUE NOT NULL,author_id TEXT NOT NULL,content TEXT,created INT NOT NULL,plus_one_data BLOB,comment_flags INT NOT NULL DEFAULT(0));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 5007
    return-void
.end method

.method private cx(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 5012
    const-string v0, "CREATE TABLE manual_autoawesome  (_id INTEGER PRIMARY KEY AUTOINCREMENT,render_type INT NOT NULL DEFAULT(0),icon_url TEXT NOT NULL,short_name TEXT NOT NULL,min_num_photos INT NOT NULL DEFAULT(1),max_num_photos INT NOT NULL DEFAULT(1),disallow_animated_inputs INT NOT NULL DEFAULT(0));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 5020
    return-void
.end method

.method private cy(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 5023
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 5024
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 5025
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 5026
    const-string v0, "ALTER TABLE all_tiles ADD COLUMN background_color INTEGER NOT NULL DEFAULT \'0\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 5027
    return-void
.end method

.method private cz(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 5030
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 5031
    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 4694
    iget-object v0, p0, Ldrg;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "debug.plus.frontend.config"

    .line 4695
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 4696
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 4697
    return-void
.end method

.method private e(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 2012
    const-string v0, "ALTER TABLE activities ADD COLUMN original_author_avatar_url TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2013
    return-void
.end method

.method private f(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 9

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v8, 0x1

    .line 2022
    const-string v1, "photo"

    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v3

    const-string v0, "url"

    aput-object v0, v2, v8

    const-string v3, "url NOT NULL"

    move-object v0, p1

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2024
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0, v8}, Landroid/content/ContentValues;-><init>(I)V

    .line 2026
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2027
    const-string v2, "url"

    const/4 v3, 0x1

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljbd;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2028
    const-string v2, "photo"

    const-string v3, "_id = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 2029
    invoke-interface {v1, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    .line 2028
    invoke-virtual {p1, v2, v0, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2032
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2034
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2035
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2036
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2037
    return-void
.end method

.method private g(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 2069
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2070
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2071
    const-string v0, "DROP TABLE IF EXISTS activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2072
    const-string v0, "CREATE TABLE activities (_id INTEGER PRIMARY KEY, activity_id TEXT UNIQUE NOT NULL, data_state INT NOT NULL DEFAULT (0 ), author_id TEXT NOT NULL, source_id TEXT, source_name TEXT, total_comment_count INT NOT NULL, plus_one_data BLOB, public INT NOT NULL, spam INT NOT NULL, acl_display TEXT, can_comment INT NOT NULL, can_reshare INT NOT NULL, has_muted INT NOT NULL, has_read INT NOT NULL, loc BLOB, created INT NOT NULL, is_edited INT NOT NULL DEFAULT(0), modified INT NOT NULL, whats_hot BLOB, content_flags INT NOT NULL DEFAULT(0), annotation TEXT, annotation_plaintext TEXT, title TEXT, title_plaintext TEXT, original_author_id TEXT, original_author_name TEXT, original_author_avatar_url TEXT, comment BLOB, permalink TEXT, event_id TEXT, PHOTO_COLLECTION BLOB, square_update BLOB, square_reshare_update BLOB, embed_deep_link BLOB, album_id TEXT, embed_media BLOB, embed_photo_album BLOB, embed_checkin BLOB, embed_place BLOB, embed_place_review BLOB, embed_skyjam BLOB, embed_appinvite BLOB, embed_hangout BLOB, embed_square BLOB, embed_emotishare BLOB, promo BLOB);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2120
    return-void
.end method

.method private h(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 2726
    const-string v0, "DELETE FROM all_tiles"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2727
    const-string v0, "DELETE FROM tile_requests"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2728
    const-string v0, "DELETE FROM events"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2729
    const-string v0, "DELETE FROM event_activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2730
    const-string v0, "DELETE FROM event_people"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2731
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2732
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2733
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2734
    return-void
.end method

.method private i(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 2737
    const-string v0, "DELETE FROM all_tiles"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2738
    const-string v0, "DELETE FROM tile_requests"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2739
    const-string v0, "DELETE FROM events"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2740
    const-string v0, "DELETE FROM event_activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2741
    const-string v0, "DELETE FROM event_people"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2742
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2743
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2744
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2745
    return-void
.end method

.method private j(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 2748
    const-string v0, "DELETE FROM all_tiles"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2749
    const-string v0, "DELETE FROM tile_requests"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2750
    const-string v0, "DELETE FROM events"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2751
    const-string v0, "DELETE FROM event_activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2752
    const-string v0, "DELETE FROM event_people"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2753
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2754
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2755
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2756
    return-void
.end method

.method private k(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 2759
    const-string v0, "DELETE FROM all_tiles"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2760
    const-string v0, "DELETE FROM tile_requests"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2761
    const-string v0, "DELETE FROM events"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2762
    const-string v0, "DELETE FROM event_activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2763
    const-string v0, "DELETE FROM event_people"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2764
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2765
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2766
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2767
    return-void
.end method

.method private l(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 2770
    const-string v0, "DELETE FROM all_tiles"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2771
    const-string v0, "ALTER TABLE all_tiles ADD COLUMN last_refresh_time INTEGER NOT NULL DEFAULT \'0\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2773
    const-string v0, "DROP INDEX IF EXISTS tile_idx"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2774
    const-string v0, "CREATE INDEX tile_idx ON all_tiles ( view_id, view_order, type, cluster_id, tile_id, title, subtitle, image_url, image_width, image_height, cluster_count, comment_count, plusone_count, acl, user_actions, media_attr, timestamp, last_refresh_time );"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2795
    return-void
.end method

.method private m(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 2798
    const-string v0, "DELETE FROM all_tiles"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2799
    const-string v0, "DELETE FROM tile_requests"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2800
    const-string v0, "DELETE FROM events"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2801
    const-string v0, "DELETE FROM event_activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2802
    const-string v0, "DELETE FROM event_people"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2803
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2804
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2805
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2807
    const-string v0, "ALTER TABLE event_activities ADD COLUMN tile_id TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2808
    return-void
.end method

.method private n(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 2811
    const-string v0, "DELETE FROM all_tiles"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2812
    const-string v0, "DELETE FROM tile_requests"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2813
    const-string v0, "DELETE FROM events"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2814
    const-string v0, "DELETE FROM event_activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2815
    const-string v0, "DELETE FROM event_people"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2816
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2817
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2818
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2819
    return-void
.end method

.method private o(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 2822
    const-string v0, "DELETE FROM all_tiles"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2823
    const-string v0, "DELETE FROM tile_requests"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2824
    const-string v0, "DELETE FROM events"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2825
    const-string v0, "DELETE FROM event_activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2826
    const-string v0, "DELETE FROM event_people"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2827
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2828
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2829
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2830
    return-void
.end method

.method private p(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 5

    .prologue
    .line 2833
    const-string v0, "DELETE FROM event_themes"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2834
    new-instance v1, Ljava/io/File;

    iget-object v0, p0, Ldrg;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    const-string v2, "event_themes"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 2837
    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2838
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 2839
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    .line 2838
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2841
    :cond_0
    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2846
    :cond_1
    :goto_1
    return-void

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method private q(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 2849
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2850
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2851
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2852
    const-string v0, "ALTER TABLE activities ADD COLUMN num_reshares INT NOT NULL DEFAULT(0)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2853
    return-void
.end method

.method private r(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 2856
    const-string v0, "ALTER TABLE account_status ADD COLUMN last_notification_sync_version INT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2857
    return-void
.end method

.method private s(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 12

    .prologue
    const/4 v11, 0x2

    const/4 v10, 0x0

    const/4 v9, 0x1

    const/4 v4, 0x0

    .line 2860
    const-string v1, "all_tiles"

    new-array v2, v11, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v10

    const-string v0, "image_url"

    aput-object v0, v2, v9

    const-string v3, "image_url NOT NULL"

    move-object v0, p1

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2862
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8, v9}, Landroid/content/ContentValues;-><init>(I)V

    .line 2864
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2865
    const-string v0, "image_url"

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljbd;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2866
    const-string v0, "all_tiles"

    const-string v2, "_id = ?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 2867
    invoke-interface {v1, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v5

    .line 2866
    invoke-virtual {p1, v0, v8, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2870
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2872
    const-string v1, "event_activities"

    new-array v2, v11, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v10

    const-string v0, "url"

    aput-object v0, v2, v9

    const-string v3, "url NOT NULL"

    move-object v0, p1

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2875
    :goto_1
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2876
    const-string v0, "url"

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljbd;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2877
    const-string v0, "event_activities"

    const-string v2, "_id = ?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 2878
    invoke-interface {v1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 2877
    invoke-virtual {p1, v0, v8, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    .line 2881
    :catchall_1
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2883
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2884
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2885
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2886
    return-void
.end method

.method private t(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 2889
    const-string v0, "DELETE FROM all_tiles"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2890
    const-string v0, "DELETE FROM tile_requests"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2891
    const-string v0, "DELETE FROM events"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2892
    const-string v0, "DELETE FROM event_activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2893
    const-string v0, "DELETE FROM event_people"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2894
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2895
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2896
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2897
    return-void
.end method

.method private u(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 2900
    const-string v0, "UPDATE all_tiles SET media_attr = media_attr & ~65536"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2901
    return-void
.end method

.method private v(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 2904
    const-string v0, "ALTER TABLE account_status ADD COLUMN unviewed_notifications_count INT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2905
    const-string v0, "ALTER TABLE account_status ADD COLUMN has_unread_notifications INT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2906
    return-void
.end method

.method private w(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 2909
    const-string v0, "ALTER TABLE account_status ADD COLUMN last_viewed_notification_version INT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2910
    return-void
.end method

.method private x(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 2913
    const-string v0, "DELETE FROM activities"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2914
    const-string v0, "DELETE FROM activity_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2915
    const-string v0, "DELETE FROM activity_streams"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2916
    return-void
.end method

.method private y(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 2919
    const-string v0, "DROP TABLE IF EXISTS guns;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2920
    const-string v0, "CREATE TABLE guns ( _id INTEGER, key TEXT UNIQUE NOT NULL, creation_time INT NOT NULL, collapsed_description TEXT, collapsed_destination TEXT, collapsed_heading TEXT, collapsed_icon TEXT, entity_reference TEXT, entity_reference_type TEXT, expanded_description TEXT, expanded_destination TEXT, expanded_heading TEXT, expanded_icon TEXT, priority TEXT, read_state INT NOT NULL DEFAULT(0), type INT NOT NULL DEFAULT(0), category INT NOT NULL DEFAULT(0), seen INT NOT NULL DEFAULT(0), actors BLOB, activity_id TEXT, event_id TEXT, album_id TEXT, community_id TEXT, display_index INT NOT NULL DEFAULT(0), updated_version INT NOT NULL DEFAULT(0), push_enabled INT NOT NULL DEFAULT(0) );"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2947
    return-void
.end method

.method private z(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 2950
    const-string v0, "DELETE FROM all_tiles WHERE view_id = \'best\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2951
    const-string v0, "DELETE FROM tile_requests WHERE view_id = \'best\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2952
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 183
    invoke-super {p0}, Liab;->a()V

    .line 184
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sput-wide v0, Ldrg;->a:J

    .line 185
    return-void
.end method

.method protected a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3

    .prologue
    .line 1693
    invoke-super {p0, p1}, Liab;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1696
    invoke-static {}, Lcom/google/android/apps/plus/content/EsProvider;->c()[Ljava/lang/String;

    move-result-object v1

    .line 1697
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 1698
    aget-object v2, v1, v0

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1697
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1700
    :cond_0
    return-void
.end method

.method public a(Landroid/database/sqlite/SQLiteDatabase;I)V
    .locals 6

    .prologue
    .line 1711
    invoke-super {p0, p1}, Liab;->b(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1714
    iget-object v0, p0, Ldrg;->c:Landroid/content/Context;

    const-class v1, Lhei;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 1715
    invoke-interface {v0, p2}, Lhei;->c(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1716
    invoke-interface {v0, p2}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1718
    const-string v1, "UPDATE account_status SET user_id=\'"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "user_id"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x10

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' WHERE "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " IS NULL"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1722
    :cond_0
    return-void
.end method

.method protected b(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 1704
    iget v0, p0, Ldrg;->d:I

    invoke-virtual {p0, p1, v0}, Ldrg;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 1705
    return-void
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 213
    invoke-super {p0, p1}, Liab;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 215
    invoke-static {}, Lcom/google/android/apps/plus/content/EsProvider;->a()[Ljava/lang/String;

    move-result-object v2

    move v0, v1

    .line 220
    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_0

    .line 221
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 220
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 224
    :cond_0
    invoke-static {}, Lcom/google/android/apps/plus/content/EsProvider;->b()[Ljava/lang/String;

    move-result-object v2

    move v0, v1

    .line 225
    :goto_1
    array-length v3, v2

    if-ge v0, v3, :cond_1

    .line 226
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 225
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 229
    :cond_1
    invoke-static {}, Lcom/google/android/apps/plus/content/EsProvider;->c()[Ljava/lang/String;

    move-result-object v0

    .line 230
    :goto_2
    array-length v2, v0

    if-ge v1, v2, :cond_2

    .line 231
    aget-object v2, v0, v1

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 230
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 234
    :cond_2
    iget-object v0, p0, Ldrg;->c:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/apps/plus/content/EsProvider;->a(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 235
    return-void
.end method

.method public onOpen(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 202
    invoke-super {p0, p1}, Liab;->onOpen(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 208
    iget-object v0, p0, Ldrg;->c:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/apps/plus/content/EsProvider;->b(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 209
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 6

    .prologue
    const/16 v5, 0x4c5

    const/16 v0, 0x57b

    const/16 v1, 0x52b

    const/4 v4, -0x1

    .line 239
    const-string v2, "EsDatabaseHelper"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 240
    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x2d

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Upgrade database: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " --> "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 244
    :cond_0
    if-ge p3, p2, :cond_2

    .line 246
    :try_start_0
    invoke-virtual {p0, p1}, Ldrg;->b(Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1674
    iget v0, p0, Ldrg;->d:I

    if-eq v0, v4, :cond_1

    .line 1675
    iget-object v0, p0, Ldrg;->c:Landroid/content/Context;

    const-class v1, Lhei;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iget v1, p0, Ldrg;->d:I

    .line 1676
    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1677
    invoke-static {v0}, Lfud;->a(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    .line 1678
    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    invoke-static {v0, v1, v2}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1682
    iget-object v1, p0, Ldrg;->c:Landroid/content/Context;

    invoke-static {v1}, Lhqv;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    invoke-static {v0, v1, v2}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1684
    iget-object v1, p0, Ldrg;->c:Landroid/content/Context;

    .line 1685
    invoke-static {}, Lcom/google/android/apps/photos/content/GooglePhotoDownsyncProvider;->a()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1684
    invoke-static {v0, v1, v2}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1688
    :cond_1
    :goto_0
    return-void

    .line 250
    :cond_2
    if-ge p2, v5, :cond_3

    .line 251
    :try_start_1
    invoke-virtual {p0, p1}, Ldrg;->b(Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1674
    iget v0, p0, Ldrg;->d:I

    if-eq v0, v4, :cond_1

    .line 1675
    iget-object v0, p0, Ldrg;->c:Landroid/content/Context;

    const-class v1, Lhei;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iget v1, p0, Ldrg;->d:I

    .line 1676
    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1677
    invoke-static {v0}, Lfud;->a(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    .line 1678
    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    invoke-static {v0, v1, v2}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1682
    iget-object v1, p0, Ldrg;->c:Landroid/content/Context;

    invoke-static {v1}, Lhqv;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    invoke-static {v0, v1, v2}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1684
    iget-object v1, p0, Ldrg;->c:Landroid/content/Context;

    .line 1685
    invoke-static {}, Lcom/google/android/apps/photos/content/GooglePhotoDownsyncProvider;->a()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1684
    invoke-static {v0, v1, v2}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    .line 255
    :cond_3
    if-ne p2, v5, :cond_4

    .line 256
    :try_start_2
    const-string v2, "CREATE TABLE people_suggestion_events (action_type TEXT, person_id BLOB, suggestion_id BLOB, suggestion_ui TEXT, timestamp INT)"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "DELETE FROM notifications"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "ALTER TABLE notifications ADD COLUMN pd_album_name TEXT"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 257
    const/16 p2, 0x514

    .line 260
    :cond_4
    const/16 v2, 0x515

    if-ge p2, v2, :cond_5

    .line 261
    const-string v2, "CREATE TABLE square_member_status (square_id TEXT NOT NULL, membership_status INT NOT NULL, member_count INT NOT NULL DEFAULT(0), token TEXT, UNIQUE (square_id, membership_status), FOREIGN KEY (square_id) REFERENCES squares(square_id) ON DELETE CASCADE)"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 262
    const/16 p2, 0x515

    .line 265
    :cond_5
    const/16 v2, 0x516

    if-ge p2, v2, :cond_6

    .line 267
    const/16 p2, 0x516

    .line 270
    :cond_6
    const/16 v2, 0x517

    if-ge p2, v2, :cond_7

    .line 271
    const-string v2, "ALTER TABLE photos_in_album RENAME TO tmp_table"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "CREATE TABLE photos_in_album (_id INTEGER PRIMARY KEY, photo_id INT NOT NULL, collection_id TEXT NOT NULL, FOREIGN KEY (photo_id)REFERENCES photo (photo_id) ON DELETE CASCADE);"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "INSERT INTO photos_in_album(_id, photo_id, collection_id) SELECT _id, photo_id, album_id FROM tmp_table;"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "DROP TABLE tmp_table;"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "ALTER TABLE photos_in_event RENAME TO tmp_table"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "CREATE TABLE photos_in_event (_id INTEGER PRIMARY KEY, photo_id INT NOT NULL, collection_id TEXT NOT NULL, UNIQUE (photo_id, collection_id) FOREIGN KEY (photo_id) REFERENCES photo(photo_id) ON DELETE CASCADE);"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "INSERT INTO photos_in_event(_id, photo_id, collection_id) SELECT _id, photo_id, event_id FROM tmp_table;"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "DROP TABLE tmp_table;"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "ALTER TABLE photos_in_stream RENAME TO tmp_table"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "CREATE TABLE photos_in_stream (_id INTEGER PRIMARY KEY, photo_id INT NOT NULL, collection_id TEXT NOT NULL, FOREIGN KEY (photo_id) REFERENCES photo(photo_id) ON DELETE CASCADE);"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "INSERT INTO photos_in_stream(_id, photo_id, collection_id) SELECT _id, photo_id, stream_id FROM tmp_table;"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "DROP TABLE tmp_table;"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "ALTER TABLE photos_of_user RENAME TO tmp_table"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "CREATE TABLE photos_of_user (_id INTEGER PRIMARY KEY, photo_id INT NOT NULL, collection_id TEXT NOT NULL, FOREIGN KEY (photo_id) REFERENCES photo(photo_id) ON DELETE CASCADE);"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "INSERT INTO photos_of_user(photo_id, collection_id) SELECT photo_id, photo_of_user_id FROM tmp_table;"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "DROP TABLE tmp_table;"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "CREATE INDEX photos_in_stream_stream_id ON photos_in_stream(collection_id)"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "CREATE INDEX photos_in_album_album_id ON photos_in_album(collection_id)"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "CREATE INDEX photos_in_event_event_id ON photos_in_event(collection_id)"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "DROP INDEX IF EXISTS photos_of_user_photo_id"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "DROP INDEX IF EXISTS photo_comment_photo_id"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "CREATE INDEX photo_comment_photo_id ON photo_comment(photo_id)"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "CREATE INDEX photo_comment_comment_id ON photo_comment(comment_id)"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "CREATE INDEX photos_of_user_user_id ON photos_of_user(collection_id)"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 272
    const/16 p2, 0x517

    .line 275
    :cond_7
    const/16 v2, 0x518

    if-ge p2, v2, :cond_8

    .line 276
    const-string v2, "ALTER TABLE events ADD COLUMN plus_one_data BLOB"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 277
    const/16 p2, 0x518

    .line 280
    :cond_8
    const/16 v2, 0x519

    if-ge p2, v2, :cond_9

    .line 281
    const-string v2, "DROP TABLE IF EXISTS photos_in_album"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "CREATE TABLE photos_in_album (_id INTEGER PRIMARY KEY, photo_id INT NOT NULL, collection_id TEXT NOT NULL, sort_index INT NOT NULL, FOREIGN KEY (photo_id) REFERENCES photo(photo_id) ON DELETE CASCADE);"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 282
    const/16 p2, 0x519

    .line 285
    :cond_9
    const/16 v2, 0x51a

    if-ge p2, v2, :cond_a

    .line 286
    const-string v2, "DELETE FROM activities"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "DELETE FROM activity_comments"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "DELETE FROM activity_streams"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "ALTER TABLE activities ADD COLUMN square_update BLOB"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 287
    const/16 p2, 0x51a

    .line 290
    :cond_a
    const/16 v2, 0x51b

    if-ge p2, v2, :cond_b

    .line 291
    const-string v2, "DROP TABLE IF EXISTS media"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 292
    const/16 p2, 0x51b

    .line 295
    :cond_b
    const/16 v2, 0x51c

    if-ge p2, v2, :cond_c

    .line 296
    const-string v2, "ALTER TABLE contacts ADD COLUMN interaction_sort_key TEXT"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "UPDATE account_status SET people_sync_time=-1, people_last_update_token=null"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "UPDATE contacts SET last_updated_time=-1"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 297
    const/16 p2, 0x51c

    .line 300
    :cond_c
    const/16 v2, 0x51d

    if-ge p2, v2, :cond_d

    .line 301
    const-string v2, "DROP TABLE IF EXISTS album"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "CREATE TABLE album ( _id INTEGER PRIMARY KEY AUTOINCREMENT, album_id TEXT UNIQUE NOT NULL, title TEXT, photo_count INT, sort_order INT NOT NULL DEFAULT( 100 ), owner_id TEXT, timestamp INT, entity_version INT, album_type TEXT NOT NULL DEFAULT(\'ALL_OTHERS\'), cover_photo_id INT, stream_id TEXT, is_activity BOOLEAN DEFAULT \'0\', audience INT NOT NULL DEFAULT( -1 ));"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 302
    const/16 p2, 0x51d

    .line 305
    :cond_d
    const/16 v2, 0x51e

    if-ge p2, v2, :cond_e

    .line 306
    const-string v2, "ALTER TABLE event_activities ADD COLUMN photo_id INT"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 307
    const/16 p2, 0x51e

    .line 310
    :cond_e
    const/16 v2, 0x51f

    if-ge p2, v2, :cond_f

    .line 311
    const-string v2, "DROP TABLE IF EXISTS album"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "CREATE TABLE album ( _id INTEGER PRIMARY KEY AUTOINCREMENT, album_id TEXT UNIQUE NOT NULL, title TEXT, photo_count INT, sort_order INT NOT NULL DEFAULT( 100 ), owner_id TEXT, timestamp INT, entity_version INT, album_type TEXT NOT NULL DEFAULT(\'ALL_OTHERS\'), cover_photo_id INT, stream_id TEXT, is_activity BOOLEAN DEFAULT \'0\', audience INT NOT NULL DEFAULT( -1 ));"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 312
    const/16 p2, 0x51f

    .line 315
    :cond_f
    const/16 v2, 0x520

    if-ge p2, v2, :cond_10

    .line 316
    const-string v2, "ALTER TABLE events ADD COLUMN instant_share_end_time INT"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 317
    const/16 p2, 0x520

    .line 320
    :cond_10
    const/16 v2, 0x521

    if-ge p2, v2, :cond_11

    .line 321
    const-string v2, "CREATE INDEX photo_timestamp ON photo(timestamp)"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 322
    const/16 p2, 0x521

    .line 325
    :cond_11
    const/16 v2, 0x522

    if-ge p2, v2, :cond_12

    .line 326
    const-string v2, "DELETE FROM activities"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "DELETE FROM activity_comments"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "DELETE FROM activity_streams"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 327
    const/16 p2, 0x522

    .line 330
    :cond_12
    const/16 v2, 0x523

    if-ge p2, v2, :cond_13

    .line 331
    const-string v2, "DELETE FROM activities"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "DELETE FROM activity_comments"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "DELETE FROM activity_streams"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 332
    const/16 p2, 0x523

    .line 335
    :cond_13
    const/16 v2, 0x524

    if-ge p2, v2, :cond_14

    .line 336
    const-string v2, "ALTER TABLE activities ADD COLUMN comment BLOB"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 337
    const/16 p2, 0x524

    .line 340
    :cond_14
    const/16 v2, 0x525

    if-ge p2, v2, :cond_15

    .line 341
    const-string v2, "DELETE FROM activities"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "DELETE FROM activity_comments"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "DELETE FROM activity_streams"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 342
    const/16 p2, 0x525

    .line 345
    :cond_15
    const/16 v2, 0x526

    if-ge p2, v2, :cond_16

    .line 346
    const-string v2, "ALTER TABLE activities ADD COLUMN square_reshare_update BLOB"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 347
    const/16 p2, 0x526

    .line 350
    :cond_16
    const/16 v2, 0x527

    if-ge p2, v2, :cond_17

    .line 351
    const-string v2, "DELETE FROM activities"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "DELETE FROM activity_comments"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "DELETE FROM activity_streams"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "ALTER TABLE activities ADD COLUMN promo BLOB"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 352
    const/16 p2, 0x527

    .line 355
    :cond_17
    const/16 v2, 0x528

    if-ge p2, v2, :cond_18

    .line 356
    const-string v2, "CREATE TABLE all_tiles ( _id INTEGER PRIMARY KEY AUTOINCREMENT, view_id TEXT NOT NULL, collection_id TEXT, tile_id TEXT NOT NULL, type TEXT NOT NULL, title TEXT, subtitle TEXT, image_url TEXT, image_width INTEGER, image_height INTEGER, color INTEGER, comment_count INTEGER, plusone_count INTEGER, parent_key INTEGER, parent_title TEXT, data BLOB, view_order INTEGER NOT NULL, hidden BOOLEAN NOT NULL DEFAULT \'0\', mine BOOLEAN NOT NULL DEFAULT \'0\' );"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "CREATE INDEX tile_idx ON all_tiles ( view_id, view_order, type, hidden, mine, collection_id, tile_id, title, subtitle, image_url, image_width, image_height, comment_count, plusone_count );"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "CREATE TABLE scroll_sections ( _id INTEGER PRIMARY KEY AUTOINCREMENT, view_id TEXT NOT NULL, row INTEGER NOT NULL, tile_id TEXT NOT NULL, title TEXT, view_order INTEGER NOT NULL, landscape BOOLEAN NOT NULL DEFAULT \'0\' );"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "CREATE INDEX scroll_idx ON scroll_sections ( view_id, landscape , view_order, row, tile_id, title );"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "CREATE TABLE tile_requests ( view_id TEXT NOT NULL, resume_token TEXT, last_refresh_time INTEGER NOT NULL DEFAULT \'0\', last_refresh_token TEXT );"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "CREATE INDEX tile_request_idx ON tile_requests ( view_id );"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 357
    const/16 p2, 0x528

    .line 360
    :cond_18
    const/16 v2, 0x529

    if-ge p2, v2, :cond_19

    .line 361
    const-string v2, "ALTER TABLE circles ADD COLUMN notifications_enabled INT NOT NULL DEFAULT(0)"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "ALTER TABLE squares ADD COLUMN volume INT"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "UPDATE squares SET volume=2"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "UPDATE account_status SET circle_sync_time=-1"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 362
    const/16 p2, 0x529

    .line 365
    :cond_19
    const/16 v2, 0x52a

    if-ge p2, v2, :cond_1a

    .line 366
    const-string v2, "DELETE FROM activities"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "DELETE FROM activity_comments"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "DELETE FROM activity_streams"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 367
    const/16 p2, 0x52a

    .line 370
    :cond_1a
    if-ge p2, v1, :cond_1b

    .line 371
    invoke-direct {p0, p1}, Ldrg;->e(Landroid/database/sqlite/SQLiteDatabase;)V

    move p2, v1

    .line 375
    :cond_1b
    if-ge p2, v1, :cond_1c

    .line 376
    invoke-direct {p0, p1}, Ldrg;->e(Landroid/database/sqlite/SQLiteDatabase;)V

    move p2, v1

    .line 380
    :cond_1c
    const/16 v1, 0x52c

    if-ge p2, v1, :cond_1d

    .line 381
    const-string v1, "DELETE FROM activities"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM activity_comments"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM activity_streams"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 382
    const/16 p2, 0x52c

    .line 385
    :cond_1d
    const/16 v1, 0x52d

    if-ge p2, v1, :cond_1e

    .line 386
    invoke-direct {p0, p1}, Ldrg;->f(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 387
    const/16 p2, 0x52d

    .line 390
    :cond_1e
    const/16 v1, 0x52e

    if-ge p2, v1, :cond_1f

    .line 391
    const-string v1, "ALTER TABLE activities ADD COLUMN permalink TEXT"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 392
    const/16 p2, 0x52e

    .line 395
    :cond_1f
    const/16 v1, 0x52f

    if-ge p2, v1, :cond_20

    .line 396
    const-string v1, "DELETE FROM activities"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM activity_comments"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM activity_streams"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 397
    const/16 p2, 0x52f

    .line 400
    :cond_20
    const/16 v1, 0x530

    if-ge p2, v1, :cond_21

    .line 401
    const-string v1, "DELETE FROM activities"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM activity_comments"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM activity_streams"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 402
    const/16 p2, 0x530

    .line 405
    :cond_21
    const/16 v1, 0x531

    if-ge p2, v1, :cond_22

    .line 406
    const-string v1, "DELETE FROM activities"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM activity_comments"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM activity_streams"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 407
    const/16 p2, 0x531

    .line 410
    :cond_22
    const/16 v1, 0x532

    if-ge p2, v1, :cond_23

    .line 411
    const-string v1, "DELETE FROM activities"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM activity_comments"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM activity_streams"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 412
    const/16 p2, 0x532

    .line 415
    :cond_23
    const/16 v1, 0x533

    if-ge p2, v1, :cond_24

    .line 416
    invoke-direct {p0, p1}, Ldrg;->g(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 417
    const/16 p2, 0x533

    .line 420
    :cond_24
    const/16 v1, 0x534

    if-ge p2, v1, :cond_25

    .line 421
    invoke-direct {p0, p1}, Ldrg;->g(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 422
    const/16 p2, 0x534

    .line 425
    :cond_25
    const/16 v1, 0x535

    if-ge p2, v1, :cond_26

    .line 426
    const-string v1, "DELETE FROM activities"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM activity_comments"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM activity_streams"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "ALTER TABLE activities ADD COLUMN is_plusoneable INT NOT NULL DEFAULT(1)"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 427
    const/16 p2, 0x535

    .line 430
    :cond_26
    const/16 v1, 0x536

    if-ge p2, v1, :cond_27

    .line 431
    const-string v1, "DELETE FROM activities"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM activity_comments"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM activity_streams"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 432
    const/16 p2, 0x536

    .line 435
    :cond_27
    const/16 v1, 0x537

    if-ge p2, v1, :cond_28

    .line 436
    const-string v1, "DELETE FROM photo"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "ALTER TABLE photo ADD COLUMN orientation INT NOT NULL DEFAULT(0)"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 437
    const/16 p2, 0x537

    .line 440
    :cond_28
    const/16 v1, 0x538

    if-ge p2, v1, :cond_29

    .line 441
    const-string v1, "DROP TABLE IF EXISTS all_tiles"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DROP INDEX IF EXISTS tile_idx"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "CREATE TABLE all_tiles ( _id INTEGER PRIMARY KEY AUTOINCREMENT, view_id TEXT NOT NULL, collection_id TEXT, tile_id TEXT NOT NULL, type TEXT NOT NULL, title TEXT, subtitle TEXT, image_url TEXT, image_width INTEGER, image_height INTEGER, color INTEGER, collection_count INTEGER, comment_count INTEGER, plusone_count INTEGER, parent_key INTEGER, parent_title TEXT, data BLOB, view_order INTEGER NOT NULL, hidden BOOLEAN NOT NULL DEFAULT \'0\', mine BOOLEAN NOT NULL DEFAULT \'0\' );"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "CREATE INDEX tile_idx ON all_tiles ( view_id, view_order, type, hidden, mine, collection_id, tile_id, title, subtitle, image_url, image_width, image_height, collection_count, comment_count, plusone_count );"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 442
    const/16 p2, 0x538

    .line 445
    :cond_29
    const/16 v1, 0x539

    if-ge p2, v1, :cond_2a

    .line 446
    const-string v1, "DELETE FROM photo"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "ALTER TABLE photo ADD COLUMN rotation INT NOT NULL DEFAULT(0)"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 447
    const/16 p2, 0x539

    .line 450
    :cond_2a
    const/16 v1, 0x53a

    if-ge p2, v1, :cond_2b

    .line 451
    const-string v1, "DELETE FROM photo_comment"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM photo_plusone"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM photos_in_album"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM photos_in_event"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM photos_of_user"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM photos_in_stream"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM photo_shape"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM photo"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 452
    const/16 p2, 0x53a

    .line 455
    :cond_2b
    const/16 v1, 0x53b

    if-ge p2, v1, :cond_2c

    .line 456
    const-string v1, "DELETE FROM all_tiles"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DROP INDEX IF EXISTS tile_idx"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "ALTER TABLE all_tiles ADD COLUMN acl INTEGER"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "CREATE INDEX tile_idx ON all_tiles ( view_id, view_order, type, hidden, mine, collection_id, tile_id, title, subtitle, image_url, image_width, image_height, collection_count, comment_count, plusone_count, acl );"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 457
    const/16 p2, 0x53b

    .line 460
    :cond_2c
    const/16 v1, 0x53c

    if-ge p2, v1, :cond_2d

    .line 461
    const-string v1, "DROP TABLE IF EXISTS all_tiles"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DROP INDEX IF EXISTS tile_idx"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "CREATE TABLE all_tiles ( _id INTEGER PRIMARY KEY AUTOINCREMENT, view_id TEXT NOT NULL, collection_id TEXT, tile_id TEXT NOT NULL, type TEXT NOT NULL, title TEXT, subtitle TEXT, image_url TEXT, image_width INTEGER, image_height INTEGER, color INTEGER, collection_count INTEGER, comment_count INTEGER, plusone_count INTEGER, parent_id TEXT, parent_title TEXT, data BLOB, view_order INTEGER NOT NULL, hidden BOOLEAN NOT NULL DEFAULT \'0\', mine BOOLEAN NOT NULL DEFAULT \'0\', acl INTEGER );"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "CREATE INDEX tile_idx ON all_tiles ( view_id, view_order, type, hidden, mine, collection_id, tile_id, title, subtitle, image_url, image_width, image_height, collection_count, comment_count, plusone_count, acl );"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 462
    const/16 p2, 0x53c

    .line 465
    :cond_2d
    const/16 v1, 0x53d

    if-ge p2, v1, :cond_2e

    .line 466
    const-string v1, "CREATE TABLE photo_comments (_id INTEGER PRIMARY KEY AUTOINCREMENT, tile_id TEXT NOT NULL, comment_id TEXT UNIQUE NOT NULL, author_id TEXT NOT NULL, content TEXT NOT NULL, view_order INT NOT NULL, update_time INT NOT NULL, plusone_count INT DEFAULT (0), plusone_by_viewer BOOLEAN DEFAULT \'0\');"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "CREATE INDEX photo_comments_idx ON photo_comments( comment_id )"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "ALTER TABLE photo_comment RENAME TO photo_comment_old"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 467
    const/16 p2, 0x53d

    .line 470
    :cond_2e
    const/16 v1, 0x53e

    if-ge p2, v1, :cond_2f

    .line 471
    const-string v1, "DELETE FROM all_tiles"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM tile_requests"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM photo_comments"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 472
    const/16 p2, 0x53e

    .line 475
    :cond_2f
    const/16 v1, 0x53f

    if-ge p2, v1, :cond_30

    .line 476
    const-string v1, "DELETE FROM all_tiles"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM tile_requests"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM photo_comments"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 477
    const/16 p2, 0x53f

    .line 480
    :cond_30
    const/16 v1, 0x540

    if-ge p2, v1, :cond_31

    .line 481
    const-string v1, "DELETE FROM activities"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM activity_comments"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM activity_streams"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "ALTER TABLE activity_streams ADD COLUMN stream_token TEXT"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 482
    const/16 p2, 0x540

    .line 485
    :cond_31
    const/16 v1, 0x541

    if-ge p2, v1, :cond_32

    .line 486
    const-string v1, "DELETE FROM all_tiles"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM tile_requests"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 487
    const/16 p2, 0x541

    .line 490
    :cond_32
    const/16 v1, 0x542

    if-ge p2, v1, :cond_33

    .line 491
    const-string v1, "DELETE FROM all_tiles"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM tile_requests"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "ALTER TABLE all_tiles ADD COLUMN photo_id INTEGER"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "ALTER TABLE all_tiles ADD COLUMN owner_id TEXT"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 492
    const/16 p2, 0x542

    .line 495
    :cond_33
    const/16 v1, 0x543

    if-ge p2, v1, :cond_34

    .line 496
    const-string v1, "DROP TABLE IF EXISTS all_tiles"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DROP INDEX IF EXISTS tile_idx"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM tile_requests"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "CREATE TABLE all_tiles ( _id INTEGER PRIMARY KEY AUTOINCREMENT, view_id TEXT NOT NULL, cluster_id TEXT, tile_id TEXT NOT NULL, type TEXT NOT NULL, title TEXT, subtitle TEXT, image_url TEXT, image_width INTEGER, image_height INTEGER, color INTEGER, cluster_count INTEGER, comment_count INTEGER, plusone_count INTEGER, parent_id TEXT, parent_title TEXT, data BLOB, view_order INTEGER NOT NULL, hidden BOOLEAN NOT NULL DEFAULT \'0\', mine BOOLEAN NOT NULL DEFAULT \'0\', acl INTEGER, photo_id INTEGER, owner_id INTEGER, );"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "CREATE INDEX tile_idx ON all_tiles ( view_id, view_order, type, hidden, mine, cluster_id, tile_id, title, subtitle, image_url, image_width, image_height, cluster_count, comment_count, plusone_count, acl );"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 497
    const/16 p2, 0x543

    .line 500
    :cond_34
    const/16 v1, 0x544

    if-ge p2, v1, :cond_35

    .line 501
    const-string v1, "DELETE FROM events"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM event_activities"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM event_people"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DROP INDEX IF EXISTS photos_in_event_event_id"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DROP TABLE IF EXISTS photos_in_event"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 502
    const/16 p2, 0x544

    .line 505
    :cond_35
    const/16 v1, 0x545

    if-ge p2, v1, :cond_36

    .line 506
    const-string v1, "DELETE FROM all_tiles"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM tile_requests"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 507
    const/16 p2, 0x545

    .line 510
    :cond_36
    const/16 v1, 0x546

    if-ge p2, v1, :cond_37

    .line 511
    const-string v1, "DELETE FROM all_tiles"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM tile_requests"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 512
    const/16 p2, 0x546

    .line 515
    :cond_37
    const/16 v1, 0x547

    if-ge p2, v1, :cond_38

    .line 516
    const-string v1, "DROP INDEX IF EXISTS tile_idx"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM all_tiles"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM tile_requests"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "ALTER TABLE all_tiles ADD COLUMN user_actions INTEGER NOT NULL DEFAULT \'0\'"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "CREATE INDEX tile_idx ON all_tiles ( view_id, view_order, type, hidden, mine, cluster_id, tile_id, title, subtitle, image_url, image_width, image_height, cluster_count, comment_count, plusone_count, acl, user_actions );"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 517
    const/16 p2, 0x547

    .line 520
    :cond_38
    const/16 v1, 0x548

    if-ge p2, v1, :cond_39

    .line 521
    const-string v1, "DELETE FROM activities"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM activity_comments"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM activity_streams"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "ALTER TABLE activities ADD COLUMN social_friends_plus_oned BLOB"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 522
    const/16 p2, 0x548

    .line 525
    :cond_39
    const/16 v1, 0x549

    if-ge p2, v1, :cond_3a

    .line 526
    const-string v1, "DELETE FROM events"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM event_activities"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM event_people"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM activities"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM activity_comments"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM activity_streams"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "ALTER TABLE events ADD COLUMN deleted INT DEFAULT (0)"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 527
    const/16 p2, 0x549

    .line 530
    :cond_3a
    const/16 v1, 0x54a

    if-ge p2, v1, :cond_3b

    .line 531
    const-string v1, "DROP INDEX IF EXISTS tile_idx"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "CREATE INDEX tile_idx ON all_tiles ( view_id, view_order, type, hidden, mine, cluster_id, tile_id, title, subtitle, image_url, image_width, image_height, cluster_count, comment_count, plusone_count, acl, user_actions, photo_id );"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 532
    const/16 p2, 0x54a

    .line 535
    :cond_3b
    const/16 v1, 0x54b

    if-ge p2, v1, :cond_3c

    .line 536
    const-string v1, "DROP INDEX IF EXISTS tile_idx"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "CREATE INDEX tile_idx ON all_tiles ( view_id, view_order, type, hidden, mine, cluster_id, tile_id, title, subtitle, image_url, image_width, image_height, cluster_count, comment_count, plusone_count, acl, user_actions );"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 537
    const/16 p2, 0x54b

    .line 541
    :cond_3c
    const/16 v1, 0x54c

    if-ge p2, v1, :cond_3d

    .line 542
    const-string v1, "DELETE FROM activities"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM activity_comments"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM activity_streams"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "ALTER TABLE activities ADD COLUMN embed_google_offer_v2 BLOB"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 543
    const/16 p2, 0x54c

    .line 546
    :cond_3d
    const/16 v1, 0x54d

    if-ge p2, v1, :cond_3e

    .line 547
    const-string v1, "DROP TABLE IF EXISTS photo_home"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DROP TABLE IF EXISTS photo_home_cover"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DROP TABLE IF EXISTS album"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DROP TABLE IF EXISTS album_cover"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DROP TABLE IF EXISTS photo"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DROP TABLE IF EXISTS photo_comment_old"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DROP TABLE IF EXISTS photo_plusone"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DROP TABLE IF EXISTS photos_of_user"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DROP TABLE IF EXISTS photos_in_stream"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DROP TABLE IF EXISTS photo_shape"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 548
    const/16 p2, 0x54d

    .line 551
    :cond_3e
    const/16 v1, 0x54e

    if-ge p2, v1, :cond_3f

    .line 552
    const-string v1, "DELETE FROM profiles"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "ALTER TABLE profiles ADD COLUMN people_data_proto BLOB"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 553
    const/16 p2, 0x54e

    .line 556
    :cond_3f
    const/16 v1, 0x54f

    if-ge p2, v1, :cond_40

    .line 557
    const-string v1, "DELETE FROM all_tiles WHERE view_id LIKE \'albums%\'"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM tile_requests WHERE view_id LIKE \'albums%\'"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 558
    const/16 p2, 0x54f

    .line 561
    :cond_40
    const/16 v1, 0x550

    if-ge p2, v1, :cond_41

    .line 562
    const-string v1, "DELETE FROM activities"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM activity_comments"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM activity_streams"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "ALTER TABLE activities ADD COLUMN relateds BLOB"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 563
    const/16 p2, 0x550

    .line 566
    :cond_41
    const/16 v1, 0x551

    if-ge p2, v1, :cond_42

    .line 567
    const-string v1, "DELETE FROM all_tiles WHERE view_id LIKE \'album:%\'"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM tile_requests WHERE view_id LIKE \'album:%\'"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 568
    const/16 p2, 0x551

    .line 571
    :cond_42
    const/16 v1, 0x552

    if-ge p2, v1, :cond_43

    .line 572
    const-string v1, "DELETE FROM all_tiles"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM tile_requests"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "ALTER TABLE all_tiles ADD COLUMN media_attr INTEGER NOT NULL DEFAULT \'0\'"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DROP INDEX IF EXISTS tile_idx"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "CREATE INDEX tile_idx ON all_tiles ( view_id, view_order, type, hidden, mine, cluster_id, tile_id, title, subtitle, image_url, image_width, image_height, cluster_count, comment_count, plusone_count, acl, user_actions, media_attr );"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 573
    const/16 p2, 0x552

    .line 576
    :cond_43
    const/16 v1, 0x553

    if-ge p2, v1, :cond_44

    .line 577
    const-string v1, "DELETE FROM activities"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM activity_comments"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM activity_streams"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 578
    const/16 p2, 0x553

    .line 581
    :cond_44
    const/16 v1, 0x554

    if-ge p2, v1, :cond_45

    .line 582
    const-string v1, "DELETE FROM activities"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM activity_streams"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM activity_comments"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 583
    const/16 p2, 0x554

    .line 586
    :cond_45
    const/16 v1, 0x555

    if-ge p2, v1, :cond_46

    .line 587
    const-string v1, "DELETE FROM all_tiles"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM tile_requests"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 588
    const/16 p2, 0x555

    .line 591
    :cond_46
    const/16 v1, 0x556

    if-ge p2, v1, :cond_47

    .line 592
    const-string v1, "DELETE FROM activities"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM activity_comments"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM activity_streams"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "ALTER TABLE activities ADD COLUMN is_stranger_post INT NOT NULL DEFAULT(0)"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 593
    const/16 p2, 0x556

    .line 596
    :cond_47
    const/16 v1, 0x557

    if-ge p2, v1, :cond_48

    .line 597
    const-string v1, "DELETE FROM activities"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM activity_comments"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM activity_streams"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 598
    const/16 p2, 0x557

    .line 601
    :cond_48
    const/16 v1, 0x558

    if-ge p2, v1, :cond_49

    .line 602
    const-string v1, "DELETE FROM all_tiles"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM tile_requests"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 603
    const/16 p2, 0x558

    .line 606
    :cond_49
    const/16 v1, 0x559

    if-ge p2, v1, :cond_4a

    .line 607
    const-string v1, "DELETE FROM events"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM event_activities"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM event_people"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 608
    const/16 p2, 0x559

    .line 611
    :cond_4a
    const/16 v1, 0x55a

    if-ge p2, v1, :cond_4b

    .line 612
    const-string v1, "DELETE FROM all_tiles"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM tile_requests"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM events"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM event_activities"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM event_people"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM activities"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM activity_comments"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM activity_streams"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 613
    const/16 p2, 0x55a

    .line 616
    :cond_4b
    const/16 v1, 0x55b

    if-ge p2, v1, :cond_4c

    .line 617
    const-string v1, "DELETE FROM all_tiles"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM tile_requests"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "ALTER TABLE all_tiles ADD COLUMN timestamp INTEGER NOT NULL DEFAULT \'0\'"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DROP INDEX IF EXISTS tile_idx"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "CREATE INDEX tile_idx ON all_tiles ( view_id, view_order, type, hidden, mine, cluster_id, tile_id, title, subtitle, image_url, image_width, image_height, cluster_count, comment_count, plusone_count, acl, user_actions, media_attr, timestamp );"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 618
    const/16 p2, 0x55b

    .line 621
    :cond_4c
    const/16 v1, 0x55c

    if-ge p2, v1, :cond_4d

    .line 622
    const-string v1, "DELETE FROM events"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM event_activities"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM event_people"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM activities"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM activity_comments"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM activity_streams"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DROP TABLE IF EXISTS all_tiles"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DROP INDEX IF EXISTS tile_idx"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DROP TABLE IF EXISTS scroll_sections"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DROP INDEX IF EXISTS scroll_idx"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "CREATE TABLE all_tiles ( _id INTEGER PRIMARY KEY AUTOINCREMENT, view_id TEXT NOT NULL, view_order INTEGER NOT NULL, type TEXT NOT NULL, cluster_id TEXT, tile_id TEXT NOT NULL, title TEXT, subtitle TEXT, image_url TEXT, image_width INTEGER, image_height INTEGER, cluster_count INTEGER, comment_count INTEGER, plusone_count INTEGER, acl INTEGER, user_actions INTEGER NOT NULL DEFAULT \'0\', media_attr INTEGER NOT NULL DEFAULT \'0\', timestamp INTEGER NOT NULL DEFAULT \'0\', data BLOB, parent_id TEXT, photo_id INTEGER, owner_id TEXT );"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "CREATE INDEX tile_idx ON all_tiles ( view_id, view_order, type, cluster_id, tile_id, title, subtitle, image_url, image_width, image_height, cluster_count, comment_count, plusone_count, acl, user_actions, media_attr, timestamp );"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 623
    const/16 p2, 0x55c

    .line 626
    :cond_4d
    const/16 v1, 0x55d

    if-ge p2, v1, :cond_4e

    .line 627
    const-string v1, "DELETE FROM photo_comments"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "ALTER TABLE photo_comments ADD COLUMN plusone_timestamp INT DEFAULT(0)"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 628
    const/16 p2, 0x55d

    .line 631
    :cond_4e
    const/16 v1, 0x55e

    if-ge p2, v1, :cond_4f

    .line 632
    const-string v1, "DELETE FROM all_tiles"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM tile_requests"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM events"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM event_activities"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM event_people"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM activities"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM activity_comments"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM activity_streams"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 633
    const/16 p2, 0x55e

    .line 636
    :cond_4f
    const/16 v1, 0x55f

    if-ge p2, v1, :cond_50

    .line 637
    const-string v1, "DELETE FROM profiles"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 638
    const/16 p2, 0x55f

    .line 641
    :cond_50
    const/16 v1, 0x560

    if-ge p2, v1, :cond_51

    .line 642
    invoke-direct {p0, p1}, Ldrg;->h(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 643
    const/16 p2, 0x560

    .line 646
    :cond_51
    const/16 v1, 0x561

    if-ge p2, v1, :cond_52

    .line 647
    invoke-direct {p0, p1}, Ldrg;->i(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 648
    const/16 p2, 0x561

    .line 651
    :cond_52
    const/16 v1, 0x562

    if-ge p2, v1, :cond_53

    .line 652
    invoke-direct {p0, p1}, Ldrg;->j(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 653
    const/16 p2, 0x562

    .line 656
    :cond_53
    const/16 v1, 0x563

    if-ge p2, v1, :cond_54

    .line 657
    invoke-direct {p0, p1}, Ldrg;->k(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 658
    const/16 p2, 0x563

    .line 661
    :cond_54
    const/16 v1, 0x564

    if-ge p2, v1, :cond_55

    .line 662
    invoke-direct {p0, p1}, Ldrg;->l(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 663
    const/16 p2, 0x564

    .line 666
    :cond_55
    const/16 v1, 0x565

    if-ge p2, v1, :cond_56

    .line 667
    invoke-direct {p0, p1}, Ldrg;->m(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 668
    const/16 p2, 0x565

    .line 671
    :cond_56
    const/16 v1, 0x566

    if-ge p2, v1, :cond_57

    .line 672
    invoke-direct {p0, p1}, Ldrg;->n(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 673
    const/16 p2, 0x566

    .line 676
    :cond_57
    const/16 v1, 0x567

    if-ge p2, v1, :cond_58

    .line 677
    invoke-direct {p0, p1}, Ldrg;->o(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 678
    const/16 p2, 0x567

    .line 681
    :cond_58
    const/16 v1, 0x568

    if-ge p2, v1, :cond_59

    .line 682
    invoke-direct {p0, p1}, Ldrg;->p(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 683
    const/16 p2, 0x568

    .line 686
    :cond_59
    const/16 v1, 0x578

    if-ge p2, v1, :cond_5a

    .line 687
    invoke-direct {p0, p1}, Ldrg;->q(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 688
    const/16 p2, 0x578

    .line 691
    :cond_5a
    if-ge p2, v0, :cond_5b

    .line 692
    invoke-direct {p0, p1}, Ldrg;->r(Landroid/database/sqlite/SQLiteDatabase;)V

    move p2, v0

    .line 696
    :cond_5b
    if-ge p2, v0, :cond_5c

    .line 697
    invoke-direct {p0, p1}, Ldrg;->r(Landroid/database/sqlite/SQLiteDatabase;)V

    move p2, v0

    .line 701
    :cond_5c
    const/16 v0, 0x57c

    if-ge p2, v0, :cond_5d

    .line 702
    invoke-direct {p0, p1}, Ldrg;->s(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 703
    const/16 p2, 0x57c

    .line 706
    :cond_5d
    const/16 v0, 0x57d

    if-ge p2, v0, :cond_5e

    .line 707
    invoke-direct {p0, p1}, Ldrg;->t(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 708
    const/16 p2, 0x57d

    .line 711
    :cond_5e
    const/16 v0, 0x57e

    if-ge p2, v0, :cond_5f

    .line 712
    invoke-direct {p0, p1}, Ldrg;->u(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 713
    const/16 p2, 0x57e

    .line 716
    :cond_5f
    const/16 v0, 0x580

    if-ge p2, v0, :cond_60

    .line 717
    invoke-direct {p0, p1}, Ldrg;->v(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 718
    const/16 p2, 0x580

    .line 721
    :cond_60
    const/16 v0, 0x581

    if-ge p2, v0, :cond_61

    .line 722
    invoke-direct {p0, p1}, Ldrg;->w(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 723
    const/16 p2, 0x581

    .line 726
    :cond_61
    const/16 v0, 0x582

    if-ge p2, v0, :cond_62

    .line 727
    invoke-direct {p0, p1}, Ldrg;->x(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 728
    const/16 p2, 0x582

    .line 731
    :cond_62
    const/16 v0, 0x583

    if-ge p2, v0, :cond_63

    .line 732
    invoke-direct {p0, p1}, Ldrg;->y(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 733
    const/16 p2, 0x583

    .line 736
    :cond_63
    const/16 v0, 0x584

    if-ge p2, v0, :cond_64

    .line 737
    invoke-direct {p0, p1}, Ldrg;->z(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 738
    const/16 p2, 0x584

    .line 741
    :cond_64
    const/16 v0, 0x585

    if-ge p2, v0, :cond_65

    .line 742
    invoke-direct {p0, p1}, Ldrg;->A(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 743
    const/16 p2, 0x585

    .line 746
    :cond_65
    const/16 v0, 0x586

    if-ge p2, v0, :cond_66

    .line 747
    invoke-direct {p0, p1}, Ldrg;->B(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 748
    const/16 p2, 0x586

    .line 751
    :cond_66
    const/16 v0, 0x587

    if-ge p2, v0, :cond_67

    .line 752
    invoke-direct {p0, p1}, Ldrg;->C(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 753
    const/16 p2, 0x587

    .line 756
    :cond_67
    const/16 v0, 0x588

    if-ge p2, v0, :cond_68

    .line 757
    invoke-direct {p0, p1}, Ldrg;->D(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 758
    const/16 p2, 0x588

    .line 761
    :cond_68
    const/16 v0, 0x589

    if-ge p2, v0, :cond_69

    .line 762
    invoke-direct {p0, p1}, Ldrg;->E(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 763
    const/16 p2, 0x589

    .line 766
    :cond_69
    const/16 v0, 0x58a

    if-ge p2, v0, :cond_6a

    .line 767
    invoke-direct {p0, p1}, Ldrg;->F(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 768
    const/16 p2, 0x58a

    .line 771
    :cond_6a
    const/16 v0, 0x58b

    if-ge p2, v0, :cond_6b

    .line 772
    invoke-direct {p0, p1}, Ldrg;->G(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 773
    const/16 p2, 0x58b

    .line 776
    :cond_6b
    const/16 v0, 0x58c

    if-ge p2, v0, :cond_6c

    .line 777
    invoke-direct {p0, p1}, Ldrg;->H(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 778
    const/16 p2, 0x58c

    .line 781
    :cond_6c
    const/16 v0, 0x58d

    if-ge p2, v0, :cond_6d

    .line 782
    invoke-direct {p0, p1}, Ldrg;->I(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 783
    const/16 p2, 0x58d

    .line 786
    :cond_6d
    const/16 v0, 0x58e

    if-ge p2, v0, :cond_6e

    .line 787
    invoke-direct {p0, p1}, Ldrg;->J(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 788
    const/16 p2, 0x58e

    .line 791
    :cond_6e
    const/16 v0, 0x5dc

    if-ge p2, v0, :cond_6f

    .line 792
    invoke-direct {p0, p1}, Ldrg;->K(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 793
    const/16 p2, 0x5dc

    .line 796
    :cond_6f
    const/16 v0, 0x5dd

    if-ge p2, v0, :cond_70

    .line 797
    invoke-direct {p0, p1}, Ldrg;->L(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 798
    const/16 p2, 0x5dd

    .line 801
    :cond_70
    const/16 v0, 0x5de

    if-ge p2, v0, :cond_71

    .line 802
    invoke-direct {p0, p1}, Ldrg;->M(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 803
    const/16 p2, 0x5de

    .line 806
    :cond_71
    const/16 v0, 0x5df

    if-ge p2, v0, :cond_72

    .line 807
    invoke-direct {p0, p1}, Ldrg;->N(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 808
    const/16 p2, 0x5df

    .line 811
    :cond_72
    const/16 v0, 0x5e0

    if-ge p2, v0, :cond_73

    .line 812
    invoke-direct {p0, p1}, Ldrg;->O(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 813
    const/16 p2, 0x5e0

    .line 816
    :cond_73
    const/16 v0, 0x5e1

    if-ge p2, v0, :cond_74

    .line 817
    invoke-direct {p0, p1}, Ldrg;->P(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 818
    const/16 p2, 0x5e1

    .line 821
    :cond_74
    const/16 v0, 0x5e2

    if-ge p2, v0, :cond_75

    .line 822
    invoke-direct {p0, p1}, Ldrg;->Q(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 823
    const/16 p2, 0x5e2

    .line 826
    :cond_75
    const/16 v0, 0x5e3

    if-ge p2, v0, :cond_76

    .line 827
    invoke-direct {p0, p1}, Ldrg;->R(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 828
    const/16 p2, 0x5e3

    .line 831
    :cond_76
    const/16 v0, 0x5e4

    if-ge p2, v0, :cond_77

    .line 832
    invoke-direct {p0, p1}, Ldrg;->S(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 833
    const/16 p2, 0x5e4

    .line 836
    :cond_77
    const/16 v0, 0x5e5

    if-ge p2, v0, :cond_78

    .line 837
    invoke-direct {p0, p1}, Ldrg;->T(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 838
    const/16 p2, 0x5e5

    .line 841
    :cond_78
    const/16 v0, 0x5e6

    if-ge p2, v0, :cond_79

    .line 842
    invoke-direct {p0, p1}, Ldrg;->U(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 843
    const/16 p2, 0x5e6

    .line 846
    :cond_79
    const/16 v0, 0x5e7

    if-ge p2, v0, :cond_7a

    .line 847
    invoke-direct {p0, p1}, Ldrg;->V(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 848
    const/16 p2, 0x5e7

    .line 851
    :cond_7a
    const/16 v0, 0x5e8

    if-ge p2, v0, :cond_7b

    .line 852
    invoke-direct {p0, p1}, Ldrg;->W(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 853
    const/16 p2, 0x5e8

    .line 856
    :cond_7b
    const/16 v0, 0x5e9

    if-ge p2, v0, :cond_7c

    .line 857
    invoke-direct {p0, p1}, Ldrg;->X(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 858
    const/16 p2, 0x5e9

    .line 861
    :cond_7c
    const/16 v0, 0x5ea

    if-ge p2, v0, :cond_7d

    .line 862
    invoke-direct {p0, p1}, Ldrg;->Y(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 863
    const/16 p2, 0x5ea

    .line 866
    :cond_7d
    const/16 v0, 0x5eb

    if-ge p2, v0, :cond_7e

    .line 867
    invoke-direct {p0, p1}, Ldrg;->Z(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 868
    const/16 p2, 0x5eb

    .line 871
    :cond_7e
    const/16 v0, 0x5ec

    if-ge p2, v0, :cond_7f

    .line 872
    invoke-direct {p0, p1}, Ldrg;->aa(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 873
    const/16 p2, 0x5ec

    .line 876
    :cond_7f
    const/16 v0, 0x5ed

    if-ge p2, v0, :cond_80

    .line 877
    invoke-direct {p0, p1}, Ldrg;->ab(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 878
    const/16 p2, 0x5ed

    .line 881
    :cond_80
    const/16 v0, 0x5ee

    if-ge p2, v0, :cond_81

    .line 882
    invoke-direct {p0, p1}, Ldrg;->ac(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 883
    const/16 p2, 0x5ee

    .line 886
    :cond_81
    const/16 v0, 0x5ef

    if-ge p2, v0, :cond_82

    .line 887
    invoke-direct {p0, p1}, Ldrg;->ad(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 888
    const/16 p2, 0x5ef

    .line 891
    :cond_82
    const/16 v0, 0x5f0

    if-ge p2, v0, :cond_83

    .line 892
    invoke-direct {p0, p1}, Ldrg;->ae(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 893
    const/16 p2, 0x5f0

    .line 896
    :cond_83
    const/16 v0, 0x5f1

    if-ge p2, v0, :cond_84

    .line 897
    invoke-direct {p0, p1}, Ldrg;->af(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 898
    const/16 p2, 0x5f1

    .line 901
    :cond_84
    const/16 v0, 0x5f2

    if-ge p2, v0, :cond_85

    .line 902
    invoke-direct {p0, p1}, Ldrg;->ag(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 903
    const/16 p2, 0x5f2

    .line 906
    :cond_85
    const/16 v0, 0x5f3

    if-ge p2, v0, :cond_86

    .line 907
    invoke-direct {p0, p1}, Ldrg;->ah(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 908
    const/16 p2, 0x5f3

    .line 911
    :cond_86
    const/16 v0, 0x5f4

    if-ge p2, v0, :cond_87

    .line 912
    invoke-direct {p0, p1}, Ldrg;->ai(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 913
    const/16 p2, 0x5f4

    .line 916
    :cond_87
    const/16 v0, 0x5f5

    if-ge p2, v0, :cond_88

    .line 917
    invoke-direct {p0, p1}, Ldrg;->aj(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 918
    const/16 p2, 0x5f5

    .line 921
    :cond_88
    const/16 v0, 0x5f6

    if-ge p2, v0, :cond_89

    .line 922
    invoke-direct {p0, p1}, Ldrg;->ak(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 923
    const/16 p2, 0x5f6

    .line 926
    :cond_89
    const/16 v0, 0x5f7

    if-ge p2, v0, :cond_8a

    .line 927
    invoke-direct {p0, p1}, Ldrg;->al(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 928
    const/16 p2, 0x5f7

    .line 931
    :cond_8a
    const/16 v0, 0x5f8

    if-ge p2, v0, :cond_8b

    .line 932
    invoke-direct {p0, p1}, Ldrg;->am(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 933
    const/16 p2, 0x5f8

    .line 936
    :cond_8b
    const/16 v0, 0x5f9

    if-ge p2, v0, :cond_8c

    .line 937
    invoke-direct {p0, p1}, Ldrg;->an(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 938
    const/16 p2, 0x5f9

    .line 941
    :cond_8c
    const/16 v0, 0x5fa

    if-ge p2, v0, :cond_8d

    .line 942
    invoke-direct {p0, p1}, Ldrg;->ao(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 943
    const/16 p2, 0x5fa

    .line 946
    :cond_8d
    const/16 v0, 0x5fb

    if-ge p2, v0, :cond_8e

    .line 947
    invoke-direct {p0, p1}, Ldrg;->ap(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 948
    const/16 p2, 0x5fb

    .line 951
    :cond_8e
    const/16 v0, 0x5fc

    if-ge p2, v0, :cond_8f

    .line 952
    invoke-direct {p0, p1}, Ldrg;->aq(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 953
    const/16 p2, 0x5fc

    .line 956
    :cond_8f
    const/16 v0, 0x5fd

    if-ge p2, v0, :cond_90

    .line 957
    invoke-direct {p0, p1}, Ldrg;->ar(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 958
    const/16 p2, 0x5fd

    .line 961
    :cond_90
    const/16 v0, 0x5fe

    if-ge p2, v0, :cond_91

    .line 962
    invoke-direct {p0, p1}, Ldrg;->as(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 963
    const/16 p2, 0x5fe

    .line 966
    :cond_91
    const/16 v0, 0x5ff

    if-ge p2, v0, :cond_92

    .line 967
    invoke-direct {p0, p1}, Ldrg;->at(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 968
    const/16 p2, 0x5ff

    .line 971
    :cond_92
    const/16 v0, 0x600

    if-ge p2, v0, :cond_93

    .line 972
    invoke-direct {p0, p1}, Ldrg;->au(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 973
    const/16 p2, 0x600

    .line 976
    :cond_93
    const/16 v0, 0x601

    if-ge p2, v0, :cond_94

    .line 977
    invoke-direct {p0, p1}, Ldrg;->av(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 978
    const/16 p2, 0x601

    .line 981
    :cond_94
    const/16 v0, 0x602

    if-ge p2, v0, :cond_95

    .line 982
    invoke-direct {p0, p1}, Ldrg;->aw(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 983
    const/16 p2, 0x602

    .line 986
    :cond_95
    const/16 v0, 0x603

    if-ge p2, v0, :cond_96

    .line 987
    invoke-direct {p0, p1}, Ldrg;->ax(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 988
    const/16 p2, 0x603

    .line 991
    :cond_96
    const/16 v0, 0x604

    if-ge p2, v0, :cond_97

    .line 992
    invoke-direct {p0, p1}, Ldrg;->ay(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 993
    const/16 p2, 0x604

    .line 996
    :cond_97
    const/16 v0, 0x605

    if-ge p2, v0, :cond_98

    .line 997
    invoke-direct {p0, p1}, Ldrg;->az(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 998
    const/16 p2, 0x605

    .line 1001
    :cond_98
    const/16 v0, 0x606

    if-ge p2, v0, :cond_99

    .line 1002
    invoke-direct {p0, p1}, Ldrg;->aA(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1003
    const/16 p2, 0x606

    .line 1006
    :cond_99
    const/16 v0, 0x607

    if-ge p2, v0, :cond_9a

    .line 1007
    invoke-direct {p0, p1}, Ldrg;->aB(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1008
    const/16 p2, 0x607

    .line 1011
    :cond_9a
    const/16 v0, 0x608

    if-ge p2, v0, :cond_9b

    .line 1012
    invoke-direct {p0, p1}, Ldrg;->aC(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1013
    const/16 p2, 0x608

    .line 1016
    :cond_9b
    const/16 v0, 0x609

    if-ge p2, v0, :cond_9c

    .line 1017
    invoke-direct {p0, p1}, Ldrg;->aD(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1018
    const/16 p2, 0x609

    .line 1021
    :cond_9c
    const/16 v0, 0x60a

    if-ge p2, v0, :cond_9d

    .line 1022
    invoke-direct {p0, p1}, Ldrg;->aE(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1023
    const/16 p2, 0x60a

    .line 1026
    :cond_9d
    const/16 v0, 0x60b

    if-ge p2, v0, :cond_9e

    .line 1027
    invoke-direct {p0, p1}, Ldrg;->aF(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1028
    const/16 p2, 0x60b

    .line 1031
    :cond_9e
    const/16 v0, 0x60c

    if-ge p2, v0, :cond_9f

    .line 1032
    invoke-direct {p0, p1}, Ldrg;->aG(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1033
    const/16 p2, 0x60c

    .line 1036
    :cond_9f
    const/16 v0, 0x60d

    if-ge p2, v0, :cond_a0

    .line 1037
    invoke-direct {p0, p1}, Ldrg;->aH(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1038
    const/16 p2, 0x60d

    .line 1041
    :cond_a0
    const/16 v0, 0x60e

    if-ge p2, v0, :cond_a1

    .line 1042
    invoke-direct {p0, p1}, Ldrg;->aI(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1043
    const/16 p2, 0x60e

    .line 1046
    :cond_a1
    const/16 v0, 0x60f

    if-ge p2, v0, :cond_a2

    .line 1047
    invoke-direct {p0, p1}, Ldrg;->aJ(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1048
    const/16 p2, 0x60f

    .line 1051
    :cond_a2
    const/16 v0, 0x610

    if-ge p2, v0, :cond_a3

    .line 1052
    invoke-direct {p0, p1}, Ldrg;->aK(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1053
    const/16 p2, 0x610

    .line 1056
    :cond_a3
    const/16 v0, 0x611

    if-ge p2, v0, :cond_a4

    .line 1057
    invoke-direct {p0, p1}, Ldrg;->aL(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1058
    const/16 p2, 0x611

    .line 1061
    :cond_a4
    const/16 v0, 0x612

    if-ge p2, v0, :cond_a5

    .line 1062
    invoke-direct {p0, p1}, Ldrg;->aM(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1063
    const/16 p2, 0x612

    .line 1066
    :cond_a5
    const/16 v0, 0x613

    if-ge p2, v0, :cond_a6

    .line 1067
    invoke-direct {p0, p1}, Ldrg;->aN(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1068
    const/16 p2, 0x613

    .line 1071
    :cond_a6
    const/16 v0, 0x614

    if-ge p2, v0, :cond_a7

    .line 1072
    invoke-direct {p0, p1}, Ldrg;->aO(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1073
    const/16 p2, 0x614

    .line 1076
    :cond_a7
    const/16 v0, 0x615

    if-ge p2, v0, :cond_a8

    .line 1077
    invoke-direct {p0, p1}, Ldrg;->aP(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1078
    const/16 p2, 0x615

    .line 1081
    :cond_a8
    const/16 v0, 0x616

    if-ge p2, v0, :cond_a9

    .line 1082
    invoke-direct {p0, p1}, Ldrg;->aQ(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1083
    const/16 p2, 0x616

    .line 1086
    :cond_a9
    const/16 v0, 0x617

    if-ge p2, v0, :cond_aa

    .line 1087
    invoke-direct {p0, p1}, Ldrg;->aR(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1088
    const/16 p2, 0x617

    .line 1091
    :cond_aa
    const/16 v0, 0x618

    if-ge p2, v0, :cond_ab

    .line 1092
    invoke-direct {p0, p1}, Ldrg;->aS(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1093
    const/16 p2, 0x618

    .line 1096
    :cond_ab
    const/16 v0, 0x619

    if-ge p2, v0, :cond_ac

    .line 1097
    invoke-direct {p0, p1}, Ldrg;->aT(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1098
    const/16 p2, 0x619

    .line 1101
    :cond_ac
    const/16 v0, 0x61a

    if-ge p2, v0, :cond_ad

    .line 1102
    invoke-direct {p0, p1}, Ldrg;->aU(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1103
    const/16 p2, 0x61a

    .line 1106
    :cond_ad
    const/16 v0, 0x61b

    if-ge p2, v0, :cond_ae

    .line 1107
    invoke-direct {p0, p1}, Ldrg;->aV(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1108
    const/16 p2, 0x61b

    .line 1111
    :cond_ae
    const/16 v0, 0x61c

    if-ge p2, v0, :cond_af

    .line 1112
    invoke-direct {p0, p1}, Ldrg;->aW(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1113
    const/16 p2, 0x61c

    .line 1116
    :cond_af
    const/16 v0, 0x61d

    if-ge p2, v0, :cond_b0

    .line 1117
    invoke-direct {p0, p1}, Ldrg;->aX(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1118
    const/16 p2, 0x61d

    .line 1121
    :cond_b0
    const/16 v0, 0x61e

    if-ge p2, v0, :cond_b1

    .line 1122
    invoke-direct {p0, p1}, Ldrg;->aY(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1123
    const/16 p2, 0x61e

    .line 1126
    :cond_b1
    const/16 v0, 0x61f

    if-ge p2, v0, :cond_b2

    .line 1127
    invoke-direct {p0, p1}, Ldrg;->aZ(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1128
    const/16 p2, 0x61f

    .line 1131
    :cond_b2
    const/16 v0, 0x640

    if-ge p2, v0, :cond_b3

    .line 1132
    invoke-direct {p0, p1}, Ldrg;->ba(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1133
    const/16 p2, 0x640

    .line 1136
    :cond_b3
    const/16 v0, 0x641

    if-ge p2, v0, :cond_b4

    .line 1137
    invoke-direct {p0, p1}, Ldrg;->bb(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1138
    const/16 p2, 0x641

    .line 1141
    :cond_b4
    const/16 v0, 0x642

    if-ge p2, v0, :cond_b5

    .line 1142
    invoke-direct {p0, p1}, Ldrg;->bc(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1143
    const/16 p2, 0x642

    .line 1146
    :cond_b5
    const/16 v0, 0x643

    if-ge p2, v0, :cond_b6

    .line 1147
    invoke-direct {p0, p1}, Ldrg;->bd(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1148
    const/16 p2, 0x643

    .line 1151
    :cond_b6
    const/16 v0, 0x644

    if-ge p2, v0, :cond_b7

    .line 1152
    invoke-direct {p0, p1}, Ldrg;->be(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1153
    const/16 p2, 0x644

    .line 1156
    :cond_b7
    const/16 v0, 0x645

    if-ge p2, v0, :cond_b8

    .line 1157
    invoke-direct {p0, p1}, Ldrg;->bf(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1158
    const/16 p2, 0x645

    .line 1161
    :cond_b8
    const/16 v0, 0x646

    if-ge p2, v0, :cond_b9

    .line 1162
    invoke-direct {p0, p1}, Ldrg;->bg(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1163
    const/16 p2, 0x646

    .line 1166
    :cond_b9
    const/16 v0, 0x647

    if-ge p2, v0, :cond_ba

    .line 1167
    invoke-direct {p0, p1}, Ldrg;->bh(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1168
    const/16 p2, 0x647

    .line 1171
    :cond_ba
    const/16 v0, 0x648

    if-ge p2, v0, :cond_bb

    .line 1172
    invoke-direct {p0, p1}, Ldrg;->bi(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1173
    const/16 p2, 0x648

    .line 1176
    :cond_bb
    const/16 v0, 0x649

    if-ge p2, v0, :cond_bc

    .line 1177
    invoke-direct {p0, p1}, Ldrg;->bj(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1178
    const/16 p2, 0x649

    .line 1181
    :cond_bc
    const/16 v0, 0x64a

    if-ge p2, v0, :cond_bd

    .line 1182
    invoke-direct {p0, p1}, Ldrg;->bk(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1183
    const/16 p2, 0x64a

    .line 1186
    :cond_bd
    const/16 v0, 0x64b

    if-ge p2, v0, :cond_be

    .line 1187
    invoke-direct {p0, p1}, Ldrg;->bl(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1188
    const/16 p2, 0x64b

    .line 1191
    :cond_be
    const/16 v0, 0x64c

    if-ge p2, v0, :cond_bf

    .line 1192
    invoke-direct {p0, p1}, Ldrg;->bm(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1193
    const/16 p2, 0x64c

    .line 1196
    :cond_bf
    const/16 v0, 0x64d

    if-ge p2, v0, :cond_c0

    .line 1197
    invoke-direct {p0, p1}, Ldrg;->bn(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1198
    const/16 p2, 0x64d

    .line 1201
    :cond_c0
    const/16 v0, 0x64e

    if-ge p2, v0, :cond_c1

    .line 1202
    invoke-direct {p0, p1}, Ldrg;->bo(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1203
    const/16 p2, 0x64e

    .line 1206
    :cond_c1
    const/16 v0, 0x64f

    if-ge p2, v0, :cond_c2

    .line 1207
    invoke-direct {p0, p1}, Ldrg;->bp(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1208
    const/16 p2, 0x64f

    .line 1211
    :cond_c2
    const/16 v0, 0x650

    if-ge p2, v0, :cond_c3

    .line 1212
    invoke-direct {p0, p1}, Ldrg;->bq(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1213
    const/16 p2, 0x650

    .line 1216
    :cond_c3
    const/16 v0, 0x651

    if-ge p2, v0, :cond_c4

    .line 1217
    invoke-direct {p0, p1}, Ldrg;->br(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1218
    const/16 p2, 0x651

    .line 1221
    :cond_c4
    const/16 v0, 0x652

    if-ge p2, v0, :cond_c5

    .line 1222
    invoke-direct {p0, p1}, Ldrg;->bs(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1223
    const/16 p2, 0x652

    .line 1226
    :cond_c5
    const/16 v0, 0x653

    if-ge p2, v0, :cond_c6

    .line 1227
    const/16 p2, 0x653

    .line 1231
    :cond_c6
    const/16 v0, 0x654

    if-ge p2, v0, :cond_c7

    .line 1232
    invoke-direct {p0, p1}, Ldrg;->bt(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1233
    const/16 p2, 0x654

    .line 1236
    :cond_c7
    const/16 v0, 0x655

    if-ge p2, v0, :cond_c8

    .line 1237
    invoke-direct {p0, p1}, Ldrg;->bu(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1238
    const/16 p2, 0x655

    .line 1241
    :cond_c8
    const/16 v0, 0x656

    if-ge p2, v0, :cond_c9

    .line 1242
    invoke-direct {p0, p1}, Ldrg;->bv(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1243
    const/16 p2, 0x656

    .line 1246
    :cond_c9
    const/16 v0, 0x657

    if-ge p2, v0, :cond_ca

    .line 1247
    invoke-direct {p0, p1}, Ldrg;->bw(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1248
    const/16 p2, 0x657

    .line 1251
    :cond_ca
    const/16 v0, 0x658

    if-ge p2, v0, :cond_cb

    .line 1252
    invoke-direct {p0, p1}, Ldrg;->bx(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1253
    const/16 p2, 0x658

    .line 1256
    :cond_cb
    const/16 v0, 0x659

    if-ge p2, v0, :cond_cc

    .line 1257
    invoke-direct {p0, p1}, Ldrg;->by(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1258
    const/16 p2, 0x659

    .line 1261
    :cond_cc
    const/16 v0, 0x65a

    if-ge p2, v0, :cond_cd

    .line 1262
    invoke-direct {p0, p1}, Ldrg;->bz(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1263
    const/16 p2, 0x65a

    .line 1266
    :cond_cd
    const/16 v0, 0x65b

    if-ge p2, v0, :cond_ce

    .line 1267
    invoke-direct {p0, p1}, Ldrg;->bA(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1268
    const/16 p2, 0x65b

    .line 1271
    :cond_ce
    const/16 v0, 0x65c

    if-ge p2, v0, :cond_cf

    .line 1272
    invoke-direct {p0, p1}, Ldrg;->bB(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1273
    const/16 p2, 0x65c

    .line 1276
    :cond_cf
    const/16 v0, 0x65d

    if-ge p2, v0, :cond_d0

    .line 1277
    invoke-direct {p0, p1}, Ldrg;->bC(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1278
    const/16 p2, 0x65d

    .line 1281
    :cond_d0
    const/16 v0, 0x65e

    if-ge p2, v0, :cond_d1

    .line 1282
    invoke-direct {p0, p1}, Ldrg;->bD(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1283
    const/16 p2, 0x65e

    .line 1286
    :cond_d1
    const/16 v0, 0x65f

    if-ge p2, v0, :cond_d2

    .line 1287
    invoke-direct {p0, p1}, Ldrg;->bF(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1288
    const/16 p2, 0x65f

    .line 1291
    :cond_d2
    const/16 v0, 0x660

    if-ge p2, v0, :cond_d3

    .line 1292
    const/16 p2, 0x660

    .line 1296
    :cond_d3
    const/16 v0, 0x661

    if-ge p2, v0, :cond_d4

    .line 1297
    invoke-direct {p0, p1}, Ldrg;->bG(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1298
    const/16 p2, 0x661

    .line 1301
    :cond_d4
    const/16 v0, 0x662

    if-ge p2, v0, :cond_d5

    .line 1302
    const/16 p2, 0x662

    .line 1306
    :cond_d5
    const/16 v0, 0x663

    if-ge p2, v0, :cond_d6

    .line 1307
    const/16 p2, 0x663

    .line 1311
    :cond_d6
    const/16 v0, 0x664

    if-ge p2, v0, :cond_d7

    .line 1312
    const/16 p2, 0x664

    .line 1316
    :cond_d7
    const/16 v0, 0x665

    if-ge p2, v0, :cond_d8

    .line 1317
    invoke-direct {p0, p1}, Ldrg;->bH(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1318
    const/16 p2, 0x665

    .line 1321
    :cond_d8
    const/16 v0, 0x666

    if-ge p2, v0, :cond_d9

    .line 1322
    invoke-direct {p0, p1}, Ldrg;->bI(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1323
    const/16 p2, 0x666

    .line 1326
    :cond_d9
    const/16 v0, 0x667

    if-ge p2, v0, :cond_da

    .line 1327
    invoke-direct {p0, p1}, Ldrg;->bJ(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1328
    const/16 p2, 0x667

    .line 1331
    :cond_da
    const/16 v0, 0x668

    if-ge p2, v0, :cond_db

    .line 1332
    invoke-direct {p0, p1}, Ldrg;->bK(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1333
    const/16 p2, 0x668

    .line 1336
    :cond_db
    const/16 v0, 0x669

    if-ge p2, v0, :cond_dc

    .line 1337
    invoke-direct {p0, p1}, Ldrg;->bL(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1338
    const/16 p2, 0x669

    .line 1341
    :cond_dc
    const/16 v0, 0x66a

    if-ge p2, v0, :cond_dd

    .line 1342
    invoke-direct {p0, p1}, Ldrg;->bM(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1343
    const/16 p2, 0x66a

    .line 1346
    :cond_dd
    const/16 v0, 0x66b

    if-ge p2, v0, :cond_de

    .line 1347
    invoke-direct {p0, p1}, Ldrg;->bN(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1348
    const/16 p2, 0x66b

    .line 1351
    :cond_de
    const/16 v0, 0x66c

    if-ge p2, v0, :cond_df

    .line 1352
    invoke-direct {p0, p1}, Ldrg;->bO(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1353
    const/16 p2, 0x66c

    .line 1356
    :cond_df
    const/16 v0, 0x66d

    if-ge p2, v0, :cond_e0

    .line 1357
    invoke-direct {p0, p1}, Ldrg;->bP(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1358
    const/16 p2, 0x66d

    .line 1361
    :cond_e0
    const/16 v0, 0x66e

    if-ge p2, v0, :cond_e1

    .line 1362
    invoke-direct {p0, p1}, Ldrg;->bQ(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1363
    const/16 p2, 0x66e

    .line 1366
    :cond_e1
    const/16 v0, 0x66f

    if-ge p2, v0, :cond_e2

    .line 1367
    invoke-direct {p0, p1}, Ldrg;->bR(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1368
    const/16 p2, 0x66f

    .line 1371
    :cond_e2
    const/16 v0, 0x670

    if-ge p2, v0, :cond_e3

    .line 1372
    invoke-direct {p0, p1}, Ldrg;->bS(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1373
    const/16 p2, 0x670

    .line 1376
    :cond_e3
    const/16 v0, 0x671

    if-ge p2, v0, :cond_e4

    .line 1377
    invoke-direct {p0, p1}, Ldrg;->bT(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1378
    const/16 p2, 0x671

    .line 1381
    :cond_e4
    const/16 v0, 0x672

    if-ge p2, v0, :cond_e5

    .line 1382
    invoke-direct {p0, p1}, Ldrg;->bU(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1383
    const/16 p2, 0x672

    .line 1386
    :cond_e5
    const/16 v0, 0x673

    if-ge p2, v0, :cond_e6

    .line 1387
    invoke-direct {p0, p1}, Ldrg;->bV(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1388
    const/16 p2, 0x673

    .line 1391
    :cond_e6
    const/16 v0, 0x674

    if-ge p2, v0, :cond_e7

    .line 1392
    invoke-direct {p0, p1}, Ldrg;->bW(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1393
    const/16 p2, 0x674

    .line 1396
    :cond_e7
    const/16 v0, 0x675

    if-ge p2, v0, :cond_e8

    .line 1397
    invoke-direct {p0, p1}, Ldrg;->bX(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1398
    const/16 p2, 0x675

    .line 1401
    :cond_e8
    const/16 v0, 0x676

    if-ge p2, v0, :cond_e9

    .line 1402
    invoke-direct {p0, p1}, Ldrg;->bY(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1403
    const/16 p2, 0x676

    .line 1406
    :cond_e9
    const/16 v0, 0x677

    if-ge p2, v0, :cond_ea

    .line 1407
    invoke-direct {p0, p1}, Ldrg;->bZ(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1408
    const/16 p2, 0x677

    .line 1411
    :cond_ea
    const/16 v0, 0x678

    if-ge p2, v0, :cond_eb

    .line 1412
    invoke-direct {p0, p1}, Ldrg;->ca(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1413
    const/16 p2, 0x678

    .line 1416
    :cond_eb
    const/16 v0, 0x679

    if-ge p2, v0, :cond_ec

    .line 1417
    invoke-direct {p0, p1}, Ldrg;->cb(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1418
    const/16 p2, 0x679

    .line 1421
    :cond_ec
    const/16 v0, 0x67a

    if-ge p2, v0, :cond_ed

    .line 1422
    invoke-direct {p0, p1}, Ldrg;->cc(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1423
    const/16 p2, 0x67a

    .line 1426
    :cond_ed
    const/16 v0, 0x67b

    if-ge p2, v0, :cond_ee

    .line 1427
    invoke-direct {p0, p1}, Ldrg;->cd(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1428
    const/16 p2, 0x67b

    .line 1433
    :cond_ee
    const/16 v0, 0x67d

    if-ge p2, v0, :cond_ef

    .line 1434
    invoke-direct {p0, p1}, Ldrg;->ce(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1435
    const/16 p2, 0x67d

    .line 1438
    :cond_ef
    const/16 v0, 0x67e

    if-ge p2, v0, :cond_f0

    .line 1439
    invoke-direct {p0, p1}, Ldrg;->cf(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1440
    const/16 p2, 0x67e

    .line 1443
    :cond_f0
    const/16 v0, 0x67f

    if-ge p2, v0, :cond_f1

    .line 1444
    invoke-direct {p0, p1}, Ldrg;->cg(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1445
    const/16 p2, 0x67f

    .line 1448
    :cond_f1
    const/16 v0, 0x680

    if-ge p2, v0, :cond_f2

    .line 1449
    invoke-direct {p0, p1}, Ldrg;->ch(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1450
    const/16 p2, 0x680

    .line 1453
    :cond_f2
    const/16 v0, 0x681

    if-ge p2, v0, :cond_f3

    .line 1454
    invoke-direct {p0, p1}, Ldrg;->ci(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1455
    const/16 p2, 0x681

    .line 1458
    :cond_f3
    const/16 v0, 0x682

    if-ge p2, v0, :cond_f4

    .line 1459
    invoke-direct {p0, p1}, Ldrg;->cj(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1460
    const/16 p2, 0x682

    .line 1463
    :cond_f4
    const/16 v0, 0x683

    if-ge p2, v0, :cond_f5

    .line 1464
    invoke-direct {p0, p1}, Ldrg;->ck(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1465
    const/16 p2, 0x683

    .line 1468
    :cond_f5
    const/16 v0, 0x684

    if-ge p2, v0, :cond_f6

    .line 1469
    invoke-direct {p0, p1}, Ldrg;->cl(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1470
    const/16 p2, 0x684

    .line 1473
    :cond_f6
    const/16 v0, 0x685

    if-ge p2, v0, :cond_f7

    .line 1474
    invoke-direct {p0}, Ldrg;->d()V

    .line 1475
    const/16 p2, 0x685

    .line 1478
    :cond_f7
    const/16 v0, 0x686

    if-ge p2, v0, :cond_f8

    .line 1479
    invoke-direct {p0, p1}, Ldrg;->cm(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1480
    const/16 p2, 0x686

    .line 1483
    :cond_f8
    const/16 v0, 0x687

    if-ge p2, v0, :cond_f9

    .line 1484
    const/16 p2, 0x687

    .line 1488
    :cond_f9
    const/16 v0, 0x688

    if-ge p2, v0, :cond_fa

    .line 1489
    invoke-direct {p0, p1}, Ldrg;->cn(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1490
    const/16 p2, 0x688

    .line 1493
    :cond_fa
    const/16 v0, 0x689

    if-ge p2, v0, :cond_fb

    .line 1494
    invoke-direct {p0, p1}, Ldrg;->co(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1495
    const/16 p2, 0x689

    .line 1498
    :cond_fb
    const/16 v0, 0x68a

    if-ge p2, v0, :cond_fc

    .line 1499
    invoke-direct {p0, p1}, Ldrg;->cp(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1500
    const/16 p2, 0x68a

    .line 1503
    :cond_fc
    const/16 v0, 0x68b

    if-ge p2, v0, :cond_fd

    .line 1504
    invoke-direct {p0, p1}, Ldrg;->cq(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1505
    const/16 p2, 0x68b

    .line 1510
    :cond_fd
    const/16 v0, 0x68d

    if-ge p2, v0, :cond_fe

    .line 1511
    invoke-direct {p0, p1}, Ldrg;->cr(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1512
    const/16 p2, 0x68d

    .line 1515
    :cond_fe
    const/16 v0, 0x68e

    if-ge p2, v0, :cond_ff

    .line 1516
    invoke-direct {p0, p1}, Ldrg;->cs(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1517
    const/16 p2, 0x68e

    .line 1520
    :cond_ff
    const/16 v0, 0x68f

    if-ge p2, v0, :cond_100

    .line 1521
    invoke-direct {p0, p1}, Ldrg;->ct(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1522
    const/16 p2, 0x68f

    .line 1525
    :cond_100
    const/16 v0, 0x690

    if-ge p2, v0, :cond_101

    .line 1526
    invoke-direct {p0, p1}, Ldrg;->cu(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1527
    const/16 p2, 0x690

    .line 1530
    :cond_101
    const/16 v0, 0x691

    if-ge p2, v0, :cond_102

    .line 1531
    invoke-direct {p0, p1}, Ldrg;->cv(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1532
    const/16 p2, 0x691

    .line 1535
    :cond_102
    const/16 v0, 0x692

    if-ge p2, v0, :cond_103

    .line 1536
    invoke-direct {p0, p1}, Ldrg;->cw(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1537
    const/16 p2, 0x692

    .line 1540
    :cond_103
    const/16 v0, 0x693

    if-ge p2, v0, :cond_104

    .line 1541
    invoke-direct {p0, p1}, Ldrg;->cx(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1542
    const/16 p2, 0x693

    .line 1545
    :cond_104
    const/16 v0, 0x694

    if-ge p2, v0, :cond_105

    .line 1546
    invoke-direct {p0, p1}, Ldrg;->cy(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1547
    const/16 p2, 0x694

    .line 1550
    :cond_105
    const/16 v0, 0x695

    if-ge p2, v0, :cond_106

    .line 1551
    invoke-direct {p0, p1}, Ldrg;->cz(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1552
    const/16 p2, 0x695

    .line 1555
    :cond_106
    const/16 v0, 0x696

    if-ge p2, v0, :cond_107

    .line 1556
    invoke-direct {p0, p1}, Ldrg;->cA(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1557
    const/16 p2, 0x696

    .line 1560
    :cond_107
    const/16 v0, 0x697

    if-ge p2, v0, :cond_108

    .line 1561
    invoke-direct {p0, p1}, Ldrg;->cB(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1562
    const/16 p2, 0x697

    .line 1565
    :cond_108
    const/16 v0, 0x698

    if-ge p2, v0, :cond_109

    .line 1566
    invoke-direct {p0, p1}, Ldrg;->cC(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1567
    const/16 p2, 0x698

    .line 1570
    :cond_109
    const/16 v0, 0x699

    if-ge p2, v0, :cond_10a

    .line 1572
    const/16 p2, 0x699

    .line 1575
    :cond_10a
    const/16 v0, 0x69a

    if-ge p2, v0, :cond_10b

    .line 1576
    invoke-direct {p0, p1}, Ldrg;->cD(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1577
    const/16 p2, 0x69a

    .line 1580
    :cond_10b
    const/16 v0, 0x69b

    if-ge p2, v0, :cond_10c

    .line 1581
    invoke-direct {p0, p1}, Ldrg;->cE(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1582
    const/16 p2, 0x69b

    .line 1585
    :cond_10c
    const/16 v0, 0x69c

    if-ge p2, v0, :cond_10d

    .line 1586
    invoke-direct {p0, p1}, Ldrg;->cF(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1587
    const/16 p2, 0x69c

    .line 1590
    :cond_10d
    const/16 v0, 0x69d

    if-ge p2, v0, :cond_10e

    .line 1591
    invoke-direct {p0, p1}, Ldrg;->cG(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1592
    const/16 p2, 0x69d

    .line 1595
    :cond_10e
    const/16 v0, 0x69e

    if-ge p2, v0, :cond_10f

    .line 1599
    const/16 p2, 0x69e

    .line 1602
    :cond_10f
    const/16 v0, 0x69f

    if-ge p2, v0, :cond_110

    .line 1603
    invoke-direct {p0, p1}, Ldrg;->cH(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1604
    const/16 p2, 0x69f

    .line 1607
    :cond_110
    const/16 v0, 0x6a0

    if-ge p2, v0, :cond_111

    .line 1608
    invoke-direct {p0, p1}, Ldrg;->cI(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1609
    const/16 p2, 0x6a0

    .line 1612
    :cond_111
    const/16 v0, 0x6a1

    if-ge p2, v0, :cond_112

    .line 1613
    invoke-direct {p0, p1}, Ldrg;->cJ(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1614
    const/16 p2, 0x6a1

    .line 1617
    :cond_112
    const/16 v0, 0x6a3

    if-ge p2, v0, :cond_113

    .line 1618
    invoke-direct {p0, p1}, Ldrg;->cK(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1619
    const/16 p2, 0x6a3

    .line 1622
    :cond_113
    const/16 v0, 0x6a4

    if-ge p2, v0, :cond_114

    .line 1623
    invoke-direct {p0, p1}, Ldrg;->cL(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1624
    const/16 p2, 0x6a4

    .line 1627
    :cond_114
    const/16 v0, 0x6a5

    if-ge p2, v0, :cond_115

    .line 1628
    invoke-direct {p0, p1}, Ldrg;->cM(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1629
    const/16 p2, 0x6a5

    .line 1632
    :cond_115
    const/16 v0, 0x6a6

    if-ge p2, v0, :cond_116

    .line 1633
    invoke-direct {p0, p1}, Ldrg;->cN(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1634
    const/16 p2, 0x6a6

    .line 1637
    :cond_116
    const/16 v0, 0x6a7

    if-ge p2, v0, :cond_117

    .line 1638
    invoke-direct {p0, p1}, Ldrg;->cO(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1639
    const/16 p2, 0x6a7

    .line 1642
    :cond_117
    const/16 v0, 0x6a8

    if-ge p2, v0, :cond_118

    .line 1643
    invoke-direct {p0, p1}, Ldrg;->cP(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1644
    const/16 p2, 0x6a8

    .line 1647
    :cond_118
    const/16 v0, 0x6a9

    if-ge p2, v0, :cond_119

    .line 1648
    invoke-direct {p0, p1}, Ldrg;->cQ(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1649
    const/16 p2, 0x6a9

    .line 1652
    :cond_119
    const/16 v0, 0x6aa

    if-ge p2, v0, :cond_11a

    .line 1653
    invoke-direct {p0, p1}, Ldrg;->cR(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1654
    const/16 p2, 0x6aa

    .line 1657
    :cond_11a
    const/16 v0, 0x6ab

    if-ge p2, v0, :cond_11b

    .line 1658
    invoke-direct {p0, p1}, Ldrg;->cS(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1659
    const/16 p2, 0x6ab

    .line 1662
    :cond_11b
    invoke-virtual {p0, p1}, Ldrg;->d(Landroid/database/sqlite/SQLiteDatabase;)Z

    .line 1663
    invoke-virtual {p0, p1}, Ldrg;->c(Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1674
    iget v0, p0, Ldrg;->d:I

    if-eq v0, v4, :cond_1

    .line 1675
    iget-object v0, p0, Ldrg;->c:Landroid/content/Context;

    const-class v1, Lhei;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iget v1, p0, Ldrg;->d:I

    .line 1676
    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1677
    invoke-static {v0}, Lfud;->a(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    .line 1678
    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    invoke-static {v0, v1, v2}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1682
    iget-object v1, p0, Ldrg;->c:Landroid/content/Context;

    invoke-static {v1}, Lhqv;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    invoke-static {v0, v1, v2}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1684
    iget-object v1, p0, Ldrg;->c:Landroid/content/Context;

    .line 1685
    invoke-static {}, Lcom/google/android/apps/photos/content/GooglePhotoDownsyncProvider;->a()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1684
    invoke-static {v0, v1, v2}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 1664
    :catch_0
    move-exception v0

    .line 1665
    :try_start_3
    const-string v1, "EsDatabaseHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x37

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Failed to upgrade database: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " --> "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1667
    invoke-static {}, Lfvc;->d()Z

    move-result v1

    if-eqz v1, :cond_11c

    .line 1668
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x37

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Failed to upgrade database: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " --> "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lhzt;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1672
    :cond_11c
    invoke-virtual {p0, p1}, Ldrg;->b(Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1674
    iget v0, p0, Ldrg;->d:I

    if-eq v0, v4, :cond_1

    .line 1675
    iget-object v0, p0, Ldrg;->c:Landroid/content/Context;

    const-class v1, Lhei;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iget v1, p0, Ldrg;->d:I

    .line 1676
    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1677
    invoke-static {v0}, Lfud;->a(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    .line 1678
    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    invoke-static {v0, v1, v2}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1682
    iget-object v1, p0, Ldrg;->c:Landroid/content/Context;

    invoke-static {v1}, Lhqv;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    invoke-static {v0, v1, v2}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1684
    iget-object v1, p0, Ldrg;->c:Landroid/content/Context;

    .line 1685
    invoke-static {}, Lcom/google/android/apps/photos/content/GooglePhotoDownsyncProvider;->a()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1684
    invoke-static {v0, v1, v2}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 1674
    :catchall_0
    move-exception v0

    move-object v1, v0

    iget v0, p0, Ldrg;->d:I

    if-eq v0, v4, :cond_11d

    .line 1675
    iget-object v0, p0, Ldrg;->c:Landroid/content/Context;

    const-class v2, Lhei;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iget v2, p0, Ldrg;->d:I

    .line 1676
    invoke-interface {v0, v2}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v2, "account_name"

    invoke-interface {v0, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1677
    invoke-static {v0}, Lfud;->a(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    .line 1678
    const-string v2, "com.google.android.apps.plus.content.EsProvider"

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    invoke-static {v0, v2, v3}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1682
    iget-object v2, p0, Ldrg;->c:Landroid/content/Context;

    invoke-static {v2}, Lhqv;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    invoke-static {v0, v2, v3}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1684
    iget-object v2, p0, Ldrg;->c:Landroid/content/Context;

    .line 1685
    invoke-static {}, Lcom/google/android/apps/photos/content/GooglePhotoDownsyncProvider;->a()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 1684
    invoke-static {v0, v2, v3}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1686
    :cond_11d
    throw v1
.end method
