.class public final Ledh;
.super Llgr;
.source "PG"


# instance fields
.field private Q:Landroid/widget/CheckBox;

.field private R:Z

.field private S:Z

.field private T:Ledd;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2010
    invoke-direct {p0}, Llgr;-><init>()V

    .line 2008
    sget-object v0, Ledd;->a:Ledd;

    iput-object v0, p0, Ledh;->T:Ledd;

    .line 2011
    return-void
.end method

.method public constructor <init>(ZLedd;)V
    .locals 1

    .prologue
    .line 2014
    invoke-direct {p0}, Llgr;-><init>()V

    .line 2008
    sget-object v0, Ledd;->a:Ledd;

    iput-object v0, p0, Ledh;->T:Ledd;

    .line 2015
    iput-boolean p1, p0, Ledh;->R:Z

    .line 2016
    iput-object p2, p0, Ledh;->T:Ledd;

    .line 2017
    return-void
.end method


# virtual methods
.method public c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 10

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 2029
    if-eqz p1, :cond_0

    .line 2030
    const-string v0, "has_checked_in_id"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Ledh;->R:Z

    .line 2031
    const-string v0, "first_time_id"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Ledh;->S:Z

    .line 2032
    const-string v0, "dialog_type"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ledd;->valueOf(Ljava/lang/String;)Ledd;

    move-result-object v0

    iput-object v0, p0, Ledh;->T:Ledd;

    .line 2035
    :cond_0
    invoke-virtual {p0}, Ledh;->W_()Landroid/content/Context;

    move-result-object v4

    .line 2036
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 2038
    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2039
    const v2, 0x7f040096

    const/4 v6, 0x0

    invoke-virtual {v0, v2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .line 2040
    const v0, 0x7f10027e

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Ledh;->Q:Landroid/widget/CheckBox;

    .line 2041
    iget-object v2, p0, Ledh;->Q:Landroid/widget/CheckBox;

    iget-boolean v0, p0, Ledh;->R:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 2043
    const v0, 0x7f100238

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2045
    sget-object v2, Lecr;->a:[I

    iget-object v7, p0, Ledh;->T:Ledd;

    invoke-virtual {v7}, Ledd;->ordinal()I

    move-result v7

    aget v2, v2, v7

    packed-switch v2, :pswitch_data_0

    .line 2054
    const v2, 0x7f0a0919

    .line 2057
    :goto_1
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 2059
    const v0, 0x7f10027d

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2060
    const v2, 0x7f0a091c

    new-array v7, v3, [Ljava/lang/Object;

    const-string v8, "plus_mobile_events"

    .line 2062
    const-string v9, "https://support.google.com/plus/?hl=%locale%"

    invoke-static {v4, v8, v9}, Litk;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    aput-object v8, v7, v1

    .line 2060
    invoke-virtual {v5, v2, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Llhv;->a(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2063
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2065
    const v0, 0x7f0a091f

    .line 2066
    const-string v2, "event"

    .line 2067
    invoke-virtual {v4, v2, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 2068
    const-string v5, "hasUsedInstantShare"

    invoke-interface {v2, v5}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    move v1, v3

    :cond_1
    iput-boolean v1, p0, Ledh;->S:Z

    .line 2069
    iget-boolean v1, p0, Ledh;->S:Z

    if-eqz v1, :cond_2

    .line 2070
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "hasUsedInstantShare"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2071
    const v0, 0x7f0a091e

    .line 2074
    :cond_2
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 2075
    const v2, 0x7f0a0918

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 2076
    invoke-virtual {v2, v6}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 2077
    invoke-virtual {v2, v0, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f0a0597

    .line 2078
    invoke-virtual {v0, v2, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2080
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    .line 2041
    :cond_3
    const/16 v0, 0x8

    goto/16 :goto_0

    .line 2047
    :pswitch_0
    const v2, 0x7f0a091a

    .line 2048
    goto :goto_1

    .line 2050
    :pswitch_1
    const v2, 0x7f0a091b

    .line 2051
    goto :goto_1

    .line 2045
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2021
    invoke-super {p0, p1}, Llgr;->e(Landroid/os/Bundle;)V

    .line 2022
    const-string v0, "has_checked_in_id"

    iget-boolean v1, p0, Ledh;->R:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2023
    const-string v0, "first_time_id"

    iget-boolean v1, p0, Ledh;->S:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2024
    const-string v0, "dialog_type"

    iget-object v1, p0, Ledh;->T:Ledd;

    invoke-virtual {v1}, Ledd;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2025
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 2085
    packed-switch p2, :pswitch_data_0

    .line 2098
    :cond_0
    :goto_0
    return-void

    .line 2087
    :pswitch_0
    invoke-virtual {p0}, Ledh;->u_()Lu;

    move-result-object v0

    .line 2088
    instance-of v1, v0, Leco;

    if-eqz v1, :cond_0

    .line 2089
    check-cast v0, Leco;

    iget-object v1, p0, Ledh;->Q:Landroid/widget/CheckBox;

    .line 2090
    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    iget-boolean v2, p0, Ledh;->S:Z

    invoke-static {v0, v1, v2}, Leco;->a(Leco;ZZ)V

    goto :goto_0

    .line 2095
    :pswitch_1
    invoke-virtual {p0}, Ledh;->a()V

    goto :goto_0

    .line 2085
    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
