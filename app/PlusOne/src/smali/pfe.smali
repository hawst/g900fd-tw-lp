.class public final Lpfe;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:[Lpfi;

.field public c:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 9
    const v0, 0x46b86e3

    new-instance v1, Lpff;

    invoke-direct {v1}, Lpff;-><init>()V

    .line 14
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    .line 13
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 19
    sget-object v0, Lpfi;->a:[Lpfi;

    iput-object v0, p0, Lpfe;->b:[Lpfi;

    .line 22
    const/high16 v0, -0x80000000

    iput v0, p0, Lpfe;->c:I

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 46
    .line 47
    iget-object v0, p0, Lpfe;->a:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 48
    const/4 v0, 0x1

    iget-object v2, p0, Lpfe;->a:Ljava/lang/String;

    .line 49
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 51
    :goto_0
    iget-object v2, p0, Lpfe;->b:[Lpfi;

    if-eqz v2, :cond_1

    .line 52
    iget-object v2, p0, Lpfe;->b:[Lpfi;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 53
    if-eqz v4, :cond_0

    .line 54
    const/4 v5, 0x2

    .line 55
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 52
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 59
    :cond_1
    iget v1, p0, Lpfe;->c:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_2

    .line 60
    const/4 v1, 0x3

    iget v2, p0, Lpfe;->c:I

    .line 61
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 63
    :cond_2
    iget-object v1, p0, Lpfe;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 64
    iput v0, p0, Lpfe;->ai:I

    .line 65
    return v0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lpfe;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 73
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 74
    sparse-switch v0, :sswitch_data_0

    .line 78
    iget-object v2, p0, Lpfe;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 79
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lpfe;->ah:Ljava/util/List;

    .line 82
    :cond_1
    iget-object v2, p0, Lpfe;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 84
    :sswitch_0
    return-object p0

    .line 89
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpfe;->a:Ljava/lang/String;

    goto :goto_0

    .line 93
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 94
    iget-object v0, p0, Lpfe;->b:[Lpfi;

    if-nez v0, :cond_3

    move v0, v1

    .line 95
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lpfi;

    .line 96
    iget-object v3, p0, Lpfe;->b:[Lpfi;

    if-eqz v3, :cond_2

    .line 97
    iget-object v3, p0, Lpfe;->b:[Lpfi;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 99
    :cond_2
    iput-object v2, p0, Lpfe;->b:[Lpfi;

    .line 100
    :goto_2
    iget-object v2, p0, Lpfe;->b:[Lpfi;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 101
    iget-object v2, p0, Lpfe;->b:[Lpfi;

    new-instance v3, Lpfi;

    invoke-direct {v3}, Lpfi;-><init>()V

    aput-object v3, v2, v0

    .line 102
    iget-object v2, p0, Lpfe;->b:[Lpfi;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 103
    invoke-virtual {p1}, Loxn;->a()I

    .line 100
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 94
    :cond_3
    iget-object v0, p0, Lpfe;->b:[Lpfi;

    array-length v0, v0

    goto :goto_1

    .line 106
    :cond_4
    iget-object v2, p0, Lpfe;->b:[Lpfi;

    new-instance v3, Lpfi;

    invoke-direct {v3}, Lpfi;-><init>()V

    aput-object v3, v2, v0

    .line 107
    iget-object v2, p0, Lpfe;->b:[Lpfi;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 111
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 112
    if-eqz v0, :cond_5

    const/4 v2, 0x1

    if-eq v0, v2, :cond_5

    const/4 v2, 0x2

    if-eq v0, v2, :cond_5

    const/4 v2, 0x3

    if-eq v0, v2, :cond_5

    const/4 v2, 0x4

    if-eq v0, v2, :cond_5

    const/4 v2, 0x5

    if-ne v0, v2, :cond_6

    .line 118
    :cond_5
    iput v0, p0, Lpfe;->c:I

    goto/16 :goto_0

    .line 120
    :cond_6
    iput v1, p0, Lpfe;->c:I

    goto/16 :goto_0

    .line 74
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 27
    iget-object v0, p0, Lpfe;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 28
    const/4 v0, 0x1

    iget-object v1, p0, Lpfe;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 30
    :cond_0
    iget-object v0, p0, Lpfe;->b:[Lpfi;

    if-eqz v0, :cond_2

    .line 31
    iget-object v1, p0, Lpfe;->b:[Lpfi;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 32
    if-eqz v3, :cond_1

    .line 33
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 31
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 37
    :cond_2
    iget v0, p0, Lpfe;->c:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_3

    .line 38
    const/4 v0, 0x3

    iget v1, p0, Lpfe;->c:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 40
    :cond_3
    iget-object v0, p0, Lpfe;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 42
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lpfe;->a(Loxn;)Lpfe;

    move-result-object v0

    return-object v0
.end method
