.class public final Lmer;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lokd;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 11662
    invoke-direct {p0}, Loxq;-><init>()V

    .line 11665
    iput-object v0, p0, Lmer;->apiHeader:Llyr;

    .line 11668
    iput-object v0, p0, Lmer;->a:Lokd;

    .line 11662
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 11685
    const/4 v0, 0x0

    .line 11686
    iget-object v1, p0, Lmer;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 11687
    const/4 v0, 0x1

    iget-object v1, p0, Lmer;->apiHeader:Llyr;

    .line 11688
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 11690
    :cond_0
    iget-object v1, p0, Lmer;->a:Lokd;

    if-eqz v1, :cond_1

    .line 11691
    const/4 v1, 0x2

    iget-object v2, p0, Lmer;->a:Lokd;

    .line 11692
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11694
    :cond_1
    iget-object v1, p0, Lmer;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11695
    iput v0, p0, Lmer;->ai:I

    .line 11696
    return v0
.end method

.method public a(Loxn;)Lmer;
    .locals 2

    .prologue
    .line 11704
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 11705
    sparse-switch v0, :sswitch_data_0

    .line 11709
    iget-object v1, p0, Lmer;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 11710
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmer;->ah:Ljava/util/List;

    .line 11713
    :cond_1
    iget-object v1, p0, Lmer;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 11715
    :sswitch_0
    return-object p0

    .line 11720
    :sswitch_1
    iget-object v0, p0, Lmer;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 11721
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmer;->apiHeader:Llyr;

    .line 11723
    :cond_2
    iget-object v0, p0, Lmer;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 11727
    :sswitch_2
    iget-object v0, p0, Lmer;->a:Lokd;

    if-nez v0, :cond_3

    .line 11728
    new-instance v0, Lokd;

    invoke-direct {v0}, Lokd;-><init>()V

    iput-object v0, p0, Lmer;->a:Lokd;

    .line 11730
    :cond_3
    iget-object v0, p0, Lmer;->a:Lokd;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 11705
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 11673
    iget-object v0, p0, Lmer;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 11674
    const/4 v0, 0x1

    iget-object v1, p0, Lmer;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 11676
    :cond_0
    iget-object v0, p0, Lmer;->a:Lokd;

    if-eqz v0, :cond_1

    .line 11677
    const/4 v0, 0x2

    iget-object v1, p0, Lmer;->a:Lokd;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 11679
    :cond_1
    iget-object v0, p0, Lmer;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 11681
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 11658
    invoke-virtual {p0, p1}, Lmer;->a(Loxn;)Lmer;

    move-result-object v0

    return-object v0
.end method
