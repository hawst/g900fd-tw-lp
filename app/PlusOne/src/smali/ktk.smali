.class public Lktk;
.super Lhny;
.source "PG"


# static fields
.field private static volatile h:Lktk;


# instance fields
.field private final a:I

.field private final b:Lkfo;

.field private final c:Lktq;

.field private final d:[I

.field private final e:Ljava/util/concurrent/locks/Lock;

.field private f:Lhoz;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lktk;-><init>(Landroid/content/Context;IZ)V

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;IZ)V
    .locals 2

    .prologue
    .line 60
    const-string v0, "GetSquaresTask"

    invoke-direct {p0, p1, v0}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 34
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lktk;->e:Ljava/util/concurrent/locks/Lock;

    .line 62
    iput p2, p0, Lktk;->a:I

    .line 63
    new-instance v0, Lkfo;

    iget v1, p0, Lktk;->a:I

    invoke-direct {v0, p1, v1}, Lkfo;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lktk;->b:Lkfo;

    .line 64
    const-class v0, Lktq;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lktq;

    iput-object v0, p0, Lktk;->c:Lktq;

    .line 65
    if-eqz p3, :cond_0

    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    :goto_0
    iput-object v0, p0, Lktk;->d:[I

    .line 68
    return-void

    .line 65
    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    goto :goto_0

    nop

    :array_0
    .array-data 4
        0x1
        0x2
        0x3
    .end array-data

    :array_1
    .array-data 4
        0x1
        0x2
    .end array-data
.end method

.method public static a(Landroid/content/Context;I)Lktk;
    .locals 3

    .prologue
    .line 43
    sget-object v0, Lktk;->h:Lktk;

    .line 44
    if-eqz v0, :cond_0

    iget v1, v0, Lktk;->a:I

    if-ne v1, p1, :cond_0

    iget-object v1, v0, Lktk;->f:Lhoz;

    if-eqz v1, :cond_3

    .line 45
    :cond_0
    const-class v1, Lktk;

    monitor-enter v1

    .line 46
    :try_start_0
    sget-object v0, Lktk;->h:Lktk;

    .line 47
    if-eqz v0, :cond_1

    iget v2, v0, Lktk;->a:I

    if-ne v2, p1, :cond_1

    iget-object v2, v0, Lktk;->f:Lhoz;

    if-eqz v2, :cond_2

    .line 48
    :cond_1
    new-instance v0, Lktk;

    invoke-direct {v0, p0, p1}, Lktk;-><init>(Landroid/content/Context;I)V

    sput-object v0, Lktk;->h:Lktk;

    .line 50
    :cond_2
    monitor-exit v1

    .line 52
    :cond_3
    return-object v0

    .line 50
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 6

    .prologue
    .line 72
    iget-object v0, p0, Lktk;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->tryLock()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 75
    :try_start_0
    new-instance v2, Lktj;

    invoke-virtual {p0}, Lktk;->f()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lktk;->b:Lkfo;

    iget-object v3, p0, Lktk;->d:[I

    invoke-direct {v2, v0, v1, v3}, Lktj;-><init>(Landroid/content/Context;Lkfo;[I)V

    .line 77
    invoke-virtual {v2}, Lktj;->l()V

    .line 79
    invoke-virtual {v2}, Lktj;->t()Z

    move-result v0

    if-nez v0, :cond_0

    .line 80
    invoke-virtual {v2}, Lktj;->D()Loxu;

    move-result-object v0

    check-cast v0, Lmcz;

    iget-object v3, v0, Lmcz;->a:Lnto;

    .line 81
    iget-object v0, v3, Lnto;->c:[Lnru;

    if-eqz v0, :cond_1

    iget-object v0, v3, Lnto;->c:[Lnru;

    move-object v1, v0

    .line 84
    :goto_0
    iget-object v0, v3, Lnto;->a:[Lnrx;

    if-eqz v0, :cond_2

    iget-object v0, v3, Lnto;->a:[Lnrx;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 88
    :goto_1
    :try_start_1
    iget-object v4, p0, Lktk;->c:Lktq;

    iget v5, p0, Lktk;->a:I

    iget-object v3, v3, Lnto;->b:[Lnsq;

    invoke-interface {v4, v5, v1, v0, v3}, Lktq;->a(I[Lnru;[Lnrx;[Lnsq;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 96
    :cond_0
    :try_start_2
    new-instance v0, Lhoz;

    iget v1, v2, Lkff;->i:I

    iget-object v2, v2, Lkff;->k:Ljava/lang/Exception;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    iput-object v0, p0, Lktk;->f:Lhoz;

    .line 97
    iget-object v0, p0, Lktk;->f:Lhoz;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 99
    iget-object v1, p0, Lktk;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 107
    :goto_2
    return-object v0

    .line 81
    :cond_1
    :try_start_3
    sget-object v0, Lnru;->a:[Lnru;

    move-object v1, v0

    goto :goto_0

    .line 84
    :cond_2
    sget-object v0, Lnrx;->a:[Lnrx;

    goto :goto_1

    .line 90
    :catch_0
    move-exception v0

    .line 91
    new-instance v1, Lhoz;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v1, v2, v0, v3}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    iput-object v1, p0, Lktk;->f:Lhoz;

    .line 92
    iget-object v0, p0, Lktk;->f:Lhoz;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 99
    iget-object v1, p0, Lktk;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_2

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lktk;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    .line 103
    :cond_3
    iget-object v0, p0, Lktk;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 105
    iget-object v0, p0, Lktk;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 107
    iget-object v0, p0, Lktk;->f:Lhoz;

    goto :goto_2
.end method
