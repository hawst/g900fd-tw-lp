.class public final Loxj;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Loxj;


# instance fields
.field private b:Ljava/lang/Integer;

.field private c:Ljava/lang/Float;

.field private d:Ljava/lang/Integer;

.field private e:Ljava/lang/Float;

.field private f:Ljava/lang/Float;

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 202
    const/4 v0, 0x0

    new-array v0, v0, [Loxj;

    sput-object v0, Loxj;->a:[Loxj;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 203
    invoke-direct {p0}, Loxq;-><init>()V

    .line 252
    const/high16 v0, -0x80000000

    iput v0, p0, Loxj;->g:I

    .line 203
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 275
    const/4 v0, 0x1

    iget-object v1, p0, Loxj;->b:Ljava/lang/Integer;

    .line 277
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 278
    const/4 v1, 0x2

    iget-object v2, p0, Loxj;->d:Ljava/lang/Integer;

    .line 279
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 280
    const/4 v1, 0x3

    iget v2, p0, Loxj;->g:I

    .line 281
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 282
    iget-object v1, p0, Loxj;->f:Ljava/lang/Float;

    if-eqz v1, :cond_0

    .line 283
    const/4 v1, 0x4

    iget-object v2, p0, Loxj;->f:Ljava/lang/Float;

    .line 284
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 286
    :cond_0
    iget-object v1, p0, Loxj;->c:Ljava/lang/Float;

    if-eqz v1, :cond_1

    .line 287
    const/4 v1, 0x5

    iget-object v2, p0, Loxj;->c:Ljava/lang/Float;

    .line 288
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 290
    :cond_1
    iget-object v1, p0, Loxj;->e:Ljava/lang/Float;

    if-eqz v1, :cond_2

    .line 291
    const/4 v1, 0x6

    iget-object v2, p0, Loxj;->e:Ljava/lang/Float;

    .line 292
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 294
    :cond_2
    iget-object v1, p0, Loxj;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 295
    iput v0, p0, Loxj;->ai:I

    .line 296
    return v0
.end method

.method public a(Loxn;)Loxj;
    .locals 2

    .prologue
    .line 304
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 305
    sparse-switch v0, :sswitch_data_0

    .line 309
    iget-object v1, p0, Loxj;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 310
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Loxj;->ah:Ljava/util/List;

    .line 313
    :cond_1
    iget-object v1, p0, Loxj;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 315
    :sswitch_0
    return-object p0

    .line 320
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Loxj;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 324
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Loxj;->d:Ljava/lang/Integer;

    goto :goto_0

    .line 328
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 329
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa

    if-eq v0, v1, :cond_2

    const/16 v1, 0xb

    if-eq v0, v1, :cond_2

    const/16 v1, 0xc

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd

    if-eq v0, v1, :cond_2

    const/16 v1, 0x2d

    if-eq v0, v1, :cond_2

    const/16 v1, 0x2b

    if-eq v0, v1, :cond_2

    const/16 v1, 0x2c

    if-eq v0, v1, :cond_2

    const/16 v1, 0xc8

    if-eq v0, v1, :cond_2

    const/16 v1, 0xdc

    if-eq v0, v1, :cond_2

    const/16 v1, 0xdd

    if-eq v0, v1, :cond_2

    const/16 v1, 0xde

    if-eq v0, v1, :cond_2

    const/16 v1, 0xdf

    if-eq v0, v1, :cond_2

    const/16 v1, 0xe0

    if-eq v0, v1, :cond_2

    const/16 v1, 0xe1

    if-eq v0, v1, :cond_2

    const/16 v1, 0xe2

    if-eq v0, v1, :cond_2

    const/16 v1, 0xe3

    if-eq v0, v1, :cond_2

    const/16 v1, 0x12c

    if-eq v0, v1, :cond_2

    const/16 v1, 0x12e

    if-eq v0, v1, :cond_2

    const/16 v1, 0xf0

    if-eq v0, v1, :cond_2

    const/16 v1, 0xf1

    if-eq v0, v1, :cond_2

    const/16 v1, 0x138

    if-eq v0, v1, :cond_2

    const/16 v1, 0x13a

    if-eq v0, v1, :cond_2

    const/16 v1, 0x13b

    if-eq v0, v1, :cond_2

    const/16 v1, 0x13c

    if-eq v0, v1, :cond_2

    const/16 v1, 0x3a98

    if-ne v0, v1, :cond_3

    .line 362
    :cond_2
    iput v0, p0, Loxj;->g:I

    goto/16 :goto_0

    .line 364
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Loxj;->g:I

    goto/16 :goto_0

    .line 369
    :sswitch_4
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Loxj;->f:Ljava/lang/Float;

    goto/16 :goto_0

    .line 373
    :sswitch_5
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Loxj;->c:Ljava/lang/Float;

    goto/16 :goto_0

    .line 377
    :sswitch_6
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Loxj;->e:Ljava/lang/Float;

    goto/16 :goto_0

    .line 305
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x25 -> :sswitch_4
        0x2d -> :sswitch_5
        0x35 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 257
    const/4 v0, 0x1

    iget-object v1, p0, Loxj;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 258
    const/4 v0, 0x2

    iget-object v1, p0, Loxj;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 259
    const/4 v0, 0x3

    iget v1, p0, Loxj;->g:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 260
    iget-object v0, p0, Loxj;->f:Ljava/lang/Float;

    if-eqz v0, :cond_0

    .line 261
    const/4 v0, 0x4

    iget-object v1, p0, Loxj;->f:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 263
    :cond_0
    iget-object v0, p0, Loxj;->c:Ljava/lang/Float;

    if-eqz v0, :cond_1

    .line 264
    const/4 v0, 0x5

    iget-object v1, p0, Loxj;->c:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 266
    :cond_1
    iget-object v0, p0, Loxj;->e:Ljava/lang/Float;

    if-eqz v0, :cond_2

    .line 267
    const/4 v0, 0x6

    iget-object v1, p0, Loxj;->e:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 269
    :cond_2
    iget-object v0, p0, Loxj;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 271
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 199
    invoke-virtual {p0, p1}, Loxj;->a(Loxn;)Loxj;

    move-result-object v0

    return-object v0
.end method
