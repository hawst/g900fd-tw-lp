.class public final Lohl;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:I

.field public c:Ljava/lang/Integer;

.field public d:Ljava/lang/Integer;

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 6487
    invoke-direct {p0}, Loxq;-><init>()V

    .line 6497
    const/high16 v0, -0x80000000

    iput v0, p0, Lohl;->b:I

    .line 6487
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 6529
    const/4 v0, 0x0

    .line 6530
    iget-object v1, p0, Lohl;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 6531
    const/4 v0, 0x1

    iget-object v1, p0, Lohl;->a:Ljava/lang/String;

    .line 6532
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 6534
    :cond_0
    iget v1, p0, Lohl;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_1

    .line 6535
    const/4 v1, 0x2

    iget v2, p0, Lohl;->b:I

    .line 6536
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6538
    :cond_1
    iget-object v1, p0, Lohl;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 6539
    const/4 v1, 0x3

    iget-object v2, p0, Lohl;->c:Ljava/lang/Integer;

    .line 6540
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6542
    :cond_2
    iget-object v1, p0, Lohl;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 6543
    const/4 v1, 0x4

    iget-object v2, p0, Lohl;->d:Ljava/lang/Integer;

    .line 6544
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6546
    :cond_3
    iget-object v1, p0, Lohl;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 6547
    const/4 v1, 0x5

    iget-object v2, p0, Lohl;->e:Ljava/lang/String;

    .line 6548
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6550
    :cond_4
    iget-object v1, p0, Lohl;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6551
    iput v0, p0, Lohl;->ai:I

    .line 6552
    return v0
.end method

.method public a(Loxn;)Lohl;
    .locals 2

    .prologue
    .line 6560
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 6561
    sparse-switch v0, :sswitch_data_0

    .line 6565
    iget-object v1, p0, Lohl;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 6566
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lohl;->ah:Ljava/util/List;

    .line 6569
    :cond_1
    iget-object v1, p0, Lohl;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6571
    :sswitch_0
    return-object p0

    .line 6576
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lohl;->a:Ljava/lang/String;

    goto :goto_0

    .line 6580
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 6581
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 6583
    :cond_2
    iput v0, p0, Lohl;->b:I

    goto :goto_0

    .line 6585
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lohl;->b:I

    goto :goto_0

    .line 6590
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lohl;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 6594
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lohl;->d:Ljava/lang/Integer;

    goto :goto_0

    .line 6598
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lohl;->e:Ljava/lang/String;

    goto :goto_0

    .line 6561
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 6508
    iget-object v0, p0, Lohl;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 6509
    const/4 v0, 0x1

    iget-object v1, p0, Lohl;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 6511
    :cond_0
    iget v0, p0, Lohl;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 6512
    const/4 v0, 0x2

    iget v1, p0, Lohl;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 6514
    :cond_1
    iget-object v0, p0, Lohl;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 6515
    const/4 v0, 0x3

    iget-object v1, p0, Lohl;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 6517
    :cond_2
    iget-object v0, p0, Lohl;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 6518
    const/4 v0, 0x4

    iget-object v1, p0, Lohl;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 6520
    :cond_3
    iget-object v0, p0, Lohl;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 6521
    const/4 v0, 0x5

    iget-object v1, p0, Lohl;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 6523
    :cond_4
    iget-object v0, p0, Lohl;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 6525
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6483
    invoke-virtual {p0, p1}, Lohl;->a(Loxn;)Lohl;

    move-result-object v0

    return-object v0
.end method
