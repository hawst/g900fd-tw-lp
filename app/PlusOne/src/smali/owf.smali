.class public final Lowf;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lowf;


# instance fields
.field private b:I

.field private c:I

.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 254
    const/4 v0, 0x0

    new-array v0, v0, [Lowf;

    sput-object v0, Lowf;->a:[Lowf;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/high16 v0, -0x80000000

    .line 255
    invoke-direct {p0}, Loxq;-><init>()V

    .line 276
    iput v0, p0, Lowf;->b:I

    .line 279
    iput v0, p0, Lowf;->c:I

    .line 282
    iput v0, p0, Lowf;->d:I

    .line 255
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    const/high16 v3, -0x80000000

    .line 302
    const/4 v0, 0x0

    .line 303
    iget v1, p0, Lowf;->b:I

    if-eq v1, v3, :cond_0

    .line 304
    const/4 v0, 0x1

    iget v1, p0, Lowf;->b:I

    .line 305
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 307
    :cond_0
    iget v1, p0, Lowf;->c:I

    if-eq v1, v3, :cond_1

    .line 308
    const/4 v1, 0x2

    iget v2, p0, Lowf;->c:I

    .line 309
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 311
    :cond_1
    iget v1, p0, Lowf;->d:I

    if-eq v1, v3, :cond_2

    .line 312
    const/4 v1, 0x3

    iget v2, p0, Lowf;->d:I

    .line 313
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 315
    :cond_2
    iget-object v1, p0, Lowf;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 316
    iput v0, p0, Lowf;->ai:I

    .line 317
    return v0
.end method

.method public a(Loxn;)Lowf;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 325
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 326
    sparse-switch v0, :sswitch_data_0

    .line 330
    iget-object v1, p0, Lowf;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 331
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lowf;->ah:Ljava/util/List;

    .line 334
    :cond_1
    iget-object v1, p0, Lowf;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 336
    :sswitch_0
    return-object p0

    .line 341
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 342
    if-eqz v0, :cond_2

    if-ne v0, v3, :cond_3

    .line 344
    :cond_2
    iput v0, p0, Lowf;->b:I

    goto :goto_0

    .line 346
    :cond_3
    iput v2, p0, Lowf;->b:I

    goto :goto_0

    .line 351
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 352
    if-eqz v0, :cond_4

    if-eq v0, v3, :cond_4

    if-ne v0, v4, :cond_5

    .line 355
    :cond_4
    iput v0, p0, Lowf;->c:I

    goto :goto_0

    .line 357
    :cond_5
    iput v2, p0, Lowf;->c:I

    goto :goto_0

    .line 362
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 363
    if-eqz v0, :cond_6

    if-eq v0, v3, :cond_6

    if-eq v0, v4, :cond_6

    const/4 v1, 0x3

    if-ne v0, v1, :cond_7

    .line 367
    :cond_6
    iput v0, p0, Lowf;->d:I

    goto :goto_0

    .line 369
    :cond_7
    iput v2, p0, Lowf;->d:I

    goto :goto_0

    .line 326
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 287
    iget v0, p0, Lowf;->b:I

    if-eq v0, v2, :cond_0

    .line 288
    const/4 v0, 0x1

    iget v1, p0, Lowf;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 290
    :cond_0
    iget v0, p0, Lowf;->c:I

    if-eq v0, v2, :cond_1

    .line 291
    const/4 v0, 0x2

    iget v1, p0, Lowf;->c:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 293
    :cond_1
    iget v0, p0, Lowf;->d:I

    if-eq v0, v2, :cond_2

    .line 294
    const/4 v0, 0x3

    iget v1, p0, Lowf;->d:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 296
    :cond_2
    iget-object v0, p0, Lowf;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 298
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 251
    invoke-virtual {p0, p1}, Lowf;->a(Loxn;)Lowf;

    move-result-object v0

    return-object v0
.end method
