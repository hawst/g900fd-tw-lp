.class public final Lisq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xc
.end annotation


# static fields
.field private static d:Lisr;


# instance fields
.field private final a:Landroid/mtp/MtpDevice;

.field private b:Liso;

.field private final c:J

.field private e:Lisu;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 73
    new-instance v0, Lisr;

    invoke-direct {v0}, Lisr;-><init>()V

    sput-object v0, Lisq;->d:Lisr;

    return-void
.end method

.method constructor <init>(Liso;)V
    .locals 2

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 140
    new-instance v0, Lisu;

    invoke-direct {v0}, Lisu;-><init>()V

    iput-object v0, p0, Lisq;->e:Lisu;

    .line 86
    iput-object p1, p0, Lisq;->b:Liso;

    .line 87
    invoke-virtual {p1}, Liso;->b()Landroid/mtp/MtpDevice;

    move-result-object v0

    iput-object v0, p0, Lisq;->a:Landroid/mtp/MtpDevice;

    .line 88
    invoke-virtual {p1}, Liso;->k()J

    move-result-wide v0

    iput-wide v0, p0, Lisq;->c:J

    .line 89
    return-void
.end method

.method public static a()Lisr;
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lisq;->d:Lisr;

    return-object v0
.end method


# virtual methods
.method protected a(Ljava/util/SortedMap;)I
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/SortedMap",
            "<",
            "Lisu;",
            "Ljava/util/List",
            "<",
            "Lisj;",
            ">;>;)I"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 157
    .line 158
    iget-object v0, p0, Lisq;->a:Landroid/mtp/MtpDevice;

    invoke-virtual {v0}, Landroid/mtp/MtpDevice;->getStorageIds()[I

    move-result-object v4

    array-length v5, v4

    move v3, v2

    move v0, v2

    :goto_0
    if-ge v3, v5, :cond_7

    aget v6, v4, v3

    .line 159
    iget-object v1, p0, Lisq;->b:Liso;

    iget-object v7, p0, Lisq;->a:Landroid/mtp/MtpDevice;

    iget-wide v8, p0, Lisq;->c:J

    invoke-virtual {v1, v7, v8, v9}, Liso;->a(Landroid/mtp/MtpDevice;J)Z

    move-result v1

    if-nez v1, :cond_0

    .line 160
    new-instance v0, Liss;

    invoke-direct {v0}, Liss;-><init>()V

    throw v0

    .line 162
    :cond_0
    new-instance v7, Ljava/util/Stack;

    invoke-direct {v7}, Ljava/util/Stack;-><init>()V

    .line 163
    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v7, v1}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    move v1, v0

    .line 164
    :goto_1
    invoke-virtual {v7}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 165
    iget-object v0, p0, Lisq;->b:Liso;

    iget-object v8, p0, Lisq;->a:Landroid/mtp/MtpDevice;

    iget-wide v10, p0, Lisq;->c:J

    invoke-virtual {v0, v8, v10, v11}, Liso;->a(Landroid/mtp/MtpDevice;J)Z

    move-result v0

    if-nez v0, :cond_1

    .line 166
    new-instance v0, Liss;

    invoke-direct {v0}, Liss;-><init>()V

    throw v0

    .line 168
    :cond_1
    invoke-virtual {v7}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 169
    iget-object v8, p0, Lisq;->a:Landroid/mtp/MtpDevice;

    invoke-virtual {v8, v6, v2, v0}, Landroid/mtp/MtpDevice;->getObjectHandles(III)[I

    move-result-object v8

    array-length v9, v8

    move v0, v1

    move v1, v2

    :goto_2
    if-ge v1, v9, :cond_5

    aget v10, v8, v1

    .line 170
    iget-object v11, p0, Lisq;->a:Landroid/mtp/MtpDevice;

    invoke-virtual {v11, v10}, Landroid/mtp/MtpDevice;->getObjectInfo(I)Landroid/mtp/MtpObjectInfo;

    move-result-object v11

    .line 171
    if-nez v11, :cond_2

    .line 172
    new-instance v0, Liss;

    invoke-direct {v0}, Liss;-><init>()V

    throw v0

    .line 174
    :cond_2
    invoke-virtual {v11}, Landroid/mtp/MtpObjectInfo;->getFormat()I

    move-result v12

    .line 175
    const/16 v13, 0x3001

    if-ne v12, v13, :cond_4

    .line 176
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    .line 169
    :cond_3
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 177
    :cond_4
    iget-object v10, p0, Lisq;->b:Liso;

    invoke-virtual {v10, v12}, Liso;->a(I)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 178
    add-int/lit8 v0, v0, 0x1

    .line 179
    new-instance v10, Lisj;

    invoke-direct {v10, v11}, Lisj;-><init>(Landroid/mtp/MtpObjectInfo;)V

    invoke-virtual {p0, v10, p1, v0}, Lisq;->a(Lisj;Ljava/util/SortedMap;I)V

    goto :goto_3

    :cond_5
    move v1, v0

    .line 182
    goto :goto_1

    .line 158
    :cond_6
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v0, v1

    goto/16 :goto_0

    .line 184
    :cond_7
    return v0
.end method

.method protected a(Lisj;Ljava/util/SortedMap;I)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lisj;",
            "Ljava/util/SortedMap",
            "<",
            "Lisu;",
            "Ljava/util/List",
            "<",
            "Lisj;",
            ">;>;I)V"
        }
    .end annotation

    .prologue
    .line 144
    iget-object v0, p0, Lisq;->e:Lisu;

    invoke-virtual {p1}, Lisj;->c()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lisu;->a(J)V

    .line 145
    iget-object v0, p0, Lisq;->e:Lisu;

    invoke-interface {p2, v0}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 146
    if-nez v0, :cond_0

    .line 147
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 148
    iget-object v1, p0, Lisq;->e:Lisu;

    invoke-interface {p2, v1, v0}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    new-instance v1, Lisu;

    invoke-direct {v1}, Lisu;-><init>()V

    iput-object v1, p0, Lisq;->e:Lisu;

    .line 151
    :cond_0
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 152
    iget-object v0, p0, Lisq;->b:Liso;

    invoke-virtual {v0, p1, p3}, Liso;->a(Lisj;I)V

    .line 153
    return-void
.end method

.method public run()V
    .locals 17

    .prologue
    .line 94
    :try_start_0
    new-instance v3, Ljava/util/TreeMap;

    invoke-direct {v3}, Ljava/util/TreeMap;-><init>()V

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lisq;->a(Ljava/util/SortedMap;)I

    move-result v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lisq;->b:Liso;

    invoke-virtual {v4}, Liso;->j()V

    invoke-interface {v3}, Ljava/util/SortedMap;->size()I

    move-result v4

    new-array v13, v4, [Lisg;

    new-array v14, v2, [Lisj;

    add-int/2addr v2, v4

    new-array v15, v2, [I

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v2, 0x0

    invoke-interface {v3}, Ljava/util/SortedMap;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v16

    move v11, v2

    :goto_0
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Ljava/util/Map$Entry;

    move-object v3, v0

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v7

    add-int v5, v4, v7

    add-int/lit8 v12, v5, 0x1

    invoke-static {v15, v4, v12, v11}, Ljava/util/Arrays;->fill([IIII)V

    add-int/lit8 v5, v12, -0x1

    const/4 v8, 0x0

    move v9, v8

    move v10, v6

    :goto_1
    if-ge v9, v7, :cond_0

    invoke-interface {v2, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lisj;

    aput-object v8, v14, v10

    add-int/lit8 v10, v10, 0x1

    add-int/lit8 v8, v9, 0x1

    move v9, v8

    goto :goto_1

    :cond_0
    new-instance v2, Lisg;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lisu;

    invoke-direct/range {v2 .. v7}, Lisg;-><init>(Lisu;IIII)V

    aput-object v2, v13, v11

    add-int/lit8 v2, v11, 0x1

    move v11, v2

    move v6, v10

    move v4, v12

    goto :goto_0

    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lisq;->b:Liso;

    move-object/from16 v0, p0

    iget-object v3, v0, Lisq;->a:Landroid/mtp/MtpDevice;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lisq;->c:J

    new-instance v6, List;

    invoke-direct {v6, v15, v14, v13}, List;-><init>([I[Lisj;[Lisg;)V

    invoke-virtual {v2, v3, v4, v5, v6}, Liso;->a(Landroid/mtp/MtpDevice;JList;)Z

    move-result v2

    if-nez v2, :cond_2

    new-instance v2, Liss;

    invoke-direct {v2}, Liss;-><init>()V

    throw v2
    :try_end_0
    .catch Liss; {:try_start_0 .. :try_end_0} :catch_0

    .line 96
    :catch_0
    move-exception v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lisq;->b:Liso;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Liso;->a(Z)V

    .line 98
    :cond_2
    return-void
.end method
