.class public final Ldvs;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static g:Ldvs;


# instance fields
.field private a:Ljava/lang/String;

.field private b:I

.field private c:[Ldvo;

.field private d:Lhgw;

.field private e:[I

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Ldvs;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 48
    sget-object v0, Ldvs;->g:Ldvs;

    if-nez v0, :cond_0

    .line 49
    new-instance v0, Ldvs;

    invoke-direct {v0}, Ldvs;-><init>()V

    sput-object v0, Ldvs;->g:Ldvs;

    .line 52
    :cond_0
    sget-object v0, Ldvs;->g:Ldvs;

    iput-object v2, v0, Ldvs;->a:Ljava/lang/String;

    .line 53
    sget-object v0, Ldvs;->g:Ldvs;

    const/4 v1, 0x0

    iput v1, v0, Ldvs;->b:I

    .line 54
    sget-object v0, Ldvs;->g:Ldvs;

    iput-object v2, v0, Ldvs;->c:[Ldvo;

    .line 55
    sget-object v0, Ldvs;->g:Ldvs;

    iput-object v2, v0, Ldvs;->d:Lhgw;

    .line 56
    sget-object v0, Ldvs;->g:Ldvs;

    iput-object v2, v0, Ldvs;->e:[I

    .line 57
    sget-object v0, Ldvs;->g:Ldvs;

    iput-object v2, v0, Ldvs;->f:Ljava/lang/String;

    .line 58
    sget-object v0, Ldvs;->g:Ldvs;

    return-object v0
.end method


# virtual methods
.method public a(I)Ldvs;
    .locals 0

    .prologue
    .line 67
    iput p1, p0, Ldvs;->b:I

    .line 68
    return-object p0
.end method

.method public a(Landroid/content/Context;ILnpk;)Ldvs;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 83
    if-nez p3, :cond_2

    move-object v1, v0

    .line 85
    :goto_0
    if-eqz v1, :cond_1

    .line 87
    invoke-static {p1, p2, v1}, Ldvq;->a(Landroid/content/Context;ILocf;)Lhgw;

    move-result-object v2

    iput-object v2, p0, Ldvs;->d:Lhgw;

    .line 89
    iget-object v2, v1, Locf;->d:Locd;

    if-eqz v2, :cond_0

    iget-object v2, v1, Locf;->d:Locd;

    iget-object v2, v2, Locd;->a:Ljava/lang/String;

    .line 90
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v0, v1, Locf;->d:Locd;

    iget-object v0, v0, Locd;->a:Ljava/lang/String;

    :cond_0
    iput-object v0, p0, Ldvs;->f:Ljava/lang/String;

    .line 93
    invoke-static {v1}, Ldvq;->a(Locf;)[I

    move-result-object v0

    iput-object v0, p0, Ldvs;->e:[I

    .line 95
    :cond_1
    return-object p0

    .line 83
    :cond_2
    iget-object v1, p3, Lnpk;->a:Locf;

    goto :goto_0
.end method

.method public a(Lhgw;)Ldvs;
    .locals 0

    .prologue
    .line 77
    iput-object p1, p0, Ldvs;->d:Lhgw;

    .line 78
    return-object p0
.end method

.method public a(Ljava/lang/String;)Ldvs;
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Ldvs;->a:Ljava/lang/String;

    .line 63
    return-object p0
.end method

.method public a([I)Ldvs;
    .locals 0

    .prologue
    .line 104
    iput-object p1, p0, Ldvs;->e:[I

    .line 105
    return-object p0
.end method

.method public a([Ldvo;)Ldvs;
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Ldvs;->c:[Ldvo;

    .line 73
    return-object p0
.end method

.method public b()Ldvq;
    .locals 7

    .prologue
    .line 109
    new-instance v0, Ldvq;

    iget-object v1, p0, Ldvs;->a:Ljava/lang/String;

    iget v2, p0, Ldvs;->b:I

    iget-object v3, p0, Ldvs;->c:[Ldvo;

    iget-object v4, p0, Ldvs;->d:Lhgw;

    iget-object v5, p0, Ldvs;->e:[I

    iget-object v6, p0, Ldvs;->f:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Ldvq;-><init>(Ljava/lang/String;I[Ldvo;Lhgw;[ILjava/lang/String;)V

    return-object v0
.end method

.method public b(Ljava/lang/String;)Ldvs;
    .locals 0

    .prologue
    .line 99
    iput-object p1, p0, Ldvs;->f:Ljava/lang/String;

    .line 100
    return-object p0
.end method
