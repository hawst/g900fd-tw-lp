.class public final Lnyj;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Integer;

.field public d:Ljava/lang/Boolean;

.field public e:Ljava/lang/Boolean;

.field private f:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5641
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 5682
    const/4 v0, 0x0

    .line 5683
    iget-object v1, p0, Lnyj;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 5684
    const/4 v0, 0x1

    iget-object v1, p0, Lnyj;->a:Ljava/lang/Integer;

    .line 5685
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 5687
    :cond_0
    iget-object v1, p0, Lnyj;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 5688
    const/4 v1, 0x2

    iget-object v2, p0, Lnyj;->b:Ljava/lang/Integer;

    .line 5689
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5691
    :cond_1
    iget-object v1, p0, Lnyj;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 5692
    const/4 v1, 0x3

    iget-object v2, p0, Lnyj;->c:Ljava/lang/Integer;

    .line 5693
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5695
    :cond_2
    iget-object v1, p0, Lnyj;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 5696
    const/4 v1, 0x4

    iget-object v2, p0, Lnyj;->d:Ljava/lang/Boolean;

    .line 5697
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 5699
    :cond_3
    iget-object v1, p0, Lnyj;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 5700
    const/4 v1, 0x5

    iget-object v2, p0, Lnyj;->e:Ljava/lang/Boolean;

    .line 5701
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 5703
    :cond_4
    iget-object v1, p0, Lnyj;->f:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    .line 5704
    const/4 v1, 0x6

    iget-object v2, p0, Lnyj;->f:Ljava/lang/Boolean;

    .line 5705
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 5707
    :cond_5
    iget-object v1, p0, Lnyj;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5708
    iput v0, p0, Lnyj;->ai:I

    .line 5709
    return v0
.end method

.method public a(Loxn;)Lnyj;
    .locals 2

    .prologue
    .line 5717
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 5718
    sparse-switch v0, :sswitch_data_0

    .line 5722
    iget-object v1, p0, Lnyj;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 5723
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnyj;->ah:Ljava/util/List;

    .line 5726
    :cond_1
    iget-object v1, p0, Lnyj;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5728
    :sswitch_0
    return-object p0

    .line 5733
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnyj;->a:Ljava/lang/Integer;

    goto :goto_0

    .line 5737
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnyj;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 5741
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnyj;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 5745
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnyj;->d:Ljava/lang/Boolean;

    goto :goto_0

    .line 5749
    :sswitch_5
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnyj;->e:Ljava/lang/Boolean;

    goto :goto_0

    .line 5753
    :sswitch_6
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnyj;->f:Ljava/lang/Boolean;

    goto :goto_0

    .line 5718
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 5658
    iget-object v0, p0, Lnyj;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 5659
    const/4 v0, 0x1

    iget-object v1, p0, Lnyj;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 5661
    :cond_0
    iget-object v0, p0, Lnyj;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 5662
    const/4 v0, 0x2

    iget-object v1, p0, Lnyj;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 5664
    :cond_1
    iget-object v0, p0, Lnyj;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 5665
    const/4 v0, 0x3

    iget-object v1, p0, Lnyj;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 5667
    :cond_2
    iget-object v0, p0, Lnyj;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 5668
    const/4 v0, 0x4

    iget-object v1, p0, Lnyj;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 5670
    :cond_3
    iget-object v0, p0, Lnyj;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 5671
    const/4 v0, 0x5

    iget-object v1, p0, Lnyj;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 5673
    :cond_4
    iget-object v0, p0, Lnyj;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 5674
    const/4 v0, 0x6

    iget-object v1, p0, Lnyj;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 5676
    :cond_5
    iget-object v0, p0, Lnyj;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 5678
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5637
    invoke-virtual {p0, p1}, Lnyj;->a(Loxn;)Lnyj;

    move-result-object v0

    return-object v0
.end method
