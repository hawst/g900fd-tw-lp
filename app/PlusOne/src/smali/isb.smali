.class public abstract Lisb;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xc
.end annotation


# instance fields
.field private a:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lisc;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lisb;->a:Ljava/util/Collection;

    .line 36
    return-void
.end method


# virtual methods
.method public abstract a(IZ)V
.end method

.method public a(Lisc;)V
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lisb;->a:Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 62
    return-void
.end method

.method public abstract a(I)Z
.end method

.method public b()V
    .locals 2

    .prologue
    .line 53
    iget-object v0, p0, Lisb;->a:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lisc;

    .line 54
    invoke-interface {v0}, Lisc;->a()V

    goto :goto_0

    .line 56
    :cond_0
    return-void
.end method

.method public b(IZ)V
    .locals 2

    .prologue
    .line 45
    invoke-virtual {p0, p1}, Lisb;->a(I)Z

    move-result v0

    if-eq v0, p2, :cond_0

    .line 46
    iget-object v0, p0, Lisb;->a:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lisc;

    .line 47
    invoke-interface {v0, p1, p2}, Lisc;->a(IZ)V

    goto :goto_0

    .line 50
    :cond_0
    return-void
.end method

.method public b(Lisc;)V
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lisb;->a:Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    .line 66
    return-void
.end method
