.class public final Lneb;
.super Loxq;
.source "PG"


# instance fields
.field public a:I

.field private b:Ljava/lang/String;

.field private c:[Lndx;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3603
    invoke-direct {p0}, Loxq;-><init>()V

    .line 3608
    const/high16 v0, -0x80000000

    iput v0, p0, Lneb;->a:I

    .line 3611
    sget-object v0, Lndx;->a:[Lndx;

    iput-object v0, p0, Lneb;->c:[Lndx;

    .line 3603
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 3635
    .line 3636
    iget-object v0, p0, Lneb;->b:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 3637
    const/4 v0, 0x1

    iget-object v2, p0, Lneb;->b:Ljava/lang/String;

    .line 3638
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3640
    :goto_0
    iget v2, p0, Lneb;->a:I

    const/high16 v3, -0x80000000

    if-eq v2, v3, :cond_0

    .line 3641
    const/4 v2, 0x2

    iget v3, p0, Lneb;->a:I

    .line 3642
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 3644
    :cond_0
    iget-object v2, p0, Lneb;->c:[Lndx;

    if-eqz v2, :cond_2

    .line 3645
    iget-object v2, p0, Lneb;->c:[Lndx;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 3646
    if-eqz v4, :cond_1

    .line 3647
    const/4 v5, 0x3

    .line 3648
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 3645
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 3652
    :cond_2
    iget-object v1, p0, Lneb;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3653
    iput v0, p0, Lneb;->ai:I

    .line 3654
    return v0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lneb;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3662
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3663
    sparse-switch v0, :sswitch_data_0

    .line 3667
    iget-object v2, p0, Lneb;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 3668
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lneb;->ah:Ljava/util/List;

    .line 3671
    :cond_1
    iget-object v2, p0, Lneb;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3673
    :sswitch_0
    return-object p0

    .line 3678
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lneb;->b:Ljava/lang/String;

    goto :goto_0

    .line 3682
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 3683
    if-eqz v0, :cond_2

    const/4 v2, 0x1

    if-eq v0, v2, :cond_2

    const/4 v2, 0x2

    if-eq v0, v2, :cond_2

    const/4 v2, 0x3

    if-ne v0, v2, :cond_3

    .line 3687
    :cond_2
    iput v0, p0, Lneb;->a:I

    goto :goto_0

    .line 3689
    :cond_3
    iput v1, p0, Lneb;->a:I

    goto :goto_0

    .line 3694
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 3695
    iget-object v0, p0, Lneb;->c:[Lndx;

    if-nez v0, :cond_5

    move v0, v1

    .line 3696
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lndx;

    .line 3697
    iget-object v3, p0, Lneb;->c:[Lndx;

    if-eqz v3, :cond_4

    .line 3698
    iget-object v3, p0, Lneb;->c:[Lndx;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3700
    :cond_4
    iput-object v2, p0, Lneb;->c:[Lndx;

    .line 3701
    :goto_2
    iget-object v2, p0, Lneb;->c:[Lndx;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 3702
    iget-object v2, p0, Lneb;->c:[Lndx;

    new-instance v3, Lndx;

    invoke-direct {v3}, Lndx;-><init>()V

    aput-object v3, v2, v0

    .line 3703
    iget-object v2, p0, Lneb;->c:[Lndx;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 3704
    invoke-virtual {p1}, Loxn;->a()I

    .line 3701
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 3695
    :cond_5
    iget-object v0, p0, Lneb;->c:[Lndx;

    array-length v0, v0

    goto :goto_1

    .line 3707
    :cond_6
    iget-object v2, p0, Lneb;->c:[Lndx;

    new-instance v3, Lndx;

    invoke-direct {v3}, Lndx;-><init>()V

    aput-object v3, v2, v0

    .line 3708
    iget-object v2, p0, Lneb;->c:[Lndx;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 3663
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 3616
    iget-object v0, p0, Lneb;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 3617
    const/4 v0, 0x1

    iget-object v1, p0, Lneb;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3619
    :cond_0
    iget v0, p0, Lneb;->a:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 3620
    const/4 v0, 0x2

    iget v1, p0, Lneb;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 3622
    :cond_1
    iget-object v0, p0, Lneb;->c:[Lndx;

    if-eqz v0, :cond_3

    .line 3623
    iget-object v1, p0, Lneb;->c:[Lndx;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 3624
    if-eqz v3, :cond_2

    .line 3625
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 3623
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3629
    :cond_3
    iget-object v0, p0, Lneb;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3631
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3599
    invoke-virtual {p0, p1}, Lneb;->a(Loxn;)Lneb;

    move-result-object v0

    return-object v0
.end method
