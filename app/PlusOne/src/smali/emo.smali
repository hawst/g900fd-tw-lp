.class public final Lemo;
.super Lhye;
.source "PG"


# instance fields
.field private final b:I

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private e:Z

.field private final f:Ldp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">.dp;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 151
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lhye;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    .line 147
    new-instance v0, Ldp;

    invoke-direct {v0, p0}, Ldp;-><init>(Ldo;)V

    iput-object v0, p0, Lemo;->f:Ldp;

    .line 153
    iput p2, p0, Lemo;->b:I

    .line 154
    iput-object p3, p0, Lemo;->c:Ljava/lang/String;

    .line 155
    iput-object p4, p0, Lemo;->d:Ljava/lang/String;

    .line 156
    return-void
.end method


# virtual methods
.method public C()Landroid/database/Cursor;
    .locals 18

    .prologue
    .line 183
    invoke-virtual/range {p0 .. p0}, Lemo;->n()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, p0

    iget v3, v0, Lemo;->b:I

    invoke-static {v2, v3}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v2

    .line 184
    invoke-virtual {v2}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 186
    const/4 v12, 0x0

    .line 187
    const-wide/16 v10, 0x0

    .line 188
    const/4 v3, 0x2

    new-array v6, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lemo;->c:Ljava/lang/String;

    aput-object v4, v6, v3

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget-object v4, v0, Lemo;->d:Ljava/lang/String;

    aput-object v4, v6, v3

    .line 189
    const-string v3, "all_tiles"

    sget-object v4, Lemr;->a:[Ljava/lang/String;

    const-string v5, "view_id = ? AND tile_id = ?"

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 194
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_b

    .line 195
    new-instance v3, Lnym;

    invoke-direct {v3}, Lnym;-><init>()V

    const/4 v4, 0x0

    .line 196
    invoke-interface {v6, v4}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v4

    .line 195
    invoke-static {v3, v4}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v3

    check-cast v3, Lnym;

    .line 197
    const/4 v4, 0x1

    invoke-interface {v6, v4}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v4

    .line 204
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move-wide v12, v4

    move-object v14, v3

    .line 207
    :goto_1
    if-nez v14, :cond_0

    .line 208
    const/4 v2, 0x6

    const-string v3, "PhotoDetailsLoader"

    move-object/from16 v0, p0

    iget-object v4, v0, Lemo;->d:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lemo;->c:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x31

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "Could not find a photo with tileId: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " and viewId: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Llse;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 210
    const/4 v2, 0x0

    .line 316
    :goto_2
    return-object v2

    .line 199
    :catch_0
    move-exception v3

    .line 201
    :try_start_1
    const-string v4, "PhotoDetailsLoader"

    const-string v5, "Unable to parse Photo from byte array."

    invoke-static {v4, v5, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 202
    const/4 v3, 0x0

    .line 204
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move-wide v12, v10

    move-object v14, v3

    .line 205
    goto :goto_1

    .line 204
    :catchall_0
    move-exception v2

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v2

    .line 213
    :cond_0
    const/4 v3, 0x1

    new-array v6, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, v14, Lnym;->h:Lnyz;

    iget-object v4, v4, Lnyz;->c:Ljava/lang/String;

    aput-object v4, v6, v3

    .line 214
    iget-object v3, v14, Lnym;->h:Lnyz;

    iget-object v11, v3, Lnyz;->e:Ljava/lang/String;

    .line 215
    iget-object v3, v14, Lnym;->h:Lnyz;

    iget-object v10, v3, Lnyz;->d:Ljava/lang/String;

    .line 216
    const-string v3, "contacts"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v7, "avatar"

    aput-object v7, v4, v5

    const/4 v5, 0x1

    const-string v7, "name"

    aput-object v7, v4, v5

    const-string v5, "gaia_id = ?"

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    .line 222
    :try_start_2
    invoke-interface {v5}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 223
    const/4 v3, 0x0

    invoke-interface {v5, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lhst;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 224
    const/4 v3, 0x1

    invoke-interface {v5, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v3

    .line 227
    :goto_3
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    .line 230
    iget-object v6, v14, Lnym;->d:Ljava/lang/String;

    .line 231
    iget-object v5, v14, Lnym;->n:Ljava/lang/Double;

    invoke-static {v5}, Llsl;->a(Ljava/lang/Double;)D

    move-result-wide v8

    const-wide v10, 0x408f400000000000L    # 1000.0

    mul-double/2addr v8, v10

    double-to-long v8, v8

    .line 232
    iget-object v15, v14, Lnym;->r:Loae;

    .line 233
    const-wide/32 v10, 0x10000000

    and-long/2addr v10, v12

    const-wide/16 v16, 0x0

    cmp-long v5, v10, v16

    if-eqz v5, :cond_1

    const/4 v5, 0x1

    move v11, v5

    .line 235
    :goto_4
    const-wide/32 v16, 0x4000000

    and-long v12, v12, v16

    const-wide/16 v16, 0x0

    cmp-long v5, v12, v16

    if-eqz v5, :cond_2

    const/4 v5, 0x1

    move v10, v5

    .line 241
    :goto_5
    sget-object v5, Lemp;->a:[Ljava/lang/String;

    const/4 v5, 0x7

    new-array v5, v5, [Ljava/lang/Object;

    .line 242
    new-instance v12, Landroid/database/MatrixCursor;

    sget-object v7, Lemp;->a:[Ljava/lang/String;

    invoke-direct {v12, v7}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 243
    const/4 v7, 0x0

    const/4 v13, 0x0

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v5, v7

    .line 244
    const/4 v7, 0x1

    const/4 v13, 0x0

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v5, v7

    .line 245
    const/4 v7, 0x2

    iget-object v13, v14, Lnym;->h:Lnyz;

    iget-object v13, v13, Lnyz;->c:Ljava/lang/String;

    aput-object v13, v5, v7

    .line 246
    const/4 v7, 0x3

    aput-object v3, v5, v7

    .line 247
    const/4 v3, 0x4

    aput-object v4, v5, v3

    .line 248
    const/4 v3, 0x5

    aput-object v6, v5, v3

    .line 249
    const/4 v3, 0x6

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v5, v3

    .line 250
    invoke-virtual {v12, v5}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 253
    new-instance v13, Landroid/database/MatrixCursor;

    sget-object v3, Lems;->a:[Ljava/lang/String;

    invoke-direct {v13, v3}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 254
    new-instance v14, Landroid/database/MatrixCursor;

    sget-object v3, Lemq;->a:[Ljava/lang/String;

    invoke-direct {v14, v3}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 257
    const/4 v3, 0x1

    new-array v6, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lemo;->d:Ljava/lang/String;

    aput-object v4, v6, v3

    .line 258
    const-string v3, "photo_comments JOIN contacts ON photo_comments.author_id=contacts.gaia_id"

    sget-object v4, Lemq;->a:[Ljava/lang/String;

    const-string v5, "tile_id = ?"

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-string v9, "view_order ASC"

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 262
    :try_start_3
    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v2

    .line 264
    sget-object v4, Lems;->a:[Ljava/lang/String;

    const/16 v4, 0x8

    new-array v4, v4, [Ljava/lang/Object;

    .line 265
    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    .line 266
    const/4 v5, 0x1

    const/4 v6, 0x2

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    .line 267
    const/4 v5, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v5

    .line 268
    const/4 v5, 0x3

    if-eqz v15, :cond_3

    iget-object v2, v15, Loae;->a:Ljava/lang/String;

    :goto_6
    aput-object v2, v4, v5

    .line 269
    const/4 v5, 0x5

    if-eqz v15, :cond_5

    iget-object v2, v15, Loae;->c:Ljava/lang/Boolean;

    .line 270
    invoke-static {v2}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    .line 269
    :goto_7
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v5

    .line 271
    const/4 v5, 0x4

    if-eqz v15, :cond_6

    iget-object v2, v15, Loae;->e:Ljava/lang/Integer;

    .line 272
    invoke-static {v2}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v2

    .line 271
    :goto_8
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v5

    .line 273
    const/4 v5, 0x6

    if-eqz v11, :cond_7

    const/4 v2, 0x1

    :goto_9
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v5

    .line 274
    const/4 v5, 0x7

    if-eqz v10, :cond_8

    const/4 v2, 0x1

    :goto_a
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v5

    .line 275
    invoke-virtual {v13, v4}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 276
    const/4 v2, 0x2

    .line 278
    :goto_b
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 279
    const/4 v4, 0x2

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 280
    const/4 v5, 0x3

    invoke-interface {v3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 281
    const/4 v6, 0x4

    .line 282
    invoke-interface {v3, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 283
    const/4 v7, 0x5

    invoke-interface {v3, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 285
    const/4 v8, 0x6

    invoke-interface {v3, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    .line 286
    const/4 v9, 0x7

    invoke-interface {v3, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 287
    const/16 v10, 0x8

    invoke-interface {v3, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 289
    sget-object v11, Lemq;->a:[Ljava/lang/String;

    const/16 v11, 0xb

    new-array v11, v11, [Ljava/lang/Object;

    .line 290
    const/4 v15, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v11, v15

    .line 291
    const/4 v15, 0x1

    const/16 v16, 0x1

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v11, v15

    .line 292
    const/4 v15, 0x2

    aput-object v4, v11, v15

    .line 293
    const/4 v4, 0x3

    aput-object v5, v11, v4

    .line 294
    const/4 v4, 0x4

    aput-object v6, v11, v4

    .line 295
    const/4 v4, 0x5

    aput-object v7, v11, v4

    .line 296
    const/4 v4, 0x6

    aput-object v8, v11, v4

    .line 297
    const/4 v4, 0x7

    aput-object v9, v11, v4

    .line 298
    const/16 v4, 0x8

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v11, v4

    .line 299
    const/16 v4, 0x9

    const/16 v5, 0x9

    invoke-interface {v3, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v11, v4

    .line 301
    const/16 v4, 0xa

    const/16 v5, 0xa

    invoke-interface {v3, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v11, v4

    .line 303
    invoke-virtual {v14, v11}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 304
    add-int/lit8 v2, v2, 0x1

    .line 305
    goto :goto_b

    .line 227
    :catchall_1
    move-exception v2

    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    throw v2

    .line 233
    :cond_1
    const/4 v5, 0x0

    move v11, v5

    goto/16 :goto_4

    .line 235
    :cond_2
    const/4 v5, 0x0

    move v10, v5

    goto/16 :goto_5

    .line 268
    :cond_3
    const/4 v2, 0x0

    goto/16 :goto_6

    .line 270
    :cond_4
    const/4 v2, 0x0

    goto/16 :goto_7

    :cond_5
    const/4 v2, 0x0

    goto/16 :goto_7

    .line 272
    :cond_6
    const/4 v2, 0x0

    goto/16 :goto_8

    .line 273
    :cond_7
    const/4 v2, 0x0

    goto/16 :goto_9

    .line 274
    :cond_8
    const/4 v2, 0x0

    goto/16 :goto_a

    .line 306
    :cond_9
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 311
    new-instance v2, Landroid/database/MergeCursor;

    const/4 v3, 0x3

    new-array v3, v3, [Landroid/database/Cursor;

    const/4 v4, 0x0

    aput-object v12, v3, v4

    const/4 v4, 0x1

    aput-object v13, v3, v4

    const/4 v4, 0x2

    aput-object v14, v3, v4

    invoke-direct {v2, v3}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    goto/16 :goto_2

    .line 308
    :catchall_2
    move-exception v2

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    throw v2

    :cond_a
    move-object v3, v10

    move-object v4, v11

    goto/16 :goto_3

    :cond_b
    move-wide v4, v10

    move-object v3, v12

    goto/16 :goto_0
.end method

.method public g()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 160
    invoke-super {p0}, Lhye;->g()V

    .line 162
    iget-boolean v0, p0, Lemo;->e:Z

    if-nez v0, :cond_0

    .line 163
    invoke-virtual {p0}, Lemo;->n()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lemo;->d:Ljava/lang/String;

    .line 164
    invoke-static {v1}, Ljvd;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lemo;->f:Ldp;

    .line 163
    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 166
    invoke-virtual {p0}, Lemo;->n()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lemo;->d:Ljava/lang/String;

    .line 167
    invoke-static {v1}, Ljvj;->p(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lemo;->f:Ldp;

    .line 166
    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 169
    const/4 v0, 0x1

    iput-boolean v0, p0, Lemo;->e:Z

    .line 171
    :cond_0
    return-void
.end method

.method protected w()V
    .locals 2

    .prologue
    .line 175
    iget-boolean v0, p0, Lemo;->e:Z

    if-eqz v0, :cond_0

    .line 176
    invoke-virtual {p0}, Lemo;->n()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lemo;->f:Ldp;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 177
    const/4 v0, 0x0

    iput-boolean v0, p0, Lemo;->e:Z

    .line 179
    :cond_0
    return-void
.end method
