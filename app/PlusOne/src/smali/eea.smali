.class public final Leea;
.super Legi;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lbc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Legi;",
        "Landroid/view/View$OnClickListener;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

.field private ah:Lezg;

.field private ai:Z

.field private final aj:Licq;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 41
    invoke-direct {p0}, Legi;-><init>()V

    .line 51
    new-instance v0, Licq;

    iget-object v1, p0, Leea;->av:Llqm;

    invoke-direct {v0, v1}, Licq;-><init>(Llqr;)V

    iput-object v0, p0, Leea;->aj:Licq;

    .line 249
    return-void
.end method

.method private a(Landroid/view/View;Z)V
    .locals 2

    .prologue
    .line 224
    if-nez p1, :cond_0

    .line 242
    :goto_0
    return-void

    .line 228
    :cond_0
    invoke-virtual {p0}, Leea;->U()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 229
    iget-object v0, p0, Leea;->aj:Licq;

    sget-object v1, Lict;->a:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    .line 230
    iget-object v0, p0, Leea;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->setVisibility(I)V

    .line 240
    :cond_1
    :goto_1
    invoke-virtual {p0}, Leea;->Z_()V

    .line 241
    invoke-virtual {p0}, Leea;->ag()V

    goto :goto_0

    .line 232
    :cond_2
    iget-object v0, p0, Leea;->aj:Licq;

    sget-object v1, Lict;->b:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    .line 233
    iget-object v0, p0, Leea;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->setVisibility(I)V

    .line 235
    if-eqz p2, :cond_1

    .line 236
    iget-object v0, p0, Leea;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->h()V

    goto :goto_1
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 136
    sget-object v0, Lhmw;->an:Lhmw;

    return-object v0
.end method

.method protected U()Z
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Leea;->ah:Lezg;

    if-eqz v0, :cond_0

    iget-object v0, p0, Leea;->ah:Lezg;

    invoke-virtual {v0}, Lezg;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public Z()Z
    .locals 1

    .prologue
    .line 258
    const/4 v0, 0x0

    return v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 61
    iget-object v0, p0, Leea;->at:Llnl;

    invoke-virtual {p1, v0}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400de

    invoke-super {p0, v0, p2, p3, v1}, Legi;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;

    move-result-object v1

    .line 63
    iget-object v0, p0, Leea;->at:Llnl;

    invoke-virtual {v0}, Llnl;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0d0291

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    .line 66
    new-instance v0, Lezg;

    iget-object v3, p0, Leea;->at:Llnl;

    iget-object v4, p0, Leea;->P:Lhee;

    invoke-interface {v4}, Lhee;->d()I

    invoke-direct {v0, v3}, Lezg;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Leea;->ah:Lezg;

    .line 67
    iget-object v0, p0, Leea;->ah:Lezg;

    invoke-virtual {v0, p0}, Lezg;->a(Landroid/view/View$OnClickListener;)V

    .line 69
    const v0, 0x7f100304

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    iput-object v0, p0, Leea;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    .line 70
    iget-object v0, p0, Leea;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->b(I)V

    .line 71
    iget-object v0, p0, Leea;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    new-instance v2, Ljvl;

    iget-object v3, p0, Leea;->at:Llnl;

    invoke-direct {v2, v3}, Ljvl;-><init>(Landroid/content/Context;)V

    iget v2, v2, Ljvl;->a:I

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(I)V

    .line 72
    iget-object v0, p0, Leea;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->b(Z)V

    .line 73
    iget-object v0, p0, Leea;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    new-instance v2, Leeb;

    invoke-direct {v2}, Leeb;-><init>()V

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(Llkg;)V

    .line 74
    iget-object v0, p0, Leea;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    iget-object v2, p0, Leea;->ah:Lezg;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(Landroid/widget/ListAdapter;)V

    .line 75
    iget-object v0, p0, Leea;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    const v2, 0x7f020415

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->g(I)V

    .line 76
    iget-object v0, p0, Leea;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(Llkc;)V

    .line 78
    iget-object v0, p0, Leea;->P:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    invoke-static {}, Ljfb;->f()Ljfb;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljfb;->b(I)Z

    move-result v0

    iput-boolean v0, p0, Leea;->ai:Z

    .line 79
    iget-object v0, p0, Leea;->P:Lhee;

    invoke-interface {v0}, Lhee;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Leea;->ai:Z

    if-eqz v0, :cond_0

    .line 80
    invoke-virtual {p0}, Leea;->w()Lbb;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v5, v2, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 83
    :cond_0
    invoke-direct {p0, v1, v5}, Leea;->a(Landroid/view/View;Z)V

    .line 85
    return-object v1
.end method

.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 175
    packed-switch p1, :pswitch_data_0

    .line 181
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 177
    :pswitch_0
    new-instance v0, Levg;

    iget-object v1, p0, Leea;->at:Llnl;

    iget-object v2, p0, Leea;->P:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    iget v3, p0, Leea;->aa:I

    invoke-direct {v0, v1, v2, v3}, Levg;-><init>(Landroid/content/Context;II)V

    goto :goto_0

    .line 175
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 55
    invoke-super {p0, p1}, Legi;->a(Landroid/os/Bundle;)V

    .line 56
    return-void
.end method

.method public a(Ldo;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 201
    invoke-virtual {p1}, Ldo;->o()I

    move-result v0

    if-nez v0, :cond_0

    .line 202
    iget-object v0, p0, Leea;->ah:Lezg;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lezg;->a(Landroid/database/Cursor;)V

    .line 204
    :cond_0
    return-void
.end method

.method public a(Ldo;Landroid/database/Cursor;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 188
    invoke-virtual {p1}, Ldo;->o()I

    move-result v0

    .line 189
    packed-switch v0, :pswitch_data_0

    .line 196
    :goto_0
    invoke-virtual {p0}, Leea;->x()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Leea;->a(Landroid/view/View;Z)V

    .line 197
    return-void

    .line 191
    :pswitch_0
    iget-object v0, p0, Leea;->ah:Lezg;

    invoke-virtual {v0, p2}, Lezg;->a(Landroid/database/Cursor;)V

    goto :goto_0

    .line 189
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 41
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Leea;->a(Ldo;Landroid/database/Cursor;)V

    return-void
.end method

.method public a(Loo;)V
    .locals 1

    .prologue
    .line 118
    invoke-super {p0, p1}, Legi;->a(Loo;)V

    .line 119
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Loo;->c(Z)V

    .line 120
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 113
    invoke-super {p0, p1}, Legi;->a(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public aO_()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 90
    invoke-super {p0}, Legi;->aO_()V

    .line 93
    iget-boolean v0, p0, Leea;->ai:Z

    iget-object v1, p0, Leea;->P:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    invoke-static {}, Ljfb;->f()Ljfb;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljfb;->b(I)Z

    move-result v1

    iput-boolean v1, p0, Leea;->ai:Z

    iget-boolean v1, p0, Leea;->ai:Z

    if-eq v0, v1, :cond_0

    iget-boolean v0, p0, Leea;->ai:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Leea;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v3, v1, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 95
    :cond_0
    :goto_0
    invoke-virtual {p0}, Leea;->x()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f100319

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 96
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 97
    return-void

    .line 93
    :cond_1
    invoke-virtual {p0}, Leea;->w()Lbb;

    move-result-object v0

    invoke-virtual {v0, v3}, Lbb;->a(I)V

    goto :goto_0
.end method

.method protected b(Lhjk;)V
    .locals 1

    .prologue
    .line 124
    invoke-super {p0, p1}, Legi;->b(Lhjk;)V

    .line 125
    const v0, 0x7f0a0b06

    invoke-interface {p1, v0}, Lhjk;->d(I)V

    .line 126
    invoke-virtual {p0}, Leea;->x()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Llsn;->b(Landroid/view/View;)V

    .line 127
    return-void
.end method

.method public c(Landroid/view/View;)Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 141
    if-nez p1, :cond_0

    move v0, v2

    .line 161
    :goto_0
    return v0

    .line 145
    :cond_0
    instance-of v0, p1, Lcom/google/android/apps/plus/views/NewAutoAwesomeMovieTileView;

    if-eqz v0, :cond_1

    .line 146
    iget-object v0, p0, Leea;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Leea;->at:Llnl;

    invoke-direct {v1, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->eG:Lhmv;

    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    invoke-static {}, Ljfb;->f()Ljfb;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljfb;->a(Z)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "movie_maker_session_id"

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "account_id"

    iget-object v2, p0, Leea;->P:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Leea;->b(Landroid/content/Intent;)V

    move v0, v3

    .line 147
    goto :goto_0

    .line 150
    :cond_1
    const v0, 0x7f100093

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_2

    move v0, v2

    .line 151
    goto :goto_0

    .line 154
    :cond_2
    instance-of v0, p1, Lcom/google/android/apps/plus/views/AutoAwesomeMovieTileView;

    if-eqz v0, :cond_3

    .line 155
    const v0, 0x7f10007f

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 156
    iget-object v1, p0, Leea;->at:Llnl;

    const-class v4, Lcnt;

    invoke-static {v1, v4}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcnt;

    .line 157
    invoke-virtual {v1, v0, v2}, Lcnt;->a(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 158
    invoke-virtual {p0, v0}, Leea;->b(Landroid/content/Intent;)V

    move v0, v3

    .line 159
    goto :goto_0

    :cond_3
    move v0, v2

    .line 161
    goto :goto_0
.end method

.method public g()V
    .locals 1

    .prologue
    .line 101
    invoke-super {p0}, Legi;->g()V

    .line 102
    iget-object v0, p0, Leea;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-static {v0}, Llii;->c(Landroid/view/View;)V

    .line 103
    return-void
.end method

.method public h()V
    .locals 1

    .prologue
    .line 107
    invoke-super {p0}, Legi;->h()V

    .line 108
    iget-object v0, p0, Leea;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-static {v0}, Llii;->d(Landroid/view/View;)V

    .line 109
    return-void
.end method
