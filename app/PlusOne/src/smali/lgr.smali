.class public Llgr;
.super Lloj;
.source "PG"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/content/DialogInterface$OnMultiChoiceClickListener;


# instance fields
.field private Q:Llgs;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lloj;-><init>()V

    .line 48
    return-void
.end method

.method private V()Llgs;
    .locals 1

    .prologue
    .line 355
    iget-object v0, p0, Llgr;->Q:Llgs;

    if-eqz v0, :cond_0

    .line 356
    iget-object v0, p0, Llgr;->Q:Llgs;

    .line 367
    :goto_0
    return-object v0

    .line 359
    :cond_0
    invoke-virtual {p0}, Llgr;->u_()Lu;

    move-result-object v0

    instance-of v0, v0, Llgs;

    if-eqz v0, :cond_1

    .line 360
    invoke-virtual {p0}, Llgr;->u_()Lu;

    move-result-object v0

    check-cast v0, Llgs;

    goto :goto_0

    .line 363
    :cond_1
    invoke-virtual {p0}, Llgr;->n()Lz;

    move-result-object v0

    instance-of v0, v0, Llgs;

    if-eqz v0, :cond_2

    .line 364
    invoke-virtual {p0}, Llgr;->n()Lz;

    move-result-object v0

    check-cast v0, Llgs;

    goto :goto_0

    .line 367
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;II)Llgr;
    .locals 7

    .prologue
    .line 114
    new-instance v0, Llgr;

    invoke-direct {v0}, Llgr;-><init>()V

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    invoke-virtual/range {v0 .. v6}, Llgr;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;II)Llgr;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;[Ljava/lang/String;)Llgr;
    .locals 2

    .prologue
    .line 172
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 173
    if-eqz p0, :cond_0

    .line 174
    const-string v1, "title"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    :cond_0
    if-eqz p1, :cond_1

    .line 177
    const-string v1, "list"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 180
    :cond_1
    new-instance v1, Llgr;

    invoke-direct {v1}, Llgr;-><init>()V

    .line 181
    invoke-virtual {v1, v0}, Llgr;->f(Landroid/os/Bundle;)V

    .line 182
    return-object v1
.end method

.method public static a(Ljava/lang/String;[Ljava/lang/String;[ZLjava/lang/String;Ljava/lang/String;)Llgr;
    .locals 2

    .prologue
    .line 198
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 199
    if-eqz p0, :cond_0

    .line 200
    const-string v1, "title"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    :cond_0
    if-eqz p1, :cond_1

    .line 204
    const-string v1, "multi_choice_list"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 207
    :cond_1
    if-eqz p2, :cond_2

    .line 208
    const-string v1, "multi_choice_list_states"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putBooleanArray(Ljava/lang/String;[Z)V

    .line 211
    :cond_2
    if-eqz p3, :cond_3

    .line 212
    const-string v1, "positive"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    :cond_3
    if-eqz p4, :cond_4

    .line 216
    const-string v1, "negative"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    :cond_4
    new-instance v1, Llgr;

    invoke-direct {v1}, Llgr;-><init>()V

    .line 220
    invoke-virtual {v1, v0}, Llgr;->f(Landroid/os/Bundle;)V

    .line 221
    return-object v1
.end method

.method public static b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)Llgr;
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 95
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v5, v4

    invoke-static/range {v0 .. v5}, Llgr;->a(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;II)Llgr;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public W_()Landroid/content/Context;
    .locals 1

    .prologue
    .line 240
    invoke-virtual {p0}, Llgr;->n()Lz;

    move-result-object v0

    return-object v0
.end method

.method public a(Llgs;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 232
    iput-object p1, p0, Llgr;->Q:Llgs;

    .line 233
    return-void
.end method

.method public b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;II)Llgr;
    .locals 2

    .prologue
    .line 133
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 134
    if-eqz p1, :cond_0

    .line 135
    const-string v1, "title"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    :cond_0
    if-eqz p2, :cond_1

    .line 139
    const-string v1, "message"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 142
    :cond_1
    if-eqz p3, :cond_2

    .line 143
    const-string v1, "positive"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    :cond_2
    if-eqz p4, :cond_3

    .line 147
    const-string v1, "negative"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    :cond_3
    if-eqz p5, :cond_4

    .line 151
    const-string v1, "icon"

    invoke-virtual {v0, v1, p5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 154
    :cond_4
    if-eqz p6, :cond_5

    .line 155
    const-string v1, "icon_attribute"

    invoke-virtual {v0, v1, p6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 158
    :cond_5
    invoke-virtual {p0, v0}, Llgr;->f(Landroid/os/Bundle;)V

    .line 159
    return-object p0
.end method

.method public c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 7

    .prologue
    .line 245
    invoke-virtual {p0}, Llgr;->k()Landroid/os/Bundle;

    move-result-object v4

    .line 247
    invoke-virtual {p0}, Llgr;->W_()Landroid/content/Context;

    move-result-object v1

    .line 248
    new-instance v5, Landroid/app/AlertDialog$Builder;

    invoke-direct {v5, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 249
    const-string v2, "title"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 250
    const-string v2, "title"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 253
    :cond_0
    const-string v2, "message"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 254
    const-string v2, "message"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v3

    .line 259
    :try_start_0
    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f04007d

    const/4 v6, 0x0

    invoke-virtual {v1, v2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .line 261
    const v1, 0x7f100139

    invoke-virtual {v6, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 263
    if-eqz v1, :cond_1

    .line 264
    instance-of v2, v3, Ljava/lang/String;

    if-eqz v2, :cond_8

    .line 265
    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0

    invoke-static {v1, v2}, Llif;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 270
    :cond_1
    :goto_0
    invoke-virtual {v5, v6}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 277
    :cond_2
    :goto_1
    const-string v1, "positive"

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 278
    const-string v1, "positive"

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 281
    :cond_3
    const-string v1, "negative"

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 282
    const-string v1, "negative"

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 285
    :cond_4
    const-string v1, "icon_attribute"

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_9

    .line 286
    const-string v1, "icon_attribute"

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v5, v1}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    .line 291
    :cond_5
    :goto_2
    const-string v1, "list"

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 292
    const-string v1, "list"

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1, p0}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 295
    :cond_6
    const-string v1, "multi_choice_list"

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 296
    const-string v1, "multi_choice_list"

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 298
    const-string v1, "multi_choice_list_states"

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 299
    const-string v1, "multi_choice_list_states"

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->getBooleanArray(Ljava/lang/String;)[Z

    move-result-object v1

    .line 303
    :goto_3
    invoke-virtual {v5, v2, v1, p0}, Landroid/app/AlertDialog$Builder;->setMultiChoiceItems([Ljava/lang/CharSequence;[ZLandroid/content/DialogInterface$OnMultiChoiceClickListener;)Landroid/app/AlertDialog$Builder;

    .line 306
    :cond_7
    invoke-virtual {v5}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    return-object v1

    .line 266
    :cond_8
    :try_start_1
    instance-of v2, v3, Landroid/text/Spannable;

    if-eqz v2, :cond_1

    .line 267
    move-object v0, v3

    check-cast v0, Landroid/text/Spannable;

    move-object v2, v0

    invoke-static {v1, v2}, Llif;->a(Landroid/widget/TextView;Landroid/text/Spannable;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 271
    :catch_0
    move-exception v1

    .line 272
    const-string v2, "AlertFragmentDialog"

    const-string v6, "Cannot inflated view"

    invoke-static {v2, v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 273
    invoke-virtual {v5, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    goto/16 :goto_1

    .line 287
    :cond_9
    const-string v1, "icon"

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 288
    const-string v1, "icon"

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v5, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    goto :goto_2

    .line 301
    :cond_a
    array-length v1, v2

    new-array v1, v1, [Z

    goto :goto_3
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 3

    .prologue
    .line 348
    invoke-direct {p0}, Llgr;->V()Llgs;

    move-result-object v0

    .line 349
    if-eqz v0, :cond_0

    .line 350
    invoke-virtual {p0}, Llgr;->k()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p0}, Llgr;->j()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Llgs;->c(Landroid/os/Bundle;Ljava/lang/String;)V

    .line 352
    :cond_0
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 311
    invoke-direct {p0}, Llgr;->V()Llgs;

    move-result-object v0

    .line 312
    if-eqz v0, :cond_0

    .line 313
    packed-switch p2, :pswitch_data_0

    .line 325
    invoke-virtual {p0}, Llgr;->k()Landroid/os/Bundle;

    move-result-object v1

    .line 326
    const-string v2, "list"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    if-ltz p2, :cond_0

    .line 327
    invoke-virtual {p0}, Llgr;->j()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, p2, v1, v2}, Llgs;->a(ILandroid/os/Bundle;Ljava/lang/String;)V

    .line 333
    :cond_0
    :goto_0
    return-void

    .line 315
    :pswitch_0
    invoke-virtual {p0}, Llgr;->k()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p0}, Llgr;->j()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Llgs;->a(Landroid/os/Bundle;Ljava/lang/String;)V

    goto :goto_0

    .line 320
    :pswitch_1
    invoke-virtual {p0}, Llgr;->k()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p0}, Llgr;->j()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Llgs;->b(Landroid/os/Bundle;Ljava/lang/String;)V

    goto :goto_0

    .line 313
    nop

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onClick(Landroid/content/DialogInterface;IZ)V
    .locals 3

    .prologue
    .line 337
    invoke-direct {p0}, Llgr;->V()Llgs;

    move-result-object v0

    .line 338
    if-eqz v0, :cond_0

    .line 339
    invoke-virtual {p0}, Llgr;->k()Landroid/os/Bundle;

    move-result-object v1

    .line 340
    const-string v2, "multi_choice_list"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    if-ltz p2, :cond_0

    .line 341
    invoke-virtual {p0}, Llgr;->j()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, p2, p3, v1, v2}, Llgs;->a(IZLandroid/os/Bundle;Ljava/lang/String;)V

    .line 344
    :cond_0
    return-void
.end method
