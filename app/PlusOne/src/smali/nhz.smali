.class public final Lnhz;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Integer;

.field private b:Ljava/lang/Integer;

.field private c:Ljava/lang/Integer;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1630
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1661
    const/4 v0, 0x0

    .line 1662
    iget-object v1, p0, Lnhz;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 1663
    const/4 v0, 0x1

    iget-object v1, p0, Lnhz;->b:Ljava/lang/Integer;

    .line 1664
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1666
    :cond_0
    iget-object v1, p0, Lnhz;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 1667
    const/4 v1, 0x2

    iget-object v2, p0, Lnhz;->c:Ljava/lang/Integer;

    .line 1668
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1670
    :cond_1
    iget-object v1, p0, Lnhz;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 1671
    const/4 v1, 0x3

    iget-object v2, p0, Lnhz;->a:Ljava/lang/Integer;

    .line 1672
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1674
    :cond_2
    iget-object v1, p0, Lnhz;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 1675
    const/4 v1, 0x4

    iget-object v2, p0, Lnhz;->d:Ljava/lang/String;

    .line 1676
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1678
    :cond_3
    iget-object v1, p0, Lnhz;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1679
    iput v0, p0, Lnhz;->ai:I

    .line 1680
    return v0
.end method

.method public a(Loxn;)Lnhz;
    .locals 2

    .prologue
    .line 1688
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1689
    sparse-switch v0, :sswitch_data_0

    .line 1693
    iget-object v1, p0, Lnhz;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1694
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnhz;->ah:Ljava/util/List;

    .line 1697
    :cond_1
    iget-object v1, p0, Lnhz;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1699
    :sswitch_0
    return-object p0

    .line 1704
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnhz;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 1708
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnhz;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 1712
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnhz;->a:Ljava/lang/Integer;

    goto :goto_0

    .line 1716
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnhz;->d:Ljava/lang/String;

    goto :goto_0

    .line 1689
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1643
    iget-object v0, p0, Lnhz;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 1644
    const/4 v0, 0x1

    iget-object v1, p0, Lnhz;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1646
    :cond_0
    iget-object v0, p0, Lnhz;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 1647
    const/4 v0, 0x2

    iget-object v1, p0, Lnhz;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1649
    :cond_1
    iget-object v0, p0, Lnhz;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 1650
    const/4 v0, 0x3

    iget-object v1, p0, Lnhz;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1652
    :cond_2
    iget-object v0, p0, Lnhz;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1653
    const/4 v0, 0x4

    iget-object v1, p0, Lnhz;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1655
    :cond_3
    iget-object v0, p0, Lnhz;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1657
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1626
    invoke-virtual {p0, p1}, Lnhz;->a(Loxn;)Lnhz;

    move-result-object v0

    return-object v0
.end method
