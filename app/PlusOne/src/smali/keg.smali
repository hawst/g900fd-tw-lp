.class public final enum Lkeg;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lkeg;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lkeg;

.field public static final enum b:Lkeg;

.field private static final synthetic c:[Lkeg;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 17
    new-instance v0, Lkeg;

    const-string v1, "EXACT"

    invoke-direct {v0, v1, v2}, Lkeg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lkeg;->a:Lkeg;

    .line 18
    new-instance v0, Lkeg;

    const-string v1, "APPROXIMATE"

    invoke-direct {v0, v1, v3}, Lkeg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lkeg;->b:Lkeg;

    .line 16
    const/4 v0, 0x2

    new-array v0, v0, [Lkeg;

    sget-object v1, Lkeg;->a:Lkeg;

    aput-object v1, v0, v2

    sget-object v1, Lkeg;->b:Lkeg;

    aput-object v1, v0, v3

    sput-object v0, Lkeg;->c:[Lkeg;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lkeg;
    .locals 1

    .prologue
    .line 16
    const-class v0, Lkeg;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lkeg;

    return-object v0
.end method

.method public static values()[Lkeg;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lkeg;->c:[Lkeg;

    invoke-virtual {v0}, [Lkeg;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lkeg;

    return-object v0
.end method
