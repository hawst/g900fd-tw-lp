.class public final Lpvl;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lpgx;

.field public b:[Lpvt;

.field public c:Lpvm;

.field public d:Lpvn;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1152
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1155
    iput-object v1, p0, Lpvl;->a:Lpgx;

    .line 1158
    sget-object v0, Lpvt;->a:[Lpvt;

    iput-object v0, p0, Lpvl;->b:[Lpvt;

    .line 1161
    iput-object v1, p0, Lpvl;->c:Lpvm;

    .line 1164
    iput-object v1, p0, Lpvl;->d:Lpvn;

    .line 1152
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1191
    .line 1192
    iget-object v0, p0, Lpvl;->a:Lpgx;

    if-eqz v0, :cond_4

    .line 1193
    const/4 v0, 0x1

    iget-object v2, p0, Lpvl;->a:Lpgx;

    .line 1194
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1196
    :goto_0
    iget-object v2, p0, Lpvl;->b:[Lpvt;

    if-eqz v2, :cond_1

    .line 1197
    iget-object v2, p0, Lpvl;->b:[Lpvt;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 1198
    if-eqz v4, :cond_0

    .line 1199
    const/4 v5, 0x2

    .line 1200
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 1197
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1204
    :cond_1
    iget-object v1, p0, Lpvl;->c:Lpvm;

    if-eqz v1, :cond_2

    .line 1205
    const/4 v1, 0x3

    iget-object v2, p0, Lpvl;->c:Lpvm;

    .line 1206
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1208
    :cond_2
    iget-object v1, p0, Lpvl;->d:Lpvn;

    if-eqz v1, :cond_3

    .line 1209
    const/4 v1, 0x4

    iget-object v2, p0, Lpvl;->d:Lpvn;

    .line 1210
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1212
    :cond_3
    iget-object v1, p0, Lpvl;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1213
    iput v0, p0, Lpvl;->ai:I

    .line 1214
    return v0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lpvl;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1222
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1223
    sparse-switch v0, :sswitch_data_0

    .line 1227
    iget-object v2, p0, Lpvl;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 1228
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lpvl;->ah:Ljava/util/List;

    .line 1231
    :cond_1
    iget-object v2, p0, Lpvl;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1233
    :sswitch_0
    return-object p0

    .line 1238
    :sswitch_1
    iget-object v0, p0, Lpvl;->a:Lpgx;

    if-nez v0, :cond_2

    .line 1239
    new-instance v0, Lpgx;

    invoke-direct {v0}, Lpgx;-><init>()V

    iput-object v0, p0, Lpvl;->a:Lpgx;

    .line 1241
    :cond_2
    iget-object v0, p0, Lpvl;->a:Lpgx;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1245
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1246
    iget-object v0, p0, Lpvl;->b:[Lpvt;

    if-nez v0, :cond_4

    move v0, v1

    .line 1247
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lpvt;

    .line 1248
    iget-object v3, p0, Lpvl;->b:[Lpvt;

    if-eqz v3, :cond_3

    .line 1249
    iget-object v3, p0, Lpvl;->b:[Lpvt;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1251
    :cond_3
    iput-object v2, p0, Lpvl;->b:[Lpvt;

    .line 1252
    :goto_2
    iget-object v2, p0, Lpvl;->b:[Lpvt;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 1253
    iget-object v2, p0, Lpvl;->b:[Lpvt;

    new-instance v3, Lpvt;

    invoke-direct {v3}, Lpvt;-><init>()V

    aput-object v3, v2, v0

    .line 1254
    iget-object v2, p0, Lpvl;->b:[Lpvt;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1255
    invoke-virtual {p1}, Loxn;->a()I

    .line 1252
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1246
    :cond_4
    iget-object v0, p0, Lpvl;->b:[Lpvt;

    array-length v0, v0

    goto :goto_1

    .line 1258
    :cond_5
    iget-object v2, p0, Lpvl;->b:[Lpvt;

    new-instance v3, Lpvt;

    invoke-direct {v3}, Lpvt;-><init>()V

    aput-object v3, v2, v0

    .line 1259
    iget-object v2, p0, Lpvl;->b:[Lpvt;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1263
    :sswitch_3
    iget-object v0, p0, Lpvl;->c:Lpvm;

    if-nez v0, :cond_6

    .line 1264
    new-instance v0, Lpvm;

    invoke-direct {v0}, Lpvm;-><init>()V

    iput-object v0, p0, Lpvl;->c:Lpvm;

    .line 1266
    :cond_6
    iget-object v0, p0, Lpvl;->c:Lpvm;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1270
    :sswitch_4
    iget-object v0, p0, Lpvl;->d:Lpvn;

    if-nez v0, :cond_7

    .line 1271
    new-instance v0, Lpvn;

    invoke-direct {v0}, Lpvn;-><init>()V

    iput-object v0, p0, Lpvl;->d:Lpvn;

    .line 1273
    :cond_7
    iget-object v0, p0, Lpvl;->d:Lpvn;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1223
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 1169
    iget-object v0, p0, Lpvl;->a:Lpgx;

    if-eqz v0, :cond_0

    .line 1170
    const/4 v0, 0x1

    iget-object v1, p0, Lpvl;->a:Lpgx;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1172
    :cond_0
    iget-object v0, p0, Lpvl;->b:[Lpvt;

    if-eqz v0, :cond_2

    .line 1173
    iget-object v1, p0, Lpvl;->b:[Lpvt;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 1174
    if-eqz v3, :cond_1

    .line 1175
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 1173
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1179
    :cond_2
    iget-object v0, p0, Lpvl;->c:Lpvm;

    if-eqz v0, :cond_3

    .line 1180
    const/4 v0, 0x3

    iget-object v1, p0, Lpvl;->c:Lpvm;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1182
    :cond_3
    iget-object v0, p0, Lpvl;->d:Lpvn;

    if-eqz v0, :cond_4

    .line 1183
    const/4 v0, 0x4

    iget-object v1, p0, Lpvl;->d:Lpvn;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1185
    :cond_4
    iget-object v0, p0, Lpvl;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1187
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1148
    invoke-virtual {p0, p1}, Lpvl;->a(Loxn;)Lpvl;

    move-result-object v0

    return-object v0
.end method
