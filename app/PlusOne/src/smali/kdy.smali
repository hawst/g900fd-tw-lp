.class final Lkdy;
.super Lgl;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lgl",
        "<",
        "Lkdc;",
        "Lcom/google/android/libraries/social/resources/images/ImageResource;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(I)V
    .locals 0

    .prologue
    .line 208
    invoke-direct {p0, p1}, Lgl;-><init>(I)V

    return-void
.end method


# virtual methods
.method protected a(Lcom/google/android/libraries/social/resources/images/ImageResource;)I
    .locals 1

    .prologue
    .line 212
    invoke-virtual {p1}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getSizeInBytes()I

    move-result v0

    return v0
.end method

.method protected bridge synthetic a(ZLjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 208
    check-cast p2, Lkdc;

    check-cast p3, Lcom/google/android/libraries/social/resources/images/ImageResource;

    invoke-virtual {p0, p1, p2, p3}, Lkdy;->a(ZLkdc;Lcom/google/android/libraries/social/resources/images/ImageResource;)V

    return-void
.end method

.method protected a(ZLkdc;Lcom/google/android/libraries/social/resources/images/ImageResource;)V
    .locals 6

    .prologue
    .line 218
    if-eqz p1, :cond_1

    .line 219
    invoke-virtual {p3}, Lcom/google/android/libraries/social/resources/images/ImageResource;->isDebugLogEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 220
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1a

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Evicted image from cache: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->logDebug(Ljava/lang/String;)V

    .line 222
    const-string v0, "ImageResourceManager"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 223
    const-string v0, "Currently in cache: "

    invoke-virtual {p3, v0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->logDebug(Ljava/lang/String;)V

    .line 224
    invoke-virtual {p0}, Lkdy;->h()Ljava/util/Map;

    move-result-object v1

    .line 225
    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkdc;

    .line 226
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 227
    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/resources/images/ImageResource;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getSizeInBytes()I

    move-result v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x1a

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "   id: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "; size: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 226
    invoke-virtual {p3, v0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->logDebug(Ljava/lang/String;)V

    goto :goto_0

    .line 232
    :cond_0
    invoke-virtual {p3}, Lcom/google/android/libraries/social/resources/images/ImageResource;->recycle()V

    .line 234
    :cond_1
    return-void
.end method

.method protected synthetic c(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 208
    check-cast p1, Lcom/google/android/libraries/social/resources/images/ImageResource;

    invoke-virtual {p0, p1}, Lkdy;->a(Lcom/google/android/libraries/social/resources/images/ImageResource;)I

    move-result v0

    return v0
.end method
