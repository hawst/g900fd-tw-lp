.class public final Lprc;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/Integer;

.field private c:[Lprd;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 217
    invoke-direct {p0}, Loxq;-><init>()V

    .line 224
    sget-object v0, Lprd;->a:[Lprd;

    iput-object v0, p0, Lprc;->c:[Lprd;

    .line 217
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 248
    .line 249
    iget-object v0, p0, Lprc;->a:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 250
    const/4 v0, 0x1

    iget-object v2, p0, Lprc;->a:Ljava/lang/String;

    .line 251
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 253
    :goto_0
    iget-object v2, p0, Lprc;->b:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    .line 254
    const/4 v2, 0x2

    iget-object v3, p0, Lprc;->b:Ljava/lang/Integer;

    .line 255
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 257
    :cond_0
    iget-object v2, p0, Lprc;->c:[Lprd;

    if-eqz v2, :cond_2

    .line 258
    iget-object v2, p0, Lprc;->c:[Lprd;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 259
    if-eqz v4, :cond_1

    .line 260
    const/4 v5, 0x3

    .line 261
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 258
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 265
    :cond_2
    iget-object v1, p0, Lprc;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 266
    iput v0, p0, Lprc;->ai:I

    .line 267
    return v0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lprc;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 275
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 276
    sparse-switch v0, :sswitch_data_0

    .line 280
    iget-object v2, p0, Lprc;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 281
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lprc;->ah:Ljava/util/List;

    .line 284
    :cond_1
    iget-object v2, p0, Lprc;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 286
    :sswitch_0
    return-object p0

    .line 291
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lprc;->a:Ljava/lang/String;

    goto :goto_0

    .line 295
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lprc;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 299
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 300
    iget-object v0, p0, Lprc;->c:[Lprd;

    if-nez v0, :cond_3

    move v0, v1

    .line 301
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lprd;

    .line 302
    iget-object v3, p0, Lprc;->c:[Lprd;

    if-eqz v3, :cond_2

    .line 303
    iget-object v3, p0, Lprc;->c:[Lprd;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 305
    :cond_2
    iput-object v2, p0, Lprc;->c:[Lprd;

    .line 306
    :goto_2
    iget-object v2, p0, Lprc;->c:[Lprd;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 307
    iget-object v2, p0, Lprc;->c:[Lprd;

    new-instance v3, Lprd;

    invoke-direct {v3}, Lprd;-><init>()V

    aput-object v3, v2, v0

    .line 308
    iget-object v2, p0, Lprc;->c:[Lprd;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 309
    invoke-virtual {p1}, Loxn;->a()I

    .line 306
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 300
    :cond_3
    iget-object v0, p0, Lprc;->c:[Lprd;

    array-length v0, v0

    goto :goto_1

    .line 312
    :cond_4
    iget-object v2, p0, Lprc;->c:[Lprd;

    new-instance v3, Lprd;

    invoke-direct {v3}, Lprd;-><init>()V

    aput-object v3, v2, v0

    .line 313
    iget-object v2, p0, Lprc;->c:[Lprd;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 276
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 229
    iget-object v0, p0, Lprc;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 230
    const/4 v0, 0x1

    iget-object v1, p0, Lprc;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 232
    :cond_0
    iget-object v0, p0, Lprc;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 233
    const/4 v0, 0x2

    iget-object v1, p0, Lprc;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 235
    :cond_1
    iget-object v0, p0, Lprc;->c:[Lprd;

    if-eqz v0, :cond_3

    .line 236
    iget-object v1, p0, Lprc;->c:[Lprd;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 237
    if-eqz v3, :cond_2

    .line 238
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 236
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 242
    :cond_3
    iget-object v0, p0, Lprc;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 244
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 213
    invoke-virtual {p0, p1}, Lprc;->a(Loxn;)Lprc;

    move-result-object v0

    return-object v0
.end method
