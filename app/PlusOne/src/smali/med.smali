.class public final Lmed;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lmpv;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 19276
    invoke-direct {p0}, Loxq;-><init>()V

    .line 19279
    iput-object v0, p0, Lmed;->apiHeader:Llyr;

    .line 19282
    iput-object v0, p0, Lmed;->a:Lmpv;

    .line 19276
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 19299
    const/4 v0, 0x0

    .line 19300
    iget-object v1, p0, Lmed;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 19301
    const/4 v0, 0x1

    iget-object v1, p0, Lmed;->apiHeader:Llyr;

    .line 19302
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 19304
    :cond_0
    iget-object v1, p0, Lmed;->a:Lmpv;

    if-eqz v1, :cond_1

    .line 19305
    const/4 v1, 0x2

    iget-object v2, p0, Lmed;->a:Lmpv;

    .line 19306
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 19308
    :cond_1
    iget-object v1, p0, Lmed;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 19309
    iput v0, p0, Lmed;->ai:I

    .line 19310
    return v0
.end method

.method public a(Loxn;)Lmed;
    .locals 2

    .prologue
    .line 19318
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 19319
    sparse-switch v0, :sswitch_data_0

    .line 19323
    iget-object v1, p0, Lmed;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 19324
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmed;->ah:Ljava/util/List;

    .line 19327
    :cond_1
    iget-object v1, p0, Lmed;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 19329
    :sswitch_0
    return-object p0

    .line 19334
    :sswitch_1
    iget-object v0, p0, Lmed;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 19335
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmed;->apiHeader:Llyr;

    .line 19337
    :cond_2
    iget-object v0, p0, Lmed;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 19341
    :sswitch_2
    iget-object v0, p0, Lmed;->a:Lmpv;

    if-nez v0, :cond_3

    .line 19342
    new-instance v0, Lmpv;

    invoke-direct {v0}, Lmpv;-><init>()V

    iput-object v0, p0, Lmed;->a:Lmpv;

    .line 19344
    :cond_3
    iget-object v0, p0, Lmed;->a:Lmpv;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 19319
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 19287
    iget-object v0, p0, Lmed;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 19288
    const/4 v0, 0x1

    iget-object v1, p0, Lmed;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 19290
    :cond_0
    iget-object v0, p0, Lmed;->a:Lmpv;

    if-eqz v0, :cond_1

    .line 19291
    const/4 v0, 0x2

    iget-object v1, p0, Lmed;->a:Lmpv;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 19293
    :cond_1
    iget-object v0, p0, Lmed;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 19295
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 19272
    invoke-virtual {p0, p1}, Lmed;->a(Loxn;)Lmed;

    move-result-object v0

    return-object v0
.end method
