.class public final Lofj;
.super Loxq;
.source "PG"


# instance fields
.field public a:I

.field public b:[Lofv;

.field public c:Ljava/lang/String;

.field private d:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1023
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1026
    const/high16 v0, -0x80000000

    iput v0, p0, Lofj;->a:I

    .line 1029
    sget-object v0, Lofv;->a:[Lofv;

    iput-object v0, p0, Lofj;->b:[Lofv;

    .line 1023
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1060
    .line 1061
    iget v0, p0, Lofj;->a:I

    const/high16 v2, -0x80000000

    if-eq v0, v2, :cond_4

    .line 1062
    const/4 v0, 0x1

    iget v2, p0, Lofj;->a:I

    .line 1063
    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1065
    :goto_0
    iget-object v2, p0, Lofj;->b:[Lofv;

    if-eqz v2, :cond_1

    .line 1066
    iget-object v2, p0, Lofj;->b:[Lofv;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 1067
    if-eqz v4, :cond_0

    .line 1068
    const/4 v5, 0x2

    .line 1069
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 1066
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1073
    :cond_1
    iget-object v1, p0, Lofj;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 1074
    const/4 v1, 0x3

    iget-object v2, p0, Lofj;->d:Ljava/lang/Integer;

    .line 1075
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1077
    :cond_2
    iget-object v1, p0, Lofj;->c:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 1078
    const/4 v1, 0x4

    iget-object v2, p0, Lofj;->c:Ljava/lang/String;

    .line 1079
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1081
    :cond_3
    iget-object v1, p0, Lofj;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1082
    iput v0, p0, Lofj;->ai:I

    .line 1083
    return v0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lofj;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1091
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1092
    sparse-switch v0, :sswitch_data_0

    .line 1096
    iget-object v2, p0, Lofj;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 1097
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lofj;->ah:Ljava/util/List;

    .line 1100
    :cond_1
    iget-object v2, p0, Lofj;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1102
    :sswitch_0
    return-object p0

    .line 1107
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1108
    if-eqz v0, :cond_2

    const/4 v2, 0x1

    if-eq v0, v2, :cond_2

    const/4 v2, 0x2

    if-eq v0, v2, :cond_2

    const/4 v2, 0x3

    if-eq v0, v2, :cond_2

    const/4 v2, 0x4

    if-eq v0, v2, :cond_2

    const/4 v2, 0x5

    if-eq v0, v2, :cond_2

    const/16 v2, 0x64

    if-eq v0, v2, :cond_2

    const/16 v2, 0x65

    if-eq v0, v2, :cond_2

    const/16 v2, 0x66

    if-eq v0, v2, :cond_2

    const/16 v2, 0xc8

    if-eq v0, v2, :cond_2

    const/16 v2, 0xc9

    if-eq v0, v2, :cond_2

    const/16 v2, 0xca

    if-eq v0, v2, :cond_2

    const/16 v2, 0xcb

    if-eq v0, v2, :cond_2

    const/16 v2, 0xcc

    if-eq v0, v2, :cond_2

    const/16 v2, 0xcd

    if-eq v0, v2, :cond_2

    const/16 v2, 0xce

    if-eq v0, v2, :cond_2

    const/16 v2, 0xcf

    if-eq v0, v2, :cond_2

    const/16 v2, 0x12c

    if-eq v0, v2, :cond_2

    const/16 v2, 0x191

    if-eq v0, v2, :cond_2

    const/16 v2, 0x192

    if-eq v0, v2, :cond_2

    const/16 v2, 0x1f4

    if-eq v0, v2, :cond_2

    const/16 v2, 0x258

    if-eq v0, v2, :cond_2

    const/16 v2, 0x259

    if-eq v0, v2, :cond_2

    const/16 v2, 0x25a

    if-eq v0, v2, :cond_2

    const/16 v2, 0x25b

    if-eq v0, v2, :cond_2

    const/16 v2, 0x25c

    if-eq v0, v2, :cond_2

    const/16 v2, 0x25d

    if-eq v0, v2, :cond_2

    const/16 v2, 0x25e

    if-eq v0, v2, :cond_2

    const/16 v2, 0x2bc

    if-eq v0, v2, :cond_2

    const/16 v2, 0x2bd

    if-eq v0, v2, :cond_2

    const/16 v2, 0x320

    if-eq v0, v2, :cond_2

    const/16 v2, 0x321

    if-eq v0, v2, :cond_2

    const/16 v2, 0x322

    if-eq v0, v2, :cond_2

    const/16 v2, 0x323

    if-eq v0, v2, :cond_2

    const/16 v2, 0x384

    if-ne v0, v2, :cond_3

    .line 1143
    :cond_2
    iput v0, p0, Lofj;->a:I

    goto/16 :goto_0

    .line 1145
    :cond_3
    iput v1, p0, Lofj;->a:I

    goto/16 :goto_0

    .line 1150
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1151
    iget-object v0, p0, Lofj;->b:[Lofv;

    if-nez v0, :cond_5

    move v0, v1

    .line 1152
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lofv;

    .line 1153
    iget-object v3, p0, Lofj;->b:[Lofv;

    if-eqz v3, :cond_4

    .line 1154
    iget-object v3, p0, Lofj;->b:[Lofv;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1156
    :cond_4
    iput-object v2, p0, Lofj;->b:[Lofv;

    .line 1157
    :goto_2
    iget-object v2, p0, Lofj;->b:[Lofv;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 1158
    iget-object v2, p0, Lofj;->b:[Lofv;

    new-instance v3, Lofv;

    invoke-direct {v3}, Lofv;-><init>()V

    aput-object v3, v2, v0

    .line 1159
    iget-object v2, p0, Lofj;->b:[Lofv;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1160
    invoke-virtual {p1}, Loxn;->a()I

    .line 1157
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1151
    :cond_5
    iget-object v0, p0, Lofj;->b:[Lofv;

    array-length v0, v0

    goto :goto_1

    .line 1163
    :cond_6
    iget-object v2, p0, Lofj;->b:[Lofv;

    new-instance v3, Lofv;

    invoke-direct {v3}, Lofv;-><init>()V

    aput-object v3, v2, v0

    .line 1164
    iget-object v2, p0, Lofj;->b:[Lofv;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1168
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lofj;->d:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 1172
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lofj;->c:Ljava/lang/String;

    goto/16 :goto_0

    .line 1092
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 1038
    iget v0, p0, Lofj;->a:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 1039
    const/4 v0, 0x1

    iget v1, p0, Lofj;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1041
    :cond_0
    iget-object v0, p0, Lofj;->b:[Lofv;

    if-eqz v0, :cond_2

    .line 1042
    iget-object v1, p0, Lofj;->b:[Lofv;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 1043
    if-eqz v3, :cond_1

    .line 1044
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 1042
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1048
    :cond_2
    iget-object v0, p0, Lofj;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 1049
    const/4 v0, 0x3

    iget-object v1, p0, Lofj;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1051
    :cond_3
    iget-object v0, p0, Lofj;->c:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 1052
    const/4 v0, 0x4

    iget-object v1, p0, Lofj;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1054
    :cond_4
    iget-object v0, p0, Lofj;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1056
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1019
    invoke-virtual {p0, p1}, Lofj;->a(Loxn;)Lofj;

    move-result-object v0

    return-object v0
.end method
