.class public final Lokm;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:[Lokp;

.field private c:[Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/Integer;

.field private f:Ljava/lang/Integer;

.field private g:Ljava/lang/Long;

.field private h:Ljava/lang/Long;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:I

.field private m:Ljava/lang/Boolean;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 305
    invoke-direct {p0}, Loxq;-><init>()V

    .line 310
    sget-object v0, Lokp;->a:[Lokp;

    iput-object v0, p0, Lokm;->b:[Lokp;

    .line 313
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lokm;->c:[Ljava/lang/String;

    .line 332
    const/high16 v0, -0x80000000

    iput v0, p0, Lokm;->l:I

    .line 305
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 398
    const/4 v0, 0x1

    iget-object v2, p0, Lokm;->a:Ljava/lang/String;

    .line 400
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 401
    iget-object v2, p0, Lokm;->c:[Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lokm;->c:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 403
    iget-object v4, p0, Lokm;->c:[Ljava/lang/String;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_0
    if-ge v2, v5, :cond_0

    aget-object v6, v4, v2

    .line 405
    invoke-static {v6}, Loxo;->b(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v3, v6

    .line 403
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 407
    :cond_0
    add-int/2addr v0, v3

    .line 408
    iget-object v2, p0, Lokm;->c:[Ljava/lang/String;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 410
    :cond_1
    iget-object v2, p0, Lokm;->d:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 411
    const/16 v2, 0x8

    iget-object v3, p0, Lokm;->d:Ljava/lang/String;

    .line 412
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 414
    :cond_2
    iget-object v2, p0, Lokm;->g:Ljava/lang/Long;

    if-eqz v2, :cond_3

    .line 415
    const/16 v2, 0x9

    iget-object v3, p0, Lokm;->g:Ljava/lang/Long;

    .line 416
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Loxo;->e(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 418
    :cond_3
    iget-object v2, p0, Lokm;->b:[Lokp;

    if-eqz v2, :cond_5

    .line 419
    iget-object v2, p0, Lokm;->b:[Lokp;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    .line 420
    if-eqz v4, :cond_4

    .line 421
    const/16 v5, 0xa

    .line 422
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 419
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 426
    :cond_5
    iget-object v1, p0, Lokm;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 427
    const/16 v1, 0xb

    iget-object v2, p0, Lokm;->f:Ljava/lang/Integer;

    .line 428
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 430
    :cond_6
    iget-object v1, p0, Lokm;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    .line 431
    const/16 v1, 0xc

    iget-object v2, p0, Lokm;->e:Ljava/lang/Integer;

    .line 432
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 434
    :cond_7
    iget-object v1, p0, Lokm;->h:Ljava/lang/Long;

    if-eqz v1, :cond_8

    .line 435
    const/16 v1, 0xd

    iget-object v2, p0, Lokm;->h:Ljava/lang/Long;

    .line 436
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 438
    :cond_8
    iget-object v1, p0, Lokm;->i:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 439
    const/16 v1, 0xe

    iget-object v2, p0, Lokm;->i:Ljava/lang/String;

    .line 440
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 442
    :cond_9
    iget-object v1, p0, Lokm;->j:Ljava/lang/String;

    if-eqz v1, :cond_a

    .line 443
    const/16 v1, 0xf

    iget-object v2, p0, Lokm;->j:Ljava/lang/String;

    .line 444
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 446
    :cond_a
    iget-object v1, p0, Lokm;->k:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 447
    const/16 v1, 0x10

    iget-object v2, p0, Lokm;->k:Ljava/lang/String;

    .line 448
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 450
    :cond_b
    iget v1, p0, Lokm;->l:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_c

    .line 451
    const/16 v1, 0x11

    iget v2, p0, Lokm;->l:I

    .line 452
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 454
    :cond_c
    iget-object v1, p0, Lokm;->m:Ljava/lang/Boolean;

    if-eqz v1, :cond_d

    .line 455
    const/16 v1, 0x12

    iget-object v2, p0, Lokm;->m:Ljava/lang/Boolean;

    .line 456
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 458
    :cond_d
    iget-object v1, p0, Lokm;->n:Ljava/lang/String;

    if-eqz v1, :cond_e

    .line 459
    const/16 v1, 0x13

    iget-object v2, p0, Lokm;->n:Ljava/lang/String;

    .line 460
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 462
    :cond_e
    iget-object v1, p0, Lokm;->o:Ljava/lang/Boolean;

    if-eqz v1, :cond_f

    .line 463
    const/16 v1, 0x14

    iget-object v2, p0, Lokm;->o:Ljava/lang/Boolean;

    .line 464
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 466
    :cond_f
    iget-object v1, p0, Lokm;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 467
    iput v0, p0, Lokm;->ai:I

    .line 468
    return v0
.end method

.method public a(Loxn;)Lokm;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 476
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 477
    sparse-switch v0, :sswitch_data_0

    .line 481
    iget-object v2, p0, Lokm;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 482
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lokm;->ah:Ljava/util/List;

    .line 485
    :cond_1
    iget-object v2, p0, Lokm;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 487
    :sswitch_0
    return-object p0

    .line 492
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lokm;->a:Ljava/lang/String;

    goto :goto_0

    .line 496
    :sswitch_2
    const/16 v0, 0x22

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 497
    iget-object v0, p0, Lokm;->c:[Ljava/lang/String;

    array-length v0, v0

    .line 498
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 499
    iget-object v3, p0, Lokm;->c:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 500
    iput-object v2, p0, Lokm;->c:[Ljava/lang/String;

    .line 501
    :goto_1
    iget-object v2, p0, Lokm;->c:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_2

    .line 502
    iget-object v2, p0, Lokm;->c:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 503
    invoke-virtual {p1}, Loxn;->a()I

    .line 501
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 506
    :cond_2
    iget-object v2, p0, Lokm;->c:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto :goto_0

    .line 510
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lokm;->d:Ljava/lang/String;

    goto :goto_0

    .line 514
    :sswitch_4
    invoke-virtual {p1}, Loxn;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lokm;->g:Ljava/lang/Long;

    goto :goto_0

    .line 518
    :sswitch_5
    const/16 v0, 0x52

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 519
    iget-object v0, p0, Lokm;->b:[Lokp;

    if-nez v0, :cond_4

    move v0, v1

    .line 520
    :goto_2
    add-int/2addr v2, v0

    new-array v2, v2, [Lokp;

    .line 521
    iget-object v3, p0, Lokm;->b:[Lokp;

    if-eqz v3, :cond_3

    .line 522
    iget-object v3, p0, Lokm;->b:[Lokp;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 524
    :cond_3
    iput-object v2, p0, Lokm;->b:[Lokp;

    .line 525
    :goto_3
    iget-object v2, p0, Lokm;->b:[Lokp;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 526
    iget-object v2, p0, Lokm;->b:[Lokp;

    new-instance v3, Lokp;

    invoke-direct {v3}, Lokp;-><init>()V

    aput-object v3, v2, v0

    .line 527
    iget-object v2, p0, Lokm;->b:[Lokp;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 528
    invoke-virtual {p1}, Loxn;->a()I

    .line 525
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 519
    :cond_4
    iget-object v0, p0, Lokm;->b:[Lokp;

    array-length v0, v0

    goto :goto_2

    .line 531
    :cond_5
    iget-object v2, p0, Lokm;->b:[Lokp;

    new-instance v3, Lokp;

    invoke-direct {v3}, Lokp;-><init>()V

    aput-object v3, v2, v0

    .line 532
    iget-object v2, p0, Lokm;->b:[Lokp;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 536
    :sswitch_6
    invoke-virtual {p1}, Loxn;->m()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lokm;->f:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 540
    :sswitch_7
    invoke-virtual {p1}, Loxn;->m()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lokm;->e:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 544
    :sswitch_8
    invoke-virtual {p1}, Loxn;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lokm;->h:Ljava/lang/Long;

    goto/16 :goto_0

    .line 548
    :sswitch_9
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lokm;->i:Ljava/lang/String;

    goto/16 :goto_0

    .line 552
    :sswitch_a
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lokm;->j:Ljava/lang/String;

    goto/16 :goto_0

    .line 556
    :sswitch_b
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lokm;->k:Ljava/lang/String;

    goto/16 :goto_0

    .line 560
    :sswitch_c
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 561
    if-eqz v0, :cond_6

    const/4 v2, 0x1

    if-ne v0, v2, :cond_7

    .line 563
    :cond_6
    iput v0, p0, Lokm;->l:I

    goto/16 :goto_0

    .line 565
    :cond_7
    iput v1, p0, Lokm;->l:I

    goto/16 :goto_0

    .line 570
    :sswitch_d
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lokm;->m:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 574
    :sswitch_e
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lokm;->n:Ljava/lang/String;

    goto/16 :goto_0

    .line 578
    :sswitch_f
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lokm;->o:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 477
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x22 -> :sswitch_2
        0x42 -> :sswitch_3
        0x48 -> :sswitch_4
        0x52 -> :sswitch_5
        0x58 -> :sswitch_6
        0x60 -> :sswitch_7
        0x68 -> :sswitch_8
        0x72 -> :sswitch_9
        0x7a -> :sswitch_a
        0x82 -> :sswitch_b
        0x88 -> :sswitch_c
        0x90 -> :sswitch_d
        0x9a -> :sswitch_e
        0xa0 -> :sswitch_f
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 343
    const/4 v1, 0x1

    iget-object v2, p0, Lokm;->a:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 344
    iget-object v1, p0, Lokm;->c:[Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 345
    iget-object v2, p0, Lokm;->c:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 346
    const/4 v5, 0x4

    invoke-virtual {p1, v5, v4}, Loxo;->a(ILjava/lang/String;)V

    .line 345
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 349
    :cond_0
    iget-object v1, p0, Lokm;->d:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 350
    const/16 v1, 0x8

    iget-object v2, p0, Lokm;->d:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 352
    :cond_1
    iget-object v1, p0, Lokm;->g:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 353
    const/16 v1, 0x9

    iget-object v2, p0, Lokm;->g:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Loxo;->a(IJ)V

    .line 355
    :cond_2
    iget-object v1, p0, Lokm;->b:[Lokp;

    if-eqz v1, :cond_4

    .line 356
    iget-object v1, p0, Lokm;->b:[Lokp;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 357
    if-eqz v3, :cond_3

    .line 358
    const/16 v4, 0xa

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 356
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 362
    :cond_4
    iget-object v0, p0, Lokm;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 363
    const/16 v0, 0xb

    iget-object v1, p0, Lokm;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->c(II)V

    .line 365
    :cond_5
    iget-object v0, p0, Lokm;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 366
    const/16 v0, 0xc

    iget-object v1, p0, Lokm;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->c(II)V

    .line 368
    :cond_6
    iget-object v0, p0, Lokm;->h:Ljava/lang/Long;

    if-eqz v0, :cond_7

    .line 369
    const/16 v0, 0xd

    iget-object v1, p0, Lokm;->h:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(IJ)V

    .line 371
    :cond_7
    iget-object v0, p0, Lokm;->i:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 372
    const/16 v0, 0xe

    iget-object v1, p0, Lokm;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 374
    :cond_8
    iget-object v0, p0, Lokm;->j:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 375
    const/16 v0, 0xf

    iget-object v1, p0, Lokm;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 377
    :cond_9
    iget-object v0, p0, Lokm;->k:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 378
    const/16 v0, 0x10

    iget-object v1, p0, Lokm;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 380
    :cond_a
    iget v0, p0, Lokm;->l:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_b

    .line 381
    const/16 v0, 0x11

    iget v1, p0, Lokm;->l:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 383
    :cond_b
    iget-object v0, p0, Lokm;->m:Ljava/lang/Boolean;

    if-eqz v0, :cond_c

    .line 384
    const/16 v0, 0x12

    iget-object v1, p0, Lokm;->m:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 386
    :cond_c
    iget-object v0, p0, Lokm;->n:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 387
    const/16 v0, 0x13

    iget-object v1, p0, Lokm;->n:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 389
    :cond_d
    iget-object v0, p0, Lokm;->o:Ljava/lang/Boolean;

    if-eqz v0, :cond_e

    .line 390
    const/16 v0, 0x14

    iget-object v1, p0, Lokm;->o:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 392
    :cond_e
    iget-object v0, p0, Lokm;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 394
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 301
    invoke-virtual {p0, p1}, Lokm;->a(Loxn;)Lokm;

    move-result-object v0

    return-object v0
.end method
