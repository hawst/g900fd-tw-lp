.class public final Lnpy;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lnpx;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field private d:Lnpz;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1327
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1330
    sget-object v0, Lnpx;->a:[Lnpx;

    iput-object v0, p0, Lnpy;->a:[Lnpx;

    .line 1337
    const/4 v0, 0x0

    iput-object v0, p0, Lnpy;->d:Lnpz;

    .line 1327
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1364
    .line 1365
    iget-object v1, p0, Lnpy;->a:[Lnpx;

    if-eqz v1, :cond_1

    .line 1366
    iget-object v2, p0, Lnpy;->a:[Lnpx;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 1367
    if-eqz v4, :cond_0

    .line 1368
    const/4 v5, 0x1

    .line 1369
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 1366
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1373
    :cond_1
    iget-object v1, p0, Lnpy;->b:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1374
    const/4 v1, 0x2

    iget-object v2, p0, Lnpy;->b:Ljava/lang/String;

    .line 1375
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1377
    :cond_2
    iget-object v1, p0, Lnpy;->c:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 1378
    const/4 v1, 0x3

    iget-object v2, p0, Lnpy;->c:Ljava/lang/String;

    .line 1379
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1381
    :cond_3
    iget-object v1, p0, Lnpy;->d:Lnpz;

    if-eqz v1, :cond_4

    .line 1382
    const/4 v1, 0x4

    iget-object v2, p0, Lnpy;->d:Lnpz;

    .line 1383
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1385
    :cond_4
    iget-object v1, p0, Lnpy;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1386
    iput v0, p0, Lnpy;->ai:I

    .line 1387
    return v0
.end method

.method public a(Loxn;)Lnpy;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1395
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1396
    sparse-switch v0, :sswitch_data_0

    .line 1400
    iget-object v2, p0, Lnpy;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 1401
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnpy;->ah:Ljava/util/List;

    .line 1404
    :cond_1
    iget-object v2, p0, Lnpy;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1406
    :sswitch_0
    return-object p0

    .line 1411
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1412
    iget-object v0, p0, Lnpy;->a:[Lnpx;

    if-nez v0, :cond_3

    move v0, v1

    .line 1413
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnpx;

    .line 1414
    iget-object v3, p0, Lnpy;->a:[Lnpx;

    if-eqz v3, :cond_2

    .line 1415
    iget-object v3, p0, Lnpy;->a:[Lnpx;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1417
    :cond_2
    iput-object v2, p0, Lnpy;->a:[Lnpx;

    .line 1418
    :goto_2
    iget-object v2, p0, Lnpy;->a:[Lnpx;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 1419
    iget-object v2, p0, Lnpy;->a:[Lnpx;

    new-instance v3, Lnpx;

    invoke-direct {v3}, Lnpx;-><init>()V

    aput-object v3, v2, v0

    .line 1420
    iget-object v2, p0, Lnpy;->a:[Lnpx;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1421
    invoke-virtual {p1}, Loxn;->a()I

    .line 1418
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1412
    :cond_3
    iget-object v0, p0, Lnpy;->a:[Lnpx;

    array-length v0, v0

    goto :goto_1

    .line 1424
    :cond_4
    iget-object v2, p0, Lnpy;->a:[Lnpx;

    new-instance v3, Lnpx;

    invoke-direct {v3}, Lnpx;-><init>()V

    aput-object v3, v2, v0

    .line 1425
    iget-object v2, p0, Lnpy;->a:[Lnpx;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1429
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnpy;->b:Ljava/lang/String;

    goto :goto_0

    .line 1433
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnpy;->c:Ljava/lang/String;

    goto :goto_0

    .line 1437
    :sswitch_4
    iget-object v0, p0, Lnpy;->d:Lnpz;

    if-nez v0, :cond_5

    .line 1438
    new-instance v0, Lnpz;

    invoke-direct {v0}, Lnpz;-><init>()V

    iput-object v0, p0, Lnpy;->d:Lnpz;

    .line 1440
    :cond_5
    iget-object v0, p0, Lnpy;->d:Lnpz;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1396
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 1342
    iget-object v0, p0, Lnpy;->a:[Lnpx;

    if-eqz v0, :cond_1

    .line 1343
    iget-object v1, p0, Lnpy;->a:[Lnpx;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 1344
    if-eqz v3, :cond_0

    .line 1345
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 1343
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1349
    :cond_1
    iget-object v0, p0, Lnpy;->b:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1350
    const/4 v0, 0x2

    iget-object v1, p0, Lnpy;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1352
    :cond_2
    iget-object v0, p0, Lnpy;->c:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1353
    const/4 v0, 0x3

    iget-object v1, p0, Lnpy;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1355
    :cond_3
    iget-object v0, p0, Lnpy;->d:Lnpz;

    if-eqz v0, :cond_4

    .line 1356
    const/4 v0, 0x4

    iget-object v1, p0, Lnpy;->d:Lnpz;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1358
    :cond_4
    iget-object v0, p0, Lnpy;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1360
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1323
    invoke-virtual {p0, p1}, Lnpy;->a(Loxn;)Lnpy;

    move-result-object v0

    return-object v0
.end method
