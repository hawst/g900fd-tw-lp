.class public final Lbrs;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private synthetic a:[Ljava/lang/String;

.field private synthetic b:[Ljava/lang/String;

.field private synthetic c:Lcom/google/android/apps/moviemaker/picker/PickerActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/moviemaker/picker/PickerActivity;[Ljava/lang/String;[Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 146
    iput-object p1, p0, Lbrs;->c:Lcom/google/android/apps/moviemaker/picker/PickerActivity;

    iput-object p2, p0, Lbrs;->a:[Ljava/lang/String;

    iput-object p3, p0, Lbrs;->b:[Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/database/Cursor;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 157
    iget-object v0, p0, Lbrs;->c:Lcom/google/android/apps/moviemaker/picker/PickerActivity;

    invoke-static {v0, p1}, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->b(Lcom/google/android/apps/moviemaker/picker/PickerActivity;Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 158
    iget-object v0, p0, Lbrs;->c:Lcom/google/android/apps/moviemaker/picker/PickerActivity;

    invoke-static {v0}, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->b(Lcom/google/android/apps/moviemaker/picker/PickerActivity;)V

    .line 159
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 150
    new-instance v0, Landroid/content/CursorLoader;

    iget-object v1, p0, Lbrs;->c:Lcom/google/android/apps/moviemaker/picker/PickerActivity;

    sget-object v2, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iget-object v3, p0, Lbrs;->a:[Ljava/lang/String;

    const-string v4, "bucket_display_name=\'Camera\'"

    iget-object v5, p0, Lbrs;->b:[Ljava/lang/String;

    const-string v6, "datetaken DESC"

    invoke-direct/range {v0 .. v6}, Landroid/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 146
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p2}, Lbrs;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/content/Loader;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 163
    iget-object v0, p0, Lbrs;->c:Lcom/google/android/apps/moviemaker/picker/PickerActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->b(Lcom/google/android/apps/moviemaker/picker/PickerActivity;Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 164
    return-void
.end method
