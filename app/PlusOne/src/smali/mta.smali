.class public final Lmta;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Integer;

.field private b:I

.field private c:Lmtb;

.field private d:Lmtb;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3141
    invoke-direct {p0}, Loxq;-><init>()V

    .line 3244
    const/high16 v0, -0x80000000

    iput v0, p0, Lmta;->b:I

    .line 3247
    iput-object v1, p0, Lmta;->c:Lmtb;

    .line 3250
    iput-object v1, p0, Lmta;->d:Lmtb;

    .line 3141
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 3273
    const/4 v0, 0x0

    .line 3274
    iget-object v1, p0, Lmta;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 3275
    const/4 v0, 0x1

    iget-object v1, p0, Lmta;->a:Ljava/lang/Integer;

    .line 3276
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3278
    :cond_0
    iget v1, p0, Lmta;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_1

    .line 3279
    const/4 v1, 0x2

    iget v2, p0, Lmta;->b:I

    .line 3280
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3282
    :cond_1
    iget-object v1, p0, Lmta;->c:Lmtb;

    if-eqz v1, :cond_2

    .line 3283
    const/4 v1, 0x3

    iget-object v2, p0, Lmta;->c:Lmtb;

    .line 3284
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3286
    :cond_2
    iget-object v1, p0, Lmta;->d:Lmtb;

    if-eqz v1, :cond_3

    .line 3287
    const/4 v1, 0x4

    iget-object v2, p0, Lmta;->d:Lmtb;

    .line 3288
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3290
    :cond_3
    iget-object v1, p0, Lmta;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3291
    iput v0, p0, Lmta;->ai:I

    .line 3292
    return v0
.end method

.method public a(Loxn;)Lmta;
    .locals 2

    .prologue
    .line 3300
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3301
    sparse-switch v0, :sswitch_data_0

    .line 3305
    iget-object v1, p0, Lmta;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 3306
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmta;->ah:Ljava/util/List;

    .line 3309
    :cond_1
    iget-object v1, p0, Lmta;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3311
    :sswitch_0
    return-object p0

    .line 3316
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmta;->a:Ljava/lang/Integer;

    goto :goto_0

    .line 3320
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 3321
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 3325
    :cond_2
    iput v0, p0, Lmta;->b:I

    goto :goto_0

    .line 3327
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lmta;->b:I

    goto :goto_0

    .line 3332
    :sswitch_3
    iget-object v0, p0, Lmta;->c:Lmtb;

    if-nez v0, :cond_4

    .line 3333
    new-instance v0, Lmtb;

    invoke-direct {v0}, Lmtb;-><init>()V

    iput-object v0, p0, Lmta;->c:Lmtb;

    .line 3335
    :cond_4
    iget-object v0, p0, Lmta;->c:Lmtb;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3339
    :sswitch_4
    iget-object v0, p0, Lmta;->d:Lmtb;

    if-nez v0, :cond_5

    .line 3340
    new-instance v0, Lmtb;

    invoke-direct {v0}, Lmtb;-><init>()V

    iput-object v0, p0, Lmta;->d:Lmtb;

    .line 3342
    :cond_5
    iget-object v0, p0, Lmta;->d:Lmtb;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3301
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 3255
    iget-object v0, p0, Lmta;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 3256
    const/4 v0, 0x1

    iget-object v1, p0, Lmta;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 3258
    :cond_0
    iget v0, p0, Lmta;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 3259
    const/4 v0, 0x2

    iget v1, p0, Lmta;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 3261
    :cond_1
    iget-object v0, p0, Lmta;->c:Lmtb;

    if-eqz v0, :cond_2

    .line 3262
    const/4 v0, 0x3

    iget-object v1, p0, Lmta;->c:Lmtb;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 3264
    :cond_2
    iget-object v0, p0, Lmta;->d:Lmtb;

    if-eqz v0, :cond_3

    .line 3265
    const/4 v0, 0x4

    iget-object v1, p0, Lmta;->d:Lmtb;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 3267
    :cond_3
    iget-object v0, p0, Lmta;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3269
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3137
    invoke-virtual {p0, p1}, Lmta;->a(Loxn;)Lmta;

    move-result-object v0

    return-object v0
.end method
