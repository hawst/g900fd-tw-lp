.class public final Loax;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Loax;


# instance fields
.field public b:I

.field public c:Ljava/lang/Long;

.field public d:[Lpax;

.field private e:Ljava/lang/Long;

.field private f:[Lpaf;

.field private g:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Loax;

    sput-object v0, Loax;->a:[Loax;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 21
    const/high16 v0, -0x80000000

    iput v0, p0, Loax;->b:I

    .line 28
    sget-object v0, Lpaf;->a:[Lpaf;

    iput-object v0, p0, Loax;->f:[Lpaf;

    .line 33
    sget-object v0, Lpax;->a:[Lpax;

    iput-object v0, p0, Loax;->d:[Lpax;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 70
    .line 71
    iget v0, p0, Loax;->b:I

    const/high16 v2, -0x80000000

    if-eq v0, v2, :cond_7

    .line 72
    const/4 v0, 0x1

    iget v2, p0, Loax;->b:I

    .line 73
    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 75
    :goto_0
    iget-object v2, p0, Loax;->e:Ljava/lang/Long;

    if-eqz v2, :cond_0

    .line 76
    const/4 v2, 0x2

    iget-object v3, p0, Loax;->e:Ljava/lang/Long;

    .line 77
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Loxo;->f(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 79
    :cond_0
    iget-object v2, p0, Loax;->c:Ljava/lang/Long;

    if-eqz v2, :cond_1

    .line 80
    const/4 v2, 0x3

    iget-object v3, p0, Loax;->c:Ljava/lang/Long;

    .line 81
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Loxo;->f(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 83
    :cond_1
    iget-object v2, p0, Loax;->f:[Lpaf;

    if-eqz v2, :cond_3

    .line 84
    iget-object v3, p0, Loax;->f:[Lpaf;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_3

    aget-object v5, v3, v2

    .line 85
    if-eqz v5, :cond_2

    .line 86
    const/4 v6, 0x4

    .line 87
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 84
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 91
    :cond_3
    iget-object v2, p0, Loax;->g:Ljava/lang/Integer;

    if-eqz v2, :cond_4

    .line 92
    const/4 v2, 0x5

    iget-object v3, p0, Loax;->g:Ljava/lang/Integer;

    .line 93
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 95
    :cond_4
    iget-object v2, p0, Loax;->d:[Lpax;

    if-eqz v2, :cond_6

    .line 96
    iget-object v2, p0, Loax;->d:[Lpax;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    .line 97
    if-eqz v4, :cond_5

    .line 98
    const/4 v5, 0x6

    .line 99
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 96
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 103
    :cond_6
    iget-object v1, p0, Loax;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 104
    iput v0, p0, Loax;->ai:I

    .line 105
    return v0

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Loax;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 113
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 114
    sparse-switch v0, :sswitch_data_0

    .line 118
    iget-object v2, p0, Loax;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 119
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Loax;->ah:Ljava/util/List;

    .line 122
    :cond_1
    iget-object v2, p0, Loax;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 124
    :sswitch_0
    return-object p0

    .line 129
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 130
    if-eq v0, v4, :cond_2

    const/4 v2, 0x2

    if-eq v0, v2, :cond_2

    const/4 v2, 0x3

    if-eq v0, v2, :cond_2

    const/4 v2, 0x4

    if-eq v0, v2, :cond_2

    const/4 v2, 0x5

    if-ne v0, v2, :cond_3

    .line 135
    :cond_2
    iput v0, p0, Loax;->b:I

    goto :goto_0

    .line 137
    :cond_3
    iput v4, p0, Loax;->b:I

    goto :goto_0

    .line 142
    :sswitch_2
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Loax;->e:Ljava/lang/Long;

    goto :goto_0

    .line 146
    :sswitch_3
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Loax;->c:Ljava/lang/Long;

    goto :goto_0

    .line 150
    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 151
    iget-object v0, p0, Loax;->f:[Lpaf;

    if-nez v0, :cond_5

    move v0, v1

    .line 152
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lpaf;

    .line 153
    iget-object v3, p0, Loax;->f:[Lpaf;

    if-eqz v3, :cond_4

    .line 154
    iget-object v3, p0, Loax;->f:[Lpaf;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 156
    :cond_4
    iput-object v2, p0, Loax;->f:[Lpaf;

    .line 157
    :goto_2
    iget-object v2, p0, Loax;->f:[Lpaf;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 158
    iget-object v2, p0, Loax;->f:[Lpaf;

    new-instance v3, Lpaf;

    invoke-direct {v3}, Lpaf;-><init>()V

    aput-object v3, v2, v0

    .line 159
    iget-object v2, p0, Loax;->f:[Lpaf;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 160
    invoke-virtual {p1}, Loxn;->a()I

    .line 157
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 151
    :cond_5
    iget-object v0, p0, Loax;->f:[Lpaf;

    array-length v0, v0

    goto :goto_1

    .line 163
    :cond_6
    iget-object v2, p0, Loax;->f:[Lpaf;

    new-instance v3, Lpaf;

    invoke-direct {v3}, Lpaf;-><init>()V

    aput-object v3, v2, v0

    .line 164
    iget-object v2, p0, Loax;->f:[Lpaf;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 168
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Loax;->g:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 172
    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 173
    iget-object v0, p0, Loax;->d:[Lpax;

    if-nez v0, :cond_8

    move v0, v1

    .line 174
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lpax;

    .line 175
    iget-object v3, p0, Loax;->d:[Lpax;

    if-eqz v3, :cond_7

    .line 176
    iget-object v3, p0, Loax;->d:[Lpax;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 178
    :cond_7
    iput-object v2, p0, Loax;->d:[Lpax;

    .line 179
    :goto_4
    iget-object v2, p0, Loax;->d:[Lpax;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    .line 180
    iget-object v2, p0, Loax;->d:[Lpax;

    new-instance v3, Lpax;

    invoke-direct {v3}, Lpax;-><init>()V

    aput-object v3, v2, v0

    .line 181
    iget-object v2, p0, Loax;->d:[Lpax;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 182
    invoke-virtual {p1}, Loxn;->a()I

    .line 179
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 173
    :cond_8
    iget-object v0, p0, Loax;->d:[Lpax;

    array-length v0, v0

    goto :goto_3

    .line 185
    :cond_9
    iget-object v2, p0, Loax;->d:[Lpax;

    new-instance v3, Lpax;

    invoke-direct {v3}, Lpax;-><init>()V

    aput-object v3, v2, v0

    .line 186
    iget-object v2, p0, Loax;->d:[Lpax;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 114
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 38
    iget v1, p0, Loax;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 39
    const/4 v1, 0x1

    iget v2, p0, Loax;->b:I

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 41
    :cond_0
    iget-object v1, p0, Loax;->e:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 42
    const/4 v1, 0x2

    iget-object v2, p0, Loax;->e:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Loxo;->b(IJ)V

    .line 44
    :cond_1
    iget-object v1, p0, Loax;->c:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 45
    const/4 v1, 0x3

    iget-object v2, p0, Loax;->c:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Loxo;->b(IJ)V

    .line 47
    :cond_2
    iget-object v1, p0, Loax;->f:[Lpaf;

    if-eqz v1, :cond_4

    .line 48
    iget-object v2, p0, Loax;->f:[Lpaf;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 49
    if-eqz v4, :cond_3

    .line 50
    const/4 v5, 0x4

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 48
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 54
    :cond_4
    iget-object v1, p0, Loax;->g:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 55
    const/4 v1, 0x5

    iget-object v2, p0, Loax;->g:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 57
    :cond_5
    iget-object v1, p0, Loax;->d:[Lpax;

    if-eqz v1, :cond_7

    .line 58
    iget-object v1, p0, Loax;->d:[Lpax;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_7

    aget-object v3, v1, v0

    .line 59
    if-eqz v3, :cond_6

    .line 60
    const/4 v4, 0x6

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 58
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 64
    :cond_7
    iget-object v0, p0, Loax;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 66
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Loax;->a(Loxn;)Loax;

    move-result-object v0

    return-object v0
.end method
