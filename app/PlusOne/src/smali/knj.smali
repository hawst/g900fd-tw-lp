.class public final Lknj;
.super Lkgg;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkgg",
        "<",
        "Lmee;",
        "Lmef;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lkng;

.field private c:Lkmz;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Lkng;Lkey;)V
    .locals 6

    .prologue
    .line 41
    new-instance v2, Lkfo;

    invoke-direct {v2, p1, p2, p5}, Lkfo;-><init>(Landroid/content/Context;ILkey;)V

    const-string v3, "linkpreview"

    new-instance v4, Lmee;

    invoke-direct {v4}, Lmee;-><init>()V

    new-instance v5, Lmef;

    invoke-direct {v5}, Lmef;-><init>()V

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lkgg;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Loxu;Loxu;)V

    .line 43
    iput-object p3, p0, Lknj;->a:Ljava/lang/String;

    .line 44
    iput-object p4, p0, Lknj;->b:Lkng;

    .line 45
    return-void
.end method


# virtual methods
.method protected a(Lmee;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 56
    new-instance v0, Lode;

    invoke-direct {v0}, Lode;-><init>()V

    iput-object v0, p1, Lmee;->a:Lode;

    .line 57
    iget-object v0, p1, Lmee;->a:Lode;

    .line 59
    iget-object v1, p0, Lknj;->a:Ljava/lang/String;

    iput-object v1, v0, Lode;->a:Ljava/lang/String;

    .line 60
    iget-object v1, p0, Lknj;->b:Lkng;

    if-eqz v1, :cond_0

    .line 61
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lode;->f:Ljava/lang/Boolean;

    .line 62
    iget-object v1, p0, Lknj;->b:Lkng;

    iget-object v1, v1, Lkng;->a:Ljava/lang/String;

    iput-object v1, v0, Lode;->h:Ljava/lang/String;

    .line 63
    iget-object v1, p0, Lknj;->b:Lkng;

    iget-object v1, v1, Lkng;->b:Ljava/lang/String;

    iput-object v1, v0, Lode;->d:Ljava/lang/String;

    .line 64
    iget-object v1, p0, Lknj;->b:Lkng;

    iget-object v1, v1, Lkng;->c:Ljava/lang/String;

    iput-object v1, v0, Lode;->e:Ljava/lang/String;

    .line 66
    :cond_0
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lode;->c:Ljava/lang/Boolean;

    .line 67
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lode;->b:Ljava/lang/Boolean;

    .line 69
    new-instance v1, Loxz;

    invoke-direct {v1}, Loxz;-><init>()V

    iput-object v1, v0, Lode;->g:Loxz;

    .line 70
    iget-object v3, v0, Lode;->g:Loxz;

    new-instance v4, Ljava/util/ArrayList;

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Integer;

    const/16 v1, 0x16d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    const/16 v1, 0x151

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v6

    const/4 v1, 0x2

    const/16 v5, 0x162

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v0, v1

    const/4 v1, 0x3

    const/16 v5, 0x18f

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v0, v1

    const/4 v1, 0x4

    const/16 v5, 0x189

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v0, v1

    const/4 v1, 0x5

    const/16 v5, 0x14f

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iget-object v1, p0, Lknj;->f:Landroid/content/Context;

    const-class v0, Lhei;

    invoke-static {v1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    new-array v5, v6, [Ljava/lang/String;

    const-string v6, "logged_in"

    aput-object v6, v5, v2

    invoke-interface {v0, v5}, Lhei;->a([Ljava/lang/String;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    const/16 v0, 0x187

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    new-array v5, v0, [I

    move v1, v2

    :goto_1
    array-length v0, v5

    if-ge v1, v0, :cond_3

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v5, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_2
    const-class v0, Lieh;

    invoke-static {v1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    sget-object v6, Lkni;->a:Lief;

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v6, v1}, Lieh;->b(Lief;I)Z

    move-result v0

    goto :goto_0

    :cond_3
    iput-object v5, v3, Loxz;->a:[I

    .line 71
    return-void
.end method

.method protected a(Lmef;)V
    .locals 1

    .prologue
    .line 49
    iget-object v0, p1, Lmef;->a:Locz;

    if-eqz v0, :cond_0

    .line 50
    iget-object v0, p1, Lmef;->a:Locz;

    invoke-static {v0}, Lknb;->a(Locz;)Lkmz;

    move-result-object v0

    iput-object v0, p0, Lknj;->c:Lkmz;

    .line 52
    :cond_0
    return-void
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 26
    check-cast p1, Lmee;

    invoke-virtual {p0, p1}, Lknj;->a(Lmee;)V

    return-void
.end method

.method protected synthetic a_(Loxu;)V
    .locals 0

    .prologue
    .line 26
    check-cast p1, Lmef;

    invoke-virtual {p0, p1}, Lknj;->a(Lmef;)V

    return-void
.end method

.method public i()Lkmz;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lknj;->c:Lkmz;

    return-object v0
.end method
