.class public final Licx;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static a:Z

.field private static final b:Lloy;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 27
    const/4 v0, 0x0

    sput-boolean v0, Licx;->a:Z

    .line 28
    new-instance v0, Lloy;

    const-string v1, "use.bazaar"

    invoke-direct {v0, v1}, Lloy;-><init>(Ljava/lang/String;)V

    sput-object v0, Licx;->b:Lloy;

    return-void
.end method

.method static synthetic a()Lloy;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Licx;->b:Lloy;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lae;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const v3, 0x7f0a040f

    const/4 v5, 0x1

    .line 66
    new-instance v1, Licw;

    invoke-direct {v1, p0}, Licw;-><init>(Landroid/content/Context;)V

    .line 67
    invoke-virtual {v1}, Licw;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    sget-boolean v1, Licx;->a:Z

    if-nez v1, :cond_1

    .line 68
    new-instance v1, Licw;

    invoke-direct {v1, p0}, Licw;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Licw;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    const v1, 0x7f0a040b

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0a040c

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3, v0}, Licz;->a(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)Licz;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Llgr;->b(Z)V

    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    new-instance v1, Licy;

    invoke-direct {v1, p1}, Licy;-><init>(Lae;)V

    invoke-virtual {v0, v1}, Llgr;->a(Llgs;)V

    :try_start_0
    const-string v1, "app_upgrade"

    invoke-virtual {v0, p1, v1}, Llgr;->a(Lae;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    sput-boolean v5, Licx;->a:Z

    .line 70
    :cond_1
    return-void

    .line 68
    :cond_2
    invoke-virtual {v1}, Licw;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    const v0, 0x7f0a040d

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v2, 0x7f0a040e

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0a0410

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v2, v3, v4}, Licz;->a(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)Licz;

    move-result-object v0

    invoke-virtual {v0, v5}, Llgr;->b(Z)V

    invoke-virtual {v1}, Licw;->h()V

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x29

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "AlertFragmentDialog.show threw exception "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method static synthetic a(Z)Z
    .locals 0

    .prologue
    .line 19
    sput-boolean p0, Licx;->a:Z

    return p0
.end method
