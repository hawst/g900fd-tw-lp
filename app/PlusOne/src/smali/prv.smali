.class public final Lprv;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:[Lpru;

.field private c:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 15
    sget-object v0, Lpru;->a:[Lpru;

    iput-object v0, p0, Lprv;->b:[Lpru;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 41
    .line 42
    iget-object v0, p0, Lprv;->a:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 43
    const/4 v0, 0x1

    iget-object v2, p0, Lprv;->a:Ljava/lang/String;

    .line 44
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 46
    :goto_0
    iget-object v2, p0, Lprv;->b:[Lpru;

    if-eqz v2, :cond_1

    .line 47
    iget-object v2, p0, Lprv;->b:[Lpru;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 48
    if-eqz v4, :cond_0

    .line 49
    const/4 v5, 0x2

    .line 50
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 47
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 54
    :cond_1
    iget-object v1, p0, Lprv;->c:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 55
    const/4 v1, 0x3

    iget-object v2, p0, Lprv;->c:Ljava/lang/Long;

    .line 56
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 58
    :cond_2
    iget-object v1, p0, Lprv;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 59
    iput v0, p0, Lprv;->ai:I

    .line 60
    return v0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lprv;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 68
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 69
    sparse-switch v0, :sswitch_data_0

    .line 73
    iget-object v2, p0, Lprv;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 74
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lprv;->ah:Ljava/util/List;

    .line 77
    :cond_1
    iget-object v2, p0, Lprv;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 79
    :sswitch_0
    return-object p0

    .line 84
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lprv;->a:Ljava/lang/String;

    goto :goto_0

    .line 88
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 89
    iget-object v0, p0, Lprv;->b:[Lpru;

    if-nez v0, :cond_3

    move v0, v1

    .line 90
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lpru;

    .line 91
    iget-object v3, p0, Lprv;->b:[Lpru;

    if-eqz v3, :cond_2

    .line 92
    iget-object v3, p0, Lprv;->b:[Lpru;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 94
    :cond_2
    iput-object v2, p0, Lprv;->b:[Lpru;

    .line 95
    :goto_2
    iget-object v2, p0, Lprv;->b:[Lpru;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 96
    iget-object v2, p0, Lprv;->b:[Lpru;

    new-instance v3, Lpru;

    invoke-direct {v3}, Lpru;-><init>()V

    aput-object v3, v2, v0

    .line 97
    iget-object v2, p0, Lprv;->b:[Lpru;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 98
    invoke-virtual {p1}, Loxn;->a()I

    .line 95
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 89
    :cond_3
    iget-object v0, p0, Lprv;->b:[Lpru;

    array-length v0, v0

    goto :goto_1

    .line 101
    :cond_4
    iget-object v2, p0, Lprv;->b:[Lpru;

    new-instance v3, Lpru;

    invoke-direct {v3}, Lpru;-><init>()V

    aput-object v3, v2, v0

    .line 102
    iget-object v2, p0, Lprv;->b:[Lpru;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 106
    :sswitch_3
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lprv;->c:Ljava/lang/Long;

    goto :goto_0

    .line 69
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 22
    iget-object v0, p0, Lprv;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 23
    const/4 v0, 0x1

    iget-object v1, p0, Lprv;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 25
    :cond_0
    iget-object v0, p0, Lprv;->b:[Lpru;

    if-eqz v0, :cond_2

    .line 26
    iget-object v1, p0, Lprv;->b:[Lpru;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 27
    if-eqz v3, :cond_1

    .line 28
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 26
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 32
    :cond_2
    iget-object v0, p0, Lprv;->c:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 33
    const/4 v0, 0x3

    iget-object v1, p0, Lprv;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 35
    :cond_3
    iget-object v0, p0, Lprv;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 37
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lprv;->a(Loxn;)Lprv;

    move-result-object v0

    return-object v0
.end method
