.class public final Lpdi;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field private b:Ljava/lang/Integer;

.field private c:Ljava/lang/Integer;

.field private d:Ljava/lang/Boolean;

.field private e:Ljava/lang/Boolean;

.field private f:Ljava/lang/Boolean;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/Integer;

.field private i:Lpcp;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Loxq;-><init>()V

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lpdi;->i:Lpcp;

    .line 9
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 66
    const/4 v0, 0x0

    .line 67
    iget-object v1, p0, Lpdi;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 68
    const/4 v0, 0x1

    iget-object v1, p0, Lpdi;->a:Ljava/lang/String;

    .line 69
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 71
    :cond_0
    iget-object v1, p0, Lpdi;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 72
    const/4 v1, 0x2

    iget-object v2, p0, Lpdi;->b:Ljava/lang/Integer;

    .line 73
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 75
    :cond_1
    iget-object v1, p0, Lpdi;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 76
    const/4 v1, 0x3

    iget-object v2, p0, Lpdi;->c:Ljava/lang/Integer;

    .line 77
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 79
    :cond_2
    iget-object v1, p0, Lpdi;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 80
    const/4 v1, 0x4

    iget-object v2, p0, Lpdi;->d:Ljava/lang/Boolean;

    .line 81
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 83
    :cond_3
    iget-object v1, p0, Lpdi;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 84
    const/4 v1, 0x5

    iget-object v2, p0, Lpdi;->e:Ljava/lang/Boolean;

    .line 85
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 87
    :cond_4
    iget-object v1, p0, Lpdi;->f:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    .line 88
    const/4 v1, 0x6

    iget-object v2, p0, Lpdi;->f:Ljava/lang/Boolean;

    .line 89
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 91
    :cond_5
    iget-object v1, p0, Lpdi;->g:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 92
    const/4 v1, 0x7

    iget-object v2, p0, Lpdi;->g:Ljava/lang/String;

    .line 93
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 95
    :cond_6
    iget-object v1, p0, Lpdi;->h:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    .line 96
    const/16 v1, 0x8

    iget-object v2, p0, Lpdi;->h:Ljava/lang/Integer;

    .line 97
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 99
    :cond_7
    iget-object v1, p0, Lpdi;->i:Lpcp;

    if-eqz v1, :cond_8

    .line 100
    const/16 v1, 0x9

    iget-object v2, p0, Lpdi;->i:Lpcp;

    .line 101
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 103
    :cond_8
    iget-object v1, p0, Lpdi;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 104
    iput v0, p0, Lpdi;->ai:I

    .line 105
    return v0
.end method

.method public a(Loxn;)Lpdi;
    .locals 2

    .prologue
    .line 113
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 114
    sparse-switch v0, :sswitch_data_0

    .line 118
    iget-object v1, p0, Lpdi;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 119
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpdi;->ah:Ljava/util/List;

    .line 122
    :cond_1
    iget-object v1, p0, Lpdi;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 124
    :sswitch_0
    return-object p0

    .line 129
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpdi;->a:Ljava/lang/String;

    goto :goto_0

    .line 133
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lpdi;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 137
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lpdi;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 141
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lpdi;->d:Ljava/lang/Boolean;

    goto :goto_0

    .line 145
    :sswitch_5
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lpdi;->e:Ljava/lang/Boolean;

    goto :goto_0

    .line 149
    :sswitch_6
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lpdi;->f:Ljava/lang/Boolean;

    goto :goto_0

    .line 153
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpdi;->g:Ljava/lang/String;

    goto :goto_0

    .line 157
    :sswitch_8
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lpdi;->h:Ljava/lang/Integer;

    goto :goto_0

    .line 161
    :sswitch_9
    iget-object v0, p0, Lpdi;->i:Lpcp;

    if-nez v0, :cond_2

    .line 162
    new-instance v0, Lpcp;

    invoke-direct {v0}, Lpcp;-><init>()V

    iput-object v0, p0, Lpdi;->i:Lpcp;

    .line 164
    :cond_2
    iget-object v0, p0, Lpdi;->i:Lpcp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 114
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 33
    iget-object v0, p0, Lpdi;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 34
    const/4 v0, 0x1

    iget-object v1, p0, Lpdi;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 36
    :cond_0
    iget-object v0, p0, Lpdi;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 37
    const/4 v0, 0x2

    iget-object v1, p0, Lpdi;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 39
    :cond_1
    iget-object v0, p0, Lpdi;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 40
    const/4 v0, 0x3

    iget-object v1, p0, Lpdi;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 42
    :cond_2
    iget-object v0, p0, Lpdi;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 43
    const/4 v0, 0x4

    iget-object v1, p0, Lpdi;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 45
    :cond_3
    iget-object v0, p0, Lpdi;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 46
    const/4 v0, 0x5

    iget-object v1, p0, Lpdi;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 48
    :cond_4
    iget-object v0, p0, Lpdi;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 49
    const/4 v0, 0x6

    iget-object v1, p0, Lpdi;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 51
    :cond_5
    iget-object v0, p0, Lpdi;->g:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 52
    const/4 v0, 0x7

    iget-object v1, p0, Lpdi;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 54
    :cond_6
    iget-object v0, p0, Lpdi;->h:Ljava/lang/Integer;

    if-eqz v0, :cond_7

    .line 55
    const/16 v0, 0x8

    iget-object v1, p0, Lpdi;->h:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 57
    :cond_7
    iget-object v0, p0, Lpdi;->i:Lpcp;

    if-eqz v0, :cond_8

    .line 58
    const/16 v0, 0x9

    iget-object v1, p0, Lpdi;->i:Lpcp;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 60
    :cond_8
    iget-object v0, p0, Lpdi;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 62
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Lpdi;->a(Loxn;)Lpdi;

    move-result-object v0

    return-object v0
.end method
