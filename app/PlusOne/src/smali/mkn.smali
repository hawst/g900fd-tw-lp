.class public final Lmkn;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lmzj;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2104
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2107
    iput-object v0, p0, Lmkn;->apiHeader:Llyr;

    .line 2110
    iput-object v0, p0, Lmkn;->a:Lmzj;

    .line 2104
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 2127
    const/4 v0, 0x0

    .line 2128
    iget-object v1, p0, Lmkn;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 2129
    const/4 v0, 0x1

    iget-object v1, p0, Lmkn;->apiHeader:Llyr;

    .line 2130
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2132
    :cond_0
    iget-object v1, p0, Lmkn;->a:Lmzj;

    if-eqz v1, :cond_1

    .line 2133
    const/4 v1, 0x2

    iget-object v2, p0, Lmkn;->a:Lmzj;

    .line 2134
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2136
    :cond_1
    iget-object v1, p0, Lmkn;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2137
    iput v0, p0, Lmkn;->ai:I

    .line 2138
    return v0
.end method

.method public a(Loxn;)Lmkn;
    .locals 2

    .prologue
    .line 2146
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2147
    sparse-switch v0, :sswitch_data_0

    .line 2151
    iget-object v1, p0, Lmkn;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 2152
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmkn;->ah:Ljava/util/List;

    .line 2155
    :cond_1
    iget-object v1, p0, Lmkn;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2157
    :sswitch_0
    return-object p0

    .line 2162
    :sswitch_1
    iget-object v0, p0, Lmkn;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 2163
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmkn;->apiHeader:Llyr;

    .line 2165
    :cond_2
    iget-object v0, p0, Lmkn;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2169
    :sswitch_2
    iget-object v0, p0, Lmkn;->a:Lmzj;

    if-nez v0, :cond_3

    .line 2170
    new-instance v0, Lmzj;

    invoke-direct {v0}, Lmzj;-><init>()V

    iput-object v0, p0, Lmkn;->a:Lmzj;

    .line 2172
    :cond_3
    iget-object v0, p0, Lmkn;->a:Lmzj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2147
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 2115
    iget-object v0, p0, Lmkn;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 2116
    const/4 v0, 0x1

    iget-object v1, p0, Lmkn;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2118
    :cond_0
    iget-object v0, p0, Lmkn;->a:Lmzj;

    if-eqz v0, :cond_1

    .line 2119
    const/4 v0, 0x2

    iget-object v1, p0, Lmkn;->a:Lmzj;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2121
    :cond_1
    iget-object v0, p0, Lmkn;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2123
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2100
    invoke-virtual {p0, p1}, Lmkn;->a(Loxn;)Lmkn;

    move-result-object v0

    return-object v0
.end method
