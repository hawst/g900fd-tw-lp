.class final Lfov;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lbc",
        "<",
        "Lfob;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lfos;


# direct methods
.method constructor <init>(Lfos;)V
    .locals 0

    .prologue
    .line 328
    iput-object p1, p0, Lfov;->a:Lfos;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Lfob;",
            ">;"
        }
    .end annotation

    .prologue
    .line 332
    new-instance v0, Lfqd;

    iget-object v1, p0, Lfov;->a:Lfos;

    invoke-virtual {v1}, Lfos;->n()Lz;

    move-result-object v1

    iget-object v2, p0, Lfov;->a:Lfos;

    invoke-static {v2}, Lfos;->c(Lfos;)Lhee;

    move-result-object v2

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    iget-object v3, p0, Lfov;->a:Lfos;

    invoke-static {v3}, Lfos;->d(Lfos;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lfov;->a:Lfos;

    .line 333
    invoke-virtual {v4}, Lfos;->k()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "auth_key"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lfov;->a:Lfos;

    .line 334
    invoke-virtual {v5}, Lfos;->k()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "gpinv"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    const/4 v7, 0x0

    iget-object v8, p0, Lfov;->a:Lfos;

    .line 335
    invoke-static {v8}, Lfos;->e(Lfos;)Landroid/graphics/Point;

    move-result-object v8

    invoke-static {v8}, Lfss;->a(Landroid/graphics/Point;)[Landroid/graphics/Point;

    move-result-object v8

    invoke-direct/range {v0 .. v8}, Lfqd;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ[Landroid/graphics/Point;)V

    return-object v0
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Lfob;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 406
    return-void
.end method

.method public a(Ldo;Lfob;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Lfob;",
            ">;",
            "Lfob;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x2

    const/4 v5, 0x0

    const/4 v2, 0x0

    const/4 v4, 0x1

    .line 340
    iget-object v0, p2, Lfob;->a:Lmmq;

    if-eqz v0, :cond_7

    .line 341
    iget-object v0, p0, Lfov;->a:Lfos;

    .line 342
    invoke-static {v0}, Lfos;->f(Lfos;)Lcom/google/android/apps/plus/stories/views/StoryLayout;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lfov;->a:Lfos;

    invoke-static {v0}, Lfos;->f(Lfos;)Lcom/google/android/apps/plus/stories/views/StoryLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    .line 343
    :goto_0
    iget-object v0, p0, Lfov;->a:Lfos;

    iget-object v3, p2, Lfob;->a:Lmmq;

    invoke-static {v0, v3}, Lfos;->a(Lfos;Lmmq;)Lmmq;

    .line 344
    iget-object v0, p0, Lfov;->a:Lfos;

    invoke-static {v0}, Lfos;->g(Lfos;)Lfqu;

    move-result-object v0

    invoke-virtual {v0, p2}, Lfqu;->a(Lfob;)V

    .line 345
    if-eqz v1, :cond_0

    move-object v0, v1

    check-cast v0, Lfti;

    .line 346
    invoke-virtual {v0}, Lfti;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 347
    iget-object v0, p0, Lfov;->a:Lfos;

    invoke-static {v0}, Lfos;->f(Lfos;)Lcom/google/android/apps/plus/stories/views/StoryLayout;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 352
    :cond_0
    iget-object v0, p0, Lfov;->a:Lfos;

    invoke-static {v0, v4}, Lfos;->a(Lfos;Z)V

    .line 353
    iget-object v0, p0, Lfov;->a:Lfos;

    invoke-virtual {v0}, Lfos;->U()V

    .line 354
    iget-object v0, p0, Lfov;->a:Lfos;

    invoke-static {v0}, Lfos;->h(Lfos;)V

    .line 355
    iget-object v0, p0, Lfov;->a:Lfos;

    invoke-static {v0}, Lfos;->i(Lfos;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lfov;->a:Lfos;

    invoke-static {v0}, Lfos;->j(Lfos;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 356
    iget-object v0, p0, Lfov;->a:Lfos;

    invoke-virtual {v0, v4}, Lfos;->a(Z)V

    .line 358
    :cond_1
    iget-object v0, p0, Lfov;->a:Lfos;

    invoke-static {v0}, Lfos;->k(Lfos;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lfov;->a:Lfos;

    invoke-static {v0}, Lfos;->l(Lfos;)[Lmmr;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 360
    iget-object v0, p0, Lfov;->a:Lfos;

    iget-object v1, p0, Lfov;->a:Lfos;

    invoke-static {v1}, Lfos;->l(Lfos;)[Lmmr;

    move-result-object v1

    invoke-static {v0, v1}, Lfos;->a(Lfos;[Lmmr;)V

    .line 361
    iget-object v0, p0, Lfov;->a:Lfos;

    invoke-static {v0, v5}, Lfos;->b(Lfos;Z)Z

    .line 363
    :cond_2
    iget-object v0, p0, Lfov;->a:Lfos;

    invoke-static {v0}, Lfos;->m(Lfos;)Lmmq;

    move-result-object v0

    iget-object v0, v0, Lmmq;->g:Lmlz;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lfov;->a:Lfos;

    invoke-static {v0}, Lfos;->m(Lfos;)Lmmq;

    move-result-object v0

    iget-object v0, v0, Lmmq;->g:Lmlz;

    iget-object v0, v0, Lmlz;->b:Logr;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lfov;->a:Lfos;

    .line 364
    invoke-static {v0}, Lfos;->m(Lfos;)Lmmq;

    move-result-object v0

    iget-object v0, v0, Lmmq;->g:Lmlz;

    iget-object v0, v0, Lmlz;->a:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 365
    iget-object v0, p0, Lfov;->a:Lfos;

    invoke-virtual {v0}, Lfos;->w()Lbb;

    move-result-object v0

    iget-object v1, p0, Lfov;->a:Lfos;

    invoke-static {v1}, Lfos;->n(Lfos;)Lbc;

    move-result-object v1

    invoke-virtual {v0, v4, v2, v1}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 367
    :cond_3
    iget-object v0, p0, Lfov;->a:Lfos;

    invoke-static {v0}, Lfos;->s(Lfos;)Lhoc;

    move-result-object v0

    new-instance v1, Lfow;

    iget-object v2, p0, Lfov;->a:Lfos;

    invoke-static {v2}, Lfos;->o(Lfos;)Llnl;

    move-result-object v2

    const-string v3, "CheckStoriesPromo"

    invoke-direct {v1, p0, v2, v3}, Lfow;-><init>(Lfov;Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lhoc;->b(Lhny;)V

    .line 378
    iget-object v0, p0, Lfov;->a:Lfos;

    invoke-static {v0}, Lfos;->t(Lfos;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lfov;->a:Lfos;

    invoke-static {v0}, Lfos;->u(Lfos;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-boolean v0, p2, Lfob;->d:Z

    if-eqz v0, :cond_4

    .line 380
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p2, Lfob;->c:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x36ee80

    cmp-long v0, v0, v2

    if-lez v0, :cond_5

    .line 382
    :cond_4
    iget-object v0, p0, Lfov;->a:Lfos;

    invoke-static {v0, v4}, Lfos;->d(Lfos;Z)Z

    .line 383
    iget-object v0, p0, Lfov;->a:Lfos;

    invoke-static {v0}, Lfos;->v(Lfos;)V

    .line 401
    :cond_5
    :goto_1
    iget-object v0, p0, Lfov;->a:Lfos;

    invoke-static {v0}, Lfos;->x(Lfos;)Lhje;

    move-result-object v0

    invoke-virtual {v0}, Lhje;->a()V

    .line 402
    return-void

    :cond_6
    move-object v1, v2

    .line 342
    goto/16 :goto_0

    .line 385
    :cond_7
    iget v0, p2, Lfob;->e:I

    if-ne v0, v1, :cond_8

    .line 386
    iget-object v0, p0, Lfov;->a:Lfos;

    invoke-static {v0, v4}, Lfos;->a(Lfos;I)I

    .line 387
    iget-object v0, p0, Lfov;->a:Lfos;

    invoke-static {v0}, Lfos;->e(Lfos;)Landroid/graphics/Point;

    move-result-object v0

    invoke-static {v0}, Lfss;->a(Landroid/graphics/Point;)[Landroid/graphics/Point;

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_5

    .line 388
    invoke-virtual {p1}, Ldo;->s()V

    goto :goto_1

    .line 391
    :cond_8
    iget-object v0, p0, Lfov;->a:Lfos;

    invoke-static {v0, v1}, Lfos;->a(Lfos;I)I

    .line 392
    iget-object v0, p0, Lfov;->a:Lfos;

    invoke-static {v0}, Lfos;->k(Lfos;)Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lfov;->a:Lfos;

    invoke-static {v0}, Lfos;->l(Lfos;)[Lmmr;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 395
    iget-object v0, p0, Lfov;->a:Lfos;

    invoke-static {v0, v2}, Lfos;->b(Lfos;[Lmmr;)[Lmmr;

    .line 396
    iget-object v0, p0, Lfov;->a:Lfos;

    invoke-static {v0, v5}, Lfos;->b(Lfos;Z)Z

    .line 397
    iget-object v0, p0, Lfov;->a:Lfos;

    invoke-static {v0}, Lfos;->w(Lfos;)V

    .line 399
    :cond_9
    iget-object v0, p0, Lfov;->a:Lfos;

    iget-object v1, p0, Lfov;->a:Lfos;

    const v2, 0x7f0a01be

    invoke-virtual {v1, v2}, Lfos;->e_(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lfos;->a(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 328
    check-cast p2, Lfob;

    invoke-virtual {p0, p1, p2}, Lfov;->a(Ldo;Lfob;)V

    return-void
.end method
