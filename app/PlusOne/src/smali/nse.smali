.class public final Lnse;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lnsd;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 365
    invoke-direct {p0}, Loxq;-><init>()V

    .line 368
    sget-object v0, Lnsd;->a:[Lnsd;

    iput-object v0, p0, Lnse;->a:[Lnsd;

    .line 365
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 386
    .line 387
    iget-object v1, p0, Lnse;->a:[Lnsd;

    if-eqz v1, :cond_1

    .line 388
    iget-object v2, p0, Lnse;->a:[Lnsd;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 389
    if-eqz v4, :cond_0

    .line 390
    const/16 v5, 0xb

    .line 391
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 388
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 395
    :cond_1
    iget-object v1, p0, Lnse;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 396
    iput v0, p0, Lnse;->ai:I

    .line 397
    return v0
.end method

.method public a(Loxn;)Lnse;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 405
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 406
    sparse-switch v0, :sswitch_data_0

    .line 410
    iget-object v2, p0, Lnse;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 411
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnse;->ah:Ljava/util/List;

    .line 414
    :cond_1
    iget-object v2, p0, Lnse;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 416
    :sswitch_0
    return-object p0

    .line 421
    :sswitch_1
    const/16 v0, 0x5a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 422
    iget-object v0, p0, Lnse;->a:[Lnsd;

    if-nez v0, :cond_3

    move v0, v1

    .line 423
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnsd;

    .line 424
    iget-object v3, p0, Lnse;->a:[Lnsd;

    if-eqz v3, :cond_2

    .line 425
    iget-object v3, p0, Lnse;->a:[Lnsd;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 427
    :cond_2
    iput-object v2, p0, Lnse;->a:[Lnsd;

    .line 428
    :goto_2
    iget-object v2, p0, Lnse;->a:[Lnsd;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 429
    iget-object v2, p0, Lnse;->a:[Lnsd;

    new-instance v3, Lnsd;

    invoke-direct {v3}, Lnsd;-><init>()V

    aput-object v3, v2, v0

    .line 430
    iget-object v2, p0, Lnse;->a:[Lnsd;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 431
    invoke-virtual {p1}, Loxn;->a()I

    .line 428
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 422
    :cond_3
    iget-object v0, p0, Lnse;->a:[Lnsd;

    array-length v0, v0

    goto :goto_1

    .line 434
    :cond_4
    iget-object v2, p0, Lnse;->a:[Lnsd;

    new-instance v3, Lnsd;

    invoke-direct {v3}, Lnsd;-><init>()V

    aput-object v3, v2, v0

    .line 435
    iget-object v2, p0, Lnse;->a:[Lnsd;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 406
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x5a -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 373
    iget-object v0, p0, Lnse;->a:[Lnsd;

    if-eqz v0, :cond_1

    .line 374
    iget-object v1, p0, Lnse;->a:[Lnsd;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 375
    if-eqz v3, :cond_0

    .line 376
    const/16 v4, 0xb

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 374
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 380
    :cond_1
    iget-object v0, p0, Lnse;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 382
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 361
    invoke-virtual {p0, p1}, Lnse;->a(Loxn;)Lnse;

    move-result-object v0

    return-object v0
.end method
