.class public Lhgw;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lhgw;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:I

.field private final b:[Ljqs;

.field private final c:[Lhxc;

.field private d:[Lkxr;

.field private e:[Lhxs;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 537
    new-instance v0, Lhgx;

    invoke-direct {v0}, Lhgx;-><init>()V

    sput-object v0, Lhgw;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 234
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-array v0, v1, [Lkxr;

    iput-object v0, p0, Lhgw;->d:[Lkxr;

    .line 41
    new-array v0, v1, [Lhxs;

    iput-object v0, p0, Lhgw;->e:[Lhxs;

    .line 235
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 236
    new-array v0, v0, [Ljqs;

    iput-object v0, p0, Lhgw;->b:[Ljqs;

    .line 237
    iget-object v0, p0, Lhgw;->b:[Ljqs;

    sget-object v1, Ljqs;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readTypedArray([Ljava/lang/Object;Landroid/os/Parcelable$Creator;)V

    .line 239
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 240
    new-array v0, v0, [Lhxc;

    iput-object v0, p0, Lhgw;->c:[Lhxc;

    .line 241
    iget-object v0, p0, Lhgw;->c:[Lhxc;

    sget-object v1, Lhxc;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readTypedArray([Ljava/lang/Object;Landroid/os/Parcelable$Creator;)V

    .line 243
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 244
    new-array v0, v0, [Lkxr;

    iput-object v0, p0, Lhgw;->d:[Lkxr;

    .line 245
    iget-object v0, p0, Lhgw;->d:[Lkxr;

    sget-object v1, Lkxr;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readTypedArray([Ljava/lang/Object;Landroid/os/Parcelable$Creator;)V

    .line 247
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 248
    new-array v0, v0, [Lhxs;

    iput-object v0, p0, Lhgw;->e:[Lhxs;

    .line 249
    iget-object v0, p0, Lhgw;->e:[Lhxs;

    sget-object v1, Lhxs;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readTypedArray([Ljava/lang/Object;Landroid/os/Parcelable$Creator;)V

    .line 251
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lhgw;->a:I

    .line 252
    return-void
.end method

.method public constructor <init>(Lhxc;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 201
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-array v0, v1, [Lkxr;

    iput-object v0, p0, Lhgw;->d:[Lkxr;

    .line 41
    new-array v0, v1, [Lhxs;

    iput-object v0, p0, Lhgw;->e:[Lhxs;

    .line 202
    new-array v0, v1, [Ljqs;

    iput-object v0, p0, Lhgw;->b:[Ljqs;

    .line 203
    const/4 v0, 0x1

    new-array v0, v0, [Lhxc;

    iput-object v0, p0, Lhgw;->c:[Lhxc;

    .line 204
    new-array v0, v1, [Lkxr;

    iput-object v0, p0, Lhgw;->d:[Lkxr;

    .line 205
    new-array v0, v1, [Lhxs;

    iput-object v0, p0, Lhgw;->e:[Lhxs;

    .line 206
    iget-object v0, p0, Lhgw;->c:[Lhxc;

    aput-object p1, v0, v1

    .line 207
    iput v1, p0, Lhgw;->a:I

    .line 208
    return-void
.end method

.method public constructor <init>(Lhxs;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 225
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-array v0, v1, [Lkxr;

    iput-object v0, p0, Lhgw;->d:[Lkxr;

    .line 41
    new-array v0, v1, [Lhxs;

    iput-object v0, p0, Lhgw;->e:[Lhxs;

    .line 226
    new-array v0, v1, [Ljqs;

    iput-object v0, p0, Lhgw;->b:[Ljqs;

    .line 227
    new-array v0, v1, [Lhxc;

    iput-object v0, p0, Lhgw;->c:[Lhxc;

    .line 228
    new-array v0, v1, [Lkxr;

    iput-object v0, p0, Lhgw;->d:[Lkxr;

    .line 229
    const/4 v0, 0x1

    new-array v0, v0, [Lhxs;

    iput-object v0, p0, Lhgw;->e:[Lhxs;

    .line 230
    iget-object v0, p0, Lhgw;->e:[Lhxs;

    aput-object p1, v0, v1

    .line 231
    iput v1, p0, Lhgw;->a:I

    .line 232
    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljqs;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lhxc;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 100
    const/4 v1, 0x0

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    invoke-direct {p0, p1, p2, v1, v0}, Lhgw;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;I)V

    .line 101
    return-void

    .line 100
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/util/List;Ljava/util/List;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljqs;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lhxc;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 112
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0, p3}, Lhgw;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;I)V

    .line 113
    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljqs;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lhxc;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lhxs;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 124
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Lhgw;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;I)V

    .line 126
    if-eqz p3, :cond_0

    .line 127
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lhxs;

    iput-object v0, p0, Lhgw;->e:[Lhxs;

    .line 128
    iget-object v0, p0, Lhgw;->e:[Lhxs;

    invoke-interface {p3, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 130
    :cond_0
    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljqs;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lhxc;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lkxr;",
            ">;I)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 161
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-array v0, v1, [Lkxr;

    iput-object v0, p0, Lhgw;->d:[Lkxr;

    .line 41
    new-array v0, v1, [Lhxs;

    iput-object v0, p0, Lhgw;->e:[Lhxs;

    .line 162
    if-eqz p1, :cond_0

    .line 163
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljqs;

    iput-object v0, p0, Lhgw;->b:[Ljqs;

    .line 164
    iget-object v0, p0, Lhgw;->b:[Ljqs;

    invoke-interface {p1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 169
    :goto_0
    if-eqz p2, :cond_1

    .line 170
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lhxc;

    iput-object v0, p0, Lhgw;->c:[Lhxc;

    .line 171
    iget-object v0, p0, Lhgw;->c:[Lhxc;

    invoke-interface {p2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 176
    :goto_1
    if-eqz p3, :cond_2

    .line 177
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lkxr;

    iput-object v0, p0, Lhgw;->d:[Lkxr;

    .line 178
    iget-object v0, p0, Lhgw;->d:[Lkxr;

    invoke-interface {p3, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 183
    :goto_2
    iput p4, p0, Lhgw;->a:I

    .line 184
    return-void

    .line 166
    :cond_0
    new-array v0, v1, [Ljqs;

    iput-object v0, p0, Lhgw;->b:[Ljqs;

    goto :goto_0

    .line 173
    :cond_1
    new-array v0, v1, [Lhxc;

    iput-object v0, p0, Lhgw;->c:[Lhxc;

    goto :goto_1

    .line 180
    :cond_2
    new-array v0, v1, [Lkxr;

    iput-object v0, p0, Lhgw;->d:[Lkxr;

    goto :goto_2
.end method

.method public constructor <init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljqs;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lhxc;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lkxr;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lhxs;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 144
    invoke-direct {p0, p1, p2, p3, p5}, Lhgw;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;I)V

    .line 146
    if-eqz p4, :cond_0

    .line 147
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lhxs;

    iput-object v0, p0, Lhgw;->e:[Lhxs;

    .line 148
    iget-object v0, p0, Lhgw;->e:[Lhxs;

    invoke-interface {p4, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 150
    :cond_0
    return-void
.end method

.method public constructor <init>(Ljqs;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 189
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-array v0, v1, [Lkxr;

    iput-object v0, p0, Lhgw;->d:[Lkxr;

    .line 41
    new-array v0, v1, [Lhxs;

    iput-object v0, p0, Lhgw;->e:[Lhxs;

    .line 190
    new-array v0, v2, [Ljqs;

    iput-object v0, p0, Lhgw;->b:[Ljqs;

    .line 191
    new-array v0, v1, [Lhxc;

    iput-object v0, p0, Lhgw;->c:[Lhxc;

    .line 192
    new-array v0, v1, [Lkxr;

    iput-object v0, p0, Lhgw;->d:[Lkxr;

    .line 193
    new-array v0, v1, [Lhxs;

    iput-object v0, p0, Lhgw;->e:[Lhxs;

    .line 194
    iget-object v0, p0, Lhgw;->b:[Ljqs;

    aput-object p1, v0, v1

    .line 195
    iput v2, p0, Lhgw;->a:I

    .line 196
    return-void
.end method

.method public constructor <init>(Lkxr;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 213
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-array v0, v1, [Lkxr;

    iput-object v0, p0, Lhgw;->d:[Lkxr;

    .line 41
    new-array v0, v1, [Lhxs;

    iput-object v0, p0, Lhgw;->e:[Lhxs;

    .line 214
    new-array v0, v1, [Ljqs;

    iput-object v0, p0, Lhgw;->b:[Ljqs;

    .line 215
    new-array v0, v1, [Lhxc;

    iput-object v0, p0, Lhgw;->c:[Lhxc;

    .line 216
    const/4 v0, 0x1

    new-array v0, v0, [Lkxr;

    iput-object v0, p0, Lhgw;->d:[Lkxr;

    .line 217
    iget-object v0, p0, Lhgw;->d:[Lkxr;

    aput-object p1, v0, v1

    .line 218
    new-array v0, v1, [Lhxs;

    iput-object v0, p0, Lhgw;->e:[Lhxs;

    .line 219
    iput v1, p0, Lhgw;->a:I

    .line 220
    return-void
.end method

.method public static a(Lhgw;)Lhgw;
    .locals 0

    .prologue
    .line 592
    if-eqz p0, :cond_0

    .line 593
    invoke-virtual {p0}, Lhgw;->n()V

    .line 595
    :cond_0
    return-object p0
.end method

.method public static a(Ljava/lang/Iterable;)Lhgw;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lhgw;",
            ">;)",
            "Lhgw;"
        }
    .end annotation

    .prologue
    .line 707
    if-nez p0, :cond_0

    .line 708
    const/4 v0, 0x0

    .line 759
    :goto_0
    return-object v0

    .line 711
    :cond_0
    new-instance v2, Ljava/util/LinkedHashSet;

    invoke-direct {v2}, Ljava/util/LinkedHashSet;-><init>()V

    .line 712
    new-instance v3, Ljava/util/LinkedHashSet;

    invoke-direct {v3}, Ljava/util/LinkedHashSet;-><init>()V

    .line 713
    new-instance v4, Ljava/util/LinkedHashSet;

    invoke-direct {v4}, Ljava/util/LinkedHashSet;-><init>()V

    .line 714
    new-instance v6, Ljava/util/LinkedHashSet;

    invoke-direct {v6}, Ljava/util/LinkedHashSet;-><init>()V

    .line 715
    const/4 v5, 0x0

    .line 717
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhgw;

    .line 718
    if-eqz v0, :cond_1

    .line 719
    invoke-virtual {v0}, Lhgw;->a()[Ljqs;

    move-result-object v7

    .line 724
    if-eqz v7, :cond_2

    .line 725
    invoke-static {v2, v7}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 729
    :cond_2
    invoke-virtual {v0}, Lhgw;->b()[Lhxc;

    move-result-object v7

    .line 730
    if-eqz v7, :cond_3

    .line 731
    invoke-static {v3, v7}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 735
    :cond_3
    invoke-virtual {v0}, Lhgw;->c()[Lkxr;

    move-result-object v7

    .line 736
    if-eqz v7, :cond_4

    .line 737
    invoke-static {v4, v7}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 741
    :cond_4
    invoke-virtual {v0}, Lhgw;->d()[Lhxs;

    move-result-object v7

    .line 742
    if-eqz v7, :cond_5

    .line 743
    invoke-static {v6, v7}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 747
    :cond_5
    invoke-virtual {v0}, Lhgw;->e()I

    move-result v0

    add-int/2addr v5, v0

    .line 748
    goto :goto_1

    .line 750
    :cond_6
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 751
    invoke-static {v1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 752
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 753
    invoke-static {v2}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 754
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 755
    invoke-static {v3}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 756
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v6}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 758
    invoke-static {v4}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 759
    new-instance v0, Lhgw;

    invoke-direct/range {v0 .. v5}, Lhgw;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;I)V

    goto :goto_0
.end method

.method public static a(Ljava/util/List;)Lhgw;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/os/Parcelable;",
            ">;)",
            "Lhgw;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 53
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 54
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 55
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 56
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 57
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v12

    .line 59
    const/4 v0, 0x0

    move v7, v0

    :goto_0
    if-ge v7, v12, :cond_8

    .line 60
    invoke-interface {p0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    .line 63
    instance-of v1, v0, Ljsm;

    if-eqz v1, :cond_0

    .line 64
    check-cast v0, Ljsm;

    invoke-virtual {v0}, Ljsm;->b()Ljod;

    move-result-object v0

    .line 65
    new-instance v1, Lhxc;

    invoke-interface {v0}, Ljod;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0}, Ljod;->c()I

    move-result v3

    invoke-interface {v0}, Ljod;->b()Ljava/lang/String;

    move-result-object v4

    .line 66
    invoke-interface {v0}, Ljod;->d()I

    move-result v0

    invoke-direct {v1, v2, v3, v4, v0}, Lhxc;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    .line 65
    invoke-interface {v8, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 59
    :goto_1
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    .line 69
    :cond_0
    instance-of v1, v0, Ljsw;

    if-eqz v1, :cond_5

    move-object v1, v0

    .line 70
    check-cast v1, Ljsw;

    invoke-virtual {v1}, Ljsw;->b()Ljpv;

    move-result-object v5

    .line 71
    check-cast v0, Ljsw;

    invoke-virtual {v0}, Ljsw;->c()Z

    move-result v6

    .line 72
    new-instance v0, Ljpu;

    invoke-interface {v5}, Ljpv;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljpu;-><init>(Ljava/lang/String;)V

    .line 73
    invoke-virtual {v0}, Ljpu;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljpv;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x2

    if-le v1, v2, :cond_1

    const-string v1, "e:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 74
    :goto_2
    new-instance v0, Ljqs;

    invoke-interface {v5}, Ljpv;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v5}, Ljpv;->c()Ljava/lang/String;

    move-result-object v2

    .line 75
    invoke-interface {v5}, Ljpv;->d()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v5}, Ljpv;->g()I

    move-result v5

    const/4 v13, 0x2

    if-ne v5, v13, :cond_3

    const/4 v5, 0x1

    :goto_3
    if-nez v6, :cond_4

    const/4 v6, 0x1

    :goto_4
    invoke-direct/range {v0 .. v6}, Ljqs;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 74
    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 73
    :cond_1
    const/4 v3, 0x0

    goto :goto_2

    :cond_2
    const/4 v3, 0x0

    goto :goto_2

    .line 75
    :cond_3
    const/4 v5, 0x0

    goto :goto_3

    :cond_4
    const/4 v6, 0x0

    goto :goto_4

    .line 78
    :cond_5
    instance-of v1, v0, Lkwt;

    if-eqz v1, :cond_6

    .line 79
    check-cast v0, Lkwt;

    invoke-virtual {v0}, Lkwt;->b()Lkxr;

    move-result-object v0

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 82
    :cond_6
    instance-of v1, v0, Lhxq;

    if-eqz v1, :cond_7

    .line 83
    check-cast v0, Lhxq;

    invoke-virtual {v0}, Lhxq;->a()Lhxs;

    move-result-object v0

    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 87
    :cond_7
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "unknown item in selection %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 90
    :cond_8
    new-instance v0, Lhgw;

    move-object v1, v9

    move-object v2, v8

    move-object v3, v10

    move-object v4, v11

    move v5, v12

    invoke-direct/range {v0 .. v5}, Lhgw;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;I)V

    return-object v0
.end method

.method public static a(Landroid/content/res/Resources;Lhxc;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 386
    invoke-virtual {p1}, Lhxc;->b()Ljava/lang/String;

    move-result-object v0

    .line 387
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 390
    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f0a057b

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/content/res/Resources;Lhxs;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 410
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lhxs;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 411
    invoke-virtual {p1}, Lhxs;->b()Ljava/lang/String;

    move-result-object v0

    .line 413
    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f0a057b

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/content/res/Resources;Ljqs;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 374
    invoke-virtual {p1}, Ljqs;->b()Ljava/lang/String;

    move-result-object v0

    .line 375
    invoke-virtual {p1}, Ljqs;->c()Ljava/lang/String;

    move-result-object v1

    .line 376
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 381
    :goto_0
    return-object v0

    .line 378
    :cond_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    move-object v0, v1

    .line 379
    goto :goto_0

    .line 381
    :cond_1
    const v0, 0x104000e

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/content/res/Resources;Lkxr;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 395
    invoke-virtual {p1}, Lkxr;->b()Ljava/lang/String;

    move-result-object v0

    .line 396
    invoke-virtual {p1}, Lkxr;->d()Ljava/lang/String;

    move-result-object v1

    .line 397
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 398
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 399
    const v2, 0x7f0a0534

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    aput-object v1, v3, v0

    invoke-virtual {p0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 404
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const v0, 0x7f0a0533

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lhgw;Lhgw;)Z
    .locals 2

    .prologue
    .line 610
    if-ne p0, p1, :cond_0

    .line 611
    const/4 v0, 0x1

    .line 616
    :goto_0
    return v0

    .line 613
    :cond_0
    if-eqz p0, :cond_1

    if-nez p1, :cond_2

    .line 614
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 616
    :cond_2
    invoke-direct {p0}, Lhgw;->q()Ljava/util/HashSet;

    move-result-object v0

    invoke-direct {p1}, Lhgw;->q()Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public static b(Lhgw;Lhgw;)Lhgw;
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 676
    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lhgw;->a()[Ljqs;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 677
    new-instance v2, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lhgw;->b()[Lhxc;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 678
    new-instance v3, Ljava/util/ArrayList;

    .line 679
    invoke-virtual {p0}, Lhgw;->c()[Lkxr;

    move-result-object v4

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 680
    new-instance v4, Ljava/util/ArrayList;

    .line 681
    invoke-virtual {p0}, Lhgw;->d()[Lhxs;

    move-result-object v5

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 684
    invoke-virtual {p1}, Lhgw;->a()[Ljqs;

    move-result-object v6

    array-length v7, v6

    move v5, v0

    :goto_0
    if-ge v5, v7, :cond_0

    aget-object v8, v6, v5

    .line 685
    invoke-interface {v1, v8}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 684
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 687
    :cond_0
    invoke-virtual {p1}, Lhgw;->b()[Lhxc;

    move-result-object v6

    array-length v7, v6

    move v5, v0

    :goto_1
    if-ge v5, v7, :cond_1

    aget-object v8, v6, v5

    .line 688
    invoke-interface {v2, v8}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 687
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 690
    :cond_1
    invoke-virtual {p1}, Lhgw;->c()[Lkxr;

    move-result-object v6

    array-length v7, v6

    move v5, v0

    :goto_2
    if-ge v5, v7, :cond_2

    aget-object v8, v6, v5

    .line 691
    invoke-interface {v3, v8}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 690
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 693
    :cond_2
    invoke-virtual {p1}, Lhgw;->d()[Lhxs;

    move-result-object v5

    array-length v6, v5

    :goto_3
    if-ge v0, v6, :cond_3

    aget-object v7, v5, v0

    .line 694
    invoke-interface {v4, v7}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 693
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 698
    :cond_3
    new-instance v0, Lhgw;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    invoke-direct/range {v0 .. v5}, Lhgw;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;I)V

    return-object v0
.end method

.method private q()Ljava/util/HashSet;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 620
    new-instance v3, Ljava/util/HashSet;

    invoke-virtual {p0}, Lhgw;->g()I

    move-result v0

    .line 621
    invoke-virtual {p0}, Lhgw;->h()I

    move-result v2

    add-int/2addr v0, v2

    invoke-virtual {p0}, Lhgw;->i()I

    move-result v2

    add-int/2addr v0, v2

    invoke-virtual {p0}, Lhgw;->j()I

    move-result v2

    add-int/2addr v0, v2

    invoke-direct {v3, v0}, Ljava/util/HashSet;-><init>(I)V

    .line 623
    invoke-virtual {p0}, Lhgw;->a()[Ljqs;

    move-result-object v2

    array-length v4, v2

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, v2, v0

    .line 624
    invoke-virtual {v5}, Ljqs;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 623
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 626
    :cond_0
    invoke-virtual {p0}, Lhgw;->b()[Lhxc;

    move-result-object v2

    array-length v4, v2

    move v0, v1

    :goto_1
    if-ge v0, v4, :cond_1

    aget-object v5, v2, v0

    .line 627
    invoke-virtual {v5}, Lhxc;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 626
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 629
    :cond_1
    invoke-virtual {p0}, Lhgw;->c()[Lkxr;

    move-result-object v4

    array-length v5, v4

    move v2, v1

    :goto_2
    if-ge v2, v5, :cond_3

    aget-object v0, v4, v2

    .line 631
    invoke-virtual {v0}, Lkxr;->a()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lkxr;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_2

    invoke-virtual {v6, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-virtual {v3, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 629
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 631
    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3

    .line 633
    :cond_3
    invoke-virtual {p0}, Lhgw;->d()[Lhxs;

    move-result-object v2

    array-length v4, v2

    move v0, v1

    :goto_4
    if-ge v0, v4, :cond_4

    aget-object v1, v2, v0

    .line 634
    invoke-virtual {v1}, Lhxs;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 633
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 637
    :cond_4
    return-object v3
.end method


# virtual methods
.method public a(Landroid/content/Context;)Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 421
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 422
    const v1, 0x7f0a0532

    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 424
    iget-object v1, p0, Lhgw;->c:[Lhxc;

    array-length v1, v1

    iget-object v2, p0, Lhgw;->b:[Ljqs;

    array-length v2, v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lhgw;->d:[Lkxr;

    array-length v2, v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lhgw;->e:[Lhxs;

    array-length v2, v2

    add-int v6, v1, v2

    .line 426
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 429
    iget-object v8, p0, Lhgw;->c:[Lhxc;

    array-length v9, v8

    move v2, v0

    move v1, v0

    :goto_0
    if-ge v2, v9, :cond_1

    aget-object v3, v8, v2

    .line 430
    invoke-static {v4, v3}, Lhgw;->a(Landroid/content/res/Resources;Lhxc;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 431
    add-int/lit8 v3, v1, 0x1

    .line 432
    if-ge v3, v6, :cond_0

    .line 433
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 429
    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v3

    goto :goto_0

    .line 437
    :cond_1
    iget-object v8, p0, Lhgw;->b:[Ljqs;

    array-length v9, v8

    move v2, v0

    :goto_1
    if-ge v2, v9, :cond_3

    aget-object v3, v8, v2

    .line 438
    invoke-static {v4, v3}, Lhgw;->a(Landroid/content/res/Resources;Ljqs;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 439
    add-int/lit8 v3, v1, 0x1

    .line 440
    if-ge v3, v6, :cond_2

    .line 441
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 437
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v3

    goto :goto_1

    .line 445
    :cond_3
    iget-object v8, p0, Lhgw;->d:[Lkxr;

    array-length v9, v8

    move v2, v0

    :goto_2
    if-ge v2, v9, :cond_5

    aget-object v3, v8, v2

    .line 446
    invoke-static {v4, v3}, Lhgw;->a(Landroid/content/res/Resources;Lkxr;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 447
    add-int/lit8 v3, v1, 0x1

    .line 448
    if-ge v3, v6, :cond_4

    .line 449
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 445
    :cond_4
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v3

    goto :goto_2

    .line 453
    :cond_5
    iget-object v2, p0, Lhgw;->e:[Lhxs;

    array-length v3, v2

    :goto_3
    if-ge v0, v3, :cond_7

    aget-object v8, v2, v0

    .line 454
    invoke-static {v4, v8}, Lhgw;->a(Landroid/content/res/Resources;Lhxs;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 455
    add-int/lit8 v1, v1, 0x1

    .line 456
    if-ge v1, v6, :cond_6

    .line 457
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 453
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 461
    :cond_7
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(I)Ljqs;
    .locals 1

    .prologue
    .line 341
    iget-object v0, p0, Lhgw;->b:[Ljqs;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public a()[Ljqs;
    .locals 1

    .prologue
    .line 271
    iget-object v0, p0, Lhgw;->b:[Ljqs;

    return-object v0
.end method

.method public b(I)Lhxc;
    .locals 1

    .prologue
    .line 348
    iget-object v0, p0, Lhgw;->c:[Lhxc;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public b(Landroid/content/Context;)Ljava/lang/String;
    .locals 11

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 468
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 469
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 471
    iget-object v3, p0, Lhgw;->c:[Lhxc;

    array-length v6, v3

    move v0, v2

    :goto_0
    if-ge v0, v6, :cond_0

    aget-object v7, v3, v0

    .line 472
    invoke-static {v4, v7}, Lhgw;->a(Landroid/content/res/Resources;Lhxc;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 471
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 475
    :cond_0
    iget-object v3, p0, Lhgw;->d:[Lkxr;

    array-length v6, v3

    move v0, v2

    :goto_1
    if-ge v0, v6, :cond_1

    aget-object v7, v3, v0

    .line 476
    invoke-static {v4, v7}, Lhgw;->a(Landroid/content/res/Resources;Lkxr;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 475
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 479
    :cond_1
    iget-object v3, p0, Lhgw;->e:[Lhxs;

    array-length v6, v3

    move v0, v2

    :goto_2
    if-ge v0, v6, :cond_2

    aget-object v7, v3, v0

    .line 480
    invoke-static {v4, v7}, Lhgw;->a(Landroid/content/res/Resources;Lhxs;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 479
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 483
    :cond_2
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, v9, :cond_3

    move v0, v1

    .line 485
    :goto_3
    iget-object v6, p0, Lhgw;->b:[Ljqs;

    array-length v7, v6

    move v3, v2

    :goto_4
    if-ge v3, v7, :cond_4

    aget-object v8, v6, v3

    .line 486
    invoke-static {v4, v8}, Lhgw;->a(Landroid/content/res/Resources;Ljqs;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 485
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_3
    move v0, v2

    .line 483
    goto :goto_3

    .line 489
    :cond_4
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 505
    if-eqz v0, :cond_5

    .line 508
    const v0, 0x7f0a0540

    new-array v3, v9, [Ljava/lang/Object;

    .line 509
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v3, v2

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    aput-object v2, v3, v1

    .line 508
    invoke-virtual {v4, v0, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 512
    :goto_5
    return-object v0

    .line 491
    :pswitch_0
    const-string v0, ""

    goto :goto_5

    .line 494
    :pswitch_1
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_5

    .line 497
    :pswitch_2
    const v0, 0x7f0a053e

    new-array v3, v9, [Ljava/lang/Object;

    .line 498
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v3, v2

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    aput-object v2, v3, v1

    .line 497
    invoke-virtual {v4, v0, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    .line 501
    :pswitch_3
    const v0, 0x7f0a053f

    new-array v3, v10, [Ljava/lang/Object;

    .line 502
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v3, v2

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    aput-object v2, v3, v1

    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    aput-object v1, v3, v9

    .line 501
    invoke-virtual {v4, v0, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    .line 511
    :cond_5
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    .line 512
    const v3, 0x7f11002e

    new-array v6, v10, [Ljava/lang/Object;

    .line 513
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    aput-object v7, v6, v2

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    aput-object v2, v6, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v6, v9

    .line 512
    invoke-virtual {v4, v3, v0, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    .line 489
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public b(Lhgw;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 645
    invoke-virtual {p1}, Lhgw;->a()[Ljqs;

    move-result-object v2

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 646
    invoke-virtual {p0}, Lhgw;->a()[Ljqs;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljqs;->a([Ljqs;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 665
    :cond_0
    :goto_1
    return v0

    .line 645
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 650
    :cond_2
    invoke-virtual {p1}, Lhgw;->b()[Lhxc;

    move-result-object v2

    array-length v3, v2

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 651
    invoke-virtual {p0}, Lhgw;->b()[Lhxc;

    move-result-object v5

    invoke-virtual {v4, v5}, Lhxc;->a([Lhxc;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 650
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 655
    :cond_3
    invoke-virtual {p1}, Lhgw;->c()[Lkxr;

    move-result-object v2

    array-length v3, v2

    move v1, v0

    :goto_3
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 656
    invoke-virtual {p0}, Lhgw;->c()[Lkxr;

    move-result-object v5

    invoke-virtual {v4, v5}, Lkxr;->a([Lkxr;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 655
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 660
    :cond_4
    invoke-virtual {p1}, Lhgw;->d()[Lhxs;

    move-result-object v2

    array-length v3, v2

    move v1, v0

    :goto_4
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    .line 661
    invoke-virtual {p0}, Lhgw;->d()[Lhxs;

    move-result-object v5

    invoke-virtual {v4, v5}, Lhxs;->a([Lhxs;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 660
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 665
    :cond_5
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public b()[Lhxc;
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Lhgw;->c:[Lhxc;

    return-object v0
.end method

.method public c(I)Lkxr;
    .locals 1

    .prologue
    .line 355
    iget-object v0, p0, Lhgw;->d:[Lkxr;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public c()[Lkxr;
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Lhgw;->d:[Lkxr;

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0}, Lhgw;->l()Lhgw;

    move-result-object v0

    return-object v0
.end method

.method public d(I)Lhxs;
    .locals 1

    .prologue
    .line 362
    iget-object v0, p0, Lhgw;->e:[Lhxs;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public d()[Lhxs;
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Lhgw;->e:[Lhxs;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 531
    const/4 v0, 0x0

    return v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 299
    iget v0, p0, Lhgw;->a:I

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 559
    instance-of v0, p1, Lhgw;

    if-eqz v0, :cond_0

    .line 560
    check-cast p1, Lhgw;

    .line 561
    iget v0, p0, Lhgw;->a:I

    iget v1, p1, Lhgw;->a:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lhgw;->b:[Ljqs;

    iget-object v1, p1, Lhgw;->b:[Ljqs;

    .line 562
    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhgw;->c:[Lhxc;

    iget-object v1, p1, Lhgw;->c:[Lhxc;

    .line 563
    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhgw;->d:[Lkxr;

    iget-object v1, p1, Lhgw;->d:[Lkxr;

    .line 564
    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhgw;->e:[Lhxs;

    iget-object v1, p1, Lhgw;->e:[Lhxs;

    .line 565
    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 566
    const/4 v0, 0x1

    .line 569
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()I
    .locals 3

    .prologue
    .line 306
    const/4 v0, 0x0

    iget v1, p0, Lhgw;->a:I

    invoke-virtual {p0}, Lhgw;->g()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public g()I
    .locals 1

    .prologue
    .line 313
    iget-object v0, p0, Lhgw;->b:[Ljqs;

    array-length v0, v0

    return v0
.end method

.method public h()I
    .locals 1

    .prologue
    .line 320
    iget-object v0, p0, Lhgw;->c:[Lhxc;

    array-length v0, v0

    return v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 574
    iget v0, p0, Lhgw;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 576
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lhgw;->b:[Ljqs;

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 577
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lhgw;->c:[Lhxc;

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 578
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lhgw;->d:[Lkxr;

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 579
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lhgw;->e:[Lhxs;

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 580
    return v0
.end method

.method public i()I
    .locals 1

    .prologue
    .line 327
    iget-object v0, p0, Lhgw;->d:[Lkxr;

    array-length v0, v0

    return v0
.end method

.method public j()I
    .locals 1

    .prologue
    .line 334
    iget-object v0, p0, Lhgw;->e:[Lhxs;

    array-length v0, v0

    return v0
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 369
    invoke-virtual {p0}, Lhgw;->g()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lhgw;->h()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lhgw;->i()I

    move-result v0

    if-nez v0, :cond_0

    .line 370
    invoke-virtual {p0}, Lhgw;->j()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l()Lhgw;
    .locals 6

    .prologue
    .line 552
    new-instance v0, Lhgw;

    invoke-virtual {p0}, Lhgw;->a()[Ljqs;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0}, Lhgw;->b()[Lhxc;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 553
    invoke-virtual {p0}, Lhgw;->c()[Lkxr;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {p0}, Lhgw;->d()[Lhxs;

    move-result-object v4

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    iget v5, p0, Lhgw;->a:I

    invoke-direct/range {v0 .. v5}, Lhgw;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;I)V

    return-object v0
.end method

.method public m()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 584
    invoke-virtual {p0}, Lhgw;->h()I

    move-result v2

    if-ne v2, v0, :cond_0

    const/16 v2, 0x65

    invoke-virtual {p0, v1}, Lhgw;->b(I)Lhxc;

    move-result-object v3

    invoke-virtual {v3}, Lhxc;->c()I

    move-result v3

    if-ne v2, v3, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public n()V
    .locals 1

    .prologue
    .line 602
    iget-object v0, p0, Lhgw;->b:[Ljqs;

    invoke-static {v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 603
    iget-object v0, p0, Lhgw;->c:[Lhxc;

    invoke-static {v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 604
    return-void
.end method

.method public o()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 763
    invoke-virtual {p0}, Lhgw;->i()I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {p0, v0}, Lhgw;->c(I)Lkxr;

    move-result-object v1

    invoke-virtual {v1}, Lkxr;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public p()Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Parcelable;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 772
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move v0, v1

    .line 774
    :goto_0
    iget-object v2, p0, Lhgw;->b:[Ljqs;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 775
    iget-object v2, p0, Lhgw;->b:[Ljqs;

    aget-object v2, v2, v0

    .line 777
    invoke-static {}, Ljsw;->a()Ljsy;

    move-result-object v4

    new-instance v5, Lhhi;

    invoke-direct {v5, v2}, Lhhi;-><init>(Ljqs;)V

    .line 778
    invoke-virtual {v4, v5}, Ljsy;->a(Ljpv;)Ljsy;

    move-result-object v4

    .line 779
    invoke-virtual {v2}, Ljqs;->f()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x1

    :goto_1
    invoke-virtual {v4, v2}, Ljsy;->a(Z)Ljsy;

    move-result-object v2

    .line 780
    invoke-virtual {v2}, Ljsy;->a()Ljsw;

    move-result-object v2

    .line 776
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 774
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v2, v1

    .line 779
    goto :goto_1

    :cond_1
    move v0, v1

    .line 783
    :goto_2
    iget-object v2, p0, Lhgw;->c:[Lhxc;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 784
    iget-object v2, p0, Lhgw;->c:[Lhxc;

    aget-object v2, v2, v0

    .line 786
    invoke-static {}, Ljsm;->a()Ljso;

    move-result-object v4

    new-instance v5, Lhhc;

    invoke-direct {v5, v2}, Lhhc;-><init>(Lhxc;)V

    .line 787
    invoke-virtual {v4, v5}, Ljso;->a(Ljod;)Ljso;

    move-result-object v2

    .line 788
    invoke-virtual {v2}, Ljso;->a()Ljsm;

    move-result-object v2

    .line 785
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 783
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 791
    :cond_2
    :goto_3
    iget-object v0, p0, Lhgw;->d:[Lkxr;

    array-length v0, v0

    if-ge v1, v0, :cond_3

    .line 792
    iget-object v0, p0, Lhgw;->d:[Lkxr;

    aget-object v0, v0, v1

    .line 794
    invoke-static {}, Lkwt;->a()Lkwv;

    move-result-object v2

    .line 795
    invoke-virtual {v2, v0}, Lkwv;->a(Lkxr;)Lkwv;

    move-result-object v0

    .line 796
    invoke-virtual {v0}, Lkwv;->a()Lkwt;

    move-result-object v0

    .line 793
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 791
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 799
    :cond_3
    return-object v3
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 520
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Audience circles: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 521
    iget-object v1, p0, Lhgw;->c:[Lhxc;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", users: "

    .line 522
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lhgw;->b:[Ljqs;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", squares: "

    .line 523
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lhgw;->d:[Lkxr;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", clx: "

    .line 524
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lhgw;->e:[Lhxs;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", hidden users: "

    .line 525
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lhgw;->f()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 526
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 256
    iget-object v0, p0, Lhgw;->b:[Ljqs;

    array-length v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 257
    iget-object v0, p0, Lhgw;->b:[Ljqs;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    .line 258
    iget-object v0, p0, Lhgw;->c:[Lhxc;

    array-length v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 259
    iget-object v0, p0, Lhgw;->c:[Lhxc;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    .line 260
    iget-object v0, p0, Lhgw;->d:[Lkxr;

    array-length v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 261
    iget-object v0, p0, Lhgw;->d:[Lkxr;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    .line 262
    iget-object v0, p0, Lhgw;->e:[Lhxs;

    array-length v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 263
    iget-object v0, p0, Lhgw;->e:[Lhxs;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    .line 264
    iget v0, p0, Lhgw;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 265
    return-void
.end method
