.class public final Lmte;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmte;


# instance fields
.field private b:Ljava/lang/Double;

.field private c:Ljava/lang/Integer;

.field private d:Ljava/lang/String;

.field private e:Lmsn;

.field private f:[Lmso;

.field private g:Ljava/lang/Integer;

.field private h:Ljava/lang/Integer;

.field private i:Ljava/lang/Integer;

.field private j:Ljava/lang/Long;

.field private k:Ljava/lang/Integer;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/Integer;

.field private n:[Lmso;

.field private o:Ljava/lang/Integer;

.field private p:Ljava/lang/Integer;

.field private q:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2749
    const/4 v0, 0x0

    new-array v0, v0, [Lmte;

    sput-object v0, Lmte;->a:[Lmte;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2750
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2759
    const/4 v0, 0x0

    iput-object v0, p0, Lmte;->e:Lmsn;

    .line 2762
    sget-object v0, Lmso;->a:[Lmso;

    iput-object v0, p0, Lmte;->f:[Lmso;

    .line 2779
    sget-object v0, Lmso;->a:[Lmso;

    iput-object v0, p0, Lmte;->n:[Lmso;

    .line 2750
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2852
    .line 2853
    iget-object v0, p0, Lmte;->b:Ljava/lang/Double;

    if-eqz v0, :cond_11

    .line 2854
    const/4 v0, 0x1

    iget-object v2, p0, Lmte;->b:Ljava/lang/Double;

    .line 2855
    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x8

    add-int/lit8 v0, v0, 0x0

    .line 2857
    :goto_0
    iget-object v2, p0, Lmte;->c:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    .line 2858
    const/4 v2, 0x2

    iget-object v3, p0, Lmte;->c:Ljava/lang/Integer;

    .line 2859
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 2861
    :cond_0
    iget-object v2, p0, Lmte;->d:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 2862
    const/4 v2, 0x3

    iget-object v3, p0, Lmte;->d:Ljava/lang/String;

    .line 2863
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2865
    :cond_1
    iget-object v2, p0, Lmte;->e:Lmsn;

    if-eqz v2, :cond_2

    .line 2866
    const/4 v2, 0x4

    iget-object v3, p0, Lmte;->e:Lmsn;

    .line 2867
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2869
    :cond_2
    iget-object v2, p0, Lmte;->f:[Lmso;

    if-eqz v2, :cond_4

    .line 2870
    iget-object v3, p0, Lmte;->f:[Lmso;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_4

    aget-object v5, v3, v2

    .line 2871
    if-eqz v5, :cond_3

    .line 2872
    const/4 v6, 0x5

    .line 2873
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 2870
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2877
    :cond_4
    iget-object v2, p0, Lmte;->g:Ljava/lang/Integer;

    if-eqz v2, :cond_5

    .line 2878
    const/4 v2, 0x6

    iget-object v3, p0, Lmte;->g:Ljava/lang/Integer;

    .line 2879
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 2881
    :cond_5
    iget-object v2, p0, Lmte;->h:Ljava/lang/Integer;

    if-eqz v2, :cond_6

    .line 2882
    const/4 v2, 0x7

    iget-object v3, p0, Lmte;->h:Ljava/lang/Integer;

    .line 2883
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 2885
    :cond_6
    iget-object v2, p0, Lmte;->i:Ljava/lang/Integer;

    if-eqz v2, :cond_7

    .line 2886
    const/16 v2, 0x8

    iget-object v3, p0, Lmte;->i:Ljava/lang/Integer;

    .line 2887
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 2889
    :cond_7
    iget-object v2, p0, Lmte;->j:Ljava/lang/Long;

    if-eqz v2, :cond_8

    .line 2890
    const/16 v2, 0x9

    iget-object v3, p0, Lmte;->j:Ljava/lang/Long;

    .line 2891
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Loxo;->e(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 2893
    :cond_8
    iget-object v2, p0, Lmte;->l:Ljava/lang/String;

    if-eqz v2, :cond_9

    .line 2894
    const/16 v2, 0xa

    iget-object v3, p0, Lmte;->l:Ljava/lang/String;

    .line 2895
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2897
    :cond_9
    iget-object v2, p0, Lmte;->k:Ljava/lang/Integer;

    if-eqz v2, :cond_a

    .line 2898
    const/16 v2, 0xb

    iget-object v3, p0, Lmte;->k:Ljava/lang/Integer;

    .line 2899
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 2901
    :cond_a
    iget-object v2, p0, Lmte;->m:Ljava/lang/Integer;

    if-eqz v2, :cond_b

    .line 2902
    const/16 v2, 0xc

    iget-object v3, p0, Lmte;->m:Ljava/lang/Integer;

    .line 2903
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 2905
    :cond_b
    iget-object v2, p0, Lmte;->n:[Lmso;

    if-eqz v2, :cond_d

    .line 2906
    iget-object v2, p0, Lmte;->n:[Lmso;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_d

    aget-object v4, v2, v1

    .line 2907
    if-eqz v4, :cond_c

    .line 2908
    const/16 v5, 0xd

    .line 2909
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 2906
    :cond_c
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2913
    :cond_d
    iget-object v1, p0, Lmte;->o:Ljava/lang/Integer;

    if-eqz v1, :cond_e

    .line 2914
    const/16 v1, 0xe

    iget-object v2, p0, Lmte;->o:Ljava/lang/Integer;

    .line 2915
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2917
    :cond_e
    iget-object v1, p0, Lmte;->p:Ljava/lang/Integer;

    if-eqz v1, :cond_f

    .line 2918
    const/16 v1, 0xf

    iget-object v2, p0, Lmte;->p:Ljava/lang/Integer;

    .line 2919
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2921
    :cond_f
    iget-object v1, p0, Lmte;->q:Ljava/lang/Integer;

    if-eqz v1, :cond_10

    .line 2922
    const/16 v1, 0x10

    iget-object v2, p0, Lmte;->q:Ljava/lang/Integer;

    .line 2923
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2925
    :cond_10
    iget-object v1, p0, Lmte;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2926
    iput v0, p0, Lmte;->ai:I

    .line 2927
    return v0

    :cond_11
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lmte;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2935
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2936
    sparse-switch v0, :sswitch_data_0

    .line 2940
    iget-object v2, p0, Lmte;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 2941
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmte;->ah:Ljava/util/List;

    .line 2944
    :cond_1
    iget-object v2, p0, Lmte;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2946
    :sswitch_0
    return-object p0

    .line 2951
    :sswitch_1
    invoke-virtual {p1}, Loxn;->c()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lmte;->b:Ljava/lang/Double;

    goto :goto_0

    .line 2955
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmte;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 2959
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmte;->d:Ljava/lang/String;

    goto :goto_0

    .line 2963
    :sswitch_4
    iget-object v0, p0, Lmte;->e:Lmsn;

    if-nez v0, :cond_2

    .line 2964
    new-instance v0, Lmsn;

    invoke-direct {v0}, Lmsn;-><init>()V

    iput-object v0, p0, Lmte;->e:Lmsn;

    .line 2966
    :cond_2
    iget-object v0, p0, Lmte;->e:Lmsn;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2970
    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 2971
    iget-object v0, p0, Lmte;->f:[Lmso;

    if-nez v0, :cond_4

    move v0, v1

    .line 2972
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lmso;

    .line 2973
    iget-object v3, p0, Lmte;->f:[Lmso;

    if-eqz v3, :cond_3

    .line 2974
    iget-object v3, p0, Lmte;->f:[Lmso;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2976
    :cond_3
    iput-object v2, p0, Lmte;->f:[Lmso;

    .line 2977
    :goto_2
    iget-object v2, p0, Lmte;->f:[Lmso;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 2978
    iget-object v2, p0, Lmte;->f:[Lmso;

    new-instance v3, Lmso;

    invoke-direct {v3}, Lmso;-><init>()V

    aput-object v3, v2, v0

    .line 2979
    iget-object v2, p0, Lmte;->f:[Lmso;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 2980
    invoke-virtual {p1}, Loxn;->a()I

    .line 2977
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2971
    :cond_4
    iget-object v0, p0, Lmte;->f:[Lmso;

    array-length v0, v0

    goto :goto_1

    .line 2983
    :cond_5
    iget-object v2, p0, Lmte;->f:[Lmso;

    new-instance v3, Lmso;

    invoke-direct {v3}, Lmso;-><init>()V

    aput-object v3, v2, v0

    .line 2984
    iget-object v2, p0, Lmte;->f:[Lmso;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 2988
    :sswitch_6
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmte;->g:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 2992
    :sswitch_7
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmte;->h:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 2996
    :sswitch_8
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmte;->i:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 3000
    :sswitch_9
    invoke-virtual {p1}, Loxn;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lmte;->j:Ljava/lang/Long;

    goto/16 :goto_0

    .line 3004
    :sswitch_a
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmte;->l:Ljava/lang/String;

    goto/16 :goto_0

    .line 3008
    :sswitch_b
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmte;->k:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 3012
    :sswitch_c
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmte;->m:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 3016
    :sswitch_d
    const/16 v0, 0x6a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 3017
    iget-object v0, p0, Lmte;->n:[Lmso;

    if-nez v0, :cond_7

    move v0, v1

    .line 3018
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lmso;

    .line 3019
    iget-object v3, p0, Lmte;->n:[Lmso;

    if-eqz v3, :cond_6

    .line 3020
    iget-object v3, p0, Lmte;->n:[Lmso;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3022
    :cond_6
    iput-object v2, p0, Lmte;->n:[Lmso;

    .line 3023
    :goto_4
    iget-object v2, p0, Lmte;->n:[Lmso;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_8

    .line 3024
    iget-object v2, p0, Lmte;->n:[Lmso;

    new-instance v3, Lmso;

    invoke-direct {v3}, Lmso;-><init>()V

    aput-object v3, v2, v0

    .line 3025
    iget-object v2, p0, Lmte;->n:[Lmso;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 3026
    invoke-virtual {p1}, Loxn;->a()I

    .line 3023
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 3017
    :cond_7
    iget-object v0, p0, Lmte;->n:[Lmso;

    array-length v0, v0

    goto :goto_3

    .line 3029
    :cond_8
    iget-object v2, p0, Lmte;->n:[Lmso;

    new-instance v3, Lmso;

    invoke-direct {v3}, Lmso;-><init>()V

    aput-object v3, v2, v0

    .line 3030
    iget-object v2, p0, Lmte;->n:[Lmso;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 3034
    :sswitch_e
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmte;->o:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 3038
    :sswitch_f
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmte;->p:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 3042
    :sswitch_10
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmte;->q:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 2936
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x6a -> :sswitch_d
        0x70 -> :sswitch_e
        0x78 -> :sswitch_f
        0x80 -> :sswitch_10
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2790
    iget-object v1, p0, Lmte;->b:Ljava/lang/Double;

    if-eqz v1, :cond_0

    .line 2791
    const/4 v1, 0x1

    iget-object v2, p0, Lmte;->b:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Loxo;->a(ID)V

    .line 2793
    :cond_0
    iget-object v1, p0, Lmte;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 2794
    const/4 v1, 0x2

    iget-object v2, p0, Lmte;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 2796
    :cond_1
    iget-object v1, p0, Lmte;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 2797
    const/4 v1, 0x3

    iget-object v2, p0, Lmte;->d:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 2799
    :cond_2
    iget-object v1, p0, Lmte;->e:Lmsn;

    if-eqz v1, :cond_3

    .line 2800
    const/4 v1, 0x4

    iget-object v2, p0, Lmte;->e:Lmsn;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 2802
    :cond_3
    iget-object v1, p0, Lmte;->f:[Lmso;

    if-eqz v1, :cond_5

    .line 2803
    iget-object v2, p0, Lmte;->f:[Lmso;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    .line 2804
    if-eqz v4, :cond_4

    .line 2805
    const/4 v5, 0x5

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 2803
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2809
    :cond_5
    iget-object v1, p0, Lmte;->g:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 2810
    const/4 v1, 0x6

    iget-object v2, p0, Lmte;->g:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 2812
    :cond_6
    iget-object v1, p0, Lmte;->h:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    .line 2813
    const/4 v1, 0x7

    iget-object v2, p0, Lmte;->h:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 2815
    :cond_7
    iget-object v1, p0, Lmte;->i:Ljava/lang/Integer;

    if-eqz v1, :cond_8

    .line 2816
    const/16 v1, 0x8

    iget-object v2, p0, Lmte;->i:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 2818
    :cond_8
    iget-object v1, p0, Lmte;->j:Ljava/lang/Long;

    if-eqz v1, :cond_9

    .line 2819
    const/16 v1, 0x9

    iget-object v2, p0, Lmte;->j:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Loxo;->a(IJ)V

    .line 2821
    :cond_9
    iget-object v1, p0, Lmte;->l:Ljava/lang/String;

    if-eqz v1, :cond_a

    .line 2822
    const/16 v1, 0xa

    iget-object v2, p0, Lmte;->l:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 2824
    :cond_a
    iget-object v1, p0, Lmte;->k:Ljava/lang/Integer;

    if-eqz v1, :cond_b

    .line 2825
    const/16 v1, 0xb

    iget-object v2, p0, Lmte;->k:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 2827
    :cond_b
    iget-object v1, p0, Lmte;->m:Ljava/lang/Integer;

    if-eqz v1, :cond_c

    .line 2828
    const/16 v1, 0xc

    iget-object v2, p0, Lmte;->m:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 2830
    :cond_c
    iget-object v1, p0, Lmte;->n:[Lmso;

    if-eqz v1, :cond_e

    .line 2831
    iget-object v1, p0, Lmte;->n:[Lmso;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_e

    aget-object v3, v1, v0

    .line 2832
    if-eqz v3, :cond_d

    .line 2833
    const/16 v4, 0xd

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 2831
    :cond_d
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2837
    :cond_e
    iget-object v0, p0, Lmte;->o:Ljava/lang/Integer;

    if-eqz v0, :cond_f

    .line 2838
    const/16 v0, 0xe

    iget-object v1, p0, Lmte;->o:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2840
    :cond_f
    iget-object v0, p0, Lmte;->p:Ljava/lang/Integer;

    if-eqz v0, :cond_10

    .line 2841
    const/16 v0, 0xf

    iget-object v1, p0, Lmte;->p:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2843
    :cond_10
    iget-object v0, p0, Lmte;->q:Ljava/lang/Integer;

    if-eqz v0, :cond_11

    .line 2844
    const/16 v0, 0x10

    iget-object v1, p0, Lmte;->q:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2846
    :cond_11
    iget-object v0, p0, Lmte;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2848
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2746
    invoke-virtual {p0, p1}, Lmte;->a(Loxn;)Lmte;

    move-result-object v0

    return-object v0
.end method
