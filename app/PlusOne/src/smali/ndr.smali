.class public final Lndr;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:[Lnzx;

.field public c:[Lnzx;

.field public d:[Lnyz;

.field public e:[Lndp;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 240
    invoke-direct {p0}, Loxq;-><init>()V

    .line 245
    sget-object v0, Lnzx;->a:[Lnzx;

    iput-object v0, p0, Lndr;->b:[Lnzx;

    .line 248
    sget-object v0, Lnzx;->a:[Lnzx;

    iput-object v0, p0, Lndr;->c:[Lnzx;

    .line 251
    sget-object v0, Lnyz;->a:[Lnyz;

    iput-object v0, p0, Lndr;->d:[Lnyz;

    .line 254
    sget-object v0, Lndp;->a:[Lndp;

    iput-object v0, p0, Lndr;->e:[Lndp;

    .line 240
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 296
    .line 297
    iget-object v0, p0, Lndr;->a:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 298
    const/4 v0, 0x1

    iget-object v2, p0, Lndr;->a:Ljava/lang/String;

    .line 299
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 301
    :goto_0
    iget-object v2, p0, Lndr;->b:[Lnzx;

    if-eqz v2, :cond_1

    .line 302
    iget-object v3, p0, Lndr;->b:[Lnzx;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 303
    if-eqz v5, :cond_0

    .line 304
    const/4 v6, 0x2

    .line 305
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 302
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 309
    :cond_1
    iget-object v2, p0, Lndr;->c:[Lnzx;

    if-eqz v2, :cond_3

    .line 310
    iget-object v3, p0, Lndr;->c:[Lnzx;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_3

    aget-object v5, v3, v2

    .line 311
    if-eqz v5, :cond_2

    .line 312
    const/4 v6, 0x3

    .line 313
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 310
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 317
    :cond_3
    iget-object v2, p0, Lndr;->d:[Lnyz;

    if-eqz v2, :cond_5

    .line 318
    iget-object v3, p0, Lndr;->d:[Lnyz;

    array-length v4, v3

    move v2, v1

    :goto_3
    if-ge v2, v4, :cond_5

    aget-object v5, v3, v2

    .line 319
    if-eqz v5, :cond_4

    .line 320
    const/4 v6, 0x4

    .line 321
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 318
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 325
    :cond_5
    iget-object v2, p0, Lndr;->e:[Lndp;

    if-eqz v2, :cond_7

    .line 326
    iget-object v2, p0, Lndr;->e:[Lndp;

    array-length v3, v2

    :goto_4
    if-ge v1, v3, :cond_7

    aget-object v4, v2, v1

    .line 327
    if-eqz v4, :cond_6

    .line 328
    const/4 v5, 0x5

    .line 329
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 326
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 333
    :cond_7
    iget-object v1, p0, Lndr;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 334
    iput v0, p0, Lndr;->ai:I

    .line 335
    return v0

    :cond_8
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lndr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 343
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 344
    sparse-switch v0, :sswitch_data_0

    .line 348
    iget-object v2, p0, Lndr;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 349
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lndr;->ah:Ljava/util/List;

    .line 352
    :cond_1
    iget-object v2, p0, Lndr;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 354
    :sswitch_0
    return-object p0

    .line 359
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lndr;->a:Ljava/lang/String;

    goto :goto_0

    .line 363
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 364
    iget-object v0, p0, Lndr;->b:[Lnzx;

    if-nez v0, :cond_3

    move v0, v1

    .line 365
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnzx;

    .line 366
    iget-object v3, p0, Lndr;->b:[Lnzx;

    if-eqz v3, :cond_2

    .line 367
    iget-object v3, p0, Lndr;->b:[Lnzx;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 369
    :cond_2
    iput-object v2, p0, Lndr;->b:[Lnzx;

    .line 370
    :goto_2
    iget-object v2, p0, Lndr;->b:[Lnzx;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 371
    iget-object v2, p0, Lndr;->b:[Lnzx;

    new-instance v3, Lnzx;

    invoke-direct {v3}, Lnzx;-><init>()V

    aput-object v3, v2, v0

    .line 372
    iget-object v2, p0, Lndr;->b:[Lnzx;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 373
    invoke-virtual {p1}, Loxn;->a()I

    .line 370
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 364
    :cond_3
    iget-object v0, p0, Lndr;->b:[Lnzx;

    array-length v0, v0

    goto :goto_1

    .line 376
    :cond_4
    iget-object v2, p0, Lndr;->b:[Lnzx;

    new-instance v3, Lnzx;

    invoke-direct {v3}, Lnzx;-><init>()V

    aput-object v3, v2, v0

    .line 377
    iget-object v2, p0, Lndr;->b:[Lnzx;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 381
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 382
    iget-object v0, p0, Lndr;->c:[Lnzx;

    if-nez v0, :cond_6

    move v0, v1

    .line 383
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lnzx;

    .line 384
    iget-object v3, p0, Lndr;->c:[Lnzx;

    if-eqz v3, :cond_5

    .line 385
    iget-object v3, p0, Lndr;->c:[Lnzx;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 387
    :cond_5
    iput-object v2, p0, Lndr;->c:[Lnzx;

    .line 388
    :goto_4
    iget-object v2, p0, Lndr;->c:[Lnzx;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    .line 389
    iget-object v2, p0, Lndr;->c:[Lnzx;

    new-instance v3, Lnzx;

    invoke-direct {v3}, Lnzx;-><init>()V

    aput-object v3, v2, v0

    .line 390
    iget-object v2, p0, Lndr;->c:[Lnzx;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 391
    invoke-virtual {p1}, Loxn;->a()I

    .line 388
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 382
    :cond_6
    iget-object v0, p0, Lndr;->c:[Lnzx;

    array-length v0, v0

    goto :goto_3

    .line 394
    :cond_7
    iget-object v2, p0, Lndr;->c:[Lnzx;

    new-instance v3, Lnzx;

    invoke-direct {v3}, Lnzx;-><init>()V

    aput-object v3, v2, v0

    .line 395
    iget-object v2, p0, Lndr;->c:[Lnzx;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 399
    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 400
    iget-object v0, p0, Lndr;->d:[Lnyz;

    if-nez v0, :cond_9

    move v0, v1

    .line 401
    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lnyz;

    .line 402
    iget-object v3, p0, Lndr;->d:[Lnyz;

    if-eqz v3, :cond_8

    .line 403
    iget-object v3, p0, Lndr;->d:[Lnyz;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 405
    :cond_8
    iput-object v2, p0, Lndr;->d:[Lnyz;

    .line 406
    :goto_6
    iget-object v2, p0, Lndr;->d:[Lnyz;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_a

    .line 407
    iget-object v2, p0, Lndr;->d:[Lnyz;

    new-instance v3, Lnyz;

    invoke-direct {v3}, Lnyz;-><init>()V

    aput-object v3, v2, v0

    .line 408
    iget-object v2, p0, Lndr;->d:[Lnyz;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 409
    invoke-virtual {p1}, Loxn;->a()I

    .line 406
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 400
    :cond_9
    iget-object v0, p0, Lndr;->d:[Lnyz;

    array-length v0, v0

    goto :goto_5

    .line 412
    :cond_a
    iget-object v2, p0, Lndr;->d:[Lnyz;

    new-instance v3, Lnyz;

    invoke-direct {v3}, Lnyz;-><init>()V

    aput-object v3, v2, v0

    .line 413
    iget-object v2, p0, Lndr;->d:[Lnyz;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 417
    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 418
    iget-object v0, p0, Lndr;->e:[Lndp;

    if-nez v0, :cond_c

    move v0, v1

    .line 419
    :goto_7
    add-int/2addr v2, v0

    new-array v2, v2, [Lndp;

    .line 420
    iget-object v3, p0, Lndr;->e:[Lndp;

    if-eqz v3, :cond_b

    .line 421
    iget-object v3, p0, Lndr;->e:[Lndp;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 423
    :cond_b
    iput-object v2, p0, Lndr;->e:[Lndp;

    .line 424
    :goto_8
    iget-object v2, p0, Lndr;->e:[Lndp;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_d

    .line 425
    iget-object v2, p0, Lndr;->e:[Lndp;

    new-instance v3, Lndp;

    invoke-direct {v3}, Lndp;-><init>()V

    aput-object v3, v2, v0

    .line 426
    iget-object v2, p0, Lndr;->e:[Lndp;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 427
    invoke-virtual {p1}, Loxn;->a()I

    .line 424
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 418
    :cond_c
    iget-object v0, p0, Lndr;->e:[Lndp;

    array-length v0, v0

    goto :goto_7

    .line 430
    :cond_d
    iget-object v2, p0, Lndr;->e:[Lndp;

    new-instance v3, Lndp;

    invoke-direct {v3}, Lndp;-><init>()V

    aput-object v3, v2, v0

    .line 431
    iget-object v2, p0, Lndr;->e:[Lndp;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 344
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 259
    iget-object v1, p0, Lndr;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 260
    const/4 v1, 0x1

    iget-object v2, p0, Lndr;->a:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 262
    :cond_0
    iget-object v1, p0, Lndr;->b:[Lnzx;

    if-eqz v1, :cond_2

    .line 263
    iget-object v2, p0, Lndr;->b:[Lnzx;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 264
    if-eqz v4, :cond_1

    .line 265
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 263
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 269
    :cond_2
    iget-object v1, p0, Lndr;->c:[Lnzx;

    if-eqz v1, :cond_4

    .line 270
    iget-object v2, p0, Lndr;->c:[Lnzx;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 271
    if-eqz v4, :cond_3

    .line 272
    const/4 v5, 0x3

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 270
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 276
    :cond_4
    iget-object v1, p0, Lndr;->d:[Lnyz;

    if-eqz v1, :cond_6

    .line 277
    iget-object v2, p0, Lndr;->d:[Lnyz;

    array-length v3, v2

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    .line 278
    if-eqz v4, :cond_5

    .line 279
    const/4 v5, 0x4

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 277
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 283
    :cond_6
    iget-object v1, p0, Lndr;->e:[Lndp;

    if-eqz v1, :cond_8

    .line 284
    iget-object v1, p0, Lndr;->e:[Lndp;

    array-length v2, v1

    :goto_3
    if-ge v0, v2, :cond_8

    aget-object v3, v1, v0

    .line 285
    if-eqz v3, :cond_7

    .line 286
    const/4 v4, 0x5

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 284
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 290
    :cond_8
    iget-object v0, p0, Lndr;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 292
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 236
    invoke-virtual {p0, p1}, Lndr;->a(Loxn;)Lndr;

    move-result-object v0

    return-object v0
.end method
