.class public final Lfut;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Ljava/util/List;)[B
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Loxu;",
            ">(",
            "Ljava/util/List",
            "<TT;>;)[B"
        }
    .end annotation

    .prologue
    .line 74
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Loxu;

    invoke-interface {p0, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Loxu;

    invoke-static {v0}, Lfut;->a([Loxu;)[B

    move-result-object v0

    return-object v0
.end method

.method public static a([Loxu;)[B
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Loxu;",
            ">([TT;)[B"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 78
    if-eqz p0, :cond_0

    array-length v0, p0

    if-nez v0, :cond_2

    :cond_0
    move-object v0, v1

    .line 91
    :cond_1
    :goto_0
    return-object v0

    .line 82
    :cond_2
    :try_start_0
    array-length v0, p0

    invoke-static {v0}, Loxo;->i(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    move v3, v0

    move v0, v2

    :goto_1
    array-length v4, p0

    if-ge v0, v4, :cond_3

    aget-object v4, p0, v0

    invoke-static {v4}, Loxo;->c(Loxu;)I

    move-result v4

    add-int/2addr v3, v4

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    new-array v0, v3, [B

    .line 83
    const/4 v3, 0x0

    array-length v4, v0

    invoke-static {v0, v3, v4}, Loxo;->a([BII)Loxo;

    move-result-object v3

    .line 84
    array-length v4, p0

    invoke-virtual {v3, v4}, Loxo;->a(I)V

    .line 85
    :goto_2
    array-length v4, p0

    if-ge v2, v4, :cond_1

    .line 86
    aget-object v4, p0, v2

    invoke-virtual {v3, v4}, Loxo;->b(Loxu;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 85
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 89
    :catch_0
    move-exception v0

    .line 90
    const-string v2, "MessageNanoList"

    const-string v3, "Failed to serialize messages"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    .line 91
    goto :goto_0
.end method

.method public static a([BLjava/lang/Class;)[Loxu;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Loxu;",
            ">([B",
            "Ljava/lang/Class",
            "<TT;>;)[TT;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 38
    if-eqz p0, :cond_0

    array-length v2, p0

    if-nez v2, :cond_1

    :cond_0
    move-object v1, v3

    .line 62
    :goto_0
    return-object v1

    .line 42
    :cond_1
    const/4 v2, 0x0

    :try_start_0
    array-length v4, p0

    invoke-static {p0, v2, v4}, Loxn;->a([BII)Loxn;

    move-result-object v5

    .line 43
    invoke-virtual {v5}, Loxn;->g()I

    move-result v6

    .line 44
    if-nez v6, :cond_2

    move-object v1, v3

    .line 45
    goto :goto_0

    .line 47
    :cond_2
    invoke-static {p1, v6}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v2

    move v4, v1

    .line 48
    :goto_1
    if-ge v4, v6, :cond_3

    .line 49
    invoke-virtual {p1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Loxu;

    .line 50
    invoke-virtual {v5, v1}, Loxn;->a(Loxu;)V

    .line 51
    invoke-static {v2, v4, v1}, Ljava/lang/reflect/Array;->set(Ljava/lang/Object;ILjava/lang/Object;)V

    .line 48
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_1

    .line 53
    :cond_3
    move-object v0, v2

    check-cast v0, [Loxu;

    move-object v1, v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 54
    :catch_0
    move-exception v1

    .line 55
    const-string v2, "MessageNanoList"

    const-string v4, "Failed to deserialize messages"

    invoke-static {v2, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v1, v3

    .line 56
    goto :goto_0

    .line 57
    :catch_1
    move-exception v1

    .line 58
    const-string v2, "MessageNanoList"

    const-string v4, "Failed to access class when deserializing"

    invoke-static {v2, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v1, v3

    .line 59
    goto :goto_0

    .line 60
    :catch_2
    move-exception v1

    .line 61
    const-string v2, "MessageNanoList"

    const-string v4, "Failed to instantiate message when deserializing"

    invoke-static {v2, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v1, v3

    .line 62
    goto :goto_0
.end method
