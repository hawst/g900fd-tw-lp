.class public final Lmjl;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lnfu;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 27376
    invoke-direct {p0}, Loxq;-><init>()V

    .line 27379
    iput-object v0, p0, Lmjl;->apiHeader:Llyr;

    .line 27382
    iput-object v0, p0, Lmjl;->a:Lnfu;

    .line 27376
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 27399
    const/4 v0, 0x0

    .line 27400
    iget-object v1, p0, Lmjl;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 27401
    const/4 v0, 0x1

    iget-object v1, p0, Lmjl;->apiHeader:Llyr;

    .line 27402
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 27404
    :cond_0
    iget-object v1, p0, Lmjl;->a:Lnfu;

    if-eqz v1, :cond_1

    .line 27405
    const/4 v1, 0x2

    iget-object v2, p0, Lmjl;->a:Lnfu;

    .line 27406
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 27408
    :cond_1
    iget-object v1, p0, Lmjl;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 27409
    iput v0, p0, Lmjl;->ai:I

    .line 27410
    return v0
.end method

.method public a(Loxn;)Lmjl;
    .locals 2

    .prologue
    .line 27418
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 27419
    sparse-switch v0, :sswitch_data_0

    .line 27423
    iget-object v1, p0, Lmjl;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 27424
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmjl;->ah:Ljava/util/List;

    .line 27427
    :cond_1
    iget-object v1, p0, Lmjl;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 27429
    :sswitch_0
    return-object p0

    .line 27434
    :sswitch_1
    iget-object v0, p0, Lmjl;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 27435
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmjl;->apiHeader:Llyr;

    .line 27437
    :cond_2
    iget-object v0, p0, Lmjl;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 27441
    :sswitch_2
    iget-object v0, p0, Lmjl;->a:Lnfu;

    if-nez v0, :cond_3

    .line 27442
    new-instance v0, Lnfu;

    invoke-direct {v0}, Lnfu;-><init>()V

    iput-object v0, p0, Lmjl;->a:Lnfu;

    .line 27444
    :cond_3
    iget-object v0, p0, Lmjl;->a:Lnfu;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 27419
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 27387
    iget-object v0, p0, Lmjl;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 27388
    const/4 v0, 0x1

    iget-object v1, p0, Lmjl;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 27390
    :cond_0
    iget-object v0, p0, Lmjl;->a:Lnfu;

    if-eqz v0, :cond_1

    .line 27391
    const/4 v0, 0x2

    iget-object v1, p0, Lmjl;->a:Lnfu;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 27393
    :cond_1
    iget-object v0, p0, Lmjl;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 27395
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 27372
    invoke-virtual {p0, p1}, Lmjl;->a(Loxn;)Lmjl;

    move-result-object v0

    return-object v0
.end method
