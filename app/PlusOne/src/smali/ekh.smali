.class public final Lekh;
.super Lhyd;
.source "PG"


# instance fields
.field private synthetic e:Lekd;


# direct methods
.method public constructor <init>(Lekd;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 161
    iput-object p1, p0, Lekh;->e:Lekd;

    .line 162
    invoke-direct {p0, p2, p3}, Lhyd;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    .line 163
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 213
    iget-object v0, p0, Lekh;->e:Lekd;

    invoke-virtual {v0, v2}, Lekd;->a_(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040164

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 12

    .prologue
    const/4 v8, 0x0

    const/4 v6, 0x0

    const/4 v3, 0x1

    .line 172
    check-cast p1, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;

    .line 175
    const/4 v0, 0x7

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v3, :cond_0

    move v1, v3

    .line 178
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/PeopleListRowView;

    .line 179
    iget-object v2, p0, Lekh;->e:Lekd;

    iget-object v4, p0, Lekh;->e:Lekd;

    iget-object v4, v4, Lekd;->U:Lhxh;

    invoke-virtual {v0, v2, v4, v1}, Lcom/google/android/apps/plus/views/PeopleListRowView;->a(Lfze;Lhxh;Z)V

    .line 181
    invoke-interface {p3, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 182
    const/4 v2, 0x2

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 183
    const/4 v4, 0x3

    invoke-interface {p3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 184
    const/4 v5, 0x6

    invoke-interface {p3, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    if-eqz v5, :cond_1

    move v5, v3

    .line 185
    :goto_1
    const/16 v7, 0x8

    .line 186
    invoke-interface {p3, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    if-eqz v7, :cond_2

    move v9, v3

    .line 187
    :goto_2
    const/16 v7, 0x9

    invoke-interface {p3, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    if-eqz v7, :cond_3

    move v10, v3

    .line 191
    :goto_3
    if-eqz v5, :cond_4

    .line 193
    const-string v7, "15"

    move-object v3, v8

    .line 200
    :goto_4
    if-eqz v9, :cond_5

    iget-object v5, p0, Lekh;->e:Lekd;

    iget-object v5, v5, Lekd;->W:Ljava/lang/String;

    :goto_5
    iget-object v11, p0, Lekh;->e:Lekd;

    iget-object v11, v11, Lekd;->Y:Lfuw;

    invoke-virtual/range {v0 .. v11}, Lcom/google/android/apps/plus/views/PeopleListRowView;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;ZZLfuw;)V

    .line 204
    iget-object v1, p0, Lekh;->e:Lekd;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/PeopleListRowView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 206
    invoke-interface {p3}, Landroid/database/Cursor;->getPosition()I

    .line 207
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->c()V

    .line 209
    return-void

    :cond_0
    move v1, v6

    .line 175
    goto :goto_0

    :cond_1
    move v5, v6

    .line 184
    goto :goto_1

    :cond_2
    move v9, v6

    .line 186
    goto :goto_2

    :cond_3
    move v10, v6

    .line 187
    goto :goto_3

    .line 195
    :cond_4
    const/4 v3, 0x4

    .line 196
    invoke-interface {p3, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 195
    invoke-static {v3}, Lhst;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 197
    const/4 v5, 0x5

    invoke-interface {p3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    goto :goto_4

    :cond_5
    move-object v5, v8

    .line 200
    goto :goto_5
.end method

.method public isEnabled(I)Z
    .locals 1

    .prologue
    .line 167
    const/4 v0, 0x0

    return v0
.end method
