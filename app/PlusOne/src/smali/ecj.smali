.class final Lecj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lyp;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<+",
            "Ljuf;",
            ">;"
        }
    .end annotation
.end field

.field private final c:I

.field private final d:I

.field private final e:Landroid/app/Activity;

.field private final f:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/app/Activity;ILjava/lang/String;ILjava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/app/Activity;",
            "I",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/ArrayList",
            "<+",
            "Ljuf;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1072
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1073
    iput-object p1, p0, Lecj;->f:Landroid/content/Context;

    .line 1074
    iput-object p2, p0, Lecj;->e:Landroid/app/Activity;

    .line 1075
    iput-object p4, p0, Lecj;->a:Ljava/lang/String;

    .line 1076
    iput-object p6, p0, Lecj;->b:Ljava/util/ArrayList;

    .line 1077
    iput p3, p0, Lecj;->c:I

    .line 1078
    iput p5, p0, Lecj;->d:I

    .line 1079
    return-void
.end method


# virtual methods
.method public a(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1083
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f100671

    if-ne v0, v1, :cond_1

    .line 1084
    iget-object v0, p0, Lecj;->f:Landroid/content/Context;

    iget v1, p0, Lecj;->c:I

    iget-object v2, p0, Lecj;->b:Ljava/util/ArrayList;

    invoke-static {v0, v1, v2}, Leyq;->a(Landroid/content/Context;ILjava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v0

    .line 1085
    iget-object v1, p0, Lecj;->e:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 1086
    iget-object v0, p0, Lecj;->e:Landroid/app/Activity;

    const v1, 0x7f05001e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->overridePendingTransition(II)V

    .line 1096
    :cond_0
    :goto_0
    return v3

    .line 1087
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f100672

    if-ne v0, v1, :cond_0

    .line 1088
    iget-object v0, p0, Lecj;->e:Landroid/app/Activity;

    iget v1, p0, Lecj;->c:I

    invoke-static {v0, v1}, Leyq;->g(Landroid/content/Context;I)Ljuj;

    move-result-object v0

    iget-object v1, p0, Lecj;->a:Ljava/lang/String;

    .line 1089
    invoke-virtual {v0, v1}, Ljuj;->a(Ljava/lang/String;)Ljuj;

    move-result-object v0

    const/4 v1, 0x4

    .line 1090
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljuj;->a(Ljava/lang/Integer;)Ljuj;

    move-result-object v0

    iget v1, p0, Lecj;->d:I

    .line 1091
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljuj;->b(Ljava/lang/Integer;)Ljuj;

    move-result-object v0

    .line 1092
    invoke-virtual {v0, v3}, Ljuj;->d(Z)Ljuj;

    move-result-object v0

    .line 1093
    invoke-virtual {v0}, Ljuj;->a()Landroid/content/Intent;

    move-result-object v0

    .line 1094
    iget-object v1, p0, Lecj;->e:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
