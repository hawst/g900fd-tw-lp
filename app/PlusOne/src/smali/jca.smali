.class public final Ljca;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Landroid/content/Intent;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/libraries/social/mediapicker/MediaPickerActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v0, p0, Ljca;->a:Landroid/content/Intent;

    .line 96
    return-void
.end method


# virtual methods
.method public a()Ljca;
    .locals 3

    .prologue
    .line 110
    iget-object v0, p0, Ljca;->a:Landroid/content/Intent;

    const-string v1, "media_picker_mode"

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 112
    return-object p0
.end method

.method public a(I)Ljca;
    .locals 2

    .prologue
    .line 122
    iget-object v0, p0, Ljca;->a:Landroid/content/Intent;

    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 123
    return-object p0
.end method

.method public a(Ljava/lang/String;)Ljca;
    .locals 2

    .prologue
    .line 99
    iget-object v0, p0, Ljca;->a:Landroid/content/Intent;

    const-string v1, "header_text"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 100
    return-object p0
.end method

.method public b()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Ljca;->a:Landroid/content/Intent;

    return-object v0
.end method
