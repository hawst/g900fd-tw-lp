.class public final Lnoy;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnoy;


# instance fields
.field public b:Ljava/lang/String;

.field public c:I

.field public d:Ljava/lang/Boolean;

.field private e:I

.field private f:I

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/Boolean;

.field private i:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1125
    const/4 v0, 0x0

    new-array v0, v0, [Lnoy;

    sput-object v0, Lnoy;->a:[Lnoy;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/high16 v0, -0x80000000

    .line 1126
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1129
    iput v0, p0, Lnoy;->e:I

    .line 1132
    iput v0, p0, Lnoy;->f:I

    .line 1141
    iput v0, p0, Lnoy;->c:I

    .line 1126
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    const/high16 v3, -0x80000000

    .line 1180
    const/4 v0, 0x0

    .line 1181
    iget v1, p0, Lnoy;->e:I

    if-eq v1, v3, :cond_0

    .line 1182
    const/4 v0, 0x1

    iget v1, p0, Lnoy;->e:I

    .line 1183
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1185
    :cond_0
    iget-object v1, p0, Lnoy;->h:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 1186
    const/4 v1, 0x2

    iget-object v2, p0, Lnoy;->h:Ljava/lang/Boolean;

    .line 1187
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1189
    :cond_1
    iget v1, p0, Lnoy;->f:I

    if-eq v1, v3, :cond_2

    .line 1190
    const/4 v1, 0x3

    iget v2, p0, Lnoy;->f:I

    .line 1191
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1193
    :cond_2
    iget-object v1, p0, Lnoy;->b:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 1194
    const/4 v1, 0x4

    iget-object v2, p0, Lnoy;->b:Ljava/lang/String;

    .line 1195
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1197
    :cond_3
    iget v1, p0, Lnoy;->c:I

    if-eq v1, v3, :cond_4

    .line 1198
    const/4 v1, 0x5

    iget v2, p0, Lnoy;->c:I

    .line 1199
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1201
    :cond_4
    iget-object v1, p0, Lnoy;->i:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    .line 1202
    const/4 v1, 0x6

    iget-object v2, p0, Lnoy;->i:Ljava/lang/Boolean;

    .line 1203
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1205
    :cond_5
    iget-object v1, p0, Lnoy;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    .line 1206
    const/4 v1, 0x7

    iget-object v2, p0, Lnoy;->d:Ljava/lang/Boolean;

    .line 1207
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1209
    :cond_6
    iget-object v1, p0, Lnoy;->g:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 1210
    const/16 v1, 0x8

    iget-object v2, p0, Lnoy;->g:Ljava/lang/String;

    .line 1211
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1213
    :cond_7
    iget-object v1, p0, Lnoy;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1214
    iput v0, p0, Lnoy;->ai:I

    .line 1215
    return v0
.end method

.method public a(Loxn;)Lnoy;
    .locals 7

    .prologue
    const/4 v6, 0x7

    const/4 v5, 0x4

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 1223
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1224
    sparse-switch v0, :sswitch_data_0

    .line 1228
    iget-object v1, p0, Lnoy;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1229
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnoy;->ah:Ljava/util/List;

    .line 1232
    :cond_1
    iget-object v1, p0, Lnoy;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1234
    :sswitch_0
    return-object p0

    .line 1239
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1240
    if-eq v0, v2, :cond_2

    if-eq v0, v3, :cond_2

    if-eq v0, v4, :cond_2

    if-eq v0, v5, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    if-eq v0, v6, :cond_2

    const/16 v1, 0x8

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa

    if-eq v0, v1, :cond_2

    const/16 v1, 0xc

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd

    if-eq v0, v1, :cond_2

    const/16 v1, 0xe

    if-eq v0, v1, :cond_2

    const/16 v1, 0xf

    if-eq v0, v1, :cond_2

    const/16 v1, 0x10

    if-eq v0, v1, :cond_2

    const/16 v1, 0x12

    if-eq v0, v1, :cond_2

    const/16 v1, 0x14

    if-eq v0, v1, :cond_2

    const/16 v1, 0x15

    if-eq v0, v1, :cond_2

    const/16 v1, 0x16

    if-eq v0, v1, :cond_2

    const/16 v1, 0x17

    if-eq v0, v1, :cond_2

    const/16 v1, 0x18

    if-eq v0, v1, :cond_2

    const/16 v1, 0x19

    if-eq v0, v1, :cond_2

    const/16 v1, 0x1a

    if-eq v0, v1, :cond_2

    const/16 v1, 0x1b

    if-eq v0, v1, :cond_2

    const/16 v1, 0x1c

    if-eq v0, v1, :cond_2

    const/16 v1, 0x1d

    if-eq v0, v1, :cond_2

    const/16 v1, 0x1e

    if-eq v0, v1, :cond_2

    const/16 v1, 0x1f

    if-eq v0, v1, :cond_2

    const/16 v1, 0x20

    if-eq v0, v1, :cond_2

    const/16 v1, 0x21

    if-eq v0, v1, :cond_2

    const/16 v1, 0x22

    if-eq v0, v1, :cond_2

    const/16 v1, 0x23

    if-eq v0, v1, :cond_2

    const/16 v1, 0x24

    if-eq v0, v1, :cond_2

    const/16 v1, 0x25

    if-eq v0, v1, :cond_2

    const/16 v1, 0x26

    if-eq v0, v1, :cond_2

    const/16 v1, 0x27

    if-eq v0, v1, :cond_2

    const/16 v1, 0x28

    if-eq v0, v1, :cond_2

    const/16 v1, 0x29

    if-eq v0, v1, :cond_2

    const/16 v1, 0x2a

    if-eq v0, v1, :cond_2

    const/16 v1, 0x2b

    if-eq v0, v1, :cond_2

    const/16 v1, 0x2c

    if-eq v0, v1, :cond_2

    const/16 v1, 0x2d

    if-eq v0, v1, :cond_2

    const/16 v1, 0x2e

    if-eq v0, v1, :cond_2

    const/16 v1, 0x2f

    if-eq v0, v1, :cond_2

    const/16 v1, 0x30

    if-eq v0, v1, :cond_2

    const/16 v1, 0x31

    if-eq v0, v1, :cond_2

    const/16 v1, 0x32

    if-eq v0, v1, :cond_2

    const/16 v1, 0x33

    if-eq v0, v1, :cond_2

    const/16 v1, 0x34

    if-eq v0, v1, :cond_2

    const/16 v1, 0x35

    if-eq v0, v1, :cond_2

    const/16 v1, 0x36

    if-eq v0, v1, :cond_2

    const/16 v1, 0x37

    if-eq v0, v1, :cond_2

    const/16 v1, 0x38

    if-eq v0, v1, :cond_2

    const/16 v1, 0x39

    if-eq v0, v1, :cond_2

    const/16 v1, 0x3a

    if-eq v0, v1, :cond_2

    const/16 v1, 0x3b

    if-eq v0, v1, :cond_2

    const/16 v1, 0x3c

    if-eq v0, v1, :cond_2

    const/16 v1, 0x3d

    if-eq v0, v1, :cond_2

    const/16 v1, 0x3e

    if-eq v0, v1, :cond_2

    const/16 v1, 0x3f

    if-eq v0, v1, :cond_2

    const/16 v1, 0x40

    if-eq v0, v1, :cond_2

    const/16 v1, 0x41

    if-eq v0, v1, :cond_2

    const/16 v1, 0x42

    if-eq v0, v1, :cond_2

    const/16 v1, 0x43

    if-eq v0, v1, :cond_2

    const/16 v1, 0x45

    if-eq v0, v1, :cond_2

    const/16 v1, 0x46

    if-eq v0, v1, :cond_2

    const/16 v1, 0x47

    if-eq v0, v1, :cond_2

    const/16 v1, 0x48

    if-eq v0, v1, :cond_2

    const/16 v1, 0x49

    if-eq v0, v1, :cond_2

    const/16 v1, 0x4a

    if-eq v0, v1, :cond_2

    const/16 v1, 0x4b

    if-eq v0, v1, :cond_2

    const/16 v1, 0x4c

    if-eq v0, v1, :cond_2

    const/16 v1, 0x4d

    if-eq v0, v1, :cond_2

    const/16 v1, 0x4e

    if-eq v0, v1, :cond_2

    const/16 v1, 0x50

    if-eq v0, v1, :cond_2

    const/16 v1, 0x51

    if-eq v0, v1, :cond_2

    const/16 v1, 0x52

    if-eq v0, v1, :cond_2

    const/16 v1, 0x53

    if-eq v0, v1, :cond_2

    const/16 v1, 0x54

    if-eq v0, v1, :cond_2

    const/16 v1, 0x55

    if-eq v0, v1, :cond_2

    const/16 v1, 0x56

    if-eq v0, v1, :cond_2

    const/16 v1, 0x57

    if-eq v0, v1, :cond_2

    const/16 v1, 0x58

    if-eq v0, v1, :cond_2

    const/16 v1, 0x59

    if-eq v0, v1, :cond_2

    const/16 v1, 0x5a

    if-eq v0, v1, :cond_2

    const/16 v1, 0x5b

    if-eq v0, v1, :cond_2

    const/16 v1, 0x5c

    if-eq v0, v1, :cond_2

    const/16 v1, 0x5d

    if-eq v0, v1, :cond_2

    const/16 v1, 0x5e

    if-eq v0, v1, :cond_2

    const/16 v1, 0x5f

    if-eq v0, v1, :cond_2

    const/16 v1, 0x60

    if-eq v0, v1, :cond_2

    const/16 v1, 0x61

    if-ne v0, v1, :cond_3

    .line 1331
    :cond_2
    iput v0, p0, Lnoy;->e:I

    goto/16 :goto_0

    .line 1333
    :cond_3
    iput v2, p0, Lnoy;->e:I

    goto/16 :goto_0

    .line 1338
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnoy;->h:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 1342
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1343
    if-eq v0, v2, :cond_4

    if-eq v0, v3, :cond_4

    if-eq v0, v4, :cond_4

    if-eq v0, v5, :cond_4

    const/4 v1, 0x5

    if-eq v0, v1, :cond_4

    if-eq v0, v6, :cond_4

    const/16 v1, 0x8

    if-eq v0, v1, :cond_4

    const/16 v1, 0x9

    if-eq v0, v1, :cond_4

    const/16 v1, 0xa

    if-eq v0, v1, :cond_4

    const/16 v1, 0xb

    if-eq v0, v1, :cond_4

    const/16 v1, 0xc

    if-eq v0, v1, :cond_4

    const/16 v1, 0xd

    if-eq v0, v1, :cond_4

    const/16 v1, 0xe

    if-eq v0, v1, :cond_4

    const/16 v1, 0xf

    if-eq v0, v1, :cond_4

    const/16 v1, 0x10

    if-eq v0, v1, :cond_4

    const/16 v1, 0x12

    if-eq v0, v1, :cond_4

    const/16 v1, 0x13

    if-ne v0, v1, :cond_5

    .line 1360
    :cond_4
    iput v0, p0, Lnoy;->f:I

    goto/16 :goto_0

    .line 1362
    :cond_5
    iput v2, p0, Lnoy;->f:I

    goto/16 :goto_0

    .line 1367
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnoy;->b:Ljava/lang/String;

    goto/16 :goto_0

    .line 1371
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1372
    if-eq v0, v2, :cond_6

    if-eq v0, v3, :cond_6

    if-eq v0, v4, :cond_6

    if-eq v0, v5, :cond_6

    const/4 v1, 0x5

    if-eq v0, v1, :cond_6

    const/4 v1, 0x6

    if-eq v0, v1, :cond_6

    if-eq v0, v6, :cond_6

    const/16 v1, 0x8

    if-eq v0, v1, :cond_6

    const/16 v1, 0x9

    if-ne v0, v1, :cond_7

    .line 1381
    :cond_6
    iput v0, p0, Lnoy;->c:I

    goto/16 :goto_0

    .line 1383
    :cond_7
    iput v2, p0, Lnoy;->c:I

    goto/16 :goto_0

    .line 1388
    :sswitch_6
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnoy;->i:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 1392
    :sswitch_7
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnoy;->d:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 1396
    :sswitch_8
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnoy;->g:Ljava/lang/String;

    goto/16 :goto_0

    .line 1224
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 1150
    iget v0, p0, Lnoy;->e:I

    if-eq v0, v2, :cond_0

    .line 1151
    const/4 v0, 0x1

    iget v1, p0, Lnoy;->e:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1153
    :cond_0
    iget-object v0, p0, Lnoy;->h:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 1154
    const/4 v0, 0x2

    iget-object v1, p0, Lnoy;->h:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 1156
    :cond_1
    iget v0, p0, Lnoy;->f:I

    if-eq v0, v2, :cond_2

    .line 1157
    const/4 v0, 0x3

    iget v1, p0, Lnoy;->f:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1159
    :cond_2
    iget-object v0, p0, Lnoy;->b:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1160
    const/4 v0, 0x4

    iget-object v1, p0, Lnoy;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1162
    :cond_3
    iget v0, p0, Lnoy;->c:I

    if-eq v0, v2, :cond_4

    .line 1163
    const/4 v0, 0x5

    iget v1, p0, Lnoy;->c:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1165
    :cond_4
    iget-object v0, p0, Lnoy;->i:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 1166
    const/4 v0, 0x6

    iget-object v1, p0, Lnoy;->i:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 1168
    :cond_5
    iget-object v0, p0, Lnoy;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    .line 1169
    const/4 v0, 0x7

    iget-object v1, p0, Lnoy;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 1171
    :cond_6
    iget-object v0, p0, Lnoy;->g:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 1172
    const/16 v0, 0x8

    iget-object v1, p0, Lnoy;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1174
    :cond_7
    iget-object v0, p0, Lnoy;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1176
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1122
    invoke-virtual {p0, p1}, Lnoy;->a(Loxn;)Lnoy;

    move-result-object v0

    return-object v0
.end method
