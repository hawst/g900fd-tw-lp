.class public final Lmui;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Boolean;

.field private b:Ljava/lang/Boolean;

.field private c:Ljava/lang/Integer;

.field private d:Ljava/lang/Integer;

.field private e:Ljava/lang/Integer;

.field private f:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8574
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 8615
    const/4 v0, 0x0

    .line 8616
    iget-object v1, p0, Lmui;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 8617
    const/4 v0, 0x1

    iget-object v1, p0, Lmui;->a:Ljava/lang/Boolean;

    .line 8618
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 8620
    :cond_0
    iget-object v1, p0, Lmui;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 8621
    const/4 v1, 0x2

    iget-object v2, p0, Lmui;->b:Ljava/lang/Boolean;

    .line 8622
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 8624
    :cond_1
    iget-object v1, p0, Lmui;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 8625
    const/4 v1, 0x3

    iget-object v2, p0, Lmui;->c:Ljava/lang/Integer;

    .line 8626
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 8628
    :cond_2
    iget-object v1, p0, Lmui;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 8629
    const/4 v1, 0x4

    iget-object v2, p0, Lmui;->d:Ljava/lang/Integer;

    .line 8630
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 8632
    :cond_3
    iget-object v1, p0, Lmui;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 8633
    const/4 v1, 0x5

    iget-object v2, p0, Lmui;->e:Ljava/lang/Integer;

    .line 8634
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 8636
    :cond_4
    iget-object v1, p0, Lmui;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 8637
    const/4 v1, 0x6

    iget-object v2, p0, Lmui;->f:Ljava/lang/Integer;

    .line 8638
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 8640
    :cond_5
    iget-object v1, p0, Lmui;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8641
    iput v0, p0, Lmui;->ai:I

    .line 8642
    return v0
.end method

.method public a(Loxn;)Lmui;
    .locals 2

    .prologue
    .line 8650
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 8651
    sparse-switch v0, :sswitch_data_0

    .line 8655
    iget-object v1, p0, Lmui;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 8656
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmui;->ah:Ljava/util/List;

    .line 8659
    :cond_1
    iget-object v1, p0, Lmui;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 8661
    :sswitch_0
    return-object p0

    .line 8666
    :sswitch_1
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmui;->a:Ljava/lang/Boolean;

    goto :goto_0

    .line 8670
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmui;->b:Ljava/lang/Boolean;

    goto :goto_0

    .line 8674
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmui;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 8678
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmui;->d:Ljava/lang/Integer;

    goto :goto_0

    .line 8682
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmui;->e:Ljava/lang/Integer;

    goto :goto_0

    .line 8686
    :sswitch_6
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmui;->f:Ljava/lang/Integer;

    goto :goto_0

    .line 8651
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 8591
    iget-object v0, p0, Lmui;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 8592
    const/4 v0, 0x1

    iget-object v1, p0, Lmui;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 8594
    :cond_0
    iget-object v0, p0, Lmui;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 8595
    const/4 v0, 0x2

    iget-object v1, p0, Lmui;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 8597
    :cond_1
    iget-object v0, p0, Lmui;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 8598
    const/4 v0, 0x3

    iget-object v1, p0, Lmui;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 8600
    :cond_2
    iget-object v0, p0, Lmui;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 8601
    const/4 v0, 0x4

    iget-object v1, p0, Lmui;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 8603
    :cond_3
    iget-object v0, p0, Lmui;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 8604
    const/4 v0, 0x5

    iget-object v1, p0, Lmui;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 8606
    :cond_4
    iget-object v0, p0, Lmui;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 8607
    const/4 v0, 0x6

    iget-object v1, p0, Lmui;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 8609
    :cond_5
    iget-object v0, p0, Lmui;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 8611
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 8570
    invoke-virtual {p0, p1}, Lmui;->a(Loxn;)Lmui;

    move-result-object v0

    return-object v0
.end method
