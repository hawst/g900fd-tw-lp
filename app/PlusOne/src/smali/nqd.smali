.class public final Lnqd;
.super Loxq;
.source "PG"


# instance fields
.field public a:I

.field private b:Ljava/lang/Boolean;

.field private c:Lnqf;

.field private d:Lnqf;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 657
    invoke-direct {p0}, Loxq;-><init>()V

    .line 668
    const/high16 v0, -0x80000000

    iput v0, p0, Lnqd;->a:I

    .line 673
    iput-object v1, p0, Lnqd;->c:Lnqf;

    .line 676
    iput-object v1, p0, Lnqd;->d:Lnqf;

    .line 657
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 699
    const/4 v0, 0x0

    .line 700
    iget v1, p0, Lnqd;->a:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 701
    const/4 v0, 0x1

    iget v1, p0, Lnqd;->a:I

    .line 702
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 704
    :cond_0
    iget-object v1, p0, Lnqd;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 705
    const/4 v1, 0x2

    iget-object v2, p0, Lnqd;->b:Ljava/lang/Boolean;

    .line 706
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 708
    :cond_1
    iget-object v1, p0, Lnqd;->c:Lnqf;

    if-eqz v1, :cond_2

    .line 709
    const/4 v1, 0x3

    iget-object v2, p0, Lnqd;->c:Lnqf;

    .line 710
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 712
    :cond_2
    iget-object v1, p0, Lnqd;->d:Lnqf;

    if-eqz v1, :cond_3

    .line 713
    const/4 v1, 0x4

    iget-object v2, p0, Lnqd;->d:Lnqf;

    .line 714
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 716
    :cond_3
    iget-object v1, p0, Lnqd;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 717
    iput v0, p0, Lnqd;->ai:I

    .line 718
    return v0
.end method

.method public a(Loxn;)Lnqd;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 726
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 727
    sparse-switch v0, :sswitch_data_0

    .line 731
    iget-object v1, p0, Lnqd;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 732
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnqd;->ah:Ljava/util/List;

    .line 735
    :cond_1
    iget-object v1, p0, Lnqd;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 737
    :sswitch_0
    return-object p0

    .line 742
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 743
    if-eq v0, v2, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-ne v0, v1, :cond_3

    .line 748
    :cond_2
    iput v0, p0, Lnqd;->a:I

    goto :goto_0

    .line 750
    :cond_3
    iput v2, p0, Lnqd;->a:I

    goto :goto_0

    .line 755
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnqd;->b:Ljava/lang/Boolean;

    goto :goto_0

    .line 759
    :sswitch_3
    iget-object v0, p0, Lnqd;->c:Lnqf;

    if-nez v0, :cond_4

    .line 760
    new-instance v0, Lnqf;

    invoke-direct {v0}, Lnqf;-><init>()V

    iput-object v0, p0, Lnqd;->c:Lnqf;

    .line 762
    :cond_4
    iget-object v0, p0, Lnqd;->c:Lnqf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 766
    :sswitch_4
    iget-object v0, p0, Lnqd;->d:Lnqf;

    if-nez v0, :cond_5

    .line 767
    new-instance v0, Lnqf;

    invoke-direct {v0}, Lnqf;-><init>()V

    iput-object v0, p0, Lnqd;->d:Lnqf;

    .line 769
    :cond_5
    iget-object v0, p0, Lnqd;->d:Lnqf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 727
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 681
    iget v0, p0, Lnqd;->a:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 682
    const/4 v0, 0x1

    iget v1, p0, Lnqd;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 684
    :cond_0
    iget-object v0, p0, Lnqd;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 685
    const/4 v0, 0x2

    iget-object v1, p0, Lnqd;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 687
    :cond_1
    iget-object v0, p0, Lnqd;->c:Lnqf;

    if-eqz v0, :cond_2

    .line 688
    const/4 v0, 0x3

    iget-object v1, p0, Lnqd;->c:Lnqf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 690
    :cond_2
    iget-object v0, p0, Lnqd;->d:Lnqf;

    if-eqz v0, :cond_3

    .line 691
    const/4 v0, 0x4

    iget-object v1, p0, Lnqd;->d:Lnqf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 693
    :cond_3
    iget-object v0, p0, Lnqd;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 695
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 653
    invoke-virtual {p0, p1}, Lnqd;->a(Loxn;)Lnqd;

    move-result-object v0

    return-object v0
.end method
