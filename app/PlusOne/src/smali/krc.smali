.class public final Lkrc;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:I

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;


# direct methods
.method private constructor <init>(ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    iput p1, p0, Lkrc;->a:I

    .line 81
    iput-object p2, p0, Lkrc;->b:Ljava/lang/String;

    .line 82
    iput-object p3, p0, Lkrc;->c:Ljava/lang/String;

    .line 83
    return-void
.end method

.method public static a(ILjava/lang/String;)Lkrc;
    .locals 2

    .prologue
    .line 63
    new-instance v0, Lkrc;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lkrc;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 87
    instance-of v1, p1, Lkrb;

    if-eqz v1, :cond_0

    .line 88
    check-cast p1, Lkrc;

    .line 89
    iget v1, p0, Lkrc;->a:I

    iget v2, p1, Lkrc;->a:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lkrc;->b:Ljava/lang/String;

    iget-object v2, p1, Lkrc;->b:Ljava/lang/String;

    .line 90
    invoke-static {v1, v2}, Llsh;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lkrc;->c:Ljava/lang/String;

    iget-object v2, p1, Lkrc;->c:Ljava/lang/String;

    .line 91
    invoke-static {v1, v2}, Llsh;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 93
    :cond_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 98
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lkrc;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lkrc;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lkrc;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    invoke-super {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
