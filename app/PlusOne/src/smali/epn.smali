.class public final enum Lepn;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lepn;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lepn;

.field public static final enum b:Lepn;

.field public static final enum c:Lepn;

.field public static final enum d:Lepn;

.field public static final enum e:Lepn;

.field public static final enum f:Lepn;

.field public static final enum g:Lepn;

.field public static final enum h:Lepn;

.field public static final enum i:Lepn;

.field public static final enum j:Lepn;

.field private static enum k:Lepn;

.field private static final synthetic o:[Lepn;


# instance fields
.field private final l:I

.field private final m:I

.field private final n:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/16 v9, 0xa

    const/4 v2, 0x0

    .line 28
    new-instance v0, Lepn;

    const-string v1, "FindMyFace"

    const/16 v3, 0x3c

    const-string v5, "FMF"

    move v4, v2

    invoke-direct/range {v0 .. v5}, Lepn;-><init>(Ljava/lang/String;IIILjava/lang/String;)V

    sput-object v0, Lepn;->a:Lepn;

    .line 29
    new-instance v3, Lepn;

    const-string v4, "SignIn"

    const-string v8, "SIGNIN"

    move v5, v10

    move v6, v2

    move v7, v10

    invoke-direct/range {v3 .. v8}, Lepn;-><init>(Ljava/lang/String;IIILjava/lang/String;)V

    sput-object v3, Lepn;->b:Lepn;

    .line 30
    new-instance v3, Lepn;

    const-string v4, "PhotosInDrive"

    const/16 v6, 0x28

    const-string v8, "DRIVE"

    move v5, v11

    move v7, v11

    invoke-direct/range {v3 .. v8}, Lepn;-><init>(Ljava/lang/String;IIILjava/lang/String;)V

    sput-object v3, Lepn;->c:Lepn;

    .line 31
    new-instance v3, Lepn;

    const-string v4, "AutoBackup"

    const/16 v6, 0x14

    const-string v8, "AUTO_BACKUP"

    move v5, v12

    move v7, v12

    invoke-direct/range {v3 .. v8}, Lepn;-><init>(Ljava/lang/String;IIILjava/lang/String;)V

    sput-object v3, Lepn;->d:Lepn;

    .line 32
    new-instance v3, Lepn;

    const-string v4, "AutoAwesomeMovie"

    const/4 v5, 0x4

    const/16 v6, 0x46

    const/4 v7, 0x4

    const-string v8, "NEW_AAM"

    invoke-direct/range {v3 .. v8}, Lepn;-><init>(Ljava/lang/String;IIILjava/lang/String;)V

    sput-object v3, Lepn;->e:Lepn;

    .line 33
    new-instance v3, Lepn;

    const-string v4, "FolderAutoBackup"

    const/4 v5, 0x5

    const/16 v6, 0x1e

    const/4 v7, 0x5

    const-string v8, "LOCAL_FOLDER_AUTO_BACKUP"

    invoke-direct/range {v3 .. v8}, Lepn;-><init>(Ljava/lang/String;IIILjava/lang/String;)V

    sput-object v3, Lepn;->k:Lepn;

    .line 34
    new-instance v3, Lepn;

    const-string v4, "Story"

    const/4 v5, 0x6

    const/16 v6, 0x32

    const/4 v7, 0x6

    const-string v8, "STORY"

    invoke-direct/range {v3 .. v8}, Lepn;-><init>(Ljava/lang/String;IIILjava/lang/String;)V

    sput-object v3, Lepn;->f:Lepn;

    .line 35
    new-instance v3, Lepn;

    const-string v4, "StoryWelcome"

    const/4 v5, 0x7

    const/4 v7, 0x7

    const-string v8, "STORY_WELCOME"

    move v6, v9

    invoke-direct/range {v3 .. v8}, Lepn;-><init>(Ljava/lang/String;IIILjava/lang/String;)V

    sput-object v3, Lepn;->g:Lepn;

    .line 36
    new-instance v3, Lepn;

    const-string v4, "Location"

    const/16 v5, 0x8

    const/16 v6, 0x5a

    const/16 v7, 0x8

    const-string v8, "LOCATION"

    invoke-direct/range {v3 .. v8}, Lepn;-><init>(Ljava/lang/String;IIILjava/lang/String;)V

    sput-object v3, Lepn;->h:Lepn;

    .line 37
    new-instance v3, Lepn;

    const-string v4, "AutoBackupReminder"

    const/16 v5, 0x9

    const/16 v6, 0x19

    const/16 v7, 0x9

    const-string v8, "AUTO_BACKUP_REMINDER"

    invoke-direct/range {v3 .. v8}, Lepn;-><init>(Ljava/lang/String;IIILjava/lang/String;)V

    sput-object v3, Lepn;->i:Lepn;

    .line 38
    new-instance v3, Lepn;

    const-string v4, "StoryAutoBackup"

    const/16 v6, 0x50

    const-string v8, "STORY_AUTO_BACKUP"

    move v5, v9

    move v7, v9

    invoke-direct/range {v3 .. v8}, Lepn;-><init>(Ljava/lang/String;IIILjava/lang/String;)V

    sput-object v3, Lepn;->j:Lepn;

    .line 26
    const/16 v0, 0xb

    new-array v0, v0, [Lepn;

    sget-object v1, Lepn;->a:Lepn;

    aput-object v1, v0, v2

    sget-object v1, Lepn;->b:Lepn;

    aput-object v1, v0, v10

    sget-object v1, Lepn;->c:Lepn;

    aput-object v1, v0, v11

    sget-object v1, Lepn;->d:Lepn;

    aput-object v1, v0, v12

    const/4 v1, 0x4

    sget-object v2, Lepn;->e:Lepn;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lepn;->k:Lepn;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lepn;->f:Lepn;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lepn;->g:Lepn;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lepn;->h:Lepn;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lepn;->i:Lepn;

    aput-object v2, v0, v1

    sget-object v1, Lepn;->j:Lepn;

    aput-object v1, v0, v9

    sput-object v0, Lepn;->o:[Lepn;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 53
    iput p3, p0, Lepn;->l:I

    .line 54
    iput p4, p0, Lepn;->m:I

    .line 55
    iput-object p5, p0, Lepn;->n:Ljava/lang/String;

    .line 56
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lepn;
    .locals 1

    .prologue
    .line 26
    const-class v0, Lepn;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lepn;

    return-object v0
.end method

.method public static values()[Lepn;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lepn;->o:[Lepn;

    invoke-virtual {v0}, [Lepn;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lepn;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 62
    iget v0, p0, Lepn;->l:I

    return v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lepn;->m:I

    return v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lepn;->n:Ljava/lang/String;

    return-object v0
.end method
