.class public abstract Lkfw;
.super Lkff;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<RS:",
        "Loxu;",
        ">",
        "Lkff;"
    }
.end annotation


# instance fields
.field private a:Loxu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TRS;"
        }
    .end annotation
.end field

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private p:Z


# direct methods
.method protected constructor <init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Ljava/lang/String;Loxu;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lkfo;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "TRS;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 48
    .line 52
    invoke-virtual {p2}, Lkfo;->c()Lkey;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v0, Lkfz;

    invoke-virtual {p2}, Lkfo;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p1, v2, p7, v1}, Lkfz;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lkey;)V

    .line 48
    :goto_0
    invoke-direct {p0, p1, p2, p3, v0}, Lkff;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Lkfj;)V

    .line 53
    iput-object p4, p0, Lkfw;->b:Ljava/lang/String;

    .line 54
    iput-object p5, p0, Lkfw;->a:Loxu;

    .line 55
    iput-object p6, p0, Lkfw;->c:Ljava/lang/String;

    .line 56
    return-void

    .line 52
    :cond_0
    new-instance v0, Lkfv;

    invoke-virtual {p2}, Lkfo;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p1, v1, p7}, Lkfv;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public B()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x0

    return-object v0
.end method

.method public C()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lkfw;->b:Ljava/lang/String;

    return-object v0
.end method

.method public D()Loxu;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TRS;"
        }
    .end annotation

    .prologue
    .line 107
    iget-boolean v0, p0, Lkfw;->p:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lkfw;->a:Loxu;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public Q_()Ljava/lang/String;
    .locals 5

    .prologue
    .line 71
    iget-object v0, p0, Lkfw;->f:Landroid/content/Context;

    iget-object v1, p0, Lkfw;->c:Ljava/lang/String;

    invoke-virtual {p0}, Lkfw;->C()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    .line 72
    invoke-virtual {p0}, Lkfw;->B()Landroid/os/Bundle;

    move-result-object v4

    .line 71
    invoke-static {v0, v1, v2, v3, v4}, Lkfy;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLandroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public R_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    const-string v0, "application/x-protobuf"

    return-object v0
.end method

.method public a([B)Loxu;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)TRS;"
        }
    .end annotation

    .prologue
    .line 133
    iget-object v0, p0, Lkfw;->a:Loxu;

    if-eqz v0, :cond_0

    .line 134
    const/4 v0, 0x0

    array-length v1, p1

    invoke-static {p1, v0, v1}, Loxn;->a([BII)Loxn;

    move-result-object v0

    .line 135
    iget-object v1, p0, Lkfw;->a:Loxu;

    invoke-virtual {v1, v0}, Loxu;->b(Loxn;)Loxu;

    .line 136
    const/4 v0, 0x1

    iput-boolean v0, p0, Lkfw;->p:Z

    .line 137
    iget-object v0, p0, Lkfw;->a:Loxu;

    .line 139
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a([BLjava/lang/String;)V
    .locals 1

    .prologue
    .line 112
    invoke-virtual {p0, p1}, Lkfw;->a([B)Loxu;

    .line 114
    invoke-virtual {p0}, Lkfw;->A()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lkfw;->a:Loxu;

    invoke-static {v0}, Loxv;->a(Loxu;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lkfw;->d([BLjava/lang/String;)V

    .line 118
    :cond_0
    iget-object v0, p0, Lkfw;->a:Loxu;

    invoke-virtual {p0, v0}, Lkfw;->a_(Loxu;)V

    .line 119
    return-void
.end method

.method public a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 144
    invoke-virtual {p0}, Lkfw;->q()Lkfj;

    move-result-object v0

    invoke-interface {v0, p1}, Lkfj;->b(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public a_(Loxu;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TRS;)V"
        }
    .end annotation

    .prologue
    .line 101
    return-void
.end method

.method public b([BLjava/lang/String;)V
    .locals 3

    .prologue
    .line 123
    invoke-super {p0, p1, p2}, Lkff;->b([BLjava/lang/String;)V

    .line 124
    const-string v0, "HttpOperation"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 125
    const-string v0, "HttpOperation error: Response follows: \n"

    new-instance v1, Ljava/lang/String;

    const-string v2, "UTF-8"

    invoke-direct {v1, p1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 127
    :cond_0
    :goto_0
    return-void

    .line 125
    :cond_1
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lkfw;->b:Ljava/lang/String;

    return-object v0
.end method
