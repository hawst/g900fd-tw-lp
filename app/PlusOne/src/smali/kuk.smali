.class public final Lkuk;
.super Llol;
.source "PG"

# interfaces
.implements Lhjj;
.implements Lhmm;
.implements Lhmq;
.implements Lhob;
.implements Lktb;
.implements Lktc;
.implements Lllm;
.implements Lze;


# instance fields
.field private final N:Lkuu;

.field private final O:Lkuv;

.field private P:Lhee;

.field private Q:Ljava/lang/String;

.field private R:I

.field private S:Landroid/widget/ListView;

.field private T:Lkvj;

.field private U:Landroid/support/v7/widget/SearchView;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 56
    invoke-direct {p0}, Llol;-><init>()V

    .line 70
    new-instance v0, Lllo;

    iget-object v1, p0, Lkuk;->av:Llqm;

    invoke-direct {v0, v1, p0}, Lllo;-><init>(Llqr;Lllm;)V

    .line 72
    new-instance v0, Lhmf;

    iget-object v1, p0, Lkuk;->av:Llqm;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lhmf;-><init>(Llqr;B)V

    .line 75
    new-instance v0, Lhje;

    iget-object v1, p0, Lkuk;->av:Llqm;

    invoke-direct {v0, p0, v1, p0}, Lhje;-><init>(Lu;Llqr;Lhjj;)V

    .line 78
    new-instance v0, Lkuu;

    iget-object v1, p0, Lkuk;->av:Llqm;

    invoke-direct {v0, p0, v1}, Lkuu;-><init>(Lu;Llqr;)V

    iput-object v0, p0, Lkuk;->N:Lkuu;

    .line 80
    new-instance v0, Lkuv;

    iget-object v1, p0, Lkuk;->av:Llqm;

    iget-object v2, p0, Lkuk;->N:Lkuu;

    invoke-direct {v0, v1, v2}, Lkuv;-><init>(Llqr;Lkuu;)V

    iput-object v0, p0, Lkuk;->O:Lkuv;

    return-void
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 210
    sget-object v0, Lhmw;->J:Lhmw;

    return-object v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 183
    const v0, 0x7f0401f8

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 184
    const v0, 0x102000a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lkuk;->S:Landroid/widget/ListView;

    .line 185
    iget-object v0, p0, Lkuk;->S:Landroid/widget/ListView;

    iget-object v2, p0, Lkuk;->T:Lkvj;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 186
    return-object v1
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v2, -0x1

    .line 118
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 120
    invoke-virtual {p0}, Lkuk;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 121
    const-string v1, "square_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lkuk;->Q:Ljava/lang/String;

    .line 123
    if-eqz p1, :cond_0

    .line 124
    const-string v0, "membership_status"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lkuk;->R:I

    .line 131
    :goto_0
    invoke-virtual {p0}, Lkuk;->w()Lbb;

    move-result-object v3

    .line 132
    new-instance v0, Lkvj;

    iget-object v1, p0, Lkuk;->at:Llnl;

    .line 133
    invoke-virtual {p0}, Lkuk;->p()Lae;

    move-result-object v2

    iget-object v4, p0, Lkuk;->P:Lhee;

    invoke-interface {v4}, Lhee;->d()I

    move-result v4

    iget-object v5, p0, Lkuk;->Q:Ljava/lang/String;

    iget v6, p0, Lkuk;->R:I

    .line 134
    invoke-static {v6}, Lkto;->a(I)Z

    move-result v6

    invoke-direct/range {v0 .. v6}, Lkvj;-><init>(Landroid/content/Context;Lae;Lbb;ILjava/lang/String;Z)V

    iput-object v0, p0, Lkuk;->T:Lkvj;

    .line 135
    iget-object v0, p0, Lkuk;->T:Lkvj;

    invoke-virtual {v0, p1}, Lkvj;->a(Landroid/os/Bundle;)V

    .line 136
    iget-object v1, p0, Lkuk;->T:Lkvj;

    iget-object v0, p0, Lkuk;->au:Llnh;

    const-class v2, Lkve;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkve;

    invoke-virtual {v1, v0}, Lkvj;->a(Lkve;)V

    .line 137
    return-void

    .line 127
    :cond_0
    const-string v1, "square_membership"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lkuk;->R:I

    goto :goto_0
.end method

.method public a(Lhjk;)V
    .locals 0

    .prologue
    .line 178
    return-void
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 2

    .prologue
    .line 239
    const-string v0, "EditSquareMembershipTask"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 240
    invoke-virtual {p2}, Lhoz;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 243
    iget-object v0, p0, Lkuk;->T:Lkvj;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lkvj;->b(Ljava/lang/String;)V

    .line 244
    iget-object v0, p0, Lkuk;->T:Lkvj;

    iget-object v1, p0, Lkuk;->U:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v1}, Landroid/support/v7/widget/SearchView;->c()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkvj;->b(Ljava/lang/String;)V

    .line 247
    :cond_0
    return-void
.end method

.method public a(Loo;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 150
    invoke-static {p1, v3}, Lley;->a(Loo;Z)V

    .line 152
    new-instance v0, Landroid/support/v7/widget/SearchView;

    invoke-virtual {p1}, Loo;->i()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/widget/SearchView;-><init>(Landroid/content/Context;)V

    .line 153
    iget-object v1, p0, Lkuk;->at:Llnl;

    invoke-static {v1, v0}, Lley;->a(Landroid/content/Context;Landroid/support/v7/widget/SearchView;)V

    .line 154
    invoke-virtual {v0, v3}, Landroid/support/v7/widget/SearchView;->a(Z)V

    .line 155
    invoke-virtual {p0}, Lkuk;->o()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0818

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SearchView;->a(Ljava/lang/CharSequence;)V

    .line 156
    invoke-virtual {v0, p0}, Landroid/support/v7/widget/SearchView;->a(Lze;)V

    .line 158
    iput-object v0, p0, Lkuk;->U:Landroid/support/v7/widget/SearchView;

    .line 160
    invoke-virtual {p1, v0}, Loo;->a(Landroid/view/View;)V

    .line 161
    invoke-virtual {p1, v4}, Loo;->e(Z)V

    .line 162
    invoke-virtual {p1, v3}, Loo;->d(Z)V

    .line 163
    invoke-virtual {p1, v4}, Loo;->c(Z)V

    .line 164
    invoke-static {p1, v4}, Lley;->a(Loo;Z)V

    .line 166
    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView;->requestFocus()Z

    .line 167
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 232
    invoke-virtual {p0}, Lkuk;->x()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Llsn;->b(Landroid/view/View;)V

    .line 233
    const/4 v0, 0x0

    return v0
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 271
    const/4 v0, 0x0

    return v0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 224
    invoke-virtual {p0}, Lkuk;->x()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Llsn;->b(Landroid/view/View;)V

    .line 225
    iget-object v0, p0, Lkuk;->U:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView;->clearFocus()V

    .line 226
    const/4 v0, 0x1

    return v0
.end method

.method public aO_()V
    .locals 2

    .prologue
    .line 203
    invoke-super {p0}, Llol;->aO_()V

    .line 204
    iget-object v0, p0, Lkuk;->S:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 205
    return-void
.end method

.method public ac_()Lhmk;
    .locals 3

    .prologue
    .line 266
    new-instance v0, Lkqw;

    sget-object v1, Lomv;->v:Lhmn;

    iget-object v2, p0, Lkuk;->Q:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lkqw;-><init>(Lhmn;Ljava/lang/String;)V

    return-object v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 261
    const-string v0, "extra_square_id"

    iget-object v1, p0, Lkuk;->Q:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    return-void
.end method

.method public b(Loo;)V
    .locals 1

    .prologue
    .line 171
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Loo;->a(Landroid/view/View;)V

    .line 172
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Loo;->e(Z)V

    .line 173
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Loo;->d(Z)V

    .line 174
    return-void
.end method

.method public b(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 216
    iget-object v0, p0, Lkuk;->T:Lkvj;

    if-eqz v0, :cond_0

    .line 217
    iget-object v1, p0, Lkuk;->T:Lkvj;

    if-nez p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lkvj;->b(Ljava/lang/String;)V

    .line 219
    :cond_0
    const/4 v0, 0x1

    return v0

    .line 217
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 256
    iget v0, p0, Lkuk;->R:I

    return v0
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 105
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 106
    iget-object v0, p0, Lkuk;->au:Llnh;

    const-class v1, Lhmq;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 107
    iget-object v0, p0, Lkuk;->au:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Lkuk;->P:Lhee;

    .line 108
    iget-object v0, p0, Lkuk;->au:Llnh;

    const-class v1, Lhoc;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    invoke-virtual {v0, p0}, Lhoc;->a(Lhob;)Lhoc;

    .line 109
    iget-object v0, p0, Lkuk;->au:Llnh;

    const-class v1, Lhmm;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 110
    iget-object v0, p0, Lkuk;->au:Llnh;

    const-class v1, Lkuz;

    iget-object v2, p0, Lkuk;->N:Lkuu;

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 111
    iget-object v0, p0, Lkuk;->au:Llnh;

    const-class v1, Lkve;

    iget-object v2, p0, Lkuk;->O:Lkuv;

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 112
    iget-object v0, p0, Lkuk;->au:Llnh;

    const-class v1, Lktc;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 113
    iget-object v0, p0, Lkuk;->au:Llnh;

    const-class v1, Lktb;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 114
    return-void
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lkuk;->Q:Ljava/lang/String;

    return-object v0
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 141
    invoke-super {p0, p1}, Llol;->e(Landroid/os/Bundle;)V

    .line 142
    iget-object v0, p0, Lkuk;->T:Lkvj;

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lkuk;->T:Lkvj;

    invoke-virtual {v0, p1}, Lkvj;->b(Landroid/os/Bundle;)V

    .line 145
    :cond_0
    const-string v0, "membership_status"

    iget v1, p0, Lkuk;->R:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 146
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 191
    invoke-super {p0}, Llol;->g()V

    .line 192
    iget-object v0, p0, Lkuk;->T:Lkvj;

    invoke-virtual {v0}, Lkvj;->d()V

    .line 193
    return-void
.end method

.method public h()V
    .locals 1

    .prologue
    .line 197
    invoke-super {p0}, Llol;->g()V

    .line 198
    iget-object v0, p0, Lkuk;->T:Lkvj;

    invoke-virtual {v0}, Lkvj;->e()V

    .line 199
    return-void
.end method
