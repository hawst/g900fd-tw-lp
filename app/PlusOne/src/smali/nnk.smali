.class public final Lnnk;
.super Loxq;
.source "PG"


# static fields
.field public static final a:Loxr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Loxr",
            "<",
            "Lnnk;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:[Lnnj;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1552
    const v0, 0x282ac73

    new-instance v1, Lnnl;

    invoke-direct {v1}, Lnnl;-><init>()V

    .line 1557
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    move-result-object v0

    sput-object v0, Lnnk;->a:Loxr;

    .line 1556
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1553
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1560
    sget-object v0, Lnnj;->a:[Lnnj;

    iput-object v0, p0, Lnnk;->b:[Lnnj;

    .line 1553
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1578
    .line 1579
    iget-object v1, p0, Lnnk;->b:[Lnnj;

    if-eqz v1, :cond_1

    .line 1580
    iget-object v2, p0, Lnnk;->b:[Lnnj;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 1581
    if-eqz v4, :cond_0

    .line 1582
    const/4 v5, 0x1

    .line 1583
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 1580
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1587
    :cond_1
    iget-object v1, p0, Lnnk;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1588
    iput v0, p0, Lnnk;->ai:I

    .line 1589
    return v0
.end method

.method public a(Loxn;)Lnnk;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1597
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1598
    sparse-switch v0, :sswitch_data_0

    .line 1602
    iget-object v2, p0, Lnnk;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 1603
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnnk;->ah:Ljava/util/List;

    .line 1606
    :cond_1
    iget-object v2, p0, Lnnk;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1608
    :sswitch_0
    return-object p0

    .line 1613
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1614
    iget-object v0, p0, Lnnk;->b:[Lnnj;

    if-nez v0, :cond_3

    move v0, v1

    .line 1615
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnnj;

    .line 1616
    iget-object v3, p0, Lnnk;->b:[Lnnj;

    if-eqz v3, :cond_2

    .line 1617
    iget-object v3, p0, Lnnk;->b:[Lnnj;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1619
    :cond_2
    iput-object v2, p0, Lnnk;->b:[Lnnj;

    .line 1620
    :goto_2
    iget-object v2, p0, Lnnk;->b:[Lnnj;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 1621
    iget-object v2, p0, Lnnk;->b:[Lnnj;

    new-instance v3, Lnnj;

    invoke-direct {v3}, Lnnj;-><init>()V

    aput-object v3, v2, v0

    .line 1622
    iget-object v2, p0, Lnnk;->b:[Lnnj;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1623
    invoke-virtual {p1}, Loxn;->a()I

    .line 1620
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1614
    :cond_3
    iget-object v0, p0, Lnnk;->b:[Lnnj;

    array-length v0, v0

    goto :goto_1

    .line 1626
    :cond_4
    iget-object v2, p0, Lnnk;->b:[Lnnj;

    new-instance v3, Lnnj;

    invoke-direct {v3}, Lnnj;-><init>()V

    aput-object v3, v2, v0

    .line 1627
    iget-object v2, p0, Lnnk;->b:[Lnnj;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1598
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 1565
    iget-object v0, p0, Lnnk;->b:[Lnnj;

    if-eqz v0, :cond_1

    .line 1566
    iget-object v1, p0, Lnnk;->b:[Lnnj;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 1567
    if-eqz v3, :cond_0

    .line 1568
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 1566
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1572
    :cond_1
    iget-object v0, p0, Lnnk;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1574
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1549
    invoke-virtual {p0, p1}, Lnnk;->a(Loxn;)Lnnk;

    move-result-object v0

    return-object v0
.end method
