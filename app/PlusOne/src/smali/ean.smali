.class public abstract Lean;
.super Leak;
.source "PG"

# interfaces
.implements Landroid/widget/AbsListView$RecyclerListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Ldid;
.implements Lhob;


# instance fields
.field public N:Landroid/widget/ListView;

.field private O:Lhxh;

.field private P:Ljava/lang/Integer;

.field private Q:Ldib;

.field private final R:Landroid/database/DataSetObserver;

.field private final S:Lfhh;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Leak;-><init>()V

    .line 61
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 65
    new-instance v0, Leao;

    invoke-direct {v0, p0}, Leao;-><init>(Lean;)V

    iput-object v0, p0, Lean;->R:Landroid/database/DataSetObserver;

    .line 75
    new-instance v0, Leap;

    invoke-direct {v0, p0}, Leap;-><init>(Lean;)V

    iput-object v0, p0, Lean;->S:Lfhh;

    return-void
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 342
    sget-object v0, Lhmw;->r:Lhmw;

    return-object v0
.end method

.method public abstract U()Landroid/widget/ListAdapter;
.end method

.method public abstract V()Z
.end method

.method public abstract W()Z
.end method

.method public abstract X()I
.end method

.method protected Y()Z
    .locals 1

    .prologue
    .line 192
    invoke-virtual {p0}, Lean;->V()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lean;->O:Lhxh;

    invoke-virtual {v0}, Lhxh;->a()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 155
    invoke-virtual {p0, p1, p2}, Lean;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 156
    const v0, 0x102000a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lean;->N:Landroid/widget/ListView;

    .line 157
    iget-object v0, p0, Lean;->N:Landroid/widget/ListView;

    invoke-virtual {p0}, Lean;->U()Landroid/widget/ListAdapter;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 158
    iget-object v0, p0, Lean;->N:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 159
    iget-object v0, p0, Lean;->N:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setRecyclerListener(Landroid/widget/AbsListView$RecyclerListener;)V

    .line 160
    return-object v1
.end method

.method public a(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 254
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    if-nez p1, :cond_0

    .line 255
    const-string v0, "person_id"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 256
    const-string v1, "display_name"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 257
    const-string v2, "selected_circle_ids"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 259
    invoke-virtual {p0, v0, v1, v2}, Lean;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 262
    :cond_0
    invoke-super {p0, p1, p2, p3}, Leak;->a(IILandroid/content/Intent;)V

    .line 263
    return-void
.end method

.method protected a(ILfib;)V
    .locals 3

    .prologue
    .line 308
    iget-object v0, p0, Lean;->P:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lean;->P:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq p1, v0, :cond_1

    .line 324
    :cond_0
    :goto_0
    return-void

    .line 312
    :cond_1
    invoke-virtual {p0}, Lean;->p()Lae;

    move-result-object v0

    const-string v1, "req_pending"

    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lt;

    .line 314
    if-eqz v0, :cond_2

    .line 315
    invoke-virtual {v0}, Lt;->a()V

    .line 318
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lean;->P:Ljava/lang/Integer;

    .line 320
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lfib;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 321
    invoke-virtual {p0}, Lean;->n()Lz;

    move-result-object v0

    const v1, 0x7f0a0592

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 322
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public a(Landroid/app/Activity;)V
    .locals 4

    .prologue
    .line 105
    invoke-super {p0, p1}, Leak;->a(Landroid/app/Activity;)V

    .line 107
    new-instance v0, Lhxh;

    iget-object v1, p0, Lean;->at:Llnl;

    invoke-virtual {p0}, Lean;->w()Lbb;

    move-result-object v2

    invoke-virtual {p0}, Lean;->e()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lhxh;-><init>(Landroid/content/Context;Lbb;I)V

    iput-object v0, p0, Lean;->O:Lhxh;

    .line 108
    iget-object v0, p0, Lean;->O:Lhxh;

    iget-object v1, p0, Lean;->R:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lhxh;->a(Landroid/database/DataSetObserver;)V

    .line 109
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 113
    if-eqz p1, :cond_0

    .line 114
    const-string v0, "request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 115
    const-string v0, "request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lean;->P:Ljava/lang/Integer;

    .line 118
    :cond_0
    invoke-super {p0, p1}, Leak;->a(Landroid/os/Bundle;)V

    .line 119
    invoke-virtual {p0}, Lean;->ae()Lhoc;

    move-result-object v0

    invoke-virtual {v0, p0}, Lhoc;->a(Lhob;)Lhoc;

    .line 120
    iget-object v0, p0, Lean;->O:Lhxh;

    invoke-virtual {v0}, Lhxh;->b()V

    .line 121
    invoke-virtual {p0, p1}, Lean;->k(Landroid/os/Bundle;)V

    .line 122
    iget-object v1, p0, Lean;->at:Llnl;

    iget-object v0, p0, Lean;->au:Llnh;

    const-class v2, Lhms;

    .line 123
    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    .line 122
    invoke-static {v1, v0, p1}, Ldib;->a(Landroid/content/Context;Lhms;Landroid/os/Bundle;)Ldib;

    move-result-object v0

    iput-object v0, p0, Lean;->Q:Ldib;

    .line 124
    return-void
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 3

    .prologue
    .line 348
    const-string v0, "ModifyCircleMembershipsTask"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 349
    iget-object v0, p0, Lean;->Q:Ldib;

    if-eqz v0, :cond_0

    .line 350
    invoke-static {p2}, Lhoz;->a(Lhoz;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 351
    invoke-virtual {p0}, Lean;->e()I

    move-result v0

    .line 352
    iget-object v1, p0, Lean;->Q:Ldib;

    invoke-virtual {v1, v0}, Ldib;->a(I)V

    .line 356
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lean;->Q:Ldib;

    .line 359
    :cond_0
    return-void

    .line 354
    :cond_1
    invoke-virtual {p0}, Lean;->n()Lz;

    move-result-object v0

    invoke-virtual {p2}, Lhoz;->c()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method protected a(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 270
    .line 271
    new-instance v0, Ldpj;

    invoke-virtual {p0}, Lean;->n()Lz;

    move-result-object v1

    invoke-direct {v0, v1}, Ldpj;-><init>(Landroid/content/Context;)V

    .line 275
    invoke-virtual {p0}, Lean;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Ldpj;->a(I)Ldpj;

    move-result-object v0

    .line 276
    invoke-virtual {v0, p1}, Ldpj;->a(Ljava/lang/String;)Ldpj;

    move-result-object v0

    .line 277
    invoke-virtual {v0, p2}, Ldpj;->b(Ljava/lang/String;)Ldpj;

    move-result-object v0

    .line 278
    invoke-virtual {p0}, Lean;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Ldpj;->b(I)Ldpj;

    move-result-object v0

    .line 279
    invoke-virtual {v0, p3}, Ldpj;->a(Ljava/util/ArrayList;)Ldpj;

    move-result-object v0

    .line 280
    invoke-virtual {v0, v5}, Ldpj;->b(Ljava/util/ArrayList;)Ldpj;

    move-result-object v0

    const/4 v1, 0x1

    .line 281
    invoke-virtual {v0, v1}, Ldpj;->a(Z)Ldpj;

    move-result-object v0

    .line 282
    invoke-virtual {v0, v2}, Ldpj;->b(Z)Ldpj;

    move-result-object v0

    .line 283
    invoke-virtual {v0, v2}, Ldpj;->c(Z)Ldpj;

    move-result-object v0

    .line 284
    invoke-virtual {v0}, Ldpj;->a()Ldpi;

    move-result-object v0

    .line 285
    invoke-virtual {p0}, Lean;->ae()Lhoc;

    move-result-object v1

    invoke-virtual {v1, v0}, Lhoc;->c(Lhny;)V

    .line 287
    new-instance v0, Ldib;

    iget-object v1, p0, Lean;->at:Llnl;

    iget-object v2, p0, Lean;->au:Llnh;

    const-class v3, Lhms;

    invoke-virtual {v2, v3}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lhms;

    move-object v3, p1

    move-object v4, p3

    move-object v6, p0

    invoke-direct/range {v0 .. v6}, Ldib;-><init>(Landroid/content/Context;Lhms;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ldid;)V

    iput-object v0, p0, Lean;->Q:Ldib;

    .line 289
    return-void
.end method

.method public aO_()V
    .locals 2

    .prologue
    .line 221
    invoke-super {p0}, Leak;->aO_()V

    .line 222
    iget-object v0, p0, Lean;->S:Lfhh;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(Lfhh;)V

    .line 224
    iget-object v0, p0, Lean;->P:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 225
    iget-object v0, p0, Lean;->P:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 226
    iget-object v0, p0, Lean;->P:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(I)Lfib;

    move-result-object v0

    .line 227
    iget-object v1, p0, Lean;->P:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v1, v0}, Lean;->a(ILfib;)V

    .line 228
    const/4 v0, 0x0

    iput-object v0, p0, Lean;->P:Ljava/lang/Integer;

    .line 231
    :cond_0
    invoke-virtual {p0}, Lean;->x()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lean;->c(Landroid/view/View;)V

    .line 232
    return-void
.end method

.method public aR_()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 337
    const/4 v0, 0x0

    return-object v0
.end method

.method public b()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 331
    const/4 v0, 0x0

    return-object v0
.end method

.method public c(Landroid/view/View;)V
    .locals 2

    .prologue
    const v1, 0x7f100249

    .line 200
    invoke-virtual {p0}, Lean;->Y()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 201
    invoke-virtual {p0, p1}, Lean;->d(Landroid/view/View;)V

    .line 217
    :goto_0
    return-void

    .line 203
    :cond_0
    invoke-virtual {p0}, Lean;->W()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 204
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 213
    :cond_1
    invoke-virtual {p0, p1}, Lean;->h(Landroid/view/View;)V

    goto :goto_0

    .line 207
    :cond_2
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 208
    invoke-virtual {p0}, Lean;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 209
    const v0, 0x7f10025f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 210
    invoke-virtual {p0}, Lean;->X()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 211
    invoke-virtual {p0, p1}, Lean;->g(Landroid/view/View;)V

    goto :goto_0
.end method

.method public c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 244
    invoke-virtual {p0}, Lean;->e()I

    move-result v1

    .line 246
    invoke-virtual {p0}, Lean;->n()Lz;

    move-result-object v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-static/range {v0 .. v5}, Leyq;->b(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 245
    invoke-virtual {p0, v0, v5}, Lean;->a(Landroid/content/Intent;I)V

    .line 250
    return-void
.end method

.method public e()I
    .locals 3

    .prologue
    .line 95
    invoke-virtual {p0}, Lean;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 96
    invoke-virtual {p0}, Lean;->n()Lz;

    .line 97
    const-string v1, "account_id"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 99
    return v0
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 128
    invoke-super {p0, p1}, Leak;->e(Landroid/os/Bundle;)V

    .line 129
    iget-object v0, p0, Lean;->P:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 130
    const-string v0, "request_id"

    iget-object v1, p0, Lean;->P:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 132
    :cond_0
    iget-object v0, p0, Lean;->Q:Ldib;

    if-eqz v0, :cond_1

    .line 133
    iget-object v0, p0, Lean;->Q:Ldib;

    invoke-virtual {v0, p1}, Ldib;->a(Landroid/os/Bundle;)V

    .line 135
    :cond_1
    return-void
.end method

.method public abstract k(Landroid/os/Bundle;)V
.end method

.method public onMovedToScrapHeap(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 165
    instance-of v0, p1, Lljh;

    if-eqz v0, :cond_0

    .line 166
    check-cast p1, Lljh;

    invoke-interface {p1}, Lljh;->a()V

    .line 168
    :cond_0
    return-void
.end method

.method public z()V
    .locals 1

    .prologue
    .line 236
    invoke-super {p0}, Leak;->z()V

    .line 237
    iget-object v0, p0, Lean;->S:Lfhh;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(Lfhh;)V

    .line 238
    return-void
.end method
