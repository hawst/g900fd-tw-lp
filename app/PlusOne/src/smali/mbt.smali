.class public final Lmbt;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnwb;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 8260
    invoke-direct {p0}, Loxq;-><init>()V

    .line 8263
    iput-object v0, p0, Lmbt;->apiHeader:Llyr;

    .line 8266
    iput-object v0, p0, Lmbt;->a:Lnwb;

    .line 8260
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 8283
    const/4 v0, 0x0

    .line 8284
    iget-object v1, p0, Lmbt;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 8285
    const/4 v0, 0x1

    iget-object v1, p0, Lmbt;->apiHeader:Llyr;

    .line 8286
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 8288
    :cond_0
    iget-object v1, p0, Lmbt;->a:Lnwb;

    if-eqz v1, :cond_1

    .line 8289
    const/4 v1, 0x2

    iget-object v2, p0, Lmbt;->a:Lnwb;

    .line 8290
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8292
    :cond_1
    iget-object v1, p0, Lmbt;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8293
    iput v0, p0, Lmbt;->ai:I

    .line 8294
    return v0
.end method

.method public a(Loxn;)Lmbt;
    .locals 2

    .prologue
    .line 8302
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 8303
    sparse-switch v0, :sswitch_data_0

    .line 8307
    iget-object v1, p0, Lmbt;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 8308
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmbt;->ah:Ljava/util/List;

    .line 8311
    :cond_1
    iget-object v1, p0, Lmbt;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 8313
    :sswitch_0
    return-object p0

    .line 8318
    :sswitch_1
    iget-object v0, p0, Lmbt;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 8319
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmbt;->apiHeader:Llyr;

    .line 8321
    :cond_2
    iget-object v0, p0, Lmbt;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 8325
    :sswitch_2
    iget-object v0, p0, Lmbt;->a:Lnwb;

    if-nez v0, :cond_3

    .line 8326
    new-instance v0, Lnwb;

    invoke-direct {v0}, Lnwb;-><init>()V

    iput-object v0, p0, Lmbt;->a:Lnwb;

    .line 8328
    :cond_3
    iget-object v0, p0, Lmbt;->a:Lnwb;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 8303
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 8271
    iget-object v0, p0, Lmbt;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 8272
    const/4 v0, 0x1

    iget-object v1, p0, Lmbt;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 8274
    :cond_0
    iget-object v0, p0, Lmbt;->a:Lnwb;

    if-eqz v0, :cond_1

    .line 8275
    const/4 v0, 0x2

    iget-object v1, p0, Lmbt;->a:Lnwb;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 8277
    :cond_1
    iget-object v0, p0, Lmbt;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 8279
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 8256
    invoke-virtual {p0, p1}, Lmbt;->a(Loxn;)Lmbt;

    move-result-object v0

    return-object v0
.end method
