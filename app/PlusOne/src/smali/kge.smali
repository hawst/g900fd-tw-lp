.class public final Lkge;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/16 v9, 0xe

    const/4 v4, 0x1

    const/4 v8, 0x2

    const/4 v0, 0x0

    .line 20
    new-array v2, v9, [Ljava/lang/String;

    const-string v1, "getmobileexperiments"

    aput-object v1, v2, v0

    const-string v1, "registerdevice"

    aput-object v1, v2, v4

    const-string v1, "fetchnotifications"

    aput-object v1, v2, v8

    const/4 v1, 0x3

    const-string v3, "fetchnotificationscount"

    aput-object v3, v2, v1

    const/4 v1, 0x4

    const-string v3, "getphotossettings"

    aput-object v3, v2, v1

    const/4 v1, 0x5

    const-string v3, "getsimpleprofile"

    aput-object v3, v2, v1

    const/4 v1, 0x6

    const-string v3, "getmobilesettings"

    aput-object v3, v2, v1

    const/4 v1, 0x7

    const-string v3, "loadblockedpeople"

    aput-object v3, v2, v1

    const/16 v1, 0x8

    const-string v3, "loadpeople"

    aput-object v3, v2, v1

    const/16 v1, 0x9

    const-string v3, "loadsocialnetwork"

    aput-object v3, v2, v1

    const/16 v1, 0xa

    const-string v3, "getactivities"

    aput-object v3, v2, v1

    const/16 v1, 0xb

    const-string v3, "getvolumecontrols"

    aput-object v3, v2, v1

    const/16 v1, 0xc

    const-string v3, "readcollectionsbyid"

    aput-object v3, v2, v1

    const/16 v1, 0xd

    const-string v3, "syncuserhighlights"

    aput-object v3, v2, v1

    .line 38
    new-array v3, v8, [Ljava/lang/String;

    const-string v1, "getappupgradestatus"

    aput-object v1, v3, v0

    const-string v1, "postclientlogs"

    aput-object v1, v3, v4

    .line 43
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    move v1, v0

    .line 44
    :goto_0
    if-ge v1, v9, :cond_0

    .line 45
    aget-object v5, v2, v1

    aget-object v6, v2, v1

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "background"

    invoke-virtual {v6, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 47
    :cond_0
    :goto_1
    if-ge v0, v8, :cond_1

    .line 48
    aget-object v1, v3, v0

    aget-object v2, v3, v0

    invoke-interface {v4, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 50
    :cond_1
    invoke-static {v4}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lkge;->a:Ljava/util/Map;

    .line 51
    return-void
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 54
    sget-object v0, Lkge;->a:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 56
    if-nez v0, :cond_1

    .line 57
    const-string v0, "BackgroundEndpoints"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 59
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x39

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Calling operation ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] from sync loop with no sync endpoint"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 63
    :cond_0
    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method
