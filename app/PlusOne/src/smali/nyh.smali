.class public final Lnyh;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Double;

.field public b:Ljava/lang/Double;

.field private c:Ljava/lang/Double;

.field private d:Ljava/lang/Double;

.field private e:Ljava/lang/Double;

.field private f:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6874
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 6911
    const/4 v0, 0x1

    iget-object v1, p0, Lnyh;->a:Ljava/lang/Double;

    .line 6913
    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x8

    add-int/lit8 v0, v0, 0x0

    .line 6914
    const/4 v1, 0x2

    iget-object v2, p0, Lnyh;->b:Ljava/lang/Double;

    .line 6915
    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 6916
    iget-object v1, p0, Lnyh;->c:Ljava/lang/Double;

    if-eqz v1, :cond_0

    .line 6917
    const/4 v1, 0x3

    iget-object v2, p0, Lnyh;->c:Ljava/lang/Double;

    .line 6918
    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 6920
    :cond_0
    iget-object v1, p0, Lnyh;->d:Ljava/lang/Double;

    if-eqz v1, :cond_1

    .line 6921
    const/4 v1, 0x4

    iget-object v2, p0, Lnyh;->d:Ljava/lang/Double;

    .line 6922
    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 6924
    :cond_1
    iget-object v1, p0, Lnyh;->e:Ljava/lang/Double;

    if-eqz v1, :cond_2

    .line 6925
    const/4 v1, 0x5

    iget-object v2, p0, Lnyh;->e:Ljava/lang/Double;

    .line 6926
    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 6928
    :cond_2
    iget-object v1, p0, Lnyh;->f:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 6929
    const/4 v1, 0x6

    iget-object v2, p0, Lnyh;->f:Ljava/lang/Boolean;

    .line 6930
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 6932
    :cond_3
    iget-object v1, p0, Lnyh;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6933
    iput v0, p0, Lnyh;->ai:I

    .line 6934
    return v0
.end method

.method public a(Loxn;)Lnyh;
    .locals 2

    .prologue
    .line 6942
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 6943
    sparse-switch v0, :sswitch_data_0

    .line 6947
    iget-object v1, p0, Lnyh;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 6948
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnyh;->ah:Ljava/util/List;

    .line 6951
    :cond_1
    iget-object v1, p0, Lnyh;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6953
    :sswitch_0
    return-object p0

    .line 6958
    :sswitch_1
    invoke-virtual {p1}, Loxn;->c()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lnyh;->a:Ljava/lang/Double;

    goto :goto_0

    .line 6962
    :sswitch_2
    invoke-virtual {p1}, Loxn;->c()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lnyh;->b:Ljava/lang/Double;

    goto :goto_0

    .line 6966
    :sswitch_3
    invoke-virtual {p1}, Loxn;->c()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lnyh;->c:Ljava/lang/Double;

    goto :goto_0

    .line 6970
    :sswitch_4
    invoke-virtual {p1}, Loxn;->c()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lnyh;->d:Ljava/lang/Double;

    goto :goto_0

    .line 6974
    :sswitch_5
    invoke-virtual {p1}, Loxn;->c()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lnyh;->e:Ljava/lang/Double;

    goto :goto_0

    .line 6978
    :sswitch_6
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnyh;->f:Ljava/lang/Boolean;

    goto :goto_0

    .line 6943
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x11 -> :sswitch_2
        0x19 -> :sswitch_3
        0x21 -> :sswitch_4
        0x29 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 6891
    const/4 v0, 0x1

    iget-object v1, p0, Lnyh;->a:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(ID)V

    .line 6892
    const/4 v0, 0x2

    iget-object v1, p0, Lnyh;->b:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(ID)V

    .line 6893
    iget-object v0, p0, Lnyh;->c:Ljava/lang/Double;

    if-eqz v0, :cond_0

    .line 6894
    const/4 v0, 0x3

    iget-object v1, p0, Lnyh;->c:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(ID)V

    .line 6896
    :cond_0
    iget-object v0, p0, Lnyh;->d:Ljava/lang/Double;

    if-eqz v0, :cond_1

    .line 6897
    const/4 v0, 0x4

    iget-object v1, p0, Lnyh;->d:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(ID)V

    .line 6899
    :cond_1
    iget-object v0, p0, Lnyh;->e:Ljava/lang/Double;

    if-eqz v0, :cond_2

    .line 6900
    const/4 v0, 0x5

    iget-object v1, p0, Lnyh;->e:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(ID)V

    .line 6902
    :cond_2
    iget-object v0, p0, Lnyh;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 6903
    const/4 v0, 0x6

    iget-object v1, p0, Lnyh;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 6905
    :cond_3
    iget-object v0, p0, Lnyh;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 6907
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6870
    invoke-virtual {p0, p1}, Lnyh;->a(Loxn;)Lnyh;

    move-result-object v0

    return-object v0
.end method
