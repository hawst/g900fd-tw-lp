.class public final Lnuz;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:I

.field private e:Loep;

.field private f:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/high16 v1, -0x80000000

    .line 1795
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1804
    const/4 v0, 0x0

    iput-object v0, p0, Lnuz;->e:Loep;

    .line 1807
    iput v1, p0, Lnuz;->d:I

    .line 1810
    iput v1, p0, Lnuz;->f:I

    .line 1795
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    const/high16 v3, -0x80000000

    .line 1839
    const/4 v0, 0x0

    .line 1840
    iget-object v1, p0, Lnuz;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1841
    const/4 v0, 0x1

    iget-object v1, p0, Lnuz;->a:Ljava/lang/String;

    .line 1842
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1844
    :cond_0
    iget-object v1, p0, Lnuz;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1845
    const/4 v1, 0x2

    iget-object v2, p0, Lnuz;->b:Ljava/lang/String;

    .line 1846
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1848
    :cond_1
    iget-object v1, p0, Lnuz;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1849
    const/4 v1, 0x3

    iget-object v2, p0, Lnuz;->c:Ljava/lang/String;

    .line 1850
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1852
    :cond_2
    iget-object v1, p0, Lnuz;->e:Loep;

    if-eqz v1, :cond_3

    .line 1853
    const/4 v1, 0x4

    iget-object v2, p0, Lnuz;->e:Loep;

    .line 1854
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1856
    :cond_3
    iget v1, p0, Lnuz;->d:I

    if-eq v1, v3, :cond_4

    .line 1857
    const/4 v1, 0x5

    iget v2, p0, Lnuz;->d:I

    .line 1858
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1860
    :cond_4
    iget v1, p0, Lnuz;->f:I

    if-eq v1, v3, :cond_5

    .line 1861
    const/4 v1, 0x6

    iget v2, p0, Lnuz;->f:I

    .line 1862
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1864
    :cond_5
    iget-object v1, p0, Lnuz;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1865
    iput v0, p0, Lnuz;->ai:I

    .line 1866
    return v0
.end method

.method public a(Loxn;)Lnuz;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1874
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1875
    sparse-switch v0, :sswitch_data_0

    .line 1879
    iget-object v1, p0, Lnuz;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1880
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnuz;->ah:Ljava/util/List;

    .line 1883
    :cond_1
    iget-object v1, p0, Lnuz;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1885
    :sswitch_0
    return-object p0

    .line 1890
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnuz;->a:Ljava/lang/String;

    goto :goto_0

    .line 1894
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnuz;->b:Ljava/lang/String;

    goto :goto_0

    .line 1898
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnuz;->c:Ljava/lang/String;

    goto :goto_0

    .line 1902
    :sswitch_4
    iget-object v0, p0, Lnuz;->e:Loep;

    if-nez v0, :cond_2

    .line 1903
    new-instance v0, Loep;

    invoke-direct {v0}, Loep;-><init>()V

    iput-object v0, p0, Lnuz;->e:Loep;

    .line 1905
    :cond_2
    iget-object v0, p0, Lnuz;->e:Loep;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1909
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1910
    if-eqz v0, :cond_3

    if-eq v0, v3, :cond_3

    if-ne v0, v4, :cond_4

    .line 1913
    :cond_3
    iput v0, p0, Lnuz;->d:I

    goto :goto_0

    .line 1915
    :cond_4
    iput v2, p0, Lnuz;->d:I

    goto :goto_0

    .line 1920
    :sswitch_6
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1921
    if-eqz v0, :cond_5

    if-eq v0, v3, :cond_5

    if-eq v0, v4, :cond_5

    const/16 v1, 0xe

    if-eq v0, v1, :cond_5

    const/16 v1, 0xa

    if-eq v0, v1, :cond_5

    const/16 v1, 0xb

    if-eq v0, v1, :cond_5

    const/4 v1, 0x3

    if-eq v0, v1, :cond_5

    const/4 v1, 0x4

    if-eq v0, v1, :cond_5

    const/4 v1, 0x5

    if-eq v0, v1, :cond_5

    const/4 v1, 0x6

    if-eq v0, v1, :cond_5

    const/4 v1, 0x7

    if-eq v0, v1, :cond_5

    const/16 v1, 0x9

    if-eq v0, v1, :cond_5

    const/16 v1, 0xc

    if-eq v0, v1, :cond_5

    const/16 v1, 0xd

    if-eq v0, v1, :cond_5

    const/16 v1, 0xf

    if-eq v0, v1, :cond_5

    const/16 v1, 0x10

    if-eq v0, v1, :cond_5

    const/16 v1, 0x27

    if-eq v0, v1, :cond_5

    const/16 v1, 0x16

    if-eq v0, v1, :cond_5

    const/16 v1, 0x11

    if-eq v0, v1, :cond_5

    const/16 v1, 0x12

    if-eq v0, v1, :cond_5

    const/16 v1, 0x13

    if-eq v0, v1, :cond_5

    const/16 v1, 0x14

    if-eq v0, v1, :cond_5

    const/16 v1, 0x15

    if-eq v0, v1, :cond_5

    const/16 v1, 0x17

    if-eq v0, v1, :cond_5

    const/16 v1, 0x18

    if-eq v0, v1, :cond_5

    const/16 v1, 0x19

    if-eq v0, v1, :cond_5

    const/16 v1, 0x1a

    if-eq v0, v1, :cond_5

    const/16 v1, 0x1b

    if-eq v0, v1, :cond_5

    const/16 v1, 0x1c

    if-eq v0, v1, :cond_5

    const/16 v1, 0x1d

    if-eq v0, v1, :cond_5

    const/16 v1, 0x1e

    if-eq v0, v1, :cond_5

    const/16 v1, 0x1f

    if-eq v0, v1, :cond_5

    const/16 v1, 0x2c

    if-eq v0, v1, :cond_5

    const/16 v1, 0x2d

    if-eq v0, v1, :cond_5

    const/16 v1, 0x31

    if-eq v0, v1, :cond_5

    const/16 v1, 0x32

    if-eq v0, v1, :cond_5

    const/16 v1, 0x22

    if-eq v0, v1, :cond_5

    const/16 v1, 0x2e

    if-eq v0, v1, :cond_5

    const/16 v1, 0x20

    if-eq v0, v1, :cond_5

    const/16 v1, 0x21

    if-eq v0, v1, :cond_5

    const/16 v1, 0x23

    if-eq v0, v1, :cond_5

    const/16 v1, 0x24

    if-eq v0, v1, :cond_5

    const/16 v1, 0x25

    if-eq v0, v1, :cond_5

    const/16 v1, 0x26

    if-eq v0, v1, :cond_5

    const/16 v1, 0x28

    if-eq v0, v1, :cond_5

    const/16 v1, 0x29

    if-eq v0, v1, :cond_5

    const/16 v1, 0x2a

    if-eq v0, v1, :cond_5

    const/16 v1, 0x2b

    if-eq v0, v1, :cond_5

    const/16 v1, 0x2f

    if-eq v0, v1, :cond_5

    const/16 v1, 0x30

    if-eq v0, v1, :cond_5

    const/16 v1, 0x33

    if-eq v0, v1, :cond_5

    const/16 v1, 0x34

    if-ne v0, v1, :cond_6

    .line 1973
    :cond_5
    iput v0, p0, Lnuz;->f:I

    goto/16 :goto_0

    .line 1975
    :cond_6
    iput v2, p0, Lnuz;->f:I

    goto/16 :goto_0

    .line 1875
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 1815
    iget-object v0, p0, Lnuz;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1816
    const/4 v0, 0x1

    iget-object v1, p0, Lnuz;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1818
    :cond_0
    iget-object v0, p0, Lnuz;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1819
    const/4 v0, 0x2

    iget-object v1, p0, Lnuz;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1821
    :cond_1
    iget-object v0, p0, Lnuz;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1822
    const/4 v0, 0x3

    iget-object v1, p0, Lnuz;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1824
    :cond_2
    iget-object v0, p0, Lnuz;->e:Loep;

    if-eqz v0, :cond_3

    .line 1825
    const/4 v0, 0x4

    iget-object v1, p0, Lnuz;->e:Loep;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1827
    :cond_3
    iget v0, p0, Lnuz;->d:I

    if-eq v0, v2, :cond_4

    .line 1828
    const/4 v0, 0x5

    iget v1, p0, Lnuz;->d:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1830
    :cond_4
    iget v0, p0, Lnuz;->f:I

    if-eq v0, v2, :cond_5

    .line 1831
    const/4 v0, 0x6

    iget v1, p0, Lnuz;->f:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1833
    :cond_5
    iget-object v0, p0, Lnuz;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1835
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1791
    invoke-virtual {p0, p1}, Lnuz;->a(Loxn;)Lnuz;

    move-result-object v0

    return-object v0
.end method
