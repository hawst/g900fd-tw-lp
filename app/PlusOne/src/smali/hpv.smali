.class final Lhpv;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lheq;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 184
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 187
    const-string v0, "AutobackupAccountSettingsManager-AutoBackupAsAccountSettingMigration"

    return-object v0
.end method

.method public a(Landroid/content/Context;Lhem;)V
    .locals 3

    .prologue
    .line 192
    const-class v0, Lhqe;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhqe;

    .line 193
    invoke-virtual {v0}, Lhqe;->c()V

    .line 194
    invoke-virtual {v0}, Lhqe;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 195
    invoke-virtual {v0}, Lhqe;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "account_name"

    invoke-interface {p2, v1}, Lhem;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 196
    :goto_0
    const-string v1, "com.google.android.libraries.social.autobackup.AutobackupAccountSettingsManager"

    invoke-interface {p2, v1}, Lhem;->h(Ljava/lang/String;)Lhem;

    move-result-object v1

    .line 200
    const-string v2, "auto_backup_enabled"

    invoke-interface {v1, v2}, Lhem;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 201
    const-string v2, "auto_backup_enabled"

    invoke-interface {v1, v2, v0}, Lhem;->c(Ljava/lang/String;Z)Lhem;

    .line 203
    :cond_0
    return-void

    .line 195
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
