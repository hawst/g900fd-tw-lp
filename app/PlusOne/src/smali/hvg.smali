.class public final Lhvg;
.super Landroid/widget/TextView;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lljh;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static a:Lhwd;


# instance fields
.field private b:Lhtz;

.field private c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 27
    sget-object v0, Lhvg;->a:Lhwd;

    if-nez v0, :cond_0

    invoke-static {p1}, Lhwd;->a(Landroid/content/Context;)Lhwd;

    move-result-object v0

    sput-object v0, Lhvg;->a:Lhwd;

    :cond_0
    const v0, 0x7f020416

    invoke-virtual {p0, v0}, Lhvg;->setBackgroundResource(I)V

    const v0, 0x7f090082

    invoke-virtual {p0, p1, v0}, Lhvg;->setTextAppearance(Landroid/content/Context;I)V

    invoke-virtual {p0, p0}, Lhvg;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lhvg;->setClickable(Z)V

    .line 28
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 99
    const/4 v0, 0x0

    iput-object v0, p0, Lhvg;->b:Lhtz;

    .line 100
    const/4 v0, 0x0

    iput v0, p0, Lhvg;->c:I

    .line 101
    return-void
.end method

.method public a(Lhtz;I)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 54
    iput p2, p0, Lhvg;->c:I

    .line 55
    iput-object p1, p0, Lhvg;->b:Lhtz;

    .line 56
    iget-object v0, p0, Lhvg;->b:Lhtz;

    if-nez v0, :cond_0

    move v0, v1

    .line 57
    :goto_0
    if-nez v0, :cond_1

    iget v2, p0, Lhvg;->c:I

    if-nez v2, :cond_1

    .line 74
    :goto_1
    return-void

    .line 56
    :cond_0
    iget-object v0, p0, Lhvg;->b:Lhtz;

    invoke-interface {v0}, Lhtz;->b()I

    move-result v0

    goto :goto_0

    .line 60
    :cond_1
    invoke-virtual {p0}, Lhvg;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 61
    invoke-static {}, Llsu;->a()Ljava/lang/StringBuilder;

    move-result-object v3

    .line 62
    if-lez v0, :cond_2

    .line 63
    const v4, 0x7f110020

    new-array v5, v7, [Ljava/lang/Object;

    .line 64
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    .line 63
    invoke-virtual {v2, v4, v0, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 66
    :cond_2
    if-lez v0, :cond_3

    iget v0, p0, Lhvg;->c:I

    if-lez v0, :cond_3

    .line 67
    const-string v0, " "

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 69
    :cond_3
    iget v0, p0, Lhvg;->c:I

    if-lez v0, :cond_4

    .line 70
    const v0, 0x7f110021

    iget v4, p0, Lhvg;->c:I

    new-array v5, v7, [Ljava/lang/Object;

    iget v6, p0, Lhvg;->c:I

    .line 71
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    .line 70
    invoke-virtual {v2, v0, v4, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 73
    :cond_4
    invoke-static {v3}, Llsu;->a(Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lhvg;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 87
    iget-object v0, p0, Lhvg;->b:Lhtz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhvg;->b:Lhtz;

    invoke-interface {v0}, Lhtz;->a()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    iget v0, p0, Lhvg;->c:I

    if-lez v0, :cond_2

    .line 89
    :cond_1
    invoke-virtual {p0}, Lhvg;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lhti;

    invoke-static {v0, v1}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhti;

    .line 91
    if-eqz v0, :cond_2

    .line 92
    invoke-interface {v0}, Lhti;->a()V

    .line 95
    :cond_2
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 78
    invoke-super {p0, p1}, Landroid/widget/TextView;->onDraw(Landroid/graphics/Canvas;)V

    .line 79
    invoke-virtual {p0}, Lhvg;->getWidth()I

    move-result v6

    .line 80
    invoke-virtual {p0}, Lhvg;->getHeight()I

    move-result v7

    .line 81
    int-to-float v3, v6

    sget-object v0, Lhvg;->a:Lhwd;

    iget-object v5, v0, Lhwd;->B:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    move v4, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 82
    int-to-float v2, v7

    int-to-float v3, v6

    int-to-float v4, v7

    sget-object v0, Lhvg;->a:Lhwd;

    iget-object v5, v0, Lhwd;->B:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 83
    return-void
.end method
