.class public final Lksh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field private synthetic a:Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;)V
    .locals 0

    .prologue
    .line 289
    iput-object p1, p0, Lksh;->a:Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2

    .prologue
    .line 292
    iget-object v0, p0, Lksh;->a:Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;

    check-cast p2, Lkrp;

    invoke-static {v0, p2}, Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;->a(Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;Lkrp;)Lkrp;

    .line 293
    iget-object v0, p0, Lksh;->a:Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;

    invoke-static {v0}, Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;->c(Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;)Lkrp;

    move-result-object v0

    iget-object v1, p0, Lksh;->a:Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;

    invoke-static {v1}, Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;->b(Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;)Lksg;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkrp;->a(Lkrl;)V

    .line 297
    sget-boolean v0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->a:Z

    if-nez v0, :cond_0

    .line 298
    iget-object v0, p0, Lksh;->a:Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;->finish()V

    .line 313
    :goto_0
    return-void

    .line 302
    :cond_0
    iget-object v0, p0, Lksh;->a:Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;

    invoke-static {v0}, Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;->c(Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;)Lkrp;

    move-result-object v0

    invoke-virtual {v0}, Lkrp;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 303
    iget-object v0, p0, Lksh;->a:Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;

    invoke-static {v0}, Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;->d(Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;)V

    goto :goto_0

    .line 308
    :cond_1
    iget-object v0, p0, Lksh;->a:Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;

    invoke-static {v0}, Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;->e(Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;)V

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    .prologue
    .line 317
    iget-object v0, p0, Lksh;->a:Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;->a(Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;Lkrp;)Lkrp;

    .line 321
    return-void
.end method
