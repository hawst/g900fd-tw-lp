.class public final Lfxh;
.super Lfwp;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static h:Z

.field private static i:I

.field private static j:I

.field private static k:I

.field private static l:I

.field private static m:I

.field private static n:I


# instance fields
.field private a:Lnzx;

.field private b:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

.field private c:Lcom/google/android/libraries/social/media/ui/MediaView;

.field private d:Lfwn;

.field private e:Lizu;

.field private f:Landroid/widget/TextView;

.field private g:Z

.field private o:Lfxf;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lfwp;-><init>(Landroid/content/Context;)V

    .line 47
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfxh;->g:Z

    .line 61
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 209
    invoke-super {p0}, Lfwp;->a()V

    .line 210
    iget-object v0, p0, Lfxh;->c:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->a()V

    .line 211
    const/4 v0, 0x0

    iput-object v0, p0, Lfxh;->o:Lfxf;

    .line 212
    iget-object v0, p0, Lfxh;->d:Lfwn;

    invoke-virtual {v0}, Lfwn;->a()V

    .line 213
    return-void
.end method

.method public a(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 73
    invoke-super {p0, p1, p2, p3}, Lfwp;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 75
    sget-boolean v0, Lfxh;->h:Z

    if-nez v0, :cond_0

    .line 76
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 78
    const v1, 0x7f0d02d0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lfxh;->m:I

    .line 80
    const v1, 0x7f0d02d5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lfxh;->n:I

    .line 83
    const v1, 0x7f0d02d7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lfxh;->l:I

    .line 86
    const v1, 0x7f0d02ca

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lfxh;->k:I

    .line 88
    const v1, 0x7f0d02cb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lfxh;->i:I

    .line 90
    const v1, 0x7f0d02d8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lfxh;->j:I

    .line 93
    sput-boolean v3, Lfxh;->h:Z

    .line 96
    :cond_0
    new-instance v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/libraries/social/media/ui/MediaView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lfxh;->c:Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 97
    iget-object v0, p0, Lfxh;->c:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/media/ui/MediaView;->d(I)V

    .line 98
    iget-object v0, p0, Lfxh;->c:Lcom/google/android/libraries/social/media/ui/MediaView;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->b(I)V

    .line 99
    iget-object v0, p0, Lfxh;->c:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 100
    iget-object v0, p0, Lfxh;->c:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {p0, v0}, Lfxh;->addView(Landroid/view/View;)V

    .line 102
    new-instance v0, Lfwn;

    invoke-direct {v0, p1, p2, p3}, Lfwn;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lfxh;->d:Lfwn;

    .line 103
    iget-object v0, p0, Lfxh;->d:Lfwn;

    invoke-virtual {p0, v0}, Lfxh;->addView(Landroid/view/View;)V

    .line 105
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 107
    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v1, p0, Lfxh;->f:Landroid/widget/TextView;

    .line 108
    iget-object v1, p0, Lfxh;->f:Landroid/widget/TextView;

    const v2, 0x7f0b02ef

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 110
    iget-object v1, p0, Lfxh;->f:Landroid/widget/TextView;

    const v2, 0x7f0b013a

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 112
    iget-object v1, p0, Lfxh;->f:Landroid/widget/TextView;

    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 113
    iget-object v1, p0, Lfxh;->f:Landroid/widget/TextView;

    const v2, 0x7f0a0722

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 114
    iget-object v0, p0, Lfxh;->f:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lfxh;->addView(Landroid/view/View;)V

    .line 116
    new-instance v0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lfxh;->b:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    .line 117
    iget-object v0, p0, Lfxh;->b:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 118
    iget-object v0, p0, Lfxh;->b:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(I)V

    .line 119
    iget-object v0, p0, Lfxh;->b:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->b(I)V

    .line 120
    iget-object v0, p0, Lfxh;->b:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {p0, v0}, Lfxh;->addView(Landroid/view/View;)V

    .line 121
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;[BLfxf;)V
    .locals 6

    .prologue
    const/16 v3, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 178
    :try_start_0
    new-instance v0, Lnzx;

    invoke-direct {v0}, Lnzx;-><init>()V

    invoke-static {v0, p7}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lnzx;

    iput-object v0, p0, Lfxh;->a:Lnzx;
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    .line 183
    iget-object v0, p0, Lfxh;->a:Lnzx;

    sget-object v4, Lnzu;->a:Loxr;

    invoke-virtual {v0, v4}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnzu;

    iget-object v4, v0, Lnzu;->b:Lnym;

    .line 185
    invoke-static {v4}, Ljvd;->b(Lnym;)Ljac;

    move-result-object v5

    .line 186
    iget-object v0, v4, Lnym;->m:Lnzb;

    if-eqz v0, :cond_4

    iget-object v0, v4, Lnym;->m:Lnzb;

    iget v0, v0, Lnzb;->b:I

    if-nez v0, :cond_4

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lfxh;->g:Z

    .line 189
    iget-boolean v0, p0, Lfxh;->g:Z

    if-nez v0, :cond_0

    .line 190
    invoke-virtual {p0}, Lfxh;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p6, v5}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v0

    iput-object v0, p0, Lfxh;->e:Lizu;

    .line 191
    iget-object v0, p0, Lfxh;->c:Lcom/google/android/libraries/social/media/ui/MediaView;

    iget-object v5, p0, Lfxh;->e:Lizu;

    invoke-virtual {v0, v5, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;Z)V

    .line 194
    :cond_0
    iget-object v5, p0, Lfxh;->c:Lcom/google/android/libraries/social/media/ui/MediaView;

    iget-boolean v0, p0, Lfxh;->g:Z

    if-eqz v0, :cond_5

    move v0, v3

    :goto_1
    invoke-virtual {v5, v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->setVisibility(I)V

    .line 195
    iget-object v0, p0, Lfxh;->f:Landroid/widget/TextView;

    iget-boolean v5, p0, Lfxh;->g:Z

    if-eqz v5, :cond_6

    :goto_2
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 197
    iget-object v0, p0, Lfxh;->d:Lfwn;

    .line 198
    invoke-virtual {p0}, Lfxh;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, p4, p5}, Llhu;->a(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v2

    const/4 v3, 0x0

    .line 197
    invoke-virtual {v0, p1, v2, v3, v1}, Lfwn;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)V

    .line 199
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lfxh;->b:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0, p2, p3}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lfxh;->b:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 200
    :cond_2
    iput-object p8, p0, Lfxh;->o:Lfxf;

    .line 202
    iget-boolean v0, p0, Lfxh;->g:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lfxh;->o:Lfxf;

    if-eqz v0, :cond_3

    .line 203
    iget-object v0, p0, Lfxh;->o:Lfxf;

    iget-object v1, v4, Lnym;->h:Lnyz;

    iget-object v1, v1, Lnyz;->c:Ljava/lang/String;

    iget-object v2, v4, Lnym;->e:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lfxf;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    :cond_3
    :goto_3
    return-void

    .line 179
    :catch_0
    move-exception v0

    .line 180
    const-string v1, "EventPhotoCardLayout"

    const-string v2, "Unable to parse Tile from byte array."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_3

    :cond_4
    move v0, v2

    .line 186
    goto :goto_0

    :cond_5
    move v0, v2

    .line 194
    goto :goto_1

    :cond_6
    move v2, v3

    .line 195
    goto :goto_2
.end method

.method protected measureChildren(II)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/high16 v7, 0x40000000    # 2.0f

    .line 125
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 126
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 127
    sget v2, Lfxh;->l:I

    sub-int v2, v1, v2

    .line 134
    iget-boolean v3, p0, Lfxh;->g:Z

    if-eqz v3, :cond_0

    .line 135
    iget-object v3, p0, Lfxh;->f:Landroid/widget/TextView;

    invoke-static {v3, v8, v8}, Lfxh;->a(Landroid/view/View;II)V

    .line 136
    iget-object v3, p0, Lfxh;->f:Landroid/widget/TextView;

    invoke-static {v0, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 137
    invoke-static {v2, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 136
    invoke-virtual {v3, v4, v5}, Landroid/widget/TextView;->measure(II)V

    .line 145
    :goto_0
    sget v3, Lfxh;->i:I

    add-int/lit8 v3, v3, 0x0

    .line 146
    sget v4, Lfxh;->k:I

    add-int/2addr v4, v3

    .line 147
    sget v5, Lfxh;->j:I

    sub-int v5, v1, v5

    sget v6, Lfxh;->k:I

    sub-int/2addr v5, v6

    .line 149
    iget-object v6, p0, Lfxh;->b:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-static {v6, v3, v5}, Lfxh;->a(Landroid/view/View;II)V

    .line 150
    iget-object v3, p0, Lfxh;->b:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    sget v5, Lfxh;->k:I

    invoke-static {v5, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    sget v6, Lfxh;->k:I

    .line 151
    invoke-static {v6, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    .line 150
    invoke-virtual {v3, v5, v6}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->measure(II)V

    .line 153
    sget v3, Lfxh;->m:I

    add-int/2addr v3, v4

    .line 154
    sub-int/2addr v0, v3

    sget v4, Lfxh;->n:I

    sub-int/2addr v0, v4

    .line 155
    sub-int/2addr v1, v2

    .line 158
    iget-object v4, p0, Lfxh;->d:Lfwn;

    invoke-static {v0, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    const/high16 v5, -0x80000000

    .line 159
    invoke-static {v1, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 158
    invoke-virtual {v4, v0, v5}, Lfwn;->measure(II)V

    .line 161
    iget-object v0, p0, Lfxh;->d:Lfwn;

    invoke-virtual {v0}, Lfwn;->getMeasuredHeight()I

    move-result v0

    .line 162
    sub-int v0, v1, v0

    div-int/lit8 v0, v0, 0x2

    invoke-static {v8, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/2addr v0, v2

    .line 163
    iget-object v1, p0, Lfxh;->d:Lfwn;

    invoke-static {v1, v3, v0}, Lfxh;->a(Landroid/view/View;II)V

    .line 164
    return-void

    .line 139
    :cond_0
    iget-object v3, p0, Lfxh;->c:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-static {v3, v8, v8}, Lfxh;->a(Landroid/view/View;II)V

    .line 140
    iget-object v3, p0, Lfxh;->c:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-static {v0, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 141
    invoke-static {v2, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 140
    invoke-virtual {v3, v4, v5}, Lcom/google/android/libraries/social/media/ui/MediaView;->measure(II)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 231
    iget-object v0, p0, Lfxh;->b:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lfxh;->o:Lfxf;

    if-eqz v0, :cond_1

    .line 232
    iget-object v0, p0, Lfxh;->o:Lfxf;

    check-cast p1, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {p1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->e()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lfxf;->b(Ljava/lang/String;)V

    .line 236
    :cond_0
    :goto_0
    return-void

    .line 233
    :cond_1
    iget-object v0, p0, Lfxh;->c:Lcom/google/android/libraries/social/media/ui/MediaView;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lfxh;->o:Lfxf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfxh;->a:Lnzx;

    if-eqz v0, :cond_0

    .line 234
    iget-object v0, p0, Lfxh;->o:Lfxf;

    iget-object v1, p0, Lfxh;->a:Lnzx;

    invoke-interface {v0, v1}, Lfxf;->a(Lnzx;)V

    goto :goto_0
.end method
