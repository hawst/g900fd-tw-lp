.class final Lhvz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic a:Lhvy;


# direct methods
.method constructor <init>(Lhvy;)V
    .locals 0

    .prologue
    .line 107
    iput-object p1, p0, Lhvz;->a:Lhvy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 110
    iget-object v1, p0, Lhvz;->a:Lhvy;

    invoke-static {v1}, Lhvy;->a(Lhvy;)[Lhwa;

    move-result-object v1

    if-nez v1, :cond_1

    .line 163
    :cond_0
    :goto_0
    return-void

    .line 114
    :cond_1
    iget-object v1, p0, Lhvz;->a:Lhvy;

    invoke-static {v1}, Lhvy;->a(Lhvy;)[Lhwa;

    move-result-object v1

    array-length v4, v1

    .line 115
    if-le v4, v5, :cond_0

    .line 119
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 120
    iget-object v1, p0, Lhvz;->a:Lhvy;

    invoke-static {v1}, Lhvy;->b(Lhvy;)I

    move-result v1

    if-nez v1, :cond_5

    .line 121
    iget-object v1, p0, Lhvz;->a:Lhvy;

    invoke-static {v1}, Lhvy;->c(Lhvy;)J

    move-result-wide v6

    cmp-long v1, v2, v6

    if-ltz v1, :cond_4

    .line 122
    iget-object v1, p0, Lhvz;->a:Lhvy;

    invoke-static {v1, v5}, Lhvy;->a(Lhvy;I)I

    .line 123
    iget-object v1, p0, Lhvz;->a:Lhvy;

    const-wide/16 v6, 0x3e8

    add-long/2addr v2, v6

    invoke-static {v1, v2, v3}, Lhvy;->a(Lhvy;J)J

    .line 124
    iget-object v1, p0, Lhvz;->a:Lhvy;

    iget-object v2, p0, Lhvz;->a:Lhvy;

    invoke-static {v2}, Lhvy;->d(Lhvy;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    rem-int/2addr v2, v4

    invoke-static {v1, v2}, Lhvy;->b(Lhvy;I)I

    .line 126
    iget-object v1, p0, Lhvz;->a:Lhvy;

    invoke-static {v1}, Lhvy;->a(Lhvy;)[Lhwa;

    move-result-object v1

    aget-object v1, v1, v0

    iget-object v1, v1, Lhwa;->d:Landroid/graphics/Rect;

    .line 128
    iget v5, v1, Landroid/graphics/Rect;->left:I

    .line 129
    iget v6, v1, Landroid/graphics/Rect;->right:I

    move v1, v0

    move v2, v0

    .line 132
    :goto_1
    if-ge v1, v4, :cond_3

    .line 133
    iget-object v0, p0, Lhvz;->a:Lhvy;

    .line 134
    invoke-static {v0}, Lhvy;->e(Lhvy;)I

    move-result v0

    if-ne v1, v0, :cond_2

    iget-object v0, p0, Lhvz;->a:Lhvy;

    invoke-static {v0}, Lhvy;->f(Lhvy;)I

    move-result v0

    :goto_2
    add-int v3, v2, v0

    .line 135
    iget-object v0, p0, Lhvz;->a:Lhvy;

    invoke-static {v0}, Lhvy;->a(Lhvy;)[Lhwa;

    move-result-object v0

    aget-object v0, v0, v1

    iget-object v0, v0, Lhwa;->d:Landroid/graphics/Rect;

    invoke-virtual {v0, v5, v2, v6, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 132
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v2, v3

    goto :goto_1

    .line 134
    :cond_2
    iget-object v0, p0, Lhvz;->a:Lhvy;

    invoke-static {v0}, Lhvy;->f(Lhvy;)I

    move-result v0

    div-int/lit8 v0, v0, 0x4

    goto :goto_2

    .line 139
    :cond_3
    iget-object v0, p0, Lhvz;->a:Lhvy;

    invoke-virtual {v0}, Lhvy;->invalidate()V

    .line 160
    :cond_4
    :goto_3
    iget-object v0, p0, Lhvz;->a:Lhvy;

    invoke-static {v0}, Lhvy;->g(Lhvy;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 161
    invoke-static {}, Llsx;->d()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lhvz;->a:Lhvy;

    invoke-static {v1}, Lhvy;->h(Lhvy;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x10

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 141
    :cond_5
    iget-object v1, p0, Lhvz;->a:Lhvy;

    invoke-static {v1}, Lhvy;->b(Lhvy;)I

    move-result v1

    if-ne v1, v5, :cond_0

    .line 142
    iget-object v1, p0, Lhvz;->a:Lhvy;

    invoke-static {v1}, Lhvy;->c(Lhvy;)J

    move-result-wide v6

    cmp-long v1, v2, v6

    if-ltz v1, :cond_6

    .line 143
    iget-object v1, p0, Lhvz;->a:Lhvy;

    invoke-static {v1, v0}, Lhvy;->a(Lhvy;I)I

    .line 144
    iget-object v1, p0, Lhvz;->a:Lhvy;

    const-wide/16 v6, 0x1b58

    add-long/2addr v2, v6

    invoke-static {v1, v2, v3}, Lhvy;->a(Lhvy;J)J

    .line 145
    iget-object v1, p0, Lhvz;->a:Lhvy;

    invoke-static {v1, v8}, Lhvy;->a(Lhvy;F)F

    .line 146
    iget-object v1, p0, Lhvz;->a:Lhvy;

    iget-object v2, p0, Lhvz;->a:Lhvy;

    invoke-static {v2}, Lhvy;->e(Lhvy;)I

    move-result v2

    invoke-static {v1, v2}, Lhvy;->c(Lhvy;I)I

    .line 147
    :goto_4
    if-ge v0, v4, :cond_7

    .line 148
    iget-object v1, p0, Lhvz;->a:Lhvy;

    invoke-static {v1}, Lhvy;->a(Lhvy;)[Lhwa;

    move-result-object v1

    aget-object v1, v1, v0

    iget-object v1, v1, Lhwa;->d:Landroid/graphics/Rect;

    iget-object v2, p0, Lhvz;->a:Lhvy;

    .line 149
    invoke-static {v2}, Lhvy;->a(Lhvy;)[Lhwa;

    move-result-object v2

    aget-object v2, v2, v0

    iget-object v2, v2, Lhwa;->d:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 147
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 152
    :cond_6
    iget-object v0, p0, Lhvz;->a:Lhvy;

    iget-object v1, p0, Lhvz;->a:Lhvy;

    .line 153
    invoke-static {v1}, Lhvy;->c(Lhvy;)J

    move-result-wide v4

    sub-long v2, v4, v2

    long-to-float v1, v2

    mul-float/2addr v1, v8

    const/high16 v2, 0x447a0000    # 1000.0f

    div-float/2addr v1, v2

    .line 152
    invoke-static {v0, v1}, Lhvy;->a(Lhvy;F)F

    .line 155
    :cond_7
    iget-object v0, p0, Lhvz;->a:Lhvy;

    invoke-virtual {v0}, Lhvy;->invalidate()V

    goto :goto_3
.end method
