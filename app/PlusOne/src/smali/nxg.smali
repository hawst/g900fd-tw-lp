.class public final Lnxg;
.super Loxq;
.source "PG"


# instance fields
.field public a:[I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 119
    invoke-direct {p0}, Loxq;-><init>()V

    .line 122
    sget-object v0, Loxx;->a:[I

    iput-object v0, p0, Lnxg;->a:[I

    .line 119
    return-void
.end method


# virtual methods
.method public a()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 138
    .line 139
    iget-object v1, p0, Lnxg;->a:[I

    if-eqz v1, :cond_1

    iget-object v1, p0, Lnxg;->a:[I

    array-length v1, v1

    if-lez v1, :cond_1

    .line 141
    iget-object v2, p0, Lnxg;->a:[I

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v0, v3, :cond_0

    aget v4, v2, v0

    .line 143
    invoke-static {v4}, Loxo;->i(I)I

    move-result v4

    add-int/2addr v1, v4

    .line 141
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 146
    :cond_0
    iget-object v0, p0, Lnxg;->a:[I

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v1

    .line 148
    :cond_1
    iget-object v1, p0, Lnxg;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 149
    iput v0, p0, Lnxg;->ai:I

    .line 150
    return v0
.end method

.method public a(Loxn;)Lnxg;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 158
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 159
    sparse-switch v0, :sswitch_data_0

    .line 163
    iget-object v1, p0, Lnxg;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 164
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnxg;->ah:Ljava/util/List;

    .line 167
    :cond_1
    iget-object v1, p0, Lnxg;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 169
    :sswitch_0
    return-object p0

    .line 174
    :sswitch_1
    const/16 v0, 0x8

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 175
    iget-object v0, p0, Lnxg;->a:[I

    array-length v0, v0

    .line 176
    add-int/2addr v1, v0

    new-array v1, v1, [I

    .line 177
    iget-object v2, p0, Lnxg;->a:[I

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 178
    iput-object v1, p0, Lnxg;->a:[I

    .line 179
    :goto_1
    iget-object v1, p0, Lnxg;->a:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 180
    iget-object v1, p0, Lnxg;->a:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    .line 181
    invoke-virtual {p1}, Loxn;->a()I

    .line 179
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 184
    :cond_2
    iget-object v1, p0, Lnxg;->a:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    goto :goto_0

    .line 159
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 127
    iget-object v0, p0, Lnxg;->a:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lnxg;->a:[I

    array-length v0, v0

    if-lez v0, :cond_0

    .line 128
    iget-object v1, p0, Lnxg;->a:[I

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget v3, v1, v0

    .line 129
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->a(II)V

    .line 128
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 132
    :cond_0
    iget-object v0, p0, Lnxg;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 134
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 115
    invoke-virtual {p0, p1}, Lnxg;->a(Loxn;)Lnxg;

    move-result-object v0

    return-object v0
.end method
