.class final Lkek;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lkee;",
            "Lkel;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lkel;


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lkek;->a:Ljava/util/Map;

    .line 24
    new-instance v0, Lkel;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lkel;-><init>(Lkee;)V

    iput-object v0, p0, Lkek;->b:Lkel;

    .line 133
    return-void
.end method

.method private static a(Lkel;)V
    .locals 2

    .prologue
    .line 124
    iget-object v0, p0, Lkel;->a:Lkel;

    iget-object v1, p0, Lkel;->b:Lkel;

    iput-object v1, v0, Lkel;->b:Lkel;

    .line 125
    iget-object v0, p0, Lkel;->b:Lkel;

    iget-object v1, p0, Lkel;->a:Lkel;

    iput-object v1, v0, Lkel;->a:Lkel;

    .line 126
    return-void
.end method


# virtual methods
.method public a()Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    .line 73
    iget-object v0, p0, Lkek;->b:Lkel;

    iget-object v0, v0, Lkel;->b:Lkel;

    .line 74
    const/4 v1, 0x0

    .line 75
    :goto_0
    iget-object v2, p0, Lkek;->b:Lkel;

    if-eq v0, v2, :cond_1

    .line 76
    invoke-virtual {v0}, Lkel;->a()I

    move-result v2

    if-lez v2, :cond_0

    .line 77
    invoke-virtual {v0}, Lkel;->b()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 89
    :goto_1
    return-object v0

    .line 84
    :cond_0
    iget-object v2, p0, Lkek;->a:Ljava/util/Map;

    iget-object v3, v0, Lkel;->c:Lkee;

    invoke-interface {v2, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    invoke-static {v0}, Lkek;->a(Lkel;)V

    .line 87
    iget-object v0, v0, Lkel;->b:Lkel;

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method public a(Lkee;)Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 52
    iget-object v0, p0, Lkek;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkel;

    .line 54
    if-nez v0, :cond_0

    .line 55
    new-instance v0, Lkel;

    invoke-direct {v0, p1}, Lkel;-><init>(Lkee;)V

    .line 56
    iget-object v1, p0, Lkek;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    :goto_0
    iget-object v1, p0, Lkek;->b:Lkel;

    iput-object v1, v0, Lkel;->b:Lkel;

    iget-object v1, p0, Lkek;->b:Lkel;

    iget-object v1, v1, Lkel;->a:Lkel;

    iput-object v1, v0, Lkel;->a:Lkel;

    iget-object v1, v0, Lkel;->a:Lkel;

    iput-object v0, v1, Lkel;->b:Lkel;

    iget-object v1, p0, Lkek;->b:Lkel;

    iput-object v0, v1, Lkel;->a:Lkel;

    .line 64
    invoke-virtual {v0}, Lkel;->b()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0

    .line 58
    :cond_0
    invoke-static {v0}, Lkek;->a(Lkel;)V

    goto :goto_0
.end method

.method public a(Lkee;Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 31
    iget-object v0, p0, Lkek;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkel;

    .line 32
    if-nez v0, :cond_0

    .line 33
    new-instance v0, Lkel;

    invoke-direct {v0, p1}, Lkel;-><init>(Lkee;)V

    .line 34
    iget-object v1, p0, Lkek;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    iget-object v1, p0, Lkek;->b:Lkel;

    iget-object v1, v1, Lkel;->b:Lkel;

    iput-object v1, v0, Lkel;->b:Lkel;

    iget-object v1, p0, Lkek;->b:Lkel;

    iput-object v1, v0, Lkel;->a:Lkel;

    iget-object v1, v0, Lkel;->b:Lkel;

    iput-object v0, v1, Lkel;->a:Lkel;

    iget-object v1, p0, Lkek;->b:Lkel;

    iput-object v0, v1, Lkel;->b:Lkel;

    .line 40
    :cond_0
    invoke-virtual {v0, p2}, Lkel;->a(Landroid/graphics/Bitmap;)V

    .line 41
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 7

    .prologue
    .line 94
    iget-object v0, p0, Lkek;->b:Lkel;

    iget-object v1, v0, Lkel;->a:Lkel;

    .line 95
    const-string v0, "GroupedLinkedMap("

    .line 96
    iget-object v2, p0, Lkek;->a:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 97
    :goto_0
    iget-object v2, p0, Lkek;->b:Lkel;

    if-eq v1, v2, :cond_0

    .line 98
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, v1, Lkel;->c:Lkee;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lkel;->a()I

    move-result v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x10

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "{"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "} "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 99
    iget-object v1, v1, Lkel;->a:Lkel;

    goto :goto_0

    .line 101
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 103
    :cond_1
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
