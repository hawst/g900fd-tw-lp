.class public final Lpw;
.super Landroid/app/Dialog;
.source "PG"


# instance fields
.field private final a:Lvp;

.field private final b:Lpx;

.field private c:Lvn;

.field private d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lvy;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lpy;

.field private f:Landroid/widget/ListView;

.field private g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lpw;-><init>(Landroid/content/Context;I)V

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lqi;->a(Landroid/content/Context;Z)Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 54
    sget-object v0, Lvn;->a:Lvn;

    iput-object v0, p0, Lpw;->c:Lvn;

    .line 66
    invoke-virtual {p0}, Lpw;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 68
    invoke-static {v0}, Lvp;->a(Landroid/content/Context;)Lvp;

    move-result-object v0

    iput-object v0, p0, Lpw;->a:Lvp;

    .line 69
    new-instance v0, Lpx;

    invoke-direct {v0, p0}, Lpx;-><init>(Lpw;)V

    iput-object v0, p0, Lpw;->b:Lpx;

    .line 70
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 180
    iget-boolean v0, p0, Lpw;->g:Z

    if-eqz v0, :cond_0

    .line 181
    iget-object v0, p0, Lpw;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 182
    iget-object v0, p0, Lpw;->d:Ljava/util/ArrayList;

    iget-object v1, p0, Lpw;->a:Lvp;

    invoke-virtual {v1}, Lvp;->a()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 183
    iget-object v0, p0, Lpw;->d:Ljava/util/ArrayList;

    invoke-virtual {p0, v0}, Lpw;->a(Ljava/util/List;)V

    .line 184
    iget-object v0, p0, Lpw;->d:Ljava/util/ArrayList;

    sget-object v1, Lpz;->a:Lpz;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 185
    iget-object v0, p0, Lpw;->e:Lpy;

    invoke-virtual {v0}, Lpy;->notifyDataSetChanged()V

    .line 187
    :cond_0
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lvy;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 115
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    add-int/lit8 v1, v0, -0x1

    if-lez v0, :cond_0

    .line 116
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lvy;

    invoke-virtual {p0, v0}, Lpw;->a(Lvy;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 117
    invoke-interface {p1, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move v0, v1

    goto :goto_0

    .line 120
    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public a(Lvn;)V
    .locals 3

    .prologue
    .line 88
    if-nez p1, :cond_0

    .line 89
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "selector must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 92
    :cond_0
    iget-object v0, p0, Lpw;->c:Lvn;

    invoke-virtual {v0, p1}, Lvn;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 93
    iput-object p1, p0, Lpw;->c:Lvn;

    .line 95
    iget-boolean v0, p0, Lpw;->g:Z

    if-eqz v0, :cond_1

    .line 96
    iget-object v0, p0, Lpw;->a:Lvp;

    iget-object v1, p0, Lpw;->b:Lpx;

    invoke-virtual {v0, v1}, Lvp;->a(Lvq;)V

    .line 97
    iget-object v0, p0, Lpw;->a:Lvp;

    iget-object v1, p0, Lpw;->b:Lpx;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v1, v2}, Lvp;->a(Lvn;Lvq;I)V

    .line 101
    :cond_1
    invoke-virtual {p0}, Lpw;->a()V

    .line 103
    :cond_2
    return-void
.end method

.method public a(Lvy;)Z
    .locals 1

    .prologue
    .line 134
    invoke-virtual {p1}, Lvy;->g()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lvy;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lpw;->c:Lvn;

    invoke-virtual {p1, v0}, Lvy;->a(Lvn;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 161
    invoke-super {p0}, Landroid/app/Dialog;->onAttachedToWindow()V

    .line 163
    iput-boolean v3, p0, Lpw;->g:Z

    .line 164
    iget-object v0, p0, Lpw;->a:Lvp;

    iget-object v1, p0, Lpw;->c:Lvn;

    iget-object v2, p0, Lpw;->b:Lpx;

    invoke-virtual {v0, v1, v2, v3}, Lvp;->a(Lvn;Lvq;I)V

    .line 165
    invoke-virtual {p0}, Lpw;->a()V

    .line 166
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 139
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 141
    invoke-virtual {p0}, Lpw;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/Window;->requestFeature(I)Z

    .line 143
    const v0, 0x7f040113

    invoke-virtual {p0, v0}, Lpw;->setContentView(I)V

    .line 144
    const v0, 0x7f0a05a1

    invoke-virtual {p0, v0}, Lpw;->setTitle(I)V

    .line 147
    invoke-virtual {p0}, Lpw;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {p0}, Lpw;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f01009c

    invoke-static {v1, v2}, Lqi;->a(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v3, v1}, Landroid/view/Window;->setFeatureDrawableResource(II)V

    .line 151
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lpw;->d:Ljava/util/ArrayList;

    .line 152
    new-instance v0, Lpy;

    invoke-virtual {p0}, Lpw;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lpw;->d:Ljava/util/ArrayList;

    invoke-direct {v0, p0, v1, v2}, Lpy;-><init>(Lpw;Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Lpw;->e:Lpy;

    .line 153
    const v0, 0x7f10038d

    invoke-virtual {p0, v0}, Lpw;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lpw;->f:Landroid/widget/ListView;

    .line 154
    iget-object v0, p0, Lpw;->f:Landroid/widget/ListView;

    iget-object v1, p0, Lpw;->e:Lpy;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 155
    iget-object v0, p0, Lpw;->f:Landroid/widget/ListView;

    iget-object v1, p0, Lpw;->e:Lpy;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 156
    iget-object v0, p0, Lpw;->f:Landroid/widget/ListView;

    const v1, 0x1020004

    invoke-virtual {p0, v1}, Lpw;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 157
    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 170
    const/4 v0, 0x0

    iput-boolean v0, p0, Lpw;->g:Z

    .line 171
    iget-object v0, p0, Lpw;->a:Lvp;

    iget-object v1, p0, Lpw;->b:Lpx;

    invoke-virtual {v0, v1}, Lvp;->a(Lvq;)V

    .line 173
    invoke-super {p0}, Landroid/app/Dialog;->onDetachedFromWindow()V

    .line 174
    return-void
.end method
