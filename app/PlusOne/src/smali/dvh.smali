.class public final Ldvh;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private synthetic a:Landroid/content/Context;

.field private synthetic b:I

.field private synthetic c:Z

.field private synthetic d:Ldvj;


# direct methods
.method public constructor <init>(Landroid/content/Context;IZLdvj;)V
    .locals 0

    .prologue
    .line 840
    iput-object p1, p0, Ldvh;->a:Landroid/content/Context;

    iput p2, p0, Ldvh;->b:I

    iput-boolean p3, p0, Ldvh;->c:Z

    iput-object p4, p0, Ldvh;->d:Ldvj;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs a()Ljava/lang/Void;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 843
    iget-object v0, p0, Ldvh;->a:Landroid/content/Context;

    iget v1, p0, Ldvh;->b:I

    .line 844
    invoke-static {v1}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->b(I)Ljava/lang/String;

    move-result-object v1

    .line 843
    invoke-virtual {v0, v1, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 847
    const-string v1, "aam_cluster_max_view_timestamp"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 848
    iget-object v1, p0, Ldvh;->a:Landroid/content/Context;

    iget v4, p0, Ldvh;->b:I

    .line 849
    invoke-static {v1, v4}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->i(Landroid/content/Context;I)J

    move-result-wide v4

    .line 848
    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 850
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "new_aam_count"

    invoke-interface {v0, v1, v6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "aam_cluster_max_view_timestamp"

    .line 851
    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 852
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 853
    iget-object v0, p0, Ldvh;->a:Landroid/content/Context;

    iget v1, p0, Ldvh;->b:I

    iget-boolean v4, p0, Ldvh;->c:Z

    invoke-static {v0, v1, v4}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->b(Landroid/content/Context;IZ)V

    .line 854
    iget-object v0, p0, Ldvh;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->b(Landroid/content/Context;)Ljdw;

    move-result-object v0

    iget v1, p0, Ldvh;->b:I

    .line 855
    invoke-interface {v0, v1, v2, v3}, Ljdw;->a(IJ)V

    .line 856
    iget-object v0, p0, Ldvh;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->b(Landroid/content/Context;)Ljdw;

    move-result-object v0

    iget v1, p0, Ldvh;->b:I

    invoke-interface {v0, v1, v2, v3}, Ljdw;->a(IJ)V

    .line 857
    const/4 v0, 0x0

    return-object v0
.end method

.method protected b()V
    .locals 1

    .prologue
    .line 862
    iget-object v0, p0, Ldvh;->d:Ldvj;

    if-eqz v0, :cond_0

    .line 863
    iget-object v0, p0, Ldvh;->d:Ldvj;

    invoke-interface {v0}, Ldvj;->a()V

    .line 865
    :cond_0
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 840
    invoke-virtual {p0}, Ldvh;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 840
    invoke-virtual {p0}, Ldvh;->b()V

    return-void
.end method
