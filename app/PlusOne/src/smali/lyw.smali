.class public final Llyw;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lmri;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3967
    invoke-direct {p0}, Loxq;-><init>()V

    .line 3970
    iput-object v0, p0, Llyw;->apiHeader:Llyq;

    .line 3973
    iput-object v0, p0, Llyw;->a:Lmri;

    .line 3967
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 3990
    const/4 v0, 0x0

    .line 3991
    iget-object v1, p0, Llyw;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 3992
    const/4 v0, 0x1

    iget-object v1, p0, Llyw;->apiHeader:Llyq;

    .line 3993
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3995
    :cond_0
    iget-object v1, p0, Llyw;->a:Lmri;

    if-eqz v1, :cond_1

    .line 3996
    const/4 v1, 0x2

    iget-object v2, p0, Llyw;->a:Lmri;

    .line 3997
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3999
    :cond_1
    iget-object v1, p0, Llyw;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4000
    iput v0, p0, Llyw;->ai:I

    .line 4001
    return v0
.end method

.method public a(Loxn;)Llyw;
    .locals 2

    .prologue
    .line 4009
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 4010
    sparse-switch v0, :sswitch_data_0

    .line 4014
    iget-object v1, p0, Llyw;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 4015
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llyw;->ah:Ljava/util/List;

    .line 4018
    :cond_1
    iget-object v1, p0, Llyw;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4020
    :sswitch_0
    return-object p0

    .line 4025
    :sswitch_1
    iget-object v0, p0, Llyw;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 4026
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Llyw;->apiHeader:Llyq;

    .line 4028
    :cond_2
    iget-object v0, p0, Llyw;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 4032
    :sswitch_2
    iget-object v0, p0, Llyw;->a:Lmri;

    if-nez v0, :cond_3

    .line 4033
    new-instance v0, Lmri;

    invoke-direct {v0}, Lmri;-><init>()V

    iput-object v0, p0, Llyw;->a:Lmri;

    .line 4035
    :cond_3
    iget-object v0, p0, Llyw;->a:Lmri;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 4010
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 3978
    iget-object v0, p0, Llyw;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 3979
    const/4 v0, 0x1

    iget-object v1, p0, Llyw;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 3981
    :cond_0
    iget-object v0, p0, Llyw;->a:Lmri;

    if-eqz v0, :cond_1

    .line 3982
    const/4 v0, 0x2

    iget-object v1, p0, Llyw;->a:Lmri;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 3984
    :cond_1
    iget-object v0, p0, Llyw;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3986
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3963
    invoke-virtual {p0, p1}, Llyw;->a(Loxn;)Llyw;

    move-result-object v0

    return-object v0
.end method
