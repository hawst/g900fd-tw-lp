.class public final Lpbr;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lpbr;


# instance fields
.field private b:Lozs;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:[Ljava/lang/String;

.field private g:Ljava/lang/Boolean;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:I

.field private p:Loya;

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Lpbr;

    sput-object v0, Lpbr;->a:[Lpbr;

    .line 13
    const v0, 0x1a5c095

    new-instance v1, Lpbs;

    invoke-direct {v1}, Lpbs;-><init>()V

    .line 14
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    .line 13
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 25
    iput-object v1, p0, Lpbr;->b:Lozs;

    .line 34
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lpbr;->f:[Ljava/lang/String;

    .line 53
    const/high16 v0, -0x80000000

    iput v0, p0, Lpbr;->o:I

    .line 56
    iput-object v1, p0, Lpbr;->p:Loya;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 134
    .line 135
    iget-object v0, p0, Lpbr;->b:Lozs;

    if-eqz v0, :cond_13

    .line 136
    const/4 v0, 0x1

    iget-object v2, p0, Lpbr;->b:Lozs;

    .line 137
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 139
    :goto_0
    iget-object v2, p0, Lpbr;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 140
    const/4 v2, 0x2

    iget-object v3, p0, Lpbr;->c:Ljava/lang/String;

    .line 141
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 143
    :cond_0
    iget-object v2, p0, Lpbr;->d:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 144
    const/4 v2, 0x3

    iget-object v3, p0, Lpbr;->d:Ljava/lang/String;

    .line 145
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 147
    :cond_1
    iget-object v2, p0, Lpbr;->e:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 148
    const/4 v2, 0x4

    iget-object v3, p0, Lpbr;->e:Ljava/lang/String;

    .line 149
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 151
    :cond_2
    iget-object v2, p0, Lpbr;->g:Ljava/lang/Boolean;

    if-eqz v2, :cond_3

    .line 152
    const/4 v2, 0x5

    iget-object v3, p0, Lpbr;->g:Ljava/lang/Boolean;

    .line 153
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 155
    :cond_3
    iget-object v2, p0, Lpbr;->h:Ljava/lang/String;

    if-eqz v2, :cond_4

    .line 156
    const/4 v2, 0x6

    iget-object v3, p0, Lpbr;->h:Ljava/lang/String;

    .line 157
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 159
    :cond_4
    iget-object v2, p0, Lpbr;->i:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 160
    const/4 v2, 0x7

    iget-object v3, p0, Lpbr;->i:Ljava/lang/String;

    .line 161
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 163
    :cond_5
    iget-object v2, p0, Lpbr;->j:Ljava/lang/String;

    if-eqz v2, :cond_6

    .line 164
    const/16 v2, 0x8

    iget-object v3, p0, Lpbr;->j:Ljava/lang/String;

    .line 165
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 167
    :cond_6
    iget-object v2, p0, Lpbr;->k:Ljava/lang/String;

    if-eqz v2, :cond_7

    .line 168
    const/16 v2, 0x9

    iget-object v3, p0, Lpbr;->k:Ljava/lang/String;

    .line 169
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 171
    :cond_7
    iget-object v2, p0, Lpbr;->l:Ljava/lang/String;

    if-eqz v2, :cond_8

    .line 172
    const/16 v2, 0xa

    iget-object v3, p0, Lpbr;->l:Ljava/lang/String;

    .line 173
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 175
    :cond_8
    iget-object v2, p0, Lpbr;->m:Ljava/lang/String;

    if-eqz v2, :cond_9

    .line 176
    const/16 v2, 0xb

    iget-object v3, p0, Lpbr;->m:Ljava/lang/String;

    .line 177
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 179
    :cond_9
    iget-object v2, p0, Lpbr;->n:Ljava/lang/String;

    if-eqz v2, :cond_a

    .line 180
    const/16 v2, 0xc

    iget-object v3, p0, Lpbr;->n:Ljava/lang/String;

    .line 181
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 183
    :cond_a
    iget v2, p0, Lpbr;->o:I

    const/high16 v3, -0x80000000

    if-eq v2, v3, :cond_b

    .line 184
    const/16 v2, 0xd

    iget v3, p0, Lpbr;->o:I

    .line 185
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 187
    :cond_b
    iget-object v2, p0, Lpbr;->f:[Ljava/lang/String;

    if-eqz v2, :cond_d

    iget-object v2, p0, Lpbr;->f:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_d

    .line 189
    iget-object v3, p0, Lpbr;->f:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_c

    aget-object v5, v3, v1

    .line 191
    invoke-static {v5}, Loxo;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 189
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 193
    :cond_c
    add-int/2addr v0, v2

    .line 194
    iget-object v1, p0, Lpbr;->f:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 196
    :cond_d
    iget-object v1, p0, Lpbr;->p:Loya;

    if-eqz v1, :cond_e

    .line 197
    const/16 v1, 0xf

    iget-object v2, p0, Lpbr;->p:Loya;

    .line 198
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 200
    :cond_e
    iget-object v1, p0, Lpbr;->q:Ljava/lang/String;

    if-eqz v1, :cond_f

    .line 201
    const/16 v1, 0x10

    iget-object v2, p0, Lpbr;->q:Ljava/lang/String;

    .line 202
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 204
    :cond_f
    iget-object v1, p0, Lpbr;->r:Ljava/lang/String;

    if-eqz v1, :cond_10

    .line 205
    const/16 v1, 0x11

    iget-object v2, p0, Lpbr;->r:Ljava/lang/String;

    .line 206
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 208
    :cond_10
    iget-object v1, p0, Lpbr;->s:Ljava/lang/String;

    if-eqz v1, :cond_11

    .line 209
    const/16 v1, 0x12

    iget-object v2, p0, Lpbr;->s:Ljava/lang/String;

    .line 210
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 212
    :cond_11
    iget-object v1, p0, Lpbr;->t:Ljava/lang/String;

    if-eqz v1, :cond_12

    .line 213
    const/16 v1, 0x13

    iget-object v2, p0, Lpbr;->t:Ljava/lang/String;

    .line 214
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 216
    :cond_12
    iget-object v1, p0, Lpbr;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 217
    iput v0, p0, Lpbr;->ai:I

    .line 218
    return v0

    :cond_13
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lpbr;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 226
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 227
    sparse-switch v0, :sswitch_data_0

    .line 231
    iget-object v1, p0, Lpbr;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 232
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpbr;->ah:Ljava/util/List;

    .line 235
    :cond_1
    iget-object v1, p0, Lpbr;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 237
    :sswitch_0
    return-object p0

    .line 242
    :sswitch_1
    iget-object v0, p0, Lpbr;->b:Lozs;

    if-nez v0, :cond_2

    .line 243
    new-instance v0, Lozs;

    invoke-direct {v0}, Lozs;-><init>()V

    iput-object v0, p0, Lpbr;->b:Lozs;

    .line 245
    :cond_2
    iget-object v0, p0, Lpbr;->b:Lozs;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 249
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpbr;->c:Ljava/lang/String;

    goto :goto_0

    .line 253
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpbr;->d:Ljava/lang/String;

    goto :goto_0

    .line 257
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpbr;->e:Ljava/lang/String;

    goto :goto_0

    .line 261
    :sswitch_5
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lpbr;->g:Ljava/lang/Boolean;

    goto :goto_0

    .line 265
    :sswitch_6
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpbr;->h:Ljava/lang/String;

    goto :goto_0

    .line 269
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpbr;->i:Ljava/lang/String;

    goto :goto_0

    .line 273
    :sswitch_8
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpbr;->j:Ljava/lang/String;

    goto :goto_0

    .line 277
    :sswitch_9
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpbr;->k:Ljava/lang/String;

    goto :goto_0

    .line 281
    :sswitch_a
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpbr;->l:Ljava/lang/String;

    goto :goto_0

    .line 285
    :sswitch_b
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpbr;->m:Ljava/lang/String;

    goto :goto_0

    .line 289
    :sswitch_c
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpbr;->n:Ljava/lang/String;

    goto :goto_0

    .line 293
    :sswitch_d
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 294
    if-eqz v0, :cond_3

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    const/4 v1, 0x4

    if-ne v0, v1, :cond_4

    .line 299
    :cond_3
    iput v0, p0, Lpbr;->o:I

    goto/16 :goto_0

    .line 301
    :cond_4
    iput v3, p0, Lpbr;->o:I

    goto/16 :goto_0

    .line 306
    :sswitch_e
    const/16 v0, 0x72

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 307
    iget-object v0, p0, Lpbr;->f:[Ljava/lang/String;

    array-length v0, v0

    .line 308
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    .line 309
    iget-object v2, p0, Lpbr;->f:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 310
    iput-object v1, p0, Lpbr;->f:[Ljava/lang/String;

    .line 311
    :goto_1
    iget-object v1, p0, Lpbr;->f:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_5

    .line 312
    iget-object v1, p0, Lpbr;->f:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 313
    invoke-virtual {p1}, Loxn;->a()I

    .line 311
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 316
    :cond_5
    iget-object v1, p0, Lpbr;->f:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto/16 :goto_0

    .line 320
    :sswitch_f
    iget-object v0, p0, Lpbr;->p:Loya;

    if-nez v0, :cond_6

    .line 321
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    iput-object v0, p0, Lpbr;->p:Loya;

    .line 323
    :cond_6
    iget-object v0, p0, Lpbr;->p:Loya;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 327
    :sswitch_10
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpbr;->q:Ljava/lang/String;

    goto/16 :goto_0

    .line 331
    :sswitch_11
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpbr;->r:Ljava/lang/String;

    goto/16 :goto_0

    .line 335
    :sswitch_12
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpbr;->s:Ljava/lang/String;

    goto/16 :goto_0

    .line 339
    :sswitch_13
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpbr;->t:Ljava/lang/String;

    goto/16 :goto_0

    .line 227
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x68 -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
        0x82 -> :sswitch_10
        0x8a -> :sswitch_11
        0x92 -> :sswitch_12
        0x9a -> :sswitch_13
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 69
    iget-object v0, p0, Lpbr;->b:Lozs;

    if-eqz v0, :cond_0

    .line 70
    const/4 v0, 0x1

    iget-object v1, p0, Lpbr;->b:Lozs;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 72
    :cond_0
    iget-object v0, p0, Lpbr;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 73
    const/4 v0, 0x2

    iget-object v1, p0, Lpbr;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 75
    :cond_1
    iget-object v0, p0, Lpbr;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 76
    const/4 v0, 0x3

    iget-object v1, p0, Lpbr;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 78
    :cond_2
    iget-object v0, p0, Lpbr;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 79
    const/4 v0, 0x4

    iget-object v1, p0, Lpbr;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 81
    :cond_3
    iget-object v0, p0, Lpbr;->g:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 82
    const/4 v0, 0x5

    iget-object v1, p0, Lpbr;->g:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 84
    :cond_4
    iget-object v0, p0, Lpbr;->h:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 85
    const/4 v0, 0x6

    iget-object v1, p0, Lpbr;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 87
    :cond_5
    iget-object v0, p0, Lpbr;->i:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 88
    const/4 v0, 0x7

    iget-object v1, p0, Lpbr;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 90
    :cond_6
    iget-object v0, p0, Lpbr;->j:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 91
    const/16 v0, 0x8

    iget-object v1, p0, Lpbr;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 93
    :cond_7
    iget-object v0, p0, Lpbr;->k:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 94
    const/16 v0, 0x9

    iget-object v1, p0, Lpbr;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 96
    :cond_8
    iget-object v0, p0, Lpbr;->l:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 97
    const/16 v0, 0xa

    iget-object v1, p0, Lpbr;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 99
    :cond_9
    iget-object v0, p0, Lpbr;->m:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 100
    const/16 v0, 0xb

    iget-object v1, p0, Lpbr;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 102
    :cond_a
    iget-object v0, p0, Lpbr;->n:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 103
    const/16 v0, 0xc

    iget-object v1, p0, Lpbr;->n:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 105
    :cond_b
    iget v0, p0, Lpbr;->o:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_c

    .line 106
    const/16 v0, 0xd

    iget v1, p0, Lpbr;->o:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 108
    :cond_c
    iget-object v0, p0, Lpbr;->f:[Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 109
    iget-object v1, p0, Lpbr;->f:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_d

    aget-object v3, v1, v0

    .line 110
    const/16 v4, 0xe

    invoke-virtual {p1, v4, v3}, Loxo;->a(ILjava/lang/String;)V

    .line 109
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 113
    :cond_d
    iget-object v0, p0, Lpbr;->p:Loya;

    if-eqz v0, :cond_e

    .line 114
    const/16 v0, 0xf

    iget-object v1, p0, Lpbr;->p:Loya;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 116
    :cond_e
    iget-object v0, p0, Lpbr;->q:Ljava/lang/String;

    if-eqz v0, :cond_f

    .line 117
    const/16 v0, 0x10

    iget-object v1, p0, Lpbr;->q:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 119
    :cond_f
    iget-object v0, p0, Lpbr;->r:Ljava/lang/String;

    if-eqz v0, :cond_10

    .line 120
    const/16 v0, 0x11

    iget-object v1, p0, Lpbr;->r:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 122
    :cond_10
    iget-object v0, p0, Lpbr;->s:Ljava/lang/String;

    if-eqz v0, :cond_11

    .line 123
    const/16 v0, 0x12

    iget-object v1, p0, Lpbr;->s:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 125
    :cond_11
    iget-object v0, p0, Lpbr;->t:Ljava/lang/String;

    if-eqz v0, :cond_12

    .line 126
    const/16 v0, 0x13

    iget-object v1, p0, Lpbr;->t:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 128
    :cond_12
    iget-object v0, p0, Lpbr;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 130
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lpbr;->a(Loxn;)Lpbr;

    move-result-object v0

    return-object v0
.end method
