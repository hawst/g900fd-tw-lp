.class public final Lokp;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lokp;


# instance fields
.field private b:Ljava/lang/String;

.field private c:[Ljava/lang/Integer;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/Boolean;

.field private g:Ljava/lang/Boolean;

.field private h:Ljava/lang/Boolean;

.field private i:Ljava/lang/Integer;

.field private j:Ljava/lang/Boolean;

.field private k:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 109
    const/4 v0, 0x0

    new-array v0, v0, [Lokp;

    sput-object v0, Lokp;->a:[Lokp;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 110
    invoke-direct {p0}, Loxq;-><init>()V

    .line 115
    sget-object v0, Loxx;->g:[Ljava/lang/Integer;

    iput-object v0, p0, Lokp;->c:[Ljava/lang/Integer;

    .line 110
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 174
    .line 175
    iget-object v0, p0, Lokp;->b:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 176
    const/4 v0, 0x1

    iget-object v2, p0, Lokp;->b:Ljava/lang/String;

    .line 177
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 179
    :goto_0
    iget-object v2, p0, Lokp;->e:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 180
    const/4 v2, 0x2

    iget-object v3, p0, Lokp;->e:Ljava/lang/String;

    .line 181
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 183
    :cond_0
    iget-object v2, p0, Lokp;->f:Ljava/lang/Boolean;

    if-eqz v2, :cond_1

    .line 184
    const/4 v2, 0x3

    iget-object v3, p0, Lokp;->f:Ljava/lang/Boolean;

    .line 185
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 187
    :cond_1
    iget-object v2, p0, Lokp;->g:Ljava/lang/Boolean;

    if-eqz v2, :cond_2

    .line 188
    const/4 v2, 0x4

    iget-object v3, p0, Lokp;->g:Ljava/lang/Boolean;

    .line 189
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 191
    :cond_2
    iget-object v2, p0, Lokp;->h:Ljava/lang/Boolean;

    if-eqz v2, :cond_3

    .line 192
    const/4 v2, 0x5

    iget-object v3, p0, Lokp;->h:Ljava/lang/Boolean;

    .line 193
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 195
    :cond_3
    iget-object v2, p0, Lokp;->i:Ljava/lang/Integer;

    if-eqz v2, :cond_4

    .line 196
    const/4 v2, 0x6

    iget-object v3, p0, Lokp;->i:Ljava/lang/Integer;

    .line 197
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 199
    :cond_4
    iget-object v2, p0, Lokp;->j:Ljava/lang/Boolean;

    if-eqz v2, :cond_5

    .line 200
    const/4 v2, 0x7

    iget-object v3, p0, Lokp;->j:Ljava/lang/Boolean;

    .line 201
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 203
    :cond_5
    iget-object v2, p0, Lokp;->c:[Ljava/lang/Integer;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lokp;->c:[Ljava/lang/Integer;

    array-length v2, v2

    if-lez v2, :cond_7

    .line 205
    iget-object v3, p0, Lokp;->c:[Ljava/lang/Integer;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_6

    aget-object v5, v3, v1

    .line 207
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-static {v5}, Loxo;->i(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 205
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 209
    :cond_6
    add-int/2addr v0, v2

    .line 210
    iget-object v1, p0, Lokp;->c:[Ljava/lang/Integer;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 212
    :cond_7
    iget-object v1, p0, Lokp;->k:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    .line 213
    const/16 v1, 0x9

    iget-object v2, p0, Lokp;->k:Ljava/lang/Boolean;

    .line 214
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 216
    :cond_8
    iget-object v1, p0, Lokp;->d:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 217
    const/16 v1, 0xa

    iget-object v2, p0, Lokp;->d:Ljava/lang/String;

    .line 218
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 220
    :cond_9
    iget-object v1, p0, Lokp;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 221
    iput v0, p0, Lokp;->ai:I

    .line 222
    return v0

    :cond_a
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lokp;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 230
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 231
    sparse-switch v0, :sswitch_data_0

    .line 235
    iget-object v1, p0, Lokp;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 236
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lokp;->ah:Ljava/util/List;

    .line 239
    :cond_1
    iget-object v1, p0, Lokp;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 241
    :sswitch_0
    return-object p0

    .line 246
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lokp;->b:Ljava/lang/String;

    goto :goto_0

    .line 250
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lokp;->e:Ljava/lang/String;

    goto :goto_0

    .line 254
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lokp;->f:Ljava/lang/Boolean;

    goto :goto_0

    .line 258
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lokp;->g:Ljava/lang/Boolean;

    goto :goto_0

    .line 262
    :sswitch_5
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lokp;->h:Ljava/lang/Boolean;

    goto :goto_0

    .line 266
    :sswitch_6
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lokp;->i:Ljava/lang/Integer;

    goto :goto_0

    .line 270
    :sswitch_7
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lokp;->j:Ljava/lang/Boolean;

    goto :goto_0

    .line 274
    :sswitch_8
    const/16 v0, 0x40

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 275
    iget-object v0, p0, Lokp;->c:[Ljava/lang/Integer;

    array-length v0, v0

    .line 276
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/Integer;

    .line 277
    iget-object v2, p0, Lokp;->c:[Ljava/lang/Integer;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 278
    iput-object v1, p0, Lokp;->c:[Ljava/lang/Integer;

    .line 279
    :goto_1
    iget-object v1, p0, Lokp;->c:[Ljava/lang/Integer;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 280
    iget-object v1, p0, Lokp;->c:[Ljava/lang/Integer;

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    .line 281
    invoke-virtual {p1}, Loxn;->a()I

    .line 279
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 284
    :cond_2
    iget-object v1, p0, Lokp;->c:[Ljava/lang/Integer;

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    goto/16 :goto_0

    .line 288
    :sswitch_9
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lokp;->k:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 292
    :sswitch_a
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lokp;->d:Ljava/lang/String;

    goto/16 :goto_0

    .line 231
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 136
    iget-object v0, p0, Lokp;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 137
    const/4 v0, 0x1

    iget-object v1, p0, Lokp;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 139
    :cond_0
    iget-object v0, p0, Lokp;->e:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 140
    const/4 v0, 0x2

    iget-object v1, p0, Lokp;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 142
    :cond_1
    iget-object v0, p0, Lokp;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 143
    const/4 v0, 0x3

    iget-object v1, p0, Lokp;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 145
    :cond_2
    iget-object v0, p0, Lokp;->g:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 146
    const/4 v0, 0x4

    iget-object v1, p0, Lokp;->g:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 148
    :cond_3
    iget-object v0, p0, Lokp;->h:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 149
    const/4 v0, 0x5

    iget-object v1, p0, Lokp;->h:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 151
    :cond_4
    iget-object v0, p0, Lokp;->i:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 152
    const/4 v0, 0x6

    iget-object v1, p0, Lokp;->i:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 154
    :cond_5
    iget-object v0, p0, Lokp;->j:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    .line 155
    const/4 v0, 0x7

    iget-object v1, p0, Lokp;->j:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 157
    :cond_6
    iget-object v0, p0, Lokp;->c:[Ljava/lang/Integer;

    if-eqz v0, :cond_7

    .line 158
    iget-object v1, p0, Lokp;->c:[Ljava/lang/Integer;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_7

    aget-object v3, v1, v0

    .line 159
    const/16 v4, 0x8

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p1, v4, v3}, Loxo;->a(II)V

    .line 158
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 162
    :cond_7
    iget-object v0, p0, Lokp;->k:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    .line 163
    const/16 v0, 0x9

    iget-object v1, p0, Lokp;->k:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 165
    :cond_8
    iget-object v0, p0, Lokp;->d:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 166
    const/16 v0, 0xa

    iget-object v1, p0, Lokp;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 168
    :cond_9
    iget-object v0, p0, Lokp;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 170
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 106
    invoke-virtual {p0, p1}, Lokp;->a(Loxn;)Lokp;

    move-result-object v0

    return-object v0
.end method
