.class public final Lomn;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Float;

.field private b:Ljava/lang/Float;

.field private c:[Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 182
    invoke-direct {p0}, Loxq;-><init>()V

    .line 189
    sget-object v0, Loxx;->g:[Ljava/lang/Integer;

    iput-object v0, p0, Lomn;->c:[Ljava/lang/Integer;

    .line 182
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 211
    .line 212
    iget-object v0, p0, Lomn;->a:Ljava/lang/Float;

    if-eqz v0, :cond_3

    .line 213
    const/4 v0, 0x1

    iget-object v2, p0, Lomn;->a:Ljava/lang/Float;

    .line 214
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x0

    .line 216
    :goto_0
    iget-object v2, p0, Lomn;->b:Ljava/lang/Float;

    if-eqz v2, :cond_0

    .line 217
    const/4 v2, 0x2

    iget-object v3, p0, Lomn;->b:Ljava/lang/Float;

    .line 218
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 220
    :cond_0
    iget-object v2, p0, Lomn;->c:[Ljava/lang/Integer;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lomn;->c:[Ljava/lang/Integer;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 222
    iget-object v3, p0, Lomn;->c:[Ljava/lang/Integer;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_1

    aget-object v5, v3, v1

    .line 224
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-static {v5}, Loxo;->i(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 222
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 226
    :cond_1
    add-int/2addr v0, v2

    .line 227
    iget-object v1, p0, Lomn;->c:[Ljava/lang/Integer;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 229
    :cond_2
    iget-object v1, p0, Lomn;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 230
    iput v0, p0, Lomn;->ai:I

    .line 231
    return v0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lomn;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 239
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 240
    sparse-switch v0, :sswitch_data_0

    .line 244
    iget-object v1, p0, Lomn;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 245
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lomn;->ah:Ljava/util/List;

    .line 248
    :cond_1
    iget-object v1, p0, Lomn;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 250
    :sswitch_0
    return-object p0

    .line 255
    :sswitch_1
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lomn;->a:Ljava/lang/Float;

    goto :goto_0

    .line 259
    :sswitch_2
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lomn;->b:Ljava/lang/Float;

    goto :goto_0

    .line 263
    :sswitch_3
    const/16 v0, 0x18

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 264
    iget-object v0, p0, Lomn;->c:[Ljava/lang/Integer;

    array-length v0, v0

    .line 265
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/Integer;

    .line 266
    iget-object v2, p0, Lomn;->c:[Ljava/lang/Integer;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 267
    iput-object v1, p0, Lomn;->c:[Ljava/lang/Integer;

    .line 268
    :goto_1
    iget-object v1, p0, Lomn;->c:[Ljava/lang/Integer;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 269
    iget-object v1, p0, Lomn;->c:[Ljava/lang/Integer;

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    .line 270
    invoke-virtual {p1}, Loxn;->a()I

    .line 268
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 273
    :cond_2
    iget-object v1, p0, Lomn;->c:[Ljava/lang/Integer;

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    .line 240
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x15 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 194
    iget-object v0, p0, Lomn;->a:Ljava/lang/Float;

    if-eqz v0, :cond_0

    .line 195
    const/4 v0, 0x1

    iget-object v1, p0, Lomn;->a:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 197
    :cond_0
    iget-object v0, p0, Lomn;->b:Ljava/lang/Float;

    if-eqz v0, :cond_1

    .line 198
    const/4 v0, 0x2

    iget-object v1, p0, Lomn;->b:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 200
    :cond_1
    iget-object v0, p0, Lomn;->c:[Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 201
    iget-object v1, p0, Lomn;->c:[Ljava/lang/Integer;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 202
    const/4 v4, 0x3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p1, v4, v3}, Loxo;->a(II)V

    .line 201
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 205
    :cond_2
    iget-object v0, p0, Lomn;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 207
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 178
    invoke-virtual {p0, p1}, Lomn;->a(Loxn;)Lomn;

    move-result-object v0

    return-object v0
.end method
