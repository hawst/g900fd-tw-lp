.class public final Lnec;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3206
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 3232
    const/4 v0, 0x0

    .line 3233
    iget-object v1, p0, Lnec;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3234
    const/4 v0, 0x1

    iget-object v1, p0, Lnec;->a:Ljava/lang/String;

    .line 3235
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3237
    :cond_0
    iget-object v1, p0, Lnec;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 3238
    const/4 v1, 0x2

    iget-object v2, p0, Lnec;->b:Ljava/lang/String;

    .line 3239
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3241
    :cond_1
    iget-object v1, p0, Lnec;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 3242
    const/4 v1, 0x3

    iget-object v2, p0, Lnec;->c:Ljava/lang/String;

    .line 3243
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3245
    :cond_2
    iget-object v1, p0, Lnec;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3246
    iput v0, p0, Lnec;->ai:I

    .line 3247
    return v0
.end method

.method public a(Loxn;)Lnec;
    .locals 2

    .prologue
    .line 3255
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3256
    sparse-switch v0, :sswitch_data_0

    .line 3260
    iget-object v1, p0, Lnec;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 3261
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnec;->ah:Ljava/util/List;

    .line 3264
    :cond_1
    iget-object v1, p0, Lnec;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3266
    :sswitch_0
    return-object p0

    .line 3271
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnec;->a:Ljava/lang/String;

    goto :goto_0

    .line 3275
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnec;->b:Ljava/lang/String;

    goto :goto_0

    .line 3279
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnec;->c:Ljava/lang/String;

    goto :goto_0

    .line 3256
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 3217
    iget-object v0, p0, Lnec;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 3218
    const/4 v0, 0x1

    iget-object v1, p0, Lnec;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3220
    :cond_0
    iget-object v0, p0, Lnec;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 3221
    const/4 v0, 0x2

    iget-object v1, p0, Lnec;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3223
    :cond_1
    iget-object v0, p0, Lnec;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 3224
    const/4 v0, 0x3

    iget-object v1, p0, Lnec;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3226
    :cond_2
    iget-object v0, p0, Lnec;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3228
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3202
    invoke-virtual {p0, p1}, Lnec;->a(Loxn;)Lnec;

    move-result-object v0

    return-object v0
.end method
