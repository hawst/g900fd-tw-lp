.class public final Lmjy;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnqn;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 33775
    invoke-direct {p0}, Loxq;-><init>()V

    .line 33778
    iput-object v0, p0, Lmjy;->apiHeader:Llyq;

    .line 33781
    iput-object v0, p0, Lmjy;->a:Lnqn;

    .line 33775
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 33798
    const/4 v0, 0x0

    .line 33799
    iget-object v1, p0, Lmjy;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 33800
    const/4 v0, 0x1

    iget-object v1, p0, Lmjy;->apiHeader:Llyq;

    .line 33801
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 33803
    :cond_0
    iget-object v1, p0, Lmjy;->a:Lnqn;

    if-eqz v1, :cond_1

    .line 33804
    const/4 v1, 0x2

    iget-object v2, p0, Lmjy;->a:Lnqn;

    .line 33805
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 33807
    :cond_1
    iget-object v1, p0, Lmjy;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 33808
    iput v0, p0, Lmjy;->ai:I

    .line 33809
    return v0
.end method

.method public a(Loxn;)Lmjy;
    .locals 2

    .prologue
    .line 33817
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 33818
    sparse-switch v0, :sswitch_data_0

    .line 33822
    iget-object v1, p0, Lmjy;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 33823
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmjy;->ah:Ljava/util/List;

    .line 33826
    :cond_1
    iget-object v1, p0, Lmjy;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 33828
    :sswitch_0
    return-object p0

    .line 33833
    :sswitch_1
    iget-object v0, p0, Lmjy;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 33834
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmjy;->apiHeader:Llyq;

    .line 33836
    :cond_2
    iget-object v0, p0, Lmjy;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 33840
    :sswitch_2
    iget-object v0, p0, Lmjy;->a:Lnqn;

    if-nez v0, :cond_3

    .line 33841
    new-instance v0, Lnqn;

    invoke-direct {v0}, Lnqn;-><init>()V

    iput-object v0, p0, Lmjy;->a:Lnqn;

    .line 33843
    :cond_3
    iget-object v0, p0, Lmjy;->a:Lnqn;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 33818
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 33786
    iget-object v0, p0, Lmjy;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 33787
    const/4 v0, 0x1

    iget-object v1, p0, Lmjy;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 33789
    :cond_0
    iget-object v0, p0, Lmjy;->a:Lnqn;

    if-eqz v0, :cond_1

    .line 33790
    const/4 v0, 0x2

    iget-object v1, p0, Lmjy;->a:Lnqn;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 33792
    :cond_1
    iget-object v0, p0, Lmjy;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 33794
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 33771
    invoke-virtual {p0, p1}, Lmjy;->a(Loxn;)Lmjy;

    move-result-object v0

    return-object v0
.end method
