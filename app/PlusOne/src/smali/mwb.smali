.class public final Lmwb;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmwb;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12100
    const/4 v0, 0x0

    new-array v0, v0, [Lmwb;

    sput-object v0, Lmwb;->a:[Lmwb;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12101
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 12122
    const/4 v0, 0x0

    .line 12123
    iget-object v1, p0, Lmwb;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 12124
    const/4 v0, 0x1

    iget-object v1, p0, Lmwb;->b:Ljava/lang/String;

    .line 12125
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 12127
    :cond_0
    iget-object v1, p0, Lmwb;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 12128
    const/4 v1, 0x2

    iget-object v2, p0, Lmwb;->c:Ljava/lang/Integer;

    .line 12129
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 12131
    :cond_1
    iget-object v1, p0, Lmwb;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12132
    iput v0, p0, Lmwb;->ai:I

    .line 12133
    return v0
.end method

.method public a(Loxn;)Lmwb;
    .locals 2

    .prologue
    .line 12141
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 12142
    sparse-switch v0, :sswitch_data_0

    .line 12146
    iget-object v1, p0, Lmwb;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 12147
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmwb;->ah:Ljava/util/List;

    .line 12150
    :cond_1
    iget-object v1, p0, Lmwb;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 12152
    :sswitch_0
    return-object p0

    .line 12157
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmwb;->b:Ljava/lang/String;

    goto :goto_0

    .line 12161
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmwb;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 12142
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 12110
    iget-object v0, p0, Lmwb;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 12111
    const/4 v0, 0x1

    iget-object v1, p0, Lmwb;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 12113
    :cond_0
    iget-object v0, p0, Lmwb;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 12114
    const/4 v0, 0x2

    iget-object v1, p0, Lmwb;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 12116
    :cond_1
    iget-object v0, p0, Lmwb;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 12118
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 12097
    invoke-virtual {p0, p1}, Lmwb;->a(Loxn;)Lmwb;

    move-result-object v0

    return-object v0
.end method
