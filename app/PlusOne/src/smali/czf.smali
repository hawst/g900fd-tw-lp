.class public Lczf;
.super Llol;
.source "PG"


# instance fields
.field private final N:Lczi;

.field private final O:Lczj;

.field private P:Ldgr;

.field private Q:Ldeo;

.field private R:Ldec;

.field private S:Ldef;

.field private T:Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 26
    invoke-direct {p0}, Llol;-><init>()V

    .line 27
    new-instance v0, Lczi;

    invoke-direct {v0, p0}, Lczi;-><init>(Lczf;)V

    iput-object v0, p0, Lczf;->N:Lczi;

    .line 28
    new-instance v0, Lczj;

    invoke-direct {v0, p0}, Lczj;-><init>(Lczf;)V

    iput-object v0, p0, Lczf;->O:Lczj;

    .line 39
    new-instance v0, Ldep;

    iget-object v1, p0, Lczf;->av:Llqm;

    new-instance v2, Lczg;

    invoke-direct {v2, p0}, Lczg;-><init>(Lczf;)V

    invoke-direct {v0, v1, v2}, Ldep;-><init>(Llqr;Ldeq;)V

    .line 45
    new-instance v0, Lded;

    iget-object v1, p0, Lczf;->av:Llqm;

    new-instance v2, Lczh;

    invoke-direct {v2, p0}, Lczh;-><init>(Lczf;)V

    invoke-direct {v0, v1, v2}, Lded;-><init>(Llqr;Ldee;)V

    .line 118
    return-void
.end method

.method static synthetic a(Lczf;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 26
    iget-object v0, p0, Lczf;->Q:Ldeo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lczf;->Q:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lczf;->Q:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    invoke-interface {v0}, Lddl;->g()Lnym;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lczf;->R:Ldec;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lczf;->T:Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lczf;->Q:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v2

    invoke-interface {v2}, Lddl;->L()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-interface {v2}, Lddl;->J()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Lddl;->I()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    iget-object v3, p0, Lczf;->T:Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;

    invoke-interface {v2}, Lddl;->J()Z

    move-result v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->a(Z)V

    iget-object v3, p0, Lczf;->T:Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;

    if-eqz v0, :cond_3

    :goto_2
    invoke-virtual {v3, v1}, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->setVisibility(I)V

    iget-object v0, p0, Lczf;->T:Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;

    invoke-interface {v2}, Lddl;->g()Lnym;

    move-result-object v1

    iget-object v1, v1, Lnym;->x:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->a(Ljava/lang/Long;)V

    iget-object v0, p0, Lczf;->R:Ldec;

    invoke-virtual {v0}, Ldec;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lczf;->R:Ldec;

    invoke-virtual {v1}, Ldec;->c()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lczf;->R:Ldec;

    invoke-virtual {v2}, Ldec;->d()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lczf;->R:Ldec;

    invoke-virtual {v3}, Ldec;->e()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lczf;->T:Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;

    invoke-virtual {v4, v0, v1, v2}, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lczf;->T:Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lczf;->T:Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;

    iget-object v1, p0, Lczf;->N:Lczi;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->a(Lczk;)V

    iget-object v0, p0, Lczf;->T:Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;

    iget-object v1, p0, Lczf;->O:Lczj;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lczf;->T:Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;

    invoke-virtual {v0}, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->invalidate()V

    iget-object v0, p0, Lczf;->T:Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;

    invoke-virtual {v0}, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;->requestLayout()V

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    const/16 v1, 0x8

    goto :goto_2
.end method

.method static synthetic b(Lczf;)Ldef;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lczf;->S:Ldef;

    return-object v0
.end method

.method static synthetic c(Lczf;)Ldgr;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lczf;->P:Ldgr;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 66
    const v0, 0x7f04017d

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;

    iput-object v0, p0, Lczf;->T:Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;

    .line 68
    iget-object v0, p0, Lczf;->T:Lcom/google/android/apps/photos/viewer/components/captionbar/CaptionBarView;

    return-object v0
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 55
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 56
    iget-object v0, p0, Lczf;->au:Llnh;

    const-class v1, Ldgr;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldgr;

    iput-object v0, p0, Lczf;->P:Ldgr;

    .line 58
    iget-object v0, p0, Lczf;->au:Llnh;

    const-class v1, Ldeo;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldeo;

    iput-object v0, p0, Lczf;->Q:Ldeo;

    .line 59
    iget-object v0, p0, Lczf;->au:Llnh;

    const-class v1, Ldec;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldec;

    iput-object v0, p0, Lczf;->R:Ldec;

    .line 60
    iget-object v0, p0, Lczf;->au:Llnh;

    const-class v1, Ldef;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldef;

    iput-object v0, p0, Lczf;->S:Ldef;

    .line 61
    return-void
.end method
