.class public final Lkro;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Object;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private synthetic a:Lcom/google/android/libraries/social/socialcast/impl/CastService;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/social/socialcast/impl/CastService;)V
    .locals 0

    .prologue
    .line 703
    iput-object p1, p0, Lkro;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs a()Ljava/lang/String;
    .locals 4

    .prologue
    .line 706
    const-string v0, ""

    .line 708
    :try_start_0
    iget-object v1, p0, Lkro;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    invoke-static {v1}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->f(Lcom/google/android/libraries/social/socialcast/impl/CastService;)Lkfs;

    move-result-object v1

    iget-object v2, p0, Lkro;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lkro;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    invoke-static {v3}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->e(Lcom/google/android/libraries/social/socialcast/impl/CastService;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lkfs;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 713
    iget-object v1, p0, Lkro;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    invoke-static {v1, v0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->a(Lcom/google/android/libraries/social/socialcast/impl/CastService;Ljava/lang/String;)Ljava/lang/String;

    .line 714
    :goto_0
    return-object v0

    .line 709
    :catch_0
    move-exception v1

    .line 710
    const-string v2, "CastService"

    const-string v3, "error getting auth token"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method protected a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 719
    iget-object v0, p0, Lkro;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->a(Lcom/google/android/libraries/social/socialcast/impl/CastService;Lkro;)Lkro;

    .line 721
    iget-object v0, p0, Lkro;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    invoke-static {v0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->g(Lcom/google/android/libraries/social/socialcast/impl/CastService;)Lihr;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lkro;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    invoke-static {v0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->g(Lcom/google/android/libraries/social/socialcast/impl/CastService;)Lihr;

    move-result-object v0

    invoke-interface {v0}, Lihr;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 743
    :cond_0
    :goto_0
    return-void

    .line 732
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_4

    .line 733
    :cond_2
    const-string v0, "CastService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 734
    const-string v0, "CastService"

    const-string v1, "Error getting auth token."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 736
    :cond_3
    iget-object v0, p0, Lkro;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->stopSelf()V

    goto :goto_0

    .line 740
    :cond_4
    iget-object v0, p0, Lkro;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    invoke-static {v0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->h(Lcom/google/android/libraries/social/socialcast/impl/CastService;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 741
    iget-object v0, p0, Lkro;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    invoke-static {v0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->i(Lcom/google/android/libraries/social/socialcast/impl/CastService;)V

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 703
    invoke-virtual {p0}, Lkro;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 703
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lkro;->a(Ljava/lang/String;)V

    return-void
.end method
