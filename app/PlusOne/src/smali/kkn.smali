.class public final Lkkn;
.super Lhny;
.source "PG"


# instance fields
.field private final a:Lkjc;

.field private final b:Lkkm;


# direct methods
.method public constructor <init>(Lkjc;Lkkm;)V
    .locals 2

    .prologue
    .line 92
    invoke-virtual {p1}, Lkjc;->e()Landroid/content/Context;

    move-result-object v0

    const-string v1, "CreatePostTask"

    invoke-direct {p0, v0, v1}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 93
    iput-object p1, p0, Lkkn;->a:Lkjc;

    .line 94
    iput-object p2, p0, Lkkn;->b:Lkkm;

    .line 95
    return-void
.end method

.method static a(Landroid/content/Context;Lkjc;ZI)Ljava/lang/String;
    .locals 2

    .prologue
    .line 288
    if-eqz p2, :cond_0

    .line 289
    const v0, 0x7f0a02d0

    .line 297
    :goto_0
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 290
    :cond_0
    const/4 v0, 0x1

    if-eq p3, v0, :cond_1

    const/4 v0, 0x2

    if-ne p3, v0, :cond_2

    .line 291
    :cond_1
    const v0, 0x7f0a02d5

    goto :goto_0

    .line 292
    :cond_2
    invoke-virtual {p1}, Lkjc;->v()Lkzz;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lkjc;->v()Lkzz;

    move-result-object v0

    invoke-virtual {v0}, Lkzz;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 293
    const v0, 0x7f0a0456

    goto :goto_0

    .line 295
    :cond_3
    const v0, 0x7f0a02d1

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;Landroid/content/ContentResolver;Lizu;Lnzi;ZLjava/util/Set;Ljava/util/Set;)Lody;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/content/ContentResolver;",
            "Lizu;",
            "Lnzi;",
            "Z",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lody;"
        }
    .end annotation

    .prologue
    .line 369
    new-instance v8, Lody;

    invoke-direct {v8}, Lody;-><init>()V

    .line 374
    sget-object v2, Ljac;->b:Ljac;

    invoke-virtual {p2}, Lizu;->g()Ljac;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljac;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 375
    const/4 v2, 0x2

    .line 384
    :goto_0
    iput v2, v8, Lody;->c:I

    .line 386
    invoke-virtual {p2}, Lizu;->e()Landroid/net/Uri;

    move-result-object v11

    .line 387
    invoke-virtual {p2}, Lizu;->j()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 388
    const/4 v2, 0x0

    .line 402
    :cond_0
    invoke-virtual {p2}, Lizu;->j()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {p2}, Lizu;->c()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, p5

    invoke-interface {v0, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 403
    const-string v2, "CreatePostTask"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 404
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x22

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Duplicate server reference found; "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 406
    :cond_1
    const/4 v2, 0x0

    .line 528
    :goto_1
    return-object v2

    .line 376
    :cond_2
    sget-object v2, Ljac;->c:Ljac;

    invoke-virtual {p2}, Lizu;->g()Ljac;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljac;->equals(Ljava/lang/Object;)Z

    .line 378
    const/4 v2, 0x1

    goto :goto_0

    .line 389
    :cond_3
    if-eqz v11, :cond_4

    .line 390
    invoke-static {p0}, Lhrx;->a(Landroid/content/Context;)Lhrx;

    move-result-object v2

    .line 391
    invoke-virtual {v11}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lhrx;->a(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    .line 392
    if-nez v2, :cond_0

    .line 394
    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x2b

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Could not determine fingerprint for media: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 395
    const/4 v2, 0x0

    goto :goto_1

    .line 398
    :cond_4
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x29

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "No photo ID or local Uri for attachment: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 399
    const/4 v2, 0x0

    goto :goto_1

    .line 410
    :cond_5
    if-eqz v2, :cond_7

    .line 414
    move-object/from16 v0, p6

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 415
    const-string v2, "CreatePostTask"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 416
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x16

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Duplicate CAID found; "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 418
    :cond_6
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 422
    :cond_7
    invoke-virtual {p2}, Lizu;->j()Z

    move-result v3

    if-nez v3, :cond_14

    .line 423
    if-eqz v2, :cond_8

    .line 425
    invoke-static {v2}, Ljbh;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v8, Lody;->f:Ljava/lang/String;

    .line 426
    move-object/from16 v0, p6

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 431
    :cond_8
    if-eqz p4, :cond_e

    .line 432
    invoke-virtual {p2}, Lizu;->e()Landroid/net/Uri;

    move-result-object v3

    .line 433
    invoke-static {p1, v3}, Llsb;->b(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    .line 435
    const-string v4, "image/jpeg"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 436
    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    move-object v10, v3

    .line 448
    :goto_2
    if-eqz v10, :cond_12

    .line 449
    const/4 v9, 0x0

    .line 451
    :try_start_0
    const-class v2, Lizs;

    invoke-static {p0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lizs;

    const/4 v4, 0x0

    const/16 v5, 0x140

    const/16 v6, 0x140

    const/16 v7, 0x100

    move-object v3, p2

    invoke-virtual/range {v2 .. v7}, Lizs;->a(Lizu;IIII)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;
    :try_end_0
    .catch Lkdn; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lkde; {:try_start_0 .. :try_end_0} :catch_1

    move-object v3, v2

    .line 461
    :goto_3
    if-nez v3, :cond_f

    .line 462
    const-string v2, "CreatePostTask"

    invoke-virtual {p2}, Lizu;->e()Landroid/net/Uri;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x1b

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Bitmap decoding failed for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 499
    :goto_4
    new-instance v2, Loed;

    invoke-direct {v2}, Loed;-><init>()V

    .line 501
    invoke-static {p1, v11}, Lhsf;->b(Landroid/content/ContentResolver;Landroid/net/Uri;)J

    move-result-wide v4

    long-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    iput-object v3, v2, Loed;->a:Ljava/lang/Double;

    .line 502
    iput-object v2, v8, Lody;->h:Loed;

    .line 512
    :goto_5
    if-eqz p3, :cond_a

    .line 515
    :try_start_1
    new-instance v2, Lnzi;

    invoke-direct {v2}, Lnzi;-><init>()V

    invoke-static {p3}, Lnzi;->a(Loxu;)[B

    move-result-object v3

    invoke-static {v2, v3}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v2

    check-cast v2, Lnzi;
    :try_end_1
    .catch Loxt; {:try_start_1 .. :try_end_1} :catch_2

    .line 518
    :try_start_2
    iget-object v3, v2, Lnzi;->b:Lpla;

    if-eqz v3, :cond_9

    .line 519
    iget-object v3, v2, Lnzi;->b:Lpla;

    const/4 v4, 0x0

    iput-object v4, v3, Lpla;->b:Ljava/lang/Long;
    :try_end_2
    .catch Loxt; {:try_start_2 .. :try_end_2} :catch_3

    .line 525
    :cond_9
    :goto_6
    iput-object v2, v8, Lody;->g:Lnzi;

    :cond_a
    move-object v2, v8

    .line 528
    goto/16 :goto_1

    .line 437
    :cond_b
    const-string v4, "image/png"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 438
    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    move-object v10, v3

    goto/16 :goto_2

    .line 439
    :cond_c
    invoke-static {v3}, Llsb;->c(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 440
    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    move-object v10, v3

    goto/16 :goto_2

    .line 442
    :cond_d
    const/4 v3, 0x0

    move-object v10, v3

    .line 444
    goto/16 :goto_2

    .line 445
    :cond_e
    const/4 v3, 0x0

    move-object v10, v3

    goto/16 :goto_2

    .line 455
    :catch_0
    move-exception v2

    invoke-virtual {v2}, Lkdn;->printStackTrace()V

    move-object v3, v9

    .line 459
    goto/16 :goto_3

    .line 457
    :catch_1
    move-exception v2

    invoke-virtual {v2}, Lkde;->printStackTrace()V

    move-object v3, v9

    goto/16 :goto_3

    .line 464
    :cond_f
    const/4 v2, 0x0

    .line 467
    :try_start_3
    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    if-ne v10, v4, :cond_10

    .line 469
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-virtual {p2}, Lizu;->e()Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v4

    .line 471
    new-instance v5, Lidp;

    invoke-direct {v5}, Lidp;-><init>()V

    .line 472
    invoke-virtual {v5, v4}, Lidp;->a(Ljava/io/InputStream;)V

    .line 473
    sget v4, Lidp;->a:I

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v4, v6}, Lidp;->b(ILjava/lang/Object;)Z

    .line 474
    sget v4, Lidp;->b:I

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v4, v6}, Lidp;->b(ILjava/lang/Object;)Z

    .line 476
    new-instance v4, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v4}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 477
    invoke-virtual {v5, v3, v4}, Lidp;->a(Landroid/graphics/Bitmap;Ljava/io/OutputStream;)V

    .line 478
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4

    move-result-object v2

    .line 481
    :cond_10
    :goto_7
    if-nez v2, :cond_11

    .line 485
    const/16 v2, 0x55

    const/4 v4, 0x1

    invoke-static {v3, v10, v2, v4}, Llrw;->a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$CompressFormat;IZ)[B

    move-result-object v2

    .line 488
    :cond_11
    const/4 v3, 0x0

    .line 489
    invoke-static {v2, v3}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v2

    .line 490
    iput-object v2, v8, Lody;->d:Ljava/lang/String;

    .line 491
    const/4 v2, 0x1

    iput v2, v8, Lody;->e:I

    goto/16 :goto_4

    .line 493
    :cond_12
    if-nez v2, :cond_13

    .line 495
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 497
    :cond_13
    const/4 v2, 0x2

    iput v2, v8, Lody;->e:I

    goto/16 :goto_4

    .line 504
    :cond_14
    new-instance v2, Lodz;

    invoke-direct {v2}, Lodz;-><init>()V

    .line 506
    invoke-virtual {p2}, Lizu;->b()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lodz;->a:Ljava/lang/String;

    .line 507
    invoke-virtual {p2}, Lizu;->c()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lodz;->b:Ljava/lang/String;

    .line 508
    iput-object v2, v8, Lody;->b:Lodz;

    .line 509
    invoke-virtual {p2}, Lizu;->c()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, p5

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_5

    .line 522
    :catch_2
    move-exception v2

    move-object v3, v2

    move-object v2, p3

    .line 523
    :goto_8
    const-string v4, "CreatePostTask"

    const-string v5, "Failed to copy the editInfo object."

    invoke-static {v4, v5, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_6

    .line 522
    :catch_3
    move-exception v3

    goto :goto_8

    :catch_4
    move-exception v4

    goto :goto_7
.end method

.method private static a(Landroid/content/Context;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;)Loea;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lizu;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lnzi;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lizu;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Loea;"
        }
    .end annotation

    .prologue
    .line 322
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 323
    const/4 v0, 0x0

    .line 363
    :goto_0
    return-object v0

    .line 326
    :cond_0
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 328
    new-instance v5, Ljava/util/HashSet;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v5, v0}, Ljava/util/HashSet;-><init>(I)V

    .line 329
    new-instance v6, Ljava/util/HashSet;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v6, v0}, Ljava/util/HashSet;-><init>(I)V

    .line 330
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 331
    const/4 v2, 0x0

    .line 332
    const/4 v0, 0x0

    move v7, v0

    move v8, v2

    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v7, v0, :cond_2

    .line 333
    invoke-interface {p1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lizu;

    .line 334
    invoke-interface {p2, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lnzi;

    .line 336
    const/4 v0, 0x4

    if-ge v8, v0, :cond_1

    .line 337
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    const/4 v4, 0x4

    if-ge v0, v4, :cond_1

    const/4 v4, 0x1

    :goto_2
    move-object v0, p0

    .line 339
    invoke-static/range {v0 .. v6}, Lkkn;->a(Landroid/content/Context;Landroid/content/ContentResolver;Lizu;Lnzi;ZLjava/util/Set;Ljava/util/Set;)Lody;

    move-result-object v0

    .line 341
    if-eqz v0, :cond_5

    .line 342
    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 343
    invoke-interface {p3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 345
    iget-object v0, v0, Lody;->d:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 346
    add-int/lit8 v8, v8, 0x1

    move v2, v8

    .line 332
    :goto_3
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    move v8, v2

    goto :goto_1

    .line 337
    :cond_1
    const/4 v4, 0x0

    goto :goto_2

    .line 351
    :cond_2
    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 353
    const/4 v0, 0x0

    goto :goto_0

    .line 356
    :cond_3
    new-instance v1, Loea;

    invoke-direct {v1}, Loea;-><init>()V

    .line 358
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lody;

    .line 357
    invoke-interface {v9, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lody;

    iput-object v0, v1, Loea;->b:[Lody;

    .line 359
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 360
    iput-object p4, v1, Loea;->d:Ljava/lang/String;

    :cond_4
    move-object v0, v1

    .line 363
    goto :goto_0

    :cond_5
    move v2, v8

    goto :goto_3
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 12

    .prologue
    const/4 v2, 0x0

    const/4 v11, 0x1

    const/4 v1, 0x0

    .line 99
    invoke-virtual {p0}, Lkkn;->f()Landroid/content/Context;

    move-result-object v4

    .line 100
    new-instance v5, Lodg;

    invoke-direct {v5}, Lodg;-><init>()V

    .line 102
    new-instance v0, Lpee;

    invoke-direct {v0}, Lpee;-><init>()V

    iput-object v0, v5, Lodg;->b:Lpee;

    .line 103
    iget-object v0, v5, Lodg;->b:Lpee;

    new-instance v3, Lpef;

    invoke-direct {v3}, Lpef;-><init>()V

    iput-object v3, v0, Lpee;->a:Lpef;

    .line 104
    iget-object v0, v5, Lodg;->b:Lpee;

    iget-object v0, v0, Lpee;->a:Lpef;

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, v0, Lpef;->a:Ljava/lang/Boolean;

    .line 105
    iget-object v0, v5, Lodg;->b:Lpee;

    new-instance v3, Lpeg;

    invoke-direct {v3}, Lpeg;-><init>()V

    iput-object v3, v0, Lpee;->b:Lpeg;

    .line 107
    iget-object v0, v5, Lodg;->b:Lpee;

    iget-object v0, v0, Lpee;->b:Lpeg;

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, v0, Lpeg;->a:Ljava/lang/Boolean;

    .line 109
    iget-object v0, p0, Lkkn;->a:Lkjc;

    invoke-virtual {v0}, Lkjc;->h()Lkey;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lkkn;->a:Lkjc;

    invoke-virtual {v0}, Lkjc;->h()Lkey;

    move-result-object v0

    invoke-virtual {v0}, Lkey;->e()Lkey;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 110
    iget-object v0, p0, Lkkn;->a:Lkjc;

    invoke-virtual {v0}, Lkjc;->h()Lkey;

    move-result-object v0

    invoke-virtual {v0}, Lkey;->e()Lkey;

    move-result-object v0

    invoke-virtual {v0}, Lkey;->c()Ljava/lang/String;

    move-result-object v3

    .line 111
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 112
    const-string v0, "Mobile"

    .line 113
    const-string v6, "com.google.android.apps.social"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 115
    :try_start_0
    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    .line 116
    const/4 v7, 0x0

    .line 117
    invoke-virtual {v6, v3, v7}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    .line 116
    invoke-virtual {v6, v3}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v3

    .line 118
    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 123
    :cond_0
    :goto_0
    new-instance v3, Lodh;

    invoke-direct {v3}, Lodh;-><init>()V

    .line 124
    iput-object v0, v3, Lodh;->a:Ljava/lang/String;

    .line 126
    iput-object v3, v5, Lodg;->p:Lodh;

    .line 130
    :cond_1
    iget-object v0, p0, Lkkn;->a:Lkjc;

    .line 131
    invoke-virtual {v0}, Lkjc;->k()Lhgw;

    move-result-object v0

    iget-object v3, p0, Lkkn;->a:Lkjc;

    invoke-virtual {v3}, Lkjc;->l()Lhgw;

    move-result-object v3

    .line 130
    invoke-static {v0, v3}, Lhft;->a(Lhgw;Lhgw;)Lock;

    move-result-object v0

    iput-object v0, v5, Lodg;->j:Lock;

    .line 132
    iget-object v0, p0, Lkkn;->a:Lkjc;

    invoke-virtual {v0}, Lkjc;->s()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v5, Lodg;->k:Ljava/lang/Boolean;

    .line 134
    iget-object v0, p0, Lkkn;->a:Lkjc;

    invoke-virtual {v0}, Lkjc;->m()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v5, Lodg;->c:Ljava/lang/String;

    .line 136
    iget-object v0, p0, Lkkn;->a:Lkjc;

    invoke-virtual {v0}, Lkjc;->o()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v5, Lodg;->a:Ljava/lang/String;

    .line 138
    iget-object v0, p0, Lkkn;->a:Lkjc;

    invoke-virtual {v0}, Lkjc;->k()Lhgw;

    move-result-object v0

    invoke-virtual {v0}, Lhgw;->i()I

    move-result v0

    if-lez v0, :cond_3

    .line 139
    iget-object v0, p0, Lkkn;->a:Lkjc;

    invoke-virtual {v0}, Lkjc;->k()Lhgw;

    move-result-object v0

    invoke-virtual {v0}, Lhgw;->c()[Lkxr;

    move-result-object v3

    .line 140
    array-length v0, v3

    new-array v6, v0, [Lodl;

    move v0, v1

    .line 141
    :goto_1
    array-length v7, v3

    if-ge v0, v7, :cond_2

    .line 142
    new-instance v7, Lodl;

    invoke-direct {v7}, Lodl;-><init>()V

    .line 143
    aget-object v8, v3, v0

    invoke-virtual {v8}, Lkxr;->a()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lodl;->b:Ljava/lang/String;

    .line 144
    aget-object v8, v3, v0

    invoke-virtual {v8}, Lkxr;->c()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lodl;->c:Ljava/lang/String;

    .line 145
    aput-object v7, v6, v0

    .line 141
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 147
    :cond_2
    iput-object v6, v5, Lodg;->m:[Lodl;

    .line 150
    :cond_3
    iget-object v0, p0, Lkkn;->a:Lkjc;

    invoke-virtual {v0}, Lkjc;->r()Llae;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 151
    iget-object v0, p0, Lkkn;->a:Lkjc;

    invoke-virtual {v0}, Lkjc;->r()Llae;

    move-result-object v0

    invoke-virtual {v0}, Llae;->j()Lofq;

    move-result-object v0

    iput-object v0, v5, Lodg;->i:Lofq;

    .line 154
    :cond_4
    iget-object v0, p0, Lkkn;->a:Lkjc;

    invoke-virtual {v0}, Lkjc;->t()Lkzm;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 155
    new-instance v0, Lohc;

    invoke-direct {v0}, Lohc;-><init>()V

    iput-object v0, v5, Lodg;->o:Lohc;

    .line 156
    iget-object v0, v5, Lodg;->o:Lohc;

    const/16 v3, 0x16

    iput v3, v0, Lohc;->a:I

    .line 157
    iget-object v0, v5, Lodg;->o:Lohc;

    new-instance v3, Lohb;

    invoke-direct {v3}, Lohb;-><init>()V

    iput-object v3, v0, Lohc;->b:Lohb;

    .line 158
    iget-object v0, v5, Lodg;->o:Lohc;

    iget-object v0, v0, Lohc;->b:Lohb;

    new-instance v3, Loal;

    invoke-direct {v3}, Loal;-><init>()V

    iput-object v3, v0, Lohb;->a:Loal;

    .line 160
    new-instance v0, Lodo;

    invoke-direct {v0}, Lodo;-><init>()V

    .line 161
    iget-object v3, p0, Lkkn;->a:Lkjc;

    invoke-virtual {v3}, Lkjc;->t()Lkzm;

    move-result-object v3

    invoke-virtual {v3}, Lkzm;->a()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lodo;->c:Ljava/lang/String;

    .line 162
    iget-object v3, p0, Lkkn;->a:Lkjc;

    invoke-virtual {v3}, Lkjc;->t()Lkzm;

    move-result-object v3

    invoke-virtual {v3}, Lkzm;->b()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lodo;->b:Ljava/lang/String;

    .line 164
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, v0, Lodo;->d:Ljava/lang/Boolean;

    .line 165
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, v0, Lodo;->e:Ljava/lang/Boolean;

    .line 167
    iget-object v3, v5, Lodg;->o:Lohc;

    iget-object v3, v3, Lohc;->b:Lohb;

    iget-object v3, v3, Lohb;->a:Loal;

    iput-object v0, v3, Loal;->a:Lodo;

    .line 168
    iget-object v0, v5, Lodg;->o:Lohc;

    iget-object v0, v0, Lohc;->b:Lohb;

    iget-object v0, v0, Lohb;->a:Loal;

    iget-object v3, p0, Lkkn;->a:Lkjc;

    .line 169
    invoke-virtual {v3}, Lkjc;->t()Lkzm;

    move-result-object v3

    invoke-virtual {v3}, Lkzm;->c()Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v0, Loal;->b:Ljava/lang/Integer;

    .line 172
    :cond_5
    iget-object v0, p0, Lkkn;->a:Lkjc;

    invoke-virtual {v0}, Lkjc;->i()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lkkn;->a:Lkjc;

    invoke-virtual {v0}, Lkjc;->j()Loya;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 173
    iget-object v0, p0, Lkkn;->a:Lkjc;

    invoke-virtual {v0}, Lkjc;->i()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v5, Lodg;->h:Ljava/lang/String;

    .line 174
    iget-object v0, p0, Lkkn;->a:Lkjc;

    invoke-virtual {v0}, Lkjc;->j()Loya;

    move-result-object v0

    iput-object v0, v5, Lodg;->q:Loya;

    .line 179
    :cond_6
    :goto_2
    iget-object v0, p0, Lkkn;->a:Lkjc;

    invoke-virtual {v0}, Lkjc;->c()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v5, Lodg;->e:Ljava/lang/Boolean;

    .line 180
    iget-object v0, p0, Lkkn;->a:Lkjc;

    invoke-virtual {v0}, Lkjc;->b()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v5, Lodg;->f:Ljava/lang/Boolean;

    .line 182
    iget-object v0, p0, Lkkn;->a:Lkjc;

    invoke-virtual {v0}, Lkjc;->d()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 183
    iget-object v0, p0, Lkkn;->a:Lkjc;

    invoke-virtual {v0}, Lkjc;->k()Lhgw;

    move-result-object v0

    iget-object v3, v5, Lodg;->l:[Lofh;

    array-length v3, v3

    if-lez v3, :cond_8

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "EmailDeliveryIndicators.length > 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 175
    :cond_7
    iget-object v0, p0, Lkkn;->a:Lkjc;

    invoke-virtual {v0}, Lkjc;->u()Loya;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 176
    iget-object v0, p0, Lkkn;->a:Lkjc;

    invoke-virtual {v0}, Lkjc;->u()Loya;

    move-result-object v0

    iput-object v0, v5, Lodg;->q:Loya;

    goto :goto_2

    .line 183
    :cond_8
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0}, Lhgw;->b()[Lhxc;

    move-result-object v7

    array-length v8, v7

    move v3, v1

    :goto_3
    if-ge v3, v8, :cond_b

    aget-object v9, v7, v3

    invoke-virtual {v9}, Lhxc;->a()Ljava/lang/String;

    move-result-object v0

    const-string v10, "f."

    invoke-virtual {v0, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_a

    const/4 v10, 0x2

    invoke-virtual {v0, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    :goto_4
    invoke-virtual {v9}, Lhxc;->c()I

    move-result v9

    if-ne v9, v11, :cond_9

    if-eqz v0, :cond_9

    new-instance v9, Lofh;

    invoke-direct {v9}, Lofh;-><init>()V

    iput-object v0, v9, Lofh;->b:Ljava/lang/String;

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_9
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    :cond_a
    move-object v0, v2

    goto :goto_4

    :cond_b
    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_c

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lofh;

    iput-object v0, v5, Lodg;->l:[Lofh;

    iget-object v0, v5, Lodg;->l:[Lofh;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 186
    :cond_c
    iget-object v0, p0, Lkkn;->a:Lkjc;

    invoke-virtual {v0}, Lkjc;->v()Lkzz;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 187
    new-instance v0, Lodi;

    invoke-direct {v0}, Lodi;-><init>()V

    iput-object v0, v5, Lodg;->s:Lodi;

    .line 188
    iget-object v0, p0, Lkkn;->a:Lkjc;

    invoke-virtual {v0}, Lkjc;->v()Lkzz;

    move-result-object v0

    invoke-virtual {v0}, Lkzz;->d()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 189
    iget-object v0, v5, Lodg;->s:Lodi;

    new-instance v3, Loar;

    invoke-direct {v3}, Loar;-><init>()V

    iput-object v3, v0, Lodi;->a:Loar;

    .line 191
    iget-object v0, v5, Lodg;->s:Lodi;

    iget-object v0, v0, Lodi;->a:Loar;

    iget-object v3, p0, Lkkn;->a:Lkjc;

    .line 192
    invoke-virtual {v3}, Lkjc;->v()Lkzz;

    move-result-object v3

    invoke-virtual {v3}, Lkzz;->a()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Loar;->a:Ljava/lang/String;

    .line 193
    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v5, Lodg;->f:Ljava/lang/Boolean;

    .line 201
    :cond_d
    :goto_5
    iget-object v0, p0, Lkkn;->a:Lkjc;

    invoke-virtual {v0}, Lkjc;->q()Ljava/util/ArrayList;

    move-result-object v0

    .line 204
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 206
    if-eqz v0, :cond_14

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_14

    .line 207
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 208
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 210
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_6
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizr;

    .line 211
    invoke-interface {v0}, Lizr;->f()Lizu;

    move-result-object v9

    .line 212
    instance-of v10, v0, Ljuf;

    if-eqz v10, :cond_f

    check-cast v0, Ljuf;

    .line 213
    invoke-interface {v0}, Ljuf;->h()Lnzi;

    move-result-object v0

    .line 215
    :goto_7
    invoke-interface {v6, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 216
    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 195
    :cond_e
    iget-object v0, v5, Lodg;->s:Lodi;

    new-instance v3, Loaq;

    invoke-direct {v3}, Loaq;-><init>()V

    iput-object v3, v0, Lodi;->b:Loaq;

    .line 196
    iget-object v0, v5, Lodg;->s:Lodi;

    iget-object v0, v0, Lodi;->b:Loaq;

    iget-object v3, p0, Lkkn;->a:Lkjc;

    .line 197
    invoke-virtual {v3}, Lkjc;->v()Lkzz;

    move-result-object v3

    invoke-virtual {v3}, Lkzz;->a()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Loaq;->a:Ljava/lang/String;

    goto :goto_5

    :cond_f
    move-object v0, v2

    .line 213
    goto :goto_7

    .line 219
    :cond_10
    iget-object v0, p0, Lkkn;->a:Lkjc;

    invoke-virtual {v0}, Lkjc;->f()I

    move-result v0

    invoke-static {v4, v0, v6}, Ljvj;->b(Landroid/content/Context;ILjava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 221
    :try_start_1
    iget-object v2, p0, Lkkn;->a:Lkjc;

    .line 222
    invoke-virtual {v2}, Lkjc;->w()Ljava/lang/String;

    move-result-object v2

    .line 221
    invoke-static {v4, v0, v7, v3, v2}, Lkkn;->a(Landroid/content/Context;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;)Loea;
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v2

    .line 235
    :cond_11
    :goto_8
    if-eqz v2, :cond_12

    .line 236
    iput-object v2, v5, Lodg;->g:Loea;

    .line 239
    :cond_12
    const-class v0, Lkjb;

    .line 240
    invoke-static {v4, v0}, Llnh;->c(Landroid/content/Context;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v2

    .line 242
    :goto_9
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_16

    .line 243
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkjb;

    iget-object v6, p0, Lkkn;->a:Lkjc;

    invoke-interface {v0, v5}, Lkjb;->a(Lodg;)V

    .line 242
    add-int/lit8 v1, v1, 0x1

    goto :goto_9

    .line 223
    :catch_0
    move-exception v0

    move-object v2, v0

    .line 224
    iget-object v0, p0, Lkkn;->a:Lkjc;

    invoke-virtual {v0}, Lkjc;->g()Lhmr;

    move-result-object v0

    if-eqz v0, :cond_13

    iget-object v0, p0, Lkkn;->a:Lkjc;

    invoke-virtual {v0}, Lkjc;->f()I

    move-result v0

    const/4 v3, -0x1

    if-eq v0, v3, :cond_13

    .line 225
    iget-object v0, p0, Lkkn;->a:Lkjc;

    invoke-virtual {v0}, Lkjc;->g()Lhmr;

    move-result-object v0

    sget-object v3, Lhmv;->dG:Lhmv;

    invoke-virtual {v0, v3}, Lhmr;->a(Lhmv;)Lhmr;

    .line 226
    const-class v0, Lhms;

    invoke-static {v4, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    iget-object v3, p0, Lkkn;->a:Lkjc;

    invoke-virtual {v3}, Lkjc;->g()Lhmr;

    move-result-object v3

    invoke-interface {v0, v3}, Lhms;->a(Lhmr;)V

    .line 228
    :cond_13
    new-instance v0, Lhoz;

    iget-object v3, p0, Lkkn;->a:Lkjc;

    .line 229
    invoke-static {v4, v3, v11, v1}, Lkkn;->a(Landroid/content/Context;Lkjc;ZI)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    .line 251
    :goto_a
    return-object v0

    .line 231
    :cond_14
    iget-object v0, p0, Lkkn;->a:Lkjc;

    invoke-virtual {v0}, Lkjc;->x()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_15

    iget-object v0, p0, Lkkn;->a:Lkjc;

    invoke-virtual {v0}, Lkjc;->y()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 232
    :cond_15
    iget-object v0, p0, Lkkn;->a:Lkjc;

    invoke-virtual {v0}, Lkjc;->x()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lkkn;->a:Lkjc;

    invoke-virtual {v2}, Lkjc;->y()Ljava/lang/String;

    move-result-object v2

    new-instance v6, Loeb;

    invoke-direct {v6}, Loeb;-><init>()V

    iput-object v0, v6, Loeb;->b:Ljava/lang/String;

    iput-object v2, v6, Loeb;->a:Ljava/lang/String;

    new-instance v2, Loea;

    invoke-direct {v2}, Loea;-><init>()V

    iput-object v6, v2, Loea;->a:Loeb;

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v2, Loea;->c:Ljava/lang/Boolean;

    goto/16 :goto_8

    .line 246
    :cond_16
    iget-object v0, p0, Lkkn;->a:Lkjc;

    invoke-virtual {v0}, Lkjc;->z()Lkji;

    move-result-object v0

    .line 247
    if-eqz v0, :cond_17

    .line 248
    iget-object v1, p0, Lkkn;->a:Lkjc;

    invoke-interface {v0, v5}, Lkji;->a(Lodg;)V

    .line 251
    :cond_17
    iget-object v0, p0, Lkkn;->b:Lkkm;

    iget-object v1, p0, Lkkn;->a:Lkjc;

    invoke-interface {v0, v4, v5, v3, v1}, Lkkm;->a(Landroid/content/Context;Lodg;Ljava/util/List;Lkjc;)Lhoz;

    move-result-object v0

    goto :goto_a

    :catch_1
    move-exception v3

    goto/16 :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 533
    invoke-virtual {p0}, Lkkn;->f()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a058d

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
