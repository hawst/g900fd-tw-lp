.class public final Ldoy;
.super Lhny;
.source "PG"


# instance fields
.field private final a:I

.field private final b:Ljava/lang/String;

.field private final c:I

.field private final d:Z

.field private final e:[Lnhm;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;IZ)V
    .locals 7

    .prologue
    .line 55
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, Ldoy;-><init>(Landroid/content/Context;ILjava/lang/String;IZ[Lnhm;)V

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;IZ[Lnhm;)V
    .locals 1

    .prologue
    .line 61
    const-string v0, "GetFriendLocationsTask"

    invoke-direct {p0, p1, v0}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 64
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Ldoy;->a(I)Lhny;

    .line 66
    iput p2, p0, Ldoy;->a:I

    .line 67
    iput-object p3, p0, Ldoy;->b:Ljava/lang/String;

    .line 68
    iput p4, p0, Ldoy;->c:I

    .line 69
    iput-boolean p5, p0, Ldoy;->d:Z

    .line 70
    iput-object p6, p0, Ldoy;->e:[Lnhm;

    .line 71
    return-void
.end method

.method public static a(Lhoz;)[Lnhm;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 74
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 81
    :goto_0
    return-object v0

    .line 78
    :cond_1
    invoke-virtual {p0}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v0

    .line 79
    const-string v2, "user_device_locations"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lhyv;

    .line 80
    if-nez v0, :cond_2

    move-object v0, v1

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    new-array v1, v1, [Lnhm;

    .line 81
    invoke-virtual {v0, v1}, Lhyv;->a([Loxu;)[Loxu;

    move-result-object v0

    check-cast v0, [Lnhm;

    goto :goto_0
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 20

    .prologue
    .line 118
    invoke-virtual/range {p0 .. p0}, Ldoy;->f()Landroid/content/Context;

    move-result-object v3

    .line 119
    move-object/from16 v0, p0

    iget v4, v0, Ldoy;->a:I

    .line 120
    new-instance v5, Litx;

    move-object/from16 v0, p0

    iget v2, v0, Ldoy;->c:I

    move-object/from16 v0, p0

    iget-boolean v6, v0, Ldoy;->d:Z

    invoke-direct {v5, v3, v4, v2, v6}, Litx;-><init>(Landroid/content/Context;IIZ)V

    .line 122
    new-instance v6, Lity;

    invoke-direct {v6, v3, v4}, Lity;-><init>(Landroid/content/Context;I)V

    .line 125
    new-instance v7, Lkfo;

    invoke-direct {v7, v3, v4}, Lkfo;-><init>(Landroid/content/Context;I)V

    .line 126
    invoke-static {v3, v7}, Lkgi;->a(Landroid/content/Context;Lkfo;)Lkfu;

    move-result-object v8

    .line 127
    invoke-virtual {v8, v5}, Lkfu;->a(Lkff;)V

    .line 128
    invoke-virtual {v8, v6}, Lkfu;->a(Lkff;)V

    .line 130
    const/4 v2, 0x0

    .line 131
    move-object/from16 v0, p0

    iget-object v9, v0, Ldoy;->b:Ljava/lang/String;

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 132
    new-instance v2, Lkbv;

    move-object/from16 v0, p0

    iget-object v9, v0, Ldoy;->b:Ljava/lang/String;

    invoke-direct {v2, v3, v7, v4, v9}, Lkbv;-><init>(Landroid/content/Context;Lkfo;ILjava/lang/String;)V

    .line 133
    invoke-virtual {v8, v2}, Lkfu;->a(Lkff;)V

    .line 136
    :cond_0
    invoke-virtual {v8}, Lkfu;->l()V

    .line 138
    invoke-virtual {v5}, Litx;->i()[Lnhm;

    move-result-object v4

    .line 141
    invoke-virtual {v6}, Lity;->i()Lnhl;

    move-result-object v6

    .line 142
    invoke-virtual {v5}, Litx;->j()I

    move-result v5

    .line 144
    new-instance v7, Lhoz;

    iget v9, v8, Lkff;->i:I

    .line 145
    iget-object v10, v8, Lkff;->k:Ljava/lang/Exception;

    invoke-virtual {v8}, Lkfu;->t()Z

    move-result v3

    if-eqz v3, :cond_4

    move-object/from16 v0, p0

    iget v3, v0, Ldoy;->c:I

    const/4 v8, 0x1

    if-gt v3, v8, :cond_4

    const/4 v3, 0x1

    :goto_0
    if-eqz v3, :cond_5

    invoke-virtual/range {p0 .. p0}, Ldoy;->f()Landroid/content/Context;

    move-result-object v3

    const v8, 0x7f0a0592

    invoke-virtual {v3, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    :goto_1
    invoke-direct {v7, v9, v10, v3}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    .line 146
    invoke-virtual {v7}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v8

    .line 148
    if-eqz v2, :cond_2

    .line 149
    invoke-virtual {v2}, Lkbv;->i()Lnjt;

    move-result-object v9

    .line 150
    if-eqz v4, :cond_1

    if-eqz v9, :cond_1

    iget-object v2, v9, Lnjt;->d:Lnib;

    if-eqz v2, :cond_1

    iget-object v2, v9, Lnjt;->e:Lnkd;

    if-eqz v2, :cond_1

    iget-object v2, v9, Lnjt;->e:Lnkd;

    iget-object v2, v2, Lnkd;->m:Lnik;

    if-eqz v2, :cond_1

    iget-object v2, v9, Lnjt;->e:Lnkd;

    iget-object v2, v2, Lnkd;->m:Lnik;

    iget-object v2, v2, Lnik;->a:[Lnij;

    if-eqz v2, :cond_1

    iget-object v2, v9, Lnjt;->e:Lnkd;

    iget-object v2, v2, Lnkd;->m:Lnik;

    iget-object v2, v2, Lnik;->a:[Lnij;

    array-length v2, v2

    if-nez v2, :cond_6

    :cond_1
    move-object v2, v4

    .line 151
    :goto_2
    const-string v3, "target_profile"

    new-instance v4, Lhyt;

    invoke-direct {v4, v9}, Lhyt;-><init>(Loxu;)V

    invoke-virtual {v8, v3, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    move-object v4, v2

    .line 154
    :cond_2
    const-string v2, "user_device_locations"

    new-instance v3, Lhyv;

    invoke-direct {v3, v4}, Lhyv;-><init>([Loxu;)V

    invoke-virtual {v8, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 156
    const-string v2, "owner_device_location"

    new-instance v3, Lhyt;

    invoke-direct {v3, v6}, Lhyt;-><init>(Loxu;)V

    invoke-virtual {v8, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 158
    const-string v2, "delay_interval"

    invoke-virtual {v8, v2, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 160
    if-eqz v4, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Ldoy;->e:[Lnhm;

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget v2, v0, Ldoy;->c:I

    const/4 v3, 0x1

    if-gt v2, v3, :cond_a

    .line 162
    :cond_3
    return-object v7

    .line 145
    :cond_4
    const/4 v3, 0x0

    goto :goto_0

    :cond_5
    const/4 v3, 0x0

    goto :goto_1

    .line 150
    :cond_6
    iget-object v3, v9, Lnjt;->a:Ljava/lang/String;

    const/4 v2, 0x0

    :goto_3
    array-length v10, v4

    if-ge v2, v10, :cond_8

    aget-object v10, v4, v2

    iget-object v10, v10, Lnhm;->b:Ljava/lang/String;

    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    move-object v2, v4

    goto :goto_2

    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_8
    new-instance v2, Lnhm;

    invoke-direct {v2}, Lnhm;-><init>()V

    iput-object v3, v2, Lnhm;->b:Ljava/lang/String;

    iget-object v3, v9, Lnjt;->g:Ljava/lang/String;

    iput-object v3, v2, Lnhm;->d:Ljava/lang/String;

    iget-object v3, v9, Lnjt;->d:Lnib;

    iget-object v3, v3, Lnib;->f:Ljava/lang/String;

    iput-object v3, v2, Lnhm;->e:Ljava/lang/String;

    iget-object v3, v9, Lnjt;->e:Lnkd;

    iget-object v3, v3, Lnkd;->m:Lnik;

    iget-object v3, v3, Lnik;->a:[Lnij;

    iput-object v3, v2, Lnhm;->c:[Lnij;

    array-length v3, v4

    add-int/lit8 v3, v3, 0x1

    new-array v3, v3, [Lnhm;

    const/4 v10, 0x0

    aput-object v2, v3, v10

    const/4 v2, 0x0

    :goto_4
    array-length v10, v4

    if-ge v2, v10, :cond_9

    add-int/lit8 v10, v2, 0x1

    aget-object v11, v4, v2

    aput-object v11, v3, v10

    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_9
    move-object v2, v3

    goto :goto_2

    .line 160
    :cond_a
    invoke-virtual/range {p0 .. p0}, Ldoy;->f()Landroid/content/Context;

    move-result-object v5

    const-class v2, Lhei;

    invoke-static {v5, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lhei;

    move-object/from16 v0, p0

    iget v3, v0, Ldoy;->a:I

    invoke-interface {v2, v3}, Lhei;->a(I)Lhej;

    move-result-object v2

    const-string v3, "account_name"

    invoke-interface {v2, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    array-length v3, v4

    const/4 v2, 0x0

    :goto_5
    if-ge v2, v3, :cond_b

    aget-object v9, v4, v2

    iget-object v10, v9, Lnhm;->b:Ljava/lang/String;

    invoke-virtual {v8, v10, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_b
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    move-object/from16 v0, p0

    iget-object v4, v0, Ldoy;->e:[Lnhm;

    array-length v9, v4

    const/4 v2, 0x0

    move v3, v2

    :goto_6
    if-ge v3, v9, :cond_11

    aget-object v12, v4, v3

    iget-object v2, v12, Lnhm;->b:Ljava/lang/String;

    invoke-virtual {v8, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    const/4 v2, 0x4

    iget-object v12, v12, Lnhm;->b:Ljava/lang/String;

    invoke-virtual {v8, v12}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_7
    new-instance v12, Lkoj;

    move-object/from16 v0, p0

    iget v13, v0, Ldoy;->c:I

    invoke-direct {v12, v6, v2, v13}, Lkoj;-><init>(Ljava/lang/String;II)V

    invoke-virtual {v12, v5}, Lkoj;->a(Landroid/content/Context;)V

    :cond_c
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_6

    :cond_d
    iget-object v2, v12, Lnhm;->b:Ljava/lang/String;

    invoke-virtual {v8, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lnhm;

    iget-object v13, v12, Lnhm;->b:Ljava/lang/String;

    invoke-virtual {v8, v13}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v13, v12, Lnhm;->c:[Lnij;

    if-eqz v13, :cond_c

    iget-object v13, v12, Lnhm;->c:[Lnij;

    array-length v13, v13

    if-eqz v13, :cond_c

    iget-object v13, v2, Lnhm;->c:[Lnij;

    if-eqz v13, :cond_c

    iget-object v13, v2, Lnhm;->c:[Lnij;

    array-length v13, v13

    if-eqz v13, :cond_c

    iget-object v12, v12, Lnhm;->c:[Lnij;

    invoke-static {v12}, Liuo;->a([Lnij;)Lnij;

    move-result-object v12

    iget-object v2, v2, Lnhm;->c:[Lnij;

    invoke-static {v2}, Liuo;->a([Lnij;)Lnij;

    move-result-object v2

    iget v13, v12, Lnij;->b:I

    const/4 v14, 0x2

    if-eq v13, v14, :cond_c

    iget v13, v2, Lnij;->b:I

    const/4 v14, 0x2

    if-eq v13, v14, :cond_c

    iget-object v12, v12, Lnij;->e:Ljava/lang/Long;

    invoke-virtual {v12}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    iget-object v2, v2, Lnij;->e:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    sub-long v16, v10, v12

    const-wide/32 v18, 0x1d4c0

    cmp-long v2, v16, v18

    if-gez v2, :cond_e

    const/4 v2, 0x3

    goto :goto_7

    :cond_e
    sub-long v16, v10, v14

    const-wide/32 v18, 0x1d4c0

    cmp-long v2, v16, v18

    if-gez v2, :cond_f

    const/4 v2, 0x0

    goto :goto_7

    :cond_f
    cmp-long v2, v14, v12

    if-lez v2, :cond_10

    const/4 v2, 0x1

    goto :goto_7

    :cond_10
    const/4 v2, 0x2

    goto :goto_7

    :cond_11
    invoke-virtual {v8}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_12
    :goto_8
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lnhm;

    iget-object v4, v2, Lnhm;->c:[Lnij;

    if-eqz v4, :cond_12

    iget-object v4, v2, Lnhm;->c:[Lnij;

    array-length v4, v4

    if-eqz v4, :cond_12

    iget-object v2, v2, Lnhm;->c:[Lnij;

    invoke-static {v2}, Liuo;->b([Lnij;)Z

    move-result v2

    if-nez v2, :cond_12

    new-instance v2, Lkoj;

    const/4 v4, 0x5

    move-object/from16 v0, p0

    iget v8, v0, Ldoy;->c:I

    invoke-direct {v2, v6, v4, v8}, Lkoj;-><init>(Ljava/lang/String;II)V

    invoke-virtual {v2, v5}, Lkoj;->a(Landroid/content/Context;)V

    goto :goto_8
.end method
