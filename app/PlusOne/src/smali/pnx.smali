.class public final Lpnx;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/Long;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 184
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 230
    const/4 v0, 0x0

    .line 231
    iget-object v1, p0, Lpnx;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 232
    const/4 v0, 0x1

    iget-object v1, p0, Lpnx;->a:Ljava/lang/String;

    .line 233
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 235
    :cond_0
    iget-object v1, p0, Lpnx;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 236
    const/4 v1, 0x2

    iget-object v2, p0, Lpnx;->b:Ljava/lang/String;

    .line 237
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 239
    :cond_1
    iget-object v1, p0, Lpnx;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 240
    const/4 v1, 0x3

    iget-object v2, p0, Lpnx;->c:Ljava/lang/String;

    .line 241
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 243
    :cond_2
    iget-object v1, p0, Lpnx;->e:Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 244
    const/4 v1, 0x4

    iget-object v2, p0, Lpnx;->e:Ljava/lang/Long;

    .line 245
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 247
    :cond_3
    iget-object v1, p0, Lpnx;->d:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 248
    const/4 v1, 0x5

    iget-object v2, p0, Lpnx;->d:Ljava/lang/String;

    .line 249
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 251
    :cond_4
    iget-object v1, p0, Lpnx;->f:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 252
    const/4 v1, 0x6

    iget-object v2, p0, Lpnx;->f:Ljava/lang/String;

    .line 253
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 255
    :cond_5
    iget-object v1, p0, Lpnx;->g:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 256
    const/4 v1, 0x7

    iget-object v2, p0, Lpnx;->g:Ljava/lang/String;

    .line 257
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 259
    :cond_6
    iget-object v1, p0, Lpnx;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 260
    iput v0, p0, Lpnx;->ai:I

    .line 261
    return v0
.end method

.method public a(Loxn;)Lpnx;
    .locals 2

    .prologue
    .line 269
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 270
    sparse-switch v0, :sswitch_data_0

    .line 274
    iget-object v1, p0, Lpnx;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 275
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpnx;->ah:Ljava/util/List;

    .line 278
    :cond_1
    iget-object v1, p0, Lpnx;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 280
    :sswitch_0
    return-object p0

    .line 285
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpnx;->a:Ljava/lang/String;

    goto :goto_0

    .line 289
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpnx;->b:Ljava/lang/String;

    goto :goto_0

    .line 293
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpnx;->c:Ljava/lang/String;

    goto :goto_0

    .line 297
    :sswitch_4
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lpnx;->e:Ljava/lang/Long;

    goto :goto_0

    .line 301
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpnx;->d:Ljava/lang/String;

    goto :goto_0

    .line 305
    :sswitch_6
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpnx;->f:Ljava/lang/String;

    goto :goto_0

    .line 309
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpnx;->g:Ljava/lang/String;

    goto :goto_0

    .line 270
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 203
    iget-object v0, p0, Lpnx;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 204
    const/4 v0, 0x1

    iget-object v1, p0, Lpnx;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 206
    :cond_0
    iget-object v0, p0, Lpnx;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 207
    const/4 v0, 0x2

    iget-object v1, p0, Lpnx;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 209
    :cond_1
    iget-object v0, p0, Lpnx;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 210
    const/4 v0, 0x3

    iget-object v1, p0, Lpnx;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 212
    :cond_2
    iget-object v0, p0, Lpnx;->e:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 213
    const/4 v0, 0x4

    iget-object v1, p0, Lpnx;->e:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 215
    :cond_3
    iget-object v0, p0, Lpnx;->d:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 216
    const/4 v0, 0x5

    iget-object v1, p0, Lpnx;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 218
    :cond_4
    iget-object v0, p0, Lpnx;->f:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 219
    const/4 v0, 0x6

    iget-object v1, p0, Lpnx;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 221
    :cond_5
    iget-object v0, p0, Lpnx;->g:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 222
    const/4 v0, 0x7

    iget-object v1, p0, Lpnx;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 224
    :cond_6
    iget-object v0, p0, Lpnx;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 226
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 180
    invoke-virtual {p0, p1}, Lpnx;->a(Loxn;)Lpnx;

    move-result-object v0

    return-object v0
.end method
