.class public final Lncp;
.super Loxq;
.source "PG"


# instance fields
.field private a:Loxe;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 682
    invoke-direct {p0}, Loxq;-><init>()V

    .line 685
    const/4 v0, 0x0

    iput-object v0, p0, Lncp;->a:Loxe;

    .line 682
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 709
    const/4 v0, 0x0

    .line 710
    iget-object v1, p0, Lncp;->a:Loxe;

    if-eqz v1, :cond_0

    .line 711
    const/4 v0, 0x1

    iget-object v1, p0, Lncp;->a:Loxe;

    .line 712
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 714
    :cond_0
    iget-object v1, p0, Lncp;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 715
    const/4 v1, 0x2

    iget-object v2, p0, Lncp;->b:Ljava/lang/String;

    .line 716
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 718
    :cond_1
    iget-object v1, p0, Lncp;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 719
    const/4 v1, 0x3

    iget-object v2, p0, Lncp;->c:Ljava/lang/Boolean;

    .line 720
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 722
    :cond_2
    iget-object v1, p0, Lncp;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 723
    iput v0, p0, Lncp;->ai:I

    .line 724
    return v0
.end method

.method public a(Loxn;)Lncp;
    .locals 2

    .prologue
    .line 732
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 733
    sparse-switch v0, :sswitch_data_0

    .line 737
    iget-object v1, p0, Lncp;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 738
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lncp;->ah:Ljava/util/List;

    .line 741
    :cond_1
    iget-object v1, p0, Lncp;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 743
    :sswitch_0
    return-object p0

    .line 748
    :sswitch_1
    iget-object v0, p0, Lncp;->a:Loxe;

    if-nez v0, :cond_2

    .line 749
    new-instance v0, Loxe;

    invoke-direct {v0}, Loxe;-><init>()V

    iput-object v0, p0, Lncp;->a:Loxe;

    .line 751
    :cond_2
    iget-object v0, p0, Lncp;->a:Loxe;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 755
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lncp;->b:Ljava/lang/String;

    goto :goto_0

    .line 759
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lncp;->c:Ljava/lang/Boolean;

    goto :goto_0

    .line 733
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 694
    iget-object v0, p0, Lncp;->a:Loxe;

    if-eqz v0, :cond_0

    .line 695
    const/4 v0, 0x1

    iget-object v1, p0, Lncp;->a:Loxe;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 697
    :cond_0
    iget-object v0, p0, Lncp;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 698
    const/4 v0, 0x2

    iget-object v1, p0, Lncp;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 700
    :cond_1
    iget-object v0, p0, Lncp;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 701
    const/4 v0, 0x3

    iget-object v1, p0, Lncp;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 703
    :cond_2
    iget-object v0, p0, Lncp;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 705
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 678
    invoke-virtual {p0, p1}, Lncp;->a(Loxn;)Lncp;

    move-result-object v0

    return-object v0
.end method
