.class public Lknk;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Lgl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lgl",
            "<",
            "Lknl;",
            "Lkmz;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v0, Lgl;

    const/16 v1, 0x14

    invoke-direct {v0, v1}, Lgl;-><init>(I)V

    iput-object v0, p0, Lknk;->a:Lgl;

    .line 22
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lknk;->b:Landroid/content/Context;

    .line 23
    return-void
.end method

.method private a(ILkey;Lknl;)Lkmz;
    .locals 6

    .prologue
    .line 51
    new-instance v0, Lknj;

    iget-object v1, p0, Lknk;->b:Landroid/content/Context;

    iget-object v2, p3, Lknl;->a:Landroid/net/Uri;

    .line 52
    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p3, Lknl;->b:Lkng;

    move v2, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lknj;-><init>(Landroid/content/Context;ILjava/lang/String;Lkng;Lkey;)V

    .line 54
    invoke-virtual {v0}, Lknj;->l()V

    .line 56
    invoke-virtual {v0}, Lknj;->t()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 57
    const/4 v0, 0x0

    .line 66
    :cond_0
    :goto_0
    return-object v0

    .line 60
    :cond_1
    invoke-virtual {v0}, Lknj;->i()Lkmz;

    move-result-object v0

    .line 61
    if-eqz v0, :cond_0

    .line 62
    iget-object v1, p0, Lknk;->a:Lgl;

    monitor-enter v1

    .line 63
    :try_start_0
    iget-object v2, p0, Lknk;->a:Lgl;

    invoke-virtual {v2, p3, v0}, Lgl;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public a(ILkey;Ljava/lang/String;Lkng;Z)Lkmz;
    .locals 3

    .prologue
    .line 30
    new-instance v1, Lknl;

    invoke-direct {v1, p3, p4}, Lknl;-><init>(Ljava/lang/String;Lkng;)V

    .line 32
    const/4 v0, 0x0

    .line 33
    if-nez p5, :cond_0

    .line 34
    iget-object v2, p0, Lknk;->a:Lgl;

    monitor-enter v2

    .line 35
    :try_start_0
    iget-object v0, p0, Lknk;->a:Lgl;

    invoke-virtual {v0, v1}, Lgl;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkmz;

    .line 36
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 39
    :cond_0
    if-nez v0, :cond_1

    .line 40
    invoke-direct {p0, p1, p2, v1}, Lknk;->a(ILkey;Lknl;)Lkmz;

    move-result-object v0

    .line 43
    :cond_1
    return-object v0

    .line 36
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
