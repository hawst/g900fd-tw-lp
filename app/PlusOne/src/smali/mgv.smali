.class public final Lmgv;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnfn;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 22030
    invoke-direct {p0}, Loxq;-><init>()V

    .line 22033
    iput-object v0, p0, Lmgv;->apiHeader:Llyr;

    .line 22036
    iput-object v0, p0, Lmgv;->a:Lnfn;

    .line 22030
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 22053
    const/4 v0, 0x0

    .line 22054
    iget-object v1, p0, Lmgv;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 22055
    const/4 v0, 0x1

    iget-object v1, p0, Lmgv;->apiHeader:Llyr;

    .line 22056
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 22058
    :cond_0
    iget-object v1, p0, Lmgv;->a:Lnfn;

    if-eqz v1, :cond_1

    .line 22059
    const/4 v1, 0x2

    iget-object v2, p0, Lmgv;->a:Lnfn;

    .line 22060
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 22062
    :cond_1
    iget-object v1, p0, Lmgv;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 22063
    iput v0, p0, Lmgv;->ai:I

    .line 22064
    return v0
.end method

.method public a(Loxn;)Lmgv;
    .locals 2

    .prologue
    .line 22072
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 22073
    sparse-switch v0, :sswitch_data_0

    .line 22077
    iget-object v1, p0, Lmgv;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 22078
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmgv;->ah:Ljava/util/List;

    .line 22081
    :cond_1
    iget-object v1, p0, Lmgv;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 22083
    :sswitch_0
    return-object p0

    .line 22088
    :sswitch_1
    iget-object v0, p0, Lmgv;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 22089
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmgv;->apiHeader:Llyr;

    .line 22091
    :cond_2
    iget-object v0, p0, Lmgv;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 22095
    :sswitch_2
    iget-object v0, p0, Lmgv;->a:Lnfn;

    if-nez v0, :cond_3

    .line 22096
    new-instance v0, Lnfn;

    invoke-direct {v0}, Lnfn;-><init>()V

    iput-object v0, p0, Lmgv;->a:Lnfn;

    .line 22098
    :cond_3
    iget-object v0, p0, Lmgv;->a:Lnfn;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 22073
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 22041
    iget-object v0, p0, Lmgv;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 22042
    const/4 v0, 0x1

    iget-object v1, p0, Lmgv;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 22044
    :cond_0
    iget-object v0, p0, Lmgv;->a:Lnfn;

    if-eqz v0, :cond_1

    .line 22045
    const/4 v0, 0x2

    iget-object v1, p0, Lmgv;->a:Lnfn;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 22047
    :cond_1
    iget-object v0, p0, Lmgv;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 22049
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 22026
    invoke-virtual {p0, p1}, Lmgv;->a(Loxn;)Lmgv;

    move-result-object v0

    return-object v0
.end method
