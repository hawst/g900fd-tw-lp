.class public final Lmmz;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmmz;


# instance fields
.field public b:Ljava/lang/Long;

.field public c:[Lmna;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7160
    const/4 v0, 0x0

    new-array v0, v0, [Lmmz;

    sput-object v0, Lmmz;->a:[Lmmz;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 7161
    invoke-direct {p0}, Loxq;-><init>()V

    .line 7252
    sget-object v0, Lmna;->a:[Lmna;

    iput-object v0, p0, Lmmz;->c:[Lmna;

    .line 7161
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 7273
    .line 7274
    iget-object v0, p0, Lmmz;->b:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 7275
    const/4 v0, 0x1

    iget-object v2, p0, Lmmz;->b:Ljava/lang/Long;

    .line 7276
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Loxo;->f(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 7278
    :goto_0
    iget-object v2, p0, Lmmz;->c:[Lmna;

    if-eqz v2, :cond_1

    .line 7279
    iget-object v2, p0, Lmmz;->c:[Lmna;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 7280
    if-eqz v4, :cond_0

    .line 7281
    const/4 v5, 0x2

    .line 7282
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 7279
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 7286
    :cond_1
    iget-object v1, p0, Lmmz;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7287
    iput v0, p0, Lmmz;->ai:I

    .line 7288
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lmmz;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 7296
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 7297
    sparse-switch v0, :sswitch_data_0

    .line 7301
    iget-object v2, p0, Lmmz;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 7302
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmmz;->ah:Ljava/util/List;

    .line 7305
    :cond_1
    iget-object v2, p0, Lmmz;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 7307
    :sswitch_0
    return-object p0

    .line 7312
    :sswitch_1
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lmmz;->b:Ljava/lang/Long;

    goto :goto_0

    .line 7316
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 7317
    iget-object v0, p0, Lmmz;->c:[Lmna;

    if-nez v0, :cond_3

    move v0, v1

    .line 7318
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lmna;

    .line 7319
    iget-object v3, p0, Lmmz;->c:[Lmna;

    if-eqz v3, :cond_2

    .line 7320
    iget-object v3, p0, Lmmz;->c:[Lmna;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 7322
    :cond_2
    iput-object v2, p0, Lmmz;->c:[Lmna;

    .line 7323
    :goto_2
    iget-object v2, p0, Lmmz;->c:[Lmna;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 7324
    iget-object v2, p0, Lmmz;->c:[Lmna;

    new-instance v3, Lmna;

    invoke-direct {v3}, Lmna;-><init>()V

    aput-object v3, v2, v0

    .line 7325
    iget-object v2, p0, Lmmz;->c:[Lmna;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 7326
    invoke-virtual {p1}, Loxn;->a()I

    .line 7323
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 7317
    :cond_3
    iget-object v0, p0, Lmmz;->c:[Lmna;

    array-length v0, v0

    goto :goto_1

    .line 7329
    :cond_4
    iget-object v2, p0, Lmmz;->c:[Lmna;

    new-instance v3, Lmna;

    invoke-direct {v3}, Lmna;-><init>()V

    aput-object v3, v2, v0

    .line 7330
    iget-object v2, p0, Lmmz;->c:[Lmna;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 7297
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 7257
    iget-object v0, p0, Lmmz;->b:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 7258
    const/4 v0, 0x1

    iget-object v1, p0, Lmmz;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 7260
    :cond_0
    iget-object v0, p0, Lmmz;->c:[Lmna;

    if-eqz v0, :cond_2

    .line 7261
    iget-object v1, p0, Lmmz;->c:[Lmna;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 7262
    if-eqz v3, :cond_1

    .line 7263
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 7261
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 7267
    :cond_2
    iget-object v0, p0, Lmmz;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 7269
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 7157
    invoke-virtual {p0, p1}, Lmmz;->a(Loxn;)Lmmz;

    move-result-object v0

    return-object v0
.end method
