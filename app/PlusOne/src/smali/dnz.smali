.class public final Ldnz;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmko;",
        "Lmkp;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:[I


# instance fields
.field private final b:Ljava/lang/String;

.field private final p:Ljava/lang/String;

.field private final q:Lhgw;

.field private final r:Lhgw;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 24
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const/16 v2, 0xc

    aput v2, v0, v1

    sput-object v0, Ldnz;->a:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Lhgw;Lhgw;)V
    .locals 6

    .prologue
    .line 33
    const-string v3, "collectionupdate"

    new-instance v4, Lmko;

    invoke-direct {v4}, Lmko;-><init>()V

    new-instance v5, Lmkp;

    invoke-direct {v5}, Lmkp;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 36
    iput-object p3, p0, Ldnz;->b:Ljava/lang/String;

    .line 37
    iput-object p4, p0, Ldnz;->p:Ljava/lang/String;

    .line 38
    iput-object p5, p0, Ldnz;->q:Lhgw;

    .line 39
    iput-object p6, p0, Ldnz;->r:Lhgw;

    .line 40
    return-void
.end method


# virtual methods
.method protected a(Lmko;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 44
    new-instance v0, Lnai;

    invoke-direct {v0}, Lnai;-><init>()V

    iput-object v0, p1, Lmko;->a:Lnai;

    .line 45
    iget-object v0, p1, Lmko;->a:Lnai;

    .line 47
    iget-object v1, p0, Ldnz;->b:Ljava/lang/String;

    iget-object v2, p0, Ldnz;->p:Ljava/lang/String;

    invoke-static {v1, v2}, Ljvj;->a(Ljava/lang/String;Ljava/lang/String;)Lmzo;

    move-result-object v1

    iput-object v1, v0, Lnai;->a:Lmzo;

    .line 48
    sget-object v1, Ldnz;->a:[I

    iput-object v1, v0, Lnai;->b:[I

    .line 49
    new-instance v1, Lnaf;

    invoke-direct {v1}, Lnaf;-><init>()V

    iput-object v1, v0, Lnai;->e:Lnaf;

    .line 51
    new-instance v1, Locl;

    invoke-direct {v1}, Locl;-><init>()V

    .line 52
    iget-object v2, p0, Ldnz;->q:Lhgw;

    if-eqz v2, :cond_0

    .line 53
    iget-object v2, p0, Ldnz;->q:Lhgw;

    invoke-static {v2, v3}, Lhft;->a(Lhgw;Lhgw;)Lock;

    move-result-object v2

    iput-object v2, v1, Locl;->a:Lock;

    .line 55
    :cond_0
    iget-object v2, p0, Ldnz;->r:Lhgw;

    if-eqz v2, :cond_1

    .line 56
    iget-object v2, p0, Ldnz;->r:Lhgw;

    invoke-static {v2, v3}, Lhft;->a(Lhgw;Lhgw;)Lock;

    move-result-object v2

    iput-object v2, v1, Locl;->b:Lock;

    .line 58
    :cond_1
    iget-object v0, v0, Lnai;->e:Lnaf;

    iput-object v1, v0, Lnaf;->a:Locl;

    .line 59
    return-void
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 21
    check-cast p1, Lmko;

    invoke-virtual {p0, p1}, Ldnz;->a(Lmko;)V

    return-void
.end method

.method protected bridge synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 21
    return-void
.end method
