.class public final Lpnn;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lpnn;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Lpno;

.field private e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 499
    const/4 v0, 0x0

    new-array v0, v0, [Lpnn;

    sput-object v0, Lpnn;->a:[Lpnn;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 500
    invoke-direct {p0}, Loxq;-><init>()V

    .line 606
    const/4 v0, 0x0

    iput-object v0, p0, Lpnn;->d:Lpno;

    .line 500
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 631
    const/4 v0, 0x0

    .line 632
    iget-object v1, p0, Lpnn;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 633
    const/4 v0, 0x1

    iget-object v1, p0, Lpnn;->b:Ljava/lang/String;

    .line 634
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 636
    :cond_0
    iget-object v1, p0, Lpnn;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 637
    const/4 v1, 0x2

    iget-object v2, p0, Lpnn;->c:Ljava/lang/String;

    .line 638
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 640
    :cond_1
    iget-object v1, p0, Lpnn;->d:Lpno;

    if-eqz v1, :cond_2

    .line 641
    const/4 v1, 0x3

    iget-object v2, p0, Lpnn;->d:Lpno;

    .line 642
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 644
    :cond_2
    iget-object v1, p0, Lpnn;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 645
    const/4 v1, 0x4

    iget-object v2, p0, Lpnn;->e:Ljava/lang/String;

    .line 646
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 648
    :cond_3
    iget-object v1, p0, Lpnn;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 649
    iput v0, p0, Lpnn;->ai:I

    .line 650
    return v0
.end method

.method public a(Loxn;)Lpnn;
    .locals 2

    .prologue
    .line 658
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 659
    sparse-switch v0, :sswitch_data_0

    .line 663
    iget-object v1, p0, Lpnn;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 664
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpnn;->ah:Ljava/util/List;

    .line 667
    :cond_1
    iget-object v1, p0, Lpnn;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 669
    :sswitch_0
    return-object p0

    .line 674
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpnn;->b:Ljava/lang/String;

    goto :goto_0

    .line 678
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpnn;->c:Ljava/lang/String;

    goto :goto_0

    .line 682
    :sswitch_3
    iget-object v0, p0, Lpnn;->d:Lpno;

    if-nez v0, :cond_2

    .line 683
    new-instance v0, Lpno;

    invoke-direct {v0}, Lpno;-><init>()V

    iput-object v0, p0, Lpnn;->d:Lpno;

    .line 685
    :cond_2
    iget-object v0, p0, Lpnn;->d:Lpno;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 689
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpnn;->e:Ljava/lang/String;

    goto :goto_0

    .line 659
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 613
    iget-object v0, p0, Lpnn;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 614
    const/4 v0, 0x1

    iget-object v1, p0, Lpnn;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 616
    :cond_0
    iget-object v0, p0, Lpnn;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 617
    const/4 v0, 0x2

    iget-object v1, p0, Lpnn;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 619
    :cond_1
    iget-object v0, p0, Lpnn;->d:Lpno;

    if-eqz v0, :cond_2

    .line 620
    const/4 v0, 0x3

    iget-object v1, p0, Lpnn;->d:Lpno;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 622
    :cond_2
    iget-object v0, p0, Lpnn;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 623
    const/4 v0, 0x4

    iget-object v1, p0, Lpnn;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 625
    :cond_3
    iget-object v0, p0, Lpnn;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 627
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 496
    invoke-virtual {p0, p1}, Lpnn;->a(Loxn;)Lpnn;

    move-result-object v0

    return-object v0
.end method
