.class public final Lfuy;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Lloy;

.field private static final b:[Ljava/lang/String;

.field private static final c:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 22
    new-instance v0, Lloy;

    const-string v1, "debug.plus.restore_p_intent"

    invoke-direct {v0, v1}, Lloy;-><init>(Ljava/lang/String;)V

    sput-object v0, Lfuy;->a:Lloy;

    .line 26
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "com.google.android.apps.photos.phone.GetContentActivityAlias"

    aput-object v1, v0, v3

    const-string v1, "com.google.android.apps.photos.phone.SendContentActivityAlias"

    aput-object v1, v0, v4

    const-string v1, "com.google.android.apps.photos.phone.SetWallpaperActivityAlias"

    aput-object v1, v0, v5

    const/4 v1, 0x3

    const-string v2, "com.google.android.apps.plus.phone.VideoViewActivityAlias"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "com.google.android.apps.plus.phone.HostPhotoViewIntentActivityAlias"

    aput-object v2, v0, v1

    sput-object v0, Lfuy;->b:[Ljava/lang/String;

    .line 34
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "com.google.android.apps.photoeditor.fragments.PhotoEditorActivityAlias"

    aput-object v1, v0, v3

    const-string v1, "com.google.android.apps.photoeditor.fragments.PlusCropActivityAlias"

    aput-object v1, v0, v4

    sput-object v0, Lfuy;->c:[Ljava/lang/String;

    return-void
.end method

.method public static a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    const-string v0, "com.google.android.apps.photos"

    return-object v0
.end method

.method public static a(Landroid/content/Context;Z)V
    .locals 1

    .prologue
    .line 60
    if-eqz p1, :cond_0

    const-class v0, Lcom/google/android/apps/plus/phone/ConversationListActivity;

    .line 61
    invoke-static {p0, v0}, Lfug;->b(Landroid/content/Context;Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 62
    const-class v0, Lcom/google/android/apps/plus/phone/ConversationListActivity;

    invoke-static {p0, v0}, Lfug;->a(Landroid/content/Context;Ljava/lang/Class;)V

    .line 64
    :goto_0
    return-void

    .line 63
    :cond_0
    sget-object v0, Lfuy;->a:Lloy;

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;ZZ)V
    .locals 7

    .prologue
    const/4 v6, 0x5

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 78
    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    .line 79
    sget-object v2, Lfuy;->b:[Ljava/lang/String;

    move v1, v0

    :goto_0
    if-ge v1, v6, :cond_0

    aget-object v3, v2, v1

    .line 80
    invoke-static {p0, v3, v0}, Lfug;->a(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 79
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 82
    :cond_0
    sget-object v1, Lfuy;->c:[Ljava/lang/String;

    :goto_1
    if-ge v0, v5, :cond_2

    aget-object v2, v1, v0

    .line 83
    invoke-static {p0, v2, v4}, Lfug;->a(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 82
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 85
    :cond_1
    if-eqz p1, :cond_3

    sget-object v0, Lfuy;->a:Lloy;

    .line 93
    :cond_2
    return-void

    .line 86
    :cond_3
    sget-object v2, Lfuy;->b:[Ljava/lang/String;

    move v1, v0

    :goto_2
    if-ge v1, v6, :cond_4

    aget-object v3, v2, v1

    .line 87
    invoke-static {p0, v3, v0}, Lfug;->b(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 86
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 89
    :cond_4
    sget-object v1, Lfuy;->c:[Ljava/lang/String;

    :goto_3
    if-ge v0, v5, :cond_2

    aget-object v2, v1, v0

    .line 90
    invoke-static {p0, v2, v4}, Lfug;->b(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 89
    add-int/lit8 v0, v0, 0x1

    goto :goto_3
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 45
    const-string v0, "com.google.android.apps.photos"

    invoke-static {p0, v0}, Lfug;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "com.google.android.apps.photos"

    .line 46
    invoke-static {p0, v0}, Lfug;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 40
    const-string v0, "com.google.android.apps.photos"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static b(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 50
    invoke-static {p0}, Lfuy;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "com.google.android.apps.photos"

    const-string v1, "com.google.android.apps.photos.trampoline"

    .line 51
    invoke-static {p0, v0, v1}, Lfug;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 55
    invoke-static {p0}, Lfuy;->a(Landroid/content/Context;)Z

    move-result v0

    invoke-static {p0, v0}, Lfuy;->a(Landroid/content/Context;Z)V

    .line 56
    return-void
.end method

.method public static d(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 70
    const-class v0, Lfur;

    .line 71
    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfur;

    .line 72
    invoke-virtual {v0}, Lfur;->c()Z

    move-result v0

    .line 73
    invoke-static {p0}, Lfuy;->b(Landroid/content/Context;)Z

    move-result v1

    invoke-static {p0, v0, v1}, Lfuy;->a(Landroid/content/Context;ZZ)V

    .line 74
    return-void
.end method

.method public static e(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 96
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.google.android.apps.photos"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 97
    return-object v0
.end method
