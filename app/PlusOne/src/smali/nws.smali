.class public final Lnws;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Long;

.field private b:Ljava/lang/Long;

.field private c:Ljava/lang/Boolean;

.field private d:I

.field private e:Ljava/lang/Long;

.field private f:Ljava/lang/Long;

.field private g:Ljava/lang/Integer;

.field private h:[Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Loxq;-><init>()V

    .line 24
    const/high16 v0, -0x80000000

    iput v0, p0, Lnws;->d:I

    .line 33
    sget-object v0, Loxx;->h:[Ljava/lang/Long;

    iput-object v0, p0, Lnws;->h:[Ljava/lang/Long;

    .line 9
    return-void
.end method


# virtual methods
.method public a()I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 70
    .line 71
    iget-object v0, p0, Lnws;->a:Ljava/lang/Long;

    if-eqz v0, :cond_8

    .line 72
    const/4 v0, 0x1

    iget-object v2, p0, Lnws;->a:Ljava/lang/Long;

    .line 73
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Loxo;->f(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 75
    :goto_0
    iget-object v2, p0, Lnws;->b:Ljava/lang/Long;

    if-eqz v2, :cond_0

    .line 76
    const/4 v2, 0x2

    iget-object v3, p0, Lnws;->b:Ljava/lang/Long;

    .line 77
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Loxo;->f(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 79
    :cond_0
    iget-object v2, p0, Lnws;->c:Ljava/lang/Boolean;

    if-eqz v2, :cond_1

    .line 80
    const/4 v2, 0x3

    iget-object v3, p0, Lnws;->c:Ljava/lang/Boolean;

    .line 81
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 83
    :cond_1
    iget v2, p0, Lnws;->d:I

    const/high16 v3, -0x80000000

    if-eq v2, v3, :cond_2

    .line 84
    const/4 v2, 0x4

    iget v3, p0, Lnws;->d:I

    .line 85
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 87
    :cond_2
    iget-object v2, p0, Lnws;->e:Ljava/lang/Long;

    if-eqz v2, :cond_3

    .line 88
    const/4 v2, 0x5

    iget-object v3, p0, Lnws;->e:Ljava/lang/Long;

    .line 89
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Loxo;->f(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 91
    :cond_3
    iget-object v2, p0, Lnws;->f:Ljava/lang/Long;

    if-eqz v2, :cond_4

    .line 92
    const/4 v2, 0x6

    iget-object v3, p0, Lnws;->f:Ljava/lang/Long;

    .line 93
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Loxo;->f(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 95
    :cond_4
    iget-object v2, p0, Lnws;->g:Ljava/lang/Integer;

    if-eqz v2, :cond_5

    .line 96
    const/4 v2, 0x7

    iget-object v3, p0, Lnws;->g:Ljava/lang/Integer;

    .line 97
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 99
    :cond_5
    iget-object v2, p0, Lnws;->h:[Ljava/lang/Long;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lnws;->h:[Ljava/lang/Long;

    array-length v2, v2

    if-lez v2, :cond_7

    .line 101
    iget-object v3, p0, Lnws;->h:[Ljava/lang/Long;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_6

    aget-object v5, v3, v1

    .line 103
    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Loxo;->f(J)I

    move-result v5

    add-int/2addr v2, v5

    .line 101
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 105
    :cond_6
    add-int/2addr v0, v2

    .line 106
    iget-object v1, p0, Lnws;->h:[Ljava/lang/Long;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 108
    :cond_7
    iget-object v1, p0, Lnws;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 109
    iput v0, p0, Lnws;->ai:I

    .line 110
    return v0

    :cond_8
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lnws;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 118
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 119
    sparse-switch v0, :sswitch_data_0

    .line 123
    iget-object v1, p0, Lnws;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 124
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnws;->ah:Ljava/util/List;

    .line 127
    :cond_1
    iget-object v1, p0, Lnws;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 129
    :sswitch_0
    return-object p0

    .line 134
    :sswitch_1
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lnws;->a:Ljava/lang/Long;

    goto :goto_0

    .line 138
    :sswitch_2
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lnws;->b:Ljava/lang/Long;

    goto :goto_0

    .line 142
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnws;->c:Ljava/lang/Boolean;

    goto :goto_0

    .line 146
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 147
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 150
    :cond_2
    iput v0, p0, Lnws;->d:I

    goto :goto_0

    .line 152
    :cond_3
    iput v4, p0, Lnws;->d:I

    goto :goto_0

    .line 157
    :sswitch_5
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lnws;->e:Ljava/lang/Long;

    goto :goto_0

    .line 161
    :sswitch_6
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lnws;->f:Ljava/lang/Long;

    goto :goto_0

    .line 165
    :sswitch_7
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnws;->g:Ljava/lang/Integer;

    goto :goto_0

    .line 169
    :sswitch_8
    const/16 v0, 0x40

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 170
    iget-object v0, p0, Lnws;->h:[Ljava/lang/Long;

    array-length v0, v0

    .line 171
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/Long;

    .line 172
    iget-object v2, p0, Lnws;->h:[Ljava/lang/Long;

    invoke-static {v2, v4, v1, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 173
    iput-object v1, p0, Lnws;->h:[Ljava/lang/Long;

    .line 174
    :goto_1
    iget-object v1, p0, Lnws;->h:[Ljava/lang/Long;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_4

    .line 175
    iget-object v1, p0, Lnws;->h:[Ljava/lang/Long;

    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v0

    .line 176
    invoke-virtual {p1}, Loxn;->a()I

    .line 174
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 179
    :cond_4
    iget-object v1, p0, Lnws;->h:[Ljava/lang/Long;

    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v0

    goto/16 :goto_0

    .line 119
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 8

    .prologue
    .line 38
    iget-object v0, p0, Lnws;->a:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 39
    const/4 v0, 0x1

    iget-object v1, p0, Lnws;->a:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 41
    :cond_0
    iget-object v0, p0, Lnws;->b:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 42
    const/4 v0, 0x2

    iget-object v1, p0, Lnws;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 44
    :cond_1
    iget-object v0, p0, Lnws;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 45
    const/4 v0, 0x3

    iget-object v1, p0, Lnws;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 47
    :cond_2
    iget v0, p0, Lnws;->d:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_3

    .line 48
    const/4 v0, 0x4

    iget v1, p0, Lnws;->d:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 50
    :cond_3
    iget-object v0, p0, Lnws;->e:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 51
    const/4 v0, 0x5

    iget-object v1, p0, Lnws;->e:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 53
    :cond_4
    iget-object v0, p0, Lnws;->f:Ljava/lang/Long;

    if-eqz v0, :cond_5

    .line 54
    const/4 v0, 0x6

    iget-object v1, p0, Lnws;->f:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 56
    :cond_5
    iget-object v0, p0, Lnws;->g:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 57
    const/4 v0, 0x7

    iget-object v1, p0, Lnws;->g:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 59
    :cond_6
    iget-object v0, p0, Lnws;->h:[Ljava/lang/Long;

    if-eqz v0, :cond_7

    .line 60
    iget-object v1, p0, Lnws;->h:[Ljava/lang/Long;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_7

    aget-object v3, v1, v0

    .line 61
    const/16 v4, 0x8

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {p1, v4, v6, v7}, Loxo;->b(IJ)V

    .line 60
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 64
    :cond_7
    iget-object v0, p0, Lnws;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 66
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Lnws;->a(Loxn;)Lnws;

    move-result-object v0

    return-object v0
.end method
