.class public Lllg;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lze;


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lllh;",
            ">;"
        }
    .end annotation
.end field

.field private b:Landroid/support/v7/widget/SearchView;

.field private c:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lllg;->a:Ljava/util/List;

    .line 19
    const/4 v0, 0x0

    iput-boolean v0, p0, Lllg;->c:Z

    .line 29
    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 72
    iget-object v0, p0, Lllg;->b:Landroid/support/v7/widget/SearchView;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/SearchView;->a(Ljava/lang/CharSequence;Z)V

    .line 73
    return-void
.end method

.method public a(Landroid/support/v7/widget/SearchView;)V
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lllg;->b:Landroid/support/v7/widget/SearchView;

    .line 33
    invoke-virtual {p1, p0}, Landroid/support/v7/widget/SearchView;->a(Lze;)V

    .line 34
    return-void
.end method

.method public a(Lllh;)V
    .locals 2

    .prologue
    .line 41
    iget-object v0, p0, Lllg;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 43
    iget-object v0, p0, Lllg;->b:Landroid/support/v7/widget/SearchView;

    if-eqz v0, :cond_0

    .line 44
    iget-object v0, p0, Lllg;->b:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView;->c()Ljava/lang/CharSequence;

    move-result-object v0

    .line 45
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 46
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lllh;->a(Ljava/lang/String;)V

    .line 49
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 37
    iput-boolean p1, p0, Lllg;->c:Z

    .line 38
    return-void
.end method

.method public a(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 53
    iget-object v0, p0, Lllg;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 54
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 55
    iget-object v0, p0, Lllg;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lllh;

    invoke-interface {v0, p1}, Lllh;->a(Ljava/lang/String;)V

    .line 54
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 57
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public b(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 62
    iget-boolean v0, p0, Lllg;->c:Z

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lllg;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 64
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 65
    iget-object v0, p0, Lllg;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lllh;

    invoke-interface {v0, p1}, Lllh;->a(Ljava/lang/String;)V

    .line 64
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 68
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
