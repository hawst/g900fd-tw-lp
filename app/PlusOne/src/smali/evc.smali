.class public final Levc;
.super Lhxz;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lhxz",
        "<",
        "Lcpg;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:I

.field private final c:Ljava/lang/Long;

.field private final d:Ldp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldo",
            "<",
            "Lcpg;",
            ">.dp;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;IJ)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lhxz;-><init>(Landroid/content/Context;)V

    .line 19
    new-instance v0, Ldp;

    invoke-direct {v0, p0}, Ldp;-><init>(Ldo;)V

    iput-object v0, p0, Levc;->d:Ldp;

    .line 23
    iput p2, p0, Levc;->b:I

    .line 24
    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Levc;->c:Ljava/lang/Long;

    .line 25
    return-void
.end method


# virtual methods
.method protected D_()Z
    .locals 4

    .prologue
    .line 29
    invoke-virtual {p0}, Levc;->n()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget v1, p0, Levc;->b:I

    .line 30
    invoke-static {v1}, Ljvd;->a(I)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Levc;->d:Ldp;

    .line 29
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 32
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic j()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 14
    invoke-virtual {p0}, Levc;->l()Lcpg;

    move-result-object v0

    return-object v0
.end method

.method protected k()Z
    .locals 2

    .prologue
    .line 37
    invoke-virtual {p0}, Levc;->n()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Levc;->d:Ldp;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 38
    const/4 v0, 0x1

    return v0
.end method

.method public l()Lcpg;
    .locals 7

    .prologue
    .line 43
    iget v0, p0, Levc;->b:I

    invoke-static {v0}, Lcpi;->a(I)Lcpi;

    move-result-object v1

    .line 44
    invoke-virtual {p0}, Levc;->n()Landroid/content/Context;

    move-result-object v2

    iget v3, p0, Levc;->b:I

    iget-object v0, p0, Levc;->c:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sget-object v6, Lcph;->b:Lcph;

    invoke-virtual/range {v1 .. v6}, Lcpi;->a(Landroid/content/Context;IJLcph;)Lcpg;

    move-result-object v0

    return-object v0
.end method
