.class public final Lmvn;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Double;

.field public d:I

.field public e:I

.field public f:Ljava/lang/Integer;

.field private g:I

.field private h:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/high16 v0, -0x80000000

    .line 14100
    invoke-direct {p0}, Loxq;-><init>()V

    .line 14128
    iput v0, p0, Lmvn;->d:I

    .line 14131
    iput v0, p0, Lmvn;->e:I

    .line 14134
    iput v0, p0, Lmvn;->g:I

    .line 14100
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    const/high16 v3, -0x80000000

    .line 14173
    const/4 v0, 0x0

    .line 14174
    iget-object v1, p0, Lmvn;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 14175
    const/4 v0, 0x1

    iget-object v1, p0, Lmvn;->a:Ljava/lang/String;

    .line 14176
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 14178
    :cond_0
    iget-object v1, p0, Lmvn;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 14179
    const/4 v1, 0x2

    iget-object v2, p0, Lmvn;->b:Ljava/lang/Integer;

    .line 14180
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 14182
    :cond_1
    iget-object v1, p0, Lmvn;->c:Ljava/lang/Double;

    if-eqz v1, :cond_2

    .line 14183
    const/4 v1, 0x3

    iget-object v2, p0, Lmvn;->c:Ljava/lang/Double;

    .line 14184
    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 14186
    :cond_2
    iget v1, p0, Lmvn;->d:I

    if-eq v1, v3, :cond_3

    .line 14187
    const/4 v1, 0x4

    iget v2, p0, Lmvn;->d:I

    .line 14188
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 14190
    :cond_3
    iget v1, p0, Lmvn;->e:I

    if-eq v1, v3, :cond_4

    .line 14191
    const/4 v1, 0x5

    iget v2, p0, Lmvn;->e:I

    .line 14192
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 14194
    :cond_4
    iget v1, p0, Lmvn;->g:I

    if-eq v1, v3, :cond_5

    .line 14195
    const/4 v1, 0x6

    iget v2, p0, Lmvn;->g:I

    .line 14196
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 14198
    :cond_5
    iget-object v1, p0, Lmvn;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 14199
    const/4 v1, 0x7

    iget-object v2, p0, Lmvn;->f:Ljava/lang/Integer;

    .line 14200
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 14202
    :cond_6
    iget-object v1, p0, Lmvn;->h:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    .line 14203
    const/16 v1, 0x8

    iget-object v2, p0, Lmvn;->h:Ljava/lang/Integer;

    .line 14204
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 14206
    :cond_7
    iget-object v1, p0, Lmvn;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 14207
    iput v0, p0, Lmvn;->ai:I

    .line 14208
    return v0
.end method

.method public a(Loxn;)Lmvn;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 14216
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 14217
    sparse-switch v0, :sswitch_data_0

    .line 14221
    iget-object v1, p0, Lmvn;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 14222
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmvn;->ah:Ljava/util/List;

    .line 14225
    :cond_1
    iget-object v1, p0, Lmvn;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 14227
    :sswitch_0
    return-object p0

    .line 14232
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmvn;->a:Ljava/lang/String;

    goto :goto_0

    .line 14236
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmvn;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 14240
    :sswitch_3
    invoke-virtual {p1}, Loxn;->c()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lmvn;->c:Ljava/lang/Double;

    goto :goto_0

    .line 14244
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 14245
    if-eqz v0, :cond_2

    if-eq v0, v3, :cond_2

    if-ne v0, v4, :cond_3

    .line 14248
    :cond_2
    iput v0, p0, Lmvn;->d:I

    goto :goto_0

    .line 14250
    :cond_3
    iput v2, p0, Lmvn;->d:I

    goto :goto_0

    .line 14255
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 14256
    if-eqz v0, :cond_4

    if-eq v0, v3, :cond_4

    if-eq v0, v4, :cond_4

    const/4 v1, 0x3

    if-ne v0, v1, :cond_5

    .line 14260
    :cond_4
    iput v0, p0, Lmvn;->e:I

    goto :goto_0

    .line 14262
    :cond_5
    iput v2, p0, Lmvn;->e:I

    goto :goto_0

    .line 14267
    :sswitch_6
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 14268
    if-eqz v0, :cond_6

    if-eq v0, v3, :cond_6

    if-ne v0, v4, :cond_7

    .line 14271
    :cond_6
    iput v0, p0, Lmvn;->g:I

    goto :goto_0

    .line 14273
    :cond_7
    iput v2, p0, Lmvn;->g:I

    goto :goto_0

    .line 14278
    :sswitch_7
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmvn;->f:Ljava/lang/Integer;

    goto :goto_0

    .line 14282
    :sswitch_8
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmvn;->h:Ljava/lang/Integer;

    goto :goto_0

    .line 14217
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x19 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    const/high16 v4, -0x80000000

    .line 14143
    iget-object v0, p0, Lmvn;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 14144
    const/4 v0, 0x1

    iget-object v1, p0, Lmvn;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 14146
    :cond_0
    iget-object v0, p0, Lmvn;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 14147
    const/4 v0, 0x2

    iget-object v1, p0, Lmvn;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 14149
    :cond_1
    iget-object v0, p0, Lmvn;->c:Ljava/lang/Double;

    if-eqz v0, :cond_2

    .line 14150
    const/4 v0, 0x3

    iget-object v1, p0, Lmvn;->c:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(ID)V

    .line 14152
    :cond_2
    iget v0, p0, Lmvn;->d:I

    if-eq v0, v4, :cond_3

    .line 14153
    const/4 v0, 0x4

    iget v1, p0, Lmvn;->d:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 14155
    :cond_3
    iget v0, p0, Lmvn;->e:I

    if-eq v0, v4, :cond_4

    .line 14156
    const/4 v0, 0x5

    iget v1, p0, Lmvn;->e:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 14158
    :cond_4
    iget v0, p0, Lmvn;->g:I

    if-eq v0, v4, :cond_5

    .line 14159
    const/4 v0, 0x6

    iget v1, p0, Lmvn;->g:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 14161
    :cond_5
    iget-object v0, p0, Lmvn;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 14162
    const/4 v0, 0x7

    iget-object v1, p0, Lmvn;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 14164
    :cond_6
    iget-object v0, p0, Lmvn;->h:Ljava/lang/Integer;

    if-eqz v0, :cond_7

    .line 14165
    const/16 v0, 0x8

    iget-object v1, p0, Lmvn;->h:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 14167
    :cond_7
    iget-object v0, p0, Lmvn;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 14169
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 14096
    invoke-virtual {p0, p1}, Lmvn;->a(Loxn;)Lmvn;

    move-result-object v0

    return-object v0
.end method
