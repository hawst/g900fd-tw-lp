.class public final Lkmu;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lbc",
        "<",
        "Lkmz;",
        ">;"
    }
.end annotation


# instance fields
.field private synthetic a:Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;)V
    .locals 0

    .prologue
    .line 283
    iput-object p1, p0, Lkmu;->a:Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Lkmz;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 286
    packed-switch p1, :pswitch_data_0

    .line 299
    :goto_0
    return-object v0

    .line 288
    :pswitch_0
    iget-object v1, p0, Lkmu;->a:Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;

    iget-object v1, v1, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->u:Lkkr;

    invoke-virtual {v1}, Lkkr;->b()Lkey;

    move-result-object v3

    .line 289
    if-nez v3, :cond_0

    .line 290
    iget-object v1, p0, Lkmu;->a:Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;

    const-string v2, "com.google.android.apps.social"

    invoke-static {v1, v2, v0}, Lkje;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lkey;

    move-result-object v3

    .line 293
    :cond_0
    new-instance v0, Lknm;

    iget-object v1, p0, Lkmu;->a:Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;

    iget-object v2, p0, Lkmu;->a:Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;

    iget-object v2, v2, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->f:Lhee;

    .line 294
    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    iget-object v4, p0, Lkmu;->a:Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;

    iget-object v4, v4, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->t:Lkki;

    .line 295
    invoke-virtual {v4}, Lkki;->m()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lkmu;->a:Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;

    iget-object v5, v5, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->u:Lkkr;

    invoke-virtual {v5}, Lkkr;->c()Lkng;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lknm;-><init>(Landroid/content/Context;ILkey;Ljava/lang/String;Lkng;)V

    goto :goto_0

    .line 286
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Lkmz;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 326
    return-void
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 283
    check-cast p2, Lkmz;

    invoke-virtual {p0, p1, p2}, Lkmu;->a(Ldo;Lkmz;)V

    return-void
.end method

.method public a(Ldo;Lkmz;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Lkmz;",
            ">;",
            "Lkmz;",
            ")V"
        }
    .end annotation

    .prologue
    .line 305
    invoke-virtual {p1}, Ldo;->o()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 319
    :goto_0
    return-void

    .line 307
    :pswitch_0
    iget-object v0, p0, Lkmu;->a:Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;

    .line 308
    if-eqz p2, :cond_0

    sget-object v0, Lhmv;->dI:Lhmv;

    move-object v1, v0

    .line 311
    :goto_1
    iget-object v0, p0, Lkmu;->a:Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;

    iget-object v0, v0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->f:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v2

    .line 312
    iget-object v0, p0, Lkmu;->a:Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;

    invoke-static {v0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->a(Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;)Llnh;

    move-result-object v0

    const-class v3, Lhms;

    invoke-virtual {v0, v3}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v3, Lhmr;

    iget-object v4, p0, Lkmu;->a:Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;

    invoke-direct {v3, v4, v2}, Lhmr;-><init>(Landroid/content/Context;I)V

    .line 314
    invoke-virtual {v3, v1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 312
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 316
    iget-object v0, p0, Lkmu;->a:Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;

    invoke-virtual {v0, p2}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->a(Lkmz;)V

    .line 318
    iget-object v0, p0, Lkmu;->a:Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->q()V

    goto :goto_0

    .line 308
    :cond_0
    sget-object v0, Lhmv;->dJ:Lhmv;

    move-object v1, v0

    goto :goto_1

    .line 305
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method
