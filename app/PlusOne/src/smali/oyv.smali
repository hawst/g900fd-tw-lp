.class public final Loyv;
.super Loxq;
.source "PG"


# static fields
.field public static final a:Loxr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Loxr",
            "<",
            "Loyv;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private A:Ljava/lang/String;

.field private B:Ljava/lang/String;

.field private C:Ljava/lang/String;

.field private D:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Lpdi;

.field public f:I

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:[Loya;

.field private k:Loya;

.field private l:Ljava/lang/String;

.field private m:[Loya;

.field private n:Ljava/lang/String;

.field private o:Loya;

.field private p:[Loya;

.field private q:Ljava/lang/Boolean;

.field private r:Ljava/lang/String;

.field private s:Loya;

.field private t:[Loya;

.field private u:I

.field private v:Loya;

.field private w:Ljava/lang/String;

.field private x:Ljava/lang/String;

.field private y:Loya;

.field private z:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 9
    const v0, 0x2837b39

    new-instance v1, Loyw;

    invoke-direct {v1}, Loyw;-><init>()V

    .line 14
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    move-result-object v0

    sput-object v0, Loyv;->a:Loxr;

    .line 13
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 27
    iput-object v1, p0, Loyv;->e:Lpdi;

    .line 32
    sget-object v0, Loya;->a:[Loya;

    iput-object v0, p0, Loyv;->j:[Loya;

    .line 35
    iput-object v1, p0, Loyv;->k:Loya;

    .line 40
    sget-object v0, Loya;->a:[Loya;

    iput-object v0, p0, Loyv;->m:[Loya;

    .line 45
    iput-object v1, p0, Loyv;->o:Loya;

    .line 48
    sget-object v0, Loya;->a:[Loya;

    iput-object v0, p0, Loyv;->p:[Loya;

    .line 55
    iput-object v1, p0, Loyv;->s:Loya;

    .line 58
    sget-object v0, Loya;->a:[Loya;

    iput-object v0, p0, Loyv;->t:[Loya;

    .line 61
    iput v2, p0, Loyv;->u:I

    .line 64
    iput-object v1, p0, Loyv;->v:Loya;

    .line 67
    iput v2, p0, Loyv;->f:I

    .line 74
    iput-object v1, p0, Loyv;->y:Loya;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 8

    .prologue
    const/high16 v7, -0x80000000

    const/4 v1, 0x0

    .line 198
    .line 199
    iget-object v0, p0, Loyv;->b:Ljava/lang/String;

    if-eqz v0, :cond_20

    .line 200
    const/4 v0, 0x1

    iget-object v2, p0, Loyv;->b:Ljava/lang/String;

    .line 201
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 203
    :goto_0
    iget-object v2, p0, Loyv;->g:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 204
    const/4 v2, 0x2

    iget-object v3, p0, Loyv;->g:Ljava/lang/String;

    .line 205
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 207
    :cond_0
    iget-object v2, p0, Loyv;->c:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 208
    const/4 v2, 0x3

    iget-object v3, p0, Loyv;->c:Ljava/lang/String;

    .line 209
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 211
    :cond_1
    iget-object v2, p0, Loyv;->d:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 212
    const/4 v2, 0x4

    iget-object v3, p0, Loyv;->d:Ljava/lang/String;

    .line 213
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 215
    :cond_2
    iget-object v2, p0, Loyv;->h:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 216
    const/4 v2, 0x5

    iget-object v3, p0, Loyv;->h:Ljava/lang/String;

    .line 217
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 219
    :cond_3
    iget-object v2, p0, Loyv;->e:Lpdi;

    if-eqz v2, :cond_4

    .line 220
    const/4 v2, 0x6

    iget-object v3, p0, Loyv;->e:Lpdi;

    .line 221
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 223
    :cond_4
    iget-object v2, p0, Loyv;->i:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 224
    const/4 v2, 0x7

    iget-object v3, p0, Loyv;->i:Ljava/lang/String;

    .line 225
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 227
    :cond_5
    iget-object v2, p0, Loyv;->j:[Loya;

    if-eqz v2, :cond_7

    .line 228
    iget-object v3, p0, Loyv;->j:[Loya;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_7

    aget-object v5, v3, v2

    .line 229
    if-eqz v5, :cond_6

    .line 230
    const/16 v6, 0x8

    .line 231
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 228
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 235
    :cond_7
    iget-object v2, p0, Loyv;->k:Loya;

    if-eqz v2, :cond_8

    .line 236
    const/16 v2, 0x9

    iget-object v3, p0, Loyv;->k:Loya;

    .line 237
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 239
    :cond_8
    iget-object v2, p0, Loyv;->l:Ljava/lang/String;

    if-eqz v2, :cond_9

    .line 240
    const/16 v2, 0xa

    iget-object v3, p0, Loyv;->l:Ljava/lang/String;

    .line 241
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 243
    :cond_9
    iget-object v2, p0, Loyv;->m:[Loya;

    if-eqz v2, :cond_b

    .line 244
    iget-object v3, p0, Loyv;->m:[Loya;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_b

    aget-object v5, v3, v2

    .line 245
    if-eqz v5, :cond_a

    .line 246
    const/16 v6, 0xb

    .line 247
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 244
    :cond_a
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 251
    :cond_b
    iget-object v2, p0, Loyv;->n:Ljava/lang/String;

    if-eqz v2, :cond_c

    .line 252
    const/16 v2, 0xc

    iget-object v3, p0, Loyv;->n:Ljava/lang/String;

    .line 253
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 255
    :cond_c
    iget-object v2, p0, Loyv;->o:Loya;

    if-eqz v2, :cond_d

    .line 256
    const/16 v2, 0x12

    iget-object v3, p0, Loyv;->o:Loya;

    .line 257
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 259
    :cond_d
    iget-object v2, p0, Loyv;->p:[Loya;

    if-eqz v2, :cond_f

    .line 260
    iget-object v3, p0, Loyv;->p:[Loya;

    array-length v4, v3

    move v2, v1

    :goto_3
    if-ge v2, v4, :cond_f

    aget-object v5, v3, v2

    .line 261
    if-eqz v5, :cond_e

    .line 262
    const/16 v6, 0x2a

    .line 263
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 260
    :cond_e
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 267
    :cond_f
    iget-object v2, p0, Loyv;->q:Ljava/lang/Boolean;

    if-eqz v2, :cond_10

    .line 268
    const/16 v2, 0x41

    iget-object v3, p0, Loyv;->q:Ljava/lang/Boolean;

    .line 269
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 271
    :cond_10
    iget-object v2, p0, Loyv;->r:Ljava/lang/String;

    if-eqz v2, :cond_11

    .line 272
    const/16 v2, 0x4b

    iget-object v3, p0, Loyv;->r:Ljava/lang/String;

    .line 273
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 275
    :cond_11
    iget-object v2, p0, Loyv;->s:Loya;

    if-eqz v2, :cond_12

    .line 276
    const/16 v2, 0x52

    iget-object v3, p0, Loyv;->s:Loya;

    .line 277
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 279
    :cond_12
    iget-object v2, p0, Loyv;->t:[Loya;

    if-eqz v2, :cond_14

    .line 280
    iget-object v2, p0, Loyv;->t:[Loya;

    array-length v3, v2

    :goto_4
    if-ge v1, v3, :cond_14

    aget-object v4, v2, v1

    .line 281
    if-eqz v4, :cond_13

    .line 282
    const/16 v5, 0x53

    .line 283
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 280
    :cond_13
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 287
    :cond_14
    iget v1, p0, Loyv;->u:I

    if-eq v1, v7, :cond_15

    .line 288
    const/16 v1, 0x5a

    iget v2, p0, Loyv;->u:I

    .line 289
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 291
    :cond_15
    iget-object v1, p0, Loyv;->v:Loya;

    if-eqz v1, :cond_16

    .line 292
    const/16 v1, 0x60

    iget-object v2, p0, Loyv;->v:Loya;

    .line 293
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 295
    :cond_16
    iget v1, p0, Loyv;->f:I

    if-eq v1, v7, :cond_17

    .line 296
    const/16 v1, 0x6e

    iget v2, p0, Loyv;->f:I

    .line 297
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 299
    :cond_17
    iget-object v1, p0, Loyv;->w:Ljava/lang/String;

    if-eqz v1, :cond_18

    .line 300
    const/16 v1, 0x6f

    iget-object v2, p0, Loyv;->w:Ljava/lang/String;

    .line 301
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 303
    :cond_18
    iget-object v1, p0, Loyv;->x:Ljava/lang/String;

    if-eqz v1, :cond_19

    .line 304
    const/16 v1, 0x70

    iget-object v2, p0, Loyv;->x:Ljava/lang/String;

    .line 305
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 307
    :cond_19
    iget-object v1, p0, Loyv;->y:Loya;

    if-eqz v1, :cond_1a

    .line 308
    const/16 v1, 0xb9

    iget-object v2, p0, Loyv;->y:Loya;

    .line 309
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 311
    :cond_1a
    iget-object v1, p0, Loyv;->z:Ljava/lang/String;

    if-eqz v1, :cond_1b

    .line 312
    const/16 v1, 0xbc

    iget-object v2, p0, Loyv;->z:Ljava/lang/String;

    .line 313
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 315
    :cond_1b
    iget-object v1, p0, Loyv;->A:Ljava/lang/String;

    if-eqz v1, :cond_1c

    .line 316
    const/16 v1, 0xbd

    iget-object v2, p0, Loyv;->A:Ljava/lang/String;

    .line 317
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 319
    :cond_1c
    iget-object v1, p0, Loyv;->B:Ljava/lang/String;

    if-eqz v1, :cond_1d

    .line 320
    const/16 v1, 0xbe

    iget-object v2, p0, Loyv;->B:Ljava/lang/String;

    .line 321
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 323
    :cond_1d
    iget-object v1, p0, Loyv;->C:Ljava/lang/String;

    if-eqz v1, :cond_1e

    .line 324
    const/16 v1, 0xbf

    iget-object v2, p0, Loyv;->C:Ljava/lang/String;

    .line 325
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 327
    :cond_1e
    iget-object v1, p0, Loyv;->D:Ljava/lang/String;

    if-eqz v1, :cond_1f

    .line 328
    const/16 v1, 0xfe

    iget-object v2, p0, Loyv;->D:Ljava/lang/String;

    .line 329
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 331
    :cond_1f
    iget-object v1, p0, Loyv;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 332
    iput v0, p0, Loyv;->ai:I

    .line 333
    return v0

    :cond_20
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Loyv;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 341
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 342
    sparse-switch v0, :sswitch_data_0

    .line 346
    iget-object v2, p0, Loyv;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 347
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Loyv;->ah:Ljava/util/List;

    .line 350
    :cond_1
    iget-object v2, p0, Loyv;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 352
    :sswitch_0
    return-object p0

    .line 357
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyv;->b:Ljava/lang/String;

    goto :goto_0

    .line 361
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyv;->g:Ljava/lang/String;

    goto :goto_0

    .line 365
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyv;->c:Ljava/lang/String;

    goto :goto_0

    .line 369
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyv;->d:Ljava/lang/String;

    goto :goto_0

    .line 373
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyv;->h:Ljava/lang/String;

    goto :goto_0

    .line 377
    :sswitch_6
    iget-object v0, p0, Loyv;->e:Lpdi;

    if-nez v0, :cond_2

    .line 378
    new-instance v0, Lpdi;

    invoke-direct {v0}, Lpdi;-><init>()V

    iput-object v0, p0, Loyv;->e:Lpdi;

    .line 380
    :cond_2
    iget-object v0, p0, Loyv;->e:Lpdi;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 384
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyv;->i:Ljava/lang/String;

    goto :goto_0

    .line 388
    :sswitch_8
    const/16 v0, 0x42

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 389
    iget-object v0, p0, Loyv;->j:[Loya;

    if-nez v0, :cond_4

    move v0, v1

    .line 390
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Loya;

    .line 391
    iget-object v3, p0, Loyv;->j:[Loya;

    if-eqz v3, :cond_3

    .line 392
    iget-object v3, p0, Loyv;->j:[Loya;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 394
    :cond_3
    iput-object v2, p0, Loyv;->j:[Loya;

    .line 395
    :goto_2
    iget-object v2, p0, Loyv;->j:[Loya;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 396
    iget-object v2, p0, Loyv;->j:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 397
    iget-object v2, p0, Loyv;->j:[Loya;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 398
    invoke-virtual {p1}, Loxn;->a()I

    .line 395
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 389
    :cond_4
    iget-object v0, p0, Loyv;->j:[Loya;

    array-length v0, v0

    goto :goto_1

    .line 401
    :cond_5
    iget-object v2, p0, Loyv;->j:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 402
    iget-object v2, p0, Loyv;->j:[Loya;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 406
    :sswitch_9
    iget-object v0, p0, Loyv;->k:Loya;

    if-nez v0, :cond_6

    .line 407
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    iput-object v0, p0, Loyv;->k:Loya;

    .line 409
    :cond_6
    iget-object v0, p0, Loyv;->k:Loya;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 413
    :sswitch_a
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyv;->l:Ljava/lang/String;

    goto/16 :goto_0

    .line 417
    :sswitch_b
    const/16 v0, 0x5a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 418
    iget-object v0, p0, Loyv;->m:[Loya;

    if-nez v0, :cond_8

    move v0, v1

    .line 419
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Loya;

    .line 420
    iget-object v3, p0, Loyv;->m:[Loya;

    if-eqz v3, :cond_7

    .line 421
    iget-object v3, p0, Loyv;->m:[Loya;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 423
    :cond_7
    iput-object v2, p0, Loyv;->m:[Loya;

    .line 424
    :goto_4
    iget-object v2, p0, Loyv;->m:[Loya;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    .line 425
    iget-object v2, p0, Loyv;->m:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 426
    iget-object v2, p0, Loyv;->m:[Loya;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 427
    invoke-virtual {p1}, Loxn;->a()I

    .line 424
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 418
    :cond_8
    iget-object v0, p0, Loyv;->m:[Loya;

    array-length v0, v0

    goto :goto_3

    .line 430
    :cond_9
    iget-object v2, p0, Loyv;->m:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 431
    iget-object v2, p0, Loyv;->m:[Loya;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 435
    :sswitch_c
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyv;->n:Ljava/lang/String;

    goto/16 :goto_0

    .line 439
    :sswitch_d
    iget-object v0, p0, Loyv;->o:Loya;

    if-nez v0, :cond_a

    .line 440
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    iput-object v0, p0, Loyv;->o:Loya;

    .line 442
    :cond_a
    iget-object v0, p0, Loyv;->o:Loya;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 446
    :sswitch_e
    const/16 v0, 0x152

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 447
    iget-object v0, p0, Loyv;->p:[Loya;

    if-nez v0, :cond_c

    move v0, v1

    .line 448
    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Loya;

    .line 449
    iget-object v3, p0, Loyv;->p:[Loya;

    if-eqz v3, :cond_b

    .line 450
    iget-object v3, p0, Loyv;->p:[Loya;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 452
    :cond_b
    iput-object v2, p0, Loyv;->p:[Loya;

    .line 453
    :goto_6
    iget-object v2, p0, Loyv;->p:[Loya;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_d

    .line 454
    iget-object v2, p0, Loyv;->p:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 455
    iget-object v2, p0, Loyv;->p:[Loya;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 456
    invoke-virtual {p1}, Loxn;->a()I

    .line 453
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 447
    :cond_c
    iget-object v0, p0, Loyv;->p:[Loya;

    array-length v0, v0

    goto :goto_5

    .line 459
    :cond_d
    iget-object v2, p0, Loyv;->p:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 460
    iget-object v2, p0, Loyv;->p:[Loya;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 464
    :sswitch_f
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Loyv;->q:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 468
    :sswitch_10
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyv;->r:Ljava/lang/String;

    goto/16 :goto_0

    .line 472
    :sswitch_11
    iget-object v0, p0, Loyv;->s:Loya;

    if-nez v0, :cond_e

    .line 473
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    iput-object v0, p0, Loyv;->s:Loya;

    .line 475
    :cond_e
    iget-object v0, p0, Loyv;->s:Loya;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 479
    :sswitch_12
    const/16 v0, 0x29a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 480
    iget-object v0, p0, Loyv;->t:[Loya;

    if-nez v0, :cond_10

    move v0, v1

    .line 481
    :goto_7
    add-int/2addr v2, v0

    new-array v2, v2, [Loya;

    .line 482
    iget-object v3, p0, Loyv;->t:[Loya;

    if-eqz v3, :cond_f

    .line 483
    iget-object v3, p0, Loyv;->t:[Loya;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 485
    :cond_f
    iput-object v2, p0, Loyv;->t:[Loya;

    .line 486
    :goto_8
    iget-object v2, p0, Loyv;->t:[Loya;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_11

    .line 487
    iget-object v2, p0, Loyv;->t:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 488
    iget-object v2, p0, Loyv;->t:[Loya;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 489
    invoke-virtual {p1}, Loxn;->a()I

    .line 486
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 480
    :cond_10
    iget-object v0, p0, Loyv;->t:[Loya;

    array-length v0, v0

    goto :goto_7

    .line 492
    :cond_11
    iget-object v2, p0, Loyv;->t:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 493
    iget-object v2, p0, Loyv;->t:[Loya;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 497
    :sswitch_13
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 498
    if-eqz v0, :cond_12

    if-ne v0, v4, :cond_13

    .line 500
    :cond_12
    iput v0, p0, Loyv;->u:I

    goto/16 :goto_0

    .line 502
    :cond_13
    iput v1, p0, Loyv;->u:I

    goto/16 :goto_0

    .line 507
    :sswitch_14
    iget-object v0, p0, Loyv;->v:Loya;

    if-nez v0, :cond_14

    .line 508
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    iput-object v0, p0, Loyv;->v:Loya;

    .line 510
    :cond_14
    iget-object v0, p0, Loyv;->v:Loya;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 514
    :sswitch_15
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 515
    if-eq v0, v4, :cond_15

    const/4 v2, 0x2

    if-eq v0, v2, :cond_15

    const/4 v2, 0x3

    if-eq v0, v2, :cond_15

    const/4 v2, 0x4

    if-eq v0, v2, :cond_15

    const/4 v2, 0x5

    if-eq v0, v2, :cond_15

    const/4 v2, 0x6

    if-eq v0, v2, :cond_15

    const/4 v2, 0x7

    if-eq v0, v2, :cond_15

    const/16 v2, 0x8

    if-eq v0, v2, :cond_15

    const/16 v2, 0x9

    if-eq v0, v2, :cond_15

    const/16 v2, 0xa

    if-eq v0, v2, :cond_15

    const/16 v2, 0xb

    if-eq v0, v2, :cond_15

    const/16 v2, 0xc

    if-eq v0, v2, :cond_15

    const/16 v2, 0xd

    if-eq v0, v2, :cond_15

    const/16 v2, 0xe

    if-eq v0, v2, :cond_15

    const/16 v2, 0xf

    if-eq v0, v2, :cond_15

    const/16 v2, 0x10

    if-eq v0, v2, :cond_15

    const/16 v2, 0x11

    if-eq v0, v2, :cond_15

    const/16 v2, 0x12

    if-eq v0, v2, :cond_15

    const/16 v2, 0x13

    if-eq v0, v2, :cond_15

    const/16 v2, 0x14

    if-ne v0, v2, :cond_16

    .line 535
    :cond_15
    iput v0, p0, Loyv;->f:I

    goto/16 :goto_0

    .line 537
    :cond_16
    iput v4, p0, Loyv;->f:I

    goto/16 :goto_0

    .line 542
    :sswitch_16
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyv;->w:Ljava/lang/String;

    goto/16 :goto_0

    .line 546
    :sswitch_17
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyv;->x:Ljava/lang/String;

    goto/16 :goto_0

    .line 550
    :sswitch_18
    iget-object v0, p0, Loyv;->y:Loya;

    if-nez v0, :cond_17

    .line 551
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    iput-object v0, p0, Loyv;->y:Loya;

    .line 553
    :cond_17
    iget-object v0, p0, Loyv;->y:Loya;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 557
    :sswitch_19
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyv;->z:Ljava/lang/String;

    goto/16 :goto_0

    .line 561
    :sswitch_1a
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyv;->A:Ljava/lang/String;

    goto/16 :goto_0

    .line 565
    :sswitch_1b
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyv;->B:Ljava/lang/String;

    goto/16 :goto_0

    .line 569
    :sswitch_1c
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyv;->C:Ljava/lang/String;

    goto/16 :goto_0

    .line 573
    :sswitch_1d
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyv;->D:Ljava/lang/String;

    goto/16 :goto_0

    .line 342
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x92 -> :sswitch_d
        0x152 -> :sswitch_e
        0x208 -> :sswitch_f
        0x25a -> :sswitch_10
        0x292 -> :sswitch_11
        0x29a -> :sswitch_12
        0x2d0 -> :sswitch_13
        0x302 -> :sswitch_14
        0x370 -> :sswitch_15
        0x37a -> :sswitch_16
        0x382 -> :sswitch_17
        0x5ca -> :sswitch_18
        0x5e2 -> :sswitch_19
        0x5ea -> :sswitch_1a
        0x5f2 -> :sswitch_1b
        0x5fa -> :sswitch_1c
        0x7f2 -> :sswitch_1d
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 7

    .prologue
    const/high16 v6, -0x80000000

    const/4 v0, 0x0

    .line 89
    iget-object v1, p0, Loyv;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 90
    const/4 v1, 0x1

    iget-object v2, p0, Loyv;->b:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 92
    :cond_0
    iget-object v1, p0, Loyv;->g:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 93
    const/4 v1, 0x2

    iget-object v2, p0, Loyv;->g:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 95
    :cond_1
    iget-object v1, p0, Loyv;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 96
    const/4 v1, 0x3

    iget-object v2, p0, Loyv;->c:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 98
    :cond_2
    iget-object v1, p0, Loyv;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 99
    const/4 v1, 0x4

    iget-object v2, p0, Loyv;->d:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 101
    :cond_3
    iget-object v1, p0, Loyv;->h:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 102
    const/4 v1, 0x5

    iget-object v2, p0, Loyv;->h:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 104
    :cond_4
    iget-object v1, p0, Loyv;->e:Lpdi;

    if-eqz v1, :cond_5

    .line 105
    const/4 v1, 0x6

    iget-object v2, p0, Loyv;->e:Lpdi;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 107
    :cond_5
    iget-object v1, p0, Loyv;->i:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 108
    const/4 v1, 0x7

    iget-object v2, p0, Loyv;->i:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 110
    :cond_6
    iget-object v1, p0, Loyv;->j:[Loya;

    if-eqz v1, :cond_8

    .line 111
    iget-object v2, p0, Loyv;->j:[Loya;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_8

    aget-object v4, v2, v1

    .line 112
    if-eqz v4, :cond_7

    .line 113
    const/16 v5, 0x8

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 111
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 117
    :cond_8
    iget-object v1, p0, Loyv;->k:Loya;

    if-eqz v1, :cond_9

    .line 118
    const/16 v1, 0x9

    iget-object v2, p0, Loyv;->k:Loya;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 120
    :cond_9
    iget-object v1, p0, Loyv;->l:Ljava/lang/String;

    if-eqz v1, :cond_a

    .line 121
    const/16 v1, 0xa

    iget-object v2, p0, Loyv;->l:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 123
    :cond_a
    iget-object v1, p0, Loyv;->m:[Loya;

    if-eqz v1, :cond_c

    .line 124
    iget-object v2, p0, Loyv;->m:[Loya;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_c

    aget-object v4, v2, v1

    .line 125
    if-eqz v4, :cond_b

    .line 126
    const/16 v5, 0xb

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 124
    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 130
    :cond_c
    iget-object v1, p0, Loyv;->n:Ljava/lang/String;

    if-eqz v1, :cond_d

    .line 131
    const/16 v1, 0xc

    iget-object v2, p0, Loyv;->n:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 133
    :cond_d
    iget-object v1, p0, Loyv;->o:Loya;

    if-eqz v1, :cond_e

    .line 134
    const/16 v1, 0x12

    iget-object v2, p0, Loyv;->o:Loya;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 136
    :cond_e
    iget-object v1, p0, Loyv;->p:[Loya;

    if-eqz v1, :cond_10

    .line 137
    iget-object v2, p0, Loyv;->p:[Loya;

    array-length v3, v2

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_10

    aget-object v4, v2, v1

    .line 138
    if-eqz v4, :cond_f

    .line 139
    const/16 v5, 0x2a

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 137
    :cond_f
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 143
    :cond_10
    iget-object v1, p0, Loyv;->q:Ljava/lang/Boolean;

    if-eqz v1, :cond_11

    .line 144
    const/16 v1, 0x41

    iget-object v2, p0, Loyv;->q:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IZ)V

    .line 146
    :cond_11
    iget-object v1, p0, Loyv;->r:Ljava/lang/String;

    if-eqz v1, :cond_12

    .line 147
    const/16 v1, 0x4b

    iget-object v2, p0, Loyv;->r:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 149
    :cond_12
    iget-object v1, p0, Loyv;->s:Loya;

    if-eqz v1, :cond_13

    .line 150
    const/16 v1, 0x52

    iget-object v2, p0, Loyv;->s:Loya;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 152
    :cond_13
    iget-object v1, p0, Loyv;->t:[Loya;

    if-eqz v1, :cond_15

    .line 153
    iget-object v1, p0, Loyv;->t:[Loya;

    array-length v2, v1

    :goto_3
    if-ge v0, v2, :cond_15

    aget-object v3, v1, v0

    .line 154
    if-eqz v3, :cond_14

    .line 155
    const/16 v4, 0x53

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 153
    :cond_14
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 159
    :cond_15
    iget v0, p0, Loyv;->u:I

    if-eq v0, v6, :cond_16

    .line 160
    const/16 v0, 0x5a

    iget v1, p0, Loyv;->u:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 162
    :cond_16
    iget-object v0, p0, Loyv;->v:Loya;

    if-eqz v0, :cond_17

    .line 163
    const/16 v0, 0x60

    iget-object v1, p0, Loyv;->v:Loya;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 165
    :cond_17
    iget v0, p0, Loyv;->f:I

    if-eq v0, v6, :cond_18

    .line 166
    const/16 v0, 0x6e

    iget v1, p0, Loyv;->f:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 168
    :cond_18
    iget-object v0, p0, Loyv;->w:Ljava/lang/String;

    if-eqz v0, :cond_19

    .line 169
    const/16 v0, 0x6f

    iget-object v1, p0, Loyv;->w:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 171
    :cond_19
    iget-object v0, p0, Loyv;->x:Ljava/lang/String;

    if-eqz v0, :cond_1a

    .line 172
    const/16 v0, 0x70

    iget-object v1, p0, Loyv;->x:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 174
    :cond_1a
    iget-object v0, p0, Loyv;->y:Loya;

    if-eqz v0, :cond_1b

    .line 175
    const/16 v0, 0xb9

    iget-object v1, p0, Loyv;->y:Loya;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 177
    :cond_1b
    iget-object v0, p0, Loyv;->z:Ljava/lang/String;

    if-eqz v0, :cond_1c

    .line 178
    const/16 v0, 0xbc

    iget-object v1, p0, Loyv;->z:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 180
    :cond_1c
    iget-object v0, p0, Loyv;->A:Ljava/lang/String;

    if-eqz v0, :cond_1d

    .line 181
    const/16 v0, 0xbd

    iget-object v1, p0, Loyv;->A:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 183
    :cond_1d
    iget-object v0, p0, Loyv;->B:Ljava/lang/String;

    if-eqz v0, :cond_1e

    .line 184
    const/16 v0, 0xbe

    iget-object v1, p0, Loyv;->B:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 186
    :cond_1e
    iget-object v0, p0, Loyv;->C:Ljava/lang/String;

    if-eqz v0, :cond_1f

    .line 187
    const/16 v0, 0xbf

    iget-object v1, p0, Loyv;->C:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 189
    :cond_1f
    iget-object v0, p0, Loyv;->D:Ljava/lang/String;

    if-eqz v0, :cond_20

    .line 190
    const/16 v0, 0xfe

    iget-object v1, p0, Loyv;->D:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 192
    :cond_20
    iget-object v0, p0, Loyv;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 194
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Loyv;->a(Loxn;)Loyv;

    move-result-object v0

    return-object v0
.end method
