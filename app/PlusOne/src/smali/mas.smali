.class public final Lmas;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmqp;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 17737
    invoke-direct {p0}, Loxq;-><init>()V

    .line 17740
    iput-object v0, p0, Lmas;->apiHeader:Llyq;

    .line 17743
    iput-object v0, p0, Lmas;->a:Lmqp;

    .line 17737
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 17760
    const/4 v0, 0x0

    .line 17761
    iget-object v1, p0, Lmas;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 17762
    const/4 v0, 0x1

    iget-object v1, p0, Lmas;->apiHeader:Llyq;

    .line 17763
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 17765
    :cond_0
    iget-object v1, p0, Lmas;->a:Lmqp;

    if-eqz v1, :cond_1

    .line 17766
    const/4 v1, 0x2

    iget-object v2, p0, Lmas;->a:Lmqp;

    .line 17767
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 17769
    :cond_1
    iget-object v1, p0, Lmas;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 17770
    iput v0, p0, Lmas;->ai:I

    .line 17771
    return v0
.end method

.method public a(Loxn;)Lmas;
    .locals 2

    .prologue
    .line 17779
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 17780
    sparse-switch v0, :sswitch_data_0

    .line 17784
    iget-object v1, p0, Lmas;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 17785
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmas;->ah:Ljava/util/List;

    .line 17788
    :cond_1
    iget-object v1, p0, Lmas;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 17790
    :sswitch_0
    return-object p0

    .line 17795
    :sswitch_1
    iget-object v0, p0, Lmas;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 17796
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmas;->apiHeader:Llyq;

    .line 17798
    :cond_2
    iget-object v0, p0, Lmas;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 17802
    :sswitch_2
    iget-object v0, p0, Lmas;->a:Lmqp;

    if-nez v0, :cond_3

    .line 17803
    new-instance v0, Lmqp;

    invoke-direct {v0}, Lmqp;-><init>()V

    iput-object v0, p0, Lmas;->a:Lmqp;

    .line 17805
    :cond_3
    iget-object v0, p0, Lmas;->a:Lmqp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 17780
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 17748
    iget-object v0, p0, Lmas;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 17749
    const/4 v0, 0x1

    iget-object v1, p0, Lmas;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 17751
    :cond_0
    iget-object v0, p0, Lmas;->a:Lmqp;

    if-eqz v0, :cond_1

    .line 17752
    const/4 v0, 0x2

    iget-object v1, p0, Lmas;->a:Lmqp;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 17754
    :cond_1
    iget-object v0, p0, Lmas;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 17756
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 17733
    invoke-virtual {p0, p1}, Lmas;->a(Loxn;)Lmas;

    move-result-object v0

    return-object v0
.end method
