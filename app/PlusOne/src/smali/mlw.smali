.class public final Lmlw;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmlh;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 221
    invoke-direct {p0}, Loxq;-><init>()V

    .line 224
    const/4 v0, 0x0

    iput-object v0, p0, Lmlw;->a:Lmlh;

    .line 221
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 248
    const/4 v0, 0x0

    .line 249
    iget-object v1, p0, Lmlw;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 250
    const/4 v0, 0x1

    iget-object v1, p0, Lmlw;->b:Ljava/lang/String;

    .line 251
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 253
    :cond_0
    iget-object v1, p0, Lmlw;->a:Lmlh;

    if-eqz v1, :cond_1

    .line 254
    const/16 v1, 0x8

    iget-object v2, p0, Lmlw;->a:Lmlh;

    .line 255
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 257
    :cond_1
    iget-object v1, p0, Lmlw;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 258
    const/16 v1, 0x9

    iget-object v2, p0, Lmlw;->c:Ljava/lang/String;

    .line 259
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 261
    :cond_2
    iget-object v1, p0, Lmlw;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 262
    iput v0, p0, Lmlw;->ai:I

    .line 263
    return v0
.end method

.method public a(Loxn;)Lmlw;
    .locals 2

    .prologue
    .line 271
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 272
    sparse-switch v0, :sswitch_data_0

    .line 276
    iget-object v1, p0, Lmlw;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 277
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmlw;->ah:Ljava/util/List;

    .line 280
    :cond_1
    iget-object v1, p0, Lmlw;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 282
    :sswitch_0
    return-object p0

    .line 287
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmlw;->b:Ljava/lang/String;

    goto :goto_0

    .line 291
    :sswitch_2
    iget-object v0, p0, Lmlw;->a:Lmlh;

    if-nez v0, :cond_2

    .line 292
    new-instance v0, Lmlh;

    invoke-direct {v0}, Lmlh;-><init>()V

    iput-object v0, p0, Lmlw;->a:Lmlh;

    .line 294
    :cond_2
    iget-object v0, p0, Lmlw;->a:Lmlh;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 298
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmlw;->c:Ljava/lang/String;

    goto :goto_0

    .line 272
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x42 -> :sswitch_2
        0x4a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 233
    iget-object v0, p0, Lmlw;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 234
    const/4 v0, 0x1

    iget-object v1, p0, Lmlw;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 236
    :cond_0
    iget-object v0, p0, Lmlw;->a:Lmlh;

    if-eqz v0, :cond_1

    .line 237
    const/16 v0, 0x8

    iget-object v1, p0, Lmlw;->a:Lmlh;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 239
    :cond_1
    iget-object v0, p0, Lmlw;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 240
    const/16 v0, 0x9

    iget-object v1, p0, Lmlw;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 242
    :cond_2
    iget-object v0, p0, Lmlw;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 244
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 217
    invoke-virtual {p0, p1}, Lmlw;->a(Loxn;)Lmlw;

    move-result-object v0

    return-object v0
.end method
