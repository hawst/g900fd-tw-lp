.class public final Lnsl;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnsl;


# instance fields
.field private b:I

.field private c:Lnsp;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1736
    const/4 v0, 0x0

    new-array v0, v0, [Lnsl;

    sput-object v0, Lnsl;->a:[Lnsl;

    .line 1740
    const v0, 0x2f2ecd2

    new-instance v1, Lnsm;

    invoke-direct {v1}, Lnsm;-><init>()V

    .line 1741
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    .line 1740
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1737
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1744
    const/high16 v0, -0x80000000

    iput v0, p0, Lnsl;->b:I

    .line 1747
    const/4 v0, 0x0

    iput-object v0, p0, Lnsl;->c:Lnsp;

    .line 1737
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1764
    const/4 v0, 0x0

    .line 1765
    iget v1, p0, Lnsl;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 1766
    const/4 v0, 0x1

    iget v1, p0, Lnsl;->b:I

    .line 1767
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1769
    :cond_0
    iget-object v1, p0, Lnsl;->c:Lnsp;

    if-eqz v1, :cond_1

    .line 1770
    const/4 v1, 0x2

    iget-object v2, p0, Lnsl;->c:Lnsp;

    .line 1771
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1773
    :cond_1
    iget-object v1, p0, Lnsl;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1774
    iput v0, p0, Lnsl;->ai:I

    .line 1775
    return v0
.end method

.method public a(Loxn;)Lnsl;
    .locals 2

    .prologue
    .line 1783
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1784
    sparse-switch v0, :sswitch_data_0

    .line 1788
    iget-object v1, p0, Lnsl;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1789
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnsl;->ah:Ljava/util/List;

    .line 1792
    :cond_1
    iget-object v1, p0, Lnsl;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1794
    :sswitch_0
    return-object p0

    .line 1799
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1800
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 1802
    :cond_2
    iput v0, p0, Lnsl;->b:I

    goto :goto_0

    .line 1804
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lnsl;->b:I

    goto :goto_0

    .line 1809
    :sswitch_2
    iget-object v0, p0, Lnsl;->c:Lnsp;

    if-nez v0, :cond_4

    .line 1810
    new-instance v0, Lnsp;

    invoke-direct {v0}, Lnsp;-><init>()V

    iput-object v0, p0, Lnsl;->c:Lnsp;

    .line 1812
    :cond_4
    iget-object v0, p0, Lnsl;->c:Lnsp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1784
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1752
    iget v0, p0, Lnsl;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 1753
    const/4 v0, 0x1

    iget v1, p0, Lnsl;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1755
    :cond_0
    iget-object v0, p0, Lnsl;->c:Lnsp;

    if-eqz v0, :cond_1

    .line 1756
    const/4 v0, 0x2

    iget-object v1, p0, Lnsl;->c:Lnsp;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1758
    :cond_1
    iget-object v0, p0, Lnsl;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1760
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1733
    invoke-virtual {p0, p1}, Lnsl;->a(Loxn;)Lnsl;

    move-result-object v0

    return-object v0
.end method
