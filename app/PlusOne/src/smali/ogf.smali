.class public final Logf;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:[Logg;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8461
    invoke-direct {p0}, Loxq;-><init>()V

    .line 8539
    sget-object v0, Logg;->a:[Logg;

    iput-object v0, p0, Logf;->b:[Logg;

    .line 8461
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 8560
    .line 8561
    iget-object v0, p0, Logf;->a:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 8562
    const/4 v0, 0x1

    iget-object v2, p0, Logf;->a:Ljava/lang/String;

    .line 8563
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 8565
    :goto_0
    iget-object v2, p0, Logf;->b:[Logg;

    if-eqz v2, :cond_1

    .line 8566
    iget-object v2, p0, Logf;->b:[Logg;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 8567
    if-eqz v4, :cond_0

    .line 8568
    const/4 v5, 0x2

    .line 8569
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 8566
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 8573
    :cond_1
    iget-object v1, p0, Logf;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8574
    iput v0, p0, Logf;->ai:I

    .line 8575
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Logf;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 8583
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 8584
    sparse-switch v0, :sswitch_data_0

    .line 8588
    iget-object v2, p0, Logf;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 8589
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Logf;->ah:Ljava/util/List;

    .line 8592
    :cond_1
    iget-object v2, p0, Logf;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 8594
    :sswitch_0
    return-object p0

    .line 8599
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Logf;->a:Ljava/lang/String;

    goto :goto_0

    .line 8603
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 8604
    iget-object v0, p0, Logf;->b:[Logg;

    if-nez v0, :cond_3

    move v0, v1

    .line 8605
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Logg;

    .line 8606
    iget-object v3, p0, Logf;->b:[Logg;

    if-eqz v3, :cond_2

    .line 8607
    iget-object v3, p0, Logf;->b:[Logg;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 8609
    :cond_2
    iput-object v2, p0, Logf;->b:[Logg;

    .line 8610
    :goto_2
    iget-object v2, p0, Logf;->b:[Logg;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 8611
    iget-object v2, p0, Logf;->b:[Logg;

    new-instance v3, Logg;

    invoke-direct {v3}, Logg;-><init>()V

    aput-object v3, v2, v0

    .line 8612
    iget-object v2, p0, Logf;->b:[Logg;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 8613
    invoke-virtual {p1}, Loxn;->a()I

    .line 8610
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 8604
    :cond_3
    iget-object v0, p0, Logf;->b:[Logg;

    array-length v0, v0

    goto :goto_1

    .line 8616
    :cond_4
    iget-object v2, p0, Logf;->b:[Logg;

    new-instance v3, Logg;

    invoke-direct {v3}, Logg;-><init>()V

    aput-object v3, v2, v0

    .line 8617
    iget-object v2, p0, Logf;->b:[Logg;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 8584
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 8544
    iget-object v0, p0, Logf;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 8545
    const/4 v0, 0x1

    iget-object v1, p0, Logf;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 8547
    :cond_0
    iget-object v0, p0, Logf;->b:[Logg;

    if-eqz v0, :cond_2

    .line 8548
    iget-object v1, p0, Logf;->b:[Logg;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 8549
    if-eqz v3, :cond_1

    .line 8550
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 8548
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 8554
    :cond_2
    iget-object v0, p0, Logf;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 8556
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 8457
    invoke-virtual {p0, p1}, Logf;->a(Loxn;)Logf;

    move-result-object v0

    return-object v0
.end method
