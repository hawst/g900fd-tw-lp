.class public final Lepz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lejk;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)[Landroid/net/Uri;
    .locals 6

    .prologue
    .line 24
    const-class v0, Lctz;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctz;

    invoke-virtual {v0}, Lctz;->a()Ljcn;

    move-result-object v0

    .line 25
    if-eqz p1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljcn;->k()I

    move-result v1

    if-nez v1, :cond_1

    .line 26
    :cond_0
    const/4 v0, 0x0

    .line 47
    :goto_0
    return-object v0

    .line 28
    :cond_1
    const-class v1, Ljuf;

    invoke-virtual {v0, v1}, Ljcn;->a(Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v3

    .line 29
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 30
    new-array v1, v0, [Landroid/net/Uri;

    .line 31
    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_1
    if-ltz v2, :cond_4

    .line 32
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljuf;

    .line 33
    invoke-interface {v0}, Ljuf;->f()Lizu;

    move-result-object v0

    .line 34
    invoke-virtual {v0}, Lizu;->i()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 35
    invoke-virtual {v0}, Lizu;->e()Landroid/net/Uri;

    move-result-object v0

    aput-object v0, v1, v2

    .line 31
    :cond_2
    :goto_2
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_1

    .line 36
    :cond_3
    invoke-virtual {v0}, Lizu;->h()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 37
    invoke-virtual {v0}, Lizu;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lizu;->g()Ljac;

    move-result-object v0

    invoke-static {p1, v4, v0}, Lcom/google/android/apps/photos/content/GooglePhotosImageProvider;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Landroid/net/Uri;

    move-result-object v0

    .line 42
    const-string v4, "com.android.bluetooth"

    const/4 v5, 0x1

    invoke-virtual {p1, v4, v0, v5}, Landroid/content/Context;->grantUriPermission(Ljava/lang/String;Landroid/net/Uri;I)V

    .line 44
    aput-object v0, v1, v2

    goto :goto_2

    :cond_4
    move-object v0, v1

    .line 47
    goto :goto_0
.end method
