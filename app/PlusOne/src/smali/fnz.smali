.class public final Lfnz;
.super Lllq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:I


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Lllq;-><init>()V

    .line 31
    const-string v0, ""

    iput-object v0, p0, Lfnz;->d:Ljava/lang/String;

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lpda;Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v4, 0x0

    .line 38
    invoke-direct {p0}, Lllq;-><init>()V

    .line 31
    const-string v0, ""

    iput-object v0, p0, Lfnz;->d:Ljava/lang/String;

    .line 39
    iget-object v0, p2, Lpda;->b:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p2, Lpda;->b:Ljava/lang/String;

    :goto_0
    iput-object v0, p0, Lfnz;->a:Ljava/lang/String;

    .line 40
    iget-object v0, p2, Lpda;->d:[Loya;

    if-eqz v0, :cond_0

    iget-object v0, p2, Lpda;->d:[Loya;

    array-length v0, v0

    if-lez v0, :cond_0

    .line 41
    iget-object v0, p2, Lpda;->d:[Loya;

    aget-object v0, v0, v4

    sget-object v1, Lozv;->a:Loxr;

    invoke-virtual {v0, v1}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lozv;

    iget-object v0, v0, Lozv;->b:Ljava/lang/String;

    iput-object v0, p0, Lfnz;->b:Ljava/lang/String;

    .line 43
    :cond_0
    iput-object p3, p0, Lfnz;->c:Ljava/lang/String;

    .line 45
    const-string v0, ""

    .line 46
    iget-object v1, p2, Lpda;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 47
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyy-MM-dd\'T\'HH:mm:ss\'Z\'"

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 49
    :try_start_0
    iget-object v2, p2, Lpda;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    .line 50
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 51
    invoke-virtual {v2, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 54
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    const/4 v1, 0x1

    .line 55
    invoke-virtual {v2, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    if-ne v1, v2, :cond_3

    const v1, 0x10018

    .line 52
    :goto_1
    invoke-static {p1, v6, v7, v1}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    move-object v1, v0

    .line 64
    :goto_2
    const-string v0, ""

    .line 65
    iget-object v2, p2, Lpda;->f:Ljava/lang/Integer;

    if-eqz v2, :cond_8

    .line 66
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f110006

    iget-object v3, p2, Lpda;->f:Ljava/lang/Integer;

    .line 67
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 66
    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v0

    new-array v2, v8, [Ljava/lang/Object;

    iget-object v3, p2, Lpda;->f:Ljava/lang/Integer;

    aput-object v3, v2, v4

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 70
    :goto_3
    const-string v0, ""

    .line 71
    iget-object v3, p2, Lpda;->g:Ljava/lang/Integer;

    if-eqz v3, :cond_7

    .line 72
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f110007

    iget-object v5, p2, Lpda;->g:Ljava/lang/Integer;

    .line 73
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 72
    invoke-virtual {v0, v3, v5}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v0

    new-array v3, v8, [Ljava/lang/Object;

    iget-object v5, p2, Lpda;->g:Ljava/lang/Integer;

    aput-object v5, v3, v4

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 76
    :goto_4
    iget-object v0, p2, Lpda;->c:[Loya;

    if-eqz v0, :cond_1

    iget-object v0, p2, Lpda;->c:[Loya;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 77
    iget-object v0, p2, Lpda;->c:[Loya;

    aget-object v0, v0, v4

    sget-object v5, Lpai;->a:Loxr;

    invoke-virtual {v0, v5}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpai;

    iget-object v0, v0, Lpai;->d:Ljava/lang/String;

    iput-object v0, p0, Lfnz;->e:Ljava/lang/String;

    .line 80
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x0

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfnz;->d:Ljava/lang/String;

    .line 82
    iget-object v0, p2, Lpda;->h:Ljava/lang/String;

    if-eqz v0, :cond_6

    const-string v1, "#"

    iget-object v0, p2, Lpda;->h:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 83
    :goto_5
    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    :goto_6
    iput v0, p0, Lfnz;->f:I

    .line 84
    return-void

    .line 39
    :cond_2
    const-string v0, ""

    goto/16 :goto_0

    .line 55
    :cond_3
    const v1, 0x10010

    goto/16 :goto_1

    :catch_0
    move-exception v1

    :cond_4
    move-object v1, v0

    goto/16 :goto_2

    .line 82
    :cond_5
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_5

    :cond_6
    move v0, v4

    .line 83
    goto :goto_6

    :cond_7
    move-object v3, v0

    goto :goto_4

    :cond_8
    move-object v2, v0

    goto/16 :goto_3
.end method

.method public static a([B)Lfnz;
    .locals 3

    .prologue
    .line 109
    if-nez p0, :cond_0

    .line 110
    const/4 v0, 0x0

    .line 123
    :goto_0
    return-object v0

    .line 113
    :cond_0
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 114
    new-instance v0, Lfnz;

    invoke-direct {v0}, Lfnz;-><init>()V

    .line 116
    invoke-static {v1}, Lfnz;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lfnz;->a:Ljava/lang/String;

    .line 117
    invoke-static {v1}, Lfnz;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lfnz;->b:Ljava/lang/String;

    .line 118
    invoke-static {v1}, Lfnz;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lfnz;->c:Ljava/lang/String;

    .line 119
    invoke-static {v1}, Lfnz;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lfnz;->d:Ljava/lang/String;

    .line 120
    invoke-static {v1}, Lfnz;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lfnz;->e:Ljava/lang/String;

    .line 121
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v1

    iput v1, v0, Lfnz;->f:I

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lfnz;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lfnz;->b:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lfnz;->c:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lfnz;->d:Ljava/lang/String;

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lfnz;->e:Ljava/lang/String;

    return-object v0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 147
    iget v0, p0, Lfnz;->f:I

    return v0
.end method
