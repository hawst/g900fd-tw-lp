.class public final Lfwb;
.super Landroid/view/SurfaceView;
.source "PG"

# interfaces
.implements Lkdd;


# static fields
.field private static g:Lizs;


# instance fields
.field private a:Landroid/view/SurfaceHolder;

.field private b:Ljava/lang/String;

.field private c:Landroid/media/MediaPlayer;

.field private d:Landroid/media/MediaPlayer$OnPreparedListener;

.field private e:Landroid/media/MediaPlayer$OnErrorListener;

.field private f:Lkda;

.field private h:Lizu;

.field private i:Landroid/view/SurfaceHolder$Callback;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 68
    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    .line 39
    new-instance v0, Lfwc;

    invoke-direct {v0, p0}, Lfwc;-><init>(Lfwb;)V

    iput-object v0, p0, Lfwb;->i:Landroid/view/SurfaceHolder$Callback;

    .line 69
    invoke-virtual {p0}, Lfwb;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iget-object v1, p0, Lfwb;->i:Landroid/view/SurfaceHolder$Callback;

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    invoke-virtual {p0}, Lfwb;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setType(I)V

    sget-object v0, Lfwb;->g:Lizs;

    if-nez v0, :cond_0

    const-class v0, Lizs;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizs;

    sput-object v0, Lfwb;->g:Lizs;

    .line 70
    :cond_0
    return-void
.end method

.method static synthetic a(Lfwb;Landroid/view/SurfaceHolder;)Landroid/view/SurfaceHolder;
    .locals 0

    .prologue
    .line 28
    iput-object p1, p0, Lfwb;->a:Landroid/view/SurfaceHolder;

    return-object p1
.end method

.method static synthetic a(Lfwb;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lfwb;->f()V

    return-void
.end method

.method static synthetic b(Lfwb;)Landroid/view/SurfaceHolder;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lfwb;->a:Landroid/view/SurfaceHolder;

    return-object v0
.end method

.method static synthetic c(Lfwb;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lfwb;->e()V

    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 101
    iget-object v0, p0, Lfwb;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfwb;->a:Landroid/view/SurfaceHolder;

    if-nez v0, :cond_1

    .line 116
    :cond_0
    :goto_0
    return-void

    .line 104
    :cond_1
    invoke-direct {p0}, Lfwb;->f()V

    .line 107
    :try_start_0
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lfwb;->c:Landroid/media/MediaPlayer;

    .line 108
    iget-object v0, p0, Lfwb;->c:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lfwb;->d:Landroid/media/MediaPlayer$OnPreparedListener;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 109
    iget-object v0, p0, Lfwb;->c:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lfwb;->e:Landroid/media/MediaPlayer$OnErrorListener;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 110
    iget-object v0, p0, Lfwb;->c:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lfwb;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    .line 111
    iget-object v0, p0, Lfwb;->c:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lfwb;->a:Landroid/view/SurfaceHolder;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    .line 112
    iget-object v0, p0, Lfwb;->c:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepareAsync()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 114
    :catch_0
    move-exception v0

    invoke-direct {p0}, Lfwb;->f()V

    goto :goto_0
.end method

.method private f()V
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lfwb;->c:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lfwb;->c:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    .line 121
    iget-object v0, p0, Lfwb;->c:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 122
    const/4 v0, 0x0

    iput-object v0, p0, Lfwb;->c:Landroid/media/MediaPlayer;

    .line 124
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 203
    invoke-direct {p0}, Lfwb;->f()V

    .line 204
    return-void
.end method

.method public a(Landroid/media/MediaPlayer$OnErrorListener;)V
    .locals 2

    .prologue
    .line 143
    iput-object p1, p0, Lfwb;->e:Landroid/media/MediaPlayer$OnErrorListener;

    .line 145
    iget-object v0, p0, Lfwb;->c:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Lfwb;->c:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lfwb;->e:Landroid/media/MediaPlayer$OnErrorListener;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 148
    :cond_0
    return-void
.end method

.method public a(Landroid/media/MediaPlayer$OnPreparedListener;)V
    .locals 2

    .prologue
    .line 131
    iput-object p1, p0, Lfwb;->d:Landroid/media/MediaPlayer$OnPreparedListener;

    .line 133
    iget-object v0, p0, Lfwb;->c:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 134
    iget-object v0, p0, Lfwb;->c:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lfwb;->d:Landroid/media/MediaPlayer$OnPreparedListener;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 136
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 155
    invoke-virtual {p0}, Lfwb;->c()V

    .line 156
    invoke-virtual {p0}, Lfwb;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Ljac;->b:Ljac;

    invoke-static {v0, p1, v1}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v0

    iput-object v0, p0, Lfwb;->h:Lizu;

    .line 157
    invoke-virtual {p0}, Lfwb;->b()V

    .line 158
    return-void
.end method

.method public a(Lkda;)V
    .locals 2

    .prologue
    .line 191
    iget-object v0, p0, Lfwb;->f:Lkda;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lfwb;->f:Lkda;

    invoke-virtual {v0}, Lkda;->getStatus()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 192
    iget-object v0, p0, Lfwb;->f:Lkda;

    invoke-virtual {v0}, Lkda;->getCachedFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfwb;->b:Ljava/lang/String;

    .line 193
    invoke-direct {p0}, Lfwb;->e()V

    .line 194
    invoke-virtual {p0}, Lfwb;->requestLayout()V

    .line 195
    invoke-virtual {p0}, Lfwb;->invalidate()V

    .line 197
    :cond_0
    return-void
.end method

.method public b()V
    .locals 4

    .prologue
    .line 174
    iget-object v0, p0, Lfwb;->h:Lizu;

    if-eqz v0, :cond_0

    .line 175
    sget-object v0, Lfwb;->g:Lizs;

    iget-object v1, p0, Lfwb;->h:Lizu;

    const/4 v2, 0x4

    const/16 v3, 0x22

    invoke-virtual {v0, v1, v2, v3, p0}, Lizs;->a(Lizu;IILkdd;)Lcom/google/android/libraries/social/media/MediaResource;

    move-result-object v0

    iput-object v0, p0, Lfwb;->f:Lkda;

    .line 179
    :cond_0
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lfwb;->f:Lkda;

    if-eqz v0, :cond_0

    .line 184
    iget-object v0, p0, Lfwb;->f:Lkda;

    invoke-virtual {v0, p0}, Lkda;->unregister(Lkdd;)V

    .line 185
    const/4 v0, 0x0

    iput-object v0, p0, Lfwb;->f:Lkda;

    .line 187
    :cond_0
    return-void
.end method

.method public d()V
    .locals 0

    .prologue
    .line 210
    invoke-direct {p0}, Lfwb;->e()V

    .line 211
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 0

    .prologue
    .line 162
    invoke-super {p0}, Landroid/view/SurfaceView;->onAttachedToWindow()V

    .line 163
    invoke-virtual {p0}, Lfwb;->b()V

    .line 164
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 168
    invoke-super {p0}, Landroid/view/SurfaceView;->onDetachedFromWindow()V

    .line 169
    invoke-virtual {p0}, Lfwb;->c()V

    .line 170
    return-void
.end method
