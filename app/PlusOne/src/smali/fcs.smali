.class public final Lfcs;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lheg;
.implements Llnx;
.implements Llqz;
.implements Llrb;
.implements Llrc;
.implements Llrd;
.implements Llrg;


# instance fields
.field private final a:Landroid/app/Activity;

.field private b:Lhee;

.field private c:Z

.field private d:I

.field private e:Ljava/lang/String;

.field private f:J


# direct methods
.method public constructor <init>(Landroid/app/Activity;Llqr;)V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const/4 v0, 0x0

    iput v0, p0, Lfcs;->d:I

    .line 49
    iput-object p1, p0, Lfcs;->a:Landroid/app/Activity;

    .line 50
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 51
    return-void
.end method

.method private a()V
    .locals 11

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 119
    iget-object v2, p0, Lfcs;->b:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    .line 121
    iget-boolean v3, p0, Lfcs;->c:Z

    if-eqz v3, :cond_2

    iget-object v3, p0, Lfcs;->b:Lhee;

    .line 122
    invoke-interface {v3}, Lhee;->e()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lfcs;->e:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lfcs;->a:Landroid/app/Activity;

    .line 123
    invoke-virtual {v3}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    iget-object v4, p0, Lfcs;->e:Ljava/lang/String;

    invoke-virtual {v3, v4, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_0
    iget-object v3, p0, Lfcs;->a:Landroid/app/Activity;

    .line 124
    iget-wide v4, p0, Lfcs;->f:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-nez v4, :cond_3

    :cond_1
    :goto_0
    if-eqz v0, :cond_2

    iget-object v0, p0, Lfcs;->a:Landroid/app/Activity;

    .line 125
    invoke-static {v0, v2}, Lcom/google/android/apps/plus/phone/AddProfilePhotoSpringboardActivity;->a(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 126
    iget-object v0, p0, Lfcs;->a:Landroid/app/Activity;

    iget-object v1, p0, Lfcs;->a:Landroid/app/Activity;

    iget-object v2, p0, Lfcs;->b:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    iget v3, p0, Lfcs;->d:I

    new-instance v4, Landroid/content/Intent;

    const-class v5, Lcom/google/android/apps/plus/phone/AddProfilePhotoSpringboardActivity;

    invoke-direct {v4, v1, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "account_id"

    invoke-virtual {v4, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "springboard_launcher"

    invoke-virtual {v4, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {v0, v4}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 128
    :cond_2
    return-void

    .line 124
    :cond_3
    const-string v4, "com.google.android.plus.NOTIFICATIONS"

    invoke-virtual {v3, v4, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-string v6, "add_profile_photo_prompt_launcher_timestamp"

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0xb

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v3, v6, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    cmp-long v8, v6, v4

    if-nez v8, :cond_4

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v8, "add_profile_photo_prompt_launcher_timestamp"

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, 0xb

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v3, v8, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_4
    sub-long/2addr v4, v6

    iget-wide v6, p0, Lfcs;->f:J

    cmp-long v3, v4, v6

    if-gtz v3, :cond_1

    move v0, v1

    goto/16 :goto_0
.end method


# virtual methods
.method public a(I)Lfcs;
    .locals 0

    .prologue
    .line 57
    iput p1, p0, Lfcs;->d:I

    .line 58
    return-object p0
.end method

.method public a(J)Lfcs;
    .locals 1

    .prologue
    .line 75
    iput-wide p1, p0, Lfcs;->f:J

    .line 76
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lfcs;
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Lfcs;->e:Ljava/lang/String;

    .line 67
    return-object p0
.end method

.method public a(Landroid/content/Context;Llnh;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 97
    const-class v0, Lhee;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Lfcs;->b:Lhee;

    .line 98
    iget-object v0, p0, Lfcs;->b:Lhee;

    invoke-interface {v0, p0}, Lhee;->a(Lheg;)Lhee;

    .line 99
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 81
    if-eqz p1, :cond_0

    .line 82
    const-string v0, "springboard_type"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lfcs;->d:I

    .line 83
    const-string v0, "intent_trigger"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfcs;->e:Ljava/lang/String;

    .line 84
    const-string v0, "launch_delay"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lfcs;->f:J

    .line 86
    :cond_0
    return-void
.end method

.method public a(ZIIII)V
    .locals 0

    .prologue
    .line 115
    invoke-direct {p0}, Lfcs;->a()V

    .line 116
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 103
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfcs;->c:Z

    .line 104
    invoke-direct {p0}, Lfcs;->a()V

    .line 105
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 90
    const-string v0, "springboard_type"

    iget v1, p0, Lfcs;->d:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 91
    const-string v0, "intent_trigger"

    iget-object v1, p0, Lfcs;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    const-string v0, "launch_delay"

    iget-wide v2, p0, Lfcs;->f:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 93
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 109
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfcs;->c:Z

    .line 110
    return-void
.end method
