.class public final Lqah;
.super Loxq;
.source "PG"


# instance fields
.field private a:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1120
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1123
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lqah;->a:[Ljava/lang/String;

    .line 1120
    return-void
.end method


# virtual methods
.method public a()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1139
    .line 1140
    iget-object v1, p0, Lqah;->a:[Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lqah;->a:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_1

    .line 1142
    iget-object v2, p0, Lqah;->a:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 1144
    invoke-static {v4}, Loxo;->b(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v1, v4

    .line 1142
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1147
    :cond_0
    iget-object v0, p0, Lqah;->a:[Ljava/lang/String;

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v1

    .line 1149
    :cond_1
    iget-object v1, p0, Lqah;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1150
    iput v0, p0, Lqah;->ai:I

    .line 1151
    return v0
.end method

.method public a(Loxn;)Lqah;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1159
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1160
    sparse-switch v0, :sswitch_data_0

    .line 1164
    iget-object v1, p0, Lqah;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1165
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lqah;->ah:Ljava/util/List;

    .line 1168
    :cond_1
    iget-object v1, p0, Lqah;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1170
    :sswitch_0
    return-object p0

    .line 1175
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 1176
    iget-object v0, p0, Lqah;->a:[Ljava/lang/String;

    array-length v0, v0

    .line 1177
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    .line 1178
    iget-object v2, p0, Lqah;->a:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1179
    iput-object v1, p0, Lqah;->a:[Ljava/lang/String;

    .line 1180
    :goto_1
    iget-object v1, p0, Lqah;->a:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 1181
    iget-object v1, p0, Lqah;->a:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 1182
    invoke-virtual {p1}, Loxn;->a()I

    .line 1180
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1185
    :cond_2
    iget-object v1, p0, Lqah;->a:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    .line 1160
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 1128
    iget-object v0, p0, Lqah;->a:[Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1129
    iget-object v1, p0, Lqah;->a:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 1130
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->a(ILjava/lang/String;)V

    .line 1129
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1133
    :cond_0
    iget-object v0, p0, Lqah;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1135
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1116
    invoke-virtual {p0, p1}, Lqah;->a(Loxn;)Lqah;

    move-result-object v0

    return-object v0
.end method
