.class public final Lbrt;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/widget/SimpleCursorAdapter$ViewBinder;


# instance fields
.field private synthetic a:Lcom/google/android/apps/moviemaker/picker/PickerActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/moviemaker/picker/PickerActivity;)V
    .locals 0

    .prologue
    .line 273
    iput-object p1, p0, Lbrt;->a:Lcom/google/android/apps/moviemaker/picker/PickerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public setViewValue(Landroid/view/View;Landroid/database/Cursor;I)Z
    .locals 4

    .prologue
    .line 276
    move-object v0, p2

    check-cast v0, Lbro;

    .line 277
    check-cast p1, Lcom/google/android/apps/moviemaker/picker/PickerTileView;

    .line 279
    iget-object v1, p0, Lbrt;->a:Lcom/google/android/apps/moviemaker/picker/PickerActivity;

    invoke-static {v1}, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->c(Lcom/google/android/apps/moviemaker/picker/PickerActivity;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->a(Landroid/graphics/Bitmap;)V

    .line 280
    iget-object v1, p0, Lbrt;->a:Lcom/google/android/apps/moviemaker/picker/PickerActivity;

    .line 281
    invoke-static {v1}, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->d(Lcom/google/android/apps/moviemaker/picker/PickerActivity;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lbrt;->a:Lcom/google/android/apps/moviemaker/picker/PickerActivity;

    .line 282
    invoke-static {v2}, Lcom/google/android/apps/moviemaker/picker/PickerActivity;->e(Lcom/google/android/apps/moviemaker/picker/PickerActivity;)[Z

    move-result-object v2

    invoke-virtual {v0}, Lbro;->a()I

    move-result v0

    aget-boolean v0, v2, v0

    .line 283
    invoke-interface {p2, p3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 280
    invoke-virtual {p1, v1, v0, v2, v3}, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->a(Landroid/os/Handler;ZJ)V

    .line 285
    const/4 v0, 0x1

    return v0
.end method
