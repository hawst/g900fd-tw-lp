.class public final Lnai;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmzo;

.field public b:[I

.field public c:Lnab;

.field public d:Lnac;

.field public e:Lnaf;

.field public f:Lnal;

.field public g:Lnam;

.field public h:Ljava/lang/Boolean;

.field public i:Lnak;

.field private j:Lmzy;

.field private k:Lnad;

.field private l:[Lnae;

.field private m:[Lnag;

.field private n:Lnah;

.field private o:Lmzz;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 13
    iput-object v1, p0, Lnai;->a:Lmzo;

    .line 16
    sget-object v0, Loxx;->a:[I

    iput-object v0, p0, Lnai;->b:[I

    .line 19
    iput-object v1, p0, Lnai;->j:Lmzy;

    .line 22
    iput-object v1, p0, Lnai;->c:Lnab;

    .line 25
    iput-object v1, p0, Lnai;->d:Lnac;

    .line 28
    iput-object v1, p0, Lnai;->k:Lnad;

    .line 31
    sget-object v0, Lnae;->a:[Lnae;

    iput-object v0, p0, Lnai;->l:[Lnae;

    .line 34
    iput-object v1, p0, Lnai;->e:Lnaf;

    .line 37
    sget-object v0, Lnag;->a:[Lnag;

    iput-object v0, p0, Lnai;->m:[Lnag;

    .line 40
    iput-object v1, p0, Lnai;->n:Lnah;

    .line 43
    iput-object v1, p0, Lnai;->f:Lnal;

    .line 46
    iput-object v1, p0, Lnai;->g:Lnam;

    .line 49
    iput-object v1, p0, Lnai;->o:Lmzz;

    .line 54
    iput-object v1, p0, Lnai;->i:Lnak;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 120
    .line 121
    iget-object v0, p0, Lnai;->a:Lmzo;

    if-eqz v0, :cond_11

    .line 122
    const/4 v0, 0x1

    iget-object v2, p0, Lnai;->a:Lmzo;

    .line 123
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 125
    :goto_0
    iget-object v2, p0, Lnai;->b:[I

    if-eqz v2, :cond_1

    iget-object v2, p0, Lnai;->b:[I

    array-length v2, v2

    if-lez v2, :cond_1

    .line 127
    iget-object v4, p0, Lnai;->b:[I

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_1
    if-ge v2, v5, :cond_0

    aget v6, v4, v2

    .line 129
    invoke-static {v6}, Loxo;->i(I)I

    move-result v6

    add-int/2addr v3, v6

    .line 127
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 131
    :cond_0
    add-int/2addr v0, v3

    .line 132
    iget-object v2, p0, Lnai;->b:[I

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 134
    :cond_1
    iget-object v2, p0, Lnai;->j:Lmzy;

    if-eqz v2, :cond_2

    .line 135
    const/4 v2, 0x3

    iget-object v3, p0, Lnai;->j:Lmzy;

    .line 136
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 138
    :cond_2
    iget-object v2, p0, Lnai;->d:Lnac;

    if-eqz v2, :cond_3

    .line 139
    const/4 v2, 0x4

    iget-object v3, p0, Lnai;->d:Lnac;

    .line 140
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 142
    :cond_3
    iget-object v2, p0, Lnai;->k:Lnad;

    if-eqz v2, :cond_4

    .line 143
    const/4 v2, 0x5

    iget-object v3, p0, Lnai;->k:Lnad;

    .line 144
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 146
    :cond_4
    iget-object v2, p0, Lnai;->l:[Lnae;

    if-eqz v2, :cond_6

    .line 147
    iget-object v3, p0, Lnai;->l:[Lnae;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_6

    aget-object v5, v3, v2

    .line 148
    if-eqz v5, :cond_5

    .line 149
    const/4 v6, 0x7

    .line 150
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 147
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 154
    :cond_6
    iget-object v2, p0, Lnai;->e:Lnaf;

    if-eqz v2, :cond_7

    .line 155
    const/16 v2, 0x8

    iget-object v3, p0, Lnai;->e:Lnaf;

    .line 156
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 158
    :cond_7
    iget-object v2, p0, Lnai;->m:[Lnag;

    if-eqz v2, :cond_9

    .line 159
    iget-object v2, p0, Lnai;->m:[Lnag;

    array-length v3, v2

    :goto_3
    if-ge v1, v3, :cond_9

    aget-object v4, v2, v1

    .line 160
    if-eqz v4, :cond_8

    .line 161
    const/16 v5, 0x9

    .line 162
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 159
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 166
    :cond_9
    iget-object v1, p0, Lnai;->n:Lnah;

    if-eqz v1, :cond_a

    .line 167
    const/16 v1, 0xa

    iget-object v2, p0, Lnai;->n:Lnah;

    .line 168
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 170
    :cond_a
    iget-object v1, p0, Lnai;->f:Lnal;

    if-eqz v1, :cond_b

    .line 171
    const/16 v1, 0xb

    iget-object v2, p0, Lnai;->f:Lnal;

    .line 172
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 174
    :cond_b
    iget-object v1, p0, Lnai;->g:Lnam;

    if-eqz v1, :cond_c

    .line 175
    const/16 v1, 0xc

    iget-object v2, p0, Lnai;->g:Lnam;

    .line 176
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 178
    :cond_c
    iget-object v1, p0, Lnai;->c:Lnab;

    if-eqz v1, :cond_d

    .line 179
    const/16 v1, 0xd

    iget-object v2, p0, Lnai;->c:Lnab;

    .line 180
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 182
    :cond_d
    iget-object v1, p0, Lnai;->i:Lnak;

    if-eqz v1, :cond_e

    .line 183
    const/16 v1, 0xe

    iget-object v2, p0, Lnai;->i:Lnak;

    .line 184
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 186
    :cond_e
    iget-object v1, p0, Lnai;->o:Lmzz;

    if-eqz v1, :cond_f

    .line 187
    const/16 v1, 0xf

    iget-object v2, p0, Lnai;->o:Lmzz;

    .line 188
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 190
    :cond_f
    iget-object v1, p0, Lnai;->h:Ljava/lang/Boolean;

    if-eqz v1, :cond_10

    .line 191
    const/16 v1, 0x10

    iget-object v2, p0, Lnai;->h:Ljava/lang/Boolean;

    .line 192
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 194
    :cond_10
    iget-object v1, p0, Lnai;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 195
    iput v0, p0, Lnai;->ai:I

    .line 196
    return v0

    :cond_11
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lnai;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 204
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 205
    sparse-switch v0, :sswitch_data_0

    .line 209
    iget-object v2, p0, Lnai;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 210
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnai;->ah:Ljava/util/List;

    .line 213
    :cond_1
    iget-object v2, p0, Lnai;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 215
    :sswitch_0
    return-object p0

    .line 220
    :sswitch_1
    iget-object v0, p0, Lnai;->a:Lmzo;

    if-nez v0, :cond_2

    .line 221
    new-instance v0, Lmzo;

    invoke-direct {v0}, Lmzo;-><init>()V

    iput-object v0, p0, Lnai;->a:Lmzo;

    .line 223
    :cond_2
    iget-object v0, p0, Lnai;->a:Lmzo;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 227
    :sswitch_2
    const/16 v0, 0x10

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 228
    iget-object v0, p0, Lnai;->b:[I

    array-length v0, v0

    .line 229
    add-int/2addr v2, v0

    new-array v2, v2, [I

    .line 230
    iget-object v3, p0, Lnai;->b:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 231
    iput-object v2, p0, Lnai;->b:[I

    .line 232
    :goto_1
    iget-object v2, p0, Lnai;->b:[I

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_3

    .line 233
    iget-object v2, p0, Lnai;->b:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v3

    aput v3, v2, v0

    .line 234
    invoke-virtual {p1}, Loxn;->a()I

    .line 232
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 237
    :cond_3
    iget-object v2, p0, Lnai;->b:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v3

    aput v3, v2, v0

    goto :goto_0

    .line 241
    :sswitch_3
    iget-object v0, p0, Lnai;->j:Lmzy;

    if-nez v0, :cond_4

    .line 242
    new-instance v0, Lmzy;

    invoke-direct {v0}, Lmzy;-><init>()V

    iput-object v0, p0, Lnai;->j:Lmzy;

    .line 244
    :cond_4
    iget-object v0, p0, Lnai;->j:Lmzy;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 248
    :sswitch_4
    iget-object v0, p0, Lnai;->d:Lnac;

    if-nez v0, :cond_5

    .line 249
    new-instance v0, Lnac;

    invoke-direct {v0}, Lnac;-><init>()V

    iput-object v0, p0, Lnai;->d:Lnac;

    .line 251
    :cond_5
    iget-object v0, p0, Lnai;->d:Lnac;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 255
    :sswitch_5
    iget-object v0, p0, Lnai;->k:Lnad;

    if-nez v0, :cond_6

    .line 256
    new-instance v0, Lnad;

    invoke-direct {v0}, Lnad;-><init>()V

    iput-object v0, p0, Lnai;->k:Lnad;

    .line 258
    :cond_6
    iget-object v0, p0, Lnai;->k:Lnad;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 262
    :sswitch_6
    const/16 v0, 0x3a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 263
    iget-object v0, p0, Lnai;->l:[Lnae;

    if-nez v0, :cond_8

    move v0, v1

    .line 264
    :goto_2
    add-int/2addr v2, v0

    new-array v2, v2, [Lnae;

    .line 265
    iget-object v3, p0, Lnai;->l:[Lnae;

    if-eqz v3, :cond_7

    .line 266
    iget-object v3, p0, Lnai;->l:[Lnae;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 268
    :cond_7
    iput-object v2, p0, Lnai;->l:[Lnae;

    .line 269
    :goto_3
    iget-object v2, p0, Lnai;->l:[Lnae;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    .line 270
    iget-object v2, p0, Lnai;->l:[Lnae;

    new-instance v3, Lnae;

    invoke-direct {v3}, Lnae;-><init>()V

    aput-object v3, v2, v0

    .line 271
    iget-object v2, p0, Lnai;->l:[Lnae;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 272
    invoke-virtual {p1}, Loxn;->a()I

    .line 269
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 263
    :cond_8
    iget-object v0, p0, Lnai;->l:[Lnae;

    array-length v0, v0

    goto :goto_2

    .line 275
    :cond_9
    iget-object v2, p0, Lnai;->l:[Lnae;

    new-instance v3, Lnae;

    invoke-direct {v3}, Lnae;-><init>()V

    aput-object v3, v2, v0

    .line 276
    iget-object v2, p0, Lnai;->l:[Lnae;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 280
    :sswitch_7
    iget-object v0, p0, Lnai;->e:Lnaf;

    if-nez v0, :cond_a

    .line 281
    new-instance v0, Lnaf;

    invoke-direct {v0}, Lnaf;-><init>()V

    iput-object v0, p0, Lnai;->e:Lnaf;

    .line 283
    :cond_a
    iget-object v0, p0, Lnai;->e:Lnaf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 287
    :sswitch_8
    const/16 v0, 0x4a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 288
    iget-object v0, p0, Lnai;->m:[Lnag;

    if-nez v0, :cond_c

    move v0, v1

    .line 289
    :goto_4
    add-int/2addr v2, v0

    new-array v2, v2, [Lnag;

    .line 290
    iget-object v3, p0, Lnai;->m:[Lnag;

    if-eqz v3, :cond_b

    .line 291
    iget-object v3, p0, Lnai;->m:[Lnag;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 293
    :cond_b
    iput-object v2, p0, Lnai;->m:[Lnag;

    .line 294
    :goto_5
    iget-object v2, p0, Lnai;->m:[Lnag;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_d

    .line 295
    iget-object v2, p0, Lnai;->m:[Lnag;

    new-instance v3, Lnag;

    invoke-direct {v3}, Lnag;-><init>()V

    aput-object v3, v2, v0

    .line 296
    iget-object v2, p0, Lnai;->m:[Lnag;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 297
    invoke-virtual {p1}, Loxn;->a()I

    .line 294
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 288
    :cond_c
    iget-object v0, p0, Lnai;->m:[Lnag;

    array-length v0, v0

    goto :goto_4

    .line 300
    :cond_d
    iget-object v2, p0, Lnai;->m:[Lnag;

    new-instance v3, Lnag;

    invoke-direct {v3}, Lnag;-><init>()V

    aput-object v3, v2, v0

    .line 301
    iget-object v2, p0, Lnai;->m:[Lnag;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 305
    :sswitch_9
    iget-object v0, p0, Lnai;->n:Lnah;

    if-nez v0, :cond_e

    .line 306
    new-instance v0, Lnah;

    invoke-direct {v0}, Lnah;-><init>()V

    iput-object v0, p0, Lnai;->n:Lnah;

    .line 308
    :cond_e
    iget-object v0, p0, Lnai;->n:Lnah;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 312
    :sswitch_a
    iget-object v0, p0, Lnai;->f:Lnal;

    if-nez v0, :cond_f

    .line 313
    new-instance v0, Lnal;

    invoke-direct {v0}, Lnal;-><init>()V

    iput-object v0, p0, Lnai;->f:Lnal;

    .line 315
    :cond_f
    iget-object v0, p0, Lnai;->f:Lnal;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 319
    :sswitch_b
    iget-object v0, p0, Lnai;->g:Lnam;

    if-nez v0, :cond_10

    .line 320
    new-instance v0, Lnam;

    invoke-direct {v0}, Lnam;-><init>()V

    iput-object v0, p0, Lnai;->g:Lnam;

    .line 322
    :cond_10
    iget-object v0, p0, Lnai;->g:Lnam;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 326
    :sswitch_c
    iget-object v0, p0, Lnai;->c:Lnab;

    if-nez v0, :cond_11

    .line 327
    new-instance v0, Lnab;

    invoke-direct {v0}, Lnab;-><init>()V

    iput-object v0, p0, Lnai;->c:Lnab;

    .line 329
    :cond_11
    iget-object v0, p0, Lnai;->c:Lnab;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 333
    :sswitch_d
    iget-object v0, p0, Lnai;->i:Lnak;

    if-nez v0, :cond_12

    .line 334
    new-instance v0, Lnak;

    invoke-direct {v0}, Lnak;-><init>()V

    iput-object v0, p0, Lnai;->i:Lnak;

    .line 336
    :cond_12
    iget-object v0, p0, Lnai;->i:Lnak;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 340
    :sswitch_e
    iget-object v0, p0, Lnai;->o:Lmzz;

    if-nez v0, :cond_13

    .line 341
    new-instance v0, Lmzz;

    invoke-direct {v0}, Lmzz;-><init>()V

    iput-object v0, p0, Lnai;->o:Lmzz;

    .line 343
    :cond_13
    iget-object v0, p0, Lnai;->o:Lmzz;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 347
    :sswitch_f
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnai;->h:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 205
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x3a -> :sswitch_6
        0x42 -> :sswitch_7
        0x4a -> :sswitch_8
        0x52 -> :sswitch_9
        0x5a -> :sswitch_a
        0x62 -> :sswitch_b
        0x6a -> :sswitch_c
        0x72 -> :sswitch_d
        0x7a -> :sswitch_e
        0x80 -> :sswitch_f
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 59
    iget-object v1, p0, Lnai;->a:Lmzo;

    if-eqz v1, :cond_0

    .line 60
    const/4 v1, 0x1

    iget-object v2, p0, Lnai;->a:Lmzo;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 62
    :cond_0
    iget-object v1, p0, Lnai;->b:[I

    if-eqz v1, :cond_1

    iget-object v1, p0, Lnai;->b:[I

    array-length v1, v1

    if-lez v1, :cond_1

    .line 63
    iget-object v2, p0, Lnai;->b:[I

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget v4, v2, v1

    .line 64
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Loxo;->a(II)V

    .line 63
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 67
    :cond_1
    iget-object v1, p0, Lnai;->j:Lmzy;

    if-eqz v1, :cond_2

    .line 68
    const/4 v1, 0x3

    iget-object v2, p0, Lnai;->j:Lmzy;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 70
    :cond_2
    iget-object v1, p0, Lnai;->d:Lnac;

    if-eqz v1, :cond_3

    .line 71
    const/4 v1, 0x4

    iget-object v2, p0, Lnai;->d:Lnac;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 73
    :cond_3
    iget-object v1, p0, Lnai;->k:Lnad;

    if-eqz v1, :cond_4

    .line 74
    const/4 v1, 0x5

    iget-object v2, p0, Lnai;->k:Lnad;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 76
    :cond_4
    iget-object v1, p0, Lnai;->l:[Lnae;

    if-eqz v1, :cond_6

    .line 77
    iget-object v2, p0, Lnai;->l:[Lnae;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    .line 78
    if-eqz v4, :cond_5

    .line 79
    const/4 v5, 0x7

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 77
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 83
    :cond_6
    iget-object v1, p0, Lnai;->e:Lnaf;

    if-eqz v1, :cond_7

    .line 84
    const/16 v1, 0x8

    iget-object v2, p0, Lnai;->e:Lnaf;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 86
    :cond_7
    iget-object v1, p0, Lnai;->m:[Lnag;

    if-eqz v1, :cond_9

    .line 87
    iget-object v1, p0, Lnai;->m:[Lnag;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_9

    aget-object v3, v1, v0

    .line 88
    if-eqz v3, :cond_8

    .line 89
    const/16 v4, 0x9

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 87
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 93
    :cond_9
    iget-object v0, p0, Lnai;->n:Lnah;

    if-eqz v0, :cond_a

    .line 94
    const/16 v0, 0xa

    iget-object v1, p0, Lnai;->n:Lnah;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 96
    :cond_a
    iget-object v0, p0, Lnai;->f:Lnal;

    if-eqz v0, :cond_b

    .line 97
    const/16 v0, 0xb

    iget-object v1, p0, Lnai;->f:Lnal;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 99
    :cond_b
    iget-object v0, p0, Lnai;->g:Lnam;

    if-eqz v0, :cond_c

    .line 100
    const/16 v0, 0xc

    iget-object v1, p0, Lnai;->g:Lnam;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 102
    :cond_c
    iget-object v0, p0, Lnai;->c:Lnab;

    if-eqz v0, :cond_d

    .line 103
    const/16 v0, 0xd

    iget-object v1, p0, Lnai;->c:Lnab;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 105
    :cond_d
    iget-object v0, p0, Lnai;->i:Lnak;

    if-eqz v0, :cond_e

    .line 106
    const/16 v0, 0xe

    iget-object v1, p0, Lnai;->i:Lnak;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 108
    :cond_e
    iget-object v0, p0, Lnai;->o:Lmzz;

    if-eqz v0, :cond_f

    .line 109
    const/16 v0, 0xf

    iget-object v1, p0, Lnai;->o:Lmzz;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 111
    :cond_f
    iget-object v0, p0, Lnai;->h:Ljava/lang/Boolean;

    if-eqz v0, :cond_10

    .line 112
    const/16 v0, 0x10

    iget-object v1, p0, Lnai;->h:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 114
    :cond_10
    iget-object v0, p0, Lnai;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 116
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lnai;->a(Loxn;)Lnai;

    move-result-object v0

    return-object v0
.end method
