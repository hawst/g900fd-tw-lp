.class public final Ldpy;
.super Lhny;
.source "PG"


# instance fields
.field private final a:I

.field private final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Z

.field private final d:Lduo;

.field private final e:[Ljuy;

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILduo;[Ljuy;)V
    .locals 1

    .prologue
    .line 61
    const-string v0, "RemovePhotosFromTrashTask"

    invoke-direct {p0, p1, v0}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 62
    iput p2, p0, Ldpy;->a:I

    .line 63
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldpy;->c:Z

    .line 64
    const/4 v0, 0x0

    iput-object v0, p0, Ldpy;->b:Ljava/util/ArrayList;

    .line 65
    iput-object p3, p0, Ldpy;->d:Lduo;

    .line 66
    iput-object p4, p0, Ldpy;->e:[Ljuy;

    .line 67
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 48
    const-string v0, "RemovePhotosFromTrashTask"

    invoke-direct {p0, p1, v0}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 49
    iput p2, p0, Ldpy;->a:I

    .line 50
    iput-object p3, p0, Ldpy;->b:Ljava/util/ArrayList;

    .line 51
    iput-boolean p4, p0, Ldpy;->c:Z

    .line 52
    iput-object v1, p0, Ldpy;->d:Lduo;

    .line 53
    iput-object v1, p0, Ldpy;->e:[Ljuy;

    .line 54
    return-void
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 14

    .prologue
    const/4 v13, 0x1

    const/4 v3, 0x0

    .line 71
    invoke-virtual {p0}, Ldpy;->f()Landroid/content/Context;

    move-result-object v0

    .line 74
    const/4 v1, 0x0

    .line 77
    iget-object v2, p0, Ldpy;->d:Lduo;

    if-eqz v2, :cond_5

    .line 78
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 79
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 82
    iget-object v4, p0, Ldpy;->d:Lduo;

    iget v5, p0, Ldpy;->a:I

    invoke-interface {v4, v0, v5}, Lduo;->a(Landroid/content/Context;I)[Ldwj;

    move-result-object v5

    .line 84
    array-length v6, v5

    move v4, v3

    :goto_0
    if-ge v4, v6, :cond_2

    aget-object v7, v5, v4

    .line 85
    invoke-virtual {v7}, Ldwj;->d()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 86
    invoke-virtual {v7}, Ldwj;->e()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 89
    :cond_0
    invoke-virtual {v7}, Ldwj;->b()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 90
    invoke-virtual {v7}, Ldwj;->a()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 84
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_2
    move-object v7, v1

    .line 97
    :goto_1
    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_6

    .line 98
    invoke-static {v0, v2}, Ldwq;->a(Landroid/content/Context;Ljava/util/ArrayList;)Z

    move-result v1

    if-eqz v1, :cond_6

    move v12, v13

    .line 100
    :goto_2
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 101
    if-eqz v2, :cond_b

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_b

    .line 102
    iget v1, p0, Ldpy;->a:I

    iget-boolean v4, p0, Ldpy;->c:Z

    .line 103
    invoke-static/range {v0 .. v5}, Ldwq;->a(Landroid/content/Context;ILjava/util/ArrayList;ZZLjava/util/ArrayList;)Z

    move-result v1

    if-eqz v1, :cond_7

    move v1, v13

    .line 107
    :goto_3
    if-eqz v7, :cond_a

    invoke-virtual {v7}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_a

    .line 108
    new-instance v6, Ldjc;

    iget v8, p0, Ldpy;->a:I

    .line 109
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/Long;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Ljava/lang/Long;

    iget-boolean v2, p0, Ldpy;->c:Z

    if-eqz v2, :cond_8

    const/4 v11, 0x3

    :goto_4
    move-object v7, v0

    move v10, v3

    invoke-direct/range {v6 .. v11}, Ldjc;-><init>(Landroid/content/Context;I[Ljava/lang/Long;ZI)V

    .line 112
    invoke-virtual {v6}, Lkff;->l()V

    .line 114
    if-eqz v1, :cond_9

    invoke-virtual {v6}, Lkff;->t()Z

    move-result v1

    if-nez v1, :cond_9

    .line 117
    :goto_5
    if-eqz v13, :cond_3

    iget-object v1, p0, Ldpy;->e:[Ljuy;

    if-eqz v1, :cond_3

    .line 118
    iget v1, p0, Ldpy;->a:I

    iget-object v2, p0, Ldpy;->e:[Ljuy;

    invoke-static {v0, v1, v2}, Ljvj;->a(Landroid/content/Context;I[Ljuy;)V

    .line 119
    iget v1, p0, Ldpy;->a:I

    iget-object v2, p0, Ldpy;->e:[Ljuy;

    invoke-static {v0, v1, v2}, Ljvd;->a(Landroid/content/Context;I[Ljuy;)V

    .line 122
    :cond_3
    if-eqz v12, :cond_4

    .line 123
    const-class v1, Lfew;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfew;

    .line 124
    invoke-virtual {v1, v3}, Lfew;->a(Z)V

    .line 125
    invoke-static {v0}, Lcom/google/android/libraries/social/autobackup/FingerprintScannerIntentService;->a(Landroid/content/Context;)V

    .line 128
    :cond_4
    new-instance v0, Lhoz;

    invoke-direct {v0, v13}, Lhoz;-><init>(Z)V

    .line 129
    invoke-virtual {v0}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "restore"

    iget-boolean v3, p0, Ldpy;->c:Z

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 130
    invoke-virtual {v0}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "restored_uris"

    invoke-virtual {v1, v2, v5}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 131
    invoke-virtual {v0}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "resolver"

    iget-object v3, p0, Ldpy;->d:Lduo;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 132
    return-object v0

    .line 94
    :cond_5
    iget-object v2, p0, Ldpy;->b:Ljava/util/ArrayList;

    move-object v7, v1

    goto/16 :goto_1

    :cond_6
    move v12, v3

    .line 98
    goto/16 :goto_2

    :cond_7
    move v1, v3

    .line 103
    goto :goto_3

    .line 109
    :cond_8
    const/4 v11, 0x2

    goto :goto_4

    :cond_9
    move v13, v3

    .line 114
    goto :goto_5

    :cond_a
    move v13, v1

    goto :goto_5

    :cond_b
    move v1, v13

    goto/16 :goto_3
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 139
    iput-object p1, p0, Ldpy;->f:Ljava/lang/String;

    .line 140
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Ldpy;->f:Ljava/lang/String;

    return-object v0
.end method
