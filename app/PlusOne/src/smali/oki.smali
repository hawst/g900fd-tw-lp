.class public final Loki;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Loki;


# instance fields
.field private b:I

.field private c:Ljava/lang/Long;

.field private d:Ljava/lang/Long;

.field private e:Ljava/lang/Integer;

.field private f:Ljava/lang/Integer;

.field private g:Ljava/lang/Integer;

.field private h:Ljava/lang/Boolean;

.field private i:Ljava/lang/Boolean;

.field private j:Ljava/lang/Boolean;

.field private k:Ljava/lang/Boolean;

.field private l:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2141
    const/4 v0, 0x0

    new-array v0, v0, [Loki;

    sput-object v0, Loki;->a:[Loki;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2142
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2153
    const/high16 v0, -0x80000000

    iput v0, p0, Loki;->b:I

    .line 2142
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 2215
    const/4 v0, 0x1

    iget v1, p0, Loki;->b:I

    .line 2217
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2218
    iget-object v1, p0, Loki;->c:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 2219
    const/4 v1, 0x2

    iget-object v2, p0, Loki;->c:Ljava/lang/Long;

    .line 2220
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2222
    :cond_0
    iget-object v1, p0, Loki;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 2223
    const/4 v1, 0x4

    iget-object v2, p0, Loki;->f:Ljava/lang/Integer;

    .line 2224
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2226
    :cond_1
    iget-object v1, p0, Loki;->g:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 2227
    const/4 v1, 0x5

    iget-object v2, p0, Loki;->g:Ljava/lang/Integer;

    .line 2228
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2230
    :cond_2
    iget-object v1, p0, Loki;->h:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 2231
    const/4 v1, 0x6

    iget-object v2, p0, Loki;->h:Ljava/lang/Boolean;

    .line 2232
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2234
    :cond_3
    iget-object v1, p0, Loki;->i:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 2235
    const/4 v1, 0x7

    iget-object v2, p0, Loki;->i:Ljava/lang/Boolean;

    .line 2236
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2238
    :cond_4
    iget-object v1, p0, Loki;->j:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    .line 2239
    const/16 v1, 0x8

    iget-object v2, p0, Loki;->j:Ljava/lang/Boolean;

    .line 2240
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2242
    :cond_5
    iget-object v1, p0, Loki;->l:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 2243
    const/16 v1, 0xb

    iget-object v2, p0, Loki;->l:Ljava/lang/Integer;

    .line 2244
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2246
    :cond_6
    iget-object v1, p0, Loki;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    .line 2247
    const/16 v1, 0xc

    iget-object v2, p0, Loki;->e:Ljava/lang/Integer;

    .line 2248
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2250
    :cond_7
    iget-object v1, p0, Loki;->k:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    .line 2251
    const/16 v1, 0xd

    iget-object v2, p0, Loki;->k:Ljava/lang/Boolean;

    .line 2252
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2254
    :cond_8
    iget-object v1, p0, Loki;->d:Ljava/lang/Long;

    if-eqz v1, :cond_9

    .line 2255
    const/16 v1, 0xe

    iget-object v2, p0, Loki;->d:Ljava/lang/Long;

    .line 2256
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2258
    :cond_9
    iget-object v1, p0, Loki;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2259
    iput v0, p0, Loki;->ai:I

    .line 2260
    return v0
.end method

.method public a(Loxn;)Loki;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2268
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2269
    sparse-switch v0, :sswitch_data_0

    .line 2273
    iget-object v1, p0, Loki;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 2274
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Loki;->ah:Ljava/util/List;

    .line 2277
    :cond_1
    iget-object v1, p0, Loki;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2279
    :sswitch_0
    return-object p0

    .line 2284
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 2285
    if-eq v0, v2, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-ne v0, v1, :cond_3

    .line 2290
    :cond_2
    iput v0, p0, Loki;->b:I

    goto :goto_0

    .line 2292
    :cond_3
    iput v2, p0, Loki;->b:I

    goto :goto_0

    .line 2297
    :sswitch_2
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Loki;->c:Ljava/lang/Long;

    goto :goto_0

    .line 2301
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Loki;->f:Ljava/lang/Integer;

    goto :goto_0

    .line 2305
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Loki;->g:Ljava/lang/Integer;

    goto :goto_0

    .line 2309
    :sswitch_5
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Loki;->h:Ljava/lang/Boolean;

    goto :goto_0

    .line 2313
    :sswitch_6
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Loki;->i:Ljava/lang/Boolean;

    goto :goto_0

    .line 2317
    :sswitch_7
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Loki;->j:Ljava/lang/Boolean;

    goto :goto_0

    .line 2321
    :sswitch_8
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Loki;->l:Ljava/lang/Integer;

    goto :goto_0

    .line 2325
    :sswitch_9
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Loki;->e:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 2329
    :sswitch_a
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Loki;->k:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 2333
    :sswitch_b
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Loki;->d:Ljava/lang/Long;

    goto/16 :goto_0

    .line 2269
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x20 -> :sswitch_3
        0x28 -> :sswitch_4
        0x30 -> :sswitch_5
        0x38 -> :sswitch_6
        0x40 -> :sswitch_7
        0x58 -> :sswitch_8
        0x60 -> :sswitch_9
        0x68 -> :sswitch_a
        0x70 -> :sswitch_b
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 2178
    const/4 v0, 0x1

    iget v1, p0, Loki;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2179
    iget-object v0, p0, Loki;->c:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 2180
    const/4 v0, 0x2

    iget-object v1, p0, Loki;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 2182
    :cond_0
    iget-object v0, p0, Loki;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 2183
    const/4 v0, 0x4

    iget-object v1, p0, Loki;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2185
    :cond_1
    iget-object v0, p0, Loki;->g:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 2186
    const/4 v0, 0x5

    iget-object v1, p0, Loki;->g:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2188
    :cond_2
    iget-object v0, p0, Loki;->h:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 2189
    const/4 v0, 0x6

    iget-object v1, p0, Loki;->h:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 2191
    :cond_3
    iget-object v0, p0, Loki;->i:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 2192
    const/4 v0, 0x7

    iget-object v1, p0, Loki;->i:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 2194
    :cond_4
    iget-object v0, p0, Loki;->j:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 2195
    const/16 v0, 0x8

    iget-object v1, p0, Loki;->j:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 2197
    :cond_5
    iget-object v0, p0, Loki;->l:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 2198
    const/16 v0, 0xb

    iget-object v1, p0, Loki;->l:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2200
    :cond_6
    iget-object v0, p0, Loki;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_7

    .line 2201
    const/16 v0, 0xc

    iget-object v1, p0, Loki;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2203
    :cond_7
    iget-object v0, p0, Loki;->k:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    .line 2204
    const/16 v0, 0xd

    iget-object v1, p0, Loki;->k:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 2206
    :cond_8
    iget-object v0, p0, Loki;->d:Ljava/lang/Long;

    if-eqz v0, :cond_9

    .line 2207
    const/16 v0, 0xe

    iget-object v1, p0, Loki;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 2209
    :cond_9
    iget-object v0, p0, Loki;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2211
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2138
    invoke-virtual {p0, p1}, Loki;->a(Loxn;)Loki;

    move-result-object v0

    return-object v0
.end method
