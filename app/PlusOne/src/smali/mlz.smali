.class public final Lmlz;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Logr;

.field public c:Loya;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2830
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2835
    iput-object v0, p0, Lmlz;->b:Logr;

    .line 2838
    iput-object v0, p0, Lmlz;->c:Loya;

    .line 2830
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 2858
    const/4 v0, 0x0

    .line 2859
    iget-object v1, p0, Lmlz;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2860
    const/4 v0, 0x1

    iget-object v1, p0, Lmlz;->a:Ljava/lang/String;

    .line 2861
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2863
    :cond_0
    iget-object v1, p0, Lmlz;->b:Logr;

    if-eqz v1, :cond_1

    .line 2864
    const/4 v1, 0x2

    iget-object v2, p0, Lmlz;->b:Logr;

    .line 2865
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2867
    :cond_1
    iget-object v1, p0, Lmlz;->c:Loya;

    if-eqz v1, :cond_2

    .line 2868
    const/4 v1, 0x3

    iget-object v2, p0, Lmlz;->c:Loya;

    .line 2869
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2871
    :cond_2
    iget-object v1, p0, Lmlz;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2872
    iput v0, p0, Lmlz;->ai:I

    .line 2873
    return v0
.end method

.method public a(Loxn;)Lmlz;
    .locals 2

    .prologue
    .line 2881
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2882
    sparse-switch v0, :sswitch_data_0

    .line 2886
    iget-object v1, p0, Lmlz;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 2887
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmlz;->ah:Ljava/util/List;

    .line 2890
    :cond_1
    iget-object v1, p0, Lmlz;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2892
    :sswitch_0
    return-object p0

    .line 2897
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmlz;->a:Ljava/lang/String;

    goto :goto_0

    .line 2901
    :sswitch_2
    iget-object v0, p0, Lmlz;->b:Logr;

    if-nez v0, :cond_2

    .line 2902
    new-instance v0, Logr;

    invoke-direct {v0}, Logr;-><init>()V

    iput-object v0, p0, Lmlz;->b:Logr;

    .line 2904
    :cond_2
    iget-object v0, p0, Lmlz;->b:Logr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2908
    :sswitch_3
    iget-object v0, p0, Lmlz;->c:Loya;

    if-nez v0, :cond_3

    .line 2909
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    iput-object v0, p0, Lmlz;->c:Loya;

    .line 2911
    :cond_3
    iget-object v0, p0, Lmlz;->c:Loya;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2882
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 2843
    iget-object v0, p0, Lmlz;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2844
    const/4 v0, 0x1

    iget-object v1, p0, Lmlz;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 2846
    :cond_0
    iget-object v0, p0, Lmlz;->b:Logr;

    if-eqz v0, :cond_1

    .line 2847
    const/4 v0, 0x2

    iget-object v1, p0, Lmlz;->b:Logr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2849
    :cond_1
    iget-object v0, p0, Lmlz;->c:Loya;

    if-eqz v0, :cond_2

    .line 2850
    const/4 v0, 0x3

    iget-object v1, p0, Lmlz;->c:Loya;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2852
    :cond_2
    iget-object v0, p0, Lmlz;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2854
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2826
    invoke-virtual {p0, p1}, Lmlz;->a(Loxn;)Lmlz;

    move-result-object v0

    return-object v0
.end method
