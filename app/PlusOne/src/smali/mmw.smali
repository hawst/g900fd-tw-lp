.class public final Lmmw;
.super Loxq;
.source "PG"


# instance fields
.field public a:I

.field public b:[Ljava/lang/Float;

.field public c:[Lmnf;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 6581
    invoke-direct {p0}, Loxq;-><init>()V

    .line 6590
    const/high16 v0, -0x80000000

    iput v0, p0, Lmmw;->a:I

    .line 6593
    sget-object v0, Loxx;->i:[Ljava/lang/Float;

    iput-object v0, p0, Lmmw;->b:[Ljava/lang/Float;

    .line 6596
    sget-object v0, Lmnf;->a:[Lmnf;

    iput-object v0, p0, Lmmw;->c:[Lmnf;

    .line 6581
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 6622
    .line 6623
    iget v0, p0, Lmmw;->a:I

    const/high16 v2, -0x80000000

    if-eq v0, v2, :cond_3

    .line 6624
    const/4 v0, 0x1

    iget v2, p0, Lmmw;->a:I

    .line 6625
    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 6627
    :goto_0
    iget-object v2, p0, Lmmw;->b:[Ljava/lang/Float;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lmmw;->b:[Ljava/lang/Float;

    array-length v2, v2

    if-lez v2, :cond_0

    .line 6628
    iget-object v2, p0, Lmmw;->b:[Ljava/lang/Float;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x4

    .line 6629
    add-int/2addr v0, v2

    .line 6630
    iget-object v2, p0, Lmmw;->b:[Ljava/lang/Float;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 6632
    :cond_0
    iget-object v2, p0, Lmmw;->c:[Lmnf;

    if-eqz v2, :cond_2

    .line 6633
    iget-object v2, p0, Lmmw;->c:[Lmnf;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 6634
    if-eqz v4, :cond_1

    .line 6635
    const/4 v5, 0x3

    .line 6636
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 6633
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 6640
    :cond_2
    iget-object v1, p0, Lmmw;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6641
    iput v0, p0, Lmmw;->ai:I

    .line 6642
    return v0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lmmw;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 6650
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 6651
    sparse-switch v0, :sswitch_data_0

    .line 6655
    iget-object v2, p0, Lmmw;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 6656
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmmw;->ah:Ljava/util/List;

    .line 6659
    :cond_1
    iget-object v2, p0, Lmmw;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6661
    :sswitch_0
    return-object p0

    .line 6666
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 6667
    if-eqz v0, :cond_2

    const/4 v2, 0x1

    if-eq v0, v2, :cond_2

    const/4 v2, 0x2

    if-ne v0, v2, :cond_3

    .line 6670
    :cond_2
    iput v0, p0, Lmmw;->a:I

    goto :goto_0

    .line 6672
    :cond_3
    iput v1, p0, Lmmw;->a:I

    goto :goto_0

    .line 6677
    :sswitch_2
    const/16 v0, 0x15

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 6678
    iget-object v0, p0, Lmmw;->b:[Ljava/lang/Float;

    array-length v0, v0

    .line 6679
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/Float;

    .line 6680
    iget-object v3, p0, Lmmw;->b:[Ljava/lang/Float;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 6681
    iput-object v2, p0, Lmmw;->b:[Ljava/lang/Float;

    .line 6682
    :goto_1
    iget-object v2, p0, Lmmw;->b:[Ljava/lang/Float;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 6683
    iget-object v2, p0, Lmmw;->b:[Ljava/lang/Float;

    invoke-virtual {p1}, Loxn;->d()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v2, v0

    .line 6684
    invoke-virtual {p1}, Loxn;->a()I

    .line 6682
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 6687
    :cond_4
    iget-object v2, p0, Lmmw;->b:[Ljava/lang/Float;

    invoke-virtual {p1}, Loxn;->d()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v2, v0

    goto :goto_0

    .line 6691
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 6692
    iget-object v0, p0, Lmmw;->c:[Lmnf;

    if-nez v0, :cond_6

    move v0, v1

    .line 6693
    :goto_2
    add-int/2addr v2, v0

    new-array v2, v2, [Lmnf;

    .line 6694
    iget-object v3, p0, Lmmw;->c:[Lmnf;

    if-eqz v3, :cond_5

    .line 6695
    iget-object v3, p0, Lmmw;->c:[Lmnf;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 6697
    :cond_5
    iput-object v2, p0, Lmmw;->c:[Lmnf;

    .line 6698
    :goto_3
    iget-object v2, p0, Lmmw;->c:[Lmnf;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    .line 6699
    iget-object v2, p0, Lmmw;->c:[Lmnf;

    new-instance v3, Lmnf;

    invoke-direct {v3}, Lmnf;-><init>()V

    aput-object v3, v2, v0

    .line 6700
    iget-object v2, p0, Lmmw;->c:[Lmnf;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 6701
    invoke-virtual {p1}, Loxn;->a()I

    .line 6698
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 6692
    :cond_6
    iget-object v0, p0, Lmmw;->c:[Lmnf;

    array-length v0, v0

    goto :goto_2

    .line 6704
    :cond_7
    iget-object v2, p0, Lmmw;->c:[Lmnf;

    new-instance v3, Lmnf;

    invoke-direct {v3}, Lmnf;-><init>()V

    aput-object v3, v2, v0

    .line 6705
    iget-object v2, p0, Lmmw;->c:[Lmnf;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 6651
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x15 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 6601
    iget v1, p0, Lmmw;->a:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 6602
    const/4 v1, 0x1

    iget v2, p0, Lmmw;->a:I

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 6604
    :cond_0
    iget-object v1, p0, Lmmw;->b:[Ljava/lang/Float;

    if-eqz v1, :cond_1

    .line 6605
    iget-object v2, p0, Lmmw;->b:[Ljava/lang/Float;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 6606
    const/4 v5, 0x2

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    invoke-virtual {p1, v5, v4}, Loxo;->a(IF)V

    .line 6605
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 6609
    :cond_1
    iget-object v1, p0, Lmmw;->c:[Lmnf;

    if-eqz v1, :cond_3

    .line 6610
    iget-object v1, p0, Lmmw;->c:[Lmnf;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 6611
    if-eqz v3, :cond_2

    .line 6612
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 6610
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 6616
    :cond_3
    iget-object v0, p0, Lmmw;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 6618
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6577
    invoke-virtual {p0, p1}, Lmmw;->a(Loxn;)Lmmw;

    move-result-object v0

    return-object v0
.end method
