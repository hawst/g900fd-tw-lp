.class public final Lncz;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lncy;

.field public b:Lnbi;

.field private c:[Lnbi;

.field private d:Looq;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 191
    invoke-direct {p0}, Loxq;-><init>()V

    .line 194
    sget-object v0, Lncy;->a:[Lncy;

    iput-object v0, p0, Lncz;->a:[Lncy;

    .line 197
    iput-object v1, p0, Lncz;->b:Lnbi;

    .line 200
    sget-object v0, Lnbi;->a:[Lnbi;

    iput-object v0, p0, Lncz;->c:[Lnbi;

    .line 203
    iput-object v1, p0, Lncz;->d:Looq;

    .line 191
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 234
    .line 235
    iget-object v0, p0, Lncz;->a:[Lncy;

    if-eqz v0, :cond_1

    .line 236
    iget-object v3, p0, Lncz;->a:[Lncy;

    array-length v4, v3

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 237
    if-eqz v5, :cond_0

    .line 238
    const/4 v6, 0x1

    .line 239
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 236
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 243
    :cond_2
    iget-object v2, p0, Lncz;->b:Lnbi;

    if-eqz v2, :cond_3

    .line 244
    const/4 v2, 0x2

    iget-object v3, p0, Lncz;->b:Lnbi;

    .line 245
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 247
    :cond_3
    iget-object v2, p0, Lncz;->c:[Lnbi;

    if-eqz v2, :cond_5

    .line 248
    iget-object v2, p0, Lncz;->c:[Lnbi;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    .line 249
    if-eqz v4, :cond_4

    .line 250
    const/4 v5, 0x3

    .line 251
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 248
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 255
    :cond_5
    iget-object v1, p0, Lncz;->d:Looq;

    if-eqz v1, :cond_6

    .line 256
    const/4 v1, 0x4

    iget-object v2, p0, Lncz;->d:Looq;

    .line 257
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 259
    :cond_6
    iget-object v1, p0, Lncz;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 260
    iput v0, p0, Lncz;->ai:I

    .line 261
    return v0
.end method

.method public a(Loxn;)Lncz;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 269
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 270
    sparse-switch v0, :sswitch_data_0

    .line 274
    iget-object v2, p0, Lncz;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 275
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lncz;->ah:Ljava/util/List;

    .line 278
    :cond_1
    iget-object v2, p0, Lncz;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 280
    :sswitch_0
    return-object p0

    .line 285
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 286
    iget-object v0, p0, Lncz;->a:[Lncy;

    if-nez v0, :cond_3

    move v0, v1

    .line 287
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lncy;

    .line 288
    iget-object v3, p0, Lncz;->a:[Lncy;

    if-eqz v3, :cond_2

    .line 289
    iget-object v3, p0, Lncz;->a:[Lncy;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 291
    :cond_2
    iput-object v2, p0, Lncz;->a:[Lncy;

    .line 292
    :goto_2
    iget-object v2, p0, Lncz;->a:[Lncy;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 293
    iget-object v2, p0, Lncz;->a:[Lncy;

    new-instance v3, Lncy;

    invoke-direct {v3}, Lncy;-><init>()V

    aput-object v3, v2, v0

    .line 294
    iget-object v2, p0, Lncz;->a:[Lncy;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 295
    invoke-virtual {p1}, Loxn;->a()I

    .line 292
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 286
    :cond_3
    iget-object v0, p0, Lncz;->a:[Lncy;

    array-length v0, v0

    goto :goto_1

    .line 298
    :cond_4
    iget-object v2, p0, Lncz;->a:[Lncy;

    new-instance v3, Lncy;

    invoke-direct {v3}, Lncy;-><init>()V

    aput-object v3, v2, v0

    .line 299
    iget-object v2, p0, Lncz;->a:[Lncy;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 303
    :sswitch_2
    iget-object v0, p0, Lncz;->b:Lnbi;

    if-nez v0, :cond_5

    .line 304
    new-instance v0, Lnbi;

    invoke-direct {v0}, Lnbi;-><init>()V

    iput-object v0, p0, Lncz;->b:Lnbi;

    .line 306
    :cond_5
    iget-object v0, p0, Lncz;->b:Lnbi;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 310
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 311
    iget-object v0, p0, Lncz;->c:[Lnbi;

    if-nez v0, :cond_7

    move v0, v1

    .line 312
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lnbi;

    .line 313
    iget-object v3, p0, Lncz;->c:[Lnbi;

    if-eqz v3, :cond_6

    .line 314
    iget-object v3, p0, Lncz;->c:[Lnbi;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 316
    :cond_6
    iput-object v2, p0, Lncz;->c:[Lnbi;

    .line 317
    :goto_4
    iget-object v2, p0, Lncz;->c:[Lnbi;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_8

    .line 318
    iget-object v2, p0, Lncz;->c:[Lnbi;

    new-instance v3, Lnbi;

    invoke-direct {v3}, Lnbi;-><init>()V

    aput-object v3, v2, v0

    .line 319
    iget-object v2, p0, Lncz;->c:[Lnbi;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 320
    invoke-virtual {p1}, Loxn;->a()I

    .line 317
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 311
    :cond_7
    iget-object v0, p0, Lncz;->c:[Lnbi;

    array-length v0, v0

    goto :goto_3

    .line 323
    :cond_8
    iget-object v2, p0, Lncz;->c:[Lnbi;

    new-instance v3, Lnbi;

    invoke-direct {v3}, Lnbi;-><init>()V

    aput-object v3, v2, v0

    .line 324
    iget-object v2, p0, Lncz;->c:[Lnbi;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 328
    :sswitch_4
    iget-object v0, p0, Lncz;->d:Looq;

    if-nez v0, :cond_9

    .line 329
    new-instance v0, Looq;

    invoke-direct {v0}, Looq;-><init>()V

    iput-object v0, p0, Lncz;->d:Looq;

    .line 331
    :cond_9
    iget-object v0, p0, Lncz;->d:Looq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 270
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 208
    iget-object v1, p0, Lncz;->a:[Lncy;

    if-eqz v1, :cond_1

    .line 209
    iget-object v2, p0, Lncz;->a:[Lncy;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 210
    if-eqz v4, :cond_0

    .line 211
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 209
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 215
    :cond_1
    iget-object v1, p0, Lncz;->b:Lnbi;

    if-eqz v1, :cond_2

    .line 216
    const/4 v1, 0x2

    iget-object v2, p0, Lncz;->b:Lnbi;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 218
    :cond_2
    iget-object v1, p0, Lncz;->c:[Lnbi;

    if-eqz v1, :cond_4

    .line 219
    iget-object v1, p0, Lncz;->c:[Lnbi;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 220
    if-eqz v3, :cond_3

    .line 221
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 219
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 225
    :cond_4
    iget-object v0, p0, Lncz;->d:Looq;

    if-eqz v0, :cond_5

    .line 226
    const/4 v0, 0x4

    iget-object v1, p0, Lncz;->d:Looq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 228
    :cond_5
    iget-object v0, p0, Lncz;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 230
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 187
    invoke-virtual {p0, p1}, Lncz;->a(Loxn;)Lncz;

    move-result-object v0

    return-object v0
.end method
