.class public final Lotz;
.super Loxq;
.source "PG"


# instance fields
.field private a:[B

.field private b:[B


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1322
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1343
    const/4 v0, 0x0

    .line 1344
    iget-object v1, p0, Lotz;->a:[B

    if-eqz v1, :cond_0

    .line 1345
    const/4 v0, 0x1

    iget-object v1, p0, Lotz;->a:[B

    .line 1346
    invoke-static {v0, v1}, Loxo;->b(I[B)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1348
    :cond_0
    iget-object v1, p0, Lotz;->b:[B

    if-eqz v1, :cond_1

    .line 1349
    const/4 v1, 0x2

    iget-object v2, p0, Lotz;->b:[B

    .line 1350
    invoke-static {v1, v2}, Loxo;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 1352
    :cond_1
    iget-object v1, p0, Lotz;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1353
    iput v0, p0, Lotz;->ai:I

    .line 1354
    return v0
.end method

.method public a(Loxn;)Lotz;
    .locals 2

    .prologue
    .line 1362
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1363
    sparse-switch v0, :sswitch_data_0

    .line 1367
    iget-object v1, p0, Lotz;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1368
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lotz;->ah:Ljava/util/List;

    .line 1371
    :cond_1
    iget-object v1, p0, Lotz;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1373
    :sswitch_0
    return-object p0

    .line 1378
    :sswitch_1
    invoke-virtual {p1}, Loxn;->l()[B

    move-result-object v0

    iput-object v0, p0, Lotz;->a:[B

    goto :goto_0

    .line 1382
    :sswitch_2
    invoke-virtual {p1}, Loxn;->l()[B

    move-result-object v0

    iput-object v0, p0, Lotz;->b:[B

    goto :goto_0

    .line 1363
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1331
    iget-object v0, p0, Lotz;->a:[B

    if-eqz v0, :cond_0

    .line 1332
    const/4 v0, 0x1

    iget-object v1, p0, Lotz;->a:[B

    invoke-virtual {p1, v0, v1}, Loxo;->a(I[B)V

    .line 1334
    :cond_0
    iget-object v0, p0, Lotz;->b:[B

    if-eqz v0, :cond_1

    .line 1335
    const/4 v0, 0x2

    iget-object v1, p0, Lotz;->b:[B

    invoke-virtual {p1, v0, v1}, Loxo;->a(I[B)V

    .line 1337
    :cond_1
    iget-object v0, p0, Lotz;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1339
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1318
    invoke-virtual {p0, p1}, Lotz;->a(Loxn;)Lotz;

    move-result-object v0

    return-object v0
.end method
