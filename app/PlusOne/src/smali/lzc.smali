.class public final Llzc;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lojf;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10123
    invoke-direct {p0}, Loxq;-><init>()V

    .line 10126
    iput-object v0, p0, Llzc;->apiHeader:Llyq;

    .line 10129
    iput-object v0, p0, Llzc;->a:Lojf;

    .line 10123
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 10146
    const/4 v0, 0x0

    .line 10147
    iget-object v1, p0, Llzc;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 10148
    const/4 v0, 0x1

    iget-object v1, p0, Llzc;->apiHeader:Llyq;

    .line 10149
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 10151
    :cond_0
    iget-object v1, p0, Llzc;->a:Lojf;

    if-eqz v1, :cond_1

    .line 10152
    const/4 v1, 0x2

    iget-object v2, p0, Llzc;->a:Lojf;

    .line 10153
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10155
    :cond_1
    iget-object v1, p0, Llzc;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10156
    iput v0, p0, Llzc;->ai:I

    .line 10157
    return v0
.end method

.method public a(Loxn;)Llzc;
    .locals 2

    .prologue
    .line 10165
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 10166
    sparse-switch v0, :sswitch_data_0

    .line 10170
    iget-object v1, p0, Llzc;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 10171
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llzc;->ah:Ljava/util/List;

    .line 10174
    :cond_1
    iget-object v1, p0, Llzc;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 10176
    :sswitch_0
    return-object p0

    .line 10181
    :sswitch_1
    iget-object v0, p0, Llzc;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 10182
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Llzc;->apiHeader:Llyq;

    .line 10184
    :cond_2
    iget-object v0, p0, Llzc;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 10188
    :sswitch_2
    iget-object v0, p0, Llzc;->a:Lojf;

    if-nez v0, :cond_3

    .line 10189
    new-instance v0, Lojf;

    invoke-direct {v0}, Lojf;-><init>()V

    iput-object v0, p0, Llzc;->a:Lojf;

    .line 10191
    :cond_3
    iget-object v0, p0, Llzc;->a:Lojf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 10166
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 10134
    iget-object v0, p0, Llzc;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 10135
    const/4 v0, 0x1

    iget-object v1, p0, Llzc;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 10137
    :cond_0
    iget-object v0, p0, Llzc;->a:Lojf;

    if-eqz v0, :cond_1

    .line 10138
    const/4 v0, 0x2

    iget-object v1, p0, Llzc;->a:Lojf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 10140
    :cond_1
    iget-object v0, p0, Llzc;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 10142
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 10119
    invoke-virtual {p0, p1}, Llzc;->a(Loxn;)Llzc;

    move-result-object v0

    return-object v0
.end method
