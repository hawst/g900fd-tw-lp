.class public final Llxc;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Llxc;


# instance fields
.field private b:Llxp;

.field private c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4853
    const/4 v0, 0x0

    new-array v0, v0, [Llxc;

    sput-object v0, Llxc;->a:[Llxc;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 4854
    invoke-direct {p0}, Loxq;-><init>()V

    .line 4857
    const/4 v0, 0x0

    iput-object v0, p0, Llxc;->b:Llxp;

    .line 4854
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 4876
    const/4 v0, 0x0

    .line 4877
    iget-object v1, p0, Llxc;->b:Llxp;

    if-eqz v1, :cond_0

    .line 4878
    const/4 v0, 0x1

    iget-object v1, p0, Llxc;->b:Llxp;

    .line 4879
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 4881
    :cond_0
    iget-object v1, p0, Llxc;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 4882
    const/4 v1, 0x2

    iget-object v2, p0, Llxc;->c:Ljava/lang/String;

    .line 4883
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4885
    :cond_1
    iget-object v1, p0, Llxc;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4886
    iput v0, p0, Llxc;->ai:I

    .line 4887
    return v0
.end method

.method public a(Loxn;)Llxc;
    .locals 2

    .prologue
    .line 4895
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 4896
    sparse-switch v0, :sswitch_data_0

    .line 4900
    iget-object v1, p0, Llxc;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 4901
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llxc;->ah:Ljava/util/List;

    .line 4904
    :cond_1
    iget-object v1, p0, Llxc;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4906
    :sswitch_0
    return-object p0

    .line 4911
    :sswitch_1
    iget-object v0, p0, Llxc;->b:Llxp;

    if-nez v0, :cond_2

    .line 4912
    new-instance v0, Llxp;

    invoke-direct {v0}, Llxp;-><init>()V

    iput-object v0, p0, Llxc;->b:Llxp;

    .line 4914
    :cond_2
    iget-object v0, p0, Llxc;->b:Llxp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 4918
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llxc;->c:Ljava/lang/String;

    goto :goto_0

    .line 4896
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 4864
    iget-object v0, p0, Llxc;->b:Llxp;

    if-eqz v0, :cond_0

    .line 4865
    const/4 v0, 0x1

    iget-object v1, p0, Llxc;->b:Llxp;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 4867
    :cond_0
    iget-object v0, p0, Llxc;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 4868
    const/4 v0, 0x2

    iget-object v1, p0, Llxc;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 4870
    :cond_1
    iget-object v0, p0, Llxc;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 4872
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 4850
    invoke-virtual {p0, p1}, Llxc;->a(Loxn;)Llxc;

    move-result-object v0

    return-object v0
.end method
