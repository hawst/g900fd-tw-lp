.class public final Ljvv;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static a:I

.field public static b:I

.field public static c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const/16 v0, 0x280

    sput v0, Ljvv;->a:I

    .line 30
    const/16 v0, 0xc8

    sput v0, Ljvv;->b:I

    .line 31
    const/16 v0, 0x800

    sput v0, Ljvv;->c:I

    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 34
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 35
    const-string v0, "window"

    .line 36
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 37
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 38
    iget v0, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 40
    if-lez v0, :cond_0

    .line 41
    div-int/lit8 v1, v0, 0x2

    sput v1, Ljvv;->a:I

    .line 42
    div-int/lit8 v0, v0, 0x5

    sput v0, Ljvv;->b:I

    .line 44
    :cond_0
    return-void
.end method
