.class public final Llxg;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Llxg;


# instance fields
.field private b:Llxp;

.field private c:I

.field private d:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 5455
    const/4 v0, 0x0

    new-array v0, v0, [Llxg;

    sput-object v0, Llxg;->a:[Llxg;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5456
    invoke-direct {p0}, Loxq;-><init>()V

    .line 5466
    const/4 v0, 0x0

    iput-object v0, p0, Llxg;->b:Llxp;

    .line 5469
    const/high16 v0, -0x80000000

    iput v0, p0, Llxg;->c:I

    .line 5456
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 5491
    const/4 v0, 0x0

    .line 5492
    iget-object v1, p0, Llxg;->b:Llxp;

    if-eqz v1, :cond_0

    .line 5493
    const/4 v0, 0x1

    iget-object v1, p0, Llxg;->b:Llxp;

    .line 5494
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 5496
    :cond_0
    iget v1, p0, Llxg;->c:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_1

    .line 5497
    const/4 v1, 0x4

    iget v2, p0, Llxg;->c:I

    .line 5498
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5500
    :cond_1
    iget-object v1, p0, Llxg;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 5501
    const/4 v1, 0x5

    iget-object v2, p0, Llxg;->d:Ljava/lang/Boolean;

    .line 5502
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 5504
    :cond_2
    iget-object v1, p0, Llxg;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5505
    iput v0, p0, Llxg;->ai:I

    .line 5506
    return v0
.end method

.method public a(Loxn;)Llxg;
    .locals 2

    .prologue
    .line 5514
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 5515
    sparse-switch v0, :sswitch_data_0

    .line 5519
    iget-object v1, p0, Llxg;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 5520
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llxg;->ah:Ljava/util/List;

    .line 5523
    :cond_1
    iget-object v1, p0, Llxg;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5525
    :sswitch_0
    return-object p0

    .line 5530
    :sswitch_1
    iget-object v0, p0, Llxg;->b:Llxp;

    if-nez v0, :cond_2

    .line 5531
    new-instance v0, Llxp;

    invoke-direct {v0}, Llxp;-><init>()V

    iput-object v0, p0, Llxg;->b:Llxp;

    .line 5533
    :cond_2
    iget-object v0, p0, Llxg;->b:Llxp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 5537
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 5538
    if-eqz v0, :cond_3

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-ne v0, v1, :cond_4

    .line 5542
    :cond_3
    iput v0, p0, Llxg;->c:I

    goto :goto_0

    .line 5544
    :cond_4
    const/4 v0, 0x0

    iput v0, p0, Llxg;->c:I

    goto :goto_0

    .line 5549
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Llxg;->d:Ljava/lang/Boolean;

    goto :goto_0

    .line 5515
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x20 -> :sswitch_2
        0x28 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 5476
    iget-object v0, p0, Llxg;->b:Llxp;

    if-eqz v0, :cond_0

    .line 5477
    const/4 v0, 0x1

    iget-object v1, p0, Llxg;->b:Llxp;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 5479
    :cond_0
    iget v0, p0, Llxg;->c:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 5480
    const/4 v0, 0x4

    iget v1, p0, Llxg;->c:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 5482
    :cond_1
    iget-object v0, p0, Llxg;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 5483
    const/4 v0, 0x5

    iget-object v1, p0, Llxg;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 5485
    :cond_2
    iget-object v0, p0, Llxg;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 5487
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5452
    invoke-virtual {p0, p1}, Llxg;->a(Loxn;)Llxg;

    move-result-object v0

    return-object v0
.end method
