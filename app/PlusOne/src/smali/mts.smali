.class public final Lmts;
.super Loxq;
.source "PG"


# instance fields
.field private a:[I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 7011
    invoke-direct {p0}, Loxq;-><init>()V

    .line 7014
    sget-object v0, Loxx;->a:[I

    iput-object v0, p0, Lmts;->a:[I

    .line 7011
    return-void
.end method


# virtual methods
.method public a()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 7030
    .line 7031
    iget-object v1, p0, Lmts;->a:[I

    if-eqz v1, :cond_1

    iget-object v1, p0, Lmts;->a:[I

    array-length v1, v1

    if-lez v1, :cond_1

    .line 7033
    iget-object v2, p0, Lmts;->a:[I

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v0, v3, :cond_0

    aget v4, v2, v0

    .line 7035
    invoke-static {v4}, Loxo;->i(I)I

    move-result v4

    add-int/2addr v1, v4

    .line 7033
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 7038
    :cond_0
    iget-object v0, p0, Lmts;->a:[I

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v1

    .line 7040
    :cond_1
    iget-object v1, p0, Lmts;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7041
    iput v0, p0, Lmts;->ai:I

    .line 7042
    return v0
.end method

.method public a(Loxn;)Lmts;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 7050
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 7051
    sparse-switch v0, :sswitch_data_0

    .line 7055
    iget-object v1, p0, Lmts;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 7056
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmts;->ah:Ljava/util/List;

    .line 7059
    :cond_1
    iget-object v1, p0, Lmts;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 7061
    :sswitch_0
    return-object p0

    .line 7066
    :sswitch_1
    const/16 v0, 0x8

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 7067
    iget-object v0, p0, Lmts;->a:[I

    array-length v0, v0

    .line 7068
    add-int/2addr v1, v0

    new-array v1, v1, [I

    .line 7069
    iget-object v2, p0, Lmts;->a:[I

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 7070
    iput-object v1, p0, Lmts;->a:[I

    .line 7071
    :goto_1
    iget-object v1, p0, Lmts;->a:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 7072
    iget-object v1, p0, Lmts;->a:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    .line 7073
    invoke-virtual {p1}, Loxn;->a()I

    .line 7071
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 7076
    :cond_2
    iget-object v1, p0, Lmts;->a:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    goto :goto_0

    .line 7051
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 7019
    iget-object v0, p0, Lmts;->a:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmts;->a:[I

    array-length v0, v0

    if-lez v0, :cond_0

    .line 7020
    iget-object v1, p0, Lmts;->a:[I

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget v3, v1, v0

    .line 7021
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->a(II)V

    .line 7020
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 7024
    :cond_0
    iget-object v0, p0, Lmts;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 7026
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 7007
    invoke-virtual {p0, p1}, Lmts;->a(Loxn;)Lmts;

    move-result-object v0

    return-object v0
.end method
