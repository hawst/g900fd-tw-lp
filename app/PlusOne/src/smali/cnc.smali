.class public Lcnc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llqz;
.implements Llrd;
.implements Llre;
.implements Llrf;
.implements Llrg;


# instance fields
.field private a:Landroid/os/Bundle;

.field private b:Ljava/lang/String;

.field private c:Z

.field private final d:Los;

.field private e:Lxn;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Llqr;)V
    .locals 1

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcnc;->c:Z

    .line 73
    check-cast p1, Los;

    iput-object p1, p0, Lcnc;->d:Los;

    .line 75
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 76
    return-void
.end method

.method static synthetic a(Lcnc;Lxn;)Lxn;
    .locals 0

    .prologue
    .line 24
    iput-object p1, p0, Lcnc;->e:Lxn;

    return-object p1
.end method

.method static synthetic a(Lcnc;)Z
    .locals 1

    .prologue
    .line 24
    iget-boolean v0, p0, Lcnc;->c:Z

    return v0
.end method

.method static synthetic b(Lcnc;)Lxn;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcnc;->e:Lxn;

    return-object v0
.end method

.method private f()V
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcnc;->e:Lxn;

    if-eqz v0, :cond_0

    .line 125
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcnc;->c:Z

    .line 126
    iget-object v0, p0, Lcnc;->e:Lxn;

    invoke-virtual {v0}, Lxn;->c()V

    .line 127
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcnc;->c:Z

    .line 128
    const/4 v0, 0x0

    iput-object v0, p0, Lcnc;->e:Lxn;

    .line 130
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 88
    iget-object v0, p0, Lcnc;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcnc;->a:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lcnc;->b:Ljava/lang/String;

    iget-object v1, p0, Lcnc;->a:Landroid/os/Bundle;

    invoke-virtual {p0, v0, v1}, Lcnc;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 91
    :cond_0
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 80
    if-eqz p1, :cond_0

    .line 81
    const-string v0, "com.google.android.apps.photos.actionbar.mode.Mode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcnc;->b:Ljava/lang/String;

    .line 82
    const-string v0, "com.google.android.apps.photos.actionbar.mode.FactoryArgs"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcnc;->a:Landroid/os/Bundle;

    .line 84
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 133
    iget-object v0, p0, Lcnc;->d:Los;

    const-class v1, Lcnb;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcnb;

    .line 134
    invoke-virtual {v0, p1}, Lcnb;->a(Ljava/lang/Object;)Llno;

    move-result-object v0

    check-cast v0, Lcna;

    .line 135
    iput-object p1, p0, Lcnc;->b:Ljava/lang/String;

    iput-object p2, p0, Lcnc;->a:Landroid/os/Bundle;

    iget-object v1, p0, Lcnc;->d:Los;

    invoke-interface {v0, v1, p2}, Lcna;->a(Landroid/app/Activity;Landroid/os/Bundle;)Lcne;

    move-result-object v0

    new-instance v1, Lcnd;

    invoke-direct {v1, p0, v0}, Lcnd;-><init>(Lcnc;Lcne;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcnc;->c:Z

    iget-object v0, p0, Lcnc;->d:Los;

    invoke-virtual {v0, v1}, Los;->a(Lxo;)Lxn;

    move-result-object v0

    iput-object v0, p0, Lcnc;->e:Lxn;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcnc;->c:Z

    .line 136
    return-void
.end method

.method public aP_()V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0}, Lcnc;->f()V

    .line 97
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 101
    iget-object v0, p0, Lcnc;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcnc;->a:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    .line 102
    const-string v0, "com.google.android.apps.photos.actionbar.mode.Mode"

    iget-object v1, p0, Lcnc;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    const-string v0, "com.google.android.apps.photos.actionbar.mode.FactoryArgs"

    iget-object v1, p0, Lcnc;->a:Landroid/os/Bundle;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 105
    :cond_0
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcnc;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcnc;->e:Lxn;

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcnc;->e:Lxn;

    invoke-virtual {v0}, Lxn;->d()V

    .line 115
    :cond_0
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 118
    invoke-direct {p0}, Lcnc;->f()V

    .line 119
    iput-object v0, p0, Lcnc;->b:Ljava/lang/String;

    .line 120
    iput-object v0, p0, Lcnc;->a:Landroid/os/Bundle;

    .line 121
    return-void
.end method
