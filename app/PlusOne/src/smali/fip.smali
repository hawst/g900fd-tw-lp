.class public final Lfip;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Long;",
        "Ljava/lang/Void;",
        "[",
        "Lcpg;",
        ">;"
    }
.end annotation


# instance fields
.field private synthetic a:Landroid/os/Bundle;

.field private synthetic b:Landroid/content/Context;

.field private synthetic c:I

.field private synthetic d:Ljuk;

.field private synthetic e:Lcom/google/android/apps/plus/service/SlideshowService;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/service/SlideshowService;Landroid/os/Bundle;Landroid/content/Context;ILjuk;)V
    .locals 0

    .prologue
    .line 286
    iput-object p1, p0, Lfip;->e:Lcom/google/android/apps/plus/service/SlideshowService;

    iput-object p2, p0, Lfip;->a:Landroid/os/Bundle;

    iput-object p3, p0, Lfip;->b:Landroid/content/Context;

    iput p4, p0, Lfip;->c:I

    iput-object p5, p0, Lfip;->d:Ljuk;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected a([Lcpg;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 307
    array-length v0, p1

    if-lez v0, :cond_1

    iget-object v0, p0, Lfip;->e:Lcom/google/android/apps/plus/service/SlideshowService;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/SlideshowService;->b(Lcom/google/android/apps/plus/service/SlideshowService;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lfip;->c:I

    iget-object v1, p0, Lfip;->e:Lcom/google/android/apps/plus/service/SlideshowService;

    invoke-static {v1}, Lcom/google/android/apps/plus/service/SlideshowService;->c(Lcom/google/android/apps/plus/service/SlideshowService;)I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 308
    iget-object v0, p0, Lfip;->e:Lcom/google/android/apps/plus/service/SlideshowService;

    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/service/SlideshowService;->a(Lcom/google/android/apps/plus/service/SlideshowService;Lcpg;)Lizu;

    move-result-object v1

    .line 309
    const/4 v0, 0x0

    .line 310
    array-length v2, p1

    if-le v2, v3, :cond_0

    aget-object v2, p1, v3

    if-eqz v2, :cond_0

    .line 311
    iget-object v0, p0, Lfip;->e:Lcom/google/android/apps/plus/service/SlideshowService;

    aget-object v2, p1, v3

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/service/SlideshowService;->a(Lcom/google/android/apps/plus/service/SlideshowService;Lcpg;)Lizu;

    move-result-object v0

    .line 313
    :cond_0
    iget-object v2, p0, Lfip;->d:Ljuk;

    iget-object v3, p0, Lfip;->a:Landroid/os/Bundle;

    iget v4, p0, Lfip;->c:I

    invoke-virtual {v2, v3, v4, v1, v0}, Ljuk;->a(Landroid/os/Bundle;ILizu;Lizu;)V

    .line 315
    :cond_1
    return-void
.end method

.method protected varargs a([Ljava/lang/Long;)[Lcpg;
    .locals 8

    .prologue
    .line 289
    iget-object v0, p0, Lfip;->a:Landroid/os/Bundle;

    const-string v1, "account_id"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 291
    array-length v0, p1

    new-array v7, v0, [Lcpg;

    .line 292
    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_1

    .line 293
    aget-object v4, p1, v0

    .line 294
    if-eqz v4, :cond_0

    .line 295
    invoke-static {v3}, Lcpi;->a(I)Lcpi;

    move-result-object v1

    iget-object v2, p0, Lfip;->b:Landroid/content/Context;

    .line 296
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sget-object v6, Lcph;->b:Lcph;

    .line 295
    invoke-virtual/range {v1 .. v6}, Lcpi;->a(Landroid/content/Context;IJLcph;)Lcpg;

    move-result-object v1

    aput-object v1, v7, v0

    .line 292
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 299
    :cond_1
    return-object v7
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 286
    check-cast p1, [Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lfip;->a([Ljava/lang/Long;)[Lcpg;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 286
    check-cast p1, [Lcpg;

    invoke-virtual {p0, p1}, Lfip;->a([Lcpg;)V

    return-void
.end method
