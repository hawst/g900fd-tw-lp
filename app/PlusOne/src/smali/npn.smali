.class public final Lnpn;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Boolean;

.field private b:Lnpq;

.field private c:Lnpp;

.field private d:Lnpo;

.field private e:Lnpd;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 74
    invoke-direct {p0}, Loxq;-><init>()V

    .line 310
    iput-object v0, p0, Lnpn;->b:Lnpq;

    .line 313
    iput-object v0, p0, Lnpn;->c:Lnpp;

    .line 316
    iput-object v0, p0, Lnpn;->d:Lnpo;

    .line 319
    iput-object v0, p0, Lnpn;->e:Lnpd;

    .line 74
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 345
    const/4 v0, 0x0

    .line 346
    iget-object v1, p0, Lnpn;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 347
    const/4 v0, 0x1

    iget-object v1, p0, Lnpn;->a:Ljava/lang/Boolean;

    .line 348
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 350
    :cond_0
    iget-object v1, p0, Lnpn;->b:Lnpq;

    if-eqz v1, :cond_1

    .line 351
    const/4 v1, 0x2

    iget-object v2, p0, Lnpn;->b:Lnpq;

    .line 352
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 354
    :cond_1
    iget-object v1, p0, Lnpn;->d:Lnpo;

    if-eqz v1, :cond_2

    .line 355
    const/4 v1, 0x3

    iget-object v2, p0, Lnpn;->d:Lnpo;

    .line 356
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 358
    :cond_2
    iget-object v1, p0, Lnpn;->c:Lnpp;

    if-eqz v1, :cond_3

    .line 359
    const/4 v1, 0x4

    iget-object v2, p0, Lnpn;->c:Lnpp;

    .line 360
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 362
    :cond_3
    iget-object v1, p0, Lnpn;->e:Lnpd;

    if-eqz v1, :cond_4

    .line 363
    const/4 v1, 0x5

    iget-object v2, p0, Lnpn;->e:Lnpd;

    .line 364
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 366
    :cond_4
    iget-object v1, p0, Lnpn;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 367
    iput v0, p0, Lnpn;->ai:I

    .line 368
    return v0
.end method

.method public a(Loxn;)Lnpn;
    .locals 2

    .prologue
    .line 376
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 377
    sparse-switch v0, :sswitch_data_0

    .line 381
    iget-object v1, p0, Lnpn;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 382
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnpn;->ah:Ljava/util/List;

    .line 385
    :cond_1
    iget-object v1, p0, Lnpn;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 387
    :sswitch_0
    return-object p0

    .line 392
    :sswitch_1
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnpn;->a:Ljava/lang/Boolean;

    goto :goto_0

    .line 396
    :sswitch_2
    iget-object v0, p0, Lnpn;->b:Lnpq;

    if-nez v0, :cond_2

    .line 397
    new-instance v0, Lnpq;

    invoke-direct {v0}, Lnpq;-><init>()V

    iput-object v0, p0, Lnpn;->b:Lnpq;

    .line 399
    :cond_2
    iget-object v0, p0, Lnpn;->b:Lnpq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 403
    :sswitch_3
    iget-object v0, p0, Lnpn;->d:Lnpo;

    if-nez v0, :cond_3

    .line 404
    new-instance v0, Lnpo;

    invoke-direct {v0}, Lnpo;-><init>()V

    iput-object v0, p0, Lnpn;->d:Lnpo;

    .line 406
    :cond_3
    iget-object v0, p0, Lnpn;->d:Lnpo;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 410
    :sswitch_4
    iget-object v0, p0, Lnpn;->c:Lnpp;

    if-nez v0, :cond_4

    .line 411
    new-instance v0, Lnpp;

    invoke-direct {v0}, Lnpp;-><init>()V

    iput-object v0, p0, Lnpn;->c:Lnpp;

    .line 413
    :cond_4
    iget-object v0, p0, Lnpn;->c:Lnpp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 417
    :sswitch_5
    iget-object v0, p0, Lnpn;->e:Lnpd;

    if-nez v0, :cond_5

    .line 418
    new-instance v0, Lnpd;

    invoke-direct {v0}, Lnpd;-><init>()V

    iput-object v0, p0, Lnpn;->e:Lnpd;

    .line 420
    :cond_5
    iget-object v0, p0, Lnpn;->e:Lnpd;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 377
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 324
    iget-object v0, p0, Lnpn;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 325
    const/4 v0, 0x1

    iget-object v1, p0, Lnpn;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 327
    :cond_0
    iget-object v0, p0, Lnpn;->b:Lnpq;

    if-eqz v0, :cond_1

    .line 328
    const/4 v0, 0x2

    iget-object v1, p0, Lnpn;->b:Lnpq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 330
    :cond_1
    iget-object v0, p0, Lnpn;->d:Lnpo;

    if-eqz v0, :cond_2

    .line 331
    const/4 v0, 0x3

    iget-object v1, p0, Lnpn;->d:Lnpo;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 333
    :cond_2
    iget-object v0, p0, Lnpn;->c:Lnpp;

    if-eqz v0, :cond_3

    .line 334
    const/4 v0, 0x4

    iget-object v1, p0, Lnpn;->c:Lnpp;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 336
    :cond_3
    iget-object v0, p0, Lnpn;->e:Lnpd;

    if-eqz v0, :cond_4

    .line 337
    const/4 v0, 0x5

    iget-object v1, p0, Lnpn;->e:Lnpd;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 339
    :cond_4
    iget-object v0, p0, Lnpn;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 341
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 70
    invoke-virtual {p0, p1}, Lnpn;->a(Loxn;)Lnpn;

    move-result-object v0

    return-object v0
.end method
