.class public final Lmcn;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnln;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1780
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1783
    iput-object v0, p0, Lmcn;->apiHeader:Llyr;

    .line 1786
    iput-object v0, p0, Lmcn;->a:Lnln;

    .line 1780
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1803
    const/4 v0, 0x0

    .line 1804
    iget-object v1, p0, Lmcn;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 1805
    const/4 v0, 0x1

    iget-object v1, p0, Lmcn;->apiHeader:Llyr;

    .line 1806
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1808
    :cond_0
    iget-object v1, p0, Lmcn;->a:Lnln;

    if-eqz v1, :cond_1

    .line 1809
    const/4 v1, 0x2

    iget-object v2, p0, Lmcn;->a:Lnln;

    .line 1810
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1812
    :cond_1
    iget-object v1, p0, Lmcn;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1813
    iput v0, p0, Lmcn;->ai:I

    .line 1814
    return v0
.end method

.method public a(Loxn;)Lmcn;
    .locals 2

    .prologue
    .line 1822
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1823
    sparse-switch v0, :sswitch_data_0

    .line 1827
    iget-object v1, p0, Lmcn;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1828
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmcn;->ah:Ljava/util/List;

    .line 1831
    :cond_1
    iget-object v1, p0, Lmcn;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1833
    :sswitch_0
    return-object p0

    .line 1838
    :sswitch_1
    iget-object v0, p0, Lmcn;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 1839
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmcn;->apiHeader:Llyr;

    .line 1841
    :cond_2
    iget-object v0, p0, Lmcn;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1845
    :sswitch_2
    iget-object v0, p0, Lmcn;->a:Lnln;

    if-nez v0, :cond_3

    .line 1846
    new-instance v0, Lnln;

    invoke-direct {v0}, Lnln;-><init>()V

    iput-object v0, p0, Lmcn;->a:Lnln;

    .line 1848
    :cond_3
    iget-object v0, p0, Lmcn;->a:Lnln;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1823
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1791
    iget-object v0, p0, Lmcn;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 1792
    const/4 v0, 0x1

    iget-object v1, p0, Lmcn;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1794
    :cond_0
    iget-object v0, p0, Lmcn;->a:Lnln;

    if-eqz v0, :cond_1

    .line 1795
    const/4 v0, 0x2

    iget-object v1, p0, Lmcn;->a:Lnln;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1797
    :cond_1
    iget-object v0, p0, Lmcn;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1799
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1776
    invoke-virtual {p0, p1}, Lmcn;->a(Loxn;)Lmcn;

    move-result-object v0

    return-object v0
.end method
