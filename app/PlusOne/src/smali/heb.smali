.class public abstract Lheb;
.super Lloj;
.source "PG"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private Q:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Lloj;-><init>()V

    .line 47
    const/4 v0, 0x0

    iput v0, p0, Lheb;->Q:I

    return-void
.end method


# virtual methods
.method public abstract U()Landroid/util/SparseIntArray;
.end method

.method public V()I
    .locals 1

    .prologue
    .line 98
    const v0, 0x7f0a0551

    return v0
.end method

.method public c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6

    .prologue
    .line 51
    if-eqz p1, :cond_0

    .line 52
    const-string v0, "selected_index"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lheb;->Q:I

    .line 55
    :cond_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lheb;->N:Llnl;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 56
    invoke-virtual {p0}, Lheb;->V()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 57
    const v0, 0x104000a

    invoke-virtual {v1, v0, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 58
    const/high16 v0, 0x1040000

    invoke-virtual {v1, v0, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 59
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 61
    invoke-virtual {p0}, Lheb;->U()Landroid/util/SparseIntArray;

    move-result-object v2

    .line 62
    invoke-virtual {v2}, Landroid/util/SparseIntArray;->size()I

    move-result v0

    new-array v3, v0, [Ljava/lang/String;

    .line 63
    invoke-virtual {v2}, Landroid/util/SparseIntArray;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    .line 64
    iget-object v4, p0, Lheb;->N:Llnl;

    invoke-virtual {v2, v0}, Landroid/util/SparseIntArray;->valueAt(I)I

    move-result v5

    invoke-virtual {v4, v5}, Llnl;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    .line 63
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 66
    :cond_1
    iget v0, p0, Lheb;->Q:I

    invoke-virtual {v1, v3, v0, p0}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 68
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method protected c(I)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 110
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 111
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    const-string v3, "https://support.google.com/legal/troubleshooter/1114905"

    .line 112
    invoke-static {v3}, Litk;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 113
    invoke-virtual {p0, v0}, Lheb;->a(Landroid/content/Intent;)V

    move v0, v1

    .line 125
    :goto_0
    return v0

    .line 119
    :cond_0
    iget-object v0, p0, Lheb;->N:Llnl;

    const-class v2, Lhec;

    invoke-static {v0, v2}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhec;

    .line 120
    if-eqz v0, :cond_1

    .line 121
    invoke-interface {v0, p1}, Lhec;->c(I)V

    move v0, v1

    .line 122
    goto :goto_0

    .line 125
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 73
    invoke-super {p0, p1}, Lloj;->e(Landroid/os/Bundle;)V

    .line 74
    const-string v0, "selected_index"

    iget v1, p0, Lheb;->Q:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 75
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    .prologue
    .line 79
    packed-switch p2, :pswitch_data_0

    .line 90
    if-ltz p2, :cond_0

    .line 91
    iput p2, p0, Lheb;->Q:I

    .line 95
    :cond_0
    :goto_0
    return-void

    .line 81
    :pswitch_0
    invoke-virtual {p0}, Lheb;->U()Landroid/util/SparseIntArray;

    move-result-object v0

    iget v1, p0, Lheb;->Q:I

    invoke-virtual {v0, v1}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lheb;->c(I)Z

    goto :goto_0

    .line 85
    :pswitch_1
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    goto :goto_0

    .line 79
    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
