.class public final Lfky;
.super Llol;
.source "PG"

# interfaces
.implements Lkhf;


# instance fields
.field private N:Lkhr;

.field private O:I

.field private P:I

.field private Q:Lfur;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 50
    invoke-direct {p0}, Llol;-><init>()V

    .line 30
    new-instance v0, Lkhe;

    iget-object v1, p0, Lfky;->av:Llqm;

    invoke-direct {v0, p0, v1}, Lkhe;-><init>(Lkhf;Llqr;)V

    .line 50
    return-void
.end method

.method public constructor <init>(I)V
    .locals 2

    .prologue
    .line 52
    invoke-direct {p0}, Llol;-><init>()V

    .line 30
    new-instance v0, Lkhe;

    iget-object v1, p0, Lfky;->av:Llqm;

    invoke-direct {v0, p0, v1}, Lkhe;-><init>(Lkhf;Llqr;)V

    .line 53
    iput p1, p0, Lfky;->O:I

    .line 54
    return-void
.end method

.method static synthetic a(Lfky;)Llnl;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lfky;->at:Llnl;

    return-object v0
.end method

.method static synthetic b(Lfky;)I
    .locals 1

    .prologue
    .line 26
    iget v0, p0, Lfky;->P:I

    return v0
.end method

.method static synthetic c(Lfky;)Llnl;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lfky;->at:Llnl;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 5

    .prologue
    .line 79
    new-instance v0, Lkhr;

    iget-object v1, p0, Lfky;->at:Llnl;

    invoke-direct {v0, v1}, Lkhr;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lfky;->N:Lkhr;

    .line 80
    iget v0, p0, Lfky;->O:I

    packed-switch v0, :pswitch_data_0

    .line 130
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must specify a valid Preference type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 82
    :pswitch_1
    iget-object v0, p0, Lfky;->N:Lkhr;

    const v1, 0x7f0a0886

    .line 83
    invoke-virtual {p0, v1}, Lfky;->e_(I)Ljava/lang/String;

    move-result-object v1

    .line 82
    invoke-virtual {v0, v1}, Lkhr;->b(Ljava/lang/String;)Lcom/google/android/libraries/social/settings/PreferenceCategory;

    move-result-object v1

    .line 85
    iget-object v0, p0, Lfky;->Q:Lfur;

    invoke-virtual {v0}, Lfur;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f0a06b3

    .line 86
    invoke-virtual {p0, v0}, Lfky;->e_(I)Ljava/lang/String;

    move-result-object v0

    .line 88
    :goto_0
    iget-object v2, p0, Lfky;->N:Lkhr;

    const v3, 0x7f0a06af

    .line 89
    invoke-virtual {p0, v3}, Lfky;->e_(I)Ljava/lang/String;

    move-result-object v3

    .line 88
    invoke-virtual {v2, v3, v0}, Lkhr;->a(Ljava/lang/String;Ljava/lang/String;)Lkhl;

    move-result-object v0

    .line 90
    const-string v2, "instant_upload_settings_key"

    invoke-virtual {v0, v2}, Lkhl;->d(Ljava/lang/String;)V

    .line 91
    new-instance v2, Lfkz;

    invoke-direct {v2, p0}, Lfkz;-><init>(Lfky;)V

    invoke-virtual {v0, v2}, Lkhl;->a(Lkhq;)V

    .line 99
    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/settings/PreferenceCategory;->c(Lkhl;)Z

    .line 132
    :cond_0
    :goto_1
    return-void

    .line 86
    :cond_1
    const v0, 0x7f0a06b2

    .line 87
    invoke-virtual {p0, v0}, Lfky;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 102
    :pswitch_2
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lfky;->at:Llnl;

    const-class v2, Lcom/google/android/apps/plus/settings/GplusDeveloperSettingsActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 103
    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 104
    const-string v1, "account_id"

    iget v2, p0, Lfky;->P:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 106
    iget-object v1, p0, Lfky;->N:Lkhr;

    const v2, 0x7f0a088d

    .line 107
    invoke-virtual {p0, v2}, Lfky;->e_(I)Ljava/lang/String;

    move-result-object v2

    .line 106
    invoke-virtual {v1, v2, v0}, Lkhr;->a(Ljava/lang/String;Landroid/content/Intent;)V

    goto :goto_1

    .line 110
    :pswitch_3
    iget-object v0, p0, Lfky;->at:Llnl;

    invoke-static {v0}, Llnh;->b(Landroid/content/Context;)Llnh;

    move-result-object v0

    .line 111
    iget-object v1, p0, Lfky;->at:Llnl;

    invoke-static {v1}, Ldhv;->a(Landroid/content/Context;)I

    move-result v1

    .line 112
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    const-class v2, Llim;

    .line 113
    invoke-virtual {v0, v2}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    const-class v2, Lieh;

    .line 114
    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    sget-object v2, Llne;->a:Lief;

    .line 115
    invoke-interface {v0, v2, v1}, Lieh;->b(Lief;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lfky;->N:Lkhr;

    const v2, 0x7f0a0564

    .line 117
    invoke-virtual {p0, v2}, Lfky;->e_(I)Ljava/lang/String;

    move-result-object v2

    .line 116
    invoke-virtual {v0, v2}, Lkhr;->b(Ljava/lang/String;)Lcom/google/android/libraries/social/settings/PreferenceCategory;

    move-result-object v0

    .line 119
    const-string v2, "stream_category_preference"

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/settings/PreferenceCategory;->d(Ljava/lang/String;)V

    .line 120
    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lfky;->at:Llnl;

    const-class v4, Lcom/google/android/apps/plus/settings/StreamSettingsActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 121
    const-string v3, "account_id"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 122
    iget-object v1, p0, Lfky;->N:Lkhr;

    const v3, 0x7f0a0b41

    .line 123
    invoke-virtual {p0, v3}, Lfky;->e_(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    .line 122
    invoke-virtual {v1, v3, v4, v2}, Lkhr;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)Lkhl;

    move-result-object v1

    .line 124
    const-string v2, "stream_video_preference"

    invoke-virtual {v1, v2}, Lkhl;->d(Ljava/lang/String;)V

    .line 125
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/settings/PreferenceCategory;->c(Lkhl;)Z

    goto/16 :goto_1

    .line 80
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 65
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 66
    if-eqz p1, :cond_0

    .line 67
    const-string v0, "state_pref_type"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lfky;->O:I

    .line 69
    :cond_0
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 58
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 59
    iget-object v0, p0, Lfky;->au:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    iput v0, p0, Lfky;->P:I

    .line 60
    iget-object v0, p0, Lfky;->au:Llnh;

    iget-object v0, p0, Lfky;->at:Llnl;

    const-class v1, Lfur;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfur;

    iput-object v0, p0, Lfky;->Q:Lfur;

    .line 61
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 73
    invoke-super {p0, p1}, Llol;->e(Landroid/os/Bundle;)V

    .line 74
    const-string v0, "state_pref_type"

    iget v1, p0, Lfky;->O:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 75
    return-void
.end method
