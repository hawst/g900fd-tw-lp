.class public final Ldky;
.super Lkgg;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkgg",
        "<",
        "Lmeo;",
        "Lmep;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:[B

.field private final b:I

.field private final c:Z

.field private final p:Z

.field private final q:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lkfo;ZZZI[B)V
    .locals 6

    .prologue
    .line 42
    const-string v3, "loadsocialnetwork"

    new-instance v4, Lmeo;

    invoke-direct {v4}, Lmeo;-><init>()V

    new-instance v5, Lmep;

    invoke-direct {v5}, Lmep;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lkgg;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Loxu;Loxu;)V

    .line 44
    iput-object p7, p0, Ldky;->a:[B

    .line 45
    iput-boolean p3, p0, Ldky;->c:Z

    .line 46
    iput-boolean p4, p0, Ldky;->p:Z

    .line 47
    iput-boolean p5, p0, Ldky;->q:Z

    .line 48
    iput p6, p0, Ldky;->b:I

    .line 49
    return-void
.end method


# virtual methods
.method protected a(Lmeo;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 53
    iget-boolean v0, p0, Ldky;->c:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Ldky;->p:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Ldky;->q:Z

    if-eqz v0, :cond_3

    .line 54
    :cond_0
    new-instance v0, Lojk;

    invoke-direct {v0}, Lojk;-><init>()V

    iput-object v0, p1, Lmeo;->a:Lojk;

    .line 55
    iget-object v1, p1, Lmeo;->a:Lojk;

    .line 56
    iget-boolean v0, p0, Ldky;->c:Z

    if-eqz v0, :cond_1

    .line 57
    new-instance v0, Lojn;

    invoke-direct {v0}, Lojn;-><init>()V

    iput-object v0, v1, Lojk;->b:Lojn;

    .line 58
    iget-object v0, v1, Lojk;->b:Lojn;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v0, Lojn;->a:Ljava/lang/Boolean;

    .line 59
    iget-object v0, v1, Lojk;->b:Lojn;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v0, Lojn;->b:Ljava/lang/Boolean;

    .line 62
    :cond_1
    iget-boolean v0, p0, Ldky;->p:Z

    if-eqz v0, :cond_2

    .line 63
    new-instance v0, Lojm;

    invoke-direct {v0}, Lojm;-><init>()V

    iput-object v0, v1, Lojk;->a:Lojm;

    .line 64
    iget-object v0, v1, Lojk;->a:Lojm;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v0, Lojm;->a:Ljava/lang/Boolean;

    .line 65
    iget-object v0, v1, Lojk;->a:Lojm;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v0, Lojm;->b:Ljava/lang/Boolean;

    .line 68
    :cond_2
    iget-boolean v0, p0, Ldky;->q:Z

    if-eqz v0, :cond_3

    .line 69
    new-instance v0, Lojl;

    invoke-direct {v0}, Lojl;-><init>()V

    iput-object v0, v1, Lojk;->c:Lojl;

    .line 70
    iget-object v0, v1, Lojk;->c:Lojl;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v0, Lojl;->a:Ljava/lang/Boolean;

    .line 71
    iget-object v0, v1, Lojk;->c:Lojl;

    iget v2, p0, Ldky;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v0, Lojl;->c:Ljava/lang/Integer;

    .line 72
    iget-object v0, v1, Lojk;->c:Lojl;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v0, Lojl;->b:Ljava/lang/Boolean;

    .line 73
    iget-object v0, p0, Ldky;->a:[B

    if-eqz v0, :cond_3

    .line 75
    :try_start_0
    new-instance v0, Loiz;

    invoke-direct {v0}, Loiz;-><init>()V

    iget-object v2, p0, Ldky;->a:[B

    invoke-static {v0, v2}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Loiz;

    .line 77
    iget-object v1, v1, Lojk;->c:Lojl;

    iput-object v0, v1, Lojl;->d:Loiz;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    :cond_3
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 22
    check-cast p1, Lmeo;

    invoke-virtual {p0, p1}, Ldky;->a(Lmeo;)V

    return-void
.end method

.method public b()Lojc;
    .locals 1

    .prologue
    .line 87
    invoke-virtual {p0}, Ldky;->D()Loxu;

    move-result-object v0

    check-cast v0, Lmep;

    iget-object v0, v0, Lmep;->a:Lokc;

    iget-object v0, v0, Lokc;->b:Lojc;

    return-object v0
.end method

.method public c()Lojb;
    .locals 1

    .prologue
    .line 91
    invoke-virtual {p0}, Ldky;->D()Loxu;

    move-result-object v0

    check-cast v0, Lmep;

    iget-object v0, v0, Lmep;->a:Lokc;

    iget-object v0, v0, Lokc;->a:Lojb;

    return-object v0
.end method

.method public d()Loik;
    .locals 1

    .prologue
    .line 95
    invoke-virtual {p0}, Ldky;->D()Loxu;

    move-result-object v0

    check-cast v0, Lmep;

    iget-object v0, v0, Lmep;->a:Lokc;

    iget-object v0, v0, Lokc;->c:Loik;

    return-object v0
.end method
