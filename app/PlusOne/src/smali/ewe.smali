.class public final Lewe;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhtj;


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lewe;->a:Landroid/content/Context;

    .line 26
    return-void
.end method


# virtual methods
.method public a(Lizu;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 6

    .prologue
    .line 31
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Lewe;->a(Lizu;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    .line 32
    return-void
.end method

.method public a(Lizu;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 37
    iget-object v0, p0, Lewe;->a:Landroid/content/Context;

    const-class v1, Lizs;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizs;

    .line 38
    invoke-virtual {v0, p1, v7}, Lizs;->a(Lizu;I)Lcom/google/android/libraries/social/media/MediaResource;

    .line 40
    iget-object v0, p0, Lewe;->a:Landroid/content/Context;

    const-class v1, Lhee;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v1

    .line 41
    new-instance v2, Ldew;

    iget-object v0, p0, Lewe;->a:Landroid/content/Context;

    invoke-direct {v2, v0, v1}, Ldew;-><init>(Landroid/content/Context;I)V

    .line 43
    const/4 v0, 0x3

    new-array v3, v6, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "ALBUM"

    .line 44
    invoke-static {v4, p3, p2, v5}, Ljvj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    .line 43
    invoke-static {v0, v3}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 48
    invoke-virtual {p1}, Lizu;->c()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ldew;->b(Ljava/lang/String;)Ldew;

    move-result-object v3

    .line 49
    invoke-virtual {v3, v0}, Ldew;->d(Ljava/lang/String;)Ldew;

    move-result-object v0

    .line 50
    invoke-virtual {v0, p5}, Ldew;->c(Ljava/lang/String;)Ldew;

    move-result-object v0

    .line 51
    invoke-virtual {v0, p4}, Ldew;->e(Z)Ldew;

    move-result-object v0

    .line 52
    invoke-virtual {v0, v6}, Ldew;->f(Z)Ldew;

    move-result-object v0

    .line 53
    invoke-virtual {v0, v6}, Ldew;->h(Z)Ldew;

    .line 55
    const-string v0, "extra_gaia_id"

    invoke-static {v0, p3}, Lhmt;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v3

    .line 56
    iget-object v0, p0, Lewe;->a:Landroid/content/Context;

    const-class v4, Lhms;

    invoke-static {v0, v4}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v4, Lhmr;

    iget-object v5, p0, Lewe;->a:Landroid/content/Context;

    invoke-direct {v4, v5, v1}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v1, Lhmv;->ac:Lhmv;

    .line 58
    invoke-virtual {v4, v1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 59
    invoke-virtual {v1, v3}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    .line 56
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 61
    iget-object v0, p0, Lewe;->a:Landroid/content/Context;

    invoke-virtual {v2}, Ldew;->a()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 62
    return-void
.end method
