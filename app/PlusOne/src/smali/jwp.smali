.class final Ljwp;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:I

.field public b:J

.field public c:J

.field public d:J

.field public e:J

.field public f:J

.field public g:I

.field private h:Ljwp;

.field private i:Ljava/lang/String;

.field private j:I


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized a(Ljava/lang/String;)Ljwp;
    .locals 4

    .prologue
    .line 70
    const-class v1, Ljwp;

    monitor-enter v1

    :try_start_0
    sget-object v0, Ljwn;->a:Ljwp;

    .line 71
    if-nez v0, :cond_0

    .line 72
    new-instance v0, Ljwp;

    invoke-direct {v0}, Ljwp;-><init>()V

    .line 76
    :goto_0
    iput-object p0, v0, Ljwp;->i:Ljava/lang/String;

    .line 77
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, v0, Ljwp;->d:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 78
    monitor-exit v1

    return-object v0

    .line 74
    :cond_0
    :try_start_1
    iget-object v2, v0, Ljwp;->h:Ljwp;

    sput-object v2, Ljwn;->a:Ljwp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 70
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized a(Ljwp;)V
    .locals 2

    .prologue
    .line 82
    const-class v1, Ljwp;

    monitor-enter v1

    :try_start_0
    sget-object v0, Ljwn;->a:Ljwp;

    iput-object v0, p0, Ljwp;->h:Ljwp;

    .line 83
    sput-object p0, Ljwn;->a:Ljwp;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 84
    monitor-exit v1

    return-void

    .line 82
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 87
    const/4 v0, 0x0

    iput-object v0, p0, Ljwp;->i:Ljava/lang/String;

    .line 88
    iput v1, p0, Ljwp;->a:I

    .line 89
    iput v1, p0, Ljwp;->j:I

    .line 90
    iput-wide v2, p0, Ljwp;->b:J

    .line 91
    iput-wide v2, p0, Ljwp;->c:J

    .line 92
    iput-wide v2, p0, Ljwp;->f:J

    .line 93
    iput v1, p0, Ljwp;->g:I

    .line 94
    invoke-static {p0}, Ljwp;->a(Ljwp;)V

    .line 95
    return-void
.end method

.method public b(Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 107
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 108
    const-string v0, "["

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Ljwp;->i:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 110
    iget v0, p0, Ljwp;->a:I

    if-eqz v0, :cond_0

    .line 111
    const-string v0, " query-result:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Ljwp;->a:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 114
    :cond_0
    iget v0, p0, Ljwp;->j:I

    if-eqz v0, :cond_1

    .line 115
    const-string v0, " update:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Ljwp;->j:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 118
    :cond_1
    iget-wide v2, p0, Ljwp;->b:J

    cmp-long v0, v2, v6

    if-eqz v0, :cond_2

    .line 119
    const-string v0, " in:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ljwp;->b:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 122
    :cond_2
    iget-wide v2, p0, Ljwp;->c:J

    cmp-long v0, v2, v6

    if-eqz v0, :cond_3

    .line 123
    const-string v0, " out:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ljwp;->c:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 126
    :cond_3
    iget-wide v2, p0, Ljwp;->f:J

    cmp-long v0, v2, v6

    if-lez v0, :cond_4

    .line 127
    const-string v0, " net-time:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ljwp;->f:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 130
    :cond_4
    iget v0, p0, Ljwp;->g:I

    const/4 v2, 0x1

    if-le v0, v2, :cond_5

    .line 131
    const-string v0, " net-op:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Ljwp;->g:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 134
    :cond_5
    iget-wide v2, p0, Ljwp;->e:J

    iget-wide v4, p0, Ljwp;->d:J

    sub-long/2addr v2, v4

    .line 135
    cmp-long v0, v2, v6

    if-lez v0, :cond_6

    .line 136
    const-string v0, " time:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 139
    :cond_6
    if-eqz p1, :cond_7

    .line 140
    const-string v2, " report:"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_8

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 143
    :cond_7
    const/16 v0, 0x5d

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 140
    :cond_8
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public b(Ljwp;)V
    .locals 4

    .prologue
    .line 98
    iget v0, p0, Ljwp;->a:I

    iget v1, p1, Ljwp;->a:I

    add-int/2addr v0, v1

    iput v0, p0, Ljwp;->a:I

    .line 99
    iget v0, p0, Ljwp;->j:I

    iget v1, p1, Ljwp;->j:I

    add-int/2addr v0, v1

    iput v0, p0, Ljwp;->j:I

    .line 100
    iget-wide v0, p0, Ljwp;->b:J

    iget-wide v2, p1, Ljwp;->b:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Ljwp;->b:J

    .line 101
    iget-wide v0, p0, Ljwp;->c:J

    iget-wide v2, p1, Ljwp;->c:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Ljwp;->c:J

    .line 102
    iget-wide v0, p0, Ljwp;->f:J

    iget-wide v2, p1, Ljwp;->f:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Ljwp;->f:J

    .line 103
    iget v0, p0, Ljwp;->g:I

    iget v1, p1, Ljwp;->g:I

    add-int/2addr v0, v1

    iput v0, p0, Ljwp;->g:I

    .line 104
    return-void
.end method
