.class public final Lqa;
.super Lt;
.source "PG"


# instance fields
.field private N:Lvn;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Lt;-><init>()V

    .line 33
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lqa;->b(Z)V

    .line 45
    return-void
.end method

.method private V()V
    .locals 2

    .prologue
    .line 58
    iget-object v0, p0, Lqa;->N:Lvn;

    if-nez v0, :cond_1

    .line 59
    iget-object v0, p0, Lu;->h:Landroid/os/Bundle;

    .line 60
    if-eqz v0, :cond_0

    .line 61
    const-string v1, "selector"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lvn;->a(Landroid/os/Bundle;)Lvn;

    move-result-object v0

    iput-object v0, p0, Lqa;->N:Lvn;

    .line 63
    :cond_0
    iget-object v0, p0, Lqa;->N:Lvn;

    if-nez v0, :cond_1

    .line 64
    sget-object v0, Lvn;->a:Lvn;

    iput-object v0, p0, Lqa;->N:Lvn;

    .line 67
    :cond_1
    return-void
.end method


# virtual methods
.method public U()Lvn;
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Lqa;->V()V

    .line 54
    iget-object v0, p0, Lqa;->N:Lvn;

    return-object v0
.end method

.method public a(Landroid/content/Context;)Lpw;
    .locals 1

    .prologue
    .line 106
    new-instance v0, Lpw;

    invoke-direct {v0, p1}, Lpw;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public a(Lvn;)V
    .locals 3

    .prologue
    .line 76
    if-nez p1, :cond_0

    .line 77
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "selector must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 80
    :cond_0
    invoke-direct {p0}, Lqa;->V()V

    .line 81
    iget-object v0, p0, Lqa;->N:Lvn;

    invoke-virtual {v0, p1}, Lvn;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 82
    iput-object p1, p0, Lqa;->N:Lvn;

    .line 84
    iget-object v0, p0, Lu;->h:Landroid/os/Bundle;

    .line 85
    if-nez v0, :cond_1

    .line 86
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 88
    :cond_1
    const-string v1, "selector"

    invoke-virtual {p1}, Lvn;->d()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 89
    invoke-virtual {p0, v0}, Lqa;->f(Landroid/os/Bundle;)V

    .line 91
    invoke-virtual {p0}, Lqa;->c()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Lpw;

    .line 92
    if-eqz v0, :cond_2

    .line 93
    invoke-virtual {v0, p1}, Lpw;->a(Lvn;)V

    .line 96
    :cond_2
    return-void
.end method

.method public c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Lu;->t:Lz;

    invoke-virtual {p0, v0}, Lqa;->a(Landroid/content/Context;)Lpw;

    move-result-object v0

    .line 112
    invoke-virtual {p0}, Lqa;->U()Lvn;

    move-result-object v1

    invoke-virtual {v0, v1}, Lpw;->a(Lvn;)V

    .line 113
    return-object v0
.end method
