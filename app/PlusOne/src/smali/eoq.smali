.class final Leoq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;


# instance fields
.field final synthetic a:Leon;


# direct methods
.method constructor <init>(Leon;)V
    .locals 0

    .prologue
    .line 181
    iput-object p1, p0, Leoq;->a:Leon;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 2

    .prologue
    .line 184
    const v0, 0x7f10052f

    if-ne p2, v0, :cond_2

    .line 185
    iget-object v0, p0, Leoq;->a:Leon;

    invoke-static {v0}, Leon;->c(Leon;)V

    .line 186
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1

    .line 187
    iget-object v0, p0, Leoq;->a:Leon;

    iget-object v0, v0, Leon;->an:Landroid/animation/LayoutTransition$TransitionListener;

    if-nez v0, :cond_0

    .line 188
    iget-object v0, p0, Leoq;->a:Leon;

    new-instance v1, Leor;

    invoke-direct {v1, p0}, Leor;-><init>(Leoq;)V

    iput-object v1, v0, Leon;->an:Landroid/animation/LayoutTransition$TransitionListener;

    .line 204
    :cond_0
    iget-object v0, p0, Leoq;->a:Leon;

    iget-object v0, v0, Leon;->ao:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    .line 205
    iget-object v1, p0, Leoq;->a:Leon;

    iget-object v1, v1, Leon;->an:Landroid/animation/LayoutTransition$TransitionListener;

    invoke-virtual {v0, v1}, Landroid/animation/LayoutTransition;->addTransitionListener(Landroid/animation/LayoutTransition$TransitionListener;)V

    .line 209
    :goto_0
    iget-object v0, p0, Leoq;->a:Leon;

    iget-object v0, v0, Leon;->ap:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 215
    :goto_1
    return-void

    .line 207
    :cond_1
    iget-object v0, p0, Leoq;->a:Leon;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Leon;->b(Leon;Z)V

    goto :goto_0

    .line 211
    :cond_2
    iget-object v0, p0, Leoq;->a:Leon;

    invoke-static {v0}, Leon;->a(Leon;)V

    .line 212
    iget-object v0, p0, Leoq;->a:Leon;

    invoke-static {v0}, Leon;->b(Leon;)V

    .line 213
    iget-object v0, p0, Leoq;->a:Leon;

    iget-object v0, v0, Leon;->ap:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_1
.end method
