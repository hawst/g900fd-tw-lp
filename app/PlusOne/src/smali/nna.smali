.class public final Lnna;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnmy;

.field public b:Lnmu;

.field public c:Lnng;

.field public d:[Lnmv;

.field private e:Ljava/lang/String;

.field private f:Lnnc;

.field private g:I

.field private h:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/high16 v1, -0x80000000

    const/4 v0, 0x0

    .line 15
    invoke-direct {p0}, Loxq;-><init>()V

    .line 28
    iput-object v0, p0, Lnna;->a:Lnmy;

    .line 31
    iput-object v0, p0, Lnna;->b:Lnmu;

    .line 34
    iput-object v0, p0, Lnna;->c:Lnng;

    .line 39
    iput-object v0, p0, Lnna;->f:Lnnc;

    .line 42
    iput v1, p0, Lnna;->g:I

    .line 45
    iput v1, p0, Lnna;->h:I

    .line 48
    sget-object v0, Lnmv;->a:[Lnmv;

    iput-object v0, p0, Lnna;->d:[Lnmv;

    .line 15
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/high16 v4, -0x80000000

    .line 87
    .line 88
    iget-object v0, p0, Lnna;->a:Lnmy;

    if-eqz v0, :cond_8

    .line 89
    const/4 v0, 0x1

    iget-object v2, p0, Lnna;->a:Lnmy;

    .line 90
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 92
    :goto_0
    iget-object v2, p0, Lnna;->b:Lnmu;

    if-eqz v2, :cond_0

    .line 93
    const/4 v2, 0x2

    iget-object v3, p0, Lnna;->b:Lnmu;

    .line 94
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 96
    :cond_0
    iget-object v2, p0, Lnna;->e:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 97
    const/4 v2, 0x3

    iget-object v3, p0, Lnna;->e:Ljava/lang/String;

    .line 98
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 100
    :cond_1
    iget-object v2, p0, Lnna;->f:Lnnc;

    if-eqz v2, :cond_2

    .line 101
    const/4 v2, 0x5

    iget-object v3, p0, Lnna;->f:Lnnc;

    .line 102
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 104
    :cond_2
    iget v2, p0, Lnna;->g:I

    if-eq v2, v4, :cond_3

    .line 105
    const/4 v2, 0x6

    iget v3, p0, Lnna;->g:I

    .line 106
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 108
    :cond_3
    iget-object v2, p0, Lnna;->c:Lnng;

    if-eqz v2, :cond_4

    .line 109
    const/16 v2, 0x8

    iget-object v3, p0, Lnna;->c:Lnng;

    .line 110
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 112
    :cond_4
    iget v2, p0, Lnna;->h:I

    if-eq v2, v4, :cond_5

    .line 113
    const/16 v2, 0x9

    iget v3, p0, Lnna;->h:I

    .line 114
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 116
    :cond_5
    iget-object v2, p0, Lnna;->d:[Lnmv;

    if-eqz v2, :cond_7

    .line 117
    iget-object v2, p0, Lnna;->d:[Lnmv;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_7

    aget-object v4, v2, v1

    .line 118
    if-eqz v4, :cond_6

    .line 119
    const/16 v5, 0xa

    .line 120
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 117
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 124
    :cond_7
    iget-object v1, p0, Lnna;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 125
    iput v0, p0, Lnna;->ai:I

    .line 126
    return v0

    :cond_8
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lnna;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x1

    .line 134
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 135
    sparse-switch v0, :sswitch_data_0

    .line 139
    iget-object v2, p0, Lnna;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 140
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnna;->ah:Ljava/util/List;

    .line 143
    :cond_1
    iget-object v2, p0, Lnna;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 145
    :sswitch_0
    return-object p0

    .line 150
    :sswitch_1
    iget-object v0, p0, Lnna;->a:Lnmy;

    if-nez v0, :cond_2

    .line 151
    new-instance v0, Lnmy;

    invoke-direct {v0}, Lnmy;-><init>()V

    iput-object v0, p0, Lnna;->a:Lnmy;

    .line 153
    :cond_2
    iget-object v0, p0, Lnna;->a:Lnmy;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 157
    :sswitch_2
    iget-object v0, p0, Lnna;->b:Lnmu;

    if-nez v0, :cond_3

    .line 158
    new-instance v0, Lnmu;

    invoke-direct {v0}, Lnmu;-><init>()V

    iput-object v0, p0, Lnna;->b:Lnmu;

    .line 160
    :cond_3
    iget-object v0, p0, Lnna;->b:Lnmu;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 164
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnna;->e:Ljava/lang/String;

    goto :goto_0

    .line 168
    :sswitch_4
    iget-object v0, p0, Lnna;->f:Lnnc;

    if-nez v0, :cond_4

    .line 169
    new-instance v0, Lnnc;

    invoke-direct {v0}, Lnnc;-><init>()V

    iput-object v0, p0, Lnna;->f:Lnnc;

    .line 171
    :cond_4
    iget-object v0, p0, Lnna;->f:Lnnc;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 175
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 176
    if-eq v0, v4, :cond_5

    const/4 v2, 0x2

    if-eq v0, v2, :cond_5

    const/4 v2, 0x3

    if-ne v0, v2, :cond_6

    .line 179
    :cond_5
    iput v0, p0, Lnna;->g:I

    goto :goto_0

    .line 181
    :cond_6
    iput v4, p0, Lnna;->g:I

    goto :goto_0

    .line 186
    :sswitch_6
    iget-object v0, p0, Lnna;->c:Lnng;

    if-nez v0, :cond_7

    .line 187
    new-instance v0, Lnng;

    invoke-direct {v0}, Lnng;-><init>()V

    iput-object v0, p0, Lnna;->c:Lnng;

    .line 189
    :cond_7
    iget-object v0, p0, Lnna;->c:Lnng;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 193
    :sswitch_7
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 194
    if-ne v0, v4, :cond_8

    .line 195
    iput v0, p0, Lnna;->h:I

    goto :goto_0

    .line 197
    :cond_8
    iput v4, p0, Lnna;->h:I

    goto/16 :goto_0

    .line 202
    :sswitch_8
    const/16 v0, 0x52

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 203
    iget-object v0, p0, Lnna;->d:[Lnmv;

    if-nez v0, :cond_a

    move v0, v1

    .line 204
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnmv;

    .line 205
    iget-object v3, p0, Lnna;->d:[Lnmv;

    if-eqz v3, :cond_9

    .line 206
    iget-object v3, p0, Lnna;->d:[Lnmv;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 208
    :cond_9
    iput-object v2, p0, Lnna;->d:[Lnmv;

    .line 209
    :goto_2
    iget-object v2, p0, Lnna;->d:[Lnmv;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_b

    .line 210
    iget-object v2, p0, Lnna;->d:[Lnmv;

    new-instance v3, Lnmv;

    invoke-direct {v3}, Lnmv;-><init>()V

    aput-object v3, v2, v0

    .line 211
    iget-object v2, p0, Lnna;->d:[Lnmv;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 212
    invoke-virtual {p1}, Loxn;->a()I

    .line 209
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 203
    :cond_a
    iget-object v0, p0, Lnna;->d:[Lnmv;

    array-length v0, v0

    goto :goto_1

    .line 215
    :cond_b
    iget-object v2, p0, Lnna;->d:[Lnmv;

    new-instance v3, Lnmv;

    invoke-direct {v3}, Lnmv;-><init>()V

    aput-object v3, v2, v0

    .line 216
    iget-object v2, p0, Lnna;->d:[Lnmv;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 135
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x2a -> :sswitch_4
        0x30 -> :sswitch_5
        0x42 -> :sswitch_6
        0x48 -> :sswitch_7
        0x52 -> :sswitch_8
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    const/high16 v2, -0x80000000

    .line 53
    iget-object v0, p0, Lnna;->a:Lnmy;

    if-eqz v0, :cond_0

    .line 54
    const/4 v0, 0x1

    iget-object v1, p0, Lnna;->a:Lnmy;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 56
    :cond_0
    iget-object v0, p0, Lnna;->b:Lnmu;

    if-eqz v0, :cond_1

    .line 57
    const/4 v0, 0x2

    iget-object v1, p0, Lnna;->b:Lnmu;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 59
    :cond_1
    iget-object v0, p0, Lnna;->e:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 60
    const/4 v0, 0x3

    iget-object v1, p0, Lnna;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 62
    :cond_2
    iget-object v0, p0, Lnna;->f:Lnnc;

    if-eqz v0, :cond_3

    .line 63
    const/4 v0, 0x5

    iget-object v1, p0, Lnna;->f:Lnnc;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 65
    :cond_3
    iget v0, p0, Lnna;->g:I

    if-eq v0, v2, :cond_4

    .line 66
    const/4 v0, 0x6

    iget v1, p0, Lnna;->g:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 68
    :cond_4
    iget-object v0, p0, Lnna;->c:Lnng;

    if-eqz v0, :cond_5

    .line 69
    const/16 v0, 0x8

    iget-object v1, p0, Lnna;->c:Lnng;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 71
    :cond_5
    iget v0, p0, Lnna;->h:I

    if-eq v0, v2, :cond_6

    .line 72
    const/16 v0, 0x9

    iget v1, p0, Lnna;->h:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 74
    :cond_6
    iget-object v0, p0, Lnna;->d:[Lnmv;

    if-eqz v0, :cond_8

    .line 75
    iget-object v1, p0, Lnna;->d:[Lnmv;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_8

    aget-object v3, v1, v0

    .line 76
    if-eqz v3, :cond_7

    .line 77
    const/16 v4, 0xa

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 75
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 81
    :cond_8
    iget-object v0, p0, Lnna;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 83
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 11
    invoke-virtual {p0, p1}, Lnna;->a(Loxn;)Lnna;

    move-result-object v0

    return-object v0
.end method
