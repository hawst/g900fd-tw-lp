.class public final Lnyx;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnyx;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/Boolean;

.field private e:Ljava/lang/Boolean;

.field private f:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6379
    const/4 v0, 0x0

    new-array v0, v0, [Lnyx;

    sput-object v0, Lnyx;->a:[Lnyx;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6380
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 6412
    const/4 v0, 0x1

    iget-object v1, p0, Lnyx;->b:Ljava/lang/String;

    .line 6414
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 6415
    const/4 v1, 0x2

    iget-object v2, p0, Lnyx;->c:Ljava/lang/String;

    .line 6416
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6417
    iget-object v1, p0, Lnyx;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 6418
    const/4 v1, 0x3

    iget-object v2, p0, Lnyx;->d:Ljava/lang/Boolean;

    .line 6419
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 6421
    :cond_0
    iget-object v1, p0, Lnyx;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 6422
    const/4 v1, 0x4

    iget-object v2, p0, Lnyx;->e:Ljava/lang/Boolean;

    .line 6423
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 6425
    :cond_1
    iget-object v1, p0, Lnyx;->f:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 6426
    const/4 v1, 0x5

    iget-object v2, p0, Lnyx;->f:Ljava/lang/Boolean;

    .line 6427
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 6429
    :cond_2
    iget-object v1, p0, Lnyx;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6430
    iput v0, p0, Lnyx;->ai:I

    .line 6431
    return v0
.end method

.method public a(Loxn;)Lnyx;
    .locals 2

    .prologue
    .line 6439
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 6440
    sparse-switch v0, :sswitch_data_0

    .line 6444
    iget-object v1, p0, Lnyx;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 6445
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnyx;->ah:Ljava/util/List;

    .line 6448
    :cond_1
    iget-object v1, p0, Lnyx;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6450
    :sswitch_0
    return-object p0

    .line 6455
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnyx;->b:Ljava/lang/String;

    goto :goto_0

    .line 6459
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnyx;->c:Ljava/lang/String;

    goto :goto_0

    .line 6463
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnyx;->d:Ljava/lang/Boolean;

    goto :goto_0

    .line 6467
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnyx;->e:Ljava/lang/Boolean;

    goto :goto_0

    .line 6471
    :sswitch_5
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnyx;->f:Ljava/lang/Boolean;

    goto :goto_0

    .line 6440
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 6395
    const/4 v0, 0x1

    iget-object v1, p0, Lnyx;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 6396
    const/4 v0, 0x2

    iget-object v1, p0, Lnyx;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 6397
    iget-object v0, p0, Lnyx;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 6398
    const/4 v0, 0x3

    iget-object v1, p0, Lnyx;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 6400
    :cond_0
    iget-object v0, p0, Lnyx;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 6401
    const/4 v0, 0x4

    iget-object v1, p0, Lnyx;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 6403
    :cond_1
    iget-object v0, p0, Lnyx;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 6404
    const/4 v0, 0x5

    iget-object v1, p0, Lnyx;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 6406
    :cond_2
    iget-object v0, p0, Lnyx;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 6408
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6376
    invoke-virtual {p0, p1}, Lnyx;->a(Loxn;)Lnyx;

    move-result-object v0

    return-object v0
.end method
