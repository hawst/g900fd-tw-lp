.class public final Lqax;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lqbl;

.field public b:[Lqbk;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1894
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1897
    const/4 v0, 0x0

    iput-object v0, p0, Lqax;->a:Lqbl;

    .line 1900
    sget-object v0, Lqbk;->a:[Lqbk;

    iput-object v0, p0, Lqax;->b:[Lqbk;

    .line 1894
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1921
    .line 1922
    iget-object v0, p0, Lqax;->a:Lqbl;

    if-eqz v0, :cond_2

    .line 1923
    const/4 v0, 0x1

    iget-object v2, p0, Lqax;->a:Lqbl;

    .line 1924
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1926
    :goto_0
    iget-object v2, p0, Lqax;->b:[Lqbk;

    if-eqz v2, :cond_1

    .line 1927
    iget-object v2, p0, Lqax;->b:[Lqbk;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 1928
    if-eqz v4, :cond_0

    .line 1929
    const/4 v5, 0x2

    .line 1930
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 1927
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1934
    :cond_1
    iget-object v1, p0, Lqax;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1935
    iput v0, p0, Lqax;->ai:I

    .line 1936
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lqax;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1944
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1945
    sparse-switch v0, :sswitch_data_0

    .line 1949
    iget-object v2, p0, Lqax;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 1950
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lqax;->ah:Ljava/util/List;

    .line 1953
    :cond_1
    iget-object v2, p0, Lqax;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1955
    :sswitch_0
    return-object p0

    .line 1960
    :sswitch_1
    iget-object v0, p0, Lqax;->a:Lqbl;

    if-nez v0, :cond_2

    .line 1961
    new-instance v0, Lqbl;

    invoke-direct {v0}, Lqbl;-><init>()V

    iput-object v0, p0, Lqax;->a:Lqbl;

    .line 1963
    :cond_2
    iget-object v0, p0, Lqax;->a:Lqbl;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1967
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1968
    iget-object v0, p0, Lqax;->b:[Lqbk;

    if-nez v0, :cond_4

    move v0, v1

    .line 1969
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lqbk;

    .line 1970
    iget-object v3, p0, Lqax;->b:[Lqbk;

    if-eqz v3, :cond_3

    .line 1971
    iget-object v3, p0, Lqax;->b:[Lqbk;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1973
    :cond_3
    iput-object v2, p0, Lqax;->b:[Lqbk;

    .line 1974
    :goto_2
    iget-object v2, p0, Lqax;->b:[Lqbk;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 1975
    iget-object v2, p0, Lqax;->b:[Lqbk;

    new-instance v3, Lqbk;

    invoke-direct {v3}, Lqbk;-><init>()V

    aput-object v3, v2, v0

    .line 1976
    iget-object v2, p0, Lqax;->b:[Lqbk;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1977
    invoke-virtual {p1}, Loxn;->a()I

    .line 1974
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1968
    :cond_4
    iget-object v0, p0, Lqax;->b:[Lqbk;

    array-length v0, v0

    goto :goto_1

    .line 1980
    :cond_5
    iget-object v2, p0, Lqax;->b:[Lqbk;

    new-instance v3, Lqbk;

    invoke-direct {v3}, Lqbk;-><init>()V

    aput-object v3, v2, v0

    .line 1981
    iget-object v2, p0, Lqax;->b:[Lqbk;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1945
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 1905
    iget-object v0, p0, Lqax;->a:Lqbl;

    if-eqz v0, :cond_0

    .line 1906
    const/4 v0, 0x1

    iget-object v1, p0, Lqax;->a:Lqbl;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1908
    :cond_0
    iget-object v0, p0, Lqax;->b:[Lqbk;

    if-eqz v0, :cond_2

    .line 1909
    iget-object v1, p0, Lqax;->b:[Lqbk;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 1910
    if-eqz v3, :cond_1

    .line 1911
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 1909
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1915
    :cond_2
    iget-object v0, p0, Lqax;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1917
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1890
    invoke-virtual {p0, p1}, Lqax;->a(Loxn;)Lqax;

    move-result-object v0

    return-object v0
.end method
