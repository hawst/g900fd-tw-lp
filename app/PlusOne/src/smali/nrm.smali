.class public final Lnrm;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Long;

.field public b:[Lnrn;

.field private c:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1053
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1060
    sget-object v0, Lnrn;->a:[Lnrn;

    iput-object v0, p0, Lnrm;->b:[Lnrn;

    .line 1053
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1084
    .line 1085
    iget-object v0, p0, Lnrm;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 1086
    const/4 v0, 0x1

    iget-object v2, p0, Lnrm;->c:Ljava/lang/Integer;

    .line 1087
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1089
    :goto_0
    iget-object v2, p0, Lnrm;->a:Ljava/lang/Long;

    if-eqz v2, :cond_0

    .line 1090
    const/4 v2, 0x2

    iget-object v3, p0, Lnrm;->a:Ljava/lang/Long;

    .line 1091
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Loxo;->f(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 1093
    :cond_0
    iget-object v2, p0, Lnrm;->b:[Lnrn;

    if-eqz v2, :cond_2

    .line 1094
    iget-object v2, p0, Lnrm;->b:[Lnrn;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 1095
    if-eqz v4, :cond_1

    .line 1096
    const/4 v5, 0x3

    .line 1097
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 1094
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1101
    :cond_2
    iget-object v1, p0, Lnrm;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1102
    iput v0, p0, Lnrm;->ai:I

    .line 1103
    return v0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lnrm;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1111
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1112
    sparse-switch v0, :sswitch_data_0

    .line 1116
    iget-object v2, p0, Lnrm;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 1117
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnrm;->ah:Ljava/util/List;

    .line 1120
    :cond_1
    iget-object v2, p0, Lnrm;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1122
    :sswitch_0
    return-object p0

    .line 1127
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnrm;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 1131
    :sswitch_2
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lnrm;->a:Ljava/lang/Long;

    goto :goto_0

    .line 1135
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1136
    iget-object v0, p0, Lnrm;->b:[Lnrn;

    if-nez v0, :cond_3

    move v0, v1

    .line 1137
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnrn;

    .line 1138
    iget-object v3, p0, Lnrm;->b:[Lnrn;

    if-eqz v3, :cond_2

    .line 1139
    iget-object v3, p0, Lnrm;->b:[Lnrn;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1141
    :cond_2
    iput-object v2, p0, Lnrm;->b:[Lnrn;

    .line 1142
    :goto_2
    iget-object v2, p0, Lnrm;->b:[Lnrn;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 1143
    iget-object v2, p0, Lnrm;->b:[Lnrn;

    new-instance v3, Lnrn;

    invoke-direct {v3}, Lnrn;-><init>()V

    aput-object v3, v2, v0

    .line 1144
    iget-object v2, p0, Lnrm;->b:[Lnrn;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1145
    invoke-virtual {p1}, Loxn;->a()I

    .line 1142
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1136
    :cond_3
    iget-object v0, p0, Lnrm;->b:[Lnrn;

    array-length v0, v0

    goto :goto_1

    .line 1148
    :cond_4
    iget-object v2, p0, Lnrm;->b:[Lnrn;

    new-instance v3, Lnrn;

    invoke-direct {v3}, Lnrn;-><init>()V

    aput-object v3, v2, v0

    .line 1149
    iget-object v2, p0, Lnrm;->b:[Lnrn;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1112
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 1065
    iget-object v0, p0, Lnrm;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 1066
    const/4 v0, 0x1

    iget-object v1, p0, Lnrm;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1068
    :cond_0
    iget-object v0, p0, Lnrm;->a:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 1069
    const/4 v0, 0x2

    iget-object v1, p0, Lnrm;->a:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 1071
    :cond_1
    iget-object v0, p0, Lnrm;->b:[Lnrn;

    if-eqz v0, :cond_3

    .line 1072
    iget-object v1, p0, Lnrm;->b:[Lnrn;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 1073
    if-eqz v3, :cond_2

    .line 1074
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 1072
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1078
    :cond_3
    iget-object v0, p0, Lnrm;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1080
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1049
    invoke-virtual {p0, p1}, Lnrm;->a(Loxn;)Lnrm;

    move-result-object v0

    return-object v0
.end method
