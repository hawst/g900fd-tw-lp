.class final Lwt;
.super Landroid/os/Handler;
.source "PG"


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lwo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lwo;)V
    .locals 1

    .prologue
    .line 604
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 605
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lwt;->a:Ljava/lang/ref/WeakReference;

    .line 606
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 609
    iget-object v0, p0, Lwt;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->clear()V

    .line 610
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    .line 614
    iget-object v0, p0, Lwt;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lwo;

    .line 615
    if-eqz v0, :cond_1

    .line 616
    iget v3, p1, Landroid/os/Message;->what:I

    .line 617
    iget v4, p1, Landroid/os/Message;->arg1:I

    .line 618
    iget v5, p1, Landroid/os/Message;->arg2:I

    .line 619
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 620
    invoke-virtual {p1}, Landroid/os/Message;->peekData()Landroid/os/Bundle;

    move-result-object v6

    .line 621
    packed-switch v3, :pswitch_data_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    .line 622
    invoke-static {}, Lwn;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 623
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Unhandled message from server: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 627
    :cond_1
    return-void

    .line 621
    :pswitch_0
    invoke-virtual {v0, v4}, Lwo;->a(I)Z

    move v0, v2

    goto :goto_0

    :pswitch_1
    move v0, v2

    goto :goto_0

    :pswitch_2
    if-eqz v1, :cond_2

    instance-of v2, v1, Landroid/os/Bundle;

    if-eqz v2, :cond_0

    :cond_2
    check-cast v1, Landroid/os/Bundle;

    invoke-virtual {v0, v4, v5, v1}, Lwo;->a(IILandroid/os/Bundle;)Z

    move-result v0

    goto :goto_0

    :pswitch_3
    if-eqz v1, :cond_3

    instance-of v2, v1, Landroid/os/Bundle;

    if-eqz v2, :cond_0

    :cond_3
    check-cast v1, Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Lwo;->a(Landroid/os/Bundle;)Z

    move-result v0

    goto :goto_0

    :pswitch_4
    if-eqz v1, :cond_4

    instance-of v1, v1, Landroid/os/Bundle;

    if-eqz v1, :cond_0

    :cond_4
    invoke-virtual {v0, v4}, Lwo;->b(I)Z

    move-result v0

    goto :goto_0

    :pswitch_5
    if-eqz v1, :cond_5

    instance-of v1, v1, Landroid/os/Bundle;

    if-eqz v1, :cond_0

    :cond_5
    if-eqz v6, :cond_6

    const-string v1, "error"

    invoke-virtual {v6, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    :cond_6
    invoke-virtual {v0, v4}, Lwo;->c(I)Z

    move-result v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_3
    .end packed-switch
.end method
