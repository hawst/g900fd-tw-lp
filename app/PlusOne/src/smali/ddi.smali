.class final Lddi;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lbc",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private synthetic a:Lddf;


# direct methods
.method constructor <init>(Lddf;)V
    .locals 0

    .prologue
    .line 106
    iput-object p1, p0, Lddi;->a:Lddf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 109
    const-string v0, "photo_id"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    .line 110
    const-string v0, "view_id"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 111
    const-string v0, "tile_id"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 112
    const-string v0, "account_id"

    const/4 v1, -0x1

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v5

    .line 114
    new-instance v0, Ldvy;

    iget-object v1, p0, Lddi;->a:Lddf;

    invoke-virtual {v1}, Lddf;->n()Lz;

    move-result-object v1

    invoke-direct/range {v0 .. v5}, Ldvy;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 131
    return-void
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 106
    check-cast p2, Ljava/lang/String;

    invoke-virtual {p0, p2}, Lddi;->a(Ljava/lang/String;)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 119
    iget-object v0, p0, Lddi;->a:Lddf;

    new-instance v1, Lddz;

    invoke-direct {v1}, Lddz;-><init>()V

    iget-object v2, p0, Lddi;->a:Lddf;

    .line 120
    invoke-virtual {v2}, Lddf;->V()Lddl;

    move-result-object v2

    invoke-virtual {v1, v2}, Lddz;->a(Lddl;)Ldds;

    move-result-object v1

    .line 121
    invoke-virtual {v1, p1}, Ldds;->b(Ljava/lang/String;)Ldds;

    move-result-object v1

    .line 122
    invoke-virtual {v1}, Ldds;->a()Lddl;

    move-result-object v1

    .line 119
    invoke-virtual {v0, v1}, Lddf;->a(Lddl;)V

    .line 124
    iget-object v0, p0, Lddi;->a:Lddf;

    invoke-virtual {v0}, Lddf;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lddi;->a:Lddf;

    invoke-virtual {v2}, Lddf;->k()Landroid/os/Bundle;

    move-result-object v2

    new-instance v3, Lddg;

    iget-object v4, p0, Lddi;->a:Lddf;

    invoke-direct {v3, v4}, Lddg;-><init>(Lddf;)V

    invoke-virtual {v0, v1, v2, v3}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 126
    return-void
.end method
