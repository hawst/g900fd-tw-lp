.class public final Lkvc;
.super Lhya;
.source "PG"


# static fields
.field public static final a:[Ljava/lang/String;

.field private static final b:[Ljava/lang/String;


# instance fields
.field private c:Z

.field private d:Lkve;

.field private e:Lkvd;

.field private f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 27
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "qualified_id"

    aput-object v1, v0, v4

    const-string v1, "name"

    aput-object v1, v0, v5

    const/4 v1, 0x3

    const-string v2, "avatar"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "membership_status"

    aput-object v2, v0, v1

    sput-object v0, Lkvc;->a:[Ljava/lang/String;

    .line 50
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "remaining_count"

    aput-object v1, v0, v4

    sput-object v0, Lkvc;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ZLkve;Lkvd;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 72
    invoke-direct {p0, p1, v1}, Lhya;-><init>(Landroid/content/Context;B)V

    move v0, v1

    .line 73
    :goto_0
    const/4 v2, 0x2

    if-ge v0, v2, :cond_0

    .line 74
    invoke-virtual {p0, v1, v1}, Lkvc;->b(ZZ)V

    .line 73
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 77
    :cond_0
    iput-boolean p2, p0, Lkvc;->c:Z

    .line 78
    iput-object p3, p0, Lkvc;->d:Lkve;

    .line 79
    iput-object p4, p0, Lkvc;->e:Lkvd;

    .line 80
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x2

    return v0
.end method

.method protected a(II)I
    .locals 0

    .prologue
    .line 94
    return p1
.end method

.method protected a(Landroid/content/Context;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 125
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 127
    packed-switch p2, :pswitch_data_0

    .line 133
    const v1, 0x7f0401f9

    invoke-virtual {v0, v1, p5, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    .line 129
    :pswitch_0
    const v1, 0x7f0401fa

    invoke-virtual {v0, v1, p5, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 127
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public a(Landroid/database/Cursor;Ljava/lang/String;I)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 103
    invoke-virtual {p0, v3, p1}, Lkvc;->a(ILandroid/database/Cursor;)V

    .line 104
    iput-object p2, p0, Lkvc;->f:Ljava/lang/String;

    .line 106
    invoke-virtual {p0}, Lkvc;->c()I

    move-result v0

    .line 107
    new-instance v1, Lhym;

    sget-object v2, Lkvc;->b:[Ljava/lang/String;

    invoke-direct {v1, v2}, Lhym;-><init>([Ljava/lang/String;)V

    .line 108
    if-lez v0, :cond_0

    .line 109
    iget-object v0, p0, Lkvc;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 110
    new-array v0, v5, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v4

    invoke-virtual {v1, v0}, Lhym;->a([Ljava/lang/Object;)V

    .line 115
    :cond_0
    :goto_0
    invoke-virtual {p0, v4, v1}, Lkvc;->a(ILandroid/database/Cursor;)V

    .line 116
    return-void

    .line 111
    :cond_1
    if-lez p3, :cond_0

    .line 112
    new-array v0, v5, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v3

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v4

    invoke-virtual {v1, v0}, Lhym;->a([Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected a(Landroid/view/View;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 141
    packed-switch p2, :pswitch_data_0

    .line 179
    :goto_0
    return-void

    :pswitch_0
    move-object v0, p1

    .line 143
    check-cast v0, Lcom/google/android/libraries/social/squares/members/SquareMemberListItemView;

    .line 145
    invoke-interface {p3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    .line 146
    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    .line 147
    invoke-interface {p3, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lhst;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x4

    .line 148
    invoke-interface {p3, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iget-boolean v5, p0, Lkvc;->c:Z

    .line 144
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/libraries/social/squares/members/SquareMemberListItemView;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)Lcom/google/android/libraries/social/squares/members/SquareMemberListItemView;

    move-result-object v0

    iget-object v1, p0, Lkvc;->d:Lkve;

    .line 149
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/squares/members/SquareMemberListItemView;->a(Lkve;)Lcom/google/android/libraries/social/squares/members/SquareMemberListItemView;

    goto :goto_0

    .line 155
    :pswitch_1
    const-string v0, ""

    .line 156
    invoke-virtual {p0}, Lkvc;->as()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 158
    invoke-interface {p3, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    packed-switch v4, :pswitch_data_1

    move-object v1, v0

    move v0, v2

    .line 172
    :goto_1
    const v3, 0x7f100175

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    if-eqz v0, :cond_0

    :goto_2
    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    .line 177
    const v0, 0x7f100139

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 178
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 161
    :pswitch_2
    const v0, 0x7f0a057b

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 162
    iget-object v3, p0, Lkvc;->e:Lkvd;

    if-eqz v3, :cond_1

    .line 163
    iget-object v3, p0, Lkvc;->e:Lkvd;

    iget-object v4, p0, Lkvc;->f:Ljava/lang/String;

    invoke-interface {v3, v4}, Lkvd;->a(Ljava/lang/String;)V

    move-object v6, v0

    move v0, v1

    move-object v1, v6

    goto :goto_1

    .line 169
    :pswitch_3
    invoke-interface {p3, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 170
    const v4, 0x7f110038

    new-array v1, v1, [Ljava/lang/Object;

    .line 171
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v2

    .line 170
    invoke-virtual {v3, v4, v0, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    move v0, v2

    goto :goto_1

    .line 172
    :cond_0
    const/16 v2, 0x8

    goto :goto_2

    :cond_1
    move-object v6, v0

    move v0, v1

    move-object v1, v6

    goto :goto_1

    .line 141
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 158
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 83
    iput-boolean p1, p0, Lkvc;->c:Z

    .line 84
    invoke-virtual {p0}, Lkvc;->notifyDataSetChanged()V

    .line 85
    return-void
.end method

.method public b()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 98
    invoke-virtual {p0, v0}, Lkvc;->l(I)Landroid/database/Cursor;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method protected b(II)Z
    .locals 1

    .prologue
    .line 186
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 187
    const/4 v0, 0x0

    .line 189
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lhya;->b(II)Z

    move-result v0

    goto :goto_0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 119
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lkvc;->a(I)I

    move-result v0

    return v0
.end method
