.class public final Leof;
.super Lenl;
.source "PG"


# instance fields
.field private aA:Landroid/widget/RadioGroup;

.field private aB:[Landroid/widget/CheckBox;

.field private an:I

.field private ao:Lnip;

.field private ap:Lnip;

.field private aq:Lnjk;

.field private ar:Lnjk;

.field private as:Lnjl;

.field private aw:Lnjl;

.field private ax:Landroid/view/ViewGroup;

.field private ay:Landroid/widget/TextView;

.field private az:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lenl;-><init>()V

    return-void
.end method

.method private a(II)Landroid/widget/CheckBox;
    .locals 2

    .prologue
    .line 414
    new-instance v0, Landroid/widget/CheckBox;

    invoke-virtual {p0}, Leof;->n()Lz;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/CheckBox;-><init>(Landroid/content/Context;)V

    .line 415
    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setId(I)V

    .line 416
    invoke-virtual {p0, p2}, Leof;->e_(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 417
    iget-object v1, p0, Leof;->az:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 418
    return-object v0
.end method

.method private a(Landroid/widget/RadioGroup;II)V
    .locals 2

    .prologue
    .line 407
    new-instance v0, Landroid/widget/RadioButton;

    invoke-virtual {p0}, Leof;->n()Lz;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/RadioButton;-><init>(Landroid/content/Context;)V

    .line 408
    invoke-virtual {v0, p2}, Landroid/widget/RadioButton;->setId(I)V

    .line 409
    invoke-virtual {p0, p3}, Leof;->e_(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 410
    invoke-virtual {p1, v0}, Landroid/widget/RadioGroup;->addView(Landroid/view/View;)V

    .line 411
    return-void
.end method

.method private static e(I)I
    .locals 5

    .prologue
    const/4 v4, 0x5

    const/4 v3, 0x4

    const/4 v2, 0x0

    const/4 v1, 0x3

    const/4 v0, 0x2

    .line 54
    sparse-switch p0, :sswitch_data_0

    move v0, v2

    .line 91
    :goto_0
    :sswitch_0
    return v0

    .line 56
    :sswitch_1
    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    move v0, v1

    .line 60
    goto :goto_0

    :sswitch_3
    move v0, v2

    .line 62
    goto :goto_0

    :sswitch_4
    move v0, v1

    .line 66
    goto :goto_0

    :sswitch_5
    move v0, v3

    .line 68
    goto :goto_0

    :sswitch_6
    move v0, v4

    .line 70
    goto :goto_0

    .line 72
    :sswitch_7
    const/4 v0, 0x6

    goto :goto_0

    .line 74
    :sswitch_8
    const/4 v0, 0x7

    goto :goto_0

    .line 76
    :sswitch_9
    const/16 v0, 0x8

    goto :goto_0

    .line 78
    :sswitch_a
    const/16 v0, 0x9

    goto :goto_0

    .line 80
    :sswitch_b
    const/16 v0, 0xa

    goto :goto_0

    :sswitch_c
    move v0, v1

    .line 84
    goto :goto_0

    :sswitch_d
    move v0, v3

    .line 86
    goto :goto_0

    :sswitch_e
    move v0, v4

    .line 88
    goto :goto_0

    .line 54
    nop

    :sswitch_data_0
    .sparse-switch
        0x3e8 -> :sswitch_1
        0x3e9 -> :sswitch_0
        0x3ea -> :sswitch_2
        0x7d0 -> :sswitch_3
        0x7d1 -> :sswitch_0
        0x7d2 -> :sswitch_4
        0x7d3 -> :sswitch_5
        0x7d4 -> :sswitch_6
        0x7d5 -> :sswitch_7
        0x7d6 -> :sswitch_8
        0x7d7 -> :sswitch_9
        0x7d8 -> :sswitch_a
        0x7d9 -> :sswitch_b
        0xbb8 -> :sswitch_0
        0xbb9 -> :sswitch_c
        0xbba -> :sswitch_d
        0xbbb -> :sswitch_e
    .end sparse-switch
.end method

.method private static f(I)I
    .locals 1

    .prologue
    .line 95
    packed-switch p0, :pswitch_data_0

    .line 103
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 97
    :pswitch_0
    const/16 v0, 0x3e8

    goto :goto_0

    .line 99
    :pswitch_1
    const/16 v0, 0x3e9

    goto :goto_0

    .line 101
    :pswitch_2
    const/16 v0, 0x3ea

    goto :goto_0

    .line 95
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public U()V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 423
    invoke-super {p0}, Lenl;->U()V

    .line 425
    iget v0, p0, Leof;->an:I

    sparse-switch v0, :sswitch_data_0

    .line 446
    :cond_0
    :goto_0
    return-void

    .line 427
    :sswitch_0
    iget-object v0, p0, Leof;->ao:Lnip;

    iget v0, v0, Lnip;->b:I

    invoke-static {v0}, Leof;->f(I)I

    move-result v0

    .line 428
    new-instance v1, Lenv;

    invoke-direct {v1, p0, v0}, Lenv;-><init>(Lenl;I)V

    .line 429
    iget-object v0, p0, Leof;->aA:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    goto :goto_0

    :sswitch_1
    move v0, v1

    .line 433
    :goto_1
    iget-object v2, p0, Leof;->aB:[Landroid/widget/CheckBox;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 434
    iget-object v2, p0, Leof;->aB:[Landroid/widget/CheckBox;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Landroid/widget/CheckBox;->getId()I

    move-result v2

    invoke-static {v2}, Leof;->e(I)I

    move-result v3

    .line 435
    iget-object v2, p0, Leof;->aq:Lnjk;

    iget-object v4, v2, Lnjk;->b:[Lnjj;

    .line 436
    if-eqz v4, :cond_2

    array-length v5, v4

    move v2, v1

    :goto_2
    if-ge v2, v5, :cond_2

    aget-object v6, v4, v2

    iget v6, v6, Lnjj;->b:I

    if-ne v6, v3, :cond_1

    const/4 v2, 0x1

    .line 437
    :goto_3
    new-instance v3, Lenp;

    invoke-direct {v3, p0, v2}, Lenp;-><init>(Lenl;Z)V

    .line 438
    iget-object v2, p0, Leof;->aB:[Landroid/widget/CheckBox;

    aget-object v2, v2, v0

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 433
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 436
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_2
    move v2, v1

    goto :goto_3

    .line 443
    :sswitch_2
    iget-object v0, p0, Leof;->as:Lnjl;

    iget v0, v0, Lnjl;->b:I

    invoke-static {v0}, Leof;->f(I)I

    move-result v0

    .line 444
    new-instance v1, Lenv;

    invoke-direct {v1, p0, v0}, Lenv;-><init>(Lenl;I)V

    .line 445
    iget-object v0, p0, Leof;->aA:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    goto :goto_0

    .line 425
    nop

    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_2
        0xa -> :sswitch_0
        0x13 -> :sswitch_1
    .end sparse-switch
.end method

.method protected V()V
    .locals 5

    .prologue
    const/high16 v4, -0x80000000

    const/4 v0, 0x0

    .line 464
    invoke-super {p0}, Lenl;->V()V

    .line 469
    new-instance v1, Lnjt;

    invoke-direct {v1}, Lnjt;-><init>()V

    .line 470
    new-instance v2, Lnkd;

    invoke-direct {v2}, Lnkd;-><init>()V

    iput-object v2, v1, Lnjt;->e:Lnkd;

    .line 472
    iget v2, p0, Leof;->an:I

    sparse-switch v2, :sswitch_data_0

    .line 512
    :goto_0
    invoke-virtual {p0, v1}, Leof;->a(Lnjt;)V

    .line 513
    return-void

    .line 474
    :sswitch_0
    iget-object v2, v1, Lnjt;->e:Lnkd;

    new-instance v3, Lnip;

    invoke-direct {v3}, Lnip;-><init>()V

    iput-object v3, v2, Lnkd;->d:Lnip;

    .line 475
    iget-object v2, v1, Lnjt;->e:Lnkd;

    iget-object v2, v2, Lnkd;->d:Lnip;

    iget-object v3, p0, Leof;->aA:Landroid/widget/RadioGroup;

    invoke-virtual {v3}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v3

    invoke-static {v3}, Leof;->e(I)I

    move-result v3

    iput v3, v2, Lnip;->b:I

    .line 476
    iget-object v2, v1, Lnjt;->e:Lnkd;

    iget-object v2, v2, Lnkd;->d:Lnip;

    iget v2, v2, Lnip;->b:I

    if-ne v2, v4, :cond_0

    .line 477
    iget-object v2, v1, Lnjt;->e:Lnkd;

    iget-object v2, v2, Lnkd;->d:Lnip;

    iput v0, v2, Lnip;->b:I

    .line 479
    :cond_0
    iget-object v0, p0, Leof;->ap:Lnip;

    invoke-virtual {p0}, Leof;->aa()Lnja;

    move-result-object v2

    iput-object v2, v0, Lnip;->a:Lnja;

    .line 480
    iget-object v0, v1, Lnjt;->e:Lnkd;

    iget-object v0, v0, Lnkd;->d:Lnip;

    iget-object v2, p0, Leof;->ap:Lnip;

    iget-object v2, v2, Lnip;->a:Lnja;

    iput-object v2, v0, Lnip;->a:Lnja;

    goto :goto_0

    .line 484
    :sswitch_1
    iget-object v2, v1, Lnjt;->e:Lnkd;

    new-instance v3, Lnjk;

    invoke-direct {v3}, Lnjk;-><init>()V

    iput-object v3, v2, Lnkd;->l:Lnjk;

    .line 485
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 486
    :goto_1
    iget-object v3, p0, Leof;->aB:[Landroid/widget/CheckBox;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 487
    iget-object v3, p0, Leof;->aB:[Landroid/widget/CheckBox;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 488
    new-instance v3, Lnjj;

    invoke-direct {v3}, Lnjj;-><init>()V

    .line 489
    iget-object v4, p0, Leof;->aB:[Landroid/widget/CheckBox;

    aget-object v4, v4, v0

    invoke-virtual {v4}, Landroid/widget/CheckBox;->getId()I

    move-result v4

    invoke-static {v4}, Leof;->e(I)I

    move-result v4

    iput v4, v3, Lnjj;->b:I

    .line 490
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 486
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 493
    :cond_2
    iget-object v0, v1, Lnjt;->e:Lnkd;

    iget-object v3, v0, Lnkd;->l:Lnjk;

    .line 494
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lnjj;

    .line 493
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lnjj;

    iput-object v0, v3, Lnjk;->b:[Lnjj;

    .line 495
    iget-object v0, p0, Leof;->ar:Lnjk;

    invoke-virtual {p0}, Leof;->aa()Lnja;

    move-result-object v2

    iput-object v2, v0, Lnjk;->a:Lnja;

    .line 496
    iget-object v0, v1, Lnjt;->e:Lnkd;

    iget-object v0, v0, Lnkd;->l:Lnjk;

    iget-object v2, p0, Leof;->ar:Lnjk;

    iget-object v2, v2, Lnjk;->a:Lnja;

    iput-object v2, v0, Lnjk;->a:Lnja;

    goto/16 :goto_0

    .line 500
    :sswitch_2
    iget-object v2, v1, Lnjt;->e:Lnkd;

    new-instance v3, Lnjl;

    invoke-direct {v3}, Lnjl;-><init>()V

    iput-object v3, v2, Lnkd;->k:Lnjl;

    .line 501
    iget-object v2, v1, Lnjt;->e:Lnkd;

    iget-object v2, v2, Lnkd;->k:Lnjl;

    iget-object v3, p0, Leof;->aA:Landroid/widget/RadioGroup;

    .line 502
    invoke-virtual {v3}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v3

    invoke-static {v3}, Leof;->e(I)I

    move-result v3

    iput v3, v2, Lnjl;->b:I

    .line 503
    iget-object v2, v1, Lnjt;->e:Lnkd;

    iget-object v2, v2, Lnkd;->k:Lnjl;

    iget v2, v2, Lnjl;->b:I

    if-ne v2, v4, :cond_3

    .line 504
    iget-object v2, v1, Lnjt;->e:Lnkd;

    iget-object v2, v2, Lnkd;->k:Lnjl;

    iput v0, v2, Lnjl;->b:I

    .line 506
    :cond_3
    iget-object v0, p0, Leof;->aw:Lnjl;

    invoke-virtual {p0}, Leof;->aa()Lnja;

    move-result-object v2

    iput-object v2, v0, Lnjl;->a:Lnja;

    .line 507
    iget-object v0, v1, Lnjt;->e:Lnkd;

    iget-object v0, v0, Lnkd;->k:Lnjl;

    iget-object v2, p0, Leof;->aw:Lnjl;

    iget-object v2, v2, Lnjl;->a:Lnja;

    iput-object v2, v0, Lnjl;->a:Lnja;

    goto/16 :goto_0

    .line 472
    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_2
        0xa -> :sswitch_0
        0x13 -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 173
    .line 175
    iget v1, p0, Leof;->an:I

    sparse-switch v1, :sswitch_data_0

    move-object v1, v0

    .line 190
    :goto_0
    invoke-virtual {p0, v1}, Leof;->a(Lnja;)Lhgw;

    move-result-object v1

    invoke-static {v1}, Lhgw;->a(Lhgw;)Lhgw;

    move-result-object v1

    iput-object v1, p0, Leof;->Y:Lhgw;

    .line 191
    iget-object v1, p0, Leof;->aj:Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->h()Lhgw;

    move-result-object v1

    .line 192
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lhgw;->k()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 193
    :cond_0
    invoke-virtual {p0, v0}, Leof;->a(Lnja;)Lhgw;

    move-result-object v0

    invoke-static {v0}, Lhgw;->a(Lhgw;)Lhgw;

    move-result-object v0

    .line 195
    :goto_1
    invoke-static {v0}, Lhgw;->a(Lhgw;)Lhgw;

    move-result-object v0

    .line 196
    iget-object v1, p0, Leof;->aj:Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->a(Lhgw;)V

    .line 198
    iget-object v0, p0, Leof;->aj:Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->setEnabled(Z)V

    .line 199
    return-void

    .line 177
    :sswitch_0
    iget-object v0, p0, Leof;->ao:Lnip;

    iget-object v1, v0, Lnip;->a:Lnja;

    .line 178
    iget-object v0, p0, Leof;->ap:Lnip;

    iget-object v0, v0, Lnip;->a:Lnja;

    goto :goto_0

    .line 181
    :sswitch_1
    iget-object v0, p0, Leof;->aq:Lnjk;

    iget-object v1, v0, Lnjk;->a:Lnja;

    .line 182
    iget-object v0, p0, Leof;->ar:Lnjk;

    iget-object v0, v0, Lnjk;->a:Lnja;

    goto :goto_0

    .line 185
    :sswitch_2
    iget-object v0, p0, Leof;->as:Lnjl;

    iget-object v1, v0, Lnjl;->a:Lnja;

    .line 186
    iget-object v0, p0, Leof;->aw:Lnjl;

    iget-object v0, v0, Lnjl;->a:Lnja;

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_1

    .line 175
    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_2
        0xa -> :sswitch_0
        0x13 -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 166
    invoke-virtual {p0}, Leof;->k()Landroid/os/Bundle;

    move-result-object v0

    .line 167
    const-string v1, "profile_edit_view_type"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Leof;->an:I

    .line 168
    invoke-super {p0, p1}, Lenl;->a(Landroid/os/Bundle;)V

    .line 169
    return-void
.end method

.method protected c()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 203
    iget-object v0, p0, Leof;->Z:[B

    if-eqz v0, :cond_0

    .line 204
    iget v0, p0, Leof;->an:I

    sparse-switch v0, :sswitch_data_0

    .line 242
    :cond_0
    :goto_0
    return-void

    .line 206
    :sswitch_0
    iput-object v1, p0, Leof;->ap:Lnip;

    .line 208
    :try_start_0
    new-instance v0, Lnip;

    invoke-direct {v0}, Lnip;-><init>()V

    iget-object v1, p0, Leof;->Z:[B

    invoke-static {v0, v1}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lnip;

    iput-object v0, p0, Leof;->ap:Lnip;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 212
    :goto_1
    iget-object v0, p0, Leof;->ap:Lnip;

    if-nez v0, :cond_0

    .line 213
    new-instance v0, Lnip;

    invoke-direct {v0}, Lnip;-><init>()V

    iput-object v0, p0, Leof;->ap:Lnip;

    goto :goto_0

    .line 217
    :sswitch_1
    iput-object v1, p0, Leof;->ar:Lnjk;

    .line 219
    :try_start_1
    new-instance v0, Lnjk;

    invoke-direct {v0}, Lnjk;-><init>()V

    iget-object v1, p0, Leof;->Z:[B

    .line 220
    invoke-static {v0, v1}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lnjk;

    iput-object v0, p0, Leof;->ar:Lnjk;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 224
    :goto_2
    iget-object v0, p0, Leof;->ar:Lnjk;

    if-nez v0, :cond_0

    .line 225
    new-instance v0, Lnjk;

    invoke-direct {v0}, Lnjk;-><init>()V

    iput-object v0, p0, Leof;->ar:Lnjk;

    goto :goto_0

    .line 229
    :sswitch_2
    iput-object v1, p0, Leof;->aw:Lnjl;

    .line 231
    :try_start_2
    new-instance v0, Lnjl;

    invoke-direct {v0}, Lnjl;-><init>()V

    iget-object v1, p0, Leof;->Z:[B

    invoke-static {v0, v1}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lnjl;

    iput-object v0, p0, Leof;->aw:Lnjl;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 236
    :goto_3
    iget-object v0, p0, Leof;->aw:Lnjl;

    if-nez v0, :cond_0

    .line 237
    new-instance v0, Lnjl;

    invoke-direct {v0}, Lnjl;-><init>()V

    iput-object v0, p0, Leof;->aw:Lnjl;

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_3

    :catch_1
    move-exception v0

    goto :goto_2

    :catch_2
    move-exception v0

    goto :goto_1

    .line 204
    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_2
        0xa -> :sswitch_0
        0x13 -> :sswitch_1
    .end sparse-switch
.end method

.method protected d()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 246
    invoke-super {p0}, Lenl;->d()V

    .line 250
    iget-object v0, p0, Leof;->aa:[B

    if-eqz v0, :cond_0

    .line 251
    iget v0, p0, Leof;->an:I

    sparse-switch v0, :sswitch_data_0

    .line 293
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    iget-object v0, v1, Lnja;->a:Lock;

    if-eqz v0, :cond_1

    .line 295
    iget-object v0, v1, Lnja;->a:Lock;

    iget-object v0, v0, Lock;->b:Locn;

    iput-object v0, p0, Leof;->X:Locn;

    .line 297
    :cond_1
    return-void

    .line 253
    :sswitch_0
    iput-object v1, p0, Leof;->ao:Lnip;

    .line 255
    :try_start_0
    new-instance v0, Lnip;

    invoke-direct {v0}, Lnip;-><init>()V

    iget-object v2, p0, Leof;->aa:[B

    invoke-static {v0, v2}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lnip;

    iput-object v0, p0, Leof;->ao:Lnip;

    .line 256
    iget-object v0, p0, Leof;->ao:Lnip;

    iget-object v0, v0, Lnip;->a:Lnja;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 260
    :goto_1
    iget-object v1, p0, Leof;->ao:Lnip;

    if-nez v1, :cond_2

    .line 261
    new-instance v1, Lnip;

    invoke-direct {v1}, Lnip;-><init>()V

    iput-object v1, p0, Leof;->ao:Lnip;

    move-object v1, v0

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_1

    .line 265
    :sswitch_1
    iput-object v1, p0, Leof;->aq:Lnjk;

    .line 267
    :try_start_1
    new-instance v0, Lnjk;

    invoke-direct {v0}, Lnjk;-><init>()V

    iget-object v2, p0, Leof;->aa:[B

    invoke-static {v0, v2}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lnjk;

    iput-object v0, p0, Leof;->aq:Lnjk;

    .line 269
    iget-object v0, p0, Leof;->aq:Lnjk;

    iget-object v1, v0, Lnjk;->a:Lnja;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 273
    :goto_2
    iget-object v0, p0, Leof;->aq:Lnjk;

    if-nez v0, :cond_0

    .line 274
    new-instance v0, Lnjk;

    invoke-direct {v0}, Lnjk;-><init>()V

    iput-object v0, p0, Leof;->aq:Lnjk;

    goto :goto_0

    .line 278
    :sswitch_2
    iput-object v1, p0, Leof;->as:Lnjl;

    .line 280
    :try_start_2
    new-instance v0, Lnjl;

    invoke-direct {v0}, Lnjl;-><init>()V

    iget-object v2, p0, Leof;->aa:[B

    invoke-static {v0, v2}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lnjl;

    iput-object v0, p0, Leof;->as:Lnjl;

    .line 282
    iget-object v0, p0, Leof;->as:Lnjl;

    iget-object v1, v0, Lnjl;->a:Lnja;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 286
    :goto_3
    iget-object v0, p0, Leof;->as:Lnjl;

    if-nez v0, :cond_0

    .line 287
    new-instance v0, Lnjl;

    invoke-direct {v0}, Lnjl;-><init>()V

    iput-object v0, p0, Leof;->as:Lnjl;

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_3

    :catch_2
    move-exception v0

    goto :goto_2

    :cond_2
    move-object v1, v0

    goto :goto_0

    .line 251
    nop

    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_2
        0xa -> :sswitch_0
        0x13 -> :sswitch_1
    .end sparse-switch
.end method

.method protected e()V
    .locals 10

    .prologue
    const/16 v3, 0x7d1

    const/16 v2, 0x7d0

    const/high16 v9, -0x80000000

    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 301
    invoke-super {p0}, Lenl;->e()V

    .line 303
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Leof;->a_(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v4, 0x7f0401b8

    iget-object v5, p0, Leof;->ai:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4, v5, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Leof;->ax:Landroid/view/ViewGroup;

    .line 305
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v4, -0x1

    const/4 v5, -0x2

    invoke-direct {v0, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 307
    sget v4, Leof;->R:I

    sget v5, Leof;->R:I

    sget v6, Leof;->R:I

    sget v7, Leof;->R:I

    invoke-virtual {v0, v4, v5, v6, v7}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 309
    iget-object v4, p0, Leof;->ai:Landroid/widget/LinearLayout;

    iget-object v5, p0, Leof;->ax:Landroid/view/ViewGroup;

    invoke-virtual {v4, v5, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 311
    iget-object v0, p0, Leof;->ax:Landroid/view/ViewGroup;

    const v4, 0x7f100118

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Leof;->ay:Landroid/widget/TextView;

    .line 312
    iget-object v0, p0, Leof;->ax:Landroid/view/ViewGroup;

    const v4, 0x7f10051f

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Leof;->az:Landroid/view/ViewGroup;

    .line 314
    invoke-virtual {p0}, Leof;->o()Landroid/content/res/Resources;

    move-result-object v0

    .line 315
    iget-object v4, p0, Leof;->ax:Landroid/view/ViewGroup;

    const v5, 0x7f0200be

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/view/ViewGroup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 317
    iget-object v0, p0, Leof;->az:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 320
    iget v0, p0, Leof;->an:I

    sparse-switch v0, :sswitch_data_0

    move v0, v1

    .line 398
    :goto_0
    if-eqz v0, :cond_5

    .line 399
    iget-object v2, p0, Leof;->ay:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(I)V

    .line 400
    iget-object v0, p0, Leof;->ay:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 404
    :goto_1
    return-void

    .line 322
    :sswitch_0
    const v2, 0x7f0a031d

    .line 324
    new-instance v0, Landroid/widget/RadioGroup;

    invoke-virtual {p0}, Leof;->n()Lz;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/widget/RadioGroup;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Leof;->aA:Landroid/widget/RadioGroup;

    .line 325
    iget-object v0, p0, Leof;->aA:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v8}, Landroid/widget/RadioGroup;->setOrientation(I)V

    .line 326
    iget-object v0, p0, Leof;->az:Landroid/view/ViewGroup;

    iget-object v3, p0, Leof;->aA:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 327
    iget-object v0, p0, Leof;->aA:Landroid/widget/RadioGroup;

    const/16 v3, 0x3e8

    const v4, 0x7f0a031f

    invoke-direct {p0, v0, v3, v4}, Leof;->a(Landroid/widget/RadioGroup;II)V

    .line 328
    iget-object v0, p0, Leof;->aA:Landroid/widget/RadioGroup;

    const/16 v3, 0x3e9

    const v4, 0x7f0a0320

    invoke-direct {p0, v0, v3, v4}, Leof;->a(Landroid/widget/RadioGroup;II)V

    .line 329
    iget-object v0, p0, Leof;->aA:Landroid/widget/RadioGroup;

    const/16 v3, 0x3ea

    const v4, 0x7f0a0321

    invoke-direct {p0, v0, v3, v4}, Leof;->a(Landroid/widget/RadioGroup;II)V

    .line 331
    iget-object v0, p0, Leof;->ap:Lnip;

    iget v0, v0, Lnip;->b:I

    if-eq v0, v9, :cond_0

    iget-object v0, p0, Leof;->ap:Lnip;

    iget v0, v0, Lnip;->b:I

    .line 332
    invoke-static {v0}, Leof;->f(I)I

    move-result v0

    .line 333
    :goto_2
    iget-object v3, p0, Leof;->aA:Landroid/widget/RadioGroup;

    invoke-virtual {v3, v0}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    .line 334
    if-eqz v0, :cond_6

    .line 335
    invoke-virtual {v0, v8}, Landroid/widget/RadioButton;->setChecked(Z)V

    move v0, v2

    goto :goto_0

    :cond_0
    move v0, v1

    .line 332
    goto :goto_2

    .line 340
    :sswitch_1
    const v2, 0x7f0a0322

    .line 342
    const/4 v0, 0x4

    new-array v0, v0, [Landroid/widget/CheckBox;

    const/16 v3, 0xbb8

    const v4, 0x7f0a0325

    .line 343
    invoke-direct {p0, v3, v4}, Leof;->a(II)Landroid/widget/CheckBox;

    move-result-object v3

    aput-object v3, v0, v1

    const/16 v3, 0xbb9

    const v4, 0x7f0a0326

    .line 344
    invoke-direct {p0, v3, v4}, Leof;->a(II)Landroid/widget/CheckBox;

    move-result-object v3

    aput-object v3, v0, v8

    const/4 v3, 0x2

    const/16 v4, 0xbba

    const v5, 0x7f0a0327

    .line 345
    invoke-direct {p0, v4, v5}, Leof;->a(II)Landroid/widget/CheckBox;

    move-result-object v4

    aput-object v4, v0, v3

    const/4 v3, 0x3

    const/16 v4, 0xbbb

    const v5, 0x7f0a0328

    .line 347
    invoke-direct {p0, v4, v5}, Leof;->a(II)Landroid/widget/CheckBox;

    move-result-object v4

    aput-object v4, v0, v3

    iput-object v0, p0, Leof;->aB:[Landroid/widget/CheckBox;

    .line 350
    iget-object v0, p0, Leof;->ar:Lnjk;

    iget-object v0, v0, Lnjk;->b:[Lnjj;

    if-eqz v0, :cond_6

    .line 351
    iget-object v0, p0, Leof;->ar:Lnjk;

    iget-object v4, v0, Lnjk;->b:[Lnjj;

    array-length v5, v4

    move v3, v1

    :goto_3
    if-ge v3, v5, :cond_2

    aget-object v0, v4, v3

    .line 352
    iget v0, v0, Lnjj;->b:I

    packed-switch v0, :pswitch_data_0

    move v0, v1

    .line 353
    :goto_4
    iget-object v6, p0, Leof;->az:Landroid/view/ViewGroup;

    invoke-virtual {v6, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 354
    if-eqz v0, :cond_1

    .line 355
    invoke-virtual {v0, v8}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 351
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    .line 352
    :pswitch_0
    const/16 v0, 0xbb8

    goto :goto_4

    :pswitch_1
    const/16 v0, 0xbb9

    goto :goto_4

    :pswitch_2
    const/16 v0, 0xbba

    goto :goto_4

    :pswitch_3
    const/16 v0, 0xbbb

    goto :goto_4

    :cond_2
    move v0, v2

    .line 351
    goto/16 :goto_0

    .line 362
    :sswitch_2
    const v4, 0x7f0a032a

    .line 364
    new-instance v0, Landroid/widget/RadioGroup;

    invoke-virtual {p0}, Leof;->n()Lz;

    move-result-object v5

    invoke-direct {v0, v5}, Landroid/widget/RadioGroup;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Leof;->aA:Landroid/widget/RadioGroup;

    .line 365
    iget-object v0, p0, Leof;->aA:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v8}, Landroid/widget/RadioGroup;->setOrientation(I)V

    .line 366
    iget-object v0, p0, Leof;->az:Landroid/view/ViewGroup;

    iget-object v5, p0, Leof;->aA:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 367
    iget-object v0, p0, Leof;->aA:Landroid/widget/RadioGroup;

    const v5, 0x7f0a032d

    invoke-direct {p0, v0, v2, v5}, Leof;->a(Landroid/widget/RadioGroup;II)V

    .line 369
    iget-object v0, p0, Leof;->aA:Landroid/widget/RadioGroup;

    const v5, 0x7f0a032e

    invoke-direct {p0, v0, v3, v5}, Leof;->a(Landroid/widget/RadioGroup;II)V

    .line 371
    iget-object v0, p0, Leof;->aA:Landroid/widget/RadioGroup;

    const/16 v5, 0x7d2

    const v6, 0x7f0a032f

    invoke-direct {p0, v0, v5, v6}, Leof;->a(Landroid/widget/RadioGroup;II)V

    .line 373
    iget-object v0, p0, Leof;->aA:Landroid/widget/RadioGroup;

    const/16 v5, 0x7d3

    const v6, 0x7f0a0330

    invoke-direct {p0, v0, v5, v6}, Leof;->a(Landroid/widget/RadioGroup;II)V

    .line 375
    iget-object v0, p0, Leof;->aA:Landroid/widget/RadioGroup;

    const/16 v5, 0x7d4

    const v6, 0x7f0a0331

    invoke-direct {p0, v0, v5, v6}, Leof;->a(Landroid/widget/RadioGroup;II)V

    .line 377
    iget-object v0, p0, Leof;->aA:Landroid/widget/RadioGroup;

    const/16 v5, 0x7d5

    const v6, 0x7f0a0332

    invoke-direct {p0, v0, v5, v6}, Leof;->a(Landroid/widget/RadioGroup;II)V

    .line 379
    iget-object v0, p0, Leof;->aA:Landroid/widget/RadioGroup;

    const/16 v5, 0x7d6

    const v6, 0x7f0a0333

    invoke-direct {p0, v0, v5, v6}, Leof;->a(Landroid/widget/RadioGroup;II)V

    .line 381
    iget-object v0, p0, Leof;->aA:Landroid/widget/RadioGroup;

    const/16 v5, 0x7d7

    const v6, 0x7f0a0334

    invoke-direct {p0, v0, v5, v6}, Leof;->a(Landroid/widget/RadioGroup;II)V

    .line 383
    iget-object v0, p0, Leof;->aA:Landroid/widget/RadioGroup;

    const/16 v5, 0x7d8

    const v6, 0x7f0a0335

    invoke-direct {p0, v0, v5, v6}, Leof;->a(Landroid/widget/RadioGroup;II)V

    .line 385
    iget-object v0, p0, Leof;->aA:Landroid/widget/RadioGroup;

    const/16 v5, 0x7d9

    const v6, 0x7f0a0336

    invoke-direct {p0, v0, v5, v6}, Leof;->a(Landroid/widget/RadioGroup;II)V

    .line 388
    iget-object v0, p0, Leof;->aw:Lnjl;

    iget v0, v0, Lnjl;->b:I

    if-eq v0, v9, :cond_4

    iget-object v0, p0, Leof;->aw:Lnjl;

    iget v0, v0, Lnjl;->b:I

    .line 389
    packed-switch v0, :pswitch_data_1

    :pswitch_4
    move v0, v1

    .line 390
    :goto_5
    iget-object v2, p0, Leof;->aA:Landroid/widget/RadioGroup;

    invoke-virtual {v2, v0}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    .line 391
    if-eqz v0, :cond_3

    .line 392
    invoke-virtual {v0, v8}, Landroid/widget/RadioButton;->setChecked(Z)V

    :cond_3
    move v0, v4

    goto/16 :goto_0

    :pswitch_5
    move v0, v2

    .line 389
    goto :goto_5

    :pswitch_6
    move v0, v3

    goto :goto_5

    :pswitch_7
    const/16 v0, 0x7d2

    goto :goto_5

    :pswitch_8
    const/16 v0, 0x7d3

    goto :goto_5

    :pswitch_9
    const/16 v0, 0x7d4

    goto :goto_5

    :pswitch_a
    const/16 v0, 0x7d5

    goto :goto_5

    :pswitch_b
    const/16 v0, 0x7d6

    goto :goto_5

    :pswitch_c
    const/16 v0, 0x7d7

    goto :goto_5

    :pswitch_d
    const/16 v0, 0x7d8

    goto :goto_5

    :pswitch_e
    const/16 v0, 0x7d9

    goto :goto_5

    :cond_4
    move v0, v1

    goto :goto_5

    .line 402
    :cond_5
    iget-object v0, p0, Leof;->ay:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    :cond_6
    move v0, v2

    goto/16 :goto_0

    .line 320
    nop

    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_2
        0xa -> :sswitch_0
        0x13 -> :sswitch_1
    .end sparse-switch

    .line 352
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 389
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_5
        :pswitch_4
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method
