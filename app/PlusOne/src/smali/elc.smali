.class public final Lelc;
.super Leku;
.source "PG"


# instance fields
.field private N:I

.field private O:I

.field private ad:Z

.field private ae:Liax;

.field private final af:J

.field private ag:Liwk;

.field private ah:Leli;

.field private ai:Z

.field private final aj:Lbc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbc",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Ldvw;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 408
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lelc;-><init>(Z)V

    .line 409
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 3

    .prologue
    .line 411
    invoke-direct {p0}, Leku;-><init>()V

    .line 67
    new-instance v0, Lhmg;

    sget-object v1, Lonl;->g:Lhmn;

    invoke-direct {v0, v1}, Lhmg;-><init>(Lhmn;)V

    iget-object v1, p0, Lelc;->au:Llnh;

    .line 68
    invoke-virtual {v0, v1}, Lhmg;->a(Llnh;)Lhmg;

    .line 70
    new-instance v0, Lhmf;

    iget-object v1, p0, Lelc;->av:Llqm;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lhmf;-><init>(Llqr;B)V

    .line 93
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lelc;->af:J

    .line 329
    const/4 v0, 0x1

    iput-boolean v0, p0, Lelc;->ai:Z

    .line 331
    new-instance v0, Leld;

    invoke-direct {v0, p0}, Leld;-><init>(Lelc;)V

    iput-object v0, p0, Lelc;->aj:Lbc;

    .line 412
    iput-boolean p1, p0, Lelc;->ac:Z

    .line 413
    return-void
.end method

.method private X()V
    .locals 3

    .prologue
    .line 609
    iget-object v0, p0, Lelc;->Z:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    .line 610
    iget-object v1, p0, Lelc;->at:Llnl;

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Leyq;->d(Landroid/content/Context;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 611
    invoke-virtual {p0, v0}, Lelc;->a(Landroid/content/Intent;)V

    .line 612
    return-void
.end method

.method static synthetic a(Lelc;)Llnl;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lelc;->at:Llnl;

    return-object v0
.end method

.method static synthetic a(Lelc;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 5

    .prologue
    .line 64
    iget-object v0, p0, Lelc;->ah:Leli;

    invoke-virtual {v0, p1, p2}, Leli;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    iget-object v0, p0, Lelc;->ah:Leli;

    invoke-virtual {v0}, Leli;->notifyDataSetChanged()V

    invoke-virtual {p0}, Lelc;->x()Landroid/view/View;

    invoke-virtual {p0}, Lelc;->aa()V

    iget-object v0, p0, Lelc;->Q:Lhje;

    invoke-virtual {v0}, Lhje;->a()V

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnrp;

    iget-object v1, v0, Lnrp;->h:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    iget-object v1, v0, Lnrp;->h:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_2

    iget v1, v0, Lnrp;->g:I

    if-nez v1, :cond_3

    const/4 v1, 0x1

    :goto_2
    iget-object v3, p0, Lelc;->ae:Liax;

    iget-object v4, v0, Lnrp;->b:Lohv;

    iget-object v4, v4, Lohv;->b:Lohp;

    invoke-static {v4}, Ldsm;->a(Lohp;)Ljava/lang/String;

    move-result-object v4

    iget-object v0, v0, Lnrp;->e:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lelc;->e(I)I

    move-result v1

    invoke-interface {v3, v4, v0, v1}, Liax;->b(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_1

    :cond_3
    const/16 v1, 0xd

    goto :goto_2

    :cond_4
    new-instance v0, Ldqa;

    iget-object v1, p0, Lelc;->at:Llnl;

    iget-object v2, p0, Lelc;->Z:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    invoke-direct {v0, v1, v2}, Ldqa;-><init>(Landroid/content/Context;I)V

    iget-object v1, p0, Lelc;->at:Llnl;

    invoke-static {v1, v0}, Lhoc;->a(Landroid/content/Context;Lhny;)V

    goto :goto_0
.end method

.method static synthetic a(Lelc;Z)Z
    .locals 0

    .prologue
    .line 64
    iput-boolean p1, p0, Lelc;->ai:Z

    return p1
.end method

.method static synthetic b(Lelc;)Llnl;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lelc;->at:Llnl;

    return-object v0
.end method

.method static synthetic c(Lelc;)Llnl;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lelc;->at:Llnl;

    return-object v0
.end method

.method static synthetic d(Lelc;)V
    .locals 5

    .prologue
    const/4 v2, -0x2

    .line 64
    invoke-virtual {p0}, Lelc;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lelc;->x()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f10032c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v2, 0xc

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v2, 0xe

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    iget-object v2, p0, Lelc;->at:Llnl;

    const v3, 0x7f0a05d5

    const v4, 0x7f0a0ad9

    invoke-static {v2, v3, v4, v0, v1}, Lfuw;->a(Landroid/content/Context;IILandroid/view/ViewGroup;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    return-void
.end method

.method static synthetic e(Lelc;)Llnl;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lelc;->at:Llnl;

    return-object v0
.end method

.method static synthetic f(Lelc;)J
    .locals 2

    .prologue
    .line 64
    iget-wide v0, p0, Lelc;->af:J

    return-wide v0
.end method

.method static synthetic g(Lelc;)Llnl;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lelc;->at:Llnl;

    return-object v0
.end method

.method static synthetic h(Lelc;)Liwk;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lelc;->ag:Liwk;

    return-object v0
.end method

.method static synthetic i(Lelc;)Llnl;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lelc;->at:Llnl;

    return-object v0
.end method

.method static synthetic j(Lelc;)Llnl;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lelc;->at:Llnl;

    return-object v0
.end method

.method static synthetic k(Lelc;)Z
    .locals 1

    .prologue
    .line 64
    iget-boolean v0, p0, Lelc;->ai:Z

    return v0
.end method

.method static synthetic l(Lelc;)V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Lelc;->X()V

    return-void
.end method

.method static synthetic m(Lelc;)Llnl;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lelc;->at:Llnl;

    return-object v0
.end method

.method static synthetic n(Lelc;)Llnh;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lelc;->au:Llnh;

    return-object v0
.end method


# virtual methods
.method public A()V
    .locals 5

    .prologue
    .line 450
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 451
    const-string v0, "extra_people_notification_acceptance_per_session"

    iget v2, p0, Lelc;->N:I

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 453
    const-string v0, "extra_people_notification_dismiss_per_session"

    iget v2, p0, Lelc;->O:I

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 456
    iget-object v0, p0, Lelc;->Z:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v2

    .line 457
    iget-object v0, p0, Lelc;->au:Llnh;

    const-class v3, Lhms;

    invoke-virtual {v0, v3}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v3, Lhmr;

    iget-object v4, p0, Lelc;->at:Llnl;

    invoke-direct {v3, v4, v2}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v2, Lhmv;->bY:Lhmv;

    .line 459
    invoke-virtual {v3, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v2

    .line 460
    invoke-virtual {v2, v1}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    .line 457
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 463
    invoke-super {p0}, Leku;->A()V

    .line 464
    return-void
.end method

.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 547
    sget-object v0, Lhmw;->v:Lhmw;

    return-object v0
.end method

.method protected U()Z
    .locals 1

    .prologue
    .line 672
    const/4 v0, 0x1

    return v0
.end method

.method protected Y()I
    .locals 1

    .prologue
    .line 533
    const v0, 0x7f04015f

    return v0
.end method

.method protected Z()I
    .locals 1

    .prologue
    .line 552
    const/16 v0, 0x21

    return v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 469
    invoke-super {p0, p1, p2, p3}, Leku;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 470
    invoke-virtual {p0, v1}, Lelc;->d(Landroid/view/View;)Landroid/view/View;

    .line 472
    const v0, 0x7f10047d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 473
    if-eqz v0, :cond_0

    .line 474
    new-instance v2, Lelf;

    invoke-direct {v2, p0}, Lelf;-><init>(Lelc;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 492
    :cond_0
    iget-object v0, p0, Lelc;->ah:Leli;

    invoke-virtual {p0, v0}, Lelc;->a(Landroid/widget/ListAdapter;)V

    .line 494
    iget-object v0, p0, Lelc;->ab:Licq;

    const v2, 0x7f0a09b3

    invoke-virtual {v0, v2}, Licq;->a(I)Licq;

    move-result-object v0

    const/4 v2, 0x0

    .line 495
    invoke-virtual {v0, v2}, Licq;->a(Z)Licq;

    move-result-object v0

    new-instance v2, Lelg;

    invoke-direct {v2, p0}, Lelg;-><init>(Lelc;)V

    .line 496
    invoke-virtual {v0, v2}, Licq;->a(Lico;)Licq;

    .line 519
    return-object v1
.end method

.method protected a(II)Ldid;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 584
    .line 585
    invoke-virtual {p0, p1}, Lelc;->e(I)I

    move-result v0

    .line 584
    invoke-static {v0, v1, v1}, Ldib;->a(ILjava/lang/Integer;Ljava/lang/Integer;)Ldid;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 435
    invoke-super {p0, p1}, Leku;->a(Landroid/os/Bundle;)V

    .line 437
    if-eqz p1, :cond_0

    .line 438
    const-string v0, "circle_adds_per_session"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lelc;->N:I

    .line 439
    const-string v0, "dismiss_per_session"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lelc;->O:I

    .line 440
    const-string v0, "first_time_people_and_pages"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lelc;->ai:Z

    .line 443
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lelc;->X:Z

    .line 444
    new-instance v0, Leli;

    invoke-direct {v0, p0}, Leli;-><init>(Lelc;)V

    iput-object v0, p0, Lelc;->ah:Leli;

    .line 445
    return-void
.end method

.method public a(Lcom/google/android/apps/plus/views/PeopleListRowView;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 640
    const/4 v0, 0x1

    iput-boolean v0, p0, Lelc;->ad:Z

    .line 641
    invoke-super {p0, p1, p2}, Leku;->a(Lcom/google/android/apps/plus/views/PeopleListRowView;Ljava/lang/String;)V

    .line 642
    return-void
.end method

.method public a(Lhjk;)V
    .locals 1

    .prologue
    .line 538
    invoke-super {p0, p1}, Leku;->a(Lhjk;)V

    .line 539
    iget-object v0, p0, Lelc;->ah:Leli;

    invoke-virtual {v0}, Leli;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 540
    const v0, 0x7f100680

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    .line 542
    :cond_0
    const v0, 0x7f0a0629

    invoke-virtual {p0, v0}, Lelc;->e_(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lhjk;->a(Ljava/lang/CharSequence;)V

    .line 543
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 600
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 601
    const v1, 0x7f100680

    if-ne v0, v1, :cond_0

    .line 602
    invoke-direct {p0}, Lelc;->X()V

    .line 603
    const/4 v0, 0x1

    .line 605
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Leku;->a(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public aR_()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 595
    const/4 v0, 0x4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected ac()V
    .locals 1

    .prologue
    .line 616
    iget v0, p0, Lelc;->N:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lelc;->N:I

    .line 617
    return-void
.end method

.method public b()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 590
    const/16 v0, 0x7c

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 425
    invoke-super {p0, p1}, Leku;->c(Landroid/os/Bundle;)V

    .line 426
    iget-object v0, p0, Lelc;->au:Llnh;

    const-class v1, Liax;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liax;

    iput-object v0, p0, Lelc;->ae:Liax;

    .line 428
    new-instance v0, Liwk;

    iget-object v1, p0, Lelc;->at:Llnl;

    iget-object v2, p0, Lelc;->Z:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    invoke-direct {v0, v1, v2}, Liwk;-><init>(Landroid/content/Context;I)V

    const-class v1, Lixj;

    .line 429
    invoke-virtual {v0, v1}, Liwk;->a(Ljava/lang/Class;)Liwk;

    move-result-object v0

    iput-object v0, p0, Lelc;->ag:Liwk;

    .line 430
    return-void
.end method

.method public c(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 621
    iget v0, p0, Lelc;->O:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lelc;->O:I

    .line 624
    iget-boolean v0, p0, Lelc;->ad:Z

    if-eqz v0, :cond_0

    .line 625
    sget-object v0, Lhmv;->ca:Lhmv;

    .line 626
    const/4 v1, 0x0

    iput-boolean v1, p0, Lelc;->ad:Z

    move-object v1, v0

    .line 630
    :goto_0
    iget-object v0, p0, Lelc;->Z:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v2

    .line 631
    iget-object v0, p0, Lelc;->au:Llnh;

    const-class v3, Lhms;

    invoke-virtual {v0, v3}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v3, Lhmr;

    iget-object v4, p0, Lelc;->at:Llnl;

    invoke-direct {v3, v4, v2}, Lhmr;-><init>(Landroid/content/Context;I)V

    .line 633
    invoke-virtual {v3, v1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 631
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 635
    invoke-super {p0, p1}, Leku;->c(Landroid/view/View;)V

    .line 636
    return-void

    .line 628
    :cond_0
    sget-object v0, Lhmv;->bZ:Lhmv;

    move-object v1, v0

    goto :goto_0
.end method

.method protected d()V
    .locals 4

    .prologue
    .line 557
    const/4 v0, 0x1

    iput-boolean v0, p0, Lelc;->R:Z

    .line 558
    invoke-virtual {p0}, Lelc;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    iget-object v3, p0, Lelc;->aj:Lbc;

    invoke-virtual {v0, v1, v2, v3}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 559
    return-void
.end method

.method public e(I)I
    .locals 1

    .prologue
    .line 569
    packed-switch p1, :pswitch_data_0

    .line 577
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 571
    :pswitch_0
    const/16 v0, 0xc2

    goto :goto_0

    .line 573
    :pswitch_1
    const/16 v0, 0xe7

    goto :goto_0

    .line 575
    :pswitch_2
    const/16 v0, 0xc3

    goto :goto_0

    .line 569
    :pswitch_data_0
    .packed-switch 0xc
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected e()V
    .locals 4

    .prologue
    .line 563
    const/4 v0, 0x1

    iput-boolean v0, p0, Lelc;->R:Z

    .line 564
    invoke-virtual {p0}, Lelc;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    iget-object v3, p0, Lelc;->aj:Lbc;

    invoke-virtual {v0, v1, v2, v3}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 565
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 417
    invoke-super {p0, p1}, Leku;->e(Landroid/os/Bundle;)V

    .line 418
    const-string v0, "circle_adds_per_session"

    iget v1, p0, Lelc;->N:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 419
    const-string v0, "dismiss_per_session"

    iget v1, p0, Lelc;->O:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 420
    const-string v0, "first_time_people_and_pages"

    iget-boolean v1, p0, Lelc;->ai:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 421
    return-void
.end method
