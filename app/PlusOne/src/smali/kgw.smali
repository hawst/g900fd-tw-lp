.class public final Lkgw;
.super Lkgt;
.source "PG"


# instance fields
.field private b:Landroid/widget/EditText;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lkgw;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 75
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 70
    const v0, 0x7f01008d

    invoke-direct {p0, p1, p2, v0}, Lkgw;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 71
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 53
    invoke-direct {p0, p1, p2, p3}, Lkgt;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 55
    new-instance v0, Landroid/widget/EditText;

    invoke-direct {v0, p1, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lkgw;->b:Landroid/widget/EditText;

    .line 58
    iget-object v0, p0, Lkgw;->b:Landroid/widget/EditText;

    const v1, 0x7f100061

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setId(I)V

    .line 66
    iget-object v0, p0, Lkgw;->b:Landroid/widget/EditText;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 67
    return-void
.end method


# virtual methods
.method protected a(Landroid/content/res/TypedArray;I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 148
    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lkgw;->c:Ljava/lang/String;

    return-object v0
.end method

.method protected a(Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 192
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lkgx;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 194
    :cond_0
    invoke-super {p0, p1}, Lkgt;->a(Landroid/os/Parcelable;)V

    .line 201
    :goto_0
    return-void

    .line 198
    :cond_1
    check-cast p1, Lkgx;

    .line 199
    invoke-virtual {p1}, Lkgx;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Lkgt;->a(Landroid/os/Parcelable;)V

    .line 200
    iget-object v0, p1, Lkgx;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lkgw;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected a(Landroid/view/View;Landroid/widget/EditText;)V
    .locals 3

    .prologue
    .line 126
    const v0, 0x7f100503

    .line 127
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 128
    if-eqz v0, :cond_0

    .line 129
    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-virtual {v0, p2, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;II)V

    .line 132
    :cond_0
    return-void
.end method

.method protected a(Z)V
    .locals 2

    .prologue
    .line 136
    invoke-super {p0, p1}, Lkgt;->a(Z)V

    .line 138
    if-eqz p1, :cond_0

    .line 139
    iget-object v0, p0, Lkgw;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 140
    invoke-virtual {p0, v0}, Lkgw;->b(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 141
    invoke-virtual {p0, v0}, Lkgw;->b(Ljava/lang/String;)V

    .line 144
    :cond_0
    return-void
.end method

.method protected a(ZLjava/lang/Object;)V
    .locals 1

    .prologue
    .line 153
    if-eqz p1, :cond_0

    iget-object v0, p0, Lkgw;->c:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lkgw;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    :goto_0
    invoke-virtual {p0, p2}, Lkgw;->b(Ljava/lang/String;)V

    .line 154
    return-void

    .line 153
    :cond_0
    check-cast p2, Ljava/lang/String;

    goto :goto_0
.end method

.method public aC_()Z
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lkgw;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0}, Lkgt;->aC_()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected b(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 106
    invoke-super {p0, p1}, Lkgt;->b(Landroid/view/View;)V

    .line 108
    iget-object v1, p0, Lkgw;->b:Landroid/widget/EditText;

    .line 109
    invoke-virtual {p0}, Lkgw;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 111
    invoke-virtual {v1}, Landroid/widget/EditText;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 112
    if-eq v0, p1, :cond_1

    .line 113
    if-eqz v0, :cond_0

    .line 114
    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 116
    :cond_0
    invoke-virtual {p0, p1, v1}, Lkgw;->a(Landroid/view/View;Landroid/widget/EditText;)V

    .line 118
    :cond_1
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 83
    invoke-virtual {p0}, Lkgw;->aC_()Z

    move-result v0

    .line 85
    iput-object p1, p0, Lkgw;->c:Ljava/lang/String;

    .line 87
    invoke-virtual {p0, p1}, Lkgw;->g(Ljava/lang/String;)Z

    .line 89
    invoke-virtual {p0}, Lkgw;->aC_()Z

    move-result v1

    .line 90
    if-eq v1, v0, :cond_0

    .line 91
    invoke-virtual {p0, v1}, Lkgw;->e(Z)V

    .line 93
    :cond_0
    return-void
.end method

.method protected c()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 179
    invoke-super {p0}, Lkgt;->c()Landroid/os/Parcelable;

    move-result-object v0

    .line 180
    invoke-virtual {p0}, Lkgw;->x()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 187
    :goto_0
    return-object v0

    .line 185
    :cond_0
    new-instance v1, Lkgx;

    invoke-direct {v1, v0}, Lkgx;-><init>(Landroid/os/Parcelable;)V

    .line 186
    invoke-virtual {p0}, Lkgw;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lkgx;->a:Ljava/lang/String;

    move-object v0, v1

    .line 187
    goto :goto_0
.end method

.method protected g()Z
    .locals 1

    .prologue
    .line 174
    const/4 v0, 0x1

    return v0
.end method
