.class public final Lfig;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lleu;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lhei;

.field private final c:Llev;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lfig;->a:Landroid/content/Context;

    .line 39
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Lfig;->b:Lhei;

    .line 40
    new-instance v0, Lfih;

    invoke-direct {v0, p0}, Lfih;-><init>(Lfig;)V

    iput-object v0, p0, Lfig;->c:Llev;

    .line 64
    return-void
.end method

.method static synthetic a(Lfig;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lfig;->a:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public a()Llev;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lfig;->c:Llev;

    return-object v0
.end method

.method public a(Lkfp;ILles;)V
    .locals 3

    .prologue
    .line 72
    iget-object v0, p0, Lfig;->a:Landroid/content/Context;

    const-class v1, Lhei;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 73
    invoke-interface {v0, p2}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "is_google_plus"

    .line 74
    invoke-interface {v0, v1}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75
    iget-object v0, p0, Lfig;->a:Landroid/content/Context;

    invoke-static {v0, p2}, Ldhv;->o(Landroid/content/Context;I)V

    .line 77
    :cond_0
    iget-object v0, p0, Lfig;->b:Lhei;

    invoke-interface {v0, p2}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 78
    iget-object v0, p0, Lfig;->a:Landroid/content/Context;

    const-class v2, Liwc;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liwc;

    sget-object v2, Liwc;->a:Liwe;

    invoke-interface {v0, v1, v2}, Liwc;->a(Ljava/lang/String;Liwe;)V

    .line 80
    return-void
.end method
