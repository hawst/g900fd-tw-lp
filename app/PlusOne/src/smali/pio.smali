.class public final Lpio;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lpir;

.field private b:Lpir;

.field private c:Lpis;

.field private d:Lpis;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 175
    invoke-direct {p0}, Loxq;-><init>()V

    .line 178
    iput-object v0, p0, Lpio;->a:Lpir;

    .line 181
    iput-object v0, p0, Lpio;->b:Lpir;

    .line 184
    iput-object v0, p0, Lpio;->c:Lpis;

    .line 187
    iput-object v0, p0, Lpio;->d:Lpis;

    .line 175
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 210
    const/4 v0, 0x0

    .line 211
    iget-object v1, p0, Lpio;->a:Lpir;

    if-eqz v1, :cond_0

    .line 212
    const/4 v0, 0x1

    iget-object v1, p0, Lpio;->a:Lpir;

    .line 213
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 215
    :cond_0
    iget-object v1, p0, Lpio;->b:Lpir;

    if-eqz v1, :cond_1

    .line 216
    const/4 v1, 0x2

    iget-object v2, p0, Lpio;->b:Lpir;

    .line 217
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 219
    :cond_1
    iget-object v1, p0, Lpio;->c:Lpis;

    if-eqz v1, :cond_2

    .line 220
    const/4 v1, 0x3

    iget-object v2, p0, Lpio;->c:Lpis;

    .line 221
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 223
    :cond_2
    iget-object v1, p0, Lpio;->d:Lpis;

    if-eqz v1, :cond_3

    .line 224
    const/4 v1, 0x4

    iget-object v2, p0, Lpio;->d:Lpis;

    .line 225
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 227
    :cond_3
    iget-object v1, p0, Lpio;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 228
    iput v0, p0, Lpio;->ai:I

    .line 229
    return v0
.end method

.method public a(Loxn;)Lpio;
    .locals 2

    .prologue
    .line 237
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 238
    sparse-switch v0, :sswitch_data_0

    .line 242
    iget-object v1, p0, Lpio;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 243
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpio;->ah:Ljava/util/List;

    .line 246
    :cond_1
    iget-object v1, p0, Lpio;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 248
    :sswitch_0
    return-object p0

    .line 253
    :sswitch_1
    iget-object v0, p0, Lpio;->a:Lpir;

    if-nez v0, :cond_2

    .line 254
    new-instance v0, Lpir;

    invoke-direct {v0}, Lpir;-><init>()V

    iput-object v0, p0, Lpio;->a:Lpir;

    .line 256
    :cond_2
    iget-object v0, p0, Lpio;->a:Lpir;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 260
    :sswitch_2
    iget-object v0, p0, Lpio;->b:Lpir;

    if-nez v0, :cond_3

    .line 261
    new-instance v0, Lpir;

    invoke-direct {v0}, Lpir;-><init>()V

    iput-object v0, p0, Lpio;->b:Lpir;

    .line 263
    :cond_3
    iget-object v0, p0, Lpio;->b:Lpir;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 267
    :sswitch_3
    iget-object v0, p0, Lpio;->c:Lpis;

    if-nez v0, :cond_4

    .line 268
    new-instance v0, Lpis;

    invoke-direct {v0}, Lpis;-><init>()V

    iput-object v0, p0, Lpio;->c:Lpis;

    .line 270
    :cond_4
    iget-object v0, p0, Lpio;->c:Lpis;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 274
    :sswitch_4
    iget-object v0, p0, Lpio;->d:Lpis;

    if-nez v0, :cond_5

    .line 275
    new-instance v0, Lpis;

    invoke-direct {v0}, Lpis;-><init>()V

    iput-object v0, p0, Lpio;->d:Lpis;

    .line 277
    :cond_5
    iget-object v0, p0, Lpio;->d:Lpis;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 238
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 192
    iget-object v0, p0, Lpio;->a:Lpir;

    if-eqz v0, :cond_0

    .line 193
    const/4 v0, 0x1

    iget-object v1, p0, Lpio;->a:Lpir;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 195
    :cond_0
    iget-object v0, p0, Lpio;->b:Lpir;

    if-eqz v0, :cond_1

    .line 196
    const/4 v0, 0x2

    iget-object v1, p0, Lpio;->b:Lpir;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 198
    :cond_1
    iget-object v0, p0, Lpio;->c:Lpis;

    if-eqz v0, :cond_2

    .line 199
    const/4 v0, 0x3

    iget-object v1, p0, Lpio;->c:Lpis;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 201
    :cond_2
    iget-object v0, p0, Lpio;->d:Lpis;

    if-eqz v0, :cond_3

    .line 202
    const/4 v0, 0x4

    iget-object v1, p0, Lpio;->d:Lpis;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 204
    :cond_3
    iget-object v0, p0, Lpio;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 206
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 171
    invoke-virtual {p0, p1}, Lpio;->a(Loxn;)Lpio;

    move-result-object v0

    return-object v0
.end method
