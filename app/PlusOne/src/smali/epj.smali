.class final Lepj;
.super Landroid/widget/BaseAdapter;
.source "PG"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/view/LayoutInflater;

.field private c:[Lnsr;

.field private d:Lkvq;


# direct methods
.method public constructor <init>(Lepe;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 240
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 241
    iput-object p2, p0, Lepj;->a:Landroid/content/Context;

    .line 242
    iget-object v0, p0, Lepj;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lepj;->b:Landroid/view/LayoutInflater;

    .line 243
    invoke-static {p1}, Lepe;->e(Lepe;)Llnh;

    move-result-object v0

    const-class v1, Lkvq;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkvq;

    iput-object v0, p0, Lepj;->d:Lkvq;

    .line 244
    return-void
.end method


# virtual methods
.method public a([Lnsr;)V
    .locals 0

    .prologue
    .line 276
    iput-object p1, p0, Lepj;->c:[Lnsr;

    .line 277
    invoke-virtual {p0}, Lepj;->notifyDataSetChanged()V

    .line 278
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lepj;->c:[Lnsr;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lepj;->c:[Lnsr;

    array-length v0, v0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, Lepj;->c:[Lnsr;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 257
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 262
    if-nez p2, :cond_0

    .line 263
    iget-object v0, p0, Lepj;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f0401f7

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 264
    new-instance v1, Llka;

    const/4 v2, 0x2

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3, v4, v4}, Llka;-><init>(IIII)V

    .line 267
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 269
    :goto_0
    check-cast v0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;

    .line 270
    iget-object v1, p0, Lepj;->c:[Lnsr;

    aget-object v1, v1, p1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->a(Lnsr;)V

    .line 271
    iget-object v1, p0, Lepj;->d:Lkvq;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->a(Lkvq;)V

    .line 272
    return-object v0

    :cond_0
    move-object v0, p2

    goto :goto_0
.end method
