.class public final Lotx;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lotx;


# instance fields
.field private b:Lotf;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1005
    const/4 v0, 0x0

    new-array v0, v0, [Lotx;

    sput-object v0, Lotx;->a:[Lotx;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1006
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1009
    const/4 v0, 0x0

    iput-object v0, p0, Lotx;->b:Lotf;

    .line 1006
    return-void
.end method


# virtual methods
.method public a()I
    .locals 2

    .prologue
    .line 1023
    const/4 v0, 0x0

    .line 1024
    iget-object v1, p0, Lotx;->b:Lotf;

    if-eqz v1, :cond_0

    .line 1025
    const/4 v0, 0x1

    iget-object v1, p0, Lotx;->b:Lotf;

    .line 1026
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1028
    :cond_0
    iget-object v1, p0, Lotx;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1029
    iput v0, p0, Lotx;->ai:I

    .line 1030
    return v0
.end method

.method public a(Loxn;)Lotx;
    .locals 2

    .prologue
    .line 1038
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1039
    sparse-switch v0, :sswitch_data_0

    .line 1043
    iget-object v1, p0, Lotx;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1044
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lotx;->ah:Ljava/util/List;

    .line 1047
    :cond_1
    iget-object v1, p0, Lotx;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1049
    :sswitch_0
    return-object p0

    .line 1054
    :sswitch_1
    iget-object v0, p0, Lotx;->b:Lotf;

    if-nez v0, :cond_2

    .line 1055
    new-instance v0, Lotf;

    invoke-direct {v0}, Lotf;-><init>()V

    iput-object v0, p0, Lotx;->b:Lotf;

    .line 1057
    :cond_2
    iget-object v0, p0, Lotx;->b:Lotf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1039
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1014
    iget-object v0, p0, Lotx;->b:Lotf;

    if-eqz v0, :cond_0

    .line 1015
    const/4 v0, 0x1

    iget-object v1, p0, Lotx;->b:Lotf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1017
    :cond_0
    iget-object v0, p0, Lotx;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1019
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1002
    invoke-virtual {p0, p1}, Lotx;->a(Loxn;)Lotx;

    move-result-object v0

    return-object v0
.end method
