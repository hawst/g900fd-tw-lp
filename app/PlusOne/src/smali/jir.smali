.class public final enum Ljir;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Ljir;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Ljir;

.field public static final enum b:Ljir;

.field public static final enum c:Ljir;

.field public static final enum d:Ljir;

.field private static final synthetic e:[Ljir;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 13
    new-instance v0, Ljir;

    const-string v1, "GCM_UNREAD_RECEIVED"

    invoke-direct {v0, v1, v2}, Ljir;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ljir;->a:Ljir;

    .line 14
    new-instance v0, Ljir;

    const-string v1, "TAP_SYSTEM_TRAY"

    invoke-direct {v0, v1, v3}, Ljir;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ljir;->b:Ljir;

    .line 15
    new-instance v0, Ljir;

    const-string v1, "DISMISS_SYSTEM_TRAY"

    invoke-direct {v0, v1, v4}, Ljir;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ljir;->c:Ljir;

    .line 16
    new-instance v0, Ljir;

    const-string v1, "DISMISS_ALL"

    invoke-direct {v0, v1, v5}, Ljir;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ljir;->d:Ljir;

    .line 12
    const/4 v0, 0x4

    new-array v0, v0, [Ljir;

    sget-object v1, Ljir;->a:Ljir;

    aput-object v1, v0, v2

    sget-object v1, Ljir;->b:Ljir;

    aput-object v1, v0, v3

    sget-object v1, Ljir;->c:Ljir;

    aput-object v1, v0, v4

    sget-object v1, Ljir;->d:Ljir;

    aput-object v1, v0, v5

    sput-object v0, Ljir;->e:[Ljir;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 24
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ljir;
    .locals 1

    .prologue
    .line 12
    const-class v0, Ljir;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ljir;

    return-object v0
.end method

.method public static values()[Ljir;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Ljir;->e:[Ljir;

    invoke-virtual {v0}, [Ljir;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljir;

    return-object v0
.end method
