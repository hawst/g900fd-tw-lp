.class public final Leos;
.super Lenl;
.source "PG"

# interfaces
.implements Lhob;


# instance fields
.field private an:Landroid/widget/LinearLayout;

.field private ao:Landroid/widget/RadioGroup;

.field private ap:Lnso;

.field private aq:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private ar:Lnto;

.field private as:[B

.field private final aw:Lhoc;

.field private ax:Z

.field private ay:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 47
    invoke-direct {p0}, Lenl;-><init>()V

    .line 64
    new-instance v0, Lhoc;

    .line 65
    invoke-virtual {p0}, Leos;->z_()Llqr;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lhoc;-><init>(Lu;Llqr;)V

    invoke-virtual {v0, p0}, Lhoc;->a(Lhob;)Lhoc;

    move-result-object v0

    iput-object v0, p0, Leos;->aw:Lhoc;

    .line 64
    return-void
.end method

.method public static a([Lnrx;[Ljava/lang/String;)Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lnrx;",
            "[",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 273
    if-nez p0, :cond_1

    .line 274
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 286
    :cond_0
    return-object v0

    .line 277
    :cond_1
    new-instance v0, Ljava/util/HashMap;

    array-length v2, p0

    invoke-direct {v0, v2}, Ljava/util/HashMap;-><init>(I)V

    .line 278
    array-length v3, p0

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_2

    aget-object v4, p0, v2

    .line 279
    iget-object v4, v4, Lnrx;->b:Lnsr;

    iget-object v4, v4, Lnsr;->b:Lnsb;

    iget-object v4, v4, Lnsb;->a:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-interface {v0, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 278
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 281
    :cond_2
    if-eqz p1, :cond_0

    .line 282
    array-length v2, p1

    :goto_1
    if-ge v1, v2, :cond_0

    aget-object v3, p1, v1

    .line 283
    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 282
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method static synthetic a(Leos;)Lnso;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Leos;->ap:Lnso;

    return-object v0
.end method

.method public static a(Ljava/util/Map;)[Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;)[",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 291
    if-nez p0, :cond_0

    .line 292
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    .line 302
    :goto_0
    return-object v0

    .line 295
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    .line 296
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 297
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 298
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 299
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 302
    :cond_2
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    goto :goto_0
.end method

.method private al()V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 234
    iget-object v1, p0, Leos;->ar:Lnto;

    if-eqz v1, :cond_1

    .line 235
    iget-object v1, p0, Leos;->an:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 237
    iget-object v1, p0, Leos;->ar:Lnto;

    iget-object v3, v1, Lnto;->a:[Lnrx;

    array-length v4, v3

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    aget-object v5, v3, v1

    .line 238
    new-instance v6, Landroid/widget/CheckBox;

    invoke-virtual {p0}, Leos;->n()Lz;

    move-result-object v2

    invoke-direct {v6, v2}, Landroid/widget/CheckBox;-><init>(Landroid/content/Context;)V

    .line 239
    add-int/lit8 v2, v0, 0x1

    invoke-virtual {v6, v0}, Landroid/widget/CheckBox;->setId(I)V

    .line 240
    iget-object v0, v5, Lnrx;->b:Lnsr;

    iget-object v0, v0, Lnsr;->b:Lnsb;

    iget-object v0, v0, Lnsb;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/widget/CheckBox;->setTag(Ljava/lang/Object;)V

    .line 241
    iget-object v0, v5, Lnrx;->b:Lnsr;

    iget-object v0, v0, Lnsr;->b:Lnsb;

    iget-object v0, v0, Lnsb;->b:Lnsc;

    iget-object v0, v0, Lnsc;->a:Ljava/lang/String;

    invoke-virtual {v6, v0}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 242
    new-instance v0, Leov;

    invoke-direct {v0, p0}, Leov;-><init>(Leos;)V

    invoke-virtual {v6, v0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 248
    iget-object v0, p0, Leos;->aq:Ljava/util/Map;

    iget-object v5, v5, Lnrx;->b:Lnsr;

    iget-object v5, v5, Lnsr;->b:Lnsb;

    iget-object v5, v5, Lnsb;->a:Ljava/lang/String;

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v6, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 249
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x11

    if-lt v0, v5, :cond_0

    .line 250
    const/4 v0, 0x5

    invoke-virtual {v6, v0}, Landroid/widget/CheckBox;->setTextAlignment(I)V

    .line 252
    :cond_0
    iget-object v0, p0, Leos;->an:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 237
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_0

    .line 255
    :cond_1
    return-void
.end method

.method static synthetic b(Leos;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Leos;->an:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic c(Leos;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Leos;->aq:Ljava/util/Map;

    return-object v0
.end method


# virtual methods
.method protected U()V
    .locals 0

    .prologue
    .line 231
    return-void
.end method

.method protected V()V
    .locals 5

    .prologue
    .line 140
    invoke-super {p0}, Lenl;->V()V

    .line 141
    iget-object v0, p0, Leos;->ap:Lnso;

    iget-object v1, p0, Leos;->aq:Ljava/util/Map;

    invoke-static {v1}, Leos;->a(Ljava/util/Map;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lnso;->c:[Ljava/lang/String;

    .line 142
    new-instance v0, Lkwq;

    iget-object v1, p0, Leos;->at:Llnl;

    iget-object v2, p0, Leos;->am:Lhee;

    .line 143
    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    const-string v3, "save_settings"

    iget-object v4, p0, Leos;->ap:Lnso;

    invoke-direct {v0, v1, v2, v3, v4}, Lkwq;-><init>(Landroid/content/Context;ILjava/lang/String;Lnso;)V

    .line 145
    iget-object v1, p0, Leos;->aw:Lhoc;

    invoke-virtual {v1, v0}, Lhoc;->c(Lhny;)V

    .line 146
    return-void
.end method

.method protected Y()[B
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Leos;->aa:[B

    return-object v0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 71
    iput-boolean v0, p0, Leos;->U:Z

    .line 72
    iput-boolean v0, p0, Leos;->V:Z

    .line 73
    invoke-super {p0, p1}, Lenl;->a(Landroid/os/Bundle;)V

    .line 74
    return-void
.end method

.method protected a(Ldsx;)V
    .locals 1

    .prologue
    .line 108
    if-eqz p1, :cond_0

    iget-object v0, p1, Ldsx;->o:Lnso;

    if-eqz v0, :cond_0

    iget-object v0, p1, Ldsx;->n:Lnto;

    if-nez v0, :cond_1

    .line 109
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Leos;->ax:Z

    .line 124
    :goto_0
    return-void

    .line 112
    :cond_1
    iget-object v0, p0, Leos;->ap:Lnso;

    if-nez v0, :cond_2

    .line 113
    iget-object v0, p0, Leos;->aa:[B

    if-eqz v0, :cond_3

    .line 114
    iget-object v0, p0, Leos;->aa:[B

    iput-object v0, p0, Leos;->Z:[B

    .line 115
    invoke-virtual {p0}, Leos;->c()V

    .line 120
    :cond_2
    :goto_1
    iget-object v0, p1, Ldsx;->n:Lnto;

    iput-object v0, p0, Leos;->ar:Lnto;

    .line 121
    iget-object v0, p0, Leos;->ar:Lnto;

    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    iput-object v0, p0, Leos;->as:[B

    .line 122
    invoke-virtual {p0}, Leos;->c()V

    .line 123
    invoke-virtual {p0}, Leos;->d()V

    goto :goto_0

    .line 117
    :cond_3
    iget-object v0, p1, Ldsx;->o:Lnso;

    iput-object v0, p0, Leos;->ap:Lnso;

    goto :goto_1
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 1

    .prologue
    .line 325
    const-string v0, "save_settings"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 326
    invoke-static {p2}, Lhoz;->a(Lhoz;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 327
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Leos;->d(I)V

    .line 330
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 258
    iget-object v1, p0, Leos;->ao:Landroid/widget/RadioGroup;

    invoke-virtual {v1}, Landroid/widget/RadioGroup;->getChildCount()I

    move-result v2

    move v1, v0

    .line 259
    :goto_0
    if-ge v1, v2, :cond_0

    .line 260
    iget-object v3, p0, Leos;->ao:Landroid/widget/RadioGroup;

    invoke-virtual {v3, v1}, Landroid/widget/RadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 259
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 262
    :cond_0
    iget-object v1, p0, Leos;->an:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    .line 263
    :goto_1
    if-ge v0, v1, :cond_1

    .line 264
    iget-object v2, p0, Leos;->an:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 263
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 266
    :cond_1
    if-eqz p1, :cond_2

    iget-object v0, p0, Leos;->ap:Lnso;

    iget v0, v0, Lnso;->b:I

    const/high16 v1, -0x80000000

    if-ne v0, v1, :cond_2

    .line 267
    iget-object v0, p0, Leos;->ao:Landroid/widget/RadioGroup;

    const v1, 0x7f100539

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 269
    :cond_2
    return-void
.end method

.method protected a()[B
    .locals 2

    .prologue
    .line 97
    iget-object v0, p0, Leos;->ap:Lnso;

    iget-object v1, p0, Leos;->aq:Ljava/util/Map;

    invoke-static {v1}, Leos;->a(Ljava/util/Map;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lnso;->c:[Ljava/lang/String;

    .line 98
    iget-object v0, p0, Leos;->ap:Lnso;

    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    return-object v0
.end method

.method protected c()V
    .locals 2

    .prologue
    .line 128
    invoke-super {p0}, Lenl;->c()V

    .line 129
    new-instance v0, Lnso;

    invoke-direct {v0}, Lnso;-><init>()V

    iget-object v1, p0, Leos;->Z:[B

    invoke-static {v0, v1}, Lhys;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lnso;

    iput-object v0, p0, Leos;->ap:Lnso;

    .line 130
    return-void
.end method

.method protected d()V
    .locals 2

    .prologue
    .line 134
    invoke-super {p0}, Lenl;->d()V

    .line 135
    new-instance v0, Lnto;

    invoke-direct {v0}, Lnto;-><init>()V

    iget-object v1, p0, Leos;->as:[B

    invoke-static {v0, v1}, Lhys;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lnto;

    iput-object v0, p0, Leos;->ar:Lnto;

    .line 136
    return-void
.end method

.method protected e()V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 150
    invoke-super {p0}, Lenl;->e()V

    .line 152
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Leos;->a_(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0401bc

    iget-object v2, p0, Leos;->ai:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 154
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v5, -0x2

    invoke-direct {v1, v2, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 156
    sget v2, Leos;->R:I

    sget v5, Leos;->R:I

    sget v6, Leos;->R:I

    sget v7, Leos;->R:I

    invoke-virtual {v1, v2, v5, v6, v7}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 158
    iget-object v2, p0, Leos;->ai:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 159
    const v1, 0x7f100538

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 160
    const v2, 0x7f10052e

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioGroup;

    iput-object v2, p0, Leos;->ao:Landroid/widget/RadioGroup;

    .line 161
    const v2, 0x7f10053b

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Leos;->an:Landroid/widget/LinearLayout;

    .line 164
    const v2, 0x7f1003a8

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 165
    iget-object v2, p0, Leos;->at:Llnl;

    const-string v5, "plus_profile_tab"

    const-string v6, "https://support.google.com/plus/?hl=%locale%"

    invoke-static {v2, v5, v6}, Litk;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    .line 166
    new-instance v5, Landroid/text/SpannableStringBuilder;

    iget-object v6, p0, Leos;->at:Llnl;

    const v7, 0x7f0a04d7

    new-array v8, v3, [Ljava/lang/Object;

    aput-object v2, v8, v4

    .line 167
    invoke-virtual {v6, v7, v8}, Llnl;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Llhv;->a(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-direct {v5, v2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 168
    invoke-static {v5}, Llju;->a(Landroid/text/Spannable;)V

    .line 169
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 170
    invoke-static {}, Lljw;->a()Lljw;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 172
    iget-object v0, p0, Leos;->ap:Lnso;

    if-eqz v0, :cond_3

    iget v2, v0, Lnso;->a:I

    if-ne v2, v9, :cond_0

    move v0, v3

    :goto_0
    if-nez v0, :cond_4

    .line 173
    iput-boolean v3, p0, Leos;->ax:Z

    .line 226
    :goto_1
    return-void

    .line 172
    :cond_0
    iget v2, v0, Lnso;->a:I

    if-ne v2, v3, :cond_3

    iget v2, v0, Lnso;->b:I

    if-ne v2, v3, :cond_1

    move v0, v3

    goto :goto_0

    :cond_1
    iget v2, v0, Lnso;->b:I

    if-ne v2, v9, :cond_3

    iget-object v0, v0, Lnso;->c:[Ljava/lang/String;

    if-eqz v0, :cond_2

    move v0, v3

    goto :goto_0

    :cond_2
    move v0, v4

    goto :goto_0

    :cond_3
    move v0, v4

    goto :goto_0

    .line 177
    :cond_4
    iget-object v0, p0, Leos;->ar:Lnto;

    iget-object v0, v0, Lnto;->a:[Lnrx;

    iget-object v2, p0, Leos;->ap:Lnso;

    iget-object v2, v2, Lnso;->c:[Ljava/lang/String;

    invoke-static {v0, v2}, Leos;->a([Lnrx;[Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Leos;->aq:Ljava/util/Map;

    .line 180
    iget-boolean v0, p0, Leos;->ay:Z

    if-nez v0, :cond_7

    iget-object v0, p0, Leos;->ap:Lnso;

    iget v0, v0, Lnso;->a:I

    if-eq v0, v9, :cond_5

    iget-object v0, p0, Leos;->ap:Lnso;

    iget v0, v0, Lnso;->b:I

    if-ne v0, v3, :cond_7

    .line 182
    :cond_5
    iget-object v0, p0, Leos;->aq:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 183
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/Map$Entry;->setValue(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 185
    :cond_6
    iput-boolean v3, p0, Leos;->ay:Z

    .line 188
    :cond_7
    invoke-direct {p0}, Leos;->al()V

    .line 190
    invoke-virtual {p0, v4}, Leos;->a(Z)V

    .line 191
    new-instance v0, Leot;

    invoke-direct {v0, p0}, Leot;-><init>(Leos;)V

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 202
    iget-object v0, p0, Leos;->ap:Lnso;

    iget v0, v0, Lnso;->a:I

    if-eq v0, v9, :cond_9

    move v0, v3

    :goto_3
    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 204
    iget-object v0, p0, Leos;->ao:Landroid/widget/RadioGroup;

    new-instance v1, Leou;

    invoke-direct {v1, p0}, Leou;-><init>(Leos;)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 218
    iget-object v0, p0, Leos;->ap:Lnso;

    iget v0, v0, Lnso;->a:I

    if-ne v0, v3, :cond_8

    .line 219
    iget-object v0, p0, Leos;->ap:Lnso;

    iget v0, v0, Lnso;->b:I

    if-ne v0, v3, :cond_a

    .line 220
    iget-object v0, p0, Leos;->ao:Landroid/widget/RadioGroup;

    const v1, 0x7f100539

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    invoke-virtual {v0, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 225
    :cond_8
    :goto_4
    iget-object v0, p0, Leos;->ak:Lcom/google/android/apps/plus/views/ImageTextButton;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/ImageTextButton;->setEnabled(Z)V

    goto/16 :goto_1

    :cond_9
    move v0, v4

    .line 202
    goto :goto_3

    .line 222
    :cond_a
    iget-object v0, p0, Leos;->ao:Landroid/widget/RadioGroup;

    const v1, 0x7f10053a

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    invoke-virtual {v0, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_4
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 90
    const-string v0, "squares_data_proto"

    iget-object v1, p0, Leos;->as:[B

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 91
    const-string v0, "is_initialized_key"

    iget-boolean v1, p0, Leos;->ay:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 92
    invoke-super {p0, p1}, Lenl;->e(Landroid/os/Bundle;)V

    .line 93
    return-void
.end method

.method protected i(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 338
    iget-boolean v0, p0, Leos;->ax:Z

    if-eqz v0, :cond_0

    .line 339
    iget-object v0, p0, Leos;->ak:Lcom/google/android/apps/plus/views/ImageTextButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ImageTextButton;->setEnabled(Z)V

    .line 340
    iget-object v0, p0, Leos;->at:Llnl;

    const v1, 0x7f0a0aff

    invoke-virtual {v0, v1}, Llnl;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Leos;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 344
    :goto_0
    return-void

    .line 342
    :cond_0
    invoke-super {p0, p1}, Lenl;->i(Landroid/view/View;)V

    goto :goto_0
.end method

.method public k(Landroid/os/Bundle;)Z
    .locals 2

    .prologue
    .line 78
    if-eqz p1, :cond_0

    .line 79
    const-string v0, "squares_data_proto"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    iput-object v0, p0, Leos;->as:[B

    .line 80
    const-string v0, "is_initialized_key"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Leos;->ay:Z

    .line 84
    :goto_0
    new-instance v0, Lnto;

    invoke-direct {v0}, Lnto;-><init>()V

    iget-object v1, p0, Leos;->as:[B

    invoke-static {v0, v1}, Lhys;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lnto;

    iput-object v0, p0, Leos;->ar:Lnto;

    .line 85
    invoke-super {p0, p1}, Lenl;->k(Landroid/os/Bundle;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Leos;->ar:Lnto;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    .line 82
    :cond_0
    invoke-virtual {p0}, Leos;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "squares_data_proto"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    iput-object v0, p0, Leos;->as:[B

    goto :goto_0

    .line 85
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
