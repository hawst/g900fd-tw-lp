.class public final Lpjm;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/Long;

.field private c:Ljava/lang/Long;

.field private d:Lpjk;

.field private e:Lpjk;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1013
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1022
    iput-object v0, p0, Lpjm;->d:Lpjk;

    .line 1025
    iput-object v0, p0, Lpjm;->e:Lpjk;

    .line 1013
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 1045
    const/4 v0, 0x1

    iget-object v1, p0, Lpjm;->a:Ljava/lang/String;

    .line 1047
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1048
    const/4 v1, 0x2

    iget-object v2, p0, Lpjm;->b:Ljava/lang/Long;

    .line 1049
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1050
    const/4 v1, 0x3

    iget-object v2, p0, Lpjm;->c:Ljava/lang/Long;

    .line 1051
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1052
    iget-object v1, p0, Lpjm;->d:Lpjk;

    if-eqz v1, :cond_0

    .line 1053
    const/4 v1, 0x4

    iget-object v2, p0, Lpjm;->d:Lpjk;

    .line 1054
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1056
    :cond_0
    iget-object v1, p0, Lpjm;->e:Lpjk;

    if-eqz v1, :cond_1

    .line 1057
    const/4 v1, 0x5

    iget-object v2, p0, Lpjm;->e:Lpjk;

    .line 1058
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1060
    :cond_1
    iget-object v1, p0, Lpjm;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1061
    iput v0, p0, Lpjm;->ai:I

    .line 1062
    return v0
.end method

.method public a(Loxn;)Lpjm;
    .locals 2

    .prologue
    .line 1070
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1071
    sparse-switch v0, :sswitch_data_0

    .line 1075
    iget-object v1, p0, Lpjm;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1076
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpjm;->ah:Ljava/util/List;

    .line 1079
    :cond_1
    iget-object v1, p0, Lpjm;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1081
    :sswitch_0
    return-object p0

    .line 1086
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpjm;->a:Ljava/lang/String;

    goto :goto_0

    .line 1090
    :sswitch_2
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lpjm;->b:Ljava/lang/Long;

    goto :goto_0

    .line 1094
    :sswitch_3
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lpjm;->c:Ljava/lang/Long;

    goto :goto_0

    .line 1098
    :sswitch_4
    iget-object v0, p0, Lpjm;->d:Lpjk;

    if-nez v0, :cond_2

    .line 1099
    new-instance v0, Lpjk;

    invoke-direct {v0}, Lpjk;-><init>()V

    iput-object v0, p0, Lpjm;->d:Lpjk;

    .line 1101
    :cond_2
    iget-object v0, p0, Lpjm;->d:Lpjk;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1105
    :sswitch_5
    iget-object v0, p0, Lpjm;->e:Lpjk;

    if-nez v0, :cond_3

    .line 1106
    new-instance v0, Lpjk;

    invoke-direct {v0}, Lpjk;-><init>()V

    iput-object v0, p0, Lpjm;->e:Lpjk;

    .line 1108
    :cond_3
    iget-object v0, p0, Lpjm;->e:Lpjk;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1071
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 1030
    const/4 v0, 0x1

    iget-object v1, p0, Lpjm;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1031
    const/4 v0, 0x2

    iget-object v1, p0, Lpjm;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 1032
    const/4 v0, 0x3

    iget-object v1, p0, Lpjm;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 1033
    iget-object v0, p0, Lpjm;->d:Lpjk;

    if-eqz v0, :cond_0

    .line 1034
    const/4 v0, 0x4

    iget-object v1, p0, Lpjm;->d:Lpjk;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1036
    :cond_0
    iget-object v0, p0, Lpjm;->e:Lpjk;

    if-eqz v0, :cond_1

    .line 1037
    const/4 v0, 0x5

    iget-object v1, p0, Lpjm;->e:Lpjk;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1039
    :cond_1
    iget-object v0, p0, Lpjm;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1041
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1009
    invoke-virtual {p0, p1}, Lpjm;->a(Loxn;)Lpjm;

    move-result-object v0

    return-object v0
.end method
