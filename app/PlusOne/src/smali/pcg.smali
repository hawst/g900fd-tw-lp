.class public final Lpcg;
.super Loxq;
.source "PG"


# static fields
.field public static final a:Loxr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Loxr",
            "<",
            "Lpcg;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Lpdi;

.field private h:Ljava/lang/String;

.field private i:[Loya;

.field private j:Loya;

.field private k:[Loya;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;

.field private s:Ljava/lang/String;

.field private t:Loya;

.field private u:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 9
    const v0, 0x2255de2

    new-instance v1, Lpch;

    invoke-direct {v1}, Lpch;-><init>()V

    .line 14
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    move-result-object v0

    sput-object v0, Lpcg;->a:Loxr;

    .line 13
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 27
    iput-object v1, p0, Lpcg;->g:Lpdi;

    .line 32
    sget-object v0, Loya;->a:[Loya;

    iput-object v0, p0, Lpcg;->i:[Loya;

    .line 35
    iput-object v1, p0, Lpcg;->j:Loya;

    .line 38
    sget-object v0, Loya;->a:[Loya;

    iput-object v0, p0, Lpcg;->k:[Loya;

    .line 57
    iput-object v1, p0, Lpcg;->t:Loya;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 138
    .line 139
    iget-object v0, p0, Lpcg;->d:Ljava/lang/String;

    if-eqz v0, :cond_15

    .line 140
    const/4 v0, 0x1

    iget-object v2, p0, Lpcg;->d:Ljava/lang/String;

    .line 141
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 143
    :goto_0
    iget-object v2, p0, Lpcg;->b:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 144
    const/4 v2, 0x2

    iget-object v3, p0, Lpcg;->b:Ljava/lang/String;

    .line 145
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 147
    :cond_0
    iget-object v2, p0, Lpcg;->c:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 148
    const/4 v2, 0x3

    iget-object v3, p0, Lpcg;->c:Ljava/lang/String;

    .line 149
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 151
    :cond_1
    iget-object v2, p0, Lpcg;->e:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 152
    const/4 v2, 0x4

    iget-object v3, p0, Lpcg;->e:Ljava/lang/String;

    .line 153
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 155
    :cond_2
    iget-object v2, p0, Lpcg;->f:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 156
    const/4 v2, 0x5

    iget-object v3, p0, Lpcg;->f:Ljava/lang/String;

    .line 157
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 159
    :cond_3
    iget-object v2, p0, Lpcg;->g:Lpdi;

    if-eqz v2, :cond_4

    .line 160
    const/4 v2, 0x6

    iget-object v3, p0, Lpcg;->g:Lpdi;

    .line 161
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 163
    :cond_4
    iget-object v2, p0, Lpcg;->h:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 164
    const/4 v2, 0x7

    iget-object v3, p0, Lpcg;->h:Ljava/lang/String;

    .line 165
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 167
    :cond_5
    iget-object v2, p0, Lpcg;->i:[Loya;

    if-eqz v2, :cond_7

    .line 168
    iget-object v3, p0, Lpcg;->i:[Loya;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_7

    aget-object v5, v3, v2

    .line 169
    if-eqz v5, :cond_6

    .line 170
    const/16 v6, 0x8

    .line 171
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 168
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 175
    :cond_7
    iget-object v2, p0, Lpcg;->j:Loya;

    if-eqz v2, :cond_8

    .line 176
    const/16 v2, 0x9

    iget-object v3, p0, Lpcg;->j:Loya;

    .line 177
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 179
    :cond_8
    iget-object v2, p0, Lpcg;->k:[Loya;

    if-eqz v2, :cond_a

    .line 180
    iget-object v2, p0, Lpcg;->k:[Loya;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_a

    aget-object v4, v2, v1

    .line 181
    if-eqz v4, :cond_9

    .line 182
    const/16 v5, 0xb

    .line 183
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 180
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 187
    :cond_a
    iget-object v1, p0, Lpcg;->l:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 188
    const/16 v1, 0xc

    iget-object v2, p0, Lpcg;->l:Ljava/lang/String;

    .line 189
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 191
    :cond_b
    iget-object v1, p0, Lpcg;->m:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 192
    const/16 v1, 0x1e

    iget-object v2, p0, Lpcg;->m:Ljava/lang/String;

    .line 193
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 195
    :cond_c
    iget-object v1, p0, Lpcg;->n:Ljava/lang/String;

    if-eqz v1, :cond_d

    .line 196
    const/16 v1, 0x1f

    iget-object v2, p0, Lpcg;->n:Ljava/lang/String;

    .line 197
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 199
    :cond_d
    iget-object v1, p0, Lpcg;->o:Ljava/lang/String;

    if-eqz v1, :cond_e

    .line 200
    const/16 v1, 0x20

    iget-object v2, p0, Lpcg;->o:Ljava/lang/String;

    .line 201
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 203
    :cond_e
    iget-object v1, p0, Lpcg;->p:Ljava/lang/String;

    if-eqz v1, :cond_f

    .line 204
    const/16 v1, 0x21

    iget-object v2, p0, Lpcg;->p:Ljava/lang/String;

    .line 205
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 207
    :cond_f
    iget-object v1, p0, Lpcg;->q:Ljava/lang/String;

    if-eqz v1, :cond_10

    .line 208
    const/16 v1, 0x22

    iget-object v2, p0, Lpcg;->q:Ljava/lang/String;

    .line 209
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 211
    :cond_10
    iget-object v1, p0, Lpcg;->r:Ljava/lang/String;

    if-eqz v1, :cond_11

    .line 212
    const/16 v1, 0x23

    iget-object v2, p0, Lpcg;->r:Ljava/lang/String;

    .line 213
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 215
    :cond_11
    iget-object v1, p0, Lpcg;->s:Ljava/lang/String;

    if-eqz v1, :cond_12

    .line 216
    const/16 v1, 0x4b

    iget-object v2, p0, Lpcg;->s:Ljava/lang/String;

    .line 217
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 219
    :cond_12
    iget-object v1, p0, Lpcg;->t:Loya;

    if-eqz v1, :cond_13

    .line 220
    const/16 v1, 0xb9

    iget-object v2, p0, Lpcg;->t:Loya;

    .line 221
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 223
    :cond_13
    iget-object v1, p0, Lpcg;->u:Ljava/lang/String;

    if-eqz v1, :cond_14

    .line 224
    const/16 v1, 0xfe

    iget-object v2, p0, Lpcg;->u:Ljava/lang/String;

    .line 225
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 227
    :cond_14
    iget-object v1, p0, Lpcg;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 228
    iput v0, p0, Lpcg;->ai:I

    .line 229
    return v0

    :cond_15
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lpcg;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 237
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 238
    sparse-switch v0, :sswitch_data_0

    .line 242
    iget-object v2, p0, Lpcg;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 243
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lpcg;->ah:Ljava/util/List;

    .line 246
    :cond_1
    iget-object v2, p0, Lpcg;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 248
    :sswitch_0
    return-object p0

    .line 253
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpcg;->d:Ljava/lang/String;

    goto :goto_0

    .line 257
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpcg;->b:Ljava/lang/String;

    goto :goto_0

    .line 261
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpcg;->c:Ljava/lang/String;

    goto :goto_0

    .line 265
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpcg;->e:Ljava/lang/String;

    goto :goto_0

    .line 269
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpcg;->f:Ljava/lang/String;

    goto :goto_0

    .line 273
    :sswitch_6
    iget-object v0, p0, Lpcg;->g:Lpdi;

    if-nez v0, :cond_2

    .line 274
    new-instance v0, Lpdi;

    invoke-direct {v0}, Lpdi;-><init>()V

    iput-object v0, p0, Lpcg;->g:Lpdi;

    .line 276
    :cond_2
    iget-object v0, p0, Lpcg;->g:Lpdi;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 280
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpcg;->h:Ljava/lang/String;

    goto :goto_0

    .line 284
    :sswitch_8
    const/16 v0, 0x42

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 285
    iget-object v0, p0, Lpcg;->i:[Loya;

    if-nez v0, :cond_4

    move v0, v1

    .line 286
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Loya;

    .line 287
    iget-object v3, p0, Lpcg;->i:[Loya;

    if-eqz v3, :cond_3

    .line 288
    iget-object v3, p0, Lpcg;->i:[Loya;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 290
    :cond_3
    iput-object v2, p0, Lpcg;->i:[Loya;

    .line 291
    :goto_2
    iget-object v2, p0, Lpcg;->i:[Loya;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 292
    iget-object v2, p0, Lpcg;->i:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 293
    iget-object v2, p0, Lpcg;->i:[Loya;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 294
    invoke-virtual {p1}, Loxn;->a()I

    .line 291
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 285
    :cond_4
    iget-object v0, p0, Lpcg;->i:[Loya;

    array-length v0, v0

    goto :goto_1

    .line 297
    :cond_5
    iget-object v2, p0, Lpcg;->i:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 298
    iget-object v2, p0, Lpcg;->i:[Loya;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 302
    :sswitch_9
    iget-object v0, p0, Lpcg;->j:Loya;

    if-nez v0, :cond_6

    .line 303
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    iput-object v0, p0, Lpcg;->j:Loya;

    .line 305
    :cond_6
    iget-object v0, p0, Lpcg;->j:Loya;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 309
    :sswitch_a
    const/16 v0, 0x5a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 310
    iget-object v0, p0, Lpcg;->k:[Loya;

    if-nez v0, :cond_8

    move v0, v1

    .line 311
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Loya;

    .line 312
    iget-object v3, p0, Lpcg;->k:[Loya;

    if-eqz v3, :cond_7

    .line 313
    iget-object v3, p0, Lpcg;->k:[Loya;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 315
    :cond_7
    iput-object v2, p0, Lpcg;->k:[Loya;

    .line 316
    :goto_4
    iget-object v2, p0, Lpcg;->k:[Loya;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    .line 317
    iget-object v2, p0, Lpcg;->k:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 318
    iget-object v2, p0, Lpcg;->k:[Loya;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 319
    invoke-virtual {p1}, Loxn;->a()I

    .line 316
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 310
    :cond_8
    iget-object v0, p0, Lpcg;->k:[Loya;

    array-length v0, v0

    goto :goto_3

    .line 322
    :cond_9
    iget-object v2, p0, Lpcg;->k:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 323
    iget-object v2, p0, Lpcg;->k:[Loya;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 327
    :sswitch_b
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpcg;->l:Ljava/lang/String;

    goto/16 :goto_0

    .line 331
    :sswitch_c
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpcg;->m:Ljava/lang/String;

    goto/16 :goto_0

    .line 335
    :sswitch_d
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpcg;->n:Ljava/lang/String;

    goto/16 :goto_0

    .line 339
    :sswitch_e
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpcg;->o:Ljava/lang/String;

    goto/16 :goto_0

    .line 343
    :sswitch_f
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpcg;->p:Ljava/lang/String;

    goto/16 :goto_0

    .line 347
    :sswitch_10
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpcg;->q:Ljava/lang/String;

    goto/16 :goto_0

    .line 351
    :sswitch_11
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpcg;->r:Ljava/lang/String;

    goto/16 :goto_0

    .line 355
    :sswitch_12
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpcg;->s:Ljava/lang/String;

    goto/16 :goto_0

    .line 359
    :sswitch_13
    iget-object v0, p0, Lpcg;->t:Loya;

    if-nez v0, :cond_a

    .line 360
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    iput-object v0, p0, Lpcg;->t:Loya;

    .line 362
    :cond_a
    iget-object v0, p0, Lpcg;->t:Loya;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 366
    :sswitch_14
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpcg;->u:Ljava/lang/String;

    goto/16 :goto_0

    .line 238
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x5a -> :sswitch_a
        0x62 -> :sswitch_b
        0xf2 -> :sswitch_c
        0xfa -> :sswitch_d
        0x102 -> :sswitch_e
        0x10a -> :sswitch_f
        0x112 -> :sswitch_10
        0x11a -> :sswitch_11
        0x25a -> :sswitch_12
        0x5ca -> :sswitch_13
        0x7f2 -> :sswitch_14
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 64
    iget-object v1, p0, Lpcg;->d:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 65
    const/4 v1, 0x1

    iget-object v2, p0, Lpcg;->d:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 67
    :cond_0
    iget-object v1, p0, Lpcg;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 68
    const/4 v1, 0x2

    iget-object v2, p0, Lpcg;->b:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 70
    :cond_1
    iget-object v1, p0, Lpcg;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 71
    const/4 v1, 0x3

    iget-object v2, p0, Lpcg;->c:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 73
    :cond_2
    iget-object v1, p0, Lpcg;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 74
    const/4 v1, 0x4

    iget-object v2, p0, Lpcg;->e:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 76
    :cond_3
    iget-object v1, p0, Lpcg;->f:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 77
    const/4 v1, 0x5

    iget-object v2, p0, Lpcg;->f:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 79
    :cond_4
    iget-object v1, p0, Lpcg;->g:Lpdi;

    if-eqz v1, :cond_5

    .line 80
    const/4 v1, 0x6

    iget-object v2, p0, Lpcg;->g:Lpdi;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 82
    :cond_5
    iget-object v1, p0, Lpcg;->h:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 83
    const/4 v1, 0x7

    iget-object v2, p0, Lpcg;->h:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 85
    :cond_6
    iget-object v1, p0, Lpcg;->i:[Loya;

    if-eqz v1, :cond_8

    .line 86
    iget-object v2, p0, Lpcg;->i:[Loya;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_8

    aget-object v4, v2, v1

    .line 87
    if-eqz v4, :cond_7

    .line 88
    const/16 v5, 0x8

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 86
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 92
    :cond_8
    iget-object v1, p0, Lpcg;->j:Loya;

    if-eqz v1, :cond_9

    .line 93
    const/16 v1, 0x9

    iget-object v2, p0, Lpcg;->j:Loya;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 95
    :cond_9
    iget-object v1, p0, Lpcg;->k:[Loya;

    if-eqz v1, :cond_b

    .line 96
    iget-object v1, p0, Lpcg;->k:[Loya;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_b

    aget-object v3, v1, v0

    .line 97
    if-eqz v3, :cond_a

    .line 98
    const/16 v4, 0xb

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 96
    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 102
    :cond_b
    iget-object v0, p0, Lpcg;->l:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 103
    const/16 v0, 0xc

    iget-object v1, p0, Lpcg;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 105
    :cond_c
    iget-object v0, p0, Lpcg;->m:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 106
    const/16 v0, 0x1e

    iget-object v1, p0, Lpcg;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 108
    :cond_d
    iget-object v0, p0, Lpcg;->n:Ljava/lang/String;

    if-eqz v0, :cond_e

    .line 109
    const/16 v0, 0x1f

    iget-object v1, p0, Lpcg;->n:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 111
    :cond_e
    iget-object v0, p0, Lpcg;->o:Ljava/lang/String;

    if-eqz v0, :cond_f

    .line 112
    const/16 v0, 0x20

    iget-object v1, p0, Lpcg;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 114
    :cond_f
    iget-object v0, p0, Lpcg;->p:Ljava/lang/String;

    if-eqz v0, :cond_10

    .line 115
    const/16 v0, 0x21

    iget-object v1, p0, Lpcg;->p:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 117
    :cond_10
    iget-object v0, p0, Lpcg;->q:Ljava/lang/String;

    if-eqz v0, :cond_11

    .line 118
    const/16 v0, 0x22

    iget-object v1, p0, Lpcg;->q:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 120
    :cond_11
    iget-object v0, p0, Lpcg;->r:Ljava/lang/String;

    if-eqz v0, :cond_12

    .line 121
    const/16 v0, 0x23

    iget-object v1, p0, Lpcg;->r:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 123
    :cond_12
    iget-object v0, p0, Lpcg;->s:Ljava/lang/String;

    if-eqz v0, :cond_13

    .line 124
    const/16 v0, 0x4b

    iget-object v1, p0, Lpcg;->s:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 126
    :cond_13
    iget-object v0, p0, Lpcg;->t:Loya;

    if-eqz v0, :cond_14

    .line 127
    const/16 v0, 0xb9

    iget-object v1, p0, Lpcg;->t:Loya;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 129
    :cond_14
    iget-object v0, p0, Lpcg;->u:Ljava/lang/String;

    if-eqz v0, :cond_15

    .line 130
    const/16 v0, 0xfe

    iget-object v1, p0, Lpcg;->u:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 132
    :cond_15
    iget-object v0, p0, Lpcg;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 134
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lpcg;->a(Loxn;)Lpcg;

    move-result-object v0

    return-object v0
.end method
