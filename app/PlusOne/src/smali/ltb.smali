.class public final Lltb;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lltb;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:I

.field private h:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const/4 v0, 0x0

    new-array v0, v0, [Lltb;

    sput-object v0, Lltb;->a:[Lltb;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Loxq;-><init>()V

    .line 20
    const/high16 v0, -0x80000000

    iput v0, p0, Lltb;->g:I

    .line 9
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 56
    const/4 v0, 0x0

    .line 57
    iget-object v1, p0, Lltb;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 58
    const/4 v0, 0x1

    iget-object v1, p0, Lltb;->b:Ljava/lang/String;

    .line 59
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 61
    :cond_0
    iget-object v1, p0, Lltb;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 62
    const/4 v1, 0x2

    iget-object v2, p0, Lltb;->c:Ljava/lang/String;

    .line 63
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 65
    :cond_1
    iget-object v1, p0, Lltb;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 66
    const/4 v1, 0x3

    iget-object v2, p0, Lltb;->d:Ljava/lang/String;

    .line 67
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 69
    :cond_2
    iget-object v1, p0, Lltb;->f:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 70
    const/4 v1, 0x4

    iget-object v2, p0, Lltb;->f:Ljava/lang/String;

    .line 71
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 73
    :cond_3
    iget v1, p0, Lltb;->g:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_4

    .line 74
    const/4 v1, 0x5

    iget v2, p0, Lltb;->g:I

    .line 75
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 77
    :cond_4
    iget-object v1, p0, Lltb;->h:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 78
    const/4 v1, 0x6

    iget-object v2, p0, Lltb;->h:Ljava/lang/String;

    .line 79
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 81
    :cond_5
    iget-object v1, p0, Lltb;->e:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 82
    const/4 v1, 0x7

    iget-object v2, p0, Lltb;->e:Ljava/lang/String;

    .line 83
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 85
    :cond_6
    iget-object v1, p0, Lltb;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 86
    iput v0, p0, Lltb;->ai:I

    .line 87
    return v0
.end method

.method public a(Loxn;)Lltb;
    .locals 2

    .prologue
    .line 95
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 96
    sparse-switch v0, :sswitch_data_0

    .line 100
    iget-object v1, p0, Lltb;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 101
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lltb;->ah:Ljava/util/List;

    .line 104
    :cond_1
    iget-object v1, p0, Lltb;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 106
    :sswitch_0
    return-object p0

    .line 111
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lltb;->b:Ljava/lang/String;

    goto :goto_0

    .line 115
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lltb;->c:Ljava/lang/String;

    goto :goto_0

    .line 119
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lltb;->d:Ljava/lang/String;

    goto :goto_0

    .line 123
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lltb;->f:Ljava/lang/String;

    goto :goto_0

    .line 127
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 128
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 132
    :cond_2
    iput v0, p0, Lltb;->g:I

    goto :goto_0

    .line 134
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lltb;->g:I

    goto :goto_0

    .line 139
    :sswitch_6
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lltb;->h:Ljava/lang/String;

    goto :goto_0

    .line 143
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lltb;->e:Ljava/lang/String;

    goto :goto_0

    .line 96
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 29
    iget-object v0, p0, Lltb;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 30
    const/4 v0, 0x1

    iget-object v1, p0, Lltb;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 32
    :cond_0
    iget-object v0, p0, Lltb;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 33
    const/4 v0, 0x2

    iget-object v1, p0, Lltb;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 35
    :cond_1
    iget-object v0, p0, Lltb;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 36
    const/4 v0, 0x3

    iget-object v1, p0, Lltb;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 38
    :cond_2
    iget-object v0, p0, Lltb;->f:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 39
    const/4 v0, 0x4

    iget-object v1, p0, Lltb;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 41
    :cond_3
    iget v0, p0, Lltb;->g:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_4

    .line 42
    const/4 v0, 0x5

    iget v1, p0, Lltb;->g:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 44
    :cond_4
    iget-object v0, p0, Lltb;->h:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 45
    const/4 v0, 0x6

    iget-object v1, p0, Lltb;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 47
    :cond_5
    iget-object v0, p0, Lltb;->e:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 48
    const/4 v0, 0x7

    iget-object v1, p0, Lltb;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 50
    :cond_6
    iget-object v0, p0, Lltb;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 52
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Lltb;->a(Loxn;)Lltb;

    move-result-object v0

    return-object v0
.end method
