.class public final Llbv;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lkzl;


# static fields
.field private static final m:Lloy;

.field private static final n:Lloy;

.field private static final o:[I


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Llbu;

.field private final c:Llbt;

.field private d:[I

.field private e:[I

.field private f:[I

.field private g:[I

.field private h:Z

.field private i:Lloy;

.field private j:Lloy;

.field private k:Lloy;

.field private l:Lloy;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 65
    new-instance v0, Lloy;

    const-string v1, "debug.stream.use_large_images"

    invoke-direct {v0, v1}, Lloy;-><init>(Ljava/lang/String;)V

    sput-object v0, Llbv;->m:Lloy;

    .line 67
    new-instance v0, Lloy;

    const-string v1, "debug.stream.dynamic_hats"

    invoke-direct {v0, v1}, Lloy;-><init>(Ljava/lang/String;)V

    sput-object v0, Llbv;->n:Lloy;

    .line 71
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Llbv;->o:[I

    return-void

    :array_0
    .array-data 4
        0x2
        0xf
        0xd
        0x8
        0x4
        0x6
        0x16
        0x17
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 83
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Llbu;

    invoke-direct {v1, p1}, Llbu;-><init>(Landroid/content/Context;)V

    new-instance v2, Llbt;

    invoke-direct {v2, p1}, Llbt;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v0, v1, v2}, Llbv;-><init>(Landroid/content/Context;Llbu;Llbt;)V

    .line 86
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Llbu;Llbt;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    new-instance v0, Lloy;

    const-string v1, "debug.stream.relateds_button"

    invoke-direct {v0, v1}, Lloy;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Llbv;->i:Lloy;

    .line 62
    new-instance v0, Lloy;

    const-string v1, "debug.plus.url_attribution"

    invoke-direct {v0, v1}, Lloy;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Llbv;->j:Lloy;

    .line 63
    new-instance v0, Lloy;

    const-string v1, "debug.stream.prefetch"

    invoke-direct {v0, v1, v2}, Lloy;-><init>(Ljava/lang/String;Z)V

    iput-object v0, p0, Llbv;->k:Lloy;

    .line 64
    new-instance v0, Lloy;

    const-string v1, "debug.stream.prefetchV2"

    invoke-direct {v0, v1, v2}, Lloy;-><init>(Ljava/lang/String;Z)V

    iput-object v0, p0, Llbv;->l:Lloy;

    .line 91
    iput-object p1, p0, Llbv;->a:Landroid/content/Context;

    .line 92
    iput-object p2, p0, Llbv;->b:Llbu;

    .line 93
    iput-object p3, p0, Llbv;->c:Llbt;

    .line 94
    return-void
.end method

.method private a(Ljava/util/ArrayList;)[I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)[I"
        }
    .end annotation

    .prologue
    .line 267
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v2, v0, [I

    .line 268
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 269
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v2, v1

    .line 268
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 271
    :cond_0
    return-object v2
.end method


# virtual methods
.method public a(I[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 282
    iget-object v0, p0, Llbv;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Lhzt;->b(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 283
    const-string v1, "activity_view"

    const-string v3, "unique_activity_id=?"

    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p3, v4, v2

    const-string v8, "1"

    move-object v2, p2

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 288
    iget-object v1, p0, Llbv;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 289
    invoke-static {p3}, Llbi;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 288
    invoke-interface {v0, v1, v2}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 290
    return-object v0
.end method

.method public a(I[Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;)Landroid/database/Cursor;
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 310
    const-string v3, "stream_key=?"

    .line 311
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p3, v4, v0

    .line 313
    if-eqz p4, :cond_0

    .line 314
    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v0, " AND (activity_flags&64=0)"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    move-object v3, v0

    .line 318
    :cond_0
    if-eqz p5, :cond_1

    .line 319
    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v0, " AND (content_flags&2213!=0 AND content_flags&16=0)"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    move-object v3, v0

    .line 322
    :cond_1
    iget-object v0, p0, Llbv;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Lhzt;->b(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 323
    const-string v1, "activities_stream_view"

    const-string v7, "sort_index ASC"

    move-object v2, p2

    move-object v6, v5

    move-object v8, p6

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 327
    iget-object v1, p0, Llbv;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {p3}, Llbi;->c(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 328
    return-object v0

    .line 314
    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 319
    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public a()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lkzj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 107
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 109
    iget-object v0, p0, Llbv;->c:Llbt;

    invoke-virtual {v0}, Llbt;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 110
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 111
    iget-object v3, p0, Llbv;->c:Llbt;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Llbt;->a(Ljava/lang/Object;)Llno;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 110
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 113
    :cond_0
    return-object v1
.end method

.method public a(Ljava/lang/String;)Lkzj;
    .locals 2

    .prologue
    .line 98
    invoke-static {p1}, Llbk;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 99
    if-nez v0, :cond_0

    .line 100
    const/4 v0, 0x0

    .line 102
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Llbv;->c:Llbt;

    invoke-virtual {v1, v0}, Llbt;->a(Ljava/lang/Object;)Llno;

    move-result-object v0

    check-cast v0, Lkzj;

    goto :goto_0
.end method

.method public a(I)Lkzk;
    .locals 2

    .prologue
    .line 118
    iget-object v0, p0, Llbv;->b:Llbu;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Llbu;->a(Ljava/lang/Object;)Llno;

    move-result-object v0

    check-cast v0, Lkzk;

    return-object v0
.end method

.method public a(Landroid/content/Context;I)[I
    .locals 3

    .prologue
    .line 189
    invoke-virtual {p0, p1, p2}, Llbv;->c(Landroid/content/Context;I)Ljava/util/ArrayList;

    move-result-object v0

    .line 191
    iget-object v1, p0, Llbv;->d:[I

    if-eqz v1, :cond_0

    iget-object v1, p0, Llbv;->d:[I

    array-length v1, v1

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-eq v1, v2, :cond_1

    .line 192
    :cond_0
    invoke-direct {p0, v0}, Llbv;->a(Ljava/util/ArrayList;)[I

    move-result-object v0

    iput-object v0, p0, Llbv;->d:[I

    .line 194
    :cond_1
    iget-object v0, p0, Llbv;->d:[I

    return-object v0
.end method

.method public a(Landroid/content/Context;IZ)[I
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 145
    invoke-virtual {p0, p1, p2}, Llbv;->b(Landroid/content/Context;I)Ljava/util/ArrayList;

    move-result-object v1

    .line 146
    if-eqz p3, :cond_0

    .line 147
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 148
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 151
    :cond_0
    const-class v0, Lieh;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    .line 152
    sget-object v2, Llbq;->a:Lief;

    invoke-interface {v0, v2, p2}, Lieh;->b(Lief;I)Z

    move-result v0

    .line 154
    if-eqz v0, :cond_1

    .line 155
    const/16 v0, 0xa

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 158
    :cond_1
    if-eqz p3, :cond_4

    .line 159
    iget-object v0, p0, Llbv;->f:[I

    if-eqz v0, :cond_2

    iget-object v0, p0, Llbv;->f:[I

    array-length v0, v0

    .line 160
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-eq v0, v2, :cond_3

    .line 161
    :cond_2
    invoke-direct {p0, v1}, Llbv;->a(Ljava/util/ArrayList;)[I

    move-result-object v0

    iput-object v0, p0, Llbv;->f:[I

    .line 163
    :cond_3
    iget-object v0, p0, Llbv;->f:[I

    .line 169
    :goto_0
    return-object v0

    .line 165
    :cond_4
    iget-object v0, p0, Llbv;->e:[I

    if-eqz v0, :cond_5

    iget-object v0, p0, Llbv;->e:[I

    array-length v0, v0

    .line 166
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-eq v0, v2, :cond_6

    .line 167
    :cond_5
    invoke-direct {p0, v1}, Llbv;->a(Ljava/util/ArrayList;)[I

    move-result-object v0

    iput-object v0, p0, Llbv;->e:[I

    .line 169
    :cond_6
    iget-object v0, p0, Llbv;->e:[I

    goto :goto_0
.end method

.method public a(Z)[I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 199
    if-eqz p1, :cond_1

    .line 200
    iget-object v0, p0, Llbv;->g:[I

    if-nez v0, :cond_0

    .line 201
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Llbv;->g:[I

    .line 207
    :cond_0
    iget-object v0, p0, Llbv;->g:[I

    .line 227
    :goto_0
    return-object v0

    .line 209
    :cond_1
    iget-object v0, p0, Llbv;->a:Landroid/content/Context;

    const-class v1, Lkzf;

    .line 210
    invoke-static {v0, v1}, Llnh;->c(Landroid/content/Context;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v3

    .line 211
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 212
    sget-object v0, Llbv;->o:[I

    goto :goto_0

    .line 214
    :cond_2
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 215
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    move v1, v2

    :goto_1
    if-ge v1, v5, :cond_4

    .line 216
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkzf;

    .line 217
    invoke-interface {v0}, Lkzf;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 218
    if-eqz v0, :cond_3

    .line 219
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 215
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 222
    :cond_4
    sget-object v0, Llbv;->o:[I

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v5, v0, 0x8

    .line 223
    sget-object v0, Llbv;->o:[I

    invoke-static {v0, v5}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v1

    .line 224
    sget-object v0, Llbv;->o:[I

    const/16 v0, 0x8

    move v3, v0

    :goto_2
    if-ge v3, v5, :cond_5

    .line 225
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v1, v3

    .line 224
    add-int/lit8 v3, v3, 0x1

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_5
    move-object v0, v1

    .line 227
    goto :goto_0

    .line 201
    nop

    :array_0
    .array-data 4
        0x2
        0x4
        0x16
    .end array-data
.end method

.method public b(I[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 295
    iget-object v0, p0, Llbv;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Lhzt;->b(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 296
    const-string v1, "comments_view"

    const-string v3, "activity_id=?"

    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p3, v4, v2

    const-string v7, "created ASC"

    move-object v2, p2

    move-object v6, v5

    move-object v8, v5

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 301
    iget-object v1, p0, Llbv;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 302
    invoke-static {p3}, Llbi;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 301
    invoke-interface {v0, v1, v2}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 303
    return-object v0
.end method

.method public b()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 123
    iget-object v0, p0, Llbv;->b:Llbu;

    invoke-virtual {v0}, Llbu;->a()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public b(Landroid/content/Context;I)Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 128
    iget-object v0, p0, Llbv;->b:Llbu;

    invoke-virtual {v0}, Llbu;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 129
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 130
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 131
    iget-object v0, p0, Llbv;->b:Llbu;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v0, v4}, Llbu;->a(Ljava/lang/Object;)Llno;

    move-result-object v0

    check-cast v0, Lkzk;

    .line 132
    invoke-interface {v0}, Lkzk;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 133
    if-eqz v0, :cond_0

    .line 134
    invoke-virtual {v3, v0}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 130
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 137
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 333
    iput-boolean p1, p0, Llbv;->h:Z

    .line 334
    return-void
.end method

.method public b(I)Z
    .locals 2

    .prologue
    .line 240
    iget-object v0, p0, Llbv;->a:Landroid/content/Context;

    const-class v1, Lieh;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    .line 241
    iget-object v1, p0, Llbv;->j:Lloy;

    sget-object v1, Llbq;->c:Lief;

    .line 242
    invoke-interface {v0, v1, p1}, Lieh;->b(Lief;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(Landroid/content/Context;I)Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 175
    iget-object v0, p0, Llbv;->b:Llbu;

    invoke-virtual {v0}, Llbu;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 176
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 177
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 178
    iget-object v0, p0, Llbv;->b:Llbu;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v0, v4}, Llbu;->a(Ljava/lang/Object;)Llno;

    move-result-object v0

    check-cast v0, Lkzk;

    .line 179
    invoke-interface {v0, p1, p2}, Lkzk;->a(Landroid/content/Context;I)Ljava/util/ArrayList;

    move-result-object v0

    .line 180
    if-eqz v0, :cond_0

    .line 181
    invoke-virtual {v3, v0}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 177
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 184
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 338
    iget-boolean v0, p0, Llbv;->h:Z

    return v0
.end method

.method public c(I)Z
    .locals 2

    .prologue
    .line 276
    iget-object v0, p0, Llbv;->a:Landroid/content/Context;

    const-class v1, Lieh;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    .line 277
    sget-object v1, Llbq;->d:Lief;

    invoke-interface {v0, v1, p1}, Lieh;->b(Lief;I)Z

    move-result v0

    return v0
.end method

.method public d(I)Z
    .locals 2

    .prologue
    .line 247
    iget-object v0, p0, Llbv;->a:Landroid/content/Context;

    const-class v1, Lieh;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    .line 248
    sget-object v1, Llbv;->m:Lloy;

    sget-object v1, Llbq;->e:Lief;

    .line 249
    invoke-interface {v0, v1, p1}, Lieh;->b(Lief;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e(I)Z
    .locals 2

    .prologue
    .line 254
    iget-object v0, p0, Llbv;->a:Landroid/content/Context;

    const-class v1, Lieh;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    .line 255
    sget-object v1, Llbv;->n:Lloy;

    sget-object v1, Llbq;->h:Lief;

    .line 256
    invoke-interface {v0, v1, p1}, Lieh;->b(Lief;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f(I)Z
    .locals 2

    .prologue
    .line 261
    iget-object v0, p0, Llbv;->a:Landroid/content/Context;

    const-class v1, Lieh;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    .line 262
    iget-object v1, p0, Llbv;->i:Lloy;

    sget-object v1, Llbq;->b:Lief;

    .line 263
    invoke-interface {v0, v1, p1}, Lieh;->b(Lief;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g(I)Z
    .locals 2

    .prologue
    .line 343
    iget-object v0, p0, Llbv;->a:Landroid/content/Context;

    const-class v1, Lieh;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    .line 344
    iget-object v1, p0, Llbv;->k:Lloy;

    sget-object v1, Llbq;->f:Lief;

    invoke-interface {v0, v1, p1}, Lieh;->b(Lief;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h(I)Z
    .locals 2

    .prologue
    .line 350
    iget-object v0, p0, Llbv;->a:Landroid/content/Context;

    const-class v1, Lieh;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    .line 351
    iget-object v1, p0, Llbv;->l:Lloy;

    sget-object v1, Llbq;->g:Lief;

    invoke-interface {v0, v1, p1}, Lieh;->b(Lief;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
