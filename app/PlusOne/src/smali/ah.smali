.class final Lah;
.super Lae;
.source "PG"

# interfaces
.implements Landroid/view/LayoutInflater$Factory;


# static fields
.field private static g:Z

.field private static y:Landroid/view/animation/Interpolator;

.field private static z:Landroid/view/animation/Interpolator;


# instance fields
.field a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lu;",
            ">;"
        }
    .end annotation
.end field

.field b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lu;",
            ">;"
        }
    .end annotation
.end field

.field c:I

.field d:Lz;

.field e:Lad;

.field f:Ljava/lang/String;

.field private h:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private i:[Ljava/lang/Runnable;

.field private j:Z

.field private k:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ll;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lu;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ll;",
            ">;"
        }
    .end annotation
.end field

.field private o:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private p:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lag;",
            ">;"
        }
    .end annotation
.end field

.field private q:Lu;

.field private r:Z

.field private s:Z

.field private t:Z

.field private u:Z

.field private v:Landroid/os/Bundle;

.field private w:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;"
        }
    .end annotation
.end field

.field private x:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/high16 v3, 0x40200000    # 2.5f

    const/high16 v2, 0x3fc00000    # 1.5f

    .line 410
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lah;->g:Z

    .line 746
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0, v3}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    sput-object v0, Lah;->y:Landroid/view/animation/Interpolator;

    .line 747
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0, v2}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    sput-object v0, Lah;->z:Landroid/view/animation/Interpolator;

    .line 748
    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0, v3}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    .line 749
    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0, v2}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    return-void

    .line 410
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 409
    invoke-direct {p0}, Lae;-><init>()V

    .line 436
    const/4 v0, 0x0

    iput v0, p0, Lah;->c:I

    .line 448
    iput-object v1, p0, Lah;->v:Landroid/os/Bundle;

    .line 449
    iput-object v1, p0, Lah;->w:Landroid/util/SparseArray;

    .line 451
    new-instance v0, Lai;

    invoke-direct {v0, p0}, Lai;-><init>(Lah;)V

    iput-object v0, p0, Lah;->x:Ljava/lang/Runnable;

    .line 2205
    return-void
.end method

.method static a(FF)Landroid/view/animation/Animation;
    .locals 4

    .prologue
    .line 769
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, p0, p1}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 770
    sget-object v1, Lah;->z:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 771
    const-wide/16 v2, 0xdc

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 772
    return-object v0
.end method

.method static a(FFFF)Landroid/view/animation/Animation;
    .locals 12

    .prologue
    const-wide/16 v10, 0xdc

    const/4 v5, 0x1

    const/high16 v6, 0x3f000000    # 0.5f

    .line 755
    new-instance v9, Landroid/view/animation/AnimationSet;

    const/4 v0, 0x0

    invoke-direct {v9, v0}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 756
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    move v1, p0

    move v2, p1

    move v3, p0

    move v4, p1

    move v7, v5

    move v8, v6

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 758
    sget-object v1, Lah;->y:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/animation/ScaleAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 759
    invoke-virtual {v0, v10, v11}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V

    .line 760
    invoke-virtual {v9, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 761
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, p2, p3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 762
    sget-object v1, Lah;->z:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 763
    invoke-virtual {v0, v10, v11}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 764
    invoke-virtual {v9, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 765
    return-object v9
.end method

.method private a(Ljava/lang/RuntimeException;)V
    .locals 5

    .prologue
    .line 459
    const-string v0, "FragmentManager"

    invoke-virtual {p1}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 460
    const-string v0, "FragmentManager"

    const-string v1, "Activity state:"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 461
    new-instance v0, Lgj;

    const-string v1, "FragmentManager"

    invoke-direct {v0, v1}, Lgj;-><init>(Ljava/lang/String;)V

    .line 462
    new-instance v1, Ljava/io/PrintWriter;

    invoke-direct {v1, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    .line 463
    iget-object v0, p0, Lah;->d:Lz;

    if-eqz v0, :cond_0

    .line 465
    :try_start_0
    iget-object v0, p0, Lah;->d:Lz;

    const-string v2, "  "

    const/4 v3, 0x0

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {v0, v2, v3, v1, v4}, Lz;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 476
    :goto_0
    throw p1

    .line 466
    :catch_0
    move-exception v0

    .line 467
    const-string v1, "FragmentManager"

    const-string v2, "Failed dumping state"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 471
    :cond_0
    :try_start_1
    const-string v0, "  "

    const/4 v2, 0x0

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {p0, v0, v2, v1, v3}, Lah;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 472
    :catch_1
    move-exception v0

    .line 473
    const-string v1, "FragmentManager"

    const-string v2, "Failed dumping state"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static d(I)I
    .locals 1

    .prologue
    .line 2069
    const/4 v0, 0x0

    .line 2070
    sparse-switch p0, :sswitch_data_0

    .line 2081
    :goto_0
    return v0

    .line 2072
    :sswitch_0
    const/16 v0, 0x2002

    .line 2073
    goto :goto_0

    .line 2075
    :sswitch_1
    const/16 v0, 0x1001

    .line 2076
    goto :goto_0

    .line 2078
    :sswitch_2
    const/16 v0, 0x1003

    goto :goto_0

    .line 2070
    :sswitch_data_0
    .sparse-switch
        0x1001 -> :sswitch_0
        0x1003 -> :sswitch_2
        0x2002 -> :sswitch_1
    .end sparse-switch
.end method

.method private w()V
    .locals 3

    .prologue
    .line 1364
    iget-boolean v0, p0, Lah;->s:Z

    if-eqz v0, :cond_0

    .line 1365
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can not perform this action after onSaveInstanceState"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1368
    :cond_0
    iget-object v0, p0, Lah;->f:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1369
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can not perform this action inside of "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lah;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1372
    :cond_1
    return-void
.end method


# virtual methods
.method public a(Ll;)I
    .locals 2

    .prologue
    .line 1401
    monitor-enter p0

    .line 1402
    :try_start_0
    iget-object v0, p0, Lah;->o:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lah;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_2

    .line 1403
    :cond_0
    iget-object v0, p0, Lah;->n:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 1404
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lah;->n:Ljava/util/ArrayList;

    .line 1406
    :cond_1
    iget-object v0, p0, Lah;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 1407
    iget-object v1, p0, Lah;->n:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1409
    monitor-exit p0

    .line 1415
    :goto_0
    return v0

    .line 1412
    :cond_2
    iget-object v0, p0, Lah;->o:Ljava/util/ArrayList;

    iget-object v1, p0, Lah;->o:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1413
    iget-object v1, p0, Lah;->n:Ljava/util/ArrayList;

    invoke-virtual {v1, v0, p1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1415
    monitor-exit p0

    goto :goto_0

    .line 1417
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method a(Lu;IZI)Landroid/view/animation/Animation;
    .locals 6

    .prologue
    const v5, 0x3f79999a    # 0.975f

    const/4 v1, 0x0

    const/4 v4, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    .line 777
    iget v0, p1, Lu;->G:I

    invoke-virtual {p1, p3}, Lu;->h(Z)Landroid/view/animation/Animation;

    move-result-object v0

    .line 779
    if-eqz v0, :cond_1

    .line 831
    :cond_0
    :goto_0
    return-object v0

    .line 783
    :cond_1
    iget v0, p1, Lu;->G:I

    if-eqz v0, :cond_2

    .line 784
    iget-object v0, p0, Lah;->d:Lz;

    iget v2, p1, Lu;->G:I

    invoke-static {v0, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 785
    if-nez v0, :cond_0

    .line 790
    :cond_2
    if-nez p2, :cond_3

    move-object v0, v1

    .line 791
    goto :goto_0

    .line 794
    :cond_3
    const/4 v0, -0x1

    sparse-switch p2, :sswitch_data_0

    .line 795
    :goto_1
    if-gez v0, :cond_7

    move-object v0, v1

    .line 796
    goto :goto_0

    .line 794
    :sswitch_0
    if-eqz p3, :cond_4

    const/4 v0, 0x1

    goto :goto_1

    :cond_4
    const/4 v0, 0x2

    goto :goto_1

    :sswitch_1
    if-eqz p3, :cond_5

    const/4 v0, 0x3

    goto :goto_1

    :cond_5
    const/4 v0, 0x4

    goto :goto_1

    :sswitch_2
    if-eqz p3, :cond_6

    const/4 v0, 0x5

    goto :goto_1

    :cond_6
    const/4 v0, 0x6

    goto :goto_1

    .line 799
    :cond_7
    packed-switch v0, :pswitch_data_0

    .line 814
    if-nez p4, :cond_8

    iget-object v0, p0, Lah;->d:Lz;

    invoke-virtual {v0}, Lz;->getWindow()Landroid/view/Window;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 815
    iget-object v0, p0, Lah;->d:Lz;

    invoke-virtual {v0}, Lz;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iget p4, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 817
    :cond_8
    if-nez p4, :cond_9

    move-object v0, v1

    .line 818
    goto :goto_0

    .line 801
    :pswitch_0
    iget-object v0, p0, Lah;->d:Lz;

    const/high16 v0, 0x3f900000    # 1.125f

    invoke-static {v0, v3, v4, v3}, Lah;->a(FFFF)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    .line 803
    :pswitch_1
    iget-object v0, p0, Lah;->d:Lz;

    invoke-static {v3, v5, v3, v4}, Lah;->a(FFFF)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    .line 805
    :pswitch_2
    iget-object v0, p0, Lah;->d:Lz;

    invoke-static {v5, v3, v4, v3}, Lah;->a(FFFF)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    .line 807
    :pswitch_3
    iget-object v0, p0, Lah;->d:Lz;

    const v0, 0x3f89999a    # 1.075f

    invoke-static {v3, v0, v3, v4}, Lah;->a(FFFF)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    .line 809
    :pswitch_4
    iget-object v0, p0, Lah;->d:Lz;

    invoke-static {v4, v3}, Lah;->a(FF)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    .line 811
    :pswitch_5
    iget-object v0, p0, Lah;->d:Lz;

    invoke-static {v3, v4}, Lah;->a(FF)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    :cond_9
    move-object v0, v1

    .line 831
    goto :goto_0

    .line 794
    :sswitch_data_0
    .sparse-switch
        0x1001 -> :sswitch_0
        0x1003 -> :sswitch_2
        0x2002 -> :sswitch_1
    .end sparse-switch

    .line 799
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public a()Lat;
    .locals 1

    .prologue
    .line 481
    new-instance v0, Ll;

    invoke-direct {v0, p0}, Ll;-><init>(Lah;)V

    return-object v0
.end method

.method public a(I)Lu;
    .locals 3

    .prologue
    .line 1308
    iget-object v0, p0, Lah;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    .line 1310
    iget-object v0, p0, Lah;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_2

    .line 1311
    iget-object v0, p0, Lah;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lu;

    .line 1312
    if-eqz v0, :cond_1

    iget v2, v0, Lu;->w:I

    if-ne v2, p1, :cond_1

    .line 1326
    :cond_0
    :goto_1
    return-object v0

    .line 1310
    :cond_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 1317
    :cond_2
    iget-object v0, p0, Lah;->a:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    .line 1319
    iget-object v0, p0, Lah;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_2
    if-ltz v1, :cond_4

    .line 1320
    iget-object v0, p0, Lah;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lu;

    .line 1321
    if-eqz v0, :cond_3

    iget v2, v0, Lu;->w:I

    if-eq v2, p1, :cond_0

    .line 1319
    :cond_3
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_2

    .line 1326
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a(Landroid/os/Bundle;Ljava/lang/String;)Lu;
    .locals 5

    .prologue
    const/4 v0, -0x1

    .line 579
    invoke-virtual {p1, p2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 580
    if-ne v1, v0, :cond_1

    .line 581
    const/4 v0, 0x0

    .line 592
    :cond_0
    :goto_0
    return-object v0

    .line 583
    :cond_1
    iget-object v0, p0, Lah;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_2

    .line 584
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Fragment no longer exists for key "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": index "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lah;->a(Ljava/lang/RuntimeException;)V

    .line 587
    :cond_2
    iget-object v0, p0, Lah;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lu;

    .line 588
    if-nez v0, :cond_0

    .line 589
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Fragment no longer exists for key "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": index "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v2}, Lah;->a(Ljava/lang/RuntimeException;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Lu;
    .locals 3

    .prologue
    .line 1330
    iget-object v0, p0, Lah;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    if-eqz p1, :cond_2

    .line 1332
    iget-object v0, p0, Lah;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_2

    .line 1333
    iget-object v0, p0, Lah;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lu;

    .line 1334
    if-eqz v0, :cond_1

    iget-object v2, v0, Lu;->y:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1348
    :cond_0
    :goto_1
    return-object v0

    .line 1332
    :cond_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 1339
    :cond_2
    iget-object v0, p0, Lah;->a:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    if-eqz p1, :cond_4

    .line 1341
    iget-object v0, p0, Lah;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_2
    if-ltz v1, :cond_4

    .line 1342
    iget-object v0, p0, Lah;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lu;

    .line 1343
    if-eqz v0, :cond_3

    iget-object v2, v0, Lu;->y:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1341
    :cond_3
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_2

    .line 1348
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a(Lu;)Lx;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 602
    iget v1, p1, Lu;->f:I

    if-gez v1, :cond_0

    .line 603
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Fragment "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is not currently in the FragmentManager"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lah;->a(Ljava/lang/RuntimeException;)V

    .line 606
    :cond_0
    iget v1, p1, Lu;->a:I

    if-lez v1, :cond_1

    .line 607
    invoke-virtual {p0, p1}, Lah;->g(Lu;)Landroid/os/Bundle;

    move-result-object v1

    .line 608
    if-eqz v1, :cond_1

    new-instance v0, Lx;

    invoke-direct {v0, v1}, Lx;-><init>(Landroid/os/Bundle;)V

    .line 610
    :cond_1
    return-object v0
.end method

.method public a(II)V
    .locals 3

    .prologue
    .line 523
    if-gez p1, :cond_0

    .line 524
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Bad id: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 526
    :cond_0
    new-instance v0, Lal;

    invoke-direct {v0, p0, p1, p2}, Lal;-><init>(Lah;II)V

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lah;->a(Ljava/lang/Runnable;Z)V

    .line 531
    return-void
.end method

.method a(IIIZ)V
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 1112
    iget-object v0, p0, Lah;->d:Lz;

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 1113
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No activity"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1116
    :cond_0
    if-nez p4, :cond_2

    iget v0, p0, Lah;->c:I

    if-ne v0, p1, :cond_2

    .line 1142
    :cond_1
    :goto_0
    return-void

    .line 1120
    :cond_2
    iput p1, p0, Lah;->c:I

    .line 1121
    iget-object v0, p0, Lah;->a:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    move v6, v5

    move v7, v5

    .line 1123
    :goto_1
    iget-object v0, p0, Lah;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v6, v0, :cond_3

    .line 1124
    iget-object v0, p0, Lah;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lu;

    .line 1125
    if-eqz v1, :cond_5

    move-object v0, p0

    move v2, p1

    move v3, p2

    move v4, p3

    .line 1126
    invoke-virtual/range {v0 .. v5}, Lah;->a(Lu;IIIZ)V

    .line 1127
    iget-object v0, v1, Lu;->M:Lbd;

    if-eqz v0, :cond_5

    .line 1128
    iget-object v0, v1, Lu;->M:Lbd;

    invoke-virtual {v0}, Lbd;->a()Z

    move-result v0

    or-int/2addr v7, v0

    move v1, v7

    .line 1123
    :goto_2
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    move v7, v1

    goto :goto_1

    .line 1133
    :cond_3
    if-nez v7, :cond_4

    .line 1134
    invoke-virtual {p0}, Lah;->g()V

    .line 1137
    :cond_4
    iget-boolean v0, p0, Lah;->r:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lah;->d:Lz;

    if-eqz v0, :cond_1

    iget v0, p0, Lah;->c:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    .line 1138
    iget-object v0, p0, Lah;->d:Lz;

    invoke-virtual {v0}, Lz;->az_()V

    .line 1139
    iput-boolean v5, p0, Lah;->r:Z

    goto :goto_0

    :cond_5
    move v1, v7

    goto :goto_2
.end method

.method public a(ILl;)V
    .locals 3

    .prologue
    .line 1421
    monitor-enter p0

    .line 1422
    :try_start_0
    iget-object v0, p0, Lah;->n:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 1423
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lah;->n:Ljava/util/ArrayList;

    .line 1425
    :cond_0
    iget-object v0, p0, Lah;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 1426
    if-ge p1, v0, :cond_1

    .line 1427
    iget-object v0, p0, Lah;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, p1, p2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1442
    :goto_0
    monitor-exit p0

    return-void

    .line 1430
    :cond_1
    :goto_1
    if-ge v0, p1, :cond_3

    .line 1431
    iget-object v1, p0, Lah;->n:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1432
    iget-object v1, p0, Lah;->o:Ljava/util/ArrayList;

    if-nez v1, :cond_2

    .line 1433
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lah;->o:Ljava/util/ArrayList;

    .line 1435
    :cond_2
    iget-object v1, p0, Lah;->o:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1437
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1439
    :cond_3
    iget-object v0, p0, Lah;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1442
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method a(IZ)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1108
    invoke-virtual {p0, p1, v0, v0, p2}, Lah;->a(IIIZ)V

    .line 1109
    return-void
.end method

.method public a(Lag;)V
    .locals 1

    .prologue
    .line 555
    iget-object v0, p0, Lah;->p:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 556
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lah;->p:Ljava/util/ArrayList;

    .line 558
    :cond_0
    iget-object v0, p0, Lah;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 559
    return-void
.end method

.method public a(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 1961
    iget-object v0, p0, Lah;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 1962
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lah;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1963
    iget-object v0, p0, Lah;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lu;

    .line 1964
    if-eqz v0, :cond_0

    .line 1965
    invoke-virtual {v0, p1}, Lu;->a(Landroid/content/res/Configuration;)V

    .line 1962
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1969
    :cond_1
    return-void
.end method

.method public a(Landroid/os/Bundle;Ljava/lang/String;Lu;)V
    .locals 3

    .prologue
    .line 570
    iget v0, p3, Lu;->f:I

    if-gez v0, :cond_0

    .line 571
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not currently in the FragmentManager"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lah;->a(Ljava/lang/RuntimeException;)V

    .line 574
    :cond_0
    iget v0, p3, Lu;->f:I

    invoke-virtual {p1, p2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 575
    return-void
.end method

.method a(Landroid/os/Parcelable;Ljava/util/ArrayList;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Parcelable;",
            "Ljava/util/ArrayList",
            "<",
            "Lu;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 1788
    if-nez p1, :cond_1

    .line 1896
    :cond_0
    :goto_0
    return-void

    .line 1789
    :cond_1
    check-cast p1, Lao;

    .line 1790
    iget-object v0, p1, Lao;->a:[Lar;

    if-eqz v0, :cond_0

    .line 1794
    if-eqz p2, :cond_3

    move v1, v2

    .line 1795
    :goto_1
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 1796
    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lu;

    .line 1797
    iget-object v3, p1, Lao;->a:[Lar;

    iget v4, v0, Lu;->f:I

    aget-object v3, v3, v4

    .line 1799
    iput-object v0, v3, Lar;->b:Lu;

    .line 1800
    iput-object v6, v0, Lu;->e:Landroid/util/SparseArray;

    .line 1801
    iput v2, v0, Lu;->r:I

    .line 1802
    iput-boolean v2, v0, Lu;->p:Z

    .line 1803
    iput-boolean v2, v0, Lu;->l:Z

    .line 1804
    iput-object v6, v0, Lu;->i:Lu;

    .line 1805
    iget-object v4, v3, Lar;->a:Landroid/os/Bundle;

    if-eqz v4, :cond_2

    .line 1806
    iget-object v4, v3, Lar;->a:Landroid/os/Bundle;

    iget-object v5, p0, Lah;->d:Lz;

    invoke-virtual {v5}, Lz;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 1807
    iget-object v4, v3, Lar;->a:Landroid/os/Bundle;

    const-string v5, "android:view_state"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;

    move-result-object v4

    iput-object v4, v0, Lu;->e:Landroid/util/SparseArray;

    .line 1809
    iget-object v3, v3, Lar;->a:Landroid/os/Bundle;

    iput-object v3, v0, Lu;->d:Landroid/os/Bundle;

    .line 1795
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1816
    :cond_3
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p1, Lao;->a:[Lar;

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lah;->a:Ljava/util/ArrayList;

    .line 1817
    iget-object v0, p0, Lah;->k:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    .line 1818
    iget-object v0, p0, Lah;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_4
    move v0, v2

    .line 1820
    :goto_2
    iget-object v1, p1, Lao;->a:[Lar;

    array-length v1, v1

    if-ge v0, v1, :cond_7

    .line 1821
    iget-object v1, p1, Lao;->a:[Lar;

    aget-object v1, v1, v0

    .line 1822
    if-eqz v1, :cond_5

    .line 1823
    iget-object v3, p0, Lah;->d:Lz;

    iget-object v4, p0, Lah;->q:Lu;

    invoke-virtual {v1, v3, v4}, Lar;->a(Lz;Lu;)Lu;

    move-result-object v3

    .line 1824
    iget-object v4, p0, Lah;->a:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1829
    iput-object v6, v1, Lar;->b:Lu;

    .line 1820
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1831
    :cond_5
    iget-object v1, p0, Lah;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1832
    iget-object v1, p0, Lah;->k:Ljava/util/ArrayList;

    if-nez v1, :cond_6

    .line 1833
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lah;->k:Ljava/util/ArrayList;

    .line 1835
    :cond_6
    iget-object v1, p0, Lah;->k:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1841
    :cond_7
    if-eqz p2, :cond_a

    move v3, v2

    .line 1842
    :goto_4
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_a

    .line 1843
    invoke-virtual {p2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lu;

    .line 1844
    iget v1, v0, Lu;->j:I

    if-ltz v1, :cond_8

    .line 1845
    iget v1, v0, Lu;->j:I

    iget-object v4, p0, Lah;->a:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v1, v4, :cond_9

    .line 1846
    iget-object v1, p0, Lah;->a:Ljava/util/ArrayList;

    iget v4, v0, Lu;->j:I

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lu;

    iput-object v1, v0, Lu;->i:Lu;

    .line 1842
    :cond_8
    :goto_5
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_4

    .line 1848
    :cond_9
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "Re-attaching retained fragment "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " target no longer exists: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v4, v0, Lu;->j:I

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1850
    iput-object v6, v0, Lu;->i:Lu;

    goto :goto_5

    .line 1857
    :cond_a
    iget-object v0, p1, Lao;->b:[I

    if-eqz v0, :cond_d

    .line 1858
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p1, Lao;->b:[I

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lah;->b:Ljava/util/ArrayList;

    move v1, v2

    .line 1859
    :goto_6
    iget-object v0, p1, Lao;->b:[I

    array-length v0, v0

    if-ge v1, v0, :cond_e

    .line 1860
    iget-object v0, p0, Lah;->a:Ljava/util/ArrayList;

    iget-object v3, p1, Lao;->b:[I

    aget v3, v3, v1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lu;

    .line 1861
    if-nez v0, :cond_b

    .line 1862
    new-instance v3, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "No instantiated fragment for index #"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p1, Lao;->b:[I

    aget v5, v5, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v3}, Lah;->a(Ljava/lang/RuntimeException;)V

    .line 1865
    :cond_b
    const/4 v3, 0x1

    iput-boolean v3, v0, Lu;->l:Z

    .line 1866
    iget-object v3, p0, Lah;->b:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 1868
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already added!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1870
    :cond_c
    iget-object v3, p0, Lah;->b:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1859
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    .line 1873
    :cond_d
    iput-object v6, p0, Lah;->b:Ljava/util/ArrayList;

    .line 1877
    :cond_e
    iget-object v0, p1, Lao;->c:[Lr;

    if-eqz v0, :cond_10

    .line 1878
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p1, Lao;->c:[Lr;

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lah;->l:Ljava/util/ArrayList;

    .line 1879
    :goto_7
    iget-object v0, p1, Lao;->c:[Lr;

    array-length v0, v0

    if-ge v2, v0, :cond_0

    .line 1880
    iget-object v0, p1, Lao;->c:[Lr;

    aget-object v0, v0, v2

    invoke-virtual {v0, p0}, Lr;->a(Lah;)Ll;

    move-result-object v0

    .line 1881
    iget-object v1, p0, Lah;->l:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1889
    iget v1, v0, Ll;->g:I

    if-ltz v1, :cond_f

    .line 1890
    iget v1, v0, Ll;->g:I

    invoke-virtual {p0, v1, v0}, Lah;->a(ILl;)V

    .line 1879
    :cond_f
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    .line 1894
    :cond_10
    iput-object v6, p0, Lah;->l:Ljava/util/ArrayList;

    goto/16 :goto_0
.end method

.method public a(Ljava/lang/Runnable;Z)V
    .locals 2

    .prologue
    .line 1382
    if-nez p2, :cond_0

    .line 1383
    invoke-direct {p0}, Lah;->w()V

    .line 1385
    :cond_0
    monitor-enter p0

    .line 1386
    :try_start_0
    iget-boolean v0, p0, Lah;->t:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lah;->d:Lz;

    if-nez v0, :cond_2

    .line 1387
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Activity has been destroyed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1397
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1389
    :cond_2
    :try_start_1
    iget-object v0, p0, Lah;->h:Ljava/util/ArrayList;

    if-nez v0, :cond_3

    .line 1390
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lah;->h:Ljava/util/ArrayList;

    .line 1392
    :cond_3
    iget-object v0, p0, Lah;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1393
    iget-object v0, p0, Lah;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    .line 1394
    iget-object v0, p0, Lah;->d:Lz;

    iget-object v0, v0, Lz;->a:Landroid/os/Handler;

    iget-object v1, p0, Lah;->x:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1395
    iget-object v0, p0, Lah;->d:Lz;

    iget-object v0, v0, Lz;->a:Landroid/os/Handler;

    iget-object v1, p0, Lah;->x:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1397
    :cond_4
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public a(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 507
    new-instance v0, Lak;

    invoke-direct {v0, p0, p1, p2}, Lak;-><init>(Lah;Ljava/lang/String;I)V

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lah;->a(Ljava/lang/Runnable;Z)V

    .line 512
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 635
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "    "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 638
    iget-object v0, p0, Lah;->a:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 639
    iget-object v0, p0, Lah;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 640
    if-lez v4, :cond_1

    .line 641
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Active Fragments in "

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 642
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 643
    const-string v0, ":"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    move v2, v1

    .line 644
    :goto_0
    if-ge v2, v4, :cond_1

    .line 645
    iget-object v0, p0, Lah;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lu;

    .line 646
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "  #"

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(I)V

    .line 647
    const-string v5, ": "

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 648
    if-eqz v0, :cond_0

    .line 649
    invoke-virtual {v0, v3, p2, p3, p4}, Lu;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 644
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 655
    :cond_1
    iget-object v0, p0, Lah;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    .line 656
    iget-object v0, p0, Lah;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 657
    if-lez v4, :cond_2

    .line 658
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Added Fragments:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    move v2, v1

    .line 659
    :goto_1
    if-ge v2, v4, :cond_2

    .line 660
    iget-object v0, p0, Lah;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lu;

    .line 661
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "  #"

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(I)V

    .line 662
    const-string v5, ": "

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {v0}, Lu;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 659
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 667
    :cond_2
    iget-object v0, p0, Lah;->m:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    .line 668
    iget-object v0, p0, Lah;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 669
    if-lez v4, :cond_3

    .line 670
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Fragments Created Menus:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    move v2, v1

    .line 671
    :goto_2
    if-ge v2, v4, :cond_3

    .line 672
    iget-object v0, p0, Lah;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lu;

    .line 673
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "  #"

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(I)V

    .line 674
    const-string v5, ": "

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {v0}, Lu;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 671
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 679
    :cond_3
    iget-object v0, p0, Lah;->l:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    .line 680
    iget-object v0, p0, Lah;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 681
    if-lez v4, :cond_4

    .line 682
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Back Stack:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    move v2, v1

    .line 683
    :goto_3
    if-ge v2, v4, :cond_4

    .line 684
    iget-object v0, p0, Lah;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ll;

    .line 685
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "  #"

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(I)V

    .line 686
    const-string v5, ": "

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {v0}, Ll;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 687
    invoke-virtual {v0, v3, p3}, Ll;->a(Ljava/lang/String;Ljava/io/PrintWriter;)V

    .line 683
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 692
    :cond_4
    monitor-enter p0

    .line 693
    :try_start_0
    iget-object v0, p0, Lah;->n:Ljava/util/ArrayList;

    if-eqz v0, :cond_5

    .line 694
    iget-object v0, p0, Lah;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 695
    if-lez v3, :cond_5

    .line 696
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Back Stack Indices:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    move v2, v1

    .line 697
    :goto_4
    if-ge v2, v3, :cond_5

    .line 698
    iget-object v0, p0, Lah;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ll;

    .line 699
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v4, "  #"

    invoke-virtual {p3, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(I)V

    .line 700
    const-string v4, ": "

    invoke-virtual {p3, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 697
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    .line 705
    :cond_5
    iget-object v0, p0, Lah;->o:Ljava/util/ArrayList;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lah;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_6

    .line 706
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mAvailBackStackIndices: "

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 707
    iget-object v0, p0, Lah;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 709
    :cond_6
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 711
    iget-object v0, p0, Lah;->h:Ljava/util/ArrayList;

    if-eqz v0, :cond_7

    .line 712
    iget-object v0, p0, Lah;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 713
    if-lez v2, :cond_7

    .line 714
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Pending Actions:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 715
    :goto_5
    if-ge v1, v2, :cond_7

    .line 716
    iget-object v0, p0, Lah;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 717
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v3, "  #"

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(I)V

    .line 718
    const-string v3, ": "

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 715
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 709
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 723
    :cond_7
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "FragmentManager misc state:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 724
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "  mActivity="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lah;->d:Lz;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 725
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "  mContainer="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lah;->e:Lad;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 726
    iget-object v0, p0, Lah;->q:Lu;

    if-eqz v0, :cond_8

    .line 727
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "  mParent="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lah;->q:Lu;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 729
    :cond_8
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "  mCurState="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Lah;->c:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(I)V

    .line 730
    const-string v0, " mStateSaved="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lah;->s:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 731
    const-string v0, " mDestroyed="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lah;->t:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 732
    iget-boolean v0, p0, Lah;->r:Z

    if-eqz v0, :cond_9

    .line 733
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "  mNeedMenuInvalidate="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 734
    iget-boolean v0, p0, Lah;->r:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 736
    :cond_9
    iget-object v0, p0, Lah;->f:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 737
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "  mNoTransactionsBecause="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 738
    iget-object v0, p0, Lah;->f:Ljava/lang/String;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 740
    :cond_a
    iget-object v0, p0, Lah;->k:Ljava/util/ArrayList;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lah;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_b

    .line 741
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "  mAvailIndices: "

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 742
    iget-object v0, p0, Lah;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 744
    :cond_b
    return-void
.end method

.method public a(Lu;II)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 1212
    iget v0, p1, Lu;->r:I

    if-lez v0, :cond_4

    move v0, v1

    :goto_0
    if-nez v0, :cond_5

    move v0, v1

    .line 1214
    :goto_1
    iget-boolean v2, p1, Lu;->A:Z

    if-eqz v2, :cond_0

    if-eqz v0, :cond_3

    .line 1215
    :cond_0
    iget-object v2, p0, Lah;->b:Ljava/util/ArrayList;

    if-eqz v2, :cond_1

    .line 1216
    iget-object v2, p0, Lah;->b:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1218
    :cond_1
    iget-boolean v2, p1, Lu;->D:Z

    if-eqz v2, :cond_2

    iget-boolean v2, p1, Lu;->E:Z

    if-eqz v2, :cond_2

    .line 1219
    iput-boolean v1, p0, Lah;->r:Z

    .line 1221
    :cond_2
    iput-boolean v5, p1, Lu;->l:Z

    .line 1222
    iput-boolean v1, p1, Lu;->m:Z

    .line 1223
    if-eqz v0, :cond_6

    move v2, v5

    :goto_2
    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p3

    invoke-virtual/range {v0 .. v5}, Lah;->a(Lu;IIIZ)V

    .line 1226
    :cond_3
    return-void

    :cond_4
    move v0, v5

    .line 1212
    goto :goto_0

    :cond_5
    move v0, v5

    goto :goto_1

    :cond_6
    move v2, v1

    .line 1223
    goto :goto_2
.end method

.method a(Lu;IIIZ)V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v3, 0x0

    const/4 v7, 0x0

    .line 849
    iget-boolean v0, p1, Lu;->l:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p1, Lu;->A:Z

    if-eqz v0, :cond_1

    :cond_0
    if-le p2, v5, :cond_1

    move p2, v5

    .line 852
    :cond_1
    iget-boolean v0, p1, Lu;->m:Z

    if-eqz v0, :cond_2

    iget v0, p1, Lu;->a:I

    if-le p2, v0, :cond_2

    .line 854
    iget p2, p1, Lu;->a:I

    .line 858
    :cond_2
    iget-boolean v0, p1, Lu;->K:Z

    if-eqz v0, :cond_3

    iget v0, p1, Lu;->a:I

    if-ge v0, v9, :cond_3

    if-le p2, v6, :cond_3

    move p2, v6

    .line 861
    :cond_3
    iget v0, p1, Lu;->a:I

    if-ge v0, p2, :cond_1a

    .line 865
    iget-boolean v0, p1, Lu;->o:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p1, Lu;->p:Z

    if-nez v0, :cond_4

    .line 1101
    :goto_0
    return-void

    .line 868
    :cond_4
    iget-object v0, p1, Lu;->b:Landroid/view/View;

    if-eqz v0, :cond_5

    .line 873
    iput-object v7, p1, Lu;->b:Landroid/view/View;

    .line 874
    iget v2, p1, Lu;->c:I

    move-object v0, p0

    move-object v1, p1

    move v4, v3

    invoke-virtual/range {v0 .. v5}, Lah;->a(Lu;IIIZ)V

    .line 876
    :cond_5
    iget v0, p1, Lu;->a:I

    packed-switch v0, :pswitch_data_0

    .line 1100
    :cond_6
    :goto_1
    iput p2, p1, Lu;->a:I

    goto :goto_0

    .line 878
    :pswitch_0
    iget-object v0, p1, Lu;->d:Landroid/os/Bundle;

    if-eqz v0, :cond_8

    .line 880
    iget-object v0, p1, Lu;->d:Landroid/os/Bundle;

    iget-object v1, p0, Lah;->d:Lz;

    invoke-virtual {v1}, Lz;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 881
    iget-object v0, p1, Lu;->d:Landroid/os/Bundle;

    const-string v1, "android:view_state"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;

    move-result-object v0

    iput-object v0, p1, Lu;->e:Landroid/util/SparseArray;

    .line 883
    iget-object v0, p1, Lu;->d:Landroid/os/Bundle;

    const-string v1, "android:target_state"

    invoke-virtual {p0, v0, v1}, Lah;->a(Landroid/os/Bundle;Ljava/lang/String;)Lu;

    move-result-object v0

    iput-object v0, p1, Lu;->i:Lu;

    .line 885
    iget-object v0, p1, Lu;->i:Lu;

    if-eqz v0, :cond_7

    .line 886
    iget-object v0, p1, Lu;->d:Landroid/os/Bundle;

    const-string v1, "android:target_req_state"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p1, Lu;->k:I

    .line 889
    :cond_7
    iget-object v0, p1, Lu;->d:Landroid/os/Bundle;

    const-string v1, "android:user_visible_hint"

    invoke-virtual {v0, v1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p1, Lu;->L:Z

    .line 891
    iget-boolean v0, p1, Lu;->L:Z

    if-nez v0, :cond_8

    .line 892
    iput-boolean v5, p1, Lu;->K:Z

    .line 893
    if-le p2, v6, :cond_8

    move p2, v6

    .line 898
    :cond_8
    iget-object v0, p0, Lah;->d:Lz;

    iput-object v0, p1, Lu;->t:Lz;

    .line 899
    iget-object v0, p0, Lah;->q:Lu;

    iput-object v0, p1, Lu;->v:Lu;

    .line 900
    iget-object v0, p0, Lah;->q:Lu;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lah;->q:Lu;

    iget-object v0, v0, Lu;->u:Lah;

    :goto_2
    iput-object v0, p1, Lu;->s:Lah;

    .line 902
    iput-boolean v3, p1, Lu;->F:Z

    .line 903
    iget-object v0, p0, Lah;->d:Lz;

    invoke-virtual {p1, v0}, Lu;->a(Landroid/app/Activity;)V

    .line 904
    iget-boolean v0, p1, Lu;->F:Z

    if-nez v0, :cond_a

    .line 905
    new-instance v0, Lcz;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onAttach()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcz;-><init>(Ljava/lang/String;)V

    throw v0

    .line 900
    :cond_9
    iget-object v0, p0, Lah;->d:Lz;

    iget-object v0, v0, Lz;->b:Lah;

    goto :goto_2

    .line 908
    :cond_a
    iget-object v0, p1, Lu;->v:Lu;

    if-nez v0, :cond_b

    .line 909
    iget-object v0, p0, Lah;->d:Lz;

    invoke-virtual {v0, p1}, Lz;->a(Lu;)V

    .line 912
    :cond_b
    iget-boolean v0, p1, Lu;->C:Z

    if-nez v0, :cond_c

    .line 913
    iget-object v0, p1, Lu;->d:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Lu;->h(Landroid/os/Bundle;)V

    .line 915
    :cond_c
    iput-boolean v3, p1, Lu;->C:Z

    .line 916
    iget-boolean v0, p1, Lu;->o:Z

    if-eqz v0, :cond_e

    .line 920
    iget-object v0, p1, Lu;->d:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Lu;->a_(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p1, Lu;->d:Landroid/os/Bundle;

    invoke-virtual {p1, v0, v7, v1}, Lu;->b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p1, Lu;->I:Landroid/view/View;

    .line 922
    iget-object v0, p1, Lu;->I:Landroid/view/View;

    if-eqz v0, :cond_15

    .line 923
    iget-object v0, p1, Lu;->I:Landroid/view/View;

    iput-object v0, p1, Lu;->J:Landroid/view/View;

    .line 924
    iget-object v0, p1, Lu;->I:Landroid/view/View;

    invoke-static {v0}, Lbj;->a(Landroid/view/View;)Landroid/view/ViewGroup;

    move-result-object v0

    iput-object v0, p1, Lu;->I:Landroid/view/View;

    .line 925
    iget-boolean v0, p1, Lu;->z:Z

    if-eqz v0, :cond_d

    iget-object v0, p1, Lu;->I:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 926
    :cond_d
    iget-object v0, p1, Lu;->I:Landroid/view/View;

    iget-object v1, p1, Lu;->d:Landroid/os/Bundle;

    invoke-virtual {p1, v0, v1}, Lu;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 932
    :cond_e
    :goto_3
    :pswitch_1
    if-le p2, v5, :cond_18

    .line 933
    iget-boolean v0, p1, Lu;->o:Z

    if-nez v0, :cond_13

    .line 936
    iget v0, p1, Lu;->x:I

    if-eqz v0, :cond_28

    .line 937
    iget-object v0, p0, Lah;->e:Lad;

    iget v1, p1, Lu;->x:I

    invoke-interface {v0, v1}, Lad;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 938
    if-nez v0, :cond_f

    iget-boolean v1, p1, Lu;->q:Z

    if-nez v1, :cond_f

    .line 939
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "No view found for id 0x"

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p1, Lu;->x:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " ("

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lu;->o()Landroid/content/res/Resources;

    move-result-object v4

    iget v8, p1, Lu;->x:I

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ") for fragment "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lah;->a(Ljava/lang/RuntimeException;)V

    .line 946
    :cond_f
    :goto_4
    iput-object v0, p1, Lu;->H:Landroid/view/ViewGroup;

    .line 947
    iget-object v1, p1, Lu;->d:Landroid/os/Bundle;

    invoke-virtual {p1, v1}, Lu;->a_(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v1

    iget-object v2, p1, Lu;->d:Landroid/os/Bundle;

    invoke-virtual {p1, v1, v0, v2}, Lu;->b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p1, Lu;->I:Landroid/view/View;

    .line 949
    iget-object v1, p1, Lu;->I:Landroid/view/View;

    if-eqz v1, :cond_16

    .line 950
    iget-object v1, p1, Lu;->I:Landroid/view/View;

    iput-object v1, p1, Lu;->J:Landroid/view/View;

    .line 951
    iget-object v1, p1, Lu;->I:Landroid/view/View;

    invoke-static {v1}, Lbj;->a(Landroid/view/View;)Landroid/view/ViewGroup;

    move-result-object v1

    iput-object v1, p1, Lu;->I:Landroid/view/View;

    .line 952
    if-eqz v0, :cond_11

    .line 953
    invoke-virtual {p0, p1, p3, v5, p4}, Lah;->a(Lu;IZI)Landroid/view/animation/Animation;

    move-result-object v1

    .line 955
    if-eqz v1, :cond_10

    .line 956
    iget-object v2, p1, Lu;->I:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 958
    :cond_10
    iget-object v1, p1, Lu;->I:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 960
    :cond_11
    iget-boolean v0, p1, Lu;->z:Z

    if-eqz v0, :cond_12

    iget-object v0, p1, Lu;->I:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 961
    :cond_12
    iget-object v0, p1, Lu;->I:Landroid/view/View;

    iget-object v1, p1, Lu;->d:Landroid/os/Bundle;

    invoke-virtual {p1, v0, v1}, Lu;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 967
    :cond_13
    :goto_5
    iget-object v0, p1, Lu;->d:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Lu;->i(Landroid/os/Bundle;)V

    .line 968
    iget-object v0, p1, Lu;->I:Landroid/view/View;

    if-eqz v0, :cond_17

    .line 969
    iget-object v0, p1, Lu;->d:Landroid/os/Bundle;

    iget-object v1, p1, Lu;->e:Landroid/util/SparseArray;

    if-eqz v1, :cond_14

    iget-object v1, p1, Lu;->J:Landroid/view/View;

    iget-object v2, p1, Lu;->e:Landroid/util/SparseArray;

    invoke-virtual {v1, v2}, Landroid/view/View;->restoreHierarchyState(Landroid/util/SparseArray;)V

    iput-object v7, p1, Lu;->e:Landroid/util/SparseArray;

    :cond_14
    iput-boolean v3, p1, Lu;->F:Z

    invoke-virtual {p1, v0}, Lu;->g(Landroid/os/Bundle;)V

    iget-boolean v0, p1, Lu;->F:Z

    if-nez v0, :cond_17

    new-instance v0, Lcz;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onViewStateRestored()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcz;-><init>(Ljava/lang/String;)V

    throw v0

    .line 928
    :cond_15
    iput-object v7, p1, Lu;->J:Landroid/view/View;

    goto/16 :goto_3

    .line 963
    :cond_16
    iput-object v7, p1, Lu;->J:Landroid/view/View;

    goto :goto_5

    .line 971
    :cond_17
    iput-object v7, p1, Lu;->d:Landroid/os/Bundle;

    .line 975
    :cond_18
    :pswitch_2
    if-le p2, v6, :cond_19

    .line 976
    invoke-virtual {p1}, Lu;->M()V

    .line 980
    :cond_19
    :pswitch_3
    if-le p2, v9, :cond_6

    .line 981
    iput-boolean v5, p1, Lu;->n:Z

    .line 983
    invoke-virtual {p1}, Lu;->N()V

    .line 984
    iput-object v7, p1, Lu;->d:Landroid/os/Bundle;

    .line 985
    iput-object v7, p1, Lu;->e:Landroid/util/SparseArray;

    goto/16 :goto_1

    .line 988
    :cond_1a
    iget v0, p1, Lu;->a:I

    if-le v0, p2, :cond_6

    .line 989
    iget v0, p1, Lu;->a:I

    packed-switch v0, :pswitch_data_1

    goto/16 :goto_1

    .line 1052
    :cond_1b
    :goto_6
    :pswitch_4
    if-gtz p2, :cond_6

    .line 1053
    iget-boolean v0, p0, Lah;->t:Z

    if-eqz v0, :cond_1c

    .line 1054
    iget-object v0, p1, Lu;->b:Landroid/view/View;

    if-eqz v0, :cond_1c

    .line 1061
    iget-object v0, p1, Lu;->b:Landroid/view/View;

    .line 1062
    iput-object v7, p1, Lu;->b:Landroid/view/View;

    .line 1063
    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 1066
    :cond_1c
    iget-object v0, p1, Lu;->b:Landroid/view/View;

    if-eqz v0, :cond_23

    .line 1071
    iput p2, p1, Lu;->c:I

    move p2, v5

    .line 1072
    goto/16 :goto_1

    .line 991
    :pswitch_5
    const/4 v0, 0x5

    if-ge p2, v0, :cond_1d

    .line 992
    invoke-virtual {p1}, Lu;->P()V

    .line 994
    iput-boolean v3, p1, Lu;->n:Z

    .line 997
    :cond_1d
    :pswitch_6
    if-ge p2, v9, :cond_1e

    .line 998
    invoke-virtual {p1}, Lu;->Q()V

    .line 1002
    :cond_1e
    :pswitch_7
    if-ge p2, v6, :cond_1f

    .line 1003
    invoke-virtual {p1}, Lu;->R()V

    .line 1007
    :cond_1f
    :pswitch_8
    const/4 v0, 0x2

    if-ge p2, v0, :cond_1b

    .line 1008
    iget-object v0, p1, Lu;->I:Landroid/view/View;

    if-eqz v0, :cond_20

    .line 1012
    iget-object v0, p0, Lah;->d:Lz;

    invoke-virtual {v0}, Lz;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_20

    iget-object v0, p1, Lu;->e:Landroid/util/SparseArray;

    if-nez v0, :cond_20

    .line 1013
    invoke-virtual {p0, p1}, Lah;->f(Lu;)V

    .line 1016
    :cond_20
    invoke-virtual {p1}, Lu;->S()V

    .line 1017
    iget-object v0, p1, Lu;->I:Landroid/view/View;

    if-eqz v0, :cond_22

    iget-object v0, p1, Lu;->H:Landroid/view/ViewGroup;

    if-eqz v0, :cond_22

    .line 1019
    iget v0, p0, Lah;->c:I

    if-lez v0, :cond_27

    iget-boolean v0, p0, Lah;->t:Z

    if-nez v0, :cond_27

    .line 1020
    invoke-virtual {p0, p1, p3, v3, p4}, Lah;->a(Lu;IZI)Landroid/view/animation/Animation;

    move-result-object v0

    .line 1023
    :goto_7
    if-eqz v0, :cond_21

    .line 1025
    iget-object v1, p1, Lu;->I:Landroid/view/View;

    iput-object v1, p1, Lu;->b:Landroid/view/View;

    .line 1026
    iput p2, p1, Lu;->c:I

    .line 1027
    new-instance v1, Lam;

    invoke-direct {v1, p0, p1}, Lam;-><init>(Lah;Lu;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1043
    iget-object v1, p1, Lu;->I:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1045
    :cond_21
    iget-object v0, p1, Lu;->H:Landroid/view/ViewGroup;

    iget-object v1, p1, Lu;->I:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1047
    :cond_22
    iput-object v7, p1, Lu;->H:Landroid/view/ViewGroup;

    .line 1048
    iput-object v7, p1, Lu;->I:Landroid/view/View;

    .line 1049
    iput-object v7, p1, Lu;->J:Landroid/view/View;

    goto :goto_6

    .line 1074
    :cond_23
    iget-boolean v0, p1, Lu;->C:Z

    if-nez v0, :cond_24

    .line 1076
    invoke-virtual {p1}, Lu;->T()V

    .line 1079
    :cond_24
    iput-boolean v3, p1, Lu;->F:Z

    .line 1080
    invoke-virtual {p1}, Lu;->f()V

    .line 1081
    iget-boolean v0, p1, Lu;->F:Z

    if-nez v0, :cond_25

    .line 1082
    new-instance v0, Lcz;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onDetach()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcz;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1085
    :cond_25
    if-nez p5, :cond_6

    .line 1086
    iget-boolean v0, p1, Lu;->C:Z

    if-nez v0, :cond_26

    .line 1087
    invoke-virtual {p0, p1}, Lah;->e(Lu;)V

    goto/16 :goto_1

    .line 1089
    :cond_26
    iput-object v7, p1, Lu;->t:Lz;

    .line 1090
    iput-object v7, p1, Lu;->v:Lu;

    .line 1091
    iput-object v7, p1, Lu;->s:Lah;

    .line 1092
    iput-object v7, p1, Lu;->u:Lah;

    goto/16 :goto_1

    :cond_27
    move-object v0, v7

    goto :goto_7

    :cond_28
    move-object v0, v7

    goto/16 :goto_4

    .line 876
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 989
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
    .end packed-switch
.end method

.method public a(Lu;Z)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 1190
    iget-object v0, p0, Lah;->b:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 1191
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lah;->b:Ljava/util/ArrayList;

    .line 1193
    :cond_0
    invoke-virtual {p0, p1}, Lah;->d(Lu;)V

    .line 1195
    iget-boolean v0, p1, Lu;->A:Z

    if-nez v0, :cond_3

    .line 1196
    iget-object v0, p0, Lah;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1197
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment already added: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1199
    :cond_1
    iget-object v0, p0, Lah;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1200
    iput-boolean v1, p1, Lu;->l:Z

    .line 1201
    const/4 v0, 0x0

    iput-boolean v0, p1, Lu;->m:Z

    .line 1202
    iget-boolean v0, p1, Lu;->D:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p1, Lu;->E:Z

    if-eqz v0, :cond_2

    .line 1203
    iput-boolean v1, p0, Lah;->r:Z

    .line 1205
    :cond_2
    if-eqz p2, :cond_3

    .line 1206
    invoke-virtual {p0, p1}, Lah;->c(Lu;)V

    .line 1209
    :cond_3
    return-void
.end method

.method public a(Lz;Lad;Lu;)V
    .locals 2

    .prologue
    .line 1900
    iget-object v0, p0, Lah;->d:Lz;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already attached"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1901
    :cond_0
    iput-object p1, p0, Lah;->d:Lz;

    .line 1902
    iput-object p2, p0, Lah;->e:Lad;

    .line 1903
    iput-object p3, p0, Lah;->q:Lu;

    .line 1904
    return-void
.end method

.method public a(Landroid/view/Menu;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2015
    .line 2016
    iget-object v1, p0, Lah;->b:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    move v1, v0

    move v2, v0

    .line 2017
    :goto_0
    iget-object v0, p0, Lah;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 2018
    iget-object v0, p0, Lah;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lu;

    .line 2019
    if-eqz v0, :cond_0

    .line 2020
    invoke-virtual {v0, p1}, Lu;->b(Landroid/view/Menu;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2021
    const/4 v2, 0x1

    .line 2017
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    move v2, v0

    .line 2026
    :cond_2
    return v2
.end method

.method public a(Landroid/view/Menu;Landroid/view/MenuInflater;)Z
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 1983
    .line 1984
    const/4 v1, 0x0

    .line 1985
    iget-object v0, p0, Lah;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    move v3, v4

    move v2, v4

    .line 1986
    :goto_0
    iget-object v0, p0, Lah;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_3

    .line 1987
    iget-object v0, p0, Lah;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lu;

    .line 1988
    if-eqz v0, :cond_1

    .line 1989
    invoke-virtual {v0, p1, p2}, Lu;->b(Landroid/view/Menu;Landroid/view/MenuInflater;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1990
    const/4 v2, 0x1

    .line 1991
    if-nez v1, :cond_0

    .line 1992
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1994
    :cond_0
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    move v0, v2

    .line 1986
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v0

    goto :goto_0

    :cond_2
    move v2, v4

    .line 2000
    :cond_3
    iget-object v0, p0, Lah;->m:Ljava/util/ArrayList;

    if-eqz v0, :cond_6

    .line 2001
    :goto_1
    iget-object v0, p0, Lah;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v4, v0, :cond_6

    .line 2002
    iget-object v0, p0, Lah;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lu;

    .line 2003
    if-eqz v1, :cond_4

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 2004
    :cond_4
    invoke-virtual {v0}, Lu;->C()V

    .line 2001
    :cond_5
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 2009
    :cond_6
    iput-object v1, p0, Lah;->m:Ljava/util/ArrayList;

    .line 2011
    return v2
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2030
    iget-object v0, p0, Lah;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    move v1, v2

    .line 2031
    :goto_0
    iget-object v0, p0, Lah;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2032
    iget-object v0, p0, Lah;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lu;

    .line 2033
    if-eqz v0, :cond_1

    .line 2034
    invoke-virtual {v0, p1}, Lu;->c(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2035
    const/4 v2, 0x1

    .line 2040
    :cond_0
    return v2

    .line 2031
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method a(Ljava/lang/String;II)Z
    .locals 10

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1529
    iget-object v0, p0, Lah;->l:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 1598
    :cond_0
    :goto_0
    return v3

    .line 1532
    :cond_1
    if-nez p1, :cond_3

    if-gez p2, :cond_3

    and-int/lit8 v0, p3, 0x1

    if-nez v0, :cond_3

    .line 1533
    iget-object v0, p0, Lah;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 1534
    if-ltz v0, :cond_0

    .line 1537
    iget-object v1, p0, Lah;->l:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ll;

    .line 1538
    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    .line 1539
    new-instance v3, Landroid/util/SparseArray;

    invoke-direct {v3}, Landroid/util/SparseArray;-><init>()V

    .line 1540
    invoke-virtual {v0, v1, v3}, Ll;->a(Landroid/util/SparseArray;Landroid/util/SparseArray;)V

    .line 1541
    invoke-virtual {v0, v2, v4, v1, v3}, Ll;->a(ZLq;Landroid/util/SparseArray;Landroid/util/SparseArray;)Lq;

    .line 1596
    :cond_2
    invoke-virtual {p0}, Lah;->i()V

    move v3, v2

    .line 1598
    goto :goto_0

    .line 1544
    :cond_3
    const/4 v0, -0x1

    .line 1545
    if-nez p1, :cond_4

    if-ltz p2, :cond_b

    .line 1548
    :cond_4
    iget-object v0, p0, Lah;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v1, v0, -0x1

    .line 1549
    :goto_1
    if-ltz v1, :cond_7

    .line 1550
    iget-object v0, p0, Lah;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ll;

    .line 1551
    if-eqz p1, :cond_5

    invoke-virtual {v0}, Ll;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_7

    .line 1552
    :cond_5
    if-ltz p2, :cond_6

    iget v0, v0, Ll;->g:I

    if-eq p2, v0, :cond_7

    .line 1555
    :cond_6
    add-int/lit8 v1, v1, -0x1

    .line 1558
    goto :goto_1

    .line 1559
    :cond_7
    if-ltz v1, :cond_0

    .line 1562
    and-int/lit8 v0, p3, 0x1

    if-eqz v0, :cond_a

    .line 1563
    add-int/lit8 v1, v1, -0x1

    .line 1565
    :goto_2
    if-ltz v1, :cond_a

    .line 1566
    iget-object v0, p0, Lah;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ll;

    .line 1567
    if-eqz p1, :cond_8

    invoke-virtual {v0}, Ll;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_9

    :cond_8
    if-ltz p2, :cond_a

    iget v0, v0, Ll;->g:I

    if-ne p2, v0, :cond_a

    .line 1569
    :cond_9
    add-int/lit8 v1, v1, -0x1

    .line 1570
    goto :goto_2

    :cond_a
    move v0, v1

    .line 1576
    :cond_b
    iget-object v1, p0, Lah;->l:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1579
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1581
    iget-object v1, p0, Lah;->l:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    :goto_3
    if-le v1, v0, :cond_c

    .line 1582
    iget-object v5, p0, Lah;->l:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1581
    add-int/lit8 v1, v1, -0x1

    goto :goto_3

    .line 1584
    :cond_c
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v7, v0, -0x1

    .line 1585
    new-instance v8, Landroid/util/SparseArray;

    invoke-direct {v8}, Landroid/util/SparseArray;-><init>()V

    .line 1586
    new-instance v9, Landroid/util/SparseArray;

    invoke-direct {v9}, Landroid/util/SparseArray;-><init>()V

    move v1, v3

    .line 1587
    :goto_4
    if-gt v1, v7, :cond_d

    .line 1588
    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ll;

    invoke-virtual {v0, v8, v9}, Ll;->a(Landroid/util/SparseArray;Landroid/util/SparseArray;)V

    .line 1587
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    :cond_d
    move-object v5, v4

    move v4, v3

    .line 1591
    :goto_5
    if-gt v4, v7, :cond_2

    .line 1592
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ll;

    if-ne v4, v7, :cond_e

    move v1, v2

    :goto_6
    invoke-virtual {v0, v1, v5, v8, v9}, Ll;->a(ZLq;Landroid/util/SparseArray;Landroid/util/SparseArray;)Lq;

    move-result-object v1

    .line 1591
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    move-object v5, v1

    goto :goto_5

    :cond_e
    move v1, v3

    .line 1592
    goto :goto_6
.end method

.method public b(I)Laf;
    .locals 1

    .prologue
    .line 550
    iget-object v0, p0, Lah;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laf;

    return-object v0
.end method

.method public b(Lag;)V
    .locals 1

    .prologue
    .line 563
    iget-object v0, p0, Lah;->p:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 564
    iget-object v0, p0, Lah;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 566
    :cond_0
    return-void
.end method

.method public b(Landroid/view/Menu;)V
    .locals 2

    .prologue
    .line 2058
    iget-object v0, p0, Lah;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 2059
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lah;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2060
    iget-object v0, p0, Lah;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lu;

    .line 2061
    if-eqz v0, :cond_0

    .line 2062
    invoke-virtual {v0, p1}, Lu;->c(Landroid/view/Menu;)V

    .line 2059
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2066
    :cond_1
    return-void
.end method

.method b(Ll;)V
    .locals 1

    .prologue
    .line 1521
    iget-object v0, p0, Lah;->l:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 1522
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lah;->l:Ljava/util/ArrayList;

    .line 1524
    :cond_0
    iget-object v0, p0, Lah;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1525
    invoke-virtual {p0}, Lah;->i()V

    .line 1526
    return-void
.end method

.method public b(Lu;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 835
    iget-boolean v0, p1, Lu;->K:Z

    if-eqz v0, :cond_0

    .line 836
    iget-boolean v0, p0, Lah;->j:Z

    if-eqz v0, :cond_1

    .line 838
    const/4 v0, 0x1

    iput-boolean v0, p0, Lah;->u:Z

    .line 844
    :cond_0
    :goto_0
    return-void

    .line 841
    :cond_1
    iput-boolean v3, p1, Lu;->K:Z

    .line 842
    iget v2, p0, Lah;->c:I

    move-object v0, p0

    move-object v1, p1

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lah;->a(Lu;IIIZ)V

    goto :goto_0
.end method

.method public b(Lu;II)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1229
    iget-boolean v0, p1, Lu;->z:Z

    if-nez v0, :cond_3

    .line 1231
    iput-boolean v2, p1, Lu;->z:Z

    .line 1232
    iget-object v0, p1, Lu;->I:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 1233
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, p3}, Lah;->a(Lu;IZI)Landroid/view/animation/Animation;

    move-result-object v0

    .line 1235
    if-eqz v0, :cond_0

    .line 1236
    iget-object v1, p1, Lu;->I:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1238
    :cond_0
    iget-object v0, p1, Lu;->I:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1240
    :cond_1
    iget-boolean v0, p1, Lu;->l:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p1, Lu;->D:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p1, Lu;->E:Z

    if-eqz v0, :cond_2

    .line 1241
    iput-boolean v2, p0, Lah;->r:Z

    .line 1243
    :cond_2
    invoke-virtual {p1}, Lu;->v()V

    .line 1245
    :cond_3
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 486
    invoke-virtual {p0}, Lah;->h()Z

    move-result v0

    return v0
.end method

.method public b(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2044
    iget-object v0, p0, Lah;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    move v1, v2

    .line 2045
    :goto_0
    iget-object v0, p0, Lah;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2046
    iget-object v0, p0, Lah;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lu;

    .line 2047
    if-eqz v0, :cond_1

    .line 2048
    invoke-virtual {v0, p1}, Lu;->d(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2049
    const/4 v2, 0x1

    .line 2054
    :cond_0
    return v2

    .line 2045
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 491
    new-instance v0, Laj;

    invoke-direct {v0, p0}, Laj;-><init>(Lah;)V

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lah;->a(Ljava/lang/Runnable;Z)V

    .line 496
    return-void
.end method

.method public c(I)V
    .locals 2

    .prologue
    .line 1446
    monitor-enter p0

    .line 1447
    :try_start_0
    iget-object v0, p0, Lah;->n:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1448
    iget-object v0, p0, Lah;->o:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 1449
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lah;->o:Ljava/util/ArrayList;

    .line 1451
    :cond_0
    iget-object v0, p0, Lah;->o:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1453
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method c(Lu;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 1104
    iget v2, p0, Lah;->c:I

    move-object v0, p0

    move-object v1, p1

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lah;->a(Lu;IIIZ)V

    .line 1105
    return-void
.end method

.method public c(Lu;II)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1248
    iget-boolean v0, p1, Lu;->z:Z

    if-eqz v0, :cond_3

    .line 1250
    iput-boolean v2, p1, Lu;->z:Z

    .line 1251
    iget-object v0, p1, Lu;->I:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 1252
    invoke-virtual {p0, p1, p2, v3, p3}, Lah;->a(Lu;IZI)Landroid/view/animation/Animation;

    move-result-object v0

    .line 1254
    if-eqz v0, :cond_0

    .line 1255
    iget-object v1, p1, Lu;->I:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1257
    :cond_0
    iget-object v0, p1, Lu;->I:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1259
    :cond_1
    iget-boolean v0, p1, Lu;->l:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p1, Lu;->D:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p1, Lu;->E:Z

    if-eqz v0, :cond_2

    .line 1260
    iput-boolean v3, p0, Lah;->r:Z

    .line 1262
    :cond_2
    invoke-virtual {p1}, Lu;->v()V

    .line 1264
    :cond_3
    return-void
.end method

.method d(Lu;)V
    .locals 2

    .prologue
    .line 1156
    iget v0, p1, Lu;->f:I

    if-ltz v0, :cond_0

    .line 1171
    :goto_0
    return-void

    .line 1160
    :cond_0
    iget-object v0, p0, Lah;->k:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lah;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_3

    .line 1161
    :cond_1
    iget-object v0, p0, Lah;->a:Ljava/util/ArrayList;

    if-nez v0, :cond_2

    .line 1162
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lah;->a:Ljava/util/ArrayList;

    .line 1164
    :cond_2
    iget-object v0, p0, Lah;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v1, p0, Lah;->q:Lu;

    invoke-virtual {p1, v0, v1}, Lu;->a(ILu;)V

    .line 1165
    iget-object v0, p0, Lah;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1168
    :cond_3
    iget-object v0, p0, Lah;->k:Ljava/util/ArrayList;

    iget-object v1, p0, Lah;->k:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p0, Lah;->q:Lu;

    invoke-virtual {p1, v0, v1}, Lu;->a(ILu;)V

    .line 1169
    iget-object v0, p0, Lah;->a:Ljava/util/ArrayList;

    iget v1, p1, Lu;->f:I

    invoke-virtual {v0, v1, p1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public d(Lu;II)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x1

    .line 1267
    iget-boolean v0, p1, Lu;->A:Z

    if-nez v0, :cond_2

    .line 1269
    iput-boolean v2, p1, Lu;->A:Z

    .line 1270
    iget-boolean v0, p1, Lu;->l:Z

    if-eqz v0, :cond_2

    .line 1272
    iget-object v0, p0, Lah;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 1273
    iget-object v0, p0, Lah;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1276
    :cond_0
    iget-boolean v0, p1, Lu;->D:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p1, Lu;->E:Z

    if-eqz v0, :cond_1

    .line 1277
    iput-boolean v2, p0, Lah;->r:Z

    .line 1279
    :cond_1
    iput-boolean v5, p1, Lu;->l:Z

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p3

    .line 1280
    invoke-virtual/range {v0 .. v5}, Lah;->a(Lu;IIIZ)V

    .line 1283
    :cond_2
    return-void
.end method

.method public d()Z
    .locals 3

    .prologue
    .line 500
    invoke-direct {p0}, Lah;->w()V

    .line 501
    invoke-virtual {p0}, Lah;->b()Z

    .line 502
    iget-object v0, p0, Lah;->d:Lz;

    iget-object v0, v0, Lz;->a:Landroid/os/Handler;

    const/4 v0, 0x0

    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lah;->a(Ljava/lang/String;II)Z

    move-result v0

    return v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 545
    iget-object v0, p0, Lah;->l:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lah;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method e(Lu;)V
    .locals 3

    .prologue
    .line 1175
    iget v0, p1, Lu;->f:I

    if-gez v0, :cond_0

    .line 1187
    :goto_0
    return-void

    .line 1179
    :cond_0
    iget-object v0, p0, Lah;->a:Ljava/util/ArrayList;

    iget v1, p1, Lu;->f:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1181
    iget-object v0, p0, Lah;->k:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 1182
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lah;->k:Ljava/util/ArrayList;

    .line 1184
    :cond_1
    iget-object v0, p0, Lah;->k:Ljava/util/ArrayList;

    iget v1, p1, Lu;->f:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1185
    iget-object v0, p0, Lah;->d:Lz;

    iget-object v1, p1, Lu;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lz;->a(Ljava/lang/String;)V

    .line 1186
    invoke-virtual {p1}, Lu;->B()V

    goto :goto_0
.end method

.method public e(Lu;II)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 1286
    iget-boolean v0, p1, Lu;->A:Z

    if-eqz v0, :cond_3

    .line 1288
    iput-boolean v5, p1, Lu;->A:Z

    .line 1289
    iget-boolean v0, p1, Lu;->l:Z

    if-nez v0, :cond_3

    .line 1290
    iget-object v0, p0, Lah;->b:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 1291
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lah;->b:Ljava/util/ArrayList;

    .line 1293
    :cond_0
    iget-object v0, p0, Lah;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1294
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment already added: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1296
    :cond_1
    iget-object v0, p0, Lah;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1298
    iput-boolean v1, p1, Lu;->l:Z

    .line 1299
    iget-boolean v0, p1, Lu;->D:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p1, Lu;->E:Z

    if-eqz v0, :cond_2

    .line 1300
    iput-boolean v1, p0, Lah;->r:Z

    .line 1302
    :cond_2
    iget v2, p0, Lah;->c:I

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p3

    invoke-virtual/range {v0 .. v5}, Lah;->a(Lu;IIIZ)V

    .line 1305
    :cond_3
    return-void
.end method

.method public f()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lu;",
            ">;"
        }
    .end annotation

    .prologue
    .line 597
    iget-object v0, p0, Lah;->a:Ljava/util/ArrayList;

    return-object v0
.end method

.method f(Lu;)V
    .locals 2

    .prologue
    .line 1621
    iget-object v0, p1, Lu;->J:Landroid/view/View;

    if-nez v0, :cond_1

    .line 1634
    :cond_0
    :goto_0
    return-void

    .line 1624
    :cond_1
    iget-object v0, p0, Lah;->w:Landroid/util/SparseArray;

    if-nez v0, :cond_2

    .line 1625
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lah;->w:Landroid/util/SparseArray;

    .line 1629
    :goto_1
    iget-object v0, p1, Lu;->J:Landroid/view/View;

    iget-object v1, p0, Lah;->w:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/view/View;->saveHierarchyState(Landroid/util/SparseArray;)V

    .line 1630
    iget-object v0, p0, Lah;->w:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 1631
    iget-object v0, p0, Lah;->w:Landroid/util/SparseArray;

    iput-object v0, p1, Lu;->e:Landroid/util/SparseArray;

    .line 1632
    const/4 v0, 0x0

    iput-object v0, p0, Lah;->w:Landroid/util/SparseArray;

    goto :goto_0

    .line 1627
    :cond_2
    iget-object v0, p0, Lah;->w:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    goto :goto_1
.end method

.method g(Lu;)Landroid/os/Bundle;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1637
    .line 1639
    iget-object v0, p0, Lah;->v:Landroid/os/Bundle;

    if-nez v0, :cond_0

    .line 1640
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lah;->v:Landroid/os/Bundle;

    .line 1642
    :cond_0
    iget-object v0, p0, Lah;->v:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Lu;->j(Landroid/os/Bundle;)V

    .line 1643
    iget-object v0, p0, Lah;->v:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 1644
    iget-object v0, p0, Lah;->v:Landroid/os/Bundle;

    .line 1645
    iput-object v1, p0, Lah;->v:Landroid/os/Bundle;

    .line 1648
    :goto_0
    iget-object v1, p1, Lu;->I:Landroid/view/View;

    if-eqz v1, :cond_1

    .line 1649
    invoke-virtual {p0, p1}, Lah;->f(Lu;)V

    .line 1651
    :cond_1
    iget-object v1, p1, Lu;->e:Landroid/util/SparseArray;

    if-eqz v1, :cond_3

    .line 1652
    if-nez v0, :cond_2

    .line 1653
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1655
    :cond_2
    const-string v1, "android:view_state"

    iget-object v2, p1, Lu;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSparseParcelableArray(Ljava/lang/String;Landroid/util/SparseArray;)V

    .line 1658
    :cond_3
    iget-boolean v1, p1, Lu;->L:Z

    if-nez v1, :cond_5

    .line 1659
    if-nez v0, :cond_4

    .line 1660
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1663
    :cond_4
    const-string v1, "android:user_visible_hint"

    iget-boolean v2, p1, Lu;->L:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1666
    :cond_5
    return-object v0

    :cond_6
    move-object v0, v1

    goto :goto_0
.end method

.method g()V
    .locals 2

    .prologue
    .line 1145
    iget-object v0, p0, Lah;->a:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 1153
    :cond_0
    return-void

    .line 1147
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lah;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1148
    iget-object v0, p0, Lah;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lu;

    .line 1149
    if-eqz v0, :cond_2

    .line 1150
    invoke-virtual {p0, v0}, Lah;->b(Lu;)V

    .line 1147
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public h()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 1460
    iget-boolean v1, p0, Lah;->j:Z

    if-eqz v1, :cond_0

    .line 1461
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Recursive entry to executePendingTransactions"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1464
    :cond_0
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    iget-object v3, p0, Lah;->d:Lz;

    iget-object v3, v3, Lz;->a:Landroid/os/Handler;

    invoke-virtual {v3}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v3

    if-eq v1, v3, :cond_1

    .line 1465
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must be called from main thread of process"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move v1, v2

    .line 1473
    :goto_0
    monitor-enter p0

    .line 1474
    :try_start_0
    iget-object v3, p0, Lah;->h:Ljava/util/ArrayList;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lah;->h:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_4

    .line 1475
    :cond_2
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1496
    iget-boolean v0, p0, Lah;->u:Z

    if-eqz v0, :cond_9

    move v3, v2

    move v4, v2

    .line 1498
    :goto_1
    iget-object v0, p0, Lah;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_8

    .line 1499
    iget-object v0, p0, Lah;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lu;

    .line 1500
    if-eqz v0, :cond_3

    iget-object v5, v0, Lu;->M:Lbd;

    if-eqz v5, :cond_3

    .line 1501
    iget-object v0, v0, Lu;->M:Lbd;

    invoke-virtual {v0}, Lbd;->a()Z

    move-result v0

    or-int/2addr v4, v0

    .line 1498
    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 1478
    :cond_4
    :try_start_1
    iget-object v1, p0, Lah;->h:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 1479
    iget-object v1, p0, Lah;->i:[Ljava/lang/Runnable;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lah;->i:[Ljava/lang/Runnable;

    array-length v1, v1

    if-ge v1, v3, :cond_6

    .line 1480
    :cond_5
    new-array v1, v3, [Ljava/lang/Runnable;

    iput-object v1, p0, Lah;->i:[Ljava/lang/Runnable;

    .line 1482
    :cond_6
    iget-object v1, p0, Lah;->h:Ljava/util/ArrayList;

    iget-object v4, p0, Lah;->i:[Ljava/lang/Runnable;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 1483
    iget-object v1, p0, Lah;->h:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 1484
    iget-object v1, p0, Lah;->d:Lz;

    iget-object v1, v1, Lz;->a:Landroid/os/Handler;

    iget-object v4, p0, Lah;->x:Ljava/lang/Runnable;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1485
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1487
    iput-boolean v0, p0, Lah;->j:Z

    move v1, v2

    .line 1488
    :goto_2
    if-ge v1, v3, :cond_7

    .line 1489
    iget-object v4, p0, Lah;->i:[Ljava/lang/Runnable;

    aget-object v4, v4, v1

    invoke-interface {v4}, Ljava/lang/Runnable;->run()V

    .line 1490
    iget-object v4, p0, Lah;->i:[Ljava/lang/Runnable;

    const/4 v5, 0x0

    aput-object v5, v4, v1

    .line 1488
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1485
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1492
    :cond_7
    iput-boolean v2, p0, Lah;->j:Z

    move v1, v0

    .line 1494
    goto :goto_0

    .line 1504
    :cond_8
    if-nez v4, :cond_9

    .line 1505
    iput-boolean v2, p0, Lah;->u:Z

    .line 1506
    invoke-virtual {p0}, Lah;->g()V

    .line 1509
    :cond_9
    return v1
.end method

.method i()V
    .locals 2

    .prologue
    .line 1513
    iget-object v0, p0, Lah;->p:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 1514
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lah;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1515
    iget-object v0, p0, Lah;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lag;

    invoke-interface {v0}, Lag;->ah_()V

    .line 1514
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1518
    :cond_0
    return-void
.end method

.method j()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lu;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1602
    const/4 v1, 0x0

    .line 1603
    iget-object v0, p0, Lah;->a:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    .line 1604
    const/4 v0, 0x0

    move v3, v0

    :goto_0
    iget-object v0, p0, Lah;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_3

    .line 1605
    iget-object v0, p0, Lah;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lu;

    .line 1606
    if-eqz v0, :cond_1

    iget-boolean v2, v0, Lu;->B:Z

    if-eqz v2, :cond_1

    .line 1607
    if-nez v1, :cond_0

    .line 1608
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1610
    :cond_0
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1611
    const/4 v2, 0x1

    iput-boolean v2, v0, Lu;->C:Z

    .line 1612
    iget-object v2, v0, Lu;->i:Lu;

    if-eqz v2, :cond_2

    iget-object v2, v0, Lu;->i:Lu;

    iget v2, v2, Lu;->f:I

    :goto_1
    iput v2, v0, Lu;->j:I

    .line 1613
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 1612
    :cond_2
    const/4 v2, -0x1

    goto :goto_1

    .line 1617
    :cond_3
    return-object v1
.end method

.method k()Landroid/os/Parcelable;
    .locals 11

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 1672
    invoke-virtual {p0}, Lah;->h()Z

    .line 1674
    sget-boolean v0, Lah;->g:Z

    if-eqz v0, :cond_0

    .line 1684
    iput-boolean v1, p0, Lah;->s:Z

    .line 1687
    :cond_0
    iget-object v0, p0, Lah;->a:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lah;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_2

    .line 1782
    :cond_1
    :goto_0
    return-object v3

    .line 1692
    :cond_2
    iget-object v0, p0, Lah;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 1693
    new-array v7, v6, [Lar;

    move v5, v4

    move v2, v4

    .line 1695
    :goto_1
    if-ge v5, v6, :cond_8

    .line 1696
    iget-object v0, p0, Lah;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lu;

    .line 1697
    if-eqz v0, :cond_d

    .line 1698
    iget v2, v0, Lu;->f:I

    if-gez v2, :cond_3

    .line 1699
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Failure saving state: active "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " has cleared index: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, v0, Lu;->f:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v2, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v2}, Lah;->a(Ljava/lang/RuntimeException;)V

    .line 1706
    :cond_3
    new-instance v2, Lar;

    invoke-direct {v2, v0}, Lar;-><init>(Lu;)V

    .line 1707
    aput-object v2, v7, v5

    .line 1709
    iget v8, v0, Lu;->a:I

    if-lez v8, :cond_6

    iget-object v8, v2, Lar;->a:Landroid/os/Bundle;

    if-nez v8, :cond_6

    .line 1710
    invoke-virtual {p0, v0}, Lah;->g(Lu;)Landroid/os/Bundle;

    move-result-object v8

    iput-object v8, v2, Lar;->a:Landroid/os/Bundle;

    .line 1712
    iget-object v8, v0, Lu;->i:Lu;

    if-eqz v8, :cond_7

    .line 1713
    iget-object v8, v0, Lu;->i:Lu;

    iget v8, v8, Lu;->f:I

    if-gez v8, :cond_4

    .line 1714
    new-instance v8, Ljava/lang/IllegalStateException;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Failure saving state: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " has target not in fragment manager: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, v0, Lu;->i:Lu;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v8}, Lah;->a(Ljava/lang/RuntimeException;)V

    .line 1718
    :cond_4
    iget-object v8, v2, Lar;->a:Landroid/os/Bundle;

    if-nez v8, :cond_5

    .line 1719
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    iput-object v8, v2, Lar;->a:Landroid/os/Bundle;

    .line 1721
    :cond_5
    iget-object v8, v2, Lar;->a:Landroid/os/Bundle;

    const-string v9, "android:target_state"

    iget-object v10, v0, Lu;->i:Lu;

    invoke-virtual {p0, v8, v9, v10}, Lah;->a(Landroid/os/Bundle;Ljava/lang/String;Lu;)V

    .line 1723
    iget v8, v0, Lu;->k:I

    if-eqz v8, :cond_7

    .line 1724
    iget-object v2, v2, Lar;->a:Landroid/os/Bundle;

    const-string v8, "android:target_req_state"

    iget v0, v0, Lu;->k:I

    invoke-virtual {v2, v8, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    move v0, v1

    .line 1734
    :goto_2
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    move v2, v0

    goto/16 :goto_1

    .line 1731
    :cond_6
    iget-object v0, v0, Lu;->d:Landroid/os/Bundle;

    iput-object v0, v2, Lar;->a:Landroid/os/Bundle;

    :cond_7
    move v0, v1

    goto :goto_2

    .line 1739
    :cond_8
    if-eqz v2, :cond_1

    .line 1748
    iget-object v0, p0, Lah;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_a

    .line 1749
    iget-object v0, p0, Lah;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 1750
    if-lez v5, :cond_a

    .line 1751
    new-array v1, v5, [I

    move v2, v4

    .line 1752
    :goto_3
    if-ge v2, v5, :cond_b

    .line 1753
    iget-object v0, p0, Lah;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lu;

    iget v0, v0, Lu;->f:I

    aput v0, v1, v2

    .line 1754
    aget v0, v1, v2

    if-gez v0, :cond_9

    .line 1755
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v8, "Failure saving state: active "

    invoke-direct {v6, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lah;->b:Ljava/util/ArrayList;

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " has cleared index: "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget v8, v1, v2

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v0, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lah;->a(Ljava/lang/RuntimeException;)V

    .line 1759
    :cond_9
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    :cond_a
    move-object v1, v3

    .line 1766
    :cond_b
    iget-object v0, p0, Lah;->l:Ljava/util/ArrayList;

    if-eqz v0, :cond_c

    .line 1767
    iget-object v0, p0, Lah;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 1768
    if-lez v5, :cond_c

    .line 1769
    new-array v3, v5, [Lr;

    move v2, v4

    .line 1770
    :goto_4
    if-ge v2, v5, :cond_c

    .line 1771
    new-instance v4, Lr;

    iget-object v0, p0, Lah;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ll;

    invoke-direct {v4, v0}, Lr;-><init>(Ll;)V

    aput-object v4, v3, v2

    .line 1772
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    .line 1778
    :cond_c
    new-instance v0, Lao;

    invoke-direct {v0}, Lao;-><init>()V

    .line 1779
    iput-object v7, v0, Lao;->a:[Lar;

    .line 1780
    iput-object v1, v0, Lao;->b:[I

    .line 1781
    iput-object v3, v0, Lao;->c:[Lr;

    move-object v3, v0

    .line 1782
    goto/16 :goto_0

    :cond_d
    move v0, v2

    goto/16 :goto_2
.end method

.method public l()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1911
    iput-boolean v1, p0, Lah;->s:Z

    .line 1912
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v1}, Lah;->a(IZ)V

    .line 1913
    return-void
.end method

.method public m()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1916
    iput-boolean v1, p0, Lah;->s:Z

    .line 1917
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v1}, Lah;->a(IZ)V

    .line 1918
    return-void
.end method

.method public n()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1921
    iput-boolean v1, p0, Lah;->s:Z

    .line 1922
    const/4 v0, 0x4

    invoke-virtual {p0, v0, v1}, Lah;->a(IZ)V

    .line 1923
    return-void
.end method

.method public noteStateNotSaved()V
    .locals 1

    .prologue
    .line 1907
    const/4 v0, 0x0

    iput-boolean v0, p0, Lah;->s:Z

    .line 1908
    return-void
.end method

.method public o()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1926
    iput-boolean v1, p0, Lah;->s:Z

    .line 1927
    const/4 v0, 0x5

    invoke-virtual {p0, v0, v1}, Lah;->a(IZ)V

    .line 1928
    return-void
.end method

.method public onCreateView(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v5, -0x1

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2110
    const-string v0, "fragment"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v1

    .line 2198
    :goto_0
    return-object v0

    .line 2114
    :cond_0
    const-string v0, "class"

    invoke-interface {p3, v1, v0}, Landroid/util/AttributeSet;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2115
    sget-object v4, Lan;->a:[I

    invoke-virtual {p2, p3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v4

    .line 2116
    if-nez v0, :cond_d

    .line 2117
    invoke-virtual {v4, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v6, v0

    .line 2119
    :goto_1
    invoke-virtual {v4, v2, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v7

    .line 2120
    const/4 v0, 0x2

    invoke-virtual {v4, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 2121
    invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V

    .line 2123
    iget-object v0, p0, Lah;->d:Lz;

    invoke-static {v0, v6}, Lu;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    move-object v0, v1

    .line 2126
    goto :goto_0

    .line 2129
    :cond_1
    if-eq v7, v5, :cond_2

    invoke-virtual {p0, v7}, Lah;->a(I)Lu;

    move-result-object v1

    .line 2140
    :cond_2
    if-nez v1, :cond_3

    if-eqz v8, :cond_3

    .line 2141
    invoke-virtual {p0, v8}, Lah;->a(Ljava/lang/String;)Lu;

    move-result-object v1

    .line 2143
    :cond_3
    if-nez v1, :cond_4

    .line 2144
    invoke-virtual {p0, v3}, Lah;->a(I)Lu;

    move-result-object v1

    .line 2147
    :cond_4
    if-nez v1, :cond_7

    .line 2151
    invoke-static {p2, v6}, Lu;->a(Landroid/content/Context;Ljava/lang/String;)Lu;

    move-result-object v1

    .line 2152
    iput-boolean v2, v1, Lu;->o:Z

    .line 2153
    if-eqz v7, :cond_6

    move v0, v7

    :goto_2
    iput v0, v1, Lu;->w:I

    .line 2154
    iput v3, v1, Lu;->x:I

    .line 2155
    iput-object v8, v1, Lu;->y:Ljava/lang/String;

    .line 2156
    iput-boolean v2, v1, Lu;->p:Z

    .line 2157
    iput-object p0, v1, Lu;->s:Lah;

    .line 2158
    iget-object v0, p0, Lah;->d:Lz;

    iget-object v4, v1, Lu;->d:Landroid/os/Bundle;

    invoke-virtual {v1, v0, p3, v4}, Lu;->a(Landroid/app/Activity;Landroid/util/AttributeSet;Landroid/os/Bundle;)V

    .line 2159
    invoke-virtual {p0, v1, v2}, Lah;->a(Lu;Z)V

    .line 2182
    :cond_5
    :goto_3
    iget v0, p0, Lah;->c:I

    if-gtz v0, :cond_9

    iget-boolean v0, v1, Lu;->o:Z

    if-eqz v0, :cond_9

    move-object v0, p0

    move v4, v3

    move v5, v3

    .line 2183
    invoke-virtual/range {v0 .. v5}, Lah;->a(Lu;IIIZ)V

    .line 2188
    :goto_4
    iget-object v0, v1, Lu;->I:Landroid/view/View;

    if-nez v0, :cond_a

    .line 2189
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not create a view."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    move v0, v3

    .line 2153
    goto :goto_2

    .line 2161
    :cond_7
    iget-boolean v0, v1, Lu;->p:Z

    if-eqz v0, :cond_8

    .line 2164
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p3}, Landroid/util/AttributeSet;->getPositionDescription()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": Duplicate id 0x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v7}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", tag "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", or parent id 0x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with another fragment for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2171
    :cond_8
    iput-boolean v2, v1, Lu;->p:Z

    .line 2175
    iget-boolean v0, v1, Lu;->C:Z

    if-nez v0, :cond_5

    .line 2176
    iget-object v0, p0, Lah;->d:Lz;

    iget-object v4, v1, Lu;->d:Landroid/os/Bundle;

    invoke-virtual {v1, v0, p3, v4}, Lu;->a(Landroid/app/Activity;Landroid/util/AttributeSet;Landroid/os/Bundle;)V

    goto/16 :goto_3

    .line 2185
    :cond_9
    invoke-virtual {p0, v1}, Lah;->c(Lu;)V

    goto :goto_4

    .line 2192
    :cond_a
    if-eqz v7, :cond_b

    .line 2193
    iget-object v0, v1, Lu;->I:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setId(I)V

    .line 2195
    :cond_b
    iget-object v0, v1, Lu;->I:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_c

    .line 2196
    iget-object v0, v1, Lu;->I:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 2198
    :cond_c
    iget-object v0, v1, Lu;->I:Landroid/view/View;

    goto/16 :goto_0

    :cond_d
    move-object v6, v0

    goto/16 :goto_1
.end method

.method public p()V
    .locals 2

    .prologue
    .line 1931
    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lah;->a(IZ)V

    .line 1932
    return-void
.end method

.method public q()V
    .locals 2

    .prologue
    .line 1938
    const/4 v0, 0x1

    iput-boolean v0, p0, Lah;->s:Z

    .line 1940
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lah;->a(IZ)V

    .line 1941
    return-void
.end method

.method public r()V
    .locals 2

    .prologue
    .line 1944
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lah;->a(IZ)V

    .line 1945
    return-void
.end method

.method public s()V
    .locals 2

    .prologue
    .line 1948
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lah;->a(IZ)V

    .line 1949
    return-void
.end method

.method public t()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1952
    const/4 v0, 0x1

    iput-boolean v0, p0, Lah;->t:Z

    .line 1953
    invoke-virtual {p0}, Lah;->h()Z

    .line 1954
    invoke-virtual {p0, v2, v2}, Lah;->a(IZ)V

    .line 1955
    iput-object v1, p0, Lah;->d:Lz;

    .line 1956
    iput-object v1, p0, Lah;->e:Lad;

    .line 1957
    iput-object v1, p0, Lah;->q:Lu;

    .line 1958
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 620
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 621
    const-string v1, "FragmentManager{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 622
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 623
    const-string v1, " in "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 624
    iget-object v1, p0, Lah;->q:Lu;

    if-eqz v1, :cond_0

    .line 625
    iget-object v1, p0, Lah;->q:Lu;

    invoke-static {v1, v0}, Lgi;->a(Ljava/lang/Object;Ljava/lang/StringBuilder;)V

    .line 629
    :goto_0
    const-string v1, "}}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 630
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 627
    :cond_0
    iget-object v1, p0, Lah;->d:Lz;

    invoke-static {v1, v0}, Lgi;->a(Ljava/lang/Object;Ljava/lang/StringBuilder;)V

    goto :goto_0
.end method

.method public u()V
    .locals 2

    .prologue
    .line 1972
    iget-object v0, p0, Lah;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 1973
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lah;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1974
    iget-object v0, p0, Lah;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lu;

    .line 1975
    if-eqz v0, :cond_0

    .line 1976
    invoke-virtual {v0}, Lu;->O()V

    .line 1973
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1980
    :cond_1
    return-void
.end method

.method v()Landroid/view/LayoutInflater$Factory;
    .locals 0

    .prologue
    .line 2202
    return-object p0
.end method
