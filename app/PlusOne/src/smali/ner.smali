.class public final Lner;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:I

.field private c:Ljava/lang/Long;

.field private d:[Lnem;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2727
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2732
    const/high16 v0, -0x80000000

    iput v0, p0, Lner;->b:I

    .line 2737
    sget-object v0, Lnem;->a:[Lnem;

    iput-object v0, p0, Lner;->d:[Lnem;

    .line 2727
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2764
    .line 2765
    iget-object v0, p0, Lner;->a:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 2766
    const/4 v0, 0x1

    iget-object v2, p0, Lner;->a:Ljava/lang/String;

    .line 2767
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2769
    :goto_0
    iget v2, p0, Lner;->b:I

    const/high16 v3, -0x80000000

    if-eq v2, v3, :cond_0

    .line 2770
    const/4 v2, 0x3

    iget v3, p0, Lner;->b:I

    .line 2771
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 2773
    :cond_0
    iget-object v2, p0, Lner;->c:Ljava/lang/Long;

    if-eqz v2, :cond_1

    .line 2774
    const/4 v2, 0x4

    iget-object v3, p0, Lner;->c:Ljava/lang/Long;

    .line 2775
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Loxo;->f(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 2777
    :cond_1
    iget-object v2, p0, Lner;->d:[Lnem;

    if-eqz v2, :cond_3

    .line 2778
    iget-object v2, p0, Lner;->d:[Lnem;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 2779
    if-eqz v4, :cond_2

    .line 2780
    const/4 v5, 0x5

    .line 2781
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 2778
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2785
    :cond_3
    iget-object v1, p0, Lner;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2786
    iput v0, p0, Lner;->ai:I

    .line 2787
    return v0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lner;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2795
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2796
    sparse-switch v0, :sswitch_data_0

    .line 2800
    iget-object v2, p0, Lner;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 2801
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lner;->ah:Ljava/util/List;

    .line 2804
    :cond_1
    iget-object v2, p0, Lner;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2806
    :sswitch_0
    return-object p0

    .line 2811
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lner;->a:Ljava/lang/String;

    goto :goto_0

    .line 2815
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 2816
    if-eqz v0, :cond_2

    const/4 v2, 0x1

    if-eq v0, v2, :cond_2

    const/4 v2, 0x2

    if-eq v0, v2, :cond_2

    const/4 v2, 0x3

    if-eq v0, v2, :cond_2

    const/4 v2, 0x4

    if-eq v0, v2, :cond_2

    const/4 v2, 0x5

    if-ne v0, v2, :cond_3

    .line 2822
    :cond_2
    iput v0, p0, Lner;->b:I

    goto :goto_0

    .line 2824
    :cond_3
    iput v1, p0, Lner;->b:I

    goto :goto_0

    .line 2829
    :sswitch_3
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lner;->c:Ljava/lang/Long;

    goto :goto_0

    .line 2833
    :sswitch_4
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 2834
    iget-object v0, p0, Lner;->d:[Lnem;

    if-nez v0, :cond_5

    move v0, v1

    .line 2835
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnem;

    .line 2836
    iget-object v3, p0, Lner;->d:[Lnem;

    if-eqz v3, :cond_4

    .line 2837
    iget-object v3, p0, Lner;->d:[Lnem;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2839
    :cond_4
    iput-object v2, p0, Lner;->d:[Lnem;

    .line 2840
    :goto_2
    iget-object v2, p0, Lner;->d:[Lnem;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 2841
    iget-object v2, p0, Lner;->d:[Lnem;

    new-instance v3, Lnem;

    invoke-direct {v3}, Lnem;-><init>()V

    aput-object v3, v2, v0

    .line 2842
    iget-object v2, p0, Lner;->d:[Lnem;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 2843
    invoke-virtual {p1}, Loxn;->a()I

    .line 2840
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2834
    :cond_5
    iget-object v0, p0, Lner;->d:[Lnem;

    array-length v0, v0

    goto :goto_1

    .line 2846
    :cond_6
    iget-object v2, p0, Lner;->d:[Lnem;

    new-instance v3, Lnem;

    invoke-direct {v3}, Lnem;-><init>()V

    aput-object v3, v2, v0

    .line 2847
    iget-object v2, p0, Lner;->d:[Lnem;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 2796
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x18 -> :sswitch_2
        0x20 -> :sswitch_3
        0x2a -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 2742
    iget-object v0, p0, Lner;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2743
    const/4 v0, 0x1

    iget-object v1, p0, Lner;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 2745
    :cond_0
    iget v0, p0, Lner;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 2746
    const/4 v0, 0x3

    iget v1, p0, Lner;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2748
    :cond_1
    iget-object v0, p0, Lner;->c:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 2749
    const/4 v0, 0x4

    iget-object v1, p0, Lner;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 2751
    :cond_2
    iget-object v0, p0, Lner;->d:[Lnem;

    if-eqz v0, :cond_4

    .line 2752
    iget-object v1, p0, Lner;->d:[Lnem;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 2753
    if-eqz v3, :cond_3

    .line 2754
    const/4 v4, 0x5

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 2752
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2758
    :cond_4
    iget-object v0, p0, Lner;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2760
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2723
    invoke-virtual {p0, p1}, Lner;->a(Loxn;)Lner;

    move-result-object v0

    return-object v0
.end method
