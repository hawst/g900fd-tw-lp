.class public final Loxr;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:I

.field public b:Z

.field public c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TT;>;"
        }
    .end annotation
.end field

.field public d:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(ILoxs;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Loxs",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput p1, p0, Loxr;->a:I

    .line 52
    iget-object v0, p2, Loxs;->a:Ljava/lang/reflect/Type;

    instance-of v0, v0, Ljava/lang/reflect/ParameterizedType;

    iput-boolean v0, p0, Loxr;->b:Z

    .line 53
    iget-object v0, p2, Loxs;->a:Ljava/lang/reflect/Type;

    instance-of v0, v0, Ljava/lang/reflect/ParameterizedType;

    if-eqz v0, :cond_0

    iget-object v0, p2, Loxs;->a:Ljava/lang/reflect/Type;

    check-cast v0, Ljava/lang/reflect/ParameterizedType;

    invoke-interface {v0}, Ljava/lang/reflect/ParameterizedType;->getActualTypeArguments()[Ljava/lang/reflect/Type;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    check-cast v0, Ljava/lang/Class;

    :goto_0
    iput-object v0, p0, Loxr;->c:Ljava/lang/Class;

    .line 54
    iget-boolean v0, p0, Loxr;->b:Z

    if-eqz v0, :cond_1

    iget-object v0, p2, Loxs;->a:Ljava/lang/reflect/Type;

    check-cast v0, Ljava/lang/reflect/ParameterizedType;

    invoke-interface {v0}, Ljava/lang/reflect/ParameterizedType;->getRawType()Ljava/lang/reflect/Type;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    :goto_1
    iput-object v0, p0, Loxr;->d:Ljava/lang/Class;

    .line 56
    return-void

    .line 53
    :cond_0
    iget-object v0, p2, Loxs;->a:Ljava/lang/reflect/Type;

    check-cast v0, Ljava/lang/Class;

    goto :goto_0

    .line 54
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(ILoxs;)Loxr;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(I",
            "Loxs",
            "<TT;>;)",
            "Loxr",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 65
    new-instance v0, Loxr;

    invoke-direct {v0, p0, p1}, Loxr;-><init>(ILoxs;)V

    return-object v0
.end method
