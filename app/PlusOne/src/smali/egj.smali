.class final Legj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhkd;


# instance fields
.field private synthetic a:Legi;


# direct methods
.method constructor <init>(Legi;)V
    .locals 0

    .prologue
    .line 188
    iput-object p1, p0, Legj;->a:Legi;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILandroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 191
    if-ne p1, v2, :cond_1

    if-eqz p2, :cond_1

    .line 192
    const-string v0, "photo_picker_mode"

    iget-object v1, p0, Legj;->a:Legi;

    iget-object v1, v1, Legi;->Y:Lctq;

    .line 193
    invoke-virtual {v1}, Lctq;->c()I

    move-result v1

    .line 192
    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 197
    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    .line 198
    iget-object v1, p0, Legj;->a:Legi;

    iget-object v1, v1, Legi;->R:Lcto;

    invoke-virtual {v1, v0}, Lcto;->a(I)V

    .line 200
    const-string v0, "shareables"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Legj;->a:Legi;

    iget-boolean v0, v0, Legi;->ac:Z

    if-eqz v0, :cond_2

    const-string v0, "photo_picker_selected"

    .line 201
    invoke-virtual {p2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 203
    :cond_0
    iget-object v0, p0, Legj;->a:Legi;

    invoke-virtual {v0}, Legi;->n()Lz;

    move-result-object v0

    .line 204
    iget-object v1, p0, Legj;->a:Legi;

    invoke-virtual {v1}, Legi;->n()Lz;

    move-result-object v1

    invoke-virtual {v1, v2, p2}, Lz;->setResult(ILandroid/content/Intent;)V

    .line 205
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 220
    :cond_1
    :goto_0
    return-void

    .line 210
    :cond_2
    const-string v0, "photo_picker_selected"

    .line 211
    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Ljcn;

    .line 212
    if-eqz v0, :cond_3

    .line 213
    iget-object v1, p0, Legj;->a:Legi;

    iget-object v1, v1, Legi;->Q:Lctu;

    invoke-virtual {v1, v0}, Lctu;->a(Ljcn;)V

    .line 216
    :cond_3
    iget-object v0, p0, Legj;->a:Legi;

    iget-object v1, p0, Legj;->a:Legi;

    invoke-virtual {v1}, Legi;->x()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Legi;->e(Landroid/view/View;)V

    .line 217
    iget-object v0, p0, Legj;->a:Legi;

    invoke-static {v0}, Legi;->a(Legi;)V

    goto :goto_0
.end method
