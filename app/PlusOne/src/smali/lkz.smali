.class public final Llkz;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:I

.field private final b:I

.field private final c:Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;

.field private final d:Llko;

.field private final e:Llko;

.field private f:I

.field private g:F

.field private h:F

.field private i:F

.field private j:F

.field private k:F

.field private l:Z


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;Lllc;Lllc;)V
    .locals 2

    .prologue
    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    const/4 v0, 0x1

    iput v0, p0, Llkz;->f:I

    .line 100
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Llkz;->k:F

    .line 107
    iput-object p1, p0, Llkz;->c:Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;

    .line 108
    new-instance v0, Llko;

    invoke-direct {v0, p2}, Llko;-><init>(Lllc;)V

    iput-object v0, p0, Llkz;->d:Llko;

    .line 109
    new-instance v0, Llko;

    invoke-direct {v0, p3}, Llko;-><init>(Lllc;)V

    iput-object v0, p0, Llkz;->e:Llko;

    .line 111
    invoke-virtual {p1}, Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 112
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, Llkz;->a:I

    .line 113
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledPagingTouchSlop()I

    move-result v0

    iput v0, p0, Llkz;->b:I

    .line 114
    return-void
.end method

.method private static a(Landroid/view/View;II)Z
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 331
    instance-of v0, p0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    move-object v0, p0

    .line 332
    check-cast v0, Landroid/view/ViewGroup;

    .line 333
    invoke-virtual {p0}, Landroid/view/View;->getScrollX()I

    move-result v4

    .line 334
    invoke-virtual {p0}, Landroid/view/View;->getScrollY()I

    move-result v5

    .line 335
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    .line 337
    add-int/lit8 v3, v3, -0x1

    :goto_0
    if-ltz v3, :cond_1

    .line 339
    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 340
    add-int v7, p1, v4

    invoke-virtual {v6}, Landroid/view/View;->getLeft()I

    move-result v8

    if-lt v7, v8, :cond_0

    add-int v7, p1, v4

    invoke-virtual {v6}, Landroid/view/View;->getRight()I

    move-result v8

    if-ge v7, v8, :cond_0

    add-int v7, p2, v5

    .line 341
    invoke-virtual {v6}, Landroid/view/View;->getTop()I

    move-result v8

    if-lt v7, v8, :cond_0

    add-int v7, p2, v5

    invoke-virtual {v6}, Landroid/view/View;->getBottom()I

    move-result v8

    if-ge v7, v8, :cond_0

    add-int v7, p1, v4

    .line 342
    invoke-virtual {v6}, Landroid/view/View;->getLeft()I

    move-result v8

    sub-int/2addr v7, v8

    add-int v8, p2, v5

    invoke-virtual {v6}, Landroid/view/View;->getTop()I

    move-result v9

    sub-int/2addr v8, v9

    invoke-static {v6, v7, v8}, Llkz;->a(Landroid/view/View;II)Z

    move-result v6

    if-eqz v6, :cond_0

    move v0, v1

    .line 352
    :goto_1
    return v0

    .line 337
    :cond_0
    add-int/lit8 v3, v3, -0x1

    goto :goto_0

    .line 348
    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-ge v0, v3, :cond_6

    .line 349
    instance-of v0, p0, Landroid/widget/AbsListView;

    if-eqz v0, :cond_5

    check-cast p0, Landroid/widget/AbsListView;

    invoke-virtual {p0}, Landroid/widget/AbsListView;->getChildCount()I

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Landroid/widget/AbsListView;->getFirstVisiblePosition()I

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0, v2}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    invoke-virtual {p0}, Landroid/widget/AbsListView;->getListPaddingTop()I

    move-result v3

    if-ne v0, v3, :cond_3

    invoke-virtual {p0}, Landroid/widget/AbsListView;->getScrollY()I

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    move v0, v1

    :goto_2
    if-nez v0, :cond_4

    move v0, v1

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2

    :cond_4
    move v0, v2

    goto :goto_1

    :cond_5
    move v0, v2

    goto :goto_1

    .line 352
    :cond_6
    const/4 v0, -0x1

    invoke-static {p0, v0}, Liu;->b(Landroid/view/View;I)Z

    move-result v0

    goto :goto_1
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Llkz;->e:Llko;

    invoke-virtual {v0}, Llko;->b()V

    .line 229
    return-void
.end method

.method public a(Landroid/view/MotionEvent;)Z
    .locals 11

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/high16 v10, -0x40800000    # -1.0f

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 132
    iget-object v0, p0, Llkz;->c:Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;->h()Llkt;

    move-result-object v0

    .line 134
    sget-object v5, Llkt;->a:Llkt;

    if-ne v0, v5, :cond_1

    .line 206
    :cond_0
    :goto_0
    return v2

    .line 139
    :cond_1
    iget-object v5, p0, Llkz;->c:Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;

    invoke-virtual {v5}, Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;->i()I

    move-result v5

    iget-object v6, p0, Llkz;->c:Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;

    invoke-virtual {v6}, Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;->getScrollY()I

    move-result v6

    sub-int v6, v5, v6

    .line 140
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    .line 141
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    .line 144
    iget-object v8, p0, Llkz;->c:Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;

    invoke-virtual {v8}, Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;->d()Z

    move-result v8

    if-eqz v8, :cond_2

    sget-object v8, Llkt;->c:Llkt;

    if-ne v0, v8, :cond_2

    int-to-float v0, v6

    cmpg-float v0, v7, v0

    if-gez v0, :cond_2

    .line 147
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-ne v0, v1, :cond_2

    .line 148
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget v8, p0, Llkz;->g:F

    sub-float/2addr v0, v8

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v8, p0, Llkz;->a:I

    int-to-float v8, v8

    cmpg-float v0, v0, v8

    if-gez v0, :cond_2

    .line 149
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iget v8, p0, Llkz;->h:F

    sub-float/2addr v0, v8

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v8, p0, Llkz;->a:I

    int-to-float v8, v8

    cmpg-float v0, v0, v8

    if-gez v0, :cond_2

    .line 150
    iget-object v0, p0, Llkz;->c:Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;

    sget-object v8, Llkt;->b:Llkt;

    invoke-virtual {v0, v8}, Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;->a(Llkt;)V

    .line 151
    iget-object v0, p0, Llkz;->c:Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;

    iget-object v0, v0, Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_1

    .line 156
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_3

    .line 157
    iput v5, p0, Llkz;->g:F

    .line 158
    iput v7, p0, Llkz;->h:F

    .line 159
    int-to-float v0, v6

    sub-float v0, v7, v0

    iput v0, p0, Llkz;->i:F

    .line 160
    iput v10, p0, Llkz;->k:F

    .line 161
    iput-boolean v2, p0, Llkz;->l:Z

    .line 164
    :cond_3
    iget-object v0, p0, Llkz;->c:Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;->i()I

    move-result v0

    iget-object v5, p0, Llkz;->c:Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;

    invoke-virtual {v5}, Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;->getScrollY()I

    move-result v5

    sub-int/2addr v0, v5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v8

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v9

    if-nez v5, :cond_9

    int-to-float v0, v0

    cmpl-float v0, v9, v0

    if-gez v0, :cond_4

    iget-object v0, p0, Llkz;->c:Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;->d()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Llkz;->c:Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;->h()Llkt;

    move-result-object v0

    sget-object v5, Llkt;->c:Llkt;

    if-ne v0, v5, :cond_8

    :cond_4
    move v0, v3

    :goto_2
    iput v0, p0, Llkz;->f:I

    .line 166
    iget v0, p0, Llkz;->f:I

    if-eq v0, v1, :cond_0

    .line 170
    iget-object v0, p0, Llkz;->e:Llko;

    invoke-virtual {v0}, Llko;->c()Z

    move-result v5

    .line 171
    iget-object v0, p0, Llkz;->c:Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;->a()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_10

    move v0, v2

    .line 172
    :goto_3
    if-eqz v0, :cond_16

    .line 173
    iget-object v0, p0, Llkz;->d:Llko;

    invoke-virtual {v0}, Llko;->a()V

    .line 175
    iget v0, p0, Llkz;->f:I

    if-ne v0, v3, :cond_15

    iget v0, p0, Llkz;->k:F

    cmpl-float v0, v0, v10

    if-eqz v0, :cond_15

    .line 179
    const/4 v0, 0x0

    iget v3, p0, Llkz;->k:F

    sub-float/2addr v3, v7

    invoke-virtual {p1, v0, v3}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 186
    :goto_4
    if-eqz v5, :cond_6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iget v3, p0, Llkz;->k:F

    sub-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v3, p0, Llkz;->a:I

    int-to-float v3, v3

    cmpl-float v0, v0, v3

    if-lez v0, :cond_5

    move v2, v1

    :cond_5
    if-eqz v2, :cond_6

    .line 187
    iput-boolean v1, p0, Llkz;->l:Z

    .line 191
    :cond_6
    iget v0, p0, Llkz;->k:F

    cmpl-float v0, v0, v10

    if-nez v0, :cond_7

    .line 192
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Llkz;->k:F

    .line 195
    :cond_7
    iget-object v0, p0, Llkz;->e:Llko;

    invoke-virtual {v0, p1}, Llko;->a(Landroid/view/MotionEvent;)Z

    .line 204
    :goto_5
    iput v7, p0, Llkz;->j:F

    move v2, v1

    .line 206
    goto/16 :goto_0

    :cond_8
    move v0, v1

    .line 164
    goto :goto_2

    :cond_9
    iget v0, p0, Llkz;->f:I

    if-ne v0, v3, :cond_f

    iget v0, p0, Llkz;->g:F

    sub-float v0, v8, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v8

    iget v0, p0, Llkz;->h:F

    sub-float v0, v9, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v9

    iget v0, p0, Llkz;->b:I

    int-to-float v0, v0

    cmpl-float v0, v8, v0

    if-lez v0, :cond_a

    move v0, v1

    :goto_6
    iget v5, p0, Llkz;->a:I

    int-to-float v5, v5

    cmpl-float v5, v9, v5

    if-lez v5, :cond_b

    move v5, v1

    :goto_7
    if-eqz v5, :cond_d

    if-eqz v0, :cond_d

    cmpl-float v0, v8, v9

    if-lez v0, :cond_c

    const/4 v0, 0x3

    goto/16 :goto_2

    :cond_a
    move v0, v2

    goto :goto_6

    :cond_b
    move v5, v2

    goto :goto_7

    :cond_c
    move v0, v4

    goto/16 :goto_2

    :cond_d
    if-eqz v5, :cond_e

    move v0, v4

    goto/16 :goto_2

    :cond_e
    if-eqz v0, :cond_f

    const/4 v0, 0x3

    goto/16 :goto_2

    :cond_f
    iget v0, p0, Llkz;->f:I

    goto/16 :goto_2

    .line 171
    :cond_10
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v8

    packed-switch v0, :pswitch_data_0

    :cond_11
    move v0, v5

    goto/16 :goto_3

    :pswitch_0
    move v0, v1

    goto/16 :goto_3

    :pswitch_1
    iget v0, p0, Llkz;->f:I

    if-ne v0, v4, :cond_11

    iget-object v0, p0, Llkz;->c:Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;->j()I

    move-result v0

    iget-object v4, p0, Llkz;->c:Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;

    sget-object v9, Llkt;->d:Llkt;

    invoke-virtual {v4, v9}, Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;->b(Llkt;)I

    move-result v4

    if-ge v0, v4, :cond_12

    move v0, v2

    goto/16 :goto_3

    :cond_12
    iget v0, p0, Llkz;->j:F

    cmpg-float v0, v8, v0

    if-gez v0, :cond_13

    move v0, v1

    goto/16 :goto_3

    :cond_13
    iget-object v0, p0, Llkz;->c:Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;->a()Landroid/view/View;

    move-result-object v0

    iget v4, p0, Llkz;->g:F

    float-to-int v4, v4

    iget v8, p0, Llkz;->i:F

    float-to-int v8, v8

    invoke-static {v0, v4, v8}, Llkz;->a(Landroid/view/View;II)Z

    move-result v0

    goto/16 :goto_3

    :pswitch_2
    iget v0, p0, Llkz;->f:I

    if-ne v0, v3, :cond_14

    move v0, v1

    goto/16 :goto_3

    :cond_14
    iget v0, p0, Llkz;->f:I

    if-ne v0, v4, :cond_11

    iget-boolean v0, p0, Llkz;->l:Z

    if-nez v0, :cond_11

    move v0, v2

    goto/16 :goto_3

    .line 182
    :cond_15
    const/4 v0, 0x0

    neg-int v3, v6

    int-to-float v3, v3

    invoke-virtual {p1, v0, v3}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    goto/16 :goto_4

    .line 197
    :cond_16
    iget-object v0, p0, Llkz;->e:Llko;

    invoke-virtual {v0}, Llko;->a()V

    .line 198
    iput v10, p0, Llkz;->k:F

    .line 199
    iput-boolean v2, p0, Llkz;->l:Z

    .line 201
    iget-object v0, p0, Llkz;->d:Llko;

    invoke-virtual {v0, p1}, Llko;->a(Landroid/view/MotionEvent;)Z

    goto/16 :goto_5

    .line 171
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
