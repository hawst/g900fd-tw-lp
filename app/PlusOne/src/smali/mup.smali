.class public final Lmup;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmup;


# instance fields
.field private b:Ljava/lang/Integer;

.field private c:Ljava/lang/Integer;

.field private d:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14399
    const/4 v0, 0x0

    new-array v0, v0, [Lmup;

    sput-object v0, Lmup;->a:[Lmup;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14400
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 14426
    const/4 v0, 0x0

    .line 14427
    iget-object v1, p0, Lmup;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 14428
    const/4 v0, 0x1

    iget-object v1, p0, Lmup;->b:Ljava/lang/Integer;

    .line 14429
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 14431
    :cond_0
    iget-object v1, p0, Lmup;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 14432
    const/4 v1, 0x2

    iget-object v2, p0, Lmup;->c:Ljava/lang/Integer;

    .line 14433
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 14435
    :cond_1
    iget-object v1, p0, Lmup;->d:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 14436
    const/4 v1, 0x3

    iget-object v2, p0, Lmup;->d:Ljava/lang/Long;

    .line 14437
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 14439
    :cond_2
    iget-object v1, p0, Lmup;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 14440
    iput v0, p0, Lmup;->ai:I

    .line 14441
    return v0
.end method

.method public a(Loxn;)Lmup;
    .locals 2

    .prologue
    .line 14449
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 14450
    sparse-switch v0, :sswitch_data_0

    .line 14454
    iget-object v1, p0, Lmup;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 14455
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmup;->ah:Ljava/util/List;

    .line 14458
    :cond_1
    iget-object v1, p0, Lmup;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 14460
    :sswitch_0
    return-object p0

    .line 14465
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmup;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 14469
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmup;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 14473
    :sswitch_3
    invoke-virtual {p1}, Loxn;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lmup;->d:Ljava/lang/Long;

    goto :goto_0

    .line 14450
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 14411
    iget-object v0, p0, Lmup;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 14412
    const/4 v0, 0x1

    iget-object v1, p0, Lmup;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 14414
    :cond_0
    iget-object v0, p0, Lmup;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 14415
    const/4 v0, 0x2

    iget-object v1, p0, Lmup;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 14417
    :cond_1
    iget-object v0, p0, Lmup;->d:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 14418
    const/4 v0, 0x3

    iget-object v1, p0, Lmup;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(IJ)V

    .line 14420
    :cond_2
    iget-object v0, p0, Lmup;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 14422
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 14396
    invoke-virtual {p0, p1}, Lmup;->a(Loxn;)Lmup;

    move-result-object v0

    return-object v0
.end method
