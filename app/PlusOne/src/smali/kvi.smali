.class public final Lkvi;
.super Lhye;
.source "PG"


# instance fields
.field private final b:Ldp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">.dp;"
        }
    .end annotation
.end field

.field private final c:I

.field private final d:Ljava/lang/String;

.field private final e:I

.field private final f:[Ljava/lang/String;

.field private g:Z

.field private h:Ljava/lang/String;

.field private i:I


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;I[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lhye;-><init>(Landroid/content/Context;)V

    .line 41
    new-instance v0, Ldp;

    invoke-direct {v0, p0}, Ldp;-><init>(Ldo;)V

    iput-object v0, p0, Lkvi;->b:Ldp;

    .line 42
    iput p2, p0, Lkvi;->c:I

    .line 43
    iput-object p3, p0, Lkvi;->d:Ljava/lang/String;

    .line 44
    iput p4, p0, Lkvi;->e:I

    .line 45
    iput-object p5, p0, Lkvi;->f:[Ljava/lang/String;

    .line 46
    return-void
.end method


# virtual methods
.method public C()Landroid/database/Cursor;
    .locals 14

    .prologue
    const/4 v13, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 50
    invoke-virtual {p0}, Lkvi;->n()Landroid/content/Context;

    move-result-object v1

    .line 51
    const-class v0, Lktq;

    invoke-static {v1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Lktq;

    .line 53
    iput-object v5, p0, Lkvi;->h:Ljava/lang/String;

    .line 55
    iget v0, p0, Lkvi;->c:I

    iget-object v2, p0, Lkvi;->d:Ljava/lang/String;

    .line 56
    invoke-interface {v12, v0, v2}, Lktq;->b(ILjava/lang/String;)J

    move-result-wide v2

    .line 57
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long/2addr v6, v2

    const-wide/32 v8, 0x36ee80

    cmp-long v0, v6, v8

    if-lez v0, :cond_1

    move v0, v13

    :goto_0
    iput-boolean v0, p0, Lkvi;->g:Z

    .line 60
    const-wide/16 v6, 0x0

    cmp-long v0, v2, v6

    if-gtz v0, :cond_2

    .line 62
    new-instance v0, Lkvb;

    iget v2, p0, Lkvi;->c:I

    iget-object v3, p0, Lkvi;->d:Ljava/lang/String;

    const/16 v6, 0x1f4

    invoke-direct/range {v0 .. v6}, Lkvb;-><init>(Landroid/content/Context;ILjava/lang/String;ILjava/lang/String;I)V

    .line 65
    invoke-static {v0}, Lhoc;->a(Lhny;)Lhoz;

    move-result-object v0

    .line 66
    invoke-virtual {v0}, Lhoz;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 86
    :cond_0
    :goto_1
    return-object v5

    :cond_1
    move v0, v4

    .line 57
    goto :goto_0

    .line 71
    :cond_2
    iget v7, p0, Lkvi;->c:I

    iget-object v8, p0, Lkvi;->d:Ljava/lang/String;

    iget v9, p0, Lkvi;->e:I

    iget-object v10, p0, Lkvi;->f:[Ljava/lang/String;

    move-object v6, v12

    move-object v11, v5

    invoke-interface/range {v6 .. v11}, Lktq;->a(ILjava/lang/String;I[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    .line 73
    if-eqz v5, :cond_3

    .line 74
    iget-object v0, p0, Lkvi;->b:Ldp;

    invoke-interface {v5, v0}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    .line 77
    :cond_3
    iget v0, p0, Lkvi;->c:I

    iget-object v1, p0, Lkvi;->d:Ljava/lang/String;

    iget v2, p0, Lkvi;->e:I

    invoke-interface {v12, v0, v1, v2}, Lktq;->b(ILjava/lang/String;I)Landroid/database/Cursor;

    move-result-object v0

    .line 79
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 80
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lkvi;->h:Ljava/lang/String;

    .line 82
    invoke-interface {v0, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lkvi;->i:I

    goto :goto_1
.end method

.method public D()Z
    .locals 1

    .prologue
    .line 94
    iget-boolean v0, p0, Lkvi;->g:Z

    return v0
.end method

.method public E()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lkvi;->h:Ljava/lang/String;

    return-object v0
.end method

.method public F()I
    .locals 1

    .prologue
    .line 102
    iget v0, p0, Lkvi;->i:I

    return v0
.end method
