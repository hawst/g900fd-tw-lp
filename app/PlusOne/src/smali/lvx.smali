.class public final Llvx;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:[Llub;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Loxq;-><init>()V

    .line 14
    sget-object v0, Llub;->a:[Llub;

    iput-object v0, p0, Llvx;->b:[Llub;

    .line 9
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 35
    .line 36
    iget-object v0, p0, Llvx;->a:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 37
    const/4 v0, 0x1

    iget-object v2, p0, Llvx;->a:Ljava/lang/String;

    .line 38
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 40
    :goto_0
    iget-object v2, p0, Llvx;->b:[Llub;

    if-eqz v2, :cond_1

    .line 41
    iget-object v2, p0, Llvx;->b:[Llub;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 42
    if-eqz v4, :cond_0

    .line 43
    const/4 v5, 0x3

    .line 44
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 41
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 48
    :cond_1
    iget-object v1, p0, Llvx;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 49
    iput v0, p0, Llvx;->ai:I

    .line 50
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Llvx;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 58
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 59
    sparse-switch v0, :sswitch_data_0

    .line 63
    iget-object v2, p0, Llvx;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 64
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Llvx;->ah:Ljava/util/List;

    .line 67
    :cond_1
    iget-object v2, p0, Llvx;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 69
    :sswitch_0
    return-object p0

    .line 74
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llvx;->a:Ljava/lang/String;

    goto :goto_0

    .line 78
    :sswitch_2
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 79
    iget-object v0, p0, Llvx;->b:[Llub;

    if-nez v0, :cond_3

    move v0, v1

    .line 80
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Llub;

    .line 81
    iget-object v3, p0, Llvx;->b:[Llub;

    if-eqz v3, :cond_2

    .line 82
    iget-object v3, p0, Llvx;->b:[Llub;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 84
    :cond_2
    iput-object v2, p0, Llvx;->b:[Llub;

    .line 85
    :goto_2
    iget-object v2, p0, Llvx;->b:[Llub;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 86
    iget-object v2, p0, Llvx;->b:[Llub;

    new-instance v3, Llub;

    invoke-direct {v3}, Llub;-><init>()V

    aput-object v3, v2, v0

    .line 87
    iget-object v2, p0, Llvx;->b:[Llub;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 88
    invoke-virtual {p1}, Loxn;->a()I

    .line 85
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 79
    :cond_3
    iget-object v0, p0, Llvx;->b:[Llub;

    array-length v0, v0

    goto :goto_1

    .line 91
    :cond_4
    iget-object v2, p0, Llvx;->b:[Llub;

    new-instance v3, Llub;

    invoke-direct {v3}, Llub;-><init>()V

    aput-object v3, v2, v0

    .line 92
    iget-object v2, p0, Llvx;->b:[Llub;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 59
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x1a -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 19
    iget-object v0, p0, Llvx;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 20
    const/4 v0, 0x1

    iget-object v1, p0, Llvx;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 22
    :cond_0
    iget-object v0, p0, Llvx;->b:[Llub;

    if-eqz v0, :cond_2

    .line 23
    iget-object v1, p0, Llvx;->b:[Llub;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 24
    if-eqz v3, :cond_1

    .line 25
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 23
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 29
    :cond_2
    iget-object v0, p0, Llvx;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 31
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Llvx;->a(Loxn;)Llvx;

    move-result-object v0

    return-object v0
.end method
