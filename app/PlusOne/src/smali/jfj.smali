.class public abstract Ljfj;
.super Ljfy;
.source "PG"


# instance fields
.field private c:[Ljfk;


# direct methods
.method public constructor <init>(Ljava/lang/String;[Ljfk;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljfy;-><init>()V

    .line 29
    invoke-virtual {p0, p1}, Ljfj;->b(Ljava/lang/String;)V

    .line 30
    iput-object p2, p0, Ljfj;->c:[Ljfk;

    .line 31
    return-void
.end method

.method private f(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Ljfj;->a:Landroid/content/Context;

    iget-object v1, p0, Ljfj;->c:[Ljfk;

    aget-object v1, v1, p1

    iget v1, v1, Ljfk;->b:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Ljfj;->c:[Ljfk;

    array-length v0, v0

    return v0
.end method

.method public a(ILjgb;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 50
    iget-object v0, p0, Ljfj;->c:[Ljfk;

    aget-object v0, v0, p1

    iget v1, v0, Ljfk;->a:I

    .line 51
    invoke-direct {p0, p1}, Ljfj;->f(I)Ljava/lang/String;

    move-result-object v3

    .line 52
    invoke-virtual {p0}, Ljfj;->r()Z

    move-result v7

    move-object v0, p2

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    .line 50
    invoke-interface/range {v0 .. v7}, Ljgb;->a(ILizu;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 53
    return-void
.end method

.method public a(JLjgb;)V
    .locals 1

    .prologue
    .line 112
    long-to-int v0, p1

    invoke-virtual {p0, v0, p3}, Ljfj;->a(ILjgb;)V

    .line 113
    return-void
.end method

.method public a(Ljfz;J)V
    .locals 4

    .prologue
    .line 57
    long-to-int v0, p2

    .line 58
    invoke-direct {p0, v0}, Ljfj;->f(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljfj;->i()I

    move-result v1

    .line 59
    invoke-virtual {p0}, Ljfj;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Ljfj;->j()Landroid/content/Intent;

    move-result-object v3

    .line 58
    invoke-virtual {p1, v0, v1, v2, v3}, Ljfz;->a(Ljava/lang/String;ILjava/lang/String;Landroid/content/Intent;)V

    .line 60
    return-void
.end method

.method public a(J)Z
    .locals 1

    .prologue
    .line 100
    long-to-int v0, p1

    invoke-virtual {p0, v0}, Ljfj;->c(I)Z

    move-result v0

    return v0
.end method

.method public as_()V
    .locals 0

    .prologue
    .line 77
    return-void
.end method

.method public at_()V
    .locals 0

    .prologue
    .line 81
    return-void
.end method

.method public b(J)I
    .locals 1

    .prologue
    .line 104
    long-to-int v0, p1

    invoke-virtual {p0, v0}, Ljfj;->d(I)I

    move-result v0

    return v0
.end method

.method public c(J)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 108
    long-to-int v0, p1

    invoke-virtual {p0, v0}, Ljfj;->a(I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public c(I)Z
    .locals 1

    .prologue
    .line 35
    if-ltz p1, :cond_0

    iget-object v0, p0, Ljfj;->c:[Ljfk;

    array-length v0, v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d(I)I
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    return v0
.end method

.method public final e(I)J
    .locals 2

    .prologue
    .line 72
    int-to-long v0, p1

    return-wide v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    const/4 v0, 0x0

    return-object v0
.end method

.method public i()I
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x0

    return v0
.end method

.method public j()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 96
    const/4 v0, 0x0

    return-object v0
.end method
