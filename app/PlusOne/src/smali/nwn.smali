.class public final Lnwn;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Logr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 74
    invoke-direct {p0}, Loxq;-><init>()V

    .line 77
    sget-object v0, Logr;->a:[Logr;

    iput-object v0, p0, Lnwn;->a:[Logr;

    .line 74
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 95
    .line 96
    iget-object v1, p0, Lnwn;->a:[Logr;

    if-eqz v1, :cond_1

    .line 97
    iget-object v2, p0, Lnwn;->a:[Logr;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 98
    if-eqz v4, :cond_0

    .line 99
    const/4 v5, 0x1

    .line 100
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 97
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 104
    :cond_1
    iget-object v1, p0, Lnwn;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 105
    iput v0, p0, Lnwn;->ai:I

    .line 106
    return v0
.end method

.method public a(Loxn;)Lnwn;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 114
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 115
    sparse-switch v0, :sswitch_data_0

    .line 119
    iget-object v2, p0, Lnwn;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 120
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnwn;->ah:Ljava/util/List;

    .line 123
    :cond_1
    iget-object v2, p0, Lnwn;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 125
    :sswitch_0
    return-object p0

    .line 130
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 131
    iget-object v0, p0, Lnwn;->a:[Logr;

    if-nez v0, :cond_3

    move v0, v1

    .line 132
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Logr;

    .line 133
    iget-object v3, p0, Lnwn;->a:[Logr;

    if-eqz v3, :cond_2

    .line 134
    iget-object v3, p0, Lnwn;->a:[Logr;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 136
    :cond_2
    iput-object v2, p0, Lnwn;->a:[Logr;

    .line 137
    :goto_2
    iget-object v2, p0, Lnwn;->a:[Logr;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 138
    iget-object v2, p0, Lnwn;->a:[Logr;

    new-instance v3, Logr;

    invoke-direct {v3}, Logr;-><init>()V

    aput-object v3, v2, v0

    .line 139
    iget-object v2, p0, Lnwn;->a:[Logr;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 140
    invoke-virtual {p1}, Loxn;->a()I

    .line 137
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 131
    :cond_3
    iget-object v0, p0, Lnwn;->a:[Logr;

    array-length v0, v0

    goto :goto_1

    .line 143
    :cond_4
    iget-object v2, p0, Lnwn;->a:[Logr;

    new-instance v3, Logr;

    invoke-direct {v3}, Logr;-><init>()V

    aput-object v3, v2, v0

    .line 144
    iget-object v2, p0, Lnwn;->a:[Logr;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 115
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 82
    iget-object v0, p0, Lnwn;->a:[Logr;

    if-eqz v0, :cond_1

    .line 83
    iget-object v1, p0, Lnwn;->a:[Logr;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 84
    if-eqz v3, :cond_0

    .line 85
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 83
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 89
    :cond_1
    iget-object v0, p0, Lnwn;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 91
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 70
    invoke-virtual {p0, p1}, Lnwn;->a(Loxn;)Lnwn;

    move-result-object v0

    return-object v0
.end method
