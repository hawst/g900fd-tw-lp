.class public final Llxp;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Boolean;

.field private b:Ljava/lang/Boolean;

.field private c:I

.field private d:Ljava/lang/Long;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/Boolean;

.field private g:Ljava/lang/Boolean;

.field private h:I

.field private i:[Llwv;

.field private j:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/high16 v0, -0x80000000

    .line 1943
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1964
    iput v0, p0, Llxp;->c:I

    .line 1975
    iput v0, p0, Llxp;->h:I

    .line 1980
    sget-object v0, Llwv;->a:[Llwv;

    iput-object v0, p0, Llxp;->i:[Llwv;

    .line 1943
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/high16 v6, -0x80000000

    .line 2027
    .line 2028
    iget-object v0, p0, Llxp;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_a

    .line 2029
    const/4 v0, 0x1

    iget-object v2, p0, Llxp;->b:Ljava/lang/Boolean;

    .line 2030
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 2032
    :goto_0
    iget v2, p0, Llxp;->c:I

    if-eq v2, v6, :cond_0

    .line 2033
    const/4 v2, 0x2

    iget v3, p0, Llxp;->c:I

    .line 2034
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 2036
    :cond_0
    iget-object v2, p0, Llxp;->f:Ljava/lang/Boolean;

    if-eqz v2, :cond_1

    .line 2037
    const/4 v2, 0x3

    iget-object v3, p0, Llxp;->f:Ljava/lang/Boolean;

    .line 2038
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 2040
    :cond_1
    iget-object v2, p0, Llxp;->g:Ljava/lang/Boolean;

    if-eqz v2, :cond_2

    .line 2041
    const/4 v2, 0x4

    iget-object v3, p0, Llxp;->g:Ljava/lang/Boolean;

    .line 2042
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 2044
    :cond_2
    iget-object v2, p0, Llxp;->j:Ljava/lang/Long;

    if-eqz v2, :cond_3

    .line 2045
    const/4 v2, 0x5

    iget-object v3, p0, Llxp;->j:Ljava/lang/Long;

    .line 2046
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Loxo;->f(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 2048
    :cond_3
    iget v2, p0, Llxp;->h:I

    if-eq v2, v6, :cond_4

    .line 2049
    const/4 v2, 0x6

    iget v3, p0, Llxp;->h:I

    .line 2050
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 2052
    :cond_4
    iget-object v2, p0, Llxp;->a:Ljava/lang/Boolean;

    if-eqz v2, :cond_5

    .line 2053
    const/4 v2, 0x7

    iget-object v3, p0, Llxp;->a:Ljava/lang/Boolean;

    .line 2054
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 2056
    :cond_5
    iget-object v2, p0, Llxp;->d:Ljava/lang/Long;

    if-eqz v2, :cond_6

    .line 2057
    const/16 v2, 0x8

    iget-object v3, p0, Llxp;->d:Ljava/lang/Long;

    .line 2058
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Loxo;->f(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 2060
    :cond_6
    iget-object v2, p0, Llxp;->e:Ljava/lang/String;

    if-eqz v2, :cond_7

    .line 2061
    const/16 v2, 0x9

    iget-object v3, p0, Llxp;->e:Ljava/lang/String;

    .line 2062
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2064
    :cond_7
    iget-object v2, p0, Llxp;->i:[Llwv;

    if-eqz v2, :cond_9

    .line 2065
    iget-object v2, p0, Llxp;->i:[Llwv;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_9

    aget-object v4, v2, v1

    .line 2066
    if-eqz v4, :cond_8

    .line 2067
    const/16 v5, 0xa

    .line 2068
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 2065
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2072
    :cond_9
    iget-object v1, p0, Llxp;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2073
    iput v0, p0, Llxp;->ai:I

    .line 2074
    return v0

    :cond_a
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Llxp;
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v4, 0x2

    const/4 v1, 0x0

    .line 2082
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2083
    sparse-switch v0, :sswitch_data_0

    .line 2087
    iget-object v2, p0, Llxp;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 2088
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Llxp;->ah:Ljava/util/List;

    .line 2091
    :cond_1
    iget-object v2, p0, Llxp;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2093
    :sswitch_0
    return-object p0

    .line 2098
    :sswitch_1
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Llxp;->b:Ljava/lang/Boolean;

    goto :goto_0

    .line 2102
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 2103
    if-eq v0, v6, :cond_2

    if-eqz v0, :cond_2

    if-eq v0, v5, :cond_2

    if-eq v0, v4, :cond_2

    const/4 v2, 0x4

    if-eq v0, v2, :cond_2

    const/4 v2, 0x5

    if-eq v0, v2, :cond_2

    const/4 v2, 0x6

    if-ne v0, v2, :cond_3

    .line 2110
    :cond_2
    iput v0, p0, Llxp;->c:I

    goto :goto_0

    .line 2112
    :cond_3
    iput v6, p0, Llxp;->c:I

    goto :goto_0

    .line 2117
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Llxp;->f:Ljava/lang/Boolean;

    goto :goto_0

    .line 2121
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Llxp;->g:Ljava/lang/Boolean;

    goto :goto_0

    .line 2125
    :sswitch_5
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Llxp;->j:Ljava/lang/Long;

    goto :goto_0

    .line 2129
    :sswitch_6
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 2130
    if-eq v0, v4, :cond_4

    if-eqz v0, :cond_4

    if-ne v0, v5, :cond_5

    .line 2133
    :cond_4
    iput v0, p0, Llxp;->h:I

    goto :goto_0

    .line 2135
    :cond_5
    iput v4, p0, Llxp;->h:I

    goto :goto_0

    .line 2140
    :sswitch_7
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Llxp;->a:Ljava/lang/Boolean;

    goto :goto_0

    .line 2144
    :sswitch_8
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Llxp;->d:Ljava/lang/Long;

    goto/16 :goto_0

    .line 2148
    :sswitch_9
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llxp;->e:Ljava/lang/String;

    goto/16 :goto_0

    .line 2152
    :sswitch_a
    const/16 v0, 0x52

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 2153
    iget-object v0, p0, Llxp;->i:[Llwv;

    if-nez v0, :cond_7

    move v0, v1

    .line 2154
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Llwv;

    .line 2155
    iget-object v3, p0, Llxp;->i:[Llwv;

    if-eqz v3, :cond_6

    .line 2156
    iget-object v3, p0, Llxp;->i:[Llwv;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2158
    :cond_6
    iput-object v2, p0, Llxp;->i:[Llwv;

    .line 2159
    :goto_2
    iget-object v2, p0, Llxp;->i:[Llwv;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_8

    .line 2160
    iget-object v2, p0, Llxp;->i:[Llwv;

    new-instance v3, Llwv;

    invoke-direct {v3}, Llwv;-><init>()V

    aput-object v3, v2, v0

    .line 2161
    iget-object v2, p0, Llxp;->i:[Llwv;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 2162
    invoke-virtual {p1}, Loxn;->a()I

    .line 2159
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2153
    :cond_7
    iget-object v0, p0, Llxp;->i:[Llwv;

    array-length v0, v0

    goto :goto_1

    .line 2165
    :cond_8
    iget-object v2, p0, Llxp;->i:[Llwv;

    new-instance v3, Llwv;

    invoke-direct {v3}, Llwv;-><init>()V

    aput-object v3, v2, v0

    .line 2166
    iget-object v2, p0, Llxp;->i:[Llwv;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 2083
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    const/high16 v4, -0x80000000

    .line 1987
    iget-object v0, p0, Llxp;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 1988
    const/4 v0, 0x1

    iget-object v1, p0, Llxp;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 1990
    :cond_0
    iget v0, p0, Llxp;->c:I

    if-eq v0, v4, :cond_1

    .line 1991
    const/4 v0, 0x2

    iget v1, p0, Llxp;->c:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1993
    :cond_1
    iget-object v0, p0, Llxp;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 1994
    const/4 v0, 0x3

    iget-object v1, p0, Llxp;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 1996
    :cond_2
    iget-object v0, p0, Llxp;->g:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 1997
    const/4 v0, 0x4

    iget-object v1, p0, Llxp;->g:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 1999
    :cond_3
    iget-object v0, p0, Llxp;->j:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 2000
    const/4 v0, 0x5

    iget-object v1, p0, Llxp;->j:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 2002
    :cond_4
    iget v0, p0, Llxp;->h:I

    if-eq v0, v4, :cond_5

    .line 2003
    const/4 v0, 0x6

    iget v1, p0, Llxp;->h:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2005
    :cond_5
    iget-object v0, p0, Llxp;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    .line 2006
    const/4 v0, 0x7

    iget-object v1, p0, Llxp;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 2008
    :cond_6
    iget-object v0, p0, Llxp;->d:Ljava/lang/Long;

    if-eqz v0, :cond_7

    .line 2009
    const/16 v0, 0x8

    iget-object v1, p0, Llxp;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 2011
    :cond_7
    iget-object v0, p0, Llxp;->e:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 2012
    const/16 v0, 0x9

    iget-object v1, p0, Llxp;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 2014
    :cond_8
    iget-object v0, p0, Llxp;->i:[Llwv;

    if-eqz v0, :cond_a

    .line 2015
    iget-object v1, p0, Llxp;->i:[Llwv;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_a

    aget-object v3, v1, v0

    .line 2016
    if-eqz v3, :cond_9

    .line 2017
    const/16 v4, 0xa

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 2015
    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2021
    :cond_a
    iget-object v0, p0, Llxp;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2023
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1939
    invoke-virtual {p0, p1}, Llxp;->a(Loxn;)Llxp;

    move-result-object v0

    return-object v0
.end method
