.class public final Lpqw;
.super Loxq;
.source "PG"


# instance fields
.field private a:[Lpqz;

.field private b:Lpqx;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 787
    invoke-direct {p0}, Loxq;-><init>()V

    .line 790
    sget-object v0, Lpqz;->a:[Lpqz;

    iput-object v0, p0, Lpqw;->a:[Lpqz;

    .line 793
    const/4 v0, 0x0

    iput-object v0, p0, Lpqw;->b:Lpqx;

    .line 787
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 814
    .line 815
    iget-object v1, p0, Lpqw;->a:[Lpqz;

    if-eqz v1, :cond_1

    .line 816
    iget-object v2, p0, Lpqw;->a:[Lpqz;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 817
    if-eqz v4, :cond_0

    .line 818
    const/4 v5, 0x1

    .line 819
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 816
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 823
    :cond_1
    iget-object v1, p0, Lpqw;->b:Lpqx;

    if-eqz v1, :cond_2

    .line 824
    const/4 v1, 0x2

    iget-object v2, p0, Lpqw;->b:Lpqx;

    .line 825
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 827
    :cond_2
    iget-object v1, p0, Lpqw;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 828
    iput v0, p0, Lpqw;->ai:I

    .line 829
    return v0
.end method

.method public a(Loxn;)Lpqw;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 837
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 838
    sparse-switch v0, :sswitch_data_0

    .line 842
    iget-object v2, p0, Lpqw;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 843
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lpqw;->ah:Ljava/util/List;

    .line 846
    :cond_1
    iget-object v2, p0, Lpqw;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 848
    :sswitch_0
    return-object p0

    .line 853
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 854
    iget-object v0, p0, Lpqw;->a:[Lpqz;

    if-nez v0, :cond_3

    move v0, v1

    .line 855
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lpqz;

    .line 856
    iget-object v3, p0, Lpqw;->a:[Lpqz;

    if-eqz v3, :cond_2

    .line 857
    iget-object v3, p0, Lpqw;->a:[Lpqz;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 859
    :cond_2
    iput-object v2, p0, Lpqw;->a:[Lpqz;

    .line 860
    :goto_2
    iget-object v2, p0, Lpqw;->a:[Lpqz;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 861
    iget-object v2, p0, Lpqw;->a:[Lpqz;

    new-instance v3, Lpqz;

    invoke-direct {v3}, Lpqz;-><init>()V

    aput-object v3, v2, v0

    .line 862
    iget-object v2, p0, Lpqw;->a:[Lpqz;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 863
    invoke-virtual {p1}, Loxn;->a()I

    .line 860
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 854
    :cond_3
    iget-object v0, p0, Lpqw;->a:[Lpqz;

    array-length v0, v0

    goto :goto_1

    .line 866
    :cond_4
    iget-object v2, p0, Lpqw;->a:[Lpqz;

    new-instance v3, Lpqz;

    invoke-direct {v3}, Lpqz;-><init>()V

    aput-object v3, v2, v0

    .line 867
    iget-object v2, p0, Lpqw;->a:[Lpqz;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 871
    :sswitch_2
    iget-object v0, p0, Lpqw;->b:Lpqx;

    if-nez v0, :cond_5

    .line 872
    new-instance v0, Lpqx;

    invoke-direct {v0}, Lpqx;-><init>()V

    iput-object v0, p0, Lpqw;->b:Lpqx;

    .line 874
    :cond_5
    iget-object v0, p0, Lpqw;->b:Lpqx;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 838
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 798
    iget-object v0, p0, Lpqw;->a:[Lpqz;

    if-eqz v0, :cond_1

    .line 799
    iget-object v1, p0, Lpqw;->a:[Lpqz;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 800
    if-eqz v3, :cond_0

    .line 801
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 799
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 805
    :cond_1
    iget-object v0, p0, Lpqw;->b:Lpqx;

    if-eqz v0, :cond_2

    .line 806
    const/4 v0, 0x2

    iget-object v1, p0, Lpqw;->b:Lpqx;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 808
    :cond_2
    iget-object v0, p0, Lpqw;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 810
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 783
    invoke-virtual {p0, p1}, Lpqw;->a(Loxn;)Lpqw;

    move-result-object v0

    return-object v0
.end method
