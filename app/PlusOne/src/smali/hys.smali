.class public final Lhys;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Lloy;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 24
    new-instance v0, Lloy;

    const-string v1, "debug.social.die_hard"

    invoke-direct {v0, v1}, Lloy;-><init>(Ljava/lang/String;)V

    sput-object v0, Lhys;->a:Lloy;

    return-void
.end method

.method public static a([BLjava/lang/Class;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Loxu;",
            ">([B",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 74
    if-eqz p0, :cond_0

    array-length v1, p0

    if-nez v1, :cond_1

    :cond_0
    move-object v0, v2

    .line 97
    :goto_0
    return-object v0

    .line 78
    :cond_1
    const/4 v1, 0x0

    :try_start_0
    array-length v3, p0

    invoke-static {p0, v1, v3}, Loxn;->a([BII)Loxn;

    move-result-object v4

    .line 79
    invoke-virtual {v4}, Loxn;->g()I

    move-result v5

    .line 80
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v5}, Ljava/util/ArrayList;-><init>(I)V

    move v3, v0

    .line 81
    :goto_1
    if-ge v3, v5, :cond_2

    .line 82
    invoke-virtual {p1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Loxu;

    .line 83
    invoke-virtual {v4, v0}, Loxn;->a(Loxu;)V

    .line 84
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    .line 81
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 86
    goto :goto_0

    .line 87
    :catch_0
    move-exception v0

    .line 88
    const-string v1, "MessageNanoUtils"

    const-string v3, "Failed to deserialize"

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 89
    sget-object v0, Lhys;->a:Lloy;

    :goto_2
    move-object v0, v2

    .line 97
    goto :goto_0

    .line 90
    :catch_1
    move-exception v0

    .line 91
    const-string v1, "MessageNanoUtils"

    const-string v3, "Failed to deserialize"

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 92
    sget-object v0, Lhys;->a:Lloy;

    goto :goto_2

    .line 93
    :catch_2
    move-exception v0

    .line 94
    const-string v1, "MessageNanoUtils"

    const-string v3, "Failed to deserialize"

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 95
    sget-object v0, Lhys;->a:Lloy;

    goto :goto_2
.end method

.method public static a(Loxu;[B)Loxu;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Loxu;",
            ">(TT;[B)TT;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 35
    if-nez p1, :cond_0

    .line 45
    :goto_0
    return-object v1

    .line 39
    :cond_0
    :try_start_0
    invoke-static {p0, p1}, Loxu;->a(Loxu;[B)Loxu;
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_1
    move-object v1, v0

    .line 45
    goto :goto_0

    .line 40
    :catch_0
    move-exception v2

    .line 41
    const-string v3, "MessageNanoUtils"

    const-string v4, "Invalid binary data: "

    invoke-virtual {v2}, Loxt;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v4, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-static {v3, v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 42
    sget-object v0, Lhys;->a:Lloy;

    move-object v0, v1

    .line 43
    goto :goto_1

    .line 41
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public static a(Ljava/util/List;)[B
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Loxu;",
            ">(",
            "Ljava/util/List",
            "<TT;>;)[B"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 52
    if-nez p0, :cond_0

    move-object v0, v1

    .line 67
    :goto_0
    return-object v0

    .line 56
    :cond_0
    :try_start_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Loxo;->i(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    move v2, v3

    move v4, v0

    :goto_1
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Loxu;

    invoke-static {v0}, Loxo;->c(Loxu;)I

    move-result v0

    add-int/2addr v4, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_1
    new-array v2, v4, [B

    .line 57
    const/4 v0, 0x0

    array-length v4, v2

    invoke-static {v2, v0, v4}, Loxo;->a([BII)Loxo;

    move-result-object v4

    .line 58
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {v4, v0}, Loxo;->a(I)V

    .line 59
    :goto_2
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_2

    .line 60
    invoke-interface {p0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Loxu;

    invoke-virtual {v4, v0}, Loxo;->b(Loxu;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 59
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    :cond_2
    move-object v0, v2

    .line 62
    goto :goto_0

    .line 63
    :catch_0
    move-exception v0

    .line 64
    const-string v2, "MessageNanoUtils"

    const-string v3, "Failed to serialize"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 65
    sget-object v0, Lhys;->a:Lloy;

    move-object v0, v1

    .line 67
    goto :goto_0
.end method
