.class public final Lexu;
.super Ljfy;
.source "PG"


# instance fields
.field private c:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljfy;-><init>()V

    .line 22
    const/4 v0, 0x1

    iput-boolean v0, p0, Lexu;->c:Z

    .line 25
    const-string v0, "virtual_circles"

    invoke-virtual {p0, v0}, Lexu;->b(Ljava/lang/String;)V

    .line 26
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x1

    return v0
.end method

.method public a(I)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lexu;->a:Landroid/content/Context;

    invoke-static {v0}, Leyq;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public a(ILjgb;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 55
    const v1, 0x7f020515

    iget-object v0, p0, Lexu;->a:Landroid/content/Context;

    const v3, 0x7f0a06fa

    .line 56
    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v7, 0x0

    move-object v0, p2

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    .line 55
    invoke-interface/range {v0 .. v7}, Ljgb;->a(ILizu;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 58
    new-instance v0, Lhmk;

    sget-object v1, Lomy;->h:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    invoke-interface {p2, v0}, Ljgb;->a(Lhmk;)Lhmk;

    .line 60
    return-void
.end method

.method public a(JLjgb;)V
    .locals 1

    .prologue
    .line 101
    long-to-int v0, p1

    invoke-virtual {p0, v0, p3}, Lexu;->a(ILjgb;)V

    .line 102
    return-void
.end method

.method public a(Landroid/content/Context;I)V
    .locals 2

    .prologue
    .line 30
    invoke-super {p0, p1, p2}, Ljfy;->a(Landroid/content/Context;I)V

    .line 31
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    .line 32
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    invoke-interface {v0, p2}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 34
    const-string v1, "is_plus_page"

    invoke-interface {v0, v1}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lexu;->c:Z

    .line 36
    :cond_0
    return-void

    .line 34
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljfz;J)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 82
    new-instance v4, Landroid/content/Intent;

    iget-object v0, p0, Lexu;->a:Landroid/content/Context;

    const-class v1, Lcom/google/android/apps/plus/phone/HomeActivity;

    invoke-direct {v4, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 83
    const-string v0, "destination"

    const/4 v1, 0x5

    invoke-virtual {v4, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 84
    iget-object v0, p0, Lexu;->a:Landroid/content/Context;

    const v1, 0x7f0a06fa

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    iget-boolean v0, p0, Lexu;->c:Z

    if-eqz v0, :cond_0

    const-string v5, "stream"

    :goto_0
    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Ljfz;->a(Ljava/lang/String;ILjava/lang/String;Landroid/content/Intent;Ljava/lang/String;)V

    .line 86
    return-void

    :cond_0
    move-object v5, v3

    .line 84
    goto :goto_0
.end method

.method public a(J)Z
    .locals 1

    .prologue
    .line 89
    long-to-int v0, p1

    invoke-virtual {p0, v0}, Lexu;->c(I)Z

    move-result v0

    return v0
.end method

.method public as_()V
    .locals 0

    .prologue
    .line 64
    return-void
.end method

.method protected at_()V
    .locals 0

    .prologue
    .line 73
    return-void
.end method

.method public b(J)I
    .locals 1

    .prologue
    .line 93
    long-to-int v0, p1

    invoke-virtual {p0, v0}, Lexu;->d(I)I

    move-result v0

    return v0
.end method

.method public b(I)Z
    .locals 1

    .prologue
    .line 106
    const/4 v0, 0x0

    return v0
.end method

.method public c(J)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 97
    long-to-int v0, p1

    invoke-virtual {p0, v0}, Lexu;->a(I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public c(I)Z
    .locals 1

    .prologue
    .line 40
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d(I)I
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x0

    return v0
.end method

.method public e(I)J
    .locals 2

    .prologue
    .line 77
    const-wide/16 v0, 0x0

    return-wide v0
.end method
