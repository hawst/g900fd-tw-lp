.class public final Lfpd;
.super Llol;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lbc;
.implements Lcqm;
.implements Lept;
.implements Lhjj;
.implements Lhmq;
.implements Lhob;
.implements Lkch;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Llol;",
        "Landroid/view/View$OnClickListener;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lcqm;",
        "Lept;",
        "Lhjj;",
        "Lhmq;",
        "Lhob;",
        "Lkch;"
    }
.end annotation


# static fields
.field private static final N:Lloz;


# instance fields
.field private final O:Lhje;

.field private P:Lhee;

.field private final Q:Licq;

.field private R:Lkci;

.field private S:Lcqj;

.field private T:Lcom/google/android/apps/plus/views/FastScrollListView;

.field private U:I

.field private V:Z

.field private W:Z

.field private X:Z

.field private Y:Ldwg;

.field private Z:[Lepm;

.field private aa:Z

.field private ab:Litz;

.field private ac:Lepm;

.field private ad:Ldxr;

.field private ae:Lfpj;

.field private af:Lnyq;

.field private final ag:Liue;

.field private final ah:Lbc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbc",
            "<",
            "Lnyq;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 93
    new-instance v0, Lloz;

    const-string v1, "debug.stories.reset.promo"

    invoke-direct {v0, v1}, Lloz;-><init>(Ljava/lang/String;)V

    sput-object v0, Lfpd;->N:Lloz;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 78
    invoke-direct {p0}, Llol;-><init>()V

    .line 96
    new-instance v0, Lhje;

    iget-object v1, p0, Lfpd;->av:Llqm;

    invoke-direct {v0, p0, v1, p0}, Lhje;-><init>(Lu;Llqr;Lhjj;)V

    iput-object v0, p0, Lfpd;->O:Lhje;

    .line 101
    new-instance v0, Licq;

    iget-object v1, p0, Lfpd;->av:Llqm;

    invoke-direct {v0, v1}, Licq;-><init>(Llqr;)V

    const v1, 0x7f0a01bd

    .line 102
    invoke-virtual {v0, v1}, Licq;->b(I)Licq;

    move-result-object v0

    iput-object v0, p0, Lfpd;->Q:Licq;

    .line 104
    new-instance v0, Lkci;

    iget-object v1, p0, Lfpd;->av:Llqm;

    const v2, 0x7f10005b

    invoke-direct {v0, p0, v1, v2, p0}, Lkci;-><init>(Lu;Llqr;ILkch;)V

    iput-object v0, p0, Lfpd;->R:Lkci;

    .line 167
    new-instance v0, Lfpe;

    invoke-direct {v0, p0}, Lfpe;-><init>(Lfpd;)V

    iput-object v0, p0, Lfpd;->ag:Liue;

    .line 182
    new-instance v0, Lfpf;

    invoke-direct {v0, p0}, Lfpf;-><init>(Lfpd;)V

    iput-object v0, p0, Lfpd;->ah:Lbc;

    .line 642
    return-void
.end method

.method private U()Ljava/lang/String;
    .locals 3

    .prologue
    .line 342
    invoke-virtual {p0}, Lfpd;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "owner_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 343
    if-nez v0, :cond_0

    iget-object v1, p0, Lfpd;->P:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 344
    iget-object v0, p0, Lfpd;->P:Lhee;

    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v0

    const-string v1, "gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 346
    :cond_0
    return-object v0
.end method

.method private V()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 439
    iget-object v0, p0, Lfpd;->Z:[Lepm;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lfpd;->V:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lfpd;->aa:Z

    if-nez v0, :cond_2

    .line 442
    invoke-direct {p0}, Lfpd;->W()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfpd;->at:Llnl;

    iget-object v1, p0, Lfpd;->P:Lhee;

    .line 443
    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    iget-object v2, p0, Lfpd;->af:Lnyq;

    invoke-static {v0, v1, v2}, Lfqi;->a(Landroid/content/Context;ILnyq;)Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lfpd;->U:I

    if-ne v0, v3, :cond_2

    .line 445
    :cond_0
    iput-boolean v3, p0, Lfpd;->aa:Z

    .line 449
    invoke-direct {p0}, Lfpd;->W()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfpd;->at:Llnl;

    iget-object v1, p0, Lfpd;->P:Lhee;

    .line 450
    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    iget-object v2, p0, Lfpd;->af:Lnyq;

    invoke-static {v0, v1, v2}, Lfqi;->a(Landroid/content/Context;ILnyq;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lfpd;->U:I

    if-nez v0, :cond_1

    .line 452
    iput v3, p0, Lfpd;->U:I

    .line 454
    :cond_1
    iget-object v0, p0, Lfpd;->Y:Ldwg;

    invoke-virtual {v0}, Ldwg;->a()V

    .line 457
    :cond_2
    return-void
.end method

.method private W()Z
    .locals 1

    .prologue
    .line 482
    iget-object v0, p0, Lfpd;->S:Lcqj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfpd;->S:Lcqj;

    invoke-virtual {v0}, Lcqj;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private X()Z
    .locals 2

    .prologue
    .line 486
    iget-object v0, p0, Lfpd;->au:Llnh;

    const-class v1, Lhoc;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    const-string v1, "ListStoriesTask"

    invoke-virtual {v0, v1}, Lhoc;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lfpd;Lnyq;)Lnyq;
    .locals 0

    .prologue
    .line 78
    iput-object p1, p0, Lfpd;->af:Lnyq;

    return-object p1
.end method

.method static synthetic a(Lfpd;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0, p1}, Lfpd;->c(Landroid/view/View;)V

    return-void
.end method

.method static synthetic a(Lfpd;)Z
    .locals 1

    .prologue
    .line 78
    invoke-direct {p0}, Lfpd;->aa()Z

    move-result v0

    return v0
.end method

.method private aa()Z
    .locals 1

    .prologue
    .line 565
    iget-object v0, p0, Lfpd;->ab:Litz;

    invoke-virtual {v0}, Litz;->d()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lfpd;->ab:Litz;

    invoke-virtual {v0}, Litz;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private ab()V
    .locals 1

    .prologue
    .line 587
    invoke-virtual {p0}, Lfpd;->n()Lz;

    move-result-object v0

    check-cast v0, Los;

    invoke-virtual {v0}, Los;->h()Loo;

    move-result-object v0

    invoke-virtual {v0}, Loo;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 588
    iget-object v0, p0, Lfpd;->R:Lkci;

    invoke-virtual {v0}, Lkci;->e()V

    .line 590
    :cond_0
    iget-object v0, p0, Lfpd;->O:Lhje;

    invoke-virtual {v0}, Lhje;->a()V

    .line 591
    return-void
.end method

.method static synthetic b(Lfpd;)V
    .locals 6

    .prologue
    .line 78
    invoke-direct {p0}, Lfpd;->aa()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfpd;->Z:[Lepm;

    if-nez v0, :cond_0

    new-instance v0, Lfqi;

    iget-object v1, p0, Lfpd;->at:Llnl;

    iget-object v2, p0, Lfpd;->P:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    invoke-direct {v0, v1, v2, p0}, Lfqi;-><init>(Landroid/content/Context;ILept;)V

    iput-object v0, p0, Lfpd;->ac:Lepm;

    new-instance v0, Ldxr;

    iget-object v1, p0, Lfpd;->at:Llnl;

    iget-object v2, p0, Lfpd;->P:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    sget-object v5, Lepn;->j:Lepn;

    move-object v3, p0

    move-object v4, p0

    invoke-direct/range {v0 .. v5}, Ldxr;-><init>(Landroid/content/Context;ILept;Lu;Lepn;)V

    invoke-virtual {p0}, Lfpd;->o()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a01ea

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldxr;->a(Ljava/lang/String;)Ldxr;

    move-result-object v0

    iput-object v0, p0, Lfpd;->ad:Ldxr;

    new-instance v0, Lfpj;

    iget-object v1, p0, Lfpd;->at:Llnl;

    iget-object v2, p0, Lfpd;->P:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    iget-object v3, p0, Lfpd;->ab:Litz;

    invoke-direct {v0, v1, v2, p0, v3}, Lfpj;-><init>(Landroid/content/Context;ILept;Litz;)V

    iput-object v0, p0, Lfpd;->ae:Lfpj;

    const/4 v0, 0x3

    new-array v0, v0, [Lepm;

    const/4 v1, 0x0

    iget-object v2, p0, Lfpd;->ac:Lepm;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lfpd;->ad:Ldxr;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lfpd;->ae:Lfpj;

    aput-object v2, v0, v1

    iput-object v0, p0, Lfpd;->Z:[Lepm;

    :cond_0
    return-void
.end method

.method private c(Landroid/view/View;)V
    .locals 11

    .prologue
    const/16 v10, 0x8

    const/4 v0, 0x2

    const v3, 0x7f100263

    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 356
    if-nez p1, :cond_0

    .line 384
    :goto_0
    return-void

    .line 359
    :cond_0
    invoke-direct {p0}, Lfpd;->W()Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v4

    :cond_1
    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 383
    :cond_2
    :goto_2
    invoke-direct {p0}, Lfpd;->ab()V

    goto :goto_0

    .line 359
    :cond_3
    iget-boolean v2, p0, Lfpd;->X:Z

    if-eqz v2, :cond_4

    invoke-direct {p0}, Lfpd;->W()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v0, 0x3

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lfpd;->Z:[Lepm;

    if-eqz v2, :cond_5

    iget-boolean v2, p0, Lfpd;->V:Z

    if-eqz v2, :cond_5

    invoke-direct {p0}, Lfpd;->X()Z

    move-result v2

    if-eqz v2, :cond_6

    :cond_5
    const/4 v0, -0x1

    goto :goto_1

    :cond_6
    iget v2, p0, Lfpd;->U:I

    if-ne v2, v1, :cond_7

    move v0, v4

    goto :goto_1

    :cond_7
    iget-object v2, p0, Lfpd;->ad:Ldxr;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lfpd;->ad:Ldxr;

    invoke-virtual {v2}, Ldxr;->g()Z

    move-result v2

    if-eqz v2, :cond_9

    :cond_8
    iget-object v2, p0, Lfpd;->ae:Lfpj;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lfpd;->ae:Lfpj;

    invoke-virtual {v2}, Lfpj;->g()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_9
    iget v2, p0, Lfpd;->U:I

    if-eq v2, v0, :cond_1

    move v0, v1

    goto :goto_1

    .line 361
    :pswitch_0
    invoke-direct {p0}, Lfpd;->W()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 362
    iget-object v0, p0, Lfpd;->Q:Licq;

    sget-object v1, Lict;->a:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    goto :goto_2

    .line 366
    :pswitch_1
    iget-object v0, p0, Lfpd;->Q:Licq;

    sget-object v1, Lict;->b:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    .line 367
    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 370
    :pswitch_2
    iget-object v0, p0, Lfpd;->Q:Licq;

    sget-object v1, Lict;->b:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    .line 371
    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 372
    const v0, 0x7f100265

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lfpd;->o()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a01fb

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f100266

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2

    .line 375
    :pswitch_3
    iget-object v0, p0, Lfpd;->Q:Licq;

    sget-object v2, Lict;->b:Lict;

    invoke-virtual {v0, v2}, Licq;->a(Lict;)V

    .line 376
    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 377
    const v0, 0x7f100265

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v2, p0, Lfpd;->at:Llnl;

    const-string v3, "stories"

    const-string v5, "https://support.google.com/plus/?hl=%locale%"

    invoke-static {v2, v3, v5}, Litk;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lfpd;->at:Llnl;

    const v5, 0x7f0a01fd

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v2, v1, v4

    invoke-virtual {v3, v5, v1}, Llnl;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Llhv;->a(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    check-cast v1, Landroid/text/Spannable;

    invoke-interface {v1}, Landroid/text/Spannable;->length()I

    move-result v2

    const-class v3, Landroid/text/style/URLSpan;

    invoke-interface {v1, v4, v2, v3}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Landroid/text/style/URLSpan;

    invoke-virtual {v2}, [Landroid/text/style/URLSpan;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Landroid/text/style/URLSpan;

    array-length v5, v2

    move v3, v4

    :goto_3
    if-ge v3, v5, :cond_a

    aget-object v6, v2, v3

    invoke-interface {v1, v6}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v7

    invoke-interface {v1, v6}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v8

    invoke-interface {v1, v6}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    new-instance v9, Lfpg;

    invoke-virtual {v6}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v9, p0, v6}, Lfpg;-><init>(Lfpd;Ljava/lang/String;)V

    invoke-interface {v1, v9, v7, v8, v4}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_a
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    const v0, 0x7f100266

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2

    .line 380
    :pswitch_4
    iget-object v0, p0, Lfpd;->Q:Licq;

    sget-object v1, Lict;->c:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    goto/16 :goto_2

    .line 359
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method static synthetic c(Lfpd;)V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0}, Lfpd;->V()V

    return-void
.end method

.method static synthetic d(Lfpd;)Lhee;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lfpd;->P:Lhee;

    return-object v0
.end method

.method static synthetic e(Lfpd;)Llnl;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lfpd;->at:Llnl;

    return-object v0
.end method

.method private e()V
    .locals 1

    .prologue
    .line 269
    invoke-direct {p0}, Lfpd;->X()Z

    move-result v0

    if-nez v0, :cond_0

    .line 270
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lfpd;->a(Ljava/lang/String;)V

    .line 273
    iget-object v0, p0, Lfpd;->S:Lcqj;

    invoke-virtual {v0}, Lcqj;->b()V

    .line 274
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfpd;->W:Z

    .line 276
    :cond_0
    return-void
.end method


# virtual methods
.method public A()V
    .locals 1

    .prologue
    .line 332
    invoke-super {p0}, Llol;->A()V

    .line 333
    iget-object v0, p0, Lfpd;->ab:Litz;

    if-eqz v0, :cond_0

    .line 334
    iget-object v0, p0, Lfpd;->ab:Litz;

    invoke-virtual {v0}, Litz;->c()V

    .line 336
    :cond_0
    return-void
.end method

.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 491
    sget-object v0, Lhmw;->aB:Lhmw;

    return-object v0
.end method

.method public K_()V
    .locals 1

    .prologue
    .line 595
    iget-object v0, p0, Lfpd;->R:Lkci;

    invoke-virtual {v0}, Lkci;->c()V

    .line 596
    invoke-direct {p0}, Lfpd;->e()V

    .line 597
    return-void
.end method

.method public Y()Z
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, Lfpd;->T:Lcom/google/android/apps/plus/views/FastScrollListView;

    invoke-static {v0}, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;->a(Landroid/widget/AbsListView;)Z

    move-result v0

    return v0
.end method

.method public Z()Z
    .locals 1

    .prologue
    .line 260
    const/4 v0, 0x1

    return v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 281
    const v0, 0x7f0400e5

    invoke-virtual {p1, v0, p2, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v7

    .line 283
    new-instance v4, Lcqn;

    invoke-direct {v4, v8, v8, v8, v8}, Lcqn;-><init>(IZIZ)V

    .line 285
    invoke-virtual {p0}, Lfpd;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "account_id"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 288
    new-instance v0, Lcqj;

    iget-object v1, p0, Lfpd;->at:Llnl;

    const/16 v3, 0x8

    new-array v5, v9, [Ljava/lang/String;

    .line 289
    invoke-direct {p0}, Lfpd;->U()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v3, v5}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 290
    invoke-virtual {p0}, Lfpd;->F_()Lhmw;

    move-result-object v6

    move-object v5, p0

    invoke-direct/range {v0 .. v6}, Lcqj;-><init>(Landroid/content/Context;ILjava/lang/String;Lcqn;Lcqm;Lhmw;)V

    .line 291
    iput-object v0, p0, Lfpd;->S:Lcqj;

    .line 292
    iget-object v0, p0, Lfpd;->S:Lcqj;

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Lcqj;->a(I)V

    .line 293
    const v0, 0x7f100306

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/FastScrollListView;

    iput-object v0, p0, Lfpd;->T:Lcom/google/android/apps/plus/views/FastScrollListView;

    .line 294
    iget-object v0, p0, Lfpd;->T:Lcom/google/android/apps/plus/views/FastScrollListView;

    new-instance v1, Lecl;

    invoke-direct {v1}, Lecl;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/FastScrollListView;->setRecyclerListener(Landroid/widget/AbsListView$RecyclerListener;)V

    .line 295
    iget-object v0, p0, Lfpd;->T:Lcom/google/android/apps/plus/views/FastScrollListView;

    iget-object v1, p0, Lfpd;->S:Lcqj;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/FastScrollListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 297
    new-instance v0, Ldwg;

    iget-object v1, p0, Lfpd;->at:Llnl;

    invoke-virtual {p0}, Lfpd;->w()Lbb;

    move-result-object v3

    iget-object v2, p0, Lfpd;->P:Lhee;

    .line 298
    invoke-interface {v2}, Lhee;->d()I

    move-result v5

    move-object v2, p0

    move v4, v9

    move v6, v8

    invoke-direct/range {v0 .. v6}, Ldwg;-><init>(Landroid/content/Context;Lept;Lbb;III)V

    iput-object v0, p0, Lfpd;->Y:Ldwg;

    .line 300
    const v0, 0x7f100263

    .line 301
    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f100266

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 302
    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 304
    invoke-direct {p0, v7}, Lfpd;->c(Landroid/view/View;)V

    .line 306
    return-object v7
.end method

.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 351
    new-instance v0, Lfpi;

    iget-object v1, p0, Lfpd;->at:Llnl;

    iget-object v2, p0, Lfpd;->P:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    const/16 v3, 0x8

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    .line 352
    invoke-direct {p0}, Lfpd;->U()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    .line 351
    invoke-static {v3, v4}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lfpi;-><init>(Landroid/content/Context;ILjava/lang/String;)V

    return-object v0
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 461
    if-eqz p1, :cond_0

    invoke-direct {p0}, Lfpd;->X()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lfpd;->X:Z

    if-nez v0, :cond_0

    .line 462
    invoke-interface {p1}, Landroid/database/Cursor;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "last_refresh_time"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 463
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long v0, v2, v0

    const-wide/32 v2, 0x36ee80

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 464
    invoke-direct {p0}, Lfpd;->e()V

    .line 467
    :cond_0
    iget-object v0, p0, Lfpd;->S:Lcqj;

    invoke-virtual {v0, p1}, Lcqj;->a(Landroid/database/Cursor;)V

    .line 470
    invoke-direct {p0}, Lfpd;->X()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lfpd;->W()Z

    move-result v0

    if-nez v0, :cond_2

    .line 471
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfpd;->V:Z

    .line 473
    :cond_2
    invoke-direct {p0}, Lfpd;->V()V

    .line 474
    invoke-virtual {p0}, Lfpd;->x()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lfpd;->c(Landroid/view/View;)V

    .line 475
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 203
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 204
    if-eqz p1, :cond_0

    .line 205
    const-string v0, "promo_interaction"

    .line 206
    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lfpd;->U:I

    .line 207
    const-string v0, "story_load_failure"

    .line 208
    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lfpd;->X:Z

    .line 209
    const-string v0, "story_refreshing"

    .line 210
    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lfpd;->W:Z

    .line 215
    :goto_0
    iget-object v0, p0, Lfpd;->au:Llnh;

    const-class v1, Lhoc;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    invoke-virtual {v0, p0}, Lhoc;->a(Lhob;)Lhoc;

    .line 216
    invoke-virtual {p0}, Lfpd;->w()Lbb;

    move-result-object v0

    invoke-virtual {v0, v3, v4, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 217
    invoke-virtual {p0}, Lfpd;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x2

    iget-object v2, p0, Lfpd;->ah:Lbc;

    invoke-virtual {v0, v1, v4, v2}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 218
    new-instance v0, Litz;

    iget-object v1, p0, Lfpd;->at:Llnl;

    iget-object v2, p0, Lfpd;->P:Lhee;

    .line 219
    invoke-interface {v2}, Lhee;->g()Lhej;

    move-result-object v2

    const-string v3, "account_name"

    invoke-interface {v2, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 220
    invoke-virtual {p0}, Lfpd;->p()Lae;

    move-result-object v3

    iget-object v4, p0, Lfpd;->ag:Liue;

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Litz;-><init>(Landroid/content/Context;Ljava/lang/String;Lae;Liue;Z)V

    iput-object v0, p0, Lfpd;->ab:Litz;

    .line 221
    return-void

    .line 212
    :cond_0
    iget-object v0, p0, Lfpd;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Lfpd;->at:Llnl;

    invoke-direct {v1, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->fn:Lhmv;

    .line 213
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 212
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    goto :goto_0
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 479
    return-void
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 78
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p2}, Lfpd;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method public a(Lepm;)V
    .locals 3

    .prologue
    .line 532
    if-eqz p1, :cond_1

    invoke-interface {p1}, Lepm;->j()Landroid/view/View;

    move-result-object v0

    .line 535
    :goto_0
    if-nez v0, :cond_0

    invoke-direct {p0}, Lfpd;->W()Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lfpd;->U:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 536
    const/4 v1, 0x2

    iput v1, p0, Lfpd;->U:I

    .line 539
    :cond_0
    iget-object v1, p0, Lfpd;->S:Lcqj;

    invoke-virtual {v1, v0}, Lcqj;->a(Landroid/view/View;)V

    .line 540
    invoke-virtual {p0}, Lfpd;->x()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lfpd;->c(Landroid/view/View;)V

    .line 541
    return-void

    .line 532
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lhjk;)V
    .locals 3

    .prologue
    .line 233
    const v0, 0x7f0a01bb

    invoke-interface {p1, v0}, Lhjk;->d(I)V

    .line 234
    const v0, 0x7f10053c

    new-instance v1, Litl;

    const-string v2, "stories"

    invoke-direct {v1, v2}, Litl;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 235
    sget-object v0, Lfpd;->N:Lloz;

    .line 236
    const v0, 0x7f10067b

    .line 240
    invoke-interface {p1, v0}, Lhjk;->a(I)Lhjc;

    move-result-object v0

    check-cast v0, Lhjv;

    .line 241
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lhjv;->a(I)V

    .line 242
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 323
    invoke-direct {p0}, Lfpd;->X()Z

    move-result v0

    if-nez v0, :cond_0

    .line 324
    iget-object v0, p0, Lfpd;->au:Llnh;

    const-class v1, Lhoc;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    new-instance v1, Lfnt;

    iget-object v2, p0, Lfpd;->at:Llnl;

    iget-object v3, p0, Lfpd;->P:Lhee;

    .line 325
    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    invoke-direct {v1, v2, v3, p1}, Lfnt;-><init>(Landroid/content/Context;ILjava/lang/String;)V

    .line 324
    invoke-virtual {v0, v1}, Lhoc;->b(Lhny;)V

    .line 326
    invoke-direct {p0}, Lfpd;->ab()V

    .line 328
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 497
    invoke-virtual {p3, v2}, Lhos;->a(Z)V

    .line 498
    invoke-direct {p0}, Lfpd;->ab()V

    .line 499
    iget-boolean v0, p0, Lfpd;->W:Z

    if-eqz v0, :cond_0

    .line 500
    iput-boolean v2, p0, Lfpd;->W:Z

    .line 501
    iget-object v0, p0, Lfpd;->S:Lcqj;

    invoke-virtual {v0}, Lcqj;->c()V

    .line 504
    :cond_0
    const-string v0, "ListStoriesTask"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 505
    invoke-static {p2}, Lhoz;->a(Lhoz;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 506
    iget-object v0, p0, Lfpd;->S:Lcqj;

    invoke-virtual {v0}, Lcqj;->a()V

    .line 507
    iput-boolean v3, p0, Lfpd;->X:Z

    .line 508
    invoke-direct {p0}, Lfpd;->W()Z

    move-result v0

    if-nez v0, :cond_1

    .line 509
    invoke-virtual {p0}, Lfpd;->n()Lz;

    move-result-object v0

    const v1, 0x7f0a01c0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 510
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 521
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lfpd;->x()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lfpd;->c(Landroid/view/View;)V

    .line 523
    :cond_2
    return-void

    .line 513
    :cond_3
    iput-boolean v2, p0, Lfpd;->X:Z

    .line 515
    if-eqz p2, :cond_4

    .line 516
    invoke-virtual {p2}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "has_stories"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_5

    .line 517
    :cond_4
    iput-boolean v3, p0, Lfpd;->V:Z

    .line 519
    :cond_5
    invoke-direct {p0}, Lfpd;->V()V

    goto :goto_0
.end method

.method public a(Loo;)V
    .locals 1

    .prologue
    .line 613
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lley;->a(Loo;Z)V

    .line 614
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 246
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    .line 247
    const v3, 0x7f10067b

    if-ne v2, v3, :cond_0

    .line 248
    invoke-direct {p0}, Lfpd;->e()V

    .line 255
    :goto_0
    return v0

    .line 250
    :cond_0
    const v3, 0x7f100690

    if-ne v2, v3, :cond_1

    .line 251
    new-instance v2, Lnyq;

    invoke-direct {v2}, Lnyq;-><init>()V

    new-instance v3, Lnyu;

    invoke-direct {v3}, Lnyu;-><init>()V

    iput-object v3, v2, Lnyq;->m:Lnyu;

    iget-object v3, v2, Lnyq;->m:Lnyu;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iput-object v4, v3, Lnyu;->a:Ljava/lang/Boolean;

    iget-object v3, v2, Lnyq;->m:Lnyu;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iput-object v4, v3, Lnyu;->b:Ljava/lang/Boolean;

    iget-object v3, p0, Lfpd;->P:Lhee;

    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    iget-object v4, p0, Lfpd;->at:Llnl;

    invoke-static {v4, v3, v2}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILnyq;)I

    iget-object v2, p0, Lfpd;->at:Llnl;

    sget-object v4, Lepn;->f:Lepn;

    invoke-static {v2, v3, v4}, Ldhv;->c(Landroid/content/Context;ILepn;)V

    iget-object v2, p0, Lfpd;->at:Llnl;

    sget-object v4, Lepn;->g:Lepn;

    invoke-static {v2, v3, v4}, Ldhv;->c(Landroid/content/Context;ILepn;)V

    iget-object v2, p0, Lfpd;->at:Llnl;

    sget-object v4, Lepn;->j:Lepn;

    invoke-static {v2, v3, v4}, Ldhv;->c(Landroid/content/Context;ILepn;)V

    iget-object v2, p0, Lfpd;->at:Llnl;

    sget-object v4, Lepn;->h:Lepn;

    invoke-static {v2, v3, v4}, Ldhv;->c(Landroid/content/Context;ILepn;)V

    .line 252
    iput v1, p0, Lfpd;->U:I

    goto :goto_0

    :cond_1
    move v0, v1

    .line 255
    goto :goto_0
.end method

.method public aO_()V
    .locals 1

    .prologue
    .line 317
    invoke-super {p0}, Llol;->aO_()V

    .line 318
    invoke-virtual {p0}, Lfpd;->x()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lfpd;->c(Landroid/view/View;)V

    .line 319
    return-void
.end method

.method public aQ_()Z
    .locals 1

    .prologue
    .line 601
    invoke-direct {p0}, Lfpd;->X()Z

    move-result v0

    return v0
.end method

.method public ac()V
    .locals 2

    .prologue
    .line 545
    iget-object v0, p0, Lfpd;->S:Lcqj;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcqj;->a(Landroid/view/View;)V

    .line 546
    return-void
.end method

.method public ad()[Lepm;
    .locals 1

    .prologue
    .line 527
    iget-object v0, p0, Lfpd;->Z:[Lepm;

    return-object v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 622
    return-void
.end method

.method public b(Loo;)V
    .locals 0

    .prologue
    .line 618
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 606
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 607
    iget-object v0, p0, Lfpd;->au:Llnh;

    const-class v1, Lhmq;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 608
    iget-object v0, p0, Lfpd;->au:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Lfpd;->P:Lhee;

    .line 609
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 225
    invoke-super {p0, p1}, Llol;->e(Landroid/os/Bundle;)V

    .line 226
    const-string v0, "promo_interaction"

    iget v1, p0, Lfpd;->U:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 227
    const-string v0, "story_load_failure"

    iget-boolean v1, p0, Lfpd;->X:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 228
    const-string v0, "story_refreshing"

    iget-boolean v1, p0, Lfpd;->W:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 229
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 311
    invoke-super {p0}, Llol;->g()V

    .line 312
    iget-object v0, p0, Lfpd;->T:Lcom/google/android/apps/plus/views/FastScrollListView;

    invoke-static {v0}, Llii;->c(Landroid/view/View;)V

    .line 313
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 550
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f100266

    if-ne v0, v1, :cond_2

    .line 551
    const/4 v0, 0x1

    iput v0, p0, Lfpd;->U:I

    .line 552
    invoke-virtual {p0}, Lfpd;->x()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lfpd;->c(Landroid/view/View;)V

    .line 554
    iget-object v0, p0, Lfpd;->ad:Ldxr;

    if-eqz v0, :cond_0

    .line 555
    iget-object v0, p0, Lfpd;->ad:Ldxr;

    invoke-virtual {v0}, Ldxr;->h()V

    .line 557
    :cond_0
    iget-object v0, p0, Lfpd;->ae:Lfpj;

    if-eqz v0, :cond_1

    .line 558
    iget-object v0, p0, Lfpd;->ae:Lfpj;

    invoke-virtual {v0}, Lfpj;->h()V

    .line 560
    :cond_1
    invoke-direct {p0}, Lfpd;->V()V

    .line 562
    :cond_2
    return-void
.end method
