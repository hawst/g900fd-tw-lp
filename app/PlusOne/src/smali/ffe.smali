.class public final Lffe;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final A:Ljava/lang/String;

.field private static final B:Ljava/lang/String;

.field private static final C:Ljava/lang/String;

.field private static final D:Ljava/lang/String;

.field private static final E:Ljava/lang/String;

.field private static final F:Ljava/lang/String;

.field private static final G:Ljava/lang/String;

.field private static final H:Ljava/lang/String;

.field private static final I:Ljava/lang/String;

.field private static final J:Ljava/lang/String;

.field private static final K:Ljava/lang/String;

.field private static L:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final M:[Ljava/lang/String;

.field private static final N:[Ljava/lang/String;

.field private static final O:[Ljava/lang/String;

.field private static final P:[Ljava/lang/String;

.field private static final Q:[Ljava/lang/String;

.field private static final R:[Ljava/lang/String;

.field private static S:Z

.field private static T:Z

.field private static U:I

.field private static V:I

.field private static final a:Landroid/net/Uri;

.field private static final b:Landroid/net/Uri;

.field private static final c:[Ljava/lang/String;

.field private static final d:[Ljava/lang/String;

.field private static final e:[Ljava/lang/String;

.field private static final f:[Ljava/lang/String;

.field private static final g:[Ljava/lang/String;

.field private static h:[Ljava/lang/String;

.field private static final i:[Ljava/lang/String;

.field private static final j:Ljava/lang/String;

.field private static final k:Ljava/lang/String;

.field private static final l:Ljava/lang/String;

.field private static final m:Ljava/lang/String;

.field private static final n:Ljava/lang/String;

.field private static final o:Ljava/lang/String;

.field private static final p:Ljava/lang/String;

.field private static final q:Ljava/lang/String;

.field private static final r:Ljava/lang/String;

.field private static final s:Ljava/lang/String;

.field private static final t:Ljava/lang/String;

.field private static final u:Ljava/lang/String;

.field private static final v:Ljava/lang/String;

.field private static final w:Ljava/lang/String;

.field private static final x:Ljava/lang/String;

.field private static final y:Ljava/lang/String;

.field private static final z:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v4, 0x2

    const/4 v3, 0x0

    .line 104
    sget-object v0, Landroid/provider/ContactsContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v1, "profile/raw_contacts"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lffe;->a:Landroid/net/Uri;

    .line 120
    sget-object v0, Landroid/provider/ContactsContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v1, "photo_dimensions"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lffe;->b:Landroid/net/Uri;

    .line 262
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "sourceid"

    aput-object v1, v0, v5

    const-string v1, "sync1"

    aput-object v1, v0, v4

    sput-object v0, Lffe;->c:[Ljava/lang/String;

    .line 270
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "name"

    aput-object v1, v0, v3

    const-string v1, "last_updated_time"

    aput-object v1, v0, v5

    const-string v1, "profile_proto"

    aput-object v1, v0, v4

    sput-object v0, Lffe;->d:[Ljava/lang/String;

    .line 281
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "person_id"

    aput-object v1, v0, v3

    const-string v1, "last_updated_time"

    aput-object v1, v0, v5

    const-string v1, "for_sharing"

    aput-object v1, v0, v4

    sput-object v0, Lffe;->e:[Ljava/lang/String;

    .line 291
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "person_id"

    aput-object v1, v0, v3

    const-string v1, "name"

    aput-object v1, v0, v5

    const-string v1, "contact_proto"

    aput-object v1, v0, v4

    sput-object v0, Lffe;->f:[Ljava/lang/String;

    .line 302
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "sourceid"

    aput-object v1, v0, v3

    const-string v1, "mimetype"

    aput-object v1, v0, v5

    const-string v1, "data_id"

    aput-object v1, v0, v4

    const-string v1, "data1"

    aput-object v1, v0, v6

    const-string v1, "data2"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "data3"

    aput-object v2, v0, v1

    sput-object v0, Lffe;->g:[Ljava/lang/String;

    .line 318
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "sync1"

    aput-object v1, v0, v5

    sput-object v0, Lffe;->h:[Ljava/lang/String;

    .line 326
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "mimetype"

    aput-object v1, v0, v3

    const-string v1, "_id"

    aput-object v1, v0, v5

    const-string v1, "data1"

    aput-object v1, v0, v4

    const-string v1, "data2"

    aput-object v1, v0, v6

    const-string v1, "data3"

    aput-object v1, v0, v7

    sput-object v0, Lffe;->i:[Ljava/lang/String;

    .line 340
    const-string v0, "1"

    sput-object v0, Lffe;->j:Ljava/lang/String;

    .line 341
    const-string v0, "2"

    sput-object v0, Lffe;->k:Ljava/lang/String;

    .line 342
    const-string v0, "3"

    sput-object v0, Lffe;->l:Ljava/lang/String;

    .line 343
    const-string v0, "0"

    sput-object v0, Lffe;->m:Ljava/lang/String;

    .line 345
    const-string v0, "1"

    sput-object v0, Lffe;->n:Ljava/lang/String;

    .line 346
    const-string v0, "2"

    sput-object v0, Lffe;->o:Ljava/lang/String;

    .line 347
    const-string v0, "3"

    sput-object v0, Lffe;->p:Ljava/lang/String;

    .line 348
    const-string v0, "0"

    sput-object v0, Lffe;->q:Ljava/lang/String;

    .line 350
    const-string v0, "1"

    sput-object v0, Lffe;->r:Ljava/lang/String;

    .line 351
    const-string v0, "3"

    sput-object v0, Lffe;->s:Ljava/lang/String;

    .line 352
    const-string v0, "7"

    sput-object v0, Lffe;->t:Ljava/lang/String;

    .line 353
    const-string v0, "5"

    sput-object v0, Lffe;->u:Ljava/lang/String;

    .line 354
    const-string v0, "4"

    sput-object v0, Lffe;->v:Ljava/lang/String;

    .line 355
    const-string v0, "2"

    sput-object v0, Lffe;->w:Ljava/lang/String;

    .line 356
    const-string v0, "6"

    sput-object v0, Lffe;->x:Ljava/lang/String;

    .line 357
    const-string v0, "13"

    sput-object v0, Lffe;->y:Ljava/lang/String;

    .line 358
    const-string v0, "10"

    sput-object v0, Lffe;->z:Ljava/lang/String;

    .line 359
    const-string v0, "19"

    sput-object v0, Lffe;->A:Ljava/lang/String;

    .line 360
    const-string v0, "9"

    sput-object v0, Lffe;->B:Ljava/lang/String;

    .line 361
    const-string v0, "14"

    sput-object v0, Lffe;->C:Ljava/lang/String;

    .line 362
    const-string v0, "11"

    sput-object v0, Lffe;->D:Ljava/lang/String;

    .line 363
    const-string v0, "8"

    sput-object v0, Lffe;->E:Ljava/lang/String;

    .line 364
    const-string v0, "15"

    sput-object v0, Lffe;->F:Ljava/lang/String;

    .line 365
    const-string v0, "16"

    sput-object v0, Lffe;->G:Ljava/lang/String;

    .line 366
    const-string v0, "17"

    sput-object v0, Lffe;->H:Ljava/lang/String;

    .line 367
    const-string v0, "18"

    sput-object v0, Lffe;->I:Ljava/lang/String;

    .line 368
    const-string v0, "12"

    sput-object v0, Lffe;->J:Ljava/lang/String;

    .line 369
    const-string v0, "0"

    sput-object v0, Lffe;->K:Ljava/lang/String;

    .line 378
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    .line 379
    sput-object v0, Lffe;->L:Landroid/util/SparseArray;

    sget-object v1, Lffe;->r:Ljava/lang/String;

    invoke-virtual {v0, v4, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 380
    sget-object v0, Lffe;->L:Landroid/util/SparseArray;

    sget-object v1, Lffe;->s:Ljava/lang/String;

    invoke-virtual {v0, v6, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 381
    sget-object v0, Lffe;->L:Landroid/util/SparseArray;

    sget-object v1, Lffe;->t:Ljava/lang/String;

    invoke-virtual {v0, v7, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 382
    sget-object v0, Lffe;->L:Landroid/util/SparseArray;

    const/4 v1, 0x5

    sget-object v2, Lffe;->u:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 383
    sget-object v0, Lffe;->L:Landroid/util/SparseArray;

    const/4 v1, 0x6

    sget-object v2, Lffe;->v:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 384
    sget-object v0, Lffe;->L:Landroid/util/SparseArray;

    const/4 v1, 0x7

    sget-object v2, Lffe;->w:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 385
    sget-object v0, Lffe;->L:Landroid/util/SparseArray;

    const/16 v1, 0x8

    sget-object v2, Lffe;->x:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 386
    sget-object v0, Lffe;->L:Landroid/util/SparseArray;

    const/16 v1, 0x9

    sget-object v2, Lffe;->y:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 387
    sget-object v0, Lffe;->L:Landroid/util/SparseArray;

    const/16 v1, 0xa

    sget-object v2, Lffe;->z:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 388
    sget-object v0, Lffe;->L:Landroid/util/SparseArray;

    const/16 v1, 0xb

    sget-object v2, Lffe;->A:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 389
    sget-object v0, Lffe;->L:Landroid/util/SparseArray;

    const/16 v1, 0xc

    sget-object v2, Lffe;->B:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 390
    sget-object v0, Lffe;->L:Landroid/util/SparseArray;

    const/16 v1, 0xd

    sget-object v2, Lffe;->C:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 391
    sget-object v0, Lffe;->L:Landroid/util/SparseArray;

    const/16 v1, 0xe

    sget-object v2, Lffe;->D:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 392
    sget-object v0, Lffe;->L:Landroid/util/SparseArray;

    const/16 v1, 0xf

    sget-object v2, Lffe;->E:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 393
    sget-object v0, Lffe;->L:Landroid/util/SparseArray;

    const/16 v1, 0x10

    sget-object v2, Lffe;->F:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 394
    sget-object v0, Lffe;->L:Landroid/util/SparseArray;

    const/16 v1, 0x11

    sget-object v2, Lffe;->G:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 395
    sget-object v0, Lffe;->L:Landroid/util/SparseArray;

    const/16 v1, 0x12

    sget-object v2, Lffe;->H:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 396
    sget-object v0, Lffe;->L:Landroid/util/SparseArray;

    const/16 v1, 0x13

    sget-object v2, Lffe;->I:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 397
    sget-object v0, Lffe;->L:Landroid/util/SparseArray;

    const/16 v1, 0x14

    sget-object v2, Lffe;->J:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 406
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "sourceid"

    aput-object v1, v0, v5

    const-string v1, "title"

    aput-object v1, v0, v4

    sput-object v0, Lffe;->M:[Ljava/lang/String;

    .line 427
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "sourceid"

    aput-object v1, v0, v5

    const-string v1, "sync2"

    aput-object v1, v0, v4

    sput-object v0, Lffe;->N:[Ljava/lang/String;

    .line 443
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "gaia_id"

    aput-object v1, v0, v3

    const-string v1, "avatar"

    aput-object v1, v0, v5

    sput-object v0, Lffe;->O:[Ljava/lang/String;

    .line 453
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "sourceid"

    aput-object v1, v0, v5

    const-string v1, "data_id"

    aput-object v1, v0, v4

    const-string v1, "mimetype"

    aput-object v1, v0, v6

    const-string v1, "sync3"

    aput-object v1, v0, v7

    sput-object v0, Lffe;->P:[Ljava/lang/String;

    .line 479
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "account_type"

    aput-object v1, v0, v5

    const-string v1, "account_name"

    aput-object v1, v0, v4

    const-string v1, "data_set"

    aput-object v1, v0, v6

    const-string v1, "sourceid"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "sync2"

    aput-object v2, v0, v1

    sput-object v0, Lffe;->Q:[Ljava/lang/String;

    .line 495
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "raw_contact_id"

    aput-object v1, v0, v5

    const-string v1, "data5"

    aput-object v1, v0, v4

    const-string v1, "data4"

    aput-object v1, v0, v6

    const-string v1, "data3"

    aput-object v1, v0, v7

    sput-object v0, Lffe;->R:[Ljava/lang/String;

    .line 545
    sput v3, Lffe;->V:I

    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;)I
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 2277
    const/16 v6, 0x60

    .line 2278
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2279
    sget-object v1, Lffe;->b:Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2281
    if-eqz v1, :cond_1

    .line 2283
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2284
    invoke-interface {v1, p1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 2287
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2290
    :goto_1
    return v0

    .line 2287
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    move v0, v6

    goto :goto_0

    :cond_1
    move v0, v6

    goto :goto_1
.end method

.method private static a(Ljava/lang/String;)I
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2294
    if-nez p0, :cond_1

    move v0, v1

    .line 2303
    :cond_0
    :goto_0
    return v0

    .line 2298
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 2299
    if-eqz v0, :cond_2

    if-ne v0, v1, :cond_0

    .line 2300
    :cond_2
    const/4 v0, 0x2

    goto :goto_0
.end method

.method private static a(Lffj;Ljava/lang/String;Ljava/lang/String;)Lffi;
    .locals 2

    .prologue
    .line 1468
    new-instance v0, Lffi;

    invoke-direct {v0}, Lffi;-><init>()V

    .line 1469
    iput-object p1, v0, Lffi;->d:Ljava/lang/String;

    .line 1470
    iput-object p2, v0, Lffi;->e:Ljava/lang/String;

    .line 1471
    iget-object v1, p0, Lffj;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1472
    return-object v0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, -0x1

    .line 668
    invoke-static {p0}, Ldhv;->g(Landroid/content/Context;)I

    move-result v0

    .line 669
    const/4 v1, 0x0

    sput v1, Lffe;->V:I

    .line 670
    invoke-static {p0}, Lffe;->f(Landroid/content/Context;)Z

    move-result v1

    .line 671
    if-nez v0, :cond_1

    if-eqz v1, :cond_1

    .line 672
    invoke-static {p0, v4}, Ldhv;->s(Landroid/content/Context;I)V

    .line 674
    invoke-static {p0}, Ldhv;->a(Landroid/content/Context;)I

    move-result v0

    .line 675
    if-eq v0, v3, :cond_0

    .line 676
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->a(Landroid/content/Context;I)V

    .line 686
    :cond_0
    :goto_0
    return-void

    .line 678
    :cond_1
    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    if-nez v1, :cond_0

    .line 679
    invoke-static {p0, v4}, Ldhv;->s(Landroid/content/Context;I)V

    .line 681
    invoke-static {p0}, Ldhv;->a(Landroid/content/Context;)I

    move-result v0

    .line 682
    if-eq v0, v3, :cond_0

    .line 683
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->a(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2358
    invoke-static {p0}, Lffe;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2368
    :goto_0
    return-void

    .line 2362
    :cond_0
    invoke-static {p0, v1}, Ldhv;->a(Landroid/content/Context;Z)V

    .line 2363
    invoke-static {p0, v1}, Ldhv;->b(Landroid/content/Context;Z)V

    .line 2364
    new-instance v0, Lkfp;

    invoke-direct {v0}, Lkfp;-><init>()V

    .line 2365
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lkfp;->a(Z)V

    .line 2366
    invoke-static {p0, p1, v0}, Lffe;->c(Landroid/content/Context;ILkfp;)V

    .line 2367
    invoke-static {p0}, Lffe;->b(Landroid/content/Context;)V

    goto :goto_0
.end method

.method protected static a(Landroid/content/Context;ILffg;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2078
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 2079
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2080
    invoke-static {v0}, Lffe;->c(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iget-wide v2, p2, Lffg;->a:J

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 2082
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 2083
    const-string v2, "sync2"

    iget v3, p2, Lffg;->d:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2084
    const-string v2, "sync3"

    iget v3, p2, Lffg;->d:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2085
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 2086
    invoke-virtual {v2, v0, v1, v4, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2087
    return-void
.end method

.method private static a(Landroid/content/Context;ILjava/lang/String;J[B)V
    .locals 15

    .prologue
    .line 1215
    invoke-static {p0}, Lffe;->c(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1301
    :cond_0
    :goto_0
    return-void

    .line 1220
    :cond_1
    const-class v2, Lhei;

    invoke-static {p0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lhei;

    move/from16 v0, p1

    invoke-interface {v2, v0}, Lhei;->a(I)Lhej;

    move-result-object v2

    .line 1221
    const-string v3, "account_name"

    invoke-interface {v2, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1222
    const-string v3, "gaia_id"

    invoke-interface {v2, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1223
    invoke-static {v2}, Ljpu;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1225
    const/4 v8, 0x0

    .line 1226
    new-instance v11, Lffj;

    invoke-direct {v11}, Lffj;-><init>()V

    .line 1227
    const/4 v3, 0x1

    iput-boolean v3, v11, Lffj;->e:Z

    .line 1228
    iput-object v2, v11, Lffj;->b:Ljava/lang/String;

    .line 1229
    move-object/from16 v0, p2

    iput-object v0, v11, Lffj;->d:Ljava/lang/String;

    .line 1230
    move-wide/from16 v0, p3

    iput-wide v0, v11, Lffj;->c:J

    .line 1232
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 1233
    invoke-static {v10}, Lffe;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 1235
    sget-object v4, Lffe;->h:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    .line 1237
    if-eqz v4, :cond_0

    .line 1242
    :try_start_0
    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_f

    .line 1243
    const/4 v3, 0x0

    invoke-interface {v4, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    iput-wide v6, v11, Lffj;->a:J

    .line 1244
    iget-wide v6, v11, Lffj;->c:J

    const/4 v3, 0x1

    .line 1245
    invoke-interface {v4, v3}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-wide v8

    cmp-long v3, v6, v8

    if-nez v3, :cond_4

    const/4 v3, 0x1

    .line 1248
    :goto_1
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 1251
    iget-wide v4, v11, Lffj;->a:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_2

    if-nez v3, :cond_7

    .line 1252
    :cond_2
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 1253
    move-object/from16 v0, p2

    iput-object v0, v11, Lffj;->d:Ljava/lang/String;

    .line 1255
    if-eqz p5, :cond_3

    .line 1258
    :try_start_1
    new-instance v3, Lnjt;

    invoke-direct {v3}, Lnjt;-><init>()V

    move-object/from16 v0, p5

    invoke-static {v3, v0}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v3

    check-cast v3, Lnjt;

    .line 1259
    iget-object v4, v3, Lnjt;->d:Lnib;

    if-eqz v4, :cond_3

    iget-object v4, v3, Lnjt;->d:Lnib;

    iget-object v4, v4, Lnib;->c:Lnif;

    if-eqz v4, :cond_3

    .line 1260
    iget-object v3, v3, Lnjt;->d:Lnib;

    iget-object v3, v3, Lnib;->c:Lnif;

    invoke-static {v11, v3}, Lffe;->a(Lffj;Lnif;)V
    :try_end_1
    .catch Loxt; {:try_start_1 .. :try_end_1} :catch_0

    .line 1267
    :cond_3
    :goto_2
    iget-wide v4, v11, Lffj;->a:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-eqz v3, :cond_6

    .line 1268
    iget-wide v4, v11, Lffj;->a:J

    sget-object v3, Lffe;->a:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "data"

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "account_type"

    const-string v5, "com.google"

    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "data_set"

    const-string v5, "plus"

    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "account_name"

    invoke-virtual {v3, v4, v10}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "caller_is_syncadapter"

    const-string v5, "true"

    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lffe;->i:[Ljava/lang/String;

    const-string v5, "mimetype IN (\'vnd.android.cursor.item/email_v2\',\'vnd.android.cursor.item/phone_v2\',\'vnd.android.cursor.item/postal-address_v2\',\'vnd.android.cursor.item/group_membership\')"

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 1270
    if-eqz v13, :cond_0

    .line 1275
    :goto_3
    :try_start_2
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1276
    const/4 v2, 0x1

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const/4 v2, 0x0

    .line 1277
    invoke-interface {v13, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v2, 0x2

    .line 1278
    invoke-interface {v13, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v2, 0x3

    .line 1279
    invoke-interface {v13, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v2, 0x4

    .line 1280
    invoke-interface {v13, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    move-object v3, v11

    .line 1276
    invoke-static/range {v3 .. v9}, Lffe;->a(Lffj;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3

    .line 1283
    :catchall_0
    move-exception v2

    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    throw v2

    .line 1245
    :cond_4
    const/4 v3, 0x0

    goto/16 :goto_1

    .line 1248
    :catchall_1
    move-exception v2

    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    throw v2

    .line 1262
    :catch_0
    move-exception v3

    .line 1263
    const-string v4, "GooglePlusContactsSync"

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x34

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Unable to deserialize simple profile from database: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 1283
    :cond_5
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 1287
    :cond_6
    iget-wide v2, v11, Lffj;->a:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_a

    invoke-static {v10}, Lffe;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1289
    :goto_4
    const/4 v3, 0x1

    invoke-static {p0, v2, v12, v11, v3}, Lffe;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/util/ArrayList;Lffj;Z)V

    .line 1290
    const/4 v2, 0x1

    invoke-static {p0, v12, v2}, Lffe;->a(Landroid/content/Context;Ljava/util/ArrayList;Z)[Landroid/content/ContentProviderResult;

    move-result-object v2

    .line 1291
    iget-wide v4, v11, Lffj;->a:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-nez v3, :cond_7

    if-eqz v2, :cond_7

    array-length v3, v2

    if-lez v3, :cond_7

    .line 1292
    const/4 v3, 0x0

    aget-object v2, v2, v3

    iget-object v2, v2, Landroid/content/ContentProviderResult;->uri:Landroid/net/Uri;

    .line 1293
    invoke-static {v2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v2

    iput-wide v2, v11, Lffj;->a:J

    .line 1297
    :cond_7
    iget-wide v2, v11, Lffj;->a:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 1299
    invoke-static {v10}, Lffe;->c(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-wide v4, v11, Lffj;->a:J

    invoke-static {v2, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    .line 1298
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v4, Lffe;->Q:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    if-eqz v7, :cond_0

    const-wide/16 v8, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    :try_start_3
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v10

    if-eqz v10, :cond_8

    const/4 v2, 0x2

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v2, 0x1

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v2, 0x3

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x0

    invoke-interface {v7, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    const/4 v5, 0x4

    invoke-interface {v7, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v5, 0x5

    invoke-interface {v7, v5}, Landroid/database/Cursor;->getInt(I)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-result v5

    :cond_8
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    const-wide/16 v10, 0x0

    cmp-long v7, v8, v10

    if-eqz v7, :cond_0

    invoke-static {v6}, Ljpu;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_0

    const-string v7, "com.google"

    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    const-string v3, "plus"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    :cond_9
    const-string v2, "GooglePlusContactsSync"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "GooglePlusContactsSync"

    const-string v3, "Cannot refresh raw contact. It is not a Google+ contact."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1288
    :cond_a
    invoke-static {v10}, Lffe;->c(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    goto/16 :goto_4

    .line 1298
    :catchall_2
    move-exception v2

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v2

    :cond_b
    const-class v2, Lhei;

    invoke-static {p0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lhei;

    const/4 v3, -0x1

    move/from16 v0, p1

    if-ne v0, v3, :cond_c

    invoke-interface {v2, v4}, Lhei;->a(Ljava/lang/String;)I

    move-result p1

    const/4 v3, -0x1

    move/from16 v0, p1

    if-eq v0, v3, :cond_c

    move/from16 v0, p1

    invoke-interface {v2, v0}, Lhei;->a(I)Lhej;

    move-result-object v3

    invoke-interface {v3}, Lhej;->a()Z

    move-result v3

    if-nez v3, :cond_c

    const/16 p1, -0x1

    :cond_c
    const/4 v3, -0x1

    move/from16 v0, p1

    if-ne v0, v3, :cond_d

    const-string v2, "GooglePlusContactsSync"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "GooglePlusContactsSync"

    const-string v3, "Cannot refresh raw contact. It does not belong to a logged in G+ account"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_d
    move/from16 v0, p1

    invoke-interface {v2, v0}, Lhei;->a(I)Lhej;

    move-result-object v2

    const-string v3, "gaia_id"

    invoke-interface {v2, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_e

    const/4 v2, 0x0

    move/from16 v0, p1

    invoke-static {p0, v0, v2}, Lffe;->f(Landroid/content/Context;ILkfp;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_e
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    new-instance v3, Lffg;

    invoke-direct {v3}, Lffg;-><init>()V

    iput-object v6, v3, Lffg;->b:Ljava/lang/String;

    iput-wide v8, v3, Lffg;->a:J

    iput v5, v3, Lffg;->d:I

    invoke-virtual {v2, v6, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v3, Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move/from16 v0, p1

    invoke-static {p0, v0, v2, v3}, Lffe;->a(Landroid/content/Context;ILjava/util/HashMap;Ljava/util/ArrayList;)V

    invoke-virtual {v2}, Ljava/util/HashMap;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    new-instance v3, Lkfp;

    invoke-direct {v3}, Lkfp;-><init>()V

    const-string v4, "Large avatar sync for a single raw contact"

    invoke-virtual {v3, v4}, Lkfp;->b(Ljava/lang/String;)V

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lkfp;->a(Z)V

    const-string v4, "display_max_dim"

    invoke-static {p0, v4}, Lffe;->a(Landroid/content/Context;Ljava/lang/String;)I

    move-result v4

    move/from16 v0, p1

    invoke-static {p0, v0, v3, v2, v4}, Lffe;->a(Landroid/content/Context;ILkfp;Ljava/util/HashMap;I)V

    invoke-virtual {v3}, Lkfp;->g()V

    goto/16 :goto_0

    :cond_f
    move v3, v8

    goto/16 :goto_1
.end method

.method private static a(Landroid/content/Context;ILjava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lffj;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 1692
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 1693
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1694
    invoke-static {v0}, Lffe;->c(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1695
    invoke-virtual {p3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lffj;

    .line 1696
    const-string v3, "GooglePlusContactsSync"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1697
    invoke-static {v0}, Lffe;->a(Lffj;)V

    .line 1700
    :cond_0
    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    .line 1701
    invoke-virtual {v3, v9}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "_id=?"

    new-array v5, v9, [Ljava/lang/String;

    iget-wide v6, v0, Lffj;->a:J

    .line 1703
    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v8

    .line 1702
    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    .line 1704
    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    .line 1700
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1707
    :cond_1
    invoke-static {p0, p2, v8}, Lffe;->a(Landroid/content/Context;Ljava/util/ArrayList;Z)[Landroid/content/ContentProviderResult;

    .line 1708
    return-void
.end method

.method private static a(Landroid/content/Context;ILjava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/HashMap;ZLjava/util/HashMap;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lffj;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lffj;",
            ">;Z",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1310
    const-class v1, Lhei;

    invoke-static {p0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhei;

    .line 1311
    invoke-interface {v1, p1}, Lhei;->a(I)Lhej;

    move-result-object v1

    const-string v2, "account_name"

    invoke-interface {v1, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1312
    invoke-static {v1}, Lffe;->c(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    .line 1313
    invoke-static {v1}, Lffe;->d(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    .line 1314
    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->d:Landroid/net/Uri;

    invoke-static {v1, p1}, Lcom/google/android/apps/plus/content/EsProvider;->a(Landroid/net/Uri;I)Landroid/net/Uri;

    move-result-object v2

    .line 1315
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v8

    .line 1316
    const/4 v7, 0x0

    move v4, v7

    .line 1317
    :goto_0
    if-ge v4, v8, :cond_5

    .line 1318
    add-int/lit8 v1, v4, 0x20

    .line 1319
    if-le v1, v8, :cond_7

    move v7, v8

    .line 1323
    :goto_1
    sub-int v1, v7, v4

    new-array v5, v1, [Ljava/lang/String;

    .line 1324
    const/4 v1, 0x0

    move v3, v1

    :goto_2
    array-length v1, v5

    if-ge v3, v1, :cond_0

    .line 1325
    add-int v1, v4, v3

    invoke-virtual {p3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lffj;

    iget-object v1, v1, Lffj;->b:Ljava/lang/String;

    aput-object v1, v5, v3

    .line 1324
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    .line 1328
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "person_id IN ("

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    :goto_3
    array-length v3, v5

    if-ge v1, v3, :cond_2

    if-eqz v1, :cond_1

    const/16 v3, 0x2c

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_1
    const/16 v3, 0x3f

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_2
    const/16 v1, 0x29

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v3, Lffe;->f:[Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    :cond_3
    :goto_4
    :try_start_0
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x0

    invoke-interface {v3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p4, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lffj;

    const/4 v4, 0x1

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lffj;->d:Ljava/lang/String;

    const/4 v4, 0x2

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-static {v4}, Ldsm;->a([B)Ldsv;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-static {p0, v1, v4}, Lffe;->a(Landroid/content/Context;Lffj;Ldsv;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_4

    :catchall_0
    move-exception v1

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    throw v1

    :cond_4
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 1329
    move-object/from16 v0, p6

    invoke-static {v5, p4, v0}, Lffe;->a([Ljava/lang/String;Ljava/util/HashMap;Ljava/util/HashMap;)V

    .line 1331
    if-nez p5, :cond_6

    invoke-static {p0, v10, v5, p4}, Lffe;->a(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/util/HashMap;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 1338
    :cond_5
    return-void

    .line 1334
    :cond_6
    invoke-static {p0, v9, p2, v5, p4}, Lffe;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/util/ArrayList;[Ljava/lang/String;Ljava/util/HashMap;)V

    .line 1335
    const/4 v1, 0x0

    invoke-static {p0, p2, v1}, Lffe;->a(Landroid/content/Context;Ljava/util/ArrayList;Z)[Landroid/content/ContentProviderResult;

    move v4, v7

    .line 1337
    goto/16 :goto_0

    :cond_7
    move v7, v1

    goto/16 :goto_1
.end method

.method private static a(Landroid/content/Context;ILjava/util/HashMap;)V
    .locals 14
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lffh;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 855
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 856
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 857
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 858
    invoke-static {v2}, Lffe;->e(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 859
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 860
    invoke-virtual/range {p2 .. p2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lffh;

    .line 861
    const-string v1, "GooglePlusContactsSync"

    const/4 v7, 0x3

    invoke-static {v1, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 862
    iget-boolean v1, v0, Lffh;->d:Z

    if-nez v1, :cond_1

    const-string v1, "[DELETE]"

    :goto_1
    iget-object v7, v0, Lffh;->b:Ljava/lang/String;

    iget-object v8, v0, Lffh;->c:Ljava/lang/String;

    iget-wide v10, v0, Lffh;->a:J

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    add-int/lit8 v12, v12, 0x22

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v13

    add-int/2addr v12, v13

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v13

    add-int/2addr v12, v13

    invoke-direct {v9, v12}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v9, " "

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, " "

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, " (group_id="

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, ")"

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 865
    :cond_0
    iget-boolean v1, v0, Lffh;->d:Z

    if-eqz v1, :cond_3

    iget-wide v8, v0, Lffh;->a:J

    const-wide/16 v10, 0x0

    cmp-long v1, v8, v10

    if-nez v1, :cond_3

    .line 866
    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const-string v7, "sourceid"

    iget-object v8, v0, Lffh;->b:Ljava/lang/String;

    .line 867
    invoke-virtual {v1, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const-string v7, "title"

    iget-object v0, v0, Lffh;->c:Ljava/lang/String;

    .line 868
    invoke-virtual {v1, v7, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "group_is_read_only"

    const/4 v7, 0x1

    .line 869
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v0, v1, v7}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    .line 870
    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    .line 866
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 862
    :cond_1
    iget-wide v8, v0, Lffh;->a:J

    const-wide/16 v10, 0x0

    cmp-long v1, v8, v10

    if-nez v1, :cond_2

    const-string v1, "[INSERT]"

    goto/16 :goto_1

    :cond_2
    const-string v1, "[UPDATE]"

    goto/16 :goto_1

    .line 871
    :cond_3
    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const-string v7, "_id=?"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    iget-wide v10, v0, Lffh;->a:J

    .line 880
    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v8, v9

    .line 879
    invoke-virtual {v1, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    .line 881
    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    .line 878
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 885
    :cond_4
    const/4 v0, 0x1

    invoke-static {p0, v5, v0}, Lffe;->a(Landroid/content/Context;Ljava/util/ArrayList;Z)[Landroid/content/ContentProviderResult;

    .line 889
    invoke-virtual/range {p2 .. p2}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 890
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 891
    const-string v1, "sync1"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 892
    invoke-static {v2}, Lffe;->c(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v4, 0x0

    invoke-virtual {v3, v1, v0, v2, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 894
    :cond_5
    return-void
.end method

.method protected static a(Landroid/content/Context;ILjava/util/HashMap;Ljava/util/ArrayList;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lffg;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1977
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v4, v1, [Ljava/lang/String;

    .line 1978
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 1979
    const-string v1, "gaia_id IN ("

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v1, v0

    .line 1980
    :goto_0
    array-length v0, v4

    if-ge v1, v0, :cond_1

    .line 1981
    if-eqz v1, :cond_0

    .line 1982
    const/16 v0, 0x2c

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1984
    :cond_0
    const/16 v0, 0x3f

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1985
    invoke-virtual {p3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    .line 1980
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1987
    :cond_1
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1988
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->d:Landroid/net/Uri;

    .line 1989
    invoke-static {v1, p1}, Lcom/google/android/apps/plus/content/EsProvider;->a(Landroid/net/Uri;I)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lffe;->O:[Ljava/lang/String;

    .line 1990
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    .line 1988
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1991
    if-nez v1, :cond_2

    .line 2010
    :goto_1
    return-void

    .line 1996
    :cond_2
    :goto_2
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1997
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1998
    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1999
    invoke-static {v3}, Lffe;->a(Ljava/lang/String;)I

    move-result v4

    .line 2000
    invoke-virtual {p2, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lffg;

    .line 2001
    if-eqz v0, :cond_3

    iget v5, v0, Lffg;->d:I

    if-ne v5, v4, :cond_4

    .line 2002
    :cond_3
    invoke-virtual {p2, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 2009
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 2004
    :cond_4
    :try_start_1
    iput v4, v0, Lffg;->d:I

    .line 2005
    iput-object v3, v0, Lffg;->c:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 2009
    :cond_5
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1
.end method

.method private static a(Landroid/content/Context;ILjava/util/HashMap;Ljava/util/ArrayList;Ljava/util/HashMap;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lffj;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1091
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1092
    invoke-virtual {p2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lffj;

    .line 1093
    iget-boolean v2, v0, Lffj;->e:Z

    if-eqz v2, :cond_0

    iget-wide v4, v0, Lffj;->a:J

    const-wide/16 v6, 0x0

    cmp-long v2, v4, v6

    if-nez v2, :cond_0

    .line 1094
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1098
    :cond_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1099
    const/4 v5, 0x1

    move-object v0, p0

    move v1, p1

    move-object v2, p3

    move-object v4, p2

    move-object v6, p4

    invoke-static/range {v0 .. v6}, Lffe;->a(Landroid/content/Context;ILjava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/HashMap;ZLjava/util/HashMap;)V

    .line 1101
    :cond_2
    return-void
.end method

.method public static a(Landroid/content/Context;ILkfp;)V
    .locals 9

    .prologue
    const/16 v1, 0x2a

    const/16 v0, 0xd

    const/4 v7, 0x0

    .line 552
    invoke-static {p0}, Lffe;->c(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p2}, Lkfp;->c()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 586
    :cond_0
    :goto_0
    return-void

    .line 556
    :cond_1
    const-string v2, "Android contacts sync"

    invoke-virtual {p2, v2}, Lkfp;->c(Ljava/lang/String;)V

    .line 558
    :try_start_0
    invoke-static {p0}, Ldsm;->a(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 559
    invoke-static {p0}, Ldsm;->b(Landroid/content/Context;)V

    .line 560
    const/4 v0, 0x1

    invoke-static {p0, v0}, Ldsm;->b(Landroid/content/Context;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 585
    :cond_2
    :goto_1
    invoke-virtual {p2}, Lkfp;->f()V

    goto :goto_0

    .line 565
    :cond_3
    const/4 v2, 0x0

    :try_start_1
    invoke-static {p0, v2}, Ldsm;->b(Landroid/content/Context;Z)V

    .line 567
    invoke-static {p0, p2}, Lffe;->a(Landroid/content/Context;Lkfp;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 568
    const/4 v2, -0x1

    if-ne p1, v2, :cond_4

    .line 585
    invoke-virtual {p2}, Lkfp;->f()V

    goto :goto_0

    .line 572
    :cond_4
    :try_start_2
    invoke-static {p0}, Lffe;->c(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-static {p0, p1}, Ldhv;->j(Landroid/content/Context;I)Z

    move-result v2

    if-nez v2, :cond_5

    invoke-static {p0, p1, p2}, Lffe;->d(Landroid/content/Context;ILkfp;)V

    invoke-static {p0, p1, p2}, Lffe;->e(Landroid/content/Context;ILkfp;)V

    .line 573
    :cond_5
    invoke-static {p0}, Lffe;->c(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {p2}, Lkfp;->c()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 574
    :cond_6
    :goto_2
    invoke-static {p0}, Lffe;->e(Landroid/content/Context;)V

    .line 575
    invoke-static {p0}, Lffe;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p2}, Lkfp;->c()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 577
    :cond_7
    :goto_3
    invoke-static {p0, p1, p2}, Lffe;->f(Landroid/content/Context;ILkfp;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 578
    invoke-static {p0, p1, p2}, Lffe;->f(Landroid/content/Context;ILkfp;)Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v0, "Android:Circles"

    invoke-virtual {p2, v0}, Lkfp;->c(Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Ldsm;->b(Landroid/content/Context;IZ)V

    invoke-static {p0, p1}, Lffe;->b(Landroid/content/Context;I)Ljava/util/HashMap;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-static {p0, p1, v0}, Lffe;->a(Landroid/content/Context;ILjava/util/HashMap;)V

    :cond_8
    if-nez v0, :cond_f

    move v0, v7

    :goto_4
    invoke-virtual {p2, v0}, Lkfp;->m(I)V

    .line 579
    :cond_9
    invoke-static {p0, p1, p2}, Lffe;->f(Landroid/content/Context;ILkfp;)Z

    move-result v0

    if-eqz v0, :cond_12

    const-string v0, "Android:Contacts"

    invoke-virtual {p2, v0}, Lkfp;->c(Ljava/lang/String;)V

    invoke-static {p0, p1, p2}, Ldsm;->a(Landroid/content/Context;ILkfp;)Z

    move-result v0

    if-nez v0, :cond_10

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Could not sync contacts from server"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 585
    :catchall_0
    move-exception v0

    invoke-virtual {p2}, Lkfp;->f()V

    throw v0

    .line 573
    :cond_a
    :try_start_3
    invoke-static {p0, p1}, Ldhv;->h(Landroid/content/Context;I)I

    move-result v2

    if-ge v2, v0, :cond_18

    invoke-static {p0, p1, p2}, Lffe;->c(Landroid/content/Context;ILkfp;)V

    invoke-static {p0, p1, p2}, Lffe;->d(Landroid/content/Context;ILkfp;)V

    invoke-static {p0, p1, p2}, Lffe;->e(Landroid/content/Context;ILkfp;)V

    const/16 v2, 0xd

    invoke-static {p0, p1, v2}, Ldhv;->a(Landroid/content/Context;II)V

    :goto_5
    if-ge v0, v1, :cond_b

    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v2, "account_name"

    invoke-interface {v0, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "Android:DeleteStreamItems"

    invoke-virtual {p2, v2}, Lkfp;->c(Ljava/lang/String;)V

    sget-object v2, Landroid/provider/ContactsContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v3, "stream_items"

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "account_type"

    const-string v4, "com.google"

    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "data_set"

    const-string v4, "plus"

    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "account_name"

    invoke-virtual {v2, v3, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "caller_is_syncadapter"

    const-string v3, "true"

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v0, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p2, v0}, Lkfp;->l(I)V

    invoke-virtual {p2}, Lkfp;->f()V

    const/16 v0, 0x2a

    invoke-static {p0, p1, v0}, Ldhv;->a(Landroid/content/Context;II)V

    move v0, v1

    :cond_b
    const/16 v1, 0x2c

    if-ge v0, v1, :cond_6

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v0, "sync1"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v2, "account_name"

    invoke-interface {v0, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {v0}, Lffe;->c(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v1, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {v0}, Lffe;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v0, v1, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :goto_6
    const/16 v0, 0x2c

    :try_start_5
    invoke-static {p0, p1, v0}, Ldhv;->a(Landroid/content/Context;II)V

    goto/16 :goto_2

    :catch_0
    move-exception v0

    const-string v1, "GooglePlusContactsSync"

    const-string v2, "Cannot update contacts"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_6

    .line 575
    :cond_c
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "is_plus_page"

    invoke-interface {v0, v1}, Lhej;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_d

    invoke-static {p0, p1, p2}, Lffe;->c(Landroid/content/Context;ILkfp;)V

    goto/16 :goto_3

    :cond_d
    const-string v1, "gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljpu;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Android:MyProfile"

    invoke-virtual {p2, v1}, Lkfp;->c(Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->e:Landroid/net/Uri;

    invoke-static {v1, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/apps/plus/content/EsProvider;->a(Landroid/net/Uri;I)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lffe;->d:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v8

    :try_start_6
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    invoke-interface {v8, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_e

    const/4 v0, 0x2

    invoke-interface {v8, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_e

    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v0, 0x1

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const/4 v0, 0x2

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v6

    move-object v1, p0

    move v2, p1

    invoke-static/range {v1 .. v6}, Lffe;->a(Landroid/content/Context;ILjava/lang/String;J[B)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :cond_e
    :try_start_7
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    invoke-virtual {p2}, Lkfp;->f()V

    goto/16 :goto_3

    :catchall_1
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0

    .line 578
    :cond_f
    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    goto/16 :goto_4

    .line 579
    :cond_10
    invoke-static {p0, p1}, Lffe;->c(Landroid/content/Context;I)Ljava/util/HashMap;

    move-result-object v0

    invoke-static {p0, p1}, Lffe;->d(Landroid/content/Context;I)Ljava/util/HashMap;

    move-result-object v1

    if-eqz v0, :cond_11

    if-eqz v1, :cond_11

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-static {p0, p1, v0, v2}, Lffe;->b(Landroid/content/Context;ILjava/util/HashMap;Ljava/util/ArrayList;)V

    invoke-static {p0, p1, v0, v2, v1}, Lffe;->a(Landroid/content/Context;ILjava/util/HashMap;Ljava/util/ArrayList;Ljava/util/HashMap;)V

    invoke-static {p0, p1, v0, v2, v1}, Lffe;->b(Landroid/content/Context;ILjava/util/HashMap;Ljava/util/ArrayList;Ljava/util/HashMap;)V

    const/4 v1, 0x1

    invoke-static {p0, v2, v1}, Lffe;->a(Landroid/content/Context;Ljava/util/ArrayList;Z)[Landroid/content/ContentProviderResult;

    :cond_11
    if-nez v0, :cond_16

    move v0, v7

    :goto_7
    invoke-virtual {p2, v0}, Lkfp;->m(I)V

    .line 580
    :cond_12
    invoke-static {p0, p1, p2}, Lffe;->f(Landroid/content/Context;ILkfp;)Z

    move-result v0

    if-eqz v0, :cond_15

    const-string v0, "Android:LargeAvatars"

    invoke-virtual {p2, v0}, Lkfp;->c(Ljava/lang/String;)V

    const-string v0, "display_max_dim"

    invoke-static {p0, v0}, Lffe;->a(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v2, "account_name"

    invoke-interface {v0, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "large.avatar.size"

    const/16 v4, 0x100

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    if-eq v1, v3, :cond_13

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "sync2"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {v0}, Lffe;->c(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v4, v0, v3, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    :cond_13
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "large.avatar.size"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    invoke-static {p0, p1}, Lffe;->e(Landroid/content/Context;I)Ljava/util/HashMap;

    move-result-object v0

    if-eqz v0, :cond_14

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_17

    :cond_14
    invoke-virtual {p2}, Lkfp;->f()V

    .line 581
    :cond_15
    :goto_8
    invoke-static {p0, p1, p2}, Lffe;->b(Landroid/content/Context;ILkfp;)V

    goto/16 :goto_1

    .line 579
    :cond_16
    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    goto :goto_7

    .line 580
    :cond_17
    invoke-static {p0, p1, p2, v0, v1}, Lffe;->a(Landroid/content/Context;ILkfp;Ljava/util/HashMap;I)V

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    invoke-virtual {p2, v0}, Lkfp;->m(I)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_8

    :cond_18
    move v0, v2

    goto/16 :goto_5
.end method

.method private static a(Landroid/content/Context;ILkfp;Ljava/util/HashMap;I)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Lkfp;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lffg;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 2018
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 2019
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 2020
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v12

    .line 2021
    new-instance v13, Ljava/util/ArrayList;

    invoke-virtual/range {p3 .. p3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-direct {v13, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 2022
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v8

    .line 2023
    const/4 v1, 0x0

    .line 2024
    :goto_0
    if-ge v1, v8, :cond_4

    invoke-virtual/range {p2 .. p2}, Lkfp;->c()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2025
    add-int/lit8 v0, v1, 0x8

    .line 2026
    if-le v0, v8, :cond_5

    move v7, v8

    :goto_1
    move v10, v1

    .line 2030
    :goto_2
    if-ge v10, v7, :cond_3

    .line 2031
    invoke-virtual {v13, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lffg;

    .line 2032
    iget v0, v6, Lffg;->d:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 2033
    invoke-static {p0, p1, v6}, Lffe;->a(Landroid/content/Context;ILffg;)V

    .line 2030
    :cond_0
    :goto_3
    add-int/lit8 v0, v10, 0x1

    move v10, v0

    goto :goto_2

    .line 2034
    :cond_1
    iget-object v0, v6, Lffg;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2035
    const-class v0, Lizs;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizs;

    .line 2036
    const/4 v9, 0x0

    .line 2038
    :try_start_0
    iget-object v1, v6, Lffg;->c:Ljava/lang/String;

    .line 2039
    invoke-static {v1}, Lhst;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljac;->a:Ljac;

    .line 2038
    invoke-static {p0, v1, v2}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v1

    .line 2040
    const/4 v2, 0x0

    const/16 v5, 0x28

    move/from16 v3, p4

    move/from16 v4, p4

    invoke-virtual/range {v0 .. v5}, Lizs;->a(Lizu;IIII)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B
    :try_end_0
    .catch Lkdn; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lkde; {:try_start_0 .. :try_end_0} :catch_2

    .line 2050
    :goto_4
    if-eqz v0, :cond_0

    .line 2051
    invoke-static {v11}, Lffe;->c(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-wide v2, v6, Lffg;->a:J

    invoke-static {v1, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 2053
    const-string v2, "display_photo"

    invoke-static {v1, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2056
    :try_start_1
    invoke-virtual {v12, v1}, Landroid/content/ContentResolver;->openOutputStream(Landroid/net/Uri;)Ljava/io/OutputStream;

    move-result-object v1

    .line 2057
    invoke-virtual {v1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 2058
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    .line 2059
    invoke-static {p0, p1, v6}, Lffe;->a(Landroid/content/Context;ILffg;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    .line 2060
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2061
    const-string v0, "GooglePlusContactsSync"

    const/4 v2, 0x6

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2062
    const-string v2, "GooglePlusContactsSync"

    const-string v3, "Could not store large avatar: "

    iget-object v0, v6, Lffg;->b:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_5
    invoke-static {v2, v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_3

    .line 2044
    :catch_1
    move-exception v0

    .line 2045
    const-string v1, "GooglePlusContactsSync"

    const-string v2, "Could not download avatar"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v9

    .line 2048
    goto :goto_4

    .line 2046
    :catch_2
    move-exception v0

    .line 2047
    const-string v1, "GooglePlusContactsSync"

    const-string v2, "Canceled"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v9

    goto :goto_4

    .line 2062
    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_5

    :cond_3
    move v1, v7

    .line 2070
    goto/16 :goto_0

    .line 2071
    :cond_4
    return-void

    :cond_5
    move v7, v0

    goto/16 :goto_1
.end method

.method private static a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Lkfp;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 2503
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2504
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 2505
    new-array v2, v8, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v2, v7

    const/4 v5, 0x0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2507
    if-eqz v1, :cond_1

    .line 2509
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2510
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2513
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2517
    :cond_1
    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2532
    :goto_1
    return-void

    .line 2521
    :cond_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2522
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2523
    invoke-static {p1}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "_id=?"

    new-array v5, v8, [Ljava/lang/String;

    aput-object v0, v5, v7

    .line 2524
    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    .line 2525
    invoke-virtual {v0, v8}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    .line 2526
    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    .line 2523
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2527
    invoke-static {p0, v1, v7}, Lffe;->a(Landroid/content/Context;Ljava/util/ArrayList;Z)[Landroid/content/ContentProviderResult;

    .line 2528
    invoke-virtual {p4}, Lkfp;->e()V

    goto :goto_2

    .line 2531
    :cond_3
    invoke-static {p0, v1, v8}, Lffe;->a(Landroid/content/Context;Ljava/util/ArrayList;Z)[Landroid/content/ContentProviderResult;

    goto :goto_1
.end method

.method private static a(Landroid/content/Context;Landroid/net/Uri;Ljava/util/ArrayList;Lffj;Z)V
    .locals 10
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/net/Uri;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;",
            "Lffj;",
            "Z)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const-wide/16 v8, 0x0

    const/4 v6, 0x1

    .line 1572
    const-string v0, "GooglePlusContactsSync"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1573
    invoke-static {p3}, Lffe;->a(Lffj;)V

    .line 1576
    :cond_0
    invoke-static {p0}, Lffe;->f(Landroid/content/Context;)Z

    move-result v0

    .line 1578
    if-eqz v0, :cond_4

    .line 1579
    const-string v0, "vnd.android.cursor.item/vnd.googleplus.profile.comm"

    .line 1585
    :goto_0
    iget-wide v2, p3, Lffj;->a:J

    cmp-long v2, v2, v8

    if-nez v2, :cond_6

    .line 1586
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 1587
    invoke-static {p1}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    .line 1588
    invoke-virtual {v2, v6}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v3, "sourceid"

    iget-object v4, p3, Lffj;->b:Ljava/lang/String;

    .line 1589
    invoke-virtual {v2, v3, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v3, "sync1"

    iget-wide v4, p3, Lffj;->c:J

    .line 1590
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v3, "raw_contact_is_read_only"

    .line 1591
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    .line 1592
    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    .line 1587
    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1593
    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v3, "raw_contact_id"

    .line 1594
    invoke-virtual {v2, v3, v1}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v3, "mimetype"

    const-string v4, "vnd.android.cursor.item/name"

    .line 1595
    invoke-virtual {v2, v3, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v3, "data1"

    iget-object v4, p3, Lffj;->d:Ljava/lang/String;

    .line 1596
    invoke-virtual {v2, v3, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    .line 1597
    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    .line 1593
    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1598
    if-nez p4, :cond_1

    .line 1599
    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v3, "raw_contact_id"

    .line 1600
    invoke-virtual {v2, v3, v1}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v3, "mimetype"

    .line 1601
    invoke-virtual {v2, v3, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v3, "data4"

    const/16 v4, 0xa

    .line 1602
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v3, "data5"

    const-string v4, "conversation"

    .line 1603
    invoke-virtual {v2, v3, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v3, "data3"

    const v4, 0x7f0a0880

    .line 1605
    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1604
    invoke-virtual {v2, v3, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    .line 1606
    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    .line 1599
    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1608
    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v3, "raw_contact_id"

    .line 1609
    invoke-virtual {v2, v3, v1}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v3, "mimetype"

    .line 1610
    invoke-virtual {v2, v3, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v2, "data4"

    const/16 v3, 0xe

    .line 1611
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v2, "data5"

    const-string v3, "hangout"

    .line 1612
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v2, "data3"

    const v3, 0x7f0a0881

    .line 1614
    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1613
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    .line 1615
    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    .line 1608
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1617
    sget-object v0, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v2, "raw_contact_id"

    .line 1618
    invoke-virtual {v0, v2, v1}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v2, "mimetype"

    const-string v3, "vnd.android.cursor.item/vnd.googleplus.profile"

    .line 1619
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v2, "data4"

    const/16 v3, 0x14

    .line 1620
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v2, "data5"

    const-string v3, "addtocircle"

    .line 1621
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v2, "data3"

    const v3, 0x7f0a0882

    .line 1623
    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1622
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    .line 1624
    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    .line 1617
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1626
    :cond_1
    sget-object v0, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v2, "raw_contact_id"

    .line 1627
    invoke-virtual {v0, v2, v1}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v2, "mimetype"

    const-string v3, "vnd.android.cursor.item/vnd.googleplus.profile"

    .line 1628
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v2, "data4"

    const/16 v3, 0x1e

    .line 1629
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v2, "data5"

    const-string v3, "view"

    .line 1630
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v2, "data3"

    const v3, 0x7f0a087f

    .line 1632
    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1631
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    .line 1633
    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    .line 1626
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1634
    iget-object v0, p3, Lffj;->b:Ljava/lang/String;

    invoke-static {v0}, Ljpu;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1635
    if-eqz v0, :cond_2

    .line 1636
    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v3, "raw_contact_id"

    .line 1637
    invoke-virtual {v2, v3, v1}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v3, "mimetype"

    const-string v4, "vnd.android.cursor.item/identity"

    .line 1638
    invoke-virtual {v2, v3, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v3, "data2"

    const-string v4, "com.google"

    .line 1639
    invoke-virtual {v2, v3, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v3, "data1"

    const-string v4, "gprofile:"

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_5

    invoke-virtual {v4, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1640
    :goto_1
    invoke-virtual {v2, v3, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    .line 1641
    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    .line 1636
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1652
    :cond_2
    :goto_2
    iget-object v0, p3, Lffj;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lffi;

    .line 1654
    iget-boolean v2, v0, Lffi;->b:Z

    if-nez v2, :cond_7

    .line 1655
    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    iget-wide v4, v0, Lffi;->a:J

    .line 1656
    invoke-static {v2, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 1655
    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    .line 1657
    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    .line 1682
    :goto_4
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1581
    :cond_4
    const-string v0, "vnd.android.cursor.item/vnd.googleplus.profile"

    goto/16 :goto_0

    .line 1639
    :cond_5
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 1644
    :cond_6
    invoke-static {p1}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    .line 1645
    invoke-virtual {v0, v6}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v2, "_id=?"

    new-array v3, v6, [Ljava/lang/String;

    iget-wide v4, p3, Lffj;->a:J

    .line 1647
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    .line 1646
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v2, "sync1"

    iget-wide v4, p3, Lffj;->c:J

    .line 1648
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    .line 1649
    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    .line 1644
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1660
    :cond_7
    iget-wide v4, v0, Lffi;->a:J

    cmp-long v2, v4, v8

    if-nez v2, :cond_9

    .line 1661
    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    .line 1662
    const-string v4, "mimetype"

    iget-object v5, v0, Lffi;->d:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 1663
    iget-wide v4, p3, Lffj;->a:J

    cmp-long v4, v4, v8

    if-nez v4, :cond_8

    .line 1664
    const-string v4, "raw_contact_id"

    invoke-virtual {v2, v4, v1}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    .line 1676
    :goto_5
    const-string v4, "data1"

    iget-object v5, v0, Lffi;->e:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 1677
    const-string v4, "data2"

    iget-object v5, v0, Lffi;->f:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 1678
    const-string v4, "data3"

    iget-object v0, v0, Lffi;->g:Ljava/lang/String;

    invoke-virtual {v2, v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 1679
    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    goto :goto_4

    .line 1667
    :cond_8
    const-string v4, "raw_contact_id"

    iget-wide v6, p3, Lffj;->a:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    goto :goto_5

    .line 1669
    :cond_9
    iget-boolean v2, v0, Lffi;->c:Z

    if-eqz v2, :cond_3

    .line 1670
    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    iget-wide v4, v0, Lffi;->a:J

    .line 1671
    invoke-static {v2, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    .line 1670
    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    goto :goto_5

    .line 1684
    :cond_a
    return-void
.end method

.method private static a(Landroid/content/Context;Landroid/net/Uri;Ljava/util/ArrayList;[Ljava/lang/String;Ljava/util/HashMap;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/net/Uri;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;[",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lffj;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1559
    move v1, v2

    :goto_0
    array-length v0, p3

    if-ge v1, v0, :cond_0

    .line 1560
    aget-object v0, p3, v1

    invoke-virtual {p4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lffj;

    .line 1561
    invoke-static {p0, p1, p2, v0, v2}, Lffe;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/util/ArrayList;Lffj;Z)V

    .line 1559
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1563
    :cond_0
    return-void
.end method

.method private static a(Landroid/content/Context;Lffj;Ldsv;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v7, 0x1

    .line 1380
    iget-object v0, p2, Ldsv;->a:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 1382
    iget-object v0, p2, Ldsv;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Loib;

    .line 1383
    const-string v3, "vnd.android.cursor.item/email_v2"

    iget-object v4, v0, Loib;->d:Ljava/lang/String;

    invoke-static {p1, v3, v4}, Lffe;->a(Lffj;Ljava/lang/String;Ljava/lang/String;)Lffi;

    move-result-object v3

    .line 1384
    iput-boolean v7, v3, Lffi;->b:Z

    .line 1385
    iget-object v4, v0, Loib;->b:Ljava/lang/Integer;

    iget-object v5, v0, Loib;->c:Ljava/lang/String;

    if-nez v4, :cond_0

    move v0, v1

    :goto_1
    packed-switch v0, :pswitch_data_0

    sget-object v0, Lffe;->m:Ljava/lang/String;

    iput-object v0, v3, Lffi;->f:Ljava/lang/String;

    iput-object v5, v3, Lffi;->g:Ljava/lang/String;

    goto :goto_0

    :cond_0
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_1

    :pswitch_0
    sget-object v0, Lffe;->j:Ljava/lang/String;

    iput-object v0, v3, Lffi;->f:Ljava/lang/String;

    goto :goto_0

    :pswitch_1
    sget-object v0, Lffe;->k:Ljava/lang/String;

    iput-object v0, v3, Lffi;->f:Ljava/lang/String;

    goto :goto_0

    :pswitch_2
    sget-object v0, Lffe;->l:Ljava/lang/String;

    iput-object v0, v3, Lffi;->f:Ljava/lang/String;

    goto :goto_0

    .line 1389
    :cond_1
    iget-object v0, p2, Ldsv;->b:Ljava/util/List;

    if-eqz v0, :cond_5

    .line 1391
    iget-object v0, p2, Ldsv;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Loim;

    .line 1392
    const-string v3, "vnd.android.cursor.item/phone_v2"

    iget-object v4, v0, Loim;->d:Ljava/lang/String;

    invoke-static {p1, v3, v4}, Lffe;->a(Lffj;Ljava/lang/String;Ljava/lang/String;)Lffi;

    move-result-object v3

    .line 1393
    iput-boolean v7, v3, Lffi;->b:Z

    .line 1394
    iget-object v4, v0, Loim;->b:Ljava/lang/Integer;

    iget-object v5, v0, Loim;->c:Ljava/lang/String;

    if-nez v4, :cond_2

    const/4 v0, 0x0

    :goto_3
    if-eqz v0, :cond_3

    iput-object v0, v3, Lffi;->f:Ljava/lang/String;

    goto :goto_2

    :cond_2
    sget-object v0, Lffe;->L:Landroid/util/SparseArray;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {v0, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_3

    :cond_3
    if-eqz v4, :cond_4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v4, 0x15

    if-ne v0, v4, :cond_4

    sget-object v0, Lffe;->K:Ljava/lang/String;

    iput-object v0, v3, Lffi;->f:Ljava/lang/String;

    const v0, 0x7f0a0506

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lffi;->g:Ljava/lang/String;

    goto :goto_2

    :cond_4
    sget-object v0, Lffe;->K:Ljava/lang/String;

    iput-object v0, v3, Lffi;->f:Ljava/lang/String;

    iput-object v5, v3, Lffi;->g:Ljava/lang/String;

    goto :goto_2

    .line 1398
    :cond_5
    iget-object v0, p2, Ldsv;->c:Ljava/util/List;

    if-eqz v0, :cond_7

    .line 1399
    iget-object v0, p2, Ldsv;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lohr;

    .line 1400
    const-string v3, "vnd.android.cursor.item/postal-address_v2"

    iget-object v4, v0, Lohr;->b:Ljava/lang/String;

    invoke-static {p1, v3, v4}, Lffe;->a(Lffj;Ljava/lang/String;Ljava/lang/String;)Lffi;

    move-result-object v3

    .line 1402
    iput-boolean v7, v3, Lffi;->b:Z

    .line 1403
    iget-object v4, v0, Lohr;->c:Ljava/lang/Integer;

    iget-object v5, v0, Lohr;->d:Ljava/lang/String;

    if-nez v4, :cond_6

    move v0, v1

    :goto_5
    packed-switch v0, :pswitch_data_1

    sget-object v0, Lffe;->q:Ljava/lang/String;

    iput-object v0, v3, Lffi;->f:Ljava/lang/String;

    iput-object v5, v3, Lffi;->g:Ljava/lang/String;

    goto :goto_4

    :cond_6
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_5

    :pswitch_3
    sget-object v0, Lffe;->n:Ljava/lang/String;

    iput-object v0, v3, Lffi;->f:Ljava/lang/String;

    goto :goto_4

    :pswitch_4
    sget-object v0, Lffe;->o:Ljava/lang/String;

    iput-object v0, v3, Lffi;->f:Ljava/lang/String;

    goto :goto_4

    :pswitch_5
    sget-object v0, Lffe;->p:Ljava/lang/String;

    iput-object v0, v3, Lffi;->f:Ljava/lang/String;

    goto :goto_4

    .line 1406
    :cond_7
    return-void

    .line 1385
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 1403
    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private static a(Landroid/content/Context;Ljava/util/HashSet;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/net/Uri;",
            "[",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2341
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v4, 0x0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2343
    if-eqz v1, :cond_1

    .line 2345
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2346
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2349
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2352
    :cond_1
    return-void
.end method

.method public static a(Landroid/content/Context;Lkfp;)V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v1, 0x0

    .line 593
    invoke-static {p0}, Lffe;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 609
    :cond_0
    :goto_0
    return-void

    .line 597
    :cond_1
    invoke-static {p0}, Ldhv;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 599
    :try_start_0
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "logged_in"

    aput-object v4, v2, v3

    invoke-interface {v0, v2}, Lhei;->a([Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    if-nez v5, :cond_4

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, ""

    aput-object v1, v4, v0

    const-string v0, "?"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    const-string v0, "Android:DeleteProfilesOtherAccounts"

    invoke-virtual {p1, v0}, Lkfp;->c(Ljava/lang/String;)V

    sget-object v0, Lffe;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "caller_is_syncadapter"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    const-string v1, "data_set=\'plus\' AND account_name NOT IN ("

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "account_type"

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v5, "com.google"

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0x9

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "=\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1, v4, p1}, Lffe;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Lkfp;)V

    invoke-virtual {p1}, Lkfp;->f()V

    const-string v0, "Android:DeleteContactsOtherAccounts"

    invoke-virtual {p1, v0}, Lkfp;->c(Ljava/lang/String;)V

    sget-object v0, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "caller_is_syncadapter"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    const-string v1, "data_set=\'plus\' AND account_name NOT IN ("

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "account_type"

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v5, "com.google"

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0x9

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "=\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1, v4, p1}, Lffe;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Lkfp;)V

    invoke-virtual {p1}, Lkfp;->f()V

    const-string v0, "Android:DeleteCirclesOtherAccounts"

    invoke-virtual {p1, v0}, Lkfp;->c(Ljava/lang/String;)V

    sget-object v0, Landroid/provider/ContactsContract$Groups;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "caller_is_syncadapter"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v5, "_id"

    aput-object v5, v2, v3

    const-string v3, "data_set=\'plus\' AND account_name NOT IN ("

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "account_type"

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "com.google"

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, 0x9

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v9, v10

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v9, v10

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v9, v10

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ") AND "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "=\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    if-eqz v2, :cond_7

    :goto_1
    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v1, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v0, v3, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, v0}, Lkfp;->l(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    :try_start_2
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    .line 601
    :catch_0
    move-exception v0

    .line 602
    const-string v1, "GooglePlusContactsSync"

    const-string v2, "Failed to clear out contacts"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 606
    :cond_3
    :goto_2
    invoke-static {p0}, Ldhv;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 607
    invoke-static {p0, v11}, Ldhv;->b(Landroid/content/Context;Z)V

    goto/16 :goto_0

    .line 599
    :cond_4
    :try_start_3
    new-array v4, v5, [Ljava/lang/String;

    move v2, v1

    :goto_3
    if-ge v2, v5, :cond_2

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v1

    const-string v7, "account_name"

    invoke-interface {v1, v7}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v2

    if-lez v2, :cond_5

    const/16 v1, 0x2c

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_5
    const/16 v1, 0x3f

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_3

    :cond_6
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_7
    invoke-virtual {p1}, Lkfp;->f()V

    .line 600
    const/4 v0, 0x1

    invoke-static {p0, v0}, Ldhv;->a(Landroid/content/Context;Z)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_2
.end method

.method private static a(Lffj;)V
    .locals 14

    .prologue
    const-wide/16 v12, 0x0

    .line 2765
    iget-boolean v0, p0, Lffj;->e:Z

    if-nez v0, :cond_1

    .line 2766
    const-string v0, "[DELETE]"

    .line 2773
    :goto_0
    iget-object v1, p0, Lffj;->b:Ljava/lang/String;

    iget-object v2, p0, Lffj;->d:Ljava/lang/String;

    iget-wide v4, p0, Lffj;->a:J

    iget-wide v6, p0, Lffj;->c:J

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0x4a

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    invoke-direct {v3, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " (raw_contact_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") last_updated="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 2775
    iget-object v0, p0, Lffj;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lffi;

    .line 2776
    iget-boolean v1, v0, Lffi;->b:Z

    if-nez v1, :cond_3

    .line 2777
    const-string v1, "[DELETE]"

    .line 2786
    :goto_2
    iget-object v3, v0, Lffi;->d:Ljava/lang/String;

    iget-wide v4, v0, Lffi;->a:J

    iget-object v6, v0, Lffi;->e:Ljava/lang/String;

    iget-object v7, v0, Lffi;->f:Ljava/lang/String;

    iget-object v0, v0, Lffi;->g:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, 0x2f

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v9, v10

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v9, v10

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v9, v10

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v9, v10

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v9, "    "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v8, " "

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " (data_id="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ") \'"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\', type="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 2767
    :cond_1
    iget-wide v0, p0, Lffj;->a:J

    cmp-long v0, v0, v12

    if-nez v0, :cond_2

    .line 2768
    const-string v0, "[INSERT]"

    goto/16 :goto_0

    .line 2770
    :cond_2
    const-string v0, "[UPDATE]"

    goto/16 :goto_0

    .line 2778
    :cond_3
    iget-wide v4, v0, Lffi;->a:J

    cmp-long v1, v4, v12

    if-nez v1, :cond_4

    .line 2779
    const-string v1, "[INSERT]"

    goto/16 :goto_2

    .line 2780
    :cond_4
    iget-boolean v1, v0, Lffi;->c:Z

    if-eqz v1, :cond_0

    .line 2781
    const-string v1, "[UPDATE]"

    goto/16 :goto_2

    .line 2790
    :cond_5
    return-void
.end method

.method private static a(Lffj;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 1520
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    move-object p4, v0

    .line 1523
    :cond_0
    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    move-object p5, v0

    .line 1526
    :cond_1
    invoke-static {p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    move-object p6, v0

    .line 1530
    :cond_2
    iget-object v0, p0, Lffj;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lffi;

    .line 1531
    iget-object v2, v0, Lffi;->d:Ljava/lang/String;

    invoke-static {v2, p3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, v0, Lffi;->e:Ljava/lang/String;

    .line 1532
    invoke-static {v2, p4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-wide v2, v0, Lffi;->a:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    .line 1533
    iput-wide p1, v0, Lffi;->a:J

    .line 1536
    iget-object v1, v0, Lffi;->f:Ljava/lang/String;

    invoke-static {v1, p5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, v0, Lffi;->g:Ljava/lang/String;

    .line 1537
    invoke-static {v1, p6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 1538
    :cond_4
    iput-object p5, v0, Lffi;->f:Ljava/lang/String;

    .line 1539
    iput-object p6, v0, Lffi;->g:Ljava/lang/String;

    .line 1540
    const/4 v1, 0x1

    iput-boolean v1, v0, Lffi;->c:Z

    .line 1550
    :cond_5
    :goto_0
    return-void

    .line 1547
    :cond_6
    new-instance v0, Lffi;

    invoke-direct {v0}, Lffi;-><init>()V

    .line 1548
    iput-wide p1, v0, Lffi;->a:J

    .line 1549
    iget-object v1, p0, Lffj;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private static a(Lffj;Lnif;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v0, 0x0

    .line 1414
    iget-object v1, p1, Lnif;->b:[Lnjy;

    if-eqz v1, :cond_4

    .line 1415
    iget-object v2, p1, Lnif;->b:[Lnjy;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 1416
    const-string v5, "vnd.android.cursor.item/email_v2"

    iget-object v6, v4, Lnjy;->d:Ljava/lang/String;

    invoke-static {p0, v5, v6}, Lffe;->a(Lffj;Ljava/lang/String;Ljava/lang/String;)Lffi;

    move-result-object v5

    .line 1417
    iput-boolean v8, v5, Lffi;->b:Z

    .line 1418
    iget-object v6, v4, Lnjy;->c:Lnie;

    if-eqz v6, :cond_0

    .line 1419
    iget-object v4, v4, Lnjy;->c:Lnie;

    const-string v6, "HOME"

    iget v7, v4, Lnie;->a:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    sget-object v4, Lffe;->j:Ljava/lang/String;

    iput-object v4, v5, Lffi;->f:Ljava/lang/String;

    .line 1415
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1419
    :cond_1
    const-string v6, "WORK"

    iget v7, v4, Lnie;->a:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    sget-object v4, Lffe;->k:Ljava/lang/String;

    iput-object v4, v5, Lffi;->f:Ljava/lang/String;

    goto :goto_1

    :cond_2
    const-string v6, "OTHER"

    iget v7, v4, Lnie;->a:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    sget-object v4, Lffe;->l:Ljava/lang/String;

    iput-object v4, v5, Lffi;->f:Ljava/lang/String;

    goto :goto_1

    :cond_3
    const-string v6, "CUSTOM"

    iget v7, v4, Lnie;->a:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    sget-object v6, Lffe;->m:Ljava/lang/String;

    iput-object v6, v5, Lffi;->f:Ljava/lang/String;

    iget-object v4, v4, Lnie;->b:Ljava/lang/String;

    iput-object v4, v5, Lffi;->g:Ljava/lang/String;

    goto :goto_1

    .line 1424
    :cond_4
    iget-object v1, p1, Lnif;->a:[Lnka;

    if-eqz v1, :cond_9

    .line 1425
    iget-object v2, p1, Lnif;->a:[Lnka;

    array-length v3, v2

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_9

    aget-object v4, v2, v1

    .line 1426
    const-string v5, "vnd.android.cursor.item/phone_v2"

    iget-object v6, v4, Lnka;->d:Ljava/lang/String;

    invoke-static {p0, v5, v6}, Lffe;->a(Lffj;Ljava/lang/String;Ljava/lang/String;)Lffi;

    move-result-object v5

    .line 1427
    iput-boolean v8, v5, Lffi;->b:Z

    .line 1428
    iget-object v6, v4, Lnka;->c:Lnie;

    if-eqz v6, :cond_5

    .line 1429
    iget-object v4, v4, Lnka;->c:Lnie;

    const-string v6, "HOME"

    iget v7, v4, Lnie;->a:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    sget-object v4, Lffe;->r:Ljava/lang/String;

    iput-object v4, v5, Lffi;->f:Ljava/lang/String;

    .line 1425
    :cond_5
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1429
    :cond_6
    const-string v6, "WORK"

    iget v7, v4, Lnie;->a:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    sget-object v4, Lffe;->s:Ljava/lang/String;

    iput-object v4, v5, Lffi;->f:Ljava/lang/String;

    goto :goto_3

    :cond_7
    const-string v6, "OTHER"

    iget v7, v4, Lnie;->a:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    sget-object v4, Lffe;->t:Ljava/lang/String;

    iput-object v4, v5, Lffi;->f:Ljava/lang/String;

    goto :goto_3

    :cond_8
    const-string v6, "CUSTOM"

    iget v7, v4, Lnie;->a:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    sget-object v6, Lffe;->K:Ljava/lang/String;

    iput-object v6, v5, Lffi;->f:Ljava/lang/String;

    iget-object v4, v4, Lnie;->b:Ljava/lang/String;

    iput-object v4, v5, Lffi;->g:Ljava/lang/String;

    goto :goto_3

    .line 1434
    :cond_9
    iget-object v1, p1, Lnif;->c:[Lnjx;

    if-eqz v1, :cond_e

    .line 1435
    iget-object v1, p1, Lnif;->c:[Lnjx;

    array-length v2, v1

    :goto_4
    if-ge v0, v2, :cond_e

    aget-object v3, v1, v0

    .line 1436
    const-string v4, "vnd.android.cursor.item/postal-address_v2"

    iget-object v5, v3, Lnjx;->d:Ljava/lang/String;

    invoke-static {p0, v4, v5}, Lffe;->a(Lffj;Ljava/lang/String;Ljava/lang/String;)Lffi;

    move-result-object v4

    .line 1438
    iput-boolean v8, v4, Lffi;->b:Z

    .line 1439
    iget-object v5, v3, Lnjx;->c:Lnie;

    if-eqz v5, :cond_a

    .line 1440
    iget-object v3, v3, Lnjx;->c:Lnie;

    const-string v5, "HOME"

    iget v6, v3, Lnie;->a:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_b

    sget-object v3, Lffe;->n:Ljava/lang/String;

    iput-object v3, v4, Lffi;->f:Ljava/lang/String;

    .line 1435
    :cond_a
    :goto_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 1440
    :cond_b
    const-string v5, "WORK"

    iget v6, v3, Lnie;->a:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_c

    sget-object v3, Lffe;->o:Ljava/lang/String;

    iput-object v3, v4, Lffi;->f:Ljava/lang/String;

    goto :goto_5

    :cond_c
    const-string v5, "OTHER"

    iget v6, v3, Lnie;->a:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_d

    sget-object v3, Lffe;->p:Ljava/lang/String;

    iput-object v3, v4, Lffi;->f:Ljava/lang/String;

    goto :goto_5

    :cond_d
    const-string v5, "CUSTOM"

    iget v6, v3, Lnie;->a:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_a

    sget-object v5, Lffe;->q:Ljava/lang/String;

    iput-object v5, v4, Lffi;->f:Ljava/lang/String;

    iget-object v3, v3, Lnie;->b:Ljava/lang/String;

    iput-object v3, v4, Lffi;->g:Ljava/lang/String;

    goto :goto_5

    .line 1444
    :cond_e
    return-void
.end method

.method private static a([Ljava/lang/String;Ljava/util/HashMap;Ljava/util/HashMap;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lffj;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1452
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    array-length v0, p0

    if-ge v2, v0, :cond_1

    .line 1453
    aget-object v0, p0, v2

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lffj;

    .line 1454
    const-string v1, "plus"

    invoke-virtual {p2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 1455
    if-eqz v1, :cond_0

    .line 1456
    const-string v3, "vnd.android.cursor.item/group_membership"

    .line 1457
    invoke-virtual {v1}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1456
    invoke-static {v0, v3, v1}, Lffe;->a(Lffj;Ljava/lang/String;Ljava/lang/String;)Lffi;

    move-result-object v0

    .line 1458
    const/4 v1, 0x1

    iput-boolean v1, v0, Lffi;->b:Z

    .line 1452
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1461
    :cond_1
    return-void
.end method

.method private static a(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/util/HashMap;)Z
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/net/Uri;",
            "[",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lffj;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    const/4 v6, 0x0

    .line 1480
    array-length v0, p2

    new-array v4, v0, [Ljava/lang/String;

    .line 1481
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 1482
    const-string v0, "_id IN ("

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v1, v6

    .line 1483
    :goto_0
    array-length v0, p2

    if-ge v1, v0, :cond_1

    .line 1484
    aget-object v0, p2, v1

    invoke-virtual {p3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lffj;

    iget-wide v10, v0, Lffj;->a:J

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    .line 1485
    if-eqz v1, :cond_0

    .line 1486
    const/16 v0, 0x2c

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1488
    :cond_0
    const/16 v0, 0x3f

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1483
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1490
    :cond_1
    const-string v0, ") AND mimetype IN (\'vnd.android.cursor.item/email_v2\',\'vnd.android.cursor.item/phone_v2\',\'vnd.android.cursor.item/postal-address_v2\',\'vnd.android.cursor.item/group_membership\')"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1492
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1493
    sget-object v2, Lffe;->g:[Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 1494
    if-nez v9, :cond_2

    .line 1512
    :goto_1
    return v6

    .line 1499
    :cond_2
    :goto_2
    :try_start_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1500
    const/4 v0, 0x0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1501
    invoke-virtual {p3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lffj;

    .line 1502
    const/4 v0, 0x2

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    const/4 v0, 0x1

    .line 1503
    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v0, 0x3

    .line 1504
    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v0, 0x4

    .line 1505
    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v0, 0x5

    .line 1506
    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 1502
    invoke-static/range {v1 .. v7}, Lffe;->a(Lffj;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 1509
    :catchall_0
    move-exception v0

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_3
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    move v6, v8

    .line 1512
    goto :goto_1
.end method

.method private static a([BI)[B
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2182
    if-nez p0, :cond_0

    move-object p0, v0

    .line 2206
    :goto_0
    return-object p0

    .line 2186
    :cond_0
    const/4 v1, 0x0

    array-length v2, p0

    invoke-static {p0, v1, v2}, Lfus;->a([BII)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 2187
    if-nez v1, :cond_1

    move-object p0, v0

    .line 2188
    goto :goto_0

    .line 2190
    :cond_1
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    if-gt v2, p1, :cond_2

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    if-gt v2, p1, :cond_2

    .line 2191
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    .line 2195
    :cond_2
    invoke-static {v1, p1}, Llrw;->a(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 2196
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 2197
    if-nez v2, :cond_3

    move-object p0, v0

    .line 2200
    goto :goto_0

    .line 2203
    :cond_3
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    const/16 v1, 0xfa0

    invoke-direct {v0, v1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 2204
    sget-object v1, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x64

    invoke-virtual {v2, v1, v3, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 2205
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 2206
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object p0

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;Ljava/util/ArrayList;IZ)[Landroid/content/ContentProviderResult;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;IZ)[",
            "Landroid/content/ContentProviderResult;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1723
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 1725
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 1726
    if-nez v2, :cond_1

    .line 1744
    :cond_0
    :goto_0
    return-object v0

    .line 1730
    :cond_1
    if-nez p3, :cond_2

    if-lt v2, p2, :cond_0

    .line 1736
    :cond_2
    :try_start_0
    const-string v2, "com.android.contacts"

    invoke-virtual {v1, v2, p1}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1743
    :cond_3
    :goto_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    goto :goto_0

    .line 1737
    :catch_0
    move-exception v1

    .line 1738
    const-string v2, "GooglePlusContactsSync"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1739
    const-string v2, "GooglePlusContactsSync"

    const-string v3, "Cannot apply a batch of content provider operations"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method private static a(Landroid/content/Context;Ljava/util/ArrayList;Z)[Landroid/content/ContentProviderResult;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;Z)[",
            "Landroid/content/ContentProviderResult;"
        }
    .end annotation

    .prologue
    .line 1715
    const/16 v0, 0x80

    invoke-static {p0, p1, v0, p2}, Lffe;->a(Landroid/content/Context;Ljava/util/ArrayList;IZ)[Landroid/content/ContentProviderResult;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/lang/String;)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 2579
    sget-object v0, Lffe;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_type"

    const-string v2, "com.google"

    .line 2580
    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "data_set"

    const-string v2, "plus"

    .line 2581
    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_name"

    .line 2582
    invoke-virtual {v0, v1, p0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "caller_is_syncadapter"

    const-string v2, "true"

    .line 2583
    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 2584
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static b(Landroid/content/Context;I)Ljava/util/HashMap;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lffh;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v3, 0x0

    .line 813
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 814
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 815
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 816
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 817
    invoke-static {v1}, Lffe;->e(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lffe;->M:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 818
    if-nez v1, :cond_0

    .line 846
    :goto_0
    return-object v3

    .line 823
    :cond_0
    :goto_1
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 824
    new-instance v0, Lffh;

    invoke-direct {v0}, Lffh;-><init>()V

    .line 825
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, v0, Lffh;->a:J

    .line 826
    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lffh;->b:Ljava/lang/String;

    .line 827
    const/4 v2, 0x2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lffh;->c:Ljava/lang/String;

    .line 828
    iget-object v2, v0, Lffh;->b:Ljava/lang/String;

    invoke-virtual {v6, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 831
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 834
    const-string v0, "plus"

    invoke-virtual {v6, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lffh;

    .line 835
    if-nez v0, :cond_2

    .line 836
    new-instance v0, Lffh;

    invoke-direct {v0}, Lffh;-><init>()V

    .line 837
    const-string v1, "plus"

    iput-object v1, v0, Lffh;->b:Ljava/lang/String;

    .line 838
    const v1, 0x7f0a0966

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lffh;->c:Ljava/lang/String;

    .line 839
    iput-boolean v7, v0, Lffh;->d:Z

    .line 840
    const-string v1, "plus"

    invoke-virtual {v6, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_2
    move-object v3, v6

    .line 846
    goto :goto_0

    .line 843
    :cond_2
    const-string v0, "plus"

    invoke-virtual {v6, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2
.end method

.method public static b(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 2374
    invoke-static {p0}, Lffe;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2395
    :cond_0
    :goto_0
    return-void

    .line 2378
    :cond_1
    invoke-static {p0}, Ldhv;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2383
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lfff;

    invoke-direct {v1, p0}, Lfff;-><init>(Landroid/content/Context;)V

    const-string v2, "contact_cleanup"

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 2394
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method private static b(Landroid/content/Context;ILjava/util/HashMap;Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lffj;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1132
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1133
    invoke-virtual {p2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lffj;

    .line 1134
    iget-boolean v3, v0, Lffj;->e:Z

    if-nez v3, :cond_0

    .line 1135
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1139
    :cond_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1140
    invoke-static {p0, p1, p3, v1}, Lffe;->a(Landroid/content/Context;ILjava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 1142
    :cond_2
    return-void
.end method

.method private static b(Landroid/content/Context;ILjava/util/HashMap;Ljava/util/ArrayList;Ljava/util/HashMap;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lffj;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1110
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1112
    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 1115
    invoke-virtual {p2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lffj;

    .line 1116
    iget-boolean v2, v0, Lffj;->e:Z

    if-eqz v2, :cond_0

    iget-wide v4, v0, Lffj;->a:J

    const-wide/16 v6, 0x0

    cmp-long v2, v4, v6

    if-eqz v2, :cond_0

    .line 1117
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1121
    :cond_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1122
    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p3

    move-object v4, p2

    move-object v6, p4

    invoke-static/range {v0 .. v6}, Lffe;->a(Landroid/content/Context;ILjava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/HashMap;ZLjava/util/HashMap;)V

    .line 1124
    :cond_2
    return-void
.end method

.method private static b(Landroid/content/Context;ILkfp;)V
    .locals 16

    .prologue
    .line 2093
    invoke-static/range {p0 .. p2}, Lffe;->f(Landroid/content/Context;ILkfp;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2145
    :goto_0
    return-void

    .line 2097
    :cond_0
    const-string v2, "Android:Avatars"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lkfp;->c(Ljava/lang/String;)V

    .line 2099
    invoke-static/range {p0 .. p1}, Lffe;->f(Landroid/content/Context;I)Ljava/util/HashMap;

    move-result-object v2

    .line 2100
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/util/HashMap;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2101
    :cond_1
    invoke-virtual/range {p2 .. p2}, Lkfp;->f()V

    goto :goto_0

    .line 2105
    :cond_2
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 2106
    new-instance v9, Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-direct {v9, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 2107
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 2108
    const/4 v3, 0x0

    .line 2109
    :goto_1
    if-ge v3, v5, :cond_7

    invoke-virtual/range {p2 .. p2}, Lkfp;->c()Z

    move-result v2

    if-nez v2, :cond_7

    .line 2110
    add-int/lit8 v2, v3, 0x8

    .line 2111
    if-le v2, v5, :cond_8

    move v4, v5

    :goto_2
    move v7, v3

    .line 2115
    :goto_3
    if-ge v7, v4, :cond_6

    .line 2116
    invoke-virtual {v9, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lffg;

    .line 2117
    iget-object v3, v2, Lffg;->c:Ljava/lang/String;

    if-eqz v3, :cond_4

    .line 2118
    const-class v3, Lhso;

    move-object/from16 v0, p0

    invoke-static {v0, v3}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lhso;

    .line 2119
    iget-object v6, v2, Lffg;->c:Ljava/lang/String;

    invoke-static {v6}, Lhst;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 2120
    const/4 v6, 0x0

    .line 2122
    const/4 v11, 0x2

    const/4 v12, 0x0

    const/16 v13, 0x28

    :try_start_0
    invoke-interface {v3, v10, v11, v12, v13}, Lhso;->a(Ljava/lang/String;III)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [B
    :try_end_0
    .catch Lkdn; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lkde; {:try_start_0 .. :try_end_0} :catch_1

    move-object v6, v3

    .line 2132
    :goto_4
    if-eqz v6, :cond_4

    .line 2133
    const-class v3, Lhei;

    move-object/from16 v0, p0

    invoke-static {v0, v3}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lhei;

    move/from16 v0, p1

    invoke-interface {v3, v0}, Lhei;->a(I)Lhej;

    move-result-object v3

    const-string v10, "account_name"

    invoke-interface {v3, v10}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    sget v10, Lffe;->U:I

    if-nez v10, :cond_3

    const-string v10, "thumbnail_max_dim"

    move-object/from16 v0, p0

    invoke-static {v0, v10}, Lffe;->a(Landroid/content/Context;Ljava/lang/String;)I

    move-result v10

    sput v10, Lffe;->U:I

    :cond_3
    invoke-static {v3}, Lffe;->c(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const/4 v10, 0x1

    invoke-virtual {v3, v10}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v10, "_id=?"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/String;

    const/4 v12, 0x0

    iget-wide v14, v2, Lffg;->a:J

    invoke-static {v14, v15}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-virtual {v3, v10, v11}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v10, "sync3"

    iget v11, v2, Lffg;->d:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v3, v10, v11}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-wide v10, v2, Lffg;->e:J

    const-wide/16 v12, 0x0

    cmp-long v3, v10, v12

    if-eqz v3, :cond_5

    sget-object v3, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v10, "_id=?"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/String;

    const/4 v12, 0x0

    iget-wide v14, v2, Lffg;->e:J

    invoke-static {v14, v15}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v11, v12

    invoke-virtual {v3, v10, v11}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v3, "data15"

    sget v10, Lffe;->U:I

    invoke-static {v6, v10}, Lffe;->a([BI)[B

    move-result-object v6

    invoke-virtual {v2, v3, v6}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2115
    :cond_4
    :goto_5
    add-int/lit8 v2, v7, 0x1

    move v7, v2

    goto/16 :goto_3

    .line 2127
    :catch_0
    move-exception v3

    .line 2128
    const-string v10, "GooglePlusContactsSync"

    const-string v11, "Cannot download avatar"

    invoke-static {v10, v11, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_4

    .line 2129
    :catch_1
    move-exception v3

    .line 2130
    const-string v10, "GooglePlusContactsSync"

    const-string v11, "Canceled"

    invoke-static {v10, v11, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_4

    .line 2133
    :cond_5
    sget-object v3, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v10, "raw_contact_id"

    iget-wide v12, v2, Lffg;->a:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v3, v10, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v3, "mimetype"

    const-string v10, "vnd.android.cursor.item/photo"

    invoke-virtual {v2, v3, v10}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v3, "data15"

    sget v10, Lffe;->U:I

    invoke-static {v6, v10}, Lffe;->a([BI)[B

    move-result-object v6

    invoke-virtual {v2, v3, v6}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 2138
    :cond_6
    const/16 v2, 0x8

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v8, v2, v3}, Lffe;->a(Landroid/content/Context;Ljava/util/ArrayList;IZ)[Landroid/content/ContentProviderResult;

    move v3, v4

    .line 2141
    goto/16 :goto_1

    .line 2143
    :cond_7
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-static {v0, v8, v2}, Lffe;->a(Landroid/content/Context;Ljava/util/ArrayList;Z)[Landroid/content/ContentProviderResult;

    .line 2144
    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Lkfp;->m(I)V

    goto/16 :goto_0

    :cond_8
    move v4, v2

    goto/16 :goto_2
.end method

.method private static c(Ljava/lang/String;)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 2614
    sget-object v0, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_type"

    const-string v2, "com.google"

    .line 2615
    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "data_set"

    const-string v2, "plus"

    .line 2616
    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_name"

    .line 2617
    invoke-virtual {v0, v1, p0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "caller_is_syncadapter"

    const-string v2, "true"

    .line 2618
    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 2619
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static c(Landroid/content/Context;I)Ljava/util/HashMap;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lffj;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1009
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 1010
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1011
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1012
    invoke-static {v1}, Lffe;->c(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1014
    sget-object v2, Lffe;->c:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 1015
    if-nez v2, :cond_0

    .line 1068
    :goto_0
    return-object v3

    .line 1019
    :cond_0
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1021
    :goto_1
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1022
    new-instance v4, Lffj;

    invoke-direct {v4}, Lffj;-><init>()V

    .line 1023
    const/4 v5, 0x0

    invoke-interface {v2, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    iput-wide v6, v4, Lffj;->a:J

    .line 1024
    const/4 v5, 0x1

    invoke-interface {v2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lffj;->b:Ljava/lang/String;

    .line 1025
    const/4 v5, 0x2

    invoke-interface {v2, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    iput-wide v6, v4, Lffj;->c:J

    .line 1026
    iget-object v5, v4, Lffj;->b:Ljava/lang/String;

    invoke-virtual {v1, v5, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 1029
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 1032
    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->d:Landroid/net/Uri;

    .line 1033
    invoke-static {v2, p1}, Lcom/google/android/apps/plus/content/EsProvider;->a(Landroid/net/Uri;I)Landroid/net/Uri;

    move-result-object v5

    sget-object v6, Lffe;->e:[Ljava/lang/String;

    const-string v7, "in_my_circles=1 AND profile_type!=2 AND for_sharing!=0"

    move-object v4, v0

    move-object v8, v3

    move-object v9, v3

    .line 1032
    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 1039
    if-eqz v2, :cond_4

    .line 1040
    :goto_2
    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1041
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1042
    const/4 v0, 0x1

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 1043
    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lffj;

    .line 1044
    if-nez v0, :cond_2

    .line 1045
    new-instance v0, Lffj;

    invoke-direct {v0}, Lffj;-><init>()V

    .line 1046
    iput-object v3, v0, Lffj;->b:Ljava/lang/String;

    .line 1047
    iput-wide v4, v0, Lffj;->c:J

    .line 1048
    iget-object v3, v0, Lffj;->b:Ljava/lang/String;

    invoke-virtual {v1, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1056
    :goto_3
    const/4 v3, 0x1

    iput-boolean v3, v0, Lffj;->e:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_2

    .line 1060
    :catchall_1
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    .line 1050
    :cond_2
    :try_start_2
    iget-wide v6, v0, Lffj;->c:J

    cmp-long v6, v6, v4

    if-nez v6, :cond_3

    .line 1051
    invoke-virtual {v1, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 1053
    :cond_3
    iput-wide v4, v0, Lffj;->c:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_3

    .line 1060
    :cond_4
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 1064
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 1065
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v2, "gaia_id"

    invoke-interface {v0, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1066
    invoke-static {v0}, Ljpu;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1067
    invoke-virtual {v1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-object v3, v1

    .line 1068
    goto/16 :goto_0
.end method

.method private static c(Landroid/content/Context;ILkfp;)V
    .locals 9

    .prologue
    const-wide/16 v6, 0x0

    const/4 v3, 0x0

    .line 2402
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 2403
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 2404
    const-string v0, "Android:DeleteProfile"

    invoke-virtual {p2, v0}, Lkfp;->c(Ljava/lang/String;)V

    .line 2405
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2407
    invoke-static {v8}, Lffe;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lffe;->h:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2409
    if-eqz v1, :cond_2

    .line 2411
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2412
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v4

    .line 2415
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2419
    :goto_1
    cmp-long v1, v4, v6

    if-eqz v1, :cond_0

    .line 2421
    invoke-static {v8}, Lffe;->c(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 2420
    invoke-virtual {v0, v1, v3, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 2423
    invoke-virtual {p2, v0}, Lkfp;->l(I)V

    .line 2426
    :cond_0
    invoke-virtual {p2}, Lkfp;->f()V

    .line 2427
    return-void

    .line 2415
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    move-wide v4, v6

    goto :goto_0

    :cond_2
    move-wide v4, v6

    goto :goto_1
.end method

.method public static c(Landroid/content/Context;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2689
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-ge v1, v2, :cond_1

    .line 2697
    :cond_0
    :goto_0
    return v0

    .line 2693
    :cond_1
    sget-object v1, Lfvc;->j:Lfvc;

    invoke-virtual {v1}, Lfvc;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2697
    invoke-static {p0}, Lffe;->d(Landroid/content/Context;)Z

    move-result v0

    goto :goto_0
.end method

.method private static d(Ljava/lang/String;)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 2635
    sget-object v0, Landroid/provider/ContactsContract$RawContactsEntity;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_type"

    const-string v2, "com.google"

    .line 2636
    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "data_set"

    const-string v2, "plus"

    .line 2637
    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_name"

    .line 2638
    invoke-virtual {v0, v1, p0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "caller_is_syncadapter"

    const-string v2, "true"

    .line 2639
    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 2640
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static d(Landroid/content/Context;I)Ljava/util/HashMap;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1148
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1149
    const-class v1, Lhei;

    invoke-static {p0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhei;

    .line 1150
    invoke-interface {v1, p1}, Lhei;->a(I)Lhej;

    move-result-object v1

    const-string v2, "account_name"

    invoke-interface {v1, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1151
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 1152
    invoke-static {v1}, Lffe;->e(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lffe;->M:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1154
    if-nez v1, :cond_0

    .line 1167
    :goto_0
    return-object v3

    .line 1159
    :cond_0
    :goto_1
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1160
    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    .line 1161
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 1160
    invoke-virtual {v6, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 1164
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move-object v3, v6

    .line 1167
    goto :goto_0
.end method

.method private static d(Landroid/content/Context;ILkfp;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2539
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 2540
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2541
    const-string v1, "Android:DeleteContacts"

    invoke-virtual {p2, v1}, Lkfp;->c(Ljava/lang/String;)V

    .line 2542
    invoke-static {v0}, Lffe;->c(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0, v0, v2, v2, p2}, Lffe;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Lkfp;)V

    .line 2543
    invoke-virtual {p2}, Lkfp;->f()V

    .line 2544
    return-void
.end method

.method public static d(Landroid/content/Context;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2704
    sget-boolean v1, Lffe;->S:Z

    if-eqz v1, :cond_0

    .line 2705
    sget-boolean v0, Lffe;->T:Z

    .line 2720
    :goto_0
    return v0

    .line 2709
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "com.android.contacts"

    .line 2710
    invoke-virtual {v1, v2}, Landroid/content/ContentResolver;->acquireContentProviderClient(Ljava/lang/String;)Landroid/content/ContentProviderClient;

    move-result-object v1

    .line 2711
    if-eqz v1, :cond_2

    :goto_1
    sput-boolean v0, Lffe;->T:Z

    .line 2712
    if-eqz v1, :cond_1

    .line 2713
    invoke-virtual {v1}, Landroid/content/ContentProviderClient;->release()Z

    .line 2715
    :cond_1
    const/4 v0, 0x1

    sput-boolean v0, Lffe;->S:Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 2720
    :goto_2
    sget-boolean v0, Lffe;->T:Z

    goto :goto_0

    .line 2711
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 2717
    :catch_0
    move-exception v0

    const-string v0, "GooglePlusContactsSync"

    const-string v1, "Cannot determine availability of the contacts provider"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method private static e(Ljava/lang/String;)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 2659
    sget-object v0, Landroid/provider/ContactsContract$Groups;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_type"

    const-string v2, "com.google"

    .line 2660
    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "data_set"

    const-string v2, "plus"

    .line 2661
    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_name"

    .line 2662
    invoke-virtual {v0, v1, p0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "caller_is_syncadapter"

    const-string v2, "true"

    .line 2663
    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 2664
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static e(Landroid/content/Context;I)Ljava/util/HashMap;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lffg;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v12, 0x0

    .line 1917
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "account_type"

    const-string v4, "com.google"

    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "data_set"

    const-string v4, "plus"

    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "account_name"

    invoke-virtual {v2, v3, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "caller_is_syncadapter"

    const-string v3, "true"

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/String;

    const-string v0, "raw_contact_id"

    aput-object v0, v3, v12

    const-string v4, "starred!=0 AND (mimetype=\'vnd.android.cursor.item/vnd.googleplus.profile\' OR mimetype=\'vnd.android.cursor.item/vnd.googleplus.profile.comm\')"

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lffe;->a(Landroid/content/Context;Ljava/util/HashSet;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v10, "starred=0 AND times_contacted>0 AND (mimetype=\'vnd.android.cursor.item/vnd.googleplus.profile\' OR mimetype=\'vnd.android.cursor.item/vnd.googleplus.profile.comm\')"

    const-string v11, "times_contacted DESC LIMIT 8"

    move-object v6, p0

    move-object v7, v1

    move-object v8, v2

    move-object v9, v3

    invoke-static/range {v6 .. v11}, Lffe;->a(Landroid/content/Context;Ljava/util/HashSet;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v10, "starred=0 AND last_time_contacted>0 AND (mimetype=\'vnd.android.cursor.item/vnd.googleplus.profile\' OR mimetype=\'vnd.android.cursor.item/vnd.googleplus.profile.comm\')"

    const-string v11, "last_time_contacted DESC LIMIT 8"

    move-object v6, p0

    move-object v7, v1

    move-object v8, v2

    move-object v9, v3

    invoke-static/range {v6 .. v11}, Lffe;->a(Landroid/content/Context;Ljava/util/HashSet;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-array v0, v12, [Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    .line 1918
    array-length v0, v4

    if-nez v0, :cond_1

    .line 1968
    :cond_0
    :goto_0
    return-object v5

    .line 1922
    :cond_1
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 1925
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1926
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 1927
    const-string v1, "_id IN ("

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v1, v12

    .line 1928
    :goto_1
    array-length v2, v4

    if-ge v1, v2, :cond_3

    .line 1929
    if-eqz v1, :cond_2

    .line 1930
    const/16 v2, 0x2c

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1932
    :cond_2
    const/16 v2, 0x3f

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1928
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1934
    :cond_3
    const/16 v1, 0x29

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1936
    const-class v1, Lhei;

    invoke-static {p0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhei;

    .line 1937
    invoke-interface {v1, p1}, Lhei;->a(I)Lhej;

    move-result-object v1

    const-string v2, "account_name"

    invoke-interface {v1, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1940
    const-string v2, " OR sync2 != 0"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1941
    invoke-static {v1}, Lffe;->c(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lffe;->N:[Ljava/lang/String;

    .line 1942
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1941
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1943
    if-eqz v1, :cond_0

    .line 1948
    :cond_4
    :goto_2
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1949
    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1950
    invoke-static {v0}, Ljpu;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1951
    if-eqz v0, :cond_4

    .line 1952
    new-instance v2, Lffg;

    invoke-direct {v2}, Lffg;-><init>()V

    .line 1953
    iput-object v0, v2, Lffg;->b:Ljava/lang/String;

    .line 1954
    const/4 v3, 0x0

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v2, Lffg;->a:J

    .line 1955
    const/4 v3, 0x2

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v2, Lffg;->d:I

    .line 1956
    invoke-virtual {v6, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 1960
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_5
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1963
    invoke-virtual {v6}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 1964
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1965
    invoke-static {p0, p1, v6, v0}, Lffe;->a(Landroid/content/Context;ILjava/util/HashMap;Ljava/util/ArrayList;)V

    :cond_6
    move-object v5, v6

    .line 1968
    goto/16 :goto_0
.end method

.method private static e(Landroid/content/Context;)V
    .locals 14
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x2

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 695
    invoke-static {p0}, Ldhv;->g(Landroid/content/Context;)I

    move-result v0

    .line 696
    if-eq v0, v1, :cond_1

    .line 769
    :cond_0
    :goto_0
    return-void

    .line 700
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 702
    invoke-static {p0}, Lffe;->f(Landroid/content/Context;)Z

    move-result v9

    .line 703
    if-eqz v9, :cond_2

    .line 704
    const-string v1, "vnd.android.cursor.item/vnd.googleplus.profile.comm"

    move-object v6, v1

    .line 709
    :goto_1
    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "caller_is_syncadapter"

    const-string v3, "true"

    .line 710
    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v10

    .line 712
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 718
    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lffe;->R:[Ljava/lang/String;

    const-string v3, "account_type=\'com.google\' AND data_set=\'plus\' AND mimetype IN (\'vnd.android.cursor.item/vnd.googleplus.profile\',\'vnd.android.cursor.item/vnd.googleplus.profile.comm\') AND data5 IN (\'conversation\',\'hangout\')"

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 728
    if-eqz v1, :cond_0

    .line 733
    :goto_2
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 734
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 735
    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 736
    const/4 v0, 0x2

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 737
    const/4 v0, 0x3

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 739
    const-string v0, "conversation"

    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 740
    const v0, 0x7f0a0880

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 748
    :goto_3
    invoke-static {v10, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    .line 747
    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    .line 748
    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    .line 747
    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 749
    invoke-static {v10}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v3, "raw_contact_id"

    .line 750
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v3, "mimetype"

    .line 751
    invoke-virtual {v2, v3, v6}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v3, "data5"

    .line 752
    invoke-virtual {v2, v3, v12}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v3, "data4"

    .line 753
    invoke-virtual {v2, v3, v13}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v3, "data3"

    .line 754
    invoke-virtual {v2, v3, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    .line 755
    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    .line 749
    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 757
    const/16 v0, 0x80

    const/4 v2, 0x0

    invoke-static {p0, v11, v0, v2}, Lffe;->a(Landroid/content/Context;Ljava/util/ArrayList;IZ)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 761
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 706
    :cond_2
    const-string v1, "vnd.android.cursor.item/vnd.googleplus.profile"

    move-object v6, v1

    goto/16 :goto_1

    .line 741
    :cond_3
    :try_start_1
    const-string v0, "hangout"

    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 742
    const v0, 0x7f0a0881

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 744
    :cond_4
    const/4 v0, 0x4

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_3

    .line 761
    :cond_5
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 764
    invoke-static {p0, v11, v7}, Lffe;->a(Landroid/content/Context;Ljava/util/ArrayList;Z)[Landroid/content/ContentProviderResult;

    .line 766
    if-eqz v9, :cond_6

    move v0, v7

    :goto_4
    invoke-static {p0, v0}, Ldhv;->s(Landroid/content/Context;I)V

    goto/16 :goto_0

    :cond_6
    move v0, v8

    goto :goto_4
.end method

.method private static e(Landroid/content/Context;ILkfp;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2551
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 2552
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2553
    const-string v1, "Android:DeleteCircles"

    invoke-virtual {p2, v1}, Lkfp;->c(Ljava/lang/String;)V

    .line 2554
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v0}, Lffe;->e(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 2555
    invoke-virtual {p2, v0}, Lkfp;->l(I)V

    .line 2556
    invoke-virtual {p2}, Lkfp;->f()V

    .line 2557
    return-void
.end method

.method private static f(Landroid/content/Context;I)Ljava/util/HashMap;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lffg;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 2215
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2216
    const-class v1, Lhei;

    invoke-static {p0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhei;

    .line 2217
    invoke-interface {v1, p1}, Lhei;->a(I)Lhej;

    move-result-object v1

    const-string v2, "account_name"

    invoke-interface {v1, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2218
    invoke-static {v1}, Lffe;->d(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lffe;->P:[Ljava/lang/String;

    const-string v3, "(mimetype=\'vnd.android.cursor.item/photo\' OR mimetype=\'vnd.android.cursor.item/vnd.googleplus.profile\' OR mimetype=\'vnd.android.cursor.item/vnd.googleplus.profile\') AND (sync2=0 OR sync2 IS NULL)"

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 2220
    if-nez v2, :cond_0

    .line 2270
    :goto_0
    return-object v4

    .line 2224
    :cond_0
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 2226
    :cond_1
    :goto_1
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2227
    const/4 v1, 0x1

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2228
    invoke-static {v1}, Ljpu;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2229
    if-eqz v3, :cond_1

    .line 2230
    invoke-virtual {v6, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lffg;

    .line 2231
    if-nez v1, :cond_2

    .line 2232
    new-instance v1, Lffg;

    invoke-direct {v1}, Lffg;-><init>()V

    .line 2233
    iput-object v3, v1, Lffg;->b:Ljava/lang/String;

    .line 2234
    const/4 v5, 0x0

    invoke-interface {v2, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    iput-wide v8, v1, Lffg;->a:J

    .line 2235
    const/4 v5, 0x4

    invoke-interface {v2, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    iput v5, v1, Lffg;->d:I

    .line 2236
    invoke-virtual {v6, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2239
    :cond_2
    const/4 v3, 0x3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2240
    const-string v5, "vnd.android.cursor.item/photo"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2241
    const/4 v3, 0x2

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    iput-wide v8, v1, Lffg;->e:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 2246
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_3
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 2249
    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->d:Landroid/net/Uri;

    invoke-static {v1, p1}, Lcom/google/android/apps/plus/content/EsProvider;->a(Landroid/net/Uri;I)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lffe;->O:[Ljava/lang/String;

    const-string v3, "in_my_circles=1"

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2252
    :cond_4
    :goto_2
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2253
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2254
    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2255
    invoke-static {v3}, Lffe;->a(Ljava/lang/String;)I

    move-result v4

    .line 2256
    invoke-virtual {v6, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lffg;

    .line 2257
    if-eqz v0, :cond_4

    .line 2258
    iget v5, v0, Lffg;->d:I

    if-ne v5, v4, :cond_5

    .line 2259
    invoke-virtual {v6, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_2

    .line 2267
    :catchall_1
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 2261
    :cond_5
    :try_start_2
    iput v4, v0, Lffg;->d:I

    .line 2262
    iput-object v3, v0, Lffg;->c:Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_2

    .line 2267
    :cond_6
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move-object v4, v6

    .line 2270
    goto/16 :goto_0
.end method

.method private static f(Landroid/content/Context;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 772
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 773
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 774
    const-string v3, "android.intent.action.VIEW"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 775
    const-string v3, "vnd.android.cursor.item/vnd.googleplus.profile.comm"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 776
    const/high16 v3, 0x10000

    .line 777
    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    .line 778
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 779
    sput v0, Lffe;->V:I

    .line 784
    :goto_0
    sget v1, Lffe;->V:I

    if-ne v1, v0, :cond_1

    :goto_1
    return v0

    .line 781
    :cond_0
    const/4 v1, 0x2

    sput v1, Lffe;->V:I

    goto :goto_0

    .line 784
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static f(Landroid/content/Context;ILkfp;)Z
    .locals 1

    .prologue
    .line 2680
    invoke-static {p0}, Lffe;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2681
    invoke-static {p0, p1}, Ldhv;->j(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p2, :cond_0

    .line 2682
    invoke-virtual {p2}, Lkfp;->c()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
