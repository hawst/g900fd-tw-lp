.class public final Lkmh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private synthetic a:Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;)V
    .locals 0

    .prologue
    .line 344
    iput-object p1, p0, Lkmh;->a:Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 347
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 349
    const v1, 0x7f100598

    if-eq v0, v1, :cond_0

    const v1, 0x7f10059b

    if-ne v0, v1, :cond_3

    .line 353
    :cond_0
    iget-object v0, p0, Lkmh;->a:Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;

    iget-object v0, v0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->s:Ljbx;

    invoke-virtual {v0}, Ljbx;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 354
    iget-object v0, p0, Lkmh;->a:Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;

    iget-object v0, v0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->s:Ljbx;

    invoke-virtual {v0, v2}, Ljbx;->a(I)V

    .line 355
    iget-object v0, p0, Lkmh;->a:Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->u()V

    .line 357
    :cond_1
    iget-object v0, p0, Lkmh;->a:Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->a(Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;I)V

    .line 389
    :cond_2
    :goto_0
    return-void

    .line 358
    :cond_3
    const v1, 0x7f100599

    if-ne v0, v1, :cond_4

    .line 359
    iget-object v0, p0, Lkmh;->a:Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;

    invoke-static {v0, v2}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->a(Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;I)V

    goto :goto_0

    .line 360
    :cond_4
    const v1, 0x7f10059f

    if-ne v0, v1, :cond_5

    .line 361
    iget-object v0, p0, Lkmh;->a:Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->D()V

    goto :goto_0

    .line 362
    :cond_5
    const v1, 0x7f1005a2

    if-ne v0, v1, :cond_6

    .line 363
    iget-object v0, p0, Lkmh;->a:Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->H()V

    goto :goto_0

    .line 364
    :cond_6
    const v1, 0x7f10059c

    if-ne v0, v1, :cond_7

    .line 365
    iget-object v0, p0, Lkmh;->a:Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;

    invoke-static {v0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->b(Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;)V

    goto :goto_0

    .line 366
    :cond_7
    const v1, 0x7f100561

    if-eq v0, v1, :cond_8

    const v1, 0x7f100563

    if-eq v0, v1, :cond_8

    const v1, 0x7f100562

    if-ne v0, v1, :cond_9

    .line 369
    :cond_8
    iget-object v0, p0, Lkmh;->a:Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;

    invoke-static {v0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->c(Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;)Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->a()Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->x()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_2

    .line 372
    iget-object v0, p0, Lkmh;->a:Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;

    invoke-static {v0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->d(Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;)V

    .line 373
    iget-object v0, p0, Lkmh;->a:Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->v()V

    .line 374
    iget-object v0, p0, Lkmh;->a:Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;

    invoke-static {v0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->c(Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;)Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->a()Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->Y()V

    goto :goto_0

    .line 375
    :cond_9
    const v1, 0x7f10059a

    if-ne v0, v1, :cond_a

    .line 376
    iget-object v0, p0, Lkmh;->a:Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;

    const-string v1, "clipboard"

    .line 377
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/ClipboardManager;

    .line 378
    iget-object v1, p0, Lkmh;->a:Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;

    invoke-virtual {v0}, Landroid/text/ClipboardManager;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->a(Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;Ljava/lang/CharSequence;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 380
    :cond_a
    const v1, 0x7f10059d

    if-ne v0, v1, :cond_b

    .line 381
    iget-object v0, p0, Lkmh;->a:Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;

    invoke-static {v0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->e(Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;)V

    goto/16 :goto_0

    .line 382
    :cond_b
    const v1, 0x7f100573

    if-eq v0, v1, :cond_c

    const v1, 0x7f100594

    if-ne v0, v1, :cond_d

    .line 384
    :cond_c
    iget-object v0, p0, Lkmh;->a:Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;

    invoke-static {v0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->d(Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;)V

    goto/16 :goto_0

    .line 385
    :cond_d
    const v1, 0x7f10058b

    if-ne v0, v1, :cond_2

    .line 386
    iget-object v0, p0, Lkmh;->a:Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->v()V

    .line 387
    iget-object v0, p0, Lkmh;->a:Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;

    invoke-static {v0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->c(Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;)Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->a()Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->Y()V

    goto/16 :goto_0
.end method
