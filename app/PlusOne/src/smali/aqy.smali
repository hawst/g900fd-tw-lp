.class public Laqy;
.super Lamn;
.source "PG"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Larm;

.field private final c:Larh;

.field private final d:Landroid/content/Context;

.field private final e:Lawh;

.field private final f:Laqa;

.field private final g:Ljfb;

.field private final h:Larj;

.field private final j:Laro;

.field private final k:Lcdu;

.field private final l:Laqb;

.field private final m:Lanh;

.field private final n:Latw;

.field private final o:Lark;

.field private final p:Ljava/util/concurrent/Executor;

.field private final q:Lawk;

.field private final r:Ljava/lang/Runnable;

.field private final s:Lasn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lasn",
            "<",
            "Larl;",
            ">;"
        }
    .end annotation
.end field

.field private final t:Lasn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lasn",
            "<",
            "Larg;",
            ">;"
        }
    .end annotation
.end field

.field private u:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    const-class v0, Laqy;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Laqy;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lawh;Laqa;Ljfb;Lbjp;Lbjp;Lbgf;Lbte;Lbjf;Lbig;Laro;Larj;Lchl;Lald;Lamy;Lasy;Lcdu;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Laqb;Lanh;Lark;Z)V
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lawh;",
            "Laqa;",
            "Ljfb;",
            "Lbjp",
            "<",
            "Lbhk;",
            ">;",
            "Lbjp",
            "<",
            "Lbhl;",
            ">;",
            "Lbgf;",
            "Lbte;",
            "Lbjf;",
            "Lbig;",
            "Laro;",
            "Larj;",
            "Lchl;",
            "Lald;",
            "Lamy;",
            "Lasy;",
            "Lcdu;",
            "Ljava/util/concurrent/Executor;",
            "Ljava/util/concurrent/Executor;",
            "Laqb;",
            "Lanh;",
            "Lark;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 219
    invoke-direct/range {p0 .. p0}, Lamn;-><init>()V

    .line 128
    new-instance v1, Larf;

    move-object/from16 v0, p0

    invoke-direct {v1, v0}, Larf;-><init>(Laqy;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Laqy;->b:Larm;

    .line 129
    new-instance v1, Lare;

    move-object/from16 v0, p0

    invoke-direct {v1, v0}, Lare;-><init>(Laqy;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Laqy;->c:Larh;

    .line 146
    new-instance v1, Laqz;

    move-object/from16 v0, p0

    invoke-direct {v1, v0}, Laqz;-><init>(Laqy;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Laqy;->q:Lawk;

    .line 156
    new-instance v1, Lari;

    move-object/from16 v0, p0

    invoke-direct {v1, v0}, Lari;-><init>(Laqy;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Laqy;->r:Ljava/lang/Runnable;

    .line 163
    new-instance v1, Lara;

    const-class v2, Larl;

    move-object/from16 v0, p0

    invoke-direct {v1, v0, v2}, Lara;-><init>(Laqy;Ljava/lang/Class;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Laqy;->s:Lasn;

    .line 181
    new-instance v1, Larb;

    const-class v2, Larg;

    move-object/from16 v0, p0

    invoke-direct {v1, v0, v2}, Larb;-><init>(Laqy;Ljava/lang/Class;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Laqy;->t:Lasn;

    .line 220
    const-string v1, "context"

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    move-object/from16 v0, p0

    iput-object v1, v0, Laqy;->d:Landroid/content/Context;

    .line 221
    const-string v1, "state"

    const/4 v2, 0x0

    move-object/from16 v0, p2

    invoke-static {v0, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lawh;

    move-object/from16 v0, p0

    iput-object v1, v0, Laqy;->e:Lawh;

    .line 222
    const-string v1, "notificationsHelper"

    const/4 v2, 0x0

    move-object/from16 v0, p3

    invoke-static {v0, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Laqa;

    move-object/from16 v0, p0

    iput-object v1, v0, Laqy;->f:Laqa;

    .line 223
    const-string v1, "movieMakerProvider"

    const/4 v2, 0x0

    move-object/from16 v0, p4

    invoke-static {v0, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljfb;

    move-object/from16 v0, p0

    iput-object v1, v0, Laqy;->g:Ljfb;

    .line 224
    const-string v1, "display"

    const/4 v2, 0x0

    move-object/from16 v0, p12

    invoke-static {v0, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Larj;

    move-object/from16 v0, p0

    iput-object v1, v0, Laqy;->h:Larj;

    .line 225
    const-string v1, "saveVideoNameProvider"

    .line 226
    const/4 v2, 0x0

    move-object/from16 v0, p11

    invoke-static {v0, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Laro;

    move-object/from16 v0, p0

    iput-object v1, v0, Laqy;->j:Laro;

    .line 227
    const-string v1, "analyticsSession"

    const/4 v2, 0x0

    move-object/from16 v0, p17

    invoke-static {v0, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcdu;

    move-object/from16 v0, p0

    iput-object v1, v0, Laqy;->k:Lcdu;

    .line 228
    const-string v1, "onInvalidUriDetectedListener"

    const/4 v2, 0x0

    move-object/from16 v0, p20

    invoke-static {v0, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Laqb;

    move-object/from16 v0, p0

    iput-object v1, v0, Laqy;->l:Laqb;

    .line 230
    const-string v1, "gservicesSettings"

    const/4 v2, 0x0

    move-object/from16 v0, p21

    invoke-static {v0, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lanh;

    move-object/from16 v0, p0

    iput-object v1, v0, Laqy;->m:Lanh;

    .line 231
    const-string v1, "shareIntentStarter"

    const/4 v2, 0x0

    move-object/from16 v0, p22

    invoke-static {v0, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lark;

    move-object/from16 v0, p0

    iput-object v1, v0, Laqy;->o:Lark;

    .line 232
    const-string v1, "mainThreadExecutor"

    const/4 v2, 0x0

    move-object/from16 v0, p18

    invoke-static {v0, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Executor;

    move-object/from16 v0, p0

    iput-object v1, v0, Laqy;->p:Ljava/util/concurrent/Executor;

    .line 234
    new-instance v1, Latw;

    new-instance v15, Larn;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Larn;-><init>(Laqy;)V

    move-object/from16 v2, p1

    move-object/from16 v3, p5

    move-object/from16 v4, p6

    move-object/from16 v5, p7

    move-object/from16 v6, p8

    move-object/from16 v7, p16

    move-object/from16 v8, p14

    move-object/from16 v9, p15

    move-object/from16 v10, p13

    move-object/from16 v11, p19

    move-object/from16 v12, p18

    move-object/from16 v13, p9

    move-object/from16 v14, p10

    move/from16 v16, p23

    invoke-direct/range {v1 .. v16}, Latw;-><init>(Landroid/content/Context;Lbjp;Lbjp;Lbgf;Lbte;Lasy;Lald;Lamy;Lchl;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lbjf;Lbig;Latx;Z)V

    move-object/from16 v0, p0

    iput-object v1, v0, Laqy;->n:Latw;

    .line 250
    return-void
.end method

.method static synthetic a(Laqy;Landroid/net/Uri;J)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 57
    const-string v0, "timestampMs"

    invoke-static {p2, p3, v0}, Lcec;->b(JLjava/lang/CharSequence;)J

    new-instance v0, Landroid/content/ContentValues;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/content/ContentValues;-><init>(I)V

    const-string v1, "datetaken"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    iget-object v1, p0, Laqy;->d:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v1, p1, v0, v3, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method static synthetic a(Laqy;Larg;)V
    .locals 1

    .prologue
    .line 57
    invoke-virtual {p0}, Laqy;->e()V

    iget-object v0, p0, Laqy;->c:Larh;

    invoke-interface {p1, v0}, Larg;->a(Larh;)V

    iget-object v0, p0, Laqy;->e:Lawh;

    invoke-interface {v0}, Lawh;->az()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Laqy;->e:Lawh;

    invoke-interface {v0}, Lawh;->aB()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Larg;->t_()V

    :cond_0
    return-void
.end method

.method static synthetic a(Laqy;Larl;)V
    .locals 1

    .prologue
    .line 57
    invoke-virtual {p0}, Laqy;->e()V

    iget-object v0, p0, Laqy;->b:Larm;

    invoke-interface {p1, v0}, Larl;->a(Larm;)V

    return-void
.end method

.method static synthetic a(Laqy;)Z
    .locals 1

    .prologue
    .line 57
    iget-boolean v0, p0, Laqy;->u:Z

    return v0
.end method

.method static synthetic a(Laqy;Z)Z
    .locals 0

    .prologue
    .line 57
    iput-boolean p1, p0, Laqy;->u:Z

    return p1
.end method

.method static synthetic b(Laqy;)Lawh;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Laqy;->e:Lawh;

    return-object v0
.end method

.method static synthetic b(Laqy;Larg;)V
    .locals 1

    .prologue
    .line 57
    invoke-virtual {p0}, Laqy;->e()V

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Larg;->a(Larh;)V

    return-void
.end method

.method static synthetic b(Laqy;Larl;)V
    .locals 1

    .prologue
    .line 57
    invoke-virtual {p0}, Laqy;->e()V

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Larl;->a(Larm;)V

    return-void
.end method

.method static synthetic c(Laqy;)Lcdu;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Laqy;->k:Lcdu;

    return-object v0
.end method

.method static synthetic d(Laqy;)Ljfb;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Laqy;->g:Ljfb;

    return-object v0
.end method

.method static synthetic e(Laqy;)Lark;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Laqy;->o:Lark;

    return-object v0
.end method

.method static synthetic f(Laqy;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Laqy;->r:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic g(Laqy;)Ljava/util/concurrent/Executor;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Laqy;->p:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method static synthetic h(Laqy;)Lasn;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Laqy;->t:Lasn;

    return-object v0
.end method

.method static synthetic i(Laqy;)Larj;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Laqy;->h:Larj;

    return-object v0
.end method

.method static synthetic i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    sget-object v0, Laqy;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic j(Laqy;)Lasn;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Laqy;->s:Lasn;

    return-object v0
.end method

.method private j()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 405
    iget-object v0, p0, Laqy;->e:Lawh;

    invoke-interface {v0}, Lawh;->aJ()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 420
    :goto_0
    return v0

    .line 410
    :cond_0
    iget-object v0, p0, Laqy;->e:Lawh;

    invoke-interface {v0}, Lawh;->F()Lboi;

    move-result-object v0

    .line 411
    iget-object v3, p0, Laqy;->m:Lanh;

    invoke-virtual {v3}, Lanh;->X()I

    move-result v3

    .line 412
    invoke-virtual {v0}, Lboi;->q()I

    move-result v0

    const-string v4, "storyboard.getMaxInputVideoHeight()"

    invoke-static {v0, v4}, Lcec;->a(ILjava/lang/CharSequence;)I

    move-result v0

    const/16 v4, 0x258

    if-lt v0, v4, :cond_1

    if-lez v3, :cond_2

    const/16 v4, 0x2d0

    if-ge v3, v4, :cond_2

    :cond_1
    sget-object v0, Latt;->a:[Latu;

    .line 413
    :goto_1
    array-length v3, v0

    if-ne v3, v1, :cond_5

    .line 415
    iget-object v3, p0, Laqy;->e:Lawh;

    aget-object v0, v0, v2

    invoke-interface {v3, v0}, Lawh;->a(Latu;)V

    move v0, v1

    .line 416
    goto :goto_0

    .line 412
    :cond_2
    const/16 v4, 0x384

    if-lt v0, v4, :cond_3

    if-lez v3, :cond_4

    const/16 v0, 0x438

    if-ge v3, v0, :cond_4

    :cond_3
    sget-object v0, Latt;->b:[Latu;

    goto :goto_1

    :cond_4
    sget-object v0, Latt;->c:[Latu;

    goto :goto_1

    .line 419
    :cond_5
    iget-object v1, p0, Laqy;->h:Larj;

    invoke-interface {v1, v0}, Larj;->a([Latu;)V

    move v0, v2

    .line 420
    goto :goto_0
.end method

.method private k()V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 504
    iget-object v0, p0, Laqy;->e:Lawh;

    invoke-interface {v0, v4}, Lawh;->b(Landroid/content/Intent;)V

    .line 505
    iget-object v0, p0, Laqy;->t:Lasn;

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Larg;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Larg;->n(Z)V

    .line 509
    iget-object v0, p0, Laqy;->e:Lawh;

    invoke-interface {v0}, Lawh;->aG()Ljava/lang/String;

    move-result-object v6

    .line 510
    iget-object v0, p0, Laqy;->n:Latw;

    iget-object v1, p0, Laqy;->e:Lawh;

    .line 511
    invoke-interface {v1}, Lawh;->F()Lboi;

    move-result-object v1

    iget-object v2, p0, Laqy;->e:Lawh;

    .line 512
    invoke-interface {v2}, Lawh;->aI()Latu;

    move-result-object v2

    new-instance v3, Ljava/io/File;

    iget-object v5, p0, Laqy;->e:Lawh;

    .line 513
    invoke-interface {v5}, Lawh;->aF()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    if-nez v6, :cond_0

    move-object v5, v4

    :goto_0
    iget-object v6, p0, Laqy;->m:Lanh;

    .line 516
    invoke-virtual {v6}, Lanh;->j()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Laqy;->m:Lanh;

    .line 517
    invoke-virtual {v7}, Lanh;->k()Z

    move-result v7

    .line 510
    invoke-virtual/range {v0 .. v7}, Latw;->a(Lboi;Latu;Ljava/io/File;Ljava/io/File;Ljava/io/File;Ljava/lang/String;Z)V

    .line 518
    return-void

    .line 513
    :cond_0
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic k(Laqy;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Laqy;->k()V

    return-void
.end method

.method private l()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 526
    new-instance v1, Ljava/io/File;

    iget-object v0, p0, Laqy;->e:Lawh;

    .line 527
    invoke-interface {v0}, Lawh;->aE()Ljava/lang/String;

    move-result-object v0

    const-string v2, "mState.getSavingOutputFileName()"

    .line 526
    invoke-static {v0, v2, v3}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 528
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 530
    new-instance v1, Ljava/io/File;

    iget-object v0, p0, Laqy;->e:Lawh;

    .line 531
    invoke-interface {v0}, Lawh;->aF()Ljava/lang/String;

    move-result-object v0

    const-string v2, "mState.getTemporarySavingOutputFileName()"

    .line 530
    invoke-static {v0, v2, v3}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 533
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 535
    iget-object v0, p0, Laqy;->e:Lawh;

    invoke-interface {v0}, Lawh;->aG()Ljava/lang/String;

    move-result-object v0

    .line 536
    if-eqz v0, :cond_0

    .line 537
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 539
    :cond_0
    return-void
.end method

.method static synthetic l(Laqy;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Laqy;->l()V

    return-void
.end method

.method static synthetic m(Laqy;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Laqy;->d:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic n(Laqy;)Laqb;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Laqy;->l:Laqb;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 254
    invoke-super {p0}, Lamn;->a()V

    .line 269
    iget-object v0, p0, Laqy;->e:Lawh;

    iget-object v1, p0, Laqy;->q:Lawk;

    invoke-interface {v0, v1}, Lawh;->i(Lawk;)V

    .line 271
    iget-object v0, p0, Laqy;->f:Laqa;

    const v1, 0x7f100002

    invoke-interface {v0, v1}, Laqa;->a(I)V

    .line 273
    iget-object v0, p0, Laqy;->t:Lasn;

    invoke-virtual {v0}, Lasn;->a()V

    .line 274
    iget-object v0, p0, Laqy;->s:Lasn;

    invoke-virtual {v0}, Lasn;->a()V

    .line 278
    iget-object v0, p0, Laqy;->e:Lawh;

    invoke-interface {v0}, Lawh;->az()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 279
    iget-object v0, p0, Laqy;->e:Lawh;

    invoke-interface {v0}, Lawh;->aD()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 280
    iget-object v0, p0, Laqy;->e:Lawh;

    invoke-interface {v0, v2}, Lawh;->l(Z)V

    .line 285
    :cond_0
    :goto_0
    return-void

    .line 281
    :cond_1
    iget-object v0, p0, Laqy;->e:Lawh;

    invoke-interface {v0}, Lawh;->aB()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 282
    iget-object v0, p0, Laqy;->e:Lawh;

    invoke-interface {v0, v2}, Lawh;->l(Z)V

    goto :goto_0
.end method

.method public a(Larg;)V
    .locals 1

    .prologue
    .line 355
    iget-object v0, p0, Laqy;->t:Lasn;

    invoke-virtual {v0, p1}, Lasn;->c(Ljava/lang/Object;)V

    .line 356
    return-void
.end method

.method public a(Larl;)V
    .locals 1

    .prologue
    .line 341
    iget-object v0, p0, Laqy;->s:Lasn;

    invoke-virtual {v0, p1}, Lasn;->c(Ljava/lang/Object;)V

    .line 342
    return-void
.end method

.method public a(Z)V
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 371
    invoke-virtual {p0}, Laqy;->e()V

    .line 372
    iget-object v0, p0, Laqy;->e:Lawh;

    invoke-interface {v0, v9}, Lawh;->l(Z)V

    .line 373
    iget-object v0, p0, Laqy;->e:Lawh;

    invoke-interface {v0}, Lawh;->aK()V

    .line 374
    iget-object v2, p0, Laqy;->e:Lawh;

    iget-object v0, p0, Laqy;->e:Lawh;

    invoke-interface {v0}, Lawh;->F()Lboi;

    move-result-object v0

    invoke-virtual {v0}, Lboi;->g()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v10, v11, v8}, Lboi;->b(JZ)Lbmd;

    move-result-object v0

    iget-object v0, v0, Lbmd;->e:Ljeg;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbml;

    invoke-interface {v0}, Lbml;->b()Lbmu;

    move-result-object v1

    iget-wide v4, v1, Lbmu;->b:J

    const-wide/16 v6, -0x1

    cmp-long v1, v4, v6

    if-eqz v1, :cond_0

    invoke-interface {v0}, Lbml;->b()Lbmu;

    move-result-object v1

    iget-wide v4, v1, Lbmu;->b:J

    cmp-long v1, v4, v10

    if-gez v1, :cond_2

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    :goto_0
    invoke-interface {v2, v0, v1}, Lawh;->g(J)V

    .line 375
    iget-object v0, p0, Laqy;->j:Laro;

    iget-object v1, p0, Laqy;->e:Lawh;

    invoke-interface {v1}, Lawh;->A()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Laro;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 376
    sget-object v1, Laqy;->a:Ljava/lang/String;

    iget-object v1, p0, Laqy;->e:Lawh;

    invoke-interface {v1}, Lawh;->A()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x15

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "saving to video ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] > "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 377
    iget-object v1, p0, Laqy;->e:Lawh;

    invoke-interface {v1, v0}, Lawh;->b(Ljava/lang/String;)V

    .line 378
    iget-object v1, p0, Laqy;->e:Lawh;

    const-string v2, "%s.tmp"

    new-array v3, v9, [Ljava/lang/Object;

    aput-object v0, v3, v8

    .line 379
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 378
    invoke-interface {v1, v0}, Lawh;->c(Ljava/lang/String;)V

    .line 380
    iget-object v0, p0, Laqy;->k:Lcdu;

    iget-object v1, p0, Laqy;->e:Lawh;

    invoke-interface {v1}, Lawh;->o()Lbza;

    move-result-object v1

    iget-object v2, p0, Laqy;->e:Lawh;

    invoke-interface {v2}, Lawh;->w()Lboh;

    move-result-object v2

    iget-object v3, p0, Laqy;->e:Lawh;

    .line 381
    invoke-interface {v3}, Lawh;->F()Lboi;

    move-result-object v3

    invoke-virtual {v3}, Lboi;->n()J

    move-result-wide v4

    move v3, p1

    .line 380
    invoke-virtual/range {v0 .. v5}, Lcdu;->a(Lbza;Lboh;ZJ)V

    .line 382
    invoke-direct {p0}, Laqy;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 383
    invoke-direct {p0}, Laqy;->k()V

    .line 385
    :cond_1
    return-void

    .line 374
    :cond_2
    invoke-interface {v0}, Lbml;->b()Lbmu;

    move-result-object v0

    iget-wide v0, v0, Lbmu;->b:J

    const-wide/16 v4, 0x3e8

    div-long/2addr v0, v4

    goto/16 :goto_0
.end method

.method public b()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 313
    iget-object v0, p0, Laqy;->e:Lawh;

    iget-object v1, p0, Laqy;->q:Lawk;

    invoke-interface {v0, v1}, Lawh;->j(Lawk;)V

    .line 317
    iget-object v0, p0, Laqy;->s:Lasn;

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Larl;

    invoke-interface {v0}, Larl;->a()V

    .line 318
    iget-object v0, p0, Laqy;->t:Lasn;

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Larg;

    invoke-interface {v0, v6}, Larg;->n(Z)V

    .line 320
    iget-object v0, p0, Laqy;->t:Lasn;

    invoke-virtual {v0}, Lasn;->b()V

    .line 321
    iget-object v0, p0, Laqy;->s:Lasn;

    invoke-virtual {v0}, Lasn;->b()V

    .line 323
    iget-object v0, p0, Laqy;->e:Lawh;

    invoke-interface {v0}, Lawh;->az()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 324
    iget-object v0, p0, Laqy;->e:Lawh;

    invoke-interface {v0}, Lawh;->aD()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Laqy;->e:Lawh;

    invoke-interface {v0}, Lawh;->aB()Z

    move-result v0

    if-nez v0, :cond_0

    .line 325
    iget-object v0, p0, Laqy;->e:Lawh;

    invoke-interface {v0, v7}, Lawh;->n(Z)V

    .line 327
    :cond_0
    iget-object v0, p0, Laqy;->e:Lawh;

    invoke-interface {v0}, Lawh;->aC()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 328
    iget-object v1, p0, Laqy;->f:Laqa;

    const v2, 0x7f100002

    .line 330
    new-instance v3, Landroid/app/Notification$Builder;

    iget-object v0, p0, Laqy;->d:Landroid/content/Context;

    invoke-direct {v3, v0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    const v0, 0x7f030001

    invoke-virtual {v3, v0}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    iget-object v0, p0, Laqy;->e:Lawh;

    invoke-interface {v0}, Lawh;->A()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Laqy;->d:Landroid/content/Context;

    const v4, 0x7f0a012b

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v3, v0}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    iget-object v0, p0, Laqy;->d:Landroid/content/Context;

    const v4, 0x7f0a0129

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    new-instance v0, Landroid/content/Intent;

    iget-object v4, p0, Laqy;->d:Landroid/content/Context;

    const-class v5, Lcom/google/android/apps/moviemaker/MovieMakerActivity;

    invoke-direct {v0, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v4, "android.intent.action.MAIN"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v4, "android.intent.category.LAUNCHER"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v4, "com.google.android.apps.moviemaker.app.ResumeSaving"

    invoke-virtual {v0, v4, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    iget-object v4, p0, Laqy;->d:Landroid/content/Context;

    const/high16 v5, 0x8000000

    invoke-static {v4, v6, v0, v5}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    const v4, 0x7f020320

    iget-object v5, p0, Laqy;->d:Landroid/content/Context;

    const v6, 0x7f0a012a

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5, v0}, Landroid/app/Notification$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    invoke-virtual {v3, v0}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    invoke-virtual {v3}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    .line 328
    invoke-interface {v1, v2, v0}, Laqa;->a(ILandroid/app/Notification;)V

    .line 333
    :cond_1
    invoke-virtual {p0}, Laqy;->d()V

    .line 334
    invoke-super {p0}, Lamn;->b()V

    .line 335
    return-void

    .line 330
    :cond_2
    iget-object v0, p0, Laqy;->e:Lawh;

    invoke-interface {v0}, Lawh;->A()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public b(Larg;)V
    .locals 1

    .prologue
    .line 362
    iget-object v0, p0, Laqy;->t:Lasn;

    invoke-virtual {v0, p1}, Lasn;->d(Ljava/lang/Object;)V

    .line 363
    return-void
.end method

.method public b(Larl;)V
    .locals 1

    .prologue
    .line 348
    iget-object v0, p0, Laqy;->s:Lasn;

    invoke-virtual {v0, p1}, Lasn;->d(Ljava/lang/Object;)V

    .line 349
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 291
    iget-object v0, p0, Laqy;->e:Lawh;

    invoke-interface {v0}, Lawh;->aC()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 292
    iget-object v0, p0, Laqy;->f:Laqa;

    const v1, 0x7f100002

    invoke-interface {v0, v1}, Laqa;->a(I)V

    .line 295
    :cond_0
    iget-object v0, p0, Laqy;->e:Lawh;

    invoke-interface {v0}, Lawh;->az()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 296
    iget-object v0, p0, Laqy;->e:Lawh;

    invoke-interface {v0}, Lawh;->aE()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 297
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Laqy;->e:Lawh;

    invoke-interface {v1}, Lawh;->aE()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 300
    :cond_1
    iget-object v0, p0, Laqy;->e:Lawh;

    invoke-interface {v0}, Lawh;->aG()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 301
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Laqy;->e:Lawh;

    invoke-interface {v1}, Lawh;->aG()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 304
    :cond_2
    iget-object v0, p0, Laqy;->e:Lawh;

    invoke-interface {v0}, Lawh;->aF()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 305
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Laqy;->e:Lawh;

    invoke-interface {v1}, Lawh;->aF()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 307
    :cond_3
    iget-object v0, p0, Laqy;->e:Lawh;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lawh;->l(Z)V

    .line 309
    :cond_4
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 388
    invoke-virtual {p0}, Laqy;->e()V

    .line 389
    iget-object v0, p0, Laqy;->n:Latw;

    invoke-virtual {v0}, Latw;->a()V

    .line 390
    return-void
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 393
    iget-object v0, p0, Laqy;->e:Lawh;

    invoke-interface {v0}, Lawh;->az()Z

    move-result v0

    return v0
.end method

.method public g()V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 454
    iget-object v0, p0, Laqy;->e:Lawh;

    invoke-interface {v0}, Lawh;->G()Z

    move-result v0

    if-nez v0, :cond_0

    .line 456
    iput-boolean v3, p0, Laqy;->u:Z

    .line 501
    :goto_0
    return-void

    .line 459
    :cond_0
    invoke-direct {p0}, Laqy;->j()Z

    move-result v0

    if-nez v0, :cond_1

    .line 461
    iget-object v0, p0, Laqy;->h:Larj;

    invoke-interface {v0}, Larj;->l()V

    goto :goto_0

    .line 464
    :cond_1
    iget-object v0, p0, Laqy;->e:Lawh;

    invoke-interface {v0}, Lawh;->aF()Ljava/lang/String;

    move-result-object v0

    .line 465
    if-nez v0, :cond_2

    .line 467
    sget-object v0, Laqy;->a:Ljava/lang/String;

    .line 468
    invoke-direct {p0}, Laqy;->l()V

    .line 469
    invoke-virtual {p0, v4}, Laqy;->a(Z)V

    goto :goto_0

    .line 472
    :cond_2
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 473
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_3

    .line 475
    sget-object v0, Laqy;->a:Ljava/lang/String;

    .line 476
    invoke-direct {p0}, Laqy;->l()V

    .line 477
    invoke-virtual {p0, v4}, Laqy;->a(Z)V

    goto :goto_0

    .line 483
    :cond_3
    iget-object v0, p0, Laqy;->e:Lawh;

    invoke-interface {v0}, Lawh;->aG()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 485
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Laqy;->e:Lawh;

    invoke-interface {v2}, Lawh;->aG()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 487
    :cond_4
    const-string v0, "%s.resume.tmp"

    new-array v2, v3, [Ljava/lang/Object;

    iget-object v3, p0, Laqy;->e:Lawh;

    .line 488
    invoke-interface {v3}, Lawh;->aE()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    .line 487
    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 489
    iget-object v2, p0, Laqy;->e:Lawh;

    invoke-interface {v2, v0}, Lawh;->d(Ljava/lang/String;)V

    .line 490
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 491
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 492
    invoke-virtual {v1, v2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 494
    sget-object v0, Laqy;->a:Ljava/lang/String;

    .line 495
    invoke-direct {p0}, Laqy;->l()V

    .line 496
    invoke-virtual {p0, v4}, Laqy;->a(Z)V

    goto :goto_0

    .line 499
    :cond_5
    iget-object v0, p0, Laqy;->h:Larj;

    invoke-interface {v0}, Larj;->l()V

    .line 500
    invoke-direct {p0}, Laqy;->k()V

    goto :goto_0
.end method

.method h()V
    .locals 2

    .prologue
    .line 622
    invoke-direct {p0}, Laqy;->l()V

    .line 623
    invoke-virtual {p0}, Laqy;->d()V

    .line 624
    iget-object v0, p0, Laqy;->e:Lawh;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lawh;->l(Z)V

    .line 625
    return-void
.end method
