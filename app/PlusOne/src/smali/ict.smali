.class public final enum Lict;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lict;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lict;

.field public static final enum b:Lict;

.field public static final enum c:Lict;

.field private static final synthetic d:[Lict;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 47
    new-instance v0, Lict;

    const-string v1, "LOADING"

    invoke-direct {v0, v1, v2}, Lict;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lict;->a:Lict;

    .line 49
    new-instance v0, Lict;

    const-string v1, "LOADED"

    invoke-direct {v0, v1, v3}, Lict;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lict;->b:Lict;

    .line 51
    new-instance v0, Lict;

    const-string v1, "EMPTY"

    invoke-direct {v0, v1, v4}, Lict;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lict;->c:Lict;

    .line 45
    const/4 v0, 0x3

    new-array v0, v0, [Lict;

    sget-object v1, Lict;->a:Lict;

    aput-object v1, v0, v2

    sget-object v1, Lict;->b:Lict;

    aput-object v1, v0, v3

    sget-object v1, Lict;->c:Lict;

    aput-object v1, v0, v4

    sput-object v0, Lict;->d:[Lict;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lict;
    .locals 1

    .prologue
    .line 45
    const-class v0, Lict;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lict;

    return-object v0
.end method

.method public static values()[Lict;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lict;->d:[Lict;

    invoke-virtual {v0}, [Lict;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lict;

    return-object v0
.end method
