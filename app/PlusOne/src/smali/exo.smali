.class public final Lexo;
.super Lhyd;
.source "PG"


# instance fields
.field private e:Landroid/view/View$OnClickListener;

.field private f:Lljv;

.field private g:Z

.field private h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/View$OnClickListener;Lljv;Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;Z)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 49
    invoke-direct {p0, p1, p2}, Lhyd;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    .line 51
    iput-object p3, p0, Lexo;->e:Landroid/view/View$OnClickListener;

    .line 53
    iput-object p4, p0, Lexo;->f:Lljv;

    .line 54
    iput-boolean p6, p0, Lexo;->h:Z

    .line 56
    const/4 v0, 0x1

    iput-boolean v0, p0, Lexo;->g:Z

    .line 57
    const/4 v0, 0x2

    invoke-virtual {p5, v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->c(I)V

    .line 58
    invoke-static {p1}, Llcm;->b(Landroid/content/Context;)I

    move-result v0

    invoke-virtual {p5, v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(I)V

    .line 60
    invoke-static {p1}, Llcm;->a(Landroid/content/Context;)I

    move-result v2

    .line 61
    invoke-virtual {p5, v2}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->b(I)V

    .line 62
    iget-boolean v0, p0, Lexo;->h:Z

    if-eqz v0, :cond_0

    neg-int v0, v2

    div-int/lit8 v0, v0, 0x2

    :goto_0
    invoke-virtual {p5, v2, v0, v2, v1}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->setPadding(IIII)V

    .line 65
    new-instance v0, Lexp;

    invoke-direct {v0}, Lexp;-><init>()V

    invoke-virtual {p5, v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(Llkg;)V

    .line 74
    return-void

    :cond_0
    move v0, v1

    .line 62
    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v6, 0x2

    const/4 v5, -0x2

    const/4 v4, -0x3

    const/4 v3, 0x1

    .line 177
    .line 178
    invoke-interface {p2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 203
    :goto_0
    return-object v0

    .line 180
    :pswitch_0
    const/4 v1, 0x0

    const/4 v2, 0x5

    invoke-static {p1, v0, v1, v2}, Llhx;->a(Landroid/content/Context;Landroid/util/AttributeSet;II)Landroid/widget/TextView;

    move-result-object v0

    .line 182
    check-cast p3, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    .line 183
    new-instance v1, Llka;

    .line 185
    invoke-virtual {p3}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->b()I

    move-result v2

    invoke-direct {v1, v6, v4, v2, v3}, Llka;-><init>(IIII)V

    .line 187
    iput v5, v1, Llka;->height:I

    .line 189
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 193
    :pswitch_1
    new-instance v0, Lfxm;

    invoke-direct {v0, p1}, Lfxm;-><init>(Landroid/content/Context;)V

    .line 194
    new-instance v1, Llka;

    invoke-direct {v1, v6, v4, v3, v3}, Llka;-><init>(IIII)V

    .line 198
    iget-boolean v2, p0, Lexo;->g:Z

    if-eqz v2, :cond_0

    .line 199
    iput v5, v1, Llka;->height:I

    .line 202
    :cond_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 178
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    .line 112
    invoke-interface {p3}, Landroid/database/Cursor;->getPosition()I

    move-result v1

    invoke-virtual {p0}, Lexo;->getCount()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 113
    invoke-interface {p3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 122
    :cond_0
    :goto_0
    return-void

    .line 115
    :pswitch_0
    check-cast p1, Landroid/widget/TextView;

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v3, :cond_1

    const v0, 0x7f0a0719

    :goto_1
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :cond_1
    const v0, 0x7f0a071a

    goto :goto_1

    .line 118
    :pswitch_1
    check-cast p1, Lfxm;

    const/4 v1, 0x5

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-eqz v1, :cond_2

    :goto_2
    const/4 v1, 0x3

    const/4 v2, 0x4

    invoke-static {p3, v1, v2}, Ldrm;->a(Landroid/database/Cursor;II)Lidh;

    move-result-object v1

    iget-object v2, p0, Lexo;->e:Landroid/view/View$OnClickListener;

    iget-object v3, p0, Lexo;->f:Lljv;

    invoke-virtual {p1, v2}, Lfxm;->a(Landroid/view/View$OnClickListener;)V

    const/4 v2, 0x6

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x7

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lhst;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v1, v0, v2, v3}, Lfxm;->a(Lidh;ZLjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    .line 113
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 149
    iget-boolean v0, p0, Lexo;->h:Z

    if-eqz v0, :cond_0

    .line 150
    invoke-super {p0}, Lhyd;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 152
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lhyd;->getCount()I

    move-result v0

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, -0x1

    .line 88
    iget-boolean v0, p0, Lexo;->h:Z

    if-eqz v0, :cond_1

    .line 89
    if-nez p1, :cond_0

    move v0, v1

    .line 107
    :goto_0
    return v0

    .line 92
    :cond_0
    add-int/lit8 p1, p1, -0x1

    .line 94
    :cond_1
    invoke-virtual {p0, p1}, Lexo;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 96
    if-nez v0, :cond_2

    move v0, v1

    .line 97
    goto :goto_0

    .line 100
    :cond_2
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 107
    invoke-super {p0, p1}, Lhyd;->getItemViewType(I)I

    move-result v0

    goto :goto_0

    .line 102
    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_1
    move v0, v2

    .line 104
    goto :goto_0

    .line 100
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    .line 158
    iget-boolean v0, p0, Lexo;->h:Z

    if-eqz v0, :cond_1

    .line 159
    if-nez p1, :cond_0

    .line 160
    new-instance v0, Landroid/view/View;

    iget-object v1, p0, Lexo;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 161
    check-cast p3, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    .line 162
    new-instance v1, Llka;

    const/4 v2, 0x2

    const/4 v3, -0x3

    .line 164
    invoke-virtual {p3}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->b()I

    move-result v4

    const/4 v5, 0x1

    invoke-direct {v1, v2, v3, v4, v5}, Llka;-><init>(IIII)V

    .line 166
    iget-object v2, p0, Lexo;->c:Landroid/content/Context;

    invoke-static {v2}, Ljgh;->a(Landroid/content/Context;)F

    move-result v2

    float-to-int v2, v2

    iput v2, v1, Llka;->height:I

    .line 167
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 172
    :goto_0
    return-object v0

    .line 170
    :cond_0
    add-int/lit8 p1, p1, -0x1

    .line 172
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lhyd;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x2

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x0

    return v0
.end method
