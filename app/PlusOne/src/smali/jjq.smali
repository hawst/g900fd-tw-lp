.class public Ljjq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljjp;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lhei;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Ljjq;->a:Landroid/content/Context;

    .line 35
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Ljjq;->b:Lhei;

    .line 36
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 123
    iget-object v0, p0, Ljjq;->b:Lhei;

    invoke-interface {v0, p1, p2}, Lhei;->b(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 124
    iget-object v1, p0, Ljjq;->b:Lhei;

    invoke-interface {v1, v0}, Lhei;->c(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 160
    :goto_0
    return-void

    .line 128
    :cond_0
    iget-object v1, p0, Ljjq;->b:Lhei;

    invoke-interface {v1, v0}, Lhei;->b(I)Lhek;

    move-result-object v0

    .line 129
    if-eqz p3, :cond_1

    .line 130
    const-string v1, "guns_registration_status"

    sget-object v2, Ljiw;->f:Ljiw;

    invoke-virtual {v2}, Ljiw;->a()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lhek;->b(Ljava/lang/String;I)Lhek;

    .line 131
    const-string v1, "guns_registration_retry_count"

    invoke-interface {v0, v1, v3}, Lhek;->b(Ljava/lang/String;I)Lhek;

    .line 151
    :goto_1
    invoke-interface {v0}, Lhek;->c()I

    .line 159
    new-instance v0, Ljkn;

    invoke-direct {v0}, Ljkn;-><init>()V

    iget-object v1, p0, Ljjq;->a:Landroid/content/Context;

    invoke-virtual {v0, v1, p1, p2}, Ljkn;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 133
    :cond_1
    const-string v1, "guns_registration_status"

    sget-object v2, Ljiw;->g:Ljiw;

    .line 134
    invoke-virtual {v2}, Ljiw;->a()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lhek;->a(Ljava/lang/String;I)I

    move-result v1

    .line 133
    invoke-static {v1}, Ljiw;->a(I)Ljiw;

    move-result-object v1

    .line 135
    sget-object v2, Ljjr;->a:[I

    invoke-virtual {v1}, Ljiw;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    goto :goto_1

    .line 141
    :pswitch_0
    const-string v1, "guns_registration_status"

    sget-object v2, Ljiw;->d:Ljiw;

    invoke-virtual {v2}, Ljiw;->a()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lhek;->b(Ljava/lang/String;I)Lhek;

    goto :goto_1

    .line 144
    :pswitch_1
    const-string v1, "guns_registration_retry_count"

    invoke-interface {v0, v1, v3}, Lhek;->a(Ljava/lang/String;I)I

    move-result v1

    .line 145
    const/16 v2, 0xa

    if-ne v1, v2, :cond_2

    .line 146
    const-string v1, "guns_registration_status"

    sget-object v2, Ljiw;->b:Ljiw;

    invoke-virtual {v2}, Ljiw;->a()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lhek;->b(Ljava/lang/String;I)Lhek;

    goto :goto_1

    .line 149
    :cond_2
    const-string v2, "guns_registration_retry_count"

    add-int/lit8 v1, v1, 0x1

    invoke-interface {v0, v2, v1}, Lhek;->b(Ljava/lang/String;I)Lhek;

    goto :goto_1

    .line 135
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 170
    iget-object v0, p0, Ljjq;->b:Lhei;

    invoke-interface {v0, p1, p2}, Lhei;->b(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 171
    iget-object v1, p0, Ljjq;->b:Lhei;

    invoke-interface {v1, v0}, Lhei;->c(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 207
    :goto_0
    return-void

    .line 175
    :cond_0
    iget-object v1, p0, Ljjq;->b:Lhei;

    invoke-interface {v1, v0}, Lhei;->b(I)Lhek;

    move-result-object v0

    .line 176
    if-eqz p3, :cond_1

    .line 177
    const-string v1, "guns_registration_status"

    sget-object v2, Ljiw;->a:Ljiw;

    invoke-virtual {v2}, Ljiw;->a()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lhek;->b(Ljava/lang/String;I)Lhek;

    .line 178
    const-string v1, "guns_registration_retry_count"

    invoke-interface {v0, v1, v3}, Lhek;->b(Ljava/lang/String;I)Lhek;

    .line 198
    :goto_1
    invoke-interface {v0}, Lhek;->c()I

    .line 206
    new-instance v0, Ljkn;

    invoke-direct {v0}, Ljkn;-><init>()V

    iget-object v1, p0, Ljjq;->a:Landroid/content/Context;

    invoke-virtual {v0, v1, p1, p2}, Ljkn;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 180
    :cond_1
    const-string v1, "guns_registration_status"

    sget-object v2, Ljiw;->g:Ljiw;

    .line 181
    invoke-virtual {v2}, Ljiw;->a()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lhek;->a(Ljava/lang/String;I)I

    move-result v1

    .line 180
    invoke-static {v1}, Ljiw;->a(I)Ljiw;

    move-result-object v1

    .line 182
    sget-object v2, Ljjr;->a:[I

    invoke-virtual {v1}, Ljiw;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_1

    .line 188
    :pswitch_1
    const-string v1, "guns_registration_status"

    sget-object v2, Ljiw;->e:Ljiw;

    invoke-virtual {v2}, Ljiw;->a()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lhek;->b(Ljava/lang/String;I)Lhek;

    goto :goto_1

    .line 191
    :pswitch_2
    const-string v1, "guns_registration_retry_count"

    invoke-interface {v0, v1, v3}, Lhek;->a(Ljava/lang/String;I)I

    move-result v1

    .line 192
    const/16 v2, 0xa

    if-lt v1, v2, :cond_2

    .line 193
    const-string v1, "guns_registration_status"

    sget-object v2, Ljiw;->c:Ljiw;

    invoke-virtual {v2}, Ljiw;->a()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lhek;->b(Ljava/lang/String;I)Lhek;

    goto :goto_1

    .line 196
    :cond_2
    const-string v2, "guns_registration_retry_count"

    add-int/lit8 v1, v1, 0x1

    invoke-interface {v0, v2, v1}, Lhek;->b(Ljava/lang/String;I)Lhek;

    goto :goto_1

    .line 182
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private c(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 219
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 220
    const-string v0, "NotsRegMgr"

    const-string v1, "Cannot handle registration: accountName or effectiveGaiaId must be present."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 273
    :cond_0
    :goto_0
    return-void

    .line 224
    :cond_1
    iget-object v0, p0, Ljjq;->b:Lhei;

    invoke-interface {v0, p1, p2}, Lhei;->b(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 225
    iget-object v1, p0, Ljjq;->b:Lhei;

    invoke-interface {v1, v0}, Lhei;->c(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 229
    new-instance v1, Ljjh;

    iget-object v2, p0, Ljjq;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, Ljjh;-><init>(Landroid/content/Context;)V

    .line 230
    invoke-virtual {v1}, Ljjh;->b()Ljava/lang/String;

    move-result-object v2

    .line 232
    if-eqz p3, :cond_2

    .line 234
    invoke-virtual {v1}, Ljjh;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 236
    sget-object v1, Ljiw;->f:Ljiw;

    sget-object v2, Ljiw;->d:Ljiw;

    invoke-virtual {p0, v1, v2}, Ljjq;->a(Ljiw;Ljiw;)V

    .line 239
    iget-object v1, p0, Ljjq;->b:Lhei;

    invoke-interface {v1, v0}, Lhei;->b(I)Lhek;

    move-result-object v0

    .line 240
    const-string v1, "guns_registration_status"

    sget-object v2, Ljiw;->d:Ljiw;

    invoke-virtual {v2}, Ljiw;->a()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lhek;->b(Ljava/lang/String;I)Lhek;

    move-result-object v0

    const-string v1, "guns_registration_retry_count"

    .line 241
    invoke-interface {v0, v1, v4}, Lhek;->b(Ljava/lang/String;I)Lhek;

    move-result-object v0

    .line 242
    invoke-interface {v0}, Lhek;->c()I

    .line 244
    invoke-virtual {p0}, Ljjq;->a()V

    goto :goto_0

    .line 249
    :cond_2
    if-nez v2, :cond_3

    .line 250
    const-string v0, "NotsRegMgr"

    const-string v1, "GCM Registration failed when registering."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 251
    invoke-virtual {p0, p1, p2}, Ljjq;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 255
    :cond_3
    invoke-virtual {p0, p1, p2}, Ljjq;->h(Ljava/lang/String;Ljava/lang/String;)Ljiw;

    move-result-object v0

    .line 256
    sget-object v1, Ljiw;->b:Ljiw;

    if-ne v0, v1, :cond_4

    if-eqz p3, :cond_4

    .line 258
    iget-object v0, p0, Ljjq;->b:Lhei;

    iget-object v1, p0, Ljjq;->b:Lhei;

    invoke-interface {v1, p1, p2}, Lhei;->b(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Lhei;->b(I)Lhek;

    move-result-object v0

    .line 260
    const-string v1, "guns_registration_status"

    sget-object v3, Ljiw;->d:Ljiw;

    invoke-virtual {v3}, Ljiw;->a()I

    move-result v3

    invoke-interface {v0, v1, v3}, Lhek;->b(Ljava/lang/String;I)Lhek;

    .line 261
    const-string v1, "guns_registration_retry_count"

    invoke-interface {v0, v1, v4}, Lhek;->b(Ljava/lang/String;I)Lhek;

    .line 262
    invoke-interface {v0}, Lhek;->c()I

    .line 265
    :cond_4
    new-instance v0, Lkfo;

    invoke-direct {v0, p1, p2}, Lkfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    new-instance v1, Ljjz;

    iget-object v3, p0, Ljjq;->a:Landroid/content/Context;

    invoke-direct {v1, v3, v0, v2}, Ljjz;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;)V

    .line 268
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 269
    const-string v0, "is_register_op"

    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 270
    iget-object v0, p0, Ljjq;->a:Landroid/content/Context;

    const-class v3, Ljjo;

    invoke-static {v0, v3}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljjo;

    iget-object v3, p0, Ljjq;->a:Landroid/content/Context;

    .line 271
    invoke-virtual {v0, v1, p0, v3, v2}, Ljjo;->a(Lkgg;Ljjp;Landroid/content/Context;Landroid/os/Bundle;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 338
    new-instance v0, Ljjh;

    iget-object v1, p0, Ljjq;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Ljjh;-><init>(Landroid/content/Context;)V

    .line 340
    invoke-virtual {v0}, Ljjh;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 342
    sget-object v0, Ljiw;->f:Ljiw;

    sget-object v1, Ljiw;->d:Ljiw;

    invoke-virtual {p0, v0, v1}, Ljjq;->a(Ljiw;Ljiw;)V

    .line 347
    :cond_0
    iget-object v0, p0, Ljjq;->b:Lhei;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "logged_in"

    aput-object v3, v1, v2

    invoke-interface {v0, v1}, Lhei;->a([Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 348
    iget-object v2, p0, Ljjq;->b:Lhei;

    invoke-interface {v2, v0}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 349
    const-string v2, "guns_registration_status"

    sget-object v3, Ljiw;->g:Ljiw;

    .line 350
    invoke-virtual {v3}, Ljiw;->a()I

    move-result v3

    invoke-interface {v0, v2, v3}, Lhej;->a(Ljava/lang/String;I)I

    move-result v2

    .line 349
    invoke-static {v2}, Ljiw;->a(I)Ljiw;

    move-result-object v2

    .line 351
    sget-object v3, Ljiw;->d:Ljiw;

    if-ne v2, v3, :cond_2

    .line 352
    const-string v2, "account_name"

    .line 353
    invoke-interface {v0, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "effective_gaia_id"

    .line 354
    invoke-interface {v0, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 352
    invoke-virtual {p0, v2, v0}, Ljjq;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 355
    :cond_2
    sget-object v3, Ljiw;->e:Ljiw;

    if-ne v2, v3, :cond_1

    .line 356
    const-string v2, "account_name"

    .line 357
    invoke-interface {v0, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "effective_gaia_id"

    invoke-interface {v0, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 356
    invoke-virtual {p0, v2, v0}, Ljjq;->g(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 360
    :cond_3
    return-void
.end method

.method a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Ljjq;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 83
    return-void
.end method

.method public a(Ljiw;Ljiw;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 321
    iget-object v0, p0, Ljjq;->b:Lhei;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "logged_in"

    aput-object v2, v1, v5

    invoke-interface {v0, v1}, Lhei;->a([Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 322
    iget-object v2, p0, Ljjq;->b:Lhei;

    invoke-interface {v2, v0}, Lhei;->a(I)Lhej;

    move-result-object v2

    .line 323
    const-string v3, "guns_registration_status"

    sget-object v4, Ljiw;->g:Ljiw;

    .line 324
    invoke-virtual {v4}, Ljiw;->a()I

    move-result v4

    invoke-interface {v2, v3, v4}, Lhej;->a(Ljava/lang/String;I)I

    move-result v2

    .line 323
    invoke-static {v2}, Ljiw;->a(I)Ljiw;

    move-result-object v2

    .line 325
    if-ne v2, p1, :cond_0

    .line 326
    iget-object v2, p0, Ljjq;->b:Lhei;

    invoke-interface {v2, v0}, Lhei;->b(I)Lhek;

    move-result-object v0

    .line 327
    const-string v2, "guns_registration_status"

    invoke-virtual {p2}, Ljiw;->a()I

    move-result v3

    invoke-interface {v0, v2, v3}, Lhek;->b(Ljava/lang/String;I)Lhek;

    move-result-object v0

    const-string v2, "guns_registration_retry_count"

    .line 328
    invoke-interface {v0, v2, v5}, Lhek;->b(Ljava/lang/String;I)Lhek;

    move-result-object v0

    .line 329
    invoke-interface {v0}, Lhek;->c()I

    goto :goto_0

    .line 332
    :cond_1
    return-void
.end method

.method public a(Lkgg;Landroid/content/Context;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 40
    iget-object v0, p1, Lkff;->g:Lkfo;

    .line 41
    invoke-virtual {v0}, Lkfo;->a()Ljava/lang/String;

    move-result-object v1

    .line 42
    invoke-virtual {v0}, Lkfo;->b()Ljava/lang/String;

    move-result-object v0

    .line 44
    const-string v2, "is_register_op"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 46
    check-cast p1, Ljjz;

    .line 47
    invoke-virtual {p1}, Ljjz;->t()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 48
    const-string v2, "NotsRegMgr"

    const-string v3, "Registration Failed"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    invoke-virtual {p0, v1, v0}, Ljjq;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    :goto_0
    return-void

    .line 53
    :cond_0
    invoke-virtual {p0, v1, v0}, Ljjq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 60
    :cond_1
    check-cast p1, Ljkq;

    .line 61
    invoke-virtual {p1}, Ljkq;->t()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 62
    const-string v2, "NotsRegMgr"

    const-string v3, "Failed to unregister."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    invoke-virtual {p0, v1, v0}, Ljjq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 67
    :cond_2
    invoke-virtual {p0, v1, v0}, Ljjq;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Ljjq;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 93
    return-void
.end method

.method c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 102
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Ljjq;->b(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 103
    return-void
.end method

.method d(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 112
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Ljjq;->b(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 113
    return-void
.end method

.method e(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 210
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Ljjq;->c(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 211
    return-void
.end method

.method f(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 214
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Ljjq;->c(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 215
    return-void
.end method

.method g(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 276
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 277
    const-string v0, "NotsRegMgr"

    const-string v1, "Cannot handle unregistration: accountName or effectiveGaiaId must be present."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 314
    :cond_0
    :goto_0
    return-void

    .line 281
    :cond_1
    iget-object v0, p0, Ljjq;->b:Lhei;

    invoke-interface {v0, p1, p2}, Lhei;->b(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 282
    iget-object v1, p0, Ljjq;->b:Lhei;

    invoke-interface {v1, v0}, Lhei;->c(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 286
    invoke-virtual {p0, p1, p2}, Ljjq;->h(Ljava/lang/String;Ljava/lang/String;)Ljiw;

    move-result-object v0

    .line 287
    sget-object v1, Ljiw;->a:Ljiw;

    if-eq v0, v1, :cond_0

    .line 290
    sget-object v1, Ljiw;->c:Ljiw;

    if-ne v0, v1, :cond_2

    .line 292
    iget-object v0, p0, Ljjq;->b:Lhei;

    iget-object v1, p0, Ljjq;->b:Lhei;

    invoke-interface {v1, p1, p2}, Lhei;->b(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Lhei;->b(I)Lhek;

    move-result-object v0

    .line 294
    const-string v1, "guns_registration_status"

    sget-object v2, Ljiw;->e:Ljiw;

    invoke-virtual {v2}, Ljiw;->a()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lhek;->b(Ljava/lang/String;I)Lhek;

    .line 295
    const-string v1, "guns_registration_retry_count"

    invoke-interface {v0, v1, v4}, Lhek;->b(Ljava/lang/String;I)Lhek;

    .line 296
    invoke-interface {v0}, Lhek;->c()I

    .line 299
    :cond_2
    new-instance v0, Ljjh;

    iget-object v1, p0, Ljjq;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Ljjh;-><init>(Landroid/content/Context;)V

    .line 300
    invoke-virtual {v0}, Ljjh;->b()Ljava/lang/String;

    move-result-object v0

    .line 301
    if-nez v0, :cond_3

    .line 302
    const-string v1, "NotsRegMgr"

    const-string v2, "Unregistration failed due to invalid gcm token"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 303
    invoke-virtual {p0, p1, p2}, Ljjq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    :cond_3
    new-instance v1, Lkfo;

    invoke-direct {v1, p1, p2}, Lkfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    new-instance v2, Ljkq;

    iget-object v3, p0, Ljjq;->a:Landroid/content/Context;

    invoke-direct {v2, v3, v1, v0}, Ljkq;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;)V

    .line 309
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 310
    const-string v0, "is_register_op"

    invoke-virtual {v1, v0, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 311
    iget-object v0, p0, Ljjq;->a:Landroid/content/Context;

    const-class v3, Ljjo;

    invoke-static {v0, v3}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljjo;

    iget-object v3, p0, Ljjq;->a:Landroid/content/Context;

    .line 312
    invoke-virtual {v0, v2, p0, v3, v1}, Ljjo;->a(Lkgg;Ljjp;Landroid/content/Context;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public h(Ljava/lang/String;Ljava/lang/String;)Ljiw;
    .locals 3

    .prologue
    .line 363
    iget-object v0, p0, Ljjq;->b:Lhei;

    invoke-interface {v0, p1, p2}, Lhei;->b(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 364
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 365
    iget-object v1, p0, Ljjq;->b:Lhei;

    .line 366
    invoke-interface {v1, v0}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "guns_registration_status"

    sget-object v2, Ljiw;->g:Ljiw;

    .line 367
    invoke-virtual {v2}, Ljiw;->a()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lhej;->a(Ljava/lang/String;I)I

    move-result v0

    .line 365
    invoke-static {v0}, Ljiw;->a(I)Ljiw;

    move-result-object v0

    .line 370
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Ljiw;->g:Ljiw;

    goto :goto_0
.end method
