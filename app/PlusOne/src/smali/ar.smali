.class final Lar;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lar;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field a:Landroid/os/Bundle;

.field b:Lu;

.field private c:Ljava/lang/String;

.field private d:I

.field private e:Z

.field private f:I

.field private g:I

.field private h:Ljava/lang/String;

.field private i:Z

.field private j:Z

.field private k:Landroid/os/Bundle;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 140
    new-instance v0, Las;

    invoke-direct {v0}, Las;-><init>()V

    sput-object v0, Lar;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lar;->c:Ljava/lang/String;

    .line 81
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lar;->d:I

    .line 82
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lar;->e:Z

    .line 83
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lar;->f:I

    .line 84
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lar;->g:I

    .line 85
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lar;->h:Ljava/lang/String;

    .line 86
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lar;->i:Z

    .line 87
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2

    :goto_2
    iput-boolean v1, p0, Lar;->j:Z

    .line 88
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lar;->k:Landroid/os/Bundle;

    .line 89
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lar;->a:Landroid/os/Bundle;

    .line 90
    return-void

    :cond_0
    move v0, v2

    .line 82
    goto :goto_0

    :cond_1
    move v0, v2

    .line 86
    goto :goto_1

    :cond_2
    move v1, v2

    .line 87
    goto :goto_2
.end method

.method public constructor <init>(Lu;)V
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lar;->c:Ljava/lang/String;

    .line 69
    iget v0, p1, Lu;->f:I

    iput v0, p0, Lar;->d:I

    .line 70
    iget-boolean v0, p1, Lu;->o:Z

    iput-boolean v0, p0, Lar;->e:Z

    .line 71
    iget v0, p1, Lu;->w:I

    iput v0, p0, Lar;->f:I

    .line 72
    iget v0, p1, Lu;->x:I

    iput v0, p0, Lar;->g:I

    .line 73
    iget-object v0, p1, Lu;->y:Ljava/lang/String;

    iput-object v0, p0, Lar;->h:Ljava/lang/String;

    .line 74
    iget-boolean v0, p1, Lu;->B:Z

    iput-boolean v0, p0, Lar;->i:Z

    .line 75
    iget-boolean v0, p1, Lu;->A:Z

    iput-boolean v0, p0, Lar;->j:Z

    .line 76
    iget-object v0, p1, Lu;->h:Landroid/os/Bundle;

    iput-object v0, p0, Lar;->k:Landroid/os/Bundle;

    .line 77
    return-void
.end method


# virtual methods
.method public a(Lz;Lu;)Lu;
    .locals 2

    .prologue
    .line 93
    iget-object v0, p0, Lar;->b:Lu;

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lar;->b:Lu;

    .line 117
    :goto_0
    return-object v0

    .line 97
    :cond_0
    iget-object v0, p0, Lar;->k:Landroid/os/Bundle;

    if-eqz v0, :cond_1

    .line 98
    iget-object v0, p0, Lar;->k:Landroid/os/Bundle;

    invoke-virtual {p1}, Lz;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 101
    :cond_1
    iget-object v0, p0, Lar;->c:Ljava/lang/String;

    iget-object v1, p0, Lar;->k:Landroid/os/Bundle;

    invoke-static {p1, v0, v1}, Lu;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Lu;

    move-result-object v0

    iput-object v0, p0, Lar;->b:Lu;

    .line 103
    iget-object v0, p0, Lar;->a:Landroid/os/Bundle;

    if-eqz v0, :cond_2

    .line 104
    iget-object v0, p0, Lar;->a:Landroid/os/Bundle;

    invoke-virtual {p1}, Lz;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 105
    iget-object v0, p0, Lar;->b:Lu;

    iget-object v1, p0, Lar;->a:Landroid/os/Bundle;

    iput-object v1, v0, Lu;->d:Landroid/os/Bundle;

    .line 107
    :cond_2
    iget-object v0, p0, Lar;->b:Lu;

    iget v1, p0, Lar;->d:I

    invoke-virtual {v0, v1, p2}, Lu;->a(ILu;)V

    .line 108
    iget-object v0, p0, Lar;->b:Lu;

    iget-boolean v1, p0, Lar;->e:Z

    iput-boolean v1, v0, Lu;->o:Z

    .line 109
    iget-object v0, p0, Lar;->b:Lu;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lu;->q:Z

    .line 110
    iget-object v0, p0, Lar;->b:Lu;

    iget v1, p0, Lar;->f:I

    iput v1, v0, Lu;->w:I

    .line 111
    iget-object v0, p0, Lar;->b:Lu;

    iget v1, p0, Lar;->g:I

    iput v1, v0, Lu;->x:I

    .line 112
    iget-object v0, p0, Lar;->b:Lu;

    iget-object v1, p0, Lar;->h:Ljava/lang/String;

    iput-object v1, v0, Lu;->y:Ljava/lang/String;

    .line 113
    iget-object v0, p0, Lar;->b:Lu;

    iget-boolean v1, p0, Lar;->i:Z

    iput-boolean v1, v0, Lu;->B:Z

    .line 114
    iget-object v0, p0, Lar;->b:Lu;

    iget-boolean v1, p0, Lar;->j:Z

    iput-boolean v1, v0, Lu;->A:Z

    .line 115
    iget-object v0, p0, Lar;->b:Lu;

    iget-object v1, p1, Lz;->b:Lah;

    iput-object v1, v0, Lu;->s:Lah;

    .line 117
    iget-object v0, p0, Lar;->b:Lu;

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 124
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 128
    iget-object v0, p0, Lar;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 129
    iget v0, p0, Lar;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 130
    iget-boolean v0, p0, Lar;->e:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 131
    iget v0, p0, Lar;->f:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 132
    iget v0, p0, Lar;->g:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 133
    iget-object v0, p0, Lar;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 134
    iget-boolean v0, p0, Lar;->i:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 135
    iget-boolean v0, p0, Lar;->j:Z

    if-eqz v0, :cond_2

    :goto_2
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 136
    iget-object v0, p0, Lar;->k:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 137
    iget-object v0, p0, Lar;->a:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 138
    return-void

    :cond_0
    move v0, v2

    .line 130
    goto :goto_0

    :cond_1
    move v0, v2

    .line 134
    goto :goto_1

    :cond_2
    move v1, v2

    .line 135
    goto :goto_2
.end method
