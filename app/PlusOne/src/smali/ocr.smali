.class public final Locr;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Locr;


# instance fields
.field private b:[Locv;

.field private c:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x0

    new-array v0, v0, [Locr;

    sput-object v0, Locr;->a:[Locr;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Loxq;-><init>()V

    .line 547
    sget-object v0, Locv;->a:[Locv;

    iput-object v0, p0, Locr;->b:[Locv;

    .line 25
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 570
    .line 571
    iget-object v1, p0, Locr;->b:[Locv;

    if-eqz v1, :cond_1

    .line 572
    iget-object v2, p0, Locr;->b:[Locv;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 573
    if-eqz v4, :cond_0

    .line 574
    const/4 v5, 0x1

    .line 575
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 572
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 579
    :cond_1
    iget-object v1, p0, Locr;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 580
    const/4 v1, 0x2

    iget-object v2, p0, Locr;->c:Ljava/lang/Integer;

    .line 581
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 583
    :cond_2
    iget-object v1, p0, Locr;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 584
    iput v0, p0, Locr;->ai:I

    .line 585
    return v0
.end method

.method public a(Loxn;)Locr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 593
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 594
    sparse-switch v0, :sswitch_data_0

    .line 598
    iget-object v2, p0, Locr;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 599
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Locr;->ah:Ljava/util/List;

    .line 602
    :cond_1
    iget-object v2, p0, Locr;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 604
    :sswitch_0
    return-object p0

    .line 609
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 610
    iget-object v0, p0, Locr;->b:[Locv;

    if-nez v0, :cond_3

    move v0, v1

    .line 611
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Locv;

    .line 612
    iget-object v3, p0, Locr;->b:[Locv;

    if-eqz v3, :cond_2

    .line 613
    iget-object v3, p0, Locr;->b:[Locv;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 615
    :cond_2
    iput-object v2, p0, Locr;->b:[Locv;

    .line 616
    :goto_2
    iget-object v2, p0, Locr;->b:[Locv;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 617
    iget-object v2, p0, Locr;->b:[Locv;

    new-instance v3, Locv;

    invoke-direct {v3}, Locv;-><init>()V

    aput-object v3, v2, v0

    .line 618
    iget-object v2, p0, Locr;->b:[Locv;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 619
    invoke-virtual {p1}, Loxn;->a()I

    .line 616
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 610
    :cond_3
    iget-object v0, p0, Locr;->b:[Locv;

    array-length v0, v0

    goto :goto_1

    .line 622
    :cond_4
    iget-object v2, p0, Locr;->b:[Locv;

    new-instance v3, Locv;

    invoke-direct {v3}, Locv;-><init>()V

    aput-object v3, v2, v0

    .line 623
    iget-object v2, p0, Locr;->b:[Locv;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 627
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Locr;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 594
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 554
    iget-object v0, p0, Locr;->b:[Locv;

    if-eqz v0, :cond_1

    .line 555
    iget-object v1, p0, Locr;->b:[Locv;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 556
    if-eqz v3, :cond_0

    .line 557
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 555
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 561
    :cond_1
    iget-object v0, p0, Locr;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 562
    const/4 v0, 0x2

    iget-object v1, p0, Locr;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 564
    :cond_2
    iget-object v0, p0, Locr;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 566
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Locr;->a(Loxn;)Locr;

    move-result-object v0

    return-object v0
.end method
