.class public final Lcqx;
.super Landroid/widget/BaseAdapter;
.source "PG"

# interfaces
.implements Lenf;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/BaseAdapter;",
        "Lenf",
        "<",
        "Lizu;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lctm;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 22
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcqx;->a:Ljava/util/List;

    .line 153
    return-void
.end method


# virtual methods
.method public a(I)F
    .locals 2

    .prologue
    .line 107
    new-instance v1, Lcqz;

    invoke-direct {v1}, Lcqz;-><init>()V

    .line 108
    iget-object v0, p0, Lcqx;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctm;

    .line 109
    invoke-interface {v0, v1}, Lctm;->a(Lctn;)V

    .line 110
    iget v0, v1, Lcqz;->a:F

    return v0
.end method

.method public a(F)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 84
    iget-object v0, p0, Lcqx;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    .line 85
    new-instance v4, Lcqz;

    invoke-direct {v4}, Lcqz;-><init>()V

    move v2, v1

    .line 88
    :goto_0
    if-ge v1, v3, :cond_1

    .line 89
    iget-object v0, p0, Lcqx;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctm;

    .line 90
    invoke-interface {v0, v4}, Lctm;->a(Lctn;)V

    .line 92
    const/4 v0, 0x0

    iget v5, v4, Lcqz;->a:F

    cmpg-float v0, v0, v5

    if-gtz v0, :cond_0

    iget v0, v4, Lcqz;->a:F

    cmpg-float v0, v0, p1

    if-gtz v0, :cond_0

    move v0, v1

    .line 95
    :goto_1
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_0

    .line 94
    :cond_0
    iget v0, v4, Lcqz;->a:F

    cmpl-float v0, v0, p1

    if-gtz v0, :cond_1

    move v0, v2

    goto :goto_1

    .line 99
    :cond_1
    return v2
.end method

.method public a(Landroid/net/Uri;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 114
    iget-object v0, p0, Lcqx;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 128
    :goto_0
    return v0

    .line 117
    :cond_0
    iget-object v0, p0, Lcqx;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    .line 118
    new-instance v4, Lcqy;

    invoke-direct {v4}, Lcqy;-><init>()V

    move v2, v1

    .line 119
    :goto_1
    if-ge v2, v3, :cond_2

    .line 120
    iget-object v0, p0, Lcqx;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctm;

    invoke-interface {v0, v4}, Lctm;->a(Lctn;)V

    .line 122
    iget-object v0, v4, Lcqy;->a:Lizu;

    .line 123
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lizu;->i()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v0}, Lizu;->e()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    .line 124
    goto :goto_0

    .line 119
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    move v0, v1

    .line 128
    goto :goto_0
.end method

.method public a(II)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List",
            "<",
            "Lizu;",
            ">;"
        }
    .end annotation

    .prologue
    .line 37
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 38
    new-instance v2, Lcqy;

    invoke-direct {v2}, Lcqy;-><init>()V

    .line 39
    :goto_0
    if-ge p1, p2, :cond_1

    .line 40
    iget-object v0, p0, Lcqx;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctm;

    invoke-interface {v0, v2}, Lctm;->a(Lctn;)V

    .line 41
    iget-object v0, v2, Lcqy;->a:Lizu;

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, v2, Lcqy;->a:Lizu;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 39
    :cond_0
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 46
    :cond_1
    return-object v1
.end method

.method public a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lctm;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 31
    iput-object p1, p0, Lcqx;->a:Ljava/util/List;

    .line 32
    invoke-virtual {p0}, Lcqx;->notifyDataSetChanged()V

    .line 33
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcqx;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcqx;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 61
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcqx;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctm;

    invoke-interface {v0}, Lctm;->a()I

    move-result v0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcqx;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctm;

    invoke-interface {v0, p2, p3}, Lctm;->a(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x3

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcqx;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    return v0
.end method
