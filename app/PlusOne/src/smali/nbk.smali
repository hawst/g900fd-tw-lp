.class public final Lnbk;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnbk;


# instance fields
.field private b:I

.field private c:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1381
    const/4 v0, 0x0

    new-array v0, v0, [Lnbk;

    sput-object v0, Lnbk;->a:[Lnbk;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1382
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1391
    const/high16 v0, -0x80000000

    iput v0, p0, Lnbk;->b:I

    .line 1382
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1410
    const/4 v0, 0x0

    .line 1411
    iget v1, p0, Lnbk;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 1412
    const/4 v0, 0x1

    iget v1, p0, Lnbk;->b:I

    .line 1413
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1415
    :cond_0
    iget-object v1, p0, Lnbk;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 1416
    const/4 v1, 0x2

    iget-object v2, p0, Lnbk;->c:Ljava/lang/Integer;

    .line 1417
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1419
    :cond_1
    iget-object v1, p0, Lnbk;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1420
    iput v0, p0, Lnbk;->ai:I

    .line 1421
    return v0
.end method

.method public a(Loxn;)Lnbk;
    .locals 2

    .prologue
    .line 1429
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1430
    sparse-switch v0, :sswitch_data_0

    .line 1434
    iget-object v1, p0, Lnbk;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1435
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnbk;->ah:Ljava/util/List;

    .line 1438
    :cond_1
    iget-object v1, p0, Lnbk;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1440
    :sswitch_0
    return-object p0

    .line 1445
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1446
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 1449
    :cond_2
    iput v0, p0, Lnbk;->b:I

    goto :goto_0

    .line 1451
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lnbk;->b:I

    goto :goto_0

    .line 1456
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnbk;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 1430
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1398
    iget v0, p0, Lnbk;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 1399
    const/4 v0, 0x1

    iget v1, p0, Lnbk;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1401
    :cond_0
    iget-object v0, p0, Lnbk;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 1402
    const/4 v0, 0x2

    iget-object v1, p0, Lnbk;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1404
    :cond_1
    iget-object v0, p0, Lnbk;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1406
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1378
    invoke-virtual {p0, p1}, Lnbk;->a(Loxn;)Lnbk;

    move-result-object v0

    return-object v0
.end method
