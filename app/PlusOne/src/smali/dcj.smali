.class final Ldcj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field private synthetic a:Ldbs;


# direct methods
.method constructor <init>(Ldbs;)V
    .locals 0

    .prologue
    .line 1346
    iput-object p1, p0, Ldcj;->a:Ldbs;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1427
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    .line 1428
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    .line 1430
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 1449
    :goto_0
    :pswitch_0
    const/4 v0, 0x0

    :goto_1
    return v0

    .line 1432
    :pswitch_1
    iget-object v0, p0, Ldcj;->a:Ldbs;

    invoke-static {v0, v4}, Ldbs;->a(Ldbs;Llip;)Llip;

    goto :goto_0

    .line 1437
    :pswitch_2
    iget-object v2, p0, Ldcj;->a:Ldbs;

    invoke-static {v2}, Ldbs;->v(Ldbs;)Llip;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1438
    iget-object v2, p0, Ldcj;->a:Ldbs;

    invoke-static {v2}, Ldbs;->v(Ldbs;)Llip;

    move-result-object v2

    const/4 v3, 0x3

    invoke-interface {v2, v0, v1, v3}, Llip;->a(III)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1439
    iget-object v0, p0, Ldcj;->a:Ldbs;

    invoke-static {v0}, Ldbs;->i(Ldbs;)Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;->invalidate()V

    .line 1440
    iget-object v0, p0, Ldcj;->a:Ldbs;

    invoke-static {v0, v4}, Ldbs;->a(Ldbs;Llip;)Llip;

    .line 1441
    const/4 v0, 0x1

    goto :goto_1

    .line 1444
    :cond_0
    iget-object v0, p0, Ldcj;->a:Ldbs;

    invoke-static {v0, v4}, Ldbs;->a(Ldbs;Llip;)Llip;

    goto :goto_0

    .line 1430
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v1, 0x1

    .line 1349
    iget-object v0, p0, Ldcj;->a:Ldbs;

    invoke-static {v0}, Ldbs;->b(Ldbs;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1423
    :cond_0
    :goto_0
    return v1

    .line 1352
    :cond_1
    iget-object v0, p0, Ldcj;->a:Ldbs;

    invoke-static {v0}, Ldbs;->s(Ldbs;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1355
    invoke-direct {p0, p2}, Ldcj;->a(Landroid/view/MotionEvent;)Z

    move-result v1

    goto :goto_0

    .line 1357
    :cond_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v5, v0

    .line 1358
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v6, v0

    .line 1360
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1417
    :cond_3
    :goto_1
    :pswitch_0
    iget-object v0, p0, Ldcj;->a:Ldbs;

    invoke-static {v0}, Ldbs;->s(Ldbs;)Z

    move-result v0

    if-nez v0, :cond_c

    iget-object v0, p0, Ldcj;->a:Ldbs;

    invoke-static {v0}, Ldbs;->b(Ldbs;)Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Ldcj;->a:Ldbs;

    invoke-virtual {v0}, Ldbs;->U()V

    move v2, v1

    :goto_2
    if-nez v2, :cond_0

    .line 1423
    invoke-direct {p0, p2}, Ldcj;->a(Landroid/view/MotionEvent;)Z

    move-result v1

    goto :goto_0

    .line 1363
    :pswitch_1
    iget-object v0, p0, Ldcj;->a:Ldbs;

    invoke-static {v0}, Ldbs;->t(Ldbs;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v4, v0

    move v2, v3

    :goto_3
    if-ltz v4, :cond_4

    if-nez v2, :cond_4

    .line 1364
    iget-object v0, p0, Ldcj;->a:Ldbs;

    invoke-static {v0}, Ldbs;->t(Ldbs;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llip;

    .line 1365
    invoke-interface {v0, v5, v6, v3}, Llip;->a(III)Z

    move-result v7

    if-eqz v7, :cond_f

    .line 1366
    iget-object v2, p0, Ldcj;->a:Ldbs;

    invoke-static {v2, v0}, Ldbs;->a(Ldbs;Llip;)Llip;

    .line 1367
    iget-object v0, p0, Ldcj;->a:Ldbs;

    invoke-static {v0}, Ldbs;->i(Ldbs;)Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;->invalidate()V

    move v0, v1

    .line 1363
    :goto_4
    add-int/lit8 v2, v4, -0x1

    move v4, v2

    move v2, v0

    goto :goto_3

    .line 1371
    :cond_4
    iget-object v0, p0, Ldcj;->a:Ldbs;

    invoke-static {v0}, Ldbs;->u(Ldbs;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v4, v0

    :goto_5
    if-ltz v4, :cond_5

    if-nez v2, :cond_5

    .line 1372
    iget-object v0, p0, Ldcj;->a:Ldbs;

    invoke-static {v0}, Ldbs;->u(Ldbs;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llip;

    .line 1373
    invoke-interface {v0, v5, v6, v3}, Llip;->a(III)Z

    move-result v7

    if-eqz v7, :cond_e

    .line 1374
    iget-object v2, p0, Ldcj;->a:Ldbs;

    invoke-static {v2, v0}, Ldbs;->a(Ldbs;Llip;)Llip;

    .line 1375
    iget-object v0, p0, Ldcj;->a:Ldbs;

    invoke-static {v0}, Ldbs;->i(Ldbs;)Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;->invalidate()V

    move v0, v1

    .line 1371
    :goto_6
    add-int/lit8 v2, v4, -0x1

    move v4, v2

    move v2, v0

    goto :goto_5

    .line 1380
    :cond_5
    if-eqz v2, :cond_3

    goto/16 :goto_0

    .line 1388
    :pswitch_2
    iget-object v0, p0, Ldcj;->a:Ldbs;

    invoke-static {v0, v4}, Ldbs;->a(Ldbs;Llip;)Llip;

    .line 1389
    iget-object v0, p0, Ldcj;->a:Ldbs;

    invoke-static {v0}, Ldbs;->t(Ldbs;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_7
    if-ltz v2, :cond_7

    .line 1390
    iget-object v0, p0, Ldcj;->a:Ldbs;

    invoke-static {v0}, Ldbs;->t(Ldbs;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llip;

    .line 1391
    invoke-interface {v0, v5, v6, v1}, Llip;->a(III)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1392
    iget-object v0, p0, Ldcj;->a:Ldbs;

    invoke-static {v0}, Ldbs;->i(Ldbs;)Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;->invalidate()V

    goto/16 :goto_0

    .line 1389
    :cond_6
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_7

    .line 1396
    :cond_7
    iget-object v0, p0, Ldcj;->a:Ldbs;

    invoke-static {v0}, Ldbs;->u(Ldbs;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_8
    if-ltz v2, :cond_3

    .line 1397
    iget-object v0, p0, Ldcj;->a:Ldbs;

    invoke-static {v0}, Ldbs;->u(Ldbs;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llip;

    .line 1398
    invoke-interface {v0, v5, v6, v1}, Llip;->a(III)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1399
    iget-object v0, p0, Ldcj;->a:Ldbs;

    invoke-static {v0}, Ldbs;->i(Ldbs;)Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;->invalidate()V

    goto/16 :goto_0

    .line 1396
    :cond_8
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_8

    .line 1407
    :pswitch_3
    iget-object v0, p0, Ldcj;->a:Ldbs;

    invoke-static {v0}, Ldbs;->v(Ldbs;)Llip;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 1408
    iget-object v0, p0, Ldcj;->a:Ldbs;

    invoke-static {v0}, Ldbs;->v(Ldbs;)Llip;

    move-result-object v0

    const/4 v2, 0x3

    invoke-interface {v0, v5, v6, v2}, Llip;->a(III)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1409
    iget-object v0, p0, Ldcj;->a:Ldbs;

    invoke-static {v0}, Ldbs;->i(Ldbs;)Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;->invalidate()V

    .line 1412
    :cond_9
    iget-object v0, p0, Ldcj;->a:Ldbs;

    invoke-static {v0, v4}, Ldbs;->a(Ldbs;Llip;)Llip;

    goto/16 :goto_1

    .line 1417
    :cond_a
    iget-object v0, p0, Ldcj;->a:Ldbs;

    invoke-static {v0}, Ldbs;->w(Ldbs;)Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Ldcj;->a:Ldbs;

    invoke-static {v0}, Ldbs;->x(Ldbs;)Z

    move-result v0

    if-nez v0, :cond_c

    iget-object v0, p0, Ldcj;->a:Ldbs;

    invoke-static {v0}, Ldbs;->r(Ldbs;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v4, v0

    move v2, v3

    :goto_9
    if-ltz v4, :cond_b

    iget-object v0, p0, Ldcj;->a:Ldbs;

    invoke-static {v0}, Ldbs;->r(Ldbs;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbp;

    invoke-virtual {v0}, Ldbp;->f()Z

    move-result v5

    if-eqz v5, :cond_d

    invoke-virtual {v0, v3}, Ldbp;->b(Z)V

    move v0, v1

    :goto_a
    add-int/lit8 v2, v4, -0x1

    move v4, v2

    move v2, v0

    goto :goto_9

    :cond_b
    iget-object v0, p0, Ldcj;->a:Ldbs;

    invoke-static {v0}, Ldbs;->i(Ldbs;)Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;->invalidate()V

    goto/16 :goto_2

    :cond_c
    move v2, v3

    goto/16 :goto_2

    :cond_d
    move v0, v2

    goto :goto_a

    :cond_e
    move v0, v2

    goto/16 :goto_6

    :cond_f
    move v0, v2

    goto/16 :goto_4

    .line 1360
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
