.class public final Lmfd;
.super Loxq;
.source "PG"


# instance fields
.field private a:Loke;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 11824
    invoke-direct {p0}, Loxq;-><init>()V

    .line 11827
    iput-object v0, p0, Lmfd;->apiHeader:Llyr;

    .line 11830
    iput-object v0, p0, Lmfd;->a:Loke;

    .line 11824
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 11847
    const/4 v0, 0x0

    .line 11848
    iget-object v1, p0, Lmfd;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 11849
    const/4 v0, 0x1

    iget-object v1, p0, Lmfd;->apiHeader:Llyr;

    .line 11850
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 11852
    :cond_0
    iget-object v1, p0, Lmfd;->a:Loke;

    if-eqz v1, :cond_1

    .line 11853
    const/4 v1, 0x2

    iget-object v2, p0, Lmfd;->a:Loke;

    .line 11854
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11856
    :cond_1
    iget-object v1, p0, Lmfd;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11857
    iput v0, p0, Lmfd;->ai:I

    .line 11858
    return v0
.end method

.method public a(Loxn;)Lmfd;
    .locals 2

    .prologue
    .line 11866
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 11867
    sparse-switch v0, :sswitch_data_0

    .line 11871
    iget-object v1, p0, Lmfd;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 11872
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmfd;->ah:Ljava/util/List;

    .line 11875
    :cond_1
    iget-object v1, p0, Lmfd;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 11877
    :sswitch_0
    return-object p0

    .line 11882
    :sswitch_1
    iget-object v0, p0, Lmfd;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 11883
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmfd;->apiHeader:Llyr;

    .line 11885
    :cond_2
    iget-object v0, p0, Lmfd;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 11889
    :sswitch_2
    iget-object v0, p0, Lmfd;->a:Loke;

    if-nez v0, :cond_3

    .line 11890
    new-instance v0, Loke;

    invoke-direct {v0}, Loke;-><init>()V

    iput-object v0, p0, Lmfd;->a:Loke;

    .line 11892
    :cond_3
    iget-object v0, p0, Lmfd;->a:Loke;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 11867
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 11835
    iget-object v0, p0, Lmfd;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 11836
    const/4 v0, 0x1

    iget-object v1, p0, Lmfd;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 11838
    :cond_0
    iget-object v0, p0, Lmfd;->a:Loke;

    if-eqz v0, :cond_1

    .line 11839
    const/4 v0, 0x2

    iget-object v1, p0, Lmfd;->a:Loke;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 11841
    :cond_1
    iget-object v0, p0, Lmfd;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 11843
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 11820
    invoke-virtual {p0, p1}, Lmfd;->a(Loxn;)Lmfd;

    move-result-object v0

    return-object v0
.end method
