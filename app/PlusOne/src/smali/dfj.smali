.class final Ldfj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfzz;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation


# instance fields
.field private a:Landroid/graphics/RectF;

.field private b:Landroid/graphics/RectF;

.field private c:Landroid/graphics/RectF;

.field private d:Landroid/graphics/RectF;

.field private final e:[I

.field private final f:Landroid/graphics/Matrix;

.field private g:F

.field private h:F

.field private i:Z

.field private synthetic j:Ldex;


# direct methods
.method public constructor <init>(Ldex;Landroid/graphics/RectF;)V
    .locals 1

    .prologue
    .line 1377
    iput-object p1, p0, Ldfj;->j:Ldex;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1366
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Ldfj;->d:Landroid/graphics/RectF;

    .line 1367
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Ldfj;->e:[I

    .line 1368
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Ldfj;->f:Landroid/graphics/Matrix;

    .line 1378
    iput-object p2, p0, Ldfj;->a:Landroid/graphics/RectF;

    .line 1379
    return-void
.end method


# virtual methods
.method public a(F)V
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 1397
    iget-object v0, p0, Ldfj;->j:Ldex;

    invoke-static {v0}, Ldex;->l(Ldex;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setAlpha(F)V

    .line 1398
    iget-object v0, p0, Ldfj;->c:Landroid/graphics/RectF;

    iget-object v1, p0, Ldfj;->a:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    iget-object v2, p0, Ldfj;->b:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    iget-object v3, p0, Ldfj;->a:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    sub-float/2addr v2, v3

    mul-float/2addr v2, p1

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 1400
    iget-object v0, p0, Ldfj;->c:Landroid/graphics/RectF;

    iget-object v1, p0, Ldfj;->a:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    iget-object v2, p0, Ldfj;->b:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    iget-object v3, p0, Ldfj;->a:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    sub-float/2addr v2, v3

    mul-float/2addr v2, p1

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 1402
    iget-object v0, p0, Ldfj;->c:Landroid/graphics/RectF;

    iget-object v1, p0, Ldfj;->a:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->right:F

    iget-object v2, p0, Ldfj;->b:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->right:F

    iget-object v3, p0, Ldfj;->a:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    sub-float/2addr v2, v3

    mul-float/2addr v2, p1

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 1404
    iget-object v0, p0, Ldfj;->c:Landroid/graphics/RectF;

    iget-object v1, p0, Ldfj;->a:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    iget-object v2, p0, Ldfj;->b:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    iget-object v3, p0, Ldfj;->a:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v2, v3

    mul-float/2addr v2, p1

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 1406
    iget-object v0, p0, Ldfj;->f:Landroid/graphics/Matrix;

    iget-object v1, p0, Ldfj;->b:Landroid/graphics/RectF;

    iget-object v2, p0, Ldfj;->c:Landroid/graphics/RectF;

    sget-object v3, Landroid/graphics/Matrix$ScaleToFit;->CENTER:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    .line 1409
    iget-object v0, p0, Ldfj;->b:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    iget v1, p0, Ldfj;->g:F

    mul-float/2addr v0, v1

    sub-float v1, v4, p1

    mul-float/2addr v0, v1

    .line 1411
    iget-object v1, p0, Ldfj;->b:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    iget v2, p0, Ldfj;->h:F

    mul-float/2addr v1, v2

    sub-float v2, v4, p1

    mul-float/2addr v1, v2

    .line 1413
    iget-object v2, p0, Ldfj;->d:Landroid/graphics/RectF;

    iget-object v3, p0, Ldfj;->b:Landroid/graphics/RectF;

    invoke-virtual {v2, v3}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 1414
    iget-object v2, p0, Ldfj;->d:Landroid/graphics/RectF;

    invoke-virtual {v2, v0, v1}, Landroid/graphics/RectF;->inset(FF)V

    .line 1415
    return-void
.end method

.method public a(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1463
    iget-object v0, p0, Ldfj;->f:Landroid/graphics/Matrix;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 1464
    iget-object v0, p0, Ldfj;->d:Landroid/graphics/RectF;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/RectF;)Z

    .line 1465
    return-void
.end method

.method public a(Landroid/view/View;Landroid/graphics/drawable/Drawable;Landroid/graphics/RectF;)V
    .locals 1

    .prologue
    .line 1456
    invoke-virtual {p0, p1, p2, p3}, Ldfj;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;Landroid/graphics/RectF;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1457
    iget-object v0, p0, Ldfj;->j:Ldex;

    invoke-static {v0, p1}, Ldex;->b(Ldex;Landroid/view/View;)V

    .line 1459
    :cond_0
    return-void
.end method

.method public b(Landroid/view/View;Landroid/graphics/drawable/Drawable;Landroid/graphics/RectF;)Z
    .locals 5

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    const/4 v0, 0x1

    .line 1382
    iget-boolean v1, p0, Ldfj;->i:Z

    if-eqz v1, :cond_0

    .line 1383
    const/4 v0, 0x0

    .line 1393
    :goto_0
    return v0

    .line 1385
    :cond_0
    iput-boolean v0, p0, Ldfj;->i:Z

    .line 1387
    iget-object v1, p0, Ldfj;->e:[I

    invoke-virtual {p1, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    iget-object v1, p0, Ldfj;->a:Landroid/graphics/RectF;

    const/4 v2, 0x0

    iget-object v3, p0, Ldfj;->e:[I

    aget v3, v3, v0

    neg-int v3, v3

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/RectF;->offset(FF)V

    .line 1388
    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    if-le v1, v2, :cond_1

    iget-object v1, p0, Ldfj;->a:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v1, v2

    iget-object v2, p0, Ldfj;->a:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    sub-float/2addr v1, v2

    div-float/2addr v1, v4

    iget-object v2, p0, Ldfj;->a:Landroid/graphics/RectF;

    iget v3, v2, Landroid/graphics/RectF;->left:F

    sub-float/2addr v3, v1

    iput v3, v2, Landroid/graphics/RectF;->left:F

    iget-object v2, p0, Ldfj;->a:Landroid/graphics/RectF;

    iget v3, v2, Landroid/graphics/RectF;->right:F

    add-float/2addr v3, v1

    iput v3, v2, Landroid/graphics/RectF;->right:F

    iget-object v2, p0, Ldfj;->a:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    div-float/2addr v1, v2

    iput v1, p0, Ldfj;->g:F

    .line 1390
    :goto_1
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1, p3}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    iput-object v1, p0, Ldfj;->b:Landroid/graphics/RectF;

    .line 1391
    new-instance v1, Landroid/graphics/RectF;

    iget-object v2, p0, Ldfj;->a:Landroid/graphics/RectF;

    invoke-direct {v1, v2}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    iput-object v1, p0, Ldfj;->c:Landroid/graphics/RectF;

    goto :goto_0

    .line 1388
    :cond_1
    iget-object v1, p0, Ldfj;->a:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v1

    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v1, v2

    iget-object v2, p0, Ldfj;->a:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    sub-float/2addr v1, v2

    div-float/2addr v1, v4

    iget-object v2, p0, Ldfj;->a:Landroid/graphics/RectF;

    iget v3, v2, Landroid/graphics/RectF;->top:F

    sub-float/2addr v3, v1

    iput v3, v2, Landroid/graphics/RectF;->top:F

    iget-object v2, p0, Ldfj;->a:Landroid/graphics/RectF;

    iget v3, v2, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v3, v1

    iput v3, v2, Landroid/graphics/RectF;->bottom:F

    iget-object v2, p0, Ldfj;->a:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    div-float/2addr v1, v2

    iput v1, p0, Ldfj;->h:F

    goto :goto_1
.end method
