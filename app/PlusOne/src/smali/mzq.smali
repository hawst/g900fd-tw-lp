.class public final Lmzq;
.super Loxq;
.source "PG"


# instance fields
.field public a:I

.field public b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 222
    invoke-direct {p0}, Loxq;-><init>()V

    .line 232
    const/high16 v0, -0x80000000

    iput v0, p0, Lmzq;->a:I

    .line 222
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 251
    const/4 v0, 0x0

    .line 252
    iget v1, p0, Lmzq;->a:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 253
    const/4 v0, 0x1

    iget v1, p0, Lmzq;->a:I

    .line 254
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 256
    :cond_0
    iget-object v1, p0, Lmzq;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 257
    const/4 v1, 0x2

    iget-object v2, p0, Lmzq;->b:Ljava/lang/String;

    .line 258
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 260
    :cond_1
    iget-object v1, p0, Lmzq;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 261
    iput v0, p0, Lmzq;->ai:I

    .line 262
    return v0
.end method

.method public a(Loxn;)Lmzq;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 270
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 271
    sparse-switch v0, :sswitch_data_0

    .line 275
    iget-object v1, p0, Lmzq;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 276
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmzq;->ah:Ljava/util/List;

    .line 279
    :cond_1
    iget-object v1, p0, Lmzq;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 281
    :sswitch_0
    return-object p0

    .line 286
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 287
    if-eq v0, v2, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    .line 291
    :cond_2
    iput v0, p0, Lmzq;->a:I

    goto :goto_0

    .line 293
    :cond_3
    iput v2, p0, Lmzq;->a:I

    goto :goto_0

    .line 298
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmzq;->b:Ljava/lang/String;

    goto :goto_0

    .line 271
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 239
    iget v0, p0, Lmzq;->a:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 240
    const/4 v0, 0x1

    iget v1, p0, Lmzq;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 242
    :cond_0
    iget-object v0, p0, Lmzq;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 243
    const/4 v0, 0x2

    iget-object v1, p0, Lmzq;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 245
    :cond_1
    iget-object v0, p0, Lmzq;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 247
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 218
    invoke-virtual {p0, p1}, Lmzq;->a(Loxn;)Lmzq;

    move-result-object v0

    return-object v0
.end method
