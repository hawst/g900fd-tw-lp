.class public final Ljjs;
.super Ljkv;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljkv",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:I

.field private final c:I

.field private final d:Landroid/content/Context;

.field private final e:Ldp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">.dp;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;II)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0, p1}, Ljkv;-><init>(Landroid/content/Context;)V

    .line 22
    new-instance v0, Ldp;

    invoke-direct {v0, p0}, Ldp;-><init>(Ldo;)V

    iput-object v0, p0, Ljjs;->e:Ldp;

    .line 26
    iput p2, p0, Ljjs;->b:I

    .line 27
    iput p3, p0, Ljjs;->c:I

    .line 28
    iput-object p1, p0, Ljjs;->d:Landroid/content/Context;

    .line 29
    return-void
.end method


# virtual methods
.method public synthetic d()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 14
    invoke-virtual {p0}, Ljjs;->f()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public final f()Landroid/database/Cursor;
    .locals 3

    .prologue
    .line 33
    iget-object v0, p0, Ljjs;->d:Landroid/content/Context;

    const-class v1, Ljip;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljip;

    iget v1, p0, Ljjs;->b:I

    iget v2, p0, Ljjs;->c:I

    invoke-interface {v0, v1, v2}, Ljip;->a(II)Landroid/database/Cursor;

    move-result-object v0

    .line 34
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    .line 35
    iget-object v1, p0, Ljjs;->e:Ldp;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    .line 36
    iget-object v1, p0, Ljjs;->d:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Ljkw;->b:Landroid/net/Uri;

    invoke-interface {v0, v1, v2}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 38
    return-object v0
.end method
