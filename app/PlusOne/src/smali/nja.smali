.class public final Lnja;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lock;

.field public b:I

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/Boolean;

.field private e:Ljava/lang/Boolean;

.field private f:Ljava/lang/Boolean;

.field private g:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Loxq;-><init>()V

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, Lnja;->a:Lock;

    .line 38
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lnja;->g:[Ljava/lang/String;

    .line 41
    const/high16 v0, -0x80000000

    iput v0, p0, Lnja;->b:I

    .line 15
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 75
    .line 76
    iget-object v0, p0, Lnja;->c:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 77
    const/4 v0, 0x1

    iget-object v2, p0, Lnja;->c:Ljava/lang/String;

    .line 78
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 80
    :goto_0
    iget-object v2, p0, Lnja;->d:Ljava/lang/Boolean;

    if-eqz v2, :cond_0

    .line 81
    const/4 v2, 0x2

    iget-object v3, p0, Lnja;->d:Ljava/lang/Boolean;

    .line 82
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 84
    :cond_0
    iget-object v2, p0, Lnja;->e:Ljava/lang/Boolean;

    if-eqz v2, :cond_1

    .line 85
    const/4 v2, 0x3

    iget-object v3, p0, Lnja;->e:Ljava/lang/Boolean;

    .line 86
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 88
    :cond_1
    iget-object v2, p0, Lnja;->f:Ljava/lang/Boolean;

    if-eqz v2, :cond_2

    .line 89
    const/4 v2, 0x4

    iget-object v3, p0, Lnja;->f:Ljava/lang/Boolean;

    .line 90
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 92
    :cond_2
    iget-object v2, p0, Lnja;->g:[Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lnja;->g:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_4

    .line 94
    iget-object v3, p0, Lnja;->g:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_3

    aget-object v5, v3, v1

    .line 96
    invoke-static {v5}, Loxo;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 94
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 98
    :cond_3
    add-int/2addr v0, v2

    .line 99
    iget-object v1, p0, Lnja;->g:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 101
    :cond_4
    iget v1, p0, Lnja;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_5

    .line 102
    const/4 v1, 0x6

    iget v2, p0, Lnja;->b:I

    .line 103
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 105
    :cond_5
    iget-object v1, p0, Lnja;->a:Lock;

    if-eqz v1, :cond_6

    .line 106
    const/4 v1, 0x7

    iget-object v2, p0, Lnja;->a:Lock;

    .line 107
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 109
    :cond_6
    iget-object v1, p0, Lnja;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 110
    iput v0, p0, Lnja;->ai:I

    .line 111
    return v0

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lnja;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 119
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 120
    sparse-switch v0, :sswitch_data_0

    .line 124
    iget-object v1, p0, Lnja;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 125
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnja;->ah:Ljava/util/List;

    .line 128
    :cond_1
    iget-object v1, p0, Lnja;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 130
    :sswitch_0
    return-object p0

    .line 135
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnja;->c:Ljava/lang/String;

    goto :goto_0

    .line 139
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnja;->d:Ljava/lang/Boolean;

    goto :goto_0

    .line 143
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnja;->e:Ljava/lang/Boolean;

    goto :goto_0

    .line 147
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnja;->f:Ljava/lang/Boolean;

    goto :goto_0

    .line 151
    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 152
    iget-object v0, p0, Lnja;->g:[Ljava/lang/String;

    array-length v0, v0

    .line 153
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    .line 154
    iget-object v2, p0, Lnja;->g:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 155
    iput-object v1, p0, Lnja;->g:[Ljava/lang/String;

    .line 156
    :goto_1
    iget-object v1, p0, Lnja;->g:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 157
    iget-object v1, p0, Lnja;->g:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 158
    invoke-virtual {p1}, Loxn;->a()I

    .line 156
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 161
    :cond_2
    iget-object v1, p0, Lnja;->g:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    .line 165
    :sswitch_6
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 166
    if-eq v0, v4, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    const/4 v1, 0x4

    if-eq v0, v1, :cond_3

    const/4 v1, 0x5

    if-eq v0, v1, :cond_3

    const/4 v1, 0x6

    if-ne v0, v1, :cond_4

    .line 172
    :cond_3
    iput v0, p0, Lnja;->b:I

    goto/16 :goto_0

    .line 174
    :cond_4
    iput v4, p0, Lnja;->b:I

    goto/16 :goto_0

    .line 179
    :sswitch_7
    iget-object v0, p0, Lnja;->a:Lock;

    if-nez v0, :cond_5

    .line 180
    new-instance v0, Lock;

    invoke-direct {v0}, Lock;-><init>()V

    iput-object v0, p0, Lnja;->a:Lock;

    .line 182
    :cond_5
    iget-object v0, p0, Lnja;->a:Lock;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 120
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 46
    iget-object v0, p0, Lnja;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 47
    const/4 v0, 0x1

    iget-object v1, p0, Lnja;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 49
    :cond_0
    iget-object v0, p0, Lnja;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 50
    const/4 v0, 0x2

    iget-object v1, p0, Lnja;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 52
    :cond_1
    iget-object v0, p0, Lnja;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 53
    const/4 v0, 0x3

    iget-object v1, p0, Lnja;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 55
    :cond_2
    iget-object v0, p0, Lnja;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 56
    const/4 v0, 0x4

    iget-object v1, p0, Lnja;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 58
    :cond_3
    iget-object v0, p0, Lnja;->g:[Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 59
    iget-object v1, p0, Lnja;->g:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 60
    const/4 v4, 0x5

    invoke-virtual {p1, v4, v3}, Loxo;->a(ILjava/lang/String;)V

    .line 59
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 63
    :cond_4
    iget v0, p0, Lnja;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_5

    .line 64
    const/4 v0, 0x6

    iget v1, p0, Lnja;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 66
    :cond_5
    iget-object v0, p0, Lnja;->a:Lock;

    if-eqz v0, :cond_6

    .line 67
    const/4 v0, 0x7

    iget-object v1, p0, Lnja;->a:Lock;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 69
    :cond_6
    iget-object v0, p0, Lnja;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 71
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 11
    invoke-virtual {p0, p1}, Lnja;->a(Loxn;)Lnja;

    move-result-object v0

    return-object v0
.end method
