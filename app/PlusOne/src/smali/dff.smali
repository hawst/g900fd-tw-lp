.class final Ldff;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:I

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;


# direct methods
.method private constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1550
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 1551
    iput-object p1, p0, Ldff;->a:Landroid/content/Context;

    .line 1552
    iput p2, p0, Ldff;->b:I

    .line 1553
    iput-object p3, p0, Ldff;->c:Ljava/lang/String;

    .line 1554
    iput-object p4, p0, Ldff;->d:Ljava/lang/String;

    .line 1555
    return-void
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1570
    new-instance v0, Ldff;

    invoke-direct {v0, p0, p1, p2, p3}, Ldff;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 1571
    invoke-virtual {v0, v1}, Ldff;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1572
    return-void
.end method


# virtual methods
.method protected varargs a()Ljava/lang/Boolean;
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 1560
    iget-object v0, p0, Ldff;->a:Landroid/content/Context;

    iget v1, p0, Ldff;->b:I

    iget-object v2, p0, Ldff;->c:Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v5, p0, Ldff;->d:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Ldtc;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Lkff;

    move-result-object v0

    .line 1562
    if-eqz v0, :cond_0

    .line 1563
    invoke-virtual {v0}, Lkff;->l()V

    .line 1565
    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lkff;->t()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    :goto_0
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_2
    const/4 v4, 0x0

    goto :goto_0
.end method

.method protected a(Ljava/lang/Boolean;)V
    .locals 3

    .prologue
    .line 1576
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1577
    iget-object v0, p0, Ldff;->a:Landroid/content/Context;

    iget-object v1, p0, Ldff;->a:Landroid/content/Context;

    const v2, 0x7f0a07ac

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 1578
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1580
    :cond_0
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1541
    invoke-virtual {p0}, Ldff;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1541
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Ldff;->a(Ljava/lang/Boolean;)V

    return-void
.end method
