.class public final Lcgp;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(FLjava/lang/CharSequence;FF)F
    .locals 2

    .prologue
    .line 1328
    cmpg-float v0, p0, p2

    if-ltz v0, :cond_0

    cmpl-float v0, p0, p3

    if-lez v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " must be between "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " and "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcgp;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/IllegalStateException;

    :cond_1
    return p0
.end method

.method public static a(ILjava/lang/CharSequence;II)I
    .locals 1

    .prologue
    .line 1336
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, Lcgp;->a(ILjava/lang/CharSequence;IILjava/lang/CharSequence;)I

    move-result v0

    return v0
.end method

.method public static a(ILjava/lang/CharSequence;IILjava/lang/CharSequence;)I
    .locals 2

    .prologue
    .line 335
    if-lt p0, p2, :cond_0

    if-le p0, p3, :cond_1

    .line 336
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 337
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " must be between "

    .line 338
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 339
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " and "

    .line 340
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 341
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    .line 342
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 343
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 336
    invoke-static {v0, p4}, Lcgp;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/IllegalStateException;

    .line 346
    :cond_1
    return p0
.end method

.method public static a(ILjava/lang/CharSequence;Ljava/util/Collection;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(I",
            "Ljava/lang/CharSequence;",
            "Ljava/util/Collection",
            "<TT;>;)I"
        }
    .end annotation

    .prologue
    .line 1394
    const/4 v0, 0x0

    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    const/4 v2, 0x0

    invoke-static {p0, p1, v0, v1, v2}, Lcgp;->a(ILjava/lang/CharSequence;IILjava/lang/CharSequence;)I

    move-result v0

    return v0
.end method

.method public static a(JLjava/lang/CharSequence;)J
    .locals 2

    .prologue
    .line 1406
    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " must be positive: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcgp;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/IllegalStateException;

    :cond_0
    return-wide p0
.end method

.method public static a(Ljava/lang/CharSequence;)Ljava/lang/IllegalStateException;
    .locals 2

    .prologue
    .line 37
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/IllegalStateException;
    .locals 2

    .prologue
    .line 53
    if-eqz p1, :cond_0

    .line 54
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/CharSequence;)V

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v0}, Lcgp;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    .line 56
    :cond_0
    invoke-static {p0}, Lcgp;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0
.end method

.method public static a(Ljava/lang/CharSequence;Ljava/lang/Throwable;)Ljava/lang/IllegalStateException;
    .locals 2

    .prologue
    .line 41
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-object v0
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/CharSequence;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 67
    if-nez p0, :cond_0

    .line 68
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " must not be null"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v0, p2}, Lcgp;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/IllegalStateException;

    .line 70
    :cond_0
    return-object p0
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/Object;Ljava/lang/CharSequence;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Ljava/lang/CharSequence;",
            "TT;",
            "Ljava/lang/CharSequence;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 835
    invoke-static {p0, p2}, Lbqh;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 836
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 837
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " must be equal to "

    .line 838
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 839
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 836
    invoke-static {v0, p3}, Lcgp;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/IllegalStateException;

    .line 842
    :cond_0
    return-object p0
.end method

.method public static a(Ljava/util/Collection;Ljava/lang/CharSequence;)Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/util/Collection",
            "<*>;>(TT;",
            "Ljava/lang/CharSequence;",
            ")TT;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1314
    invoke-static {p0, p1, v2}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    invoke-interface {p0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " must be empty"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v0, v2}, Lcgp;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/IllegalStateException;

    :cond_0
    return-object p0
.end method

.method public static a(ILjava/lang/CharSequence;ILjava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 780
    if-eq p0, p2, :cond_0

    .line 781
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 782
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " must be equal to "

    .line 783
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 784
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 781
    invoke-static {v0, p3}, Lcgp;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/IllegalStateException;

    .line 787
    :cond_0
    return-void
.end method

.method public static a(JLjava/lang/CharSequence;J)V
    .locals 3

    .prologue
    .line 1504
    cmp-long v0, p0, p3

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " must be equal to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcgp;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/IllegalStateException;

    .line 1505
    :cond_0
    return-void
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Ljava/lang/CharSequence;",
            "TT;)V"
        }
    .end annotation

    .prologue
    .line 1529
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/Object;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 1530
    return-void
.end method

.method public static a(ZLjava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 60
    if-nez p0, :cond_0

    .line 61
    invoke-static {p1}, Lcgp;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    .line 63
    :cond_0
    return-void
.end method

.method public static a(ZLjava/lang/CharSequence;Z)V
    .locals 2

    .prologue
    .line 1524
    if-eq p0, p2, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " must be equal to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcgp;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/IllegalStateException;

    .line 1525
    :cond_0
    return-void
.end method

.method public static b(ILjava/lang/CharSequence;ILjava/lang/CharSequence;)I
    .locals 2

    .prologue
    .line 1004
    if-ne p0, p2, :cond_0

    .line 1005
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1006
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " must not be equal to "

    .line 1007
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1008
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1005
    invoke-static {v0, p3}, Lcgp;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/IllegalStateException;

    .line 1011
    :cond_0
    return p0
.end method

.method public static b(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/CharSequence;",
            ")V"
        }
    .end annotation

    .prologue
    .line 75
    if-eqz p0, :cond_0

    .line 76
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " must be null"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v0, p2}, Lcgp;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/IllegalStateException;

    .line 78
    :cond_0
    return-void
.end method

.method public static b(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/Object;Ljava/lang/CharSequence;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Ljava/lang/CharSequence;",
            "TT;",
            "Ljava/lang/CharSequence;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1194
    if-eq p0, p2, :cond_0

    .line 1195
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1196
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " must be same as "

    .line 1197
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1198
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1195
    invoke-static {v0, p3}, Lcgp;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/IllegalStateException;

    .line 1201
    :cond_0
    return-void
.end method
