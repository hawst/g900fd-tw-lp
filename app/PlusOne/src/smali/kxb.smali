.class public final Lkxb;
.super Llol;
.source "PG"

# interfaces
.implements Lhjj;
.implements Lhmq;
.implements Lkvs;
.implements Lkxa;
.implements Lze;


# instance fields
.field private final N:Lhje;

.field private O:Lhee;

.field private P:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

.field private Q:Lkwz;

.field private R:Ljava/lang/String;

.field private S:Ljava/lang/String;

.field private T:Landroid/support/v7/widget/SearchView;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 48
    invoke-direct {p0}, Llol;-><init>()V

    .line 58
    new-instance v0, Lhje;

    iget-object v1, p0, Lkxb;->av:Llqm;

    invoke-direct {v0, p0, v1, p0}, Lhje;-><init>(Lu;Llqr;Lhjj;)V

    iput-object v0, p0, Lkxb;->N:Lhje;

    .line 64
    new-instance v0, Lhnw;

    new-instance v1, Lkxe;

    invoke-direct {v1, p0}, Lkxe;-><init>(Lkxb;)V

    invoke-direct {v0, p0, v1}, Lhnw;-><init>(Llrn;Lhnv;)V

    .line 84
    return-void
.end method

.method static synthetic a(Lkxb;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lkxb;->S:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lkxb;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 48
    iput-object p1, p0, Lkxb;->R:Ljava/lang/String;

    return-object p1
.end method

.method private a()V
    .locals 2

    .prologue
    .line 206
    iget-object v0, p0, Lkxb;->P:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->setVisibility(I)V

    .line 207
    return-void
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 275
    sget-object v0, Lhmw;->J:Lhmw;

    return-object v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 179
    const v0, 0x7f0400e1

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 180
    const v0, 0x7f100304

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    iput-object v0, p0, Lkxb;->P:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    .line 181
    iget-object v0, p0, Lkxb;->P:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    iget-object v2, p0, Lkxb;->Q:Lkwz;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(Landroid/widget/ListAdapter;)V

    .line 182
    iget-object v0, p0, Lkxb;->P:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    const v2, 0x7f020415

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->g(I)V

    .line 184
    iget-object v0, p0, Lkxb;->P:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->c(I)V

    .line 185
    iget-object v0, p0, Lkxb;->P:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    iget-object v2, p0, Lkxb;->at:Llnl;

    invoke-static {v2}, Llcm;->b(Landroid/content/Context;)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(I)V

    .line 187
    iget-object v0, p0, Lkxb;->at:Llnl;

    invoke-static {v0}, Llcm;->a(Landroid/content/Context;)I

    move-result v0

    .line 188
    iget-object v2, p0, Lkxb;->P:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-virtual {v2, v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->b(I)V

    .line 189
    iget-object v2, p0, Lkxb;->P:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-virtual {v2, v0, v3, v0, v3}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->setPadding(IIII)V

    .line 191
    iget-object v0, p0, Lkxb;->P:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    new-instance v2, Lkxd;

    invoke-direct {v2}, Lkxd;-><init>()V

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(Llkg;)V

    .line 201
    invoke-direct {p0}, Lkxb;->a()V

    .line 202
    return-object v1
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 92
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 94
    new-instance v5, Lkvq;

    iget-object v0, p0, Lkxb;->at:Llnl;

    iget-object v1, p0, Lkxb;->O:Lhee;

    .line 95
    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    invoke-direct {v5, v0, p0, v1}, Lkvq;-><init>(Landroid/content/Context;Lu;I)V

    .line 96
    const/4 v0, 0x1

    invoke-virtual {v5, v0}, Lkvq;->a(Z)V

    .line 98
    new-instance v0, Lkwz;

    invoke-virtual {p0}, Lkxb;->n()Lz;

    move-result-object v1

    invoke-virtual {p0}, Lkxb;->p()Lae;

    move-result-object v2

    .line 99
    invoke-virtual {p0}, Lkxb;->w()Lbb;

    move-result-object v3

    iget-object v4, p0, Lkxb;->O:Lhee;

    invoke-interface {v4}, Lhee;->d()I

    move-result v4

    invoke-direct/range {v0 .. v5}, Lkwz;-><init>(Landroid/content/Context;Lae;Lbb;ILkvq;)V

    iput-object v0, p0, Lkxb;->Q:Lkwz;

    .line 100
    iget-object v0, p0, Lkxb;->Q:Lkwz;

    invoke-virtual {v0, p1}, Lkwz;->a(Landroid/os/Bundle;)V

    .line 101
    iget-object v0, p0, Lkxb;->Q:Lkwz;

    invoke-virtual {v0, p0}, Lkwz;->a(Lkxa;)V

    .line 103
    if-eqz p1, :cond_0

    .line 104
    const-string v0, "query"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkxb;->S:Ljava/lang/String;

    .line 105
    const-string v0, "delayed_query"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkxb;->R:Ljava/lang/String;

    .line 109
    :goto_0
    return-void

    .line 107
    :cond_0
    invoke-virtual {p0}, Lkxb;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "query"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkxb;->R:Ljava/lang/String;

    goto :goto_0
.end method

.method public a(Lhjk;)V
    .locals 0

    .prologue
    .line 169
    return-void
.end method

.method public a(Ljava/lang/String;I)V
    .locals 4

    .prologue
    .line 280
    iget-object v0, p0, Lkxb;->O:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v1

    .line 281
    iget-object v0, p0, Lkxb;->au:Llnh;

    const-class v2, Lhms;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    iget-object v3, p0, Lkxb;->at:Llnl;

    invoke-direct {v2, v3, v1}, Lhmr;-><init>(Landroid/content/Context;I)V

    .line 283
    invoke-static {p2}, Lkvu;->b(I)Lhmv;

    move-result-object v1

    invoke-virtual {v2, v1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    const-string v2, "extra_square_id"

    .line 284
    invoke-static {v2, p1}, Lhmt;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v1, v2}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    .line 281
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 286
    return-void
.end method

.method public a(Ljava/lang/String;ILhoz;)V
    .locals 0

    .prologue
    .line 289
    return-void
.end method

.method public a(Loo;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 127
    invoke-static {p1, v4}, Lley;->a(Loo;Z)V

    .line 129
    invoke-virtual {p0}, Lkxb;->n()Lz;

    move-result-object v0

    .line 130
    new-instance v1, Landroid/support/v7/widget/SearchView;

    invoke-virtual {p1}, Loo;->i()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/support/v7/widget/SearchView;-><init>(Landroid/content/Context;)V

    .line 131
    invoke-static {v0, v1}, Lley;->a(Landroid/content/Context;Landroid/support/v7/widget/SearchView;)V

    .line 132
    invoke-virtual {p0}, Lkxb;->o()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a04e8

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/SearchView;->a(Ljava/lang/CharSequence;)V

    .line 133
    invoke-virtual {v1, v4}, Landroid/support/v7/widget/SearchView;->a(Z)V

    .line 134
    iget-object v0, p0, Lkxb;->S:Ljava/lang/String;

    invoke-virtual {v1, v0, v4}, Landroid/support/v7/widget/SearchView;->a(Ljava/lang/CharSequence;Z)V

    .line 135
    invoke-virtual {v1, p0}, Landroid/support/v7/widget/SearchView;->a(Lze;)V

    .line 136
    invoke-virtual {v1}, Landroid/support/v7/widget/SearchView;->requestFocus()Z

    .line 138
    new-instance v0, Lkxc;

    invoke-direct {v0, p0, v1}, Lkxc;-><init>(Lkxb;Landroid/support/v7/widget/SearchView;)V

    const-wide/16 v2, 0x32

    invoke-virtual {v1, v0, v2, v3}, Landroid/support/v7/widget/SearchView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 150
    iput-object v1, p0, Lkxb;->T:Landroid/support/v7/widget/SearchView;

    .line 152
    invoke-virtual {p1, v5}, Loo;->c(Z)V

    .line 153
    invoke-static {p1, v5}, Lley;->a(Loo;Z)V

    .line 154
    invoke-virtual {p1, v5}, Loo;->d(Z)V

    .line 155
    invoke-virtual {p1, v1}, Loo;->a(Landroid/view/View;)V

    .line 156
    invoke-virtual {p1, v5}, Loo;->e(Z)V

    .line 157
    invoke-virtual {p1, v4}, Loo;->d(Z)V

    .line 158
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 173
    const/4 v0, 0x0

    return v0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 258
    iput-object p1, p0, Lkxb;->S:Ljava/lang/String;

    .line 259
    invoke-virtual {p0}, Lkxb;->x()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Llsn;->b(Landroid/view/View;)V

    .line 260
    iget-object v0, p0, Lkxb;->T:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView;->clearFocus()V

    .line 261
    const/4 v0, 0x1

    return v0
.end method

.method public aO_()V
    .locals 3

    .prologue
    .line 223
    invoke-super {p0}, Llol;->aO_()V

    .line 225
    iget-object v0, p0, Lkxb;->R:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 226
    iget-object v0, p0, Lkxb;->R:Ljava/lang/String;

    iput-object v0, p0, Lkxb;->S:Ljava/lang/String;

    .line 227
    iget-object v0, p0, Lkxb;->T:Landroid/support/v7/widget/SearchView;

    iget-object v1, p0, Lkxb;->S:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/SearchView;->a(Ljava/lang/CharSequence;Z)V

    .line 228
    iget-object v0, p0, Lkxb;->Q:Lkwz;

    iget-object v1, p0, Lkxb;->S:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lkwz;->b(Ljava/lang/String;)V

    .line 229
    const/4 v0, 0x0

    iput-object v0, p0, Lkxb;->R:Ljava/lang/String;

    .line 230
    iget-object v0, p0, Lkxb;->N:Lhje;

    invoke-virtual {v0}, Lhje;->a()V

    .line 233
    :cond_0
    invoke-virtual {p0}, Lkxb;->x()Landroid/view/View;

    invoke-direct {p0}, Lkxb;->a()V

    .line 234
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 82
    return-void
.end method

.method public b(Loo;)V
    .locals 1

    .prologue
    .line 162
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Loo;->a(Landroid/view/View;)V

    .line 163
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Loo;->e(Z)V

    .line 164
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Loo;->d(Z)V

    .line 165
    return-void
.end method

.method public b(Ljava/lang/String;)Z
    .locals 5

    .prologue
    .line 238
    if-nez p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lkxb;->S:Ljava/lang/String;

    .line 239
    iget-object v0, p0, Lkxb;->Q:Lkwz;

    if-eqz v0, :cond_0

    .line 240
    iget-object v0, p0, Lkxb;->Q:Lkwz;

    iget-object v1, p0, Lkxb;->S:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lkwz;->b(Ljava/lang/String;)V

    .line 242
    iget-object v0, p0, Lkxb;->S:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 243
    const-string v0, "extra_search_type"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Lhmt;->a(Ljava/lang/String;I)Landroid/os/Bundle;

    move-result-object v1

    .line 245
    iget-object v0, p0, Lkxb;->O:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v2

    .line 246
    iget-object v0, p0, Lkxb;->au:Llnh;

    const-class v3, Lhms;

    invoke-virtual {v0, v3}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v3, Lhmr;

    iget-object v4, p0, Lkxb;->at:Llnl;

    invoke-direct {v3, v4, v2}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v2, Lhmv;->eU:Lhmv;

    .line 248
    invoke-virtual {v3, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v2

    .line 249
    invoke-virtual {v2, v1}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    .line 246
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 253
    :cond_0
    const/4 v0, 0x1

    return v0

    .line 238
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 75
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 76
    iget-object v0, p0, Lkxb;->au:Llnh;

    const-class v1, Lhmq;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 77
    iget-object v0, p0, Lkxb;->au:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Lkxb;->O:Lhee;

    .line 78
    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 266
    iget-object v0, p0, Lkxb;->O:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v1

    .line 268
    iget-object v0, p0, Lkxb;->au:Llnh;

    iget-object v0, p0, Lkxb;->at:Llnl;

    const-class v2, Lkte;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkte;

    const/4 v2, 0x0

    .line 269
    invoke-interface {v0, v1, p1, v2}, Lkte;->a(ILjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 268
    invoke-virtual {p0, v0}, Lkxb;->a(Landroid/content/Intent;)V

    .line 271
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 113
    invoke-super {p0, p1}, Llol;->e(Landroid/os/Bundle;)V

    .line 114
    iget-object v0, p0, Lkxb;->Q:Lkwz;

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lkxb;->Q:Lkwz;

    invoke-virtual {v0, p1}, Lkwz;->b(Landroid/os/Bundle;)V

    .line 117
    :cond_0
    iget-object v0, p0, Lkxb;->R:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 118
    const-string v0, "delayed_query"

    iget-object v1, p0, Lkxb;->R:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    :cond_1
    iget-object v0, p0, Lkxb;->S:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 121
    const-string v0, "query"

    iget-object v1, p0, Lkxb;->S:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    :cond_2
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 211
    invoke-super {p0}, Llol;->g()V

    .line 212
    iget-object v0, p0, Lkxb;->Q:Lkwz;

    invoke-virtual {v0}, Lkwz;->d()V

    .line 213
    return-void
.end method

.method public h()V
    .locals 1

    .prologue
    .line 217
    invoke-super {p0}, Llol;->g()V

    .line 218
    iget-object v0, p0, Lkxb;->Q:Lkwz;

    invoke-virtual {v0}, Lkwz;->e()V

    .line 219
    return-void
.end method
