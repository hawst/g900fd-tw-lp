.class public final Lmgc;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnec;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 27457
    invoke-direct {p0}, Loxq;-><init>()V

    .line 27460
    iput-object v0, p0, Lmgc;->apiHeader:Llyq;

    .line 27463
    iput-object v0, p0, Lmgc;->a:Lnec;

    .line 27457
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 27480
    const/4 v0, 0x0

    .line 27481
    iget-object v1, p0, Lmgc;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 27482
    const/4 v0, 0x1

    iget-object v1, p0, Lmgc;->apiHeader:Llyq;

    .line 27483
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 27485
    :cond_0
    iget-object v1, p0, Lmgc;->a:Lnec;

    if-eqz v1, :cond_1

    .line 27486
    const/4 v1, 0x2

    iget-object v2, p0, Lmgc;->a:Lnec;

    .line 27487
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 27489
    :cond_1
    iget-object v1, p0, Lmgc;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 27490
    iput v0, p0, Lmgc;->ai:I

    .line 27491
    return v0
.end method

.method public a(Loxn;)Lmgc;
    .locals 2

    .prologue
    .line 27499
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 27500
    sparse-switch v0, :sswitch_data_0

    .line 27504
    iget-object v1, p0, Lmgc;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 27505
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmgc;->ah:Ljava/util/List;

    .line 27508
    :cond_1
    iget-object v1, p0, Lmgc;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 27510
    :sswitch_0
    return-object p0

    .line 27515
    :sswitch_1
    iget-object v0, p0, Lmgc;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 27516
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmgc;->apiHeader:Llyq;

    .line 27518
    :cond_2
    iget-object v0, p0, Lmgc;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 27522
    :sswitch_2
    iget-object v0, p0, Lmgc;->a:Lnec;

    if-nez v0, :cond_3

    .line 27523
    new-instance v0, Lnec;

    invoke-direct {v0}, Lnec;-><init>()V

    iput-object v0, p0, Lmgc;->a:Lnec;

    .line 27525
    :cond_3
    iget-object v0, p0, Lmgc;->a:Lnec;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 27500
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 27468
    iget-object v0, p0, Lmgc;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 27469
    const/4 v0, 0x1

    iget-object v1, p0, Lmgc;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 27471
    :cond_0
    iget-object v0, p0, Lmgc;->a:Lnec;

    if-eqz v0, :cond_1

    .line 27472
    const/4 v0, 0x2

    iget-object v1, p0, Lmgc;->a:Lnec;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 27474
    :cond_1
    iget-object v0, p0, Lmgc;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 27476
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 27453
    invoke-virtual {p0, p1}, Lmgc;->a(Loxn;)Lmgc;

    move-result-object v0

    return-object v0
.end method
