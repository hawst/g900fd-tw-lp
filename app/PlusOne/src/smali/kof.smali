.class public Lkof;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhlv;


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I

.field public final d:Z

.field public final e:I

.field public final f:I

.field public final g:I

.field public final h:I

.field private i:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;ZIIIIIII)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lkof;->i:Ljava/lang/String;

    .line 41
    iput-boolean p2, p0, Lkof;->d:Z

    .line 42
    iput p3, p0, Lkof;->a:I

    .line 43
    iput p4, p0, Lkof;->c:I

    .line 44
    iput p5, p0, Lkof;->b:I

    .line 45
    iput p6, p0, Lkof;->e:I

    .line 46
    iput p7, p0, Lkof;->f:I

    .line 47
    iput p8, p0, Lkof;->g:I

    .line 48
    iput p9, p0, Lkof;->h:I

    .line 49
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Lhlx;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lkof;->i:Ljava/lang/String;

    return-object v0
.end method

.method public a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 58
    const-class v0, Lhlw;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhlw;

    invoke-interface {v0, p1, p0}, Lhlw;->a(Landroid/content/Context;Lhlv;)V

    .line 59
    return-void
.end method

.method public a(Lhmp;)V
    .locals 0

    .prologue
    .line 62
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 66
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "NetworkQueuesFailureEvent: numProcessed=%d, numPhotos=%d, numPhotosUploaded=%d, numVideos=%d, numVideosUploaded=%d, userCanceled=%s"

    const/4 v0, 0x6

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    iget v4, p0, Lkof;->a:I

    .line 69
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x1

    iget v4, p0, Lkof;->e:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x2

    iget v4, p0, Lkof;->f:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x3

    iget v4, p0, Lkof;->g:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x4

    iget v4, p0, Lkof;->h:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v4, 0x5

    iget-boolean v0, p0, Lkof;->d:Z

    if-eqz v0, :cond_0

    const-string v0, "yes"

    :goto_0
    aput-object v0, v3, v4

    .line 66
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 69
    :cond_0
    const-string v0, "no"

    goto :goto_0
.end method
