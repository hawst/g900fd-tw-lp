.class public final Lppb;
.super Loxq;
.source "PG"


# instance fields
.field private a:I

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Lppf;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Loxq;-><init>()V

    .line 24
    const/high16 v0, -0x80000000

    iput v0, p0, Lppb;->a:I

    .line 31
    const/4 v0, 0x0

    iput-object v0, p0, Lppb;->d:Lppf;

    .line 14
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 54
    const/4 v0, 0x0

    .line 55
    iget v1, p0, Lppb;->a:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 56
    const/4 v0, 0x1

    iget v1, p0, Lppb;->a:I

    .line 57
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 59
    :cond_0
    iget-object v1, p0, Lppb;->d:Lppf;

    if-eqz v1, :cond_1

    .line 60
    const/4 v1, 0x2

    iget-object v2, p0, Lppb;->d:Lppf;

    .line 61
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 63
    :cond_1
    iget-object v1, p0, Lppb;->b:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 64
    const/4 v1, 0x3

    iget-object v2, p0, Lppb;->b:Ljava/lang/String;

    .line 65
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 67
    :cond_2
    iget-object v1, p0, Lppb;->c:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 68
    const/4 v1, 0x4

    iget-object v2, p0, Lppb;->c:Ljava/lang/String;

    .line 69
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 71
    :cond_3
    iget-object v1, p0, Lppb;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 72
    iput v0, p0, Lppb;->ai:I

    .line 73
    return v0
.end method

.method public a(Loxn;)Lppb;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 81
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 82
    sparse-switch v0, :sswitch_data_0

    .line 86
    iget-object v1, p0, Lppb;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 87
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lppb;->ah:Ljava/util/List;

    .line 90
    :cond_1
    iget-object v1, p0, Lppb;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 92
    :sswitch_0
    return-object p0

    .line 97
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 98
    if-eq v0, v2, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    .line 102
    :cond_2
    iput v0, p0, Lppb;->a:I

    goto :goto_0

    .line 104
    :cond_3
    iput v2, p0, Lppb;->a:I

    goto :goto_0

    .line 109
    :sswitch_2
    iget-object v0, p0, Lppb;->d:Lppf;

    if-nez v0, :cond_4

    .line 110
    new-instance v0, Lppf;

    invoke-direct {v0}, Lppf;-><init>()V

    iput-object v0, p0, Lppb;->d:Lppf;

    .line 112
    :cond_4
    iget-object v0, p0, Lppb;->d:Lppf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 116
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lppb;->b:Ljava/lang/String;

    goto :goto_0

    .line 120
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lppb;->c:Ljava/lang/String;

    goto :goto_0

    .line 82
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 36
    iget v0, p0, Lppb;->a:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 37
    const/4 v0, 0x1

    iget v1, p0, Lppb;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 39
    :cond_0
    iget-object v0, p0, Lppb;->d:Lppf;

    if-eqz v0, :cond_1

    .line 40
    const/4 v0, 0x2

    iget-object v1, p0, Lppb;->d:Lppf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 42
    :cond_1
    iget-object v0, p0, Lppb;->b:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 43
    const/4 v0, 0x3

    iget-object v1, p0, Lppb;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 45
    :cond_2
    iget-object v0, p0, Lppb;->c:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 46
    const/4 v0, 0x4

    iget-object v1, p0, Lppb;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 48
    :cond_3
    iget-object v0, p0, Lppb;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 50
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 10
    invoke-virtual {p0, p1}, Lppb;->a(Loxn;)Lppb;

    move-result-object v0

    return-object v0
.end method
