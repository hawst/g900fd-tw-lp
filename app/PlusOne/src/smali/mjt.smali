.class public final Lmjt;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnpn;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 4534
    invoke-direct {p0}, Loxq;-><init>()V

    .line 4537
    iput-object v0, p0, Lmjt;->apiHeader:Llyr;

    .line 4540
    iput-object v0, p0, Lmjt;->a:Lnpn;

    .line 4534
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 4557
    const/4 v0, 0x0

    .line 4558
    iget-object v1, p0, Lmjt;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 4559
    const/4 v0, 0x1

    iget-object v1, p0, Lmjt;->apiHeader:Llyr;

    .line 4560
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 4562
    :cond_0
    iget-object v1, p0, Lmjt;->a:Lnpn;

    if-eqz v1, :cond_1

    .line 4563
    const/4 v1, 0x2

    iget-object v2, p0, Lmjt;->a:Lnpn;

    .line 4564
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4566
    :cond_1
    iget-object v1, p0, Lmjt;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4567
    iput v0, p0, Lmjt;->ai:I

    .line 4568
    return v0
.end method

.method public a(Loxn;)Lmjt;
    .locals 2

    .prologue
    .line 4576
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 4577
    sparse-switch v0, :sswitch_data_0

    .line 4581
    iget-object v1, p0, Lmjt;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 4582
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmjt;->ah:Ljava/util/List;

    .line 4585
    :cond_1
    iget-object v1, p0, Lmjt;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4587
    :sswitch_0
    return-object p0

    .line 4592
    :sswitch_1
    iget-object v0, p0, Lmjt;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 4593
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmjt;->apiHeader:Llyr;

    .line 4595
    :cond_2
    iget-object v0, p0, Lmjt;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 4599
    :sswitch_2
    iget-object v0, p0, Lmjt;->a:Lnpn;

    if-nez v0, :cond_3

    .line 4600
    new-instance v0, Lnpn;

    invoke-direct {v0}, Lnpn;-><init>()V

    iput-object v0, p0, Lmjt;->a:Lnpn;

    .line 4602
    :cond_3
    iget-object v0, p0, Lmjt;->a:Lnpn;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 4577
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 4545
    iget-object v0, p0, Lmjt;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 4546
    const/4 v0, 0x1

    iget-object v1, p0, Lmjt;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 4548
    :cond_0
    iget-object v0, p0, Lmjt;->a:Lnpn;

    if-eqz v0, :cond_1

    .line 4549
    const/4 v0, 0x2

    iget-object v1, p0, Lmjt;->a:Lnpn;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 4551
    :cond_1
    iget-object v0, p0, Lmjt;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 4553
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 4530
    invoke-virtual {p0, p1}, Lmjt;->a(Loxn;)Lmjt;

    move-result-object v0

    return-object v0
.end method
