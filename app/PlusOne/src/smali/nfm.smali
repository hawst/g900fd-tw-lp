.class public final Lnfm;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnfm;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1575
    const/4 v0, 0x0

    new-array v0, v0, [Lnfm;

    sput-object v0, Lnfm;->a:[Lnfm;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1576
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1607
    const/4 v0, 0x0

    .line 1608
    iget-object v1, p0, Lnfm;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1609
    const/4 v0, 0x1

    iget-object v1, p0, Lnfm;->b:Ljava/lang/String;

    .line 1610
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1612
    :cond_0
    iget-object v1, p0, Lnfm;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1613
    const/4 v1, 0x2

    iget-object v2, p0, Lnfm;->c:Ljava/lang/String;

    .line 1614
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1616
    :cond_1
    iget-object v1, p0, Lnfm;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1617
    const/4 v1, 0x3

    iget-object v2, p0, Lnfm;->d:Ljava/lang/String;

    .line 1618
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1620
    :cond_2
    iget-object v1, p0, Lnfm;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 1621
    const/4 v1, 0x4

    iget-object v2, p0, Lnfm;->e:Ljava/lang/String;

    .line 1622
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1624
    :cond_3
    iget-object v1, p0, Lnfm;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1625
    iput v0, p0, Lnfm;->ai:I

    .line 1626
    return v0
.end method

.method public a(Loxn;)Lnfm;
    .locals 2

    .prologue
    .line 1634
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1635
    sparse-switch v0, :sswitch_data_0

    .line 1639
    iget-object v1, p0, Lnfm;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1640
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnfm;->ah:Ljava/util/List;

    .line 1643
    :cond_1
    iget-object v1, p0, Lnfm;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1645
    :sswitch_0
    return-object p0

    .line 1650
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnfm;->b:Ljava/lang/String;

    goto :goto_0

    .line 1654
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnfm;->c:Ljava/lang/String;

    goto :goto_0

    .line 1658
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnfm;->d:Ljava/lang/String;

    goto :goto_0

    .line 1662
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnfm;->e:Ljava/lang/String;

    goto :goto_0

    .line 1635
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1589
    iget-object v0, p0, Lnfm;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1590
    const/4 v0, 0x1

    iget-object v1, p0, Lnfm;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1592
    :cond_0
    iget-object v0, p0, Lnfm;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1593
    const/4 v0, 0x2

    iget-object v1, p0, Lnfm;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1595
    :cond_1
    iget-object v0, p0, Lnfm;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1596
    const/4 v0, 0x3

    iget-object v1, p0, Lnfm;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1598
    :cond_2
    iget-object v0, p0, Lnfm;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1599
    const/4 v0, 0x4

    iget-object v1, p0, Lnfm;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1601
    :cond_3
    iget-object v0, p0, Lnfm;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1603
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1572
    invoke-virtual {p0, p1}, Lnfm;->a(Loxn;)Lnfm;

    move-result-object v0

    return-object v0
.end method
