.class public final Lmtb;
.super Loxq;
.source "PG"


# instance fields
.field private a:[Ljava/lang/Integer;

.field private b:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3154
    invoke-direct {p0}, Loxq;-><init>()V

    .line 3157
    sget-object v0, Loxx;->g:[Ljava/lang/Integer;

    iput-object v0, p0, Lmtb;->a:[Ljava/lang/Integer;

    .line 3154
    return-void
.end method


# virtual methods
.method public a()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 3178
    .line 3179
    iget-object v1, p0, Lmtb;->a:[Ljava/lang/Integer;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lmtb;->a:[Ljava/lang/Integer;

    array-length v1, v1

    if-lez v1, :cond_1

    .line 3181
    iget-object v2, p0, Lmtb;->a:[Ljava/lang/Integer;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 3183
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v4}, Loxo;->i(I)I

    move-result v4

    add-int/2addr v1, v4

    .line 3181
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3186
    :cond_0
    iget-object v0, p0, Lmtb;->a:[Ljava/lang/Integer;

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v1

    .line 3188
    :cond_1
    iget-object v1, p0, Lmtb;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 3189
    const/4 v1, 0x2

    iget-object v2, p0, Lmtb;->b:Ljava/lang/Integer;

    .line 3190
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3192
    :cond_2
    iget-object v1, p0, Lmtb;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3193
    iput v0, p0, Lmtb;->ai:I

    .line 3194
    return v0
.end method

.method public a(Loxn;)Lmtb;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 3202
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3203
    sparse-switch v0, :sswitch_data_0

    .line 3207
    iget-object v1, p0, Lmtb;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 3208
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmtb;->ah:Ljava/util/List;

    .line 3211
    :cond_1
    iget-object v1, p0, Lmtb;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3213
    :sswitch_0
    return-object p0

    .line 3218
    :sswitch_1
    const/16 v0, 0x8

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 3219
    iget-object v0, p0, Lmtb;->a:[Ljava/lang/Integer;

    array-length v0, v0

    .line 3220
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/Integer;

    .line 3221
    iget-object v2, p0, Lmtb;->a:[Ljava/lang/Integer;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3222
    iput-object v1, p0, Lmtb;->a:[Ljava/lang/Integer;

    .line 3223
    :goto_1
    iget-object v1, p0, Lmtb;->a:[Ljava/lang/Integer;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 3224
    iget-object v1, p0, Lmtb;->a:[Ljava/lang/Integer;

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    .line 3225
    invoke-virtual {p1}, Loxn;->a()I

    .line 3223
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 3228
    :cond_2
    iget-object v1, p0, Lmtb;->a:[Ljava/lang/Integer;

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    .line 3232
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmtb;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 3203
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 3164
    iget-object v0, p0, Lmtb;->a:[Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 3165
    iget-object v1, p0, Lmtb;->a:[Ljava/lang/Integer;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 3166
    const/4 v4, 0x1

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p1, v4, v3}, Loxo;->a(II)V

    .line 3165
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3169
    :cond_0
    iget-object v0, p0, Lmtb;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 3170
    const/4 v0, 0x2

    iget-object v1, p0, Lmtb;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 3172
    :cond_1
    iget-object v0, p0, Lmtb;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3174
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3150
    invoke-virtual {p0, p1}, Lmtb;->a(Loxn;)Lmtb;

    move-result-object v0

    return-object v0
.end method
