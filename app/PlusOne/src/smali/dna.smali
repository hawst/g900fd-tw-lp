.class public final Ldna;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmas;",
        "Lmat;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:I

.field private final p:Ljava/lang/String;

.field private final q:I

.field private final r:[B


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;II[B)V
    .locals 6

    .prologue
    .line 41
    const-string v3, "eventrespond"

    new-instance v4, Lmas;

    invoke-direct {v4}, Lmas;-><init>()V

    new-instance v5, Lmat;

    invoke-direct {v5}, Lmat;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 43
    iput-object p4, p0, Ldna;->p:Ljava/lang/String;

    .line 44
    iput-object p3, p0, Ldna;->a:Ljava/lang/String;

    .line 45
    iput p5, p0, Ldna;->b:I

    .line 46
    iput p6, p0, Ldna;->q:I

    .line 47
    iput-object p7, p0, Ldna;->r:[B

    .line 48
    return-void
.end method

.method private i()V
    .locals 4

    .prologue
    .line 90
    iget-object v0, p0, Ldna;->f:Landroid/content/Context;

    iget v1, p0, Ldna;->c:I

    iget-object v2, p0, Ldna;->a:Ljava/lang/String;

    .line 91
    invoke-static {v0, v1, v2}, Ldrm;->a(Landroid/content/Context;ILjava/lang/String;)Lidh;

    move-result-object v0

    .line 95
    iget v1, p0, Ldna;->b:I

    invoke-static {v0}, Ldrm;->a(Lidh;)I

    move-result v0

    if-ne v1, v0, :cond_0

    .line 96
    iget-object v0, p0, Ldna;->f:Landroid/content/Context;

    iget v1, p0, Ldna;->c:I

    iget-object v2, p0, Ldna;->a:Ljava/lang/String;

    iget v3, p0, Ldna;->q:I

    invoke-static {v0, v1, v2, v3}, Ldrm;->a(Landroid/content/Context;ILjava/lang/String;I)I

    .line 101
    :cond_0
    iget-object v0, p0, Ldna;->f:Landroid/content/Context;

    iget v1, p0, Ldna;->c:I

    iget-object v2, p0, Ldna;->a:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Ldrm;->b(Landroid/content/Context;ILjava/lang/String;)V

    .line 102
    return-void
.end method


# virtual methods
.method public a(ILjava/lang/String;Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 84
    const/16 v0, 0xc8

    if-ne p1, v0, :cond_0

    if-eqz p3, :cond_1

    .line 85
    :cond_0
    invoke-direct {p0}, Ldna;->i()V

    .line 87
    :cond_1
    return-void
.end method

.method protected a(Lmas;)V
    .locals 3

    .prologue
    .line 52
    new-instance v0, Lmqp;

    invoke-direct {v0}, Lmqp;-><init>()V

    iput-object v0, p1, Lmas;->a:Lmqp;

    .line 53
    iget-object v0, p1, Lmas;->a:Lmqp;

    .line 54
    iget-object v1, p0, Ldna;->a:Ljava/lang/String;

    iput-object v1, v0, Lmqp;->b:Ljava/lang/String;

    .line 55
    iget v1, p0, Ldna;->b:I

    iput v1, v0, Lmqp;->a:I

    .line 57
    new-instance v1, Lmpm;

    invoke-direct {v1}, Lmpm;-><init>()V

    .line 58
    iget-object v2, p0, Ldna;->p:Ljava/lang/String;

    iput-object v2, v1, Lmpm;->b:Ljava/lang/String;

    .line 59
    iget-object v2, p0, Ldna;->a:Ljava/lang/String;

    iput-object v2, v1, Lmpm;->a:Ljava/lang/String;

    .line 60
    iput-object v1, v0, Lmqp;->c:Lmpm;

    .line 62
    iget-object v1, p0, Ldna;->r:[B

    invoke-static {v1}, Llap;->a([B)Lnwr;

    move-result-object v1

    .line 63
    if-eqz v1, :cond_0

    .line 64
    new-instance v2, Logw;

    invoke-direct {v2}, Logw;-><init>()V

    iput-object v2, v0, Lmqp;->d:Logw;

    .line 65
    iget-object v0, v0, Lmqp;->d:Logw;

    iput-object v1, v0, Logw;->a:Lnwr;

    .line 67
    :cond_0
    return-void
.end method

.method protected a(Lmat;)V
    .locals 4

    .prologue
    .line 71
    iget-object v0, p1, Lmat;->a:Lmqq;

    .line 72
    iget v0, v0, Lmqq;->a:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 73
    invoke-direct {p0}, Ldna;->i()V

    .line 80
    :goto_0
    return-void

    .line 78
    :cond_0
    iget-object v0, p0, Ldna;->f:Landroid/content/Context;

    iget v1, p0, Ldna;->c:I

    iget-object v2, p0, Ldna;->a:Ljava/lang/String;

    iget v3, p0, Ldna;->b:I

    invoke-static {v0, v1, v2, v3}, Ldrm;->a(Landroid/content/Context;ILjava/lang/String;I)I

    goto :goto_0
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 22
    check-cast p1, Lmas;

    invoke-virtual {p0, p1}, Ldna;->a(Lmas;)V

    return-void
.end method

.method protected synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 22
    check-cast p1, Lmat;

    invoke-virtual {p0, p1}, Ldna;->a(Lmat;)V

    return-void
.end method
