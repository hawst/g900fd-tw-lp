.class public final Lfww;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llip;


# instance fields
.field private b:Landroid/graphics/Rect;

.field private c:Lfwx;

.field private d:Z


# direct methods
.method public constructor <init>(IIIILfwx;Ljava/lang/CharSequence;)V
    .locals 3

    .prologue
    .line 35
    new-instance v0, Landroid/graphics/Rect;

    add-int v1, p1, p3

    add-int v2, p2, p4

    invoke-direct {v0, p1, p2, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-direct {p0, v0, p5}, Lfww;-><init>(Landroid/graphics/Rect;Lfwx;)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/graphics/Rect;Lfwx;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lfww;->b:Landroid/graphics/Rect;

    .line 41
    iput-object p2, p0, Lfww;->c:Lfwx;

    .line 42
    return-void
.end method


# virtual methods
.method public a(Llip;Llip;)I
    .locals 1

    .prologue
    .line 114
    sget-object v0, Llip;->a_:Lliq;

    invoke-virtual {v0, p1, p2}, Lliq;->a(Llip;Llip;)I

    move-result v0

    return v0
.end method

.method public a()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lfww;->b:Landroid/graphics/Rect;

    return-object v0
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 104
    iput-boolean p1, p0, Lfww;->d:Z

    .line 105
    return-void
.end method

.method public a(III)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 47
    const/4 v2, 0x3

    if-ne p3, v2, :cond_0

    .line 48
    iput-boolean v1, p0, Lfww;->d:Z

    .line 70
    :goto_0
    return v0

    .line 52
    :cond_0
    iget-object v2, p0, Lfww;->b:Landroid/graphics/Rect;

    invoke-virtual {v2, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-nez v2, :cond_2

    .line 53
    if-ne p3, v0, :cond_1

    .line 54
    iput-boolean v1, p0, Lfww;->d:Z

    :cond_1
    move v0, v1

    .line 56
    goto :goto_0

    .line 59
    :cond_2
    packed-switch p3, :pswitch_data_0

    goto :goto_0

    .line 61
    :pswitch_0
    iput-boolean v0, p0, Lfww;->d:Z

    goto :goto_0

    .line 66
    :pswitch_1
    iget-boolean v2, p0, Lfww;->d:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lfww;->c:Lfwx;

    if-eqz v2, :cond_3

    .line 67
    iget-object v2, p0, Lfww;->c:Lfwx;

    invoke-interface {v2, p0}, Lfwx;->a(Lfww;)V

    .line 69
    :cond_3
    iput-boolean v1, p0, Lfww;->d:Z

    goto :goto_0

    .line 59
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 14
    check-cast p1, Llip;

    check-cast p2, Llip;

    invoke-virtual {p0, p1, p2}, Lfww;->a(Llip;Llip;)I

    move-result v0

    return v0
.end method
