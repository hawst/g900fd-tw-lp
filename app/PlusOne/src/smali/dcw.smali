.class public final Ldcw;
.super Lddd;
.source "PG"


# instance fields
.field private P:Ldcz;

.field private Q:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Lddd;-><init>()V

    .line 30
    new-instance v0, Ldcz;

    invoke-direct {v0, p0}, Ldcz;-><init>(Ldcw;)V

    iput-object v0, p0, Ldcw;->P:Ldcz;

    .line 174
    return-void
.end method

.method static synthetic a(Ldcw;)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Ldcw;->Q:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic a(Ldcw;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 0

    .prologue
    .line 24
    iput-object p1, p0, Ldcw;->Q:Ljava/lang/Integer;

    return-object p1
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 53
    invoke-super {p0, p1}, Lddd;->a(Landroid/os/Bundle;)V

    .line 54
    if-eqz p1, :cond_0

    .line 55
    const-string v0, "refresh_request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    const-string v0, "refresh_request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldcw;->Q:Ljava/lang/Integer;

    .line 59
    :cond_0
    return-void
.end method

.method public a()Z
    .locals 4

    .prologue
    .line 35
    invoke-virtual {p0}, Ldcw;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0}, Ldcw;->k()Landroid/os/Bundle;

    move-result-object v2

    new-instance v3, Ldcy;

    invoke-direct {v3, p0}, Ldcy;-><init>(Ldcw;)V

    invoke-virtual {v0, v1, v2, v3}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 38
    const/4 v0, 0x1

    return v0
.end method

.method public aO_()V
    .locals 4

    .prologue
    .line 71
    invoke-super {p0}, Lddd;->aO_()V

    .line 72
    iget-object v0, p0, Ldcw;->P:Ldcz;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(Lfhh;)V

    .line 73
    iget-object v0, p0, Ldcw;->Q:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Ldcw;->Q:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 75
    invoke-virtual {p0}, Ldcw;->n()Lz;

    .line 76
    invoke-virtual {p0}, Ldcw;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "account_id"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    .line 78
    iget-object v0, p0, Ldcw;->Q:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(I)Lfib;

    move-result-object v0

    .line 79
    iget-object v1, p0, Ldcw;->P:Ldcz;

    iget-object v2, p0, Ldcw;->Q:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0}, Ldcw;->V()Lddl;

    move-result-object v3

    invoke-interface {v3}, Lddl;->o()J

    invoke-virtual {v1, v2, v0}, Ldcz;->f(ILfib;)V

    .line 82
    :cond_0
    return-void
.end method

.method protected e()V
    .locals 6

    .prologue
    .line 43
    invoke-super {p0}, Lddd;->e()V

    .line 44
    invoke-virtual {p0}, Ldcw;->n()Lz;

    .line 45
    invoke-virtual {p0}, Ldcw;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "account_id"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 47
    invoke-virtual {p0}, Ldcw;->n()Lz;

    move-result-object v1

    .line 48
    invoke-virtual {p0}, Ldcw;->V()Lddl;

    move-result-object v2

    invoke-interface {v2}, Lddl;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Ldcw;->V()Lddl;

    move-result-object v3

    invoke-interface {v3}, Lddl;->o()J

    move-result-wide v4

    .line 47
    invoke-static {v1, v0, v2, v4, v5}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILjava/lang/String;J)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldcw;->Q:Ljava/lang/Integer;

    .line 49
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 63
    invoke-super {p0, p1}, Lddd;->e(Landroid/os/Bundle;)V

    .line 64
    iget-object v0, p0, Ldcw;->Q:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 65
    const-string v0, "refresh_request_id"

    iget-object v1, p0, Ldcw;->Q:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 67
    :cond_0
    return-void
.end method

.method public z()V
    .locals 1

    .prologue
    .line 86
    invoke-super {p0}, Lddd;->z()V

    .line 87
    iget-object v0, p0, Ldcw;->P:Ldcz;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(Lfhh;)V

    .line 88
    return-void
.end method
