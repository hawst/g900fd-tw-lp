.class public final Lnep;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/Integer;

.field public d:Lnzi;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3292
    invoke-direct {p0}, Loxq;-><init>()V

    .line 3301
    const/4 v0, 0x0

    iput-object v0, p0, Lnep;->d:Lnzi;

    .line 3292
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 3324
    const/4 v0, 0x0

    .line 3325
    iget-object v1, p0, Lnep;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3326
    const/4 v0, 0x1

    iget-object v1, p0, Lnep;->a:Ljava/lang/String;

    .line 3327
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3329
    :cond_0
    iget-object v1, p0, Lnep;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 3330
    const/4 v1, 0x2

    iget-object v2, p0, Lnep;->b:Ljava/lang/String;

    .line 3331
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3333
    :cond_1
    iget-object v1, p0, Lnep;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 3334
    const/4 v1, 0x3

    iget-object v2, p0, Lnep;->c:Ljava/lang/Integer;

    .line 3335
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3337
    :cond_2
    iget-object v1, p0, Lnep;->d:Lnzi;

    if-eqz v1, :cond_3

    .line 3338
    const/4 v1, 0x4

    iget-object v2, p0, Lnep;->d:Lnzi;

    .line 3339
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3341
    :cond_3
    iget-object v1, p0, Lnep;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3342
    iput v0, p0, Lnep;->ai:I

    .line 3343
    return v0
.end method

.method public a(Loxn;)Lnep;
    .locals 2

    .prologue
    .line 3351
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3352
    sparse-switch v0, :sswitch_data_0

    .line 3356
    iget-object v1, p0, Lnep;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 3357
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnep;->ah:Ljava/util/List;

    .line 3360
    :cond_1
    iget-object v1, p0, Lnep;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3362
    :sswitch_0
    return-object p0

    .line 3367
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnep;->a:Ljava/lang/String;

    goto :goto_0

    .line 3371
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnep;->b:Ljava/lang/String;

    goto :goto_0

    .line 3375
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnep;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 3379
    :sswitch_4
    iget-object v0, p0, Lnep;->d:Lnzi;

    if-nez v0, :cond_2

    .line 3380
    new-instance v0, Lnzi;

    invoke-direct {v0}, Lnzi;-><init>()V

    iput-object v0, p0, Lnep;->d:Lnzi;

    .line 3382
    :cond_2
    iget-object v0, p0, Lnep;->d:Lnzi;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3352
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 3306
    iget-object v0, p0, Lnep;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 3307
    const/4 v0, 0x1

    iget-object v1, p0, Lnep;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3309
    :cond_0
    iget-object v0, p0, Lnep;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 3310
    const/4 v0, 0x2

    iget-object v1, p0, Lnep;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3312
    :cond_1
    iget-object v0, p0, Lnep;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 3313
    const/4 v0, 0x3

    iget-object v1, p0, Lnep;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 3315
    :cond_2
    iget-object v0, p0, Lnep;->d:Lnzi;

    if-eqz v0, :cond_3

    .line 3316
    const/4 v0, 0x4

    iget-object v1, p0, Lnep;->d:Lnzi;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 3318
    :cond_3
    iget-object v0, p0, Lnep;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3320
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3288
    invoke-virtual {p0, p1}, Lnep;->a(Loxn;)Lnep;

    move-result-object v0

    return-object v0
.end method
