.class public final Lmfk;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnlh;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1213
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1216
    iput-object v0, p0, Lmfk;->apiHeader:Llyq;

    .line 1219
    iput-object v0, p0, Lmfk;->a:Lnlh;

    .line 1213
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1236
    const/4 v0, 0x0

    .line 1237
    iget-object v1, p0, Lmfk;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 1238
    const/4 v0, 0x1

    iget-object v1, p0, Lmfk;->apiHeader:Llyq;

    .line 1239
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1241
    :cond_0
    iget-object v1, p0, Lmfk;->a:Lnlh;

    if-eqz v1, :cond_1

    .line 1242
    const/4 v1, 0x2

    iget-object v2, p0, Lmfk;->a:Lnlh;

    .line 1243
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1245
    :cond_1
    iget-object v1, p0, Lmfk;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1246
    iput v0, p0, Lmfk;->ai:I

    .line 1247
    return v0
.end method

.method public a(Loxn;)Lmfk;
    .locals 2

    .prologue
    .line 1255
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1256
    sparse-switch v0, :sswitch_data_0

    .line 1260
    iget-object v1, p0, Lmfk;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1261
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmfk;->ah:Ljava/util/List;

    .line 1264
    :cond_1
    iget-object v1, p0, Lmfk;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1266
    :sswitch_0
    return-object p0

    .line 1271
    :sswitch_1
    iget-object v0, p0, Lmfk;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 1272
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmfk;->apiHeader:Llyq;

    .line 1274
    :cond_2
    iget-object v0, p0, Lmfk;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1278
    :sswitch_2
    iget-object v0, p0, Lmfk;->a:Lnlh;

    if-nez v0, :cond_3

    .line 1279
    new-instance v0, Lnlh;

    invoke-direct {v0}, Lnlh;-><init>()V

    iput-object v0, p0, Lmfk;->a:Lnlh;

    .line 1281
    :cond_3
    iget-object v0, p0, Lmfk;->a:Lnlh;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1256
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1224
    iget-object v0, p0, Lmfk;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 1225
    const/4 v0, 0x1

    iget-object v1, p0, Lmfk;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1227
    :cond_0
    iget-object v0, p0, Lmfk;->a:Lnlh;

    if-eqz v0, :cond_1

    .line 1228
    const/4 v0, 0x2

    iget-object v1, p0, Lmfk;->a:Lnlh;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1230
    :cond_1
    iget-object v0, p0, Lmfk;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1232
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1209
    invoke-virtual {p0, p1}, Lmfk;->a(Loxn;)Lmfk;

    move-result-object v0

    return-object v0
.end method
