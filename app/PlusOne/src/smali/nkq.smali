.class public final Lnkq;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lnkp;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 114
    invoke-direct {p0}, Loxq;-><init>()V

    .line 117
    sget-object v0, Lnkp;->a:[Lnkp;

    iput-object v0, p0, Lnkq;->a:[Lnkp;

    .line 114
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 145
    .line 146
    iget-object v1, p0, Lnkq;->a:[Lnkp;

    if-eqz v1, :cond_1

    .line 147
    iget-object v2, p0, Lnkq;->a:[Lnkp;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 148
    if-eqz v4, :cond_0

    .line 149
    const/4 v5, 0x1

    .line 150
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 147
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 154
    :cond_1
    iget-object v1, p0, Lnkq;->b:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 155
    const/4 v1, 0x2

    iget-object v2, p0, Lnkq;->b:Ljava/lang/String;

    .line 156
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 158
    :cond_2
    iget-object v1, p0, Lnkq;->c:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 159
    const/4 v1, 0x3

    iget-object v2, p0, Lnkq;->c:Ljava/lang/String;

    .line 160
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 162
    :cond_3
    iget-object v1, p0, Lnkq;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 163
    iput v0, p0, Lnkq;->ai:I

    .line 164
    return v0
.end method

.method public a(Loxn;)Lnkq;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 172
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 173
    sparse-switch v0, :sswitch_data_0

    .line 177
    iget-object v2, p0, Lnkq;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 178
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnkq;->ah:Ljava/util/List;

    .line 181
    :cond_1
    iget-object v2, p0, Lnkq;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 183
    :sswitch_0
    return-object p0

    .line 188
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 189
    iget-object v0, p0, Lnkq;->a:[Lnkp;

    if-nez v0, :cond_3

    move v0, v1

    .line 190
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnkp;

    .line 191
    iget-object v3, p0, Lnkq;->a:[Lnkp;

    if-eqz v3, :cond_2

    .line 192
    iget-object v3, p0, Lnkq;->a:[Lnkp;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 194
    :cond_2
    iput-object v2, p0, Lnkq;->a:[Lnkp;

    .line 195
    :goto_2
    iget-object v2, p0, Lnkq;->a:[Lnkp;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 196
    iget-object v2, p0, Lnkq;->a:[Lnkp;

    new-instance v3, Lnkp;

    invoke-direct {v3}, Lnkp;-><init>()V

    aput-object v3, v2, v0

    .line 197
    iget-object v2, p0, Lnkq;->a:[Lnkp;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 198
    invoke-virtual {p1}, Loxn;->a()I

    .line 195
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 189
    :cond_3
    iget-object v0, p0, Lnkq;->a:[Lnkp;

    array-length v0, v0

    goto :goto_1

    .line 201
    :cond_4
    iget-object v2, p0, Lnkq;->a:[Lnkp;

    new-instance v3, Lnkp;

    invoke-direct {v3}, Lnkp;-><init>()V

    aput-object v3, v2, v0

    .line 202
    iget-object v2, p0, Lnkq;->a:[Lnkp;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 206
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnkq;->b:Ljava/lang/String;

    goto :goto_0

    .line 210
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnkq;->c:Ljava/lang/String;

    goto :goto_0

    .line 173
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 126
    iget-object v0, p0, Lnkq;->a:[Lnkp;

    if-eqz v0, :cond_1

    .line 127
    iget-object v1, p0, Lnkq;->a:[Lnkp;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 128
    if-eqz v3, :cond_0

    .line 129
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 127
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 133
    :cond_1
    iget-object v0, p0, Lnkq;->b:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 134
    const/4 v0, 0x2

    iget-object v1, p0, Lnkq;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 136
    :cond_2
    iget-object v0, p0, Lnkq;->c:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 137
    const/4 v0, 0x3

    iget-object v1, p0, Lnkq;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 139
    :cond_3
    iget-object v0, p0, Lnkq;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 141
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 110
    invoke-virtual {p0, p1}, Lnkq;->a(Loxn;)Lnkq;

    move-result-object v0

    return-object v0
.end method
