.class public final Ldqw;
.super Ljfb;
.source "PG"


# static fields
.field private static final c:Ljfd;


# instance fields
.field private final d:Landroid/content/Context;

.field private final e:Lhei;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 71
    new-instance v0, Ljfd;

    sget-object v1, Ljff;->b:Ljff;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v3, v3}, Ljfd;-><init>(Ljff;ILood;Ljava/lang/String;)V

    sput-object v0, Ldqw;->c:Ljfd;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 77
    invoke-direct {p0}, Ljfb;-><init>()V

    .line 78
    iput-object p1, p0, Ldqw;->d:Landroid/content/Context;

    .line 79
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Ldqw;->e:Lhei;

    .line 80
    return-void
.end method

.method private static a(Lizu;)Ljuc;
    .locals 10

    .prologue
    .line 468
    new-instance v6, Ljuc;

    invoke-direct {v6, p0}, Ljuc;-><init>(Lizu;)V

    .line 469
    new-instance v0, Ljuc;

    .line 470
    invoke-virtual {v6}, Ljuc;->k()Ljava/lang/String;

    move-result-object v1

    .line 471
    invoke-virtual {v6}, Ljuc;->j()Ljava/lang/String;

    move-result-object v2

    .line 472
    invoke-virtual {v6}, Ljuc;->f()Lizu;

    move-result-object v3

    .line 473
    invoke-virtual {v6}, Ljuc;->i()J

    move-result-wide v4

    const-wide/16 v8, 0x4000

    or-long/2addr v4, v8

    .line 474
    invoke-virtual {v6}, Ljuc;->a()J

    move-result-wide v6

    invoke-direct/range {v0 .. v7}, Ljuc;-><init>(Ljava/lang/String;Ljava/lang/String;Lizu;JJ)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 250
    new-instance v0, Ldqw;

    invoke-direct {v0, p0}, Ldqw;-><init>(Landroid/content/Context;)V

    .line 254
    sget-object v1, Ljfb;->a:Ljfb;

    if-eq v0, v1, :cond_0

    :goto_0
    sput-object v0, Ljfb;->b:Ljfb;

    .line 256
    return-void

    .line 254
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 448
    iget-object v0, p0, Ldqw;->e:Lhei;

    invoke-interface {v0, p1}, Lhei;->b(Ljava/lang/String;)I

    move-result v0

    return v0
.end method


# virtual methods
.method public a(I)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 348
    iget-object v0, p0, Ldqw;->d:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Leyq;->b(Landroid/content/Context;II)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/net/Uri;I)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 327
    iget-object v0, p0, Ldqw;->d:Landroid/content/Context;

    sget-object v1, Ljac;->b:Ljac;

    invoke-static {v0, p1, v1}, Lizu;->a(Landroid/content/Context;Landroid/net/Uri;Ljac;)Lizu;

    move-result-object v0

    .line 329
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 330
    invoke-static {v0}, Ldqw;->a(Lizu;)Ljuc;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 332
    iget-object v0, p0, Ldqw;->d:Landroid/content/Context;

    invoke-static {v0, p2, v1}, Leyq;->a(Landroid/content/Context;ILjava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljdz;Landroid/net/Uri;I)Landroid/content/Intent;
    .locals 7

    .prologue
    .line 337
    iget-object v0, p0, Ldqw;->d:Landroid/content/Context;

    iget-object v1, p1, Ljdz;->b:Ljava/lang/String;

    iget-object v2, p1, Ljdz;->a:Ljava/lang/Long;

    .line 338
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    sget-object v6, Ljac;->b:Ljac;

    .line 337
    invoke-static/range {v0 .. v6}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Ljac;)Lizu;

    move-result-object v0

    .line 340
    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 341
    invoke-static {v0}, Ldqw;->a(Lizu;)Ljuc;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 343
    iget-object v0, p0, Ldqw;->d:Landroid/content/Context;

    invoke-static {v0, p3, v1}, Leyq;->a(Landroid/content/Context;ILjava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public a(Z)Landroid/content/Intent;
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0x12
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 311
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 312
    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, p0, Ldqw;->d:Landroid/content/Context;

    const-class v3, Lcom/google/android/apps/photos/phone/GetContentActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 313
    const-string v1, "android.intent.action.GET_CONTENT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 314
    const-string v1, "exclude_tab_auto_awesome"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 315
    if-eqz p1, :cond_0

    .line 316
    const-string v1, "android.intent.extra.ALLOW_MULTIPLE"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 318
    :cond_0
    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 319
    const-string v1, "is_for_movie_maker_launch"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 320
    const-string v1, "*/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 322
    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Long;)Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    .line 444
    iget-object v0, p0, Ldqw;->d:Landroid/content/Context;

    invoke-direct {p0, p1}, Ldqw;->c(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->c(Landroid/content/Context;IJ)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 206
    invoke-direct {p0, p1}, Ldqw;->c(Ljava/lang/String;)I

    move-result v0

    .line 207
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 208
    const/4 v0, 0x0

    .line 210
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Ldqw;->d:Landroid/content/Context;

    invoke-static {v1, v0, p2}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->c(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Landroid/content/Intent;)Ljava/util/List;
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x12
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 262
    const-string v0, "shareables"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 263
    invoke-virtual {p1}, Landroid/content/Intent;->getClipData()Landroid/content/ClipData;

    move-result-object v0

    if-nez v0, :cond_0

    .line 264
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_0

    .line 265
    const-string v0, "MovieMakerProvider"

    const-string v1, "No intent data available from picker"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 266
    const/4 v0, 0x0

    .line 305
    :goto_0
    return-object v0

    .line 269
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 271
    const-string v0, "shareables"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 273
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "shareables"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 275
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljuf;

    .line 276
    invoke-interface {v0}, Ljuf;->f()Lizu;

    move-result-object v0

    invoke-virtual {v0}, Lizu;->e()Landroid/net/Uri;

    move-result-object v0

    .line 277
    if-eqz v0, :cond_1

    .line 278
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 281
    :cond_2
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    .line 296
    :cond_3
    :goto_2
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0, v1}, Ljava/util/LinkedHashSet;-><init>(Ljava/util/Collection;)V

    .line 302
    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 303
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    move-object v0, v1

    .line 305
    goto :goto_0

    .line 282
    :cond_4
    invoke-virtual {p1}, Landroid/content/Intent;->getClipData()Landroid/content/ClipData;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 285
    invoke-virtual {p1}, Landroid/content/Intent;->getClipData()Landroid/content/ClipData;

    move-result-object v2

    .line 286
    const/4 v0, 0x0

    :goto_3
    invoke-virtual {v2}, Landroid/content/ClipData;->getItemCount()I

    move-result v3

    if-ge v0, v3, :cond_5

    .line 287
    invoke-virtual {v2, v0}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 286
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 289
    :cond_5
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    goto :goto_2

    .line 290
    :cond_6
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 293
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 295
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    goto :goto_2
.end method

.method public a(Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljeb;",
            ">;"
        }
    .end annotation

    .prologue
    .line 94
    invoke-direct {p0, p1}, Ldqw;->c(Ljava/lang/String;)I

    move-result v0

    .line 95
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 98
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 100
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Ldqw;->d:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->c(Landroid/content/Context;I)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Ljed;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljed;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljej;",
            ">;"
        }
    .end annotation

    .prologue
    .line 105
    iget-object v0, p1, Ljed;->a:Ljava/lang/String;

    invoke-direct {p0, v0}, Ldqw;->c(Ljava/lang/String;)I

    move-result v0

    .line 106
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 109
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 111
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Ldqw;->d:Landroid/content/Context;

    iget-object v2, p1, Ljed;->b:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-static {v1, v0, v2, v3}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->a(Landroid/content/Context;ILjava/lang/String;Z)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Ljava/lang/Iterable;Lkfd;)Ljava/util/Map;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljdz;",
            ">;",
            "Lkfd;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljdz;",
            "Ljej;",
            ">;"
        }
    .end annotation

    .prologue
    .line 139
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljdz;

    iget-object v2, v0, Ljdz;->b:Ljava/lang/String;

    invoke-interface {v3, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, v0, Ljdz;->b:Ljava/lang/String;

    invoke-direct {p0, v2}, Ldqw;->c(Ljava/lang/String;)I

    move-result v2

    iget-object v0, v0, Ljdz;->b:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 141
    :cond_1
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 144
    const/4 v2, 0x0

    .line 146
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljdz;

    .line 147
    iget-object v0, v1, Ljdz;->b:Ljava/lang/String;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 148
    const/4 v0, -0x1

    if-eq v6, v0, :cond_2

    .line 149
    iget-object v0, p0, Ldqw;->d:Landroid/content/Context;

    iget-object v7, v1, Ljdz;->a:Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-static {v0, v6, v8, v9}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->b(Landroid/content/Context;IJ)Ljej;

    move-result-object v0

    .line 152
    if-eqz v0, :cond_3

    .line 153
    invoke-interface {v4, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 154
    :cond_3
    if-eqz p2, :cond_2

    .line 158
    if-nez v2, :cond_4

    .line 159
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    move-object v2, v0

    .line 161
    :cond_4
    invoke-virtual {v2, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 162
    if-nez v0, :cond_5

    .line 163
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 164
    invoke-virtual {v2, v6, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 166
    :cond_5
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 170
    :cond_6
    if-eqz p2, :cond_7

    if-eqz v2, :cond_7

    .line 172
    iget-object v0, p0, Ldqw;->d:Landroid/content/Context;

    invoke-static {v0, v4, v2, p2}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->a(Landroid/content/Context;Ljava/util/Map;Landroid/util/SparseArray;Lkfd;)V

    .line 175
    :cond_7
    invoke-static {v4}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;J)Ljfd;
    .locals 6

    .prologue
    .line 180
    invoke-direct {p0, p1}, Ldqw;->c(Ljava/lang/String;)I

    move-result v0

    .line 181
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 182
    sget-object v0, Ldqw;->c:Ljfd;

    .line 201
    :goto_0
    return-object v0

    .line 185
    :cond_0
    iget-object v1, p0, Ldqw;->d:Landroid/content/Context;

    invoke-static {v1, v0, p2, p3}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->a(Landroid/content/Context;IJ)Lnym;

    move-result-object v1

    .line 186
    if-nez v1, :cond_1

    .line 188
    sget-object v0, Ldqw;->c:Ljfd;

    goto :goto_0

    .line 190
    :cond_1
    iget-object v0, v1, Lnym;->G:Lnzi;

    if-eqz v0, :cond_2

    iget-object v0, v1, Lnym;->G:Lnzi;

    iget-object v0, v0, Lnzi;->d:Lopf;

    if-eqz v0, :cond_2

    iget-object v0, v1, Lnym;->G:Lnzi;

    iget-object v0, v0, Lnzi;->d:Lopf;

    iget-object v0, v0, Lopf;->c:Lpxr;

    if-eqz v0, :cond_2

    iget-object v0, v1, Lnym;->G:Lnzi;

    iget-object v0, v0, Lnzi;->d:Lopf;

    iget-object v0, v0, Lopf;->c:Lpxr;

    iget-object v0, v0, Lpxr;->a:Lood;

    if-eqz v0, :cond_2

    iget-object v0, v1, Lnym;->G:Lnzi;

    iget-object v0, v0, Lnzi;->a:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 195
    new-instance v0, Ljfd;

    sget-object v2, Ljff;->a:Ljff;

    const/16 v3, 0xc8

    iget-object v4, v1, Lnym;->G:Lnzi;

    iget-object v4, v4, Lnzi;->d:Lopf;

    iget-object v4, v4, Lopf;->c:Lpxr;

    iget-object v4, v4, Lpxr;->a:Lood;

    iget-object v1, v1, Lnym;->G:Lnzi;

    iget-object v1, v1, Lnzi;->a:Ljava/lang/String;

    invoke-direct {v0, v2, v3, v4, v1}, Ljfd;-><init>(Ljff;ILood;Ljava/lang/String;)V

    goto :goto_0

    .line 201
    :cond_2
    sget-object v0, Ldqw;->c:Ljfd;

    goto :goto_0
.end method

.method public a(Ljdz;Lllx;)Lllt;
    .locals 7

    .prologue
    .line 421
    const-string v0, "cloudPhotoId cannot be null"

    invoke-static {p1, v0}, Llsk;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 422
    iget-object v0, p0, Ldqw;->d:Landroid/content/Context;

    iget-object v1, p1, Ljdz;->b:Ljava/lang/String;

    iget-object v2, p1, Ljdz;->a:Ljava/lang/Long;

    .line 423
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 422
    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->a(Landroid/content/Context;Ljava/lang/String;J)Lnzc;

    move-result-object v0

    .line 424
    iget-object v1, p1, Ljdz;->b:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, Ljdz;->a:Ljava/lang/Long;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, Lnzc;->b:Ljava/lang/Integer;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "-"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 426
    iget-object v0, v0, Lnzc;->d:Ljava/lang/String;

    iget-object v2, p0, Ldqw;->d:Landroid/content/Context;

    .line 428
    invoke-static {v2}, Llly;->a(Landroid/content/Context;)Llmc;

    move-result-object v2

    .line 426
    new-instance v3, Llly;

    invoke-direct {v3, v2, v0}, Llly;-><init>(Llmc;Ljava/lang/String;)V

    new-instance v0, Llls;

    invoke-interface {p2}, Lllx;->a()I

    move-result v2

    invoke-direct {v0, v3, v2}, Llls;-><init>(Lllt;I)V

    new-instance v2, Lllu;

    invoke-direct {v2, v1, v0, p2}, Lllu;-><init>(Ljava/lang/String;Lllw;Lllx;)V

    new-instance v0, Lllv;

    invoke-direct {v0, v2}, Lllv;-><init>(Lllw;)V

    return-object v0
.end method

.method public a(ILjeo;)V
    .locals 1

    .prologue
    .line 242
    iget-object v0, p0, Ldqw;->d:Landroid/content/Context;

    invoke-static {v0, p1, p2}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILjeo;)I

    .line 243
    return-void
.end method

.method public a(Ljdz;)V
    .locals 6

    .prologue
    .line 435
    iget-object v0, p0, Ldqw;->d:Landroid/content/Context;

    const-class v1, Lhei;

    .line 436
    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iget-object v1, p1, Ljdz;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Lhei;->b(Ljava/lang/String;)I

    move-result v2

    .line 437
    new-instance v0, Ldkg;

    iget-object v1, p0, Ldqw;->d:Landroid/content/Context;

    iget-object v3, p1, Ljdz;->b:Ljava/lang/String;

    iget-object v4, p1, Ljdz;->a:Ljava/lang/Long;

    .line 438
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, Ldkg;-><init>(Landroid/content/Context;ILjava/lang/String;J)V

    .line 439
    invoke-virtual {v0}, Ldkg;->l()V

    .line 440
    return-void
.end method

.method public a(Ljed;Z)V
    .locals 3

    .prologue
    .line 226
    iget-object v0, p1, Ljed;->a:Ljava/lang/String;

    invoke-direct {p0, v0}, Ldqw;->c(Ljava/lang/String;)I

    move-result v0

    .line 227
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 238
    :goto_0
    return-void

    .line 233
    :cond_0
    if-eqz p2, :cond_1

    .line 234
    iget-object v1, p0, Ldqw;->d:Landroid/content/Context;

    iget-object v2, p1, Ljed;->b:Ljava/lang/String;

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->a(Landroid/content/Context;ILjava/lang/String;)V

    goto :goto_0

    .line 236
    :cond_1
    iget-object v1, p0, Ldqw;->d:Landroid/content/Context;

    iget-object v2, p1, Ljed;->b:Ljava/lang/String;

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->b(Landroid/content/Context;ILjava/lang/String;)V

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x0

    return v0
.end method

.method public a(Landroid/net/Uri;)Z
    .locals 2

    .prologue
    .line 372
    iget-object v0, p0, Ldqw;->d:Landroid/content/Context;

    invoke-static {}, Lcom/google/android/apps/photos/content/GooglePhotosImageProvider;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 89
    iget-object v0, p0, Ldqw;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->a(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljed;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljed;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljej;",
            ">;"
        }
    .end annotation

    .prologue
    .line 117
    iget-object v0, p1, Ljed;->a:Ljava/lang/String;

    invoke-direct {p0, v0}, Ldqw;->c(Ljava/lang/String;)I

    move-result v0

    .line 118
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 121
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 123
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Ldqw;->d:Landroid/content/Context;

    iget-object v2, p1, Ljed;->b:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v1, v0, v2, v3}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->a(Landroid/content/Context;ILjava/lang/String;Z)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 215
    invoke-direct {p0, p1}, Ldqw;->c(Ljava/lang/String;)I

    move-result v0

    .line 216
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 222
    :goto_0
    return-void

    .line 221
    :cond_0
    iget-object v1, p0, Ldqw;->d:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->e(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method public b(I)Z
    .locals 1

    .prologue
    .line 353
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 354
    invoke-virtual {p0}, Ldqw;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldqw;->d:Landroid/content/Context;

    .line 355
    invoke-static {v0, p1}, Ldhv;->w(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Landroid/net/Uri;)Z
    .locals 1

    .prologue
    .line 377
    invoke-virtual {p0, p1}, Ldqw;->a(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 378
    const/4 v0, 0x1

    .line 380
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Ldqw;->d:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/apps/photos/content/GooglePhotosImageProvider;->a(Landroid/content/Context;Landroid/net/Uri;)Z

    move-result v0

    goto :goto_0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 360
    invoke-virtual {p0}, Ldqw;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldqw;->d:Landroid/content/Context;

    .line 361
    invoke-static {v0}, Ldhv;->h(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(Landroid/net/Uri;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 385
    :try_start_0
    iget-object v0, p0, Ldqw;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "r"

    .line 388
    invoke-virtual {v0, p1, v1}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 389
    invoke-static {v0}, Lifu;->a(Landroid/os/ParcelFileDescriptor;)V

    const/4 v0, 0x1

    .line 391
    :goto_0
    return v0

    :catch_0
    move-exception v0

    invoke-static {v2}, Lifu;->a(Landroid/os/ParcelFileDescriptor;)V

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v2}, Lifu;->a(Landroid/os/ParcelFileDescriptor;)V

    throw v0
.end method

.method public d(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 399
    iget-object v0, p0, Ldqw;->d:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->a(Landroid/content/Context;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 366
    sget-object v0, Lfvc;->m:Lfvc;

    invoke-virtual {v0}, Lfvc;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ldqw;->d:Landroid/content/Context;

    .line 367
    invoke-static {v0}, Ljem;->a(Landroid/content/Context;)Ljem;

    move-result-object v0

    invoke-virtual {v0}, Ljem;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 404
    iget-object v0, p0, Ldqw;->d:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->b(Landroid/content/Context;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 2

    .prologue
    .line 414
    iget-object v0, p0, Ldqw;->d:Landroid/content/Context;

    const v1, 0x7f0a024d

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public f(Landroid/net/Uri;)J
    .locals 2

    .prologue
    .line 409
    iget-object v0, p0, Ldqw;->d:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->c(Landroid/content/Context;Landroid/net/Uri;)J

    move-result-wide v0

    return-wide v0
.end method
