.class public final Lhre;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhrd;


# static fields
.field private static final a:[Ljava/lang/String;

.field private static b:Lhre;


# instance fields
.field private final c:Landroid/content/Context;

.field private d:Z

.field private e:I

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Ljava/lang/String;

.field private k:J

.field private l:J

.field private m:Z

.field private n:J

.field private o:I

.field private p:Ljdn;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 26
    const/16 v0, 0x10

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "auto_upload_enabled"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "auto_upload_account_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "sync_on_wifi_only"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "video_upload_wifi_only"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "sync_on_roaming"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "sync_on_battery"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "instant_share_eventid"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "instant_share_account_id"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "instant_share_starttime"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "instant_share_endtime"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "upload_full_resolution"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "max_mobile_upload_size"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "quota_limit"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "quota_used"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "full_size_disabled"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "quota_unlimited"

    aput-object v2, v0, v1

    sput-object v0, Lhre;->a:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    iput-object p1, p0, Lhre;->c:Landroid/content/Context;

    .line 66
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lhre;
    .locals 3

    .prologue
    .line 69
    const-class v1, Lhre;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lhre;->b:Lhre;

    if-nez v0, :cond_0

    .line 70
    new-instance v0, Lhre;

    invoke-direct {v0, p0}, Lhre;-><init>(Landroid/content/Context;)V

    .line 71
    sput-object v0, Lhre;->b:Lhre;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lhre;->a(Landroid/database/Cursor;)V

    .line 73
    :cond_0
    sget-object v0, Lhre;->b:Lhre;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 69
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lhre;->a(Landroid/database/Cursor;)V

    .line 81
    return-void
.end method

.method a(Landroid/database/Cursor;)V
    .locals 30

    .prologue
    .line 172
    if-nez p1, :cond_0

    .line 173
    invoke-virtual/range {p0 .. p0}, Lhre;->m()Landroid/database/Cursor;

    move-result-object p1

    .line 177
    :cond_0
    if-eqz p1, :cond_1

    :try_start_0
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    if-nez v4, :cond_2

    .line 178
    :cond_1
    invoke-static/range {p1 .. p1}, Lifu;->a(Landroid/database/Cursor;)V

    .line 294
    :goto_0
    return-void

    .line 183
    :cond_2
    :try_start_1
    const-string v4, "auto_upload_enabled"

    .line 184
    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    if-eqz v4, :cond_f

    const/4 v5, 0x1

    .line 185
    :goto_1
    const-string v4, "auto_upload_account_id"

    .line 186
    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v20

    .line 187
    const-string v4, "sync_on_wifi_only"

    .line 188
    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    if-eqz v4, :cond_10

    const/4 v4, 0x1

    move/from16 v19, v4

    .line 189
    :goto_2
    const-string v4, "video_upload_wifi_only"

    .line 190
    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    if-eqz v4, :cond_11

    const/4 v4, 0x1

    move/from16 v18, v4

    .line 191
    :goto_3
    const-string v4, "sync_on_roaming"

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    if-eqz v4, :cond_12

    const/4 v4, 0x1

    move/from16 v17, v4

    .line 192
    :goto_4
    const-string v4, "sync_on_battery"

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    if-eqz v4, :cond_13

    const/4 v4, 0x1

    move/from16 v16, v4

    .line 193
    :goto_5
    const-string v4, "instant_share_eventid"

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 194
    const-string v6, "instant_share_account_id"

    .line 195
    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    .line 196
    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->isNull(I)Z

    move-result v7

    if-eqz v7, :cond_14

    const/4 v6, -0x1

    move v15, v6

    .line 198
    :goto_6
    const-string v6, "instant_share_starttime"

    .line 199
    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v22

    .line 200
    const-string v6, "instant_share_endtime"

    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v24

    .line 201
    const-string v6, "upload_full_resolution"

    .line 202
    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    if-eqz v6, :cond_15

    const/4 v6, 0x1

    move v14, v6

    .line 203
    :goto_7
    const-string v6, "max_mobile_upload_size"

    .line 204
    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v26

    .line 205
    const-string v6, "quota_limit"

    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 206
    const-string v8, "quota_used"

    move-object/from16 v0, p1

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v8

    move-object/from16 v0, p1

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 207
    const-string v10, "quota_unlimited"

    .line 208
    move-object/from16 v0, p1

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v10

    move-object/from16 v0, p1

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    if-eqz v10, :cond_16

    const/4 v10, 0x1

    .line 209
    :goto_8
    const-string v11, "full_size_disabled"

    .line 210
    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v11

    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v11

    if-eqz v11, :cond_17

    const/4 v11, 0x1

    .line 212
    :goto_9
    invoke-static/range {p1 .. p1}, Lifu;->a(Landroid/database/Cursor;)V

    .line 215
    const/4 v12, -0x1

    move/from16 v0, v20

    if-ne v0, v12, :cond_3

    .line 217
    const/4 v5, 0x0

    .line 218
    const/4 v4, 0x0

    .line 221
    :cond_3
    const-string v12, "iu.UploadsManager"

    const/4 v13, 0x4

    invoke-static {v12, v13}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 222
    new-instance v13, Ljava/lang/StringBuffer;

    invoke-direct {v13}, Ljava/lang/StringBuffer;-><init>()V

    .line 223
    const-string v12, "#reloadSettings()"

    invoke-virtual {v13, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    new-instance v21, Ljava/lang/StringBuilder;

    const/16 v28, 0x16

    move-object/from16 v0, v21

    move/from16 v1, v28

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v28, "; account: "

    move-object/from16 v0, v21

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    .line 224
    move-object/from16 v0, v21

    invoke-virtual {v12, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    const-string v21, "; IU: "

    .line 225
    move-object/from16 v0, v21

    invoke-virtual {v12, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v21

    if-eqz v5, :cond_18

    const-string v12, "enabled"

    :goto_a
    move-object/from16 v0, v21

    invoke-virtual {v0, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    const-string v21, "; IS: "

    .line 226
    move-object/from16 v0, v21

    invoke-virtual {v12, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v21

    if-eqz v4, :cond_19

    move-object v12, v4

    :goto_b
    move-object/from16 v0, v21

    invoke-virtual {v0, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    new-instance v21, Ljava/lang/StringBuilder;

    const/16 v28, 0x19

    move-object/from16 v0, v21

    move/from16 v1, v28

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v28, "; IS account: "

    move-object/from16 v0, v21

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    .line 227
    move-object/from16 v0, v21

    invoke-virtual {v12, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 228
    if-eqz v4, :cond_4

    .line 229
    const-string v12, "MMM dd, yyyy h:mmaa"

    .line 230
    new-instance v21, Ljava/util/Date;

    invoke-direct/range {v21 .. v23}, Ljava/util/Date;-><init>(J)V

    .line 231
    move-object/from16 v0, v21

    invoke-static {v12, v0}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v21

    .line 232
    new-instance v28, Ljava/util/Date;

    move-object/from16 v0, v28

    move-wide/from16 v1, v24

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    .line 233
    move-object/from16 v0, v28

    invoke-static {v12, v0}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v12

    .line 234
    const-string v28, " (start: "

    move-object/from16 v0, v28

    invoke-virtual {v13, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuffer;

    move-result-object v21

    const-string v28, ", end: "

    .line 235
    move-object/from16 v0, v21

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuffer;

    move-result-object v12

    const-string v21, ")"

    .line 236
    move-object/from16 v0, v21

    invoke-virtual {v12, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 238
    :cond_4
    const-string v12, "; photoWiFi: "

    invoke-virtual {v13, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    move/from16 v0, v19

    invoke-virtual {v12, v0}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v12

    const-string v13, "; videoWiFi: "

    .line 239
    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    move/from16 v0, v18

    invoke-virtual {v12, v0}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v12

    const-string v13, "; roam: "

    .line 240
    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v12

    const-string v13, "; battery: "

    .line 241
    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    move/from16 v0, v16

    invoke-virtual {v12, v0}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v12

    const-string v13, "; size: "

    .line 242
    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v13

    if-eqz v14, :cond_1a

    const-string v12, "FULL"

    :goto_c
    invoke-virtual {v13, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    const-string v13, "; maxMobile: "

    .line 243
    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    move-wide/from16 v0, v26

    invoke-virtual {v12, v0, v1}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    .line 244
    :cond_5
    const-string v12, "iu.UploadsManager"

    const/4 v13, 0x3

    invoke-static {v12, v13}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v12

    if-eqz v12, :cond_e

    .line 248
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lhre;->d:Z

    if-eq v5, v12, :cond_6

    .line 249
    new-instance v12, Ljava/lang/StringBuilder;

    const/16 v13, 0x1f

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v13, "   auto upload changed to "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 251
    :cond_6
    move-object/from16 v0, p0

    iget v12, v0, Lhre;->e:I

    move/from16 v0, v20

    if-eq v0, v12, :cond_7

    .line 252
    move-object/from16 v0, p0

    iget v12, v0, Lhre;->e:I

    new-instance v13, Ljava/lang/StringBuilder;

    const/16 v21, 0x34

    move/from16 v0, v21

    invoke-direct {v13, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v21, "   account changed from: "

    move-object/from16 v0, v21

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " --> "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move/from16 v0, v20

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 255
    :cond_7
    move-object/from16 v0, p0

    iget-object v12, v0, Lhre;->j:Ljava/lang/String;

    invoke-static {v4, v12}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_8

    .line 256
    move-object/from16 v0, p0

    iget-object v12, v0, Lhre;->j:Ljava/lang/String;

    if-nez v12, :cond_1b

    const-string v12, "<< NULL >>"

    move-object v13, v12

    :goto_d
    if-nez v4, :cond_1c

    const-string v12, "<< NULL >>"

    :goto_e
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-static {v13}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/String;->length()I

    move-result v28

    add-int/lit8 v28, v28, 0x1f

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/String;->length()I

    move-result v29

    add-int v28, v28, v29

    move-object/from16 v0, v21

    move/from16 v1, v28

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v28, "   event ID changed from: "

    move-object/from16 v0, v21

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v21, " --> "

    move-object/from16 v0, v21

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 260
    :cond_8
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lhre;->f:Z

    move/from16 v0, v19

    if-eq v0, v12, :cond_9

    .line 261
    new-instance v12, Ljava/lang/StringBuilder;

    const/16 v13, 0x21

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v13, "   wifiOnlyPhoto changed to "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move/from16 v0, v19

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 263
    :cond_9
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lhre;->g:Z

    move/from16 v0, v18

    if-eq v0, v12, :cond_a

    .line 264
    new-instance v12, Ljava/lang/StringBuilder;

    const/16 v13, 0x21

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v13, "   wifiOnlyVideo changed to "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move/from16 v0, v18

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 266
    :cond_a
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lhre;->h:Z

    move/from16 v0, v17

    if-eq v0, v12, :cond_b

    .line 267
    new-instance v12, Ljava/lang/StringBuilder;

    const/16 v13, 0x21

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v13, "   syncOnRoaming changed to "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 269
    :cond_b
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lhre;->i:Z

    move/from16 v0, v16

    if-eq v0, v12, :cond_c

    .line 270
    new-instance v12, Ljava/lang/StringBuilder;

    const/16 v13, 0x21

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v13, "   syncOnBattery changed to "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move/from16 v0, v16

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 272
    :cond_c
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lhre;->m:Z

    if-eq v14, v12, :cond_d

    .line 273
    new-instance v12, Ljava/lang/StringBuilder;

    const/16 v13, 0x21

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v13, "   uploadFullRes changed to "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 275
    :cond_d
    move-object/from16 v0, p0

    iget-wide v12, v0, Lhre;->n:J

    cmp-long v12, v26, v12

    if-eqz v12, :cond_e

    .line 276
    new-instance v12, Ljava/lang/StringBuilder;

    const/16 v13, 0x36

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v13, "   maxMobileUploadSize changed to "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-wide/from16 v0, v26

    invoke-virtual {v12, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 280
    :cond_e
    move-object/from16 v0, p0

    iput-boolean v5, v0, Lhre;->d:Z

    .line 281
    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lhre;->e:I

    .line 282
    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lhre;->f:Z

    .line 283
    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lhre;->g:Z

    .line 284
    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lhre;->h:Z

    .line 285
    move/from16 v0, v16

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lhre;->i:Z

    .line 286
    move-object/from16 v0, p0

    iput-object v4, v0, Lhre;->j:Ljava/lang/String;

    .line 287
    move-object/from16 v0, p0

    iput v15, v0, Lhre;->o:I

    .line 288
    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Lhre;->k:J

    .line 289
    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Lhre;->l:J

    .line 290
    move-object/from16 v0, p0

    iput-boolean v14, v0, Lhre;->m:Z

    .line 291
    move-wide/from16 v0, v26

    move-object/from16 v2, p0

    iput-wide v0, v2, Lhre;->n:J

    .line 292
    new-instance v5, Ljdn;

    invoke-direct/range {v5 .. v11}, Ljdn;-><init>(JJZZ)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lhre;->p:Ljdn;

    goto/16 :goto_0

    .line 184
    :cond_f
    const/4 v5, 0x0

    goto/16 :goto_1

    .line 188
    :cond_10
    const/4 v4, 0x0

    move/from16 v19, v4

    goto/16 :goto_2

    .line 190
    :cond_11
    const/4 v4, 0x0

    move/from16 v18, v4

    goto/16 :goto_3

    .line 191
    :cond_12
    const/4 v4, 0x0

    move/from16 v17, v4

    goto/16 :goto_4

    .line 192
    :cond_13
    const/4 v4, 0x0

    move/from16 v16, v4

    goto/16 :goto_5

    .line 197
    :cond_14
    :try_start_2
    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v6

    move v15, v6

    goto/16 :goto_6

    .line 202
    :cond_15
    const/4 v6, 0x0

    move v14, v6

    goto/16 :goto_7

    .line 208
    :cond_16
    const/4 v10, 0x0

    goto/16 :goto_8

    .line 210
    :cond_17
    const/4 v11, 0x0

    goto/16 :goto_9

    .line 212
    :catchall_0
    move-exception v4

    invoke-static/range {p1 .. p1}, Lifu;->a(Landroid/database/Cursor;)V

    throw v4

    .line 225
    :cond_18
    const-string v12, "disabled"

    goto/16 :goto_a

    .line 226
    :cond_19
    const-string v12, "disabled"

    goto/16 :goto_b

    .line 242
    :cond_1a
    const-string v12, "STANDARD"

    goto/16 :goto_c

    .line 256
    :cond_1b
    move-object/from16 v0, p0

    iget-object v12, v0, Lhre;->j:Ljava/lang/String;

    move-object v13, v12

    goto/16 :goto_d

    :cond_1c
    move-object v12, v4

    goto/16 :goto_e
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 95
    iget-boolean v0, p0, Lhre;->f:Z

    return v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 100
    iget-boolean v0, p0, Lhre;->g:Z

    return v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 105
    iget-boolean v0, p0, Lhre;->h:Z

    return v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 110
    iget-boolean v0, p0, Lhre;->i:Z

    return v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lhre;->j:Ljava/lang/String;

    return-object v0
.end method

.method public g()I
    .locals 1

    .prologue
    .line 125
    iget v0, p0, Lhre;->o:I

    return v0
.end method

.method public h()J
    .locals 2

    .prologue
    .line 130
    iget-wide v0, p0, Lhre;->k:J

    return-wide v0
.end method

.method public i()J
    .locals 2

    .prologue
    .line 135
    iget-wide v0, p0, Lhre;->l:J

    return-wide v0
.end method

.method public j()Z
    .locals 1

    .prologue
    .line 140
    iget-boolean v0, p0, Lhre;->m:Z

    return v0
.end method

.method public k()J
    .locals 2

    .prologue
    .line 145
    iget-wide v0, p0, Lhre;->n:J

    return-wide v0
.end method

.method public l()Ljdn;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lhre;->p:Ljdn;

    return-object v0
.end method

.method m()Landroid/database/Cursor;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 298
    iget-object v0, p0, Lhre;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lhre;->c:Landroid/content/Context;

    invoke-static {v1}, Lhqv;->b(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lhre;->a:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method
