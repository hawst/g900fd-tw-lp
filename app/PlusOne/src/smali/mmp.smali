.class public final Lmmp;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmmp;


# instance fields
.field public b:[Lmml;

.field public c:[Lmmc;

.field private d:Lmon;

.field private e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1782
    const/4 v0, 0x0

    new-array v0, v0, [Lmmp;

    sput-object v0, Lmmp;->a:[Lmmp;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1783
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1786
    const/4 v0, 0x0

    iput-object v0, p0, Lmmp;->d:Lmon;

    .line 1791
    sget-object v0, Lmml;->a:[Lmml;

    iput-object v0, p0, Lmmp;->b:[Lmml;

    .line 1794
    sget-object v0, Lmmc;->a:[Lmmc;

    iput-object v0, p0, Lmmp;->c:[Lmmc;

    .line 1783
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1825
    .line 1826
    iget-object v0, p0, Lmmp;->d:Lmon;

    if-eqz v0, :cond_5

    .line 1827
    const/4 v0, 0x1

    iget-object v2, p0, Lmmp;->d:Lmon;

    .line 1828
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1830
    :goto_0
    iget-object v2, p0, Lmmp;->e:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 1831
    const/4 v2, 0x2

    iget-object v3, p0, Lmmp;->e:Ljava/lang/String;

    .line 1832
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1834
    :cond_0
    iget-object v2, p0, Lmmp;->b:[Lmml;

    if-eqz v2, :cond_2

    .line 1835
    iget-object v3, p0, Lmmp;->b:[Lmml;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 1836
    if-eqz v5, :cond_1

    .line 1837
    const/4 v6, 0x3

    .line 1838
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 1835
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1842
    :cond_2
    iget-object v2, p0, Lmmp;->c:[Lmmc;

    if-eqz v2, :cond_4

    .line 1843
    iget-object v2, p0, Lmmp;->c:[Lmmc;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 1844
    if-eqz v4, :cond_3

    .line 1845
    const/4 v5, 0x4

    .line 1846
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 1843
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1850
    :cond_4
    iget-object v1, p0, Lmmp;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1851
    iput v0, p0, Lmmp;->ai:I

    .line 1852
    return v0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lmmp;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1860
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1861
    sparse-switch v0, :sswitch_data_0

    .line 1865
    iget-object v2, p0, Lmmp;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 1866
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmmp;->ah:Ljava/util/List;

    .line 1869
    :cond_1
    iget-object v2, p0, Lmmp;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1871
    :sswitch_0
    return-object p0

    .line 1876
    :sswitch_1
    iget-object v0, p0, Lmmp;->d:Lmon;

    if-nez v0, :cond_2

    .line 1877
    new-instance v0, Lmon;

    invoke-direct {v0}, Lmon;-><init>()V

    iput-object v0, p0, Lmmp;->d:Lmon;

    .line 1879
    :cond_2
    iget-object v0, p0, Lmmp;->d:Lmon;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1883
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmmp;->e:Ljava/lang/String;

    goto :goto_0

    .line 1887
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1888
    iget-object v0, p0, Lmmp;->b:[Lmml;

    if-nez v0, :cond_4

    move v0, v1

    .line 1889
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lmml;

    .line 1890
    iget-object v3, p0, Lmmp;->b:[Lmml;

    if-eqz v3, :cond_3

    .line 1891
    iget-object v3, p0, Lmmp;->b:[Lmml;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1893
    :cond_3
    iput-object v2, p0, Lmmp;->b:[Lmml;

    .line 1894
    :goto_2
    iget-object v2, p0, Lmmp;->b:[Lmml;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 1895
    iget-object v2, p0, Lmmp;->b:[Lmml;

    new-instance v3, Lmml;

    invoke-direct {v3}, Lmml;-><init>()V

    aput-object v3, v2, v0

    .line 1896
    iget-object v2, p0, Lmmp;->b:[Lmml;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1897
    invoke-virtual {p1}, Loxn;->a()I

    .line 1894
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1888
    :cond_4
    iget-object v0, p0, Lmmp;->b:[Lmml;

    array-length v0, v0

    goto :goto_1

    .line 1900
    :cond_5
    iget-object v2, p0, Lmmp;->b:[Lmml;

    new-instance v3, Lmml;

    invoke-direct {v3}, Lmml;-><init>()V

    aput-object v3, v2, v0

    .line 1901
    iget-object v2, p0, Lmmp;->b:[Lmml;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1905
    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1906
    iget-object v0, p0, Lmmp;->c:[Lmmc;

    if-nez v0, :cond_7

    move v0, v1

    .line 1907
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lmmc;

    .line 1908
    iget-object v3, p0, Lmmp;->c:[Lmmc;

    if-eqz v3, :cond_6

    .line 1909
    iget-object v3, p0, Lmmp;->c:[Lmmc;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1911
    :cond_6
    iput-object v2, p0, Lmmp;->c:[Lmmc;

    .line 1912
    :goto_4
    iget-object v2, p0, Lmmp;->c:[Lmmc;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_8

    .line 1913
    iget-object v2, p0, Lmmp;->c:[Lmmc;

    new-instance v3, Lmmc;

    invoke-direct {v3}, Lmmc;-><init>()V

    aput-object v3, v2, v0

    .line 1914
    iget-object v2, p0, Lmmp;->c:[Lmmc;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1915
    invoke-virtual {p1}, Loxn;->a()I

    .line 1912
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 1906
    :cond_7
    iget-object v0, p0, Lmmp;->c:[Lmmc;

    array-length v0, v0

    goto :goto_3

    .line 1918
    :cond_8
    iget-object v2, p0, Lmmp;->c:[Lmmc;

    new-instance v3, Lmmc;

    invoke-direct {v3}, Lmmc;-><init>()V

    aput-object v3, v2, v0

    .line 1919
    iget-object v2, p0, Lmmp;->c:[Lmmc;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1861
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1799
    iget-object v1, p0, Lmmp;->d:Lmon;

    if-eqz v1, :cond_0

    .line 1800
    const/4 v1, 0x1

    iget-object v2, p0, Lmmp;->d:Lmon;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 1802
    :cond_0
    iget-object v1, p0, Lmmp;->e:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1803
    const/4 v1, 0x2

    iget-object v2, p0, Lmmp;->e:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 1805
    :cond_1
    iget-object v1, p0, Lmmp;->b:[Lmml;

    if-eqz v1, :cond_3

    .line 1806
    iget-object v2, p0, Lmmp;->b:[Lmml;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 1807
    if-eqz v4, :cond_2

    .line 1808
    const/4 v5, 0x3

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 1806
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1812
    :cond_3
    iget-object v1, p0, Lmmp;->c:[Lmmc;

    if-eqz v1, :cond_5

    .line 1813
    iget-object v1, p0, Lmmp;->c:[Lmmc;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    .line 1814
    if-eqz v3, :cond_4

    .line 1815
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 1813
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1819
    :cond_5
    iget-object v0, p0, Lmmp;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1821
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1779
    invoke-virtual {p0, p1}, Lmmp;->a(Loxn;)Lmmp;

    move-result-object v0

    return-object v0
.end method
