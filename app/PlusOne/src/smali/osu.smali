.class public final Losu;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/Float;

.field private e:Ljava/lang/Float;

.field private f:Ljava/lang/Integer;

.field private g:Ljava/lang/Float;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1964
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 2010
    const/4 v0, 0x0

    .line 2011
    iget-object v1, p0, Losu;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2012
    const/4 v0, 0x1

    iget-object v1, p0, Losu;->a:Ljava/lang/String;

    .line 2013
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2015
    :cond_0
    iget-object v1, p0, Losu;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 2016
    const/4 v1, 0x2

    iget-object v2, p0, Losu;->b:Ljava/lang/String;

    .line 2017
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2019
    :cond_1
    iget-object v1, p0, Losu;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 2020
    const/4 v1, 0x3

    iget-object v2, p0, Losu;->c:Ljava/lang/String;

    .line 2021
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2023
    :cond_2
    iget-object v1, p0, Losu;->d:Ljava/lang/Float;

    if-eqz v1, :cond_3

    .line 2024
    const/4 v1, 0x4

    iget-object v2, p0, Losu;->d:Ljava/lang/Float;

    .line 2025
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 2027
    :cond_3
    iget-object v1, p0, Losu;->e:Ljava/lang/Float;

    if-eqz v1, :cond_4

    .line 2028
    const/4 v1, 0x5

    iget-object v2, p0, Losu;->e:Ljava/lang/Float;

    .line 2029
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 2031
    :cond_4
    iget-object v1, p0, Losu;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 2032
    const/4 v1, 0x6

    iget-object v2, p0, Losu;->f:Ljava/lang/Integer;

    .line 2033
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2035
    :cond_5
    iget-object v1, p0, Losu;->g:Ljava/lang/Float;

    if-eqz v1, :cond_6

    .line 2036
    const/4 v1, 0x7

    iget-object v2, p0, Losu;->g:Ljava/lang/Float;

    .line 2037
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 2039
    :cond_6
    iget-object v1, p0, Losu;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2040
    iput v0, p0, Losu;->ai:I

    .line 2041
    return v0
.end method

.method public a(Loxn;)Losu;
    .locals 2

    .prologue
    .line 2049
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2050
    sparse-switch v0, :sswitch_data_0

    .line 2054
    iget-object v1, p0, Losu;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 2055
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Losu;->ah:Ljava/util/List;

    .line 2058
    :cond_1
    iget-object v1, p0, Losu;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2060
    :sswitch_0
    return-object p0

    .line 2065
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Losu;->a:Ljava/lang/String;

    goto :goto_0

    .line 2069
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Losu;->b:Ljava/lang/String;

    goto :goto_0

    .line 2073
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Losu;->c:Ljava/lang/String;

    goto :goto_0

    .line 2077
    :sswitch_4
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Losu;->d:Ljava/lang/Float;

    goto :goto_0

    .line 2081
    :sswitch_5
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Losu;->e:Ljava/lang/Float;

    goto :goto_0

    .line 2085
    :sswitch_6
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Losu;->f:Ljava/lang/Integer;

    goto :goto_0

    .line 2089
    :sswitch_7
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Losu;->g:Ljava/lang/Float;

    goto :goto_0

    .line 2050
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x25 -> :sswitch_4
        0x2d -> :sswitch_5
        0x30 -> :sswitch_6
        0x3d -> :sswitch_7
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1983
    iget-object v0, p0, Losu;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1984
    const/4 v0, 0x1

    iget-object v1, p0, Losu;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1986
    :cond_0
    iget-object v0, p0, Losu;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1987
    const/4 v0, 0x2

    iget-object v1, p0, Losu;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1989
    :cond_1
    iget-object v0, p0, Losu;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1990
    const/4 v0, 0x3

    iget-object v1, p0, Losu;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1992
    :cond_2
    iget-object v0, p0, Losu;->d:Ljava/lang/Float;

    if-eqz v0, :cond_3

    .line 1993
    const/4 v0, 0x4

    iget-object v1, p0, Losu;->d:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 1995
    :cond_3
    iget-object v0, p0, Losu;->e:Ljava/lang/Float;

    if-eqz v0, :cond_4

    .line 1996
    const/4 v0, 0x5

    iget-object v1, p0, Losu;->e:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 1998
    :cond_4
    iget-object v0, p0, Losu;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 1999
    const/4 v0, 0x6

    iget-object v1, p0, Losu;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2001
    :cond_5
    iget-object v0, p0, Losu;->g:Ljava/lang/Float;

    if-eqz v0, :cond_6

    .line 2002
    const/4 v0, 0x7

    iget-object v1, p0, Losu;->g:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 2004
    :cond_6
    iget-object v0, p0, Losu;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2006
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1960
    invoke-virtual {p0, p1}, Losu;->a(Loxn;)Losu;

    move-result-object v0

    return-object v0
.end method
