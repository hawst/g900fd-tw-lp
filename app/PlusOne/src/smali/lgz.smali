.class public final Llgz;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:Landroid/os/Bundle;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Llgz;->a:Landroid/os/Bundle;

    return-void
.end method


# virtual methods
.method public a()Llgz;
    .locals 3

    .prologue
    .line 106
    iget-object v0, p0, Llgz;->a:Landroid/os/Bundle;

    const-string v1, "cursor_at_end"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 107
    return-object p0
.end method

.method public a(I)Llgz;
    .locals 2

    .prologue
    .line 91
    iget-object v0, p0, Llgz;->a:Landroid/os/Bundle;

    const-string v1, "cancel_resource_id"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 92
    return-object p0
.end method

.method public a(Ljava/lang/String;)Llgz;
    .locals 2

    .prologue
    .line 71
    iget-object v0, p0, Llgz;->a:Landroid/os/Bundle;

    const-string v1, "text_value"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    return-object p0
.end method

.method public b()Llgw;
    .locals 2

    .prologue
    .line 111
    new-instance v0, Llgw;

    invoke-direct {v0}, Llgw;-><init>()V

    .line 112
    iget-object v1, p0, Llgz;->a:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Llgw;->f(Landroid/os/Bundle;)V

    .line 113
    return-object v0
.end method

.method public b(Ljava/lang/String;)Llgz;
    .locals 2

    .prologue
    .line 76
    iget-object v0, p0, Llgz;->a:Landroid/os/Bundle;

    const-string v1, "dialog_title"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    return-object p0
.end method

.method public c(Ljava/lang/String;)Llgz;
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, Llgz;->a:Landroid/os/Bundle;

    const-string v1, "hint_text"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    return-object p0
.end method

.method public d(Ljava/lang/String;)Llgz;
    .locals 2

    .prologue
    .line 86
    iget-object v0, p0, Llgz;->a:Landroid/os/Bundle;

    const-string v1, "notice_text"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    return-object p0
.end method

.method public e(Ljava/lang/String;)Llgz;
    .locals 2

    .prologue
    .line 96
    iget-object v0, p0, Llgz;->a:Landroid/os/Bundle;

    const-string v1, "error_msg"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    return-object p0
.end method
