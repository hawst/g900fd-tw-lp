.class final Lhro;
.super Landroid/os/Handler;
.source "PG"


# direct methods
.method constructor <init>(Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 476
    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 477
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    .line 481
    invoke-static {}, Lhrm;->c()Lhrm;

    move-result-object v0

    if-nez v0, :cond_0

    .line 516
    :goto_0
    return-void

    .line 485
    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 519
    new-instance v0, Ljava/lang/AssertionError;

    iget v1, p1, Landroid/os/Message;->what:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x1c

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "unknown message: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 487
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v0, v0, Landroid/database/Cursor;

    if-eqz v0, :cond_1

    .line 488
    invoke-static {}, Lhrm;->c()Lhrm;

    move-result-object v0

    invoke-static {v0}, Lhrm;->b(Lhrm;)Lhre;

    move-result-object v1

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/database/Cursor;

    invoke-virtual {v1, v0}, Lhre;->a(Landroid/database/Cursor;)V

    goto :goto_0

    .line 490
    :cond_1
    invoke-static {}, Lhrm;->c()Lhrm;

    move-result-object v0

    invoke-static {v0}, Lhrm;->b(Lhrm;)Lhre;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lhre;->a(Landroid/database/Cursor;)V

    goto :goto_0

    .line 496
    :pswitch_1
    invoke-static {}, Lhrm;->c()Lhrm;

    move-result-object v1

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v1, v0}, Lhrm;->a(Lhrm;I)V

    goto :goto_0

    .line 500
    :pswitch_2
    invoke-static {}, Lhrm;->c()Lhrm;

    move-result-object v0

    invoke-static {v0}, Lhrm;->c(Lhrm;)V

    goto :goto_0

    .line 504
    :pswitch_3
    invoke-static {}, Lhrm;->c()Lhrm;

    move-result-object v1

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v1, v0}, Lhrm;->b(Lhrm;I)V

    goto :goto_0

    .line 508
    :pswitch_4
    invoke-static {}, Lhrm;->c()Lhrm;

    move-result-object v0

    invoke-static {v0}, Lhrm;->a(Lhrm;)V

    goto :goto_0

    .line 512
    :pswitch_5
    invoke-static {}, Lhrm;->c()Lhrm;

    move-result-object v0

    invoke-static {v0}, Lhrm;->d(Lhrm;)V

    goto :goto_0

    .line 485
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
