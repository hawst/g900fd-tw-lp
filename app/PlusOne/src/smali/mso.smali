.class public final Lmso;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmso;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/Boolean;

.field private f:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 825
    const/4 v0, 0x0

    new-array v0, v0, [Lmso;

    sput-object v0, Lmso;->a:[Lmso;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 826
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 862
    const/4 v0, 0x0

    .line 863
    iget-object v1, p0, Lmso;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 864
    const/4 v0, 0x1

    iget-object v1, p0, Lmso;->b:Ljava/lang/String;

    .line 865
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 867
    :cond_0
    iget-object v1, p0, Lmso;->d:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 868
    const/4 v1, 0x2

    iget-object v2, p0, Lmso;->d:Ljava/lang/String;

    .line 869
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 871
    :cond_1
    iget-object v1, p0, Lmso;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 872
    const/4 v1, 0x3

    iget-object v2, p0, Lmso;->e:Ljava/lang/Boolean;

    .line 873
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 875
    :cond_2
    iget-object v1, p0, Lmso;->c:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 876
    const/4 v1, 0x4

    iget-object v2, p0, Lmso;->c:Ljava/lang/String;

    .line 877
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 879
    :cond_3
    iget-object v1, p0, Lmso;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 880
    const/4 v1, 0x5

    iget-object v2, p0, Lmso;->f:Ljava/lang/Integer;

    .line 881
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 883
    :cond_4
    iget-object v1, p0, Lmso;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 884
    iput v0, p0, Lmso;->ai:I

    .line 885
    return v0
.end method

.method public a(Loxn;)Lmso;
    .locals 2

    .prologue
    .line 893
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 894
    sparse-switch v0, :sswitch_data_0

    .line 898
    iget-object v1, p0, Lmso;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 899
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmso;->ah:Ljava/util/List;

    .line 902
    :cond_1
    iget-object v1, p0, Lmso;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 904
    :sswitch_0
    return-object p0

    .line 909
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmso;->b:Ljava/lang/String;

    goto :goto_0

    .line 913
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmso;->d:Ljava/lang/String;

    goto :goto_0

    .line 917
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmso;->e:Ljava/lang/Boolean;

    goto :goto_0

    .line 921
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmso;->c:Ljava/lang/String;

    goto :goto_0

    .line 925
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmso;->f:Ljava/lang/Integer;

    goto :goto_0

    .line 894
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 841
    iget-object v0, p0, Lmso;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 842
    const/4 v0, 0x1

    iget-object v1, p0, Lmso;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 844
    :cond_0
    iget-object v0, p0, Lmso;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 845
    const/4 v0, 0x2

    iget-object v1, p0, Lmso;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 847
    :cond_1
    iget-object v0, p0, Lmso;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 848
    const/4 v0, 0x3

    iget-object v1, p0, Lmso;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 850
    :cond_2
    iget-object v0, p0, Lmso;->c:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 851
    const/4 v0, 0x4

    iget-object v1, p0, Lmso;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 853
    :cond_3
    iget-object v0, p0, Lmso;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 854
    const/4 v0, 0x5

    iget-object v1, p0, Lmso;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 856
    :cond_4
    iget-object v0, p0, Lmso;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 858
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 822
    invoke-virtual {p0, p1}, Lmso;->a(Loxn;)Lmso;

    move-result-object v0

    return-object v0
.end method
