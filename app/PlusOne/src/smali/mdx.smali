.class public final Lmdx;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lpfb;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3724
    invoke-direct {p0}, Loxq;-><init>()V

    .line 3727
    iput-object v0, p0, Lmdx;->apiHeader:Llyr;

    .line 3730
    iput-object v0, p0, Lmdx;->a:Lpfb;

    .line 3724
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 3747
    const/4 v0, 0x0

    .line 3748
    iget-object v1, p0, Lmdx;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 3749
    const/4 v0, 0x1

    iget-object v1, p0, Lmdx;->apiHeader:Llyr;

    .line 3750
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3752
    :cond_0
    iget-object v1, p0, Lmdx;->a:Lpfb;

    if-eqz v1, :cond_1

    .line 3753
    const/4 v1, 0x2

    iget-object v2, p0, Lmdx;->a:Lpfb;

    .line 3754
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3756
    :cond_1
    iget-object v1, p0, Lmdx;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3757
    iput v0, p0, Lmdx;->ai:I

    .line 3758
    return v0
.end method

.method public a(Loxn;)Lmdx;
    .locals 2

    .prologue
    .line 3766
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3767
    sparse-switch v0, :sswitch_data_0

    .line 3771
    iget-object v1, p0, Lmdx;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 3772
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmdx;->ah:Ljava/util/List;

    .line 3775
    :cond_1
    iget-object v1, p0, Lmdx;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3777
    :sswitch_0
    return-object p0

    .line 3782
    :sswitch_1
    iget-object v0, p0, Lmdx;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 3783
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmdx;->apiHeader:Llyr;

    .line 3785
    :cond_2
    iget-object v0, p0, Lmdx;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3789
    :sswitch_2
    iget-object v0, p0, Lmdx;->a:Lpfb;

    if-nez v0, :cond_3

    .line 3790
    new-instance v0, Lpfb;

    invoke-direct {v0}, Lpfb;-><init>()V

    iput-object v0, p0, Lmdx;->a:Lpfb;

    .line 3792
    :cond_3
    iget-object v0, p0, Lmdx;->a:Lpfb;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3767
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 3735
    iget-object v0, p0, Lmdx;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 3736
    const/4 v0, 0x1

    iget-object v1, p0, Lmdx;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 3738
    :cond_0
    iget-object v0, p0, Lmdx;->a:Lpfb;

    if-eqz v0, :cond_1

    .line 3739
    const/4 v0, 0x2

    iget-object v1, p0, Lmdx;->a:Lpfb;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 3741
    :cond_1
    iget-object v0, p0, Lmdx;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3743
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3720
    invoke-virtual {p0, p1}, Lmdx;->a(Loxn;)Lmdx;

    move-result-object v0

    return-object v0
.end method
