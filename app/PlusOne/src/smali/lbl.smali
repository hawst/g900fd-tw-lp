.class public final Llbl;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lkzk;


# instance fields
.field private final a:Llbm;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const-class v0, Llbm;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llbm;

    iput-object v0, p0, Llbl;->a:Llbm;

    .line 36
    return-void
.end method

.method private static a(Lpyk;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 98
    if-eqz p0, :cond_0

    iget-object v0, p0, Lpyk;->c:Lpym;

    if-nez v0, :cond_1

    .line 99
    :cond_0
    const-string v0, "BundleItemStoreExt"

    const-string v1, "Attempting to get ID of invalid StreamItem."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    const/4 v0, 0x0

    .line 102
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lpyk;->c:Lpym;

    iget-object v0, v0, Lpym;->b:Ljava/lang/String;

    goto :goto_0
.end method

.method private a(I)Lkzg;
    .locals 4

    .prologue
    .line 131
    iget-object v0, p0, Llbl;->a:Llbm;

    .line 132
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Llbm;->a(Ljava/lang/Object;)Llno;

    move-result-object v0

    check-cast v0, Lkzg;

    .line 133
    if-nez v0, :cond_0

    .line 134
    const-string v1, "BundleItemStoreExt"

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x3d

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Cannot find BundleStoreExtension for bundle type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    :cond_0
    return-object v0
.end method

.method private static a(Lnwp;)Z
    .locals 2

    .prologue
    .line 119
    if-eqz p0, :cond_0

    iget v0, p0, Lnwp;->b:I

    if-nez v0, :cond_1

    .line 120
    :cond_0
    const-string v0, "BundleItemStoreExt"

    const-string v1, "BundleItem is invalid."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    const/4 v0, 0x0

    .line 123
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static b(Lpyk;)Lnwp;
    .locals 2

    .prologue
    .line 110
    if-eqz p0, :cond_0

    iget v0, p0, Lpyk;->b:I

    const/4 v1, 0x6

    if-eq v0, v1, :cond_1

    .line 111
    :cond_0
    const-string v0, "BundleItemStoreExt"

    const-string v1, "Attempting to extract BundleItem extension from non-BundleItem StreamItem."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    const/4 v0, 0x0

    .line 114
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lnwp;->a:Loxr;

    invoke-virtual {p0, v0}, Lpyk;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnwp;

    goto :goto_0
.end method

.method private d()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkzg;",
            ">;"
        }
    .end annotation

    .prologue
    .line 171
    iget-object v0, p0, Llbl;->a:Llbm;

    invoke-virtual {v0}, Llbm;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 172
    new-instance v3, Ljava/util/ArrayList;

    .line 173
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 174
    const/4 v0, 0x0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    .line 175
    iget-object v0, p0, Llbl;->a:Llbm;

    .line 176
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v0, v5}, Llbm;->a(Ljava/lang/Object;)Llno;

    move-result-object v0

    check-cast v0, Lkzg;

    .line 177
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 174
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 179
    :cond_0
    return-object v3
.end method


# virtual methods
.method public a(ILpyk;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 45
    invoke-static {p2}, Llbl;->a(Lpyk;)Ljava/lang/String;

    move-result-object v1

    .line 46
    invoke-static {p2}, Llbl;->b(Lpyk;)Lnwp;

    move-result-object v2

    .line 47
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v2}, Llbl;->a(Lnwp;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 56
    :cond_0
    :goto_0
    return-object v0

    .line 51
    :cond_1
    iget v1, v2, Lnwp;->b:I

    invoke-direct {p0, v1}, Llbl;->a(I)Lkzg;

    move-result-object v1

    .line 52
    if-eqz v1, :cond_0

    .line 56
    invoke-interface {v1}, Lkzg;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 141
    invoke-direct {p0}, Llbl;->d()Ljava/util/List;

    move-result-object v2

    .line 142
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 143
    const/4 v0, 0x0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    .line 144
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkzg;

    .line 145
    invoke-interface {v0}, Lkzg;->d()Ljava/util/ArrayList;

    move-result-object v0

    .line 147
    if-eqz v0, :cond_0

    .line 148
    invoke-interface {v3, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 143
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 151
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public a(Landroid/content/Context;I)Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 156
    invoke-direct {p0}, Llbl;->d()Ljava/util/List;

    move-result-object v2

    .line 157
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 158
    const/4 v0, 0x0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    .line 159
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkzg;

    .line 160
    invoke-interface {v0}, Lkzg;->e()Ljava/util/ArrayList;

    move-result-object v0

    .line 162
    if-eqz v0, :cond_0

    .line 163
    invoke-interface {v3, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 158
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 166
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public a(Landroid/content/Context;ILandroid/database/sqlite/SQLiteDatabase;Lpyk;IZ)V
    .locals 2

    .prologue
    .line 81
    invoke-static {p4}, Llbl;->a(Lpyk;)Ljava/lang/String;

    move-result-object v0

    .line 82
    invoke-static {p4}, Llbl;->b(Lpyk;)Lnwp;

    move-result-object v1

    .line 83
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {v1}, Llbl;->a(Lnwp;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 92
    :cond_0
    :goto_0
    return-void

    .line 87
    :cond_1
    iget v0, v1, Lnwp;->b:I

    invoke-direct {p0, v0}, Llbl;->a(I)Lkzg;

    move-result-object v0

    .line 88
    if-nez v0, :cond_0

    goto :goto_0
.end method

.method public a(Landroid/content/Context;ILandroid/database/sqlite/SQLiteDatabase;Lpyk;Ljava/lang/String;JIZZLjava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 63
    invoke-static {p4}, Llbl;->a(Lpyk;)Ljava/lang/String;

    move-result-object v1

    .line 64
    invoke-static {p4}, Llbl;->b(Lpyk;)Lnwp;

    move-result-object v2

    .line 65
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v2}, Llbl;->a(Lnwp;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 74
    :cond_0
    :goto_0
    return v0

    .line 69
    :cond_1
    iget v1, v2, Lnwp;->b:I

    invoke-direct {p0, v1}, Llbl;->a(I)Lkzg;

    move-result-object v1

    .line 70
    if-eqz v1, :cond_0

    .line 74
    invoke-interface {v1}, Lkzg;->c()Z

    move-result v0

    goto :goto_0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0}, Llbl;->c()Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x6

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method
