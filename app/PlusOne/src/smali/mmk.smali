.class public final Lmmk;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmmk;


# instance fields
.field public b:Lmmf;

.field public c:Lmon;

.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 317
    const/4 v0, 0x0

    new-array v0, v0, [Lmmk;

    sput-object v0, Lmmk;->a:[Lmmk;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 318
    invoke-direct {p0}, Loxq;-><init>()V

    .line 327
    iput-object v0, p0, Lmmk;->b:Lmmf;

    .line 330
    iput-object v0, p0, Lmmk;->c:Lmon;

    .line 333
    const/high16 v0, -0x80000000

    iput v0, p0, Lmmk;->d:I

    .line 318
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 353
    const/4 v0, 0x0

    .line 354
    iget-object v1, p0, Lmmk;->b:Lmmf;

    if-eqz v1, :cond_0

    .line 355
    const/4 v0, 0x1

    iget-object v1, p0, Lmmk;->b:Lmmf;

    .line 356
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 358
    :cond_0
    iget-object v1, p0, Lmmk;->c:Lmon;

    if-eqz v1, :cond_1

    .line 359
    const/4 v1, 0x2

    iget-object v2, p0, Lmmk;->c:Lmon;

    .line 360
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 362
    :cond_1
    iget v1, p0, Lmmk;->d:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_2

    .line 363
    const/4 v1, 0x3

    iget v2, p0, Lmmk;->d:I

    .line 364
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 366
    :cond_2
    iget-object v1, p0, Lmmk;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 367
    iput v0, p0, Lmmk;->ai:I

    .line 368
    return v0
.end method

.method public a(Loxn;)Lmmk;
    .locals 2

    .prologue
    .line 376
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 377
    sparse-switch v0, :sswitch_data_0

    .line 381
    iget-object v1, p0, Lmmk;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 382
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmmk;->ah:Ljava/util/List;

    .line 385
    :cond_1
    iget-object v1, p0, Lmmk;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 387
    :sswitch_0
    return-object p0

    .line 392
    :sswitch_1
    iget-object v0, p0, Lmmk;->b:Lmmf;

    if-nez v0, :cond_2

    .line 393
    new-instance v0, Lmmf;

    invoke-direct {v0}, Lmmf;-><init>()V

    iput-object v0, p0, Lmmk;->b:Lmmf;

    .line 395
    :cond_2
    iget-object v0, p0, Lmmk;->b:Lmmf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 399
    :sswitch_2
    iget-object v0, p0, Lmmk;->c:Lmon;

    if-nez v0, :cond_3

    .line 400
    new-instance v0, Lmon;

    invoke-direct {v0}, Lmon;-><init>()V

    iput-object v0, p0, Lmmk;->c:Lmon;

    .line 402
    :cond_3
    iget-object v0, p0, Lmmk;->c:Lmon;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 406
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 407
    if-eqz v0, :cond_4

    const/4 v1, 0x1

    if-eq v0, v1, :cond_4

    const/4 v1, 0x2

    if-ne v0, v1, :cond_5

    .line 410
    :cond_4
    iput v0, p0, Lmmk;->d:I

    goto :goto_0

    .line 412
    :cond_5
    const/4 v0, 0x0

    iput v0, p0, Lmmk;->d:I

    goto :goto_0

    .line 377
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 338
    iget-object v0, p0, Lmmk;->b:Lmmf;

    if-eqz v0, :cond_0

    .line 339
    const/4 v0, 0x1

    iget-object v1, p0, Lmmk;->b:Lmmf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 341
    :cond_0
    iget-object v0, p0, Lmmk;->c:Lmon;

    if-eqz v0, :cond_1

    .line 342
    const/4 v0, 0x2

    iget-object v1, p0, Lmmk;->c:Lmon;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 344
    :cond_1
    iget v0, p0, Lmmk;->d:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_2

    .line 345
    const/4 v0, 0x3

    iget v1, p0, Lmmk;->d:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 347
    :cond_2
    iget-object v0, p0, Lmmk;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 349
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 314
    invoke-virtual {p0, p1}, Lmmk;->a(Loxn;)Lmmk;

    move-result-object v0

    return-object v0
.end method
