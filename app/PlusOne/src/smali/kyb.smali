.class public final Lkyb;
.super Lhny;
.source "PG"


# instance fields
.field private final a:I

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:I

.field private final e:Lhms;

.field private final f:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 47
    const-string v0, "EditModerationStateTask"

    invoke-direct {p0, p1, v0}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 49
    iput-object p1, p0, Lkyb;->f:Landroid/content/Context;

    .line 50
    iput p2, p0, Lkyb;->a:I

    .line 51
    iput-object p3, p0, Lkyb;->b:Ljava/lang/String;

    .line 52
    iput-object p4, p0, Lkyb;->c:Ljava/lang/String;

    .line 53
    iput p5, p0, Lkyb;->d:I

    .line 54
    const-class v0, Lhms;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    iput-object v0, p0, Lkyb;->e:Lhms;

    .line 55
    return-void
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 6

    .prologue
    .line 60
    new-instance v0, Lkya;

    invoke-virtual {p0}, Lkyb;->f()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lkfo;

    .line 61
    invoke-virtual {p0}, Lkyb;->f()Landroid/content/Context;

    move-result-object v3

    iget v4, p0, Lkyb;->a:I

    invoke-direct {v2, v3, v4}, Lkfo;-><init>(Landroid/content/Context;I)V

    iget-object v3, p0, Lkyb;->b:Ljava/lang/String;

    iget-object v4, p0, Lkyb;->c:Ljava/lang/String;

    iget v5, p0, Lkyb;->d:I

    invoke-direct/range {v0 .. v5}, Lkya;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Ljava/lang/String;I)V

    .line 63
    invoke-virtual {v0}, Lkya;->l()V

    .line 65
    invoke-virtual {v0}, Lkya;->t()Z

    move-result v1

    if-nez v1, :cond_0

    .line 66
    iget v1, p0, Lkyb;->d:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 67
    invoke-virtual {p0}, Lkyb;->f()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lkyb;->a:I

    iget-object v3, p0, Lkyb;->c:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Llap;->a(Landroid/content/Context;ILjava/lang/String;)V

    .line 74
    :cond_0
    :goto_0
    new-instance v1, Lhoz;

    iget v2, v0, Lkff;->i:I

    iget-object v3, v0, Lkff;->k:Ljava/lang/Exception;

    .line 75
    invoke-virtual {v0}, Lkya;->t()Z

    move-result v0

    invoke-virtual {p0, v0}, Lkyb;->a(Z)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v3, v0}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    .line 76
    invoke-virtual {v1}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "activity_id"

    iget-object v3, p0, Lkyb;->c:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    invoke-virtual {v1}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "moderation_state"

    iget v3, p0, Lkyb;->d:I

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 78
    return-object v1

    .line 68
    :cond_1
    iget v1, p0, Lkyb;->d:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 69
    invoke-virtual {p0}, Lkyb;->f()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lkyb;->a:I

    iget-object v3, p0, Lkyb;->c:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4}, Llap;->c(Landroid/content/Context;ILjava/lang/String;Z)V

    goto :goto_0
.end method

.method public a(Z)Ljava/lang/String;
    .locals 2

    .prologue
    .line 124
    if-eqz p1, :cond_0

    iget v0, p0, Lkyb;->d:I

    packed-switch v0, :pswitch_data_0

    const v0, 0x7f0a058e

    :goto_0
    invoke-virtual {p0}, Lkyb;->f()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0

    :pswitch_0
    const v0, 0x7f0a022f

    goto :goto_0

    :pswitch_1
    const v0, 0x7f0a04b3

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected a_(Lhoz;)V
    .locals 6

    .prologue
    .line 83
    iget v0, p0, Lkyb;->d:I

    packed-switch v0, :pswitch_data_0

    .line 84
    :goto_0
    return-void

    .line 83
    :pswitch_0
    sget-object v0, Lhmv;->ae:Lhmv;

    :goto_1
    const-string v1, "extra_activity_id"

    iget-object v2, p0, Lkyb;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lhmt;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    iget-object v2, p0, Lkyb;->b:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    if-nez v1, :cond_0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    :cond_0
    const-string v2, "extra_square_id"

    iget-object v3, p0, Lkyb;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-object v2, p0, Lkyb;->e:Lhms;

    new-instance v3, Lhmr;

    iget-object v4, p0, Lkyb;->f:Landroid/content/Context;

    iget v5, p0, Lkyb;->a:I

    invoke-direct {v3, v4, v5}, Lhmr;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v3, v0}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v0

    invoke-virtual {v0, v1}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v0

    invoke-interface {v2, v0}, Lhms;->a(Lhmr;)V

    goto :goto_0

    :pswitch_1
    sget-object v0, Lhmv;->ae:Lhmv;

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 119
    invoke-virtual {p0}, Lkyb;->f()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a058d

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
