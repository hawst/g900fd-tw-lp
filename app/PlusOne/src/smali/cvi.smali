.class public Lcvi;
.super Llol;
.source "PG"


# static fields
.field private static final N:Ldgu;


# instance fields
.field private O:Ldgr;

.field private P:Ldeo;

.field private Q:Lhee;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 28
    new-instance v0, Ldgq;

    const v1, 0x7f10069c

    invoke-direct {v0, v1}, Ldgq;-><init>(I)V

    sput-object v0, Lcvi;->N:Ldgu;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    .line 27
    invoke-direct {p0}, Llol;-><init>()V

    .line 36
    new-instance v0, Ldgn;

    iget-object v1, p0, Lcvi;->av:Llqm;

    sget-object v2, Lcvi;->N:Ldgu;

    new-instance v3, Lcvj;

    invoke-direct {v3, p0}, Lcvj;-><init>(Lcvi;)V

    invoke-direct {v0, v1, v2, v3}, Ldgn;-><init>(Llqr;Ldgu;Ldgv;)V

    .line 42
    new-instance v0, Ldep;

    iget-object v1, p0, Lcvi;->av:Llqm;

    new-instance v2, Lcvk;

    invoke-direct {v2, p0}, Lcvk;-><init>(Lcvi;)V

    invoke-direct {v0, v1, v2}, Ldep;-><init>(Llqr;Ldeq;)V

    .line 48
    return-void
.end method

.method static synthetic a(Lcvi;)V
    .locals 5

    .prologue
    .line 27
    iget-object v0, p0, Lcvi;->P:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcvi;->Q:Lhee;

    invoke-interface {v1}, Lhee;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljcn;

    invoke-direct {v1}, Ljcn;-><init>()V

    invoke-interface {v0}, Lddl;->d()Ljuf;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljcn;->a(Ljcl;)V

    new-instance v0, Ldup;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ldup;-><init>(Ljcn;Z)V

    iget-object v1, p0, Lcvi;->at:Llnl;

    iget-object v2, p0, Lcvi;->Q:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    iget-object v3, p0, Lcvi;->Q:Lhee;

    invoke-interface {v3}, Lhee;->g()Lhej;

    move-result-object v3

    const-string v4, "gaia_id"

    invoke-interface {v3, v4}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v0, v3}, Leyq;->a(Landroid/content/Context;ILduo;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcvi;->a(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method static synthetic b(Lcvi;)V
    .locals 4

    .prologue
    .line 27
    iget-object v0, p0, Lcvi;->P:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    iget-object v1, p0, Lcvi;->O:Ldgr;

    sget-object v2, Lcvi;->N:Ldgu;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lddl;->C()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Lddl;->N()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v2, v0}, Ldgr;->a(Ldgu;Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 70
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 71
    iget-object v0, p0, Lcvi;->au:Llnh;

    const-class v1, Ldgr;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldgr;

    iput-object v0, p0, Lcvi;->O:Ldgr;

    .line 72
    iget-object v0, p0, Lcvi;->au:Llnh;

    const-class v1, Ldeo;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldeo;

    iput-object v0, p0, Lcvi;->P:Ldeo;

    .line 73
    iget-object v0, p0, Lcvi;->au:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Lcvi;->Q:Lhee;

    .line 74
    return-void
.end method
