.class public final Lofx;
.super Loxq;
.source "PG"


# instance fields
.field private a:[Logr;

.field private b:Ljava/lang/Integer;

.field private c:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 7510
    invoke-direct {p0}, Loxq;-><init>()V

    .line 7513
    sget-object v0, Logr;->a:[Logr;

    iput-object v0, p0, Lofx;->a:[Logr;

    .line 7510
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 7539
    .line 7540
    iget-object v1, p0, Lofx;->a:[Logr;

    if-eqz v1, :cond_1

    .line 7541
    iget-object v2, p0, Lofx;->a:[Logr;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 7542
    if-eqz v4, :cond_0

    .line 7543
    const/4 v5, 0x1

    .line 7544
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 7541
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 7548
    :cond_1
    const/4 v1, 0x2

    iget-object v2, p0, Lofx;->b:Ljava/lang/Integer;

    .line 7549
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7550
    iget-object v1, p0, Lofx;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 7551
    const/4 v1, 0x3

    iget-object v2, p0, Lofx;->c:Ljava/lang/Boolean;

    .line 7552
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 7554
    :cond_2
    iget-object v1, p0, Lofx;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7555
    iput v0, p0, Lofx;->ai:I

    .line 7556
    return v0
.end method

.method public a(Loxn;)Lofx;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 7564
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 7565
    sparse-switch v0, :sswitch_data_0

    .line 7569
    iget-object v2, p0, Lofx;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 7570
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lofx;->ah:Ljava/util/List;

    .line 7573
    :cond_1
    iget-object v2, p0, Lofx;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 7575
    :sswitch_0
    return-object p0

    .line 7580
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 7581
    iget-object v0, p0, Lofx;->a:[Logr;

    if-nez v0, :cond_3

    move v0, v1

    .line 7582
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Logr;

    .line 7583
    iget-object v3, p0, Lofx;->a:[Logr;

    if-eqz v3, :cond_2

    .line 7584
    iget-object v3, p0, Lofx;->a:[Logr;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 7586
    :cond_2
    iput-object v2, p0, Lofx;->a:[Logr;

    .line 7587
    :goto_2
    iget-object v2, p0, Lofx;->a:[Logr;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 7588
    iget-object v2, p0, Lofx;->a:[Logr;

    new-instance v3, Logr;

    invoke-direct {v3}, Logr;-><init>()V

    aput-object v3, v2, v0

    .line 7589
    iget-object v2, p0, Lofx;->a:[Logr;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 7590
    invoke-virtual {p1}, Loxn;->a()I

    .line 7587
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 7581
    :cond_3
    iget-object v0, p0, Lofx;->a:[Logr;

    array-length v0, v0

    goto :goto_1

    .line 7593
    :cond_4
    iget-object v2, p0, Lofx;->a:[Logr;

    new-instance v3, Logr;

    invoke-direct {v3}, Logr;-><init>()V

    aput-object v3, v2, v0

    .line 7594
    iget-object v2, p0, Lofx;->a:[Logr;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 7598
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lofx;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 7602
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lofx;->c:Ljava/lang/Boolean;

    goto :goto_0

    .line 7565
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 7522
    iget-object v0, p0, Lofx;->a:[Logr;

    if-eqz v0, :cond_1

    .line 7523
    iget-object v1, p0, Lofx;->a:[Logr;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 7524
    if-eqz v3, :cond_0

    .line 7525
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 7523
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 7529
    :cond_1
    const/4 v0, 0x2

    iget-object v1, p0, Lofx;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 7530
    iget-object v0, p0, Lofx;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 7531
    const/4 v0, 0x3

    iget-object v1, p0, Lofx;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 7533
    :cond_2
    iget-object v0, p0, Lofx;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 7535
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 7506
    invoke-virtual {p0, p1}, Lofx;->a(Loxn;)Lofx;

    move-result-object v0

    return-object v0
.end method
