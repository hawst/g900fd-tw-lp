.class public final Lpns;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lpul;

.field private b:[Lpnt;

.field private c:Lppf;

.field private d:[Ljava/lang/Integer;

.field private e:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 14
    invoke-direct {p0}, Loxq;-><init>()V

    .line 116
    iput-object v1, p0, Lpns;->a:Lpul;

    .line 119
    sget-object v0, Lpnt;->a:[Lpnt;

    iput-object v0, p0, Lpns;->b:[Lpnt;

    .line 122
    iput-object v1, p0, Lpns;->c:Lppf;

    .line 125
    sget-object v0, Loxx;->g:[Ljava/lang/Integer;

    iput-object v0, p0, Lpns;->d:[Ljava/lang/Integer;

    .line 14
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 159
    .line 160
    iget-object v0, p0, Lpns;->a:Lpul;

    if-eqz v0, :cond_6

    .line 161
    const/4 v0, 0x1

    iget-object v2, p0, Lpns;->a:Lpul;

    .line 162
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 164
    :goto_0
    iget-object v2, p0, Lpns;->b:[Lpnt;

    if-eqz v2, :cond_1

    .line 165
    iget-object v3, p0, Lpns;->b:[Lpnt;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 166
    if-eqz v5, :cond_0

    .line 167
    const/4 v6, 0x2

    .line 168
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 165
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 172
    :cond_1
    iget-object v2, p0, Lpns;->c:Lppf;

    if-eqz v2, :cond_2

    .line 173
    const/4 v2, 0x3

    iget-object v3, p0, Lpns;->c:Lppf;

    .line 174
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 176
    :cond_2
    iget-object v2, p0, Lpns;->d:[Ljava/lang/Integer;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lpns;->d:[Ljava/lang/Integer;

    array-length v2, v2

    if-lez v2, :cond_4

    .line 178
    iget-object v3, p0, Lpns;->d:[Ljava/lang/Integer;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v1, v4, :cond_3

    aget-object v5, v3, v1

    .line 180
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-static {v5}, Loxo;->i(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 178
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 182
    :cond_3
    add-int/2addr v0, v2

    .line 183
    iget-object v1, p0, Lpns;->d:[Ljava/lang/Integer;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 185
    :cond_4
    iget-object v1, p0, Lpns;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 186
    const/4 v1, 0x5

    iget-object v2, p0, Lpns;->e:Ljava/lang/Integer;

    .line 187
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 189
    :cond_5
    iget-object v1, p0, Lpns;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 190
    iput v0, p0, Lpns;->ai:I

    .line 191
    return v0

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lpns;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 199
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 200
    sparse-switch v0, :sswitch_data_0

    .line 204
    iget-object v2, p0, Lpns;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 205
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lpns;->ah:Ljava/util/List;

    .line 208
    :cond_1
    iget-object v2, p0, Lpns;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 210
    :sswitch_0
    return-object p0

    .line 215
    :sswitch_1
    iget-object v0, p0, Lpns;->a:Lpul;

    if-nez v0, :cond_2

    .line 216
    new-instance v0, Lpul;

    invoke-direct {v0}, Lpul;-><init>()V

    iput-object v0, p0, Lpns;->a:Lpul;

    .line 218
    :cond_2
    iget-object v0, p0, Lpns;->a:Lpul;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 222
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 223
    iget-object v0, p0, Lpns;->b:[Lpnt;

    if-nez v0, :cond_4

    move v0, v1

    .line 224
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lpnt;

    .line 225
    iget-object v3, p0, Lpns;->b:[Lpnt;

    if-eqz v3, :cond_3

    .line 226
    iget-object v3, p0, Lpns;->b:[Lpnt;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 228
    :cond_3
    iput-object v2, p0, Lpns;->b:[Lpnt;

    .line 229
    :goto_2
    iget-object v2, p0, Lpns;->b:[Lpnt;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 230
    iget-object v2, p0, Lpns;->b:[Lpnt;

    new-instance v3, Lpnt;

    invoke-direct {v3}, Lpnt;-><init>()V

    aput-object v3, v2, v0

    .line 231
    iget-object v2, p0, Lpns;->b:[Lpnt;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 232
    invoke-virtual {p1}, Loxn;->a()I

    .line 229
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 223
    :cond_4
    iget-object v0, p0, Lpns;->b:[Lpnt;

    array-length v0, v0

    goto :goto_1

    .line 235
    :cond_5
    iget-object v2, p0, Lpns;->b:[Lpnt;

    new-instance v3, Lpnt;

    invoke-direct {v3}, Lpnt;-><init>()V

    aput-object v3, v2, v0

    .line 236
    iget-object v2, p0, Lpns;->b:[Lpnt;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 240
    :sswitch_3
    iget-object v0, p0, Lpns;->c:Lppf;

    if-nez v0, :cond_6

    .line 241
    new-instance v0, Lppf;

    invoke-direct {v0}, Lppf;-><init>()V

    iput-object v0, p0, Lpns;->c:Lppf;

    .line 243
    :cond_6
    iget-object v0, p0, Lpns;->c:Lppf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 247
    :sswitch_4
    const/16 v0, 0x20

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 248
    iget-object v0, p0, Lpns;->d:[Ljava/lang/Integer;

    array-length v0, v0

    .line 249
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/Integer;

    .line 250
    iget-object v3, p0, Lpns;->d:[Ljava/lang/Integer;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 251
    iput-object v2, p0, Lpns;->d:[Ljava/lang/Integer;

    .line 252
    :goto_3
    iget-object v2, p0, Lpns;->d:[Ljava/lang/Integer;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    .line 253
    iget-object v2, p0, Lpns;->d:[Ljava/lang/Integer;

    invoke-virtual {p1}, Loxn;->g()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    .line 254
    invoke-virtual {p1}, Loxn;->a()I

    .line 252
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 257
    :cond_7
    iget-object v2, p0, Lpns;->d:[Ljava/lang/Integer;

    invoke-virtual {p1}, Loxn;->g()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    .line 261
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lpns;->e:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 200
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 132
    iget-object v1, p0, Lpns;->a:Lpul;

    if-eqz v1, :cond_0

    .line 133
    const/4 v1, 0x1

    iget-object v2, p0, Lpns;->a:Lpul;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 135
    :cond_0
    iget-object v1, p0, Lpns;->b:[Lpnt;

    if-eqz v1, :cond_2

    .line 136
    iget-object v2, p0, Lpns;->b:[Lpnt;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 137
    if-eqz v4, :cond_1

    .line 138
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 136
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 142
    :cond_2
    iget-object v1, p0, Lpns;->c:Lppf;

    if-eqz v1, :cond_3

    .line 143
    const/4 v1, 0x3

    iget-object v2, p0, Lpns;->c:Lppf;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 145
    :cond_3
    iget-object v1, p0, Lpns;->d:[Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 146
    iget-object v1, p0, Lpns;->d:[Ljava/lang/Integer;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 147
    const/4 v4, 0x4

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p1, v4, v3}, Loxo;->a(II)V

    .line 146
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 150
    :cond_4
    iget-object v0, p0, Lpns;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 151
    const/4 v0, 0x5

    iget-object v1, p0, Lpns;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 153
    :cond_5
    iget-object v0, p0, Lpns;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 155
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 10
    invoke-virtual {p0, p1}, Lpns;->a(Loxn;)Lpns;

    move-result-object v0

    return-object v0
.end method
