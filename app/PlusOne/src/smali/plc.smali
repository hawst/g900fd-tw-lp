.class public final Lplc;
.super Loxq;
.source "PG"


# instance fields
.field private a:[Loxg;

.field private b:Ljava/lang/Integer;

.field private c:Ljava/lang/Integer;

.field private d:[Lpin;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 267
    invoke-direct {p0}, Loxq;-><init>()V

    .line 270
    sget-object v0, Loxg;->a:[Loxg;

    iput-object v0, p0, Lplc;->a:[Loxg;

    .line 277
    sget-object v0, Lpin;->a:[Lpin;

    iput-object v0, p0, Lplc;->d:[Lpin;

    .line 267
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 308
    .line 309
    iget-object v0, p0, Lplc;->a:[Loxg;

    if-eqz v0, :cond_1

    .line 310
    iget-object v3, p0, Lplc;->a:[Loxg;

    array-length v4, v3

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 311
    if-eqz v5, :cond_0

    .line 312
    const/4 v6, 0x1

    .line 313
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 310
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 317
    :cond_2
    iget-object v2, p0, Lplc;->b:Ljava/lang/Integer;

    if-eqz v2, :cond_3

    .line 318
    const/4 v2, 0x2

    iget-object v3, p0, Lplc;->b:Ljava/lang/Integer;

    .line 319
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 321
    :cond_3
    iget-object v2, p0, Lplc;->c:Ljava/lang/Integer;

    if-eqz v2, :cond_4

    .line 322
    const/4 v2, 0x3

    iget-object v3, p0, Lplc;->c:Ljava/lang/Integer;

    .line 323
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 325
    :cond_4
    iget-object v2, p0, Lplc;->d:[Lpin;

    if-eqz v2, :cond_6

    .line 326
    iget-object v2, p0, Lplc;->d:[Lpin;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    .line 327
    if-eqz v4, :cond_5

    .line 328
    const/4 v5, 0x4

    .line 329
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 326
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 333
    :cond_6
    iget-object v1, p0, Lplc;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 334
    iput v0, p0, Lplc;->ai:I

    .line 335
    return v0
.end method

.method public a(Loxn;)Lplc;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 343
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 344
    sparse-switch v0, :sswitch_data_0

    .line 348
    iget-object v2, p0, Lplc;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 349
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lplc;->ah:Ljava/util/List;

    .line 352
    :cond_1
    iget-object v2, p0, Lplc;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 354
    :sswitch_0
    return-object p0

    .line 359
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 360
    iget-object v0, p0, Lplc;->a:[Loxg;

    if-nez v0, :cond_3

    move v0, v1

    .line 361
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Loxg;

    .line 362
    iget-object v3, p0, Lplc;->a:[Loxg;

    if-eqz v3, :cond_2

    .line 363
    iget-object v3, p0, Lplc;->a:[Loxg;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 365
    :cond_2
    iput-object v2, p0, Lplc;->a:[Loxg;

    .line 366
    :goto_2
    iget-object v2, p0, Lplc;->a:[Loxg;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 367
    iget-object v2, p0, Lplc;->a:[Loxg;

    new-instance v3, Loxg;

    invoke-direct {v3}, Loxg;-><init>()V

    aput-object v3, v2, v0

    .line 368
    iget-object v2, p0, Lplc;->a:[Loxg;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 369
    invoke-virtual {p1}, Loxn;->a()I

    .line 366
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 360
    :cond_3
    iget-object v0, p0, Lplc;->a:[Loxg;

    array-length v0, v0

    goto :goto_1

    .line 372
    :cond_4
    iget-object v2, p0, Lplc;->a:[Loxg;

    new-instance v3, Loxg;

    invoke-direct {v3}, Loxg;-><init>()V

    aput-object v3, v2, v0

    .line 373
    iget-object v2, p0, Lplc;->a:[Loxg;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 377
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lplc;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 381
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lplc;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 385
    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 386
    iget-object v0, p0, Lplc;->d:[Lpin;

    if-nez v0, :cond_6

    move v0, v1

    .line 387
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lpin;

    .line 388
    iget-object v3, p0, Lplc;->d:[Lpin;

    if-eqz v3, :cond_5

    .line 389
    iget-object v3, p0, Lplc;->d:[Lpin;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 391
    :cond_5
    iput-object v2, p0, Lplc;->d:[Lpin;

    .line 392
    :goto_4
    iget-object v2, p0, Lplc;->d:[Lpin;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    .line 393
    iget-object v2, p0, Lplc;->d:[Lpin;

    new-instance v3, Lpin;

    invoke-direct {v3}, Lpin;-><init>()V

    aput-object v3, v2, v0

    .line 394
    iget-object v2, p0, Lplc;->d:[Lpin;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 395
    invoke-virtual {p1}, Loxn;->a()I

    .line 392
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 386
    :cond_6
    iget-object v0, p0, Lplc;->d:[Lpin;

    array-length v0, v0

    goto :goto_3

    .line 398
    :cond_7
    iget-object v2, p0, Lplc;->d:[Lpin;

    new-instance v3, Lpin;

    invoke-direct {v3}, Lpin;-><init>()V

    aput-object v3, v2, v0

    .line 399
    iget-object v2, p0, Lplc;->d:[Lpin;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 344
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 282
    iget-object v1, p0, Lplc;->a:[Loxg;

    if-eqz v1, :cond_1

    .line 283
    iget-object v2, p0, Lplc;->a:[Loxg;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 284
    if-eqz v4, :cond_0

    .line 285
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 283
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 289
    :cond_1
    iget-object v1, p0, Lplc;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 290
    const/4 v1, 0x2

    iget-object v2, p0, Lplc;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 292
    :cond_2
    iget-object v1, p0, Lplc;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 293
    const/4 v1, 0x3

    iget-object v2, p0, Lplc;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 295
    :cond_3
    iget-object v1, p0, Lplc;->d:[Lpin;

    if-eqz v1, :cond_5

    .line 296
    iget-object v1, p0, Lplc;->d:[Lpin;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    .line 297
    if-eqz v3, :cond_4

    .line 298
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 296
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 302
    :cond_5
    iget-object v0, p0, Lplc;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 304
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 263
    invoke-virtual {p0, p1}, Lplc;->a(Loxn;)Lplc;

    move-result-object v0

    return-object v0
.end method
