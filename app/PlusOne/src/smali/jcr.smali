.class final Ljcr;
.super Ljdi;
.source "PG"


# instance fields
.field private final h:Landroid/content/Context;

.field private final i:Ljava/lang/String;

.field private final j:Ljdh;

.field private final k:Ljava/lang/String;

.field private final l:Ljava/lang/String;

.field private final m:Z

.field private final n:I

.field private final o:Ljava/lang/String;

.field private final p:Ljava/lang/String;

.field private q:Lorg/chromium/net/HttpUrlRequest;

.field private r:Ljava/lang/String;

.field private s:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lkfj;Ljava/lang/String;Ljdh;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0, p2}, Ljdi;-><init>(Lkfj;)V

    .line 52
    iput-object p1, p0, Ljcr;->h:Landroid/content/Context;

    .line 53
    iput-object p3, p0, Ljcr;->i:Ljava/lang/String;

    .line 54
    iput-object p4, p0, Ljcr;->j:Ljdh;

    .line 55
    iput-object p5, p0, Ljcr;->k:Ljava/lang/String;

    .line 56
    iput-object p6, p0, Ljcr;->l:Ljava/lang/String;

    .line 57
    iput-boolean p7, p0, Ljcr;->m:Z

    .line 58
    iput p8, p0, Ljcr;->n:I

    .line 59
    iput-object p9, p0, Ljcr;->o:Ljava/lang/String;

    .line 60
    iput-object p10, p0, Ljcr;->p:Ljava/lang/String;

    .line 61
    iput-object p11, p0, Ljcr;->s:Ljava/lang/String;

    .line 62
    return-void
.end method


# virtual methods
.method public a()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    .line 66
    new-instance v1, Ljava/util/HashMap;

    invoke-virtual {p0}, Ljcr;->j()Lkfj;

    move-result-object v0

    iget-object v2, p0, Ljcr;->i:Ljava/lang/String;

    invoke-interface {v0, v2}, Lkfj;->a(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 68
    const-string v0, "X-Upload-Content-Type"

    iget-object v2, p0, Ljcr;->j:Ljdh;

    invoke-virtual {v2}, Ljdh;->e()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    iget-object v0, p0, Ljcr;->j:Ljdh;

    invoke-virtual {v0}, Ljdh;->k()J

    move-result-wide v2

    .line 70
    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-ltz v0, :cond_0

    .line 71
    const-string v0, "X-Upload-Content-Length"

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    :cond_0
    iget-object v0, p0, Ljcr;->s:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 74
    const-string v0, "Set-Cookie"

    iget-object v2, p0, Ljcr;->s:Ljava/lang/String;

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    :cond_1
    const-string v2, "X-Goog-Hash"

    const-string v3, "sha1="

    iget-object v0, p0, Ljcr;->j:Ljdh;

    .line 78
    invoke-virtual {v0}, Ljdh;->i()Ljbh;

    move-result-object v0

    invoke-virtual {v0}, Ljbh;->c()[B

    move-result-object v0

    invoke-static {v0, v8}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 77
    :goto_0
    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    iget-object v0, p0, Ljcr;->h:Landroid/content/Context;

    iget-object v2, p0, Ljcr;->i:Ljava/lang/String;

    const/4 v3, 0x3

    iget-object v4, p0, Ljcr;->g:Lorg/chromium/net/HttpUrlRequestListener;

    invoke-static {v0, v2, v3, v1, v4}, Ljgm;->a(Landroid/content/Context;Ljava/lang/String;ILjava/util/Map;Lorg/chromium/net/HttpUrlRequestListener;)Lorg/chromium/net/HttpUrlRequest;

    move-result-object v0

    iput-object v0, p0, Ljcr;->q:Lorg/chromium/net/HttpUrlRequest;

    .line 82
    new-instance v7, Lmky;

    invoke-direct {v7}, Lmky;-><init>()V

    .line 83
    iget-object v0, p0, Ljcr;->h:Landroid/content/Context;

    const-class v1, Ljcs;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljcs;

    iget-object v1, p0, Ljcr;->h:Landroid/content/Context;

    iget-object v2, p0, Ljcr;->p:Ljava/lang/String;

    iget-object v3, p0, Ljcr;->k:Ljava/lang/String;

    iget-object v4, p0, Ljcr;->l:Ljava/lang/String;

    iget-object v5, p0, Ljcr;->j:Ljdh;

    iget v6, p0, Ljcr;->n:I

    invoke-virtual/range {v0 .. v6}, Ljcs;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljdh;I)Lncm;

    move-result-object v0

    iput-object v0, v7, Lmky;->a:Lncm;

    .line 86
    iget-object v1, p0, Ljcr;->h:Landroid/content/Context;

    iget-object v2, p0, Ljcr;->o:Ljava/lang/String;

    const/4 v3, 0x0

    iget-boolean v0, p0, Ljcr;->m:Z

    if-eqz v0, :cond_4

    const/16 v0, 0x32

    :goto_1
    invoke-static {v1, v7, v2, v3, v0}, Lkgi;->a(Landroid/content/Context;Loxu;Ljava/lang/String;ZI)V

    .line 89
    invoke-static {v7}, Loxu;->a(Loxu;)[B

    move-result-object v0

    .line 90
    iget-object v1, p0, Ljcr;->q:Lorg/chromium/net/HttpUrlRequest;

    const-string v2, "application/x-protobuf"

    invoke-interface {v1, v2, v0}, Lorg/chromium/net/HttpUrlRequest;->a(Ljava/lang/String;[B)V

    .line 92
    const-string v0, "MediaUploader"

    invoke-static {v0, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 93
    const-string v0, "requestUrl: "

    iget-object v1, p0, Ljcr;->i:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 95
    :goto_2
    const-string v0, "MediaUploader"

    invoke-virtual {v7}, Lmky;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v8, v0, v1}, Llse;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 97
    :cond_2
    return-void

    .line 78
    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 86
    :cond_4
    const/16 v0, 0x64

    goto :goto_1

    .line 93
    :cond_5
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2
.end method

.method protected a(Lorg/chromium/net/HttpUrlRequest;)V
    .locals 1

    .prologue
    .line 106
    const-string v0, "Location"

    invoke-interface {p1, v0}, Lorg/chromium/net/HttpUrlRequest;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljcr;->r:Ljava/lang/String;

    .line 107
    return-void
.end method

.method protected b()Lorg/chromium/net/HttpUrlRequest;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Ljcr;->q:Lorg/chromium/net/HttpUrlRequest;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Ljcr;->r:Ljava/lang/String;

    return-object v0
.end method
