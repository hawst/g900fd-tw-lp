.class public final Lkyt;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhob;
.implements Lkyw;
.implements Llnx;
.implements Llrg;


# instance fields
.field private final a:Lu;

.field private b:Landroid/content/Context;

.field private c:Lhee;


# direct methods
.method public constructor <init>(Lu;Llqr;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lkyt;->a:Lu;

    .line 35
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 36
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Llnh;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 40
    iput-object p1, p0, Lkyt;->b:Landroid/content/Context;

    .line 41
    const-class v0, Lhee;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Lkyt;->c:Lhee;

    .line 42
    const-class v0, Lhoc;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    invoke-virtual {v0, p0}, Lhoc;->a(Lhob;)Lhoc;

    .line 43
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 46
    invoke-static {p1}, Lkyu;->a(Ljava/lang/String;)Lt;

    move-result-object v0

    .line 47
    iget-object v1, p0, Lkyt;->a:Lu;

    invoke-virtual {v1}, Lu;->q()Lae;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lt;->a(Lae;Ljava/lang/String;)V

    .line 48
    return-void
.end method

.method public a(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 52
    const/4 v0, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, p1, v0, v1}, Lkyt;->a(Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Integer;)V

    .line 53
    return-void
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 0

    .prologue
    .line 69
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Integer;)V
    .locals 6

    .prologue
    .line 60
    new-instance v0, Lkyr;

    iget-object v1, p0, Lkyt;->b:Landroid/content/Context;

    iget-object v2, p0, Lkyt;->c:Lhee;

    .line 61
    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lkyr;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/Boolean;Ljava/lang/Integer;)V

    .line 62
    iget-object v1, p0, Lkyt;->b:Landroid/content/Context;

    const-class v2, Lhoc;

    invoke-static {v1, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhoc;

    invoke-virtual {v1, v0}, Lhoc;->c(Lhny;)V

    .line 63
    return-void
.end method
