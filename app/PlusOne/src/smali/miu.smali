.class public final Lmiu;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmlu;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 32803
    invoke-direct {p0}, Loxq;-><init>()V

    .line 32806
    iput-object v0, p0, Lmiu;->apiHeader:Llyq;

    .line 32809
    iput-object v0, p0, Lmiu;->a:Lmlu;

    .line 32803
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 32826
    const/4 v0, 0x0

    .line 32827
    iget-object v1, p0, Lmiu;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 32828
    const/4 v0, 0x1

    iget-object v1, p0, Lmiu;->apiHeader:Llyq;

    .line 32829
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 32831
    :cond_0
    iget-object v1, p0, Lmiu;->a:Lmlu;

    if-eqz v1, :cond_1

    .line 32832
    const/4 v1, 0x2

    iget-object v2, p0, Lmiu;->a:Lmlu;

    .line 32833
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 32835
    :cond_1
    iget-object v1, p0, Lmiu;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 32836
    iput v0, p0, Lmiu;->ai:I

    .line 32837
    return v0
.end method

.method public a(Loxn;)Lmiu;
    .locals 2

    .prologue
    .line 32845
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 32846
    sparse-switch v0, :sswitch_data_0

    .line 32850
    iget-object v1, p0, Lmiu;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 32851
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmiu;->ah:Ljava/util/List;

    .line 32854
    :cond_1
    iget-object v1, p0, Lmiu;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 32856
    :sswitch_0
    return-object p0

    .line 32861
    :sswitch_1
    iget-object v0, p0, Lmiu;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 32862
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmiu;->apiHeader:Llyq;

    .line 32864
    :cond_2
    iget-object v0, p0, Lmiu;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 32868
    :sswitch_2
    iget-object v0, p0, Lmiu;->a:Lmlu;

    if-nez v0, :cond_3

    .line 32869
    new-instance v0, Lmlu;

    invoke-direct {v0}, Lmlu;-><init>()V

    iput-object v0, p0, Lmiu;->a:Lmlu;

    .line 32871
    :cond_3
    iget-object v0, p0, Lmiu;->a:Lmlu;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 32846
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 32814
    iget-object v0, p0, Lmiu;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 32815
    const/4 v0, 0x1

    iget-object v1, p0, Lmiu;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 32817
    :cond_0
    iget-object v0, p0, Lmiu;->a:Lmlu;

    if-eqz v0, :cond_1

    .line 32818
    const/4 v0, 0x2

    iget-object v1, p0, Lmiu;->a:Lmlu;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 32820
    :cond_1
    iget-object v0, p0, Lmiu;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 32822
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 32799
    invoke-virtual {p0, p1}, Lmiu;->a(Loxn;)Lmiu;

    move-result-object v0

    return-object v0
.end method
