.class public final Lely;
.super Lelj;
.source "PG"

# interfaces
.implements Ljph;
.implements Ljpj;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lae;Lbb;I)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1, p2, p3, p4}, Lelj;-><init>(Landroid/content/Context;Lae;Lbb;I)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lae;Lbb;II)V
    .locals 0

    .prologue
    .line 47
    invoke-direct/range {p0 .. p5}, Lelj;-><init>(Landroid/content/Context;Lae;Lbb;II)V

    .line 48
    return-void
.end method


# virtual methods
.method protected a(Landroid/content/Context;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 56
    packed-switch p2, :pswitch_data_0

    .line 71
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 58
    :pswitch_0
    new-instance v0, Lhxf;

    invoke-direct {v0, p1}, Lhxf;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 63
    :pswitch_1
    invoke-static {p1}, Ljpg;->a(Landroid/content/Context;)Ljpg;

    move-result-object v0

    goto :goto_0

    .line 66
    :pswitch_2
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 67
    const v1, 0x7f040169

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p5, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 56
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected a(Landroid/view/View;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)V
    .locals 10

    .prologue
    .line 92
    if-eqz p3, :cond_0

    invoke-interface {p3}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 216
    :cond_0
    :goto_0
    return-void

    .line 96
    :cond_1
    packed-switch p2, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    move-object v0, p1

    .line 98
    check-cast v0, Lhxf;

    .line 99
    const/4 v1, 0x2

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 100
    iget-object v1, p0, Lely;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lhxf;->b(Ljava/lang/String;)V

    .line 101
    const/4 v1, 0x1

    .line 102
    invoke-interface {p3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x3

    .line 104
    invoke-interface {p3, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x4

    .line 105
    invoke-interface {p3, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iget-object v5, p0, Lely;->k:Landroid/content/Context;

    iget v6, p0, Lely;->d:I

    .line 106
    const-class v7, Lhei;

    invoke-static {v5, v7}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lhei;

    invoke-interface {v5, v6}, Lhei;->a(I)Lhej;

    move-result-object v5

    const-string v6, "is_child"

    invoke-interface {v5, v6}, Lhej;->c(Ljava/lang/String;)Z

    move-result v6

    const/16 v5, 0x9

    if-eq v2, v5, :cond_2

    const/4 v5, 0x7

    if-ne v2, v5, :cond_3

    :cond_2
    const/4 v5, 0x1

    :goto_1
    if-eqz v6, :cond_4

    if-eqz v5, :cond_4

    const/4 v5, 0x1

    .line 101
    :goto_2
    invoke-virtual/range {v0 .. v5}, Lhxf;->a(Ljava/lang/String;ILjava/lang/String;IZ)V

    goto :goto_0

    .line 106
    :cond_3
    const/4 v5, 0x0

    goto :goto_1

    :cond_4
    const/4 v5, 0x0

    goto :goto_2

    :pswitch_1
    move-object v0, p1

    .line 110
    check-cast v0, Ljpg;

    .line 111
    iget-object v1, p0, Lely;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljpg;->b(Ljava/lang/String;)V

    .line 112
    iget-object v1, p0, Lely;->c:Lhxh;

    invoke-virtual {v0, v1}, Ljpg;->a(Lhxh;)V

    .line 113
    const/4 v1, 0x0

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 114
    invoke-virtual {v0, v8}, Ljpg;->a(Ljava/lang/String;)V

    .line 115
    const/4 v1, 0x2

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 116
    const/4 v2, 0x1

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 117
    const/4 v2, 0x5

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 119
    invoke-static {v2}, Lhst;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 118
    invoke-virtual {v0, v1, v9, v2}, Ljpg;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    const/16 v1, 0xc

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_9

    const/4 v1, 0x1

    .line 122
    :goto_3
    const/4 v2, 0x3

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljpg;->c(Ljava/lang/String;)V

    .line 123
    iget-object v2, p0, Lely;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljpg;->a(ZLjava/lang/String;)V

    .line 124
    const/16 v1, 0xd

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_a

    const/4 v1, 0x1

    :goto_4
    invoke-virtual {v0, v1}, Ljpg;->a(Z)V

    .line 125
    const/16 v1, 0xb

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 126
    const/4 v1, 0x6

    .line 127
    invoke-interface {p3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 128
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_b

    const/4 v2, 0x1

    move v7, v2

    .line 129
    :goto_5
    const/16 v2, 0x8

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 130
    const/4 v4, 0x0

    .line 131
    iget-boolean v3, p0, Lely;->i:Z

    if-eqz v3, :cond_5

    .line 132
    const/16 v3, 0x9

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 134
    :cond_5
    const/4 v3, 0x7

    .line 137
    invoke-interface {p3, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/16 v5, 0xa

    .line 139
    invoke-interface {p3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 134
    invoke-virtual/range {v0 .. v6}, Ljpg;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    iget-object v1, p0, Lely;->b:Lhei;

    iget v2, p0, Lely;->d:I

    invoke-interface {v1, v2}, Lhei;->a(I)Lhej;

    move-result-object v1

    const-string v2, "gaia_id"

    invoke-interface {v1, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 143
    const-string v2, "g:"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_c

    invoke-virtual {v2, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 145
    :goto_6
    iget-boolean v2, p0, Lely;->e:Z

    if-eqz v2, :cond_d

    if-nez v7, :cond_d

    .line 146
    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    const/4 v1, 0x1

    .line 145
    :goto_7
    invoke-virtual {v0, v1}, Ljpg;->c(Z)V

    .line 147
    iget-boolean v1, p0, Lely;->e:Z

    if-eqz v1, :cond_6

    iget-object v1, p0, Lely;->f:Lelt;

    if-eqz v1, :cond_6

    .line 148
    invoke-virtual {v0, p0}, Ljpg;->a(Ljph;)V

    .line 151
    :cond_6
    const/4 v1, 0x1

    .line 152
    if-eqz v9, :cond_7

    .line 155
    if-nez p4, :cond_e

    .line 156
    const/4 v1, 0x1

    .line 168
    :cond_7
    :goto_8
    invoke-virtual {v0, v1}, Ljpg;->f(Z)V

    .line 171
    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x5

    if-lt p4, v1, :cond_8

    .line 172
    invoke-virtual {p0}, Lely;->e()V

    .line 175
    :cond_8
    invoke-virtual {v0}, Ljpg;->k()V

    goto/16 :goto_0

    .line 120
    :cond_9
    const/4 v1, 0x0

    goto/16 :goto_3

    .line 124
    :cond_a
    const/4 v1, 0x0

    goto/16 :goto_4

    .line 128
    :cond_b
    const/4 v2, 0x0

    move v7, v2

    goto :goto_5

    .line 143
    :cond_c
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_6

    .line 146
    :cond_d
    const/4 v1, 0x0

    goto :goto_7

    .line 158
    :cond_e
    invoke-interface {p3}, Landroid/database/Cursor;->moveToPrevious()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 159
    const/4 v2, 0x1

    .line 160
    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 161
    invoke-static {v9, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 162
    const/4 v1, 0x0

    .line 164
    :cond_f
    invoke-interface {p3}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_8

    .line 179
    :pswitch_2
    check-cast p1, Ljpg;

    .line 180
    iget-object v0, p0, Lely;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljpg;->d(Ljava/lang/String;)V

    .line 181
    iget-boolean v0, p0, Lely;->e:Z

    invoke-virtual {p1, v0}, Ljpg;->c(Z)V

    .line 182
    iget-boolean v0, p0, Lely;->e:Z

    if-eqz v0, :cond_10

    iget-object v0, p0, Lely;->f:Lelt;

    if-eqz v0, :cond_10

    .line 183
    invoke-virtual {p1, p0}, Ljpg;->a(Ljph;)V

    .line 185
    :cond_10
    invoke-virtual {p1}, Ljpg;->k()V

    goto/16 :goto_0

    .line 189
    :pswitch_3
    check-cast p1, Ljpg;

    .line 190
    iget-object v0, p0, Lely;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljpg;->e(Ljava/lang/String;)V

    .line 191
    iget-boolean v0, p0, Lely;->e:Z

    invoke-virtual {p1, v0}, Ljpg;->c(Z)V

    .line 192
    iget-boolean v0, p0, Lely;->e:Z

    if-eqz v0, :cond_11

    iget-object v0, p0, Lely;->f:Lelt;

    if-eqz v0, :cond_11

    .line 193
    invoke-virtual {p1, p0}, Ljpg;->a(Ljph;)V

    .line 195
    :cond_11
    invoke-virtual {p1}, Ljpg;->k()V

    goto/16 :goto_0

    .line 199
    :pswitch_4
    const/16 v2, 0x8

    .line 200
    const/16 v1, 0x8

    .line 201
    const/16 v0, 0x8

    .line 202
    const/4 v3, 0x0

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    packed-switch v3, :pswitch_data_1

    .line 213
    :goto_9
    const v3, 0x7f100286

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    .line 214
    const v2, 0x7f100485

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 215
    const v1, 0x7f100486

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 204
    :pswitch_5
    const/4 v2, 0x0

    .line 205
    goto :goto_9

    .line 207
    :pswitch_6
    const/4 v1, 0x0

    .line 208
    goto :goto_9

    .line 210
    :pswitch_7
    const/4 v0, 0x0

    goto :goto_9

    .line 96
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_4
    .end packed-switch

    .line 202
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public a(Ljpg;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 226
    if-nez p2, :cond_0

    .line 227
    invoke-virtual {p1}, Ljpg;->i()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 228
    const-string v0, "add_email_dialog"

    invoke-virtual {p0, v0}, Lely;->c(Ljava/lang/String;)V

    .line 236
    :cond_0
    :goto_0
    return-void

    .line 229
    :cond_1
    invoke-virtual {p1}, Ljpg;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 230
    const-string v0, "add_sms_dialog"

    invoke-virtual {p0, v0}, Lely;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 232
    :cond_2
    iget-object v0, p0, Lely;->f:Lelt;

    invoke-virtual {p1}, Ljpg;->d()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v2, v2}, Lelt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
