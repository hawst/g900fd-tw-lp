.class public final Lppg;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lppg;


# instance fields
.field public b:Lppf;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 184
    const/4 v0, 0x0

    new-array v0, v0, [Lppg;

    sput-object v0, Lppg;->a:[Lppg;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 185
    invoke-direct {p0}, Loxq;-><init>()V

    .line 190
    const/4 v0, 0x0

    iput-object v0, p0, Lppg;->b:Lppf;

    .line 185
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 212
    const/4 v0, 0x0

    .line 213
    iget-object v1, p0, Lppg;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 214
    const/4 v0, 0x1

    iget-object v1, p0, Lppg;->c:Ljava/lang/String;

    .line 215
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 217
    :cond_0
    iget-object v1, p0, Lppg;->b:Lppf;

    if-eqz v1, :cond_1

    .line 218
    const/4 v1, 0x2

    iget-object v2, p0, Lppg;->b:Lppf;

    .line 219
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 221
    :cond_1
    iget-object v1, p0, Lppg;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 222
    const/4 v1, 0x3

    iget-object v2, p0, Lppg;->d:Ljava/lang/String;

    .line 223
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 225
    :cond_2
    iget-object v1, p0, Lppg;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 226
    iput v0, p0, Lppg;->ai:I

    .line 227
    return v0
.end method

.method public a(Loxn;)Lppg;
    .locals 2

    .prologue
    .line 235
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 236
    sparse-switch v0, :sswitch_data_0

    .line 240
    iget-object v1, p0, Lppg;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 241
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lppg;->ah:Ljava/util/List;

    .line 244
    :cond_1
    iget-object v1, p0, Lppg;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 246
    :sswitch_0
    return-object p0

    .line 251
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lppg;->c:Ljava/lang/String;

    goto :goto_0

    .line 255
    :sswitch_2
    iget-object v0, p0, Lppg;->b:Lppf;

    if-nez v0, :cond_2

    .line 256
    new-instance v0, Lppf;

    invoke-direct {v0}, Lppf;-><init>()V

    iput-object v0, p0, Lppg;->b:Lppf;

    .line 258
    :cond_2
    iget-object v0, p0, Lppg;->b:Lppf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 262
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lppg;->d:Ljava/lang/String;

    goto :goto_0

    .line 236
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 197
    iget-object v0, p0, Lppg;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 198
    const/4 v0, 0x1

    iget-object v1, p0, Lppg;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 200
    :cond_0
    iget-object v0, p0, Lppg;->b:Lppf;

    if-eqz v0, :cond_1

    .line 201
    const/4 v0, 0x2

    iget-object v1, p0, Lppg;->b:Lppf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 203
    :cond_1
    iget-object v0, p0, Lppg;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 204
    const/4 v0, 0x3

    iget-object v1, p0, Lppg;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 206
    :cond_2
    iget-object v0, p0, Lppg;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 208
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 181
    invoke-virtual {p0, p1}, Lppg;->a(Loxn;)Lppg;

    move-result-object v0

    return-object v0
.end method
