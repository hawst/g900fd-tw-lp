.class public Llgt;
.super Lt;
.source "PG"


# instance fields
.field public N:Z

.field private O:Landroid/view/View;

.field private P:Landroid/animation/Animator;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 149
    invoke-direct {p0}, Lt;-><init>()V

    .line 150
    return-void
.end method

.method private U()V
    .locals 4

    .prologue
    .line 244
    iget-object v0, p0, Llgt;->P:Landroid/animation/Animator;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    const-string v1, "LEFT 1"

    iget-object v2, p0, Llgt;->O:Landroid/view/View;

    const v3, 0x7f10019c

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "CENTER 1"

    iget-object v2, p0, Llgt;->O:Landroid/view/View;

    const v3, 0x7f10019d

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "RIGHT 1"

    iget-object v2, p0, Llgt;->O:Landroid/view/View;

    const v3, 0x7f10019e

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lhni;->a()Lhni;

    move-result-object v1

    invoke-virtual {p0}, Llgt;->n()Lz;

    move-result-object v2

    const v3, 0x7f080027

    invoke-virtual {v1, v2, v3, v0}, Lhni;->a(Landroid/content/Context;ILjava/util/Map;)Landroid/animation/Animator;

    move-result-object v0

    iput-object v0, p0, Llgt;->P:Landroid/animation/Animator;

    iget-object v0, p0, Llgt;->P:Landroid/animation/Animator;

    new-instance v1, Llgu;

    invoke-direct {v1, p0}, Llgu;-><init>(Llgt;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 245
    :cond_0
    iget-object v0, p0, Llgt;->P:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 246
    iget-object v0, p0, Llgt;->P:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 248
    :cond_1
    iget-object v0, p0, Llgt;->P:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 249
    return-void
.end method

.method private V()Z
    .locals 2

    .prologue
    .line 255
    invoke-virtual {p0}, Llgt;->n()Lz;

    invoke-static {}, Llsj;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 256
    invoke-virtual {p0}, Llgt;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "is_animated"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 136
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 137
    if-eqz p0, :cond_0

    .line 138
    const-string v1, "title"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    :cond_0
    const-string v1, "message"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    if-eqz p2, :cond_1

    .line 142
    const-string v1, "submessage"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    :cond_1
    const-string v1, "is_animated"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 145
    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLu;)Llgt;
    .locals 2

    .prologue
    .line 125
    invoke-static {p0, p1, p2, p4}, Llgt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/os/Bundle;

    move-result-object v0

    .line 127
    new-instance v1, Llgt;

    invoke-direct {v1}, Llgt;-><init>()V

    .line 128
    invoke-virtual {v1, v0}, Llgt;->f(Landroid/os/Bundle;)V

    .line 129
    invoke-virtual {v1, p3}, Llgt;->b(Z)V

    .line 130
    const/4 v0, 0x0

    invoke-virtual {v1, p5, v0}, Llgt;->a(Lu;I)V

    .line 131
    return-object v1
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;ZZ)Llgt;
    .locals 6

    .prologue
    .line 108
    const-string v0, "title"

    const/4 v5, 0x0

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    invoke-static/range {v0 .. v5}, Llgt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLu;)Llgt;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 180
    invoke-direct {p0}, Llgt;->V()Z

    move-result v0

    if-nez v0, :cond_0

    .line 181
    invoke-super {p0, p1, p2, p3}, Lt;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 199
    :goto_0
    return-object v0

    .line 184
    :cond_0
    const v0, 0x7f04004a

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Llgt;->O:Landroid/view/View;

    .line 185
    iget-object v0, p0, Llgt;->O:Landroid/view/View;

    const v1, 0x7f10019f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 187
    invoke-virtual {p0}, Llgt;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "message"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 188
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 189
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 190
    iget-object v0, p0, Llgt;->O:Landroid/view/View;

    const v1, 0x7f1001a0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 192
    invoke-virtual {p0}, Llgt;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "submessage"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 193
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 194
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 195
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 196
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 198
    :cond_1
    invoke-direct {p0}, Llgt;->U()V

    .line 199
    iget-object v0, p0, Llgt;->O:Landroid/view/View;

    goto :goto_0
.end method

.method public a(Lae;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 232
    :try_start_0
    invoke-super {p0, p1, p2}, Lt;->a(Lae;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 238
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 154
    invoke-super {p0, p1}, Lt;->a(Landroid/os/Bundle;)V

    .line 155
    invoke-direct {p0}, Llgt;->V()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 156
    const/4 v0, 0x1

    invoke-virtual {p0}, Llgt;->j_()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Llgt;->a(II)V

    .line 158
    :cond_0
    return-void
.end method

.method public aO_()V
    .locals 1

    .prologue
    .line 204
    invoke-super {p0}, Lt;->aO_()V

    .line 205
    const/4 v0, 0x0

    iput-boolean v0, p0, Llgt;->N:Z

    .line 206
    iget-object v0, p0, Llgt;->P:Landroid/animation/Animator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Llgt;->P:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->isStarted()Z

    move-result v0

    if-nez v0, :cond_0

    .line 207
    invoke-direct {p0}, Llgt;->U()V

    .line 209
    :cond_0
    return-void
.end method

.method public c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 162
    invoke-virtual {p0}, Llgt;->k()Landroid/os/Bundle;

    move-result-object v1

    .line 163
    invoke-direct {p0}, Llgt;->V()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 164
    invoke-super {p0, p1}, Lt;->c(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    .line 174
    :goto_0
    return-object v0

    .line 167
    :cond_0
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Llgt;->n()Lz;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 168
    const-string v2, "title"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 169
    const-string v2, "title"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 171
    :cond_1
    const-string v2, "message"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 172
    invoke-virtual {p0}, Llgt;->e()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 173
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    goto :goto_0
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 2

    .prologue
    .line 222
    invoke-super {p0, p1}, Lt;->onCancel(Landroid/content/DialogInterface;)V

    .line 223
    invoke-virtual {p0}, Llgt;->u_()Lu;

    move-result-object v0

    .line 224
    if-eqz v0, :cond_0

    instance-of v1, v0, Llgv;

    if-eqz v1, :cond_0

    .line 225
    check-cast v0, Llgv;

    invoke-virtual {p0}, Llgt;->k()Landroid/os/Bundle;

    move-result-object v1

    invoke-interface {v0, v1}, Llgv;->l(Landroid/os/Bundle;)V

    .line 227
    :cond_0
    return-void
.end method

.method public z()V
    .locals 1

    .prologue
    .line 213
    invoke-super {p0}, Lt;->z()V

    .line 214
    const/4 v0, 0x1

    iput-boolean v0, p0, Llgt;->N:Z

    .line 215
    iget-object v0, p0, Llgt;->P:Landroid/animation/Animator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Llgt;->P:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 216
    iget-object v0, p0, Llgt;->P:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 218
    :cond_0
    return-void
.end method
