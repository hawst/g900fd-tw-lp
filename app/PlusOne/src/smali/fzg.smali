.class public final Lfzg;
.super Landroid/view/ViewGroup;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lfvq;
.implements Lfze;
.implements Libe;
.implements Lljh;


# static fields
.field private static a:Z

.field private static b:I

.field private static c:I


# instance fields
.field private A:I

.field private B:I

.field private C:Ljava/lang/String;

.field private D:[Lcom/google/android/apps/plus/views/PeopleListRowView;

.field private d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

.field private h:Lfuw;

.field private i:[Ldvw;

.field private j:Landroid/view/View;

.field private k:Landroid/view/View;

.field private l:Landroid/view/View;

.field private m:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private o:Landroid/view/View;

.field private p:Landroid/view/View;

.field private q:Landroid/view/View;

.field private r:Landroid/view/View;

.field private s:Landroid/content/Context;

.field private t:Landroid/view/LayoutInflater;

.field private u:Lfzi;

.field private v:Lhxh;

.field private w:I

.field private x:I

.field private y:I

.field private z:[I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 163
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lfzg;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 164
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 167
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lfzg;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 168
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 171
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 126
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfzg;->d:Ljava/util/ArrayList;

    .line 127
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfzg;->e:Ljava/util/ArrayList;

    .line 128
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfzg;->f:Ljava/util/ArrayList;

    .line 134
    new-array v0, v1, [Ldvw;

    iput-object v0, p0, Lfzg;->i:[Ldvw;

    .line 155
    new-array v0, v1, [I

    iput-object v0, p0, Lfzg;->z:[I

    .line 156
    const/4 v0, 0x0

    iput v0, p0, Lfzg;->A:I

    .line 172
    invoke-virtual {p0, p1}, Lfzg;->a(Landroid/content/Context;)V

    .line 173
    return-void
.end method

.method private a(II)I
    .locals 2

    .prologue
    .line 697
    iget v0, p0, Lfzg;->w:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 698
    add-int v0, p1, p2

    sget v1, Lfzg;->b:I

    add-int p1, v0, v1

    .line 709
    :goto_0
    return p1

    .line 701
    :cond_0
    iget v0, p0, Lfzg;->B:I

    if-nez v0, :cond_1

    .line 702
    iput p2, p0, Lfzg;->B:I

    goto :goto_0

    .line 706
    :cond_1
    iget v0, p0, Lfzg;->B:I

    invoke-static {v0, p2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 707
    const/4 v1, 0x0

    iput v1, p0, Lfzg;->B:I

    .line 709
    add-int/2addr v0, p1

    sget v1, Lfzg;->b:I

    add-int p1, v0, v1

    goto :goto_0
.end method

.method private a(IIILdvw;Z)Landroid/view/View;
    .locals 6

    .prologue
    .line 319
    iget-object v0, p0, Lfzg;->s:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 320
    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object v0, p0

    move v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lfzg;->a(Ljava/lang/String;IILdvw;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private a(ILdvw;Z)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 314
    move-object v0, p0

    move v1, p1

    move v3, v2

    move-object v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lfzg;->a(IIILdvw;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private a(ILjava/util/ArrayList;)Landroid/view/View;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Loiu;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 334
    iget-object v0, p0, Lfzg;->s:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 335
    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v6, 0x0

    const/4 v7, 0x7

    move-object v0, p0

    move-object v3, v2

    move-object v4, p2

    move-object v5, v2

    move-object v8, v2

    invoke-direct/range {v0 .. v8}, Lfzg;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;ZILjava/lang/Object;)Landroid/view/View;

    move-result-object v0

    .line 337
    return-object v0
.end method

.method private a(Ljava/lang/String;IILdvw;Z)Landroid/view/View;
    .locals 6

    .prologue
    .line 326
    iget-object v0, p0, Lfzg;->s:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 327
    if-eqz p2, :cond_0

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 328
    :goto_0
    if-eqz p3, :cond_1

    invoke-virtual {v0, p3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    :goto_1
    move-object v0, p0

    move-object v1, p1

    move-object v4, p4

    move v5, p5

    .line 330
    invoke-direct/range {v0 .. v5}, Lfzg;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ldvw;Z)Landroid/view/View;

    move-result-object v0

    return-object v0

    .line 327
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 328
    :cond_1
    const v1, 0x7f0a09ca

    .line 329
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_1
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ldvw;Z)Landroid/view/View;
    .locals 9

    .prologue
    .line 342
    if-eqz p4, :cond_0

    iget-object v0, p4, Ldvw;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p4, Ldvw;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 343
    :cond_0
    const/4 v0, 0x0

    .line 351
    :goto_0
    return-object v0

    .line 346
    :cond_1
    iget-object v4, p4, Ldvw;->c:Ljava/util/ArrayList;

    .line 347
    iget-object v5, p4, Ldvw;->d:Ljava/util/ArrayList;

    .line 349
    iget v0, p4, Ldvw;->a:I

    .line 350
    packed-switch v0, :pswitch_data_0

    :pswitch_0
    const/4 v7, -0x1

    :goto_1
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v6, p5

    move-object v8, p4

    .line 349
    invoke-direct/range {v0 .. v8}, Lfzg;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;ZILjava/lang/Object;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 350
    :pswitch_1
    const/16 v7, 0x8

    goto :goto_1

    :pswitch_2
    const/4 v7, 0x1

    goto :goto_1

    :pswitch_3
    const/4 v7, 0x2

    goto :goto_1

    :pswitch_4
    const/4 v7, 0x3

    goto :goto_1

    :pswitch_5
    const/4 v7, 0x4

    goto :goto_1

    :pswitch_6
    const/4 v7, 0x5

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;ZILjava/lang/Object;)Landroid/view/View;
    .locals 10

    .prologue
    .line 359
    move/from16 v0, p7

    invoke-virtual {p0, v0}, Lfzg;->a(I)Landroid/view/View;

    move-result-object v8

    .line 361
    const v2, 0x7f10048a

    invoke-virtual {v8, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 362
    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 364
    const v2, 0x7f10048b

    invoke-virtual {v8, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 365
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 366
    invoke-virtual {v2, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 367
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 373
    :goto_0
    const/4 v2, 0x3

    new-array v2, v2, [Lcom/google/android/apps/plus/views/PeopleListRowView;

    iput-object v2, p0, Lfzg;->D:[Lcom/google/android/apps/plus/views/PeopleListRowView;

    .line 374
    iget-object v3, p0, Lfzg;->D:[Lcom/google/android/apps/plus/views/PeopleListRowView;

    const/4 v4, 0x0

    const v2, 0x7f100487

    invoke-virtual {v8, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/PeopleListRowView;

    aput-object v2, v3, v4

    .line 375
    iget-object v3, p0, Lfzg;->D:[Lcom/google/android/apps/plus/views/PeopleListRowView;

    const/4 v4, 0x1

    const v2, 0x7f100488

    invoke-virtual {v8, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/PeopleListRowView;

    aput-object v2, v3, v4

    .line 376
    iget-object v3, p0, Lfzg;->D:[Lcom/google/android/apps/plus/views/PeopleListRowView;

    const/4 v4, 0x2

    const v2, 0x7f100489

    invoke-virtual {v8, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/PeopleListRowView;

    aput-object v2, v3, v4

    .line 378
    const/4 v2, 0x0

    :goto_1
    iget-object v3, p0, Lfzg;->D:[Lcom/google/android/apps/plus/views/PeopleListRowView;

    array-length v3, v3

    if-ge v2, v3, :cond_1

    .line 379
    iget-object v3, p0, Lfzg;->D:[Lcom/google/android/apps/plus/views/PeopleListRowView;

    aget-object v3, v3, v2

    iget-object v4, p0, Lfzg;->v:Lhxh;

    move/from16 v0, p6

    invoke-virtual {v3, p0, v4, v0}, Lcom/google/android/apps/plus/views/PeopleListRowView;->a(Lfze;Lhxh;Z)V

    .line 380
    iget-object v3, p0, Lfzg;->D:[Lcom/google/android/apps/plus/views/PeopleListRowView;

    aget-object v3, v3, v2

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/views/PeopleListRowView;->setVisibility(I)V

    .line 378
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 369
    :cond_0
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 370
    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 383
    :cond_1
    invoke-virtual {p4}, Ljava/util/ArrayList;->size()I

    move-result v7

    .line 384
    const/4 v2, 0x3

    if-le v7, v2, :cond_2

    const/4 v7, 0x3

    .line 386
    :cond_2
    const/4 v6, -0x1

    .line 389
    const/4 v3, 0x0

    .line 390
    :goto_2
    if-ge v3, v7, :cond_b

    .line 391
    invoke-virtual {p4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    .line 392
    if-eqz p5, :cond_3

    const/4 v2, -0x1

    if-ne v6, v2, :cond_3

    .line 393
    invoke-virtual {p5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    .line 394
    invoke-direct {p0, v4}, Lfzg;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 395
    invoke-direct {p0, v2}, Lfzg;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 396
    invoke-static {v5, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    move v6, v3

    .line 401
    :cond_3
    const/4 v2, 0x7

    move/from16 v0, p7

    if-eq v0, v2, :cond_5

    const/4 v2, 0x1

    .line 403
    :goto_3
    if-eqz v2, :cond_4

    .line 404
    iget-object v5, p0, Lfzg;->D:[Lcom/google/android/apps/plus/views/PeopleListRowView;

    aget-object v5, v5, v3

    invoke-virtual {v5}, Lcom/google/android/apps/plus/views/PeopleListRowView;->b()V

    .line 406
    :cond_4
    iget-object v5, p0, Lfzg;->D:[Lcom/google/android/apps/plus/views/PeopleListRowView;

    aget-object v5, v5, v3

    iget-object v9, p0, Lfzg;->C:Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lfzg;->h:Lfuw;

    :goto_4
    invoke-virtual {v5, v4, v9, v2}, Lcom/google/android/apps/plus/views/PeopleListRowView;->a(Ljava/lang/Object;Ljava/lang/String;Lfuw;)V

    .line 408
    iget-object v2, p0, Lfzg;->D:[Lcom/google/android/apps/plus/views/PeopleListRowView;

    aget-object v2, v2, v3

    invoke-virtual {v2, p0}, Lcom/google/android/apps/plus/views/PeopleListRowView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 409
    iget-object v2, p0, Lfzg;->D:[Lcom/google/android/apps/plus/views/PeopleListRowView;

    aget-object v2, v2, v3

    move/from16 v0, p7

    move-object/from16 v1, p8

    invoke-virtual {v2, v0, v1}, Lcom/google/android/apps/plus/views/PeopleListRowView;->a(ILjava/lang/Object;)V

    .line 390
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 401
    :cond_5
    const/4 v2, 0x0

    goto :goto_3

    .line 406
    :cond_6
    const/4 v2, 0x0

    goto :goto_4

    .line 412
    :goto_5
    const/4 v3, 0x3

    if-ge v2, v3, :cond_8

    .line 413
    iget-object v3, p0, Lfzg;->D:[Lcom/google/android/apps/plus/views/PeopleListRowView;

    aget-object v3, v3, v2

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/google/android/apps/plus/views/PeopleListRowView;->a(Ljava/lang/Object;Lfuw;)V

    .line 414
    iget-object v3, p0, Lfzg;->D:[Lcom/google/android/apps/plus/views/PeopleListRowView;

    aget-object v3, v3, v2

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/views/PeopleListRowView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 415
    iget-object v3, p0, Lfzg;->D:[Lcom/google/android/apps/plus/views/PeopleListRowView;

    aget-object v3, v3, v2

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/views/PeopleListRowView;->setClickable(Z)V

    .line 416
    iget-object v3, p0, Lfzg;->D:[Lcom/google/android/apps/plus/views/PeopleListRowView;

    aget-object v3, v3, v2

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/PeopleListRowView;->b()V

    .line 417
    iget v3, p0, Lfzg;->w:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_7

    .line 418
    iget-object v3, p0, Lfzg;->D:[Lcom/google/android/apps/plus/views/PeopleListRowView;

    aget-object v3, v3, v2

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/views/PeopleListRowView;->setVisibility(I)V

    .line 412
    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 422
    :cond_8
    const/4 v2, -0x1

    if-eq v6, v2, :cond_9

    .line 423
    iget-object v2, p0, Lfzg;->h:Lfuw;

    iget-object v4, p0, Lfzg;->g:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    iget-object v5, p0, Lfzg;->D:[Lcom/google/android/apps/plus/views/PeopleListRowView;

    move-object v3, p0

    invoke-virtual/range {v2 .. v7}, Lfuw;->a(Landroid/view/View;Landroid/view/ViewGroup;[Lcom/google/android/apps/plus/views/PeopleListRowView;II)V

    .line 427
    :cond_9
    const v2, 0x7f100493

    invoke-virtual {v8, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 428
    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 429
    const v3, 0x7f1000ab

    invoke-static/range {p7 .. p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 430
    const v3, 0x7f1000ac

    move-object/from16 v0, p8

    invoke-virtual {v2, v3, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 431
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 433
    if-nez p3, :cond_a

    .line 434
    iget-object v2, p0, Lfzg;->s:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 435
    const v3, 0x7f0a09ca

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p3

    .line 438
    :cond_a
    const v2, 0x7f1001ec

    invoke-virtual {v8, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 439
    invoke-virtual {v2, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 441
    invoke-virtual {p0, v8}, Lfzg;->addView(Landroid/view/View;)V

    .line 443
    return-object v8

    :cond_b
    move v2, v3

    goto/16 :goto_5
.end method

.method private a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 457
    const/4 v0, 0x0

    .line 458
    instance-of v1, p1, Lnrp;

    if-eqz v1, :cond_1

    .line 459
    check-cast p1, Lnrp;

    iget-object v0, p1, Lnrp;->b:Lohv;

    iget-object v0, v0, Lohv;->b:Lohp;

    iget-object v0, v0, Lohp;->d:Ljava/lang/String;

    .line 465
    :cond_0
    :goto_0
    return-object v0

    .line 460
    :cond_1
    instance-of v1, p1, Loiu;

    if-eqz v1, :cond_2

    .line 461
    check-cast p1, Loiu;

    iget-object v0, p1, Loiu;->b:Lohv;

    iget-object v0, v0, Lohv;->b:Lohp;

    iget-object v0, v0, Lohp;->d:Ljava/lang/String;

    goto :goto_0

    .line 462
    :cond_2
    instance-of v1, p1, Lohv;

    if-eqz v1, :cond_0

    .line 463
    check-cast p1, Lohv;

    iget-object v0, p1, Lohv;->b:Lohp;

    iget-object v0, v0, Lohp;->d:Ljava/lang/String;

    goto :goto_0
.end method

.method private a(Lohl;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 469
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 470
    iget-object v0, p1, Lohl;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    move v0, v1

    .line 471
    :goto_0
    iget-object v4, p1, Lohl;->d:Ljava/lang/Integer;

    if-eqz v4, :cond_2

    .line 473
    :goto_1
    if-eqz v0, :cond_3

    if-eqz v1, :cond_3

    .line 474
    iget-object v0, p1, Lohl;->c:Ljava/lang/Integer;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 475
    const-string v0, " - "

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 476
    iget-object v0, p1, Lohl;->d:Ljava/lang/Integer;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 483
    :cond_0
    :goto_2
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    move v0, v2

    .line 470
    goto :goto_0

    :cond_2
    move v1, v2

    .line 471
    goto :goto_1

    .line 477
    :cond_3
    if-eqz v0, :cond_4

    .line 478
    iget-object v0, p1, Lohl;->c:Ljava/lang/Integer;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 479
    :cond_4
    if-eqz v1, :cond_0

    .line 480
    iget-object v0, p1, Lohl;->d:Ljava/lang/Integer;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_2
.end method

.method private a(IZ)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 605
    iget-object v0, p0, Lfzg;->z:[I

    iget v1, p0, Lfzg;->A:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lfzg;->A:I

    aput p1, v0, v1

    .line 606
    iget v0, p0, Lfzg;->A:I

    iget v1, p0, Lfzg;->w:I

    if-eq v0, v1, :cond_0

    if-eqz p2, :cond_1

    .line 607
    :cond_0
    iget v0, p0, Lfzg;->y:I

    iget-object v1, p0, Lfzg;->z:[I

    aget v1, v1, v3

    iget-object v2, p0, Lfzg;->z:[I

    aget v2, v2, v4

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lfzg;->y:I

    .line 608
    iget-object v0, p0, Lfzg;->z:[I

    iget-object v1, p0, Lfzg;->z:[I

    iput v3, p0, Lfzg;->A:I

    aput v3, v1, v4

    aput v3, v0, v3

    .line 610
    :cond_1
    return-void
.end method

.method private a(Ljava/util/List;Landroid/view/View;Landroid/view/View;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Libd;",
            ">;",
            "Landroid/view/View;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    .line 955
    if-eqz p2, :cond_1

    if-eqz p3, :cond_1

    iget-object v0, p0, Lfzg;->D:[Lcom/google/android/apps/plus/views/PeopleListRowView;

    if-eqz v0, :cond_1

    .line 956
    iget-object v1, p0, Lfzg;->D:[Lcom/google/android/apps/plus/views/PeopleListRowView;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 957
    if-eqz v3, :cond_0

    .line 958
    invoke-static {v3, p3}, Llii;->a(Landroid/view/View;Landroid/view/View;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 959
    new-instance v4, Libd;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/PeopleListRowView;->c()Ljava/lang/String;

    move-result-object v5

    .line 960
    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/PeopleListRowView;->d()Ljava/lang/String;

    move-result-object v6

    .line 962
    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/PeopleListRowView;->e()I

    move-result v3

    .line 961
    invoke-static {v3}, Ldib;->b(I)I

    move-result v3

    invoke-direct {v4, v5, v6, v3}, Libd;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    .line 959
    invoke-interface {p1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 956
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 966
    :cond_1
    return-void
.end method

.method private b(I)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 685
    iget v1, p0, Lfzg;->w:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 692
    :cond_0
    :goto_0
    return v0

    .line 689
    :cond_1
    invoke-virtual {p0}, Lfzg;->getPaddingLeft()I

    move-result v1

    if-ne p1, v1, :cond_0

    .line 690
    iget v0, p0, Lfzg;->x:I

    add-int/2addr v0, p1

    sget v1, Lfzg;->c:I

    add-int/2addr v0, v1

    goto :goto_0
.end method


# virtual methods
.method a(I)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    const v3, 0x7f04016a

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 295
    if-ne p1, v1, :cond_1

    .line 296
    iget-object v0, p0, Lfzg;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, v1, :cond_0

    .line 297
    iget-object v0, p0, Lfzg;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 309
    :goto_0
    iget-object v1, p0, Lfzg;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 310
    return-object v0

    .line 299
    :cond_0
    iget-object v0, p0, Lfzg;->t:Landroid/view/LayoutInflater;

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 300
    const v1, 0x7f1000ab

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    goto :goto_0

    .line 303
    :cond_1
    iget-object v0, p0, Lfzg;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, v1, :cond_2

    .line 304
    iget-object v0, p0, Lfzg;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    goto :goto_0

    .line 306
    :cond_2
    iget-object v0, p0, Lfzg;->t:Landroid/view/LayoutInflater;

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method a(Ljava/util/ArrayList;II)Ldvw;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ldvw;",
            ">;II)",
            "Ldvw;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 276
    packed-switch p2, :pswitch_data_0

    :pswitch_0
    const/4 v1, -0x1

    move v5, v1

    .line 278
    :goto_0
    if-nez p1, :cond_0

    move v1, v0

    :goto_1
    move v4, v0

    move v3, p3

    .line 279
    :goto_2
    if-ge v4, v1, :cond_2

    .line 280
    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldvw;

    .line 281
    iget v2, v0, Ldvw;->a:I

    if-ne v5, v2, :cond_3

    .line 282
    iget-object v2, v0, Ldvw;->c:Ljava/util/ArrayList;

    if-eqz v2, :cond_3

    iget-object v2, v0, Ldvw;->c:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 283
    add-int/lit8 v2, v3, -0x1

    if-nez v3, :cond_1

    .line 290
    :goto_3
    return-object v0

    .line 276
    :pswitch_1
    const/4 v1, 0x7

    move v5, v1

    goto :goto_0

    :pswitch_2
    move v5, v0

    goto :goto_0

    :pswitch_3
    const/4 v1, 0x1

    move v5, v1

    goto :goto_0

    :pswitch_4
    const/4 v1, 0x2

    move v5, v1

    goto :goto_0

    :pswitch_5
    const/4 v1, 0x3

    move v5, v1

    goto :goto_0

    :pswitch_6
    const/4 v1, 0x4

    move v5, v1

    goto :goto_0

    .line 278
    :cond_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    goto :goto_1

    :cond_1
    move v0, v2

    .line 279
    :goto_4
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v3, v0

    goto :goto_2

    .line 290
    :cond_2
    const/4 v0, 0x0

    goto :goto_3

    :cond_3
    move v0, v3

    goto :goto_4

    .line 276
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Landroid/view/View;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")",
            "Ljava/util/List",
            "<",
            "Libd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 918
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 920
    iget-object v0, p0, Lfzg;->j:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 921
    iget-object v0, p0, Lfzg;->j:Landroid/view/View;

    invoke-direct {p0, v1, v0, p1}, Lfzg;->a(Ljava/util/List;Landroid/view/View;Landroid/view/View;)V

    .line 924
    :cond_0
    iget-object v0, p0, Lfzg;->k:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 925
    iget-object v0, p0, Lfzg;->k:Landroid/view/View;

    invoke-direct {p0, v1, v0, p1}, Lfzg;->a(Ljava/util/List;Landroid/view/View;Landroid/view/View;)V

    .line 928
    :cond_1
    iget-object v0, p0, Lfzg;->l:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 929
    iget-object v0, p0, Lfzg;->l:Landroid/view/View;

    invoke-direct {p0, v1, v0, p1}, Lfzg;->a(Ljava/util/List;Landroid/view/View;Landroid/view/View;)V

    .line 932
    :cond_2
    iget-object v0, p0, Lfzg;->m:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    .line 933
    iget-object v0, p0, Lfzg;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 934
    invoke-direct {p0, v1, v0, p1}, Lfzg;->a(Ljava/util/List;Landroid/view/View;Landroid/view/View;)V

    goto :goto_0

    .line 938
    :cond_3
    iget-object v0, p0, Lfzg;->n:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    .line 939
    iget-object v0, p0, Lfzg;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 940
    invoke-direct {p0, v1, v0, p1}, Lfzg;->a(Ljava/util/List;Landroid/view/View;Landroid/view/View;)V

    goto :goto_1

    .line 944
    :cond_4
    iget-object v0, p0, Lfzg;->o:Landroid/view/View;

    if-eqz v0, :cond_5

    .line 945
    iget-object v0, p0, Lfzg;->o:Landroid/view/View;

    invoke-direct {p0, v1, v0, p1}, Lfzg;->a(Ljava/util/List;Landroid/view/View;Landroid/view/View;)V

    .line 948
    :cond_5
    iget-object v0, p0, Lfzg;->q:Landroid/view/View;

    invoke-direct {p0, v1, v0, p1}, Lfzg;->a(Ljava/util/List;Landroid/view/View;Landroid/view/View;)V

    .line 950
    return-object v1
.end method

.method public a()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 195
    iget-object v0, p0, Lfzg;->i:[Ldvw;

    const/4 v1, 0x0

    aput-object v2, v0, v1

    .line 196
    iget-object v0, p0, Lfzg;->i:[Ldvw;

    aput-object v2, v0, v3

    .line 198
    iput-object v2, p0, Lfzg;->j:Landroid/view/View;

    .line 199
    iput-object v2, p0, Lfzg;->k:Landroid/view/View;

    .line 200
    iput-object v2, p0, Lfzg;->l:Landroid/view/View;

    .line 201
    iput-object v2, p0, Lfzg;->m:Ljava/util/ArrayList;

    .line 202
    iput-object v2, p0, Lfzg;->n:Ljava/util/ArrayList;

    .line 203
    iput-object v2, p0, Lfzg;->o:Landroid/view/View;

    .line 204
    iput-object v2, p0, Lfzg;->q:Landroid/view/View;

    .line 205
    iput-object v2, p0, Lfzg;->r:Landroid/view/View;

    .line 207
    iput-object v2, p0, Lfzg;->u:Lfzi;

    .line 208
    iget-object v0, p0, Lfzg;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 209
    const v1, 0x7f1000ab

    invoke-virtual {v0, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 210
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v1, v3, :cond_0

    .line 211
    iget-object v1, p0, Lfzg;->e:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 213
    :cond_0
    iget-object v1, p0, Lfzg;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 216
    :cond_1
    iget-object v0, p0, Lfzg;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 218
    iget-object v0, p0, Lfzg;->h:Lfuw;

    invoke-virtual {v0}, Lfuw;->d()V

    .line 219
    return-void
.end method

.method a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 176
    iput-object p1, p0, Lfzg;->s:Landroid/content/Context;

    .line 177
    iget-object v0, p0, Lfzg;->s:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lfzg;->t:Landroid/view/LayoutInflater;

    .line 179
    sget-boolean v0, Lfzg;->a:Z

    if-nez v0, :cond_0

    .line 180
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 181
    const v1, 0x7f0d033d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    sput v0, Lfzg;->b:I

    .line 182
    new-instance v0, Llcr;

    invoke-direct {v0, p1}, Llcr;-><init>(Landroid/content/Context;)V

    .line 183
    iget v0, v0, Llcr;->d:I

    sput v0, Lfzg;->c:I

    .line 184
    const/4 v0, 0x1

    sput-boolean v0, Lfzg;->a:Z

    .line 186
    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/apps/plus/views/PeopleListRowView;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 903
    const v0, 0x7f100490

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/views/PeopleListRowView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 904
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/PeopleListRowView;->getWidth()I

    move-result v0

    int-to-float v3, v0

    const-wide/16 v4, 0x12c

    const/4 v6, 0x1

    move-object v1, p1

    .line 903
    invoke-virtual/range {v1 .. v6}, Lcom/google/android/apps/plus/views/PeopleListRowView;->a(Landroid/view/View;FJZ)V

    .line 905
    return-void
.end method

.method public a(Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;)V
    .locals 3

    .prologue
    .line 189
    iput-object p1, p0, Lfzg;->g:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    .line 190
    new-instance v0, Lfuw;

    invoke-virtual {p0}, Lfzg;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lfzg;->g:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    invoke-direct {v0, v1, v2, p0}, Lfuw;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;Lfvq;)V

    iput-object v0, p0, Lfzg;->h:Lfuw;

    .line 191
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;I)V
    .locals 1

    .prologue
    .line 861
    iget-object v0, p0, Lfzg;->u:Lfzi;

    if-eqz v0, :cond_0

    .line 862
    iget-object v0, p0, Lfzg;->u:Lfzi;

    invoke-interface {v0, p1, p2, p4}, Lfzi;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 864
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 888
    iget-object v0, p0, Lfzg;->u:Lfzi;

    if-eqz v0, :cond_0

    .line 889
    iget-object v0, p0, Lfzg;->u:Lfzi;

    invoke-interface {v0, p1, p2, p3, p4}, Lfzi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 892
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;I)V
    .locals 6

    .prologue
    .line 869
    iget-object v0, p0, Lfzg;->u:Lfzi;

    if-eqz v0, :cond_0

    .line 870
    iget-object v0, p0, Lfzg;->u:Lfzi;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lfzi;->a(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;I)V

    .line 873
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 896
    iget-object v0, p0, Lfzg;->u:Lfzi;

    if-eqz v0, :cond_0

    .line 897
    iget-object v0, p0, Lfzg;->u:Lfzi;

    invoke-interface {v0, p1, p2}, Lfzi;->a(Ljava/lang/String;Z)V

    .line 899
    :cond_0
    return-void
.end method

.method public a(Ljava/util/ArrayList;Lefr;Lfzh;Lfzh;Lhxh;Lfzi;Ljava/lang/String;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ldvw;",
            ">;",
            "Lefr;",
            "Lfzh;",
            "Lfzh;",
            "Lhxh;",
            "Lfzi;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 490
    iget-object v0, p0, Lfzg;->s:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 491
    if-eqz p4, :cond_1

    const/4 v0, 0x2

    :goto_0
    iput v0, p0, Lfzg;->w:I

    .line 492
    iput-object p6, p0, Lfzg;->u:Lfzi;

    .line 493
    iput-object p5, p0, Lfzg;->v:Lhxh;

    .line 495
    const/4 v0, 0x0

    iput-object v0, p0, Lfzg;->j:Landroid/view/View;

    .line 496
    const/4 v0, 0x0

    iput-object v0, p0, Lfzg;->k:Landroid/view/View;

    .line 497
    const/4 v0, 0x0

    iput-object v0, p0, Lfzg;->l:Landroid/view/View;

    .line 498
    const/4 v0, 0x0

    iput-object v0, p0, Lfzg;->m:Ljava/util/ArrayList;

    .line 499
    const/4 v0, 0x0

    iput-object v0, p0, Lfzg;->n:Ljava/util/ArrayList;

    .line 500
    const/4 v0, 0x0

    iput-object v0, p0, Lfzg;->o:Landroid/view/View;

    .line 501
    const/4 v0, 0x0

    iput-object v0, p0, Lfzg;->p:Landroid/view/View;

    .line 502
    const/4 v0, 0x0

    iput-object v0, p0, Lfzg;->q:Landroid/view/View;

    .line 503
    const/4 v0, 0x0

    iput-object v0, p0, Lfzg;->r:Landroid/view/View;

    .line 504
    iput-object p7, p0, Lfzg;->C:Ljava/lang/String;

    .line 505
    invoke-virtual {p0}, Lfzg;->removeAllViews()V

    .line 508
    const/4 v0, 0x0

    move v6, v0

    :goto_1
    iget v0, p0, Lfzg;->w:I

    if-ge v6, v0, :cond_5

    .line 509
    if-nez v6, :cond_2

    move-object v0, p3

    .line 511
    :goto_2
    iget-object v1, p0, Lfzg;->i:[Ldvw;

    iget v2, v0, Lfzh;->a:I

    iget v3, v0, Lfzh;->b:I

    .line 512
    invoke-virtual {p0, p1, v2, v3}, Lfzg;->a(Ljava/util/ArrayList;II)Ldvw;

    move-result-object v2

    aput-object v2, v1, v6

    .line 514
    iget v0, v0, Lfzh;->a:I

    packed-switch v0, :pswitch_data_0

    .line 508
    :cond_0
    :goto_3
    :pswitch_0
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_1

    .line 491
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move-object v0, p4

    .line 509
    goto :goto_2

    .line 516
    :pswitch_1
    const v1, 0x7f0a09b8

    const v2, 0x7f0a09bb

    const/4 v3, 0x0

    iget-object v0, p0, Lfzg;->i:[Ldvw;

    aget-object v4, v0, v6

    const/4 v5, 0x1

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lfzg;->a(IIILdvw;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lfzg;->j:Landroid/view/View;

    goto :goto_3

    .line 522
    :pswitch_2
    iget-object v0, p0, Lfzg;->i:[Ldvw;

    aget-object v0, v0, v6

    iget-object v0, v0, Ldvw;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 523
    const v1, 0x7f110071

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 524
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 523
    invoke-virtual {v7, v1, v0, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 525
    const v2, 0x7f0a09bb

    const/4 v3, 0x0

    iget-object v0, p0, Lfzg;->i:[Ldvw;

    aget-object v4, v0, v6

    const/4 v5, 0x1

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lfzg;->a(Ljava/lang/String;IILdvw;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lfzg;->k:Landroid/view/View;

    goto :goto_3

    .line 531
    :pswitch_3
    const v0, 0x7f0a09bf

    iget-object v1, p0, Lfzg;->i:[Ldvw;

    aget-object v1, v1, v6

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lfzg;->a(ILdvw;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lfzg;->l:Landroid/view/View;

    goto :goto_3

    .line 536
    :pswitch_4
    iget-object v0, p0, Lfzg;->i:[Ldvw;

    aget-object v0, v0, v6

    iget-object v0, v0, Ldvw;->b:Lohl;

    iget-object v1, v0, Lohl;->a:Ljava/lang/String;

    .line 537
    iget-object v0, p0, Lfzg;->i:[Ldvw;

    aget-object v0, v0, v6

    iget-object v0, v0, Ldvw;->b:Lohl;

    invoke-direct {p0, v0}, Lfzg;->a(Lohl;)Ljava/lang/String;

    move-result-object v2

    .line 538
    const/4 v3, 0x0

    iget-object v0, p0, Lfzg;->i:[Ldvw;

    aget-object v4, v0, v6

    const/4 v5, 0x1

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lfzg;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ldvw;Z)Landroid/view/View;

    move-result-object v0

    .line 540
    if-eqz v0, :cond_0

    .line 541
    iget-object v1, p0, Lfzg;->n:Ljava/util/ArrayList;

    if-nez v1, :cond_3

    .line 542
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lfzg;->n:Ljava/util/ArrayList;

    .line 544
    :cond_3
    iget-object v1, p0, Lfzg;->n:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 545
    new-instance v1, Lhmk;

    sget-object v2, Lonl;->b:Lhmn;

    invoke-direct {v1, v2}, Lhmk;-><init>(Lhmn;)V

    invoke-static {v0, v1}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    .line 547
    invoke-static {v0}, Lhmc;->a(Landroid/view/View;)V

    goto/16 :goto_3

    .line 552
    :pswitch_5
    iget-object v0, p0, Lfzg;->i:[Ldvw;

    aget-object v0, v0, v6

    iget-object v0, v0, Ldvw;->b:Lohl;

    iget-object v1, v0, Lohl;->a:Ljava/lang/String;

    .line 553
    iget-object v0, p0, Lfzg;->i:[Ldvw;

    aget-object v0, v0, v6

    iget-object v0, v0, Ldvw;->b:Lohl;

    invoke-direct {p0, v0}, Lfzg;->a(Lohl;)Ljava/lang/String;

    move-result-object v2

    .line 554
    const/4 v3, 0x0

    iget-object v0, p0, Lfzg;->i:[Ldvw;

    aget-object v4, v0, v6

    const/4 v5, 0x1

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lfzg;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ldvw;Z)Landroid/view/View;

    move-result-object v0

    .line 556
    if-eqz v0, :cond_0

    .line 557
    iget-object v1, p0, Lfzg;->m:Ljava/util/ArrayList;

    if-nez v1, :cond_4

    .line 558
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lfzg;->m:Ljava/util/ArrayList;

    .line 560
    :cond_4
    iget-object v1, p0, Lfzg;->m:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 561
    new-instance v1, Lhmk;

    sget-object v2, Lonl;->a:Lhmn;

    invoke-direct {v1, v2}, Lhmk;-><init>(Lhmn;)V

    invoke-static {v0, v1}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    .line 563
    invoke-static {v0}, Lhmc;->a(Landroid/view/View;)V

    goto/16 :goto_3

    .line 568
    :pswitch_6
    const v0, 0x7f0a09c7

    iget-object v1, p0, Lfzg;->i:[Ldvw;

    aget-object v1, v1, v6

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lfzg;->a(ILdvw;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lfzg;->o:Landroid/view/View;

    .line 570
    iget-object v0, p0, Lfzg;->o:Landroid/view/View;

    new-instance v1, Lhmk;

    sget-object v2, Lonl;->f:Lhmn;

    invoke-direct {v1, v2}, Lhmk;-><init>(Lhmn;)V

    invoke-static {v0, v1}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    .line 572
    iget-object v0, p0, Lfzg;->o:Landroid/view/View;

    invoke-static {v0}, Lhmc;->a(Landroid/view/View;)V

    goto/16 :goto_3

    .line 576
    :pswitch_7
    iget-object v0, p0, Lfzg;->t:Landroid/view/LayoutInflater;

    const v1, 0x7f0400bd

    const/4 v2, 0x0

    .line 577
    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lfzg;->p:Landroid/view/View;

    .line 578
    iget-object v0, p0, Lfzg;->p:Landroid/view/View;

    const v1, 0x7f1002e5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 579
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 580
    iget-object v0, p0, Lfzg;->p:Landroid/view/View;

    const v1, 0x7f1002e8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 581
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 582
    iget-object v0, p0, Lfzg;->p:Landroid/view/View;

    invoke-virtual {p0, v0}, Lfzg;->addView(Landroid/view/View;)V

    goto/16 :goto_3

    .line 586
    :pswitch_8
    const v0, 0x7f0a09c8

    iget-object v1, p2, Lefr;->a:Ljava/util/ArrayList;

    invoke-direct {p0, v0, v1}, Lfzg;->a(ILjava/util/ArrayList;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lfzg;->q:Landroid/view/View;

    .line 588
    iget-object v0, p0, Lfzg;->q:Landroid/view/View;

    new-instance v1, Lhmk;

    sget-object v2, Lonl;->c:Lhmn;

    invoke-direct {v1, v2}, Lhmk;-><init>(Lhmn;)V

    invoke-static {v0, v1}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    .line 590
    iget-object v0, p0, Lfzg;->q:Landroid/view/View;

    invoke-static {v0}, Lhmc;->a(Landroid/view/View;)V

    goto/16 :goto_3

    .line 594
    :pswitch_9
    iget-object v0, p0, Lfzg;->t:Landroid/view/LayoutInflater;

    const v1, 0x7f04016e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lfzg;->r:Landroid/view/View;

    .line 595
    iget-object v0, p0, Lfzg;->r:Landroid/view/View;

    const v1, 0x7f10048f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 596
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 597
    iget-object v0, p0, Lfzg;->r:Landroid/view/View;

    invoke-virtual {p0, v0}, Lfzg;->addView(Landroid/view/View;)V

    goto/16 :goto_3

    .line 602
    :cond_5
    return-void

    .line 514
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
    .end packed-switch
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 979
    return-void
.end method

.method public ad()V
    .locals 0

    .prologue
    .line 910
    return-void
.end method

.method public b()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 447
    iget-object v0, p0, Lfzg;->i:[Ldvw;

    aget-object v0, v0, v1

    if-eqz v0, :cond_0

    .line 448
    iget-object v0, p0, Lfzg;->i:[Ldvw;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ldvw;->c()V

    .line 450
    :cond_0
    iget-object v0, p0, Lfzg;->i:[Ldvw;

    aget-object v0, v0, v2

    if-eqz v0, :cond_1

    .line 451
    iget-object v0, p0, Lfzg;->i:[Ldvw;

    aget-object v0, v0, v2

    invoke-virtual {v0}, Ldvw;->c()V

    .line 454
    :cond_1
    return-void
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;I)Z
    .locals 6

    .prologue
    .line 878
    iget-object v0, p0, Lfzg;->u:Lfzi;

    if-eqz v0, :cond_0

    .line 879
    iget-object v0, p0, Lfzg;->u:Lfzi;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lfzi;->b(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;I)Z

    move-result v0

    .line 882
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 970
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/PeopleListRowView;

    .line 971
    iget-object v1, p0, Lfzg;->u:Lfzi;

    if-eqz v1, :cond_0

    .line 972
    iget-object v1, p0, Lfzg;->u:Lfzi;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PeopleListRowView;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PeopleListRowView;->d()Ljava/lang/String;

    move-result-object v3

    .line 973
    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PeopleListRowView;->e()I

    move-result v4

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PeopleListRowView;->f()Ljava/lang/Object;

    move-result-object v0

    .line 972
    invoke-interface {v1, v2, v3, v4, v0}, Lfzi;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V

    .line 975
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const v3, 0x7f1000ac

    .line 797
    iget-object v0, p0, Lfzg;->u:Lfzi;

    if-nez v0, :cond_1

    .line 856
    :cond_0
    :goto_0
    return-void

    .line 801
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    .line 802
    const v0, 0x7f1000ab

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 803
    const v2, 0x7f100487

    if-eq v1, v2, :cond_2

    const v2, 0x7f100488

    if-eq v1, v2, :cond_2

    const v2, 0x7f100489

    if-ne v1, v2, :cond_3

    .line 804
    :cond_2
    check-cast p1, Lcom/google/android/apps/plus/views/PeopleListRowView;

    .line 806
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Ldib;->b(I)I

    move-result v0

    .line 807
    iget-object v1, p0, Lfzg;->u:Lfzi;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/PeopleListRowView;->c()Ljava/lang/String;

    move-result-object v2

    .line 808
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/PeopleListRowView;->d()Ljava/lang/String;

    move-result-object v3

    .line 807
    invoke-interface {v1, v2, v3, v0}, Lfzi;->a(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 810
    :cond_3
    const v2, 0x7f100493

    if-ne v1, v2, :cond_4

    .line 811
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 817
    :pswitch_1
    iget-object v0, p0, Lfzg;->u:Lfzi;

    invoke-interface {v0}, Lfzi;->V()V

    goto :goto_0

    .line 813
    :pswitch_2
    iget-object v0, p0, Lfzg;->u:Lfzi;

    invoke-interface {v0}, Lfzi;->U()V

    goto :goto_0

    .line 821
    :pswitch_3
    iget-object v0, p0, Lfzg;->u:Lfzi;

    invoke-interface {v0}, Lfzi;->W()V

    goto :goto_0

    .line 826
    :pswitch_4
    invoke-virtual {p1, v3}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldvw;

    .line 827
    iget-object v1, p0, Lfzg;->u:Lfzi;

    invoke-interface {v1, v0}, Lfzi;->a(Ldvw;)V

    goto :goto_0

    .line 832
    :pswitch_5
    invoke-virtual {p1, v3}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldvw;

    .line 833
    iget-object v1, p0, Lfzg;->u:Lfzi;

    invoke-interface {v1, v0}, Lfzi;->b(Ldvw;)V

    goto :goto_0

    .line 837
    :pswitch_6
    iget-object v0, p0, Lfzg;->u:Lfzi;

    invoke-interface {v0}, Lfzi;->X()V

    goto :goto_0

    .line 841
    :pswitch_7
    iget-object v0, p0, Lfzg;->u:Lfzi;

    invoke-interface {v0}, Lfzi;->e()V

    goto :goto_0

    .line 846
    :cond_4
    const v0, 0x7f1002e5

    if-ne v1, v0, :cond_5

    .line 847
    iget-object v0, p0, Lfzg;->u:Lfzi;

    invoke-interface {v0}, Lfzi;->d()V

    goto/16 :goto_0

    .line 849
    :cond_5
    const v0, 0x7f1002e8

    if-ne v1, v0, :cond_6

    .line 850
    iget-object v0, p0, Lfzg;->u:Lfzi;

    invoke-interface {v0}, Lfzi;->c()V

    goto/16 :goto_0

    .line 852
    :cond_6
    const v0, 0x7f10048f

    if-ne v1, v0, :cond_0

    .line 853
    iget-object v0, p0, Lfzg;->u:Lfzi;

    invoke-interface {v0}, Lfzi;->Y()V

    goto/16 :goto_0

    .line 811
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_5
        :pswitch_4
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_2
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 714
    invoke-virtual {p0}, Lfzg;->getPaddingLeft()I

    move-result v1

    .line 715
    invoke-virtual {p0}, Lfzg;->getPaddingTop()I

    move-result v0

    .line 717
    iput v3, p0, Lfzg;->B:I

    .line 719
    iget-object v2, p0, Lfzg;->j:Landroid/view/View;

    if-eqz v2, :cond_0

    .line 720
    iget-object v2, p0, Lfzg;->j:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    .line 721
    iget-object v4, p0, Lfzg;->j:Landroid/view/View;

    iget v5, p0, Lfzg;->x:I

    add-int/2addr v5, v1

    add-int v6, v0, v2

    invoke-virtual {v4, v1, v0, v5, v6}, Landroid/view/View;->layout(IIII)V

    .line 722
    invoke-direct {p0, v1}, Lfzg;->b(I)I

    move-result v1

    .line 723
    invoke-direct {p0, v0, v2}, Lfzg;->a(II)I

    move-result v0

    .line 726
    :cond_0
    iget-object v2, p0, Lfzg;->k:Landroid/view/View;

    if-eqz v2, :cond_1

    .line 727
    iget-object v2, p0, Lfzg;->k:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    .line 728
    iget-object v4, p0, Lfzg;->k:Landroid/view/View;

    iget v5, p0, Lfzg;->x:I

    add-int/2addr v5, v1

    add-int v6, v0, v2

    invoke-virtual {v4, v1, v0, v5, v6}, Landroid/view/View;->layout(IIII)V

    .line 729
    invoke-direct {p0, v1}, Lfzg;->b(I)I

    move-result v1

    .line 730
    invoke-direct {p0, v0, v2}, Lfzg;->a(II)I

    move-result v0

    .line 733
    :cond_1
    iget-object v2, p0, Lfzg;->l:Landroid/view/View;

    if-eqz v2, :cond_2

    .line 734
    iget-object v2, p0, Lfzg;->l:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    .line 735
    iget-object v4, p0, Lfzg;->l:Landroid/view/View;

    iget v5, p0, Lfzg;->x:I

    add-int/2addr v5, v1

    add-int v6, v0, v2

    invoke-virtual {v4, v1, v0, v5, v6}, Landroid/view/View;->layout(IIII)V

    .line 736
    invoke-direct {p0, v1}, Lfzg;->b(I)I

    move-result v1

    .line 737
    invoke-direct {p0, v0, v2}, Lfzg;->a(II)I

    move-result v0

    .line 740
    :cond_2
    iget-object v2, p0, Lfzg;->m:Ljava/util/ArrayList;

    if-eqz v2, :cond_4

    .line 741
    iget-object v2, p0, Lfzg;->m:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v4, v3

    move v2, v1

    move v1, v0

    .line 742
    :goto_0
    if-ge v4, v5, :cond_3

    .line 743
    iget-object v0, p0, Lfzg;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 744
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    .line 745
    iget v7, p0, Lfzg;->x:I

    add-int/2addr v7, v2

    add-int v8, v1, v6

    invoke-virtual {v0, v2, v1, v7, v8}, Landroid/view/View;->layout(IIII)V

    .line 747
    invoke-direct {p0, v2}, Lfzg;->b(I)I

    move-result v2

    .line 748
    invoke-direct {p0, v1, v6}, Lfzg;->a(II)I

    move-result v1

    .line 742
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    :cond_3
    move v0, v1

    move v1, v2

    .line 752
    :cond_4
    iget-object v2, p0, Lfzg;->n:Ljava/util/ArrayList;

    if-eqz v2, :cond_6

    .line 753
    iget-object v2, p0, Lfzg;->n:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v1

    move v1, v0

    .line 754
    :goto_1
    if-ge v3, v4, :cond_5

    .line 755
    iget-object v0, p0, Lfzg;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 756
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    .line 757
    iget v6, p0, Lfzg;->x:I

    add-int/2addr v6, v2

    add-int v7, v1, v5

    invoke-virtual {v0, v2, v1, v6, v7}, Landroid/view/View;->layout(IIII)V

    .line 758
    invoke-direct {p0, v2}, Lfzg;->b(I)I

    move-result v2

    .line 759
    invoke-direct {p0, v1, v5}, Lfzg;->a(II)I

    move-result v1

    .line 754
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_5
    move v0, v1

    move v1, v2

    .line 763
    :cond_6
    iget-object v2, p0, Lfzg;->o:Landroid/view/View;

    if-eqz v2, :cond_7

    .line 764
    iget-object v2, p0, Lfzg;->o:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    .line 765
    iget-object v3, p0, Lfzg;->o:Landroid/view/View;

    iget v4, p0, Lfzg;->x:I

    add-int/2addr v4, v1

    add-int v5, v0, v2

    invoke-virtual {v3, v1, v0, v4, v5}, Landroid/view/View;->layout(IIII)V

    .line 766
    invoke-direct {p0, v1}, Lfzg;->b(I)I

    move-result v1

    .line 767
    invoke-direct {p0, v0, v2}, Lfzg;->a(II)I

    move-result v0

    .line 770
    :cond_7
    iget-object v2, p0, Lfzg;->q:Landroid/view/View;

    if-eqz v2, :cond_8

    .line 771
    iget-object v2, p0, Lfzg;->q:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    .line 772
    iget-object v3, p0, Lfzg;->q:Landroid/view/View;

    iget v4, p0, Lfzg;->x:I

    add-int/2addr v4, v1

    add-int v5, v0, v2

    invoke-virtual {v3, v1, v0, v4, v5}, Landroid/view/View;->layout(IIII)V

    .line 774
    invoke-direct {p0, v1}, Lfzg;->b(I)I

    move-result v1

    .line 775
    invoke-direct {p0, v0, v2}, Lfzg;->a(II)I

    move-result v0

    .line 778
    :cond_8
    iget-object v2, p0, Lfzg;->p:Landroid/view/View;

    if-eqz v2, :cond_9

    .line 779
    iget-object v2, p0, Lfzg;->p:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    .line 780
    iget-object v3, p0, Lfzg;->p:Landroid/view/View;

    iget v4, p0, Lfzg;->x:I

    add-int/2addr v4, v1

    add-int v5, v0, v2

    invoke-virtual {v3, v1, v0, v4, v5}, Landroid/view/View;->layout(IIII)V

    .line 782
    invoke-direct {p0, v1}, Lfzg;->b(I)I

    move-result v1

    .line 783
    invoke-direct {p0, v0, v2}, Lfzg;->a(II)I

    move-result v0

    .line 786
    :cond_9
    iget-object v2, p0, Lfzg;->r:Landroid/view/View;

    if-eqz v2, :cond_a

    .line 787
    iget-object v2, p0, Lfzg;->r:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    .line 788
    iget-object v3, p0, Lfzg;->r:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    .line 789
    iget-object v4, p0, Lfzg;->r:Landroid/view/View;

    add-int/2addr v3, v1

    add-int v5, v0, v2

    invoke-virtual {v4, v1, v0, v3, v5}, Landroid/view/View;->layout(IIII)V

    .line 790
    invoke-direct {p0, v1}, Lfzg;->b(I)I

    .line 791
    invoke-direct {p0, v0, v2}, Lfzg;->a(II)I

    .line 793
    :cond_a
    return-void
.end method

.method protected onMeasure(II)V
    .locals 9

    .prologue
    const/high16 v8, 0x40000000    # 2.0f

    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 614
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 615
    invoke-virtual {p0}, Lfzg;->getPaddingLeft()I

    move-result v0

    sub-int v0, v3, v0

    invoke-virtual {p0}, Lfzg;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    .line 617
    invoke-virtual {p0}, Lfzg;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, Lfzg;->getPaddingBottom()I

    move-result v4

    add-int/2addr v1, v4

    iput v1, p0, Lfzg;->y:I

    .line 618
    iget-object v1, p0, Lfzg;->z:[I

    iget-object v4, p0, Lfzg;->z:[I

    iput v2, p0, Lfzg;->A:I

    aput v2, v4, v7

    aput v2, v1, v2

    .line 620
    iget v1, p0, Lfzg;->w:I

    if-ne v1, v7, :cond_3

    :goto_0
    iput v0, p0, Lfzg;->x:I

    .line 621
    iget v0, p0, Lfzg;->x:I

    invoke-static {v0, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 622
    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 624
    iget-object v0, p0, Lfzg;->j:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 625
    iget-object v0, p0, Lfzg;->j:Landroid/view/View;

    invoke-virtual {v0, v4, v5}, Landroid/view/View;->measure(II)V

    .line 626
    iget-object v0, p0, Lfzg;->j:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    invoke-direct {p0, v0, v2}, Lfzg;->a(IZ)V

    .line 629
    :cond_0
    iget-object v0, p0, Lfzg;->k:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 630
    iget-object v0, p0, Lfzg;->k:Landroid/view/View;

    invoke-virtual {v0, v4, v5}, Landroid/view/View;->measure(II)V

    .line 631
    iget-object v0, p0, Lfzg;->k:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    invoke-direct {p0, v0, v2}, Lfzg;->a(IZ)V

    .line 634
    :cond_1
    iget-object v0, p0, Lfzg;->l:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 635
    iget-object v0, p0, Lfzg;->l:Landroid/view/View;

    invoke-virtual {v0, v4, v5}, Landroid/view/View;->measure(II)V

    .line 636
    iget-object v0, p0, Lfzg;->l:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    invoke-direct {p0, v0, v2}, Lfzg;->a(IZ)V

    .line 639
    :cond_2
    iget-object v0, p0, Lfzg;->m:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    .line 640
    iget-object v0, p0, Lfzg;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v1, v2

    .line 641
    :goto_1
    if-ge v1, v6, :cond_4

    .line 642
    iget-object v0, p0, Lfzg;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 643
    invoke-virtual {v0, v4, v5}, Landroid/view/View;->measure(II)V

    .line 644
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    invoke-direct {p0, v0, v2}, Lfzg;->a(IZ)V

    .line 641
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 620
    :cond_3
    sget v1, Lfzg;->c:I

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 648
    :cond_4
    iget-object v0, p0, Lfzg;->n:Ljava/util/ArrayList;

    if-eqz v0, :cond_5

    .line 649
    iget-object v0, p0, Lfzg;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v1, v2

    .line 650
    :goto_2
    if-ge v1, v6, :cond_5

    .line 651
    iget-object v0, p0, Lfzg;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 652
    invoke-virtual {v0, v4, v5}, Landroid/view/View;->measure(II)V

    .line 653
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    invoke-direct {p0, v0, v2}, Lfzg;->a(IZ)V

    .line 650
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 657
    :cond_5
    iget-object v0, p0, Lfzg;->o:Landroid/view/View;

    if-eqz v0, :cond_6

    .line 658
    iget-object v0, p0, Lfzg;->o:Landroid/view/View;

    invoke-virtual {v0, v4, v5}, Landroid/view/View;->measure(II)V

    .line 659
    iget-object v0, p0, Lfzg;->o:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    invoke-direct {p0, v0, v2}, Lfzg;->a(IZ)V

    .line 662
    :cond_6
    iget-object v0, p0, Lfzg;->p:Landroid/view/View;

    if-eqz v0, :cond_7

    .line 663
    iget-object v0, p0, Lfzg;->p:Landroid/view/View;

    invoke-virtual {v0, v4, v5}, Landroid/view/View;->measure(II)V

    .line 664
    iget-object v0, p0, Lfzg;->p:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    invoke-direct {p0, v0, v2}, Lfzg;->a(IZ)V

    .line 667
    :cond_7
    iget-object v0, p0, Lfzg;->q:Landroid/view/View;

    if-eqz v0, :cond_8

    .line 668
    iget-object v0, p0, Lfzg;->q:Landroid/view/View;

    invoke-virtual {v0, v4, v5}, Landroid/view/View;->measure(II)V

    .line 669
    iget-object v0, p0, Lfzg;->q:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    invoke-direct {p0, v0, v2}, Lfzg;->a(IZ)V

    .line 672
    :cond_8
    iget-object v0, p0, Lfzg;->r:Landroid/view/View;

    if-eqz v0, :cond_9

    .line 673
    invoke-virtual {p0}, Lfzg;->getPaddingLeft()I

    move-result v0

    sub-int v0, v3, v0

    invoke-virtual {p0}, Lfzg;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    iput v0, p0, Lfzg;->x:I

    .line 674
    iget v0, p0, Lfzg;->x:I

    invoke-static {v0, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 675
    iget-object v1, p0, Lfzg;->r:Landroid/view/View;

    invoke-virtual {v1, v0, v5}, Landroid/view/View;->measure(II)V

    .line 676
    iget-object v0, p0, Lfzg;->r:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    invoke-direct {p0, v0, v2}, Lfzg;->a(IZ)V

    .line 679
    :cond_9
    invoke-direct {p0, v2, v7}, Lfzg;->a(IZ)V

    .line 681
    iget v0, p0, Lfzg;->y:I

    invoke-virtual {p0, v3, v0}, Lfzg;->setMeasuredDimension(II)V

    .line 682
    return-void
.end method
