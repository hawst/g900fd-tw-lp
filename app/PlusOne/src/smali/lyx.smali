.class public final Llyx;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lmrk;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 4048
    invoke-direct {p0}, Loxq;-><init>()V

    .line 4051
    iput-object v0, p0, Llyx;->apiHeader:Llyr;

    .line 4054
    iput-object v0, p0, Llyx;->a:Lmrk;

    .line 4048
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 4071
    const/4 v0, 0x0

    .line 4072
    iget-object v1, p0, Llyx;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 4073
    const/4 v0, 0x1

    iget-object v1, p0, Llyx;->apiHeader:Llyr;

    .line 4074
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 4076
    :cond_0
    iget-object v1, p0, Llyx;->a:Lmrk;

    if-eqz v1, :cond_1

    .line 4077
    const/4 v1, 0x2

    iget-object v2, p0, Llyx;->a:Lmrk;

    .line 4078
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4080
    :cond_1
    iget-object v1, p0, Llyx;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4081
    iput v0, p0, Llyx;->ai:I

    .line 4082
    return v0
.end method

.method public a(Loxn;)Llyx;
    .locals 2

    .prologue
    .line 4090
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 4091
    sparse-switch v0, :sswitch_data_0

    .line 4095
    iget-object v1, p0, Llyx;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 4096
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llyx;->ah:Ljava/util/List;

    .line 4099
    :cond_1
    iget-object v1, p0, Llyx;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4101
    :sswitch_0
    return-object p0

    .line 4106
    :sswitch_1
    iget-object v0, p0, Llyx;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 4107
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Llyx;->apiHeader:Llyr;

    .line 4109
    :cond_2
    iget-object v0, p0, Llyx;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 4113
    :sswitch_2
    iget-object v0, p0, Llyx;->a:Lmrk;

    if-nez v0, :cond_3

    .line 4114
    new-instance v0, Lmrk;

    invoke-direct {v0}, Lmrk;-><init>()V

    iput-object v0, p0, Llyx;->a:Lmrk;

    .line 4116
    :cond_3
    iget-object v0, p0, Llyx;->a:Lmrk;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 4091
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 4059
    iget-object v0, p0, Llyx;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 4060
    const/4 v0, 0x1

    iget-object v1, p0, Llyx;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 4062
    :cond_0
    iget-object v0, p0, Llyx;->a:Lmrk;

    if-eqz v0, :cond_1

    .line 4063
    const/4 v0, 0x2

    iget-object v1, p0, Llyx;->a:Lmrk;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 4065
    :cond_1
    iget-object v0, p0, Llyx;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 4067
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 4044
    invoke-virtual {p0, p1}, Llyx;->a(Loxn;)Llyx;

    move-result-object v0

    return-object v0
.end method
