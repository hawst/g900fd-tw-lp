.class final Letd;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private synthetic a:Landroid/view/View;

.field private synthetic b:Lesy;


# direct methods
.method constructor <init>(Lesy;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 433
    iput-object p1, p0, Letd;->b:Lesy;

    iput-object p2, p0, Letd;->a:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12

    .prologue
    const/4 v11, 0x0

    .line 436
    iget-object v6, p0, Letd;->b:Lesy;

    iget-object v5, p0, Letd;->a:Landroid/view/View;

    iget-boolean v0, v6, Lesy;->h:Z

    if-eqz v0, :cond_0

    invoke-virtual {v6}, Lesy;->a()V

    .line 437
    :goto_0
    return-void

    .line 436
    :cond_0
    sget-object v0, Lhmv;->dt:Lhmv;

    sget-object v1, Lhmw;->V:Lhmw;

    invoke-virtual {v6, v0, v1}, Lesy;->a(Lhmv;Lhmw;)V

    const v0, 0x7f1002b3

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f1002b4

    invoke-virtual {v5, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ViewFlipper;

    const v2, 0x7f1002ba

    invoke-virtual {v5, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/locations/LocationSharingRadioGroup;

    const v3, 0x7f1002a4

    invoke-virtual {v5, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    const v4, 0x7f1002a5

    invoke-virtual {v5, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    const v4, 0x7f1002a6

    invoke-virtual {v5, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    const v8, 0x7f1002b7

    invoke-virtual {v5, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ViewFlipper;

    iget-object v8, v6, Lesy;->c:Lnhl;

    iget-object v8, v8, Lnhl;->c:Ljava/lang/Boolean;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    if-eqz v8, :cond_3

    iget-object v8, v6, Lesy;->a:Leth;

    invoke-interface {v8}, Leth;->b()V

    :goto_1
    iget-object v8, v6, Lesy;->d:Lnjt;

    iget-object v8, v8, Lnjt;->e:Lnkd;

    iget-object v8, v8, Lnkd;->m:Lnik;

    iget-object v8, v8, Lnik;->a:[Lnij;

    invoke-static {v8}, Liuo;->a([Lnij;)Lnij;

    move-result-object v8

    iget v8, v8, Lnij;->b:I

    iget-object v9, v6, Lesy;->c:Lnhl;

    iget-object v9, v9, Lnhl;->c:Ljava/lang/Boolean;

    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    if-eqz v9, :cond_1

    const/4 v9, 0x2

    if-ne v8, v9, :cond_2

    :cond_1
    const/4 v8, 0x4

    invoke-virtual {v5, v8}, Landroid/widget/ViewFlipper;->setVisibility(I)V

    :cond_2
    invoke-static {v5}, Leud;->a(Landroid/widget/ViewFlipper;)V

    iget-object v5, v6, Lesy;->b:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v8, 0x7f0a0aa6

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    iget-object v10, v6, Lesy;->d:Lnjt;

    iget-object v10, v10, Lnjt;->e:Lnkd;

    iget-object v10, v10, Lnkd;->a:Lnjb;

    iget-object v10, v10, Lnjb;->a:Ljava/lang/String;

    aput-object v10, v9, v11

    invoke-virtual {v5, v8, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v2, v11}, Lcom/google/android/apps/plus/locations/LocationSharingRadioGroup;->setVisibility(I)V

    invoke-static {v1}, Leud;->a(Landroid/view/View;)V

    invoke-virtual {v3, v11}, Landroid/widget/Button;->setVisibility(I)V

    invoke-virtual {v7, v11}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0a03f4

    invoke-virtual {v4, v0}, Landroid/widget/Button;->setText(I)V

    new-instance v0, Lete;

    invoke-direct {v0, v6, v2}, Lete;-><init>(Lesy;Lcom/google/android/apps/plus/locations/LocationSharingRadioGroup;)V

    invoke-virtual {v4, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    :cond_3
    iget-object v8, v6, Lesy;->b:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0b0316

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    invoke-virtual {v1, v8}, Landroid/widget/ViewFlipper;->setBackgroundColor(I)V

    goto :goto_1
.end method
