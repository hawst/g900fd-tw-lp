.class public final Lohw;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/Boolean;

.field private e:I

.field private f:Ljava/lang/Integer;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/Double;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/Integer;

.field private m:Ljava/lang/Boolean;

.field private n:Ljava/lang/Boolean;

.field private o:Ljava/lang/Integer;

.field private p:Ljava/lang/Boolean;

.field private q:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3235
    invoke-direct {p0}, Loxq;-><init>()V

    .line 3238
    const/high16 v0, -0x80000000

    iput v0, p0, Lohw;->e:I

    .line 3235
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 3332
    const/4 v0, 0x0

    .line 3333
    iget-object v1, p0, Lohw;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3334
    const/4 v0, 0x1

    iget-object v1, p0, Lohw;->a:Ljava/lang/String;

    .line 3335
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3337
    :cond_0
    iget-object v1, p0, Lohw;->g:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 3338
    const/4 v1, 0x2

    iget-object v2, p0, Lohw;->g:Ljava/lang/String;

    .line 3339
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3341
    :cond_1
    iget-object v1, p0, Lohw;->h:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 3342
    const/4 v1, 0x3

    iget-object v2, p0, Lohw;->h:Ljava/lang/String;

    .line 3343
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3345
    :cond_2
    iget-object v1, p0, Lohw;->i:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 3346
    const/4 v1, 0x4

    iget-object v2, p0, Lohw;->i:Ljava/lang/String;

    .line 3347
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3349
    :cond_3
    iget-object v1, p0, Lohw;->j:Ljava/lang/Double;

    if-eqz v1, :cond_4

    .line 3350
    const/4 v1, 0x5

    iget-object v2, p0, Lohw;->j:Ljava/lang/Double;

    .line 3351
    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 3353
    :cond_4
    iget-object v1, p0, Lohw;->k:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 3354
    const/4 v1, 0x6

    iget-object v2, p0, Lohw;->k:Ljava/lang/String;

    .line 3355
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3357
    :cond_5
    iget-object v1, p0, Lohw;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 3358
    const/4 v1, 0x7

    iget-object v2, p0, Lohw;->b:Ljava/lang/Integer;

    .line 3359
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3361
    :cond_6
    iget-object v1, p0, Lohw;->l:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    .line 3362
    const/16 v1, 0x8

    iget-object v2, p0, Lohw;->l:Ljava/lang/Integer;

    .line 3363
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3365
    :cond_7
    iget-object v1, p0, Lohw;->m:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    .line 3366
    const/16 v1, 0x9

    iget-object v2, p0, Lohw;->m:Ljava/lang/Boolean;

    .line 3367
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3369
    :cond_8
    iget v1, p0, Lohw;->e:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_9

    .line 3370
    const/16 v1, 0xa

    iget v2, p0, Lohw;->e:I

    .line 3371
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3373
    :cond_9
    iget-object v1, p0, Lohw;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_a

    .line 3374
    const/16 v1, 0xb

    iget-object v2, p0, Lohw;->f:Ljava/lang/Integer;

    .line 3375
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3377
    :cond_a
    iget-object v1, p0, Lohw;->n:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    .line 3378
    const/16 v1, 0xc

    iget-object v2, p0, Lohw;->n:Ljava/lang/Boolean;

    .line 3379
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3381
    :cond_b
    iget-object v1, p0, Lohw;->c:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 3382
    const/16 v1, 0xd

    iget-object v2, p0, Lohw;->c:Ljava/lang/String;

    .line 3383
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3385
    :cond_c
    iget-object v1, p0, Lohw;->o:Ljava/lang/Integer;

    if-eqz v1, :cond_d

    .line 3386
    const/16 v1, 0xe

    iget-object v2, p0, Lohw;->o:Ljava/lang/Integer;

    .line 3387
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3389
    :cond_d
    iget-object v1, p0, Lohw;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_e

    .line 3390
    const/16 v1, 0xf

    iget-object v2, p0, Lohw;->d:Ljava/lang/Boolean;

    .line 3391
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3393
    :cond_e
    iget-object v1, p0, Lohw;->p:Ljava/lang/Boolean;

    if-eqz v1, :cond_f

    .line 3394
    const/16 v1, 0x10

    iget-object v2, p0, Lohw;->p:Ljava/lang/Boolean;

    .line 3395
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3397
    :cond_f
    iget-object v1, p0, Lohw;->q:Ljava/lang/String;

    if-eqz v1, :cond_10

    .line 3398
    const/16 v1, 0x11

    iget-object v2, p0, Lohw;->q:Ljava/lang/String;

    .line 3399
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3401
    :cond_10
    iget-object v1, p0, Lohw;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3402
    iput v0, p0, Lohw;->ai:I

    .line 3403
    return v0
.end method

.method public a(Loxn;)Lohw;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 3411
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3412
    sparse-switch v0, :sswitch_data_0

    .line 3416
    iget-object v1, p0, Lohw;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 3417
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lohw;->ah:Ljava/util/List;

    .line 3420
    :cond_1
    iget-object v1, p0, Lohw;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3422
    :sswitch_0
    return-object p0

    .line 3427
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lohw;->a:Ljava/lang/String;

    goto :goto_0

    .line 3431
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lohw;->g:Ljava/lang/String;

    goto :goto_0

    .line 3435
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lohw;->h:Ljava/lang/String;

    goto :goto_0

    .line 3439
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lohw;->i:Ljava/lang/String;

    goto :goto_0

    .line 3443
    :sswitch_5
    invoke-virtual {p1}, Loxn;->c()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lohw;->j:Ljava/lang/Double;

    goto :goto_0

    .line 3447
    :sswitch_6
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lohw;->k:Ljava/lang/String;

    goto :goto_0

    .line 3451
    :sswitch_7
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lohw;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 3455
    :sswitch_8
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lohw;->l:Ljava/lang/Integer;

    goto :goto_0

    .line 3459
    :sswitch_9
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lohw;->m:Ljava/lang/Boolean;

    goto :goto_0

    .line 3463
    :sswitch_a
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 3464
    if-eq v0, v2, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 3467
    :cond_2
    iput v0, p0, Lohw;->e:I

    goto :goto_0

    .line 3469
    :cond_3
    iput v2, p0, Lohw;->e:I

    goto :goto_0

    .line 3474
    :sswitch_b
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lohw;->f:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 3478
    :sswitch_c
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lohw;->n:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 3482
    :sswitch_d
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lohw;->c:Ljava/lang/String;

    goto/16 :goto_0

    .line 3486
    :sswitch_e
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lohw;->o:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 3490
    :sswitch_f
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lohw;->d:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 3494
    :sswitch_10
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lohw;->p:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 3498
    :sswitch_11
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lohw;->q:Ljava/lang/String;

    goto/16 :goto_0

    .line 3412
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x29 -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x6a -> :sswitch_d
        0x70 -> :sswitch_e
        0x78 -> :sswitch_f
        0x80 -> :sswitch_10
        0x8a -> :sswitch_11
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 3275
    iget-object v0, p0, Lohw;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 3276
    const/4 v0, 0x1

    iget-object v1, p0, Lohw;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3278
    :cond_0
    iget-object v0, p0, Lohw;->g:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 3279
    const/4 v0, 0x2

    iget-object v1, p0, Lohw;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3281
    :cond_1
    iget-object v0, p0, Lohw;->h:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 3282
    const/4 v0, 0x3

    iget-object v1, p0, Lohw;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3284
    :cond_2
    iget-object v0, p0, Lohw;->i:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 3285
    const/4 v0, 0x4

    iget-object v1, p0, Lohw;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3287
    :cond_3
    iget-object v0, p0, Lohw;->j:Ljava/lang/Double;

    if-eqz v0, :cond_4

    .line 3288
    const/4 v0, 0x5

    iget-object v1, p0, Lohw;->j:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(ID)V

    .line 3290
    :cond_4
    iget-object v0, p0, Lohw;->k:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 3291
    const/4 v0, 0x6

    iget-object v1, p0, Lohw;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3293
    :cond_5
    iget-object v0, p0, Lohw;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 3294
    const/4 v0, 0x7

    iget-object v1, p0, Lohw;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 3296
    :cond_6
    iget-object v0, p0, Lohw;->l:Ljava/lang/Integer;

    if-eqz v0, :cond_7

    .line 3297
    const/16 v0, 0x8

    iget-object v1, p0, Lohw;->l:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 3299
    :cond_7
    iget-object v0, p0, Lohw;->m:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    .line 3300
    const/16 v0, 0x9

    iget-object v1, p0, Lohw;->m:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 3302
    :cond_8
    iget v0, p0, Lohw;->e:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_9

    .line 3303
    const/16 v0, 0xa

    iget v1, p0, Lohw;->e:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 3305
    :cond_9
    iget-object v0, p0, Lohw;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_a

    .line 3306
    const/16 v0, 0xb

    iget-object v1, p0, Lohw;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 3308
    :cond_a
    iget-object v0, p0, Lohw;->n:Ljava/lang/Boolean;

    if-eqz v0, :cond_b

    .line 3309
    const/16 v0, 0xc

    iget-object v1, p0, Lohw;->n:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 3311
    :cond_b
    iget-object v0, p0, Lohw;->c:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 3312
    const/16 v0, 0xd

    iget-object v1, p0, Lohw;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3314
    :cond_c
    iget-object v0, p0, Lohw;->o:Ljava/lang/Integer;

    if-eqz v0, :cond_d

    .line 3315
    const/16 v0, 0xe

    iget-object v1, p0, Lohw;->o:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 3317
    :cond_d
    iget-object v0, p0, Lohw;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_e

    .line 3318
    const/16 v0, 0xf

    iget-object v1, p0, Lohw;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 3320
    :cond_e
    iget-object v0, p0, Lohw;->p:Ljava/lang/Boolean;

    if-eqz v0, :cond_f

    .line 3321
    const/16 v0, 0x10

    iget-object v1, p0, Lohw;->p:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 3323
    :cond_f
    iget-object v0, p0, Lohw;->q:Ljava/lang/String;

    if-eqz v0, :cond_10

    .line 3324
    const/16 v0, 0x11

    iget-object v1, p0, Lohw;->q:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3326
    :cond_10
    iget-object v0, p0, Lohw;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3328
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3231
    invoke-virtual {p0, p1}, Lohw;->a(Loxn;)Lohw;

    move-result-object v0

    return-object v0
.end method
