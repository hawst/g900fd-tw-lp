.class public Ldzc;
.super Ldyj;
.source "PG"

# interfaces
.implements Lbc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldyj;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# static fields
.field private static final R:[Ljava/lang/String;

.field private static S:[Ljava/lang/String;

.field private static T:[I


# instance fields
.field private U:Ljava/lang/String;

.field private V:Landroid/database/Cursor;

.field private final W:Ldze;

.field private final X:Landroid/database/DataSetObserver;

.field private Y:Lhxh;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 35
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "circle_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "circle_name"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "for_sharing"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "volume"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "notifications_enabled"

    aput-object v2, v0, v1

    sput-object v0, Ldzc;->R:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ldyj;-><init>()V

    .line 54
    new-instance v0, Ldze;

    invoke-direct {v0, p0}, Ldze;-><init>(Ldzc;)V

    iput-object v0, p0, Ldzc;->W:Ldze;

    .line 58
    new-instance v0, Ldzd;

    invoke-direct {v0, p0}, Ldzd;-><init>(Ldzc;)V

    iput-object v0, p0, Ldzc;->X:Landroid/database/DataSetObserver;

    .line 68
    return-void
.end method

.method static synthetic a(Ldzc;)Ldze;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Ldzc;->W:Ldze;

    return-object v0
.end method

.method static synthetic a(Ldzc;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 27
    iput-object p1, p0, Ldzc;->V:Landroid/database/Cursor;

    return-void
.end method


# virtual methods
.method protected Z()I
    .locals 1

    .prologue
    .line 183
    const/4 v0, 0x1

    return v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 164
    invoke-super {p0, p1, p2, p3}, Ldyj;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 167
    const v0, 0x7f100207

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 168
    const v2, 0x7f0a070a

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 170
    return-object v1
.end method

.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 87
    packed-switch p1, :pswitch_data_0

    .line 94
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 89
    :pswitch_0
    new-instance v0, Ldzf;

    invoke-virtual {p0}, Ldzc;->n()Lz;

    move-result-object v1

    iget v2, p0, Ldzc;->N:I

    iget-object v3, p0, Ldzc;->U:Ljava/lang/String;

    sget-object v4, Ldzc;->R:[Ljava/lang/String;

    .line 90
    invoke-virtual {p0}, Ldzc;->Z()I

    move-result v5

    invoke-direct/range {v0 .. v5}, Ldzf;-><init>(Landroid/content/Context;ILjava/lang/String;[Ljava/lang/String;I)V

    goto :goto_0

    .line 87
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x1

    .line 130
    invoke-super {p0, p1}, Ldyj;->a(Landroid/os/Bundle;)V

    .line 132
    sget-object v0, Ldzc;->S:[Ljava/lang/String;

    if-nez v0, :cond_0

    .line 133
    new-array v0, v4, [Ljava/lang/String;

    const/4 v1, 0x0

    const v2, 0x7f0a0705

    .line 134
    invoke-virtual {p0, v2}, Ldzc;->e_(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const v1, 0x7f0a0706

    .line 135
    invoke-virtual {p0, v1}, Ldzc;->e_(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    const/4 v1, 0x2

    const v2, 0x7f0a0707

    .line 136
    invoke-virtual {p0, v2}, Ldzc;->e_(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const v2, 0x7f0a0708

    .line 137
    invoke-virtual {p0, v2}, Ldzc;->e_(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    sput-object v0, Ldzc;->S:[Ljava/lang/String;

    .line 139
    new-array v0, v4, [I

    fill-array-data v0, :array_0

    sput-object v0, Ldzc;->T:[I

    .line 147
    :cond_0
    sget-object v0, Ldzc;->S:[Ljava/lang/String;

    iput-object v0, p0, Ldzc;->P:[Ljava/lang/String;

    .line 148
    sget-object v0, Ldzc;->T:[I

    iput-object v0, p0, Ldzc;->Q:[I

    .line 150
    invoke-virtual {p0}, Ldzc;->k()Landroid/os/Bundle;

    move-result-object v0

    .line 151
    const-string v1, "circle_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldzc;->U:Ljava/lang/String;

    .line 153
    invoke-virtual {p0}, Ldzc;->w()Lbb;

    move-result-object v0

    .line 154
    const/4 v1, 0x0

    invoke-virtual {v0, v3, v1, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 156
    new-instance v1, Lhxh;

    invoke-virtual {p0}, Ldzc;->n()Lz;

    move-result-object v2

    iget v3, p0, Ldzc;->N:I

    const/16 v4, 0x10

    invoke-direct {v1, v2, v0, v3, v4}, Lhxh;-><init>(Landroid/content/Context;Lbb;II)V

    iput-object v1, p0, Ldzc;->Y:Lhxh;

    .line 157
    iget-object v0, p0, Ldzc;->Y:Lhxh;

    iget-object v1, p0, Ldzc;->X:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lhxh;->a(Landroid/database/DataSetObserver;)V

    .line 158
    iget-object v0, p0, Ldzc;->Y:Lhxh;

    invoke-virtual {v0}, Lhxh;->b()V

    .line 159
    return-void

    .line 139
    nop

    :array_0
    .array-data 4
        0x3
        0x2
        0x1
        0x0
    .end array-data
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 125
    return-void
.end method

.method public a(Ldo;Landroid/database/Cursor;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 100
    invoke-virtual {p1}, Ldo;->o()I

    move-result v0

    .line 102
    packed-switch v0, :pswitch_data_0

    .line 117
    :goto_0
    return-void

    .line 104
    :pswitch_0
    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ldzc;->U:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 105
    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    .line 106
    :cond_0
    invoke-virtual {p0, v5}, Ldzc;->d(I)V

    goto :goto_0

    .line 109
    :cond_1
    invoke-interface {p2, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 110
    invoke-interface {p2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 111
    const/4 v0, 0x3

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 112
    const/4 v0, 0x4

    .line 113
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_2

    move v4, v5

    .line 114
    :goto_1
    const/4 v0, 0x2

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_3

    move v7, v5

    .line 116
    :goto_2
    new-instance v0, Ldyq;

    if-nez v7, :cond_4

    :goto_3
    invoke-direct/range {v0 .. v5}, Ldyq;-><init>(Ljava/lang/String;Ljava/lang/String;IZZ)V

    invoke-virtual {p0, v0}, Ldzc;->a(Ldyq;)V

    goto :goto_0

    :cond_2
    move v4, v6

    .line 113
    goto :goto_1

    :cond_3
    move v7, v6

    .line 114
    goto :goto_2

    :cond_4
    move v5, v6

    .line 116
    goto :goto_3

    .line 102
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 27
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Ldzc;->a(Ldo;Landroid/database/Cursor;)V

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 207
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 239
    :goto_0
    return-void

    .line 211
    :cond_0
    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 213
    iget-object v3, p0, Ldzc;->V:Landroid/database/Cursor;

    if-eqz v3, :cond_4

    .line 215
    iget-object v3, p0, Ldzc;->V:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 217
    :cond_1
    iget-object v3, p0, Ldzc;->V:Landroid/database/Cursor;

    invoke-interface {v3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 219
    iget-object v4, p0, Ldzc;->V:Landroid/database/Cursor;

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 221
    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 222
    invoke-static {p1, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 230
    :goto_1
    if-eqz v0, :cond_4

    .line 231
    invoke-virtual {p0}, Ldzc;->n()Lz;

    move-result-object v0

    const v2, 0x7f0a08a0

    invoke-static {v0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 232
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 227
    :cond_2
    iget-object v3, p0, Ldzc;->V:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_1

    :cond_3
    move v0, v1

    goto :goto_1

    .line 237
    :cond_4
    iget-object v0, p0, Ldzc;->O:Ldyq;

    iput-object v2, v0, Ldyq;->b:Ljava/lang/String;

    .line 238
    invoke-virtual {p0}, Ldzc;->W()V

    goto :goto_0
.end method

.method public aO_()V
    .locals 4

    .prologue
    .line 175
    invoke-super {p0}, Ldyj;->aO_()V

    .line 177
    invoke-virtual {p0}, Ldzc;->w()Lbb;

    move-result-object v0

    .line 178
    const/4 v1, 0x2

    const/4 v2, 0x0

    iget-object v3, p0, Ldzc;->W:Ldze;

    invoke-virtual {v0, v1, v2, v3}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 179
    return-void
.end method

.method protected aa()Ljava/lang/String;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Ldzc;->U:Ljava/lang/String;

    return-object v0
.end method

.method protected ac()I
    .locals 1

    .prologue
    .line 193
    const/4 v0, 0x1

    return v0
.end method

.method protected ad()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 198
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 199
    const-string v1, "extra_circle_id"

    .line 200
    invoke-virtual {p0}, Ldzc;->aa()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lhmt;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 201
    const-string v2, "extra_start_view_extras"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 202
    return-object v0
.end method
