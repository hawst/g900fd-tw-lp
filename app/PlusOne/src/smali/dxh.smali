.class public final Ldxh;
.super Lt;
.source "PG"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private N:Ljava/lang/String;

.field private O:Z

.field private P:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lt;-><init>()V

    .line 27
    return-void
.end method

.method public static a(Ljava/lang/String;ZLu;)Ldxh;
    .locals 2

    .prologue
    .line 39
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 40
    const-string v1, "album_url"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    const-string v1, "is_public"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 43
    new-instance v1, Ldxh;

    invoke-direct {v1}, Ldxh;-><init>()V

    .line 44
    invoke-virtual {v1, v0}, Ldxh;->f(Landroid/os/Bundle;)V

    .line 45
    const/4 v0, 0x0

    invoke-virtual {v1, p2, v0}, Ldxh;->a(Lu;I)V

    .line 46
    return-object v1
.end method

.method static synthetic a(Ldxh;)V
    .locals 3

    .prologue
    .line 20
    invoke-virtual {p0}, Ldxh;->n()Lz;

    move-result-object v0

    const-string v1, "clipboard"

    invoke-virtual {v0, v1}, Lz;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/ClipboardManager;

    iget-object v1, p0, Ldxh;->N:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/text/ClipboardManager;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Ldxh;->n()Lz;

    move-result-object v0

    const v1, 0x7f0a0aee

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 3

    .prologue
    .line 81
    const v0, 0x7f040041

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 83
    const v0, 0x7f100189

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ldxh;->P:Landroid/widget/TextView;

    .line 84
    iget-object v0, p0, Ldxh;->P:Landroid/widget/TextView;

    iget-object v2, p0, Ldxh;->N:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 85
    iget-object v0, p0, Ldxh;->P:Landroid/widget/TextView;

    new-instance v2, Ldxi;

    invoke-direct {v2, p0}, Ldxi;-><init>(Ldxh;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 92
    return-object v1
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 51
    invoke-super {p0, p1}, Lt;->a(Landroid/os/Bundle;)V

    .line 53
    invoke-virtual {p0}, Ldxh;->k()Landroid/os/Bundle;

    move-result-object v0

    .line 54
    const-string v1, "album_url"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Ldxh;->N:Ljava/lang/String;

    .line 55
    const-string v1, "is_public"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Ldxh;->O:Z

    .line 56
    return-void
.end method

.method public c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 60
    invoke-virtual {p0}, Ldxh;->n()Lz;

    move-result-object v0

    .line 62
    invoke-virtual {p0}, Ldxh;->n()Lz;

    move-result-object v1

    invoke-virtual {v1}, Lz;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    .line 63
    invoke-virtual {p0, v1}, Ldxh;->a(Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v1

    .line 65
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v0, 0x7f0a099a

    .line 67
    invoke-virtual {p0, v0}, Ldxh;->e_(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v2, 0x0

    .line 68
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 69
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a0596

    .line 70
    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 72
    iget-boolean v1, p0, Ldxh;->O:Z

    if-nez v1, :cond_0

    .line 74
    const v1, 0x7f0a099d

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 76
    :cond_0
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1

    .prologue
    .line 109
    const/4 v0, -0x2

    if-ne p2, v0, :cond_0

    .line 110
    invoke-virtual {p0}, Ldxh;->u_()Lu;

    move-result-object v0

    instance-of v0, v0, Ldxj;

    if-eqz v0, :cond_0

    .line 111
    invoke-virtual {p0}, Ldxh;->u_()Lu;

    move-result-object v0

    check-cast v0, Ldxj;

    .line 112
    invoke-interface {v0}, Ldxj;->a()V

    .line 114
    :cond_0
    return-void
.end method
