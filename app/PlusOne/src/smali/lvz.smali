.class public final Llvz;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:I

.field private c:[I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 21
    const/high16 v0, -0x80000000

    iput v0, p0, Llvz;->b:I

    .line 24
    sget-object v0, Loxx;->a:[I

    iput-object v0, p0, Llvz;->c:[I

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 46
    .line 47
    iget-object v0, p0, Llvz;->a:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 48
    const/4 v0, 0x1

    iget-object v2, p0, Llvz;->a:Ljava/lang/String;

    .line 49
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 51
    :goto_0
    iget v2, p0, Llvz;->b:I

    const/high16 v3, -0x80000000

    if-eq v2, v3, :cond_0

    .line 52
    const/4 v2, 0x2

    iget v3, p0, Llvz;->b:I

    .line 53
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 55
    :cond_0
    iget-object v2, p0, Llvz;->c:[I

    if-eqz v2, :cond_2

    iget-object v2, p0, Llvz;->c:[I

    array-length v2, v2

    if-lez v2, :cond_2

    .line 57
    iget-object v3, p0, Llvz;->c:[I

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_1

    aget v5, v3, v1

    .line 59
    invoke-static {v5}, Loxo;->i(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 57
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 61
    :cond_1
    add-int/2addr v0, v2

    .line 62
    iget-object v1, p0, Llvz;->c:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 64
    :cond_2
    iget-object v1, p0, Llvz;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 65
    iput v0, p0, Llvz;->ai:I

    .line 66
    return v0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Llvz;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 74
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 75
    sparse-switch v0, :sswitch_data_0

    .line 79
    iget-object v1, p0, Llvz;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 80
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llvz;->ah:Ljava/util/List;

    .line 83
    :cond_1
    iget-object v1, p0, Llvz;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 85
    :sswitch_0
    return-object p0

    .line 90
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llvz;->a:Ljava/lang/String;

    goto :goto_0

    .line 94
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 95
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 98
    :cond_2
    iput v0, p0, Llvz;->b:I

    goto :goto_0

    .line 100
    :cond_3
    iput v3, p0, Llvz;->b:I

    goto :goto_0

    .line 105
    :sswitch_3
    const/16 v0, 0x18

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 106
    iget-object v0, p0, Llvz;->c:[I

    array-length v0, v0

    .line 107
    add-int/2addr v1, v0

    new-array v1, v1, [I

    .line 108
    iget-object v2, p0, Llvz;->c:[I

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 109
    iput-object v1, p0, Llvz;->c:[I

    .line 110
    :goto_1
    iget-object v1, p0, Llvz;->c:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_4

    .line 111
    iget-object v1, p0, Llvz;->c:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    .line 112
    invoke-virtual {p1}, Loxn;->a()I

    .line 110
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 115
    :cond_4
    iget-object v1, p0, Llvz;->c:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    goto :goto_0

    .line 75
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 29
    iget-object v0, p0, Llvz;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 30
    const/4 v0, 0x1

    iget-object v1, p0, Llvz;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 32
    :cond_0
    iget v0, p0, Llvz;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 33
    const/4 v0, 0x2

    iget v1, p0, Llvz;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 35
    :cond_1
    iget-object v0, p0, Llvz;->c:[I

    if-eqz v0, :cond_2

    iget-object v0, p0, Llvz;->c:[I

    array-length v0, v0

    if-lez v0, :cond_2

    .line 36
    iget-object v1, p0, Llvz;->c:[I

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget v3, v1, v0

    .line 37
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->a(II)V

    .line 36
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 40
    :cond_2
    iget-object v0, p0, Llvz;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 42
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Llvz;->a(Loxn;)Llvz;

    move-result-object v0

    return-object v0
.end method
