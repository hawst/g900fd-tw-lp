.class public final Lnoc;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnoc;


# instance fields
.field public b:I

.field public c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 600
    const/4 v0, 0x0

    new-array v0, v0, [Lnoc;

    sput-object v0, Lnoc;->a:[Lnoc;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 601
    invoke-direct {p0}, Loxq;-><init>()V

    .line 604
    const/high16 v0, -0x80000000

    iput v0, p0, Lnoc;->b:I

    .line 601
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 623
    const/4 v0, 0x0

    .line 624
    iget v1, p0, Lnoc;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 625
    const/4 v0, 0x1

    iget v1, p0, Lnoc;->b:I

    .line 626
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 628
    :cond_0
    iget-object v1, p0, Lnoc;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 629
    const/4 v1, 0x2

    iget-object v2, p0, Lnoc;->c:Ljava/lang/String;

    .line 630
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 632
    :cond_1
    iget-object v1, p0, Lnoc;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 633
    iput v0, p0, Lnoc;->ai:I

    .line 634
    return v0
.end method

.method public a(Loxn;)Lnoc;
    .locals 2

    .prologue
    .line 642
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 643
    sparse-switch v0, :sswitch_data_0

    .line 647
    iget-object v1, p0, Lnoc;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 648
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnoc;->ah:Ljava/util/List;

    .line 651
    :cond_1
    iget-object v1, p0, Lnoc;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 653
    :sswitch_0
    return-object p0

    .line 658
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 659
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 662
    :cond_2
    iput v0, p0, Lnoc;->b:I

    goto :goto_0

    .line 664
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lnoc;->b:I

    goto :goto_0

    .line 669
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnoc;->c:Ljava/lang/String;

    goto :goto_0

    .line 643
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 611
    iget v0, p0, Lnoc;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 612
    const/4 v0, 0x1

    iget v1, p0, Lnoc;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 614
    :cond_0
    iget-object v0, p0, Lnoc;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 615
    const/4 v0, 0x2

    iget-object v1, p0, Lnoc;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 617
    :cond_1
    iget-object v0, p0, Lnoc;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 619
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 597
    invoke-virtual {p0, p1}, Lnoc;->a(Loxn;)Lnoc;

    move-result-object v0

    return-object v0
.end method
