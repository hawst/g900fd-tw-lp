.class public final Lebj;
.super Llol;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lbc;
.implements Lfuf;
.implements Lhjj;
.implements Lhmq;
.implements Llgs;
.implements Llha;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Llol;",
        "Landroid/view/View$OnClickListener;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lfuf;",
        "Lhjj;",
        "Lhmq;",
        "Llgs;",
        "Llha;"
    }
.end annotation


# instance fields
.field private final N:Lhje;

.field private O:Lhee;

.field private final P:Licq;

.field private Q:Landroid/widget/ListView;

.field private R:Leuu;

.field private S:Ljava/lang/Integer;

.field private T:Ljava/lang/Integer;

.field private U:Ljava/lang/String;

.field private V:Lduo;

.field private final W:Lhob;

.field private final X:Lfhh;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 77
    invoke-direct {p0}, Llol;-><init>()V

    .line 98
    new-instance v0, Lhje;

    iget-object v1, p0, Lebj;->av:Llqm;

    invoke-direct {v0, p0, v1, p0}, Lhje;-><init>(Lu;Llqr;Lhjj;)V

    iput-object v0, p0, Lebj;->N:Lhje;

    .line 103
    new-instance v0, Licq;

    iget-object v1, p0, Lebj;->av:Llqm;

    invoke-direct {v0, v1}, Licq;-><init>(Llqr;)V

    const v1, 0x7f0a064c

    .line 104
    invoke-virtual {v0, v1}, Licq;->b(I)Licq;

    move-result-object v0

    iput-object v0, p0, Lebj;->P:Licq;

    .line 116
    new-instance v0, Lebk;

    invoke-direct {v0, p0}, Lebk;-><init>(Lebj;)V

    iput-object v0, p0, Lebj;->W:Lhob;

    .line 126
    new-instance v0, Lebl;

    invoke-direct {v0, p0}, Lebl;-><init>(Lebj;)V

    iput-object v0, p0, Lebj;->X:Lfhh;

    .line 563
    return-void
.end method

.method static synthetic a(Lebj;)Lhee;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lebj;->O:Lhee;

    return-object v0
.end method

.method private a(ILfib;)V
    .locals 3

    .prologue
    .line 291
    iget-object v0, p0, Lebj;->S:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lebj;->S:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_1

    .line 303
    :cond_0
    :goto_0
    return-void

    .line 295
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lebj;->S:Ljava/lang/Integer;

    .line 297
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lfib;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 298
    invoke-virtual {p0}, Lebj;->o()Landroid/content/res/Resources;

    move-result-object v0

    .line 299
    const v1, 0x7f0a07ac

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 300
    invoke-virtual {p0}, Lebj;->n()Lz;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 301
    invoke-virtual {p0}, Lebj;->x()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lebj;->c(Landroid/view/View;)V

    goto :goto_0
.end method

.method static synthetic a(Lebj;ILfib;)V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0, p1, p2}, Lebj;->a(ILfib;)V

    return-void
.end method

.method static synthetic a(Lebj;Lhoz;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 77
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lhoz;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iput-object v4, p0, Lebj;->U:Ljava/lang/String;

    invoke-virtual {p0}, Lebj;->n()Lz;

    move-result-object v0

    const v1, 0x7f0a0592

    invoke-static {v0, v1, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lebj;->n()Lz;

    move-result-object v1

    iget-object v0, p0, Lebj;->U:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v4, p0, Lebj;->U:Ljava/lang/String;

    invoke-virtual {p0}, Lebj;->a()V

    :goto_1
    const v0, 0x7f0a07bb

    invoke-static {v1, v0, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    const/4 v0, -0x1

    invoke-virtual {v1, v0}, Landroid/app/Activity;->setResult(I)V

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "num_photos_added"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    new-instance v2, Lebm;

    invoke-direct {v2, p0, v0}, Lebm;-><init>(Lebj;I)V

    invoke-static {v2}, Landroid/os/AsyncTask;->execute(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lebj;->au:Llnh;

    const-class v2, Lhei;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iget-object v2, p0, Lebj;->O:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    invoke-interface {v0, v2}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v2, "gaia_id"

    invoke-interface {v0, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lebj;->O:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    iget-object v3, p0, Lebj;->U:Ljava/lang/String;

    invoke-static {v1, v2, v0, v3, v4}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lebj;->T:Ljava/lang/Integer;

    goto :goto_1
.end method

.method private a(Ljtx;)Z
    .locals 2

    .prologue
    .line 461
    iget-object v0, p0, Lebj;->au:Llnh;

    const-class v1, Lhoc;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    .line 462
    const-string v1, "CopyPhotosToAlbumTask"

    invoke-virtual {v0, v1}, Lhoc;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 463
    invoke-virtual {v0, p1}, Lhoc;->c(Lhny;)V

    .line 464
    const/4 v0, 0x1

    .line 466
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lebj;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lebj;->U:Ljava/lang/String;

    return-object v0
.end method

.method private b(ILfib;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 433
    iget-object v0, p0, Lebj;->T:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lebj;->T:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_1

    .line 451
    :cond_0
    :goto_0
    return-void

    .line 436
    :cond_1
    invoke-virtual {p2}, Lfib;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 437
    const-string v0, "AddToAlbumFragment"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 438
    const-string v0, "AddToAlbumFragment"

    const-string v1, "Error getting album tiles."

    invoke-virtual {p2}, Lfib;->d()Ljava/lang/Exception;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 442
    :cond_2
    invoke-virtual {p0}, Lebj;->n()Lz;

    move-result-object v0

    const v1, 0x7f0a07bb

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 443
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 445
    iput-object v3, p0, Lebj;->T:Ljava/lang/Integer;

    .line 446
    iput-object v3, p0, Lebj;->U:Ljava/lang/String;

    .line 448
    invoke-virtual {p0}, Lebj;->n()Lz;

    move-result-object v0

    .line 449
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setResult(I)V

    .line 450
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method

.method static synthetic b(Lebj;ILfib;)V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0, p1, p2}, Lebj;->b(ILfib;)V

    return-void
.end method

.method private c(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 282
    if-nez p1, :cond_0

    .line 288
    :goto_0
    return-void

    .line 286
    :cond_0
    iget-object v0, p0, Lebj;->P:Licq;

    invoke-virtual {v0}, Licq;->e()V

    .line 287
    iget-object v0, p0, Lebj;->N:Lhje;

    invoke-virtual {v0}, Lhje;->a()V

    goto :goto_0
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 307
    sget-object v0, Lhmw;->as:Lhmw;

    return-object v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 188
    const v0, 0x7f0400d1

    invoke-virtual {p1, v0, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 190
    invoke-virtual {p0}, Lebj;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "gaia_id"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 191
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "EXTRA_GAIA_ID must be set!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 194
    :cond_0
    new-instance v0, Leuu;

    iget-object v2, p0, Lebj;->at:Llnl;

    iget-object v3, p0, Lebj;->O:Lhee;

    .line 195
    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    invoke-virtual {p0}, Lebj;->k()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "gaia_id"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v2, v7, v3, v4}, Leuu;-><init>(Landroid/content/Context;Landroid/database/Cursor;ILjava/lang/String;)V

    iput-object v0, p0, Lebj;->R:Leuu;

    .line 196
    iget-object v0, p0, Lebj;->R:Leuu;

    invoke-virtual {v0, p0}, Leuu;->a(Landroid/view/View$OnClickListener;)V

    .line 198
    const v0, 0x7f1002b2

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lebj;->Q:Landroid/widget/ListView;

    .line 199
    iget-object v0, p0, Lebj;->Q:Landroid/widget/ListView;

    new-instance v2, Lebo;

    invoke-direct {v2}, Lebo;-><init>()V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setRecyclerListener(Landroid/widget/AbsListView$RecyclerListener;)V

    .line 200
    iget-object v0, p0, Lebj;->Q:Landroid/widget/ListView;

    iget-object v2, p0, Lebj;->R:Leuu;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 202
    invoke-virtual {p0}, Lebj;->w()Lbb;

    move-result-object v0

    invoke-virtual {v0, v6, v7, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 204
    return-object v1
.end method

.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 325
    new-instance v0, Leuw;

    iget-object v1, p0, Lebj;->at:Llnl;

    iget-object v2, p0, Lebj;->O:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    const/4 v3, 0x2

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    .line 327
    invoke-virtual {p0}, Lebj;->k()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "gaia_id"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    .line 326
    invoke-static {v3, v4}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Leuw;-><init>(Landroid/content/Context;ILjava/lang/String;)V

    return-object v0
.end method

.method public a()V
    .locals 4

    .prologue
    .line 311
    iget-object v0, p0, Lebj;->S:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 321
    :goto_0
    return-void

    .line 315
    :cond_0
    iget-object v0, p0, Lebj;->at:Llnl;

    iget-object v1, p0, Lebj;->O:Lhee;

    .line 316
    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    invoke-virtual {p0}, Lebj;->k()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "gaia_id"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    .line 315
    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/service/EsService;->c(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lebj;->S:Ljava/lang/Integer;

    .line 318
    invoke-virtual {p0}, Lebj;->x()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lebj;->c(Landroid/view/View;)V

    .line 319
    iget-object v0, p0, Lebj;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Lebj;->at:Llnl;

    invoke-direct {v1, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->ek:Lhmv;

    .line 320
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 319
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    goto :goto_0
.end method

.method public a(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 518
    return-void
.end method

.method public a(IZLandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 523
    return-void
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 333
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lebj;->S:Ljava/lang/Integer;

    if-nez v0, :cond_1

    .line 334
    const/4 v0, 0x2

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    .line 335
    invoke-virtual {p0}, Lebj;->k()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "gaia_id"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v8

    .line 334
    invoke-static {v0, v1}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 336
    new-instance v1, Lfue;

    iget-object v2, p0, Lebj;->at:Llnl;

    iget-object v0, p0, Lebj;->O:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v3

    const-wide/16 v6, -0x1

    move-object v4, p0

    invoke-direct/range {v1 .. v7}, Lfue;-><init>(Landroid/content/Context;ILfuf;Ljava/lang/String;J)V

    new-array v0, v8, [Ljava/lang/Void;

    .line 337
    invoke-virtual {v1, v0}, Lfue;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 340
    :cond_1
    iget-object v0, p0, Lebj;->R:Leuu;

    invoke-virtual {v0, p1}, Leuu;->b(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 341
    invoke-virtual {p0}, Lebj;->x()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lebj;->c(Landroid/view/View;)V

    .line 342
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 160
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 162
    if-eqz p1, :cond_4

    .line 163
    const-string v0, "refresh_request"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 164
    const-string v0, "refresh_request"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lebj;->S:Ljava/lang/Integer;

    .line 167
    :cond_0
    const-string v0, "get_album_tiles_request"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 168
    const-string v0, "get_album_tiles_request"

    .line 169
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lebj;->T:Ljava/lang/Integer;

    .line 172
    :cond_1
    const-string v0, "cluster_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 173
    const-string v0, "cluster_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lebj;->U:Ljava/lang/String;

    .line 180
    :cond_2
    :goto_0
    invoke-virtual {p0}, Lebj;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "media_resolver"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 181
    invoke-virtual {p0}, Lebj;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "media_resolver"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lduo;

    iput-object v0, p0, Lebj;->V:Lduo;

    .line 183
    :cond_3
    return-void

    .line 177
    :cond_4
    invoke-virtual {p0}, Lebj;->a()V

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 487
    const-string v0, "copy_to_album"

    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 488
    const-string v0, "cluster_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 489
    const-string v0, "album_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 492
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 493
    invoke-static {v1}, Ljvj;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 495
    :cond_0
    invoke-virtual {p0}, Lebj;->n()Lz;

    move-result-object v2

    iget-object v3, p0, Lebj;->O:Lhee;

    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    new-instance v4, Lebn;

    iget-object v5, p0, Lebj;->O:Lhee;

    invoke-interface {v5}, Lhee;->d()I

    move-result v5

    iget-object v6, p0, Lebj;->V:Lduo;

    invoke-direct {v4, v5, v6}, Lebn;-><init>(ILduo;)V

    invoke-static {v2, v3, v0, v4}, Ljtx;->b(Landroid/content/Context;ILjava/lang/String;Ljtz;)Ljtx;

    move-result-object v0

    invoke-direct {p0, v0}, Lebj;->a(Ljtx;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lebj;->U:Ljava/lang/String;

    .line 497
    :cond_1
    const-string v0, "AddToAlbumFragment"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 498
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x21

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "User selected album [cluster id:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 501
    :cond_2
    return-void
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 346
    return-void
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 77
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p2}, Lebj;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method public a(Lhjk;)V
    .locals 2

    .prologue
    .line 264
    const v0, 0x7f0a0aaf

    invoke-interface {p1, v0}, Lhjk;->d(I)V

    .line 266
    const v0, 0x7f10067b

    .line 267
    invoke-interface {p1, v0}, Lhjk;->a(I)Lhjc;

    move-result-object v0

    check-cast v0, Lhjv;

    .line 268
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lhjv;->a(I)V

    .line 269
    return-void
.end method

.method public a(Ljava/lang/String;I)V
    .locals 5

    .prologue
    .line 455
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 456
    invoke-virtual {p0}, Lebj;->n()Lz;

    move-result-object v0

    iget-object v1, p0, Lebj;->O:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    new-instance v2, Lebn;

    iget-object v3, p0, Lebj;->O:Lhee;

    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    iget-object v4, p0, Lebj;->V:Lduo;

    invoke-direct {v2, v3, v4}, Lebn;-><init>(ILduo;)V

    invoke-static {v0, v1, p1, v2}, Ljtx;->a(Landroid/content/Context;ILjava/lang/String;Ljtz;)Ljtx;

    move-result-object v0

    invoke-direct {p0, v0}, Lebj;->a(Ljtx;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lebj;->U:Ljava/lang/String;

    .line 458
    :cond_0
    return-void
.end method

.method public a(Loo;)V
    .locals 2

    .prologue
    .line 534
    iget-object v0, p0, Lebj;->at:Llnl;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lhns;->a(Landroid/content/Context;I)Z

    move-result v0

    invoke-static {p1, v0}, Lley;->a(Loo;Z)V

    .line 535
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 273
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 274
    const v1, 0x7f10067b

    if-ne v0, v1, :cond_0

    .line 275
    invoke-virtual {p0}, Lebj;->a()V

    .line 276
    const/4 v0, 0x1

    .line 278
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aO_()V
    .locals 2

    .prologue
    .line 221
    invoke-super {p0}, Llol;->aO_()V

    .line 223
    iget-object v0, p0, Lebj;->X:Lfhh;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(Lfhh;)V

    .line 225
    iget-object v0, p0, Lebj;->S:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 226
    iget-object v0, p0, Lebj;->S:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 227
    iget-object v0, p0, Lebj;->S:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(I)Lfib;

    move-result-object v0

    .line 228
    iget-object v1, p0, Lebj;->S:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, v0}, Lebj;->a(ILfib;)V

    .line 232
    :cond_0
    iget-object v0, p0, Lebj;->T:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 233
    iget-object v0, p0, Lebj;->T:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 234
    iget-object v0, p0, Lebj;->T:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(I)Lfib;

    move-result-object v0

    .line 235
    iget-object v1, p0, Lebj;->T:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, v0}, Lebj;->b(ILfib;)V

    .line 238
    :cond_1
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 543
    return-void
.end method

.method public b(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 507
    return-void
.end method

.method public b(Loo;)V
    .locals 0

    .prologue
    .line 539
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 148
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 149
    iget-object v0, p0, Lebj;->au:Llnh;

    const-class v1, Lhmq;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 150
    iget-object v0, p0, Lebj;->au:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Lebj;->O:Lhee;

    .line 152
    iget-object v0, p0, Lebj;->au:Llnh;

    const-class v1, Lhoc;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    .line 153
    iget-object v1, p0, Lebj;->W:Lhob;

    invoke-virtual {v0, v1}, Lhoc;->a(Lhob;)Lhoc;

    .line 154
    new-instance v1, Lhpf;

    iget-object v2, p0, Lebj;->at:Llnl;

    .line 155
    invoke-virtual {p0}, Lebj;->p()Lae;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lhpf;-><init>(Landroid/content/Context;Lae;)V

    .line 154
    invoke-virtual {v0, v1}, Lhoc;->a(Lhos;)V

    .line 156
    return-void
.end method

.method public c(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 513
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 248
    invoke-super {p0, p1}, Llol;->e(Landroid/os/Bundle;)V

    .line 249
    iget-object v0, p0, Lebj;->S:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 250
    const-string v0, "refresh_request"

    iget-object v1, p0, Lebj;->S:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 253
    :cond_0
    iget-object v0, p0, Lebj;->T:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 254
    const-string v0, "get_album_tiles_request"

    iget-object v1, p0, Lebj;->T:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 257
    :cond_1
    iget-object v0, p0, Lebj;->U:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 258
    const-string v0, "cluster_id"

    iget-object v1, p0, Lebj;->U:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    :cond_2
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 215
    invoke-super {p0}, Llol;->g()V

    .line 216
    iget-object v0, p0, Lebj;->Q:Landroid/widget/ListView;

    invoke-static {v0}, Llii;->c(Landroid/view/View;)V

    .line 217
    return-void
.end method

.method public h()V
    .locals 1

    .prologue
    .line 242
    invoke-super {p0}, Llol;->h()V

    .line 243
    iget-object v0, p0, Lebj;->Q:Landroid/widget/ListView;

    invoke-static {v0}, Llii;->d(Landroid/view/View;)V

    .line 244
    return-void
.end method

.method public l_(Z)V
    .locals 1

    .prologue
    .line 557
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lebj;->n()Lz;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 558
    invoke-virtual {p0}, Lebj;->a()V

    .line 560
    :cond_0
    invoke-virtual {p0}, Lebj;->x()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lebj;->c(Landroid/view/View;)V

    .line 561
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x0

    .line 350
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 352
    const v1, 0x7f100187

    if-ne v0, v1, :cond_1

    .line 353
    new-instance v0, Llgz;

    invoke-direct {v0}, Llgz;-><init>()V

    .line 354
    invoke-virtual {p0}, Lebj;->o()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0aac

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Llgz;->b(Ljava/lang/String;)Llgz;

    move-result-object v0

    .line 355
    invoke-virtual {p0}, Lebj;->o()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a07d0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Llgz;->c(Ljava/lang/String;)Llgz;

    move-result-object v0

    .line 356
    invoke-virtual {p0}, Lebj;->o()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0aae

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Llgz;->d(Ljava/lang/String;)Llgz;

    move-result-object v0

    .line 357
    invoke-virtual {v0}, Llgz;->b()Llgw;

    move-result-object v0

    .line 358
    invoke-virtual {v0, p0, v7}, Llgw;->a(Lu;I)V

    .line 359
    invoke-virtual {p0}, Lebj;->p()Lae;

    move-result-object v1

    const-string v2, "create_album"

    invoke-virtual {v0, v1, v2}, Llgw;->a(Lae;Ljava/lang/String;)V

    .line 386
    :cond_0
    :goto_0
    return-void

    .line 361
    :cond_1
    const v1, 0x7f100182

    if-ne v0, v1, :cond_0

    .line 365
    const v0, 0x7f10007b

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 366
    const v1, 0x7f10007c

    invoke-virtual {p1, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 368
    invoke-virtual {p0}, Lebj;->o()Landroid/content/res/Resources;

    move-result-object v2

    .line 370
    iget-object v3, p0, Lebj;->V:Lduo;

    invoke-interface {v3}, Lduo;->a()I

    move-result v3

    .line 371
    const v4, 0x7f110063

    new-array v5, v8, [Ljava/lang/Object;

    .line 372
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    const/4 v6, 0x1

    aput-object v0, v5, v6

    .line 371
    invoke-virtual {v2, v4, v3, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 373
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v8, v0, :cond_2

    const v0, 0x7f0a07d2

    .line 374
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 376
    :goto_1
    const v1, 0x7f0a07f9

    .line 378
    invoke-virtual {p0, v1}, Lebj;->e_(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0a0597

    .line 379
    invoke-virtual {p0, v2}, Lebj;->e_(I)Ljava/lang/String;

    move-result-object v2

    .line 376
    invoke-static {v3, v0, v1, v2}, Llgr;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)Llgr;

    move-result-object v1

    .line 380
    invoke-virtual {v1, p0}, Llgr;->a(Llgs;)V

    .line 381
    invoke-virtual {v1}, Llgr;->k()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "cluster_id"

    const v0, 0x7f10007f

    .line 382
    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 381
    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    invoke-virtual {v1}, Llgr;->k()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "album_id"

    const v0, 0x7f10007d

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 384
    invoke-virtual {p0}, Lebj;->p()Lae;

    move-result-object v0

    const-string v2, "copy_to_album"

    invoke-virtual {v1, v0, v2}, Llgr;->a(Lae;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 374
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public z()V
    .locals 1

    .prologue
    .line 209
    invoke-super {p0}, Llol;->z()V

    .line 210
    iget-object v0, p0, Lebj;->X:Lfhh;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(Lfhh;)V

    .line 211
    return-void
.end method
