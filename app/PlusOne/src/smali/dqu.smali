.class public final Ldqu;
.super Lllq;
.source "PG"


# direct methods
.method public static a([B)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)",
            "Ljava/util/List",
            "<",
            "Ldqv;",
            ">;"
        }
    .end annotation

    .prologue
    .line 83
    if-nez p0, :cond_1

    .line 84
    const/4 v0, 0x0

    .line 93
    :cond_0
    return-object v0

    .line 87
    :cond_1
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 88
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 89
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v3

    .line 90
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 91
    new-instance v4, Ldqv;

    invoke-direct {v4}, Ldqv;-><init>()V

    invoke-static {v2}, Ldqu;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Ldqv;->a:Ljava/lang/String;

    invoke-static {v2}, Ldqu;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Ldqv;->b:Ljava/lang/String;

    invoke-static {v2}, Ldqu;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Ldqv;->c:Ljava/lang/String;

    invoke-static {v2}, Ldqu;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Ldqv;->d:Ljava/lang/String;

    invoke-static {v2}, Ldqu;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Ldqv;->e:Ljava/lang/String;

    invoke-static {v2}, Ldqu;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Ldqv;->f:Ljava/lang/String;

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 90
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static a(Ljava/util/List;)[B
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ldqv;",
            ">;)[B"
        }
    .end annotation

    .prologue
    .line 64
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 65
    new-instance v2, Ljava/io/DataOutputStream;

    invoke-direct {v2, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 67
    if-nez p0, :cond_0

    .line 68
    const/4 v0, 0x0

    .line 78
    :goto_0
    return-object v0

    .line 71
    :cond_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 72
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldqv;

    .line 73
    iget-object v4, v0, Ldqv;->a:Ljava/lang/String;

    invoke-static {v2, v4}, Ldqu;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v4, v0, Ldqv;->b:Ljava/lang/String;

    invoke-static {v2, v4}, Ldqu;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v4, v0, Ldqv;->c:Ljava/lang/String;

    invoke-static {v2, v4}, Ldqu;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v4, v0, Ldqv;->d:Ljava/lang/String;

    invoke-static {v2, v4}, Ldqu;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v4, v0, Ldqv;->e:Ljava/lang/String;

    invoke-static {v2, v4}, Ldqu;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v0, v0, Ldqv;->f:Ljava/lang/String;

    invoke-static {v2, v0}, Ldqu;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    goto :goto_1

    .line 76
    :cond_1
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 77
    invoke-virtual {v2}, Ljava/io/DataOutputStream;->close()V

    goto :goto_0
.end method
