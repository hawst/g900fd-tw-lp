.class public final Lmgf;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnav;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 23488
    invoke-direct {p0}, Loxq;-><init>()V

    .line 23491
    iput-object v0, p0, Lmgf;->apiHeader:Llyr;

    .line 23494
    iput-object v0, p0, Lmgf;->a:Lnav;

    .line 23488
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 23511
    const/4 v0, 0x0

    .line 23512
    iget-object v1, p0, Lmgf;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 23513
    const/4 v0, 0x1

    iget-object v1, p0, Lmgf;->apiHeader:Llyr;

    .line 23514
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 23516
    :cond_0
    iget-object v1, p0, Lmgf;->a:Lnav;

    if-eqz v1, :cond_1

    .line 23517
    const/4 v1, 0x2

    iget-object v2, p0, Lmgf;->a:Lnav;

    .line 23518
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 23520
    :cond_1
    iget-object v1, p0, Lmgf;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 23521
    iput v0, p0, Lmgf;->ai:I

    .line 23522
    return v0
.end method

.method public a(Loxn;)Lmgf;
    .locals 2

    .prologue
    .line 23530
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 23531
    sparse-switch v0, :sswitch_data_0

    .line 23535
    iget-object v1, p0, Lmgf;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 23536
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmgf;->ah:Ljava/util/List;

    .line 23539
    :cond_1
    iget-object v1, p0, Lmgf;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 23541
    :sswitch_0
    return-object p0

    .line 23546
    :sswitch_1
    iget-object v0, p0, Lmgf;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 23547
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmgf;->apiHeader:Llyr;

    .line 23549
    :cond_2
    iget-object v0, p0, Lmgf;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 23553
    :sswitch_2
    iget-object v0, p0, Lmgf;->a:Lnav;

    if-nez v0, :cond_3

    .line 23554
    new-instance v0, Lnav;

    invoke-direct {v0}, Lnav;-><init>()V

    iput-object v0, p0, Lmgf;->a:Lnav;

    .line 23556
    :cond_3
    iget-object v0, p0, Lmgf;->a:Lnav;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 23531
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 23499
    iget-object v0, p0, Lmgf;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 23500
    const/4 v0, 0x1

    iget-object v1, p0, Lmgf;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 23502
    :cond_0
    iget-object v0, p0, Lmgf;->a:Lnav;

    if-eqz v0, :cond_1

    .line 23503
    const/4 v0, 0x2

    iget-object v1, p0, Lmgf;->a:Lnav;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 23505
    :cond_1
    iget-object v0, p0, Lmgf;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 23507
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 23484
    invoke-virtual {p0, p1}, Lmgf;->a(Loxn;)Lmgf;

    move-result-object v0

    return-object v0
.end method
