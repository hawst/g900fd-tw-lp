.class public Lcyc;
.super Llol;
.source "PG"


# instance fields
.field private final N:Lcyf;

.field private O:Ldgi;

.field private P:Ldgr;

.field private Q:Ldeo;

.field private R:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 30
    invoke-direct {p0}, Llol;-><init>()V

    .line 34
    new-instance v0, Lcyf;

    invoke-direct {v0, p0}, Lcyf;-><init>(Lcyc;)V

    iput-object v0, p0, Lcyc;->N:Lcyf;

    .line 43
    new-instance v0, Ldgn;

    iget-object v1, p0, Lcyc;->av:Llqm;

    sget-object v2, Ldgx;->O:Ldgo;

    new-instance v3, Lcyd;

    invoke-direct {v3, p0}, Lcyd;-><init>(Lcyc;)V

    invoke-direct {v0, v1, v2, v3}, Ldgn;-><init>(Llqr;Ldgu;Ldgv;)V

    .line 49
    new-instance v0, Ldep;

    iget-object v1, p0, Lcyc;->av:Llqm;

    new-instance v2, Lcye;

    invoke-direct {v2, p0}, Lcye;-><init>(Lcyc;)V

    invoke-direct {v0, v1, v2}, Ldep;-><init>(Llqr;Ldeq;)V

    .line 130
    return-void
.end method

.method static synthetic a(Lcyc;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 0

    .prologue
    .line 30
    iput-object p1, p0, Lcyc;->R:Ljava/lang/Integer;

    return-object p1
.end method

.method static synthetic a(Lcyc;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 30
    iget-object v0, p0, Lcyc;->Q:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    invoke-interface {v0}, Lddl;->o()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    invoke-interface {v0}, Lddl;->j()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcyc;->Q:Ldeo;

    invoke-virtual {v1, v8}, Ldeo;->c(Z)V

    invoke-interface {v0}, Lddl;->j()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0}, Lddl;->o()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/plus/service/EsService;->a(Ljava/lang/String;J)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcyc;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "account_id"

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {p0}, Lcyc;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v3, "view_id"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0}, Lcyc;->n()Lz;

    move-result-object v1

    invoke-virtual {v1}, Lz;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0}, Lddl;->j()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0}, Lddl;->o()J

    move-result-wide v4

    invoke-interface {v0}, Lddl;->k()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v0}, Lddl;->O()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v8, 0x1

    :cond_2
    invoke-static/range {v1 .. v8}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILjava/lang/String;JLjava/lang/String;Ljava/lang/String;Z)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcyc;->R:Ljava/lang/Integer;

    iget-object v0, p0, Lcyc;->O:Ldgi;

    sget-object v1, Lhmv;->el:Lhmv;

    invoke-interface {v0, v1}, Ldgi;->a(Lhmv;)V

    goto :goto_0
.end method

.method static synthetic b(Lcyc;)V
    .locals 3

    .prologue
    .line 30
    iget-object v0, p0, Lcyc;->P:Ldgr;

    sget-object v1, Ldgx;->O:Ldgo;

    iget-object v2, p0, Lcyc;->Q:Ldeo;

    invoke-virtual {v2}, Ldeo;->a()Lddl;

    move-result-object v2

    invoke-interface {v2}, Lddl;->B()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Ldgr;->a(Ldgu;Z)V

    return-void
.end method

.method static synthetic c(Lcyc;)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcyc;->R:Ljava/lang/Integer;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 67
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 68
    if-nez p1, :cond_1

    .line 75
    :cond_0
    :goto_0
    return-void

    .line 72
    :cond_1
    const-string v0, "plusone_request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 73
    const-string v0, "plusone_request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcyc;->R:Ljava/lang/Integer;

    goto :goto_0
.end method

.method public aO_()V
    .locals 1

    .prologue
    .line 87
    invoke-super {p0}, Llol;->aO_()V

    .line 88
    iget-object v0, p0, Lcyc;->N:Lcyf;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(Lfhh;)V

    .line 90
    iget-object v0, p0, Lcyc;->R:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Lcyc;->R:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 92
    iget-object v0, p0, Lcyc;->R:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(I)Lfib;

    .line 93
    const/4 v0, 0x0

    iput-object v0, p0, Lcyc;->R:Ljava/lang/Integer;

    .line 96
    :cond_0
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 59
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 60
    iget-object v0, p0, Lcyc;->au:Llnh;

    const-class v1, Ldgi;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldgi;

    iput-object v0, p0, Lcyc;->O:Ldgi;

    .line 61
    iget-object v0, p0, Lcyc;->au:Llnh;

    const-class v1, Ldgr;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldgr;

    iput-object v0, p0, Lcyc;->P:Ldgr;

    .line 62
    iget-object v0, p0, Lcyc;->au:Llnh;

    const-class v1, Ldeo;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldeo;

    iput-object v0, p0, Lcyc;->Q:Ldeo;

    .line 63
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 79
    invoke-super {p0, p1}, Llol;->e(Landroid/os/Bundle;)V

    .line 80
    iget-object v0, p0, Lcyc;->R:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 81
    const-string v0, "plusone_request_id"

    iget-object v1, p0, Lcyc;->R:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 83
    :cond_0
    return-void
.end method

.method public z()V
    .locals 1

    .prologue
    .line 100
    invoke-super {p0}, Llol;->z()V

    .line 101
    iget-object v0, p0, Lcyc;->N:Lcyf;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(Lfhh;)V

    .line 102
    return-void
.end method
