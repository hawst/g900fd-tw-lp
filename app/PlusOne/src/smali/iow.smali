.class public final Liow;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private c:I

.field private d:Z

.field private e:J

.field private f:Ljava/lang/String;

.field private g:I

.field private h:I

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    new-instance v0, Liow;

    invoke-direct {v0}, Liow;-><init>()V

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    const/16 v0, 0x3fff

    iput v0, p0, Liow;->c:I

    .line 24
    const/4 v0, 0x7

    iput v0, p0, Liow;->g:I

    .line 165
    return-void
.end method


# virtual methods
.method public a()Liow;
    .locals 4

    .prologue
    .line 32
    new-instance v0, Liow;

    invoke-direct {v0}, Liow;-><init>()V

    iget-object v1, p0, Liow;->a:Ljava/lang/String;

    .line 33
    invoke-virtual {v0, v1}, Liow;->a(Ljava/lang/String;)Liow;

    move-result-object v0

    iget-object v1, p0, Liow;->b:Ljava/util/Collection;

    .line 34
    invoke-virtual {v0, v1}, Liow;->a(Ljava/util/Collection;)Liow;

    move-result-object v0

    iget v1, p0, Liow;->c:I

    .line 35
    invoke-virtual {v0, v1}, Liow;->a(I)Liow;

    move-result-object v0

    iget-boolean v1, p0, Liow;->d:Z

    .line 36
    invoke-virtual {v0, v1}, Liow;->a(Z)Liow;

    move-result-object v0

    iget-wide v2, p0, Liow;->e:J

    .line 37
    invoke-virtual {v0, v2, v3}, Liow;->a(J)Liow;

    move-result-object v0

    iget-object v1, p0, Liow;->f:Ljava/lang/String;

    .line 38
    invoke-virtual {v0, v1}, Liow;->b(Ljava/lang/String;)Liow;

    move-result-object v0

    iget v1, p0, Liow;->g:I

    .line 39
    invoke-virtual {v0, v1}, Liow;->b(I)Liow;

    move-result-object v0

    iget v1, p0, Liow;->h:I

    .line 40
    invoke-virtual {v0, v1}, Liow;->c(I)Liow;

    move-result-object v0

    iget v1, p0, Liow;->i:I

    .line 41
    invoke-virtual {v0, v1}, Liow;->d(I)Liow;

    move-result-object v0

    return-object v0
.end method

.method public a(I)Liow;
    .locals 0

    .prologue
    .line 55
    iput p1, p0, Liow;->c:I

    .line 56
    return-object p0
.end method

.method public a(J)Liow;
    .locals 1

    .prologue
    .line 65
    iput-wide p1, p0, Liow;->e:J

    .line 66
    return-object p0
.end method

.method public a(Ljava/lang/String;)Liow;
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Liow;->a:Ljava/lang/String;

    .line 46
    return-object p0
.end method

.method public a(Ljava/util/Collection;)Liow;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Liow;"
        }
    .end annotation

    .prologue
    .line 50
    iput-object p1, p0, Liow;->b:Ljava/util/Collection;

    .line 51
    return-object p0
.end method

.method public a(Z)Liow;
    .locals 0

    .prologue
    .line 60
    iput-boolean p1, p0, Liow;->d:Z

    .line 61
    return-object p0
.end method

.method public b(I)Liow;
    .locals 0

    .prologue
    .line 75
    iput p1, p0, Liow;->g:I

    .line 76
    return-object p0
.end method

.method public b(Ljava/lang/String;)Liow;
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Liow;->f:Ljava/lang/String;

    .line 71
    return-object p0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Liow;->a:Ljava/lang/String;

    return-object v0
.end method

.method public c(I)Liow;
    .locals 0

    .prologue
    .line 80
    iput p1, p0, Liow;->h:I

    .line 81
    return-object p0
.end method

.method public c()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 94
    iget-object v0, p0, Liow;->b:Ljava/util/Collection;

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 9
    invoke-virtual {p0}, Liow;->a()Liow;

    move-result-object v0

    return-object v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 98
    iget v0, p0, Liow;->c:I

    return v0
.end method

.method public d(I)Liow;
    .locals 0

    .prologue
    .line 85
    iput p1, p0, Liow;->i:I

    .line 86
    return-object p0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 102
    iget-boolean v0, p0, Liow;->d:Z

    return v0
.end method

.method public f()J
    .locals 2

    .prologue
    .line 106
    iget-wide v0, p0, Liow;->e:J

    return-wide v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Liow;->f:Ljava/lang/String;

    return-object v0
.end method

.method public h()I
    .locals 1

    .prologue
    .line 114
    iget v0, p0, Liow;->g:I

    return v0
.end method

.method public i()I
    .locals 1

    .prologue
    .line 118
    iget v0, p0, Liow;->h:I

    return v0
.end method

.method public j()I
    .locals 1

    .prologue
    .line 122
    iget v0, p0, Liow;->i:I

    return v0
.end method
