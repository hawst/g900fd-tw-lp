.class public final Lnjs;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Locm;

.field private b:Locb;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 195
    invoke-direct {p0}, Loxq;-><init>()V

    .line 198
    const/4 v0, 0x0

    iput-object v0, p0, Lnjs;->b:Locb;

    .line 201
    sget-object v0, Locm;->a:[Locm;

    iput-object v0, p0, Lnjs;->a:[Locm;

    .line 195
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 222
    .line 223
    iget-object v0, p0, Lnjs;->b:Locb;

    if-eqz v0, :cond_2

    .line 224
    const/4 v0, 0x1

    iget-object v2, p0, Lnjs;->b:Locb;

    .line 225
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 227
    :goto_0
    iget-object v2, p0, Lnjs;->a:[Locm;

    if-eqz v2, :cond_1

    .line 228
    iget-object v2, p0, Lnjs;->a:[Locm;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 229
    if-eqz v4, :cond_0

    .line 230
    const/4 v5, 0x2

    .line 231
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 228
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 235
    :cond_1
    iget-object v1, p0, Lnjs;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 236
    iput v0, p0, Lnjs;->ai:I

    .line 237
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lnjs;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 245
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 246
    sparse-switch v0, :sswitch_data_0

    .line 250
    iget-object v2, p0, Lnjs;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 251
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnjs;->ah:Ljava/util/List;

    .line 254
    :cond_1
    iget-object v2, p0, Lnjs;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 256
    :sswitch_0
    return-object p0

    .line 261
    :sswitch_1
    iget-object v0, p0, Lnjs;->b:Locb;

    if-nez v0, :cond_2

    .line 262
    new-instance v0, Locb;

    invoke-direct {v0}, Locb;-><init>()V

    iput-object v0, p0, Lnjs;->b:Locb;

    .line 264
    :cond_2
    iget-object v0, p0, Lnjs;->b:Locb;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 268
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 269
    iget-object v0, p0, Lnjs;->a:[Locm;

    if-nez v0, :cond_4

    move v0, v1

    .line 270
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Locm;

    .line 271
    iget-object v3, p0, Lnjs;->a:[Locm;

    if-eqz v3, :cond_3

    .line 272
    iget-object v3, p0, Lnjs;->a:[Locm;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 274
    :cond_3
    iput-object v2, p0, Lnjs;->a:[Locm;

    .line 275
    :goto_2
    iget-object v2, p0, Lnjs;->a:[Locm;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 276
    iget-object v2, p0, Lnjs;->a:[Locm;

    new-instance v3, Locm;

    invoke-direct {v3}, Locm;-><init>()V

    aput-object v3, v2, v0

    .line 277
    iget-object v2, p0, Lnjs;->a:[Locm;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 278
    invoke-virtual {p1}, Loxn;->a()I

    .line 275
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 269
    :cond_4
    iget-object v0, p0, Lnjs;->a:[Locm;

    array-length v0, v0

    goto :goto_1

    .line 281
    :cond_5
    iget-object v2, p0, Lnjs;->a:[Locm;

    new-instance v3, Locm;

    invoke-direct {v3}, Locm;-><init>()V

    aput-object v3, v2, v0

    .line 282
    iget-object v2, p0, Lnjs;->a:[Locm;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 246
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 206
    iget-object v0, p0, Lnjs;->b:Locb;

    if-eqz v0, :cond_0

    .line 207
    const/4 v0, 0x1

    iget-object v1, p0, Lnjs;->b:Locb;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 209
    :cond_0
    iget-object v0, p0, Lnjs;->a:[Locm;

    if-eqz v0, :cond_2

    .line 210
    iget-object v1, p0, Lnjs;->a:[Locm;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 211
    if-eqz v3, :cond_1

    .line 212
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 210
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 216
    :cond_2
    iget-object v0, p0, Lnjs;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 218
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 191
    invoke-virtual {p0, p1}, Lnjs;->a(Loxn;)Lnjs;

    move-result-object v0

    return-object v0
.end method
