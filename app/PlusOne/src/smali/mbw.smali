.class public final Lmbw;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lmqs;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 18385
    invoke-direct {p0}, Loxq;-><init>()V

    .line 18388
    iput-object v0, p0, Lmbw;->apiHeader:Llyq;

    .line 18391
    iput-object v0, p0, Lmbw;->a:Lmqs;

    .line 18385
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 18408
    const/4 v0, 0x0

    .line 18409
    iget-object v1, p0, Lmbw;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 18410
    const/4 v0, 0x1

    iget-object v1, p0, Lmbw;->apiHeader:Llyq;

    .line 18411
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 18413
    :cond_0
    iget-object v1, p0, Lmbw;->a:Lmqs;

    if-eqz v1, :cond_1

    .line 18414
    const/4 v1, 0x2

    iget-object v2, p0, Lmbw;->a:Lmqs;

    .line 18415
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 18417
    :cond_1
    iget-object v1, p0, Lmbw;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 18418
    iput v0, p0, Lmbw;->ai:I

    .line 18419
    return v0
.end method

.method public a(Loxn;)Lmbw;
    .locals 2

    .prologue
    .line 18427
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 18428
    sparse-switch v0, :sswitch_data_0

    .line 18432
    iget-object v1, p0, Lmbw;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 18433
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmbw;->ah:Ljava/util/List;

    .line 18436
    :cond_1
    iget-object v1, p0, Lmbw;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 18438
    :sswitch_0
    return-object p0

    .line 18443
    :sswitch_1
    iget-object v0, p0, Lmbw;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 18444
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmbw;->apiHeader:Llyq;

    .line 18446
    :cond_2
    iget-object v0, p0, Lmbw;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 18450
    :sswitch_2
    iget-object v0, p0, Lmbw;->a:Lmqs;

    if-nez v0, :cond_3

    .line 18451
    new-instance v0, Lmqs;

    invoke-direct {v0}, Lmqs;-><init>()V

    iput-object v0, p0, Lmbw;->a:Lmqs;

    .line 18453
    :cond_3
    iget-object v0, p0, Lmbw;->a:Lmqs;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 18428
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 18396
    iget-object v0, p0, Lmbw;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 18397
    const/4 v0, 0x1

    iget-object v1, p0, Lmbw;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 18399
    :cond_0
    iget-object v0, p0, Lmbw;->a:Lmqs;

    if-eqz v0, :cond_1

    .line 18400
    const/4 v0, 0x2

    iget-object v1, p0, Lmbw;->a:Lmqs;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 18402
    :cond_1
    iget-object v0, p0, Lmbw;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 18404
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 18381
    invoke-virtual {p0, p1}, Lmbw;->a(Loxn;)Lmbw;

    move-result-object v0

    return-object v0
.end method
