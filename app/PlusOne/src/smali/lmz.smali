.class public final Llmz;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Landroid/util/SparseBooleanArray;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 22
    new-instance v0, Landroid/util/SparseBooleanArray;

    invoke-direct {v0}, Landroid/util/SparseBooleanArray;-><init>()V

    .line 23
    sput-object v0, Llmz;->a:Landroid/util/SparseBooleanArray;

    const/16 v1, 0x12

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 24
    sget-object v0, Llmz;->a:Landroid/util/SparseBooleanArray;

    const/16 v1, 0x16

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 26
    sget-object v0, Llmz;->a:Landroid/util/SparseBooleanArray;

    const/16 v1, 0x24

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 27
    return-void
.end method

.method public static a(Lnym;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    invoke-static {p0}, Llmz;->b(Lnym;)Lnzc;

    move-result-object v0

    .line 61
    if-eqz v0, :cond_0

    iget-object v0, v0, Lnzc;->d:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(I)Z
    .locals 1

    .prologue
    .line 31
    sget-object v0, Llmz;->a:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0, p0}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v0

    return v0
.end method

.method public static a(Lnzb;)Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 36
    iget-object v4, p0, Lnzb;->c:[Lnzc;

    .line 37
    if-eqz v4, :cond_4

    .line 38
    array-length v5, v4

    move v3, v1

    :goto_0
    if-ge v3, v5, :cond_4

    aget-object v2, v4, v3

    .line 39
    iget-object v6, v2, Lnzc;->d:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_3

    iget-object v6, v2, Lnzc;->b:Ljava/lang/Integer;

    .line 40
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    sget-object v7, Llmz;->a:Landroid/util/SparseBooleanArray;

    invoke-virtual {v7, v6}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v6

    if-nez v6, :cond_1

    iget-object v2, v2, Lnzc;->d:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    const-string v6, "content"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    const-string v6, "file"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    move v2, v0

    :goto_1
    if-eqz v2, :cond_3

    .line 45
    :cond_1
    :goto_2
    return v0

    :cond_2
    move v2, v1

    .line 40
    goto :goto_1

    .line 38
    :cond_3
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    :cond_4
    move v0, v1

    .line 45
    goto :goto_2
.end method

.method public static b(Lnym;)Lnzc;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 68
    .line 69
    if-eqz p0, :cond_3

    iget-object v0, p0, Lnym;->m:Lnzb;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lnym;->m:Lnzb;

    iget v0, v0, Lnzb;->b:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lnym;->m:Lnzb;

    iget-object v0, v0, Lnzb;->c:[Lnzc;

    if-eqz v0, :cond_3

    .line 72
    iget-object v0, p0, Lnym;->m:Lnzb;

    iget-object v0, v0, Lnzb;->c:[Lnzc;

    move-object v4, v0

    .line 75
    :goto_0
    if-eqz v4, :cond_0

    array-length v0, v4

    if-nez v0, :cond_1

    .line 91
    :cond_0
    return-object v2

    .line 80
    :cond_1
    const/4 v1, -0x1

    .line 81
    array-length v5, v4

    const/4 v0, 0x0

    move v3, v0

    move v0, v1

    :goto_1
    if-ge v3, v5, :cond_0

    aget-object v1, v4, v3

    .line 82
    iget-object v6, v1, Lnzc;->b:Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    sget-object v7, Llmz;->a:Landroid/util/SparseBooleanArray;

    invoke-virtual {v7, v6}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v6

    if-eqz v6, :cond_2

    iget-object v6, v1, Lnzc;->d:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 83
    iget-object v6, v1, Lnzc;->c:Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    if-le v6, v0, :cond_2

    .line 87
    iget-object v0, v1, Lnzc;->c:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 81
    :goto_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move-object v2, v1

    goto :goto_1

    :cond_2
    move-object v1, v2

    goto :goto_2

    :cond_3
    move-object v4, v2

    goto :goto_0
.end method
