.class public final Lony;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lonz;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 102
    invoke-direct {p0}, Loxq;-><init>()V

    .line 105
    sget-object v0, Lonz;->a:[Lonz;

    iput-object v0, p0, Lony;->a:[Lonz;

    .line 102
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 123
    .line 124
    iget-object v1, p0, Lony;->a:[Lonz;

    if-eqz v1, :cond_1

    .line 125
    iget-object v2, p0, Lony;->a:[Lonz;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 126
    if-eqz v4, :cond_0

    .line 127
    const/4 v5, 0x1

    .line 128
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 125
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 132
    :cond_1
    iget-object v1, p0, Lony;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 133
    iput v0, p0, Lony;->ai:I

    .line 134
    return v0
.end method

.method public a(Loxn;)Lony;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 142
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 143
    sparse-switch v0, :sswitch_data_0

    .line 147
    iget-object v2, p0, Lony;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 148
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lony;->ah:Ljava/util/List;

    .line 151
    :cond_1
    iget-object v2, p0, Lony;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 153
    :sswitch_0
    return-object p0

    .line 158
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 159
    iget-object v0, p0, Lony;->a:[Lonz;

    if-nez v0, :cond_3

    move v0, v1

    .line 160
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lonz;

    .line 161
    iget-object v3, p0, Lony;->a:[Lonz;

    if-eqz v3, :cond_2

    .line 162
    iget-object v3, p0, Lony;->a:[Lonz;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 164
    :cond_2
    iput-object v2, p0, Lony;->a:[Lonz;

    .line 165
    :goto_2
    iget-object v2, p0, Lony;->a:[Lonz;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 166
    iget-object v2, p0, Lony;->a:[Lonz;

    new-instance v3, Lonz;

    invoke-direct {v3}, Lonz;-><init>()V

    aput-object v3, v2, v0

    .line 167
    iget-object v2, p0, Lony;->a:[Lonz;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 168
    invoke-virtual {p1}, Loxn;->a()I

    .line 165
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 159
    :cond_3
    iget-object v0, p0, Lony;->a:[Lonz;

    array-length v0, v0

    goto :goto_1

    .line 171
    :cond_4
    iget-object v2, p0, Lony;->a:[Lonz;

    new-instance v3, Lonz;

    invoke-direct {v3}, Lonz;-><init>()V

    aput-object v3, v2, v0

    .line 172
    iget-object v2, p0, Lony;->a:[Lonz;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 143
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 110
    iget-object v0, p0, Lony;->a:[Lonz;

    if-eqz v0, :cond_1

    .line 111
    iget-object v1, p0, Lony;->a:[Lonz;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 112
    if-eqz v3, :cond_0

    .line 113
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 111
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 117
    :cond_1
    iget-object v0, p0, Lony;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 119
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 98
    invoke-virtual {p0, p1}, Lony;->a(Loxn;)Lony;

    move-result-object v0

    return-object v0
.end method
