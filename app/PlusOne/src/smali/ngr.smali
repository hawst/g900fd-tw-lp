.class public final Lngr;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 190
    const v0, 0x39526ca

    new-instance v1, Lngs;

    invoke-direct {v1}, Lngs;-><init>()V

    .line 195
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    .line 194
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 191
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 216
    const/4 v0, 0x0

    .line 217
    iget-object v1, p0, Lngr;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 218
    const/4 v0, 0x2

    iget-object v1, p0, Lngr;->a:Ljava/lang/String;

    .line 219
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 221
    :cond_0
    iget-object v1, p0, Lngr;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 222
    const/4 v1, 0x3

    iget-object v2, p0, Lngr;->b:Ljava/lang/String;

    .line 223
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 225
    :cond_1
    iget-object v1, p0, Lngr;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 226
    iput v0, p0, Lngr;->ai:I

    .line 227
    return v0
.end method

.method public a(Loxn;)Lngr;
    .locals 2

    .prologue
    .line 235
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 236
    sparse-switch v0, :sswitch_data_0

    .line 240
    iget-object v1, p0, Lngr;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 241
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lngr;->ah:Ljava/util/List;

    .line 244
    :cond_1
    iget-object v1, p0, Lngr;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 246
    :sswitch_0
    return-object p0

    .line 251
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lngr;->a:Ljava/lang/String;

    goto :goto_0

    .line 255
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lngr;->b:Ljava/lang/String;

    goto :goto_0

    .line 236
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x1a -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 204
    iget-object v0, p0, Lngr;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 205
    const/4 v0, 0x2

    iget-object v1, p0, Lngr;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 207
    :cond_0
    iget-object v0, p0, Lngr;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 208
    const/4 v0, 0x3

    iget-object v1, p0, Lngr;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 210
    :cond_1
    iget-object v0, p0, Lngr;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 212
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 187
    invoke-virtual {p0, p1}, Lngr;->a(Loxn;)Lngr;

    move-result-object v0

    return-object v0
.end method
