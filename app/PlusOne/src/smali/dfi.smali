.class final Ldfi;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ldbh;


# instance fields
.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ldbi;",
            ">;"
        }
    .end annotation
.end field

.field private b:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1313
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1314
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Ldfi;->a:Ljava/util/Set;

    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 1340
    iget-object v0, p0, Ldfi;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbi;

    .line 1341
    iget-boolean v2, p0, Ldfi;->b:Z

    invoke-interface {v0, v2}, Ldbi;->a(Z)V

    goto :goto_0

    .line 1343
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Ldbi;)V
    .locals 1

    .prologue
    .line 1319
    iget-object v0, p0, Ldfi;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1320
    invoke-direct {p0}, Ldfi;->b()V

    .line 1321
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 1335
    iput-boolean p1, p0, Ldfi;->b:Z

    .line 1336
    invoke-direct {p0}, Ldfi;->b()V

    .line 1337
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 1330
    iget-boolean v0, p0, Ldfi;->b:Z

    return v0
.end method

.method public b(Ldbi;)V
    .locals 1

    .prologue
    .line 1325
    iget-object v0, p0, Ldfi;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1326
    return-void
.end method
