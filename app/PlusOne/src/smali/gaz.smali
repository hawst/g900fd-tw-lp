.class public final Lgaz;
.super Lgbz;
.source "PG"


# instance fields
.field private A:Lgbk;

.field private B:I

.field private y:Lozp;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lgaz;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lgaz;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0, p1, p2, p3}, Lgbz;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    const/4 v0, -0x1

    iput v0, p0, Lgaz;->B:I

    .line 53
    new-instance v0, Lgbk;

    invoke-direct {v0, p1, p2, p3}, Lgbk;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lgaz;->A:Lgbk;

    .line 54
    return-void
.end method


# virtual methods
.method protected a(III)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 86
    iput p2, p0, Lgaz;->B:I

    .line 87
    iget-object v0, p0, Lgaz;->A:Lgbk;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {p3, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 88
    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 87
    invoke-virtual {v0, v1, v2}, Lgbk;->measure(II)V

    .line 89
    iget-object v0, p0, Lgaz;->A:Lgbk;

    invoke-virtual {v0}, Lgbk;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v0, p2

    return v0
.end method

.method protected a(Landroid/graphics/Canvas;I)I
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lgaz;->A:Lgbk;

    invoke-virtual {v0}, Lgbk;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v0, p2

    return v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 111
    invoke-super {p0}, Lgbz;->a()V

    .line 112
    const/4 v0, -0x1

    iput v0, p0, Lgaz;->B:I

    .line 113
    iget-object v0, p0, Lgaz;->A:Lgbk;

    invoke-virtual {v0}, Lgbk;->a()V

    .line 114
    return-void
.end method

.method protected a(Landroid/database/Cursor;)V
    .locals 3

    .prologue
    .line 58
    const/16 v0, 0x17

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 60
    :try_start_0
    new-instance v1, Lozp;

    invoke-direct {v1}, Lozp;-><init>()V

    invoke-static {v1, v0}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lozp;

    iput-object v0, p0, Lgaz;->y:Lozp;
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    .line 65
    :goto_0
    return-void

    .line 61
    :catch_0
    move-exception v0

    .line 62
    const-string v1, "ShowtimeEventCardViewGroup"

    const-string v2, "Unable to parse HoaPlusEvent from the stored event."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public a(Ljava/lang/StringBuilder;)V
    .locals 3

    .prologue
    .line 118
    iget-object v0, p0, Lgaz;->A:Lgbk;

    if-eqz v0, :cond_0

    .line 119
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/CharSequence;

    const/4 v1, 0x0

    iget-object v2, p0, Lgaz;->A:Lgbk;

    .line 120
    invoke-virtual {v2}, Lgbk;->b()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 119
    invoke-static {p1, v0}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 122
    :cond_0
    return-void
.end method

.method protected aD_()Z
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x1

    return v0
.end method

.method protected a_(Landroid/database/Cursor;Llcr;I)V
    .locals 6

    .prologue
    .line 70
    iget-object v0, p0, Lgaz;->y:Lozp;

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lgaz;->A:Lgbk;

    invoke-virtual {p0, v0}, Lgaz;->removeView(Landroid/view/View;)V

    .line 72
    iget-object v0, p0, Lgaz;->A:Lgbk;

    invoke-virtual {p0, v0}, Lgaz;->addView(Landroid/view/View;)V

    .line 73
    iget v0, p0, Lgaz;->o:I

    invoke-virtual {p0, p2, v0}, Lgaz;->a(Lhuk;I)I

    .line 74
    iget-object v0, p0, Lgaz;->A:Lgbk;

    iget-object v1, p0, Lgaz;->y:Lozp;

    iget-object v2, p0, Lgaz;->d:Ljava/lang/String;

    iget-object v3, p0, Lgaz;->e:Ljava/lang/String;

    iget-wide v4, p0, Lgaz;->f:J

    invoke-virtual {v0, v1, v2, v3}, Lgbk;->a(Lozp;Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    :cond_0
    return-void
.end method

.method public b(Z)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 126
    invoke-super {p0, p1}, Lgbz;->b(Z)Landroid/content/Intent;

    move-result-object v0

    .line 127
    if-eqz v0, :cond_0

    .line 128
    const-string v1, "event_id"

    iget-object v2, p0, Lgaz;->y:Lozp;

    iget-object v2, v2, Lozp;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 129
    const-string v1, "owner_id"

    iget-object v2, p0, Lgaz;->y:Lozp;

    iget-object v2, v2, Lozp;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 131
    :cond_0
    return-object v0
.end method

.method protected onLayout(ZIIII)V
    .locals 6

    .prologue
    .line 99
    invoke-super/range {p0 .. p5}, Lgbz;->onLayout(ZIIII)V

    .line 100
    iget-object v0, p0, Lgaz;->A:Lgbk;

    invoke-virtual {v0}, Lgbk;->getMeasuredHeight()I

    move-result v0

    .line 101
    iget v1, p0, Lgaz;->B:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    if-eqz v0, :cond_0

    .line 102
    iget v1, p0, Lgaz;->m:I

    iget-object v2, p0, Lgaz;->q:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v1, v2

    iget-object v2, p0, Lgaz;->q:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    sub-int/2addr v1, v2

    .line 104
    iget-object v2, p0, Lgaz;->A:Lgbk;

    iget-object v3, p0, Lgaz;->q:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget v4, p0, Lgaz;->B:I

    iget-object v5, p0, Lgaz;->q:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v5

    iget v5, p0, Lgaz;->B:I

    add-int/2addr v0, v5

    invoke-virtual {v2, v3, v4, v1, v0}, Lgbk;->layout(IIII)V

    .line 107
    :cond_0
    return-void
.end method
