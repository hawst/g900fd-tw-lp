.class public final Lmtg;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lmtj;

.field public b:Lmsn;

.field private c:[Lmsn;

.field private d:[Lmso;

.field private e:[Lmte;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2013
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2016
    sget-object v0, Lmtj;->a:[Lmtj;

    iput-object v0, p0, Lmtg;->a:[Lmtj;

    .line 2019
    sget-object v0, Lmsn;->a:[Lmsn;

    iput-object v0, p0, Lmtg;->c:[Lmsn;

    .line 2022
    sget-object v0, Lmso;->a:[Lmso;

    iput-object v0, p0, Lmtg;->d:[Lmso;

    .line 2025
    sget-object v0, Lmte;->a:[Lmte;

    iput-object v0, p0, Lmtg;->e:[Lmte;

    .line 2028
    const/4 v0, 0x0

    iput-object v0, p0, Lmtg;->b:Lmsn;

    .line 2013
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2070
    .line 2071
    iget-object v0, p0, Lmtg;->a:[Lmtj;

    if-eqz v0, :cond_1

    .line 2072
    iget-object v3, p0, Lmtg;->a:[Lmtj;

    array-length v4, v3

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 2073
    if-eqz v5, :cond_0

    .line 2074
    const/4 v6, 0x1

    .line 2075
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 2072
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 2079
    :cond_2
    iget-object v2, p0, Lmtg;->c:[Lmsn;

    if-eqz v2, :cond_4

    .line 2080
    iget-object v3, p0, Lmtg;->c:[Lmsn;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_4

    aget-object v5, v3, v2

    .line 2081
    if-eqz v5, :cond_3

    .line 2082
    const/4 v6, 0x2

    .line 2083
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 2080
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2087
    :cond_4
    iget-object v2, p0, Lmtg;->d:[Lmso;

    if-eqz v2, :cond_6

    .line 2088
    iget-object v3, p0, Lmtg;->d:[Lmso;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_6

    aget-object v5, v3, v2

    .line 2089
    if-eqz v5, :cond_5

    .line 2090
    const/4 v6, 0x3

    .line 2091
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 2088
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 2095
    :cond_6
    iget-object v2, p0, Lmtg;->e:[Lmte;

    if-eqz v2, :cond_8

    .line 2096
    iget-object v2, p0, Lmtg;->e:[Lmte;

    array-length v3, v2

    :goto_3
    if-ge v1, v3, :cond_8

    aget-object v4, v2, v1

    .line 2097
    if-eqz v4, :cond_7

    .line 2098
    const/4 v5, 0x4

    .line 2099
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 2096
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 2103
    :cond_8
    iget-object v1, p0, Lmtg;->b:Lmsn;

    if-eqz v1, :cond_9

    .line 2104
    const/4 v1, 0x5

    iget-object v2, p0, Lmtg;->b:Lmsn;

    .line 2105
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2107
    :cond_9
    iget-object v1, p0, Lmtg;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2108
    iput v0, p0, Lmtg;->ai:I

    .line 2109
    return v0
.end method

.method public a(Loxn;)Lmtg;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2117
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2118
    sparse-switch v0, :sswitch_data_0

    .line 2122
    iget-object v2, p0, Lmtg;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 2123
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmtg;->ah:Ljava/util/List;

    .line 2126
    :cond_1
    iget-object v2, p0, Lmtg;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2128
    :sswitch_0
    return-object p0

    .line 2133
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 2134
    iget-object v0, p0, Lmtg;->a:[Lmtj;

    if-nez v0, :cond_3

    move v0, v1

    .line 2135
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lmtj;

    .line 2136
    iget-object v3, p0, Lmtg;->a:[Lmtj;

    if-eqz v3, :cond_2

    .line 2137
    iget-object v3, p0, Lmtg;->a:[Lmtj;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2139
    :cond_2
    iput-object v2, p0, Lmtg;->a:[Lmtj;

    .line 2140
    :goto_2
    iget-object v2, p0, Lmtg;->a:[Lmtj;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 2141
    iget-object v2, p0, Lmtg;->a:[Lmtj;

    new-instance v3, Lmtj;

    invoke-direct {v3}, Lmtj;-><init>()V

    aput-object v3, v2, v0

    .line 2142
    iget-object v2, p0, Lmtg;->a:[Lmtj;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 2143
    invoke-virtual {p1}, Loxn;->a()I

    .line 2140
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2134
    :cond_3
    iget-object v0, p0, Lmtg;->a:[Lmtj;

    array-length v0, v0

    goto :goto_1

    .line 2146
    :cond_4
    iget-object v2, p0, Lmtg;->a:[Lmtj;

    new-instance v3, Lmtj;

    invoke-direct {v3}, Lmtj;-><init>()V

    aput-object v3, v2, v0

    .line 2147
    iget-object v2, p0, Lmtg;->a:[Lmtj;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2151
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 2152
    iget-object v0, p0, Lmtg;->c:[Lmsn;

    if-nez v0, :cond_6

    move v0, v1

    .line 2153
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lmsn;

    .line 2154
    iget-object v3, p0, Lmtg;->c:[Lmsn;

    if-eqz v3, :cond_5

    .line 2155
    iget-object v3, p0, Lmtg;->c:[Lmsn;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2157
    :cond_5
    iput-object v2, p0, Lmtg;->c:[Lmsn;

    .line 2158
    :goto_4
    iget-object v2, p0, Lmtg;->c:[Lmsn;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    .line 2159
    iget-object v2, p0, Lmtg;->c:[Lmsn;

    new-instance v3, Lmsn;

    invoke-direct {v3}, Lmsn;-><init>()V

    aput-object v3, v2, v0

    .line 2160
    iget-object v2, p0, Lmtg;->c:[Lmsn;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 2161
    invoke-virtual {p1}, Loxn;->a()I

    .line 2158
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 2152
    :cond_6
    iget-object v0, p0, Lmtg;->c:[Lmsn;

    array-length v0, v0

    goto :goto_3

    .line 2164
    :cond_7
    iget-object v2, p0, Lmtg;->c:[Lmsn;

    new-instance v3, Lmsn;

    invoke-direct {v3}, Lmsn;-><init>()V

    aput-object v3, v2, v0

    .line 2165
    iget-object v2, p0, Lmtg;->c:[Lmsn;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 2169
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 2170
    iget-object v0, p0, Lmtg;->d:[Lmso;

    if-nez v0, :cond_9

    move v0, v1

    .line 2171
    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lmso;

    .line 2172
    iget-object v3, p0, Lmtg;->d:[Lmso;

    if-eqz v3, :cond_8

    .line 2173
    iget-object v3, p0, Lmtg;->d:[Lmso;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2175
    :cond_8
    iput-object v2, p0, Lmtg;->d:[Lmso;

    .line 2176
    :goto_6
    iget-object v2, p0, Lmtg;->d:[Lmso;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_a

    .line 2177
    iget-object v2, p0, Lmtg;->d:[Lmso;

    new-instance v3, Lmso;

    invoke-direct {v3}, Lmso;-><init>()V

    aput-object v3, v2, v0

    .line 2178
    iget-object v2, p0, Lmtg;->d:[Lmso;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 2179
    invoke-virtual {p1}, Loxn;->a()I

    .line 2176
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 2170
    :cond_9
    iget-object v0, p0, Lmtg;->d:[Lmso;

    array-length v0, v0

    goto :goto_5

    .line 2182
    :cond_a
    iget-object v2, p0, Lmtg;->d:[Lmso;

    new-instance v3, Lmso;

    invoke-direct {v3}, Lmso;-><init>()V

    aput-object v3, v2, v0

    .line 2183
    iget-object v2, p0, Lmtg;->d:[Lmso;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 2187
    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 2188
    iget-object v0, p0, Lmtg;->e:[Lmte;

    if-nez v0, :cond_c

    move v0, v1

    .line 2189
    :goto_7
    add-int/2addr v2, v0

    new-array v2, v2, [Lmte;

    .line 2190
    iget-object v3, p0, Lmtg;->e:[Lmte;

    if-eqz v3, :cond_b

    .line 2191
    iget-object v3, p0, Lmtg;->e:[Lmte;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2193
    :cond_b
    iput-object v2, p0, Lmtg;->e:[Lmte;

    .line 2194
    :goto_8
    iget-object v2, p0, Lmtg;->e:[Lmte;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_d

    .line 2195
    iget-object v2, p0, Lmtg;->e:[Lmte;

    new-instance v3, Lmte;

    invoke-direct {v3}, Lmte;-><init>()V

    aput-object v3, v2, v0

    .line 2196
    iget-object v2, p0, Lmtg;->e:[Lmte;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 2197
    invoke-virtual {p1}, Loxn;->a()I

    .line 2194
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 2188
    :cond_c
    iget-object v0, p0, Lmtg;->e:[Lmte;

    array-length v0, v0

    goto :goto_7

    .line 2200
    :cond_d
    iget-object v2, p0, Lmtg;->e:[Lmte;

    new-instance v3, Lmte;

    invoke-direct {v3}, Lmte;-><init>()V

    aput-object v3, v2, v0

    .line 2201
    iget-object v2, p0, Lmtg;->e:[Lmte;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 2205
    :sswitch_5
    iget-object v0, p0, Lmtg;->b:Lmsn;

    if-nez v0, :cond_e

    .line 2206
    new-instance v0, Lmsn;

    invoke-direct {v0}, Lmsn;-><init>()V

    iput-object v0, p0, Lmtg;->b:Lmsn;

    .line 2208
    :cond_e
    iget-object v0, p0, Lmtg;->b:Lmsn;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 2118
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2033
    iget-object v1, p0, Lmtg;->a:[Lmtj;

    if-eqz v1, :cond_1

    .line 2034
    iget-object v2, p0, Lmtg;->a:[Lmtj;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 2035
    if-eqz v4, :cond_0

    .line 2036
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 2034
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2040
    :cond_1
    iget-object v1, p0, Lmtg;->c:[Lmsn;

    if-eqz v1, :cond_3

    .line 2041
    iget-object v2, p0, Lmtg;->c:[Lmsn;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 2042
    if-eqz v4, :cond_2

    .line 2043
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 2041
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2047
    :cond_3
    iget-object v1, p0, Lmtg;->d:[Lmso;

    if-eqz v1, :cond_5

    .line 2048
    iget-object v2, p0, Lmtg;->d:[Lmso;

    array-length v3, v2

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    .line 2049
    if-eqz v4, :cond_4

    .line 2050
    const/4 v5, 0x3

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 2048
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2054
    :cond_5
    iget-object v1, p0, Lmtg;->e:[Lmte;

    if-eqz v1, :cond_7

    .line 2055
    iget-object v1, p0, Lmtg;->e:[Lmte;

    array-length v2, v1

    :goto_3
    if-ge v0, v2, :cond_7

    aget-object v3, v1, v0

    .line 2056
    if-eqz v3, :cond_6

    .line 2057
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 2055
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 2061
    :cond_7
    iget-object v0, p0, Lmtg;->b:Lmsn;

    if-eqz v0, :cond_8

    .line 2062
    const/4 v0, 0x5

    iget-object v1, p0, Lmtg;->b:Lmsn;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2064
    :cond_8
    iget-object v0, p0, Lmtg;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2066
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2009
    invoke-virtual {p0, p1}, Lmtg;->a(Loxn;)Lmtg;

    move-result-object v0

    return-object v0
.end method
