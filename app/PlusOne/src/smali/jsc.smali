.class public final Ljsc;
.super Llol;
.source "PG"

# interfaces
.implements Lhio;
.implements Lhyz;
.implements Lhzq;
.implements Ljpa;


# instance fields
.field private N:Ljsm;

.field private O:Ljqc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljqc",
            "<",
            "Ljod;",
            ">;"
        }
    .end annotation
.end field

.field private P:Lhzb;

.field private Q:Lhei;

.field private R:Lhee;

.field private S:Ljpr;

.field private T:Lhzl;

.field private U:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljod;",
            "Ljava/util/List",
            "<",
            "Ljpv;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private V:Ljqc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljqc",
            "<",
            "Ljod;",
            ">;"
        }
    .end annotation
.end field

.field private W:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljsm;",
            ">;"
        }
    .end annotation
.end field

.field private X:Z

.field private Y:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Llol;-><init>()V

    .line 65
    sget-object v0, Ljof;->b:Ljqc;

    iput-object v0, p0, Ljsc;->O:Ljqc;

    .line 74
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Ljsc;->W:Landroid/util/SparseArray;

    return-void
.end method

.method static synthetic a(Ljsc;)Lhzl;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Ljsc;->T:Lhzl;

    return-object v0
.end method

.method static synthetic b(Ljsc;)V
    .locals 3

    .prologue
    .line 41
    iget-object v0, p0, Ljsc;->Q:Lhei;

    iget-object v1, p0, Ljsc;->R:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    invoke-interface {v0, v1}, Lhei;->b(I)Lhek;

    move-result-object v0

    const-string v1, "minor_public_extended_dialog"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lhek;->b(Ljava/lang/String;Z)Lhek;

    move-result-object v0

    invoke-interface {v0}, Lhek;->c()I

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)I
    .locals 1

    .prologue
    .line 217
    invoke-static {p1}, Lhiu;->a(Landroid/content/Context;)I

    move-result v0

    return v0
.end method

.method public a(I)Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 297
    iget-object v0, p0, Ljsc;->W:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljsm;

    .line 298
    if-nez v0, :cond_0

    .line 299
    invoke-static {}, Ljsm;->a()Ljso;

    move-result-object v1

    iget-object v0, p0, Ljsc;->U:Ljava/util/List;

    .line 300
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljod;

    invoke-virtual {v1, v0}, Ljso;->a(Ljod;)Ljso;

    move-result-object v0

    .line 301
    invoke-virtual {v0}, Ljso;->a()Ljsm;

    move-result-object v0

    .line 302
    iget-object v1, p0, Ljsc;->W:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 304
    :cond_0
    return-object v0
.end method

.method public a(ILandroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 263
    iget-object v0, p0, Ljsc;->U:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljod;

    .line 264
    iget-object v1, p0, Ljsc;->U:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/util/List;

    .line 265
    check-cast p2, Lhiu;

    .line 266
    invoke-interface {v0}, Ljod;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Lhiu;->a(Ljava/lang/String;)V

    .line 267
    invoke-virtual {p2}, Lhiu;->a()Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;

    .line 270
    invoke-interface {v0}, Ljod;->c()I

    move-result v3

    if-ne v3, v4, :cond_0

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 271
    invoke-virtual {v2, v1}, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;->a(Ljava/util/List;)Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;

    .line 291
    :goto_0
    invoke-virtual {v2}, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;->a()Lhis;

    move-result-object v0

    invoke-virtual {p0, p1}, Ljsc;->a(I)Landroid/os/Parcelable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhis;->a(Landroid/os/Parcelable;)V

    .line 292
    return-void

    .line 277
    :cond_0
    invoke-interface {v0}, Ljod;->c()I

    move-result v0

    .line 278
    packed-switch v0, :pswitch_data_0

    .line 288
    :pswitch_0
    invoke-virtual {v2, v0}, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;->a(I)Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;

    goto :goto_0

    .line 281
    :pswitch_1
    iget-boolean v1, p0, Ljsc;->Y:Z

    if-eqz v1, :cond_1

    .line 282
    invoke-virtual {v2, v0, v4}, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;->a(IZ)Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;

    goto :goto_0

    .line 284
    :cond_1
    invoke-virtual {v2, v0}, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;->a(I)Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;

    goto :goto_0

    .line 278
    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Landroid/os/Parcelable;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 80
    iget-object v0, p0, Ljsc;->T:Lhzl;

    if-eqz v0, :cond_0

    instance-of v0, p1, Ljsm;

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Ljsc;->T:Lhzl;

    invoke-interface {v0, p1}, Lhzl;->c(Landroid/os/Parcelable;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 82
    iget-object v0, p0, Ljsc;->T:Lhzl;

    invoke-interface {v0, p1}, Lhzl;->b(Landroid/os/Parcelable;)Z

    .line 92
    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v0, p1

    .line 84
    check-cast v0, Ljsm;

    new-instance v3, Ljsd;

    invoke-direct {v3, p0, p1}, Ljsd;-><init>(Ljsc;Landroid/os/Parcelable;)V

    iget-boolean v1, p0, Ljsc;->Y:Z

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Ljsm;->b()Ljod;

    move-result-object v1

    invoke-interface {v1}, Ljod;->c()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    move v1, v2

    :goto_1
    if-eqz v1, :cond_2

    iget-object v1, p0, Ljsc;->R:Lhee;

    invoke-interface {v1}, Lhee;->g()Lhej;

    move-result-object v1

    const-string v4, "minor_public_extended_dialog"

    invoke-interface {v1, v4}, Lhej;->c(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Ljsc;->at:Llnl;

    invoke-virtual {v1}, Llnl;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0}, Ljsm;->b()Ljod;

    move-result-object v0

    invoke-interface {v0}, Ljod;->b()Ljava/lang/String;

    move-result-object v0

    const v4, 0x7f0a04f3

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v5, 0x104000a

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/high16 v6, 0x1040000

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v4, v5, v1}, Llgr;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)Llgr;

    move-result-object v0

    invoke-virtual {p0}, Ljsc;->r()Lu;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Llgr;->a(Lu;I)V

    invoke-virtual {p0}, Ljsc;->r()Lu;

    move-result-object v1

    invoke-virtual {v1}, Lu;->p()Lae;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Llgr;->a(Lae;Ljava/lang/String;)V

    new-instance v1, Ljse;

    invoke-direct {v1, p0, v3}, Ljse;-><init>(Ljsc;Ljsf;)V

    invoke-virtual {v0, v1}, Llgr;->a(Llgs;)V

    goto :goto_0

    :pswitch_1
    const/4 v1, 0x1

    goto :goto_1

    :cond_2
    invoke-interface {v3}, Ljsf;->a()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 235
    return-void
.end method

.method public a(Lhzb;)V
    .locals 0

    .prologue
    .line 314
    iput-object p1, p0, Ljsc;->P:Lhzb;

    .line 315
    return-void
.end method

.method public a(Ljqh;)V
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 146
    .line 147
    invoke-interface {p1}, Ljqh;->a()Ljava/util/List;

    move-result-object v6

    .line 148
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    .line 149
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8, v7}, Ljava/util/ArrayList;-><init>(I)V

    .line 151
    invoke-interface {p1}, Ljqh;->b()V

    .line 157
    const/4 v0, 0x0

    move v5, v0

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    :goto_0
    if-ge v5, v7, :cond_5

    .line 158
    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljod;

    .line 159
    invoke-interface {v0}, Ljod;->c()I

    move-result v9

    const/16 v10, 0x9

    if-ne v9, v10, :cond_0

    .line 160
    invoke-static {}, Ljsm;->a()Ljso;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljso;->a(Ljod;)Ljso;

    move-result-object v0

    invoke-virtual {v0}, Ljso;->a()Ljsm;

    move-result-object v0

    iput-object v0, p0, Ljsc;->N:Ljsm;

    .line 161
    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    move-object v11, v1

    move-object v1, v2

    move-object v2, v3

    move-object v3, v0

    move-object v0, v11

    .line 157
    :goto_1
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    move-object v4, v3

    move-object v3, v2

    move-object v2, v1

    move-object v1, v0

    goto :goto_0

    .line 162
    :cond_0
    invoke-interface {v0}, Ljod;->c()I

    move-result v9

    const/16 v10, 0x8

    if-ne v9, v10, :cond_1

    .line 163
    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    move-object v3, v4

    move-object v11, v2

    move-object v2, v0

    move-object v0, v1

    move-object v1, v11

    goto :goto_1

    .line 164
    :cond_1
    invoke-interface {v0}, Ljod;->c()I

    move-result v9

    const/4 v10, 0x5

    if-ne v9, v10, :cond_2

    .line 165
    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    move-object v2, v3

    move-object v3, v4

    move-object v11, v1

    move-object v1, v0

    move-object v0, v11

    goto :goto_1

    .line 166
    :cond_2
    invoke-interface {v0}, Ljod;->c()I

    move-result v9

    const/4 v10, 0x7

    if-ne v9, v10, :cond_3

    .line 167
    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    move-object v1, v2

    move-object v2, v3

    move-object v3, v4

    goto :goto_1

    .line 168
    :cond_3
    iget-object v9, p0, Ljsc;->V:Ljqc;

    invoke-interface {v9, v0}, Ljqc;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 169
    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    move-object v0, v1

    move-object v1, v2

    move-object v2, v3

    move-object v3, v4

    goto :goto_1

    .line 173
    :cond_5
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v7}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Ljsc;->U:Ljava/util/List;

    if-eqz v4, :cond_6

    iget-object v0, p0, Ljsc;->V:Ljqc;

    iget-object v5, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-interface {v0, v5}, Ljqc;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Ljsc;->U:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_6
    if-eqz v3, :cond_7

    iget-object v0, p0, Ljsc;->V:Ljqc;

    iget-object v4, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-interface {v0, v4}, Ljqc;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Ljsc;->U:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_7
    if-eqz v2, :cond_8

    iget-object v0, p0, Ljsc;->V:Ljqc;

    iget-object v3, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-interface {v0, v3}, Ljqc;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Ljsc;->U:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_8
    iget-object v0, p0, Ljsc;->U:Ljava/util/List;

    invoke-interface {v0, v8}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    if-eqz v1, :cond_9

    iget-object v0, p0, Ljsc;->V:Ljqc;

    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-interface {v0, v2}, Ljqc;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Ljsc;->U:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 176
    :cond_9
    iget-object v0, p0, Ljsc;->P:Lhzb;

    if-eqz v0, :cond_a

    .line 177
    iget-object v0, p0, Ljsc;->P:Lhzb;

    invoke-interface {v0}, Lhzb;->e()V

    .line 179
    :cond_a
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 96
    if-eqz p1, :cond_1

    sget-object v0, Ljof;->d:Ljqc;

    :goto_0
    iput-object v0, p0, Ljsc;->V:Ljqc;

    .line 97
    iget-object v0, p0, Ljsc;->T:Lhzl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljsc;->N:Ljsm;

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Ljsc;->V:Ljqc;

    sget-object v1, Ljof;->d:Ljqc;

    if-ne v0, v1, :cond_2

    .line 99
    iget-object v0, p0, Ljsc;->T:Lhzl;

    iget-object v1, p0, Ljsc;->N:Ljsm;

    invoke-interface {v0, v1}, Lhzl;->c(Landroid/os/Parcelable;)Z

    move-result v0

    iput-boolean v0, p0, Ljsc;->X:Z

    .line 100
    iget-object v0, p0, Ljsc;->T:Lhzl;

    iget-object v1, p0, Ljsc;->N:Ljsm;

    invoke-interface {v0, v1}, Lhzl;->b(Landroid/os/Parcelable;)Z

    .line 105
    :cond_0
    :goto_1
    invoke-virtual {p0}, Ljsc;->e()V

    .line 106
    return-void

    .line 96
    :cond_1
    iget-object v0, p0, Ljsc;->O:Ljqc;

    goto :goto_0

    .line 101
    :cond_2
    iget-object v0, p0, Ljsc;->V:Ljqc;

    sget-object v1, Ljof;->d:Ljqc;

    if-eq v0, v1, :cond_0

    iget-boolean v0, p0, Ljsc;->X:Z

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Ljsc;->T:Lhzl;

    iget-object v1, p0, Ljsc;->N:Ljsm;

    invoke-interface {v0, v1}, Lhzl;->a(Landroid/os/Parcelable;)Z

    goto :goto_1
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 239
    const/4 v0, 0x0

    return v0
.end method

.method public b()Landroid/view/View;
    .locals 1

    .prologue
    .line 246
    const/4 v0, 0x0

    return-object v0
.end method

.method public b(Landroid/content/Context;)Landroid/view/View;
    .locals 2

    .prologue
    .line 256
    new-instance v0, Lhiu;

    invoke-direct {v0, p1}, Lhiu;-><init>(Landroid/content/Context;)V

    .line 257
    new-instance v1, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;

    invoke-direct {v1, p1}, Lcom/google/android/libraries/social/people/providers/acl/CircleAvatarView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lhiu;->a(Landroid/view/View;)V

    .line 258
    return-object v0
.end method

.method public c(Landroid/content/Context;)Landroid/view/View;
    .locals 1

    .prologue
    .line 229
    const/4 v0, 0x0

    return-object v0
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 119
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 120
    iget-object v0, p0, Ljsc;->au:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Ljsc;->R:Lhee;

    .line 121
    iget-object v0, p0, Ljsc;->au:Llnh;

    const-class v1, Lhei;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Ljsc;->Q:Lhei;

    .line 122
    iget-object v0, p0, Ljsc;->au:Llnh;

    const-class v1, Ljpr;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljpr;

    iput-object v0, p0, Ljsc;->S:Ljpr;

    .line 123
    iget-object v0, p0, Ljsc;->au:Llnh;

    const-class v1, Lhzl;

    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhzl;

    iput-object v0, p0, Ljsc;->T:Lhzl;

    .line 124
    iget-object v0, p0, Ljsc;->R:Lhee;

    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v0

    const-string v1, "is_child"

    invoke-interface {v0, v1}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Ljsc;->Y:Z

    .line 127
    iget-object v0, p0, Ljsc;->au:Llnh;

    const-class v1, Lhin;

    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhin;

    .line 128
    if-eqz v0, :cond_0

    .line 129
    invoke-virtual {v0, p0}, Lhin;->a(Lhio;)V

    .line 133
    :cond_0
    iget-object v0, p0, Ljsc;->au:Llnh;

    const-class v1, Ljsb;

    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljsb;

    .line 134
    if-eqz v0, :cond_1

    .line 135
    iget-object v1, v0, Ljsb;->a:Ljqc;

    if-eqz v1, :cond_1

    .line 136
    iget-object v0, v0, Ljsb;->a:Ljqc;

    iput-object v0, p0, Ljsc;->O:Ljqc;

    .line 137
    iget-object v0, p0, Ljsc;->V:Ljqc;

    if-nez v0, :cond_1

    .line 138
    iget-object v0, p0, Ljsc;->O:Ljqc;

    iput-object v0, p0, Ljsc;->V:Ljqc;

    .line 142
    :cond_1
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 222
    const/4 v0, 0x0

    return v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Ljsc;->U:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Ljsc;->U:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public e()V
    .locals 4

    .prologue
    .line 319
    iget-object v0, p0, Ljsc;->S:Ljpr;

    if-eqz v0, :cond_0

    .line 320
    iget-object v0, p0, Ljsc;->W:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 321
    iget-object v0, p0, Ljsc;->S:Ljpr;

    iget-object v1, p0, Ljsc;->R:Lhee;

    .line 322
    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    const/4 v2, 0x3

    iget-object v3, p0, Ljsc;->V:Ljqc;

    .line 321
    invoke-interface {v0, p0, v1, v2, v3}, Ljpr;->a(Ljpa;IILjqc;)V

    .line 324
    :cond_0
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 110
    invoke-super {p0}, Llol;->g()V

    .line 111
    iget-object v0, p0, Ljsc;->V:Ljqc;

    if-nez v0, :cond_0

    .line 112
    iget-object v0, p0, Ljsc;->O:Ljqc;

    iput-object v0, p0, Ljsc;->V:Ljqc;

    .line 114
    :cond_0
    invoke-virtual {p0}, Ljsc;->e()V

    .line 115
    return-void
.end method
