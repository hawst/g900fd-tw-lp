.class public final Lood;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:[Lonr;

.field public c:[Ljava/lang/Long;

.field public d:[Ljava/lang/Boolean;

.field public e:[Lons;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Looe;

.field public i:Ljava/lang/Integer;

.field public j:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1477
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1482
    sget-object v0, Lonr;->a:[Lonr;

    iput-object v0, p0, Lood;->b:[Lonr;

    .line 1485
    sget-object v0, Loxx;->h:[Ljava/lang/Long;

    iput-object v0, p0, Lood;->c:[Ljava/lang/Long;

    .line 1488
    sget-object v0, Loxx;->k:[Ljava/lang/Boolean;

    iput-object v0, p0, Lood;->d:[Ljava/lang/Boolean;

    .line 1491
    sget-object v0, Lons;->a:[Lons;

    iput-object v0, p0, Lood;->e:[Lons;

    .line 1498
    const/4 v0, 0x0

    iput-object v0, p0, Lood;->h:Looe;

    .line 1477
    return-void
.end method


# virtual methods
.method public a()I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1555
    .line 1556
    iget-object v0, p0, Lood;->b:[Lonr;

    if-eqz v0, :cond_1

    .line 1557
    iget-object v3, p0, Lood;->b:[Lonr;

    array-length v4, v3

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 1558
    if-eqz v5, :cond_0

    .line 1559
    const/4 v6, 0x1

    .line 1560
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 1557
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 1564
    :cond_2
    iget-object v2, p0, Lood;->f:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 1565
    const/4 v2, 0x2

    iget-object v3, p0, Lood;->f:Ljava/lang/String;

    .line 1566
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1568
    :cond_3
    iget-object v2, p0, Lood;->h:Looe;

    if-eqz v2, :cond_4

    .line 1569
    const/4 v2, 0x3

    iget-object v3, p0, Lood;->h:Looe;

    .line 1570
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1572
    :cond_4
    iget-object v2, p0, Lood;->i:Ljava/lang/Integer;

    if-eqz v2, :cond_5

    .line 1573
    const/4 v2, 0x4

    iget-object v3, p0, Lood;->i:Ljava/lang/Integer;

    .line 1574
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 1576
    :cond_5
    iget-object v2, p0, Lood;->j:Ljava/lang/Integer;

    if-eqz v2, :cond_6

    .line 1577
    const/4 v2, 0x5

    iget-object v3, p0, Lood;->j:Ljava/lang/Integer;

    .line 1578
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 1580
    :cond_6
    iget-object v2, p0, Lood;->a:Ljava/lang/Integer;

    if-eqz v2, :cond_7

    .line 1581
    const/4 v2, 0x6

    iget-object v3, p0, Lood;->a:Ljava/lang/Integer;

    .line 1582
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 1584
    :cond_7
    iget-object v2, p0, Lood;->c:[Ljava/lang/Long;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lood;->c:[Ljava/lang/Long;

    array-length v2, v2

    if-lez v2, :cond_9

    .line 1586
    iget-object v4, p0, Lood;->c:[Ljava/lang/Long;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_1
    if-ge v2, v5, :cond_8

    aget-object v6, v4, v2

    .line 1588
    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Loxo;->f(J)I

    move-result v6

    add-int/2addr v3, v6

    .line 1586
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1590
    :cond_8
    add-int/2addr v0, v3

    .line 1591
    iget-object v2, p0, Lood;->c:[Ljava/lang/Long;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1593
    :cond_9
    iget-object v2, p0, Lood;->d:[Ljava/lang/Boolean;

    if-eqz v2, :cond_a

    iget-object v2, p0, Lood;->d:[Ljava/lang/Boolean;

    array-length v2, v2

    if-lez v2, :cond_a

    .line 1594
    iget-object v2, p0, Lood;->d:[Ljava/lang/Boolean;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    .line 1595
    add-int/2addr v0, v2

    .line 1596
    iget-object v2, p0, Lood;->d:[Ljava/lang/Boolean;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1598
    :cond_a
    iget-object v2, p0, Lood;->e:[Lons;

    if-eqz v2, :cond_c

    .line 1599
    iget-object v2, p0, Lood;->e:[Lons;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_c

    aget-object v4, v2, v1

    .line 1600
    if-eqz v4, :cond_b

    .line 1601
    const/16 v5, 0x9

    .line 1602
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 1599
    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1606
    :cond_c
    iget-object v1, p0, Lood;->g:Ljava/lang/String;

    if-eqz v1, :cond_d

    .line 1607
    const/16 v1, 0xa

    iget-object v2, p0, Lood;->g:Ljava/lang/String;

    .line 1608
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1610
    :cond_d
    iget-object v1, p0, Lood;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1611
    iput v0, p0, Lood;->ai:I

    .line 1612
    return v0
.end method

.method public a(Loxn;)Lood;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1620
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1621
    sparse-switch v0, :sswitch_data_0

    .line 1625
    iget-object v2, p0, Lood;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 1626
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lood;->ah:Ljava/util/List;

    .line 1629
    :cond_1
    iget-object v2, p0, Lood;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1631
    :sswitch_0
    return-object p0

    .line 1636
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1637
    iget-object v0, p0, Lood;->b:[Lonr;

    if-nez v0, :cond_3

    move v0, v1

    .line 1638
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lonr;

    .line 1639
    iget-object v3, p0, Lood;->b:[Lonr;

    if-eqz v3, :cond_2

    .line 1640
    iget-object v3, p0, Lood;->b:[Lonr;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1642
    :cond_2
    iput-object v2, p0, Lood;->b:[Lonr;

    .line 1643
    :goto_2
    iget-object v2, p0, Lood;->b:[Lonr;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 1644
    iget-object v2, p0, Lood;->b:[Lonr;

    new-instance v3, Lonr;

    invoke-direct {v3}, Lonr;-><init>()V

    aput-object v3, v2, v0

    .line 1645
    iget-object v2, p0, Lood;->b:[Lonr;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1646
    invoke-virtual {p1}, Loxn;->a()I

    .line 1643
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1637
    :cond_3
    iget-object v0, p0, Lood;->b:[Lonr;

    array-length v0, v0

    goto :goto_1

    .line 1649
    :cond_4
    iget-object v2, p0, Lood;->b:[Lonr;

    new-instance v3, Lonr;

    invoke-direct {v3}, Lonr;-><init>()V

    aput-object v3, v2, v0

    .line 1650
    iget-object v2, p0, Lood;->b:[Lonr;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1654
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lood;->f:Ljava/lang/String;

    goto :goto_0

    .line 1658
    :sswitch_3
    iget-object v0, p0, Lood;->h:Looe;

    if-nez v0, :cond_5

    .line 1659
    new-instance v0, Looe;

    invoke-direct {v0}, Looe;-><init>()V

    iput-object v0, p0, Lood;->h:Looe;

    .line 1661
    :cond_5
    iget-object v0, p0, Lood;->h:Looe;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1665
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lood;->i:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 1669
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lood;->j:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 1673
    :sswitch_6
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lood;->a:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 1677
    :sswitch_7
    const/16 v0, 0x38

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1678
    iget-object v0, p0, Lood;->c:[Ljava/lang/Long;

    array-length v0, v0

    .line 1679
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/Long;

    .line 1680
    iget-object v3, p0, Lood;->c:[Ljava/lang/Long;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1681
    iput-object v2, p0, Lood;->c:[Ljava/lang/Long;

    .line 1682
    :goto_3
    iget-object v2, p0, Lood;->c:[Ljava/lang/Long;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 1683
    iget-object v2, p0, Lood;->c:[Ljava/lang/Long;

    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v0

    .line 1684
    invoke-virtual {p1}, Loxn;->a()I

    .line 1682
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 1687
    :cond_6
    iget-object v2, p0, Lood;->c:[Ljava/lang/Long;

    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    .line 1691
    :sswitch_8
    const/16 v0, 0x40

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1692
    iget-object v0, p0, Lood;->d:[Ljava/lang/Boolean;

    array-length v0, v0

    .line 1693
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/Boolean;

    .line 1694
    iget-object v3, p0, Lood;->d:[Ljava/lang/Boolean;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1695
    iput-object v2, p0, Lood;->d:[Ljava/lang/Boolean;

    .line 1696
    :goto_4
    iget-object v2, p0, Lood;->d:[Ljava/lang/Boolean;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    .line 1697
    iget-object v2, p0, Lood;->d:[Ljava/lang/Boolean;

    invoke-virtual {p1}, Loxn;->j()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v0

    .line 1698
    invoke-virtual {p1}, Loxn;->a()I

    .line 1696
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 1701
    :cond_7
    iget-object v2, p0, Lood;->d:[Ljava/lang/Boolean;

    invoke-virtual {p1}, Loxn;->j()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    .line 1705
    :sswitch_9
    const/16 v0, 0x4a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1706
    iget-object v0, p0, Lood;->e:[Lons;

    if-nez v0, :cond_9

    move v0, v1

    .line 1707
    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lons;

    .line 1708
    iget-object v3, p0, Lood;->e:[Lons;

    if-eqz v3, :cond_8

    .line 1709
    iget-object v3, p0, Lood;->e:[Lons;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1711
    :cond_8
    iput-object v2, p0, Lood;->e:[Lons;

    .line 1712
    :goto_6
    iget-object v2, p0, Lood;->e:[Lons;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_a

    .line 1713
    iget-object v2, p0, Lood;->e:[Lons;

    new-instance v3, Lons;

    invoke-direct {v3}, Lons;-><init>()V

    aput-object v3, v2, v0

    .line 1714
    iget-object v2, p0, Lood;->e:[Lons;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1715
    invoke-virtual {p1}, Loxn;->a()I

    .line 1712
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 1706
    :cond_9
    iget-object v0, p0, Lood;->e:[Lons;

    array-length v0, v0

    goto :goto_5

    .line 1718
    :cond_a
    iget-object v2, p0, Lood;->e:[Lons;

    new-instance v3, Lons;

    invoke-direct {v3}, Lons;-><init>()V

    aput-object v3, v2, v0

    .line 1719
    iget-object v2, p0, Lood;->e:[Lons;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1723
    :sswitch_a
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lood;->g:Ljava/lang/String;

    goto/16 :goto_0

    .line 1621
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 1507
    iget-object v1, p0, Lood;->b:[Lonr;

    if-eqz v1, :cond_1

    .line 1508
    iget-object v2, p0, Lood;->b:[Lonr;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 1509
    if-eqz v4, :cond_0

    .line 1510
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 1508
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1514
    :cond_1
    iget-object v1, p0, Lood;->f:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1515
    const/4 v1, 0x2

    iget-object v2, p0, Lood;->f:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 1517
    :cond_2
    iget-object v1, p0, Lood;->h:Looe;

    if-eqz v1, :cond_3

    .line 1518
    const/4 v1, 0x3

    iget-object v2, p0, Lood;->h:Looe;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 1520
    :cond_3
    iget-object v1, p0, Lood;->i:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 1521
    const/4 v1, 0x4

    iget-object v2, p0, Lood;->i:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 1523
    :cond_4
    iget-object v1, p0, Lood;->j:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 1524
    const/4 v1, 0x5

    iget-object v2, p0, Lood;->j:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 1526
    :cond_5
    iget-object v1, p0, Lood;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 1527
    const/4 v1, 0x6

    iget-object v2, p0, Lood;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 1529
    :cond_6
    iget-object v1, p0, Lood;->c:[Ljava/lang/Long;

    if-eqz v1, :cond_7

    .line 1530
    iget-object v2, p0, Lood;->c:[Ljava/lang/Long;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_7

    aget-object v4, v2, v1

    .line 1531
    const/4 v5, 0x7

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {p1, v5, v6, v7}, Loxo;->b(IJ)V

    .line 1530
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1534
    :cond_7
    iget-object v1, p0, Lood;->d:[Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    .line 1535
    iget-object v2, p0, Lood;->d:[Ljava/lang/Boolean;

    array-length v3, v2

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_8

    aget-object v4, v2, v1

    .line 1536
    const/16 v5, 0x8

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-virtual {p1, v5, v4}, Loxo;->a(IZ)V

    .line 1535
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1539
    :cond_8
    iget-object v1, p0, Lood;->e:[Lons;

    if-eqz v1, :cond_a

    .line 1540
    iget-object v1, p0, Lood;->e:[Lons;

    array-length v2, v1

    :goto_3
    if-ge v0, v2, :cond_a

    aget-object v3, v1, v0

    .line 1541
    if-eqz v3, :cond_9

    .line 1542
    const/16 v4, 0x9

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 1540
    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 1546
    :cond_a
    iget-object v0, p0, Lood;->g:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 1547
    const/16 v0, 0xa

    iget-object v1, p0, Lood;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1549
    :cond_b
    iget-object v0, p0, Lood;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1551
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1473
    invoke-virtual {p0, p1}, Lood;->a(Loxn;)Lood;

    move-result-object v0

    return-object v0
.end method
