.class public final Litr;
.super Landroid/widget/BaseAdapter;
.source "PG"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Litn;

.field private final c:Z

.field private final d:Ljava/lang/String;

.field private e:[Lnhm;


# direct methods
.method public constructor <init>(Landroid/content/Context;[Lnhm;Litn;ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 27
    iput-object p1, p0, Litr;->a:Landroid/content/Context;

    .line 28
    iput-object p2, p0, Litr;->e:[Lnhm;

    .line 29
    iput-object p3, p0, Litr;->b:Litn;

    .line 30
    iput-boolean p4, p0, Litr;->c:Z

    .line 31
    iput-object p5, p0, Litr;->d:Ljava/lang/String;

    .line 32
    return-void
.end method


# virtual methods
.method public a([Lnhm;)V
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Litr;->e:[Lnhm;

    .line 37
    invoke-virtual {p0}, Litr;->notifyDataSetChanged()V

    .line 38
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Litr;->e:[Lnhm;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Litr;->e:[Lnhm;

    array-length v0, v0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Litr;->e:[Lnhm;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 52
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 58
    instance-of v0, p2, Lcom/google/android/libraries/social/location/FriendLocationsListItemView;

    if-eqz v0, :cond_0

    .line 59
    check-cast p2, Lcom/google/android/libraries/social/location/FriendLocationsListItemView;

    .line 65
    :goto_0
    iget-object v0, p0, Litr;->e:[Lnhm;

    aget-object v7, v0, p1

    .line 66
    iget-boolean v0, p0, Litr;->c:Z

    if-nez v0, :cond_1

    move-object v0, v1

    .line 67
    :goto_1
    iget-object v1, p0, Litr;->b:Litn;

    iget-object v2, p0, Litr;->d:Ljava/lang/String;

    invoke-virtual {p2, v7, v1, v0, v2}, Lcom/google/android/libraries/social/location/FriendLocationsListItemView;->a(Lnhm;Litn;Ljava/lang/CharSequence;Ljava/lang/String;)V

    .line 68
    return-object p2

    .line 61
    :cond_0
    iget-object v0, p0, Litr;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 62
    const v2, 0x7f0400b1

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/location/FriendLocationsListItemView;

    move-object p2, v0

    goto :goto_0

    .line 66
    :cond_1
    iget-object v0, v7, Lnhm;->c:[Lnij;

    invoke-static {v0}, Liuo;->a([Lnij;)Lnij;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v2, v0, Lnij;->e:Ljava/lang/Long;

    if-nez v2, :cond_3

    :cond_2
    move-object v0, v1

    goto :goto_1

    :cond_3
    iget-object v1, p0, Litr;->a:Landroid/content/Context;

    iget-object v0, v0, Lnij;->e:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const v4, 0x7f110034

    const v5, 0x7f110035

    const v6, 0x7f110036

    invoke-static/range {v1 .. v6}, Llhu;->a(Landroid/content/Context;JIII)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_1
.end method
