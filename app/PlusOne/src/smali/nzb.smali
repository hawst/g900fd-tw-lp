.class public final Lnzb;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:I

.field public c:[Lnzc;

.field public d:Ljava/lang/Long;

.field public e:Ljava/lang/String;

.field public f:I

.field private g:Lnyw;

.field private h:Ljava/lang/Boolean;

.field private i:Lnzc;

.field private j:[Lnzc;

.field private k:[I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/high16 v1, -0x80000000

    .line 204
    invoke-direct {p0}, Loxq;-><init>()V

    .line 230
    iput v1, p0, Lnzb;->b:I

    .line 233
    sget-object v0, Lnzc;->a:[Lnzc;

    iput-object v0, p0, Lnzb;->c:[Lnzc;

    .line 238
    iput-object v2, p0, Lnzb;->g:Lnyw;

    .line 245
    iput-object v2, p0, Lnzb;->i:Lnzc;

    .line 248
    iput v1, p0, Lnzb;->f:I

    .line 251
    sget-object v0, Lnzc;->a:[Lnzc;

    iput-object v0, p0, Lnzb;->j:[Lnzc;

    .line 254
    sget-object v0, Loxx;->a:[I

    iput-object v0, p0, Lnzb;->k:[I

    .line 204
    return-void
.end method


# virtual methods
.method public a()I
    .locals 8

    .prologue
    const/high16 v7, -0x80000000

    const/4 v1, 0x0

    .line 308
    .line 309
    iget-object v0, p0, Lnzb;->a:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 310
    const/4 v0, 0x1

    iget-object v2, p0, Lnzb;->a:Ljava/lang/String;

    .line 311
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 313
    :goto_0
    iget v2, p0, Lnzb;->b:I

    if-eq v2, v7, :cond_0

    .line 314
    const/4 v2, 0x2

    iget v3, p0, Lnzb;->b:I

    .line 315
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 317
    :cond_0
    iget-object v2, p0, Lnzb;->c:[Lnzc;

    if-eqz v2, :cond_2

    .line 318
    iget-object v3, p0, Lnzb;->c:[Lnzc;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 319
    if-eqz v5, :cond_1

    .line 320
    const/4 v6, 0x3

    .line 321
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 318
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 325
    :cond_2
    iget-object v2, p0, Lnzb;->d:Ljava/lang/Long;

    if-eqz v2, :cond_3

    .line 326
    const/4 v2, 0x4

    iget-object v3, p0, Lnzb;->d:Ljava/lang/Long;

    .line 327
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Loxo;->f(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 329
    :cond_3
    iget-object v2, p0, Lnzb;->g:Lnyw;

    if-eqz v2, :cond_4

    .line 330
    const/4 v2, 0x5

    iget-object v3, p0, Lnzb;->g:Lnyw;

    .line 331
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 333
    :cond_4
    iget-object v2, p0, Lnzb;->e:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 334
    const/4 v2, 0x6

    iget-object v3, p0, Lnzb;->e:Ljava/lang/String;

    .line 335
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 337
    :cond_5
    iget-object v2, p0, Lnzb;->h:Ljava/lang/Boolean;

    if-eqz v2, :cond_6

    .line 338
    const/4 v2, 0x7

    iget-object v3, p0, Lnzb;->h:Ljava/lang/Boolean;

    .line 339
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 341
    :cond_6
    iget-object v2, p0, Lnzb;->i:Lnzc;

    if-eqz v2, :cond_7

    .line 342
    const/16 v2, 0x8

    iget-object v3, p0, Lnzb;->i:Lnzc;

    .line 343
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 345
    :cond_7
    iget v2, p0, Lnzb;->f:I

    if-eq v2, v7, :cond_8

    .line 346
    const/16 v2, 0x9

    iget v3, p0, Lnzb;->f:I

    .line 347
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 349
    :cond_8
    iget-object v2, p0, Lnzb;->j:[Lnzc;

    if-eqz v2, :cond_a

    .line 350
    iget-object v3, p0, Lnzb;->j:[Lnzc;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_a

    aget-object v5, v3, v2

    .line 351
    if-eqz v5, :cond_9

    .line 352
    const/16 v6, 0xa

    .line 353
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 350
    :cond_9
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 357
    :cond_a
    iget-object v2, p0, Lnzb;->k:[I

    if-eqz v2, :cond_c

    iget-object v2, p0, Lnzb;->k:[I

    array-length v2, v2

    if-lez v2, :cond_c

    .line 359
    iget-object v3, p0, Lnzb;->k:[I

    array-length v4, v3

    move v2, v1

    :goto_3
    if-ge v1, v4, :cond_b

    aget v5, v3, v1

    .line 361
    invoke-static {v5}, Loxo;->i(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 359
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 363
    :cond_b
    add-int/2addr v0, v2

    .line 364
    iget-object v1, p0, Lnzb;->k:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 366
    :cond_c
    iget-object v1, p0, Lnzb;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 367
    iput v0, p0, Lnzb;->ai:I

    .line 368
    return v0

    :cond_d
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lnzb;
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x4

    const/4 v1, 0x0

    .line 376
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 377
    sparse-switch v0, :sswitch_data_0

    .line 381
    iget-object v2, p0, Lnzb;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 382
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnzb;->ah:Ljava/util/List;

    .line 385
    :cond_1
    iget-object v2, p0, Lnzb;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 387
    :sswitch_0
    return-object p0

    .line 392
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnzb;->a:Ljava/lang/String;

    goto :goto_0

    .line 396
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 397
    if-eq v0, v4, :cond_2

    if-eqz v0, :cond_2

    if-eq v0, v5, :cond_2

    if-eq v0, v6, :cond_2

    if-ne v0, v7, :cond_3

    .line 402
    :cond_2
    iput v0, p0, Lnzb;->b:I

    goto :goto_0

    .line 404
    :cond_3
    iput v4, p0, Lnzb;->b:I

    goto :goto_0

    .line 409
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 410
    iget-object v0, p0, Lnzb;->c:[Lnzc;

    if-nez v0, :cond_5

    move v0, v1

    .line 411
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnzc;

    .line 412
    iget-object v3, p0, Lnzb;->c:[Lnzc;

    if-eqz v3, :cond_4

    .line 413
    iget-object v3, p0, Lnzb;->c:[Lnzc;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 415
    :cond_4
    iput-object v2, p0, Lnzb;->c:[Lnzc;

    .line 416
    :goto_2
    iget-object v2, p0, Lnzb;->c:[Lnzc;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 417
    iget-object v2, p0, Lnzb;->c:[Lnzc;

    new-instance v3, Lnzc;

    invoke-direct {v3}, Lnzc;-><init>()V

    aput-object v3, v2, v0

    .line 418
    iget-object v2, p0, Lnzb;->c:[Lnzc;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 419
    invoke-virtual {p1}, Loxn;->a()I

    .line 416
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 410
    :cond_5
    iget-object v0, p0, Lnzb;->c:[Lnzc;

    array-length v0, v0

    goto :goto_1

    .line 422
    :cond_6
    iget-object v2, p0, Lnzb;->c:[Lnzc;

    new-instance v3, Lnzc;

    invoke-direct {v3}, Lnzc;-><init>()V

    aput-object v3, v2, v0

    .line 423
    iget-object v2, p0, Lnzb;->c:[Lnzc;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 427
    :sswitch_4
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lnzb;->d:Ljava/lang/Long;

    goto/16 :goto_0

    .line 431
    :sswitch_5
    iget-object v0, p0, Lnzb;->g:Lnyw;

    if-nez v0, :cond_7

    .line 432
    new-instance v0, Lnyw;

    invoke-direct {v0}, Lnyw;-><init>()V

    iput-object v0, p0, Lnzb;->g:Lnyw;

    .line 434
    :cond_7
    iget-object v0, p0, Lnzb;->g:Lnyw;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 438
    :sswitch_6
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnzb;->e:Ljava/lang/String;

    goto/16 :goto_0

    .line 442
    :sswitch_7
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnzb;->h:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 446
    :sswitch_8
    iget-object v0, p0, Lnzb;->i:Lnzc;

    if-nez v0, :cond_8

    .line 447
    new-instance v0, Lnzc;

    invoke-direct {v0}, Lnzc;-><init>()V

    iput-object v0, p0, Lnzb;->i:Lnzc;

    .line 449
    :cond_8
    iget-object v0, p0, Lnzb;->i:Lnzc;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 453
    :sswitch_9
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 454
    if-eqz v0, :cond_9

    if-eq v0, v5, :cond_9

    if-eq v0, v6, :cond_9

    if-eq v0, v7, :cond_9

    if-ne v0, v4, :cond_a

    .line 459
    :cond_9
    iput v0, p0, Lnzb;->f:I

    goto/16 :goto_0

    .line 461
    :cond_a
    iput v1, p0, Lnzb;->f:I

    goto/16 :goto_0

    .line 466
    :sswitch_a
    const/16 v0, 0x52

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 467
    iget-object v0, p0, Lnzb;->j:[Lnzc;

    if-nez v0, :cond_c

    move v0, v1

    .line 468
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lnzc;

    .line 469
    iget-object v3, p0, Lnzb;->j:[Lnzc;

    if-eqz v3, :cond_b

    .line 470
    iget-object v3, p0, Lnzb;->j:[Lnzc;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 472
    :cond_b
    iput-object v2, p0, Lnzb;->j:[Lnzc;

    .line 473
    :goto_4
    iget-object v2, p0, Lnzb;->j:[Lnzc;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_d

    .line 474
    iget-object v2, p0, Lnzb;->j:[Lnzc;

    new-instance v3, Lnzc;

    invoke-direct {v3}, Lnzc;-><init>()V

    aput-object v3, v2, v0

    .line 475
    iget-object v2, p0, Lnzb;->j:[Lnzc;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 476
    invoke-virtual {p1}, Loxn;->a()I

    .line 473
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 467
    :cond_c
    iget-object v0, p0, Lnzb;->j:[Lnzc;

    array-length v0, v0

    goto :goto_3

    .line 479
    :cond_d
    iget-object v2, p0, Lnzb;->j:[Lnzc;

    new-instance v3, Lnzc;

    invoke-direct {v3}, Lnzc;-><init>()V

    aput-object v3, v2, v0

    .line 480
    iget-object v2, p0, Lnzb;->j:[Lnzc;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 484
    :sswitch_b
    const/16 v0, 0x58

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 485
    iget-object v0, p0, Lnzb;->k:[I

    array-length v0, v0

    .line 486
    add-int/2addr v2, v0

    new-array v2, v2, [I

    .line 487
    iget-object v3, p0, Lnzb;->k:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 488
    iput-object v2, p0, Lnzb;->k:[I

    .line 489
    :goto_5
    iget-object v2, p0, Lnzb;->k:[I

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_e

    .line 490
    iget-object v2, p0, Lnzb;->k:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v3

    aput v3, v2, v0

    .line 491
    invoke-virtual {p1}, Loxn;->a()I

    .line 489
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 494
    :cond_e
    iget-object v2, p0, Lnzb;->k:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v3

    aput v3, v2, v0

    goto/16 :goto_0

    .line 377
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x58 -> :sswitch_b
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 7

    .prologue
    const/high16 v6, -0x80000000

    const/4 v0, 0x0

    .line 259
    iget-object v1, p0, Lnzb;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 260
    const/4 v1, 0x1

    iget-object v2, p0, Lnzb;->a:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 262
    :cond_0
    iget v1, p0, Lnzb;->b:I

    if-eq v1, v6, :cond_1

    .line 263
    const/4 v1, 0x2

    iget v2, p0, Lnzb;->b:I

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 265
    :cond_1
    iget-object v1, p0, Lnzb;->c:[Lnzc;

    if-eqz v1, :cond_3

    .line 266
    iget-object v2, p0, Lnzb;->c:[Lnzc;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 267
    if-eqz v4, :cond_2

    .line 268
    const/4 v5, 0x3

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 266
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 272
    :cond_3
    iget-object v1, p0, Lnzb;->d:Ljava/lang/Long;

    if-eqz v1, :cond_4

    .line 273
    const/4 v1, 0x4

    iget-object v2, p0, Lnzb;->d:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Loxo;->b(IJ)V

    .line 275
    :cond_4
    iget-object v1, p0, Lnzb;->g:Lnyw;

    if-eqz v1, :cond_5

    .line 276
    const/4 v1, 0x5

    iget-object v2, p0, Lnzb;->g:Lnyw;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 278
    :cond_5
    iget-object v1, p0, Lnzb;->e:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 279
    const/4 v1, 0x6

    iget-object v2, p0, Lnzb;->e:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 281
    :cond_6
    iget-object v1, p0, Lnzb;->h:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    .line 282
    const/4 v1, 0x7

    iget-object v2, p0, Lnzb;->h:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IZ)V

    .line 284
    :cond_7
    iget-object v1, p0, Lnzb;->i:Lnzc;

    if-eqz v1, :cond_8

    .line 285
    const/16 v1, 0x8

    iget-object v2, p0, Lnzb;->i:Lnzc;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 287
    :cond_8
    iget v1, p0, Lnzb;->f:I

    if-eq v1, v6, :cond_9

    .line 288
    const/16 v1, 0x9

    iget v2, p0, Lnzb;->f:I

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 290
    :cond_9
    iget-object v1, p0, Lnzb;->j:[Lnzc;

    if-eqz v1, :cond_b

    .line 291
    iget-object v2, p0, Lnzb;->j:[Lnzc;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_b

    aget-object v4, v2, v1

    .line 292
    if-eqz v4, :cond_a

    .line 293
    const/16 v5, 0xa

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 291
    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 297
    :cond_b
    iget-object v1, p0, Lnzb;->k:[I

    if-eqz v1, :cond_c

    iget-object v1, p0, Lnzb;->k:[I

    array-length v1, v1

    if-lez v1, :cond_c

    .line 298
    iget-object v1, p0, Lnzb;->k:[I

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_c

    aget v3, v1, v0

    .line 299
    const/16 v4, 0xb

    invoke-virtual {p1, v4, v3}, Loxo;->a(II)V

    .line 298
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 302
    :cond_c
    iget-object v0, p0, Lnzb;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 304
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 200
    invoke-virtual {p0, p1}, Lnzb;->a(Loxn;)Lnzb;

    move-result-object v0

    return-object v0
.end method
