.class public final Lpjw;
.super Loxq;
.source "PG"


# static fields
.field public static final a:Loxr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Loxr",
            "<",
            "Lpjw;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:Lpjv;

.field private c:Lpjv;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 134
    const v0, 0x2e573d6

    new-instance v1, Lpjx;

    invoke-direct {v1}, Lpjx;-><init>()V

    .line 139
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    move-result-object v0

    sput-object v0, Lpjw;->a:Loxr;

    .line 138
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 162
    const/4 v0, 0x0

    .line 163
    iget-object v1, p0, Lpjw;->b:Lpjv;

    if-eqz v1, :cond_0

    .line 164
    const/4 v0, 0x1

    iget-object v1, p0, Lpjw;->b:Lpjv;

    .line 165
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 167
    :cond_0
    iget-object v1, p0, Lpjw;->c:Lpjv;

    if-eqz v1, :cond_1

    .line 168
    const/4 v1, 0x2

    iget-object v2, p0, Lpjw;->c:Lpjv;

    .line 169
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 171
    :cond_1
    iget-object v1, p0, Lpjw;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 172
    iput v0, p0, Lpjw;->ai:I

    .line 173
    return v0
.end method

.method public a(Loxn;)Lpjw;
    .locals 2

    .prologue
    .line 181
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 182
    sparse-switch v0, :sswitch_data_0

    .line 186
    iget-object v1, p0, Lpjw;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 187
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpjw;->ah:Ljava/util/List;

    .line 190
    :cond_1
    iget-object v1, p0, Lpjw;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 192
    :sswitch_0
    return-object p0

    .line 197
    :sswitch_1
    iget-object v0, p0, Lpjw;->b:Lpjv;

    if-nez v0, :cond_2

    .line 198
    new-instance v0, Lpjv;

    invoke-direct {v0}, Lpjv;-><init>()V

    iput-object v0, p0, Lpjw;->b:Lpjv;

    .line 200
    :cond_2
    iget-object v0, p0, Lpjw;->b:Lpjv;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 204
    :sswitch_2
    iget-object v0, p0, Lpjw;->c:Lpjv;

    if-nez v0, :cond_3

    .line 205
    new-instance v0, Lpjv;

    invoke-direct {v0}, Lpjv;-><init>()V

    iput-object v0, p0, Lpjw;->c:Lpjv;

    .line 207
    :cond_3
    iget-object v0, p0, Lpjw;->c:Lpjv;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 182
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 150
    iget-object v0, p0, Lpjw;->b:Lpjv;

    if-eqz v0, :cond_0

    .line 151
    const/4 v0, 0x1

    iget-object v1, p0, Lpjw;->b:Lpjv;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 153
    :cond_0
    iget-object v0, p0, Lpjw;->c:Lpjv;

    if-eqz v0, :cond_1

    .line 154
    const/4 v0, 0x2

    iget-object v1, p0, Lpjw;->c:Lpjv;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 156
    :cond_1
    iget-object v0, p0, Lpjw;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 158
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 131
    invoke-virtual {p0, p1}, Lpjw;->a(Loxn;)Lpjw;

    move-result-object v0

    return-object v0
.end method
