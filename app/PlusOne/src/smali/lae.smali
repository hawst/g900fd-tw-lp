.class public Llae;
.super Lllq;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Llae;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:I

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:I

.field private final e:I

.field private final f:Z

.field private final g:Ljava/lang/String;

.field private final h:D


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 520
    new-instance v0, Llaf;

    invoke-direct {v0}, Llaf;-><init>()V

    sput-object v0, Llae;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(ILandroid/location/Location;)V
    .locals 4

    .prologue
    const-wide v2, 0x416312d000000000L    # 1.0E7

    .line 230
    invoke-direct {p0}, Lllq;-><init>()V

    .line 231
    if-ltz p1, :cond_0

    const/4 v0, 0x3

    if-le p1, v0, :cond_1

    .line 232
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 234
    :cond_1
    if-nez p2, :cond_2

    .line 235
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 237
    :cond_2
    iput p1, p0, Llae;->a:I

    .line 238
    const/4 v0, 0x1

    iput-boolean v0, p0, Llae;->f:Z

    .line 239
    invoke-virtual {p2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    mul-double/2addr v0, v2

    double-to-int v0, v0

    iput v0, p0, Llae;->d:I

    .line 240
    invoke-virtual {p2}, Landroid/location/Location;->getLongitude()D

    move-result-wide v0

    mul-double/2addr v0, v2

    double-to-int v0, v0

    iput v0, p0, Llae;->e:I

    .line 241
    const/4 v0, 0x0

    iput-object v0, p0, Llae;->g:Ljava/lang/String;

    iput-object v0, p0, Llae;->b:Ljava/lang/String;

    iput-object v0, p0, Llae;->c:Ljava/lang/String;

    .line 242
    invoke-virtual {p2}, Landroid/location/Location;->hasAccuracy()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p2}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    float-to-double v0, v0

    :goto_0
    iput-wide v0, p0, Llae;->h:D

    .line 243
    return-void

    .line 242
    :cond_3
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    goto :goto_0
.end method

.method public constructor <init>(ILjava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;D)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 69
    invoke-direct {p0}, Lllq;-><init>()V

    .line 70
    if-ltz p1, :cond_0

    const/4 v0, 0x3

    if-le p1, v0, :cond_1

    .line 71
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 73
    :cond_1
    iput p1, p0, Llae;->a:I

    .line 74
    iput-object p4, p0, Llae;->c:Ljava/lang/String;

    .line 75
    iput-object p5, p0, Llae;->b:Ljava/lang/String;

    .line 76
    iput-object p6, p0, Llae;->g:Ljava/lang/String;

    .line 77
    if-eqz p2, :cond_2

    if-eqz p3, :cond_2

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Llae;->f:Z

    .line 78
    iget-boolean v0, p0, Llae;->f:Z

    if-eqz v0, :cond_3

    .line 79
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Llae;->d:I

    .line 80
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Llae;->e:I

    .line 84
    :goto_1
    iput-wide p7, p0, Llae;->h:D

    .line 85
    return-void

    :cond_2
    move v0, v1

    .line 77
    goto :goto_0

    .line 82
    :cond_3
    iput v1, p0, Llae;->e:I

    iput v1, p0, Llae;->d:I

    goto :goto_1
.end method

.method public constructor <init>(ILofq;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    const-wide v6, 0x416312d000000000L    # 1.0E7

    .line 93
    invoke-direct {p0}, Lllq;-><init>()V

    .line 94
    if-ltz p1, :cond_0

    const/4 v0, 0x3

    if-gt p1, v0, :cond_0

    if-nez p2, :cond_1

    .line 96
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 98
    :cond_1
    iput p1, p0, Llae;->a:I

    .line 99
    iget-object v0, p2, Lofq;->e:Ljava/lang/String;

    iput-object v0, p0, Llae;->c:Ljava/lang/String;

    .line 100
    iget-object v0, p2, Lofq;->f:Ljava/lang/String;

    iput-object v0, p0, Llae;->b:Ljava/lang/String;

    .line 101
    iget-object v0, p2, Lofq;->g:Ljava/lang/String;

    iput-object v0, p0, Llae;->g:Ljava/lang/String;

    .line 102
    iget-object v0, p2, Lofq;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    iget-object v0, p2, Lofq;->a:Ljava/lang/Integer;

    .line 103
    :goto_0
    iget-object v3, p2, Lofq;->b:Ljava/lang/Integer;

    if-eqz v3, :cond_4

    iget-object v1, p2, Lofq;->b:Ljava/lang/Integer;

    move-object v3, v1

    .line 104
    :goto_1
    if-eqz v0, :cond_6

    if-eqz v3, :cond_6

    const/4 v1, 0x1

    :goto_2
    iput-boolean v1, p0, Llae;->f:Z

    .line 105
    iget-boolean v1, p0, Llae;->f:Z

    if-eqz v1, :cond_7

    .line 106
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Llae;->d:I

    .line 107
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Llae;->e:I

    .line 111
    :goto_3
    iget-object v0, p2, Lofq;->j:Ljava/lang/Double;

    if-nez v0, :cond_8

    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    .line 112
    :goto_4
    iput-wide v0, p0, Llae;->h:D

    .line 113
    return-void

    .line 102
    :cond_2
    iget-object v0, p2, Lofq;->c:Ljava/lang/Float;

    if-eqz v0, :cond_3

    iget-object v0, p2, Lofq;->c:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    float-to-double v4, v0

    mul-double/2addr v4, v6

    double-to-int v0, v4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_0

    .line 103
    :cond_4
    iget-object v3, p2, Lofq;->d:Ljava/lang/Float;

    if-eqz v3, :cond_5

    iget-object v1, p2, Lofq;->d:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    float-to-double v4, v1

    mul-double/2addr v4, v6

    double-to-int v1, v4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object v3, v1

    goto :goto_1

    :cond_5
    move-object v3, v1

    goto :goto_1

    :cond_6
    move v1, v2

    .line 104
    goto :goto_2

    .line 109
    :cond_7
    iput v2, p0, Llae;->e:I

    iput v2, p0, Llae;->d:I

    goto :goto_3

    .line 111
    :cond_8
    iget-object v0, p2, Lofq;->j:Ljava/lang/Double;

    .line 112
    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    goto :goto_4
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 538
    invoke-direct {p0}, Lllq;-><init>()V

    .line 539
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Llae;->a:I

    .line 540
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llae;->c:Ljava/lang/String;

    .line 541
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llae;->b:Ljava/lang/String;

    .line 542
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Llae;->f:Z

    .line 543
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Llae;->d:I

    .line 544
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Llae;->e:I

    .line 545
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Llae;->h:D

    .line 546
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llae;->g:Ljava/lang/String;

    .line 547
    return-void

    .line 542
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lilu;Lpro;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const-wide v6, 0x416312d000000000L    # 1.0E7

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 141
    invoke-direct {p0}, Lllq;-><init>()V

    .line 144
    invoke-interface {p1}, Lilu;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lilu;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    iput v0, p0, Llae;->a:I

    .line 146
    invoke-interface {p1}, Lilu;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llae;->b:Ljava/lang/String;

    .line 147
    invoke-interface {p1}, Lilu;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llae;->c:Ljava/lang/String;

    .line 148
    invoke-interface {p1}, Lilu;->d()Ling;

    move-result-object v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Llae;->f:Z

    .line 149
    iget-boolean v0, p0, Llae;->f:Z

    if-eqz v0, :cond_3

    .line 150
    invoke-interface {p1}, Lilu;->d()Ling;

    move-result-object v0

    iget-wide v4, v0, Ling;->a:D

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-static {v0}, Llsl;->a(Ljava/lang/Double;)D

    move-result-wide v4

    mul-double/2addr v4, v6

    double-to-int v0, v4

    iput v0, p0, Llae;->d:I

    .line 151
    invoke-interface {p1}, Lilu;->d()Ling;

    move-result-object v0

    iget-wide v4, v0, Ling;->b:D

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-static {v0}, Llsl;->a(Ljava/lang/Double;)D

    move-result-wide v4

    mul-double/2addr v4, v6

    double-to-int v0, v4

    iput v0, p0, Llae;->e:I

    .line 156
    :goto_2
    if-eqz p2, :cond_4

    iget-object v0, p2, Lpro;->a:Lolu;

    if-eqz v0, :cond_4

    iget-object v0, p2, Lpro;->a:Lolu;

    iget-object v0, v0, Lolu;->a:Ljava/lang/Long;

    if-eqz v0, :cond_4

    iget-object v0, p2, Lpro;->a:Lolu;

    iget-object v0, v0, Lolu;->a:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v0, v4, v8

    if-eqz v0, :cond_4

    move v0, v1

    :goto_3
    if-eqz v0, :cond_6

    invoke-interface {p1}, Lilu;->e()Z

    move-result v0

    if-nez v0, :cond_6

    .line 157
    iget-object v0, p2, Lpro;->a:Lolu;

    iget-object v0, v0, Lolu;->a:Ljava/lang/Long;

    .line 158
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v3, v4, v8

    if-gez v3, :cond_5

    .line 159
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    shr-long v6, v4, v1

    const-wide v8, 0x7fffffffffffffffL

    and-long/2addr v6, v8

    const-wide/16 v8, 0x5

    div-long/2addr v6, v8

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    const-string v3, "%d"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v1, v2

    invoke-static {v0, v3, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-wide/16 v2, 0xa

    mul-long/2addr v2, v6

    sub-long v2, v4, v2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x14

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llae;->g:Ljava/lang/String;

    .line 166
    :goto_4
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    iput-wide v0, p0, Llae;->h:D

    .line 167
    return-void

    .line 144
    :cond_1
    const/4 v0, 0x3

    goto/16 :goto_0

    :cond_2
    move v0, v2

    .line 148
    goto/16 :goto_1

    .line 153
    :cond_3
    iput v2, p0, Llae;->e:I

    iput v2, p0, Llae;->d:I

    goto/16 :goto_2

    :cond_4
    move v0, v2

    .line 156
    goto :goto_3

    .line 161
    :cond_5
    invoke-virtual {v0}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llae;->g:Ljava/lang/String;

    goto :goto_4

    .line 164
    :cond_6
    const/4 v0, 0x0

    iput-object v0, p0, Llae;->g:Ljava/lang/String;

    goto :goto_4
.end method

.method public constructor <init>(Loym;)V
    .locals 8

    .prologue
    const-wide v6, 0x416312d000000000L    # 1.0E7

    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 188
    invoke-direct {p0}, Lllq;-><init>()V

    .line 189
    if-nez p1, :cond_0

    .line 190
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 192
    :cond_0
    const/4 v0, 0x3

    iput v0, p0, Llae;->a:I

    .line 193
    iget-object v0, p1, Loym;->c:Loya;

    if-eqz v0, :cond_5

    iget-object v0, p1, Loym;->c:Loya;

    sget-object v1, Lpao;->a:Loxr;

    invoke-virtual {v0, v1}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 194
    iget-object v0, p1, Loym;->c:Loya;

    sget-object v1, Lpao;->a:Loxr;

    invoke-virtual {v0, v1}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpao;

    .line 195
    iget-object v1, v0, Lpao;->b:Ljava/lang/String;

    if-nez v1, :cond_2

    iget-object v1, p1, Loym;->b:Ljava/lang/String;

    :goto_0
    iput-object v1, p0, Llae;->c:Ljava/lang/String;

    .line 196
    iget-object v1, v0, Lpao;->d:Loya;

    if-eqz v1, :cond_3

    iget-object v1, v0, Lpao;->d:Loya;

    sget-object v3, Lpcg;->a:Loxr;

    .line 197
    invoke-virtual {v1, v3}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 198
    iget-object v1, v0, Lpao;->d:Loya;

    sget-object v3, Lpcg;->a:Loxr;

    invoke-virtual {v1, v3}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lpcg;

    iget-object v1, v1, Lpcg;->c:Ljava/lang/String;

    iput-object v1, p0, Llae;->b:Ljava/lang/String;

    .line 202
    :goto_1
    iget-object v1, v0, Lpao;->e:Loya;

    if-eqz v1, :cond_4

    iget-object v1, v0, Lpao;->e:Loya;

    sget-object v3, Lozb;->a:Loxr;

    invoke-virtual {v1, v3}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 204
    iget-object v1, v0, Lpao;->e:Loya;

    sget-object v3, Lozb;->a:Loxr;

    invoke-virtual {v1, v3}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lozb;

    .line 206
    iget-object v3, v1, Lozb;->c:Ljava/lang/Double;

    if-eqz v3, :cond_1

    iget-object v3, v1, Lozb;->d:Ljava/lang/Double;

    if-eqz v3, :cond_1

    const/4 v2, 0x1

    :cond_1
    iput-boolean v2, p0, Llae;->f:Z

    .line 207
    iget-object v2, v1, Lozb;->c:Ljava/lang/Double;

    invoke-static {v2}, Llsl;->a(Ljava/lang/Double;)D

    move-result-wide v2

    mul-double/2addr v2, v6

    double-to-int v2, v2

    iput v2, p0, Llae;->d:I

    .line 208
    iget-object v1, v1, Lozb;->d:Ljava/lang/Double;

    invoke-static {v1}, Llsl;->a(Ljava/lang/Double;)D

    move-result-wide v2

    mul-double/2addr v2, v6

    double-to-int v1, v2

    iput v1, p0, Llae;->e:I

    .line 213
    :goto_2
    iget-object v0, v0, Lpao;->f:Ljava/lang/String;

    iput-object v0, p0, Llae;->g:Ljava/lang/String;

    .line 221
    :goto_3
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    iput-wide v0, p0, Llae;->h:D

    .line 222
    return-void

    .line 195
    :cond_2
    iget-object v1, v0, Lpao;->b:Ljava/lang/String;

    goto :goto_0

    .line 200
    :cond_3
    iput-object v4, p0, Llae;->b:Ljava/lang/String;

    goto :goto_1

    .line 210
    :cond_4
    iput-boolean v2, p0, Llae;->f:Z

    .line 211
    iput v2, p0, Llae;->e:I

    iput v2, p0, Llae;->d:I

    goto :goto_2

    .line 215
    :cond_5
    iget-object v0, p1, Loym;->b:Ljava/lang/String;

    iput-object v0, p0, Llae;->c:Ljava/lang/String;

    .line 216
    iput-object v4, p0, Llae;->b:Ljava/lang/String;

    .line 217
    iput-boolean v2, p0, Llae;->f:Z

    .line 218
    iput v2, p0, Llae;->e:I

    iput v2, p0, Llae;->d:I

    .line 219
    iput-object v4, p0, Llae;->g:Ljava/lang/String;

    goto :goto_3
.end method

.method public constructor <init>(Lpdt;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const-wide v4, 0x416312d000000000L    # 1.0E7

    const/4 v2, 0x0

    .line 119
    invoke-direct {p0}, Lllq;-><init>()V

    .line 120
    if-nez p1, :cond_0

    .line 121
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 123
    :cond_0
    const/4 v0, 0x3

    iput v0, p0, Llae;->a:I

    .line 124
    iget-object v0, p1, Lpdt;->a:Ljava/lang/String;

    iput-object v0, p0, Llae;->c:Ljava/lang/String;

    .line 125
    iget-object v0, p1, Lpdt;->c:Lpdw;

    if-nez v0, :cond_1

    move-object v0, v1

    :goto_0
    iput-object v0, p0, Llae;->b:Ljava/lang/String;

    .line 126
    iget-object v0, p1, Lpdt;->d:Lpdr;

    if-eqz v0, :cond_3

    .line 127
    iget-object v0, p1, Lpdt;->d:Lpdr;

    iget-object v0, v0, Lpdr;->a:Ljava/lang/Double;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lpdt;->d:Lpdr;

    iget-object v0, v0, Lpdr;->b:Ljava/lang/Double;

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Llae;->f:Z

    .line 128
    iget-object v0, p1, Lpdt;->d:Lpdr;

    iget-object v0, v0, Lpdr;->a:Ljava/lang/Double;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Double;)D

    move-result-wide v2

    mul-double/2addr v2, v4

    double-to-int v0, v2

    iput v0, p0, Llae;->d:I

    .line 129
    iget-object v0, p1, Lpdt;->d:Lpdr;

    iget-object v0, v0, Lpdr;->b:Ljava/lang/Double;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Double;)D

    move-result-wide v2

    mul-double/2addr v2, v4

    double-to-int v0, v2

    iput v0, p0, Llae;->e:I

    .line 134
    :goto_2
    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    iput-wide v2, p0, Llae;->h:D

    .line 135
    iput-object v1, p0, Llae;->g:Ljava/lang/String;

    .line 136
    return-void

    .line 125
    :cond_1
    iget-object v0, p1, Lpdt;->c:Lpdw;

    iget-object v0, v0, Lpdw;->a:Ljava/lang/String;

    goto :goto_0

    :cond_2
    move v0, v2

    .line 127
    goto :goto_1

    .line 131
    :cond_3
    iput-boolean v2, p0, Llae;->f:Z

    .line 132
    iput v2, p0, Llae;->e:I

    iput v2, p0, Llae;->d:I

    goto :goto_2
.end method

.method public static a([B)Llae;
    .locals 11

    .prologue
    const/4 v4, 0x0

    .line 498
    if-nez p0, :cond_0

    .line 514
    :goto_0
    return-object v4

    .line 502
    :cond_0
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 504
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v2

    .line 505
    invoke-static {v1}, Llae;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v5

    .line 506
    invoke-static {v1}, Llae;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v6

    .line 507
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    .line 508
    :goto_1
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v3

    .line 509
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v10

    .line 510
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->getDouble()D

    move-result-wide v8

    .line 511
    invoke-static {v1}, Llae;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v7

    .line 513
    new-instance v1, Llae;

    if-eqz v0, :cond_3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    :goto_2
    if-eqz v0, :cond_1

    .line 514
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    :cond_1
    invoke-direct/range {v1 .. v9}, Llae;-><init>(ILjava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;D)V

    move-object v4, v1

    goto :goto_0

    .line 507
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    move-object v3, v4

    .line 513
    goto :goto_2
.end method

.method public static a(Llae;)[B
    .locals 6

    .prologue
    .line 475
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    const/16 v0, 0x20

    invoke-direct {v1, v0}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 476
    new-instance v2, Ljava/io/DataOutputStream;

    invoke-direct {v2, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 478
    iget v0, p0, Llae;->a:I

    invoke-virtual {v2, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 479
    iget-object v0, p0, Llae;->c:Ljava/lang/String;

    invoke-static {v2, v0}, Llae;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 480
    iget-object v0, p0, Llae;->b:Ljava/lang/String;

    invoke-static {v2, v0}, Llae;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 481
    iget-boolean v0, p0, Llae;->f:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 482
    iget v0, p0, Llae;->d:I

    invoke-virtual {v2, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 483
    iget v0, p0, Llae;->e:I

    invoke-virtual {v2, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 484
    iget-wide v4, p0, Llae;->h:D

    invoke-virtual {v2, v4, v5}, Ljava/io/DataOutputStream;->writeDouble(D)V

    .line 485
    iget-object v0, p0, Llae;->g:Ljava/lang/String;

    invoke-static {v2, v0}, Llae;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 487
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 488
    invoke-virtual {v2}, Ljava/io/DataOutputStream;->close()V

    .line 489
    return-object v0

    .line 481
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Loym;)[B
    .locals 1

    .prologue
    .line 465
    new-instance v0, Llae;

    invoke-direct {v0, p0}, Llae;-><init>(Loym;)V

    invoke-static {v0}, Llae;->a(Llae;)[B

    move-result-object v0

    return-object v0
.end method

.method public static a(Lpdt;)[B
    .locals 1

    .prologue
    .line 455
    new-instance v0, Llae;

    invoke-direct {v0, p0}, Llae;-><init>(Lpdt;)V

    invoke-static {v0}, Llae;->a(Llae;)[B

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Landroid/content/Context;)Ljava/lang/String;
    .locals 8

    .prologue
    const-wide v6, 0x416312d000000000L    # 1.0E7

    .line 294
    iget-object v0, p0, Llae;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 295
    iget-object v0, p0, Llae;->c:Ljava/lang/String;

    .line 304
    :goto_0
    return-object v0

    .line 297
    :cond_0
    iget-object v0, p0, Llae;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 298
    iget-object v0, p0, Llae;->b:Ljava/lang/String;

    goto :goto_0

    .line 300
    :cond_1
    invoke-virtual {p0}, Llae;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 301
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a047e

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 302
    invoke-virtual {p0}, Llae;->f()I

    move-result v4

    int-to-double v4, v4

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p0}, Llae;->g()I

    move-result v4

    int-to-double v4, v4

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v2, v3

    .line 301
    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 304
    :cond_2
    const-string v0, ""

    goto :goto_0
.end method

.method public a()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 260
    iget v1, p0, Llae;->a:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Z
    .locals 2

    .prologue
    .line 269
    iget v0, p0, Llae;->a:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Llae;)Z
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 570
    if-ne p0, p1, :cond_1

    .line 598
    :cond_0
    :goto_0
    return v0

    .line 575
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 576
    goto :goto_0

    .line 581
    :cond_2
    invoke-virtual {p0}, Llae;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p1}, Llae;->a()Z

    move-result v2

    if-nez v2, :cond_0

    .line 582
    :cond_3
    invoke-virtual {p0}, Llae;->b()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {p1}, Llae;->b()Z

    move-result v2

    if-nez v2, :cond_0

    .line 588
    :cond_4
    iget v2, p0, Llae;->a:I

    if-ne v2, v3, :cond_5

    iget v2, p1, Llae;->a:I

    if-ne v2, v3, :cond_5

    iget-object v2, p0, Llae;->c:Ljava/lang/String;

    iget-object v3, p1, Llae;->c:Ljava/lang/String;

    .line 589
    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Llae;->b:Ljava/lang/String;

    iget-object v3, p1, Llae;->b:Ljava/lang/String;

    .line 590
    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-boolean v2, p0, Llae;->f:Z

    iget-boolean v3, p1, Llae;->f:Z

    if-ne v2, v3, :cond_5

    iget v2, p0, Llae;->d:I

    iget v3, p1, Llae;->d:I

    if-ne v2, v3, :cond_5

    iget v2, p0, Llae;->e:I

    iget v3, p1, Llae;->e:I

    if-eq v2, v3, :cond_0

    :cond_5
    move v0, v1

    .line 598
    goto :goto_0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, Llae;->c:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Llae;->b:Ljava/lang/String;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 535
    const/4 v0, 0x0

    return v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 312
    iget-boolean v0, p0, Llae;->f:Z

    return v0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 320
    iget v0, p0, Llae;->d:I

    return v0
.end method

.method public g()I
    .locals 1

    .prologue
    .line 328
    iget v0, p0, Llae;->e:I

    return v0
.end method

.method public h()D
    .locals 2

    .prologue
    .line 336
    iget-wide v0, p0, Llae;->h:D

    return-wide v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 344
    iget-object v0, p0, Llae;->g:Ljava/lang/String;

    return-object v0
.end method

.method public j()Lofq;
    .locals 6

    .prologue
    .line 403
    new-instance v0, Lofq;

    invoke-direct {v0}, Lofq;-><init>()V

    .line 404
    iget-object v1, p0, Llae;->c:Ljava/lang/String;

    iput-object v1, v0, Lofq;->e:Ljava/lang/String;

    .line 405
    iget-object v1, p0, Llae;->b:Ljava/lang/String;

    iput-object v1, v0, Lofq;->f:Ljava/lang/String;

    .line 406
    iget-object v1, p0, Llae;->g:Ljava/lang/String;

    iput-object v1, v0, Lofq;->g:Ljava/lang/String;

    .line 407
    iget-boolean v1, p0, Llae;->f:Z

    if-eqz v1, :cond_0

    .line 408
    iget v1, p0, Llae;->d:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lofq;->a:Ljava/lang/Integer;

    .line 409
    iget v1, p0, Llae;->e:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lofq;->b:Ljava/lang/Integer;

    .line 411
    :cond_0
    iget-wide v2, p0, Llae;->h:D

    const-wide/16 v4, 0x0

    cmpl-double v1, v2, v4

    if-ltz v1, :cond_1

    .line 412
    iget-wide v2, p0, Llae;->h:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    iput-object v1, v0, Lofq;->j:Ljava/lang/Double;

    .line 415
    :cond_1
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 386
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "LocationValue type: "

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 387
    iget v0, p0, Llae;->a:I

    packed-switch v0, :pswitch_data_0

    iget v0, p0, Llae;->a:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x14

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "unknown("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", name: "

    .line 388
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Llae;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", addr: "

    .line 389
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Llae;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", hasCoord: "

    .line 390
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Llae;->f:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", latE7: "

    .line 391
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Llae;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", lngE7: "

    .line 392
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Llae;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", cluster: "

    .line 393
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Llae;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", precision: "

    .line 394
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Llae;->h:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 395
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 387
    :pswitch_0
    const-string v0, "precise"

    goto :goto_0

    :pswitch_1
    const-string v0, "coarse"

    goto :goto_0

    :pswitch_2
    const-string v0, "place"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 551
    iget v0, p0, Llae;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 552
    iget-object v0, p0, Llae;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 553
    iget-object v0, p0, Llae;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 554
    iget-boolean v0, p0, Llae;->f:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 555
    iget v0, p0, Llae;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 556
    iget v0, p0, Llae;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 557
    iget-wide v0, p0, Llae;->h:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 558
    iget-object v0, p0, Llae;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 559
    return-void

    .line 554
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
