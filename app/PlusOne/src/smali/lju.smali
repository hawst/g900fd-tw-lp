.class public final Llju;
.super Landroid/text/style/URLSpan;
.source "PG"


# instance fields
.field private final a:Lljv;

.field private b:Z

.field private c:Z

.field private d:Z

.field private e:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 141
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Llju;-><init>(Ljava/lang/String;ZLljv;)V

    .line 142
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ZLljv;)V
    .locals 1

    .prologue
    .line 145
    invoke-direct {p0, p1}, Landroid/text/style/URLSpan;-><init>(Ljava/lang/String;)V

    .line 146
    const/4 v0, 0x1

    iput-boolean v0, p0, Llju;->c:Z

    .line 147
    iput-boolean p2, p0, Llju;->d:Z

    .line 148
    iput-object p3, p0, Llju;->a:Lljv;

    .line 149
    return-void
.end method

.method public static a(Ljava/lang/String;Lljv;)Landroid/text/Spanned;
    .locals 2

    .prologue
    .line 53
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-static {p0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 54
    invoke-static {v0, p1}, Llju;->a(Landroid/text/Spannable;Lljv;)V

    .line 55
    return-object v0
.end method

.method public static a(Landroid/text/Spannable;)V
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    invoke-static {p0, v0}, Llju;->a(Landroid/text/Spannable;Lljv;)V

    .line 45
    return-void
.end method

.method public static a(Landroid/text/Spannable;Lljv;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 31
    invoke-interface {p0}, Landroid/text/Spannable;->length()I

    move-result v0

    const-class v1, Landroid/text/style/URLSpan;

    invoke-interface {p0, v2, v0, v1}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/URLSpan;

    move v1, v2

    .line 32
    :goto_0
    array-length v3, v0

    if-ge v1, v3, :cond_0

    .line 33
    aget-object v3, v0, v1

    .line 34
    invoke-interface {p0, v3}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v4

    .line 35
    invoke-interface {p0, v3}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v5

    .line 36
    invoke-interface {p0, v3}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    .line 37
    new-instance v6, Llju;

    invoke-virtual {v3}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v6, v3, v2, p1}, Llju;-><init>(Ljava/lang/String;ZLljv;)V

    .line 38
    invoke-interface {p0, v6, v4, v5, v2}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 32
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 41
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Z)V
    .locals 0

    .prologue
    .line 176
    iput-boolean p1, p0, Llju;->b:Z

    .line 177
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 180
    iget-boolean v0, p0, Llju;->d:Z

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Llju;->a:Lljv;

    if-eqz v0, :cond_0

    .line 186
    iget-object v0, p0, Llju;->a:Lljv;

    invoke-interface {v0, p0}, Lljv;->a(Landroid/text/style/URLSpan;)V

    .line 190
    :goto_0
    return-void

    .line 188
    :cond_0
    invoke-super {p0, p1}, Landroid/text/style/URLSpan;->onClick(Landroid/view/View;)V

    goto :goto_0
.end method

.method public updateDrawState(Landroid/text/TextPaint;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 153
    iget-boolean v0, p0, Llju;->c:Z

    if-eqz v0, :cond_0

    .line 154
    iput-boolean v1, p0, Llju;->c:Z

    .line 156
    iget v0, p1, Landroid/text/TextPaint;->bgColor:I

    iput v0, p0, Llju;->e:I

    .line 159
    :cond_0
    iget-boolean v0, p0, Llju;->b:Z

    if-eqz v0, :cond_2

    .line 160
    const v0, -0xcc4a1b

    iput v0, p1, Landroid/text/TextPaint;->bgColor:I

    .line 165
    :goto_0
    iget-boolean v0, p0, Llju;->d:Z

    if-nez v0, :cond_1

    .line 166
    const v0, -0xbd8013

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 169
    :cond_1
    invoke-virtual {p1, v1}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 170
    return-void

    .line 162
    :cond_2
    iget v0, p0, Llju;->e:I

    iput v0, p1, Landroid/text/TextPaint;->bgColor:I

    goto :goto_0
.end method
