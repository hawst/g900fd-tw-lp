.class public final Lhir;
.super Landroid/widget/TextView;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 23
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    invoke-virtual {p0}, Lhir;->getContext()Landroid/content/Context;

    move-result-object v1

    const/16 v2, 0x10

    invoke-static {v1, p0, v2}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 42
    invoke-virtual {p0}, Lhir;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 43
    const v2, 0x7f0d0221

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 44
    const v3, 0x7f0d0222

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 45
    invoke-virtual {p0, v1, v2, v1, v2}, Lhir;->setPadding(IIII)V

    .line 46
    invoke-static {}, Llsm;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lhir;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Configuration;->getLayoutDirection()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    if-eqz v0, :cond_1

    const/4 v0, 0x3

    :goto_1
    invoke-virtual {p0, v0}, Lhir;->setGravity(I)V

    .line 47
    invoke-virtual {p0}, Lhir;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b01e9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lhir;->setTextColor(I)V

    .line 24
    return-void

    .line 46
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, 0x5

    goto :goto_1
.end method


# virtual methods
.method public a(I)Lhir;
    .locals 0

    .prologue
    .line 62
    invoke-virtual {p0, p1}, Lhir;->setText(I)V

    .line 63
    return-object p0
.end method
