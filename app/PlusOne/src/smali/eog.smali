.class public final Leog;
.super Lenl;
.source "PG"


# static fields
.field private static an:Ljava/lang/Runnable;


# instance fields
.field private aA:Ljava/lang/String;

.field private aB:Ljava/lang/String;

.field private aC:Ljava/lang/String;

.field private aD:Ljava/lang/String;

.field private aE:I

.field private aF:Z

.field private aG:Z

.field private aH:Z

.field private aI:Landroid/widget/Button;

.field private aJ:Landroid/view/View;

.field private aK:Landroid/widget/RadioGroup;

.field private aL:Landroid/widget/RadioButton;

.field private aM:Landroid/widget/RadioButton;

.field private aN:Landroid/widget/RadioButton;

.field private aO:Z

.field private aP:Z

.field private final aQ:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field private final aR:Landroid/view/View$OnClickListener;

.field private final aS:Landroid/text/TextWatcher;

.field private final aT:Landroid/text/TextWatcher;

.field private ao:Landroid/view/ViewGroup;

.field private ap:Landroid/widget/EditText;

.field private aq:Landroid/widget/EditText;

.field private ar:Landroid/widget/EditText;

.field private as:Landroid/widget/TextView;

.field private aw:Landroid/view/View;

.field private ax:Ljava/lang/String;

.field private ay:Ljava/lang/String;

.field private az:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Lenl;-><init>()V

    .line 79
    const/4 v0, 0x1

    iput-boolean v0, p0, Leog;->aF:Z

    .line 90
    new-instance v0, Leoh;

    invoke-direct {v0, p0}, Leoh;-><init>(Leog;)V

    iput-object v0, p0, Leog;->aQ:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 100
    new-instance v0, Leoi;

    invoke-direct {v0, p0}, Leoi;-><init>(Leog;)V

    iput-object v0, p0, Leog;->aR:Landroid/view/View$OnClickListener;

    .line 108
    new-instance v0, Leoj;

    invoke-direct {v0, p0}, Leoj;-><init>(Leog;)V

    iput-object v0, p0, Leog;->aS:Landroid/text/TextWatcher;

    .line 123
    new-instance v0, Leok;

    invoke-direct {v0, p0}, Leok;-><init>(Leog;)V

    iput-object v0, p0, Leog;->aT:Landroid/text/TextWatcher;

    return-void
.end method

.method static synthetic a(Leog;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Leog;->ar()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Leog;I)V
    .locals 1

    .prologue
    .line 56
    const v0, 0x7f100525

    if-ne p1, v0, :cond_1

    const/4 v0, 0x1

    iput v0, p0, Leog;->aE:I

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v0, 0x7f100526

    if-ne p1, v0, :cond_2

    const/4 v0, 0x2

    iput v0, p0, Leog;->aE:I

    goto :goto_0

    :cond_2
    const v0, 0x7f100527

    if-ne p1, v0, :cond_0

    const/4 v0, 0x3

    iput v0, p0, Leog;->aE:I

    goto :goto_0
.end method

.method static synthetic a(Leog;Z)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0, p1}, Leog;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 149
    iget-object v1, p0, Leog;->aJ:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 150
    iput-boolean p1, p0, Leog;->aG:Z

    .line 151
    iget-object v1, p0, Leog;->aI:Landroid/widget/Button;

    if-eqz p1, :cond_1

    const v0, 0x7f0a0382

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setText(I)V

    .line 153
    return-void

    .line 149
    :cond_0
    const/16 v0, 0x8

    goto :goto_0

    .line 151
    :cond_1
    const v0, 0x7f0a0381

    goto :goto_1
.end method

.method private al()V
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 217
    iget-object v0, p0, Leog;->aL:Landroid/widget/RadioButton;

    invoke-direct {p0}, Leog;->am()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 218
    iget-object v1, p0, Leog;->aM:Landroid/widget/RadioButton;

    invoke-direct {p0}, Leog;->ar()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-direct {p0}, Leog;->am()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 219
    iget-object v1, p0, Leog;->aN:Landroid/widget/RadioButton;

    invoke-direct {p0}, Leog;->ar()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-direct {p0}, Leog;->am()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 220
    return-void

    .line 218
    :cond_0
    invoke-virtual {p0}, Leog;->o()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a039b

    new-array v4, v9, [Ljava/lang/Object;

    invoke-direct {p0}, Leog;->an()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-direct {p0}, Leog;->ao()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    aput-object v0, v4, v8

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 219
    :cond_1
    invoke-virtual {p0}, Leog;->o()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a039c

    new-array v4, v9, [Ljava/lang/Object;

    invoke-direct {p0}, Leog;->an()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-direct {p0}, Leog;->ao()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    aput-object v0, v4, v8

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private am()Ljava/lang/String;
    .locals 5

    .prologue
    .line 578
    invoke-virtual {p0}, Leog;->o()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a039a

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 579
    invoke-direct {p0}, Leog;->an()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-direct {p0}, Leog;->ao()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 578
    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private an()Ljava/lang/String;
    .locals 1

    .prologue
    .line 603
    iget-boolean v0, p0, Leog;->aF:Z

    if-eqz v0, :cond_0

    .line 604
    invoke-direct {p0}, Leog;->ap()Ljava/lang/String;

    move-result-object v0

    .line 606
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Leog;->aq()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private ao()Ljava/lang/String;
    .locals 1

    .prologue
    .line 611
    iget-boolean v0, p0, Leog;->aF:Z

    if-eqz v0, :cond_0

    .line 612
    invoke-direct {p0}, Leog;->aq()Ljava/lang/String;

    move-result-object v0

    .line 614
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Leog;->ap()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private ap()Ljava/lang/String;
    .locals 1

    .prologue
    .line 619
    iget-object v0, p0, Leog;->ap:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private aq()Ljava/lang/String;
    .locals 1

    .prologue
    .line 623
    iget-object v0, p0, Leog;->aq:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private ar()Ljava/lang/String;
    .locals 1

    .prologue
    .line 627
    iget-object v0, p0, Leog;->ar:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private as()I
    .locals 1

    .prologue
    .line 631
    invoke-direct {p0}, Leog;->at()I

    move-result v0

    invoke-static {v0}, Ldsm;->c(I)I

    move-result v0

    return v0
.end method

.method private at()I
    .locals 2

    .prologue
    .line 639
    iget-object v0, p0, Leog;->aK:Landroid/widget/RadioGroup;

    invoke-virtual {v0}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v0

    const v1, 0x7f100525

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const v1, 0x7f100526

    if-ne v0, v1, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    const v1, 0x7f100527

    if-ne v0, v1, :cond_2

    const/4 v0, 0x3

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Leog;)Landroid/view/View;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Leog;->aJ:Landroid/view/View;

    return-object v0
.end method

.method static synthetic b(Leog;I)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 56
    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Leog;->aL:Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Leog;->aM:Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Leog;->aN:Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private b(Z)V
    .locals 2

    .prologue
    .line 156
    iget-object v1, p0, Leog;->aK:Landroid/widget/RadioGroup;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/RadioGroup;->setVisibility(I)V

    .line 157
    return-void

    .line 156
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method static synthetic b(Leog;Z)Z
    .locals 0

    .prologue
    .line 56
    iput-boolean p1, p0, Leog;->aH:Z

    return p1
.end method

.method static synthetic c(Leog;)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Leog;->al()V

    return-void
.end method

.method static synthetic c(Leog;Z)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0, p1}, Leog;->b(Z)V

    return-void
.end method

.method static synthetic d(Leog;)Landroid/widget/RadioButton;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Leog;->aL:Landroid/widget/RadioButton;

    return-object v0
.end method

.method private d(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 521
    sget-object v0, Leog;->an:Ljava/lang/Runnable;

    if-nez v0, :cond_0

    .line 522
    new-instance v0, Leom;

    invoke-direct {v0, p0}, Leom;-><init>(Leog;)V

    sput-object v0, Leog;->an:Ljava/lang/Runnable;

    .line 566
    :cond_0
    iput-object p1, p0, Leog;->ax:Ljava/lang/String;

    .line 567
    sget-object v0, Leog;->an:Ljava/lang/Runnable;

    invoke-static {}, Llsx;->d()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 568
    sget-object v0, Leog;->an:Ljava/lang/Runnable;

    const-wide/16 v2, 0x32

    invoke-static {v0, v2, v3}, Llsx;->a(Ljava/lang/Runnable;J)V

    .line 569
    return-void
.end method

.method static synthetic e(Leog;)I
    .locals 1

    .prologue
    .line 56
    iget v0, p0, Leog;->aE:I

    return v0
.end method

.method static synthetic f(Leog;)I
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Leog;->at()I

    move-result v0

    return v0
.end method

.method static synthetic g(Leog;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Leog;->ax:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Leog;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Leog;->as:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic i(Leog;)Landroid/view/View;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Leog;->aw:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public U()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 366
    invoke-super {p0}, Lenl;->U()V

    .line 368
    new-instance v0, Lenr;

    iget-object v1, p0, Leog;->ap:Landroid/widget/EditText;

    invoke-direct {v0, p0, v1}, Lenr;-><init>(Lenl;Landroid/widget/TextView;)V

    .line 369
    new-instance v1, Lenq;

    iget-object v2, p0, Leog;->ap:Landroid/widget/EditText;

    iget-object v3, p0, Leog;->ay:Ljava/lang/String;

    invoke-direct {v1, p0, v2, v3}, Lenq;-><init>(Lenl;Landroid/view/View;Ljava/lang/String;)V

    .line 370
    iget-object v2, p0, Leog;->ap:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v1, v2, v4, v4, v4}, Lenq;->onTextChanged(Ljava/lang/CharSequence;III)V

    .line 371
    iget-object v2, p0, Leog;->ap:Landroid/widget/EditText;

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 372
    iget-object v0, p0, Leog;->ap:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 373
    iget-object v0, p0, Leog;->ap:Landroid/widget/EditText;

    iget-object v1, p0, Leog;->aS:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 375
    iget-boolean v0, p0, Leog;->aO:Z

    if-eqz v0, :cond_0

    .line 400
    :goto_0
    return-void

    .line 379
    :cond_0
    new-instance v0, Lenr;

    iget-object v1, p0, Leog;->aq:Landroid/widget/EditText;

    invoke-direct {v0, p0, v1}, Lenr;-><init>(Lenl;Landroid/widget/TextView;)V

    .line 380
    new-instance v1, Lenq;

    iget-object v2, p0, Leog;->aq:Landroid/widget/EditText;

    iget-object v3, p0, Leog;->aA:Ljava/lang/String;

    invoke-direct {v1, p0, v2, v3}, Lenq;-><init>(Lenl;Landroid/view/View;Ljava/lang/String;)V

    .line 381
    iget-object v2, p0, Leog;->aq:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v1, v2, v4, v4, v4}, Lenq;->onTextChanged(Ljava/lang/CharSequence;III)V

    .line 382
    iget-object v2, p0, Leog;->aq:Landroid/widget/EditText;

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 383
    iget-object v0, p0, Leog;->aq:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 384
    iget-object v0, p0, Leog;->aq:Landroid/widget/EditText;

    iget-object v1, p0, Leog;->aS:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 386
    new-instance v0, Lenq;

    iget-object v1, p0, Leog;->ar:Landroid/widget/EditText;

    iget-object v2, p0, Leog;->aC:Ljava/lang/String;

    invoke-direct {v0, p0, v1, v2}, Lenq;-><init>(Lenl;Landroid/view/View;Ljava/lang/String;)V

    .line 387
    iget-object v1, p0, Leog;->ar:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v0, v1, v4, v4, v4}, Lenq;->onTextChanged(Ljava/lang/CharSequence;III)V

    .line 388
    iget-object v1, p0, Leog;->ar:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 389
    iget-object v0, p0, Leog;->ar:Landroid/widget/EditText;

    iget-object v1, p0, Leog;->aT:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 391
    iget-object v0, p0, Leog;->aI:Landroid/widget/Button;

    iget-object v1, p0, Leog;->aR:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 392
    iget-object v0, p0, Leog;->aL:Landroid/widget/RadioButton;

    iget-object v1, p0, Leog;->aQ:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 393
    iget-object v0, p0, Leog;->aM:Landroid/widget/RadioButton;

    iget-object v1, p0, Leog;->aQ:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 394
    iget-object v0, p0, Leog;->aN:Landroid/widget/RadioButton;

    iget-object v1, p0, Leog;->aQ:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 396
    iget-object v0, p0, Leog;->aK:Landroid/widget/RadioGroup;

    invoke-virtual {v0}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v0

    .line 397
    new-instance v1, Lenv;

    invoke-direct {v1, p0, v0}, Lenv;-><init>(Lenl;I)V

    .line 398
    iget-object v2, p0, Leog;->aK:Landroid/widget/RadioGroup;

    invoke-virtual {v1, v2, v0}, Lenv;->onCheckedChanged(Landroid/widget/RadioGroup;I)V

    .line 399
    iget-object v0, p0, Leog;->aK:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    goto :goto_0
.end method

.method protected V()V
    .locals 3

    .prologue
    .line 438
    invoke-super {p0}, Lenl;->V()V

    .line 440
    iget-boolean v0, p0, Leog;->aO:Z

    if-nez v0, :cond_1

    .line 441
    invoke-direct {p0}, Leog;->an()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Leog;->ao()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 442
    :cond_0
    invoke-virtual {p0}, Leog;->o()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0383

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Leog;->d(Ljava/lang/String;)V

    .line 459
    :goto_0
    return-void

    .line 446
    :cond_1
    new-instance v0, Lnjt;

    invoke-direct {v0}, Lnjt;-><init>()V

    .line 448
    new-instance v1, Lnkd;

    invoke-direct {v1}, Lnkd;-><init>()V

    iput-object v1, v0, Lnjt;->e:Lnkd;

    .line 449
    iget-object v1, v0, Lnjt;->e:Lnkd;

    new-instance v2, Lnjb;

    invoke-direct {v2}, Lnjb;-><init>()V

    iput-object v2, v1, Lnkd;->a:Lnjb;

    .line 450
    iget-object v1, v0, Lnjt;->e:Lnkd;

    iget-object v1, v1, Lnkd;->a:Lnjb;

    invoke-direct {p0}, Leog;->an()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lnjb;->a:Ljava/lang/String;

    .line 451
    iget-object v1, v0, Lnjt;->e:Lnkd;

    iget-object v1, v1, Lnkd;->a:Lnjb;

    invoke-direct {p0}, Leog;->ao()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lnjb;->b:Ljava/lang/String;

    .line 452
    new-instance v1, Lnib;

    invoke-direct {v1}, Lnib;-><init>()V

    iput-object v1, v0, Lnjt;->d:Lnib;

    .line 453
    iget-object v1, v0, Lnjt;->d:Lnib;

    new-instance v2, Lnjd;

    invoke-direct {v2}, Lnjd;-><init>()V

    iput-object v2, v1, Lnib;->b:Lnjd;

    .line 454
    iget-object v1, v0, Lnjt;->d:Lnib;

    iget-object v1, v1, Lnib;->b:Lnjd;

    invoke-direct {p0}, Leog;->ar()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lnjd;->a:Ljava/lang/String;

    .line 455
    iget-object v1, v0, Lnjt;->e:Lnkd;

    new-instance v2, Lnjc;

    invoke-direct {v2}, Lnjc;-><init>()V

    iput-object v2, v1, Lnkd;->c:Lnjc;

    .line 456
    iget-object v1, v0, Lnjt;->e:Lnkd;

    iget-object v1, v1, Lnkd;->c:Lnjc;

    invoke-direct {p0}, Leog;->as()I

    move-result v2

    iput v2, v1, Lnjc;->a:I

    .line 458
    invoke-virtual {p0, v0}, Leog;->a(Lnjt;)V

    goto :goto_0
.end method

.method protected X()V
    .locals 6

    .prologue
    .line 496
    iget-object v0, p0, Leog;->au:Llnh;

    const-class v1, Liwc;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liwc;

    .line 497
    iget-object v1, p0, Leog;->am:Lhee;

    invoke-interface {v1}, Lhee;->g()Lhej;

    move-result-object v1

    const-string v2, "account_name"

    invoke-interface {v1, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 498
    invoke-virtual {p0}, Leog;->n()Lz;

    move-result-object v1

    const-class v3, Lhoc;

    invoke-static {v1, v3}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhoc;

    new-instance v3, Leol;

    .line 499
    invoke-virtual {p0}, Leog;->n()Lz;

    move-result-object v4

    const-string v5, "refreshAccount"

    invoke-direct {v3, v4, v5, v0, v2}, Leol;-><init>(Landroid/content/Context;Ljava/lang/String;Liwc;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Lhoc;->b(Lhny;)V

    .line 510
    return-void
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 404
    .line 408
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_3

    move-object v1, v0

    .line 410
    :cond_0
    const/4 v2, 0x2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 411
    packed-switch v2, :pswitch_data_0

    .line 423
    :cond_1
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_0

    move v7, v2

    move-object v2, v1

    move v1, v7

    .line 426
    :goto_1
    if-eqz v2, :cond_2

    .line 427
    new-instance v4, Lhgw;

    new-instance v5, Lhxc;

    invoke-direct {v5, v2, v1, v0, v6}, Lhxc;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    invoke-direct {v4, v5}, Lhgw;-><init>(Lhxc;)V

    iput-object v4, p0, Leog;->Y:Lhgw;

    .line 429
    invoke-virtual {p0, v2, v1, v0}, Leog;->a(Ljava/lang/String;ILjava/lang/String;)V

    .line 431
    iget-object v0, p0, Leog;->aj:Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->setEnabled(Z)V

    .line 432
    iget-object v0, p0, Leog;->aj:Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->f(I)V

    .line 434
    :cond_2
    return-void

    .line 413
    :pswitch_0
    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 414
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 417
    :pswitch_1
    if-nez v1, :cond_1

    .line 418
    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 419
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_3
    move v1, v3

    move-object v2, v0

    goto :goto_1

    .line 411
    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 171
    iput-boolean v0, p0, Leog;->V:Z

    .line 172
    iput-boolean v0, p0, Leog;->U:Z

    .line 173
    invoke-super {p0, p1}, Lenl;->a(Landroid/os/Bundle;)V

    .line 175
    if-eqz p1, :cond_4

    .line 176
    const-string v0, "given_name_first"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 177
    const-string v0, "given_name_first"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Leog;->aF:Z

    .line 179
    :cond_0
    const-string v0, "more_options_visible"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 180
    const-string v0, "more_options_visible"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Leog;->aG:Z

    .line 182
    :cond_1
    const-string v0, "changed_more_options_visible"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 183
    const-string v0, "changed_more_options_visible"

    .line 184
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Leog;->aH:Z

    .line 186
    :cond_2
    const-string v0, "name_violation"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 187
    const-string v0, "name_violation"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Leog;->aP:Z

    .line 193
    :cond_3
    :goto_0
    return-void

    .line 190
    :cond_4
    invoke-virtual {p0}, Leog;->k()Landroid/os/Bundle;

    move-result-object v0

    .line 191
    const-string v1, "name_violation"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Leog;->aP:Z

    goto :goto_0
.end method

.method protected a()[B
    .locals 5

    .prologue
    .line 277
    new-instance v0, Lnjb;

    invoke-direct {v0}, Lnjb;-><init>()V

    .line 278
    invoke-direct {p0}, Leog;->ap()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lnjb;->a:Ljava/lang/String;

    .line 279
    invoke-direct {p0}, Leog;->aq()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lnjb;->b:Ljava/lang/String;

    .line 280
    new-instance v1, Lnjc;

    invoke-direct {v1}, Lnjc;-><init>()V

    .line 281
    invoke-direct {p0}, Leog;->as()I

    move-result v2

    iput v2, v1, Lnjc;->a:I

    .line 282
    new-instance v2, Lnjd;

    invoke-direct {v2}, Lnjd;-><init>()V

    .line 283
    invoke-direct {p0}, Leog;->ar()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lnjd;->a:Ljava/lang/String;

    .line 285
    new-instance v3, Lent;

    iget-boolean v4, p0, Leog;->aO:Z

    invoke-direct {v3, v0, v1, v2, v4}, Lent;-><init>(Lnjb;Lnjc;Lnjd;Z)V

    .line 288
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 289
    const/4 v1, 0x0

    invoke-virtual {v3, v0, v1}, Lent;->writeToParcel(Landroid/os/Parcel;I)V

    .line 290
    invoke-virtual {v0}, Landroid/os/Parcel;->marshall()[B

    move-result-object v1

    .line 291
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 293
    return-object v1
.end method

.method protected c()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 224
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 225
    iget-object v1, p0, Leog;->Z:[B

    iget-object v2, p0, Leog;->Z:[B

    array-length v2, v2

    invoke-virtual {v0, v1, v3, v2}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 226
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 227
    new-instance v1, Lent;

    invoke-direct {v1, v0}, Lent;-><init>(Landroid/os/Parcel;)V

    .line 228
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 229
    iget-boolean v0, v1, Lent;->d:Z

    iput-boolean v0, p0, Leog;->aO:Z

    .line 230
    iget-object v0, v1, Lent;->a:Lnjb;

    if-eqz v0, :cond_0

    .line 231
    iget-boolean v0, p0, Leog;->aO:Z

    if-eqz v0, :cond_3

    .line 232
    iget-object v0, v1, Lent;->a:Lnjb;

    iget-object v0, v0, Lnjb;->c:Ljava/lang/String;

    iput-object v0, p0, Leog;->az:Ljava/lang/String;

    .line 241
    :cond_0
    :goto_0
    iget-object v0, v1, Lent;->b:Lnjc;

    if-eqz v0, :cond_1

    .line 242
    iget-object v0, v1, Lent;->b:Lnjc;

    iget v0, v0, Lnjc;->a:I

    invoke-static {v0}, Ldsm;->b(I)I

    move-result v0

    iput v0, p0, Leog;->aE:I

    .line 245
    :cond_1
    iget-object v0, v1, Lent;->c:Lnjd;

    if-eqz v0, :cond_2

    .line 246
    iget-object v0, v1, Lent;->c:Lnjd;

    iget-object v0, v0, Lnjd;->a:Ljava/lang/String;

    iput-object v0, p0, Leog;->aD:Ljava/lang/String;

    .line 248
    :cond_2
    return-void

    .line 234
    :cond_3
    iget-object v0, v1, Lent;->a:Lnjb;

    iget-object v0, v0, Lnjb;->a:Ljava/lang/String;

    iput-object v0, p0, Leog;->az:Ljava/lang/String;

    .line 235
    iget-object v0, v1, Lent;->a:Lnjb;

    iget-object v0, v0, Lnjb;->b:Ljava/lang/String;

    iput-object v0, p0, Leog;->aB:Ljava/lang/String;

    .line 236
    iget-object v0, v1, Lent;->a:Lnjb;

    iget-object v0, v0, Lnjb;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Leog;->az:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 237
    iget-object v0, v1, Lent;->a:Lnjb;

    iget-object v0, v0, Lnjb;->c:Ljava/lang/String;

    iget-object v2, p0, Leog;->az:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Leog;->aF:Z

    goto :goto_0
.end method

.method protected c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 464
    invoke-direct {p0, p1}, Leog;->d(Ljava/lang/String;)V

    .line 465
    return-void
.end method

.method protected d()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 252
    invoke-super {p0}, Lenl;->d()V

    .line 254
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 255
    iget-object v1, p0, Leog;->aa:[B

    iget-object v2, p0, Leog;->aa:[B

    array-length v2, v2

    invoke-virtual {v0, v1, v3, v2}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 256
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 257
    new-instance v1, Lent;

    invoke-direct {v1, v0}, Lent;-><init>(Landroid/os/Parcel;)V

    .line 258
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 259
    iget-object v0, v1, Lent;->a:Lnjb;

    if-eqz v0, :cond_0

    .line 260
    iget-boolean v0, v1, Lent;->d:Z

    if-eqz v0, :cond_2

    .line 261
    iget-object v0, v1, Lent;->a:Lnjb;

    iget-object v0, v0, Lnjb;->c:Ljava/lang/String;

    iput-object v0, p0, Leog;->ay:Ljava/lang/String;

    .line 270
    :cond_0
    :goto_0
    iget-object v0, v1, Lent;->c:Lnjd;

    if-eqz v0, :cond_1

    .line 271
    iget-object v0, v1, Lent;->c:Lnjd;

    iget-object v0, v0, Lnjd;->a:Ljava/lang/String;

    iput-object v0, p0, Leog;->aC:Ljava/lang/String;

    .line 273
    :cond_1
    return-void

    .line 263
    :cond_2
    iget-object v0, v1, Lent;->a:Lnjb;

    iget-object v0, v0, Lnjb;->a:Ljava/lang/String;

    iput-object v0, p0, Leog;->ay:Ljava/lang/String;

    .line 264
    iget-object v0, v1, Lent;->a:Lnjb;

    iget-object v0, v0, Lnjb;->b:Ljava/lang/String;

    iput-object v0, p0, Leog;->aA:Ljava/lang/String;

    .line 265
    iget-object v0, v1, Lent;->a:Lnjb;

    iget-object v0, v0, Lnjb;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Leog;->ay:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 266
    iget-object v0, v1, Lent;->a:Lnjb;

    iget-object v0, v0, Lnjb;->c:Ljava/lang/String;

    iget-object v2, p0, Leog;->ay:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Leog;->aF:Z

    goto :goto_0
.end method

.method protected d(I)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 487
    iget-boolean v0, p0, Leog;->aP:Z

    if-eqz v0, :cond_0

    if-ne p1, v1, :cond_0

    .line 488
    invoke-virtual {p0}, Leog;->n()Lz;

    move-result-object v0

    invoke-virtual {v0, v1}, Lz;->setResult(I)V

    .line 491
    :cond_0
    invoke-super {p0, p1}, Lenl;->d(I)V

    .line 492
    return-void
.end method

.method protected e()V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 298
    invoke-super {p0}, Lenl;->e()V

    .line 300
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Leog;->a_(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0401b9

    iget-object v2, p0, Leog;->ai:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Leog;->ao:Landroid/view/ViewGroup;

    .line 302
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 304
    sget v1, Leog;->R:I

    sget v2, Leog;->R:I

    sget v3, Leog;->R:I

    sget v4, Leog;->R:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 306
    iget-object v1, p0, Leog;->ai:Landroid/widget/LinearLayout;

    iget-object v2, p0, Leog;->ao:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 308
    iget-object v0, p0, Leog;->ao:Landroid/view/ViewGroup;

    const v1, 0x7f100520

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Leog;->ap:Landroid/widget/EditText;

    .line 309
    iget-object v0, p0, Leog;->ao:Landroid/view/ViewGroup;

    const v1, 0x7f100521

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Leog;->aq:Landroid/widget/EditText;

    .line 310
    iget-object v0, p0, Leog;->ao:Landroid/view/ViewGroup;

    const v1, 0x7f100523

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Leog;->ar:Landroid/widget/EditText;

    .line 311
    iget-object v0, p0, Leog;->ao:Landroid/view/ViewGroup;

    const v1, 0x7f100529

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Leog;->as:Landroid/widget/TextView;

    .line 312
    iget-object v0, p0, Leog;->ao:Landroid/view/ViewGroup;

    const v1, 0x7f10052a

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Leog;->aw:Landroid/view/View;

    .line 313
    iget-object v0, p0, Leog;->ao:Landroid/view/ViewGroup;

    const v1, 0x7f100524

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Leog;->aK:Landroid/widget/RadioGroup;

    .line 314
    iget-object v0, p0, Leog;->ao:Landroid/view/ViewGroup;

    const v1, 0x7f100525

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Leog;->aL:Landroid/widget/RadioButton;

    .line 315
    iget-object v0, p0, Leog;->ao:Landroid/view/ViewGroup;

    const v1, 0x7f100526

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Leog;->aM:Landroid/widget/RadioButton;

    .line 316
    iget-object v0, p0, Leog;->ao:Landroid/view/ViewGroup;

    const v1, 0x7f100527

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Leog;->aN:Landroid/widget/RadioButton;

    .line 317
    iget-object v0, p0, Leog;->ao:Landroid/view/ViewGroup;

    const v1, 0x7f100522

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Leog;->aJ:Landroid/view/View;

    .line 318
    iget-object v0, p0, Leog;->ao:Landroid/view/ViewGroup;

    const v1, 0x7f100528

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Leog;->aI:Landroid/widget/Button;

    .line 320
    iget-boolean v0, p0, Leog;->aO:Z

    if-eqz v0, :cond_0

    .line 321
    iget-object v0, p0, Leog;->aq:Landroid/widget/EditText;

    invoke-virtual {v0, v7}, Landroid/widget/EditText;->setVisibility(I)V

    .line 322
    iget-object v0, p0, Leog;->aI:Landroid/widget/Button;

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 323
    iget-object v0, p0, Leog;->ap:Landroid/widget/EditText;

    const v1, 0x7f0a037e

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(I)V

    .line 329
    :goto_0
    iget-object v0, p0, Leog;->ap:Landroid/widget/EditText;

    iget-object v1, p0, Leog;->az:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 330
    iget-object v0, p0, Leog;->aq:Landroid/widget/EditText;

    iget-object v1, p0, Leog;->aB:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 331
    iget-object v0, p0, Leog;->ar:Landroid/widget/EditText;

    iget-object v1, p0, Leog;->aD:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 332
    invoke-direct {p0}, Leog;->al()V

    .line 334
    iget-boolean v0, p0, Leog;->aF:Z

    if-eqz v0, :cond_1

    .line 335
    iget-object v0, p0, Leog;->ao:Landroid/view/ViewGroup;

    iget-object v1, p0, Leog;->ap:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 336
    iget-object v0, p0, Leog;->ao:Landroid/view/ViewGroup;

    iget-object v1, p0, Leog;->ap:Landroid/widget/EditText;

    invoke-virtual {v0, v1, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 342
    :goto_1
    iget-boolean v0, p0, Leog;->aH:Z

    if-eqz v0, :cond_2

    .line 343
    iget-boolean v0, p0, Leog;->aG:Z

    invoke-direct {p0, v0}, Leog;->a(Z)V

    .line 361
    :goto_2
    return-void

    .line 325
    :cond_0
    iget-object v0, p0, Leog;->aq:Landroid/widget/EditText;

    invoke-virtual {v0, v6}, Landroid/widget/EditText;->setVisibility(I)V

    .line 326
    iget-object v0, p0, Leog;->aI:Landroid/widget/Button;

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setVisibility(I)V

    .line 327
    iget-object v0, p0, Leog;->ap:Landroid/widget/EditText;

    const v1, 0x7f0a037d

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(I)V

    goto :goto_0

    .line 338
    :cond_1
    iget-object v0, p0, Leog;->ao:Landroid/view/ViewGroup;

    iget-object v1, p0, Leog;->aq:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 339
    iget-object v0, p0, Leog;->ao:Landroid/view/ViewGroup;

    iget-object v1, p0, Leog;->aq:Landroid/widget/EditText;

    invoke-virtual {v0, v1, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    goto :goto_1

    .line 344
    :cond_2
    invoke-direct {p0}, Leog;->ar()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 345
    iget v0, p0, Leog;->aE:I

    packed-switch v0, :pswitch_data_0

    .line 356
    :goto_3
    invoke-direct {p0, v5}, Leog;->a(Z)V

    .line 357
    invoke-direct {p0, v5}, Leog;->b(Z)V

    goto :goto_2

    .line 347
    :pswitch_0
    iget-object v0, p0, Leog;->aL:Landroid/widget/RadioButton;

    invoke-virtual {v0, v5}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_3

    .line 350
    :pswitch_1
    iget-object v0, p0, Leog;->aM:Landroid/widget/RadioButton;

    invoke-virtual {v0, v5}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_3

    .line 353
    :pswitch_2
    iget-object v0, p0, Leog;->aN:Landroid/widget/RadioButton;

    invoke-virtual {v0, v5}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_3

    .line 359
    :cond_3
    iget-object v0, p0, Leog;->aL:Landroid/widget/RadioButton;

    invoke-virtual {v0, v5}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_2

    .line 345
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 209
    const-string v0, "more_options_visible"

    iget-boolean v1, p0, Leog;->aG:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 210
    const-string v0, "given_name_first"

    iget-boolean v1, p0, Leog;->aF:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 211
    const-string v0, "changed_more_options_visible"

    iget-boolean v1, p0, Leog;->aH:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 212
    const-string v0, "name_violation"

    iget-boolean v1, p0, Leog;->aP:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 213
    invoke-super {p0, p1}, Lenl;->e(Landroid/os/Bundle;)V

    .line 214
    return-void
.end method

.method protected e(Landroid/view/View;)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 469
    invoke-super {p0, p1}, Lenl;->e(Landroid/view/View;)V

    .line 471
    iget-object v0, p0, Leog;->T:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 472
    sget-object v0, Leog;->an:Ljava/lang/Runnable;

    invoke-static {}, Llsx;->d()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Leog;->as:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Leog;->aw:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 474
    :cond_0
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    .line 197
    invoke-super {p0}, Lenl;->f()V

    .line 202
    iget-object v0, p0, Leog;->ap:Landroid/widget/EditText;

    iget-object v1, p0, Leog;->aS:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 203
    iget-object v0, p0, Leog;->aq:Landroid/widget/EditText;

    iget-object v1, p0, Leog;->aS:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 204
    iget-object v0, p0, Leog;->ar:Landroid/widget/EditText;

    iget-object v1, p0, Leog;->aT:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 205
    return-void
.end method

.method protected f(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 478
    invoke-super {p0, p1}, Lenl;->f(Landroid/view/View;)V

    .line 480
    iget-boolean v0, p0, Leog;->aO:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Leog;->T:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 481
    invoke-virtual {p0}, Leog;->o()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0383

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Leog;->d(Ljava/lang/String;)V

    .line 483
    :cond_0
    return-void
.end method
