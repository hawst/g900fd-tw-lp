.class public final Locn;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Locn;


# instance fields
.field public b:Lohp;

.field public c:Ljava/lang/String;

.field public d:I

.field public e:Loco;

.field public f:Loce;

.field public g:Locp;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    new-array v0, v0, [Locn;

    sput-object v0, Locn;->a:[Locn;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 45
    invoke-direct {p0}, Loxq;-><init>()V

    .line 48
    iput-object v1, p0, Locn;->b:Lohp;

    .line 53
    const/high16 v0, -0x80000000

    iput v0, p0, Locn;->d:I

    .line 56
    iput-object v1, p0, Locn;->e:Loco;

    .line 59
    iput-object v1, p0, Locn;->f:Loce;

    .line 62
    iput-object v1, p0, Locn;->g:Locp;

    .line 45
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 91
    const/4 v0, 0x0

    .line 92
    iget-object v1, p0, Locn;->b:Lohp;

    if-eqz v1, :cond_0

    .line 93
    const/4 v0, 0x1

    iget-object v1, p0, Locn;->b:Lohp;

    .line 94
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 96
    :cond_0
    iget-object v1, p0, Locn;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 97
    const/4 v1, 0x2

    iget-object v2, p0, Locn;->c:Ljava/lang/String;

    .line 98
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 100
    :cond_1
    iget v1, p0, Locn;->d:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_2

    .line 101
    const/4 v1, 0x3

    iget v2, p0, Locn;->d:I

    .line 102
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 104
    :cond_2
    iget-object v1, p0, Locn;->e:Loco;

    if-eqz v1, :cond_3

    .line 105
    const/4 v1, 0x4

    iget-object v2, p0, Locn;->e:Loco;

    .line 106
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 108
    :cond_3
    iget-object v1, p0, Locn;->f:Loce;

    if-eqz v1, :cond_4

    .line 109
    const/4 v1, 0x5

    iget-object v2, p0, Locn;->f:Loce;

    .line 110
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 112
    :cond_4
    iget-object v1, p0, Locn;->g:Locp;

    if-eqz v1, :cond_5

    .line 113
    const/4 v1, 0x6

    iget-object v2, p0, Locn;->g:Locp;

    .line 114
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 116
    :cond_5
    iget-object v1, p0, Locn;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 117
    iput v0, p0, Locn;->ai:I

    .line 118
    return v0
.end method

.method public a(Loxn;)Locn;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 126
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 127
    sparse-switch v0, :sswitch_data_0

    .line 131
    iget-object v1, p0, Locn;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 132
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Locn;->ah:Ljava/util/List;

    .line 135
    :cond_1
    iget-object v1, p0, Locn;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 137
    :sswitch_0
    return-object p0

    .line 142
    :sswitch_1
    iget-object v0, p0, Locn;->b:Lohp;

    if-nez v0, :cond_2

    .line 143
    new-instance v0, Lohp;

    invoke-direct {v0}, Lohp;-><init>()V

    iput-object v0, p0, Locn;->b:Lohp;

    .line 145
    :cond_2
    iget-object v0, p0, Locn;->b:Lohp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 149
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Locn;->c:Ljava/lang/String;

    goto :goto_0

    .line 153
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 154
    if-eq v0, v2, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    const/4 v1, 0x4

    if-eq v0, v1, :cond_3

    const/4 v1, 0x5

    if-eq v0, v1, :cond_3

    const/4 v1, 0x6

    if-eq v0, v1, :cond_3

    const/4 v1, 0x7

    if-ne v0, v1, :cond_4

    .line 161
    :cond_3
    iput v0, p0, Locn;->d:I

    goto :goto_0

    .line 163
    :cond_4
    iput v2, p0, Locn;->d:I

    goto :goto_0

    .line 168
    :sswitch_4
    iget-object v0, p0, Locn;->e:Loco;

    if-nez v0, :cond_5

    .line 169
    new-instance v0, Loco;

    invoke-direct {v0}, Loco;-><init>()V

    iput-object v0, p0, Locn;->e:Loco;

    .line 171
    :cond_5
    iget-object v0, p0, Locn;->e:Loco;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 175
    :sswitch_5
    iget-object v0, p0, Locn;->f:Loce;

    if-nez v0, :cond_6

    .line 176
    new-instance v0, Loce;

    invoke-direct {v0}, Loce;-><init>()V

    iput-object v0, p0, Locn;->f:Loce;

    .line 178
    :cond_6
    iget-object v0, p0, Locn;->f:Loce;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 182
    :sswitch_6
    iget-object v0, p0, Locn;->g:Locp;

    if-nez v0, :cond_7

    .line 183
    new-instance v0, Locp;

    invoke-direct {v0}, Locp;-><init>()V

    iput-object v0, p0, Locn;->g:Locp;

    .line 185
    :cond_7
    iget-object v0, p0, Locn;->g:Locp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 127
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Locn;->b:Lohp;

    if-eqz v0, :cond_0

    .line 68
    const/4 v0, 0x1

    iget-object v1, p0, Locn;->b:Lohp;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 70
    :cond_0
    iget-object v0, p0, Locn;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 71
    const/4 v0, 0x2

    iget-object v1, p0, Locn;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 73
    :cond_1
    iget v0, p0, Locn;->d:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_2

    .line 74
    const/4 v0, 0x3

    iget v1, p0, Locn;->d:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 76
    :cond_2
    iget-object v0, p0, Locn;->e:Loco;

    if-eqz v0, :cond_3

    .line 77
    const/4 v0, 0x4

    iget-object v1, p0, Locn;->e:Loco;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 79
    :cond_3
    iget-object v0, p0, Locn;->f:Loce;

    if-eqz v0, :cond_4

    .line 80
    const/4 v0, 0x5

    iget-object v1, p0, Locn;->f:Loce;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 82
    :cond_4
    iget-object v0, p0, Locn;->g:Locp;

    if-eqz v0, :cond_5

    .line 83
    const/4 v0, 0x6

    iget-object v1, p0, Locn;->g:Locp;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 85
    :cond_5
    iget-object v0, p0, Locn;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 87
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p0, p1}, Locn;->a(Loxn;)Locn;

    move-result-object v0

    return-object v0
.end method
