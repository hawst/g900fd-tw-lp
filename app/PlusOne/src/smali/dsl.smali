.class public final enum Ldsl;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Ldsl;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Ldsl;

.field public static final enum b:Ldsl;

.field public static final enum c:Ldsl;

.field private static final synthetic d:[Ldsl;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 93
    new-instance v0, Ldsl;

    const-string v1, "TICKLE"

    invoke-direct {v0, v1, v2}, Ldsl;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldsl;->a:Ldsl;

    .line 94
    new-instance v0, Ldsl;

    const-string v1, "ACCOUNT_SYNC"

    invoke-direct {v0, v1, v3}, Ldsl;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldsl;->b:Ldsl;

    .line 95
    new-instance v0, Ldsl;

    const-string v1, "USER_INITIATED"

    invoke-direct {v0, v1, v4}, Ldsl;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldsl;->c:Ldsl;

    .line 92
    const/4 v0, 0x3

    new-array v0, v0, [Ldsl;

    sget-object v1, Ldsl;->a:Ldsl;

    aput-object v1, v0, v2

    sget-object v1, Ldsl;->b:Ldsl;

    aput-object v1, v0, v3

    sget-object v1, Ldsl;->c:Ldsl;

    aput-object v1, v0, v4

    sput-object v0, Ldsl;->d:[Ldsl;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 92
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ldsl;
    .locals 1

    .prologue
    .line 92
    const-class v0, Ldsl;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ldsl;

    return-object v0
.end method

.method public static values()[Ldsl;
    .locals 1

    .prologue
    .line 92
    sget-object v0, Ldsl;->d:[Ldsl;

    invoke-virtual {v0}, [Ldsl;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldsl;

    return-object v0
.end method
