.class public final Lowe;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Long;

.field private b:[Lowg;

.field private c:[Lowf;

.field private d:Ljava/lang/Integer;

.field private e:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 150
    invoke-direct {p0}, Loxq;-><init>()V

    .line 382
    sget-object v0, Lowg;->a:[Lowg;

    iput-object v0, p0, Lowe;->b:[Lowg;

    .line 385
    sget-object v0, Lowf;->a:[Lowf;

    iput-object v0, p0, Lowe;->c:[Lowf;

    .line 150
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 423
    .line 424
    iget-object v0, p0, Lowe;->a:Ljava/lang/Long;

    if-eqz v0, :cond_6

    .line 425
    const/4 v0, 0x1

    iget-object v2, p0, Lowe;->a:Ljava/lang/Long;

    .line 426
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Loxo;->f(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 428
    :goto_0
    iget-object v2, p0, Lowe;->b:[Lowg;

    if-eqz v2, :cond_1

    .line 429
    iget-object v3, p0, Lowe;->b:[Lowg;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 430
    if-eqz v5, :cond_0

    .line 431
    const/4 v6, 0x2

    .line 432
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 429
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 436
    :cond_1
    iget-object v2, p0, Lowe;->c:[Lowf;

    if-eqz v2, :cond_3

    .line 437
    iget-object v2, p0, Lowe;->c:[Lowf;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 438
    if-eqz v4, :cond_2

    .line 439
    const/4 v5, 0x3

    .line 440
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 437
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 444
    :cond_3
    iget-object v1, p0, Lowe;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 445
    const/4 v1, 0x4

    iget-object v2, p0, Lowe;->d:Ljava/lang/Integer;

    .line 446
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 448
    :cond_4
    iget-object v1, p0, Lowe;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 449
    const/4 v1, 0x5

    iget-object v2, p0, Lowe;->e:Ljava/lang/Integer;

    .line 450
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 452
    :cond_5
    iget-object v1, p0, Lowe;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 453
    iput v0, p0, Lowe;->ai:I

    .line 454
    return v0

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lowe;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 462
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 463
    sparse-switch v0, :sswitch_data_0

    .line 467
    iget-object v2, p0, Lowe;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 468
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lowe;->ah:Ljava/util/List;

    .line 471
    :cond_1
    iget-object v2, p0, Lowe;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 473
    :sswitch_0
    return-object p0

    .line 478
    :sswitch_1
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lowe;->a:Ljava/lang/Long;

    goto :goto_0

    .line 482
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 483
    iget-object v0, p0, Lowe;->b:[Lowg;

    if-nez v0, :cond_3

    move v0, v1

    .line 484
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lowg;

    .line 485
    iget-object v3, p0, Lowe;->b:[Lowg;

    if-eqz v3, :cond_2

    .line 486
    iget-object v3, p0, Lowe;->b:[Lowg;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 488
    :cond_2
    iput-object v2, p0, Lowe;->b:[Lowg;

    .line 489
    :goto_2
    iget-object v2, p0, Lowe;->b:[Lowg;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 490
    iget-object v2, p0, Lowe;->b:[Lowg;

    new-instance v3, Lowg;

    invoke-direct {v3}, Lowg;-><init>()V

    aput-object v3, v2, v0

    .line 491
    iget-object v2, p0, Lowe;->b:[Lowg;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 492
    invoke-virtual {p1}, Loxn;->a()I

    .line 489
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 483
    :cond_3
    iget-object v0, p0, Lowe;->b:[Lowg;

    array-length v0, v0

    goto :goto_1

    .line 495
    :cond_4
    iget-object v2, p0, Lowe;->b:[Lowg;

    new-instance v3, Lowg;

    invoke-direct {v3}, Lowg;-><init>()V

    aput-object v3, v2, v0

    .line 496
    iget-object v2, p0, Lowe;->b:[Lowg;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 500
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 501
    iget-object v0, p0, Lowe;->c:[Lowf;

    if-nez v0, :cond_6

    move v0, v1

    .line 502
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lowf;

    .line 503
    iget-object v3, p0, Lowe;->c:[Lowf;

    if-eqz v3, :cond_5

    .line 504
    iget-object v3, p0, Lowe;->c:[Lowf;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 506
    :cond_5
    iput-object v2, p0, Lowe;->c:[Lowf;

    .line 507
    :goto_4
    iget-object v2, p0, Lowe;->c:[Lowf;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    .line 508
    iget-object v2, p0, Lowe;->c:[Lowf;

    new-instance v3, Lowf;

    invoke-direct {v3}, Lowf;-><init>()V

    aput-object v3, v2, v0

    .line 509
    iget-object v2, p0, Lowe;->c:[Lowf;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 510
    invoke-virtual {p1}, Loxn;->a()I

    .line 507
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 501
    :cond_6
    iget-object v0, p0, Lowe;->c:[Lowf;

    array-length v0, v0

    goto :goto_3

    .line 513
    :cond_7
    iget-object v2, p0, Lowe;->c:[Lowf;

    new-instance v3, Lowf;

    invoke-direct {v3}, Lowf;-><init>()V

    aput-object v3, v2, v0

    .line 514
    iget-object v2, p0, Lowe;->c:[Lowf;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 518
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lowe;->d:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 522
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lowe;->e:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 463
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 394
    iget-object v1, p0, Lowe;->a:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 395
    const/4 v1, 0x1

    iget-object v2, p0, Lowe;->a:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Loxo;->b(IJ)V

    .line 397
    :cond_0
    iget-object v1, p0, Lowe;->b:[Lowg;

    if-eqz v1, :cond_2

    .line 398
    iget-object v2, p0, Lowe;->b:[Lowg;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 399
    if-eqz v4, :cond_1

    .line 400
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 398
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 404
    :cond_2
    iget-object v1, p0, Lowe;->c:[Lowf;

    if-eqz v1, :cond_4

    .line 405
    iget-object v1, p0, Lowe;->c:[Lowf;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 406
    if-eqz v3, :cond_3

    .line 407
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 405
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 411
    :cond_4
    iget-object v0, p0, Lowe;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 412
    const/4 v0, 0x4

    iget-object v1, p0, Lowe;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 414
    :cond_5
    iget-object v0, p0, Lowe;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 415
    const/4 v0, 0x5

    iget-object v1, p0, Lowe;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 417
    :cond_6
    iget-object v0, p0, Lowe;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 419
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 146
    invoke-virtual {p0, p1}, Lowe;->a(Loxn;)Lowe;

    move-result-object v0

    return-object v0
.end method
