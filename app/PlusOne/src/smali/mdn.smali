.class public final Lmdn;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnce;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 21544
    invoke-direct {p0}, Loxq;-><init>()V

    .line 21547
    iput-object v0, p0, Lmdn;->apiHeader:Llyr;

    .line 21550
    iput-object v0, p0, Lmdn;->a:Lnce;

    .line 21544
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 21567
    const/4 v0, 0x0

    .line 21568
    iget-object v1, p0, Lmdn;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 21569
    const/4 v0, 0x1

    iget-object v1, p0, Lmdn;->apiHeader:Llyr;

    .line 21570
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 21572
    :cond_0
    iget-object v1, p0, Lmdn;->a:Lnce;

    if-eqz v1, :cond_1

    .line 21573
    const/4 v1, 0x2

    iget-object v2, p0, Lmdn;->a:Lnce;

    .line 21574
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 21576
    :cond_1
    iget-object v1, p0, Lmdn;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 21577
    iput v0, p0, Lmdn;->ai:I

    .line 21578
    return v0
.end method

.method public a(Loxn;)Lmdn;
    .locals 2

    .prologue
    .line 21586
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 21587
    sparse-switch v0, :sswitch_data_0

    .line 21591
    iget-object v1, p0, Lmdn;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 21592
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmdn;->ah:Ljava/util/List;

    .line 21595
    :cond_1
    iget-object v1, p0, Lmdn;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 21597
    :sswitch_0
    return-object p0

    .line 21602
    :sswitch_1
    iget-object v0, p0, Lmdn;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 21603
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmdn;->apiHeader:Llyr;

    .line 21605
    :cond_2
    iget-object v0, p0, Lmdn;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 21609
    :sswitch_2
    iget-object v0, p0, Lmdn;->a:Lnce;

    if-nez v0, :cond_3

    .line 21610
    new-instance v0, Lnce;

    invoke-direct {v0}, Lnce;-><init>()V

    iput-object v0, p0, Lmdn;->a:Lnce;

    .line 21612
    :cond_3
    iget-object v0, p0, Lmdn;->a:Lnce;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 21587
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 21555
    iget-object v0, p0, Lmdn;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 21556
    const/4 v0, 0x1

    iget-object v1, p0, Lmdn;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 21558
    :cond_0
    iget-object v0, p0, Lmdn;->a:Lnce;

    if-eqz v0, :cond_1

    .line 21559
    const/4 v0, 0x2

    iget-object v1, p0, Lmdn;->a:Lnce;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 21561
    :cond_1
    iget-object v0, p0, Lmdn;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 21563
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 21540
    invoke-virtual {p0, p1}, Lmdn;->a(Loxn;)Lmdn;

    move-result-object v0

    return-object v0
.end method
