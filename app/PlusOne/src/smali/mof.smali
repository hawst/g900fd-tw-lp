.class public final Lmof;
.super Loxq;
.source "PG"


# instance fields
.field public A:Lmnt;

.field public B:I

.field public C:Ljava/lang/Float;

.field public D:Lmnd;

.field public E:Lmoh;

.field private F:Ljava/lang/Float;

.field private G:I

.field private H:Lmnp;

.field public a:Lmnu;

.field public b:Lmoa;

.field public c:Ljava/lang/Float;

.field public d:Lmnv;

.field public e:Ljava/lang/Integer;

.field public f:Lmnc;

.field public g:Lmne;

.field public h:Lmne;

.field public i:Ljava/lang/Float;

.field public j:Ljava/lang/String;

.field public k:I

.field public l:Ljava/lang/Integer;

.field public m:I

.field public n:I

.field public o:Lmnq;

.field public p:Lmnu;

.field public q:Ljava/lang/Float;

.field public r:Lmne;

.field public s:Lmnr;

.field public t:Ljava/lang/Float;

.field public u:Ljava/lang/Float;

.field public v:Lmoe;

.field public w:Ljava/lang/Integer;

.field public x:[Lmog;

.field public y:[I

.field public z:Lmnb;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    const/4 v1, 0x0

    .line 4386
    invoke-direct {p0}, Loxq;-><init>()V

    .line 4523
    iput-object v1, p0, Lmof;->a:Lmnu;

    .line 4526
    iput-object v1, p0, Lmof;->b:Lmoa;

    .line 4531
    iput-object v1, p0, Lmof;->d:Lmnv;

    .line 4536
    iput-object v1, p0, Lmof;->f:Lmnc;

    .line 4539
    iput-object v1, p0, Lmof;->g:Lmne;

    .line 4542
    iput-object v1, p0, Lmof;->h:Lmne;

    .line 4549
    iput v2, p0, Lmof;->k:I

    .line 4554
    iput v2, p0, Lmof;->m:I

    .line 4557
    iput v2, p0, Lmof;->n:I

    .line 4560
    iput-object v1, p0, Lmof;->o:Lmnq;

    .line 4563
    iput-object v1, p0, Lmof;->p:Lmnu;

    .line 4570
    iput-object v1, p0, Lmof;->r:Lmne;

    .line 4573
    iput-object v1, p0, Lmof;->s:Lmnr;

    .line 4576
    iput v2, p0, Lmof;->G:I

    .line 4581
    iput-object v1, p0, Lmof;->H:Lmnp;

    .line 4586
    iput-object v1, p0, Lmof;->v:Lmoe;

    .line 4591
    sget-object v0, Lmog;->a:[Lmog;

    iput-object v0, p0, Lmof;->x:[Lmog;

    .line 4594
    sget-object v0, Loxx;->a:[I

    iput-object v0, p0, Lmof;->y:[I

    .line 4597
    iput-object v1, p0, Lmof;->z:Lmnb;

    .line 4600
    iput-object v1, p0, Lmof;->A:Lmnt;

    .line 4603
    iput v2, p0, Lmof;->B:I

    .line 4608
    iput-object v1, p0, Lmof;->D:Lmnd;

    .line 4611
    iput-object v1, p0, Lmof;->E:Lmoh;

    .line 4386
    return-void
.end method


# virtual methods
.method public a()I
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/high16 v7, -0x80000000

    .line 4730
    .line 4731
    iget-object v0, p0, Lmof;->a:Lmnu;

    if-eqz v0, :cond_23

    .line 4732
    const/4 v0, 0x1

    iget-object v2, p0, Lmof;->a:Lmnu;

    .line 4733
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 4735
    :goto_0
    iget-object v2, p0, Lmof;->b:Lmoa;

    if-eqz v2, :cond_0

    .line 4736
    const/4 v2, 0x2

    iget-object v3, p0, Lmof;->b:Lmoa;

    .line 4737
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 4739
    :cond_0
    iget-object v2, p0, Lmof;->c:Ljava/lang/Float;

    if-eqz v2, :cond_1

    .line 4740
    const/4 v2, 0x3

    iget-object v3, p0, Lmof;->c:Ljava/lang/Float;

    .line 4741
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 4743
    :cond_1
    iget-object v2, p0, Lmof;->d:Lmnv;

    if-eqz v2, :cond_2

    .line 4744
    const/4 v2, 0x4

    iget-object v3, p0, Lmof;->d:Lmnv;

    .line 4745
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 4747
    :cond_2
    iget-object v2, p0, Lmof;->e:Ljava/lang/Integer;

    if-eqz v2, :cond_3

    .line 4748
    const/4 v2, 0x5

    iget-object v3, p0, Lmof;->e:Ljava/lang/Integer;

    .line 4749
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 4751
    :cond_3
    iget-object v2, p0, Lmof;->f:Lmnc;

    if-eqz v2, :cond_4

    .line 4752
    const/4 v2, 0x6

    iget-object v3, p0, Lmof;->f:Lmnc;

    .line 4753
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 4755
    :cond_4
    iget-object v2, p0, Lmof;->g:Lmne;

    if-eqz v2, :cond_5

    .line 4756
    const/4 v2, 0x7

    iget-object v3, p0, Lmof;->g:Lmne;

    .line 4757
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 4759
    :cond_5
    iget-object v2, p0, Lmof;->h:Lmne;

    if-eqz v2, :cond_6

    .line 4760
    const/16 v2, 0x8

    iget-object v3, p0, Lmof;->h:Lmne;

    .line 4761
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 4763
    :cond_6
    iget-object v2, p0, Lmof;->i:Ljava/lang/Float;

    if-eqz v2, :cond_7

    .line 4764
    const/16 v2, 0x9

    iget-object v3, p0, Lmof;->i:Ljava/lang/Float;

    .line 4765
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 4767
    :cond_7
    iget-object v2, p0, Lmof;->j:Ljava/lang/String;

    if-eqz v2, :cond_8

    .line 4768
    const/16 v2, 0xa

    iget-object v3, p0, Lmof;->j:Ljava/lang/String;

    .line 4769
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 4771
    :cond_8
    iget-object v2, p0, Lmof;->l:Ljava/lang/Integer;

    if-eqz v2, :cond_9

    .line 4772
    const/16 v2, 0xb

    iget-object v3, p0, Lmof;->l:Ljava/lang/Integer;

    .line 4773
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 4775
    :cond_9
    iget v2, p0, Lmof;->m:I

    if-eq v2, v7, :cond_a

    .line 4776
    const/16 v2, 0xc

    iget v3, p0, Lmof;->m:I

    .line 4777
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 4779
    :cond_a
    iget v2, p0, Lmof;->n:I

    if-eq v2, v7, :cond_b

    .line 4780
    const/16 v2, 0xd

    iget v3, p0, Lmof;->n:I

    .line 4781
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 4783
    :cond_b
    iget-object v2, p0, Lmof;->o:Lmnq;

    if-eqz v2, :cond_c

    .line 4784
    const/16 v2, 0xe

    iget-object v3, p0, Lmof;->o:Lmnq;

    .line 4785
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 4787
    :cond_c
    iget-object v2, p0, Lmof;->p:Lmnu;

    if-eqz v2, :cond_d

    .line 4788
    const/16 v2, 0xf

    iget-object v3, p0, Lmof;->p:Lmnu;

    .line 4789
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 4791
    :cond_d
    iget-object v2, p0, Lmof;->F:Ljava/lang/Float;

    if-eqz v2, :cond_e

    .line 4792
    const/16 v2, 0x10

    iget-object v3, p0, Lmof;->F:Ljava/lang/Float;

    .line 4793
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 4795
    :cond_e
    iget-object v2, p0, Lmof;->q:Ljava/lang/Float;

    if-eqz v2, :cond_f

    .line 4796
    const/16 v2, 0x11

    iget-object v3, p0, Lmof;->q:Ljava/lang/Float;

    .line 4797
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 4799
    :cond_f
    iget-object v2, p0, Lmof;->r:Lmne;

    if-eqz v2, :cond_10

    .line 4800
    const/16 v2, 0x12

    iget-object v3, p0, Lmof;->r:Lmne;

    .line 4801
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 4803
    :cond_10
    iget-object v2, p0, Lmof;->s:Lmnr;

    if-eqz v2, :cond_11

    .line 4804
    const/16 v2, 0x13

    iget-object v3, p0, Lmof;->s:Lmnr;

    .line 4805
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 4807
    :cond_11
    iget v2, p0, Lmof;->G:I

    if-eq v2, v7, :cond_12

    .line 4808
    const/16 v2, 0x14

    iget v3, p0, Lmof;->G:I

    .line 4809
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 4811
    :cond_12
    iget-object v2, p0, Lmof;->t:Ljava/lang/Float;

    if-eqz v2, :cond_13

    .line 4812
    const/16 v2, 0x15

    iget-object v3, p0, Lmof;->t:Ljava/lang/Float;

    .line 4813
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 4815
    :cond_13
    iget v2, p0, Lmof;->k:I

    if-eq v2, v7, :cond_14

    .line 4816
    const/16 v2, 0x16

    iget v3, p0, Lmof;->k:I

    .line 4817
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 4819
    :cond_14
    iget-object v2, p0, Lmof;->H:Lmnp;

    if-eqz v2, :cond_15

    .line 4820
    const/16 v2, 0x17

    iget-object v3, p0, Lmof;->H:Lmnp;

    .line 4821
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 4823
    :cond_15
    iget-object v2, p0, Lmof;->u:Ljava/lang/Float;

    if-eqz v2, :cond_16

    .line 4824
    const/16 v2, 0x18

    iget-object v3, p0, Lmof;->u:Ljava/lang/Float;

    .line 4825
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 4827
    :cond_16
    iget-object v2, p0, Lmof;->w:Ljava/lang/Integer;

    if-eqz v2, :cond_17

    .line 4828
    const/16 v2, 0x19

    iget-object v3, p0, Lmof;->w:Ljava/lang/Integer;

    .line 4829
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 4831
    :cond_17
    iget-object v2, p0, Lmof;->x:[Lmog;

    if-eqz v2, :cond_19

    .line 4832
    iget-object v3, p0, Lmof;->x:[Lmog;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_19

    aget-object v5, v3, v2

    .line 4833
    if-eqz v5, :cond_18

    .line 4834
    const/16 v6, 0x1a

    .line 4835
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 4832
    :cond_18
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 4839
    :cond_19
    iget-object v2, p0, Lmof;->y:[I

    if-eqz v2, :cond_1b

    iget-object v2, p0, Lmof;->y:[I

    array-length v2, v2

    if-lez v2, :cond_1b

    .line 4841
    iget-object v3, p0, Lmof;->y:[I

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v1, v4, :cond_1a

    aget v5, v3, v1

    .line 4843
    invoke-static {v5}, Loxo;->i(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 4841
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 4845
    :cond_1a
    add-int/2addr v0, v2

    .line 4846
    iget-object v1, p0, Lmof;->y:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    .line 4848
    :cond_1b
    iget-object v1, p0, Lmof;->z:Lmnb;

    if-eqz v1, :cond_1c

    .line 4849
    const/16 v1, 0x1c

    iget-object v2, p0, Lmof;->z:Lmnb;

    .line 4850
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4852
    :cond_1c
    iget-object v1, p0, Lmof;->v:Lmoe;

    if-eqz v1, :cond_1d

    .line 4853
    const/16 v1, 0x1d

    iget-object v2, p0, Lmof;->v:Lmoe;

    .line 4854
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4856
    :cond_1d
    iget-object v1, p0, Lmof;->A:Lmnt;

    if-eqz v1, :cond_1e

    .line 4857
    const/16 v1, 0x1e

    iget-object v2, p0, Lmof;->A:Lmnt;

    .line 4858
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4860
    :cond_1e
    iget v1, p0, Lmof;->B:I

    if-eq v1, v7, :cond_1f

    .line 4861
    const/16 v1, 0x1f

    iget v2, p0, Lmof;->B:I

    .line 4862
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4864
    :cond_1f
    iget-object v1, p0, Lmof;->C:Ljava/lang/Float;

    if-eqz v1, :cond_20

    .line 4865
    const/16 v1, 0x20

    iget-object v2, p0, Lmof;->C:Ljava/lang/Float;

    .line 4866
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 4868
    :cond_20
    iget-object v1, p0, Lmof;->D:Lmnd;

    if-eqz v1, :cond_21

    .line 4869
    const/16 v1, 0x21

    iget-object v2, p0, Lmof;->D:Lmnd;

    .line 4870
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4872
    :cond_21
    iget-object v1, p0, Lmof;->E:Lmoh;

    if-eqz v1, :cond_22

    .line 4873
    const/16 v1, 0x22

    iget-object v2, p0, Lmof;->E:Lmoh;

    .line 4874
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4876
    :cond_22
    iget-object v1, p0, Lmof;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4877
    iput v0, p0, Lmof;->ai:I

    .line 4878
    return v0

    :cond_23
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lmof;
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 4886
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 4887
    sparse-switch v0, :sswitch_data_0

    .line 4891
    iget-object v2, p0, Lmof;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 4892
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmof;->ah:Ljava/util/List;

    .line 4895
    :cond_1
    iget-object v2, p0, Lmof;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4897
    :sswitch_0
    return-object p0

    .line 4902
    :sswitch_1
    iget-object v0, p0, Lmof;->a:Lmnu;

    if-nez v0, :cond_2

    .line 4903
    new-instance v0, Lmnu;

    invoke-direct {v0}, Lmnu;-><init>()V

    iput-object v0, p0, Lmof;->a:Lmnu;

    .line 4905
    :cond_2
    iget-object v0, p0, Lmof;->a:Lmnu;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 4909
    :sswitch_2
    iget-object v0, p0, Lmof;->b:Lmoa;

    if-nez v0, :cond_3

    .line 4910
    new-instance v0, Lmoa;

    invoke-direct {v0}, Lmoa;-><init>()V

    iput-object v0, p0, Lmof;->b:Lmoa;

    .line 4912
    :cond_3
    iget-object v0, p0, Lmof;->b:Lmoa;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 4916
    :sswitch_3
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lmof;->c:Ljava/lang/Float;

    goto :goto_0

    .line 4920
    :sswitch_4
    iget-object v0, p0, Lmof;->d:Lmnv;

    if-nez v0, :cond_4

    .line 4921
    new-instance v0, Lmnv;

    invoke-direct {v0}, Lmnv;-><init>()V

    iput-object v0, p0, Lmof;->d:Lmnv;

    .line 4923
    :cond_4
    iget-object v0, p0, Lmof;->d:Lmnv;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 4927
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmof;->e:Ljava/lang/Integer;

    goto :goto_0

    .line 4931
    :sswitch_6
    iget-object v0, p0, Lmof;->f:Lmnc;

    if-nez v0, :cond_5

    .line 4932
    new-instance v0, Lmnc;

    invoke-direct {v0}, Lmnc;-><init>()V

    iput-object v0, p0, Lmof;->f:Lmnc;

    .line 4934
    :cond_5
    iget-object v0, p0, Lmof;->f:Lmnc;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 4938
    :sswitch_7
    iget-object v0, p0, Lmof;->g:Lmne;

    if-nez v0, :cond_6

    .line 4939
    new-instance v0, Lmne;

    invoke-direct {v0}, Lmne;-><init>()V

    iput-object v0, p0, Lmof;->g:Lmne;

    .line 4941
    :cond_6
    iget-object v0, p0, Lmof;->g:Lmne;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 4945
    :sswitch_8
    iget-object v0, p0, Lmof;->h:Lmne;

    if-nez v0, :cond_7

    .line 4946
    new-instance v0, Lmne;

    invoke-direct {v0}, Lmne;-><init>()V

    iput-object v0, p0, Lmof;->h:Lmne;

    .line 4948
    :cond_7
    iget-object v0, p0, Lmof;->h:Lmne;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 4952
    :sswitch_9
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lmof;->i:Ljava/lang/Float;

    goto/16 :goto_0

    .line 4956
    :sswitch_a
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmof;->j:Ljava/lang/String;

    goto/16 :goto_0

    .line 4960
    :sswitch_b
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmof;->l:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 4964
    :sswitch_c
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 4965
    if-eqz v0, :cond_8

    if-eq v0, v4, :cond_8

    if-eq v0, v5, :cond_8

    if-ne v0, v6, :cond_9

    .line 4969
    :cond_8
    iput v0, p0, Lmof;->m:I

    goto/16 :goto_0

    .line 4971
    :cond_9
    iput v1, p0, Lmof;->m:I

    goto/16 :goto_0

    .line 4976
    :sswitch_d
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 4977
    if-eqz v0, :cond_a

    if-ne v0, v4, :cond_b

    .line 4979
    :cond_a
    iput v0, p0, Lmof;->n:I

    goto/16 :goto_0

    .line 4981
    :cond_b
    iput v1, p0, Lmof;->n:I

    goto/16 :goto_0

    .line 4986
    :sswitch_e
    iget-object v0, p0, Lmof;->o:Lmnq;

    if-nez v0, :cond_c

    .line 4987
    new-instance v0, Lmnq;

    invoke-direct {v0}, Lmnq;-><init>()V

    iput-object v0, p0, Lmof;->o:Lmnq;

    .line 4989
    :cond_c
    iget-object v0, p0, Lmof;->o:Lmnq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 4993
    :sswitch_f
    iget-object v0, p0, Lmof;->p:Lmnu;

    if-nez v0, :cond_d

    .line 4994
    new-instance v0, Lmnu;

    invoke-direct {v0}, Lmnu;-><init>()V

    iput-object v0, p0, Lmof;->p:Lmnu;

    .line 4996
    :cond_d
    iget-object v0, p0, Lmof;->p:Lmnu;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 5000
    :sswitch_10
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lmof;->F:Ljava/lang/Float;

    goto/16 :goto_0

    .line 5004
    :sswitch_11
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lmof;->q:Ljava/lang/Float;

    goto/16 :goto_0

    .line 5008
    :sswitch_12
    iget-object v0, p0, Lmof;->r:Lmne;

    if-nez v0, :cond_e

    .line 5009
    new-instance v0, Lmne;

    invoke-direct {v0}, Lmne;-><init>()V

    iput-object v0, p0, Lmof;->r:Lmne;

    .line 5011
    :cond_e
    iget-object v0, p0, Lmof;->r:Lmne;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 5015
    :sswitch_13
    iget-object v0, p0, Lmof;->s:Lmnr;

    if-nez v0, :cond_f

    .line 5016
    new-instance v0, Lmnr;

    invoke-direct {v0}, Lmnr;-><init>()V

    iput-object v0, p0, Lmof;->s:Lmnr;

    .line 5018
    :cond_f
    iget-object v0, p0, Lmof;->s:Lmnr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 5022
    :sswitch_14
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 5023
    if-eqz v0, :cond_10

    if-eq v0, v4, :cond_10

    if-eq v0, v5, :cond_10

    if-ne v0, v6, :cond_11

    .line 5027
    :cond_10
    iput v0, p0, Lmof;->G:I

    goto/16 :goto_0

    .line 5029
    :cond_11
    iput v1, p0, Lmof;->G:I

    goto/16 :goto_0

    .line 5034
    :sswitch_15
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lmof;->t:Ljava/lang/Float;

    goto/16 :goto_0

    .line 5038
    :sswitch_16
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 5039
    if-eqz v0, :cond_12

    if-eq v0, v4, :cond_12

    if-ne v0, v5, :cond_13

    .line 5042
    :cond_12
    iput v0, p0, Lmof;->k:I

    goto/16 :goto_0

    .line 5044
    :cond_13
    iput v1, p0, Lmof;->k:I

    goto/16 :goto_0

    .line 5049
    :sswitch_17
    iget-object v0, p0, Lmof;->H:Lmnp;

    if-nez v0, :cond_14

    .line 5050
    new-instance v0, Lmnp;

    invoke-direct {v0}, Lmnp;-><init>()V

    iput-object v0, p0, Lmof;->H:Lmnp;

    .line 5052
    :cond_14
    iget-object v0, p0, Lmof;->H:Lmnp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 5056
    :sswitch_18
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lmof;->u:Ljava/lang/Float;

    goto/16 :goto_0

    .line 5060
    :sswitch_19
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmof;->w:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 5064
    :sswitch_1a
    const/16 v0, 0xd2

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 5065
    iget-object v0, p0, Lmof;->x:[Lmog;

    if-nez v0, :cond_16

    move v0, v1

    .line 5066
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lmog;

    .line 5067
    iget-object v3, p0, Lmof;->x:[Lmog;

    if-eqz v3, :cond_15

    .line 5068
    iget-object v3, p0, Lmof;->x:[Lmog;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 5070
    :cond_15
    iput-object v2, p0, Lmof;->x:[Lmog;

    .line 5071
    :goto_2
    iget-object v2, p0, Lmof;->x:[Lmog;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_17

    .line 5072
    iget-object v2, p0, Lmof;->x:[Lmog;

    new-instance v3, Lmog;

    invoke-direct {v3}, Lmog;-><init>()V

    aput-object v3, v2, v0

    .line 5073
    iget-object v2, p0, Lmof;->x:[Lmog;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 5074
    invoke-virtual {p1}, Loxn;->a()I

    .line 5071
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 5065
    :cond_16
    iget-object v0, p0, Lmof;->x:[Lmog;

    array-length v0, v0

    goto :goto_1

    .line 5077
    :cond_17
    iget-object v2, p0, Lmof;->x:[Lmog;

    new-instance v3, Lmog;

    invoke-direct {v3}, Lmog;-><init>()V

    aput-object v3, v2, v0

    .line 5078
    iget-object v2, p0, Lmof;->x:[Lmog;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 5082
    :sswitch_1b
    const/16 v0, 0xd8

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 5083
    iget-object v0, p0, Lmof;->y:[I

    array-length v0, v0

    .line 5084
    add-int/2addr v2, v0

    new-array v2, v2, [I

    .line 5085
    iget-object v3, p0, Lmof;->y:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 5086
    iput-object v2, p0, Lmof;->y:[I

    .line 5087
    :goto_3
    iget-object v2, p0, Lmof;->y:[I

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_18

    .line 5088
    iget-object v2, p0, Lmof;->y:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v3

    aput v3, v2, v0

    .line 5089
    invoke-virtual {p1}, Loxn;->a()I

    .line 5087
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 5092
    :cond_18
    iget-object v2, p0, Lmof;->y:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v3

    aput v3, v2, v0

    goto/16 :goto_0

    .line 5096
    :sswitch_1c
    iget-object v0, p0, Lmof;->z:Lmnb;

    if-nez v0, :cond_19

    .line 5097
    new-instance v0, Lmnb;

    invoke-direct {v0}, Lmnb;-><init>()V

    iput-object v0, p0, Lmof;->z:Lmnb;

    .line 5099
    :cond_19
    iget-object v0, p0, Lmof;->z:Lmnb;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 5103
    :sswitch_1d
    iget-object v0, p0, Lmof;->v:Lmoe;

    if-nez v0, :cond_1a

    .line 5104
    new-instance v0, Lmoe;

    invoke-direct {v0}, Lmoe;-><init>()V

    iput-object v0, p0, Lmof;->v:Lmoe;

    .line 5106
    :cond_1a
    iget-object v0, p0, Lmof;->v:Lmoe;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 5110
    :sswitch_1e
    iget-object v0, p0, Lmof;->A:Lmnt;

    if-nez v0, :cond_1b

    .line 5111
    new-instance v0, Lmnt;

    invoke-direct {v0}, Lmnt;-><init>()V

    iput-object v0, p0, Lmof;->A:Lmnt;

    .line 5113
    :cond_1b
    iget-object v0, p0, Lmof;->A:Lmnt;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 5117
    :sswitch_1f
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 5118
    if-eqz v0, :cond_1c

    if-eq v0, v4, :cond_1c

    if-eq v0, v5, :cond_1c

    const/4 v2, 0x4

    if-ne v0, v2, :cond_1d

    .line 5122
    :cond_1c
    iput v0, p0, Lmof;->B:I

    goto/16 :goto_0

    .line 5124
    :cond_1d
    iput v1, p0, Lmof;->B:I

    goto/16 :goto_0

    .line 5129
    :sswitch_20
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lmof;->C:Ljava/lang/Float;

    goto/16 :goto_0

    .line 5133
    :sswitch_21
    iget-object v0, p0, Lmof;->D:Lmnd;

    if-nez v0, :cond_1e

    .line 5134
    new-instance v0, Lmnd;

    invoke-direct {v0}, Lmnd;-><init>()V

    iput-object v0, p0, Lmof;->D:Lmnd;

    .line 5136
    :cond_1e
    iget-object v0, p0, Lmof;->D:Lmnd;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 5140
    :sswitch_22
    iget-object v0, p0, Lmof;->E:Lmoh;

    if-nez v0, :cond_1f

    .line 5141
    new-instance v0, Lmoh;

    invoke-direct {v0}, Lmoh;-><init>()V

    iput-object v0, p0, Lmof;->E:Lmoh;

    .line 5143
    :cond_1f
    iget-object v0, p0, Lmof;->E:Lmoh;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 4887
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1d -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4d -> :sswitch_9
        0x52 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
        0x85 -> :sswitch_10
        0x8d -> :sswitch_11
        0x92 -> :sswitch_12
        0x9a -> :sswitch_13
        0xa0 -> :sswitch_14
        0xad -> :sswitch_15
        0xb0 -> :sswitch_16
        0xba -> :sswitch_17
        0xc5 -> :sswitch_18
        0xc8 -> :sswitch_19
        0xd2 -> :sswitch_1a
        0xd8 -> :sswitch_1b
        0xe2 -> :sswitch_1c
        0xea -> :sswitch_1d
        0xf2 -> :sswitch_1e
        0xf8 -> :sswitch_1f
        0x105 -> :sswitch_20
        0x10a -> :sswitch_21
        0x112 -> :sswitch_22
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/high16 v6, -0x80000000

    .line 4616
    iget-object v1, p0, Lmof;->a:Lmnu;

    if-eqz v1, :cond_0

    .line 4617
    const/4 v1, 0x1

    iget-object v2, p0, Lmof;->a:Lmnu;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 4619
    :cond_0
    iget-object v1, p0, Lmof;->b:Lmoa;

    if-eqz v1, :cond_1

    .line 4620
    const/4 v1, 0x2

    iget-object v2, p0, Lmof;->b:Lmoa;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 4622
    :cond_1
    iget-object v1, p0, Lmof;->c:Ljava/lang/Float;

    if-eqz v1, :cond_2

    .line 4623
    const/4 v1, 0x3

    iget-object v2, p0, Lmof;->c:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IF)V

    .line 4625
    :cond_2
    iget-object v1, p0, Lmof;->d:Lmnv;

    if-eqz v1, :cond_3

    .line 4626
    const/4 v1, 0x4

    iget-object v2, p0, Lmof;->d:Lmnv;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 4628
    :cond_3
    iget-object v1, p0, Lmof;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 4629
    const/4 v1, 0x5

    iget-object v2, p0, Lmof;->e:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 4631
    :cond_4
    iget-object v1, p0, Lmof;->f:Lmnc;

    if-eqz v1, :cond_5

    .line 4632
    const/4 v1, 0x6

    iget-object v2, p0, Lmof;->f:Lmnc;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 4634
    :cond_5
    iget-object v1, p0, Lmof;->g:Lmne;

    if-eqz v1, :cond_6

    .line 4635
    const/4 v1, 0x7

    iget-object v2, p0, Lmof;->g:Lmne;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 4637
    :cond_6
    iget-object v1, p0, Lmof;->h:Lmne;

    if-eqz v1, :cond_7

    .line 4638
    const/16 v1, 0x8

    iget-object v2, p0, Lmof;->h:Lmne;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 4640
    :cond_7
    iget-object v1, p0, Lmof;->i:Ljava/lang/Float;

    if-eqz v1, :cond_8

    .line 4641
    const/16 v1, 0x9

    iget-object v2, p0, Lmof;->i:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IF)V

    .line 4643
    :cond_8
    iget-object v1, p0, Lmof;->j:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 4644
    const/16 v1, 0xa

    iget-object v2, p0, Lmof;->j:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 4646
    :cond_9
    iget-object v1, p0, Lmof;->l:Ljava/lang/Integer;

    if-eqz v1, :cond_a

    .line 4647
    const/16 v1, 0xb

    iget-object v2, p0, Lmof;->l:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 4649
    :cond_a
    iget v1, p0, Lmof;->m:I

    if-eq v1, v6, :cond_b

    .line 4650
    const/16 v1, 0xc

    iget v2, p0, Lmof;->m:I

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 4652
    :cond_b
    iget v1, p0, Lmof;->n:I

    if-eq v1, v6, :cond_c

    .line 4653
    const/16 v1, 0xd

    iget v2, p0, Lmof;->n:I

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 4655
    :cond_c
    iget-object v1, p0, Lmof;->o:Lmnq;

    if-eqz v1, :cond_d

    .line 4656
    const/16 v1, 0xe

    iget-object v2, p0, Lmof;->o:Lmnq;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 4658
    :cond_d
    iget-object v1, p0, Lmof;->p:Lmnu;

    if-eqz v1, :cond_e

    .line 4659
    const/16 v1, 0xf

    iget-object v2, p0, Lmof;->p:Lmnu;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 4661
    :cond_e
    iget-object v1, p0, Lmof;->F:Ljava/lang/Float;

    if-eqz v1, :cond_f

    .line 4662
    const/16 v1, 0x10

    iget-object v2, p0, Lmof;->F:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IF)V

    .line 4664
    :cond_f
    iget-object v1, p0, Lmof;->q:Ljava/lang/Float;

    if-eqz v1, :cond_10

    .line 4665
    const/16 v1, 0x11

    iget-object v2, p0, Lmof;->q:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IF)V

    .line 4667
    :cond_10
    iget-object v1, p0, Lmof;->r:Lmne;

    if-eqz v1, :cond_11

    .line 4668
    const/16 v1, 0x12

    iget-object v2, p0, Lmof;->r:Lmne;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 4670
    :cond_11
    iget-object v1, p0, Lmof;->s:Lmnr;

    if-eqz v1, :cond_12

    .line 4671
    const/16 v1, 0x13

    iget-object v2, p0, Lmof;->s:Lmnr;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 4673
    :cond_12
    iget v1, p0, Lmof;->G:I

    if-eq v1, v6, :cond_13

    .line 4674
    const/16 v1, 0x14

    iget v2, p0, Lmof;->G:I

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 4676
    :cond_13
    iget-object v1, p0, Lmof;->t:Ljava/lang/Float;

    if-eqz v1, :cond_14

    .line 4677
    const/16 v1, 0x15

    iget-object v2, p0, Lmof;->t:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IF)V

    .line 4679
    :cond_14
    iget v1, p0, Lmof;->k:I

    if-eq v1, v6, :cond_15

    .line 4680
    const/16 v1, 0x16

    iget v2, p0, Lmof;->k:I

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 4682
    :cond_15
    iget-object v1, p0, Lmof;->H:Lmnp;

    if-eqz v1, :cond_16

    .line 4683
    const/16 v1, 0x17

    iget-object v2, p0, Lmof;->H:Lmnp;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 4685
    :cond_16
    iget-object v1, p0, Lmof;->u:Ljava/lang/Float;

    if-eqz v1, :cond_17

    .line 4686
    const/16 v1, 0x18

    iget-object v2, p0, Lmof;->u:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IF)V

    .line 4688
    :cond_17
    iget-object v1, p0, Lmof;->w:Ljava/lang/Integer;

    if-eqz v1, :cond_18

    .line 4689
    const/16 v1, 0x19

    iget-object v2, p0, Lmof;->w:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 4691
    :cond_18
    iget-object v1, p0, Lmof;->x:[Lmog;

    if-eqz v1, :cond_1a

    .line 4692
    iget-object v2, p0, Lmof;->x:[Lmog;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1a

    aget-object v4, v2, v1

    .line 4693
    if-eqz v4, :cond_19

    .line 4694
    const/16 v5, 0x1a

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 4692
    :cond_19
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 4698
    :cond_1a
    iget-object v1, p0, Lmof;->y:[I

    if-eqz v1, :cond_1b

    iget-object v1, p0, Lmof;->y:[I

    array-length v1, v1

    if-lez v1, :cond_1b

    .line 4699
    iget-object v1, p0, Lmof;->y:[I

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_1b

    aget v3, v1, v0

    .line 4700
    const/16 v4, 0x1b

    invoke-virtual {p1, v4, v3}, Loxo;->a(II)V

    .line 4699
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 4703
    :cond_1b
    iget-object v0, p0, Lmof;->z:Lmnb;

    if-eqz v0, :cond_1c

    .line 4704
    const/16 v0, 0x1c

    iget-object v1, p0, Lmof;->z:Lmnb;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 4706
    :cond_1c
    iget-object v0, p0, Lmof;->v:Lmoe;

    if-eqz v0, :cond_1d

    .line 4707
    const/16 v0, 0x1d

    iget-object v1, p0, Lmof;->v:Lmoe;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 4709
    :cond_1d
    iget-object v0, p0, Lmof;->A:Lmnt;

    if-eqz v0, :cond_1e

    .line 4710
    const/16 v0, 0x1e

    iget-object v1, p0, Lmof;->A:Lmnt;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 4712
    :cond_1e
    iget v0, p0, Lmof;->B:I

    if-eq v0, v6, :cond_1f

    .line 4713
    const/16 v0, 0x1f

    iget v1, p0, Lmof;->B:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 4715
    :cond_1f
    iget-object v0, p0, Lmof;->C:Ljava/lang/Float;

    if-eqz v0, :cond_20

    .line 4716
    const/16 v0, 0x20

    iget-object v1, p0, Lmof;->C:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 4718
    :cond_20
    iget-object v0, p0, Lmof;->D:Lmnd;

    if-eqz v0, :cond_21

    .line 4719
    const/16 v0, 0x21

    iget-object v1, p0, Lmof;->D:Lmnd;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 4721
    :cond_21
    iget-object v0, p0, Lmof;->E:Lmoh;

    if-eqz v0, :cond_22

    .line 4722
    const/16 v0, 0x22

    iget-object v1, p0, Lmof;->E:Lmoh;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 4724
    :cond_22
    iget-object v0, p0, Lmof;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 4726
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 4382
    invoke-virtual {p0, p1}, Lmof;->a(Loxn;)Lmof;

    move-result-object v0

    return-object v0
.end method
