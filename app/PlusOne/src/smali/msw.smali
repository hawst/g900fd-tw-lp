.class public final Lmsw;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmsy;

.field public b:I

.field private c:Ljava/lang/Integer;

.field private d:Ljava/lang/Integer;

.field private e:[Lmsx;

.field private f:[Lmsy;

.field private g:Lmtf;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1479
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1482
    iput-object v1, p0, Lmsw;->a:Lmsy;

    .line 1489
    sget-object v0, Lmsx;->a:[Lmsx;

    iput-object v0, p0, Lmsw;->e:[Lmsx;

    .line 1492
    sget-object v0, Lmsy;->a:[Lmsy;

    iput-object v0, p0, Lmsw;->f:[Lmsy;

    .line 1495
    iput-object v1, p0, Lmsw;->g:Lmtf;

    .line 1498
    const/high16 v0, -0x80000000

    iput v0, p0, Lmsw;->b:I

    .line 1479
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1538
    .line 1539
    iget-object v0, p0, Lmsw;->a:Lmsy;

    if-eqz v0, :cond_8

    .line 1540
    const/4 v0, 0x1

    iget-object v2, p0, Lmsw;->a:Lmsy;

    .line 1541
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1543
    :goto_0
    iget-object v2, p0, Lmsw;->c:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    .line 1544
    const/4 v2, 0x2

    iget-object v3, p0, Lmsw;->c:Ljava/lang/Integer;

    .line 1545
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 1547
    :cond_0
    iget-object v2, p0, Lmsw;->d:Ljava/lang/Integer;

    if-eqz v2, :cond_1

    .line 1548
    const/4 v2, 0x3

    iget-object v3, p0, Lmsw;->d:Ljava/lang/Integer;

    .line 1549
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 1551
    :cond_1
    iget-object v2, p0, Lmsw;->e:[Lmsx;

    if-eqz v2, :cond_3

    .line 1552
    iget-object v3, p0, Lmsw;->e:[Lmsx;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_3

    aget-object v5, v3, v2

    .line 1553
    if-eqz v5, :cond_2

    .line 1554
    const/4 v6, 0x4

    .line 1555
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 1552
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1559
    :cond_3
    iget-object v2, p0, Lmsw;->f:[Lmsy;

    if-eqz v2, :cond_5

    .line 1560
    iget-object v2, p0, Lmsw;->f:[Lmsy;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    .line 1561
    if-eqz v4, :cond_4

    .line 1562
    const/4 v5, 0x5

    .line 1563
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 1560
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1567
    :cond_5
    iget-object v1, p0, Lmsw;->g:Lmtf;

    if-eqz v1, :cond_6

    .line 1568
    const/4 v1, 0x6

    iget-object v2, p0, Lmsw;->g:Lmtf;

    .line 1569
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1571
    :cond_6
    iget v1, p0, Lmsw;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_7

    .line 1572
    const/4 v1, 0x7

    iget v2, p0, Lmsw;->b:I

    .line 1573
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1575
    :cond_7
    iget-object v1, p0, Lmsw;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1576
    iput v0, p0, Lmsw;->ai:I

    .line 1577
    return v0

    :cond_8
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lmsw;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1585
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1586
    sparse-switch v0, :sswitch_data_0

    .line 1590
    iget-object v2, p0, Lmsw;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 1591
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmsw;->ah:Ljava/util/List;

    .line 1594
    :cond_1
    iget-object v2, p0, Lmsw;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1596
    :sswitch_0
    return-object p0

    .line 1601
    :sswitch_1
    iget-object v0, p0, Lmsw;->a:Lmsy;

    if-nez v0, :cond_2

    .line 1602
    new-instance v0, Lmsy;

    invoke-direct {v0}, Lmsy;-><init>()V

    iput-object v0, p0, Lmsw;->a:Lmsy;

    .line 1604
    :cond_2
    iget-object v0, p0, Lmsw;->a:Lmsy;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1608
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmsw;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 1612
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmsw;->d:Ljava/lang/Integer;

    goto :goto_0

    .line 1616
    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1617
    iget-object v0, p0, Lmsw;->e:[Lmsx;

    if-nez v0, :cond_4

    move v0, v1

    .line 1618
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lmsx;

    .line 1619
    iget-object v3, p0, Lmsw;->e:[Lmsx;

    if-eqz v3, :cond_3

    .line 1620
    iget-object v3, p0, Lmsw;->e:[Lmsx;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1622
    :cond_3
    iput-object v2, p0, Lmsw;->e:[Lmsx;

    .line 1623
    :goto_2
    iget-object v2, p0, Lmsw;->e:[Lmsx;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 1624
    iget-object v2, p0, Lmsw;->e:[Lmsx;

    new-instance v3, Lmsx;

    invoke-direct {v3}, Lmsx;-><init>()V

    aput-object v3, v2, v0

    .line 1625
    iget-object v2, p0, Lmsw;->e:[Lmsx;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1626
    invoke-virtual {p1}, Loxn;->a()I

    .line 1623
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1617
    :cond_4
    iget-object v0, p0, Lmsw;->e:[Lmsx;

    array-length v0, v0

    goto :goto_1

    .line 1629
    :cond_5
    iget-object v2, p0, Lmsw;->e:[Lmsx;

    new-instance v3, Lmsx;

    invoke-direct {v3}, Lmsx;-><init>()V

    aput-object v3, v2, v0

    .line 1630
    iget-object v2, p0, Lmsw;->e:[Lmsx;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1634
    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1635
    iget-object v0, p0, Lmsw;->f:[Lmsy;

    if-nez v0, :cond_7

    move v0, v1

    .line 1636
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lmsy;

    .line 1637
    iget-object v3, p0, Lmsw;->f:[Lmsy;

    if-eqz v3, :cond_6

    .line 1638
    iget-object v3, p0, Lmsw;->f:[Lmsy;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1640
    :cond_6
    iput-object v2, p0, Lmsw;->f:[Lmsy;

    .line 1641
    :goto_4
    iget-object v2, p0, Lmsw;->f:[Lmsy;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_8

    .line 1642
    iget-object v2, p0, Lmsw;->f:[Lmsy;

    new-instance v3, Lmsy;

    invoke-direct {v3}, Lmsy;-><init>()V

    aput-object v3, v2, v0

    .line 1643
    iget-object v2, p0, Lmsw;->f:[Lmsy;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1644
    invoke-virtual {p1}, Loxn;->a()I

    .line 1641
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 1635
    :cond_7
    iget-object v0, p0, Lmsw;->f:[Lmsy;

    array-length v0, v0

    goto :goto_3

    .line 1647
    :cond_8
    iget-object v2, p0, Lmsw;->f:[Lmsy;

    new-instance v3, Lmsy;

    invoke-direct {v3}, Lmsy;-><init>()V

    aput-object v3, v2, v0

    .line 1648
    iget-object v2, p0, Lmsw;->f:[Lmsy;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1652
    :sswitch_6
    iget-object v0, p0, Lmsw;->g:Lmtf;

    if-nez v0, :cond_9

    .line 1653
    new-instance v0, Lmtf;

    invoke-direct {v0}, Lmtf;-><init>()V

    iput-object v0, p0, Lmsw;->g:Lmtf;

    .line 1655
    :cond_9
    iget-object v0, p0, Lmsw;->g:Lmtf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1659
    :sswitch_7
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1660
    if-eqz v0, :cond_a

    const/4 v2, 0x1

    if-eq v0, v2, :cond_a

    const/4 v2, 0x2

    if-eq v0, v2, :cond_a

    const/4 v2, 0x3

    if-eq v0, v2, :cond_a

    const/4 v2, 0x4

    if-eq v0, v2, :cond_a

    const/4 v2, 0x5

    if-eq v0, v2, :cond_a

    const/4 v2, 0x6

    if-eq v0, v2, :cond_a

    const/4 v2, 0x7

    if-eq v0, v2, :cond_a

    const/16 v2, 0x8

    if-eq v0, v2, :cond_a

    const/16 v2, 0x9

    if-ne v0, v2, :cond_b

    .line 1670
    :cond_a
    iput v0, p0, Lmsw;->b:I

    goto/16 :goto_0

    .line 1672
    :cond_b
    iput v1, p0, Lmsw;->b:I

    goto/16 :goto_0

    .line 1586
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1503
    iget-object v1, p0, Lmsw;->a:Lmsy;

    if-eqz v1, :cond_0

    .line 1504
    const/4 v1, 0x1

    iget-object v2, p0, Lmsw;->a:Lmsy;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 1506
    :cond_0
    iget-object v1, p0, Lmsw;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 1507
    const/4 v1, 0x2

    iget-object v2, p0, Lmsw;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 1509
    :cond_1
    iget-object v1, p0, Lmsw;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 1510
    const/4 v1, 0x3

    iget-object v2, p0, Lmsw;->d:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 1512
    :cond_2
    iget-object v1, p0, Lmsw;->e:[Lmsx;

    if-eqz v1, :cond_4

    .line 1513
    iget-object v2, p0, Lmsw;->e:[Lmsx;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 1514
    if-eqz v4, :cond_3

    .line 1515
    const/4 v5, 0x4

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 1513
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1519
    :cond_4
    iget-object v1, p0, Lmsw;->f:[Lmsy;

    if-eqz v1, :cond_6

    .line 1520
    iget-object v1, p0, Lmsw;->f:[Lmsy;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_6

    aget-object v3, v1, v0

    .line 1521
    if-eqz v3, :cond_5

    .line 1522
    const/4 v4, 0x5

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 1520
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1526
    :cond_6
    iget-object v0, p0, Lmsw;->g:Lmtf;

    if-eqz v0, :cond_7

    .line 1527
    const/4 v0, 0x6

    iget-object v1, p0, Lmsw;->g:Lmtf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1529
    :cond_7
    iget v0, p0, Lmsw;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_8

    .line 1530
    const/4 v0, 0x7

    iget v1, p0, Lmsw;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1532
    :cond_8
    iget-object v0, p0, Lmsw;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1534
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1475
    invoke-virtual {p0, p1}, Lmsw;->a(Loxn;)Lmsw;

    move-result-object v0

    return-object v0
.end method
