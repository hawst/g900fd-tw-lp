.class public final Lmcp;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnlo;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1618
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1621
    iput-object v0, p0, Lmcp;->apiHeader:Llyr;

    .line 1624
    iput-object v0, p0, Lmcp;->a:Lnlo;

    .line 1618
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1641
    const/4 v0, 0x0

    .line 1642
    iget-object v1, p0, Lmcp;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 1643
    const/4 v0, 0x1

    iget-object v1, p0, Lmcp;->apiHeader:Llyr;

    .line 1644
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1646
    :cond_0
    iget-object v1, p0, Lmcp;->a:Lnlo;

    if-eqz v1, :cond_1

    .line 1647
    const/4 v1, 0x2

    iget-object v2, p0, Lmcp;->a:Lnlo;

    .line 1648
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1650
    :cond_1
    iget-object v1, p0, Lmcp;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1651
    iput v0, p0, Lmcp;->ai:I

    .line 1652
    return v0
.end method

.method public a(Loxn;)Lmcp;
    .locals 2

    .prologue
    .line 1660
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1661
    sparse-switch v0, :sswitch_data_0

    .line 1665
    iget-object v1, p0, Lmcp;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1666
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmcp;->ah:Ljava/util/List;

    .line 1669
    :cond_1
    iget-object v1, p0, Lmcp;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1671
    :sswitch_0
    return-object p0

    .line 1676
    :sswitch_1
    iget-object v0, p0, Lmcp;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 1677
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmcp;->apiHeader:Llyr;

    .line 1679
    :cond_2
    iget-object v0, p0, Lmcp;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1683
    :sswitch_2
    iget-object v0, p0, Lmcp;->a:Lnlo;

    if-nez v0, :cond_3

    .line 1684
    new-instance v0, Lnlo;

    invoke-direct {v0}, Lnlo;-><init>()V

    iput-object v0, p0, Lmcp;->a:Lnlo;

    .line 1686
    :cond_3
    iget-object v0, p0, Lmcp;->a:Lnlo;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1661
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1629
    iget-object v0, p0, Lmcp;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 1630
    const/4 v0, 0x1

    iget-object v1, p0, Lmcp;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1632
    :cond_0
    iget-object v0, p0, Lmcp;->a:Lnlo;

    if-eqz v0, :cond_1

    .line 1633
    const/4 v0, 0x2

    iget-object v1, p0, Lmcp;->a:Lnlo;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1635
    :cond_1
    iget-object v0, p0, Lmcp;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1637
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1614
    invoke-virtual {p0, p1}, Lmcp;->a(Loxn;)Lmcp;

    move-result-object v0

    return-object v0
.end method
