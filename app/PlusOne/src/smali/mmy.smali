.class public final Lmmy;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Float;

.field public b:Ljava/lang/Float;

.field public c:Ljava/lang/Float;

.field public d:Ljava/lang/Float;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6718
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 6749
    const/4 v0, 0x0

    .line 6750
    iget-object v1, p0, Lmmy;->a:Ljava/lang/Float;

    if-eqz v1, :cond_0

    .line 6751
    const/4 v0, 0x5

    iget-object v1, p0, Lmmy;->a:Ljava/lang/Float;

    .line 6752
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x0

    .line 6754
    :cond_0
    iget-object v1, p0, Lmmy;->b:Ljava/lang/Float;

    if-eqz v1, :cond_1

    .line 6755
    const/4 v1, 0x6

    iget-object v2, p0, Lmmy;->b:Ljava/lang/Float;

    .line 6756
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 6758
    :cond_1
    iget-object v1, p0, Lmmy;->c:Ljava/lang/Float;

    if-eqz v1, :cond_2

    .line 6759
    const/4 v1, 0x7

    iget-object v2, p0, Lmmy;->c:Ljava/lang/Float;

    .line 6760
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 6762
    :cond_2
    iget-object v1, p0, Lmmy;->d:Ljava/lang/Float;

    if-eqz v1, :cond_3

    .line 6763
    const/16 v1, 0x8

    iget-object v2, p0, Lmmy;->d:Ljava/lang/Float;

    .line 6764
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 6766
    :cond_3
    iget-object v1, p0, Lmmy;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6767
    iput v0, p0, Lmmy;->ai:I

    .line 6768
    return v0
.end method

.method public a(Loxn;)Lmmy;
    .locals 2

    .prologue
    .line 6776
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 6777
    sparse-switch v0, :sswitch_data_0

    .line 6781
    iget-object v1, p0, Lmmy;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 6782
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmmy;->ah:Ljava/util/List;

    .line 6785
    :cond_1
    iget-object v1, p0, Lmmy;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6787
    :sswitch_0
    return-object p0

    .line 6792
    :sswitch_1
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lmmy;->a:Ljava/lang/Float;

    goto :goto_0

    .line 6796
    :sswitch_2
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lmmy;->b:Ljava/lang/Float;

    goto :goto_0

    .line 6800
    :sswitch_3
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lmmy;->c:Ljava/lang/Float;

    goto :goto_0

    .line 6804
    :sswitch_4
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lmmy;->d:Ljava/lang/Float;

    goto :goto_0

    .line 6777
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x2d -> :sswitch_1
        0x35 -> :sswitch_2
        0x3d -> :sswitch_3
        0x45 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 6731
    iget-object v0, p0, Lmmy;->a:Ljava/lang/Float;

    if-eqz v0, :cond_0

    .line 6732
    const/4 v0, 0x5

    iget-object v1, p0, Lmmy;->a:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 6734
    :cond_0
    iget-object v0, p0, Lmmy;->b:Ljava/lang/Float;

    if-eqz v0, :cond_1

    .line 6735
    const/4 v0, 0x6

    iget-object v1, p0, Lmmy;->b:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 6737
    :cond_1
    iget-object v0, p0, Lmmy;->c:Ljava/lang/Float;

    if-eqz v0, :cond_2

    .line 6738
    const/4 v0, 0x7

    iget-object v1, p0, Lmmy;->c:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 6740
    :cond_2
    iget-object v0, p0, Lmmy;->d:Ljava/lang/Float;

    if-eqz v0, :cond_3

    .line 6741
    const/16 v0, 0x8

    iget-object v1, p0, Lmmy;->d:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 6743
    :cond_3
    iget-object v0, p0, Lmmy;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 6745
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6714
    invoke-virtual {p0, p1}, Lmmy;->a(Loxn;)Lmmy;

    move-result-object v0

    return-object v0
.end method
