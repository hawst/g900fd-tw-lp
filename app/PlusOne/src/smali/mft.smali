.class public final Lmft;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lmyr;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3238
    invoke-direct {p0}, Loxq;-><init>()V

    .line 3241
    iput-object v0, p0, Lmft;->apiHeader:Llyr;

    .line 3244
    iput-object v0, p0, Lmft;->a:Lmyr;

    .line 3238
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 3261
    const/4 v0, 0x0

    .line 3262
    iget-object v1, p0, Lmft;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 3263
    const/4 v0, 0x1

    iget-object v1, p0, Lmft;->apiHeader:Llyr;

    .line 3264
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3266
    :cond_0
    iget-object v1, p0, Lmft;->a:Lmyr;

    if-eqz v1, :cond_1

    .line 3267
    const/4 v1, 0x2

    iget-object v2, p0, Lmft;->a:Lmyr;

    .line 3268
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3270
    :cond_1
    iget-object v1, p0, Lmft;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3271
    iput v0, p0, Lmft;->ai:I

    .line 3272
    return v0
.end method

.method public a(Loxn;)Lmft;
    .locals 2

    .prologue
    .line 3280
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3281
    sparse-switch v0, :sswitch_data_0

    .line 3285
    iget-object v1, p0, Lmft;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 3286
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmft;->ah:Ljava/util/List;

    .line 3289
    :cond_1
    iget-object v1, p0, Lmft;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3291
    :sswitch_0
    return-object p0

    .line 3296
    :sswitch_1
    iget-object v0, p0, Lmft;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 3297
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmft;->apiHeader:Llyr;

    .line 3299
    :cond_2
    iget-object v0, p0, Lmft;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3303
    :sswitch_2
    iget-object v0, p0, Lmft;->a:Lmyr;

    if-nez v0, :cond_3

    .line 3304
    new-instance v0, Lmyr;

    invoke-direct {v0}, Lmyr;-><init>()V

    iput-object v0, p0, Lmft;->a:Lmyr;

    .line 3306
    :cond_3
    iget-object v0, p0, Lmft;->a:Lmyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3281
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 3249
    iget-object v0, p0, Lmft;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 3250
    const/4 v0, 0x1

    iget-object v1, p0, Lmft;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 3252
    :cond_0
    iget-object v0, p0, Lmft;->a:Lmyr;

    if-eqz v0, :cond_1

    .line 3253
    const/4 v0, 0x2

    iget-object v1, p0, Lmft;->a:Lmyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 3255
    :cond_1
    iget-object v0, p0, Lmft;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3257
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3234
    invoke-virtual {p0, p1}, Lmft;->a(Loxn;)Lmft;

    move-result-object v0

    return-object v0
.end method
