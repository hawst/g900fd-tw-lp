.class public final Lkdw;
.super Lkdh;
.source "PG"

# interfaces
.implements Lkdv;


# static fields
.field private static final a:[Ljava/util/regex/Pattern;


# instance fields
.field private b:Lkeb;

.field private final c:Lkeu;

.field private final d:I

.field private final e:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lkdc;",
            "Lkda;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/google/android/libraries/social/resources/images/ImageResource;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lgl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lgl",
            "<",
            "Lkdc;",
            "Lcom/google/android/libraries/social/resources/images/ImageResource;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lkeh;

.field private i:J

.field private final j:Lcom/google/android/libraries/social/filecache/FileCache;

.field private final k:Lcom/google/android/libraries/social/filecache/FileCache;

.field private final l:I

.field private final m:[Lkdq;

.field private n:F

.field private o:I

.field private p:I

.field private q:I

.field private r:I

.field private final s:Ljhc;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 69
    sget-object v0, Llso;->b:Llso;

    const-wide/16 v2, 0x5

    invoke-virtual {v0, v2, v3}, Llso;->a(J)J

    .line 72
    sget-object v0, Llso;->b:Llso;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v2, v3}, Llso;->a(J)J

    .line 75
    sget-object v0, Llso;->b:Llso;

    const-wide/16 v2, 0x200

    invoke-virtual {v0, v2, v3}, Llso;->a(J)J

    .line 91
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/util/regex/Pattern;

    const/4 v1, 0x0

    const-string v2, ".+?com.*/(.+)?/.+\\..{3,4}+"

    .line 93
    invoke-static {v2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, ".+?com/.[^\\?]+?=(.+)?"

    .line 95
    invoke-static {v2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v2

    aput-object v2, v0, v1

    sput-object v0, Lkdw;->a:[Ljava/util/regex/Pattern;

    .line 91
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lkes;)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 145
    invoke-direct {p0, p1}, Lkdh;-><init>(Landroid/content/Context;)V

    .line 102
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lkdw;->e:Ljava/util/HashMap;

    .line 104
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lkdw;->f:Ljava/util/HashSet;

    .line 146
    invoke-virtual {p0}, Lkdw;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Llsg;->a(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lkdw;->l:I

    .line 148
    const-class v0, Ljhc;

    invoke-static {p1, v0}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljhc;

    iput-object v0, p0, Lkdw;->s:Ljhc;

    .line 150
    invoke-virtual {p2}, Lkes;->b()Lkeq;

    move-result-object v0

    .line 151
    new-instance v1, Lcom/google/android/libraries/social/filecache/FileCache;

    iget-object v3, v0, Lkeq;->a:Ljava/lang/String;

    iget-wide v4, v0, Lkeq;->b:J

    iget-wide v6, v0, Lkeq;->c:J

    iget v8, v0, Lkeq;->d:F

    move-object v2, p1

    invoke-direct/range {v1 .. v9}, Lcom/google/android/libraries/social/filecache/FileCache;-><init>(Landroid/content/Context;Ljava/lang/String;JJFF)V

    iput-object v1, p0, Lkdw;->j:Lcom/google/android/libraries/social/filecache/FileCache;

    .line 155
    invoke-virtual {p2}, Lkes;->c()Lkeq;

    move-result-object v0

    .line 156
    new-instance v1, Lcom/google/android/libraries/social/filecache/FileCache;

    iget-object v3, v0, Lkeq;->a:Ljava/lang/String;

    iget-wide v4, v0, Lkeq;->b:J

    iget-wide v6, v0, Lkeq;->c:J

    iget v8, v0, Lkeq;->d:F

    move-object v2, p1

    invoke-direct/range {v1 .. v9}, Lcom/google/android/libraries/social/filecache/FileCache;-><init>(Landroid/content/Context;Ljava/lang/String;JJFF)V

    iput-object v1, p0, Lkdw;->k:Lcom/google/android/libraries/social/filecache/FileCache;

    .line 160
    invoke-virtual {p2}, Lkes;->a()Lkeu;

    move-result-object v0

    iput-object v0, p0, Lkdw;->c:Lkeu;

    .line 162
    iget-object v0, p0, Lkdw;->c:Lkeu;

    iget v0, v0, Lkeu;->a:I

    int-to-double v0, v0

    const-wide/high16 v2, 0x3fe8000000000000L    # 0.75

    mul-double/2addr v0, v2

    double-to-int v0, v0

    iput v0, p0, Lkdw;->d:I

    .line 164
    new-instance v0, Lkdy;

    iget-object v1, p0, Lkdw;->c:Lkeu;

    iget v1, v1, Lkeu;->a:I

    invoke-direct {v0, v1}, Lkdy;-><init>(I)V

    iput-object v0, p0, Lkdw;->g:Lgl;

    .line 166
    invoke-static {p1}, Llsc;->a(Landroid/content/Context;)Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 167
    const/high16 v1, 0x43700000    # 240.0f

    iget v2, v0, Landroid/util/DisplayMetrics;->xdpi:F

    div-float/2addr v1, v2

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    iput v1, p0, Lkdw;->n:F

    .line 168
    iget v1, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lkdw;->o:I

    .line 169
    iget v0, p0, Lkdw;->o:I

    if-nez v0, :cond_0

    .line 170
    const/16 v0, 0x280

    iput v0, p0, Lkdw;->o:I

    .line 173
    :cond_0
    iget v0, p0, Lkdw;->o:I

    int-to-float v0, v0

    const v1, 0x3e4ccccd    # 0.2f

    mul-float/2addr v0, v1

    iget v1, p0, Lkdw;->n:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lkdw;->p:I

    .line 175
    iget v0, p0, Lkdw;->o:I

    int-to-float v0, v0

    const/high16 v1, 0x3f000000    # 0.5f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lkdw;->q:I

    .line 177
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_4

    const/4 v0, 0x0

    :cond_1
    :goto_0
    iput-object v0, p0, Lkdw;->h:Lkeh;

    .line 178
    const-class v0, Lkdq;

    invoke-static {p1, v0}, Llnh;->c(Landroid/content/Context;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    .line 179
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lkdq;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lkdq;

    iput-object v0, p0, Lkdw;->m:[Lkdq;

    .line 180
    iget-object v0, p0, Lkdw;->m:[Lkdq;

    new-instance v1, Lkdx;

    invoke-direct {v1}, Lkdx;-><init>()V

    invoke-static {v0, v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 187
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 188
    const v1, 0x7f0c0001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 189
    iget v1, p0, Lkdw;->l:I

    const/16 v2, 0x100

    if-lt v1, v2, :cond_2

    const/high16 v0, 0x1000000

    :cond_2
    iput v0, p0, Lkdw;->r:I

    .line 191
    const-string v0, "ImageResourceManager"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 192
    invoke-virtual {p0}, Lkdw;->q()V

    .line 194
    :cond_3
    return-void

    .line 177
    :cond_4
    new-instance v0, Lkeh;

    iget-object v1, p0, Lkdw;->c:Lkeu;

    iget v1, v1, Lkeu;->b:I

    invoke-direct {v0, v1}, Lkeh;-><init>(I)V

    iget-object v1, p0, Lkdw;->c:Lkeu;

    iget-boolean v1, v1, Lkeu;->e:Z

    if-eqz v1, :cond_1

    new-instance v1, Lkeb;

    iget-object v2, p0, Lkdw;->c:Lkeu;

    invoke-virtual {p0}, Lkdw;->h()I

    move-result v3

    invoke-virtual {p0}, Lkdw;->g()I

    move-result v4

    invoke-direct {v1, v2, v0, v3, v4}, Lkeb;-><init>(Lkeu;Lkeh;II)V

    iput-object v1, p0, Lkdw;->b:Lkeb;

    new-instance v1, Lkdz;

    invoke-direct {v1, p0}, Lkdz;-><init>(Lkdw;)V

    invoke-static {v1}, Llsx;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method static synthetic a(Lkdw;)Lkeb;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lkdw;->b:Lkeb;

    return-object v0
.end method


# virtual methods
.method public a(II)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 453
    iget-object v0, p0, Lkdw;->h:Lkeh;

    if-eqz v0, :cond_1

    .line 454
    iget-object v0, p0, Lkdw;->h:Lkeh;

    invoke-virtual {v0, p1, p2}, Lkeh;->b(II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 455
    if-nez v0, :cond_0

    .line 456
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 458
    :cond_0
    new-instance v1, Lkdo;

    invoke-direct {v1, v0, p1, p2}, Lkdo;-><init>(Landroid/graphics/Bitmap;II)V

    move-object v0, v1

    .line 461
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Lcom/google/android/libraries/social/resources/images/ImageResource;Ljava/nio/ByteBuffer;Z)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 198
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lkdw;->m:[Lkdq;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 199
    iget-object v1, p0, Lkdw;->m:[Lkdq;

    aget-object v1, v1, v0

    invoke-interface {v1, p1, p2, p3}, Lkdq;->a(Lcom/google/android/libraries/social/resources/images/ImageResource;Ljava/nio/ByteBuffer;Z)Ljava/lang/Object;

    move-result-object v1

    .line 200
    if-eqz v1, :cond_0

    move-object v0, v1

    .line 204
    :goto_1
    return-object v0

    .line 198
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 204
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a(Lkdc;)Lkda;
    .locals 1

    .prologue
    .line 306
    iget-object v0, p0, Lkdw;->e:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/resources/images/ImageResource;

    .line 307
    if-eqz v0, :cond_0

    .line 311
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lkdw;->g:Lgl;

    invoke-virtual {v0, p1}, Lgl;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkda;

    goto :goto_0
.end method

.method public a(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 488
    if-nez p1, :cond_0

    .line 502
    :goto_0
    return-void

    .line 492
    :cond_0
    iget-object v0, p0, Lkdw;->h:Lkeh;

    if-eqz v0, :cond_2

    .line 495
    iget-object v0, p0, Lkdw;->b:Lkeb;

    if-eqz v0, :cond_1

    .line 496
    iget-object v0, p0, Lkdw;->b:Lkeb;

    invoke-virtual {v0}, Lkeb;->a()V

    .line 498
    :cond_1
    iget-object v0, p0, Lkdw;->h:Lkeh;

    invoke-virtual {v0, p1}, Lkeh;->a(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 500
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0
.end method

.method public a(Lcom/google/android/libraries/social/resources/images/ImageResource;)V
    .locals 2

    .prologue
    .line 522
    iget-object v1, p0, Lkdw;->f:Ljava/util/HashSet;

    monitor-enter v1

    .line 523
    :try_start_0
    iget-object v0, p0, Lkdw;->f:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 524
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJJILjava/lang/String;)V
    .locals 18

    .prologue
    .line 701
    move-object/from16 v0, p0

    iget-object v2, v0, Lkdw;->s:Ljhc;

    if-eqz v2, :cond_0

    .line 702
    move-object/from16 v0, p0

    iget-object v2, v0, Lkdw;->s:Ljhc;

    const/4 v3, 0x1

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v7, 0x0

    .line 703
    const-string v6, ""

    const/4 v5, 0x0

    :goto_0
    sget-object v8, Lkdw;->a:[Ljava/util/regex/Pattern;

    const/4 v8, 0x2

    if-ge v5, v8, :cond_5

    sget-object v8, Lkdw;->a:[Ljava/util/regex/Pattern;

    aget-object v8, v8, v5

    move-object/from16 v0, p2

    invoke-virtual {v8, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/regex/Matcher;->matches()Z

    move-result v9

    if-eqz v9, :cond_1

    const/4 v5, 0x1

    invoke-virtual {v8, v5}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    :goto_1
    const-string v6, ";"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    if-lez v6, :cond_3

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    const/4 v5, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v8, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    :goto_2
    aput-object v5, v4, v7

    const-wide/16 v12, 0x0

    move-object/from16 v5, p1

    move-wide/from16 v6, p4

    move-wide/from16 v8, p6

    move-wide/from16 v10, p8

    move-wide/from16 v14, p10

    move/from16 v16, p12

    move-object/from16 v17, p13

    .line 702
    invoke-interface/range {v2 .. v17}, Ljhc;->a(I[Ljava/lang/String;Ljava/lang/String;JJJJJILjava/lang/String;)V

    .line 706
    :cond_0
    return-void

    .line 703
    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_2
    new-instance v5, Ljava/lang/String;

    invoke-direct {v5, v8}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    :cond_3
    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static/range {p3 .. p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v8

    if-eqz v8, :cond_4

    invoke-virtual {v6, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto :goto_2

    :cond_4
    new-instance v5, Ljava/lang/String;

    invoke-direct {v5, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    :cond_5
    move-object v5, v6

    goto :goto_1
.end method

.method public a(Lkda;)V
    .locals 4

    .prologue
    .line 356
    iget-object v0, p0, Lkdw;->e:Ljava/util/HashMap;

    invoke-virtual {p1}, Lkda;->getIdentifier()Lkdc;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 357
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Lkda;->getIdentifier()Lkdc;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x18

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Resource is not active: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move-object v0, p1

    .line 360
    check-cast v0, Lcom/google/android/libraries/social/resources/images/ImageResource;

    .line 361
    invoke-virtual {v0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getStatus()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 390
    :pswitch_0
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Illegal resource state: "

    .line 391
    invoke-virtual {p1}, Lkda;->getStatusAsString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 366
    :pswitch_1
    invoke-virtual {v0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->unpack()V

    .line 387
    :goto_1
    :pswitch_2
    return-void

    .line 372
    :pswitch_3
    invoke-virtual {v0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->isDebugLogEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 374
    invoke-virtual {v0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getIdentifier()Lkdc;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x17

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Requesting image load: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 373
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/resources/images/ImageResource;->logDebug(Ljava/lang/String;)V

    .line 377
    :cond_1
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/resources/images/ImageResource;->setStatus(I)V

    .line 378
    invoke-virtual {p0, p1}, Lkdw;->e(Lkda;)V

    goto :goto_1

    .line 391
    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 361
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method

.method public a(Lkda;Lkdd;)V
    .locals 4

    .prologue
    .line 316
    invoke-static {}, Llsx;->b()V

    .line 318
    invoke-virtual {p1}, Lkda;->getIdentifier()Lkdc;

    move-result-object v1

    .line 319
    iget-object v0, p0, Lkdw;->e:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkda;

    .line 320
    if-eqz v0, :cond_2

    .line 321
    if-eq v0, p1, :cond_0

    .line 322
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x50

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Duplicate resource: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". Check getManagedResource() prior to calling loadResource. "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 325
    :cond_0
    invoke-virtual {p1}, Lkda;->isDebugLogEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 326
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x19

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Adding another consumer: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lkda;->logDebug(Ljava/lang/String;)V

    .line 328
    :cond_1
    invoke-virtual {p1, p2}, Lkda;->register(Lkdd;)V

    .line 352
    :goto_0
    return-void

    .line 332
    :cond_2
    iget-object v0, p0, Lkdw;->g:Lgl;

    invoke-virtual {v0, v1}, Lgl;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkda;

    .line 333
    if-eqz v0, :cond_5

    .line 334
    if-eq v0, p1, :cond_3

    .line 335
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x50

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Duplicate resource: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". Check getManagedResource() prior to calling loadResource. "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 338
    :cond_3
    invoke-virtual {p1}, Lkda;->isDebugLogEnabled()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 339
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xc

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Activating: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lkda;->logDebug(Ljava/lang/String;)V

    .line 341
    :cond_4
    iget-object v0, p0, Lkdw;->g:Lgl;

    invoke-virtual {v0, v1}, Lgl;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 342
    iget-object v0, p0, Lkdw;->e:Ljava/util/HashMap;

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 343
    invoke-virtual {p1, p2}, Lkda;->register(Lkdd;)V

    goto :goto_0

    .line 347
    :cond_5
    iget-object v0, p0, Lkdw;->e:Ljava/util/HashMap;

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 348
    invoke-virtual {p1}, Lkda;->isDebugLogEnabled()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 349
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xe

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "loadResource: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lkda;->logDebug(Ljava/lang/String;)V

    .line 351
    :cond_6
    invoke-virtual {p1, p2}, Lkda;->register(Lkdd;)V

    goto/16 :goto_0
.end method

.method public a(Llox;)V
    .locals 10

    .prologue
    .line 588
    iget-object v0, p0, Lkdw;->g:Lgl;

    invoke-virtual {v0}, Lgl;->h()Ljava/util/Map;

    move-result-object v2

    .line 589
    iget-object v0, p0, Lkdw;->c:Lkeu;

    iget v0, v0, Lkeu;->a:I

    iget-object v1, p0, Lkdw;->g:Lgl;

    .line 590
    invoke-virtual {v1}, Lgl;->c()I

    move-result v1

    .line 591
    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v3

    iget-object v4, p0, Lkdw;->g:Lgl;

    .line 592
    invoke-virtual {v4}, Lgl;->f()I

    move-result v4

    iget-object v5, p0, Lkdw;->g:Lgl;

    .line 593
    invoke-virtual {v5}, Lgl;->d()I

    move-result v5

    iget-object v6, p0, Lkdw;->g:Lgl;

    .line 594
    invoke-virtual {v6}, Lgl;->e()I

    move-result v6

    iget-object v7, p0, Lkdw;->g:Lgl;

    .line 595
    invoke-virtual {v7}, Lgl;->g()I

    move-result v7

    new-instance v8, Ljava/lang/StringBuilder;

    const/16 v9, 0xba

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v9, "Image cache size: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v8, "; cached size: "

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; cached bitmaps: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; put count: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; hit count: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; miss count: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; eviction count: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 589
    invoke-virtual {p1, v0}, Llox;->println(Ljava/lang/String;)V

    .line 597
    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 598
    const-string v0, "Image cache is empty"

    invoke-virtual {p1, v0}, Llox;->println(Ljava/lang/String;)V

    .line 605
    :cond_0
    iget-object v0, p0, Lkdw;->h:Lkeh;

    if-eqz v0, :cond_2

    .line 606
    iget-object v0, p0, Lkdw;->h:Lkeh;

    invoke-virtual {v0}, Lkeh;->a()V

    .line 611
    :goto_0
    iget-object v1, p0, Lkdw;->f:Ljava/util/HashSet;

    monitor-enter v1

    .line 612
    :try_start_0
    iget-object v0, p0, Lkdw;->f:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 613
    iget-object v0, p0, Lkdw;->f:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/resources/images/ImageResource;

    .line 614
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0xd

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Downloading: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Llox;->println(Ljava/lang/String;)V

    goto :goto_1

    .line 617
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 600
    :cond_1
    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkdc;

    .line 601
    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/libraries/social/resources/images/ImageResource;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getSizeInBytes()I

    move-result v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x1b

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Cached: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " bytes, "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Llox;->println(Ljava/lang/String;)V

    goto :goto_2

    .line 608
    :cond_2
    const-string v0, "Bitmap pool uninitalized"

    invoke-virtual {p1, v0}, Llox;->println(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 617
    :cond_3
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 619
    iget-object v0, p0, Lkdw;->e:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 620
    const-string v0, "No active resources"

    invoke-virtual {p1, v0}, Llox;->println(Ljava/lang/String;)V

    .line 628
    :cond_4
    invoke-static {}, Llsx;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 630
    new-instance v0, Lkea;

    invoke-direct {v0, p0}, Lkea;-><init>(Lkdw;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    .line 638
    invoke-virtual {v0, v1}, Lkea;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 642
    :goto_3
    return-void

    .line 622
    :cond_5
    const-string v0, "Active resources"

    invoke-virtual {p1, v0}, Llox;->println(Ljava/lang/String;)V

    .line 623
    iget-object v0, p0, Lkdw;->e:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkda;

    .line 624
    const/4 v2, 0x4

    const-string v3, "ImageResourceManager"

    invoke-virtual {v0}, Lkda;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v3, v0}, Llse;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 640
    :cond_6
    invoke-virtual {p0, p1}, Lkdw;->b(Llox;)V

    goto :goto_3
.end method

.method public aM_()I
    .locals 1

    .prologue
    .line 139
    iget v0, p0, Lkdw;->r:I

    return v0
.end method

.method public b(II)Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 470
    const/4 v0, 0x0

    .line 471
    iget-object v1, p0, Lkdw;->h:Lkeh;

    if-eqz v1, :cond_0

    .line 472
    iget-object v0, p0, Lkdw;->h:Lkeh;

    invoke-virtual {v0, p1, p2}, Lkeh;->a(II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 475
    :cond_0
    if-nez v0, :cond_1

    .line 476
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 479
    :cond_1
    return-object v0
.end method

.method public b(Lcom/google/android/libraries/social/resources/images/ImageResource;)V
    .locals 2

    .prologue
    .line 529
    iget-object v1, p0, Lkdw;->f:Ljava/util/HashSet;

    monitor-enter v1

    .line 530
    :try_start_0
    iget-object v0, p0, Lkdw;->f:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 531
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b(Lkda;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 397
    check-cast p1, Lcom/google/android/libraries/social/resources/images/ImageResource;

    .line 398
    invoke-virtual {p1}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getIdentifier()Lkdc;

    move-result-object v0

    check-cast v0, Lkds;

    .line 399
    invoke-virtual {p1}, Lcom/google/android/libraries/social/resources/images/ImageResource;->isDebugLogEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 400
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1d

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Deactivating image resource: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/google/android/libraries/social/resources/images/ImageResource;->logDebug(Ljava/lang/String;)V

    .line 403
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getStatus()I

    move-result v1

    .line 404
    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 405
    const/4 v1, 0x7

    invoke-virtual {p1, v1}, Lcom/google/android/libraries/social/resources/images/ImageResource;->setStatus(I)V

    .line 406
    invoke-virtual {p1}, Lcom/google/android/libraries/social/resources/images/ImageResource;->cancelDownload()V

    .line 409
    :cond_1
    iget-object v1, p0, Lkdw;->e:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 411
    invoke-virtual {p0, p1}, Lkdw;->b(Lcom/google/android/libraries/social/resources/images/ImageResource;)V

    .line 413
    invoke-virtual {p1}, Lcom/google/android/libraries/social/resources/images/ImageResource;->isCacheEnabled()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 414
    invoke-virtual {p1}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getSizeInBytes()I

    move-result v1

    iget v2, p0, Lkdw;->d:I

    if-ge v1, v2, :cond_4

    .line 416
    invoke-virtual {p1}, Lcom/google/android/libraries/social/resources/images/ImageResource;->pack()V

    .line 417
    iget-wide v2, p0, Lkdw;->i:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_2

    iget-wide v2, p0, Lkdw;->i:J

    .line 418
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-gez v1, :cond_3

    .line 419
    :cond_2
    iput-wide v6, p0, Lkdw;->i:J

    .line 420
    iget-object v1, p0, Lkdw;->g:Lgl;

    invoke-virtual {v1, v0, p1}, Lgl;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 425
    :cond_3
    :goto_0
    return-void

    .line 423
    :cond_4
    invoke-virtual {p1}, Lcom/google/android/libraries/social/resources/images/ImageResource;->recycle()V

    goto :goto_0
.end method

.method b(Llox;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 645
    iget-object v0, p0, Lkdw;->j:Lcom/google/android/libraries/social/filecache/FileCache;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/filecache/FileCache;->getCapacity()J

    move-result-wide v0

    .line 646
    iget-object v2, p0, Lkdw;->j:Lcom/google/android/libraries/social/filecache/FileCache;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/filecache/FileCache;->getUsedSpace()J

    move-result-wide v2

    .line 647
    sub-long v4, v0, v2

    invoke-static {v6, v7, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    .line 648
    invoke-static {v0, v1}, Llsu;->a(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 649
    invoke-static {v2, v3}, Llsu;->a(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 650
    invoke-static {v4, v5}, Llsu;->a(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x27

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Disk cache total size: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "; used: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; free: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 648
    invoke-virtual {p1, v0}, Llox;->println(Ljava/lang/String;)V

    .line 652
    iget-object v0, p0, Lkdw;->k:Lcom/google/android/libraries/social/filecache/FileCache;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/filecache/FileCache;->computeCapacity()J

    move-result-wide v0

    .line 653
    iget-object v2, p0, Lkdw;->k:Lcom/google/android/libraries/social/filecache/FileCache;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/filecache/FileCache;->getUsedSpace()J

    move-result-wide v2

    .line 654
    sub-long v4, v0, v2

    invoke-static {v6, v7, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    .line 656
    invoke-static {v0, v1}, Llsu;->a(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 657
    invoke-static {v2, v3}, Llsu;->a(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 658
    invoke-static {v4, v5}, Llsu;->a(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x2c

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Long-term cache total size: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "; used: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; free: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 655
    invoke-virtual {p1, v0}, Llox;->println(Ljava/lang/String;)V

    .line 659
    return-void
.end method

.method public c()Lkeh;
    .locals 1

    .prologue
    .line 264
    iget-object v0, p0, Lkdw;->h:Lkeh;

    return-object v0
.end method

.method public d()Lcom/google/android/libraries/social/filecache/FileCache;
    .locals 1

    .prologue
    .line 269
    iget-object v0, p0, Lkdw;->j:Lcom/google/android/libraries/social/filecache/FileCache;

    return-object v0
.end method

.method public e()Lcom/google/android/libraries/social/filecache/FileCache;
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, Lkdw;->k:Lcom/google/android/libraries/social/filecache/FileCache;

    return-object v0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 285
    iget v0, p0, Lkdw;->o:I

    return v0
.end method

.method public g()I
    .locals 1

    .prologue
    .line 293
    iget v0, p0, Lkdw;->p:I

    return v0
.end method

.method public h()I
    .locals 1

    .prologue
    .line 301
    iget v0, p0, Lkdw;->q:I

    return v0
.end method

.method public i()Landroid/graphics/Bitmap$Config;
    .locals 2

    .prologue
    .line 432
    iget v0, p0, Lkdw;->l:I

    const/16 v1, 0x40

    if-ge v0, v1, :cond_0

    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_4444:Landroid/graphics/Bitmap$Config;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    goto :goto_0
.end method

.method public j()J
    .locals 2

    .prologue
    .line 438
    iget-object v0, p0, Lkdw;->c:Lkeu;

    iget-wide v0, v0, Lkeu;->c:J

    return-wide v0
.end method

.method public k()J
    .locals 2

    .prologue
    .line 443
    iget-object v0, p0, Lkdw;->c:Lkeu;

    iget-wide v0, v0, Lkeu;->d:J

    return-wide v0
.end method

.method public l()V
    .locals 4

    .prologue
    .line 506
    iget-object v0, p0, Lkdw;->e:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 518
    :cond_0
    return-void

    .line 510
    :cond_1
    invoke-virtual {p0}, Lkdw;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Llsa;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 511
    iget-object v0, p0, Lkdw;->e:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkda;

    .line 512
    invoke-virtual {v0}, Lkda;->getStatus()I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_2

    .line 513
    const/4 v2, 0x2

    invoke-virtual {p0, v0, v2}, Lkdw;->a(Lkda;I)V

    .line 514
    invoke-virtual {p0, v0}, Lkdw;->e(Lkda;)V

    goto :goto_0
.end method

.method public m()F
    .locals 1

    .prologue
    .line 539
    iget v0, p0, Lkdw;->n:F

    return v0
.end method

.method public n()V
    .locals 1

    .prologue
    .line 547
    iget-object v0, p0, Lkdw;->g:Lgl;

    invoke-virtual {v0}, Lgl;->b()V

    .line 548
    return-void
.end method

.method public o()V
    .locals 4

    .prologue
    .line 552
    iget-object v0, p0, Lkdw;->g:Lgl;

    invoke-virtual {v0}, Lgl;->b()V

    .line 555
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x7d0

    add-long/2addr v0, v2

    iput-wide v0, p0, Lkdw;->i:J

    .line 558
    iget-object v0, p0, Lkdw;->h:Lkeh;

    if-eqz v0, :cond_0

    .line 559
    iget-object v0, p0, Lkdw;->h:Lkeh;

    invoke-virtual {v0}, Lkeh;->b()V

    .line 561
    :cond_0
    return-void
.end method

.method public p()V
    .locals 1

    .prologue
    .line 568
    iget-object v0, p0, Lkdw;->e:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 569
    invoke-virtual {p0}, Lkdw;->q()V

    .line 571
    :cond_0
    return-void
.end method

.method public q()V
    .locals 3

    .prologue
    .line 578
    new-instance v0, Llox;

    invoke-direct {v0}, Llox;-><init>()V

    .line 579
    invoke-virtual {p0, v0}, Lkdw;->a(Llox;)V

    .line 580
    const/4 v1, 0x4

    const-string v2, "ImageResourceManager"

    invoke-virtual {v0}, Llox;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Llse;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 581
    return-void
.end method
