.class public final Llun;
.super Loxq;
.source "PG"


# instance fields
.field public a:Llut;

.field public b:[Lluk;

.field private c:[Lluu;

.field private d:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Loxq;-><init>()V

    .line 12
    const/4 v0, 0x0

    iput-object v0, p0, Llun;->a:Llut;

    .line 15
    sget-object v0, Lluk;->a:[Lluk;

    iput-object v0, p0, Llun;->b:[Lluk;

    .line 18
    sget-object v0, Lluu;->a:[Lluu;

    iput-object v0, p0, Llun;->c:[Lluu;

    .line 21
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Llun;->d:[Ljava/lang/String;

    .line 9
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 54
    .line 55
    iget-object v0, p0, Llun;->a:Llut;

    if-eqz v0, :cond_6

    .line 56
    const/4 v0, 0x1

    iget-object v2, p0, Llun;->a:Llut;

    .line 57
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 59
    :goto_0
    iget-object v2, p0, Llun;->b:[Lluk;

    if-eqz v2, :cond_1

    .line 60
    iget-object v3, p0, Llun;->b:[Lluk;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 61
    if-eqz v5, :cond_0

    .line 62
    const/4 v6, 0x2

    .line 63
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 60
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 67
    :cond_1
    iget-object v2, p0, Llun;->c:[Lluu;

    if-eqz v2, :cond_3

    .line 68
    iget-object v3, p0, Llun;->c:[Lluu;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_3

    aget-object v5, v3, v2

    .line 69
    if-eqz v5, :cond_2

    .line 70
    const/4 v6, 0x3

    .line 71
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 68
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 75
    :cond_3
    iget-object v2, p0, Llun;->d:[Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Llun;->d:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_5

    .line 77
    iget-object v3, p0, Llun;->d:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_3
    if-ge v1, v4, :cond_4

    aget-object v5, v3, v1

    .line 79
    invoke-static {v5}, Loxo;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 77
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 81
    :cond_4
    add-int/2addr v0, v2

    .line 82
    iget-object v1, p0, Llun;->d:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 84
    :cond_5
    iget-object v1, p0, Llun;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 85
    iput v0, p0, Llun;->ai:I

    .line 86
    return v0

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Llun;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 94
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 95
    sparse-switch v0, :sswitch_data_0

    .line 99
    iget-object v2, p0, Llun;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 100
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Llun;->ah:Ljava/util/List;

    .line 103
    :cond_1
    iget-object v2, p0, Llun;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 105
    :sswitch_0
    return-object p0

    .line 110
    :sswitch_1
    iget-object v0, p0, Llun;->a:Llut;

    if-nez v0, :cond_2

    .line 111
    new-instance v0, Llut;

    invoke-direct {v0}, Llut;-><init>()V

    iput-object v0, p0, Llun;->a:Llut;

    .line 113
    :cond_2
    iget-object v0, p0, Llun;->a:Llut;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 117
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 118
    iget-object v0, p0, Llun;->b:[Lluk;

    if-nez v0, :cond_4

    move v0, v1

    .line 119
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lluk;

    .line 120
    iget-object v3, p0, Llun;->b:[Lluk;

    if-eqz v3, :cond_3

    .line 121
    iget-object v3, p0, Llun;->b:[Lluk;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 123
    :cond_3
    iput-object v2, p0, Llun;->b:[Lluk;

    .line 124
    :goto_2
    iget-object v2, p0, Llun;->b:[Lluk;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 125
    iget-object v2, p0, Llun;->b:[Lluk;

    new-instance v3, Lluk;

    invoke-direct {v3}, Lluk;-><init>()V

    aput-object v3, v2, v0

    .line 126
    iget-object v2, p0, Llun;->b:[Lluk;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 127
    invoke-virtual {p1}, Loxn;->a()I

    .line 124
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 118
    :cond_4
    iget-object v0, p0, Llun;->b:[Lluk;

    array-length v0, v0

    goto :goto_1

    .line 130
    :cond_5
    iget-object v2, p0, Llun;->b:[Lluk;

    new-instance v3, Lluk;

    invoke-direct {v3}, Lluk;-><init>()V

    aput-object v3, v2, v0

    .line 131
    iget-object v2, p0, Llun;->b:[Lluk;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 135
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 136
    iget-object v0, p0, Llun;->c:[Lluu;

    if-nez v0, :cond_7

    move v0, v1

    .line 137
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lluu;

    .line 138
    iget-object v3, p0, Llun;->c:[Lluu;

    if-eqz v3, :cond_6

    .line 139
    iget-object v3, p0, Llun;->c:[Lluu;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 141
    :cond_6
    iput-object v2, p0, Llun;->c:[Lluu;

    .line 142
    :goto_4
    iget-object v2, p0, Llun;->c:[Lluu;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_8

    .line 143
    iget-object v2, p0, Llun;->c:[Lluu;

    new-instance v3, Lluu;

    invoke-direct {v3}, Lluu;-><init>()V

    aput-object v3, v2, v0

    .line 144
    iget-object v2, p0, Llun;->c:[Lluu;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 145
    invoke-virtual {p1}, Loxn;->a()I

    .line 142
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 136
    :cond_7
    iget-object v0, p0, Llun;->c:[Lluu;

    array-length v0, v0

    goto :goto_3

    .line 148
    :cond_8
    iget-object v2, p0, Llun;->c:[Lluu;

    new-instance v3, Lluu;

    invoke-direct {v3}, Lluu;-><init>()V

    aput-object v3, v2, v0

    .line 149
    iget-object v2, p0, Llun;->c:[Lluu;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 153
    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 154
    iget-object v0, p0, Llun;->d:[Ljava/lang/String;

    array-length v0, v0

    .line 155
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 156
    iget-object v3, p0, Llun;->d:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 157
    iput-object v2, p0, Llun;->d:[Ljava/lang/String;

    .line 158
    :goto_5
    iget-object v2, p0, Llun;->d:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    .line 159
    iget-object v2, p0, Llun;->d:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 160
    invoke-virtual {p1}, Loxn;->a()I

    .line 158
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 163
    :cond_9
    iget-object v2, p0, Llun;->d:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    .line 95
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 26
    iget-object v1, p0, Llun;->a:Llut;

    if-eqz v1, :cond_0

    .line 27
    const/4 v1, 0x1

    iget-object v2, p0, Llun;->a:Llut;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 29
    :cond_0
    iget-object v1, p0, Llun;->b:[Lluk;

    if-eqz v1, :cond_2

    .line 30
    iget-object v2, p0, Llun;->b:[Lluk;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 31
    if-eqz v4, :cond_1

    .line 32
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 30
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 36
    :cond_2
    iget-object v1, p0, Llun;->c:[Lluu;

    if-eqz v1, :cond_4

    .line 37
    iget-object v2, p0, Llun;->c:[Lluu;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 38
    if-eqz v4, :cond_3

    .line 39
    const/4 v5, 0x3

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 37
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 43
    :cond_4
    iget-object v1, p0, Llun;->d:[Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 44
    iget-object v1, p0, Llun;->d:[Ljava/lang/String;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    .line 45
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Loxo;->a(ILjava/lang/String;)V

    .line 44
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 48
    :cond_5
    iget-object v0, p0, Llun;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 50
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Llun;->a(Loxn;)Llun;

    move-result-object v0

    return-object v0
.end method
