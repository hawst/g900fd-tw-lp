.class final Ldfg;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcvh;


# instance fields
.field private synthetic a:Ldex;


# direct methods
.method constructor <init>(Ldex;)V
    .locals 0

    .prologue
    .line 1271
    iput-object p1, p0, Ldfg;->a:Ldex;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lizu;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1294
    invoke-virtual {p1}, Lizu;->g()Ljac;

    move-result-object v0

    sget-object v3, Ljac;->b:Ljac;

    if-ne v0, v3, :cond_0

    move v0, v1

    .line 1295
    :goto_0
    iget-object v3, p0, Ldfg;->a:Ldex;

    .line 1296
    invoke-virtual {v3}, Ldex;->o()Landroid/content/res/Resources;

    move-result-object v4

    if-eqz v0, :cond_1

    const v3, 0x7f110052

    :goto_1
    invoke-virtual {v4, v3, v1}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v4

    iget-object v3, p0, Ldfg;->a:Ldex;

    .line 1298
    invoke-virtual {v3}, Ldex;->o()Landroid/content/res/Resources;

    move-result-object v5

    if-eqz v0, :cond_2

    const v3, 0x7f110055

    :goto_2
    invoke-virtual {v5, v3, v1}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Ldfg;->a:Ldex;

    const v5, 0x7f0a07fa

    .line 1300
    invoke-virtual {v3, v5}, Ldex;->e_(I)Ljava/lang/String;

    move-result-object v3

    iget-object v5, p0, Ldfg;->a:Ldex;

    const v6, 0x7f0a07fd

    .line 1301
    invoke-virtual {v5, v6}, Ldex;->e_(I)Ljava/lang/String;

    move-result-object v5

    .line 1295
    invoke-static {v4, v1, v3, v5}, Llgr;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)Llgr;

    move-result-object v1

    .line 1302
    invoke-virtual {v1}, Llgr;->k()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "tile_id"

    invoke-virtual {v3, v4, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1303
    invoke-virtual {v1}, Llgr;->k()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "owner_id"

    invoke-virtual {p1}, Lizu;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1304
    iget-object v3, p0, Ldfg;->a:Ldex;

    invoke-virtual {v1, v3, v2}, Llgr;->a(Lu;I)V

    .line 1305
    invoke-virtual {v1}, Llgr;->k()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "is_video"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1306
    iget-object v0, p0, Ldfg;->a:Ldex;

    invoke-virtual {v0}, Ldex;->p()Lae;

    move-result-object v0

    const-string v2, "perm_delete_photo"

    invoke-virtual {v1, v0, v2}, Llgr;->a(Lae;Ljava/lang/String;)V

    .line 1307
    return-void

    :cond_0
    move v0, v2

    .line 1294
    goto :goto_0

    .line 1296
    :cond_1
    const v3, 0x7f110053

    goto :goto_1

    .line 1298
    :cond_2
    const v3, 0x7f110056

    goto :goto_2
.end method

.method public a(Ljcl;)V
    .locals 3

    .prologue
    .line 1275
    new-instance v0, Ljcn;

    invoke-direct {v0}, Ljcn;-><init>()V

    .line 1276
    invoke-virtual {v0, p1}, Ljcn;->a(Ljcl;)V

    .line 1277
    iget-object v1, p0, Ldfg;->a:Ldex;

    invoke-static {v1}, Ldex;->i(Ldex;)Lcsg;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcsg;->a(Ljcn;Z)V

    .line 1278
    return-void
.end method

.method public b(Lizu;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    .line 1282
    invoke-virtual {p1}, Lizu;->g()Ljac;

    move-result-object v0

    sget-object v2, Ljac;->b:Ljac;

    if-ne v0, v2, :cond_0

    move v0, v1

    .line 1283
    :goto_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 1284
    invoke-virtual {v2, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1285
    iget-object v3, p0, Ldfg;->a:Ldex;

    iget-object v4, p0, Ldfg;->a:Ldex;

    .line 1286
    invoke-static {v4}, Ldex;->j(Ldex;)Llnl;

    move-result-object v4

    iget-object v5, p0, Ldfg;->a:Ldex;

    invoke-static {v5}, Ldex;->k(Ldex;)Lhee;

    move-result-object v5

    invoke-interface {v5}, Lhee;->d()I

    move-result v5

    invoke-virtual {p1}, Lizu;->b()Ljava/lang/String;

    move-result-object v6

    .line 1285
    invoke-static {v4, v5, v6, v2}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILjava/lang/String;Ljava/util/ArrayList;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v3, v2}, Ldex;->a(Ldex;Ljava/lang/Integer;)Ljava/lang/Integer;

    .line 1287
    iget-object v2, p0, Ldfg;->a:Ldex;

    const/16 v3, 0x20

    iget-object v4, p0, Ldfg;->a:Ldex;

    .line 1288
    invoke-virtual {v4}, Ldex;->o()Landroid/content/res/Resources;

    move-result-object v4

    if-eqz v0, :cond_1

    const v0, 0x7f11005b

    :goto_1
    invoke-virtual {v4, v0, v1}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v0

    .line 1287
    invoke-static {v2, v3, v0}, Ldex;->a(Ldex;ILjava/lang/String;)V

    .line 1290
    return-void

    .line 1282
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1288
    :cond_1
    const v0, 0x7f11005c

    goto :goto_1
.end method
