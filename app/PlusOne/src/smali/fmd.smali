.class public final Lfmd;
.super Llol;
.source "PG"

# interfaces
.implements Lkhf;
.implements Llgs;


# instance fields
.field private N:Llgr;

.field private final O:Lfmg;

.field private final P:Lkhe;

.field private Q:Lflu;

.field private R:Lflu;

.field private S:Lkhr;

.field private T:Lhee;

.field private U:I

.field private V:Lieh;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 43
    invoke-direct {p0}, Llol;-><init>()V

    .line 59
    new-instance v0, Lfmg;

    invoke-direct {v0}, Lfmg;-><init>()V

    iput-object v0, p0, Lfmd;->O:Lfmg;

    .line 60
    new-instance v0, Lkhe;

    iget-object v1, p0, Lfmd;->av:Llqm;

    invoke-direct {v0, p0, v1}, Lkhe;-><init>(Lkhf;Llqr;)V

    iput-object v0, p0, Lfmd;->P:Lkhe;

    .line 61
    new-instance v0, Lflu;

    iget-object v1, p0, Lfmd;->av:Llqm;

    invoke-direct {v0, v1}, Lflu;-><init>(Llqr;)V

    iput-object v0, p0, Lfmd;->Q:Lflu;

    .line 62
    new-instance v0, Lflu;

    iget-object v1, p0, Lfmd;->av:Llqm;

    iget-object v2, p0, Lfmd;->O:Lfmg;

    invoke-direct {v0, v1, v2}, Lflu;-><init>(Llqr;Lfly;)V

    iput-object v0, p0, Lfmd;->R:Lflu;

    .line 73
    new-instance v0, Lhmg;

    sget-object v1, Loms;->a:Lhmn;

    invoke-direct {v0, v1}, Lhmg;-><init>(Lhmn;)V

    iget-object v1, p0, Lfmd;->au:Llnh;

    .line 74
    invoke-virtual {v0, v1}, Lhmg;->a(Llnh;)Lhmg;

    .line 75
    new-instance v0, Lhmf;

    iget-object v1, p0, Lfmd;->av:Llqm;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lhmf;-><init>(Llqr;B)V

    .line 78
    return-void
.end method

.method static synthetic a(Lfmd;Llgr;)Llgr;
    .locals 0

    .prologue
    .line 43
    iput-object p1, p0, Lfmd;->N:Llgr;

    return-object p1
.end method

.method static synthetic a(Lfmd;)Llnl;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lfmd;->at:Llnl;

    return-object v0
.end method

.method static synthetic b(Lfmd;)Llnl;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lfmd;->at:Llnl;

    return-object v0
.end method

.method static synthetic c(Lfmd;)Llnl;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lfmd;->at:Llnl;

    return-object v0
.end method

.method static synthetic d(Lfmd;)I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lfmd;->U:I

    return v0
.end method

.method private d()V
    .locals 4

    .prologue
    .line 233
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lfmd;->at:Llnl;

    const-class v2, Lcom/google/android/apps/plus/settings/profile/ProfileSettingsActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 234
    const-string v1, "account_id"

    iget v2, p0, Lfmd;->U:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 235
    iget-object v1, p0, Lfmd;->S:Lkhr;

    const v2, 0x7f0a0b4a

    .line 236
    invoke-virtual {p0, v2}, Lfmd;->e_(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    .line 235
    invoke-virtual {v1, v2, v3, v0}, Lkhr;->b(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)Lcom/google/android/libraries/social/settings/LabelPreference;

    move-result-object v0

    .line 237
    const-string v1, "profile_preference_key"

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/settings/LabelPreference;->d(Ljava/lang/String;)V

    .line 238
    const v1, 0x7f0204b8

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/settings/LabelPreference;->h(I)V

    .line 239
    iget-object v1, p0, Lfmd;->P:Lkhe;

    invoke-virtual {v1, v0}, Lkhe;->a(Lkhl;)Lkhl;

    .line 240
    return-void
.end method

.method static synthetic e(Lfmd;)Llnl;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lfmd;->at:Llnl;

    return-object v0
.end method

.method private e()V
    .locals 3

    .prologue
    .line 295
    iget-object v0, p0, Lfmd;->S:Lkhr;

    const v1, 0x7f0a0632

    invoke-virtual {p0, v1}, Lfmd;->e_(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lkhr;->a(Ljava/lang/String;Ljava/lang/String;)Lkhl;

    move-result-object v0

    .line 297
    new-instance v1, Lfmf;

    invoke-direct {v1, p0}, Lfmf;-><init>(Lfmd;)V

    invoke-virtual {v0, v1}, Lkhl;->a(Lkhq;)V

    .line 304
    iget-object v1, p0, Lfmd;->P:Lkhe;

    invoke-virtual {v1, v0}, Lkhe;->a(Lkhl;)Lkhl;

    .line 305
    return-void
.end method

.method static synthetic f(Lfmd;)Llnl;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lfmd;->at:Llnl;

    return-object v0
.end method

.method static synthetic g(Lfmd;)Llnl;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lfmd;->at:Llnl;

    return-object v0
.end method

.method static synthetic h(Lfmd;)Llnl;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lfmd;->at:Llnl;

    return-object v0
.end method

.method static synthetic i(Lfmd;)Llgr;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lfmd;->N:Llgr;

    return-object v0
.end method

.method static synthetic j(Lfmd;)Lflu;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lfmd;->Q:Lflu;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 119
    new-instance v0, Lkhr;

    iget-object v3, p0, Lfmd;->at:Llnl;

    invoke-direct {v0, v3}, Lkhr;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lfmd;->S:Lkhr;

    .line 120
    iget-object v0, p0, Lfmd;->T:Lhee;

    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v0

    .line 122
    invoke-interface {v0}, Lhej;->a()Z

    move-result v3

    if-nez v3, :cond_1

    .line 123
    invoke-virtual {p0}, Lfmd;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->finish()V

    .line 177
    :cond_0
    :goto_0
    return-void

    .line 127
    :cond_1
    const-string v3, "is_google_plus"

    invoke-interface {v0, v3}, Lhej;->c(Ljava/lang/String;)Z

    move-result v3

    .line 128
    const-string v4, "is_managed_account"

    invoke-interface {v0, v4}, Lhej;->c(Ljava/lang/String;)Z

    move-result v4

    .line 129
    invoke-static {}, Lfvc;->c()Z

    move-result v5

    .line 131
    if-eqz v3, :cond_2

    .line 132
    new-instance v0, Landroid/content/Intent;

    iget-object v6, p0, Lfmd;->at:Llnl;

    const-class v7, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;

    invoke-direct {v0, v6, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v6, "account_id"

    iget v7, p0, Lfmd;->U:I

    invoke-virtual {v0, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v6, p0, Lfmd;->S:Lkhr;

    const v7, 0x7f0a0699

    invoke-virtual {p0, v7}, Lfmd;->e_(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7, v2, v0}, Lkhr;->b(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)Lcom/google/android/libraries/social/settings/LabelPreference;

    move-result-object v0

    const-string v6, "notifications_preference_key"

    invoke-virtual {v0, v6}, Lcom/google/android/libraries/social/settings/LabelPreference;->d(Ljava/lang/String;)V

    const v6, 0x7f020525

    invoke-virtual {v0, v6}, Lcom/google/android/libraries/social/settings/LabelPreference;->h(I)V

    iget-object v6, p0, Lfmd;->P:Lkhe;

    invoke-virtual {v6, v0}, Lkhe;->a(Lkhl;)Lkhl;

    .line 133
    new-instance v0, Landroid/content/Intent;

    iget-object v6, p0, Lfmd;->at:Llnl;

    const-class v7, Lcom/google/android/apps/plus/settings/GplusPhotosSettingsActivity;

    invoke-direct {v0, v6, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v6, "account_id"

    iget v7, p0, Lfmd;->U:I

    invoke-virtual {v0, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v6, p0, Lfmd;->S:Lkhr;

    const v7, 0x7f0a069b

    invoke-virtual {p0, v7}, Lfmd;->e_(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7, v2, v0}, Lkhr;->b(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)Lcom/google/android/libraries/social/settings/LabelPreference;

    move-result-object v0

    const-string v6, "photos_preference_key"

    invoke-virtual {v0, v6}, Lcom/google/android/libraries/social/settings/LabelPreference;->d(Ljava/lang/String;)V

    const v6, 0x7f020532

    invoke-virtual {v0, v6}, Lcom/google/android/libraries/social/settings/LabelPreference;->h(I)V

    iget-object v6, p0, Lfmd;->P:Lkhe;

    invoke-virtual {v6, v0}, Lkhe;->a(Lkhl;)Lkhl;

    .line 134
    new-instance v0, Landroid/content/Intent;

    iget-object v6, p0, Lfmd;->at:Llnl;

    const-class v7, Lcom/google/android/apps/plus/settings/SharingSettingsActivity;

    invoke-direct {v0, v6, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v6, "account_id"

    iget v7, p0, Lfmd;->U:I

    invoke-virtual {v0, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v6, p0, Lfmd;->S:Lkhr;

    const v7, 0x7f0a06f1

    invoke-virtual {p0, v7}, Lfmd;->e_(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7, v2, v0}, Lkhr;->b(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)Lcom/google/android/libraries/social/settings/LabelPreference;

    move-result-object v0

    const-string v6, "sharing_preference_key"

    invoke-virtual {v0, v6}, Lcom/google/android/libraries/social/settings/LabelPreference;->d(Ljava/lang/String;)V

    const v6, 0x7f020547

    invoke-virtual {v0, v6}, Lcom/google/android/libraries/social/settings/LabelPreference;->h(I)V

    iget-object v6, p0, Lfmd;->P:Lkhe;

    invoke-virtual {v6, v0}, Lkhe;->a(Lkhl;)Lkhl;

    .line 135
    if-eqz v4, :cond_4

    .line 136
    invoke-direct {p0}, Lfmd;->d()V

    .line 151
    :cond_2
    :goto_1
    if-eqz v3, :cond_9

    if-nez v4, :cond_9

    if-nez v5, :cond_9

    .line 153
    iget-object v0, p0, Lfmd;->V:Lieh;

    sget-object v3, Ldxd;->E:Lief;

    iget v4, p0, Lfmd;->U:I

    invoke-interface {v0, v3, v4}, Lieh;->b(Lief;I)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 154
    invoke-direct {p0}, Lfmd;->e()V

    .line 155
    iget-object v0, p0, Lfmd;->S:Lkhr;

    const v3, 0x7f0a088c

    invoke-virtual {p0, v3}, Lfmd;->e_(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3, v2}, Lkhr;->a(Ljava/lang/String;Ljava/lang/String;)Lkhl;

    move-result-object v0

    new-instance v3, Lfme;

    invoke-direct {v3, p0}, Lfme;-><init>(Lfmd;)V

    invoke-virtual {v0, v3}, Lkhl;->a(Lkhq;)V

    iget-object v3, p0, Lfmd;->P:Lkhe;

    invoke-virtual {v3, v0}, Lkhe;->a(Lkhl;)Lkhl;

    .line 168
    :goto_2
    invoke-static {}, Lfvc;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    if-nez v5, :cond_3

    .line 169
    new-instance v0, Landroid/content/Intent;

    iget-object v3, p0, Lfmd;->at:Llnl;

    const-class v4, Lcom/google/android/apps/plus/settings/GplusDeveloperSettingsActivity;

    invoke-direct {v0, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v3, "account_id"

    iget v4, p0, Lfmd;->U:I

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v3, p0, Lfmd;->S:Lkhr;

    const v4, 0x7f0a088d

    invoke-virtual {p0, v4}, Lfmd;->e_(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v2, v0}, Lkhr;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)Lkhl;

    move-result-object v0

    const-string v2, "developer_preference_key"

    invoke-virtual {v0, v2}, Lkhl;->d(Ljava/lang/String;)V

    iget-object v2, p0, Lfmd;->P:Lkhe;

    invoke-virtual {v2, v0}, Lkhe;->a(Lkhl;)Lkhl;

    .line 172
    :cond_3
    invoke-virtual {p0}, Lfmd;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->f()Lae;

    move-result-object v0

    const-string v2, "delete_dialog_tag"

    .line 173
    invoke-virtual {v0, v2}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Llgr;

    iput-object v0, p0, Lfmd;->N:Llgr;

    .line 174
    iget-object v0, p0, Lfmd;->N:Llgr;

    if-eqz v0, :cond_0

    .line 175
    iget-object v0, p0, Lfmd;->N:Llgr;

    invoke-virtual {v0, p0, v1}, Llgr;->a(Lu;I)V

    goto/16 :goto_0

    .line 139
    :cond_4
    iget-object v0, p0, Lfmd;->T:Lhee;

    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v0

    const-string v6, "is_plus_page"

    invoke-interface {v0, v6}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 140
    iget-object v0, p0, Lfmd;->at:Llnl;

    iget v6, p0, Lfmd;->U:I

    invoke-static {v0, v6}, Leyq;->c(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v0

    const/high16 v6, 0x2000000

    invoke-virtual {v0, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string v6, "account_id"

    iget v7, p0, Lfmd;->U:I

    invoke-virtual {v0, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v6, p0, Lfmd;->S:Lkhr;

    const v7, 0x7f0a069a

    invoke-virtual {p0, v7}, Lfmd;->e_(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7, v2, v0}, Lkhr;->b(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)Lcom/google/android/libraries/social/settings/LabelPreference;

    move-result-object v0

    const-string v6, "device_location_preference_key"

    invoke-virtual {v0, v6}, Lcom/google/android/libraries/social/settings/LabelPreference;->d(Ljava/lang/String;)V

    const v6, 0x7f020519

    invoke-virtual {v0, v6}, Lcom/google/android/libraries/social/settings/LabelPreference;->h(I)V

    iget-object v6, p0, Lfmd;->P:Lkhe;

    invoke-virtual {v6, v0}, Lkhe;->a(Lkhl;)Lkhl;

    .line 142
    :cond_5
    new-instance v0, Landroid/content/Intent;

    iget-object v6, p0, Lfmd;->at:Llnl;

    const-class v7, Lcom/google/android/apps/plus/settings/ContactsSettingsActivity;

    invoke-direct {v0, v6, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v6, "account_id"

    iget v7, p0, Lfmd;->U:I

    invoke-virtual {v0, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v6, p0, Lfmd;->S:Lkhr;

    const v7, 0x7f0a06f3

    invoke-virtual {p0, v7}, Lfmd;->e_(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7, v2, v0}, Lkhr;->b(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)Lcom/google/android/libraries/social/settings/LabelPreference;

    move-result-object v0

    const-string v6, "contacts_preference_key"

    invoke-virtual {v0, v6}, Lcom/google/android/libraries/social/settings/LabelPreference;->d(Ljava/lang/String;)V

    const v6, 0x7f02052d

    invoke-virtual {v0, v6}, Lcom/google/android/libraries/social/settings/LabelPreference;->h(I)V

    iget-object v6, p0, Lfmd;->P:Lkhe;

    invoke-virtual {v6, v0}, Lkhe;->a(Lkhl;)Lkhl;

    .line 143
    invoke-direct {p0}, Lfmd;->d()V

    .line 145
    iget-object v0, p0, Lfmd;->T:Lhee;

    invoke-interface {v0}, Lhee;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v6, p0, Lfmd;->at:Llnl;

    iget v7, p0, Lfmd;->U:I

    const-class v0, Lieh;

    invoke-static {v6, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    sget-object v8, Ldxd;->b:Lief;

    invoke-interface {v0, v8, v7}, Lieh;->b(Lief;I)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    invoke-static {v6, v7}, Lfuj;->a(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    :goto_3
    if-eqz v0, :cond_7

    const-string v6, "android.permission.GET_ACCOUNTS"

    invoke-virtual {v8, v6, v0}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_7

    const/4 v0, 0x1

    :goto_4
    if-eqz v0, :cond_2

    .line 146
    iget-object v0, p0, Lfmd;->at:Llnl;

    iget v6, p0, Lfmd;->U:I

    invoke-static {v0, v6}, Lfuj;->a(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v0

    const-string v6, "account_id"

    iget v7, p0, Lfmd;->U:I

    invoke-virtual {v0, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v6, p0, Lfmd;->S:Lkhr;

    const v7, 0x7f0a06ef

    invoke-virtual {p0, v7}, Lfmd;->e_(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7, v2, v0}, Lkhr;->b(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)Lcom/google/android/libraries/social/settings/LabelPreference;

    move-result-object v0

    const-string v6, "apps_preference_key"

    invoke-virtual {v0, v6}, Lcom/google/android/libraries/social/settings/LabelPreference;->d(Ljava/lang/String;)V

    const v6, 0x7f0204bc

    invoke-virtual {v0, v6}, Lcom/google/android/libraries/social/settings/LabelPreference;->h(I)V

    iget-object v6, p0, Lfmd;->P:Lkhe;

    invoke-virtual {v6, v0}, Lkhe;->a(Lkhl;)Lkhl;

    goto/16 :goto_1

    :cond_6
    move-object v0, v2

    .line 145
    goto :goto_3

    :cond_7
    move v0, v1

    goto :goto_4

    .line 157
    :cond_8
    new-instance v0, Landroid/content/Intent;

    iget-object v3, p0, Lfmd;->at:Llnl;

    const-class v4, Lcom/google/android/apps/plus/settings/AccountSettingsActivity;

    invoke-direct {v0, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v3, "account_id"

    iget v4, p0, Lfmd;->U:I

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v3, p0, Lfmd;->S:Lkhr;

    const v4, 0x7f0a088e

    invoke-virtual {p0, v4}, Lfmd;->e_(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v2, v0}, Lkhr;->b(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)Lcom/google/android/libraries/social/settings/LabelPreference;

    move-result-object v0

    const-string v3, "account_preference_key"

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/settings/LabelPreference;->d(Ljava/lang/String;)V

    const v3, 0x7f02053c

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/settings/LabelPreference;->h(I)V

    iget-object v3, p0, Lfmd;->P:Lkhe;

    invoke-virtual {v3, v0}, Lkhe;->a(Lkhl;)Lkhl;

    .line 158
    :cond_9
    invoke-direct {p0}, Lfmd;->e()V

    goto/16 :goto_2
.end method

.method public a(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 344
    return-void
.end method

.method public a(IZLandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 347
    return-void
.end method

.method public a(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 318
    const-string v0, "delete_dialog_tag"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 319
    iget-object v0, p0, Lfmd;->T:Lhee;

    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v0

    const-string v1, "gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 320
    iget-object v1, p0, Lfmd;->O:Lfmg;

    invoke-virtual {v1, v0}, Lfmg;->a(Ljava/lang/String;)V

    .line 321
    iget-object v0, p0, Lfmd;->R:Lflu;

    invoke-virtual {v0}, Lflu;->a()V

    .line 323
    :cond_0
    return-void
.end method

.method public b(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 327
    const-string v0, "delete_dialog_tag"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 328
    iget-object v0, p0, Lfmd;->N:Llgr;

    if-eqz v0, :cond_0

    .line 329
    iget-object v0, p0, Lfmd;->N:Llgr;

    invoke-virtual {v0}, Llgr;->c()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->cancel()V

    .line 332
    :cond_0
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 111
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 112
    iget-object v0, p0, Lfmd;->au:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Lfmd;->T:Lhee;

    .line 113
    iget-object v0, p0, Lfmd;->T:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    iput v0, p0, Lfmd;->U:I

    .line 114
    iget-object v0, p0, Lfmd;->at:Llnl;

    const-class v1, Lieh;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    iput-object v0, p0, Lfmd;->V:Lieh;

    .line 115
    return-void
.end method

.method public c(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 336
    const-string v0, "delete_dialog_tag"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 337
    iget-object v0, p0, Lfmd;->N:Llgr;

    if-eqz v0, :cond_0

    .line 338
    iget-object v0, p0, Lfmd;->N:Llgr;

    invoke-virtual {v0}, Llgr;->c()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->cancel()V

    .line 341
    :cond_0
    return-void
.end method
