.class public final Ljtp;
.super Llol;
.source "PG"

# interfaces
.implements Lhyz;
.implements Lhzb;
.implements Lhzq;


# instance fields
.field private N:Ljava/lang/String;

.field private O:Ljtu;

.field private P:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lhyz;",
            ">;"
        }
    .end annotation
.end field

.field private Q:I

.field private R:Lhzb;

.field private S:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljpv;",
            ">;"
        }
    .end annotation
.end field

.field private T:Ljtj;

.field private U:Lhzl;

.field private V:Lllg;

.field private W:Lhhx;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Llol;-><init>()V

    .line 35
    const-string v0, "peopleSearchAggregator"

    iput-object v0, p0, Ljtp;->N:Ljava/lang/String;

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ljtp;->S:Ljava/util/List;

    .line 42
    new-instance v0, Ljtj;

    invoke-direct {v0}, Ljtj;-><init>()V

    iput-object v0, p0, Ljtp;->T:Ljtj;

    .line 47
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)I
    .locals 1

    .prologue
    .line 122
    const/4 v0, 0x1

    return v0
.end method

.method public a(I)Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Ljtp;->S:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    return-object v0
.end method

.method public a(ILandroid/view/View;)V
    .locals 1

    .prologue
    .line 164
    check-cast p2, Lcom/google/android/libraries/social/people/providers/search/PersonSearchRowView;

    iget-object v0, p0, Ljtp;->S:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljpv;

    invoke-virtual {p2, v0}, Lcom/google/android/libraries/social/people/providers/search/PersonSearchRowView;->a(Ljpv;)V

    .line 167
    invoke-virtual {p0}, Ljtp;->d()I

    move-result v0

    add-int/lit8 v0, v0, -0xa

    if-lt p1, v0, :cond_0

    .line 168
    iget-object v0, p0, Ljtp;->T:Ljtj;

    invoke-virtual {v0}, Ljtj;->a()V

    .line 170
    :cond_0
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 86
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 87
    if-nez p1, :cond_0

    .line 88
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ljtp;->P:Ljava/util/List;

    .line 89
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    iget v0, p0, Ljtp;->Q:I

    if-ge v2, v0, :cond_0

    .line 90
    iget-object v0, p0, Ljtp;->O:Ljtu;

    invoke-virtual {v0, v2}, Ljtu;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhyz;

    .line 91
    invoke-interface {v0, p0}, Lhyz;->a(Lhzb;)V

    .line 92
    invoke-virtual {p0}, Ljtp;->q()Lae;

    move-result-object v1

    invoke-virtual {v1}, Lae;->a()Lat;

    move-result-object v3

    move-object v1, v0

    check-cast v1, Lu;

    iget-object v4, p0, Ljtp;->N:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0xb

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 93
    invoke-virtual {v3, v1, v4}, Lat;->a(Lu;Ljava/lang/String;)Lat;

    move-result-object v1

    invoke-virtual {v1}, Lat;->b()I

    .line 94
    iget-object v1, p0, Ljtp;->P:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 89
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 97
    :cond_0
    return-void
.end method

.method public a(Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 51
    iget-object v0, p0, Ljtp;->U:Lhzl;

    if-eqz v0, :cond_1

    instance-of v0, p1, Ljpv;

    if-eqz v0, :cond_1

    .line 53
    invoke-static {}, Ljsw;->a()Ljsy;

    move-result-object v0

    check-cast p1, Ljpv;

    invoke-virtual {v0, p1}, Ljsy;->a(Ljpv;)Ljsy;

    move-result-object v0

    invoke-virtual {v0}, Ljsy;->a()Ljsw;

    move-result-object v0

    .line 54
    iget-object v1, p0, Ljtp;->U:Lhzl;

    invoke-interface {v1, v0}, Lhzl;->c(Landroid/os/Parcelable;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 55
    iget-object v1, p0, Ljtp;->U:Lhzl;

    invoke-interface {v1, v0}, Lhzl;->b(Landroid/os/Parcelable;)Z

    .line 60
    :goto_0
    iget-object v0, p0, Ljtp;->V:Lllg;

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Ljtp;->V:Lllg;

    invoke-virtual {v0}, Lllg;->a()V

    .line 63
    :cond_0
    iget-object v0, p0, Ljtp;->W:Lhhx;

    if-eqz v0, :cond_1

    .line 64
    iget-object v0, p0, Ljtp;->W:Lhhx;

    invoke-virtual {v0}, Lhhx;->b()V

    .line 67
    :cond_1
    return-void

    .line 57
    :cond_2
    iget-object v1, p0, Ljtp;->U:Lhzl;

    invoke-interface {v1, v0}, Lhzl;->a(Landroid/os/Parcelable;)Z

    goto :goto_0
.end method

.method public a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 149
    return-void
.end method

.method public a(Lhzb;)V
    .locals 0

    .prologue
    .line 184
    iput-object p1, p0, Ljtp;->R:Lhzb;

    .line 185
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 127
    const/4 v0, 0x0

    return v0
.end method

.method public b()Landroid/view/View;
    .locals 1

    .prologue
    .line 154
    const/4 v0, 0x0

    return-object v0
.end method

.method public b(Landroid/content/Context;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 137
    invoke-virtual {p0, v2}, Ljtp;->a_(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040172

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public c(Landroid/content/Context;)Landroid/view/View;
    .locals 1

    .prologue
    .line 143
    const/4 v0, 0x0

    return-object v0
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 71
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 73
    iget-object v0, p0, Ljtp;->au:Llnh;

    const-class v1, Ljtj;

    iget-object v2, p0, Ljtp;->T:Ljtj;

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 74
    iget-object v0, p0, Ljtp;->au:Llnh;

    const-class v1, Lhzl;

    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhzl;

    iput-object v0, p0, Ljtp;->U:Lhzl;

    .line 75
    iget-object v0, p0, Ljtp;->au:Llnh;

    const-class v1, Lllg;

    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lllg;

    iput-object v0, p0, Ljtp;->V:Lllg;

    .line 77
    iget-object v0, p0, Ljtp;->au:Llnh;

    const-class v1, Ljtr;

    .line 78
    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljtr;

    .line 79
    invoke-virtual {v0}, Ljtr;->d()Lhzc;

    move-result-object v0

    check-cast v0, Ljtu;

    iput-object v0, p0, Ljtp;->O:Ljtu;

    .line 80
    iget-object v0, p0, Ljtp;->O:Ljtu;

    invoke-virtual {v0}, Ljtu;->size()I

    move-result v0

    iput v0, p0, Ljtp;->Q:I

    .line 81
    iget-object v0, p0, Ljtp;->au:Llnh;

    const-class v1, Lhhx;

    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhhx;

    iput-object v0, p0, Ljtp;->W:Lhhx;

    .line 82
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 132
    const/4 v0, 0x0

    return v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Ljtp;->S:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public e()V
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 197
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 199
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    move v2, v3

    .line 201
    :goto_0
    iget-object v0, p0, Ljtp;->P:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 202
    iget-object v0, p0, Ljtp;->P:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhyz;

    .line 203
    invoke-interface {v0}, Lhyz;->d()I

    move-result v7

    move v4, v3

    .line 204
    :goto_1
    if-ge v4, v7, :cond_1

    .line 205
    invoke-interface {v0, v4}, Lhyz;->a(I)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Ljpv;

    .line 206
    invoke-interface {v1}, Ljpv;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 207
    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 208
    invoke-interface {v1}, Ljpv;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 204
    :cond_0
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_1

    .line 201
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 213
    :cond_2
    iput-object v5, p0, Ljtp;->S:Ljava/util/List;

    .line 214
    iget-object v0, p0, Ljtp;->R:Lhzb;

    if-eqz v0, :cond_3

    .line 215
    iget-object v0, p0, Ljtp;->R:Lhzb;

    invoke-interface {v0}, Lhzb;->e()V

    .line 217
    :cond_3
    return-void
.end method

.method public g()V
    .locals 7

    .prologue
    .line 101
    invoke-super {p0}, Llol;->g()V

    .line 104
    iget-object v0, p0, Ljtp;->P:Ljava/util/List;

    if-nez v0, :cond_1

    .line 105
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ljtp;->P:Ljava/util/List;

    .line 106
    invoke-virtual {p0}, Ljtp;->q()Lae;

    move-result-object v0

    invoke-virtual {v0}, Lae;->f()Ljava/util/List;

    move-result-object v2

    .line 107
    if-eqz v2, :cond_1

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 108
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 109
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lu;

    .line 110
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lu;->j()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Ljtp;->N:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0xb

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 111
    check-cast v0, Lhyz;

    .line 112
    iget-object v3, p0, Ljtp;->P:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 113
    invoke-interface {v0, p0}, Lhyz;->a(Lhzb;)V

    .line 108
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 118
    :cond_1
    return-void
.end method
