.class public final Lpyu;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Lpec;

.field public f:Lpyy;

.field public g:Lpyv;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/Long;

.field private l:Lpfv;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/Boolean;

.field private q:Ljava/lang/String;

.field private r:Lpyw;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 9
    invoke-direct {p0}, Loxq;-><init>()V

    .line 20
    iput-object v0, p0, Lpyu;->l:Lpfv;

    .line 40
    iput-object v0, p0, Lpyu;->e:Lpec;

    .line 45
    iput-object v0, p0, Lpyu;->f:Lpyy;

    .line 48
    iput-object v0, p0, Lpyu;->g:Lpyv;

    .line 51
    iput-object v0, p0, Lpyu;->r:Lpyw;

    .line 9
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 116
    const/4 v0, 0x0

    .line 117
    iget-object v1, p0, Lpyu;->h:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 118
    const/4 v0, 0x1

    iget-object v1, p0, Lpyu;->h:Ljava/lang/String;

    .line 119
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 121
    :cond_0
    iget-object v1, p0, Lpyu;->i:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 122
    const/4 v1, 0x2

    iget-object v2, p0, Lpyu;->i:Ljava/lang/String;

    .line 123
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 125
    :cond_1
    iget-object v1, p0, Lpyu;->j:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 126
    const/4 v1, 0x3

    iget-object v2, p0, Lpyu;->j:Ljava/lang/String;

    .line 127
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 129
    :cond_2
    iget-object v1, p0, Lpyu;->k:Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 130
    const/4 v1, 0x4

    iget-object v2, p0, Lpyu;->k:Ljava/lang/Long;

    .line 131
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 133
    :cond_3
    iget-object v1, p0, Lpyu;->l:Lpfv;

    if-eqz v1, :cond_4

    .line 134
    const/4 v1, 0x5

    iget-object v2, p0, Lpyu;->l:Lpfv;

    .line 135
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 137
    :cond_4
    iget-object v1, p0, Lpyu;->m:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 138
    const/4 v1, 0x6

    iget-object v2, p0, Lpyu;->m:Ljava/lang/String;

    .line 139
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 141
    :cond_5
    iget-object v1, p0, Lpyu;->n:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 142
    const/4 v1, 0x7

    iget-object v2, p0, Lpyu;->n:Ljava/lang/String;

    .line 143
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 145
    :cond_6
    iget-object v1, p0, Lpyu;->o:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 146
    const/16 v1, 0x8

    iget-object v2, p0, Lpyu;->o:Ljava/lang/String;

    .line 147
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 149
    :cond_7
    iget-object v1, p0, Lpyu;->a:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 150
    const/16 v1, 0x9

    iget-object v2, p0, Lpyu;->a:Ljava/lang/String;

    .line 151
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 153
    :cond_8
    iget-object v1, p0, Lpyu;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_9

    .line 154
    const/16 v1, 0xa

    iget-object v2, p0, Lpyu;->b:Ljava/lang/Integer;

    .line 155
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 157
    :cond_9
    iget-object v1, p0, Lpyu;->p:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    .line 158
    const/16 v1, 0xb

    iget-object v2, p0, Lpyu;->p:Ljava/lang/Boolean;

    .line 159
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 161
    :cond_a
    iget-object v1, p0, Lpyu;->c:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 162
    const/16 v1, 0xc

    iget-object v2, p0, Lpyu;->c:Ljava/lang/String;

    .line 163
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 165
    :cond_b
    iget-object v1, p0, Lpyu;->d:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 166
    const/16 v1, 0xd

    iget-object v2, p0, Lpyu;->d:Ljava/lang/String;

    .line 167
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 169
    :cond_c
    iget-object v1, p0, Lpyu;->e:Lpec;

    if-eqz v1, :cond_d

    .line 170
    const/16 v1, 0xe

    iget-object v2, p0, Lpyu;->e:Lpec;

    .line 171
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 173
    :cond_d
    iget-object v1, p0, Lpyu;->q:Ljava/lang/String;

    if-eqz v1, :cond_e

    .line 174
    const/16 v1, 0xf

    iget-object v2, p0, Lpyu;->q:Ljava/lang/String;

    .line 175
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 177
    :cond_e
    iget-object v1, p0, Lpyu;->f:Lpyy;

    if-eqz v1, :cond_f

    .line 178
    const/16 v1, 0x10

    iget-object v2, p0, Lpyu;->f:Lpyy;

    .line 179
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 181
    :cond_f
    iget-object v1, p0, Lpyu;->g:Lpyv;

    if-eqz v1, :cond_10

    .line 182
    const/16 v1, 0x11

    iget-object v2, p0, Lpyu;->g:Lpyv;

    .line 183
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 185
    :cond_10
    iget-object v1, p0, Lpyu;->r:Lpyw;

    if-eqz v1, :cond_11

    .line 186
    const/16 v1, 0x12

    iget-object v2, p0, Lpyu;->r:Lpyw;

    .line 187
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 189
    :cond_11
    iget-object v1, p0, Lpyu;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 190
    iput v0, p0, Lpyu;->ai:I

    .line 191
    return v0
.end method

.method public a(Loxn;)Lpyu;
    .locals 2

    .prologue
    .line 199
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 200
    sparse-switch v0, :sswitch_data_0

    .line 204
    iget-object v1, p0, Lpyu;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 205
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpyu;->ah:Ljava/util/List;

    .line 208
    :cond_1
    iget-object v1, p0, Lpyu;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 210
    :sswitch_0
    return-object p0

    .line 215
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpyu;->h:Ljava/lang/String;

    goto :goto_0

    .line 219
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpyu;->i:Ljava/lang/String;

    goto :goto_0

    .line 223
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpyu;->j:Ljava/lang/String;

    goto :goto_0

    .line 227
    :sswitch_4
    invoke-virtual {p1}, Loxn;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lpyu;->k:Ljava/lang/Long;

    goto :goto_0

    .line 231
    :sswitch_5
    iget-object v0, p0, Lpyu;->l:Lpfv;

    if-nez v0, :cond_2

    .line 232
    new-instance v0, Lpfv;

    invoke-direct {v0}, Lpfv;-><init>()V

    iput-object v0, p0, Lpyu;->l:Lpfv;

    .line 234
    :cond_2
    iget-object v0, p0, Lpyu;->l:Lpfv;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 238
    :sswitch_6
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpyu;->m:Ljava/lang/String;

    goto :goto_0

    .line 242
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpyu;->n:Ljava/lang/String;

    goto :goto_0

    .line 246
    :sswitch_8
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpyu;->o:Ljava/lang/String;

    goto :goto_0

    .line 250
    :sswitch_9
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpyu;->a:Ljava/lang/String;

    goto :goto_0

    .line 254
    :sswitch_a
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lpyu;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 258
    :sswitch_b
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lpyu;->p:Ljava/lang/Boolean;

    goto :goto_0

    .line 262
    :sswitch_c
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpyu;->c:Ljava/lang/String;

    goto/16 :goto_0

    .line 266
    :sswitch_d
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpyu;->d:Ljava/lang/String;

    goto/16 :goto_0

    .line 270
    :sswitch_e
    iget-object v0, p0, Lpyu;->e:Lpec;

    if-nez v0, :cond_3

    .line 271
    new-instance v0, Lpec;

    invoke-direct {v0}, Lpec;-><init>()V

    iput-object v0, p0, Lpyu;->e:Lpec;

    .line 273
    :cond_3
    iget-object v0, p0, Lpyu;->e:Lpec;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 277
    :sswitch_f
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpyu;->q:Ljava/lang/String;

    goto/16 :goto_0

    .line 281
    :sswitch_10
    iget-object v0, p0, Lpyu;->f:Lpyy;

    if-nez v0, :cond_4

    .line 282
    new-instance v0, Lpyy;

    invoke-direct {v0}, Lpyy;-><init>()V

    iput-object v0, p0, Lpyu;->f:Lpyy;

    .line 284
    :cond_4
    iget-object v0, p0, Lpyu;->f:Lpyy;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 288
    :sswitch_11
    iget-object v0, p0, Lpyu;->g:Lpyv;

    if-nez v0, :cond_5

    .line 289
    new-instance v0, Lpyv;

    invoke-direct {v0}, Lpyv;-><init>()V

    iput-object v0, p0, Lpyu;->g:Lpyv;

    .line 291
    :cond_5
    iget-object v0, p0, Lpyu;->g:Lpyv;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 295
    :sswitch_12
    iget-object v0, p0, Lpyu;->r:Lpyw;

    if-nez v0, :cond_6

    .line 296
    new-instance v0, Lpyw;

    invoke-direct {v0}, Lpyw;-><init>()V

    iput-object v0, p0, Lpyu;->r:Lpyw;

    .line 298
    :cond_6
    iget-object v0, p0, Lpyu;->r:Lpyw;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 200
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
        0x82 -> :sswitch_10
        0x8a -> :sswitch_11
        0x92 -> :sswitch_12
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 56
    iget-object v0, p0, Lpyu;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 57
    const/4 v0, 0x1

    iget-object v1, p0, Lpyu;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 59
    :cond_0
    iget-object v0, p0, Lpyu;->i:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 60
    const/4 v0, 0x2

    iget-object v1, p0, Lpyu;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 62
    :cond_1
    iget-object v0, p0, Lpyu;->j:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 63
    const/4 v0, 0x3

    iget-object v1, p0, Lpyu;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 65
    :cond_2
    iget-object v0, p0, Lpyu;->k:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 66
    const/4 v0, 0x4

    iget-object v1, p0, Lpyu;->k:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(IJ)V

    .line 68
    :cond_3
    iget-object v0, p0, Lpyu;->l:Lpfv;

    if-eqz v0, :cond_4

    .line 69
    const/4 v0, 0x5

    iget-object v1, p0, Lpyu;->l:Lpfv;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 71
    :cond_4
    iget-object v0, p0, Lpyu;->m:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 72
    const/4 v0, 0x6

    iget-object v1, p0, Lpyu;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 74
    :cond_5
    iget-object v0, p0, Lpyu;->n:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 75
    const/4 v0, 0x7

    iget-object v1, p0, Lpyu;->n:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 77
    :cond_6
    iget-object v0, p0, Lpyu;->o:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 78
    const/16 v0, 0x8

    iget-object v1, p0, Lpyu;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 80
    :cond_7
    iget-object v0, p0, Lpyu;->a:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 81
    const/16 v0, 0x9

    iget-object v1, p0, Lpyu;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 83
    :cond_8
    iget-object v0, p0, Lpyu;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_9

    .line 84
    const/16 v0, 0xa

    iget-object v1, p0, Lpyu;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 86
    :cond_9
    iget-object v0, p0, Lpyu;->p:Ljava/lang/Boolean;

    if-eqz v0, :cond_a

    .line 87
    const/16 v0, 0xb

    iget-object v1, p0, Lpyu;->p:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 89
    :cond_a
    iget-object v0, p0, Lpyu;->c:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 90
    const/16 v0, 0xc

    iget-object v1, p0, Lpyu;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 92
    :cond_b
    iget-object v0, p0, Lpyu;->d:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 93
    const/16 v0, 0xd

    iget-object v1, p0, Lpyu;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 95
    :cond_c
    iget-object v0, p0, Lpyu;->e:Lpec;

    if-eqz v0, :cond_d

    .line 96
    const/16 v0, 0xe

    iget-object v1, p0, Lpyu;->e:Lpec;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 98
    :cond_d
    iget-object v0, p0, Lpyu;->q:Ljava/lang/String;

    if-eqz v0, :cond_e

    .line 99
    const/16 v0, 0xf

    iget-object v1, p0, Lpyu;->q:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 101
    :cond_e
    iget-object v0, p0, Lpyu;->f:Lpyy;

    if-eqz v0, :cond_f

    .line 102
    const/16 v0, 0x10

    iget-object v1, p0, Lpyu;->f:Lpyy;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 104
    :cond_f
    iget-object v0, p0, Lpyu;->g:Lpyv;

    if-eqz v0, :cond_10

    .line 105
    const/16 v0, 0x11

    iget-object v1, p0, Lpyu;->g:Lpyv;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 107
    :cond_10
    iget-object v0, p0, Lpyu;->r:Lpyw;

    if-eqz v0, :cond_11

    .line 108
    const/16 v0, 0x12

    iget-object v1, p0, Lpyu;->r:Lpyw;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 110
    :cond_11
    iget-object v0, p0, Lpyu;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 112
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Lpyu;->a(Loxn;)Lpyu;

    move-result-object v0

    return-object v0
.end method
