.class public final Ldoi;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmla;",
        "Lmlb;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:[I


# instance fields
.field private final b:Ljava/lang/String;

.field private final p:Ljava/lang/String;

.field private final q:Z

.field private final r:Lhei;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Ldoi;->a:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x1
        0x8
        0x3
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)V
    .locals 6

    .prologue
    .line 41
    const-string v3, "userphotoalbums"

    new-instance v4, Lmla;

    invoke-direct {v4}, Lmla;-><init>()V

    new-instance v5, Lmlb;

    invoke-direct {v5}, Lmlb;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 44
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Must specify a valid owner ID"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 47
    :cond_0
    iput-object p3, p0, Ldoi;->b:Ljava/lang/String;

    .line 48
    iput-boolean p5, p0, Ldoi;->q:Z

    .line 49
    iput-object p4, p0, Ldoi;->p:Ljava/lang/String;

    .line 50
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Ldoi;->r:Lhei;

    .line 51
    return-void
.end method


# virtual methods
.method protected a(Lmla;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 55
    iget-object v0, p0, Ldoi;->r:Lhei;

    iget v2, p0, Ldoi;->c:I

    .line 56
    invoke-interface {v0, v2}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v2, "gaia_id"

    .line 57
    invoke-interface {v0, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 59
    new-instance v0, Lney;

    invoke-direct {v0}, Lney;-><init>()V

    iput-object v0, p1, Lmla;->a:Lney;

    .line 60
    iget-object v3, p1, Lmla;->a:Lney;

    .line 61
    iget-object v0, p0, Ldoi;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldoi;->b:Ljava/lang/String;

    .line 62
    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v3, Lney;->b:Ljava/lang/Boolean;

    .line 63
    iget-object v0, p0, Ldoi;->b:Ljava/lang/String;

    if-nez v0, :cond_2

    move-object v0, v2

    :goto_1
    iput-object v0, v3, Lney;->a:Ljava/lang/String;

    .line 64
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v3, Lney;->d:Ljava/lang/Integer;

    .line 65
    const/16 v0, 0x32

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v3, Lney;->e:Ljava/lang/Integer;

    .line 68
    sget-object v0, Ldoi;->a:[I

    iput-object v0, v3, Lney;->c:[I

    .line 70
    const/4 v0, 0x2

    iput v0, v3, Lney;->f:I

    .line 71
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v3, Lney;->h:Ljava/lang/Boolean;

    .line 72
    iget-object v0, p0, Ldoi;->p:Ljava/lang/String;

    iput-object v0, v3, Lney;->g:Ljava/lang/String;

    .line 73
    return-void

    .line 62
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 63
    :cond_2
    iget-object v0, p0, Ldoi;->b:Ljava/lang/String;

    goto :goto_1
.end method

.method protected a(Lmlb;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 77
    iget-object v3, p1, Lmlb;->a:Lnfo;

    .line 79
    const/4 v0, 0x2

    new-array v1, v7, [Ljava/lang/String;

    iget-object v2, p0, Ldoi;->b:Ljava/lang/String;

    aput-object v2, v1, v5

    invoke-static {v0, v1}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 81
    iget-object v0, p0, Ldoi;->f:Landroid/content/Context;

    iget v1, p0, Ldoi;->c:I

    iget-object v4, v3, Lnfo;->a:Ljava/lang/String;

    iget-boolean v6, p0, Ldoi;->q:Z

    invoke-static {v0, v1, v2, v4, v6}, Ljvj;->b(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)V

    .line 83
    iget-object v0, p0, Ldoi;->f:Landroid/content/Context;

    iget v1, p0, Ldoi;->c:I

    iget-object v3, v3, Lnfo;->b:[Lnzx;

    iget-boolean v4, p0, Ldoi;->q:Z

    const/4 v6, 0x0

    invoke-static/range {v0 .. v7}, Ljvj;->a(Landroid/content/Context;ILjava/lang/String;[Lnzx;ZZLjava/lang/String;Z)V

    .line 85
    return-void
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 21
    check-cast p1, Lmla;

    invoke-virtual {p0, p1}, Ldoi;->a(Lmla;)V

    return-void
.end method

.method protected synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 21
    check-cast p1, Lmlb;

    invoke-virtual {p0, p1}, Ldoi;->a(Lmlb;)V

    return-void
.end method
