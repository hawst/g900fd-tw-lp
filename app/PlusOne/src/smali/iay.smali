.class public final Liay;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llnx;
.implements Llql;
.implements Llrg;


# instance fields
.field private final a:I

.field private b:Liax;


# direct methods
.method public constructor <init>(Llqr;I)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput p2, p0, Liay;->a:I

    .line 28
    invoke-virtual {p1, p0}, Llqr;->a(Llrg;)Llrg;

    .line 29
    return-void
.end method

.method static synthetic a(Liay;Landroid/view/ViewGroup;)V
    .locals 4

    .prologue
    .line 20
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    instance-of v3, v0, Libe;

    if-eqz v3, :cond_0

    iget-object v3, p0, Liay;->b:Liax;

    check-cast v0, Libe;

    invoke-interface {v3, v0, p1}, Liax;->a(Libe;Landroid/view/View;)V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Llnh;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 33
    const-class v0, Liax;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liax;

    iput-object v0, p0, Liay;->b:Liax;

    .line 34
    return-void
.end method

.method public a(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 38
    iget v0, p0, Liay;->a:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 39
    instance-of v1, v0, Landroid/widget/AbsListView;

    if-eqz v1, :cond_1

    .line 40
    check-cast v0, Landroid/widget/AbsListView;

    new-instance v1, Liaz;

    invoke-direct {v1, p0}, Liaz;-><init>(Liay;)V

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 44
    :cond_0
    :goto_0
    return-void

    .line 41
    :cond_1
    instance-of v1, v0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    if-eqz v1, :cond_0

    .line 42
    check-cast v0, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    new-instance v1, Liba;

    invoke-direct {v1, p0}, Liba;-><init>(Liay;)V

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->a(Lljj;)V

    goto :goto_0
.end method
