.class public final Lmjj;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lnfw;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 26404
    invoke-direct {p0}, Loxq;-><init>()V

    .line 26407
    iput-object v0, p0, Lmjj;->apiHeader:Llyr;

    .line 26410
    iput-object v0, p0, Lmjj;->a:Lnfw;

    .line 26404
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 26427
    const/4 v0, 0x0

    .line 26428
    iget-object v1, p0, Lmjj;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 26429
    const/4 v0, 0x1

    iget-object v1, p0, Lmjj;->apiHeader:Llyr;

    .line 26430
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 26432
    :cond_0
    iget-object v1, p0, Lmjj;->a:Lnfw;

    if-eqz v1, :cond_1

    .line 26433
    const/4 v1, 0x2

    iget-object v2, p0, Lmjj;->a:Lnfw;

    .line 26434
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 26436
    :cond_1
    iget-object v1, p0, Lmjj;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 26437
    iput v0, p0, Lmjj;->ai:I

    .line 26438
    return v0
.end method

.method public a(Loxn;)Lmjj;
    .locals 2

    .prologue
    .line 26446
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 26447
    sparse-switch v0, :sswitch_data_0

    .line 26451
    iget-object v1, p0, Lmjj;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 26452
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmjj;->ah:Ljava/util/List;

    .line 26455
    :cond_1
    iget-object v1, p0, Lmjj;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 26457
    :sswitch_0
    return-object p0

    .line 26462
    :sswitch_1
    iget-object v0, p0, Lmjj;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 26463
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmjj;->apiHeader:Llyr;

    .line 26465
    :cond_2
    iget-object v0, p0, Lmjj;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 26469
    :sswitch_2
    iget-object v0, p0, Lmjj;->a:Lnfw;

    if-nez v0, :cond_3

    .line 26470
    new-instance v0, Lnfw;

    invoke-direct {v0}, Lnfw;-><init>()V

    iput-object v0, p0, Lmjj;->a:Lnfw;

    .line 26472
    :cond_3
    iget-object v0, p0, Lmjj;->a:Lnfw;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 26447
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 26415
    iget-object v0, p0, Lmjj;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 26416
    const/4 v0, 0x1

    iget-object v1, p0, Lmjj;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 26418
    :cond_0
    iget-object v0, p0, Lmjj;->a:Lnfw;

    if-eqz v0, :cond_1

    .line 26419
    const/4 v0, 0x2

    iget-object v1, p0, Lmjj;->a:Lnfw;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 26421
    :cond_1
    iget-object v0, p0, Lmjj;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 26423
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 26400
    invoke-virtual {p0, p1}, Lmjj;->a(Loxn;)Lmjj;

    move-result-object v0

    return-object v0
.end method
