.class public final Lmcx;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmoy;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 30616
    invoke-direct {p0}, Loxq;-><init>()V

    .line 30619
    iput-object v0, p0, Lmcx;->apiHeader:Llyr;

    .line 30622
    iput-object v0, p0, Lmcx;->a:Lmoy;

    .line 30616
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 30639
    const/4 v0, 0x0

    .line 30640
    iget-object v1, p0, Lmcx;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 30641
    const/4 v0, 0x1

    iget-object v1, p0, Lmcx;->apiHeader:Llyr;

    .line 30642
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 30644
    :cond_0
    iget-object v1, p0, Lmcx;->a:Lmoy;

    if-eqz v1, :cond_1

    .line 30645
    const/4 v1, 0x2

    iget-object v2, p0, Lmcx;->a:Lmoy;

    .line 30646
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 30648
    :cond_1
    iget-object v1, p0, Lmcx;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 30649
    iput v0, p0, Lmcx;->ai:I

    .line 30650
    return v0
.end method

.method public a(Loxn;)Lmcx;
    .locals 2

    .prologue
    .line 30658
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 30659
    sparse-switch v0, :sswitch_data_0

    .line 30663
    iget-object v1, p0, Lmcx;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 30664
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmcx;->ah:Ljava/util/List;

    .line 30667
    :cond_1
    iget-object v1, p0, Lmcx;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 30669
    :sswitch_0
    return-object p0

    .line 30674
    :sswitch_1
    iget-object v0, p0, Lmcx;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 30675
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmcx;->apiHeader:Llyr;

    .line 30677
    :cond_2
    iget-object v0, p0, Lmcx;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 30681
    :sswitch_2
    iget-object v0, p0, Lmcx;->a:Lmoy;

    if-nez v0, :cond_3

    .line 30682
    new-instance v0, Lmoy;

    invoke-direct {v0}, Lmoy;-><init>()V

    iput-object v0, p0, Lmcx;->a:Lmoy;

    .line 30684
    :cond_3
    iget-object v0, p0, Lmcx;->a:Lmoy;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 30659
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 30627
    iget-object v0, p0, Lmcx;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 30628
    const/4 v0, 0x1

    iget-object v1, p0, Lmcx;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 30630
    :cond_0
    iget-object v0, p0, Lmcx;->a:Lmoy;

    if-eqz v0, :cond_1

    .line 30631
    const/4 v0, 0x2

    iget-object v1, p0, Lmcx;->a:Lmoy;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 30633
    :cond_1
    iget-object v0, p0, Lmcx;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 30635
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 30612
    invoke-virtual {p0, p1}, Lmcx;->a(Loxn;)Lmcx;

    move-result-object v0

    return-object v0
.end method
