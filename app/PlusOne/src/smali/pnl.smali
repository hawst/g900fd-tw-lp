.class public final Lpnl;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lpnl;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 701
    const/4 v0, 0x0

    new-array v0, v0, [Lpnl;

    sput-object v0, Lpnl;->a:[Lpnl;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 702
    invoke-direct {p0}, Loxq;-><init>()V

    .line 715
    const/high16 v0, -0x80000000

    iput v0, p0, Lpnl;->d:I

    .line 702
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 735
    const/4 v0, 0x0

    .line 736
    iget-object v1, p0, Lpnl;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 737
    const/4 v0, 0x1

    iget-object v1, p0, Lpnl;->b:Ljava/lang/String;

    .line 738
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 740
    :cond_0
    iget-object v1, p0, Lpnl;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 741
    const/4 v1, 0x2

    iget-object v2, p0, Lpnl;->c:Ljava/lang/String;

    .line 742
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 744
    :cond_1
    iget v1, p0, Lpnl;->d:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_2

    .line 745
    const/4 v1, 0x3

    iget v2, p0, Lpnl;->d:I

    .line 746
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 748
    :cond_2
    iget-object v1, p0, Lpnl;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 749
    iput v0, p0, Lpnl;->ai:I

    .line 750
    return v0
.end method

.method public a(Loxn;)Lpnl;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 758
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 759
    sparse-switch v0, :sswitch_data_0

    .line 763
    iget-object v1, p0, Lpnl;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 764
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpnl;->ah:Ljava/util/List;

    .line 767
    :cond_1
    iget-object v1, p0, Lpnl;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 769
    :sswitch_0
    return-object p0

    .line 774
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpnl;->b:Ljava/lang/String;

    goto :goto_0

    .line 778
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpnl;->c:Ljava/lang/String;

    goto :goto_0

    .line 782
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 783
    if-eq v0, v2, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 786
    :cond_2
    iput v0, p0, Lpnl;->d:I

    goto :goto_0

    .line 788
    :cond_3
    iput v2, p0, Lpnl;->d:I

    goto :goto_0

    .line 759
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 720
    iget-object v0, p0, Lpnl;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 721
    const/4 v0, 0x1

    iget-object v1, p0, Lpnl;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 723
    :cond_0
    iget-object v0, p0, Lpnl;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 724
    const/4 v0, 0x2

    iget-object v1, p0, Lpnl;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 726
    :cond_1
    iget v0, p0, Lpnl;->d:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_2

    .line 727
    const/4 v0, 0x3

    iget v1, p0, Lpnl;->d:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 729
    :cond_2
    iget-object v0, p0, Lpnl;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 731
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 698
    invoke-virtual {p0, p1}, Lpnl;->a(Loxn;)Lpnl;

    move-result-object v0

    return-object v0
.end method
