.class public final Lomf;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lomf;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:[I

.field private e:I

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Lomh;

.field private i:Lolw;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Lomf;

    sput-object v0, Lomf;->a:[Lomf;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 57
    sget-object v0, Loxx;->a:[I

    iput-object v0, p0, Lomf;->d:[I

    .line 60
    const/high16 v0, -0x80000000

    iput v0, p0, Lomf;->e:I

    .line 67
    iput-object v1, p0, Lomf;->h:Lomh;

    .line 70
    iput-object v1, p0, Lomf;->i:Lolw;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 105
    const/4 v0, 0x1

    iget-object v2, p0, Lomf;->b:Ljava/lang/String;

    .line 107
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 108
    iget-object v2, p0, Lomf;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 109
    const/4 v2, 0x2

    iget-object v3, p0, Lomf;->c:Ljava/lang/String;

    .line 110
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 112
    :cond_0
    iget-object v2, p0, Lomf;->d:[I

    if-eqz v2, :cond_2

    iget-object v2, p0, Lomf;->d:[I

    array-length v2, v2

    if-lez v2, :cond_2

    .line 114
    iget-object v3, p0, Lomf;->d:[I

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v1, v4, :cond_1

    aget v5, v3, v1

    .line 116
    invoke-static {v5}, Loxo;->i(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 114
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 118
    :cond_1
    add-int/2addr v0, v2

    .line 119
    iget-object v1, p0, Lomf;->d:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 121
    :cond_2
    iget v1, p0, Lomf;->e:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_3

    .line 122
    const/4 v1, 0x4

    iget v2, p0, Lomf;->e:I

    .line 123
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 125
    :cond_3
    iget-object v1, p0, Lomf;->f:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 126
    const/4 v1, 0x5

    iget-object v2, p0, Lomf;->f:Ljava/lang/String;

    .line 127
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 129
    :cond_4
    iget-object v1, p0, Lomf;->g:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 130
    const/4 v1, 0x6

    iget-object v2, p0, Lomf;->g:Ljava/lang/String;

    .line 131
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 133
    :cond_5
    iget-object v1, p0, Lomf;->h:Lomh;

    if-eqz v1, :cond_6

    .line 134
    const/16 v1, 0x64

    iget-object v2, p0, Lomf;->h:Lomh;

    .line 135
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 137
    :cond_6
    iget-object v1, p0, Lomf;->i:Lolw;

    if-eqz v1, :cond_7

    .line 138
    const/16 v1, 0x1f4

    iget-object v2, p0, Lomf;->i:Lolw;

    .line 139
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 141
    :cond_7
    iget-object v1, p0, Lomf;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 142
    iput v0, p0, Lomf;->ai:I

    .line 143
    return v0
.end method

.method public a(Loxn;)Lomf;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 151
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 152
    sparse-switch v0, :sswitch_data_0

    .line 156
    iget-object v1, p0, Lomf;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 157
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lomf;->ah:Ljava/util/List;

    .line 160
    :cond_1
    iget-object v1, p0, Lomf;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 162
    :sswitch_0
    return-object p0

    .line 167
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lomf;->b:Ljava/lang/String;

    goto :goto_0

    .line 171
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lomf;->c:Ljava/lang/String;

    goto :goto_0

    .line 175
    :sswitch_3
    const/16 v0, 0x18

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 176
    iget-object v0, p0, Lomf;->d:[I

    array-length v0, v0

    .line 177
    add-int/2addr v1, v0

    new-array v1, v1, [I

    .line 178
    iget-object v2, p0, Lomf;->d:[I

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 179
    iput-object v1, p0, Lomf;->d:[I

    .line 180
    :goto_1
    iget-object v1, p0, Lomf;->d:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 181
    iget-object v1, p0, Lomf;->d:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    .line 182
    invoke-virtual {p1}, Loxn;->a()I

    .line 180
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 185
    :cond_2
    iget-object v1, p0, Lomf;->d:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    goto :goto_0

    .line 189
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 190
    if-eqz v0, :cond_3

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    const/4 v1, 0x4

    if-eq v0, v1, :cond_3

    const/4 v1, 0x5

    if-eq v0, v1, :cond_3

    const/4 v1, 0x6

    if-eq v0, v1, :cond_3

    const/4 v1, 0x7

    if-eq v0, v1, :cond_3

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    .line 199
    :cond_3
    iput v0, p0, Lomf;->e:I

    goto :goto_0

    .line 201
    :cond_4
    iput v3, p0, Lomf;->e:I

    goto :goto_0

    .line 206
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lomf;->f:Ljava/lang/String;

    goto/16 :goto_0

    .line 210
    :sswitch_6
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lomf;->g:Ljava/lang/String;

    goto/16 :goto_0

    .line 214
    :sswitch_7
    iget-object v0, p0, Lomf;->h:Lomh;

    if-nez v0, :cond_5

    .line 215
    new-instance v0, Lomh;

    invoke-direct {v0}, Lomh;-><init>()V

    iput-object v0, p0, Lomf;->h:Lomh;

    .line 217
    :cond_5
    iget-object v0, p0, Lomf;->h:Lomh;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 221
    :sswitch_8
    iget-object v0, p0, Lomf;->i:Lolw;

    if-nez v0, :cond_6

    .line 222
    new-instance v0, Lolw;

    invoke-direct {v0}, Lolw;-><init>()V

    iput-object v0, p0, Lomf;->i:Lolw;

    .line 224
    :cond_6
    iget-object v0, p0, Lomf;->i:Lolw;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 152
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x322 -> :sswitch_7
        0xfa2 -> :sswitch_8
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 75
    const/4 v0, 0x1

    iget-object v1, p0, Lomf;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 76
    iget-object v0, p0, Lomf;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 77
    const/4 v0, 0x2

    iget-object v1, p0, Lomf;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 79
    :cond_0
    iget-object v0, p0, Lomf;->d:[I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lomf;->d:[I

    array-length v0, v0

    if-lez v0, :cond_1

    .line 80
    iget-object v1, p0, Lomf;->d:[I

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget v3, v1, v0

    .line 81
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->a(II)V

    .line 80
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 84
    :cond_1
    iget v0, p0, Lomf;->e:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_2

    .line 85
    const/4 v0, 0x4

    iget v1, p0, Lomf;->e:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 87
    :cond_2
    iget-object v0, p0, Lomf;->f:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 88
    const/4 v0, 0x5

    iget-object v1, p0, Lomf;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 90
    :cond_3
    iget-object v0, p0, Lomf;->g:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 91
    const/4 v0, 0x6

    iget-object v1, p0, Lomf;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 93
    :cond_4
    iget-object v0, p0, Lomf;->h:Lomh;

    if-eqz v0, :cond_5

    .line 94
    const/16 v0, 0x64

    iget-object v1, p0, Lomf;->h:Lomh;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 96
    :cond_5
    iget-object v0, p0, Lomf;->i:Lolw;

    if-eqz v0, :cond_6

    .line 97
    const/16 v0, 0x1f4

    iget-object v1, p0, Lomf;->i:Lolw;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 99
    :cond_6
    iget-object v0, p0, Lomf;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 101
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lomf;->a(Loxn;)Lomf;

    move-result-object v0

    return-object v0
.end method
