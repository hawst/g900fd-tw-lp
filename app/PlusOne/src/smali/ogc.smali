.class public final Logc;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Integer;

.field private b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5449
    invoke-direct {p0}, Loxq;-><init>()V

    .line 5464
    const/high16 v0, -0x80000000

    iput v0, p0, Logc;->b:I

    .line 5449
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 5481
    const/4 v0, 0x0

    .line 5482
    iget-object v1, p0, Logc;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 5483
    const/4 v0, 0x1

    iget-object v1, p0, Logc;->a:Ljava/lang/Integer;

    .line 5484
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 5486
    :cond_0
    iget v1, p0, Logc;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_1

    .line 5487
    const/4 v1, 0x2

    iget v2, p0, Logc;->b:I

    .line 5488
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5490
    :cond_1
    iget-object v1, p0, Logc;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5491
    iput v0, p0, Logc;->ai:I

    .line 5492
    return v0
.end method

.method public a(Loxn;)Logc;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 5500
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 5501
    sparse-switch v0, :sswitch_data_0

    .line 5505
    iget-object v1, p0, Logc;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 5506
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Logc;->ah:Ljava/util/List;

    .line 5509
    :cond_1
    iget-object v1, p0, Logc;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5511
    :sswitch_0
    return-object p0

    .line 5516
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Logc;->a:Ljava/lang/Integer;

    goto :goto_0

    .line 5520
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 5521
    if-eq v0, v2, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-ne v0, v1, :cond_3

    .line 5528
    :cond_2
    iput v0, p0, Logc;->b:I

    goto :goto_0

    .line 5530
    :cond_3
    iput v2, p0, Logc;->b:I

    goto :goto_0

    .line 5501
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 5469
    iget-object v0, p0, Logc;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 5470
    const/4 v0, 0x1

    iget-object v1, p0, Logc;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 5472
    :cond_0
    iget v0, p0, Logc;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 5473
    const/4 v0, 0x2

    iget v1, p0, Logc;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 5475
    :cond_1
    iget-object v0, p0, Logc;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 5477
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5445
    invoke-virtual {p0, p1}, Logc;->a(Loxn;)Logc;

    move-result-object v0

    return-object v0
.end method
