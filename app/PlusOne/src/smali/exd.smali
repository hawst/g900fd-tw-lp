.class final Lexd;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lheg;
.implements Lhob;
.implements Ligy;
.implements Llnx;
.implements Llqz;
.implements Llrd;
.implements Llrg;


# instance fields
.field private a:Landroid/app/Activity;

.field private b:Ligv;

.field private c:Livx;

.field private d:Lexl;

.field private e:Lhoc;

.field private f:I


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 337
    return-void
.end method

.method private c(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 154
    iget v1, p0, Lexd;->f:I

    iget-object v2, p0, Lexd;->d:Lexl;

    invoke-virtual {v2}, Lexl;->c()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string v2, "profile_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v3, p0, Lexd;->d:Lexl;

    invoke-virtual {v3, v2}, Lexl;->a(Ljava/lang/String;)V

    :cond_0
    iget-object v2, p0, Lexd;->d:Lexl;

    invoke-virtual {v2}, Lexl;->b()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    const-string v2, "activity_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    .line 155
    :goto_0
    if-eqz v0, :cond_4

    .line 156
    iget-object v1, p0, Lexd;->b:Ligv;

    invoke-interface {v1, v0}, Ligv;->a(Landroid/content/Intent;)V

    .line 160
    :goto_1
    return-void

    .line 154
    :cond_1
    iget-object v2, p0, Lexd;->d:Lexl;

    invoke-virtual {v2}, Lexl;->c()Ljava/lang/String;

    move-result-object v2

    const-string v3, "+"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lexd;->d:Lexl;

    invoke-virtual {v0, v2}, Lexl;->b(Ljava/lang/String;)V

    :cond_3
    iget-object v0, p0, Lexd;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iget-object v2, p0, Lexd;->d:Lexl;

    iget-object v3, p0, Lexd;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v1, v0, v4}, Lexl;->a(Landroid/content/Context;ILandroid/os/Bundle;Z)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "from_url_gateway"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_0

    .line 158
    :cond_4
    iget-object v0, p0, Lexd;->b:Ligv;

    invoke-interface {v0}, Ligv;->a()V

    goto :goto_1
.end method


# virtual methods
.method public a()V
    .locals 6

    .prologue
    .line 105
    iget-object v0, p0, Lexd;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 106
    iget-object v1, p0, Lexd;->d:Lexl;

    iget-object v2, p0, Lexd;->a:Landroid/app/Activity;

    invoke-virtual {v1, v2}, Lexl;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 107
    iget-object v2, p0, Lexd;->c:Livx;

    new-instance v3, Liwg;

    invoke-direct {v3}, Liwg;-><init>()V

    .line 108
    invoke-virtual {v3}, Liwg;->d()Liwg;

    move-result-object v3

    const-string v4, "account_id"

    const/4 v5, -0x1

    .line 109
    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {v3, v0}, Liwg;->a(I)Liwg;

    move-result-object v0

    const-class v3, Liwl;

    new-instance v4, Liwm;

    invoke-direct {v4}, Liwm;-><init>()V

    .line 111
    invoke-virtual {v4, v1}, Liwm;->a(Ljava/lang/String;)Liwm;

    move-result-object v1

    .line 112
    invoke-virtual {v1}, Liwm;->a()Landroid/os/Bundle;

    move-result-object v1

    .line 110
    invoke-virtual {v0, v3, v1}, Liwg;->a(Ljava/lang/Class;Landroid/os/Bundle;)Liwg;

    move-result-object v0

    .line 107
    invoke-virtual {v2, v0}, Livx;->a(Liwg;)V

    .line 115
    return-void
.end method

.method public a(Landroid/app/Activity;Llqr;Ligv;Livx;)V
    .locals 1

    .prologue
    .line 75
    iput-object p1, p0, Lexd;->a:Landroid/app/Activity;

    .line 76
    iput-object p3, p0, Lexd;->b:Ligv;

    .line 77
    invoke-virtual {p4, p0}, Livx;->b(Lheg;)Livx;

    move-result-object v0

    iput-object v0, p0, Lexd;->c:Livx;

    .line 78
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 79
    return-void
.end method

.method public a(Landroid/content/Context;Llnh;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 99
    const-class v0, Lhoc;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    iput-object v0, p0, Lexd;->e:Lhoc;

    .line 100
    iget-object v0, p0, Lexd;->e:Lhoc;

    invoke-virtual {v0, p0}, Lhoc;->a(Lhob;)Lhoc;

    .line 101
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 83
    if-eqz p1, :cond_0

    .line 84
    const-string v0, "account_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lexd;->f:I

    .line 85
    const-string v0, "url_parser"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lexl;

    iput-object v0, p0, Lexd;->d:Lexl;

    .line 89
    :goto_0
    return-void

    .line 87
    :cond_0
    iget-object v0, p0, Lexd;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lexc;->b(Landroid/content/Intent;)Lexl;

    move-result-object v0

    iput-object v0, p0, Lexd;->d:Lexl;

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 143
    const-string v0, "resolve_task"

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 144
    invoke-virtual {p2}, Lhoz;->f()Z

    move-result v0

    if-nez v0, :cond_1

    .line 145
    invoke-virtual {p2}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {p0, v0}, Lexd;->c(Landroid/os/Bundle;)V

    .line 151
    :cond_0
    :goto_0
    return-void

    .line 147
    :cond_1
    invoke-virtual {p2}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "error"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 148
    iget-object v1, p0, Lexd;->b:Ligv;

    iget-object v2, p0, Lexd;->a:Landroid/app/Activity;

    invoke-virtual {v2, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v3, v0}, Ligv;->a(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public a(ZIIII)V
    .locals 6

    .prologue
    .line 122
    const/4 v0, -0x1

    if-eq p5, v0, :cond_1

    .line 123
    iput p5, p0, Lexd;->f:I

    .line 125
    iget-object v0, p0, Lexd;->d:Lexl;

    invoke-virtual {v0}, Lexl;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lexd;->e:Lhoc;

    new-instance v1, Lexh;

    iget-object v2, p0, Lexd;->a:Landroid/app/Activity;

    const-string v3, "resolve_task"

    iget-object v4, p0, Lexd;->d:Lexl;

    iget v5, p0, Lexd;->f:I

    invoke-direct {v1, v2, v3, v4, v5}, Lexh;-><init>(Landroid/content/Context;Ljava/lang/String;Lexl;I)V

    invoke-virtual {v0, v1}, Lhoc;->c(Lhny;)V

    .line 133
    :goto_0
    return-void

    .line 128
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-direct {p0, v0}, Lexd;->c(Landroid/os/Bundle;)V

    goto :goto_0

    .line 131
    :cond_1
    iget-object v0, p0, Lexd;->b:Ligv;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ligv;->a(I)V

    goto :goto_0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 93
    const-string v0, "account_id"

    iget v1, p0, Lexd;->f:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 94
    const-string v0, "url_parser"

    iget-object v1, p0, Lexd;->d:Lexl;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 95
    return-void
.end method
