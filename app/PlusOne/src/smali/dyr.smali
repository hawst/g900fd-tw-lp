.class public final Ldyr;
.super Ljow;
.source "PG"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 43
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Ldyr;-><init>(ZLandroid/os/Parcelable;)V

    .line 44
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 57
    invoke-direct {p0}, Ljow;-><init>()V

    .line 58
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 59
    const-string v1, "entity_name"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    const-string v1, "callback_data"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 61
    invoke-virtual {p0, v0}, Ldyr;->f(Landroid/os/Bundle;)V

    .line 62
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ldyr;-><init>(ZLandroid/os/Parcelable;)V

    .line 48
    return-void
.end method

.method public constructor <init>(ZLandroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 50
    invoke-direct {p0}, Ljow;-><init>()V

    .line 51
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 52
    const-string v1, "plus_page"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 53
    const-string v1, "callback_data"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 54
    invoke-virtual {p0, v0}, Ldyr;->f(Landroid/os/Bundle;)V

    .line 55
    return-void
.end method


# virtual methods
.method protected U()V
    .locals 2

    .prologue
    .line 98
    invoke-virtual {p0}, Ldyr;->n()Lz;

    move-result-object v0

    const v1, 0x7f0a0855

    invoke-virtual {v0, v1}, Lz;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "plusone_promo_block"

    invoke-virtual {p0, v0, v1}, Ldyr;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    return-void
.end method

.method public c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 66
    invoke-virtual {p0}, Ldyr;->k()Landroid/os/Bundle;

    move-result-object v0

    .line 67
    const-string v1, "plus_page"

    invoke-virtual {v0, v1, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 68
    const-string v2, "entity_name"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 70
    invoke-virtual {p0}, Ldyr;->n()Lz;

    move-result-object v3

    .line 71
    new-instance v4, Landroid/app/AlertDialog$Builder;

    invoke-direct {v4, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 72
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 73
    if-eqz v1, :cond_0

    const v0, 0x7f0a0848

    :goto_0
    invoke-virtual {v4, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 79
    :goto_1
    const v0, 0x104000a

    invoke-virtual {v4, v0, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 80
    const/high16 v0, 0x1040000

    invoke-virtual {v4, v0, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 81
    invoke-virtual {v4, v7}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 82
    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v3, 0x7f04007e

    const/4 v5, 0x0

    invoke-virtual {v0, v3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 84
    const v0, 0x7f100139

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 85
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 86
    if-eqz v1, :cond_2

    const v1, 0x7f0a084b

    :goto_2
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 91
    :goto_3
    invoke-virtual {p0}, Ldyr;->U()V

    .line 92
    invoke-virtual {v4, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 93
    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    .line 73
    :cond_0
    const v0, 0x7f0a0847

    goto :goto_0

    .line 76
    :cond_1
    const v0, 0x7f0a0849

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v2, v5, v6

    invoke-virtual {p0, v0, v5}, Ldyr;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    goto :goto_1

    .line 86
    :cond_2
    const v1, 0x7f0a084a

    goto :goto_2

    .line 89
    :cond_3
    const v1, 0x7f0a084c

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v2, v5, v6

    invoke-virtual {p0, v1, v5}, Ldyr;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 104
    invoke-virtual {p0}, Ldyr;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "callback_data"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 105
    packed-switch p2, :pswitch_data_0

    .line 119
    :cond_0
    :goto_0
    return-void

    .line 108
    :pswitch_0
    invoke-virtual {p0}, Ldyr;->u_()Lu;

    move-result-object v1

    .line 109
    instance-of v2, v1, Ldys;

    if-eqz v2, :cond_0

    .line 110
    check-cast v1, Ldys;

    invoke-interface {v1, v0}, Ldys;->a(Landroid/os/Parcelable;)V

    goto :goto_0

    .line 115
    :pswitch_1
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    goto :goto_0

    .line 105
    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
