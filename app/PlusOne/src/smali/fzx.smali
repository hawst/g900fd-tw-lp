.class public final Lfzx;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic a:Ljava/io/File;

.field private synthetic b:Lkda;


# direct methods
.method public constructor <init>(Ljava/io/File;Lkda;)V
    .locals 0

    .prologue
    .line 1017
    iput-object p1, p0, Lfzx;->a:Ljava/io/File;

    iput-object p2, p0, Lfzx;->b:Lkda;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 1020
    .line 1025
    :try_start_0
    invoke-static {}, Lcom/google/android/apps/plus/views/PhotoView;->o()I

    move-result v0

    int-to-float v3, v0

    .line 1027
    new-instance v4, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v4}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 1028
    const/4 v0, 0x1

    iput v0, v4, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 1029
    const/4 v0, 0x1

    iput-boolean v0, v4, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 1031
    new-instance v0, Ljava/io/FileInputStream;

    iget-object v2, p0, Lfzx;->a:Ljava/io/File;

    invoke-direct {v0, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 1032
    const/4 v2, 0x0

    invoke-static {v0, v2, v4}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 1034
    iget v0, v4, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v2, v4, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    if-le v0, v2, :cond_0

    iget v0, v4, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    :goto_0
    move v2, v0

    .line 1039
    :goto_1
    int-to-float v2, v2

    cmpl-float v2, v2, v3

    if-lez v2, :cond_1

    .line 1040
    iget v2, v4, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    shl-int/lit8 v2, v2, 0x1

    iput v2, v4, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 1041
    iget v2, v4, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    div-int v2, v0, v2

    goto :goto_1

    .line 1034
    :cond_0
    iget v0, v4, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    goto :goto_0

    .line 1045
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, v4, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 1048
    :goto_2
    iget v0, v4, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    const/16 v2, 0x10

    if-gt v0, v2, :cond_2

    .line 1050
    :try_start_1
    new-instance v0, Ljava/io/FileInputStream;

    iget-object v2, p0, Lfzx;->a:Ljava/io/File;

    invoke-direct {v0, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 1051
    const/4 v2, 0x0

    invoke-static {v0, v2, v4}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 1061
    :goto_3
    if-eqz v0, :cond_3

    .line 1067
    invoke-static {}, Lcom/google/android/apps/plus/views/PhotoView;->p()Lizs;

    move-result-object v1

    invoke-virtual {v1}, Lizs;->a()Lkdv;

    move-result-object v1

    iget-object v2, p0, Lfzx;->b:Lkda;

    invoke-interface {v1, v2, v5, v0}, Lkdv;->a(Lkda;ILjava/lang/Object;)V

    .line 1073
    :goto_4
    return-void

    .line 1053
    :catch_0
    move-exception v0

    :try_start_2
    iget v0, v4, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    shl-int/lit8 v0, v0, 0x1

    iput v0, v4, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    :catch_1
    move-exception v0

    move-object v0, v1

    goto :goto_3

    :cond_2
    move-object v0, v1

    .line 1064
    goto :goto_3

    .line 1070
    :cond_3
    invoke-static {}, Lcom/google/android/apps/plus/views/PhotoView;->p()Lizs;

    move-result-object v0

    invoke-virtual {v0}, Lizs;->a()Lkdv;

    move-result-object v0

    iget-object v2, p0, Lfzx;->b:Lkda;

    const/4 v3, 0x5

    invoke-interface {v0, v2, v3, v1}, Lkdv;->a(Lkda;ILjava/lang/Object;)V

    goto :goto_4
.end method
