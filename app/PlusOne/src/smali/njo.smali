.class public final Lnjo;
.super Loxq;
.source "PG"


# instance fields
.field public a:I

.field public b:Lnjq;

.field private c:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5271
    invoke-direct {p0}, Loxq;-><init>()V

    .line 5584
    const/high16 v0, -0x80000000

    iput v0, p0, Lnjo;->a:I

    .line 5587
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lnjo;->c:[Ljava/lang/String;

    .line 5590
    const/4 v0, 0x0

    iput-object v0, p0, Lnjo;->b:Lnjq;

    .line 5271
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 5612
    .line 5613
    iget v0, p0, Lnjo;->a:I

    const/high16 v2, -0x80000000

    if-eq v0, v2, :cond_3

    .line 5614
    const/4 v0, 0x1

    iget v2, p0, Lnjo;->a:I

    .line 5615
    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 5617
    :goto_0
    iget-object v2, p0, Lnjo;->c:[Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lnjo;->c:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 5619
    iget-object v3, p0, Lnjo;->c:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_0

    aget-object v5, v3, v1

    .line 5621
    invoke-static {v5}, Loxo;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 5619
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 5623
    :cond_0
    add-int/2addr v0, v2

    .line 5624
    iget-object v1, p0, Lnjo;->c:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 5626
    :cond_1
    iget-object v1, p0, Lnjo;->b:Lnjq;

    if-eqz v1, :cond_2

    .line 5627
    const/4 v1, 0x3

    iget-object v2, p0, Lnjo;->b:Lnjq;

    .line 5628
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5630
    :cond_2
    iget-object v1, p0, Lnjo;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5631
    iput v0, p0, Lnjo;->ai:I

    .line 5632
    return v0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lnjo;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 5640
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 5641
    sparse-switch v0, :sswitch_data_0

    .line 5645
    iget-object v1, p0, Lnjo;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 5646
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnjo;->ah:Ljava/util/List;

    .line 5649
    :cond_1
    iget-object v1, p0, Lnjo;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5651
    :sswitch_0
    return-object p0

    .line 5656
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 5657
    if-eq v0, v4, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 5660
    :cond_2
    iput v0, p0, Lnjo;->a:I

    goto :goto_0

    .line 5662
    :cond_3
    iput v4, p0, Lnjo;->a:I

    goto :goto_0

    .line 5667
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 5668
    iget-object v0, p0, Lnjo;->c:[Ljava/lang/String;

    array-length v0, v0

    .line 5669
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    .line 5670
    iget-object v2, p0, Lnjo;->c:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 5671
    iput-object v1, p0, Lnjo;->c:[Ljava/lang/String;

    .line 5672
    :goto_1
    iget-object v1, p0, Lnjo;->c:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_4

    .line 5673
    iget-object v1, p0, Lnjo;->c:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 5674
    invoke-virtual {p1}, Loxn;->a()I

    .line 5672
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 5677
    :cond_4
    iget-object v1, p0, Lnjo;->c:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    .line 5681
    :sswitch_3
    iget-object v0, p0, Lnjo;->b:Lnjq;

    if-nez v0, :cond_5

    .line 5682
    new-instance v0, Lnjq;

    invoke-direct {v0}, Lnjq;-><init>()V

    iput-object v0, p0, Lnjo;->b:Lnjq;

    .line 5684
    :cond_5
    iget-object v0, p0, Lnjo;->b:Lnjq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 5641
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 5595
    iget v0, p0, Lnjo;->a:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 5596
    const/4 v0, 0x1

    iget v1, p0, Lnjo;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 5598
    :cond_0
    iget-object v0, p0, Lnjo;->c:[Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 5599
    iget-object v1, p0, Lnjo;->c:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 5600
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->a(ILjava/lang/String;)V

    .line 5599
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 5603
    :cond_1
    iget-object v0, p0, Lnjo;->b:Lnjq;

    if-eqz v0, :cond_2

    .line 5604
    const/4 v0, 0x3

    iget-object v1, p0, Lnjo;->b:Lnjq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 5606
    :cond_2
    iget-object v0, p0, Lnjo;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 5608
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5267
    invoke-virtual {p0, p1}, Lnjo;->a(Loxn;)Lnjo;

    move-result-object v0

    return-object v0
.end method
