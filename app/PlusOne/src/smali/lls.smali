.class public final Llls;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lllw;


# instance fields
.field private final a:Lllt;

.field private final b:I


# direct methods
.method public constructor <init>(Lllt;I)V
    .locals 2

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    const-string v0, "source cannot be null"

    invoke-static {p1, v0}, Llsk;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lllt;

    iput-object v0, p0, Llls;->a:Lllt;

    .line 20
    if-lez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "chunkSizeBytes must be positive"

    invoke-static {v0, v1}, Llsk;->a(ZLjava/lang/Object;)V

    .line 21
    iput p2, p0, Llls;->b:I

    .line 22
    return-void

    .line 20
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Llls;->b:I

    return v0
.end method

.method public a(I[B)I
    .locals 4

    .prologue
    .line 26
    iget-object v0, p0, Llls;->a:Lllt;

    iget v1, p0, Llls;->b:I

    mul-int/2addr v1, p1

    int-to-long v2, v1

    invoke-static {p2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-interface {v0, v2, v3, v1}, Lllt;->a(JLjava/nio/ByteBuffer;)I

    move-result v0

    return v0
.end method

.method public a(II)Z
    .locals 6

    .prologue
    .line 46
    iget-object v0, p0, Llls;->a:Lllt;

    iget v1, p0, Llls;->b:I

    mul-int/2addr v1, p1

    int-to-long v2, v1

    iget v1, p0, Llls;->b:I

    mul-int/2addr v1, p2

    int-to-long v4, v1

    invoke-interface {v0, v2, v3, v4, v5}, Lllt;->a(JJ)Z

    move-result v0

    return v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Llls;->a:Lllt;

    invoke-interface {v0}, Lllt;->a()V

    .line 42
    return-void
.end method

.method public close()V
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Llls;->a:Lllt;

    invoke-interface {v0}, Lllt;->close()V

    .line 37
    return-void
.end method
