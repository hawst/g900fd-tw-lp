.class public final Lmjs;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnpt;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 4453
    invoke-direct {p0}, Loxq;-><init>()V

    .line 4456
    iput-object v0, p0, Lmjs;->apiHeader:Llyq;

    .line 4459
    iput-object v0, p0, Lmjs;->a:Lnpt;

    .line 4453
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 4476
    const/4 v0, 0x0

    .line 4477
    iget-object v1, p0, Lmjs;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 4478
    const/4 v0, 0x1

    iget-object v1, p0, Lmjs;->apiHeader:Llyq;

    .line 4479
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 4481
    :cond_0
    iget-object v1, p0, Lmjs;->a:Lnpt;

    if-eqz v1, :cond_1

    .line 4482
    const/4 v1, 0x2

    iget-object v2, p0, Lmjs;->a:Lnpt;

    .line 4483
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4485
    :cond_1
    iget-object v1, p0, Lmjs;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4486
    iput v0, p0, Lmjs;->ai:I

    .line 4487
    return v0
.end method

.method public a(Loxn;)Lmjs;
    .locals 2

    .prologue
    .line 4495
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 4496
    sparse-switch v0, :sswitch_data_0

    .line 4500
    iget-object v1, p0, Lmjs;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 4501
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmjs;->ah:Ljava/util/List;

    .line 4504
    :cond_1
    iget-object v1, p0, Lmjs;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4506
    :sswitch_0
    return-object p0

    .line 4511
    :sswitch_1
    iget-object v0, p0, Lmjs;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 4512
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmjs;->apiHeader:Llyq;

    .line 4514
    :cond_2
    iget-object v0, p0, Lmjs;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 4518
    :sswitch_2
    iget-object v0, p0, Lmjs;->a:Lnpt;

    if-nez v0, :cond_3

    .line 4519
    new-instance v0, Lnpt;

    invoke-direct {v0}, Lnpt;-><init>()V

    iput-object v0, p0, Lmjs;->a:Lnpt;

    .line 4521
    :cond_3
    iget-object v0, p0, Lmjs;->a:Lnpt;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 4496
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 4464
    iget-object v0, p0, Lmjs;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 4465
    const/4 v0, 0x1

    iget-object v1, p0, Lmjs;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 4467
    :cond_0
    iget-object v0, p0, Lmjs;->a:Lnpt;

    if-eqz v0, :cond_1

    .line 4468
    const/4 v0, 0x2

    iget-object v1, p0, Lmjs;->a:Lnpt;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 4470
    :cond_1
    iget-object v0, p0, Lmjs;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 4472
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 4449
    invoke-virtual {p0, p1}, Lmjs;->a(Loxn;)Lmjs;

    move-result-object v0

    return-object v0
.end method
