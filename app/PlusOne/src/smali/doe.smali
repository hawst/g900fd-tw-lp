.class public final Ldoe;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmfy;",
        "Lmfz;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:J


# direct methods
.method public constructor <init>(Landroid/content/Context;IJ)V
    .locals 7

    .prologue
    .line 32
    const-string v3, "notificationsupdatelastviewedversion"

    new-instance v4, Lmfy;

    invoke-direct {v4}, Lmfy;-><init>()V

    new-instance v5, Lmfz;

    invoke-direct {v5}, Lmfz;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 35
    iput-wide p3, p0, Ldoe;->a:J

    .line 36
    return-void
.end method


# virtual methods
.method protected a(Lmfy;)V
    .locals 4

    .prologue
    .line 40
    new-instance v0, Lmyp;

    invoke-direct {v0}, Lmyp;-><init>()V

    iput-object v0, p1, Lmfy;->a:Lmyp;

    .line 41
    iget-object v0, p1, Lmfy;->a:Lmyp;

    .line 42
    new-instance v1, Lmyk;

    invoke-direct {v1}, Lmyk;-><init>()V

    iput-object v1, v0, Lmyp;->a:Lmyk;

    .line 43
    iget-object v1, v0, Lmyp;->a:Lmyk;

    const-string v2, "android_gplus"

    iput-object v2, v1, Lmyk;->a:Ljava/lang/String;

    .line 44
    new-instance v1, Lmyo;

    invoke-direct {v1}, Lmyo;-><init>()V

    iput-object v1, v0, Lmyp;->b:Lmyo;

    .line 46
    iget-object v1, v0, Lmyp;->b:Lmyo;

    const-string v2, "GPLUS_APP_V3"

    iput-object v2, v1, Lmyo;->a:Ljava/lang/String;

    .line 47
    iget-object v0, v0, Lmyp;->b:Lmyo;

    iget-wide v2, p0, Ldoe;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lmyo;->b:Ljava/lang/Long;

    .line 48
    return-void
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 17
    check-cast p1, Lmfy;

    invoke-virtual {p0, p1}, Ldoe;->a(Lmfy;)V

    return-void
.end method

.method protected synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 17
    invoke-virtual {p0}, Ldoe;->d()V

    return-void
.end method

.method protected d()V
    .locals 4

    .prologue
    .line 52
    iget-object v0, p0, Ldoe;->f:Landroid/content/Context;

    iget v1, p0, Ldoe;->c:I

    iget-wide v2, p0, Ldoe;->a:J

    invoke-static {v0, v1, v2, v3}, Ldsf;->a(Landroid/content/Context;IJ)V

    .line 54
    return-void
.end method
