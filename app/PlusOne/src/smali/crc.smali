.class public final Lcrc;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcrb;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lcrd;

.field public final c:Lcra;

.field public final d:F


# direct methods
.method public constructor <init>(Lcra;Ljava/util/List;Lcrd;F)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcra;",
            "Ljava/util/List",
            "<",
            "Lcrb;",
            ">;",
            "Lcrd;",
            "F)V"
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcrc;->c:Lcra;

    .line 39
    iput-object p2, p0, Lcrc;->a:Ljava/util/List;

    .line 40
    iput-object p3, p0, Lcrc;->b:Lcrd;

    .line 41
    iput p4, p0, Lcrc;->d:F

    .line 42
    return-void
.end method


# virtual methods
.method public a()F
    .locals 2

    .prologue
    .line 65
    iget-object v0, p0, Lcrc;->c:Lcra;

    invoke-virtual {v0}, Lcra;->a()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcrc;->d:F

    add-float/2addr v0, v1

    return v0
.end method

.method public a(I)Lcrb;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 51
    iget-object v0, p0, Lcrc;->a:Ljava/util/List;

    if-nez v0, :cond_1

    move-object v0, v1

    .line 57
    :cond_0
    :goto_0
    return-object v0

    .line 55
    :cond_1
    iget-object v0, p0, Lcrc;->a:Ljava/util/List;

    iget-object v2, p0, Lcrc;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcrb;

    .line 57
    iget-object v2, p0, Lcrc;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-eq v2, p1, :cond_0

    invoke-virtual {v0}, Lcrb;->a()Z

    move-result v2

    if-nez v2, :cond_0

    move-object v0, v1

    goto :goto_0
.end method

.method public b()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 72
    iget-object v0, p0, Lcrc;->a:Ljava/util/List;

    if-nez v0, :cond_0

    move v0, v1

    .line 77
    :goto_0
    return v0

    .line 76
    :cond_0
    iget-object v0, p0, Lcrc;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 77
    if-lez v0, :cond_1

    iget-object v2, p0, Lcrc;->a:Ljava/util/List;

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcrb;

    invoke-virtual {v0}, Lcrb;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 84
    invoke-virtual {p0}, Lcrc;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcrc;->b:Lcrd;

    iget-boolean v0, v0, Lcrd;->a:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 92
    iget-object v0, p0, Lcrc;->a:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcrc;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    .line 94
    :goto_0
    if-lez v1, :cond_2

    iget-object v0, p0, Lcrc;->a:Ljava/util/List;

    add-int/lit8 v3, v1, -0x1

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcrb;

    iget v0, v0, Lcrb;->i:I

    const/16 v3, 0x65

    if-eq v0, v3, :cond_2

    iget-object v0, p0, Lcrc;->b:Lcrd;

    iget-boolean v0, v0, Lcrd;->a:Z

    if-eqz v0, :cond_2

    .line 95
    add-int/lit8 v0, v1, 0x1

    .line 98
    :goto_1
    iget-object v1, p0, Lcrc;->b:Lcrd;

    iget-boolean v1, v1, Lcrd;->b:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_2
    add-int/2addr v0, v1

    .line 100
    return v0

    :cond_0
    move v1, v2

    .line 92
    goto :goto_0

    :cond_1
    move v1, v2

    .line 98
    goto :goto_2

    :cond_2
    move v0, v1

    goto :goto_1
.end method
