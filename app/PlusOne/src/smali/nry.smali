.class public final Lnry;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnry;


# instance fields
.field public b:I

.field public c:Ljava/lang/Integer;

.field public d:Ljava/lang/String;

.field public e:[Lnsh;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1001
    const/4 v0, 0x0

    new-array v0, v0, [Lnry;

    sput-object v0, Lnry;->a:[Lnry;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1002
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1005
    const/high16 v0, -0x80000000

    iput v0, p0, Lnry;->b:I

    .line 1012
    sget-object v0, Lnsh;->a:[Lnsh;

    iput-object v0, p0, Lnry;->e:[Lnsh;

    .line 1002
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1039
    .line 1040
    iget v0, p0, Lnry;->b:I

    const/high16 v2, -0x80000000

    if-eq v0, v2, :cond_4

    .line 1041
    const/4 v0, 0x1

    iget v2, p0, Lnry;->b:I

    .line 1042
    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1044
    :goto_0
    iget-object v2, p0, Lnry;->c:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    .line 1045
    const/4 v2, 0x2

    iget-object v3, p0, Lnry;->c:Ljava/lang/Integer;

    .line 1046
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 1048
    :cond_0
    iget-object v2, p0, Lnry;->d:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 1049
    const/4 v2, 0x3

    iget-object v3, p0, Lnry;->d:Ljava/lang/String;

    .line 1050
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1052
    :cond_1
    iget-object v2, p0, Lnry;->e:[Lnsh;

    if-eqz v2, :cond_3

    .line 1053
    iget-object v2, p0, Lnry;->e:[Lnsh;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 1054
    if-eqz v4, :cond_2

    .line 1055
    const/4 v5, 0x4

    .line 1056
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 1053
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1060
    :cond_3
    iget-object v1, p0, Lnry;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1061
    iput v0, p0, Lnry;->ai:I

    .line 1062
    return v0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lnry;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1070
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1071
    sparse-switch v0, :sswitch_data_0

    .line 1075
    iget-object v2, p0, Lnry;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 1076
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnry;->ah:Ljava/util/List;

    .line 1079
    :cond_1
    iget-object v2, p0, Lnry;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1081
    :sswitch_0
    return-object p0

    .line 1086
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1087
    if-eqz v0, :cond_2

    const/4 v2, 0x1

    if-eq v0, v2, :cond_2

    const/4 v2, 0x2

    if-eq v0, v2, :cond_2

    const/4 v2, 0x3

    if-eq v0, v2, :cond_2

    const/4 v2, 0x4

    if-eq v0, v2, :cond_2

    const/4 v2, 0x5

    if-eq v0, v2, :cond_2

    const/4 v2, 0x6

    if-eq v0, v2, :cond_2

    const/4 v2, 0x7

    if-ne v0, v2, :cond_3

    .line 1095
    :cond_2
    iput v0, p0, Lnry;->b:I

    goto :goto_0

    .line 1097
    :cond_3
    iput v1, p0, Lnry;->b:I

    goto :goto_0

    .line 1102
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnry;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 1106
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnry;->d:Ljava/lang/String;

    goto :goto_0

    .line 1110
    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1111
    iget-object v0, p0, Lnry;->e:[Lnsh;

    if-nez v0, :cond_5

    move v0, v1

    .line 1112
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnsh;

    .line 1113
    iget-object v3, p0, Lnry;->e:[Lnsh;

    if-eqz v3, :cond_4

    .line 1114
    iget-object v3, p0, Lnry;->e:[Lnsh;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1116
    :cond_4
    iput-object v2, p0, Lnry;->e:[Lnsh;

    .line 1117
    :goto_2
    iget-object v2, p0, Lnry;->e:[Lnsh;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 1118
    iget-object v2, p0, Lnry;->e:[Lnsh;

    new-instance v3, Lnsh;

    invoke-direct {v3}, Lnsh;-><init>()V

    aput-object v3, v2, v0

    .line 1119
    iget-object v2, p0, Lnry;->e:[Lnsh;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1120
    invoke-virtual {p1}, Loxn;->a()I

    .line 1117
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1111
    :cond_5
    iget-object v0, p0, Lnry;->e:[Lnsh;

    array-length v0, v0

    goto :goto_1

    .line 1123
    :cond_6
    iget-object v2, p0, Lnry;->e:[Lnsh;

    new-instance v3, Lnsh;

    invoke-direct {v3}, Lnsh;-><init>()V

    aput-object v3, v2, v0

    .line 1124
    iget-object v2, p0, Lnry;->e:[Lnsh;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1071
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 1017
    iget v0, p0, Lnry;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 1018
    const/4 v0, 0x1

    iget v1, p0, Lnry;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1020
    :cond_0
    iget-object v0, p0, Lnry;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 1021
    const/4 v0, 0x2

    iget-object v1, p0, Lnry;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1023
    :cond_1
    iget-object v0, p0, Lnry;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1024
    const/4 v0, 0x3

    iget-object v1, p0, Lnry;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1026
    :cond_2
    iget-object v0, p0, Lnry;->e:[Lnsh;

    if-eqz v0, :cond_4

    .line 1027
    iget-object v1, p0, Lnry;->e:[Lnsh;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 1028
    if-eqz v3, :cond_3

    .line 1029
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 1027
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1033
    :cond_4
    iget-object v0, p0, Lnry;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1035
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 998
    invoke-virtual {p0, p1}, Lnry;->a(Loxn;)Lnry;

    move-result-object v0

    return-object v0
.end method
