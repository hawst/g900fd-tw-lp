.class public abstract Ljnu;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Lnjt;)I
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    .line 76
    if-eqz p0, :cond_0

    iget v2, p0, Lnjt;->b:I

    if-ne v2, v0, :cond_0

    iget-object v2, p0, Lnjt;->f:Lnjg;

    if-nez v2, :cond_2

    .line 79
    :cond_0
    const/4 v0, 0x0

    .line 93
    :cond_1
    :goto_0
    return v0

    .line 82
    :cond_2
    iget-object v2, p0, Lnjt;->f:Lnjg;

    iget v2, v2, Lnjg;->a:I

    if-ne v2, v1, :cond_3

    iget-object v2, p0, Lnjt;->f:Lnjg;

    iget-object v2, v2, Lnjg;->b:Lnix;

    if-nez v2, :cond_4

    :cond_3
    move v0, v1

    .line 83
    goto :goto_0

    .line 86
    :cond_4
    iget-object v2, p0, Lnjt;->f:Lnjg;

    iget-object v2, v2, Lnjg;->b:Lnix;

    iget-object v2, v2, Lnix;->d:Lpmm;

    if-eqz v2, :cond_1

    .line 90
    iget-object v0, p0, Lnjt;->f:Lnjg;

    iget-object v0, v0, Lnjg;->b:Lnix;

    iget v0, v0, Lnix;->c:I

    if-ne v0, v1, :cond_5

    .line 91
    const/4 v0, 0x4

    goto :goto_0

    .line 93
    :cond_5
    const/4 v0, 0x3

    goto :goto_0
.end method

.method public static a(Lnix;)Z
    .locals 1

    .prologue
    .line 474
    if-eqz p0, :cond_0

    iget-object v0, p0, Lnix;->d:Lpmm;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lnix;->d:Lpmm;

    iget-object v0, v0, Lpmm;->i:Lpqm;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lnix;->d:Lpmm;

    iget-object v0, v0, Lpmm;->i:Lpqm;

    iget-object v0, v0, Lpqm;->a:[Lpqn;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lnix;->d:Lpmm;

    iget-object v0, v0, Lpmm;->i:Lpqm;

    iget-object v0, v0, Lpqm;->a:[Lpqn;

    array-length v0, v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lnjt;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 116
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 117
    invoke-static {p0}, Ljnu;->a(Lnjt;)I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 142
    :cond_0
    :goto_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-nez v1, :cond_3

    .line 145
    :cond_1
    :goto_1
    return-object v0

    .line 120
    :pswitch_0
    iget-object v3, p0, Lnjt;->f:Lnjg;

    iget-object v3, v3, Lnjg;->b:Lnix;

    iget-object v3, v3, Lnix;->d:Lpmm;

    iget-object v3, v3, Lpmm;->b:Lpge;

    if-eqz v3, :cond_0

    .line 121
    iget-object v3, p0, Lnjt;->f:Lnjg;

    iget-object v3, v3, Lnjg;->b:Lnix;

    iget-object v3, v3, Lnix;->d:Lpmm;

    iget-object v3, v3, Lpmm;->b:Lpge;

    iget-object v3, v3, Lpge;->c:[Ljava/lang/String;

    .line 122
    if-eqz v3, :cond_1

    array-length v4, v3

    if-eqz v4, :cond_1

    .line 125
    array-length v4, v3

    :goto_2
    if-ge v1, v4, :cond_0

    aget-object v5, v3, v1

    .line 126
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    if-lez v6, :cond_2

    .line 127
    const/16 v6, 0xa

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 129
    :cond_2
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 125
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 136
    :pswitch_1
    iget-object v3, p0, Lnjt;->d:Lnib;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lnjt;->d:Lnib;

    iget-object v3, v3, Lnib;->c:Lnif;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lnjt;->d:Lnib;

    iget-object v3, v3, Lnib;->c:Lnif;

    iget-object v3, v3, Lnif;->c:[Lnjx;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lnjt;->d:Lnib;

    iget-object v3, v3, Lnib;->c:Lnif;

    iget-object v3, v3, Lnif;->c:[Lnjx;

    array-length v3, v3

    if-eqz v3, :cond_0

    .line 139
    iget-object v3, p0, Lnjt;->d:Lnib;

    iget-object v3, v3, Lnib;->c:Lnif;

    iget-object v3, v3, Lnif;->c:[Lnjx;

    aget-object v1, v3, v1

    iget-object v1, v1, Lnjx;->d:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 145
    :cond_3
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 117
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static b(Lnix;)Z
    .locals 1

    .prologue
    .line 482
    if-eqz p0, :cond_0

    iget-object v0, p0, Lnix;->b:Lnhw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lnix;->b:Lnhw;

    iget-object v0, v0, Lnhw;->a:[Lnkh;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lnix;->b:Lnhw;

    iget-object v0, v0, Lnhw;->a:[Lnkh;

    array-length v0, v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Lnjt;)Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 149
    invoke-static {p0}, Ljnu;->a(Lnjt;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 169
    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 152
    :pswitch_0
    iget-object v0, p0, Lnjt;->f:Lnjg;

    iget-object v0, v0, Lnjg;->b:Lnix;

    iget-object v0, v0, Lnix;->d:Lpmm;

    iget-object v0, v0, Lpmm;->j:Lprh;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lnjt;->f:Lnjg;

    iget-object v0, v0, Lnjg;->b:Lnix;

    iget-object v0, v0, Lnix;->d:Lpmm;

    iget-object v0, v0, Lpmm;->j:Lprh;

    iget-object v0, v0, Lprh;->a:[Lprg;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lnjt;->f:Lnjg;

    iget-object v0, v0, Lnjg;->b:Lnix;

    iget-object v0, v0, Lnix;->d:Lpmm;

    iget-object v0, v0, Lpmm;->j:Lprh;

    iget-object v0, v0, Lprh;->a:[Lprg;

    array-length v0, v0

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lnjt;->f:Lnjg;

    iget-object v0, v0, Lnjg;->b:Lnix;

    iget-object v0, v0, Lnix;->d:Lpmm;

    iget-object v0, v0, Lpmm;->j:Lprh;

    iget-object v0, v0, Lprh;->a:[Lprg;

    aget-object v0, v0, v1

    iget-object v0, v0, Lprg;->b:Ljava/lang/String;

    goto :goto_0

    .line 161
    :pswitch_1
    iget-object v0, p0, Lnjt;->d:Lnib;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lnjt;->d:Lnib;

    iget-object v0, v0, Lnib;->c:Lnif;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lnjt;->d:Lnib;

    iget-object v0, v0, Lnib;->c:Lnif;

    iget-object v0, v0, Lnif;->a:[Lnka;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lnjt;->d:Lnib;

    iget-object v0, v0, Lnib;->c:Lnif;

    iget-object v0, v0, Lnif;->a:[Lnka;

    array-length v0, v0

    if-eqz v0, :cond_0

    .line 165
    iget-object v0, p0, Lnjt;->d:Lnib;

    iget-object v0, v0, Lnib;->c:Lnif;

    iget-object v0, v0, Lnif;->a:[Lnka;

    aget-object v0, v0, v1

    iget-object v0, v0, Lnka;->d:Ljava/lang/String;

    goto :goto_0

    .line 149
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static d(Lnjt;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lnjt;->f:Lnjg;

    iget-object v0, v0, Lnjg;->b:Lnix;

    iget-object v0, v0, Lnix;->d:Lpmm;

    iget-object v0, v0, Lpmm;->a:Lpqx;

    if-nez v0, :cond_0

    .line 174
    const/4 v0, 0x0

    .line 176
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lnjt;->f:Lnjg;

    iget-object v0, v0, Lnjg;->b:Lnix;

    iget-object v0, v0, Lnix;->d:Lpmm;

    iget-object v0, v0, Lpmm;->a:Lpqx;

    iget-object v0, v0, Lpqx;->f:Ljava/lang/String;

    goto :goto_0
.end method

.method public static e(Lnjt;)Lpxp;
    .locals 1

    .prologue
    .line 181
    if-eqz p0, :cond_0

    iget-object v0, p0, Lnjt;->f:Lnjg;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lnjt;->f:Lnjg;

    iget-object v0, v0, Lnjg;->b:Lnix;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lnjt;->f:Lnjg;

    iget-object v0, v0, Lnjg;->b:Lnix;

    iget-object v0, v0, Lnix;->d:Lpmm;

    if-nez v0, :cond_1

    .line 183
    :cond_0
    const/4 v0, 0x0

    .line 185
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lnjt;->f:Lnjg;

    iget-object v0, v0, Lnjg;->b:Lnix;

    iget-object v0, v0, Lnix;->d:Lpmm;

    iget-object v0, v0, Lpmm;->o:Lpxp;

    goto :goto_0
.end method

.method public static f(Lnjt;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lnjt;->f:Lnjg;

    iget-object v0, v0, Lnjg;->b:Lnix;

    iget-object v0, v0, Lnix;->d:Lpmm;

    iget-object v0, v0, Lpmm;->p:Lpkh;

    if-nez v0, :cond_0

    .line 190
    const/4 v0, 0x0

    .line 192
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lnjt;->f:Lnjg;

    iget-object v0, v0, Lnjg;->b:Lnix;

    iget-object v0, v0, Lnix;->d:Lpmm;

    iget-object v0, v0, Lpmm;->p:Lpkh;

    iget-object v0, v0, Lpkh;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method public static g(Lnjt;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lnjt;",
            ")",
            "Ljava/util/List",
            "<",
            "Lpmx;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 353
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 354
    iget-object v1, p0, Lnjt;->f:Lnjg;

    iget-object v1, v1, Lnjg;->b:Lnix;

    iget-object v1, v1, Lnix;->d:Lpmm;

    iget-object v3, v1, Lpmm;->h:Lpmz;

    .line 355
    if-eqz v3, :cond_0

    iget-object v1, v3, Lpmz;->a:[Lpmx;

    if-eqz v1, :cond_0

    iget-object v1, v3, Lpmz;->a:[Lpmx;

    array-length v1, v1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    .line 358
    :goto_0
    if-eqz v1, :cond_1

    .line 359
    iget-object v1, v3, Lpmz;->a:[Lpmx;

    array-length v3, v1

    :goto_1
    if-ge v0, v3, :cond_1

    aget-object v4, v1, v0

    .line 360
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 359
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    move v1, v0

    .line 355
    goto :goto_0

    .line 363
    :cond_1
    return-object v2
.end method

.method public static h(Lnjt;)Lprs;
    .locals 1

    .prologue
    .line 390
    iget-object v0, p0, Lnjt;->f:Lnjg;

    iget-object v0, v0, Lnjg;->b:Lnix;

    iget-object v0, v0, Lnix;->d:Lpmm;

    iget-object v0, v0, Lpmm;->r:Lprs;

    return-object v0
.end method

.method public static i(Lnjt;)Z
    .locals 1

    .prologue
    .line 462
    invoke-static {p0}, Ljnu;->a(Lnjt;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 470
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 465
    :pswitch_0
    iget-object v0, p0, Lnjt;->f:Lnjg;

    iget-object v0, v0, Lnjg;->b:Lnix;

    invoke-static {v0}, Ljnu;->a(Lnix;)Z

    move-result v0

    goto :goto_0

    .line 468
    :pswitch_1
    iget-object v0, p0, Lnjt;->f:Lnjg;

    iget-object v0, v0, Lnjg;->b:Lnix;

    invoke-static {v0}, Ljnu;->b(Lnix;)Z

    move-result v0

    goto :goto_0

    .line 462
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static j(Lnjt;)Lppf;
    .locals 1

    .prologue
    .line 489
    invoke-static {p0}, Ljnu;->a(Lnjt;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 497
    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 492
    :pswitch_0
    iget-object v0, p0, Lnjt;->f:Lnjg;

    iget-object v0, v0, Lnjg;->b:Lnix;

    iget-object v0, v0, Lnix;->d:Lpmm;

    iget-object v0, v0, Lpmm;->f:Lpha;

    if-eqz v0, :cond_0

    .line 493
    iget-object v0, p0, Lnjt;->f:Lnjg;

    iget-object v0, v0, Lnjg;->b:Lnix;

    iget-object v0, v0, Lnix;->d:Lpmm;

    iget-object v0, v0, Lpmm;->f:Lpha;

    iget-object v0, v0, Lpha;->a:Lppf;

    goto :goto_0

    .line 489
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static k(Lnjt;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 521
    .line 523
    iget-object v1, p0, Lnjt;->f:Lnjg;

    iget-object v1, v1, Lnjg;->b:Lnix;

    iget-object v1, v1, Lnix;->d:Lpmm;

    iget-object v2, v1, Lpmm;->l:Lpui;

    .line 524
    if-eqz v2, :cond_2

    .line 525
    iget-object v1, v2, Lpui;->a:Lpuj;

    .line 527
    iget-object v3, v2, Lpui;->b:[Lpuj;

    .line 528
    if-eqz v3, :cond_3

    .line 529
    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_3

    aget-object v5, v3, v2

    .line 530
    iget-object v6, v5, Lpuj;->c:Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    const/16 v7, 0x640

    if-ne v6, v7, :cond_1

    iget-object v6, v5, Lpuj;->d:Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    const/16 v7, 0x384

    if-ne v6, v7, :cond_1

    .line 531
    iget-object v0, v5, Lpuj;->b:Ljava/lang/String;

    .line 537
    :cond_0
    :goto_1
    return-object v0

    .line 529
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    move-object v1, v0

    .line 537
    :cond_3
    if-eqz v1, :cond_0

    iget-object v0, v1, Lpuj;->b:Ljava/lang/String;

    goto :goto_1
.end method

.method public static l(Lnjt;)[Lpgq;
    .locals 1

    .prologue
    .line 541
    invoke-static {p0}, Ljnu;->a(Lnjt;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 549
    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 544
    :pswitch_0
    iget-object v0, p0, Lnjt;->f:Lnjg;

    iget-object v0, v0, Lnjg;->b:Lnix;

    iget-object v0, v0, Lnix;->d:Lpmm;

    iget-object v0, v0, Lpmm;->d:Lpgs;

    if-eqz v0, :cond_0

    .line 545
    iget-object v0, p0, Lnjt;->f:Lnjg;

    iget-object v0, v0, Lnjg;->b:Lnix;

    iget-object v0, v0, Lnix;->d:Lpmm;

    iget-object v0, v0, Lpmm;->d:Lpgs;

    iget-object v0, v0, Lpgs;->a:[Lpgq;

    goto :goto_0

    .line 541
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
