.class public final Lngh;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field private b:Lngj;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 333
    invoke-direct {p0}, Loxq;-><init>()V

    .line 338
    const/4 v0, 0x0

    iput-object v0, p0, Lngh;->b:Lngj;

    .line 333
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 355
    const/4 v0, 0x0

    .line 356
    iget-object v1, p0, Lngh;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 357
    const/4 v0, 0x1

    iget-object v1, p0, Lngh;->a:Ljava/lang/String;

    .line 358
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 360
    :cond_0
    iget-object v1, p0, Lngh;->b:Lngj;

    if-eqz v1, :cond_1

    .line 361
    const/4 v1, 0x2

    iget-object v2, p0, Lngh;->b:Lngj;

    .line 362
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 364
    :cond_1
    iget-object v1, p0, Lngh;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 365
    iput v0, p0, Lngh;->ai:I

    .line 366
    return v0
.end method

.method public a(Loxn;)Lngh;
    .locals 2

    .prologue
    .line 374
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 375
    sparse-switch v0, :sswitch_data_0

    .line 379
    iget-object v1, p0, Lngh;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 380
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lngh;->ah:Ljava/util/List;

    .line 383
    :cond_1
    iget-object v1, p0, Lngh;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 385
    :sswitch_0
    return-object p0

    .line 390
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lngh;->a:Ljava/lang/String;

    goto :goto_0

    .line 394
    :sswitch_2
    iget-object v0, p0, Lngh;->b:Lngj;

    if-nez v0, :cond_2

    .line 395
    new-instance v0, Lngj;

    invoke-direct {v0}, Lngj;-><init>()V

    iput-object v0, p0, Lngh;->b:Lngj;

    .line 397
    :cond_2
    iget-object v0, p0, Lngh;->b:Lngj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 375
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 343
    iget-object v0, p0, Lngh;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 344
    const/4 v0, 0x1

    iget-object v1, p0, Lngh;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 346
    :cond_0
    iget-object v0, p0, Lngh;->b:Lngj;

    if-eqz v0, :cond_1

    .line 347
    const/4 v0, 0x2

    iget-object v1, p0, Lngh;->b:Lngj;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 349
    :cond_1
    iget-object v0, p0, Lngh;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 351
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 329
    invoke-virtual {p0, p1}, Lngh;->a(Loxn;)Lngh;

    move-result-object v0

    return-object v0
.end method
