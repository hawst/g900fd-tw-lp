.class public final Lnlt;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lofz;

.field public b:Lnkf;

.field private c:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 241
    invoke-direct {p0}, Loxq;-><init>()V

    .line 249
    iput-object v1, p0, Lnlt;->a:Lofz;

    .line 252
    const/high16 v0, -0x80000000

    iput v0, p0, Lnlt;->c:I

    .line 255
    iput-object v1, p0, Lnlt;->b:Lnkf;

    .line 241
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 275
    const/4 v0, 0x0

    .line 276
    iget-object v1, p0, Lnlt;->a:Lofz;

    if-eqz v1, :cond_0

    .line 277
    const/4 v0, 0x1

    iget-object v1, p0, Lnlt;->a:Lofz;

    .line 278
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 280
    :cond_0
    iget v1, p0, Lnlt;->c:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_1

    .line 281
    const/4 v1, 0x2

    iget v2, p0, Lnlt;->c:I

    .line 282
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 284
    :cond_1
    iget-object v1, p0, Lnlt;->b:Lnkf;

    if-eqz v1, :cond_2

    .line 285
    const/4 v1, 0x3

    iget-object v2, p0, Lnlt;->b:Lnkf;

    .line 286
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 288
    :cond_2
    iget-object v1, p0, Lnlt;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 289
    iput v0, p0, Lnlt;->ai:I

    .line 290
    return v0
.end method

.method public a(Loxn;)Lnlt;
    .locals 2

    .prologue
    .line 298
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 299
    sparse-switch v0, :sswitch_data_0

    .line 303
    iget-object v1, p0, Lnlt;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 304
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnlt;->ah:Ljava/util/List;

    .line 307
    :cond_1
    iget-object v1, p0, Lnlt;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 309
    :sswitch_0
    return-object p0

    .line 314
    :sswitch_1
    iget-object v0, p0, Lnlt;->a:Lofz;

    if-nez v0, :cond_2

    .line 315
    new-instance v0, Lofz;

    invoke-direct {v0}, Lofz;-><init>()V

    iput-object v0, p0, Lnlt;->a:Lofz;

    .line 317
    :cond_2
    iget-object v0, p0, Lnlt;->a:Lofz;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 321
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 322
    if-eqz v0, :cond_3

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    .line 324
    :cond_3
    iput v0, p0, Lnlt;->c:I

    goto :goto_0

    .line 326
    :cond_4
    const/4 v0, 0x0

    iput v0, p0, Lnlt;->c:I

    goto :goto_0

    .line 331
    :sswitch_3
    iget-object v0, p0, Lnlt;->b:Lnkf;

    if-nez v0, :cond_5

    .line 332
    new-instance v0, Lnkf;

    invoke-direct {v0}, Lnkf;-><init>()V

    iput-object v0, p0, Lnlt;->b:Lnkf;

    .line 334
    :cond_5
    iget-object v0, p0, Lnlt;->b:Lnkf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 299
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 260
    iget-object v0, p0, Lnlt;->a:Lofz;

    if-eqz v0, :cond_0

    .line 261
    const/4 v0, 0x1

    iget-object v1, p0, Lnlt;->a:Lofz;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 263
    :cond_0
    iget v0, p0, Lnlt;->c:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 264
    const/4 v0, 0x2

    iget v1, p0, Lnlt;->c:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 266
    :cond_1
    iget-object v0, p0, Lnlt;->b:Lnkf;

    if-eqz v0, :cond_2

    .line 267
    const/4 v0, 0x3

    iget-object v1, p0, Lnlt;->b:Lnkf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 269
    :cond_2
    iget-object v0, p0, Lnlt;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 271
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 237
    invoke-virtual {p0, p1}, Lnlt;->a(Loxn;)Lnlt;

    move-result-object v0

    return-object v0
.end method
