.class public final Lmpy;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lpax;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 203
    invoke-direct {p0}, Loxq;-><init>()V

    .line 206
    sget-object v0, Lpax;->a:[Lpax;

    iput-object v0, p0, Lmpy;->a:[Lpax;

    .line 203
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 229
    .line 230
    iget-object v1, p0, Lmpy;->a:[Lpax;

    if-eqz v1, :cond_1

    .line 231
    iget-object v2, p0, Lmpy;->a:[Lpax;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 232
    if-eqz v4, :cond_0

    .line 233
    const/4 v5, 0x1

    .line 234
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 231
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 238
    :cond_1
    iget-object v1, p0, Lmpy;->b:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 239
    const/4 v1, 0x2

    iget-object v2, p0, Lmpy;->b:Ljava/lang/String;

    .line 240
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 242
    :cond_2
    iget-object v1, p0, Lmpy;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 243
    iput v0, p0, Lmpy;->ai:I

    .line 244
    return v0
.end method

.method public a(Loxn;)Lmpy;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 252
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 253
    sparse-switch v0, :sswitch_data_0

    .line 257
    iget-object v2, p0, Lmpy;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 258
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmpy;->ah:Ljava/util/List;

    .line 261
    :cond_1
    iget-object v2, p0, Lmpy;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 263
    :sswitch_0
    return-object p0

    .line 268
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 269
    iget-object v0, p0, Lmpy;->a:[Lpax;

    if-nez v0, :cond_3

    move v0, v1

    .line 270
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lpax;

    .line 271
    iget-object v3, p0, Lmpy;->a:[Lpax;

    if-eqz v3, :cond_2

    .line 272
    iget-object v3, p0, Lmpy;->a:[Lpax;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 274
    :cond_2
    iput-object v2, p0, Lmpy;->a:[Lpax;

    .line 275
    :goto_2
    iget-object v2, p0, Lmpy;->a:[Lpax;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 276
    iget-object v2, p0, Lmpy;->a:[Lpax;

    new-instance v3, Lpax;

    invoke-direct {v3}, Lpax;-><init>()V

    aput-object v3, v2, v0

    .line 277
    iget-object v2, p0, Lmpy;->a:[Lpax;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 278
    invoke-virtual {p1}, Loxn;->a()I

    .line 275
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 269
    :cond_3
    iget-object v0, p0, Lmpy;->a:[Lpax;

    array-length v0, v0

    goto :goto_1

    .line 281
    :cond_4
    iget-object v2, p0, Lmpy;->a:[Lpax;

    new-instance v3, Lpax;

    invoke-direct {v3}, Lpax;-><init>()V

    aput-object v3, v2, v0

    .line 282
    iget-object v2, p0, Lmpy;->a:[Lpax;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 286
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmpy;->b:Ljava/lang/String;

    goto :goto_0

    .line 253
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 213
    iget-object v0, p0, Lmpy;->a:[Lpax;

    if-eqz v0, :cond_1

    .line 214
    iget-object v1, p0, Lmpy;->a:[Lpax;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 215
    if-eqz v3, :cond_0

    .line 216
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 214
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 220
    :cond_1
    iget-object v0, p0, Lmpy;->b:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 221
    const/4 v0, 0x2

    iget-object v1, p0, Lmpy;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 223
    :cond_2
    iget-object v0, p0, Lmpy;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 225
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 199
    invoke-virtual {p0, p1}, Lmpy;->a(Loxn;)Lmpy;

    move-result-object v0

    return-object v0
.end method
