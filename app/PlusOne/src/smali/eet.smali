.class public Leet;
.super Lehh;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lbc;
.implements Lctp;
.implements Lcty;
.implements Ldid;
.implements Ldys;
.implements Ldyv;
.implements Lepy;
.implements Leqk;
.implements Leqz;
.implements Lfyv;
.implements Lfyz;
.implements Lfzc;
.implements Lgas;
.implements Lhec;
.implements Lhob;
.implements Ljmi;
.implements Ljms;
.implements Ljmv;
.implements Ljmx;
.implements Ljmy;
.implements Ljni;
.implements Ljnj;
.implements Ljnk;
.implements Ljns;
.implements Ljnt;
.implements Lkut;
.implements Lkwn;
.implements Lkwo;
.implements Llgs;
.implements Llhl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lehh;",
        "Landroid/view/View$OnClickListener;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lctp;",
        "Lcty;",
        "Ldid;",
        "Ldys;",
        "Ldyv;",
        "Lepy;",
        "Leqk;",
        "Leqz;",
        "Lfyv;",
        "Lfyz;",
        "Lfzc;",
        "Lgas;",
        "Lhec;",
        "Lhob;",
        "Ljmi;",
        "Ljms;",
        "Ljmv;",
        "Ljmx;",
        "Ljmy;",
        "Ljni;",
        "Ljnj;",
        "Ljnk;",
        "Ljns;",
        "Ljnt;",
        "Lkut;",
        "Lkwn;",
        "Lkwo;",
        "Llgs;",
        "Llhl;"
    }
.end annotation


# static fields
.field public static final N:Llpa;

.field private static O:I

.field private static final aw:Landroid/database/Cursor;


# instance fields
.field private aA:Lhym;

.field private aB:Landroid/database/Cursor;

.field private aC:I

.field private aD:J

.field private aE:Ljava/lang/String;

.field private aF:Ljava/lang/String;

.field private aG:Landroid/database/Cursor;

.field private aH:Landroid/database/Cursor;

.field private aI:Ljava/lang/Integer;

.field private aJ:Z

.field private aK:Ljava/lang/Integer;

.field private aL:Ljava/lang/Integer;

.field private aM:Ljava/lang/Integer;

.field private aN:Ljava/lang/Integer;

.field private aO:Ljava/lang/Integer;

.field private aP:Z

.field private aQ:Ljava/lang/Integer;

.field private aR:I

.field private aS:I

.field private aT:Z

.field private aU:Z

.field private aV:Ljava/lang/String;

.field private aW:Z

.field private aX:Z

.field private aY:Z

.field private aZ:Z

.field private final ax:Landroid/os/Handler;

.field private ay:Lequ;

.field private az:Lfba;

.field private ba:Z

.field private bb:Lhxh;

.field private bc:I

.field private bd:Z

.field private be:Z

.field private bf:Z

.field private bg:Lhmk;

.field private bh:I

.field private bi:Llfn;

.field private bj:Lgcs;

.field private bk:Ljpb;

.field private bl:J

.field private final bm:Lfhh;

.field private final bn:Lbc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbc",
            "<",
            "Ldsx;",
            ">;"
        }
    .end annotation
.end field

.field private final bo:Landroid/database/DataSetObserver;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 169
    const-class v0, Leet;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    .line 255
    new-instance v0, Llpa;

    const-string v1, "enable_profile_photo_highlights"

    invoke-direct {v0, v1}, Llpa;-><init>(Ljava/lang/String;)V

    sput-object v0, Leet;->N:Llpa;

    .line 262
    new-instance v0, Lhym;

    sget-object v1, Levl;->b:[Ljava/lang/String;

    invoke-direct {v0, v1}, Lhym;-><init>([Ljava/lang/String;)V

    sput-object v0, Leet;->aw:Landroid/database/Cursor;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 169
    invoke-direct {p0}, Lehh;-><init>()V

    .line 265
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Leet;->ax:Landroid/os/Handler;

    .line 267
    new-instance v0, Lequ;

    iget-object v1, p0, Leet;->av:Llqm;

    const/16 v2, 0x66

    invoke-direct {v0, p0, v1, p0, v2}, Lequ;-><init>(Lu;Llqr;Leqz;I)V

    .line 269
    invoke-virtual {v0, v3}, Lequ;->a(Z)Lequ;

    move-result-object v0

    iput-object v0, p0, Leet;->ay:Lequ;

    .line 296
    iput-boolean v3, p0, Leet;->aU:Z

    .line 306
    const/4 v0, -0x1

    iput v0, p0, Leet;->bc:I

    .line 309
    iput-boolean v3, p0, Leet;->be:Z

    .line 313
    const/high16 v0, -0x80000000

    iput v0, p0, Leet;->bh:I

    .line 321
    new-instance v0, Lhnw;

    new-instance v1, Leff;

    invoke-direct {v1, p0}, Leff;-><init>(Leet;)V

    invoke-direct {v0, p0, v1}, Lhnw;-><init>(Llrn;Lhnv;)V

    .line 341
    new-instance v0, Leeu;

    invoke-direct {v0, p0}, Leeu;-><init>(Leet;)V

    iput-object v0, p0, Leet;->bm:Lfhh;

    .line 447
    new-instance v0, Leev;

    invoke-direct {v0, p0}, Leev;-><init>(Leet;)V

    iput-object v0, p0, Leet;->bn:Lbc;

    .line 728
    new-instance v0, Leey;

    invoke-direct {v0, p0}, Leey;-><init>(Leet;)V

    iput-object v0, p0, Leet;->bo:Landroid/database/DataSetObserver;

    .line 2932
    return-void
.end method

.method static synthetic a(Leet;I)I
    .locals 0

    .prologue
    .line 169
    iput p1, p0, Leet;->aC:I

    return p1
.end method

.method static synthetic a(Leet;J)J
    .locals 1

    .prologue
    .line 169
    iput-wide p1, p0, Leet;->aD:J

    return-wide p1
.end method

.method static synthetic a(Leet;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 0

    .prologue
    .line 169
    iput-object p1, p0, Leet;->aK:Ljava/lang/Integer;

    return-object p1
.end method

.method static synthetic a(Leet;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Leet;->aV:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Leet;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 169
    iput-object p1, p0, Leet;->aV:Ljava/lang/String;

    return-object p1
.end method

.method private a(I[B[B)V
    .locals 2

    .prologue
    .line 1307
    iget-object v0, p0, Leet;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    .line 1308
    invoke-virtual {p0}, Leet;->n()Lz;

    move-result-object v1

    invoke-static {v1, v0, p1, p2, p3}, Leyq;->a(Landroid/content/Context;II[B[B)Landroid/content/Intent;

    move-result-object v0

    .line 1310
    const/16 v1, 0x6a

    invoke-virtual {p0, v0, v1}, Leet;->a(Landroid/content/Intent;I)V

    .line 1311
    invoke-direct {p0, p1}, Leet;->i(I)V

    .line 1312
    return-void
.end method

.method static synthetic a(Leet;ILefh;)V
    .locals 4

    .prologue
    .line 169
    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p2, Lefh;->e:Ljava/lang/Long;

    if-eqz v0, :cond_0

    iget-object v0, p2, Lefh;->e:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    invoke-direct {p0, p2}, Leet;->a(Lefh;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x68
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic a(Leet;ILfib;)V
    .locals 0

    .prologue
    .line 169
    invoke-direct {p0, p1, p2}, Leet;->c(ILfib;)V

    return-void
.end method

.method private a(Lefh;)V
    .locals 4

    .prologue
    .line 2855
    iget-object v0, p0, Leet;->az:Lfba;

    .line 2856
    invoke-virtual {v0}, Lfba;->J()Ljava/lang/Long;

    move-result-object v0

    .line 2857
    if-eqz v0, :cond_0

    iget-object v1, p1, Lefh;->e:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2858
    iget-object v1, p0, Leet;->ax:Landroid/os/Handler;

    new-instance v2, Lefd;

    invoke-direct {v2, p0, p1, v0}, Lefd;-><init>(Leet;Lefh;Ljava/lang/Long;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2885
    :goto_0
    iget-object v0, p0, Leet;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v1

    .line 2886
    iget-boolean v0, p1, Lefh;->b:Z

    if-eqz v0, :cond_1

    .line 2887
    iget-object v0, p0, Leet;->au:Llnh;

    const-class v2, Lhms;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    iget-object v3, p0, Leet;->at:Llnl;

    invoke-direct {v2, v3, v1}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v1, Lhmv;->H:Lhmv;

    .line 2889
    invoke-virtual {v2, v1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 2887
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 2897
    :goto_1
    return-void

    .line 2873
    :cond_0
    iget-object v0, p0, Leet;->ax:Landroid/os/Handler;

    new-instance v1, Lefe;

    invoke-direct {v1, p0, p1}, Lefe;-><init>(Leet;Lefh;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 2892
    :cond_1
    iget-object v0, p0, Leet;->au:Llnh;

    const-class v2, Lhms;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    iget-object v3, p0, Leet;->at:Llnl;

    invoke-direct {v2, v3, v1}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v1, Lhmv;->G:Lhmv;

    .line 2894
    invoke-virtual {v2, v1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 2892
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    goto :goto_1
.end method

.method private a(Lhmv;Landroid/content/Intent;)V
    .locals 5

    .prologue
    .line 2825
    const/4 v0, 0x0

    .line 2826
    if-eqz p2, :cond_0

    .line 2827
    const-string v1, "profile_data_id"

    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 2828
    if-lez v1, :cond_0

    .line 2829
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2830
    const-string v2, "extra_profile_edit_field"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    move-object v1, v0

    .line 2833
    :goto_0
    iget-object v0, p0, Leet;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v2

    .line 2834
    iget-object v0, p0, Leet;->au:Llnh;

    const-class v3, Lhms;

    invoke-virtual {v0, v3}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v3, Lhmr;

    iget-object v4, p0, Leet;->at:Llnl;

    invoke-direct {v3, v4, v2}, Lhmr;-><init>(Landroid/content/Context;I)V

    .line 2836
    invoke-virtual {v3, p1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v2

    .line 2837
    invoke-virtual {v2, v1}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    .line 2834
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 2839
    return-void

    :cond_0
    move-object v1, v0

    goto :goto_0
.end method

.method static synthetic a(Leet;Ldsx;)Z
    .locals 4

    .prologue
    .line 169
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p1, Ldsx;->m:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0xdbba0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Leet;Z)Z
    .locals 0

    .prologue
    .line 169
    iput-boolean p1, p0, Leet;->aX:Z

    return p1
.end method

.method private aM()V
    .locals 4

    .prologue
    .line 1112
    iget-object v0, p0, Leet;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v1

    .line 1113
    invoke-virtual {p0}, Leet;->n()Lz;

    move-result-object v0

    iget-object v2, p0, Leet;->aa:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Leyq;->f(Landroid/content/Context;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1115
    invoke-virtual {p0, v0}, Leet;->a(Landroid/content/Intent;)V

    .line 1116
    iget-object v0, p0, Leet;->au:Llnh;

    const-class v2, Lhms;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    iget-object v3, p0, Leet;->at:Llnl;

    invoke-direct {v2, v3, v1}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v1, Lhmv;->cG:Lhmv;

    .line 1118
    invoke-virtual {v2, v1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 1116
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 1120
    return-void
.end method

.method static synthetic b(Leet;)J
    .locals 2

    .prologue
    .line 169
    iget-wide v0, p0, Leet;->aD:J

    return-wide v0
.end method

.method static synthetic b(Leet;J)J
    .locals 1

    .prologue
    .line 169
    iput-wide p1, p0, Leet;->bl:J

    return-wide p1
.end method

.method static synthetic b(Leet;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 169
    iput-object p1, p0, Leet;->aE:Ljava/lang/String;

    return-object p1
.end method

.method private b(I[B[B)V
    .locals 2

    .prologue
    .line 1315
    iget-object v0, p0, Leet;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    .line 1316
    invoke-virtual {p0}, Leet;->n()Lz;

    move-result-object v1

    invoke-static {v1, v0, p1, p2, p3}, Leyq;->b(Landroid/content/Context;II[B[B)Landroid/content/Intent;

    move-result-object v0

    .line 1318
    const/16 v1, 0x6a

    invoke-virtual {p0, v0, v1}, Leet;->a(Landroid/content/Intent;I)V

    .line 1319
    invoke-direct {p0, p1}, Leet;->i(I)V

    .line 1320
    return-void
.end method

.method static synthetic b(Leet;ILfib;)V
    .locals 0

    .prologue
    .line 169
    invoke-direct {p0, p1, p2}, Leet;->d(ILfib;)V

    return-void
.end method

.method private b(ZZ)V
    .locals 13

    .prologue
    const/4 v5, 0x1

    const/4 v11, 0x0

    const/4 v3, 0x0

    .line 869
    invoke-virtual {p0}, Leet;->n()Lz;

    move-result-object v1

    .line 871
    iget-object v0, p0, Leet;->T:Liwk;

    invoke-virtual {v0}, Liwk;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 872
    iget-object v0, p0, Leet;->T:Liwk;

    invoke-virtual {v0}, Liwk;->b()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 920
    :goto_0
    return-void

    .line 876
    :cond_0
    iget-object v0, p0, Leet;->bk:Ljpb;

    iget-object v2, p0, Leet;->R:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    invoke-interface {v0, v1, v2}, Ljpb;->d(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 877
    invoke-direct {p0, p1, p2}, Leet;->c(ZZ)Landroid/os/Bundle;

    move-result-object v0

    .line 878
    iget-object v1, p0, Leet;->bk:Ljpb;

    iget-object v2, p0, Leet;->R:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    const-string v3, "first_circle_add"

    invoke-interface {v1, p0, v2, v3, v0}, Ljpb;->a(Lu;ILjava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    .line 884
    :cond_1
    if-eqz p2, :cond_5

    .line 885
    iget-object v0, p0, Leet;->as:Landroid/database/Cursor;

    invoke-static {v1, v0, p1}, Ldsm;->a(Landroid/content/Context;Landroid/database/Cursor;Z)Ljava/lang/String;

    move-result-object v0

    .line 889
    :goto_1
    iget-boolean v2, p0, Leet;->bf:Z

    if-eqz v2, :cond_4

    if-eqz p1, :cond_4

    if-eqz v0, :cond_4

    .line 891
    iget-object v2, p0, Leet;->bk:Ljpb;

    iget-object v4, p0, Leet;->R:Lhee;

    invoke-interface {v4}, Lhee;->d()I

    move-result v4

    invoke-interface {v2, v1, v4}, Ljpb;->e(Landroid/content/Context;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 892
    invoke-static {v1, p1}, Ldsm;->a(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v5

    .line 893
    invoke-direct {p0, p1, p2}, Leet;->c(ZZ)Landroid/os/Bundle;

    move-result-object v6

    .line 894
    iget-object v0, p0, Leet;->bk:Ljpb;

    iget-object v1, p0, Leet;->R:Lhee;

    .line 895
    invoke-interface {v1}, Lhee;->d()I

    move-result v2

    const-string v3, "first_circle_add_one_click"

    iget-object v1, p0, Leet;->az:Lfba;

    .line 897
    invoke-virtual {v1}, Lfba;->y()Ljava/lang/String;

    move-result-object v4

    move-object v1, p0

    .line 894
    invoke-interface/range {v0 .. v6}, Ljpb;->a(Lu;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    :cond_2
    move v12, v5

    .line 904
    :goto_2
    if-nez v0, :cond_3

    .line 905
    iget-object v0, p0, Leet;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v1

    .line 907
    invoke-virtual {p0}, Leet;->n()Lz;

    move-result-object v0

    iget-object v2, p0, Leet;->aV:Ljava/lang/String;

    move-object v4, v3

    invoke-static/range {v0 .. v5}, Leyq;->b(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x65

    .line 906
    invoke-virtual {p0, v0, v1}, Leet;->a(Landroid/content/Intent;I)V

    goto :goto_0

    .line 913
    :cond_3
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 914
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 916
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v11}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v7, p0, Leet;->aV:Ljava/lang/String;

    move-object v4, p0

    move-object v8, v3

    move-object v9, v3

    move-object v10, v3

    invoke-virtual/range {v4 .. v12}, Leet;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)V

    goto/16 :goto_0

    :cond_4
    move v12, v11

    goto :goto_2

    :cond_5
    move-object v0, v3

    goto :goto_1
.end method

.method static synthetic b(Leet;Z)Z
    .locals 0

    .prologue
    .line 169
    iput-boolean p1, p0, Leet;->aW:Z

    return p1
.end method

.method private c(ZZ)Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 923
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 924
    const-string v1, "for_sharing"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 925
    const-string v1, "prefer_default_circle"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 926
    return-object v0
.end method

.method static synthetic c(Leet;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Leet;->aE:Ljava/lang/String;

    return-object v0
.end method

.method private c(ILfib;)V
    .locals 4

    .prologue
    .line 425
    iget-object v0, p0, Leet;->aN:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Leet;->aN:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_1

    .line 445
    :cond_0
    :goto_0
    return-void

    .line 429
    :cond_1
    invoke-virtual {p0}, Leet;->aE()V

    .line 431
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lfib;->e()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p2}, Lfib;->d()Ljava/lang/Exception;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 432
    :cond_2
    iget-boolean v0, p0, Leet;->ak:Z

    if-nez v0, :cond_3

    .line 433
    invoke-virtual {p0}, Leet;->n()Lz;

    move-result-object v0

    const v1, 0x7f0a0592

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Llih;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 434
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 443
    :cond_3
    :goto_1
    const/4 v0, 0x0

    iput-object v0, p0, Leet;->aN:Ljava/lang/Integer;

    .line 444
    invoke-virtual {p0}, Leet;->ay()V

    goto :goto_0

    .line 438
    :cond_4
    invoke-virtual {p0}, Leet;->n()Lz;

    move-result-object v0

    iget-object v1, p0, Leet;->R:Lhee;

    .line 439
    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    iget-object v2, p0, Leet;->aV:Ljava/lang/String;

    const/4 v3, 0x1

    .line 438
    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/service/EsService;->f(Landroid/content/Context;ILjava/lang/String;Z)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leet;->aK:Ljava/lang/Integer;

    .line 440
    invoke-virtual {p0}, Leet;->ay()V

    goto :goto_1
.end method

.method private c(I[B[B)V
    .locals 4

    .prologue
    .line 1323
    iget-object v0, p0, Leet;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    .line 1324
    invoke-virtual {p0}, Leet;->n()Lz;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/google/android/apps/plus/phone/ProfileEditActivity;

    invoke-direct {v2, v1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v2, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "account_id"

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "profile_edit_view_type"

    invoke-virtual {v2, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "profile_edit_items_proto"

    invoke-virtual {v2, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    const-string v0, "profile_edit_roster_proto"

    invoke-virtual {v2, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    const-string v0, "profile_data_id"

    invoke-virtual {v2, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1326
    const/16 v0, 0x6a

    invoke-virtual {p0, v2, v0}, Leet;->a(Landroid/content/Intent;I)V

    .line 1327
    invoke-direct {p0, p1}, Leet;->i(I)V

    .line 1328
    return-void
.end method

.method static synthetic c(Leet;Z)Z
    .locals 0

    .prologue
    .line 169
    iput-boolean p1, p0, Leet;->be:Z

    return p1
.end method

.method static synthetic d(Leet;)Lfba;
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Leet;->az:Lfba;

    return-object v0
.end method

.method private d(ILfib;)V
    .locals 3

    .prologue
    .line 2292
    iget-object v0, p0, Leet;->aL:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Leet;->aL:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_1

    .line 2310
    :cond_0
    :goto_0
    return-void

    .line 2296
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Leet;->aL:Ljava/lang/Integer;

    .line 2298
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lfib;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2299
    invoke-virtual {p0}, Leet;->o()Landroid/content/res/Resources;

    move-result-object v0

    .line 2300
    invoke-virtual {p2}, Lfib;->c()I

    move-result v1

    const/16 v2, 0x194

    if-eq v1, v2, :cond_2

    .line 2301
    const v1, 0x7f0a07ad

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2302
    iget-boolean v1, p0, Leet;->ak:Z

    if-nez v1, :cond_2

    .line 2304
    invoke-virtual {p0}, Leet;->n()Lz;

    move-result-object v1

    const/4 v2, 0x0

    .line 2303
    invoke-static {v1, v0, v2}, Llih;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 2304
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2309
    :cond_2
    invoke-virtual {p0}, Leet;->ay()V

    goto :goto_0
.end method

.method static synthetic d(Leet;Z)Z
    .locals 0

    .prologue
    .line 169
    iput-boolean p1, p0, Leet;->aY:Z

    return p1
.end method

.method static synthetic e(Leet;)I
    .locals 1

    .prologue
    .line 169
    iget v0, p0, Leet;->aC:I

    return v0
.end method

.method static synthetic e(Leet;Z)Z
    .locals 0

    .prologue
    .line 169
    iput-boolean p1, p0, Leet;->aZ:Z

    return p1
.end method

.method private f(Landroid/content/Intent;)V
    .locals 5

    .prologue
    .line 838
    :try_start_0
    invoke-virtual {p0, p1}, Leet;->a(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 844
    :cond_0
    :goto_0
    return-void

    .line 839
    :catch_0
    move-exception v0

    .line 840
    const-string v1, "HostedProfileFragment"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 841
    const-string v1, "HostedProfileFragment"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x18

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Cannot launch activity: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method static synthetic f(Leet;)Z
    .locals 1

    .prologue
    .line 169
    iget-boolean v0, p0, Leet;->aW:Z

    return v0
.end method

.method static synthetic f(Leet;Z)Z
    .locals 0

    .prologue
    .line 169
    iput-boolean p1, p0, Leet;->ba:Z

    return p1
.end method

.method static synthetic g(Leet;)Llfn;
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Leet;->bi:Llfn;

    return-object v0
.end method

.method static synthetic g(Leet;Z)Z
    .locals 0

    .prologue
    .line 169
    iput-boolean p1, p0, Leet;->aU:Z

    return p1
.end method

.method static synthetic h(Leet;)I
    .locals 1

    .prologue
    .line 169
    iget v0, p0, Leet;->bc:I

    return v0
.end method

.method private i(I)V
    .locals 5

    .prologue
    .line 1349
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1350
    const-string v0, "extra_profile_edit_field"

    invoke-virtual {v1, v0, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1351
    iget-object v0, p0, Leet;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v2

    .line 1352
    iget-object v0, p0, Leet;->au:Llnh;

    const-class v3, Lhms;

    invoke-virtual {v0, v3}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v3, Lhmr;

    iget-object v4, p0, Leet;->at:Llnl;

    invoke-direct {v3, v4, v2}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v2, Lhmv;->y:Lhmv;

    .line 1354
    invoke-virtual {v3, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v2

    .line 1355
    invoke-virtual {v2, v1}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    .line 1352
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 1357
    return-void
.end method

.method static synthetic i(Leet;)Z
    .locals 1

    .prologue
    .line 169
    iget-boolean v0, p0, Leet;->aX:Z

    return v0
.end method

.method static synthetic j(Leet;)V
    .locals 3

    .prologue
    .line 169
    invoke-virtual {p0}, Leet;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "local_folders_only"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    new-instance v1, Leez;

    invoke-direct {v1, p0, v0}, Leez;-><init>(Leet;Z)V

    invoke-static {v1}, Llsx;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic k(Leet;)V
    .locals 3

    .prologue
    .line 169
    invoke-virtual {p0}, Leet;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "local_folders_only"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    new-instance v1, Lefa;

    invoke-direct {v1, p0, v0}, Lefa;-><init>(Leet;Z)V

    invoke-static {v1}, Llsx;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method private l(Z)V
    .locals 4

    .prologue
    .line 1383
    iget-object v0, p0, Leet;->at:Llnl;

    iget-object v1, p0, Leet;->R:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    iget-object v2, p0, Leet;->aV:Ljava/lang/String;

    iget-object v3, p0, Leet;->az:Lfba;

    .line 1384
    invoke-virtual {v3}, Lfba;->y()Ljava/lang/String;

    move-result-object v3

    .line 1383
    invoke-static {v0, v1, v2, v3, p1}, Lcom/google/android/apps/plus/service/EsService;->d(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leet;->aQ:Ljava/lang/Integer;

    .line 1385
    iget-object v0, p0, Leet;->az:Lfba;

    invoke-virtual {v0}, Lfba;->q()V

    .line 1388
    if-eqz p1, :cond_1

    .line 1389
    iget-object v0, p0, Leet;->az:Lfba;

    invoke-virtual {v0}, Lfba;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0a084e

    move v1, v0

    .line 1392
    :goto_0
    iget-object v0, p0, Leet;->au:Llnh;

    const-class v2, Lhms;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    iget-object v3, p0, Leet;->at:Llnl;

    invoke-direct {v2, v3}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v3, Lhmv;->bM:Lhmv;

    .line 1393
    invoke-virtual {v2, v3}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v2

    .line 1392
    invoke-interface {v0, v2}, Lhms;->a(Lhmr;)V

    .line 1402
    :goto_1
    invoke-virtual {p0, v1}, Leet;->g(I)V

    .line 1403
    return-void

    .line 1389
    :cond_0
    const v0, 0x7f0a084d

    move v1, v0

    goto :goto_0

    .line 1395
    :cond_1
    iget-object v0, p0, Leet;->az:Lfba;

    invoke-virtual {v0}, Lfba;->t()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f0a0854

    move v1, v0

    .line 1398
    :goto_2
    iget-object v0, p0, Leet;->au:Llnh;

    const-class v2, Lhms;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    iget-object v3, p0, Leet;->at:Llnl;

    invoke-direct {v2, v3}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v3, Lhmv;->bN:Lhmv;

    .line 1399
    invoke-virtual {v2, v3}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v2

    .line 1398
    invoke-interface {v0, v2}, Lhms;->a(Lhmr;)V

    goto :goto_1

    .line 1395
    :cond_2
    const v0, 0x7f0a0853

    move v1, v0

    goto :goto_2
.end method

.method static synthetic l(Leet;)Z
    .locals 1

    .prologue
    .line 169
    sget-object v0, Leet;->N:Llpa;

    const/4 v0, 0x1

    return v0
.end method

.method private m(Z)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2576
    const-string v0, "change_photo"

    invoke-direct {p0, v0}, Leet;->s(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2589
    :goto_0
    return-void

    .line 2579
    :cond_0
    new-instance v4, Ldyu;

    const v0, 0x7f0a08e5

    invoke-direct {v4, v0}, Ldyu;-><init>(I)V

    .line 2581
    invoke-virtual {p0}, Leet;->n()Lz;

    move-result-object v0

    invoke-static {v0}, Leyq;->b(Landroid/content/Context;)Z

    move-result v0

    invoke-virtual {v4, v0}, Ldyu;->i(Z)V

    .line 2582
    iget-object v0, p0, Leet;->az:Lfba;

    invoke-virtual {v0}, Lfba;->D()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iget-object v3, p0, Leet;->az:Lfba;

    .line 2583
    invoke-virtual {v3}, Lfba;->B()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Leet;->az:Lfba;

    .line 2584
    invoke-virtual {v3}, Lfba;->J()Ljava/lang/Long;

    move-result-object v3

    .line 2582
    :goto_2
    invoke-virtual {v4, v1, v0, v3}, Ldyu;->a(ZZLjava/lang/Long;)V

    .line 2586
    invoke-virtual {v4, p1}, Ldyu;->j(Z)V

    .line 2587
    invoke-virtual {v4, p0, v2}, Ldyu;->a(Lu;I)V

    .line 2588
    invoke-virtual {p0}, Leet;->p()Lae;

    move-result-object v0

    const-string v1, "change_photo"

    invoke-virtual {v4, v0, v1}, Ldyu;->a(Lae;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move v0, v2

    .line 2582
    goto :goto_1

    .line 2584
    :cond_2
    const/4 v3, 0x0

    goto :goto_2
.end method

.method static synthetic m(Leet;)Z
    .locals 1

    .prologue
    .line 169
    iget-boolean v0, p0, Leet;->aU:Z

    return v0
.end method

.method static synthetic n(Leet;)J
    .locals 2

    .prologue
    .line 169
    iget-wide v0, p0, Leet;->bl:J

    return-wide v0
.end method

.method static synthetic o(Leet;)Llnl;
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Leet;->at:Llnl;

    return-object v0
.end method

.method static synthetic p(Leet;)Llnl;
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Leet;->at:Llnl;

    return-object v0
.end method

.method private s(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2419
    invoke-virtual {p0}, Leet;->p()Lae;

    move-result-object v0

    invoke-virtual {v0, p1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lt;

    .line 2420
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public A()V
    .locals 2

    .prologue
    .line 738
    invoke-virtual {p0}, Leet;->n()Lz;

    move-result-object v0

    iget-object v1, p0, Leet;->aa:Ljava/lang/String;

    invoke-static {v0, v1}, Lejf;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 739
    invoke-super {p0}, Lehh;->A()V

    .line 740
    return-void
.end method

.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 2901
    iget v0, p0, Leet;->bc:I

    packed-switch v0, :pswitch_data_0

    .line 2912
    sget-object v0, Lhmw;->j:Lhmw;

    :goto_0
    return-object v0

    .line 2903
    :pswitch_0
    sget-object v0, Lhmw;->g:Lhmw;

    goto :goto_0

    .line 2905
    :pswitch_1
    sget-object v0, Lhmw;->h:Lhmw;

    goto :goto_0

    .line 2907
    :pswitch_2
    sget-object v0, Lhmw;->l:Lhmw;

    goto :goto_0

    .line 2909
    :pswitch_3
    sget-object v0, Lhmw;->i:Lhmw;

    goto :goto_0

    .line 2901
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public H_()V
    .locals 0

    .prologue
    .line 3025
    return-void
.end method

.method public I_()V
    .locals 0

    .prologue
    .line 3017
    return-void
.end method

.method protected U()Z
    .locals 1

    .prologue
    .line 724
    iget-boolean v0, p0, Leet;->aX:Z

    return v0
.end method

.method protected V()Z
    .locals 1

    .prologue
    .line 744
    const/4 v0, 0x0

    return v0
.end method

.method public X()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 807
    const/4 v0, 0x0

    iget-object v1, p0, Leet;->aa:Ljava/lang/String;

    iget-object v2, p0, Leet;->az:Lfba;

    .line 808
    invoke-virtual {v2}, Lfba;->D()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ALBUM"

    .line 807
    invoke-static {v0, v1, v2, v3}, Ljvj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 810
    iget-object v1, p0, Leet;->R:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    .line 811
    new-instance v2, Ldew;

    .line 812
    invoke-virtual {p0}, Leet;->n()Lz;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Ldew;-><init>(Landroid/content/Context;I)V

    const/4 v1, 0x3

    new-array v3, v5, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    .line 813
    invoke-static {v1, v3}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ldew;->d(Ljava/lang/String;)Ldew;

    move-result-object v0

    iget-object v1, p0, Leet;->az:Lfba;

    .line 814
    invoke-virtual {v1}, Lfba;->K()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldew;->b(Ljava/lang/String;)Ldew;

    move-result-object v0

    .line 816
    invoke-virtual {p0}, Leet;->n()Lz;

    move-result-object v1

    iget-object v2, p0, Leet;->az:Lfba;

    invoke-virtual {v2}, Lfba;->L()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljac;->a:Ljac;

    invoke-static {v1, v2, v3}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v1

    .line 815
    invoke-virtual {v0, v1}, Ldew;->a(Lizu;)Ldew;

    move-result-object v0

    .line 817
    invoke-virtual {v0, v5}, Ldew;->f(Z)Ldew;

    move-result-object v0

    .line 818
    invoke-virtual {v0}, Ldew;->a()Landroid/content/Intent;

    move-result-object v1

    .line 819
    iget-object v0, p0, Leet;->au:Llnh;

    const-class v2, Lhms;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    iget-object v3, p0, Leet;->at:Llnl;

    invoke-direct {v2, v3}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v3, Lhmv;->dZ:Lhmv;

    .line 820
    invoke-virtual {v2, v3}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v2

    .line 819
    invoke-interface {v0, v2}, Lhms;->a(Lhmr;)V

    .line 821
    invoke-virtual {p0, v1}, Leet;->a(Landroid/content/Intent;)V

    .line 822
    return-void
.end method

.method public a()I
    .locals 1

    .prologue
    .line 2978
    const/16 v0, 0x8a

    return v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 21

    .prologue
    .line 1742
    move-object/from16 v0, p0

    iget-object v3, v0, Llol;->at:Llnl;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 1743
    sget v4, Leet;->O:I

    if-nez v4, :cond_0

    .line 1744
    invoke-virtual/range {p0 .. p0}, Lu;->o()Landroid/content/res/Resources;

    move-result-object v4

    .line 1745
    const v5, 0x7f0d032f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    sput v4, Leet;->O:I

    .line 1749
    :cond_0
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-super {v0, v3, v1, v2}, Lehh;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v3

    move-object/from16 v19, v3

    check-cast v19, Landroid/view/ViewGroup;

    .line 1750
    const v3, 0x7f1005ee

    .line 1751
    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    .line 1752
    new-instance v20, Lcom/google/android/libraries/social/ui/tabbar/TabBar;

    move-object/from16 v0, p0

    iget-object v4, v0, Llol;->at:Llnl;

    move-object/from16 v0, v20

    invoke-direct {v0, v4}, Lcom/google/android/libraries/social/ui/tabbar/TabBar;-><init>(Landroid/content/Context;)V

    .line 1753
    invoke-virtual/range {v20 .. v20}, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->b()V

    .line 1754
    const/4 v4, 0x4

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1755
    invoke-virtual/range {v20 .. v21}, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->a(Llhl;)V

    .line 1756
    move-object/from16 v0, p0

    iget v4, v0, Leet;->bc:I

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->a(I)V

    .line 1757
    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v5, -0x1

    sget v6, Leet;->O:I

    invoke-direct {v4, v5, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 1759
    move-object/from16 v0, v20

    invoke-virtual {v3, v0, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1761
    move-object/from16 v0, p0

    iget-object v3, v0, Lehh;->W:Lfdj;

    check-cast v3, Lfba;

    move-object/from16 v0, p0

    iput-object v3, v0, Leet;->az:Lfba;

    .line 1762
    move-object/from16 v0, p0

    iget-object v3, v0, Leet;->az:Lfba;

    move-object/from16 v0, p0

    iget-object v4, v0, Leet;->aV:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v5, v0, Leet;->aX:Z

    move-object/from16 v0, p0

    iget-boolean v6, v0, Leet;->aW:Z

    invoke-virtual/range {p0 .. p0}, Leet;->ap()Z

    move-object/from16 v0, p0

    iget-object v7, v0, Leet;->bb:Lhxh;

    move-object/from16 v0, p0

    iget-object v8, v0, Leet;->ay:Lequ;

    invoke-virtual/range {v3 .. v8}, Lfba;->a(Ljava/lang/String;ZZLhxh;Lequ;)V

    .line 1764
    move-object/from16 v0, p0

    iget-object v3, v0, Leet;->az:Lfba;

    move-object/from16 v4, p0

    move-object/from16 v5, p0

    move-object/from16 v6, p0

    move-object/from16 v7, p0

    move-object/from16 v8, p0

    move-object/from16 v9, p0

    move-object/from16 v10, p0

    move-object/from16 v11, p0

    move-object/from16 v12, p0

    move-object/from16 v13, p0

    move-object/from16 v14, p0

    move-object/from16 v15, p0

    move-object/from16 v16, p0

    move-object/from16 v17, p0

    move-object/from16 v18, p0

    invoke-virtual/range {v3 .. v18}, Lfba;->a(Lfyz;Llhl;Ljnt;Ljni;Ljmv;Ljms;Ljmx;Lgas;Ljnj;Ljmi;Ljmy;Ljnk;Ljns;Lfzc;Lfyv;)V

    .line 1766
    sget-object v3, Leet;->N:Llpa;

    .line 1767
    move-object/from16 v0, p0

    iget-object v3, v0, Leet;->az:Lfba;

    iget-object v3, v3, Lfba;->a:Levi;

    move-object/from16 v0, p0

    invoke-virtual {v3, v0}, Lfbx;->a(Landroid/view/View$OnClickListener;)V

    .line 1769
    move-object/from16 v0, p0

    iget-object v3, v0, Leet;->aQ:Ljava/lang/Integer;

    if-eqz v3, :cond_1

    .line 1770
    move-object/from16 v0, p0

    iget-object v3, v0, Leet;->az:Lfba;

    invoke-virtual {v3}, Lfba;->q()V

    .line 1772
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Leet;->az:Lfba;

    move-object/from16 v0, v20

    invoke-virtual {v3, v0}, Lfba;->a(Lcom/google/android/libraries/social/ui/tabbar/TabBar;)V

    .line 1773
    move-object/from16 v0, p0

    iget-object v3, v0, Leet;->az:Lfba;

    move-object/from16 v0, p0

    iget v4, v0, Leet;->bc:I

    invoke-virtual {v3, v4}, Lfba;->e(I)V

    .line 1775
    move-object/from16 v0, p0

    iget-boolean v3, v0, Leet;->aJ:Z

    if-eqz v3, :cond_2

    .line 1776
    new-instance v3, Leoy;

    .line 1777
    invoke-virtual/range {p0 .. p0}, Lu;->n()Lz;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Llol;->at:Llnl;

    move-object/from16 v0, p0

    iget-object v6, v0, Leet;->az:Lfba;

    move-object/from16 v0, p0

    iget-object v7, v0, Lehh;->S:Llhd;

    const v8, 0x7f1005ec

    .line 1779
    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/FrameLayout;

    const v9, 0x7f1005ed

    .line 1780
    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    invoke-direct/range {v3 .. v9}, Leoy;-><init>(Landroid/app/Activity;Landroid/content/Context;Lfba;Llhd;Landroid/widget/FrameLayout;Landroid/view/View;)V

    .line 1781
    invoke-virtual {v3}, Leoy;->a()V

    .line 1784
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lehh;->U:Licq;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Licq;->a(Z)Licq;

    move-result-object v3

    const/4 v4, 0x0

    .line 1785
    invoke-virtual {v3, v4}, Licq;->b(Z)Licq;

    move-result-object v3

    new-instance v4, Lefb;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lefb;-><init>(Leet;)V

    .line 1786
    invoke-virtual {v3, v4}, Licq;->a(Lico;)Licq;

    .line 1800
    return-object v19
.end method

.method protected a(II)Ldid;
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 2970
    .line 2971
    invoke-virtual {p0}, Leet;->a()I

    move-result v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 2970
    invoke-static {v2, v0, v1}, Ldib;->a(IILjava/lang/Integer;)Ldid;

    move-result-object v0

    return-object v0
.end method

.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x3

    const/4 v5, 0x0

    .line 1805
    packed-switch p1, :pswitch_data_0

    .line 1832
    :cond_0
    :pswitch_0
    const-string v0, "HostedProfileFragment"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1833
    if-ne p1, v2, :cond_3

    const-string v0, "POSTS_LOADER_ID"

    .line 1834
    :goto_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x23

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Loader<Cursor> onCreateLoader() -- "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1833
    :cond_1
    invoke-super {p0, p1, p2}, Lehh;->a(ILandroid/os/Bundle;)Ldo;

    move-result-object v0

    :goto_1
    return-object v0

    .line 1807
    :pswitch_1
    sget-object v0, Leet;->N:Llpa;

    .line 1808
    new-instance v0, Levl;

    iget-object v1, p0, Leet;->at:Llnl;

    iget-object v2, p0, Leet;->R:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    iget-object v4, p0, Leet;->aV:Ljava/lang/String;

    .line 1810
    invoke-static {v4}, Ljpu;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    .line 1809
    invoke-static {v5, v3}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/16 v6, 0x20

    invoke-direct/range {v0 .. v6}, Levl;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;ZI)V

    goto :goto_1

    .line 1814
    :pswitch_2
    iget-object v0, p0, Leet;->at:Llnl;

    const-class v1, Lhxn;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhxn;

    .line 1822
    iget-boolean v1, p0, Leet;->aX:Z

    if-eqz v1, :cond_2

    .line 1823
    iget-object v1, p0, Leet;->at:Llnl;

    iget-object v1, p0, Leet;->R:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    invoke-interface {v0}, Lhxn;->a()Lhye;

    move-result-object v0

    goto :goto_1

    .line 1824
    :cond_2
    iget-boolean v1, p0, Leet;->aW:Z

    if-eqz v1, :cond_0

    .line 1825
    iget-object v1, p0, Leet;->at:Llnl;

    iget-object v1, p0, Leet;->R:Lhee;

    .line 1826
    invoke-interface {v1}, Lhee;->d()I

    iget-object v1, p0, Leet;->aa:Ljava/lang/String;

    .line 1825
    invoke-interface {v0}, Lhxn;->b()Lhye;

    move-result-object v0

    goto :goto_1

    .line 1834
    :cond_3
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 1805
    nop

    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected a(Landroid/content/Context;Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;Llcr;ILfdn;Lfdp;Levp;Llci;)Lfdj;
    .locals 9

    .prologue
    .line 1725
    new-instance v0, Lfba;

    new-instance v6, Lehp;

    invoke-direct {v6, p0}, Lehp;-><init>(Lehh;)V

    const/4 v7, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v8}, Lfba;-><init>(Landroid/content/Context;Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;Llcr;ILfdn;Lfdp;Levp;Llci;)V

    return-object v0
.end method

.method public a(I)V
    .locals 10

    .prologue
    .line 2455
    iget-object v0, p0, Leet;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v1

    .line 2456
    iget v0, p0, Leet;->aR:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_3

    .line 2457
    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    .line 2459
    invoke-virtual {p0}, Leet;->n()Lz;

    move-result-object v0

    const/4 v2, 0x1

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    .line 2460
    invoke-static {v2, v3}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/16 v8, 0xfa

    const/16 v9, 0xfa

    .line 2458
    invoke-static/range {v0 .. v9}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;IIZILjava/lang/Integer;II)Landroid/content/Intent;

    move-result-object v0

    .line 2465
    const/16 v1, 0x67

    invoke-virtual {p0, v0, v1}, Leet;->a(Landroid/content/Intent;I)V

    .line 2543
    :cond_0
    :goto_0
    return-void

    .line 2467
    :cond_1
    invoke-virtual {p0}, Leet;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "local_folders_only"

    const/4 v3, 0x0

    .line 2468
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 2469
    invoke-virtual {p0}, Leet;->n()Lz;

    move-result-object v2

    invoke-static {v2, v1}, Leyq;->f(Landroid/content/Context;I)Leyt;

    move-result-object v1

    const/4 v2, 0x1

    .line 2471
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Leyt;->a(Ljava/lang/Integer;)Leyt;

    move-result-object v1

    const/4 v2, 0x1

    .line 2472
    invoke-virtual {v1, v2}, Leyt;->b(I)Leyt;

    move-result-object v1

    const/16 v2, 0xfa

    .line 2473
    invoke-virtual {v1, v2}, Leyt;->c(I)Leyt;

    move-result-object v1

    const/16 v2, 0xfa

    .line 2474
    invoke-virtual {v1, v2}, Leyt;->d(I)Leyt;

    move-result-object v1

    const/4 v2, 0x1

    .line 2475
    invoke-virtual {v1, v2}, Leyt;->b(Z)Leyt;

    move-result-object v1

    const/4 v2, 0x1

    .line 2476
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Leyt;->b(Ljava/lang/Integer;)Leyt;

    move-result-object v1

    if-eqz v0, :cond_2

    const/4 v0, 0x2

    .line 2477
    :goto_1
    invoke-virtual {v1, v0}, Leyt;->a(I)Leyt;

    move-result-object v0

    .line 2480
    invoke-virtual {v0}, Leyt;->a()Landroid/content/Intent;

    move-result-object v0

    .line 2481
    const/16 v1, 0x67

    invoke-virtual {p0, v0, v1}, Leet;->a(Landroid/content/Intent;I)V

    goto :goto_0

    .line 2476
    :cond_2
    const/16 v0, 0x16

    goto :goto_1

    .line 2483
    :cond_3
    iget v0, p0, Leet;->aR:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    .line 2485
    invoke-virtual {p0}, Leet;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "local_folders_only"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 2486
    iput p1, p0, Leet;->aS:I

    .line 2487
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 2490
    :pswitch_1
    invoke-virtual {p0}, Leet;->n()Lz;

    move-result-object v2

    .line 2489
    invoke-static {v2, v1}, Leyq;->f(Landroid/content/Context;I)Leyt;

    move-result-object v1

    const/4 v2, 0x1

    .line 2491
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Leyt;->a(Ljava/lang/Integer;)Leyt;

    move-result-object v1

    const/4 v2, 0x3

    .line 2492
    invoke-virtual {v1, v2}, Leyt;->b(I)Leyt;

    move-result-object v1

    const/16 v2, 0x1e0

    .line 2493
    invoke-virtual {v1, v2}, Leyt;->c(I)Leyt;

    move-result-object v1

    const/16 v2, 0x10e

    .line 2494
    invoke-virtual {v1, v2}, Leyt;->d(I)Leyt;

    move-result-object v1

    const/4 v2, 0x1

    .line 2495
    invoke-virtual {v1, v2}, Leyt;->b(Z)Leyt;

    move-result-object v1

    const/4 v2, 0x1

    .line 2496
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Leyt;->b(Ljava/lang/Integer;)Leyt;

    move-result-object v1

    if-eqz v0, :cond_4

    const/4 v0, 0x2

    .line 2497
    :goto_2
    invoke-virtual {v1, v0}, Leyt;->a(I)Leyt;

    move-result-object v0

    .line 2500
    invoke-virtual {v0}, Leyt;->a()Landroid/content/Intent;

    move-result-object v0

    .line 2501
    const/16 v1, 0x68

    invoke-virtual {p0, v0, v1}, Leet;->a(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 2496
    :cond_4
    const/16 v0, 0x16

    goto :goto_2

    .line 2506
    :pswitch_2
    invoke-virtual {p0}, Leet;->n()Lz;

    move-result-object v0

    const/4 v2, 0x1

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    .line 2507
    invoke-static {v2, v3}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x3

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/16 v8, 0x1e0

    const/16 v9, 0x10e

    .line 2505
    invoke-static/range {v0 .. v9}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;IIZILjava/lang/Integer;II)Landroid/content/Intent;

    move-result-object v0

    .line 2512
    const/16 v1, 0x68

    invoke-virtual {p0, v0, v1}, Leet;->a(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 2516
    :pswitch_3
    const/4 v0, 0x0

    const-string v2, "115239603441691718952"

    const-string v3, "5745127577944303633"

    const-string v4, "ALBUM"

    invoke-static {v0, v2, v3, v4}, Ljvj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2521
    invoke-virtual {p0}, Leet;->n()Lz;

    move-result-object v0

    const/4 v3, 0x3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    .line 2522
    invoke-static {v3, v4}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/16 v8, 0x1e0

    const/16 v9, 0x10e

    .line 2520
    invoke-static/range {v0 .. v9}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;IZILjava/lang/Integer;ZII)Landroid/content/Intent;

    move-result-object v0

    .line 2527
    const/16 v1, 0x68

    invoke-virtual {p0, v0, v1}, Leet;->a(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 2531
    :pswitch_4
    iget-object v0, p0, Leet;->R:Lhee;

    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v0

    const-string v2, "gaia_id"

    invoke-interface {v0, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2532
    const/4 v2, 0x0

    iget-object v3, p0, Leet;->az:Lfba;

    .line 2533
    invoke-virtual {v3}, Lfba;->D()Ljava/lang/String;

    move-result-object v3

    const-string v4, "ALBUM"

    .line 2532
    invoke-static {v2, v0, v3, v4}, Ljvj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2536
    invoke-virtual {p0}, Leet;->n()Lz;

    move-result-object v0

    const/4 v3, 0x3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    .line 2537
    invoke-static {v3, v4}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/16 v8, 0x1e0

    const/16 v9, 0x10e

    .line 2535
    invoke-static/range {v0 .. v9}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;IZILjava/lang/Integer;ZII)Landroid/content/Intent;

    move-result-object v0

    .line 2542
    const/16 v1, 0x68

    invoke-virtual {p0, v0, v1}, Leet;->a(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 2487
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public a(IILandroid/content/Intent;)V
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    const/4 v5, 0x0

    const/4 v7, -0x1

    const/4 v1, 0x1

    const/4 v8, 0x0

    .line 2697
    if-eq p2, v7, :cond_1

    .line 2698
    const/16 v0, 0x6a

    if-ne p1, v0, :cond_0

    .line 2699
    sget-object v0, Lhmv;->A:Lhmv;

    invoke-direct {p0, v0, p3}, Leet;->a(Lhmv;Landroid/content/Intent;)V

    .line 2822
    :cond_0
    :goto_0
    return-void

    .line 2704
    :cond_1
    iget-object v0, p0, Leet;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v2

    .line 2706
    packed-switch p1, :pswitch_data_0

    .line 2818
    invoke-super {p0, p1, p2, p3}, Lehh;->a(IILandroid/content/Intent;)V

    goto :goto_0

    .line 2708
    :pswitch_0
    iget-object v0, p0, Leet;->bi:Llfn;

    iget-object v1, p0, Leet;->R:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    const-string v2, "AddToCircleFromProfile"

    const-wide/16 v10, 0x9

    invoke-interface {v0, v1, v2, v10, v11}, Llfn;->a(ILjava/lang/String;J)V

    .line 2710
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "original_circle_ids"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 2712
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "selected_circle_ids"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 2714
    iget-object v3, p0, Leet;->aV:Ljava/lang/String;

    iget-object v0, p0, Leet;->az:Lfba;

    .line 2715
    invoke-virtual {v0}, Lfba;->y()Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    move-object v6, v5

    .line 2714
    invoke-virtual/range {v0 .. v8}, Leet;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)V

    goto :goto_0

    .line 2722
    :pswitch_1
    if-eqz p3, :cond_0

    .line 2724
    const-string v0, "data"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    .line 2725
    if-eqz v0, :cond_0

    .line 2726
    iget-object v1, p0, Leet;->ax:Landroid/os/Handler;

    new-instance v2, Lefc;

    invoke-direct {v2, p0, v0}, Lefc;-><init>(Leet;[B)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 2738
    :pswitch_2
    if-eqz p3, :cond_0

    .line 2739
    new-instance v3, Lefh;

    invoke-direct {v3}, Lefh;-><init>()V

    .line 2740
    const-string v0, "coordinates"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    iput-object v0, v3, Lefh;->c:Landroid/graphics/RectF;

    .line 2741
    const-string v0, "photo_picker_rotation"

    invoke-virtual {p3, v0, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, v3, Lefh;->d:I

    .line 2742
    iget v0, p0, Leet;->aS:I

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    iput-boolean v0, v3, Lefh;->b:Z

    .line 2745
    const-string v0, "photo_id"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2746
    const-string v0, "photo_id"

    invoke-virtual {p3, v0, v10, v11}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v3, Lefh;->e:Ljava/lang/Long;

    .line 2747
    invoke-direct {p0, v3}, Leet;->a(Lefh;)V

    goto/16 :goto_0

    :cond_2
    move v0, v8

    .line 2742
    goto :goto_1

    .line 2749
    :cond_3
    const-string v0, "tile_id"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2750
    const-string v0, "tile_id"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lefh;->a:Ljava/lang/String;

    .line 2751
    new-instance v0, Lefi;

    .line 2752
    invoke-virtual {p0}, Leet;->n()Lz;

    move-result-object v1

    iget-object v2, p0, Leet;->R:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    const/16 v5, 0x68

    move-object v4, p0

    invoke-direct/range {v0 .. v5}, Lefi;-><init>(Landroid/content/Context;ILefh;Leet;I)V

    .line 2755
    const v1, 0x7f0a08ef

    invoke-virtual {p0, v1}, Leet;->g(I)V

    .line 2757
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-ge v1, v2, :cond_4

    .line 2758
    new-array v1, v8, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Lefi;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 2760
    :cond_4
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v8, [Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lefi;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 2765
    :cond_5
    const-string v0, "photo_url"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2766
    if-eqz v1, :cond_6

    .line 2767
    const-string v0, "coordinates"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    .line 2768
    invoke-virtual {p0, v1, v0}, Leet;->a(Ljava/lang/String;Landroid/graphics/RectF;)V

    .line 2770
    :cond_6
    iget-object v0, p0, Leet;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v3, p0, Leet;->at:Llnl;

    invoke-direct {v1, v3, v2}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v2, Lhmv;->G:Lhmv;

    .line 2772
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 2770
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    goto/16 :goto_0

    .line 2780
    :pswitch_3
    const-string v0, "photo_picker_crop_mode"

    invoke-virtual {p3, v0, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 2782
    const-string v1, "photo_id"

    invoke-virtual {p3, v1, v10, v11}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    .line 2783
    const/4 v1, 0x3

    if-ne v0, v1, :cond_8

    .line 2784
    const-string v0, "coordinates"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    .line 2786
    iget-object v1, p0, Leet;->az:Lfba;

    invoke-virtual {v1}, Lfba;->G()I

    move-result v1

    .line 2787
    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Leet;->az:Lfba;

    .line 2788
    invoke-virtual {v4}, Lfba;->H()I

    move-result v4

    .line 2786
    invoke-virtual {p0, v1, v3, v0, v4}, Leet;->a(ILjava/lang/String;Landroid/graphics/RectF;I)V

    .line 2795
    :cond_7
    :goto_2
    iget-object v0, p0, Leet;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v3, p0, Leet;->at:Llnl;

    invoke-direct {v1, v3, v2}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v2, Lhmv;->E:Lhmv;

    .line 2797
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 2795
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    goto/16 :goto_0

    .line 2789
    :cond_8
    const/4 v1, 0x2

    if-ne v0, v1, :cond_7

    .line 2790
    const-string v0, "top_offset"

    invoke-virtual {p3, v0, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 2791
    iget-object v1, p0, Leet;->az:Lfba;

    invoke-virtual {v1}, Lfba;->G()I

    move-result v1

    .line 2792
    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    .line 2791
    invoke-virtual {p0, v1, v3, v0}, Leet;->a(ILjava/lang/String;I)V

    goto :goto_2

    .line 2804
    :pswitch_4
    iget-object v0, p0, Leet;->at:Llnl;

    iget-object v2, p0, Leet;->R:Lhee;

    .line 2805
    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    iget-object v3, p0, Leet;->aV:Ljava/lang/String;

    .line 2804
    invoke-static {v0, v2, v3, v1}, Lcom/google/android/apps/plus/service/EsService;->f(Landroid/content/Context;ILjava/lang/String;Z)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leet;->aK:Ljava/lang/Integer;

    .line 2806
    invoke-virtual {p0}, Leet;->ay()V

    .line 2807
    sget-object v0, Lhmv;->z:Lhmv;

    invoke-direct {p0, v0, p3}, Leet;->a(Lhmv;Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 2813
    :pswitch_5
    invoke-virtual {p0}, Leet;->am()V

    goto/16 :goto_0

    .line 2706
    nop

    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public a(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2624
    return-void
.end method

.method protected a(ILfib;)V
    .locals 3

    .prologue
    .line 2201
    iget-object v0, p0, Leet;->aK:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Leet;->aK:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_1

    .line 2216
    :cond_0
    :goto_0
    return-void

    .line 2205
    :cond_1
    invoke-virtual {p0}, Leet;->aE()V

    .line 2207
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lfib;->e()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p2}, Lfib;->d()Ljava/lang/Exception;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2208
    :cond_2
    iget-boolean v0, p0, Leet;->ak:Z

    if-nez v0, :cond_3

    .line 2209
    invoke-virtual {p0}, Leet;->n()Lz;

    move-result-object v0

    const v1, 0x7f0a0592

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Llih;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 2210
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2214
    :cond_3
    const/4 v0, 0x0

    iput-object v0, p0, Leet;->aK:Ljava/lang/Integer;

    .line 2215
    invoke-virtual {p0}, Leet;->ay()V

    goto :goto_0
.end method

.method public a(ILjava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 1227
    iget-object v0, p0, Leet;->aF:Ljava/lang/String;

    iget-object v1, p0, Leet;->aE:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1228
    iget-object v0, p0, Leet;->aE:Ljava/lang/String;

    iput-object v0, p0, Leet;->aF:Ljava/lang/String;

    .line 1229
    invoke-virtual {p0}, Leet;->n()Lz;

    move-result-object v0

    iget-object v1, p0, Leet;->aa:Ljava/lang/String;

    invoke-static {v0, v1}, Lejf;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 1232
    :cond_0
    iget-object v0, p0, Leet;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v1

    .line 1233
    const/4 v0, 0x0

    .line 1234
    packed-switch p1, :pswitch_data_0

    .line 1250
    :goto_0
    if-eqz v0, :cond_1

    .line 1251
    invoke-virtual {p0, v0}, Leet;->a(Landroid/content/Intent;)V

    .line 1253
    :cond_1
    return-void

    .line 1236
    :pswitch_0
    invoke-virtual {p0}, Leet;->n()Lz;

    move-result-object v0

    iget-object v2, p0, Leet;->aa:Ljava/lang/String;

    invoke-static {v0, v1, p2, v2, v5}, Leyq;->d(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 1240
    :pswitch_1
    invoke-virtual {p0}, Leet;->n()Lz;

    move-result-object v0

    iget-object v2, p0, Leet;->aa:Ljava/lang/String;

    invoke-static {v0, v1, p2, v2, v5}, Leyq;->c(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 1245
    :pswitch_2
    invoke-virtual {p0}, Leet;->n()Lz;

    move-result-object v2

    iget-object v3, p0, Leet;->aa:Ljava/lang/String;

    new-instance v0, Landroid/content/Intent;

    const-class v4, Lcom/google/android/apps/plus/phone/PeopleListActivity;

    invoke-direct {v0, v2, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "account_id"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "people_view_type"

    const/16 v2, 0xa

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "title"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "owner_id"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "use_cached_data"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_0

    .line 1234
    nop

    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected a(ILjava/lang/String;I)V
    .locals 6

    .prologue
    .line 2680
    iget-object v0, p0, Leet;->az:Lfba;

    invoke-virtual {v0}, Lfba;->E()Ljava/lang/String;

    move-result-object v5

    .line 2681
    invoke-virtual {p0}, Leet;->n()Lz;

    move-result-object v0

    iget-object v1, p0, Leet;->R:Lhee;

    .line 2682
    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    move v2, p1

    move-object v3, p2

    move v4, p3

    .line 2681
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;IILjava/lang/String;ILjava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leet;->aN:Ljava/lang/Integer;

    .line 2684
    const v0, 0x7f0a08ef

    invoke-virtual {p0, v0}, Leet;->g(I)V

    .line 2685
    return-void
.end method

.method protected a(ILjava/lang/String;Landroid/graphics/RectF;I)V
    .locals 6

    .prologue
    .line 2668
    iget-object v0, p0, Leet;->at:Llnl;

    iget-object v1, p0, Leet;->R:Lhee;

    .line 2669
    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    .line 2668
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;IILjava/lang/String;Landroid/graphics/RectF;I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leet;->aN:Ljava/lang/Integer;

    .line 2671
    const v0, 0x7f0a08ef

    invoke-virtual {p0, v0}, Leet;->g(I)V

    .line 2672
    return-void
.end method

.method public a(IZLandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2628
    return-void
.end method

.method protected a(IZLfib;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2222
    iget-object v0, p0, Leet;->aO:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Leet;->aO:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_1

    .line 2241
    :cond_0
    :goto_0
    return-void

    .line 2226
    :cond_1
    invoke-virtual {p0}, Leet;->aE()V

    .line 2228
    const/4 v0, 0x0

    iput-object v0, p0, Leet;->aO:Ljava/lang/Integer;

    .line 2229
    invoke-virtual {p0}, Leet;->ay()V

    .line 2231
    if-eqz p3, :cond_2

    invoke-virtual {p3}, Lfib;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2232
    invoke-virtual {p0}, Leet;->n()Lz;

    move-result-object v0

    const v1, 0x7f0a0592

    invoke-static {v0, v1, v2}, Llih;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 2233
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 2234
    :cond_2
    if-eqz p2, :cond_3

    .line 2235
    invoke-virtual {p0}, Leet;->n()Lz;

    move-result-object v0

    const v1, 0x7f0a085f

    invoke-static {v0, v1, v2}, Llih;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 2236
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 2238
    :cond_3
    invoke-virtual {p0}, Leet;->n()Lz;

    move-result-object v0

    const v1, 0x7f0a0860

    invoke-static {v0, v1, v2}, Llih;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 2239
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 1450
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Leet;->bl:J

    .line 1451
    invoke-super {p0, p1}, Lehh;->a(Landroid/os/Bundle;)V

    .line 1452
    iget-object v0, p0, Leet;->bi:Llfn;

    iget-object v1, p0, Leet;->R:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    const-string v2, "ProfileLoad"

    const-wide/16 v4, 0x1

    invoke-interface {v0, v1, v2, v4, v5}, Llfn;->a(ILjava/lang/String;J)V

    .line 1455
    if-eqz p1, :cond_a

    .line 1456
    const-string v0, "profile_request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1457
    const-string v0, "profile_request_id"

    .line 1458
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leet;->aK:Ljava/lang/Integer;

    .line 1460
    :cond_0
    const-string v0, "abuse_request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1461
    const-string v0, "abuse_request_id"

    .line 1462
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leet;->aM:Ljava/lang/Integer;

    .line 1464
    :cond_1
    const-string v0, "mute_request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1465
    const-string v0, "mute_request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leet;->aO:Ljava/lang/Integer;

    .line 1466
    const-string v0, "mute_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Leet;->aP:Z

    .line 1468
    :cond_2
    const-string v0, "cover_photo_request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1469
    const-string v0, "cover_photo_request_id"

    .line 1470
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leet;->aN:Ljava/lang/Integer;

    .line 1472
    :cond_3
    const-string v0, "albums_request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1473
    const-string v0, "albums_request_id"

    .line 1474
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leet;->aL:Ljava/lang/Integer;

    .line 1476
    :cond_4
    const-string v0, "set_blocked_request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1477
    const-string v0, "set_blocked_request_id"

    .line 1478
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leet;->aQ:Ljava/lang/Integer;

    .line 1480
    :cond_5
    const-string v0, "choose_photo_target"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1481
    const-string v0, "choose_photo_target"

    .line 1482
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Leet;->aR:I

    .line 1483
    const-string v0, "choose_photo_album_hint"

    .line 1484
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Leet;->aS:I

    .line 1487
    :cond_6
    const-string v0, "already_tried_refresh"

    .line 1488
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Leet;->aT:Z

    .line 1490
    const-string v0, "contact_update"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Leet;->aD:J

    .line 1491
    const-string v0, "profile_update"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leet;->aE:Ljava/lang/String;

    .line 1493
    const-string v0, "active_tab"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Leet;->bc:I

    .line 1496
    const/4 v0, 0x0

    iput-boolean v0, p0, Leet;->aJ:Z

    .line 1497
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Leet;->bl:J

    .line 1531
    :cond_7
    :goto_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1532
    const-string v1, "person_id"

    iget-object v2, p0, Leet;->aV:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1533
    invoke-virtual {p0}, Leet;->w()Lbb;

    move-result-object v1

    const/16 v2, 0x64

    iget-object v3, p0, Leet;->bn:Lbc;

    invoke-virtual {v1, v2, v0, v3}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 1535
    iget-boolean v1, p0, Leet;->aW:Z

    if-eqz v1, :cond_8

    .line 1536
    invoke-virtual {p0}, Leet;->w()Lbb;

    move-result-object v1

    const/16 v2, 0x65

    invoke-virtual {v1, v2, v0, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 1537
    iget-object v0, p0, Leet;->bj:Lgcs;

    if-eqz v0, :cond_8

    iget-object v0, p0, Leet;->bj:Lgcs;

    iget-object v1, p0, Leet;->R:Lhee;

    .line 1538
    invoke-interface {v1}, Lhee;->d()I

    invoke-interface {v0}, Lgcs;->g()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1539
    invoke-virtual {p0}, Leet;->w()Lbb;

    move-result-object v0

    const/16 v1, 0x67

    invoke-virtual {v0, v1, v7, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 1543
    :cond_8
    new-instance v0, Lhxh;

    iget-object v1, p0, Leet;->at:Llnl;

    invoke-virtual {p0}, Leet;->w()Lbb;

    move-result-object v2

    iget-object v3, p0, Leet;->R:Lhee;

    .line 1544
    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lhxh;-><init>(Landroid/content/Context;Lbb;I)V

    iput-object v0, p0, Leet;->bb:Lhxh;

    .line 1545
    iget-object v0, p0, Leet;->bb:Lhxh;

    iget-object v1, p0, Leet;->bo:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lhxh;->a(Landroid/database/DataSetObserver;)V

    .line 1546
    iget-object v0, p0, Leet;->bb:Lhxh;

    invoke-virtual {v0}, Lhxh;->b()V

    .line 1550
    invoke-virtual {p0}, Leet;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v7, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 1551
    if-nez p1, :cond_9

    .line 1552
    invoke-virtual {p0, v6}, Leet;->j(Z)Z

    .line 1554
    :cond_9
    return-void

    .line 1500
    :cond_a
    iput-boolean v6, p0, Leet;->aJ:Z

    .line 1502
    iget-object v0, p0, Leet;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v2

    .line 1504
    invoke-virtual {p0}, Leet;->n()Lz;

    move-result-object v0

    .line 1505
    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 1506
    const-string v1, "add_profile_photo_message_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1507
    const-string v1, "add_profile_photo_message_id"

    const v3, 0x7f0a09df

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 1512
    const v1, 0x7f0a09dd

    if-eq v0, v1, :cond_c

    .line 1513
    const v1, 0x7f0a09de

    if-ne v0, v1, :cond_b

    .line 1516
    sget-object v0, Lhmv;->N:Lhmv;

    move-object v1, v0

    .line 1523
    :goto_1
    iget-object v0, p0, Leet;->au:Llnh;

    const-class v3, Lhms;

    invoke-virtual {v0, v3}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v3, Lhmr;

    iget-object v4, p0, Leet;->at:Llnl;

    invoke-direct {v3, v4, v2}, Lhmr;-><init>(Landroid/content/Context;I)V

    .line 1525
    invoke-virtual {v3, v1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    sget-object v2, Lhmw;->y:Lhmw;

    .line 1526
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmw;)Lhmr;

    move-result-object v1

    .line 1523
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    goto/16 :goto_0

    .line 1517
    :cond_b
    const v1, 0x7f0a09dc

    if-ne v0, v1, :cond_c

    .line 1519
    sget-object v0, Lhmv;->O:Lhmv;

    move-object v1, v0

    goto :goto_1

    .line 1521
    :cond_c
    sget-object v0, Lhmv;->M:Lhmv;

    move-object v1, v0

    goto :goto_1
.end method

.method public a(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2602
    const-string v0, "cover_photo_upgrade"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2604
    invoke-virtual {p0}, Leet;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "local_folders_only"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 2605
    invoke-direct {p0, v0}, Leet;->m(Z)V

    .line 2612
    :cond_0
    :goto_0
    return-void

    .line 2606
    :cond_1
    const-string v0, "first_circle_add"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "first_circle_add_one_click"

    .line 2607
    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2608
    :cond_2
    const-string v0, "for_sharing"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 2609
    const-string v1, "prefer_default_circle"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 2610
    invoke-direct {p0, v0, v1}, Leet;->b(ZZ)V

    goto :goto_0
.end method

.method public a(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 1361
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Leet;->l(Z)V

    .line 1362
    return-void
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1991
    return-void
.end method

.method public a(Ldo;Landroid/database/Cursor;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1848
    invoke-virtual {p1}, Ldo;->o()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 1979
    invoke-super {p0, p1, p2}, Lehh;->a(Ldo;Landroid/database/Cursor;)V

    .line 1981
    :cond_0
    :goto_0
    iget-object v0, p0, Leet;->az:Lfba;

    invoke-virtual {v0}, Lfba;->b()V

    .line 1982
    :cond_1
    :goto_1
    return-void

    .line 1850
    :sswitch_0
    if-eqz p2, :cond_2

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_3

    iget-boolean v0, p0, Leet;->aT:Z

    if-nez v0, :cond_3

    .line 1851
    :cond_2
    iget-object v0, p0, Leet;->aL:Ljava/lang/Integer;

    if-nez v0, :cond_1

    .line 1852
    iput-boolean v1, p0, Leet;->aT:Z

    .line 1853
    invoke-virtual {p0}, Leet;->an()V

    goto :goto_1

    .line 1860
    :cond_3
    sget-object v0, Leet;->N:Llpa;

    .line 1861
    :cond_4
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1880
    invoke-interface {p2, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const/4 v3, 0x4

    if-ne v0, v3, :cond_4

    move v0, v1

    .line 1882
    :goto_2
    iget-object v3, p0, Leet;->aG:Landroid/database/Cursor;

    if-eqz v3, :cond_5

    iget-object v3, p0, Leet;->aI:Ljava/lang/Integer;

    if-eqz v3, :cond_5

    iget-object v3, p0, Leet;->aI:Ljava/lang/Integer;

    .line 1888
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    .line 1890
    :cond_5
    const/16 v3, 0x11

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Leet;->aI:Ljava/lang/Integer;

    .line 1891
    sget-object v3, Leet;->N:Llpa;

    .line 1893
    if-eqz v0, :cond_6

    .line 1902
    :goto_3
    iget-object v0, p0, Leet;->az:Lfba;

    iget-object v0, v0, Lfba;->a:Levi;

    invoke-virtual {v0, p2}, Levi;->b(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 1903
    iget-object v0, p0, Leet;->az:Lfba;

    iget-object v0, v0, Lfba;->a:Levi;

    invoke-virtual {v0}, Levi;->a()Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Leet;->aG:Landroid/database/Cursor;

    .line 1904
    iget v0, p0, Leet;->bc:I

    if-ne v0, v4, :cond_0

    .line 1910
    sget-object v0, Leet;->N:Llpa;

    .line 1911
    iget-object v0, p0, Leet;->W:Lfdj;

    invoke-virtual {v0}, Lfdj;->i()Z

    move-result v0

    .line 1912
    iget-object v3, p0, Leet;->az:Lfba;

    invoke-virtual {v3, v2}, Lfba;->k(Z)V

    .line 1913
    iget-object v2, p0, Leet;->az:Lfba;

    invoke-virtual {p0}, Leet;->ak()Lhym;

    move-result-object v3

    invoke-virtual {v2, v3}, Lfba;->a(Landroid/database/Cursor;)V

    .line 1914
    iget-object v2, p0, Leet;->az:Lfba;

    iget-object v3, p0, Leet;->aG:Landroid/database/Cursor;

    invoke-virtual {v2, v3, v1}, Lfba;->b(Landroid/database/Cursor;I)V

    .line 1915
    iget-object v1, p0, Leet;->az:Lfba;

    invoke-virtual {v1, v0}, Lfba;->k(Z)V

    .line 1916
    iget-object v0, p0, Leet;->az:Lfba;

    invoke-virtual {v0}, Lfba;->h()V

    .line 1917
    iget-object v0, p0, Leet;->U:Licq;

    sget-object v1, Lict;->b:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    .line 1931
    invoke-virtual {p0}, Leet;->ay()V

    goto/16 :goto_0

    .line 1895
    :cond_6
    iget-object v0, p0, Leet;->aL:Ljava/lang/Integer;

    if-nez v0, :cond_7

    .line 1898
    sget-object p2, Leet;->aw:Landroid/database/Cursor;

    goto :goto_3

    .line 1900
    :cond_7
    const/4 p2, 0x0

    goto :goto_3

    .line 1937
    :sswitch_1
    iput-object p2, p0, Leet;->aH:Landroid/database/Cursor;

    .line 1938
    iget v0, p0, Leet;->bc:I

    const/4 v3, 0x5

    if-ne v0, v3, :cond_0

    .line 1939
    iget-object v0, p0, Leet;->aH:Landroid/database/Cursor;

    if-eqz v0, :cond_8

    iget-object v0, p0, Leet;->aH:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_8

    iget-boolean v0, p0, Leet;->aX:Z

    if-eqz v0, :cond_8

    .line 1940
    new-instance v0, Landroid/database/MatrixCursor;

    new-array v3, v1, [Ljava/lang/String;

    const-string v4, "dummyColumn"

    aput-object v4, v3, v2

    invoke-direct {v0, v3}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 1941
    invoke-virtual {v0}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1942
    iput-object v0, p0, Leet;->aH:Landroid/database/Cursor;

    .line 1944
    :cond_8
    iget-object v0, p0, Leet;->az:Lfba;

    invoke-virtual {p0}, Leet;->ak()Lhym;

    move-result-object v2

    invoke-virtual {v0, v2}, Lfba;->a(Landroid/database/Cursor;)V

    .line 1945
    iget-object v0, p0, Leet;->az:Lfba;

    iget-object v2, p0, Leet;->aH:Landroid/database/Cursor;

    invoke-virtual {v0, v2, v1}, Lfba;->b(Landroid/database/Cursor;I)V

    .line 1947
    iget-object v0, p0, Leet;->az:Lfba;

    invoke-virtual {v0}, Lfba;->h()V

    .line 1948
    iget-object v0, p0, Leet;->az:Lfba;

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Lfba;->a(ZI)V

    .line 1949
    iget-object v0, p0, Leet;->az:Lfba;

    invoke-virtual {v0}, Lfba;->c()V

    .line 1951
    iget-object v0, p0, Leet;->U:Licq;

    sget-object v1, Lict;->b:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    .line 1952
    invoke-virtual {p0}, Leet;->ay()V

    goto/16 :goto_0

    .line 1957
    :sswitch_2
    invoke-super {p0, p1, p2}, Lehh;->a(Ldo;Landroid/database/Cursor;)V

    goto/16 :goto_0

    .line 1961
    :sswitch_3
    iput-object p2, p0, Leet;->aB:Landroid/database/Cursor;

    .line 1962
    iget v0, p0, Leet;->bc:I

    if-nez v0, :cond_9

    .line 1963
    invoke-super {p0, p1, p2}, Lehh;->a(Ldo;Landroid/database/Cursor;)V

    .line 1964
    iget-boolean v0, p0, Leet;->bd:Z

    if-eqz v0, :cond_0

    .line 1965
    iget-object v0, p0, Leet;->W:Lfdj;

    invoke-virtual {v0, v1, v1}, Lfdj;->a(ZI)V

    .line 1966
    iget-object v0, p0, Leet;->az:Lfba;

    invoke-virtual {v0}, Lfba;->c()V

    .line 1967
    iget-object v0, p0, Leet;->az:Lfba;

    invoke-virtual {v0}, Lfba;->ak()V

    .line 1968
    iget-object v0, p0, Leet;->az:Lfba;

    invoke-virtual {v0}, Lfba;->b()V

    .line 1969
    iput-boolean v2, p0, Leet;->bd:Z

    goto/16 :goto_0

    .line 1973
    :cond_9
    iget-object v0, p0, Leet;->V:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    if-eqz v0, :cond_0

    .line 1974
    iget-object v0, p0, Leet;->V:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    iget-object v1, p0, Leet;->S:Llhd;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a(Llhc;)V

    goto/16 :goto_0

    :cond_a
    move v0, v2

    goto/16 :goto_2

    .line 1848
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x65 -> :sswitch_0
        0x67 -> :sswitch_1
    .end sparse-switch
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 169
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Leet;->a(Ldo;Landroid/database/Cursor;)V

    return-void
.end method

.method public a(Lhjk;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1606
    const v0, 0x7f10067b

    .line 1607
    invoke-interface {p1, v0}, Lhjk;->a(I)Lhjc;

    move-result-object v0

    check-cast v0, Lhjv;

    .line 1608
    invoke-virtual {v0, v1}, Lhjv;->a(I)V

    .line 1609
    invoke-virtual {p0}, Leet;->aj()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1610
    invoke-virtual {v0}, Lhjv;->b()V

    .line 1613
    :cond_0
    iget-object v0, p0, Leet;->az:Lfba;

    if-eqz v0, :cond_1

    .line 1614
    iget-object v0, p0, Leet;->az:Lfba;

    invoke-virtual {v0}, Lfba;->y()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lhjk;->a(Ljava/lang/CharSequence;)V

    .line 1617
    :cond_1
    iget-boolean v0, p0, Leet;->aW:Z

    if-eqz v0, :cond_9

    iget-boolean v0, p0, Leet;->aX:Z

    if-eqz v0, :cond_9

    move v7, v1

    .line 1621
    :goto_0
    if-eqz v7, :cond_a

    iget-object v0, p0, Leet;->az:Lfba;

    if-eqz v0, :cond_a

    iget-object v0, p0, Leet;->az:Lfba;

    .line 1624
    invoke-virtual {v0}, Lfba;->l()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Leet;->az:Lfba;

    .line 1625
    invoke-virtual {v0}, Lfba;->u()Z

    move-result v0

    if-nez v0, :cond_a

    move v0, v1

    .line 1627
    :goto_1
    iget-boolean v3, p0, Leet;->aW:Z

    if-eqz v3, :cond_b

    iget-boolean v3, p0, Leet;->aX:Z

    if-nez v3, :cond_b

    iget-object v3, p0, Leet;->az:Lfba;

    if-eqz v3, :cond_b

    iget-object v3, p0, Leet;->az:Lfba;

    .line 1631
    invoke-virtual {v3}, Lfba;->y()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_b

    iget-object v3, p0, Leet;->aO:Ljava/lang/Integer;

    if-nez v3, :cond_b

    iget-object v3, p0, Leet;->aQ:Ljava/lang/Integer;

    if-nez v3, :cond_b

    iget-object v3, p0, Leet;->aM:Ljava/lang/Integer;

    if-nez v3, :cond_b

    iget-boolean v3, p0, Leet;->ba:Z

    if-nez v3, :cond_b

    move v3, v1

    .line 1637
    :goto_2
    iget-boolean v4, p0, Leet;->aW:Z

    if-eqz v4, :cond_c

    iget-boolean v4, p0, Leet;->aX:Z

    if-nez v4, :cond_c

    iget-object v4, p0, Leet;->aO:Ljava/lang/Integer;

    if-nez v4, :cond_c

    iget-object v4, p0, Leet;->aQ:Ljava/lang/Integer;

    if-nez v4, :cond_c

    iget-object v4, p0, Leet;->aM:Ljava/lang/Integer;

    if-nez v4, :cond_c

    iget-boolean v4, p0, Leet;->ba:Z

    if-eqz v4, :cond_c

    move v4, v1

    .line 1645
    :goto_3
    iget-boolean v5, p0, Leet;->aW:Z

    if-eqz v5, :cond_d

    iget-boolean v5, p0, Leet;->aX:Z

    if-nez v5, :cond_d

    iget-object v5, p0, Leet;->az:Lfba;

    if-eqz v5, :cond_d

    iget-object v5, p0, Leet;->az:Lfba;

    .line 1649
    invoke-virtual {v5}, Lfba;->y()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_d

    iget-object v5, p0, Leet;->aQ:Ljava/lang/Integer;

    if-nez v5, :cond_d

    iget-object v5, p0, Leet;->aM:Ljava/lang/Integer;

    if-nez v5, :cond_d

    iget-boolean v5, p0, Leet;->aZ:Z

    if-nez v5, :cond_d

    move v5, v1

    .line 1654
    :goto_4
    iget-boolean v6, p0, Leet;->aW:Z

    if-eqz v6, :cond_e

    iget-boolean v6, p0, Leet;->aX:Z

    if-nez v6, :cond_e

    iget-object v6, p0, Leet;->aQ:Ljava/lang/Integer;

    if-nez v6, :cond_e

    iget-object v6, p0, Leet;->aM:Ljava/lang/Integer;

    if-nez v6, :cond_e

    iget-boolean v6, p0, Leet;->aZ:Z

    if-eqz v6, :cond_e

    move v6, v1

    .line 1661
    :goto_5
    iget-boolean v8, p0, Leet;->aW:Z

    if-eqz v8, :cond_f

    iget-boolean v8, p0, Leet;->aX:Z

    if-nez v8, :cond_f

    iget-object v8, p0, Leet;->aM:Ljava/lang/Integer;

    if-nez v8, :cond_f

    .line 1666
    :goto_6
    const v2, 0x7f1006b7

    invoke-interface {p1, v2}, Lhjk;->b(I)Landroid/view/MenuItem;

    .line 1668
    if-eqz v7, :cond_2

    .line 1669
    const v2, 0x7f1006a2

    invoke-interface {p1, v2}, Lhjk;->b(I)Landroid/view/MenuItem;

    .line 1670
    const v2, 0x7f1006a3

    invoke-interface {p1, v2}, Lhjk;->b(I)Landroid/view/MenuItem;

    .line 1672
    :cond_2
    if-eqz v0, :cond_3

    .line 1673
    const v0, 0x7f1006a4

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    .line 1675
    :cond_3
    if-eqz v3, :cond_4

    .line 1676
    const v0, 0x7f1006a5

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    .line 1678
    :cond_4
    if-eqz v4, :cond_5

    .line 1679
    const v0, 0x7f1006a6

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    .line 1681
    :cond_5
    if-eqz v5, :cond_6

    .line 1682
    const v0, 0x7f1006a7

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    .line 1684
    :cond_6
    if-eqz v6, :cond_7

    .line 1685
    const v0, 0x7f1006a8

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    .line 1687
    :cond_7
    if-eqz v1, :cond_8

    .line 1688
    const v0, 0x7f1006e0

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    .line 1691
    :cond_8
    const v0, 0x7f10053c

    new-instance v1, Litl;

    const-string v2, "plus_profile"

    invoke-direct {v1, v2}, Litl;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 1692
    const v0, 0x7f100406

    new-instance v1, Lfkx;

    const/16 v2, 0x6b

    invoke-direct {v1, v2}, Lfkx;-><init>(I)V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 1695
    iget-object v0, p0, Leet;->az:Lfba;

    invoke-virtual {v0}, Lfba;->b()V

    .line 1696
    return-void

    :cond_9
    move v7, v2

    .line 1617
    goto/16 :goto_0

    :cond_a
    move v0, v2

    .line 1625
    goto/16 :goto_1

    :cond_b
    move v3, v2

    .line 1631
    goto/16 :goto_2

    :cond_c
    move v4, v2

    .line 1637
    goto/16 :goto_3

    :cond_d
    move v5, v2

    .line 1649
    goto/16 :goto_4

    :cond_e
    move v6, v2

    .line 1654
    goto :goto_5

    :cond_f
    move v1, v2

    .line 1661
    goto :goto_6
.end method

.method public a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 826
    iget-object v0, p0, Leet;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    .line 828
    invoke-virtual {p0}, Leet;->n()Lz;

    move-result-object v1

    invoke-static {v1, v0}, Leyq;->g(Landroid/content/Context;I)Ljuj;

    move-result-object v0

    .line 829
    invoke-virtual {v0, p1}, Ljuj;->a(Ljava/lang/String;)Ljuj;

    move-result-object v0

    .line 830
    invoke-virtual {v0}, Ljuj;->a()Landroid/content/Intent;

    move-result-object v1

    .line 831
    iget-object v0, p0, Leet;->au:Llnh;

    const-class v2, Lhms;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    iget-object v3, p0, Leet;->at:Llnl;

    invoke-direct {v2, v3}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v3, Lhmv;->eb:Lhmv;

    .line 832
    invoke-virtual {v2, v3}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v2

    .line 831
    invoke-interface {v0, v2}, Lhms;->a(Lhmr;)V

    .line 833
    invoke-virtual {p0, v1}, Leet;->a(Landroid/content/Intent;)V

    .line 834
    return-void
.end method

.method protected a(Ljava/lang/String;Landroid/graphics/RectF;)V
    .locals 7

    .prologue
    .line 2644
    iget-object v0, p0, Leet;->au:Llnh;

    const-class v1, Lhoc;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lhoc;

    new-instance v0, Ldqk;

    .line 2645
    invoke-virtual {p0}, Leet;->n()Lz;

    move-result-object v1

    iget-object v2, p0, Leet;->R:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    iget-object v3, p0, Leet;->aV:Ljava/lang/String;

    invoke-static {v3}, Ljpu;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Ldqk;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Landroid/graphics/RectF;)V

    .line 2644
    invoke-virtual {v6, v0}, Lhoc;->c(Lhny;)V

    .line 2647
    return-void
.end method

.method protected a(Ljava/lang/String;Landroid/graphics/RectF;IZ)V
    .locals 6

    .prologue
    .line 2656
    iget-object v0, p0, Leet;->at:Llnl;

    iget-object v1, p0, Leet;->R:Lhee;

    .line 2657
    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    .line 2656
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILjava/lang/String;Landroid/graphics/RectF;IZ)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leet;->aN:Ljava/lang/Integer;

    .line 2658
    const v0, 0x7f0a08ef

    invoke-virtual {p0, v0}, Leet;->g(I)V

    .line 2659
    return-void
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 2995
    invoke-super {p0, p1, p2, p3}, Lehh;->a(Ljava/lang/String;Lhoz;Lhos;)V

    .line 2997
    const-string v0, "UploadCoverPhotoTask"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2998
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lhoz;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3000
    iget-object v0, p0, Leet;->at:Llnl;

    iget-object v1, p0, Leet;->R:Lhee;

    .line 3001
    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    iget-object v2, p0, Leet;->aV:Ljava/lang/String;

    .line 3000
    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/service/EsService;->f(Landroid/content/Context;ILjava/lang/String;Z)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leet;->aK:Ljava/lang/Integer;

    .line 3002
    invoke-virtual {p0}, Leet;->ay()V

    .line 3006
    :cond_0
    const-string v0, "ModifyCircleMembershipsTask"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3007
    iget-object v0, p0, Leet;->bi:Llfn;

    iget-object v1, p0, Leet;->R:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    new-array v2, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "AddToCircleFromProfile"

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Llfn;->a(I[Ljava/lang/String;)V

    .line 3009
    :cond_1
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 1181
    iget-object v0, p0, Leet;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v2

    .line 1182
    iget-object v1, p0, Leet;->at:Llnl;

    const-wide/16 v6, 0x0

    move-object v3, p1

    move-object v5, v4

    invoke-static/range {v1 .. v7}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    .line 1185
    invoke-virtual {p0, v0}, Leet;->a(Landroid/content/Intent;)V

    .line 1186
    return-void
.end method

.method public a(Ljcl;)V
    .locals 0

    .prologue
    .line 3033
    return-void
.end method

.method public a(Loo;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 1588
    invoke-super {p0, p1}, Lehh;->a(Loo;)V

    .line 1589
    invoke-static {p1, v0}, Lley;->a(Loo;Z)V

    .line 1590
    invoke-virtual {p1, v0}, Loo;->c(Z)V

    .line 1591
    iget-object v0, p0, Leet;->az:Lfba;

    if-eqz v0, :cond_0

    .line 1592
    iget-object v0, p0, Leet;->az:Lfba;

    invoke-virtual {v0, p1}, Lfba;->a(Loo;)V

    .line 1594
    :cond_0
    return-void
.end method

.method public a(ZZ)V
    .locals 1

    .prologue
    .line 2691
    iget-object v0, p0, Leet;->az:Lfba;

    invoke-virtual {v0, p1, p2}, Lfba;->a(ZZ)V

    .line 2692
    iget-object v0, p0, Leet;->Q:Lhje;

    invoke-virtual {v0}, Lhje;->a()V

    .line 2693
    return-void
.end method

.method protected a([B)V
    .locals 2

    .prologue
    .line 2634
    .line 2635
    invoke-virtual {p0}, Leet;->n()Lz;

    move-result-object v0

    iget-object v1, p0, Leet;->R:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;I[B)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leet;->aK:Ljava/lang/Integer;

    .line 2636
    const v0, 0x7f0a08ee

    invoke-virtual {p0, v0}, Leet;->g(I)V

    .line 2637
    return-void
.end method

.method protected a(Landroid/database/Cursor;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1986
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x1

    .line 2314
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 2315
    const v2, 0x7f1006a4

    if-ne v0, v2, :cond_0

    .line 2316
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Leet;->e(I)V

    move v0, v1

    .line 2367
    :goto_0
    return v0

    .line 2318
    :cond_0
    const v2, 0x7f1006a3

    if-ne v0, v2, :cond_1

    .line 2319
    invoke-virtual {p0, v5}, Leet;->b(Z)V

    move v0, v1

    .line 2320
    goto :goto_0

    .line 2321
    :cond_1
    const v2, 0x7f1006a2

    if-ne v0, v2, :cond_2

    .line 2322
    invoke-virtual {p0, v5}, Leet;->k_(Z)V

    move v0, v1

    .line 2323
    goto :goto_0

    .line 2324
    :cond_2
    const v2, 0x7f1006a5

    if-ne v0, v2, :cond_4

    .line 2325
    iget-boolean v0, p0, Leet;->aY:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Leet;->az:Lfba;

    .line 2326
    invoke-virtual {v0}, Lfba;->y()Ljava/lang/String;

    move-result-object v0

    .line 2327
    :goto_1
    new-instance v2, Lejh;

    invoke-direct {v2}, Lejh;-><init>()V

    .line 2328
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 2329
    const-string v4, "name"

    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2330
    const-string v0, "gender"

    iget-object v4, p0, Leet;->az:Lfba;

    invoke-virtual {v4}, Lfba;->ae()I

    move-result v4

    invoke-virtual {v3, v0, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2331
    const-string v0, "target_mute"

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2332
    invoke-virtual {v2, v3}, Lejh;->f(Landroid/os/Bundle;)V

    .line 2333
    invoke-virtual {v2, p0, v5}, Lejh;->a(Lu;I)V

    .line 2334
    invoke-virtual {p0}, Leet;->p()Lae;

    move-result-object v0

    const-string v3, "mute_profile"

    invoke-virtual {v2, v0, v3}, Lejh;->a(Lae;Ljava/lang/String;)V

    move v0, v1

    .line 2335
    goto :goto_0

    .line 2326
    :cond_3
    iget-object v0, p0, Leet;->az:Lfba;

    invoke-virtual {v0}, Lfba;->z()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 2336
    :cond_4
    const v2, 0x7f1006a6

    if-ne v0, v2, :cond_6

    .line 2337
    iget-boolean v0, p0, Leet;->aY:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Leet;->az:Lfba;

    .line 2338
    invoke-virtual {v0}, Lfba;->y()Ljava/lang/String;

    move-result-object v0

    .line 2339
    :goto_2
    new-instance v2, Lejh;

    invoke-direct {v2}, Lejh;-><init>()V

    .line 2340
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 2341
    const-string v4, "name"

    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2342
    const-string v0, "gender"

    iget-object v4, p0, Leet;->az:Lfba;

    invoke-virtual {v4}, Lfba;->ae()I

    move-result v4

    invoke-virtual {v3, v0, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2343
    const-string v0, "target_mute"

    invoke-virtual {v3, v0, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2344
    invoke-virtual {v2, v3}, Lejh;->f(Landroid/os/Bundle;)V

    .line 2345
    invoke-virtual {v2, p0, v5}, Lejh;->a(Lu;I)V

    .line 2346
    invoke-virtual {p0}, Leet;->p()Lae;

    move-result-object v0

    const-string v3, "unmute_profile"

    invoke-virtual {v2, v0, v3}, Lejh;->a(Lae;Ljava/lang/String;)V

    move v0, v1

    .line 2347
    goto/16 :goto_0

    .line 2338
    :cond_5
    iget-object v0, p0, Leet;->az:Lfba;

    invoke-virtual {v0}, Lfba;->z()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 2348
    :cond_6
    const v2, 0x7f1006a7

    if-ne v0, v2, :cond_7

    .line 2349
    new-instance v0, Ldyr;

    iget-object v2, p0, Leet;->az:Lfba;

    invoke-virtual {v2}, Lfba;->t()Z

    move-result v2

    invoke-direct {v0, v2}, Ldyr;-><init>(Z)V

    .line 2350
    invoke-virtual {v0, p0, v5}, Ldyr;->a(Lu;I)V

    .line 2351
    invoke-virtual {p0}, Leet;->p()Lae;

    move-result-object v2

    const-string v3, "block_person"

    invoke-virtual {v0, v2, v3}, Ldyr;->a(Lae;Ljava/lang/String;)V

    move v0, v1

    .line 2352
    goto/16 :goto_0

    .line 2353
    :cond_7
    const v2, 0x7f1006a8

    if-ne v0, v2, :cond_8

    .line 2354
    new-instance v0, Leqj;

    iget-object v2, p0, Leet;->aV:Ljava/lang/String;

    iget-object v3, p0, Leet;->az:Lfba;

    .line 2355
    invoke-virtual {v3}, Lfba;->t()Z

    move-result v3

    invoke-direct {v0, v2, v3}, Leqj;-><init>(Ljava/lang/String;Z)V

    .line 2356
    invoke-virtual {v0, p0, v5}, Leqj;->a(Lu;I)V

    .line 2357
    invoke-virtual {p0}, Leet;->p()Lae;

    move-result-object v2

    const-string v3, "unblock_person"

    invoke-virtual {v0, v2, v3}, Leqj;->a(Lae;Ljava/lang/String;)V

    move v0, v1

    .line 2358
    goto/16 :goto_0

    .line 2359
    :cond_8
    const v2, 0x7f1006e0

    if-ne v0, v2, :cond_9

    .line 2360
    new-instance v0, Lepw;

    invoke-direct {v0}, Lepw;-><init>()V

    .line 2361
    invoke-virtual {p0}, Leet;->q()Lae;

    move-result-object v2

    const-string v3, "report_abuse"

    invoke-virtual {v0, v2, v3}, Lepw;->a(Lae;Ljava/lang/String;)V

    move v0, v1

    .line 2362
    goto/16 :goto_0

    .line 2363
    :cond_9
    const v2, 0x7f1006b7

    if-ne v0, v2, :cond_b

    .line 2364
    iget-object v0, p0, Leet;->az:Lfba;

    invoke-virtual {v0}, Lfba;->A()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_a

    invoke-virtual {p0}, Leet;->n()Lz;

    move-result-object v2

    invoke-static {v2, v0}, Llsj;->a(Landroid/content/Context;Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Leet;->o()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a0760

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Leet;->at:Llnl;

    invoke-static {v2, v0, v5}, Llih;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_a
    move v0, v1

    .line 2365
    goto/16 :goto_0

    .line 2367
    :cond_b
    invoke-super {p0, p1}, Lehh;->a(Landroid/view/MenuItem;)Z

    move-result v0

    goto/16 :goto_0
.end method

.method public aO_()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 2054
    invoke-super {p0}, Lehh;->aO_()V

    .line 2056
    iget-object v0, p0, Leet;->bm:Lfhh;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(Lfhh;)V

    .line 2058
    iget-object v0, p0, Leet;->aK:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 2059
    iget-object v0, p0, Leet;->aK:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2060
    iget-object v0, p0, Leet;->aK:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(I)Lfib;

    move-result-object v0

    .line 2061
    iget-object v1, p0, Leet;->aK:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v1, v0}, Leet;->a(ILfib;)V

    .line 2062
    iput-object v3, p0, Leet;->aK:Ljava/lang/Integer;

    .line 2066
    :cond_0
    iget-object v0, p0, Leet;->aM:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 2067
    iget-object v0, p0, Leet;->aM:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2068
    iget-object v0, p0, Leet;->aM:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(I)Lfib;

    move-result-object v0

    .line 2069
    iget-object v1, p0, Leet;->aM:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v1, v0}, Leet;->b(ILfib;)V

    .line 2070
    iput-object v3, p0, Leet;->aM:Ljava/lang/Integer;

    .line 2074
    :cond_1
    iget-object v0, p0, Leet;->aO:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 2075
    iget-object v0, p0, Leet;->aO:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2076
    iget-object v0, p0, Leet;->aO:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(I)Lfib;

    move-result-object v0

    .line 2077
    iget-object v1, p0, Leet;->aO:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-boolean v2, p0, Leet;->aP:Z

    invoke-virtual {p0, v1, v2, v0}, Leet;->a(IZLfib;)V

    .line 2078
    iput-object v3, p0, Leet;->aO:Ljava/lang/Integer;

    .line 2082
    :cond_2
    iget-object v0, p0, Leet;->aQ:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 2083
    iget-object v0, p0, Leet;->aQ:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2084
    iget-object v0, p0, Leet;->aQ:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(I)Lfib;

    move-result-object v0

    .line 2085
    iget-object v1, p0, Leet;->aQ:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v1, v4, v0}, Leet;->b(IZLfib;)V

    .line 2086
    iput-object v3, p0, Leet;->aQ:Ljava/lang/Integer;

    .line 2090
    :cond_3
    iget-object v0, p0, Leet;->aN:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 2091
    iget-object v0, p0, Leet;->aN:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(I)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2092
    iget-object v0, p0, Leet;->aN:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(I)Lfib;

    move-result-object v0

    .line 2093
    iget-object v1, p0, Leet;->aN:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, v0}, Leet;->c(ILfib;)V

    .line 2094
    iput-object v3, p0, Leet;->aN:Ljava/lang/Integer;

    .line 2098
    :cond_4
    iget-object v0, p0, Leet;->aL:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 2099
    iget-object v0, p0, Leet;->aL:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(I)Z

    move-result v0

    if-nez v0, :cond_5

    .line 2100
    iget-object v0, p0, Leet;->aL:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(I)Lfib;

    move-result-object v0

    .line 2101
    iget-object v1, p0, Leet;->aL:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, v0}, Leet;->d(ILfib;)V

    .line 2102
    iput-object v3, p0, Leet;->aL:Ljava/lang/Integer;

    .line 2106
    :cond_5
    iget-boolean v0, p0, Leet;->aX:Z

    if-eqz v0, :cond_6

    iget-boolean v0, p0, Leet;->aJ:Z

    if-eqz v0, :cond_6

    .line 2109
    invoke-virtual {p0}, Leet;->am()V

    .line 2111
    :cond_6
    iput-boolean v4, p0, Leet;->aJ:Z

    .line 2113
    invoke-virtual {p0}, Leet;->ay()V

    .line 2114
    return-void
.end method

.method public aR_()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 2988
    const/4 v0, 0x0

    return-object v0
.end method

.method public a_(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 3037
    return-void
.end method

.method public aa()Z
    .locals 1

    .prologue
    .line 1732
    const/4 v0, 0x0

    return v0
.end method

.method public aa_()V
    .locals 5

    .prologue
    .line 749
    const/4 v0, 0x0

    iget-object v1, p0, Leet;->aV:Ljava/lang/String;

    .line 750
    invoke-static {v1}, Ljpu;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "profile"

    const-string v3, "ALBUM"

    .line 749
    invoke-static {v0, v1, v2, v3}, Ljvj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 752
    iget-object v1, p0, Leet;->R:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    .line 754
    invoke-virtual {p0}, Leet;->n()Lz;

    move-result-object v2

    invoke-static {v2, v1}, Leyq;->g(Landroid/content/Context;I)Ljuj;

    move-result-object v1

    const/4 v2, 0x3

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    .line 755
    invoke-static {v2, v3}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljuj;->a(Ljava/lang/String;)Ljuj;

    move-result-object v0

    .line 756
    invoke-virtual {v0}, Ljuj;->a()Landroid/content/Intent;

    move-result-object v0

    .line 757
    invoke-virtual {p0, v0}, Leet;->a(Landroid/content/Intent;)V

    .line 758
    return-void
.end method

.method public ab()V
    .locals 0

    .prologue
    .line 1737
    return-void
.end method

.method public ab_()V
    .locals 0

    .prologue
    .line 3021
    return-void
.end method

.method public ac()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 851
    iget-object v0, p0, Leet;->az:Lfba;

    invoke-virtual {v0}, Lfba;->t()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 852
    :goto_0
    iget-object v3, p0, Leet;->az:Lfba;

    invoke-virtual {v3}, Lfba;->af()Z

    move-result v3

    if-nez v3, :cond_2

    move v3, v1

    .line 853
    :goto_1
    if-eqz v3, :cond_3

    iget-boolean v3, p0, Leet;->bf:Z

    if-nez v3, :cond_0

    if-nez v0, :cond_3

    .line 855
    :cond_0
    :goto_2
    invoke-direct {p0, v0, v1}, Leet;->b(ZZ)V

    .line 856
    return-void

    :cond_1
    move v0, v2

    .line 851
    goto :goto_0

    :cond_2
    move v3, v2

    .line 852
    goto :goto_1

    :cond_3
    move v1, v2

    .line 853
    goto :goto_2
.end method

.method public ac_()Lhmk;
    .locals 3

    .prologue
    .line 3041
    iget v0, p0, Leet;->bc:I

    iget v1, p0, Leet;->bh:I

    if-eq v0, v1, :cond_0

    .line 3042
    iget v0, p0, Leet;->bc:I

    packed-switch v0, :pswitch_data_0

    .line 3063
    new-instance v0, Lkrf;

    sget-object v1, Long;->j:Lhmn;

    iget-object v2, p0, Leet;->aa:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lkrf;-><init>(Lhmn;Ljava/lang/String;)V

    iput-object v0, p0, Leet;->bg:Lhmk;

    .line 3066
    :goto_0
    iget v0, p0, Leet;->bc:I

    iput v0, p0, Leet;->bh:I

    .line 3068
    :cond_0
    iget-object v0, p0, Leet;->bg:Lhmk;

    return-object v0

    .line 3044
    :pswitch_0
    new-instance v0, Lkrf;

    sget-object v1, Long;->e:Lhmn;

    iget-object v2, p0, Leet;->aa:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lkrf;-><init>(Lhmn;Ljava/lang/String;)V

    iput-object v0, p0, Leet;->bg:Lhmk;

    goto :goto_0

    .line 3047
    :pswitch_1
    new-instance v0, Lkrf;

    sget-object v1, Long;->h:Lhmn;

    iget-object v2, p0, Leet;->aa:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lkrf;-><init>(Lhmn;Ljava/lang/String;)V

    iput-object v0, p0, Leet;->bg:Lhmk;

    goto :goto_0

    .line 3050
    :pswitch_2
    new-instance v0, Lkrf;

    sget-object v1, Long;->k:Lhmn;

    iget-object v2, p0, Leet;->aa:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lkrf;-><init>(Lhmn;Ljava/lang/String;)V

    iput-object v0, p0, Leet;->bg:Lhmk;

    goto :goto_0

    .line 3054
    :pswitch_3
    new-instance v0, Lkrf;

    sget-object v1, Long;->g:Lhmn;

    iget-object v2, p0, Leet;->aa:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lkrf;-><init>(Lhmn;Ljava/lang/String;)V

    iput-object v0, p0, Leet;->bg:Lhmk;

    goto :goto_0

    .line 3058
    :pswitch_4
    new-instance v0, Lkrf;

    sget-object v1, Long;->f:Lhmn;

    iget-object v2, p0, Leet;->aa:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lkrf;-><init>(Lhmn;Ljava/lang/String;)V

    iput-object v0, p0, Leet;->bg:Lhmk;

    goto :goto_0

    .line 3042
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public ad()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 860
    iget-boolean v0, p0, Leet;->bf:Z

    if-eqz v0, :cond_1

    .line 861
    iget-object v0, p0, Leet;->az:Lfba;

    invoke-virtual {v0}, Lfba;->t()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 862
    :goto_0
    invoke-direct {p0, v0, v2}, Leet;->b(ZZ)V

    .line 865
    :goto_1
    return v1

    :cond_0
    move v0, v2

    .line 861
    goto :goto_0

    :cond_1
    move v1, v2

    .line 865
    goto :goto_1
.end method

.method public ae()V
    .locals 0

    .prologue
    .line 1103
    invoke-direct {p0}, Leet;->aM()V

    .line 1104
    return-void
.end method

.method public af()V
    .locals 0

    .prologue
    .line 1108
    invoke-direct {p0}, Leet;->aM()V

    .line 1109
    return-void
.end method

.method public ag()V
    .locals 5

    .prologue
    .line 1197
    iget-object v0, p0, Leet;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    .line 1198
    invoke-virtual {p0}, Leet;->n()Lz;

    move-result-object v1

    iget-object v2, p0, Leet;->aV:Ljava/lang/String;

    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/google/android/apps/plus/phone/ProfileSquareListActivity;

    invoke-direct {v3, v1, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "account_id"

    invoke-virtual {v3, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "person_id"

    invoke-virtual {v3, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1199
    invoke-virtual {p0, v3}, Leet;->a(Landroid/content/Intent;)V

    .line 1200
    return-void
.end method

.method public ah()V
    .locals 2

    .prologue
    .line 1207
    iget-object v0, p0, Leet;->at:Llnl;

    iget-object v1, p0, Leet;->R:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    invoke-static {v0, v1}, Leyq;->o(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v0

    .line 1208
    invoke-virtual {p0, v0}, Leet;->a(Landroid/content/Intent;)V

    .line 1209
    return-void
.end method

.method public ai()V
    .locals 3

    .prologue
    .line 1257
    new-instance v0, Lepk;

    invoke-direct {v0}, Lepk;-><init>()V

    .line 1258
    invoke-virtual {p0}, Leet;->p()Lae;

    move-result-object v1

    const-string v2, "zagat_explanation"

    invoke-virtual {v0, v1, v2}, Lepk;->a(Lae;Ljava/lang/String;)V

    .line 1259
    return-void
.end method

.method public aj()Z
    .locals 1

    .prologue
    .line 1700
    invoke-super {p0}, Lehh;->aj()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Leet;->be:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Leet;->aK:Ljava/lang/Integer;

    if-nez v0, :cond_0

    iget-object v0, p0, Leet;->aM:Ljava/lang/Integer;

    if-nez v0, :cond_0

    iget-object v0, p0, Leet;->aO:Ljava/lang/Integer;

    if-nez v0, :cond_0

    iget-object v0, p0, Leet;->aQ:Ljava/lang/Integer;

    if-nez v0, :cond_0

    iget-object v0, p0, Leet;->aN:Ljava/lang/Integer;

    if-nez v0, :cond_0

    iget-object v0, p0, Leet;->aL:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected ak()Lhym;
    .locals 1

    .prologue
    .line 1712
    iget-object v0, p0, Leet;->aA:Lhym;

    if-nez v0, :cond_0

    .line 1713
    invoke-static {}, Lfdj;->ar()Lhym;

    move-result-object v0

    iput-object v0, p0, Leet;->aA:Lhym;

    .line 1715
    :cond_0
    iget-object v0, p0, Leet;->aA:Lhym;

    return-object v0
.end method

.method public al()V
    .locals 2

    .prologue
    .line 1995
    invoke-super {p0}, Lehh;->al()V

    .line 1996
    invoke-virtual {p0}, Leet;->am()V

    .line 1997
    iget v0, p0, Leet;->bc:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 1998
    invoke-virtual {p0}, Leet;->an()V

    .line 2000
    :cond_0
    invoke-virtual {p0}, Leet;->aB()V

    .line 2001
    return-void
.end method

.method public am()V
    .locals 4

    .prologue
    .line 2007
    iget-object v0, p0, Leet;->at:Llnl;

    iget-object v1, p0, Leet;->R:Lhee;

    .line 2008
    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    iget-object v2, p0, Leet;->aV:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/service/EsService;->f(Landroid/content/Context;ILjava/lang/String;Z)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leet;->aK:Ljava/lang/Integer;

    .line 2009
    invoke-virtual {p0}, Leet;->ay()V

    .line 2010
    return-void
.end method

.method public an()V
    .locals 5

    .prologue
    .line 2013
    sget-object v0, Leet;->N:Llpa;

    .line 2014
    iget-object v1, p0, Leet;->at:Llnl;

    iget-object v0, p0, Leet;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v2

    iget-object v0, p0, Leet;->aV:Ljava/lang/String;

    .line 2015
    invoke-static {v0}, Ljpu;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    .line 2016
    invoke-virtual {p0}, Leet;->aH()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 2014
    :goto_0
    invoke-static {v1, v2, v3, v4, v0}, Lcom/google/android/apps/plus/service/EsService;->b(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leet;->aL:Ljava/lang/Integer;

    .line 2018
    invoke-virtual {p0}, Leet;->ay()V

    .line 2023
    return-void

    .line 2016
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected ao()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2374
    iget-object v1, p0, Leet;->W:Lfdj;

    invoke-virtual {v1}, Lfdj;->getCount()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ap()Z
    .locals 5

    .prologue
    .line 2393
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SENDTO"

    const-string v2, "sms"

    const-string v3, ""

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 2394
    invoke-virtual {p0}, Leet;->n()Lz;

    move-result-object v1

    invoke-virtual {v1}, Lz;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 2395
    const/high16 v2, 0x10000

    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 2397
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 2983
    const/4 v0, 0x0

    return-object v0
.end method

.method protected b(ILfib;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2270
    iget-object v0, p0, Leet;->aM:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Leet;->aM:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_1

    .line 2286
    :cond_0
    :goto_0
    return-void

    .line 2274
    :cond_1
    invoke-virtual {p0}, Leet;->aE()V

    .line 2276
    const/4 v0, 0x0

    iput-object v0, p0, Leet;->aM:Ljava/lang/Integer;

    .line 2277
    invoke-virtual {p0}, Leet;->ay()V

    .line 2279
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lfib;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2280
    invoke-virtual {p0}, Leet;->n()Lz;

    move-result-object v0

    const v1, 0x7f0a0592

    invoke-static {v0, v1, v2}, Llih;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 2281
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 2283
    :cond_2
    invoke-virtual {p0}, Leet;->n()Lz;

    move-result-object v0

    const v1, 0x7f0a0861

    invoke-static {v0, v1, v2}, Llih;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 2284
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method protected b(IZLfib;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2248
    iget-object v0, p0, Leet;->aQ:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Leet;->aQ:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_1

    .line 2264
    :cond_0
    :goto_0
    return-void

    .line 2252
    :cond_1
    invoke-virtual {p0}, Leet;->aE()V

    .line 2254
    const/4 v0, 0x0

    iput-object v0, p0, Leet;->aQ:Ljava/lang/Integer;

    .line 2255
    invoke-virtual {p0}, Leet;->ay()V

    .line 2257
    if-eqz p3, :cond_2

    invoke-virtual {p3}, Lfib;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2258
    invoke-virtual {p0}, Leet;->n()Lz;

    move-result-object v0

    const v1, 0x7f0a0592

    invoke-static {v0, v1, v2}, Llih;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 2259
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2260
    invoke-virtual {p0, v2, p2}, Leet;->a(ZZ)V

    goto :goto_0

    .line 2262
    :cond_2
    const/4 v0, 0x1

    invoke-virtual {p0, v0, p2}, Leet;->a(ZZ)V

    goto :goto_0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2918
    invoke-super {p0, p1}, Lehh;->b(Landroid/os/Bundle;)V

    .line 2919
    iget-object v0, p0, Leet;->aa:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2920
    const-string v0, "extra_gaia_id"

    iget-object v1, p0, Leet;->aa:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2922
    :cond_0
    return-void
.end method

.method public b(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2616
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 1124
    const/4 v0, 0x0

    .line 1126
    if-eqz p1, :cond_0

    .line 1127
    invoke-static {p1}, Landroid/text/util/Rfc822Tokenizer;->tokenize(Ljava/lang/CharSequence;)[Landroid/text/util/Rfc822Token;

    move-result-object v0

    .line 1130
    :cond_0
    if-eqz v0, :cond_1

    array-length v1, v0

    if-nez v1, :cond_2

    .line 1143
    :cond_1
    :goto_0
    return-void

    .line 1134
    :cond_2
    const/4 v1, 0x0

    aget-object v0, v0, v1

    .line 1135
    invoke-virtual {v0}, Landroid/text/util/Rfc822Token;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Leet;->az:Lfba;

    .line 1136
    invoke-virtual {v1}, Lfba;->y()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1137
    iget-object v1, p0, Leet;->az:Lfba;

    invoke-virtual {v1}, Lfba;->y()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/util/Rfc822Token;->setName(Ljava/lang/String;)V

    .line 1140
    :cond_3
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.SENDTO"

    const-string v3, "mailto:"

    .line 1141
    invoke-virtual {v0}, Landroid/text/util/Rfc822Token;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1142
    invoke-direct {p0, v1}, Leet;->f(Landroid/content/Intent;)V

    goto :goto_0

    .line 1141
    :cond_4
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public b(Loo;)V
    .locals 2

    .prologue
    .line 1598
    invoke-super {p0, p1}, Lehh;->b(Loo;)V

    .line 1599
    iget-object v0, p0, Leet;->az:Lfba;

    if-eqz v0, :cond_0

    .line 1600
    iget-object v0, p0, Leet;->az:Lfba;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lfba;->a(Loo;)V

    .line 1602
    :cond_0
    return-void
.end method

.method public b(Z)V
    .locals 4

    .prologue
    .line 777
    const/4 v0, 0x2

    iput v0, p0, Leet;->aR:I

    .line 778
    iget-object v0, p0, Leet;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v1

    .line 779
    iget-object v0, p0, Leet;->az:Lfba;

    invoke-virtual {v0}, Lfba;->B()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Leet;->az:Lfba;

    invoke-virtual {v0}, Lfba;->C()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 780
    :cond_0
    iget-object v0, p0, Leet;->au:Llnh;

    const-class v2, Lhms;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    iget-object v3, p0, Leet;->at:Llnl;

    invoke-direct {v2, v3, v1}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v1, Lhmv;->D:Lhmv;

    .line 782
    invoke-virtual {v2, v1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 780
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 784
    invoke-direct {p0, p1}, Leet;->m(Z)V

    .line 792
    :goto_0
    return-void

    .line 786
    :cond_1
    iget-object v0, p0, Leet;->au:Llnh;

    const-class v2, Lhms;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    iget-object v3, p0, Leet;->at:Llnl;

    invoke-direct {v2, v3, v1}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v1, Lhmv;->F:Lhmv;

    .line 788
    invoke-virtual {v2, v1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 786
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 790
    const v0, 0x7f0a08e2

    invoke-virtual {p0, v0}, Leet;->e_(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0a08e3

    invoke-virtual {p0, v1}, Leet;->e_(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0a08e4

    invoke-virtual {p0, v2}, Leet;->e_(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0a0597

    invoke-virtual {p0, v3}, Leet;->e_(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Llgr;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)Llgr;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p0, v1}, Llgr;->a(Lu;I)V

    invoke-virtual {p0}, Leet;->p()Lae;

    move-result-object v1

    const-string v2, "cover_photo_upgrade"

    invoke-virtual {v0, v1, v2}, Llgr;->a(Lae;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public c(I)V
    .locals 4

    .prologue
    .line 1411
    const/4 v0, 0x5

    if-ne p1, v0, :cond_0

    .line 1412
    const v0, 0x7f0a0551

    .line 1413
    invoke-virtual {p0, v0}, Leet;->e_(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0a0863

    .line 1414
    invoke-virtual {p0, v1}, Leet;->e_(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x104000a

    .line 1415
    invoke-virtual {p0, v2}, Leet;->e_(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    .line 1412
    invoke-static {v0, v1, v2, v3}, Llgr;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)Llgr;

    move-result-object v0

    .line 1416
    invoke-virtual {p0}, Leet;->p()Lae;

    move-result-object v1

    const-string v2, "dialog_warning"

    invoke-virtual {v0, v1, v2}, Llgr;->a(Lae;Ljava/lang/String;)V

    .line 1422
    :goto_0
    return-void

    .line 1419
    :cond_0
    invoke-virtual {p0}, Leet;->n()Lz;

    move-result-object v0

    iget-object v1, p0, Leet;->R:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    iget-object v2, p0, Leet;->aa:Ljava/lang/String;

    .line 1418
    invoke-static {v0, v1, v2, p1}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILjava/lang/String;I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leet;->aM:Ljava/lang/Integer;

    .line 1420
    const v0, 0x7f0a055c

    invoke-virtual {p0, v0}, Leet;->g(I)V

    goto :goto_0
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 1426
    invoke-super {p0, p1}, Lehh;->c(Landroid/os/Bundle;)V

    .line 1429
    iget-object v0, p0, Leet;->au:Llnh;

    const-class v1, Lfyp;

    new-instance v2, Lctu;

    iget-object v3, p0, Leet;->av:Llqm;

    invoke-direct {v2, p0, v3, p0}, Lctu;-><init>(Lu;Llqr;Lcty;)V

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 1430
    iget-object v0, p0, Leet;->au:Llnh;

    const-class v1, Lepy;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 1431
    iget-object v0, p0, Leet;->au:Llnh;

    const-class v1, Lhtk;

    new-instance v2, Lefg;

    invoke-direct {v2, p0}, Lefg;-><init>(Leet;)V

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 1432
    iget-object v0, p0, Leet;->au:Llnh;

    const-class v1, Lkut;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 1433
    iget-object v0, p0, Leet;->au:Llnh;

    const-class v1, Lkwo;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 1434
    iget-object v0, p0, Leet;->au:Llnh;

    const-class v1, Lkwn;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 1435
    iget-object v0, p0, Leet;->au:Llnh;

    const-class v1, Lhec;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 1437
    iget-object v0, p0, Leet;->au:Llnh;

    iget-object v0, p0, Leet;->at:Llnl;

    const-class v1, Llfn;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llfn;

    iput-object v0, p0, Leet;->bi:Llfn;

    .line 1438
    iget-object v0, p0, Leet;->au:Llnh;

    const-class v1, Lgcs;

    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgcs;

    iput-object v0, p0, Leet;->bj:Lgcs;

    .line 1439
    iget-object v0, p0, Leet;->au:Llnh;

    const-class v1, Liaw;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liaw;

    iget-object v1, p0, Leet;->R:Lhee;

    .line 1440
    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    invoke-interface {v0, v1}, Liaw;->a(I)Z

    move-result v0

    iput-boolean v0, p0, Leet;->bf:Z

    .line 1441
    iget-object v0, p0, Leet;->au:Llnh;

    const-class v1, Ljpb;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljpb;

    iput-object v0, p0, Leet;->bk:Ljpb;

    .line 1443
    iget-object v0, p0, Leet;->au:Llnh;

    const-class v1, Lkvq;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkvq;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lkvq;->a(Z)V

    .line 1445
    iget-object v0, p0, Leet;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    .line 1446
    return-void
.end method

.method public c(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2620
    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 1147
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1148
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.DIAL"

    const-string v3, "tel:"

    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1149
    invoke-direct {p0, v1}, Leet;->f(Landroid/content/Intent;)V

    .line 1151
    :cond_0
    return-void

    .line 1148
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public c(Z)V
    .locals 3

    .prologue
    .line 1373
    iget-object v0, p0, Leet;->at:Llnl;

    iget-object v1, p0, Leet;->R:Lhee;

    .line 1374
    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    iget-object v2, p0, Leet;->aa:Ljava/lang/String;

    .line 1373
    invoke-static {v0, v1, v2, p1}, Lcom/google/android/apps/plus/service/EsService;->g(Landroid/content/Context;ILjava/lang/String;Z)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leet;->aO:Ljava/lang/Integer;

    .line 1375
    iput-boolean p1, p0, Leet;->aP:Z

    .line 1376
    const v0, 0x7f0a085a

    invoke-virtual {p0, v0}, Leet;->g(I)V

    .line 1377
    return-void
.end method

.method public d(I)V
    .locals 5

    .prologue
    const/4 v2, -0x1

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 931
    iget v0, p0, Leet;->bc:I

    if-eq v0, p1, :cond_3

    .line 932
    iput p1, p0, Leet;->bc:I

    .line 933
    iput-boolean v4, p0, Leet;->bd:Z

    .line 934
    iget-object v0, p0, Leet;->az:Lfba;

    if-eqz v0, :cond_2

    .line 936
    iget-object v0, p0, Leet;->az:Lfba;

    invoke-virtual {v0, p1}, Lfba;->e(I)V

    .line 937
    packed-switch p1, :pswitch_data_0

    move-object v0, v1

    .line 987
    :cond_0
    :goto_0
    iget-object v1, p0, Leet;->az:Lfba;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lfba;->k(Z)V

    .line 988
    iget-object v1, p0, Leet;->az:Lfba;

    invoke-virtual {p0}, Leet;->ak()Lhym;

    move-result-object v2

    invoke-virtual {v1, v2}, Lfba;->a(Landroid/database/Cursor;)V

    .line 989
    iget-object v1, p0, Leet;->az:Lfba;

    iget v2, p0, Leet;->X:I

    invoke-virtual {v1, v0, v2}, Lfba;->b(Landroid/database/Cursor;I)V

    .line 990
    iget-object v0, p0, Leet;->az:Lfba;

    invoke-virtual {v0, v4}, Lfba;->k(Z)V

    .line 992
    invoke-virtual {p0}, Leet;->aC()V

    .line 995
    iget v0, p0, Leet;->bc:I

    if-eqz v0, :cond_1

    .line 996
    iget-object v0, p0, Leet;->az:Lfba;

    invoke-virtual {v0, v4, v4}, Lfba;->a(ZI)V

    .line 998
    :cond_1
    iget-object v0, p0, Leet;->az:Lfba;

    invoke-virtual {v0}, Lfba;->c()V

    .line 999
    iget-object v0, p0, Leet;->az:Lfba;

    invoke-virtual {v0}, Lfba;->ak()V

    .line 1000
    iget-object v0, p0, Leet;->az:Lfba;

    invoke-virtual {v0}, Lfba;->b()V

    .line 1002
    :cond_2
    iget-boolean v0, p0, Leet;->aW:Z

    if-eqz v0, :cond_3

    .line 1003
    invoke-static {p0}, Lhmc;->b(Llol;)V

    .line 1004
    invoke-static {p0}, Lhmc;->a(Llol;)V

    .line 1007
    :cond_3
    return-void

    .line 939
    :pswitch_0
    iget-object v0, p0, Leet;->aB:Landroid/database/Cursor;

    if-eqz v0, :cond_4

    iget-object v0, p0, Leet;->aB:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 940
    iput-object v1, p0, Leet;->aB:Landroid/database/Cursor;

    .line 942
    :cond_4
    iget-object v0, p0, Leet;->aB:Landroid/database/Cursor;

    .line 943
    invoke-virtual {p0}, Leet;->w()Lbb;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3, v1, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    goto :goto_0

    .line 947
    :pswitch_1
    iget-object v0, p0, Leet;->az:Lfba;

    iget-object v1, p0, Leet;->aE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lfba;->b(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 948
    iput v2, p0, Leet;->X:I

    goto :goto_0

    .line 952
    :pswitch_2
    iget-object v0, p0, Leet;->aG:Landroid/database/Cursor;

    if-eqz v0, :cond_5

    iget-object v0, p0, Leet;->aG:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 953
    iput-object v1, p0, Leet;->aG:Landroid/database/Cursor;

    .line 955
    :cond_5
    iget-object v0, p0, Leet;->aG:Landroid/database/Cursor;

    .line 957
    sget-object v2, Leet;->N:Llpa;

    .line 958
    iget-boolean v2, p0, Leet;->aW:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Leet;->aG:Landroid/database/Cursor;

    if-nez v2, :cond_0

    .line 961
    invoke-virtual {p0}, Leet;->w()Lbb;

    move-result-object v2

    const/16 v3, 0x65

    invoke-virtual {v2, v3, v1, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    goto/16 :goto_0

    .line 966
    :pswitch_3
    iget-object v0, p0, Leet;->aH:Landroid/database/Cursor;

    if-eqz v0, :cond_6

    iget-object v0, p0, Leet;->aH:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 967
    iput-object v1, p0, Leet;->aH:Landroid/database/Cursor;

    .line 969
    :cond_6
    iget-object v0, p0, Leet;->aH:Landroid/database/Cursor;

    .line 970
    iget-boolean v2, p0, Leet;->aW:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Leet;->aH:Landroid/database/Cursor;

    if-nez v2, :cond_0

    .line 971
    invoke-virtual {p0}, Leet;->w()Lbb;

    move-result-object v2

    const/16 v3, 0x67

    invoke-virtual {v2, v3, v1, p0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    goto/16 :goto_0

    .line 976
    :pswitch_4
    iget-object v0, p0, Leet;->az:Lfba;

    invoke-virtual {v0}, Lfba;->g()Landroid/database/Cursor;

    move-result-object v0

    .line 977
    iput v2, p0, Leet;->X:I

    goto/16 :goto_0

    .line 981
    :pswitch_5
    iget-object v0, p0, Leet;->az:Lfba;

    invoke-virtual {v0}, Lfba;->e()Landroid/database/Cursor;

    move-result-object v0

    .line 982
    iput v2, p0, Leet;->X:I

    goto/16 :goto_0

    .line 937
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_3
    .end packed-switch
.end method

.method public d(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1155
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1156
    const-string v1, "geo:0,0?q="

    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1157
    invoke-virtual {p0}, Leet;->n()Lz;

    move-result-object v1

    invoke-static {v1, v0}, Lisy;->a(Landroid/content/Context;Landroid/net/Uri;)V

    .line 1159
    :cond_0
    return-void

    .line 1156
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public e()V
    .locals 0

    .prologue
    .line 3029
    return-void
.end method

.method public e(I)V
    .locals 7

    .prologue
    const/16 v6, 0x1a

    const/16 v5, 0xf

    const/16 v4, 0xd

    const/16 v3, 0xc

    const/16 v2, 0x8

    .line 1012
    iget-object v0, p0, Leet;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    .line 1013
    if-ne p1, v2, :cond_0

    .line 1015
    invoke-virtual {p0}, Leet;->n()Lz;

    move-result-object v1

    invoke-static {v1, v0}, Leyq;->c(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v0

    .line 1016
    const/16 v1, 0x6b

    invoke-virtual {p0, v0, v1}, Leet;->a(Landroid/content/Intent;I)V

    .line 1019
    :cond_0
    iget-object v0, p0, Leet;->az:Lfba;

    invoke-virtual {v0}, Lfba;->Y()[B

    move-result-object v0

    .line 1022
    packed-switch p1, :pswitch_data_0

    .line 1099
    :goto_0
    :pswitch_0
    return-void

    .line 1024
    :pswitch_1
    iget-object v1, p0, Leet;->az:Lfba;

    invoke-virtual {v1}, Lfba;->Z()[B

    move-result-object v1

    .line 1025
    const/4 v2, 0x1

    invoke-direct {p0, v2, v1, v0}, Leet;->b(I[B[B)V

    goto :goto_0

    .line 1028
    :pswitch_2
    iget-object v1, p0, Leet;->az:Lfba;

    invoke-virtual {v1}, Lfba;->V()[B

    move-result-object v1

    .line 1029
    iget-object v2, p0, Leet;->R:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    invoke-virtual {p0}, Leet;->n()Lz;

    move-result-object v3

    invoke-static {v3, v2, v6, v1, v0}, Leyq;->b(Landroid/content/Context;II[B[B)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x6a

    invoke-virtual {p0, v0, v1}, Leet;->a(Landroid/content/Intent;I)V

    invoke-direct {p0, v6}, Leet;->i(I)V

    goto :goto_0

    .line 1033
    :pswitch_3
    iget-object v0, p0, Leet;->az:Lfba;

    invoke-virtual {v0}, Lfba;->W()[B

    move-result-object v0

    .line 1034
    iget-object v1, p0, Leet;->az:Lfba;

    invoke-virtual {v1}, Lfba;->X()[B

    move-result-object v1

    .line 1035
    iget-object v2, p0, Leet;->at:Llnl;

    iget-object v3, p0, Leet;->R:Lhee;

    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    const/16 v4, 0x24

    const/4 v5, 0x0

    invoke-static {v2, v3, v4, v0, v5}, Leyq;->b(Landroid/content/Context;II[B[B)Landroid/content/Intent;

    move-result-object v0

    const-string v2, "squares_data_proto"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    const/16 v1, 0x6a

    invoke-virtual {p0, v0, v1}, Leet;->a(Landroid/content/Intent;I)V

    const/16 v0, 0x24

    invoke-direct {p0, v0}, Leet;->i(I)V

    goto :goto_0

    .line 1038
    :pswitch_4
    iget-object v1, p0, Leet;->az:Lfba;

    invoke-virtual {v1, v5}, Lfba;->f(I)[B

    move-result-object v1

    .line 1039
    invoke-direct {p0, v5, v1, v0}, Leet;->b(I[B[B)V

    goto :goto_0

    .line 1042
    :pswitch_5
    iget-object v1, p0, Leet;->az:Lfba;

    invoke-virtual {v1, v3}, Lfba;->f(I)[B

    move-result-object v1

    .line 1044
    invoke-direct {p0, v3, v1, v0}, Leet;->b(I[B[B)V

    goto :goto_0

    .line 1047
    :pswitch_6
    iget-object v1, p0, Leet;->az:Lfba;

    invoke-virtual {v1, v4}, Lfba;->f(I)[B

    move-result-object v1

    .line 1048
    invoke-direct {p0, v4, v1, v0}, Leet;->b(I[B[B)V

    goto :goto_0

    .line 1051
    :pswitch_7
    iget-object v1, p0, Leet;->az:Lfba;

    const/16 v2, 0x26

    invoke-virtual {v1, v2}, Lfba;->f(I)[B

    move-result-object v1

    .line 1052
    const/16 v2, 0x26

    invoke-direct {p0, v2, v1, v0}, Leet;->b(I[B[B)V

    goto :goto_0

    .line 1055
    :pswitch_8
    iget-object v1, p0, Leet;->az:Lfba;

    invoke-virtual {v1}, Lfba;->R()[B

    move-result-object v1

    .line 1056
    const/4 v2, 0x4

    invoke-direct {p0, v2, v1, v0}, Leet;->a(I[B[B)V

    goto/16 :goto_0

    .line 1060
    :pswitch_9
    iget-object v1, p0, Leet;->az:Lfba;

    invoke-virtual {v1}, Lfba;->S()[B

    move-result-object v1

    .line 1061
    const/4 v2, 0x3

    invoke-direct {p0, v2, v1, v0}, Leet;->a(I[B[B)V

    goto/16 :goto_0

    .line 1065
    :pswitch_a
    iget-object v1, p0, Leet;->az:Lfba;

    invoke-virtual {v1}, Lfba;->T()[B

    move-result-object v1

    .line 1066
    const/4 v2, 0x5

    invoke-direct {p0, v2, v1, v0}, Leet;->a(I[B[B)V

    goto/16 :goto_0

    .line 1071
    :pswitch_b
    iget-object v1, p0, Leet;->az:Lfba;

    invoke-virtual {v1}, Lfba;->aa()[B

    move-result-object v1

    .line 1072
    const/16 v2, 0xa

    invoke-direct {p0, v2, v1, v0}, Leet;->c(I[B[B)V

    goto/16 :goto_0

    .line 1075
    :pswitch_c
    iget-object v1, p0, Leet;->az:Lfba;

    invoke-virtual {v1}, Lfba;->ab()[B

    move-result-object v1

    .line 1076
    const/16 v2, 0x13

    invoke-direct {p0, v2, v1, v0}, Leet;->c(I[B[B)V

    goto/16 :goto_0

    .line 1079
    :pswitch_d
    iget-object v1, p0, Leet;->az:Lfba;

    invoke-virtual {v1}, Lfba;->ac()[B

    move-result-object v1

    .line 1080
    invoke-direct {p0, v2, v1, v0}, Leet;->c(I[B[B)V

    goto/16 :goto_0

    .line 1084
    :pswitch_e
    iget-object v1, p0, Leet;->az:Lfba;

    invoke-virtual {v1}, Lfba;->ad()[B

    move-result-object v1

    .line 1085
    const/16 v2, 0xe

    invoke-direct {p0, v2, v1, v0}, Leet;->a(I[B[B)V

    goto/16 :goto_0

    .line 1089
    :pswitch_f
    iget-object v1, p0, Leet;->az:Lfba;

    invoke-virtual {v1}, Lfba;->U()[B

    move-result-object v1

    .line 1090
    const/4 v2, 0x6

    invoke-direct {p0, v2, v1, v0}, Leet;->a(I[B[B)V

    goto/16 :goto_0

    .line 1094
    :pswitch_10
    iget-object v1, p0, Leet;->az:Lfba;

    invoke-virtual {v1}, Lfba;->U()[B

    move-result-object v1

    .line 1095
    const/4 v2, 0x7

    invoke-direct {p0, v2, v1, v0}, Leet;->a(I[B[B)V

    goto/16 :goto_0

    .line 1022
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_1
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 1558
    invoke-super {p0, p1}, Lehh;->e(Landroid/os/Bundle;)V

    .line 1559
    iget-object v0, p0, Leet;->aK:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 1560
    const-string v0, "profile_request_id"

    iget-object v1, p0, Leet;->aK:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1562
    :cond_0
    iget-object v0, p0, Leet;->aM:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 1563
    const-string v0, "abuse_request_id"

    iget-object v1, p0, Leet;->aM:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1565
    :cond_1
    iget-object v0, p0, Leet;->aO:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 1566
    const-string v0, "mute_request_id"

    iget-object v1, p0, Leet;->aO:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1567
    const-string v0, "mute_state"

    iget-boolean v1, p0, Leet;->aP:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1569
    :cond_2
    iget-object v0, p0, Leet;->aQ:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 1570
    const-string v0, "set_blocked_request_id"

    iget-object v1, p0, Leet;->aQ:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1572
    :cond_3
    iget-object v0, p0, Leet;->aN:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 1573
    const-string v0, "cover_photo_request_id"

    iget-object v1, p0, Leet;->aN:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1575
    :cond_4
    iget-object v0, p0, Leet;->aL:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 1576
    const-string v0, "albums_request_id"

    iget-object v1, p0, Leet;->aL:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1578
    :cond_5
    const-string v0, "already_tried_refresh"

    iget-boolean v1, p0, Leet;->aT:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1579
    const-string v0, "choose_photo_target"

    iget v1, p0, Leet;->aR:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1580
    const-string v0, "choose_photo_album_hint"

    iget v1, p0, Leet;->aS:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1581
    const-string v0, "contact_update"

    iget-wide v2, p0, Leet;->aD:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1582
    const-string v0, "profile_update"

    iget-object v1, p0, Leet;->aE:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1583
    const-string v0, "active_tab"

    iget v1, p0, Leet;->bc:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1584
    return-void
.end method

.method public e(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1163
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1164
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1165
    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1166
    invoke-direct {p0, v0}, Leet;->f(Landroid/content/Intent;)V

    .line 1168
    :cond_0
    return-void
.end method

.method public f(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 1172
    iget-object v0, p0, Leet;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v1

    .line 1174
    invoke-virtual {p0}, Leet;->n()Lz;

    move-result-object v2

    const-string v3, "g:"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const/4 v3, 0x0

    invoke-static {v2, v1, v0, v3}, Leyq;->d(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1175
    invoke-virtual {p0, v0}, Leet;->a(Landroid/content/Intent;)V

    .line 1176
    return-void

    .line 1174
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public g()V
    .locals 1

    .prologue
    .line 2038
    invoke-super {p0}, Lehh;->g()V

    .line 2039
    iget-object v0, p0, Leet;->az:Lfba;

    if-eqz v0, :cond_0

    .line 2040
    iget-object v0, p0, Leet;->az:Lfba;

    invoke-virtual {v0}, Lfba;->j()V

    .line 2042
    :cond_0
    return-void
.end method

.method public g(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1189
    return-void
.end method

.method public h()V
    .locals 1

    .prologue
    .line 2046
    invoke-super {p0}, Lehh;->h()V

    .line 2047
    iget-object v0, p0, Leet;->az:Lfba;

    if-eqz v0, :cond_0

    .line 2048
    iget-object v0, p0, Leet;->az:Lfba;

    invoke-virtual {v0}, Lfba;->k()V

    .line 2050
    :cond_0
    return-void
.end method

.method public h(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1192
    return-void
.end method

.method public i()V
    .locals 12

    .prologue
    const/16 v8, 0xfa

    const/4 v1, 0x2

    const/4 v10, 0x1

    .line 2426
    :try_start_0
    iget v0, p0, Leet;->aR:I

    if-ne v0, v10, :cond_1

    const/16 v0, 0x67

    move v11, v0

    .line 2430
    :goto_0
    iget v0, p0, Leet;->aR:I

    if-ne v0, v1, :cond_2

    const/4 v3, 0x3

    .line 2433
    :goto_1
    iget v0, p0, Leet;->aR:I

    if-ne v0, v1, :cond_3

    const/16 v9, 0x10e

    .line 2436
    :goto_2
    iget v0, p0, Leet;->aR:I

    if-ne v0, v1, :cond_0

    const/16 v8, 0x1e0

    .line 2439
    :cond_0
    iget-object v0, p0, Leet;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v1

    .line 2440
    invoke-virtual {p0}, Leet;->n()Lz;

    move-result-object v0

    const/4 v2, 0x1

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/String;

    .line 2441
    invoke-static {v2, v4}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x1

    .line 2443
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/4 v7, 0x1

    .line 2440
    invoke-static/range {v0 .. v9}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;IZILjava/lang/Integer;ZII)Landroid/content/Intent;

    move-result-object v0

    .line 2446
    invoke-virtual {p0, v0, v11}, Leet;->a(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2451
    :goto_3
    return-void

    .line 2426
    :cond_1
    const/16 v0, 0x68

    move v11, v0

    goto :goto_0

    :cond_2
    move v3, v10

    .line 2430
    goto :goto_1

    :cond_3
    move v9, v8

    .line 2433
    goto :goto_2

    .line 2448
    :catch_0
    move-exception v0

    invoke-virtual {p0}, Leet;->n()Lz;

    move-result-object v0

    const v1, 0x7f0a08ed

    invoke-static {v0, v1, v10}, Llih;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 2449
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_3
.end method

.method public i(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 1263
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.DIAL"

    const-string v3, "tel:"

    .line 1264
    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1263
    :goto_0
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1265
    const/high16 v0, 0x80000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1266
    invoke-virtual {p0, v1}, Leet;->a(Landroid/content/Intent;)V

    .line 1267
    return-void

    .line 1264
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public j(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1271
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1272
    invoke-virtual {p0}, Leet;->n()Lz;

    move-result-object v0

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Lisy;->a(Landroid/content/Context;Landroid/net/Uri;)V

    .line 1274
    :cond_0
    return-void
.end method

.method public k(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1278
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1279
    invoke-virtual {p0}, Leet;->n()Lz;

    move-result-object v0

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Lisy;->a(Landroid/content/Context;Landroid/net/Uri;)V

    .line 1281
    :cond_0
    return-void
.end method

.method public k_(Z)V
    .locals 3

    .prologue
    .line 761
    const/4 v0, 0x1

    iput v0, p0, Leet;->aR:I

    .line 762
    const-string v0, "change_photo"

    invoke-direct {p0, v0}, Leet;->s(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ldyu;

    const v1, 0x7f0a08e1

    invoke-direct {v0, v1}, Ldyu;-><init>(I)V

    invoke-virtual {p0}, Leet;->n()Lz;

    move-result-object v1

    invoke-static {v1}, Leyq;->b(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ldyu;->i(Z)V

    invoke-virtual {v0, p1}, Ldyu;->j(Z)V

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Ldyu;->a(Lu;I)V

    invoke-virtual {p0}, Leet;->p()Lae;

    move-result-object v1

    const-string v2, "change_photo"

    invoke-virtual {v0, v1, v2}, Ldyu;->a(Lae;Ljava/lang/String;)V

    .line 763
    :cond_0
    return-void
.end method

.method public l(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1285
    invoke-virtual {p0, p1}, Leet;->e(Ljava/lang/String;)V

    .line 1286
    return-void
.end method

.method public m(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1290
    invoke-virtual {p0, p1}, Leet;->e(Ljava/lang/String;)V

    .line 1291
    return-void
.end method

.method public n(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1295
    if-eqz p1, :cond_1

    .line 1296
    iget-object v0, p0, Leet;->at:Llnl;

    const-class v1, Lhxp;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhxp;

    .line 1297
    iget-object v1, p0, Leet;->at:Llnl;

    iget-object v2, p0, Leet;->R:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    invoke-interface {v0}, Lhxp;->c()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v1, v0}, Llnl;->startActivity(Landroid/content/Intent;)V

    .line 1304
    :cond_0
    :goto_0
    return-void

    .line 1299
    :cond_1
    invoke-virtual {p0}, Leet;->p()Lae;

    move-result-object v0

    const-string v1, "clx_create"

    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1300
    iget-object v0, p0, Leet;->bj:Lgcs;

    invoke-interface {v0}, Lgcs;->f()Llgr;

    move-result-object v0

    .line 1301
    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Llgr;->a(Lu;I)V

    .line 1302
    invoke-virtual {p0}, Leet;->p()Lae;

    move-result-object v1

    const-string v2, "clx_create"

    invoke-virtual {v0, v1, v2}, Llgr;->a(Lae;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public o(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1366
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Leet;->l(Z)V

    .line 1367
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const v7, 0x7f10007f

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2125
    sget-object v0, Leet;->N:Llpa;

    iget v0, p0, Leet;->bc:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    move v0, v3

    :goto_0
    if-eqz v0, :cond_4

    .line 2129
    :goto_1
    return-void

    .line 2125
    :cond_0
    iget-object v0, p0, Leet;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v5

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f1001c9

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Leet;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Leet;->at:Llnl;

    invoke-direct {v1, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->eL:Lhmv;

    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    invoke-virtual {p1, v7}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Leet;->az:Lfba;

    iget-object v2, v1, Lfba;->a:Levi;

    const v1, 0x7f100081

    invoke-virtual {p1, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v2, v1, v0}, Levi;->a(ILjava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Leet;->at:Llnl;

    invoke-static {v1, v5, v0}, Leyq;->a(Landroid/content/Context;ILjava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Leet;->a(Landroid/content/Intent;)V

    invoke-virtual {p0}, Leet;->n()Lz;

    move-result-object v0

    const v1, 0x7f05001e

    invoke-virtual {v0, v1, v3}, Lz;->overridePendingTransition(II)V

    move v0, v4

    goto :goto_0

    :cond_1
    const v0, 0x7f100079

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    move v0, v3

    goto :goto_0

    :cond_2
    const v1, 0x7f10007a

    invoke-virtual {p1, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {p1, v7}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const v2, 0x7f100098

    invoke-virtual {p1, v2}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-static {v2}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v2

    if-ne v6, v4, :cond_3

    if-nez v2, :cond_3

    const v1, 0x7f100092

    invoke-virtual {p1, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lizu;

    new-instance v2, Ldew;

    invoke-virtual {p0}, Leet;->n()Lz;

    move-result-object v6

    invoke-direct {v2, v6, v5}, Ldew;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v2, v0}, Ldew;->a(Ljava/lang/String;)Ldew;

    move-result-object v0

    invoke-virtual {v0, v1}, Ldew;->a(Lizu;)Ldew;

    move-result-object v0

    new-array v2, v4, [Ljava/lang/String;

    iget-object v5, p0, Leet;->aV:Ljava/lang/String;

    invoke-static {v5}, Ljpu;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v3

    invoke-static {v3, v2}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ldew;->d(Ljava/lang/String;)Ldew;

    move-result-object v0

    invoke-virtual {v0, v3}, Ldew;->i(Z)Ldew;

    move-result-object v0

    const/16 v2, 0x20

    invoke-virtual {v0, v2}, Ldew;->c(I)Ldew;

    move-result-object v0

    invoke-virtual {v0}, Ldew;->a()Landroid/content/Intent;

    move-result-object v2

    iget-object v0, p0, Leet;->at:Llnl;

    const-class v3, Lizs;

    invoke-static {v0, v3}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizs;

    const/4 v3, 0x5

    const/16 v5, 0x1040

    invoke-virtual {v0, v1, v3, v5}, Lizs;->c(Lizu;II)Lcom/google/android/libraries/social/media/MediaResource;

    iget-object v0, p0, Leet;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v3, p0, Leet;->at:Llnl;

    invoke-direct {v1, v3}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v3, Lhmv;->dZ:Lhmv;

    invoke-virtual {v1, v3}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    move-object v0, v2

    :goto_2
    invoke-virtual {p0, v0}, Leet;->a(Landroid/content/Intent;)V

    move v0, v4

    goto/16 :goto_0

    :cond_3
    invoke-virtual {p0}, Leet;->n()Lz;

    move-result-object v0

    invoke-static {v0, v5}, Leyq;->g(Landroid/content/Context;I)Ljuj;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljuj;->a(Ljava/lang/String;)Ljuj;

    move-result-object v0

    invoke-virtual {v0}, Ljuj;->a()Landroid/content/Intent;

    move-result-object v1

    iget-object v0, p0, Leet;->au:Llnh;

    const-class v2, Lhms;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    iget-object v3, p0, Leet;->at:Llnl;

    invoke-direct {v2, v3}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v3, Lhmv;->eb:Lhmv;

    invoke-virtual {v2, v3}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v2

    invoke-interface {v0, v2}, Lhms;->a(Lhmr;)V

    move-object v0, v1

    goto :goto_2

    .line 2128
    :cond_4
    invoke-super {p0, p1}, Lehh;->onClick(Landroid/view/View;)V

    goto/16 :goto_1
.end method

.method protected p(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1843
    iget-object v0, p0, Leet;->aa:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method public z()V
    .locals 1

    .prologue
    .line 2118
    invoke-super {p0}, Lehh;->z()V

    .line 2119
    iget-object v0, p0, Leet;->W:Lfdj;

    invoke-virtual {v0}, Lfdj;->ai()V

    .line 2120
    iget-object v0, p0, Leet;->bm:Lfhh;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(Lfhh;)V

    .line 2121
    return-void
.end method
