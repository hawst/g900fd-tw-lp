.class final Lejs;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lbc",
        "<",
        "Ljava/util/ArrayList",
        "<",
        "Ldvw;",
        ">;>;"
    }
.end annotation


# instance fields
.field private synthetic a:Lejr;


# direct methods
.method constructor <init>(Lejr;)V
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lejs;->a:Lejr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Ldvw;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 66
    new-instance v6, Lnqx;

    invoke-direct {v6}, Lnqx;-><init>()V

    .line 67
    iget-object v0, p0, Lejs;->a:Lejr;

    invoke-static {v0}, Lejr;->a(Lejr;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 75
    const/high16 v0, -0x80000000

    iput v0, v6, Lnqx;->b:I

    .line 78
    :goto_0
    const/16 v0, 0x32

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v6, Lnqx;->c:Ljava/lang/Integer;

    .line 79
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v6, Lnqx;->e:Ljava/lang/Boolean;

    .line 80
    new-instance v0, Lohl;

    invoke-direct {v0}, Lohl;-><init>()V

    iput-object v0, v6, Lnqx;->d:Lohl;

    .line 81
    iget-object v0, v6, Lnqx;->d:Lohl;

    iget-object v1, p0, Lejs;->a:Lejr;

    invoke-static {v1}, Lejr;->a(Lejr;)I

    move-result v1

    iput v1, v0, Lohl;->b:I

    .line 82
    iget-object v0, v6, Lnqx;->d:Lohl;

    iget-object v1, p0, Lejs;->a:Lejr;

    invoke-static {v1}, Lejr;->b(Lejr;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lohl;->a:Ljava/lang/String;

    .line 83
    iget-object v0, v6, Lnqx;->d:Lohl;

    iget-object v1, p0, Lejs;->a:Lejr;

    invoke-static {v1}, Lejr;->c(Lejr;)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lohl;->c:Ljava/lang/Integer;

    .line 84
    iget-object v0, v6, Lnqx;->d:Lohl;

    iget-object v1, p0, Lejs;->a:Lejr;

    invoke-static {v1}, Lejr;->d(Lejr;)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lohl;->d:Ljava/lang/Integer;

    .line 85
    new-instance v1, Lemi;

    iget-object v0, p0, Lejs;->a:Lejr;

    invoke-virtual {v0}, Lejr;->n()Lz;

    move-result-object v2

    iget-object v0, p0, Lejs;->a:Lejr;

    iget-object v0, v0, Lejr;->Z:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v3

    const-wide/32 v4, 0x7fffffff

    invoke-direct/range {v1 .. v6}, Lemi;-><init>(Landroid/content/Context;IJLnqx;)V

    return-object v1

    .line 69
    :pswitch_0
    const/4 v0, 0x3

    iput v0, v6, Lnqx;->b:I

    goto :goto_0

    .line 72
    :pswitch_1
    const/4 v0, 0x2

    iput v0, v6, Lnqx;->b:I

    goto :goto_0

    .line 67
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Ldvw;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 102
    return-void
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 62
    check-cast p2, Ljava/util/ArrayList;

    invoke-virtual {p0, p2}, Lejs;->a(Ljava/util/ArrayList;)V

    return-void
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ldvw;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 92
    iget-object v0, p0, Lejs;->a:Lejr;

    iput-boolean v2, v0, Lejr;->R:Z

    .line 93
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 94
    iget-object v1, p0, Lejs;->a:Lejr;

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldvw;

    iget-object v0, v0, Ldvw;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Lejr;->a(Ljava/util/ArrayList;)V

    .line 98
    :goto_0
    return-void

    .line 96
    :cond_0
    iget-object v0, p0, Lejs;->a:Lejr;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lejr;->a(Ljava/util/ArrayList;)V

    goto :goto_0
.end method
