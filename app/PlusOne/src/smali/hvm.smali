.class public final Lhvm;
.super Landroid/widget/TextView;
.source "PG"

# interfaces
.implements Lljh;


# static fields
.field private static a:Landroid/view/animation/Interpolator;


# instance fields
.field private final b:Lhve;

.field private c:Lhvo;

.field private d:I

.field private e:I

.field private f:[Ljava/lang/CharSequence;

.field private g:I

.field private h:Z

.field private i:Z

.field private j:I

.field private k:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    sput-object v0, Lhvm;->a:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/16 v3, 0x19

    const/4 v2, 0x0

    .line 179
    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 88
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhvm;->h:Z

    .line 91
    iput-boolean v2, p0, Lhvm;->i:Z

    .line 93
    const/4 v0, -0x1

    iput v0, p0, Lhvm;->j:I

    .line 95
    new-instance v0, Lhvn;

    invoke-direct {v0, p0}, Lhvn;-><init>(Lhvm;)V

    iput-object v0, p0, Lhvm;->k:Ljava/lang/Runnable;

    .line 194
    invoke-virtual {p0}, Lhvm;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 196
    invoke-static {v0}, Lhve;->a(Landroid/content/Context;)Lhve;

    move-result-object v1

    iput-object v1, p0, Lhvm;->b:Lhve;

    .line 198
    invoke-static {v0, p0, v3}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 204
    invoke-static {v0, v3}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v0

    .line 205
    invoke-static {v0}, Llib;->a(Landroid/text/TextPaint;)I

    move-result v0

    .line 206
    iget-object v1, p0, Lhvm;->b:Lhve;

    iget v1, v1, Lhve;->l:I

    mul-int/2addr v0, v1

    iget-object v1, p0, Lhvm;->b:Lhve;

    iget v1, v1, Lhve;->j:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iput v0, p0, Lhvm;->e:I

    .line 208
    iget-object v0, p0, Lhvm;->b:Lhve;

    iget v0, v0, Lhve;->l:I

    invoke-virtual {p0, v0}, Lhvm;->setMaxLines(I)V

    .line 211
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lhvm;->setGravity(I)V

    .line 214
    invoke-virtual {p0, v2}, Lhvm;->setMinimumHeight(I)V

    .line 215
    invoke-virtual {p0, v2}, Lhvm;->setMinimumWidth(I)V

    .line 216
    invoke-virtual {p0, v2}, Lhvm;->setMinHeight(I)V

    .line 217
    invoke-virtual {p0, v2}, Lhvm;->setMinWidth(I)V

    .line 180
    return-void
.end method

.method static synthetic a(Lhvm;)I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lhvm;->d:I

    return v0
.end method

.method static synthetic a(Lhvm;I)I
    .locals 0

    .prologue
    .line 39
    iput p1, p0, Lhvm;->j:I

    return p1
.end method

.method static synthetic b(Lhvm;)I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lhvm;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lhvm;->j:I

    return v0
.end method

.method static synthetic b(Lhvm;I)I
    .locals 0

    .prologue
    .line 39
    iput p1, p0, Lhvm;->g:I

    return p1
.end method

.method static synthetic c(Lhvm;)I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lhvm;->j:I

    return v0
.end method

.method static synthetic d(Lhvm;)I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lhvm;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lhvm;->g:I

    return v0
.end method

.method static synthetic e(Lhvm;)I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lhvm;->g:I

    return v0
.end method

.method static synthetic e()Landroid/view/animation/Interpolator;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lhvm;->a:Landroid/view/animation/Interpolator;

    return-object v0
.end method

.method static synthetic f(Lhvm;)Lhvo;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lhvm;->c:Lhvo;

    return-object v0
.end method

.method static synthetic g(Lhvm;)Z
    .locals 1

    .prologue
    .line 39
    iget-boolean v0, p0, Lhvm;->h:Z

    return v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 439
    invoke-virtual {p0}, Lhvm;->d()V

    .line 441
    iput-object v0, p0, Lhvm;->f:[Ljava/lang/CharSequence;

    .line 443
    iput-object v0, p0, Lhvm;->c:Lhvo;

    .line 444
    return-void
.end method

.method public a(Lhuo;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 235
    invoke-virtual {p0}, Lhvm;->d()V

    invoke-virtual {p0, v1, v1, v1, v1}, Lhvm;->setPadding(IIII)V

    invoke-virtual {p0, v2}, Lhvm;->setText(Ljava/lang/CharSequence;)V

    const-string v0, ""

    invoke-virtual {p0, v0}, Lhvm;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, v1}, Lhvm;->setHeight(I)V

    const/4 v0, -0x1

    iput v0, p0, Lhvm;->j:I

    invoke-virtual {p0, v1}, Lhvm;->a(Z)V

    iput-object v2, p0, Lhvm;->f:[Ljava/lang/CharSequence;

    iput v1, p0, Lhvm;->g:I

    iput v1, p0, Lhvm;->d:I

    .line 237
    if-nez p1, :cond_0

    .line 253
    :goto_0
    return-void

    .line 241
    :cond_0
    invoke-virtual {p1}, Lhuo;->a()I

    move-result v0

    const/4 v2, 0x3

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lhvm;->d:I

    .line 243
    iget v0, p0, Lhvm;->d:I

    if-gtz v0, :cond_1

    .line 244
    iput v1, p0, Lhvm;->d:I

    .line 247
    invoke-virtual {p0, v1}, Lhvm;->setHeight(I)V

    .line 248
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lhvm;->setVisibility(I)V

    goto :goto_0

    .line 250
    :cond_1
    iget v0, p0, Lhvm;->d:I

    new-array v0, v0, [Ljava/lang/CharSequence;

    iput-object v0, p0, Lhvm;->f:[Ljava/lang/CharSequence;

    move v0, v1

    :goto_1
    iget v2, p0, Lhvm;->d:I

    if-ge v0, v2, :cond_5

    invoke-virtual {p1, v0}, Lhuo;->a(I)Lhun;

    move-result-object v3

    new-instance v4, Landroid/text/SpannableStringBuilder;

    invoke-direct {v4}, Landroid/text/SpannableStringBuilder;-><init>()V

    invoke-virtual {v3}, Lhun;->a()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_6

    invoke-virtual {v4, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    new-instance v5, Landroid/text/style/StyleSpan;

    invoke-direct {v5, v8}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v6

    const/16 v7, 0x11

    invoke-virtual {v4, v5, v1, v6, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    iget-object v5, p0, Lhvm;->b:Lhve;

    iget-object v5, v5, Lhve;->a:Lfo;

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Lfo;->a(Ljava/lang/String;)Z

    move-result v2

    :goto_2
    invoke-virtual {v3}, Lhun;->b()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    iget-object v5, p0, Lhvm;->b:Lhve;

    iget-object v5, v5, Lhve;->a:Lfo;

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lfo;->a(Ljava/lang/String;)Z

    move-result v5

    invoke-virtual {v4}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v6

    if-lez v6, :cond_2

    if-ne v2, v5, :cond_4

    const-string v2, " "

    invoke-virtual {v4, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_2
    :goto_3
    invoke-virtual {v4, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_3
    iget-object v2, p0, Lhvm;->f:[Ljava/lang/CharSequence;

    aput-object v4, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    const-string v2, "\n"

    invoke-virtual {v4, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_3

    :cond_5
    invoke-virtual {p0}, Lhvm;->b()V

    iget-object v0, p0, Lhvm;->b:Lhve;

    iget v0, v0, Lhve;->j:I

    iget-object v1, p0, Lhvm;->b:Lhve;

    iget v1, v1, Lhve;->j:I

    iget-object v2, p0, Lhvm;->b:Lhve;

    iget v2, v2, Lhve;->j:I

    iget-object v3, p0, Lhvm;->b:Lhve;

    iget v3, v3, Lhve;->j:I

    invoke-virtual {p0, v0, v1, v2, v3}, Lhvm;->setPadding(IIII)V

    iget v0, p0, Lhvm;->e:I

    invoke-virtual {p0, v0}, Lhvm;->setHeight(I)V

    .line 251
    invoke-virtual {p0, v8}, Lhvm;->a(Z)V

    goto/16 :goto_0

    :cond_6
    move v2, v1

    goto :goto_2
.end method

.method public a(Lhvo;)V
    .locals 0

    .prologue
    .line 261
    iput-object p1, p0, Lhvm;->c:Lhvo;

    .line 262
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 352
    iget-boolean v0, p0, Lhvm;->i:Z

    if-eq v0, p1, :cond_0

    .line 353
    iput-boolean p1, p0, Lhvm;->i:Z

    .line 355
    iget-boolean v0, p0, Lhvm;->i:Z

    if-eqz v0, :cond_1

    .line 356
    invoke-virtual {p0}, Lhvm;->c()V

    .line 361
    :cond_0
    :goto_0
    return-void

    .line 358
    :cond_1
    invoke-virtual {p0}, Lhvm;->d()V

    goto :goto_0
.end method

.method b()V
    .locals 2

    .prologue
    .line 318
    iget-object v0, p0, Lhvm;->f:[Ljava/lang/CharSequence;

    iget v1, p0, Lhvm;->g:I

    aget-object v0, v0, v1

    invoke-virtual {p0, v0}, Lhvm;->setText(Ljava/lang/CharSequence;)V

    .line 319
    iget-object v0, p0, Lhvm;->f:[Ljava/lang/CharSequence;

    iget v1, p0, Lhvm;->g:I

    aget-object v0, v0, v1

    invoke-virtual {p0, v0}, Lhvm;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 320
    return-void
.end method

.method protected c()V
    .locals 2

    .prologue
    .line 387
    iget-boolean v0, p0, Lhvm;->i:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lhvm;->j:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 389
    invoke-static {}, Llsj;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 390
    invoke-static {p0}, Llii;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lhvm;->d:I

    if-lez v0, :cond_0

    .line 392
    invoke-static {}, Llsx;->d()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lhvm;->k:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 394
    :cond_0
    return-void
.end method

.method protected d()V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v2, -0x1

    .line 401
    iget v0, p0, Lhvm;->j:I

    if-eq v0, v2, :cond_1

    .line 402
    invoke-static {}, Llsx;->d()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lhvm;->k:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 403
    invoke-virtual {p0}, Lhvm;->clearAnimation()V

    .line 404
    invoke-static {}, Llsj;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 405
    invoke-static {p0}, Llii;->h(Landroid/view/View;)V

    .line 406
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0}, Lhvm;->setAlpha(F)V

    .line 408
    :cond_0
    iput v2, p0, Lhvm;->j:I

    .line 410
    :cond_1
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 0

    .prologue
    .line 368
    invoke-super {p0}, Landroid/widget/TextView;->onAttachedToWindow()V

    .line 369
    invoke-virtual {p0}, Lhvm;->c()V

    .line 370
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 377
    invoke-super {p0}, Landroid/widget/TextView;->onDetachedFromWindow()V

    .line 378
    invoke-virtual {p0}, Lhvm;->d()V

    .line 379
    return-void
.end method
