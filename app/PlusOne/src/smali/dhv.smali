.class public final Ldhv;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lheo;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)I
    .locals 3

    .prologue
    .line 124
    const-class v1, Ldhv;

    monitor-enter v1

    :try_start_0
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    const-string v2, "active-plus-account"

    invoke-interface {v0, v2}, Lhei;->c(Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Landroid/content/Context;ILfit;)J
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v7, 0x0

    .line 230
    .line 231
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 233
    const-string v1, "sync_status"

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "last_sync"

    aput-object v3, v2, v7

    const-string v3, "sync_data_kind = ?"

    new-array v4, v4, [Ljava/lang/String;

    iget v6, p2, Lfit;->e:I

    .line 236
    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v7

    move-object v6, v5

    move-object v7, v5

    .line 233
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 240
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 242
    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    return-wide v0

    .line 240
    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0

    .line 242
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static a(Landroid/content/Context;ILepn;)Ldwi;
    .locals 3

    .prologue
    .line 143
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 144
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "promo_stats"

    .line 145
    invoke-interface {v0, v1}, Lhej;->d(Ljava/lang/String;)Lhej;

    move-result-object v0

    .line 146
    invoke-virtual {p2}, Lepn;->b()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Lhej;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 147
    new-instance v1, Ldwi;

    invoke-direct {v1, v0}, Ldwi;-><init>(Ljava/lang/String;)V

    return-object v1
.end method

.method public static a([B)Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 830
    if-eqz p0, :cond_1

    .line 831
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 833
    const/4 v0, 0x0

    :try_start_0
    array-length v2, p0

    invoke-virtual {v1, p0, v0, v2}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 834
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 835
    invoke-virtual {v1}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 836
    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 840
    :cond_0
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 843
    :goto_0
    return-object v0

    .line 840
    :catch_0
    move-exception v0

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 843
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    goto :goto_0

    .line 840
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public static a(Landroid/content/Context;II)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 400
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    .line 401
    invoke-virtual {v0}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 402
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 403
    const-string v2, "contacts_sync_version"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 404
    const-string v2, "account_status"

    invoke-virtual {v0, v2, v1, v4, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 406
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 407
    return-void
.end method

.method public static a(Landroid/content/Context;IJ)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 312
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    .line 313
    invoke-virtual {v0}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 315
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 316
    const-string v2, "last_contacted_time"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 317
    const-string v2, "account_status"

    invoke-virtual {v0, v2, v1, v4, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 319
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 320
    return-void
.end method

.method public static a(Landroid/content/Context;ILfit;J)V
    .locals 5

    .prologue
    .line 250
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    .line 251
    invoke-virtual {v0}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 253
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 254
    const-string v2, "sync_data_kind"

    iget v3, p2, Lfit;->e:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 255
    const-string v2, "last_sync"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 256
    const-string v2, "sync_status"

    const/4 v3, 0x0

    const/4 v4, 0x5

    invoke-virtual {v0, v2, v3, v1, v4}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    .line 258
    return-void
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 180
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    invoke-interface {v0, p1}, Lhei;->b(I)Lhek;

    move-result-object v0

    .line 181
    if-eqz p2, :cond_0

    .line 182
    const-string v1, "profile_photo_url"

    invoke-interface {v0, v1, p2}, Lhek;->b(Ljava/lang/String;Ljava/lang/String;)Lhek;

    .line 186
    :goto_0
    invoke-interface {v0}, Lhek;->c()I

    .line 187
    return-void

    .line 184
    :cond_0
    const-string v1, "profile_photo_url"

    invoke-interface {v0, v1}, Lhek;->e(Ljava/lang/String;)Lhek;

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;ILjava/util/HashSet;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 678
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 679
    const-string v0, ","

    invoke-static {v0, p2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 684
    :goto_0
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 685
    invoke-interface {v0, p1}, Lhei;->b(I)Lhek;

    move-result-object v0

    const-string v2, "friend_location_circles"

    .line 686
    invoke-interface {v0, v2, v1}, Lhek;->b(Ljava/lang/String;Ljava/lang/String;)Lhek;

    move-result-object v0

    .line 687
    invoke-interface {v0}, Lhek;->c()I

    .line 688
    return-void

    .line 681
    :cond_0
    const-string v0, ""

    move-object v1, v0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;ILjava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 766
    const-wide/16 v4, -0x1

    const/4 v6, 0x0

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    invoke-static/range {v1 .. v6}, Ldhv;->a(Landroid/content/Context;ILjava/util/List;JZ)V

    .line 769
    return-void
.end method

.method private static a(Landroid/content/Context;ILjava/util/List;JZ)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;JZ)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 792
    .line 793
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 795
    new-instance v1, Landroid/content/ContentValues;

    const/4 v2, 0x3

    invoke-direct {v1, v2}, Landroid/content/ContentValues;-><init>(I)V

    .line 796
    const-string v2, "people_view_notification_count"

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 797
    const-string v2, "people_view_suggestions"

    .line 798
    invoke-static {p2}, Ldhv;->b(Ljava/util/List;)[B

    move-result-object v3

    .line 797
    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 799
    const-wide/16 v2, 0x0

    cmp-long v2, p3, v2

    if-lez v2, :cond_0

    .line 800
    const-string v2, "people_view_notification_poll_interval"

    .line 801
    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 800
    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 804
    :cond_0
    const-string v2, "account_status"

    invoke-virtual {v0, v2, v1, v4, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 806
    if-eqz p5, :cond_1

    .line 807
    const-class v0, Lhxb;

    .line 808
    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhxb;

    .line 809
    invoke-interface {v0, p2}, Lhxb;->a(Ljava/util/List;)V

    .line 813
    :cond_1
    sget-object v0, Lfit;->c:Lfit;

    .line 814
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 813
    invoke-static {p0, p1, v0, v2, v3}, Ldhv;->a(Landroid/content/Context;ILfit;J)V

    .line 816
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 817
    return-void
.end method

.method public static a(Landroid/content/Context;ILkfp;)V
    .locals 2

    .prologue
    .line 695
    new-instance v0, Ldjt;

    new-instance v1, Lkfo;

    invoke-direct {v1, p0, p1, p2}, Lkfo;-><init>(Landroid/content/Context;ILkfp;)V

    invoke-direct {v0, p0, v1, p1}, Ldjt;-><init>(Landroid/content/Context;Lkfo;I)V

    .line 697
    const-string v1, "App upgrade status"

    invoke-virtual {p2, v1}, Lkfp;->c(Ljava/lang/String;)V

    .line 699
    invoke-virtual {v0}, Ldjt;->l()V

    .line 700
    invoke-virtual {p2}, Lkfp;->f()V

    .line 702
    const-string v1, "EsAccountsData"

    invoke-virtual {v0, v1}, Ldjt;->e(Ljava/lang/String;)V

    .line 703
    return-void
.end method

.method public static a(Landroid/content/Context;IZ)V
    .locals 2

    .prologue
    .line 195
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    const-string v1, "active-plus-account"

    .line 196
    invoke-interface {v0, v1}, Lhei;->c(Ljava/lang/String;)I

    move-result v0

    .line 197
    if-eq v0, p1, :cond_0

    .line 205
    :goto_0
    return-void

    .line 201
    :cond_0
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 202
    invoke-interface {v0, p1}, Lhei;->b(I)Lhek;

    move-result-object v0

    const-string v1, "seen_plus_one_promo"

    .line 203
    invoke-interface {v0, v1, p2}, Lhek;->b(Ljava/lang/String;Z)Lhek;

    move-result-object v0

    .line 204
    invoke-interface {v0}, Lhek;->c()I

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;I[Lnrp;Z)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 773
    if-eqz p2, :cond_2

    array-length v0, p2

    .line 774
    :goto_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 775
    :goto_1
    if-ge v1, v0, :cond_3

    .line 776
    aget-object v2, p2, v1

    iget-object v2, v2, Lnrp;->b:Lohv;

    if-eqz v2, :cond_1

    aget-object v2, p2, v1

    iget-object v2, v2, Lnrp;->b:Lohv;

    iget-object v2, v2, Lohv;->b:Lohp;

    if-eqz v2, :cond_1

    aget-object v2, p2, v1

    iget-object v2, v2, Lnrp;->b:Lohv;

    iget-object v2, v2, Lohv;->b:Lohp;

    iget-object v2, v2, Lohp;->d:Ljava/lang/String;

    .line 777
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 778
    if-eqz p3, :cond_0

    aget-object v2, p2, v1

    iget-object v2, v2, Lnrp;->h:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 781
    :cond_0
    aget-object v2, p2, v1

    iget-object v2, v2, Lnrp;->b:Lohv;

    iget-object v2, v2, Lohv;->b:Lohp;

    iget-object v2, v2, Lohp;->d:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 775
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    move v0, v1

    .line 773
    goto :goto_0

    .line 785
    :cond_3
    const-wide/16 v4, -0x1

    const/4 v6, 0x1

    move-object v1, p0

    move v2, p1

    invoke-static/range {v1 .. v6}, Ldhv;->a(Landroid/content/Context;ILjava/util/List;JZ)V

    .line 788
    return-void
.end method

.method public static a(Landroid/content/Context;Z)V
    .locals 2

    .prologue
    .line 435
    const-string v0, "accounts"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 436
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 437
    const-string v1, "contacts_clean"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 438
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 439
    return-void
.end method

.method private static a(Lhei;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 1056
    invoke-interface {p0, p1}, Lhei;->c(Ljava/lang/String;)I

    move-result v0

    .line 1057
    if-eq v0, v1, :cond_0

    .line 1058
    invoke-interface {p0, v0}, Lhei;->a(I)Lhej;

    move-result-object v0

    invoke-interface {v0}, Lhej;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1059
    invoke-interface {p0, p1, v1}, Lhei;->a(Ljava/lang/String;I)V

    .line 1062
    :cond_0
    return-void
.end method

.method public static a(Landroid/content/Context;I)Z
    .locals 2

    .prologue
    .line 132
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 133
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "gplus_no_mobile_tos"

    .line 134
    invoke-interface {v0, v1}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static declared-synchronized b(Landroid/content/Context;)I
    .locals 3

    .prologue
    .line 128
    const-class v1, Ldhv;

    monitor-enter v1

    :try_start_0
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    const-string v2, "active-photos-account"

    invoke-interface {v0, v2}, Lhei;->c(Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static b(Landroid/content/Context;ILepn;)V
    .locals 3

    .prologue
    .line 156
    invoke-static {p0, p1, p2}, Ldhv;->a(Landroid/content/Context;ILepn;)Ldwi;

    move-result-object v0

    .line 157
    invoke-virtual {v0}, Ldwi;->c()Ldwi;

    move-result-object v0

    invoke-virtual {v0}, Ldwi;->toString()Ljava/lang/String;

    move-result-object v1

    .line 159
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 160
    invoke-interface {v0, p1}, Lhei;->b(I)Lhek;

    move-result-object v0

    const-string v2, "promo_stats"

    .line 161
    invoke-interface {v0, v2}, Lhek;->f(Ljava/lang/String;)Lhek;

    move-result-object v0

    .line 162
    invoke-virtual {p2}, Lepn;->b()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, v1}, Lhek;->b(Ljava/lang/String;Ljava/lang/String;)Lhek;

    move-result-object v0

    .line 163
    invoke-interface {v0}, Lhek;->c()I

    .line 164
    return-void
.end method

.method public static b(Landroid/content/Context;ILjava/lang/String;)V
    .locals 4

    .prologue
    .line 881
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 882
    invoke-interface {v0, p1}, Lhei;->b(I)Lhek;

    move-result-object v0

    const-string v1, "trending_topics"

    .line 883
    invoke-interface {v0, v1, p2}, Lhek;->b(Ljava/lang/String;Ljava/lang/String;)Lhek;

    move-result-object v0

    const-string v1, "trending_topics_ts"

    .line 884
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Lhek;->b(Ljava/lang/String;J)Lhek;

    move-result-object v0

    .line 885
    invoke-interface {v0}, Lhek;->c()I

    .line 886
    return-void
.end method

.method public static b(Landroid/content/Context;IZ)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 343
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    .line 344
    invoke-virtual {v0}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 346
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 347
    const-string v3, "wipeout_stats"

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 348
    const-string v0, "account_status"

    invoke-virtual {v1, v0, v2, v4, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 349
    return-void

    .line 347
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;Z)V
    .locals 2

    .prologue
    .line 484
    const-string v0, "accounts"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 485
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 486
    const-string v1, "contacts_stats_clean"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 487
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 488
    return-void
.end method

.method public static b(Landroid/content/Context;I)Z
    .locals 2

    .prologue
    .line 208
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    const-string v1, "active-plus-account"

    .line 209
    invoke-interface {v0, v1}, Lhei;->c(Ljava/lang/String;)I

    move-result v0

    .line 210
    if-eq v0, p1, :cond_0

    .line 212
    const/4 v0, 0x0

    .line 216
    :goto_0
    return v0

    .line 214
    :cond_0
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 215
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "seen_plus_one_promo"

    .line 216
    invoke-interface {v0, v1}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method private static b(Ljava/util/List;)[B
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)[B"
        }
    .end annotation

    .prologue
    .line 820
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 822
    :try_start_0
    invoke-virtual {v1, p0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 823
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 825
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public static c(Landroid/content/Context;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 220
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    .line 221
    invoke-virtual {v0}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 223
    const-string v1, "sync_status"

    invoke-virtual {v0, v1, v2, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 224
    return-void
.end method

.method public static c(Landroid/content/Context;ILepn;)V
    .locals 3

    .prologue
    .line 172
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 173
    invoke-interface {v0, p1}, Lhei;->b(I)Lhek;

    move-result-object v0

    const-string v1, "promo_stats"

    .line 174
    invoke-interface {v0, v1}, Lhek;->f(Ljava/lang/String;)Lhek;

    move-result-object v0

    .line 175
    invoke-virtual {p2}, Lepn;->b()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Lhek;->b(Ljava/lang/String;Ljava/lang/String;)Lhek;

    move-result-object v0

    .line 176
    invoke-interface {v0}, Lhek;->c()I

    .line 177
    return-void
.end method

.method public static c(Landroid/content/Context;IZ)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 372
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    .line 373
    invoke-virtual {v0}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 375
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 376
    const-string v2, "push_notifications"

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 377
    const-string v2, "account_status"

    invoke-virtual {v0, v2, v1, v4, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 378
    return-void
.end method

.method public static c(Landroid/content/Context;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 445
    const-string v0, "accounts"

    invoke-virtual {p0, v0, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 446
    const-string v1, "contacts_clean"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static d(Landroid/content/Context;I)J
    .locals 3

    .prologue
    .line 264
    .line 265
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 267
    :try_start_0
    const-string v1, "SELECT last_stats_sync_time  FROM account_status"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 272
    :goto_0
    return-wide v0

    :catch_0
    move-exception v0

    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public static d(Landroid/content/Context;IZ)V
    .locals 2

    .prologue
    .line 414
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 415
    invoke-interface {v0, p1}, Lhei;->b(I)Lhek;

    move-result-object v0

    const-string v1, "contacts_sync"

    .line 416
    invoke-interface {v0, v1, p2}, Lhek;->b(Ljava/lang/String;Z)Lhek;

    move-result-object v0

    .line 417
    invoke-interface {v0}, Lhek;->c()I

    .line 418
    return-void
.end method

.method public static d(Landroid/content/Context;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 494
    const-string v0, "accounts"

    invoke-virtual {p0, v0, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 495
    const-string v1, "contacts_stats_clean"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static e(Landroid/content/Context;I)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 280
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    .line 281
    invoke-virtual {v0}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 283
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 284
    const-string v2, "last_stats_sync_time"

    .line 285
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 284
    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 286
    const-string v2, "account_status"

    invoke-virtual {v0, v2, v1, v6, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 288
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 289
    return-void
.end method

.method public static e(Landroid/content/Context;IZ)V
    .locals 2

    .prologue
    .line 454
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 455
    invoke-interface {v0, p1}, Lhei;->b(I)Lhek;

    move-result-object v0

    const-string v1, "contacts_stats_sync"

    .line 456
    invoke-interface {v0, v1, p2}, Lhek;->b(Ljava/lang/String;Z)Lhek;

    move-result-object v0

    .line 457
    invoke-interface {v0}, Lhek;->c()I

    .line 458
    return-void
.end method

.method public static e(Landroid/content/Context;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 504
    const-string v0, "accounts"

    invoke-virtual {p0, v0, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 505
    const-string v1, "arkham_warm_welcome_shown"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static f(Landroid/content/Context;I)J
    .locals 3

    .prologue
    .line 295
    .line 296
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 298
    :try_start_0
    const-string v1, "SELECT last_contacted_time  FROM account_status"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 303
    :goto_0
    return-wide v0

    :catch_0
    move-exception v0

    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public static f(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 513
    const-string v0, "accounts"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 514
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 515
    const-string v1, "arkham_warm_welcome_shown"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 516
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 517
    return-void
.end method

.method public static f(Landroid/content/Context;IZ)V
    .locals 2

    .prologue
    .line 530
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 531
    invoke-interface {v0, p1}, Lhei;->b(I)Lhek;

    move-result-object v0

    const-string v1, "minor_public_extended_dialog"

    .line 532
    invoke-interface {v0, v1, p2}, Lhek;->b(Ljava/lang/String;Z)Lhek;

    move-result-object v0

    .line 533
    invoke-interface {v0}, Lhek;->c()I

    .line 534
    return-void
.end method

.method public static g(Landroid/content/Context;)I
    .locals 3

    .prologue
    .line 847
    const-string v0, "accounts"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 848
    const-string v1, "profile_action_status"

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static g(Landroid/content/Context;IZ)V
    .locals 2

    .prologue
    .line 937
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 938
    invoke-interface {v0, p1}, Lhei;->b(I)Lhek;

    move-result-object v0

    const-string v1, "auto_awesome_movies"

    .line 939
    invoke-interface {v0, v1, p2}, Lhek;->b(Ljava/lang/String;Z)Lhek;

    move-result-object v0

    .line 940
    invoke-interface {v0}, Lhek;->c()I

    .line 941
    invoke-static {p0}, Ljem;->a(Landroid/content/Context;)Ljem;

    move-result-object v0

    invoke-virtual {v0}, Ljem;->h()V

    .line 942
    return-void
.end method

.method public static g(Landroid/content/Context;I)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 326
    .line 327
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v1

    invoke-virtual {v1}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 329
    :try_start_0
    const-string v2, "SELECT wipeout_stats  FROM account_status"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    const-wide/16 v4, 0x1

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 334
    :cond_0
    :goto_0
    return v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static h(Landroid/content/Context;I)I
    .locals 3

    .prologue
    .line 355
    .line 356
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 358
    :try_start_0
    const-string v1, "SELECT contacts_sync_version  FROM account_status"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    long-to-int v0, v0

    .line 363
    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static h(Landroid/content/Context;IZ)V
    .locals 2

    .prologue
    .line 974
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 975
    invoke-interface {v0, p1}, Lhei;->b(I)Lhek;

    move-result-object v0

    const-string v1, "ab_status_bar_dismissed"

    .line 976
    invoke-interface {v0, v1, p2}, Lhek;->b(Ljava/lang/String;Z)Lhek;

    move-result-object v0

    .line 977
    invoke-interface {v0}, Lhek;->c()I

    .line 978
    return-void
.end method

.method public static h(Landroid/content/Context;)Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 904
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 905
    const v1, 0x7f0e0019

    .line 906
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v4

    .line 908
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 909
    new-array v1, v2, [Ljava/lang/String;

    const-string v5, "logged_in"

    aput-object v5, v1, v3

    invoke-interface {v0, v1}, Lhei;->a([Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 911
    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v1

    const-string v6, "auto_awesome_movies"

    .line 912
    invoke-interface {v1, v6, v4}, Lhej;->a(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    move v0, v2

    .line 917
    :goto_0
    return v0

    :cond_1
    move v0, v3

    goto :goto_0
.end method

.method public static i(Landroid/content/Context;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v1, 0x0

    const/4 v2, -0x1

    .line 1014
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    const-string v3, "accounts"

    invoke-virtual {p0, v3, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v4, "active_account"

    invoke-interface {v3, v4, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    if-eq v4, v2, :cond_0

    :try_start_0
    const-string v5, "active-plus-account"

    invoke-interface {v0, v5, v4}, Lhei;->a(Ljava/lang/String;I)V
    :try_end_0
    .catch Lhel; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    const-string v6, "active_account"

    invoke-interface {v5, v6}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_0
    const-string v5, "active_photos_account"

    invoke-interface {v3, v5, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    if-eq v4, v2, :cond_1

    :try_start_1
    const-string v4, "active-photos-account"

    invoke-interface {v0, v4, v5}, Lhei;->a(Ljava/lang/String;I)V
    :try_end_1
    .catch Lhel; {:try_start_1 .. :try_end_1} :catch_0

    :goto_1
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v3, "active_photos_account"

    invoke-interface {v0, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1015
    :cond_1
    const-string v0, "accounts"

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v3, "multi_account"

    invoke-interface {v0, v3, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_3

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    invoke-static {p0}, Ldhv;->a(Landroid/content/Context;)I

    move-result v3

    if-eq v3, v2, :cond_5

    invoke-interface {v0, v3}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v5, "is_plus_page"

    invoke-interface {v0, v5}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance v0, Ldpv;

    invoke-direct {v0, p0, v3}, Ldpv;-><init>(Landroid/content/Context;I)V

    invoke-static {p0, v0}, Lhoc;->a(Landroid/content/Context;Lhny;)V

    move v0, v2

    :goto_2
    if-eq v0, v2, :cond_2

    const-string v0, "logged_in"

    invoke-interface {v4, v0, v7}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    :cond_2
    const-string v0, "multi_account"

    invoke-interface {v4, v0, v7}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1016
    :cond_3
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    const-string v2, "active-plus-account"

    invoke-static {v0, v2}, Ldhv;->a(Lhei;Ljava/lang/String;)V

    const-string v2, "active-photos-account"

    invoke-static {v0, v2}, Ldhv;->a(Lhei;Ljava/lang/String;)V

    .line 1017
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "logged_in"

    aput-object v3, v2, v1

    invoke-interface {v0, v2}, Lhei;->a([Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    :goto_3
    if-ge v1, v3, :cond_4

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->a(Landroid/content/Context;I)V

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->d(Landroid/content/Context;I)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 1018
    :cond_4
    return-void

    :catch_0
    move-exception v0

    goto/16 :goto_1

    :catch_1
    move-exception v5

    goto/16 :goto_0

    :cond_5
    move v0, v3

    goto :goto_2
.end method

.method public static i(Landroid/content/Context;I)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 384
    .line 385
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v2

    invoke-virtual {v2}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 387
    :try_start_0
    const-string v3, "SELECT push_notifications FROM account_status"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    long-to-int v2, v2

    if-ne v2, v0, :cond_0

    .line 391
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 387
    goto :goto_0

    .line 391
    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_0
.end method

.method public static j(Landroid/content/Context;I)Z
    .locals 2

    .prologue
    .line 424
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 425
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 427
    const-string v1, "is_managed_account"

    invoke-interface {v0, v1}, Lhej;->c(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "contacts_sync"

    .line 428
    invoke-interface {v0, v1}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static k(Landroid/content/Context;I)Z
    .locals 2

    .prologue
    .line 464
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 465
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "contacts_stats_sync"

    .line 466
    invoke-interface {v0, v1}, Lhej;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static l(Landroid/content/Context;I)Z
    .locals 2

    .prologue
    .line 473
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 474
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 476
    const-string v1, "is_managed_account"

    invoke-interface {v0, v1}, Lhej;->c(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "contacts_stats_sync"

    .line 477
    invoke-interface {v0, v1}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static m(Landroid/content/Context;I)Z
    .locals 2

    .prologue
    .line 520
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 521
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "minor_public_extended_dialog"

    .line 522
    invoke-interface {v0, v1}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static n(Landroid/content/Context;I)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 537
    new-instance v1, Lnyr;

    invoke-direct {v1}, Lnyr;-><init>()V

    .line 538
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v1, Lnyr;->h:Ljava/lang/Boolean;

    .line 539
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v1, Lnyr;->l:Ljava/lang/Boolean;

    .line 540
    invoke-static {v1}, Ldtu;->a(Lnyr;)V

    .line 541
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 542
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v2, "gaia_id"

    .line 543
    invoke-interface {v0, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 545
    new-instance v2, Lhqc;

    new-instance v3, Lkfo;

    const/4 v4, 0x0

    invoke-direct {v3, p0, p1, v5, v4}, Lkfo;-><init>(Landroid/content/Context;IZLkfg;)V

    invoke-direct {v2, p0, v3, v0, v1}, Lhqc;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Lnyr;)V

    .line 548
    invoke-virtual {v2}, Lhqc;->l()V

    .line 549
    invoke-virtual {v2}, Lhqc;->t()Z

    move-result v0

    if-nez v0, :cond_2

    .line 550
    invoke-virtual {v2}, Lhqc;->i()Lnyq;

    move-result-object v1

    .line 551
    iget-object v0, v1, Lnyq;->j:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {p0, v2, v3}, Lhqd;->a(Landroid/content/Context;J)V

    .line 552
    invoke-static {p1, v1}, Ldtu;->a(ILnyq;)V

    .line 554
    iget-object v0, v1, Lnyq;->i:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 555
    iget-object v0, v1, Lnyq;->i:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    invoke-interface {v0, p1}, Lhei;->b(I)Lhek;

    move-result-object v0

    const-string v3, "auto_awesome"

    invoke-interface {v0, v3, v2}, Lhek;->b(Ljava/lang/String;Z)Lhek;

    move-result-object v0

    invoke-interface {v0}, Lhek;->c()I

    invoke-static {p0}, Ljem;->a(Landroid/content/Context;)Ljem;

    move-result-object v0

    invoke-virtual {v0}, Ljem;->h()V

    .line 558
    :cond_0
    iget-object v0, v1, Lnyq;->n:Lnyv;

    if-eqz v0, :cond_1

    iget-object v0, v1, Lnyq;->n:Lnyv;

    iget-object v0, v0, Lnyv;->a:Ljava/lang/Long;

    if-eqz v0, :cond_1

    iget-object v0, v1, Lnyq;->n:Lnyv;

    iget-object v0, v0, Lnyv;->b:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 561
    new-instance v0, Lhrp;

    new-instance v2, Lhpz;

    invoke-direct {v2, p0}, Lhpz;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, p0, p1, v2}, Lhrp;-><init>(Landroid/content/Context;ILhrr;)V

    iget-object v2, v1, Lnyq;->n:Lnyv;

    iget-object v2, v2, Lnyv;->a:Ljava/lang/Long;

    .line 563
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v1, v1, Lnyq;->n:Lnyv;

    iget-object v1, v1, Lnyv;->b:Ljava/lang/Long;

    .line 564
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 562
    invoke-virtual {v0, v2, v3, v4, v5}, Lhrp;->a(JJ)V

    .line 569
    :cond_1
    :goto_0
    return-void

    .line 567
    :cond_2
    const-string v0, "EsAccountsData"

    invoke-virtual {v2, v0}, Lhqc;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static o(Landroid/content/Context;I)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 604
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 605
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 607
    const-string v1, "settings_synced"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lhej;->a(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 623
    :cond_0
    :goto_0
    return-void

    .line 611
    :cond_1
    const-string v1, "warm_welcome_ts"

    invoke-interface {v0, v1, v4, v5}, Lhej;->a(Ljava/lang/String;J)J

    move-result-wide v0

    .line 612
    cmp-long v2, v0, v4

    if-eqz v2, :cond_0

    .line 616
    new-instance v2, Ldmx;

    .line 617
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-direct {v2, p0, p1, v0}, Ldmx;-><init>(Landroid/content/Context;ILjava/lang/Long;)V

    .line 618
    invoke-virtual {v2}, Ldmx;->l()V

    .line 620
    invoke-virtual {v2}, Ldmx;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 621
    const-string v0, "EsAccountsData"

    iget v1, v2, Lkff;->i:I

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x26

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Could not upload settings: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, v2, Lkff;->k:Ljava/lang/Exception;

    invoke-static {v0, v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static p(Landroid/content/Context;I)Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 629
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 630
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "stream_views"

    .line 631
    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 633
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 634
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 635
    array-length v3, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v1, v0

    .line 636
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 637
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 635
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 641
    :cond_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 642
    const-string v0, "v.whatshot"

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 643
    const-string v0, "v.all.circles"

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 644
    const-string v0, "v.nearby"

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 647
    :cond_2
    return-object v2
.end method

.method public static q(Landroid/content/Context;I)Ljava/util/HashSet;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 654
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 655
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "friend_location_circles"

    .line 656
    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 658
    if-nez v0, :cond_1

    .line 659
    const/4 v0, 0x0

    .line 669
    :cond_0
    return-object v0

    .line 661
    :cond_1
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 662
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 663
    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 664
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 665
    invoke-virtual {v0, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 663
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static r(Landroid/content/Context;I)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v6, 0x1

    .line 706
    new-instance v0, Ldkx;

    new-instance v1, Lkfo;

    invoke-direct {v1, p0, p1, v6, v9}, Lkfo;-><init>(Landroid/content/Context;IZLkfg;)V

    invoke-direct {v0, p0, v1, p1}, Ldkx;-><init>(Landroid/content/Context;Lkfo;I)V

    .line 710
    invoke-virtual {v0}, Ldkx;->l()V

    .line 711
    invoke-virtual {v0}, Ldkx;->t()Z

    move-result v1

    if-nez v1, :cond_1

    .line 712
    invoke-static {p0, p1}, Ldhv;->z(Landroid/content/Context;I)I

    move-result v7

    .line 713
    invoke-virtual {v0}, Ldkx;->b()Ljava/util/ArrayList;

    move-result-object v3

    .line 714
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v8

    .line 718
    invoke-virtual {v0}, Ldkx;->c()J

    move-result-wide v4

    move-object v1, p0

    move v2, p1

    .line 717
    invoke-static/range {v1 .. v6}, Ldhv;->a(Landroid/content/Context;ILjava/util/List;JZ)V

    .line 721
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->a:Landroid/net/Uri;

    .line 722
    invoke-static {v1, p1}, Lcom/google/android/apps/plus/content/EsProvider;->a(Landroid/net/Uri;I)Landroid/net/Uri;

    move-result-object v1

    .line 721
    invoke-virtual {v0, v1, v9}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 727
    if-le v8, v7, :cond_0

    .line 728
    new-instance v0, Ldhw;

    invoke-direct {v0, p0, p1}, Ldhw;-><init>(Landroid/content/Context;I)V

    invoke-static {v0}, Llsx;->a(Ljava/lang/Runnable;)V

    .line 742
    :cond_0
    :goto_0
    return-void

    .line 740
    :cond_1
    const-string v1, "EsAccountsData"

    invoke-virtual {v0, v1}, Ldkx;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static s(Landroid/content/Context;I)V
    .locals 2

    .prologue
    .line 852
    const-string v0, "accounts"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 853
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 854
    const-string v1, "profile_action_status"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 855
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 856
    return-void
.end method

.method public static t(Landroid/content/Context;I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 862
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 863
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "trending_topics"

    .line 864
    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static u(Landroid/content/Context;I)J
    .locals 4

    .prologue
    .line 871
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 872
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "trending_topics_ts"

    const-wide/16 v2, 0x0

    .line 873
    invoke-interface {v0, v1, v2, v3}, Lhej;->a(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static v(Landroid/content/Context;I)Z
    .locals 3

    .prologue
    .line 894
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 895
    const v1, 0x7f0e0018

    .line 896
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 897
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 898
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 899
    const-string v2, "auto_awesome"

    invoke-interface {v0, v2, v1}, Lhej;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static w(Landroid/content/Context;I)Z
    .locals 4

    .prologue
    .line 921
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 923
    const v1, 0x7f0e0018

    .line 924
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 925
    const v2, 0x7f0e0019

    .line 926
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    .line 928
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 929
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 931
    const-string v3, "auto_awesome"

    invoke-interface {v0, v3, v1}, Lhej;->a(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "auto_awesome_movies"

    .line 932
    invoke-interface {v0, v1, v2}, Lhej;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static x(Landroid/content/Context;I)J
    .locals 3

    .prologue
    .line 957
    .line 958
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 960
    :try_start_0
    const-string v1, "SELECT people_view_notification_poll_interval  FROM account_status"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 965
    :goto_0
    return-wide v0

    :catch_0
    move-exception v0

    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public static y(Landroid/content/Context;I)Z
    .locals 2

    .prologue
    .line 984
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 985
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "ab_status_bar_dismissed"

    .line 986
    invoke-interface {v0, v1}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static z(Landroid/content/Context;I)I
    .locals 10

    .prologue
    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 745
    .line 746
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 749
    :try_start_0
    const-string v1, "account_status"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "people_view_notification_count"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 753
    if-eqz v1, :cond_1

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 754
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    .line 757
    if-eqz v1, :cond_0

    .line 758
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 761
    :cond_0
    :goto_0
    return v0

    .line 757
    :cond_1
    if-eqz v1, :cond_2

    .line 758
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    move v0, v8

    .line 761
    goto :goto_0

    .line 757
    :catchall_0
    move-exception v0

    move-object v1, v9

    :goto_1
    if-eqz v1, :cond_3

    .line 758
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 757
    :catchall_1
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method public a(Landroid/content/Context;Lhem;)V
    .locals 0

    .prologue
    .line 991
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lheq;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 996
    new-instance v0, Ldhx;

    invoke-direct {v0}, Ldhx;-><init>()V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1011
    return-void
.end method
