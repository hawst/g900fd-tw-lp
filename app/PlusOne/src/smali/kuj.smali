.class public final Lkuj;
.super Llol;
.source "PG"

# interfaces
.implements Lbc;
.implements Lhjj;
.implements Lhjn;
.implements Lhmm;
.implements Lhmq;
.implements Lhob;
.implements Lkta;
.implements Lktb;
.implements Lktc;
.implements Lkvd;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Llol;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lhjj;",
        "Lhjn;",
        "Lhmm;",
        "Lhmq;",
        "Lhob;",
        "Lkta;",
        "Lktb;",
        "Lktc;",
        "Lkvd;"
    }
.end annotation


# static fields
.field private static final N:[Ljava/lang/String;


# instance fields
.field private final O:Lhje;

.field private final P:Lkuu;

.field private final Q:Lkuv;

.field private final R:Licq;

.field private S:Lhee;

.field private T:Ljava/lang/String;

.field private U:I

.field private V:I

.field private W:Landroid/widget/ListView;

.field private X:Lkvc;

.field private Y:Lkuw;

.field private Z:I

.field private aa:I

.field private ab:Z

.field private ac:I

.field private ad:Lcom/google/android/libraries/social/actionbar/ActionBarSpinner;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 93
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "membership_status"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "joinability"

    aput-object v2, v0, v1

    sput-object v0, Lkuj;->N:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 69
    invoke-direct {p0}, Llol;-><init>()V

    .line 101
    new-instance v0, Lhje;

    iget-object v1, p0, Lkuj;->av:Llqm;

    invoke-direct {v0, p0, v1, p0}, Lhje;-><init>(Lu;Llqr;Lhjj;)V

    iput-object v0, p0, Lkuj;->O:Lhje;

    .line 105
    new-instance v0, Lkuu;

    iget-object v1, p0, Lkuj;->av:Llqm;

    invoke-direct {v0, p0, v1}, Lkuu;-><init>(Lu;Llqr;)V

    iput-object v0, p0, Lkuj;->P:Lkuu;

    .line 107
    new-instance v0, Lkuv;

    iget-object v1, p0, Lkuj;->av:Llqm;

    iget-object v2, p0, Lkuj;->P:Lkuu;

    invoke-direct {v0, v1, v2}, Lkuv;-><init>(Llqr;Lkuu;)V

    iput-object v0, p0, Lkuj;->Q:Lkuv;

    .line 109
    new-instance v0, Licq;

    iget-object v1, p0, Lkuj;->av:Llqm;

    invoke-direct {v0, v1}, Licq;-><init>(Llqr;)V

    const v1, 0x7f0a023d

    .line 110
    invoke-virtual {v0, v1}, Licq;->b(I)Licq;

    move-result-object v0

    iput-object v0, p0, Lkuj;->R:Licq;

    .line 115
    new-instance v0, Lhmf;

    iget-object v1, p0, Lkuj;->av:Llqm;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lhmf;-><init>(Llqr;B)V

    .line 116
    return-void
.end method

.method private U()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 329
    iput v2, p0, Lkuj;->ac:I

    .line 330
    invoke-virtual {p0}, Lkuj;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1, p0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 332
    iget-object v0, p0, Lkuj;->W:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setSelection(I)V

    .line 333
    return-void
.end method

.method private V()Z
    .locals 1

    .prologue
    .line 336
    iget-object v0, p0, Lkuj;->X:Lkvc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lkuj;->X:Lkvc;

    invoke-virtual {v0}, Lkvc;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private W()V
    .locals 8

    .prologue
    .line 465
    iget-object v0, p0, Lkuj;->au:Llnh;

    const-class v1, Lhoc;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lhoc;

    .line 466
    const-string v0, "fetch_newer"

    invoke-virtual {v7, v0}, Lhoc;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lkuj;->n()Lz;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 467
    new-instance v0, Lkvb;

    invoke-virtual {p0}, Lkuj;->n()Lz;

    move-result-object v1

    iget-object v2, p0, Lkuj;->S:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    iget-object v3, p0, Lkuj;->T:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x64

    invoke-direct/range {v0 .. v6}, Lkvb;-><init>(Landroid/content/Context;ILjava/lang/String;ILjava/lang/String;I)V

    .line 470
    const-string v1, "fetch_newer"

    invoke-virtual {v0, v1}, Lhny;->b(Ljava/lang/String;)Lhny;

    .line 471
    invoke-virtual {v7, v0}, Lhoc;->b(Lhny;)V

    .line 474
    :cond_0
    iget-object v0, p0, Lkuj;->O:Lhje;

    invoke-virtual {v0}, Lhje;->a()V

    .line 475
    return-void
.end method

.method private c(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/16 v4, 0x8

    .line 348
    const v0, 0x102000a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 349
    const v0, 0x7f100249

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 350
    iget-boolean v0, p0, Lkuj;->ab:Z

    if-eqz v0, :cond_0

    .line 351
    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 352
    invoke-virtual {v3, v1}, Landroid/view/View;->setVisibility(I)V

    .line 353
    iget-object v0, p0, Lkuj;->R:Licq;

    invoke-virtual {v0}, Licq;->e()V

    .line 368
    :goto_0
    iget-object v0, p0, Lkuj;->O:Lhje;

    invoke-virtual {v0}, Lhje;->a()V

    .line 369
    return-void

    .line 354
    :cond_0
    invoke-direct {p0}, Lkuj;->V()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 355
    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 356
    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 357
    iget-object v0, p0, Lkuj;->R:Licq;

    invoke-virtual {v0}, Licq;->a()V

    goto :goto_0

    .line 358
    :cond_1
    invoke-direct {p0}, Lkuj;->V()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lkuj;->X:Lkvc;

    invoke-virtual {v0}, Lkvc;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_4

    .line 359
    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 360
    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 361
    iget-object v0, p0, Lkuj;->R:Licq;

    invoke-virtual {v0}, Licq;->d()V

    goto :goto_0

    :cond_3
    move v0, v1

    .line 358
    goto :goto_1

    .line 363
    :cond_4
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 364
    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 365
    iget-object v0, p0, Lkuj;->R:Licq;

    invoke-virtual {v0}, Licq;->e()V

    goto :goto_0
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 461
    sget-object v0, Lhmw;->I:Lhmw;

    return-object v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 211
    const v0, 0x7f0401f8

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 212
    const v0, 0x102000a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lkuj;->W:Landroid/widget/ListView;

    .line 213
    iget-object v0, p0, Lkuj;->W:Landroid/widget/ListView;

    iget-object v2, p0, Lkuj;->X:Lkvc;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 215
    invoke-virtual {p0}, Lkuj;->w()Lbb;

    move-result-object v0

    invoke-virtual {v0, v3, v4, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 216
    invoke-virtual {p0}, Lkuj;->w()Lbb;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v4, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 218
    return-object v1
.end method

.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 373
    packed-switch p1, :pswitch_data_0

    .line 383
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 375
    :pswitch_0
    new-instance v0, Lkvi;

    invoke-virtual {p0}, Lkuj;->n()Lz;

    move-result-object v1

    iget-object v2, p0, Lkuj;->S:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    iget-object v3, p0, Lkuj;->T:Ljava/lang/String;

    iget v4, p0, Lkuj;->aa:I

    sget-object v5, Lkvc;->a:[Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lkvi;-><init>(Landroid/content/Context;ILjava/lang/String;I[Ljava/lang/String;)V

    goto :goto_0

    .line 379
    :pswitch_1
    new-instance v0, Lktn;

    iget-object v1, p0, Lkuj;->at:Llnl;

    iget-object v2, p0, Lkuj;->S:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    iget-object v3, p0, Lkuj;->T:Ljava/lang/String;

    sget-object v4, Lkuj;->N:[Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Lktn;-><init>(Landroid/content/Context;ILjava/lang/String;[Ljava/lang/String;)V

    goto :goto_0

    .line 373
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    .line 169
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 171
    invoke-virtual {p0}, Lkuj;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 172
    const-string v1, "square_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lkuj;->T:Ljava/lang/String;

    .line 174
    if-eqz p1, :cond_0

    .line 175
    const-string v0, "membership_status"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lkuj;->U:I

    .line 177
    const-string v0, "joinability"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lkuj;->V:I

    .line 179
    const-string v0, "current_member_list"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lkuj;->aa:I

    .line 194
    :goto_0
    new-instance v0, Lkuw;

    iget-object v1, p0, Lkuj;->at:Llnl;

    const v2, 0x7f040037

    invoke-direct {v0, v1, v2}, Lkuw;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lkuj;->Y:Lkuw;

    .line 196
    new-instance v1, Lkvc;

    iget-object v2, p0, Lkuj;->at:Llnl;

    iget v0, p0, Lkuj;->U:I

    .line 197
    invoke-static {v0}, Lkto;->a(I)Z

    move-result v3

    iget-object v0, p0, Lkuj;->au:Llnh;

    const-class v4, Lkve;

    invoke-virtual {v0, v4}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkve;

    invoke-direct {v1, v2, v3, v0, p0}, Lkvc;-><init>(Landroid/content/Context;ZLkve;Lkvd;)V

    iput-object v1, p0, Lkuj;->X:Lkvc;

    .line 198
    return-void

    .line 182
    :cond_0
    const-string v1, "square_membership"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lkuj;->U:I

    .line 184
    const-string v1, "square_joinability"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lkuj;->V:I

    .line 186
    invoke-virtual {p0}, Lkuj;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "square_member_list_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 187
    invoke-virtual {p0}, Lkuj;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "square_member_list_type"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lkuj;->aa:I

    .line 191
    :cond_1
    invoke-direct {p0}, Lkuj;->W()V

    goto :goto_0
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 457
    return-void
.end method

.method public a(Ldo;Landroid/database/Cursor;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 388
    invoke-virtual {p1}, Ldo;->o()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 453
    :cond_0
    :goto_0
    return-void

    .line 390
    :pswitch_0
    if-nez p2, :cond_6

    :goto_1
    iput-boolean v0, p0, Lkuj;->ab:Z

    .line 391
    const/4 v0, 0x0

    .line 393
    instance-of v2, p1, Lkvi;

    if-eqz v2, :cond_5

    .line 394
    check-cast p1, Lkvi;

    .line 395
    iget-boolean v2, p0, Lkuj;->ab:Z

    if-nez v2, :cond_1

    invoke-virtual {p1}, Lkvi;->D()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 397
    invoke-direct {p0}, Lkuj;->W()V

    .line 399
    :cond_1
    invoke-virtual {p1}, Lkvi;->F()I

    move-result v2

    iput v2, p0, Lkuj;->ac:I

    .line 405
    if-eqz p2, :cond_2

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    .line 406
    :cond_2
    iget v2, p0, Lkuj;->ac:I

    sub-int/2addr v2, v1

    .line 408
    if-lez v2, :cond_3

    const/16 v3, 0x1f4

    if-ge v1, v3, :cond_3

    .line 410
    invoke-virtual {p1}, Lkvi;->E()Ljava/lang/String;

    move-result-object v0

    .line 412
    :cond_3
    const-string v3, "HostedSquareMemberList"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 413
    iget v3, p0, Lkuj;->ac:I

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x39

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "onLoadFinished count="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " totalMembers="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_4
    move v1, v2

    .line 417
    :cond_5
    iget-object v2, p0, Lkuj;->X:Lkvc;

    invoke-virtual {v2, p2, v0, v1}, Lkvc;->a(Landroid/database/Cursor;Ljava/lang/String;I)V

    .line 418
    invoke-virtual {p0}, Lkuj;->x()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lkuj;->c(Landroid/view/View;)V

    goto :goto_0

    :cond_6
    move v0, v1

    .line 390
    goto :goto_1

    .line 423
    :pswitch_1
    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 424
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 425
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 428
    iget v4, p0, Lkuj;->U:I

    if-eq v2, v4, :cond_7

    .line 429
    iput v2, p0, Lkuj;->U:I

    .line 430
    iget-object v1, p0, Lkuj;->X:Lkvc;

    iget v2, p0, Lkuj;->U:I

    invoke-static {v2}, Lkto;->a(I)Z

    move-result v2

    invoke-virtual {v1, v2}, Lkvc;->a(Z)V

    move v1, v0

    .line 434
    :cond_7
    iget v2, p0, Lkuj;->V:I

    if-eq v3, v2, :cond_8

    .line 435
    iput v3, p0, Lkuj;->V:I

    .line 439
    :goto_2
    if-eqz v0, :cond_0

    .line 440
    iget-object v0, p0, Lkuj;->Y:Lkuw;

    invoke-virtual {v0}, Lkuw;->a()V

    .line 441
    iget-object v0, p0, Lkuj;->Y:Lkuw;

    iget v1, p0, Lkuj;->aa:I

    .line 442
    invoke-virtual {v0, v1}, Lkuw;->a(I)I

    move-result v0

    iput v0, p0, Lkuj;->Z:I

    .line 445
    iget-object v0, p0, Lkuj;->Y:Lkuw;

    iget v1, p0, Lkuj;->Z:I

    .line 446
    invoke-virtual {v0, v1}, Lkuw;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkux;

    invoke-virtual {v0}, Lkux;->a()I

    move-result v0

    iput v0, p0, Lkuj;->aa:I

    .line 447
    iget-object v0, p0, Lkuj;->ad:Lcom/google/android/libraries/social/actionbar/ActionBarSpinner;

    iget v1, p0, Lkuj;->Z:I

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/actionbar/ActionBarSpinner;->setSelection(I)V

    .line 448
    invoke-direct {p0}, Lkuj;->U()V

    goto/16 :goto_0

    :cond_8
    move v0, v1

    goto :goto_2

    .line 388
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 69
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lkuj;->a(Ldo;Landroid/database/Cursor;)V

    return-void
.end method

.method public a(Lhjk;)V
    .locals 2

    .prologue
    .line 257
    const v0, 0x7f10067b

    .line 258
    invoke-interface {p1, v0}, Lhjk;->a(I)Lhjc;

    move-result-object v0

    check-cast v0, Lhjv;

    .line 259
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lhjv;->a(I)V

    .line 261
    const v0, 0x7f100680

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    .line 262
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 479
    iget-object v0, p0, Lkuj;->Y:Lkuw;

    iget v1, p0, Lkuj;->Z:I

    .line 480
    invoke-virtual {v0, v1}, Lkuw;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkux;

    invoke-virtual {v0}, Lkux;->a()I

    move-result v4

    .line 482
    iget-object v0, p0, Lkuj;->X:Lkvc;

    invoke-virtual {v0}, Lkvc;->c()I

    move-result v0

    .line 483
    rsub-int v6, v0, 0x1f4

    .line 485
    new-instance v0, Lkvb;

    invoke-virtual {p0}, Lkuj;->n()Lz;

    move-result-object v1

    iget-object v2, p0, Lkuj;->S:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    iget-object v3, p0, Lkuj;->T:Ljava/lang/String;

    move-object v5, p1

    invoke-direct/range {v0 .. v6}, Lkvb;-><init>(Landroid/content/Context;ILjava/lang/String;ILjava/lang/String;I)V

    .line 487
    const-string v1, "fetch_older"

    invoke-virtual {v0, v1}, Lhny;->b(Ljava/lang/String;)Lhny;

    .line 488
    iget-object v1, p0, Lkuj;->au:Llnh;

    const-class v2, Lhoc;

    invoke-virtual {v1, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhoc;

    invoke-virtual {v1, v0}, Lhoc;->b(Lhny;)V

    .line 490
    const-string v0, "HostedSquareMemberList"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 491
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x28

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "onLoadMoreMembers maxToFetch="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 493
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 281
    const-string v0, "fetch_newer"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "fetch_older"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 282
    :cond_0
    const-string v0, "HostedSquareMemberList"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 283
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1d

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "onReadSquareMembersComplete: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 286
    :cond_1
    invoke-static {p2}, Lhoz;->a(Lhoz;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 287
    iget-boolean v0, p0, Lkuj;->ab:Z

    if-nez v0, :cond_2

    .line 289
    invoke-virtual {p0}, Lkuj;->n()Lz;

    move-result-object v0

    const v1, 0x7f0a058f

    invoke-virtual {p0, v1}, Lkuj;->e_(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 290
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 298
    :cond_2
    :goto_0
    iget-object v0, p0, Lkuj;->O:Lhje;

    invoke-virtual {v0}, Lhje;->a()V

    .line 299
    invoke-virtual {p3, v3}, Lhos;->a(Z)V

    .line 301
    :cond_3
    return-void

    .line 292
    :cond_4
    iget-boolean v0, p0, Lkuj;->ab:Z

    if-eqz v0, :cond_2

    .line 294
    invoke-virtual {p0}, Lkuj;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v3, v1, p0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    goto :goto_0
.end method

.method public a(Loo;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 229
    invoke-static {p1, v4}, Lley;->a(Loo;Z)V

    .line 231
    iget-object v0, p0, Lkuj;->at:Llnl;

    const v1, 0x7f040031

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 232
    const v0, 0x7f100176

    .line 233
    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/actionbar/ActionBarSpinner;

    .line 234
    iget-object v2, p0, Lkuj;->Y:Lkuw;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/actionbar/ActionBarSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 235
    iget-object v2, p0, Lkuj;->Y:Lkuw;

    invoke-virtual {v2}, Lkuw;->a()V

    .line 236
    iget-object v2, p0, Lkuj;->Y:Lkuw;

    iget v3, p0, Lkuj;->aa:I

    invoke-virtual {v2, v3}, Lkuw;->a(I)I

    move-result v2

    iput v2, p0, Lkuj;->Z:I

    .line 237
    iget v2, p0, Lkuj;->Z:I

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/actionbar/ActionBarSpinner;->setSelection(I)V

    .line 238
    invoke-direct {p0}, Lkuj;->U()V

    .line 239
    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/actionbar/ActionBarSpinner;->a(Lhjn;)V

    .line 240
    iput-object v0, p0, Lkuj;->ad:Lcom/google/android/libraries/social/actionbar/ActionBarSpinner;

    .line 242
    invoke-virtual {p1, v1}, Loo;->a(Landroid/view/View;)V

    .line 243
    invoke-virtual {p1, v5}, Loo;->e(Z)V

    .line 244
    invoke-static {p1, v5}, Lley;->a(Loo;Z)V

    .line 245
    invoke-virtual {p1, v4}, Loo;->d(Z)V

    .line 246
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 266
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 267
    const v2, 0x7f10067b

    if-ne v1, v2, :cond_0

    .line 268
    invoke-direct {p0}, Lkuj;->W()V

    .line 274
    :goto_0
    return v0

    .line 270
    :cond_0
    const v2, 0x7f100680

    if-ne v1, v2, :cond_1

    .line 271
    invoke-virtual {p0}, Lkuj;->e()V

    goto :goto_0

    .line 274
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aO_()V
    .locals 1

    .prologue
    .line 223
    invoke-super {p0}, Llol;->aO_()V

    .line 224
    invoke-virtual {p0}, Lkuj;->x()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lkuj;->c(Landroid/view/View;)V

    .line 225
    return-void
.end method

.method public ac_()Lhmk;
    .locals 3

    .prologue
    .line 517
    new-instance v0, Lkqw;

    sget-object v1, Lomv;->w:Lhmn;

    iget-object v2, p0, Lkuj;->T:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lkqw;-><init>(Lhmn;Ljava/lang/String;)V

    return-object v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 507
    iget v0, p0, Lkuj;->V:I

    return v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 512
    const-string v0, "extra_square_id"

    iget-object v1, p0, Lkuj;->T:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 513
    return-void
.end method

.method public b(Loo;)V
    .locals 1

    .prologue
    .line 250
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Loo;->a(Landroid/view/View;)V

    .line 251
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Loo;->e(Z)V

    .line 252
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Loo;->d(Z)V

    .line 253
    return-void
.end method

.method public c()I
    .locals 1

    .prologue
    .line 502
    iget v0, p0, Lkuj;->U:I

    return v0
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 154
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 155
    iget-object v0, p0, Lkuj;->au:Llnh;

    const-class v1, Lhmq;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 156
    iget-object v0, p0, Lkuj;->au:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Lkuj;->S:Lhee;

    .line 158
    iget-object v0, p0, Lkuj;->au:Llnh;

    const-class v1, Lhoc;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    invoke-virtual {v0, p0}, Lhoc;->a(Lhob;)Lhoc;

    .line 159
    iget-object v0, p0, Lkuj;->au:Llnh;

    const-class v1, Lhmm;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 160
    iget-object v0, p0, Lkuj;->au:Llnh;

    const-class v1, Lkuz;

    iget-object v2, p0, Lkuj;->P:Lkuu;

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 161
    iget-object v0, p0, Lkuj;->au:Llnh;

    const-class v1, Lkve;

    iget-object v2, p0, Lkuj;->Q:Lkuv;

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 162
    iget-object v0, p0, Lkuj;->au:Llnh;

    const-class v1, Lktc;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 163
    iget-object v0, p0, Lkuj;->au:Llnh;

    const-class v1, Lktb;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 164
    iget-object v0, p0, Lkuj;->au:Llnh;

    const-class v1, Lkta;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 165
    return-void
.end method

.method public c(I)Z
    .locals 5

    .prologue
    .line 313
    iget v0, p0, Lkuj;->Z:I

    if-ne v0, p1, :cond_0

    .line 314
    const/4 v0, 0x0

    .line 325
    :goto_0
    return v0

    .line 317
    :cond_0
    iget-object v0, p0, Lkuj;->at:Llnl;

    const/4 v1, 0x4

    new-instance v2, Lhml;

    invoke-direct {v2}, Lhml;-><init>()V

    new-instance v3, Lhmk;

    sget-object v4, Lomv;->U:Lhmn;

    invoke-direct {v3, v4}, Lhmk;-><init>(Lhmn;)V

    .line 318
    invoke-virtual {v2, v3}, Lhml;->a(Lhmk;)Lhml;

    move-result-object v2

    iget-object v3, p0, Lkuj;->at:Llnl;

    .line 319
    invoke-virtual {v2, v3}, Lhml;->a(Landroid/content/Context;)Lhml;

    move-result-object v2

    .line 317
    invoke-static {v0, v1, v2}, Lhly;->a(Landroid/content/Context;ILhml;)V

    .line 321
    iput p1, p0, Lkuj;->Z:I

    .line 322
    iget-object v0, p0, Lkuj;->Y:Lkuw;

    iget v1, p0, Lkuj;->Z:I

    .line 323
    invoke-virtual {v0, v1}, Lkuw;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkux;

    invoke-virtual {v0}, Lkux;->a()I

    move-result v0

    iput v0, p0, Lkuj;->aa:I

    .line 324
    invoke-direct {p0}, Lkuj;->U()V

    .line 325
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 497
    iget-object v0, p0, Lkuj;->T:Ljava/lang/String;

    return-object v0
.end method

.method public e()V
    .locals 5

    .prologue
    .line 307
    invoke-virtual {p0}, Lkuj;->n()Lz;

    move-result-object v0

    const-class v1, Lkvh;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkvh;

    iget-object v1, p0, Lkuj;->S:Lhee;

    .line 308
    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    iget-object v2, p0, Lkuj;->T:Ljava/lang/String;

    iget v3, p0, Lkuj;->U:I

    iget v4, p0, Lkuj;->V:I

    .line 307
    invoke-interface {v0, v1, v2, v3, v4}, Lkvh;->a(ILjava/lang/String;II)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkuj;->a(Landroid/content/Intent;)V

    .line 309
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 202
    invoke-super {p0, p1}, Llol;->e(Landroid/os/Bundle;)V

    .line 203
    const-string v0, "membership_status"

    iget v1, p0, Lkuj;->U:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 204
    const-string v0, "joinability"

    iget v1, p0, Lkuj;->V:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 205
    const-string v0, "current_member_list"

    iget v1, p0, Lkuj;->aa:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 206
    return-void
.end method
