.class public final Lpkp;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lpul;

.field private b:[Lpkk;

.field private c:[Lppf;

.field private d:Lppf;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 95
    invoke-direct {p0}, Loxq;-><init>()V

    .line 98
    iput-object v1, p0, Lpkp;->a:Lpul;

    .line 101
    sget-object v0, Lpkk;->a:[Lpkk;

    iput-object v0, p0, Lpkp;->b:[Lpkk;

    .line 104
    sget-object v0, Lppf;->a:[Lppf;

    iput-object v0, p0, Lpkp;->c:[Lppf;

    .line 107
    iput-object v1, p0, Lpkp;->d:Lppf;

    .line 95
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 138
    .line 139
    iget-object v0, p0, Lpkp;->a:Lpul;

    if-eqz v0, :cond_5

    .line 140
    const/4 v0, 0x1

    iget-object v2, p0, Lpkp;->a:Lpul;

    .line 141
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 143
    :goto_0
    iget-object v2, p0, Lpkp;->b:[Lpkk;

    if-eqz v2, :cond_1

    .line 144
    iget-object v3, p0, Lpkp;->b:[Lpkk;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 145
    if-eqz v5, :cond_0

    .line 146
    const/4 v6, 0x2

    .line 147
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 144
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 151
    :cond_1
    iget-object v2, p0, Lpkp;->c:[Lppf;

    if-eqz v2, :cond_3

    .line 152
    iget-object v2, p0, Lpkp;->c:[Lppf;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 153
    if-eqz v4, :cond_2

    .line 154
    const/4 v5, 0x3

    .line 155
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 152
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 159
    :cond_3
    iget-object v1, p0, Lpkp;->d:Lppf;

    if-eqz v1, :cond_4

    .line 160
    const/4 v1, 0x4

    iget-object v2, p0, Lpkp;->d:Lppf;

    .line 161
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 163
    :cond_4
    iget-object v1, p0, Lpkp;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 164
    iput v0, p0, Lpkp;->ai:I

    .line 165
    return v0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lpkp;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 173
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 174
    sparse-switch v0, :sswitch_data_0

    .line 178
    iget-object v2, p0, Lpkp;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 179
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lpkp;->ah:Ljava/util/List;

    .line 182
    :cond_1
    iget-object v2, p0, Lpkp;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 184
    :sswitch_0
    return-object p0

    .line 189
    :sswitch_1
    iget-object v0, p0, Lpkp;->a:Lpul;

    if-nez v0, :cond_2

    .line 190
    new-instance v0, Lpul;

    invoke-direct {v0}, Lpul;-><init>()V

    iput-object v0, p0, Lpkp;->a:Lpul;

    .line 192
    :cond_2
    iget-object v0, p0, Lpkp;->a:Lpul;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 196
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 197
    iget-object v0, p0, Lpkp;->b:[Lpkk;

    if-nez v0, :cond_4

    move v0, v1

    .line 198
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lpkk;

    .line 199
    iget-object v3, p0, Lpkp;->b:[Lpkk;

    if-eqz v3, :cond_3

    .line 200
    iget-object v3, p0, Lpkp;->b:[Lpkk;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 202
    :cond_3
    iput-object v2, p0, Lpkp;->b:[Lpkk;

    .line 203
    :goto_2
    iget-object v2, p0, Lpkp;->b:[Lpkk;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 204
    iget-object v2, p0, Lpkp;->b:[Lpkk;

    new-instance v3, Lpkk;

    invoke-direct {v3}, Lpkk;-><init>()V

    aput-object v3, v2, v0

    .line 205
    iget-object v2, p0, Lpkp;->b:[Lpkk;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 206
    invoke-virtual {p1}, Loxn;->a()I

    .line 203
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 197
    :cond_4
    iget-object v0, p0, Lpkp;->b:[Lpkk;

    array-length v0, v0

    goto :goto_1

    .line 209
    :cond_5
    iget-object v2, p0, Lpkp;->b:[Lpkk;

    new-instance v3, Lpkk;

    invoke-direct {v3}, Lpkk;-><init>()V

    aput-object v3, v2, v0

    .line 210
    iget-object v2, p0, Lpkp;->b:[Lpkk;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 214
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 215
    iget-object v0, p0, Lpkp;->c:[Lppf;

    if-nez v0, :cond_7

    move v0, v1

    .line 216
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lppf;

    .line 217
    iget-object v3, p0, Lpkp;->c:[Lppf;

    if-eqz v3, :cond_6

    .line 218
    iget-object v3, p0, Lpkp;->c:[Lppf;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 220
    :cond_6
    iput-object v2, p0, Lpkp;->c:[Lppf;

    .line 221
    :goto_4
    iget-object v2, p0, Lpkp;->c:[Lppf;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_8

    .line 222
    iget-object v2, p0, Lpkp;->c:[Lppf;

    new-instance v3, Lppf;

    invoke-direct {v3}, Lppf;-><init>()V

    aput-object v3, v2, v0

    .line 223
    iget-object v2, p0, Lpkp;->c:[Lppf;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 224
    invoke-virtual {p1}, Loxn;->a()I

    .line 221
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 215
    :cond_7
    iget-object v0, p0, Lpkp;->c:[Lppf;

    array-length v0, v0

    goto :goto_3

    .line 227
    :cond_8
    iget-object v2, p0, Lpkp;->c:[Lppf;

    new-instance v3, Lppf;

    invoke-direct {v3}, Lppf;-><init>()V

    aput-object v3, v2, v0

    .line 228
    iget-object v2, p0, Lpkp;->c:[Lppf;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 232
    :sswitch_4
    iget-object v0, p0, Lpkp;->d:Lppf;

    if-nez v0, :cond_9

    .line 233
    new-instance v0, Lppf;

    invoke-direct {v0}, Lppf;-><init>()V

    iput-object v0, p0, Lpkp;->d:Lppf;

    .line 235
    :cond_9
    iget-object v0, p0, Lpkp;->d:Lppf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 174
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 112
    iget-object v1, p0, Lpkp;->a:Lpul;

    if-eqz v1, :cond_0

    .line 113
    const/4 v1, 0x1

    iget-object v2, p0, Lpkp;->a:Lpul;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 115
    :cond_0
    iget-object v1, p0, Lpkp;->b:[Lpkk;

    if-eqz v1, :cond_2

    .line 116
    iget-object v2, p0, Lpkp;->b:[Lpkk;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 117
    if-eqz v4, :cond_1

    .line 118
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 116
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 122
    :cond_2
    iget-object v1, p0, Lpkp;->c:[Lppf;

    if-eqz v1, :cond_4

    .line 123
    iget-object v1, p0, Lpkp;->c:[Lppf;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 124
    if-eqz v3, :cond_3

    .line 125
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 123
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 129
    :cond_4
    iget-object v0, p0, Lpkp;->d:Lppf;

    if-eqz v0, :cond_5

    .line 130
    const/4 v0, 0x4

    iget-object v1, p0, Lpkp;->d:Lppf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 132
    :cond_5
    iget-object v0, p0, Lpkp;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 134
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 91
    invoke-virtual {p0, p1}, Lpkp;->a(Loxn;)Lpkp;

    move-result-object v0

    return-object v0
.end method
