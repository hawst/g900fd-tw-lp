.class public final Leri;
.super Lhye;
.source "PG"


# instance fields
.field private final b:I

.field private final c:Z

.field private final d:I

.field private final e:I


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/4 v7, 0x0

    .line 53
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->b:Landroid/net/Uri;

    invoke-static {v0, p2}, Lcom/google/android/apps/plus/content/EsProvider;->a(Landroid/net/Uri;I)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Ldsh;->a:[Ljava/lang/String;

    const-string v6, "display_index ASC"

    move-object v0, p0

    move-object v1, p1

    move-object v5, v4

    invoke-direct/range {v0 .. v6}, Lhye;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    iput-boolean v7, p0, Leri;->c:Z

    .line 57
    iput v7, p0, Leri;->d:I

    .line 58
    iput v7, p0, Leri;->e:I

    .line 59
    iput p2, p0, Leri;->b:I

    .line 60
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;III)V
    .locals 7

    .prologue
    .line 43
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->b:Landroid/net/Uri;

    invoke-static {v0, p2}, Lcom/google/android/apps/plus/content/EsProvider;->a(Landroid/net/Uri;I)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Ldsh;->a:[Ljava/lang/String;

    .line 44
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v0, 0x40

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "("

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    packed-switch p3, :pswitch_data_0

    const-string v0, "read_state=0 OR pending_read!=0"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ")"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " AND "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    packed-switch p4, :pswitch_data_1

    const-string v0, "priority=3"

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const-string v6, "updated_version DESC"

    move-object v0, p0

    move-object v1, p1

    .line 43
    invoke-direct/range {v0 .. v6}, Lhye;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    const/4 v0, 0x1

    iput-boolean v0, p0, Leri;->c:Z

    .line 47
    iput p3, p0, Leri;->d:I

    .line 48
    iput p4, p0, Leri;->e:I

    .line 49
    iput p2, p0, Leri;->b:I

    .line 50
    return-void

    .line 44
    :pswitch_0
    const-string v0, "read_state=1 AND pending_read=0"

    goto :goto_0

    :pswitch_1
    const-string v0, "priority!=3"

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public C()Landroid/database/Cursor;
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x1

    const/4 v6, 0x2

    .line 106
    invoke-super {p0}, Lhye;->C()Landroid/database/Cursor;

    move-result-object v1

    .line 107
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 108
    invoke-virtual {p0}, Leri;->n()Landroid/content/Context;

    move-result-object v0

    .line 110
    iget v3, p0, Leri;->b:I

    .line 112
    const-string v4, "next_read_high_fetch_param"

    .line 113
    invoke-static {v0, v3, v6, v8}, Ldsf;->b(Landroid/content/Context;III)[B

    move-result-object v5

    .line 112
    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 116
    const-string v4, "next_read_low_fetch_param"

    .line 117
    invoke-static {v0, v3, v6, v6}, Ldsf;->b(Landroid/content/Context;III)[B

    move-result-object v5

    .line 116
    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 120
    const-string v4, "next_unread_high_fetch_param"

    .line 121
    invoke-static {v0, v3, v7, v8}, Ldsf;->b(Landroid/content/Context;III)[B

    move-result-object v5

    .line 120
    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 124
    const-string v4, "next_unread_low_fetch_param"

    .line 125
    invoke-static {v0, v3, v7, v6}, Ldsf;->b(Landroid/content/Context;III)[B

    move-result-object v5

    .line 124
    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 129
    const-string v4, "read_low_summary"

    .line 130
    invoke-static {v0, v3, v7}, Ldsf;->c(Landroid/content/Context;IZ)[B

    move-result-object v5

    .line 129
    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 131
    const-string v4, "unread_low_summary"

    const/4 v5, 0x0

    .line 132
    invoke-static {v0, v3, v5}, Ldsf;->c(Landroid/content/Context;IZ)[B

    move-result-object v0

    .line 131
    invoke-virtual {v2, v4, v0}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 134
    const/4 v0, 0x0

    .line 135
    if-eqz v1, :cond_0

    .line 136
    iget-boolean v0, p0, Leri;->c:Z

    if-eqz v0, :cond_1

    .line 137
    new-instance v0, Lexk;

    iget v3, p0, Leri;->d:I

    iget v4, p0, Leri;->e:I

    invoke-direct {v0, v1, v3, v4}, Lexk;-><init>(Landroid/database/Cursor;II)V

    .line 141
    :goto_0
    invoke-virtual {v0, v2}, Lexk;->a(Landroid/os/Bundle;)V

    .line 144
    :cond_0
    return-object v0

    .line 139
    :cond_1
    new-instance v0, Lexk;

    invoke-direct {v0, v1}, Lexk;-><init>(Landroid/database/Cursor;)V

    goto :goto_0
.end method
