.class public final Lmhs;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmzl;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 22597
    invoke-direct {p0}, Loxq;-><init>()V

    .line 22600
    iput-object v0, p0, Lmhs;->apiHeader:Llyq;

    .line 22603
    iput-object v0, p0, Lmhs;->a:Lmzl;

    .line 22597
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 22620
    const/4 v0, 0x0

    .line 22621
    iget-object v1, p0, Lmhs;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 22622
    const/4 v0, 0x1

    iget-object v1, p0, Lmhs;->apiHeader:Llyq;

    .line 22623
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 22625
    :cond_0
    iget-object v1, p0, Lmhs;->a:Lmzl;

    if-eqz v1, :cond_1

    .line 22626
    const/4 v1, 0x2

    iget-object v2, p0, Lmhs;->a:Lmzl;

    .line 22627
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 22629
    :cond_1
    iget-object v1, p0, Lmhs;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 22630
    iput v0, p0, Lmhs;->ai:I

    .line 22631
    return v0
.end method

.method public a(Loxn;)Lmhs;
    .locals 2

    .prologue
    .line 22639
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 22640
    sparse-switch v0, :sswitch_data_0

    .line 22644
    iget-object v1, p0, Lmhs;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 22645
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmhs;->ah:Ljava/util/List;

    .line 22648
    :cond_1
    iget-object v1, p0, Lmhs;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 22650
    :sswitch_0
    return-object p0

    .line 22655
    :sswitch_1
    iget-object v0, p0, Lmhs;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 22656
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmhs;->apiHeader:Llyq;

    .line 22658
    :cond_2
    iget-object v0, p0, Lmhs;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 22662
    :sswitch_2
    iget-object v0, p0, Lmhs;->a:Lmzl;

    if-nez v0, :cond_3

    .line 22663
    new-instance v0, Lmzl;

    invoke-direct {v0}, Lmzl;-><init>()V

    iput-object v0, p0, Lmhs;->a:Lmzl;

    .line 22665
    :cond_3
    iget-object v0, p0, Lmhs;->a:Lmzl;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 22640
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 22608
    iget-object v0, p0, Lmhs;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 22609
    const/4 v0, 0x1

    iget-object v1, p0, Lmhs;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 22611
    :cond_0
    iget-object v0, p0, Lmhs;->a:Lmzl;

    if-eqz v0, :cond_1

    .line 22612
    const/4 v0, 0x2

    iget-object v1, p0, Lmhs;->a:Lmzl;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 22614
    :cond_1
    iget-object v0, p0, Lmhs;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 22616
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 22593
    invoke-virtual {p0, p1}, Lmhs;->a(Loxn;)Lmhs;

    move-result-object v0

    return-object v0
.end method
