.class public final Lplv;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lpgx;

.field private b:Ljava/lang/Long;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:[Lppr;

.field private f:Lpvy;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 494
    invoke-direct {p0}, Loxq;-><init>()V

    .line 497
    iput-object v1, p0, Lplv;->a:Lpgx;

    .line 506
    sget-object v0, Lppr;->a:[Lppr;

    iput-object v0, p0, Lplv;->e:[Lppr;

    .line 509
    iput-object v1, p0, Lplv;->f:Lpvy;

    .line 494
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 542
    .line 543
    iget-object v0, p0, Lplv;->a:Lpgx;

    if-eqz v0, :cond_6

    .line 544
    const/4 v0, 0x1

    iget-object v2, p0, Lplv;->a:Lpgx;

    .line 545
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 547
    :goto_0
    iget-object v2, p0, Lplv;->b:Ljava/lang/Long;

    if-eqz v2, :cond_0

    .line 548
    const/4 v2, 0x2

    iget-object v3, p0, Lplv;->b:Ljava/lang/Long;

    .line 549
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Loxo;->f(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 551
    :cond_0
    iget-object v2, p0, Lplv;->c:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 552
    const/4 v2, 0x3

    iget-object v3, p0, Lplv;->c:Ljava/lang/String;

    .line 553
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 555
    :cond_1
    iget-object v2, p0, Lplv;->d:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 556
    const/4 v2, 0x4

    iget-object v3, p0, Lplv;->d:Ljava/lang/String;

    .line 557
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 559
    :cond_2
    iget-object v2, p0, Lplv;->e:[Lppr;

    if-eqz v2, :cond_4

    .line 560
    iget-object v2, p0, Lplv;->e:[Lppr;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 561
    if-eqz v4, :cond_3

    .line 562
    const/4 v5, 0x5

    .line 563
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 560
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 567
    :cond_4
    iget-object v1, p0, Lplv;->f:Lpvy;

    if-eqz v1, :cond_5

    .line 568
    const/4 v1, 0x6

    iget-object v2, p0, Lplv;->f:Lpvy;

    .line 569
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 571
    :cond_5
    iget-object v1, p0, Lplv;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 572
    iput v0, p0, Lplv;->ai:I

    .line 573
    return v0

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lplv;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 581
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 582
    sparse-switch v0, :sswitch_data_0

    .line 586
    iget-object v2, p0, Lplv;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 587
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lplv;->ah:Ljava/util/List;

    .line 590
    :cond_1
    iget-object v2, p0, Lplv;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 592
    :sswitch_0
    return-object p0

    .line 597
    :sswitch_1
    iget-object v0, p0, Lplv;->a:Lpgx;

    if-nez v0, :cond_2

    .line 598
    new-instance v0, Lpgx;

    invoke-direct {v0}, Lpgx;-><init>()V

    iput-object v0, p0, Lplv;->a:Lpgx;

    .line 600
    :cond_2
    iget-object v0, p0, Lplv;->a:Lpgx;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 604
    :sswitch_2
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lplv;->b:Ljava/lang/Long;

    goto :goto_0

    .line 608
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lplv;->c:Ljava/lang/String;

    goto :goto_0

    .line 612
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lplv;->d:Ljava/lang/String;

    goto :goto_0

    .line 616
    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 617
    iget-object v0, p0, Lplv;->e:[Lppr;

    if-nez v0, :cond_4

    move v0, v1

    .line 618
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lppr;

    .line 619
    iget-object v3, p0, Lplv;->e:[Lppr;

    if-eqz v3, :cond_3

    .line 620
    iget-object v3, p0, Lplv;->e:[Lppr;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 622
    :cond_3
    iput-object v2, p0, Lplv;->e:[Lppr;

    .line 623
    :goto_2
    iget-object v2, p0, Lplv;->e:[Lppr;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 624
    iget-object v2, p0, Lplv;->e:[Lppr;

    new-instance v3, Lppr;

    invoke-direct {v3}, Lppr;-><init>()V

    aput-object v3, v2, v0

    .line 625
    iget-object v2, p0, Lplv;->e:[Lppr;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 626
    invoke-virtual {p1}, Loxn;->a()I

    .line 623
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 617
    :cond_4
    iget-object v0, p0, Lplv;->e:[Lppr;

    array-length v0, v0

    goto :goto_1

    .line 629
    :cond_5
    iget-object v2, p0, Lplv;->e:[Lppr;

    new-instance v3, Lppr;

    invoke-direct {v3}, Lppr;-><init>()V

    aput-object v3, v2, v0

    .line 630
    iget-object v2, p0, Lplv;->e:[Lppr;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 634
    :sswitch_6
    iget-object v0, p0, Lplv;->f:Lpvy;

    if-nez v0, :cond_6

    .line 635
    new-instance v0, Lpvy;

    invoke-direct {v0}, Lpvy;-><init>()V

    iput-object v0, p0, Lplv;->f:Lpvy;

    .line 637
    :cond_6
    iget-object v0, p0, Lplv;->f:Lpvy;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 582
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 514
    iget-object v0, p0, Lplv;->a:Lpgx;

    if-eqz v0, :cond_0

    .line 515
    const/4 v0, 0x1

    iget-object v1, p0, Lplv;->a:Lpgx;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 517
    :cond_0
    iget-object v0, p0, Lplv;->b:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 518
    const/4 v0, 0x2

    iget-object v1, p0, Lplv;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 520
    :cond_1
    iget-object v0, p0, Lplv;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 521
    const/4 v0, 0x3

    iget-object v1, p0, Lplv;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 523
    :cond_2
    iget-object v0, p0, Lplv;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 524
    const/4 v0, 0x4

    iget-object v1, p0, Lplv;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 526
    :cond_3
    iget-object v0, p0, Lplv;->e:[Lppr;

    if-eqz v0, :cond_5

    .line 527
    iget-object v1, p0, Lplv;->e:[Lppr;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    .line 528
    if-eqz v3, :cond_4

    .line 529
    const/4 v4, 0x5

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 527
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 533
    :cond_5
    iget-object v0, p0, Lplv;->f:Lpvy;

    if-eqz v0, :cond_6

    .line 534
    const/4 v0, 0x6

    iget-object v1, p0, Lplv;->f:Lpvy;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 536
    :cond_6
    iget-object v0, p0, Lplv;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 538
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 490
    invoke-virtual {p0, p1}, Lplv;->a(Loxn;)Lplv;

    move-result-object v0

    return-object v0
.end method
