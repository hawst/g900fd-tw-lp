.class final Lpyr;
.super Landroid/os/HandlerThread;
.source "PG"

# interfaces
.implements Landroid/os/Handler$Callback;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 892
    const-string v0, "GifDecoder"

    invoke-direct {p0, v0}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 893
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 897
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lpyq;

    .line 898
    if-eqz v0, :cond_0

    iget-object v3, v0, Lpyq;->a:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_0

    invoke-static {}, Lpyq;->c()Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    move v0, v1

    .line 940
    :goto_0
    return v0

    .line 902
    :cond_1
    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    :pswitch_0
    move v0, v2

    .line 940
    goto :goto_0

    .line 908
    :cond_2
    :pswitch_1
    :try_start_0
    invoke-static {v0}, Lpyq;->a(Lpyq;)V
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 914
    :goto_1
    invoke-static {v0}, Lpyq;->b(Lpyq;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 915
    invoke-static {v0}, Lpyq;->c(Lpyq;)I

    move-result v3

    if-nez v3, :cond_5

    .line 917
    invoke-static {v0, v1}, Lpyq;->b(Lpyq;Z)Z

    .line 930
    :cond_3
    :goto_2
    invoke-static {v0}, Lpyq;->b(Lpyq;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-static {v0}, Lpyq;->g(Lpyq;)Z

    move-result v3

    if-nez v3, :cond_4

    invoke-static {v0}, Lpyq;->h(Lpyq;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 931
    :cond_4
    invoke-static {v0}, Lpyq;->j(Lpyq;)Landroid/os/Handler;

    move-result-object v3

    invoke-static {v0}, Lpyq;->j(Lpyq;)Landroid/os/Handler;

    move-result-object v4

    const/16 v5, 0xb

    .line 932
    invoke-static {v0}, Lpyq;->i(Lpyq;)I

    move-result v0

    .line 931
    invoke-virtual {v4, v5, v0, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    move v0, v1

    .line 933
    goto :goto_0

    .line 910
    :catch_0
    move-exception v3

    invoke-static {v0, v1}, Lpyq;->a(Lpyq;Z)Z

    goto :goto_1

    .line 918
    :cond_5
    invoke-static {v0}, Lpyq;->c(Lpyq;)I

    move-result v3

    if-le v3, v1, :cond_8

    .line 919
    invoke-static {v0}, Lpyq;->d(Lpyq;)I

    move-result v3

    if-eqz v3, :cond_6

    invoke-static {v0}, Lpyq;->e(Lpyq;)I

    move-result v3

    invoke-static {v0}, Lpyq;->d(Lpyq;)I

    move-result v4

    if-ge v3, v4, :cond_7

    .line 921
    :cond_6
    invoke-static {v0}, Lpyq;->f(Lpyq;)V

    goto :goto_2

    .line 923
    :cond_7
    invoke-static {v0, v1}, Lpyq;->c(Lpyq;Z)Z

    goto :goto_2

    .line 927
    :cond_8
    invoke-static {v0, v1}, Lpyq;->c(Lpyq;Z)Z

    goto :goto_2

    .line 936
    :pswitch_2
    invoke-static {v0}, Lpyq;->f(Lpyq;)V

    move v0, v1

    .line 937
    goto :goto_0

    .line 902
    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
