.class public final Lkva;
.super Lkgg;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkgg",
        "<",
        "Lmia;",
        "Lmib;",
        ">;"
    }
.end annotation


# static fields
.field private static final q:[I


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:I

.field private final c:Ljava/lang/String;

.field private final p:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lkva;->q:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x2
        0x3
        0x4
        0x6
        0x5
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Lkfo;Ljava/lang/String;ILjava/lang/String;I)V
    .locals 6

    .prologue
    .line 45
    const-string v3, "readsquaremembers"

    new-instance v4, Lmia;

    invoke-direct {v4}, Lmia;-><init>()V

    new-instance v5, Lmib;

    invoke-direct {v5}, Lmib;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lkgg;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Loxu;Loxu;)V

    .line 48
    iput-object p3, p0, Lkva;->a:Ljava/lang/String;

    .line 49
    iput p4, p0, Lkva;->b:I

    .line 50
    iput-object p5, p0, Lkva;->c:Ljava/lang/String;

    .line 51
    const/16 v0, 0x64

    invoke-static {p6, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lkva;->p:I

    .line 52
    return-void
.end method

.method private a(ILjava/lang/String;)Lnrz;
    .locals 2

    .prologue
    .line 99
    new-instance v0, Lnrz;

    invoke-direct {v0}, Lnrz;-><init>()V

    .line 100
    iput p1, v0, Lnrz;->b:I

    .line 101
    iput-object p2, v0, Lnrz;->d:Ljava/lang/String;

    .line 102
    iget v1, p0, Lkva;->p:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lnrz;->c:Ljava/lang/Integer;

    .line 103
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lnrz;->f:Ljava/lang/Boolean;

    .line 104
    return-object v0
.end method


# virtual methods
.method protected a(Lmia;)V
    .locals 6

    .prologue
    const/4 v5, 0x5

    .line 56
    new-instance v0, Lnth;

    invoke-direct {v0}, Lnth;-><init>()V

    iput-object v0, p1, Lmia;->a:Lnth;

    .line 57
    iget-object v0, p1, Lmia;->a:Lnth;

    iget-object v1, p0, Lkva;->a:Ljava/lang/String;

    iput-object v1, v0, Lnth;->a:Ljava/lang/String;

    .line 58
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 60
    iget v0, p0, Lkva;->b:I

    packed-switch v0, :pswitch_data_0

    .line 88
    sget-object v2, Lkva;->q:[I

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v5, :cond_0

    aget v3, v2, v0

    .line 89
    const/4 v4, 0x0

    invoke-direct {p0, v3, v4}, Lkva;->a(ILjava/lang/String;)Lnrz;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 88
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 62
    :pswitch_0
    const/4 v0, 0x6

    iget-object v2, p0, Lkva;->c:Ljava/lang/String;

    invoke-direct {p0, v0, v2}, Lkva;->a(ILjava/lang/String;)Lnrz;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 94
    :cond_0
    :goto_1
    iget-object v2, p1, Lmia;->a:Lnth;

    .line 95
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lnrz;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lnrz;

    iput-object v0, v2, Lnth;->b:[Lnrz;

    .line 96
    return-void

    .line 67
    :pswitch_1
    iget-object v0, p0, Lkva;->c:Ljava/lang/String;

    invoke-direct {p0, v5, v0}, Lkva;->a(ILjava/lang/String;)Lnrz;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 72
    :pswitch_2
    const/4 v0, 0x4

    iget-object v2, p0, Lkva;->c:Ljava/lang/String;

    invoke-direct {p0, v0, v2}, Lkva;->a(ILjava/lang/String;)Lnrz;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 77
    :pswitch_3
    const/4 v0, 0x2

    iget-object v2, p0, Lkva;->c:Ljava/lang/String;

    invoke-direct {p0, v0, v2}, Lkva;->a(ILjava/lang/String;)Lnrz;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 82
    :pswitch_4
    const/4 v0, 0x3

    iget-object v2, p0, Lkva;->c:Ljava/lang/String;

    invoke-direct {p0, v0, v2}, Lkva;->a(ILjava/lang/String;)Lnrz;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 60
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 19
    check-cast p1, Lmia;

    invoke-virtual {p0, p1}, Lkva;->a(Lmia;)V

    return-void
.end method
