.class public final Lmeq;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lojo;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 11581
    invoke-direct {p0}, Loxq;-><init>()V

    .line 11584
    iput-object v0, p0, Lmeq;->apiHeader:Llyq;

    .line 11587
    iput-object v0, p0, Lmeq;->a:Lojo;

    .line 11581
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 11604
    const/4 v0, 0x0

    .line 11605
    iget-object v1, p0, Lmeq;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 11606
    const/4 v0, 0x1

    iget-object v1, p0, Lmeq;->apiHeader:Llyq;

    .line 11607
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 11609
    :cond_0
    iget-object v1, p0, Lmeq;->a:Lojo;

    if-eqz v1, :cond_1

    .line 11610
    const/4 v1, 0x2

    iget-object v2, p0, Lmeq;->a:Lojo;

    .line 11611
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11613
    :cond_1
    iget-object v1, p0, Lmeq;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11614
    iput v0, p0, Lmeq;->ai:I

    .line 11615
    return v0
.end method

.method public a(Loxn;)Lmeq;
    .locals 2

    .prologue
    .line 11623
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 11624
    sparse-switch v0, :sswitch_data_0

    .line 11628
    iget-object v1, p0, Lmeq;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 11629
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmeq;->ah:Ljava/util/List;

    .line 11632
    :cond_1
    iget-object v1, p0, Lmeq;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 11634
    :sswitch_0
    return-object p0

    .line 11639
    :sswitch_1
    iget-object v0, p0, Lmeq;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 11640
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmeq;->apiHeader:Llyq;

    .line 11642
    :cond_2
    iget-object v0, p0, Lmeq;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 11646
    :sswitch_2
    iget-object v0, p0, Lmeq;->a:Lojo;

    if-nez v0, :cond_3

    .line 11647
    new-instance v0, Lojo;

    invoke-direct {v0}, Lojo;-><init>()V

    iput-object v0, p0, Lmeq;->a:Lojo;

    .line 11649
    :cond_3
    iget-object v0, p0, Lmeq;->a:Lojo;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 11624
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 11592
    iget-object v0, p0, Lmeq;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 11593
    const/4 v0, 0x1

    iget-object v1, p0, Lmeq;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 11595
    :cond_0
    iget-object v0, p0, Lmeq;->a:Lojo;

    if-eqz v0, :cond_1

    .line 11596
    const/4 v0, 0x2

    iget-object v1, p0, Lmeq;->a:Lojo;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 11598
    :cond_1
    iget-object v0, p0, Lmeq;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 11600
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 11577
    invoke-virtual {p0, p1}, Lmeq;->a(Loxn;)Lmeq;

    move-result-object v0

    return-object v0
.end method
