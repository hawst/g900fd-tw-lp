.class public Lkff;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lorg/chromium/net/HttpUrlRequestListener;


# static fields
.field private static final a:Ljava/util/concurrent/atomic/AtomicInteger;

.field public static final d:Lloy;

.field static final e:Lloy;


# instance fields
.field private b:I

.field private final c:Ljava/lang/String;

.field public final f:Landroid/content/Context;

.field public final g:Lkfo;

.field h:Lkfg;

.field public i:I

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/Exception;

.field l:I

.field public m:Ljava/nio/channels/WritableByteChannel;

.field public n:I

.field final o:Lkfk;

.field private final p:Landroid/os/ConditionVariable;

.field private final q:Ljava/lang/String;

.field private final r:Lkfj;

.field private final s:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkfe;",
            ">;"
        }
    .end annotation
.end field

.field private final t:Ljava/lang/String;

.field private u:Ljava/lang/String;

.field private v:[B

.field private w:Lorg/chromium/net/HttpUrlRequest;

.field private x:Z

.field private y:Lkgj;

.field private z:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 39
    new-instance v0, Lloy;

    const-string v1, "debug.rpc.dogfood"

    invoke-direct {v0, v1}, Lloy;-><init>(Ljava/lang/String;)V

    sput-object v0, Lkff;->d:Lloy;

    .line 40
    new-instance v0, Lloy;

    const-string v1, "debug.rpc.metrics"

    invoke-direct {v0, v1}, Lloy;-><init>(Ljava/lang/String;)V

    sput-object v0, Lkff;->e:Lloy;

    .line 48
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lkff;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Lkfj;)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 86
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Lkff;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Lkfj;Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Lkfj;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    sget-object v0, Lkff;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    iput v0, p0, Lkff;->b:I

    .line 55
    new-instance v0, Landroid/os/ConditionVariable;

    invoke-direct {v0}, Landroid/os/ConditionVariable;-><init>()V

    iput-object v0, p0, Lkff;->p:Landroid/os/ConditionVariable;

    .line 64
    const/4 v0, -0x1

    iput v0, p0, Lkff;->i:I

    .line 67
    const/4 v0, 0x3

    iput v0, p0, Lkff;->l:I

    .line 101
    iput-object p1, p0, Lkff;->f:Landroid/content/Context;

    .line 102
    iput-object p2, p0, Lkff;->g:Lkfo;

    .line 103
    iput-object p3, p0, Lkff;->q:Ljava/lang/String;

    .line 104
    iput-object p4, p0, Lkff;->r:Lkfj;

    .line 105
    iput-object p5, p0, Lkff;->c:Ljava/lang/String;

    .line 106
    iput-object p6, p0, Lkff;->t:Ljava/lang/String;

    .line 107
    iget-object v0, p0, Lkff;->f:Landroid/content/Context;

    const-class v1, Lkfe;

    invoke-static {v0, v1}, Llnh;->c(Landroid/content/Context;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkff;->s:Ljava/util/List;

    .line 108
    iget-object v0, p0, Lkff;->f:Landroid/content/Context;

    const-class v1, Lkgj;

    invoke-static {v0, v1}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkgj;

    iput-object v0, p0, Lkff;->y:Lkgj;

    .line 109
    new-instance v0, Lkfk;

    invoke-direct {v0}, Lkfk;-><init>()V

    iput-object v0, p0, Lkff;->o:Lkfk;

    .line 110
    return-void
.end method

.method public static a(Ljava/lang/Throwable;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 584
    :goto_0
    if-nez p0, :cond_1

    .line 599
    :cond_0
    :goto_1
    return v0

    .line 586
    :cond_1
    instance-of v1, p0, Ljava/lang/RuntimeException;

    if-eqz v1, :cond_2

    .line 587
    invoke-virtual {p0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    .line 588
    if-eqz v1, :cond_0

    if-eq v1, p0, :cond_0

    move-object p0, v1

    .line 591
    goto :goto_0

    .line 592
    :cond_2
    instance-of v1, p0, Ljava/io/IOException;

    if-eqz v1, :cond_0

    .line 594
    instance-of v1, p0, Lkfm;

    if-nez v1, :cond_0

    .line 596
    instance-of v1, p0, Lorg/apache/http/client/HttpResponseException;

    if-nez v1, :cond_0

    .line 599
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public static c(Ljava/lang/Exception;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 558
    if-nez p0, :cond_1

    .line 573
    :cond_0
    :goto_0
    return v0

    .line 560
    :cond_1
    instance-of v1, p0, Ljava/net/SocketException;

    if-nez v1, :cond_0

    .line 562
    instance-of v1, p0, Ljava/net/UnknownHostException;

    if-nez v1, :cond_0

    .line 564
    instance-of v1, p0, Ljavax/net/ssl/SSLException;

    if-nez v1, :cond_0

    .line 566
    instance-of v1, p0, Lorg/apache/http/client/HttpResponseException;

    if-eqz v1, :cond_2

    .line 567
    check-cast p0, Lorg/apache/http/client/HttpResponseException;

    invoke-virtual {p0}, Lorg/apache/http/client/HttpResponseException;->getStatusCode()I

    move-result v1

    const/16 v2, 0x191

    if-eq v1, v2, :cond_0

    .line 573
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public A()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 717
    iget-object v0, p0, Lkff;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_1

    .line 718
    iget-object v0, p0, Lkff;->s:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkfe;

    invoke-virtual {p0}, Lkff;->o()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4}, Lkfe;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 719
    const/4 v0, 0x1

    .line 722
    :goto_1
    return v0

    .line 717
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 722
    goto :goto_1
.end method

.method public M_()V
    .locals 0

    .prologue
    .line 150
    return-void
.end method

.method public N_()V
    .locals 0

    .prologue
    .line 159
    return-void
.end method

.method public O_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 300
    iget-object v0, p0, Lkff;->u:Ljava/lang/String;

    return-object v0
.end method

.method public P_()[B
    .locals 1

    .prologue
    .line 261
    const/4 v0, 0x0

    return-object v0
.end method

.method public Q_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Lkff;->c:Ljava/lang/String;

    return-object v0
.end method

.method public R_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 293
    iget-object v0, p0, Lkff;->t:Ljava/lang/String;

    return-object v0
.end method

.method public T_()Ljava/nio/channels/ReadableByteChannel;
    .locals 1

    .prologue
    .line 271
    const/4 v0, 0x0

    return-object v0
.end method

.method public U_()J
    .locals 2

    .prologue
    .line 279
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "You must specify a length when using streaming."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(ILjava/lang/String;Ljava/io/IOException;)V
    .locals 6

    .prologue
    const/16 v0, 0xc8

    .line 508
    if-ne p1, v0, :cond_1

    if-eqz p3, :cond_1

    .line 509
    const/4 p1, 0x0

    move v4, p1

    .line 514
    :goto_0
    iput v4, p0, Lkff;->i:I

    .line 515
    iput-object p2, p0, Lkff;->j:Ljava/lang/String;

    .line 516
    iput-object p3, p0, Lkff;->k:Ljava/lang/Exception;

    .line 518
    iget-object v0, p0, Lkff;->y:Lkgj;

    if-eqz v0, :cond_0

    if-nez v4, :cond_0

    .line 520
    iget-object v0, p0, Lkff;->y:Lkgj;

    iget-object v1, p0, Lkff;->f:Landroid/content/Context;

    iget-object v2, p0, Lkff;->g:Lkfo;

    invoke-virtual {v2}, Lkfo;->a()Ljava/lang/String;

    move-result-object v2

    .line 522
    invoke-virtual {p0}, Lkff;->Q_()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lkff;->o()Ljava/lang/String;

    move-result-object v5

    .line 521
    invoke-interface/range {v0 .. v5}, Lkgj;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Z

    .line 524
    :cond_0
    return-void

    .line 510
    :cond_1
    if-eq p1, v0, :cond_2

    if-eqz p1, :cond_2

    if-nez p3, :cond_2

    .line 511
    new-instance p3, Lorg/apache/http/client/HttpResponseException;

    invoke-direct {p3, p1, p2}, Lorg/apache/http/client/HttpResponseException;-><init>(ILjava/lang/String;)V

    move v4, p1

    goto :goto_0

    :cond_2
    move v4, p1

    goto :goto_0
.end method

.method public a(ILjava/lang/String;Ljava/lang/Exception;)V
    .locals 0

    .prologue
    .line 187
    return-void
.end method

.method public a(Lorg/chromium/net/HttpUrlRequest;)V
    .locals 0

    .prologue
    .line 164
    return-void
.end method

.method public a([BLjava/lang/String;)V
    .locals 1

    .prologue
    .line 170
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lkff;->d([BLjava/lang/String;)V

    .line 171
    return-void
.end method

.method protected a(Ljava/lang/Exception;)Z
    .locals 1

    .prologue
    .line 531
    invoke-virtual {p0, p1}, Lkff;->b(Ljava/lang/Exception;)Z

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 199
    const/4 v0, 0x0

    return v0
.end method

.method public b([BLjava/lang/String;)V
    .locals 1

    .prologue
    .line 177
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lkff;->d([BLjava/lang/String;)V

    .line 178
    return-void
.end method

.method public b(Ljava/lang/Exception;)Z
    .locals 1

    .prologue
    .line 540
    instance-of v0, p1, Lorg/apache/http/client/HttpResponseException;

    if-eqz v0, :cond_0

    .line 541
    check-cast p1, Lorg/apache/http/client/HttpResponseException;

    .line 542
    invoke-virtual {p1}, Lorg/apache/http/client/HttpResponseException;->getStatusCode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 547
    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 544
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 542
    nop

    :pswitch_data_0
    .packed-switch 0x191
        :pswitch_0
    .end packed-switch
.end method

.method public c([BLjava/lang/String;)V
    .locals 5

    .prologue
    .line 680
    const/4 v0, 0x0

    iget-object v1, p0, Lkff;->s:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 682
    :try_start_0
    iget-object v0, p0, Lkff;->s:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkfe;

    .line 683
    invoke-virtual {p0}, Lkff;->o()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Lkfe;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 684
    iget-object v3, p0, Lkff;->f:Landroid/content/Context;

    iget-object v3, p0, Lkff;->g:Lkfo;

    invoke-virtual {v3}, Lkfo;->a()Ljava/lang/String;

    invoke-virtual {p0}, Lkff;->o()Ljava/lang/String;

    move-result-object v3

    iget v4, p0, Lkff;->b:I

    invoke-interface {v0, v3, p1, p2}, Lkfe;->a(Ljava/lang/String;[BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 680
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 687
    :catch_0
    move-exception v0

    .line 688
    const-string v3, "HttpOperation"

    const-string v4, "Couldn\'t log request"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 691
    :cond_1
    return-void
.end method

.method public d(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 607
    iget-object v0, p0, Lkff;->k:Ljava/lang/Exception;

    if-eqz v0, :cond_1

    .line 608
    invoke-virtual {p0}, Lkff;->o()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lkff;->k:Ljava/lang/Exception;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1c

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "] failed due to exception: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lkff;->k:Ljava/lang/Exception;

    invoke-static {p1, v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 615
    :cond_0
    :goto_0
    return-void

    .line 609
    :cond_1
    invoke-virtual {p0}, Lkff;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 610
    const/4 v0, 0x4

    invoke-static {p1, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 611
    invoke-virtual {p0}, Lkff;->o()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lkff;->i:I

    iget-object v2, p0, Lkff;->j:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x26

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "] failed due to error: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public d([BLjava/lang/String;)V
    .locals 5

    .prologue
    .line 694
    const/4 v0, 0x0

    iget-object v1, p0, Lkff;->s:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 696
    :try_start_0
    iget-object v0, p0, Lkff;->s:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkfe;

    .line 697
    invoke-virtual {p0}, Lkff;->o()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Lkfe;->b(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 698
    iget-object v3, p0, Lkff;->f:Landroid/content/Context;

    iget-object v3, p0, Lkff;->g:Lkfo;

    invoke-virtual {v3}, Lkfo;->a()Ljava/lang/String;

    invoke-virtual {p0}, Lkff;->o()Ljava/lang/String;

    move-result-object v3

    iget v4, p0, Lkff;->b:I

    iget v4, p0, Lkff;->i:I

    invoke-interface {v0, v3, p1, p2}, Lkfe;->b(Ljava/lang/String;[BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 694
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 701
    :catch_0
    move-exception v0

    .line 702
    const-string v3, "HttpOperation"

    const-string v4, "Couldn\'t log response"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 705
    :cond_1
    return-void
.end method

.method public e(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 637
    invoke-virtual {p0}, Lkff;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 638
    invoke-virtual {p0, p1}, Lkff;->d(Ljava/lang/String;)V

    .line 639
    invoke-virtual {p0}, Lkff;->w()V

    .line 641
    :cond_0
    return-void
.end method

.method public final l()V
    .locals 2

    .prologue
    .line 131
    iget-object v0, p0, Lkff;->f:Landroid/content/Context;

    const-class v1, Lkfd;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkfd;

    .line 132
    invoke-interface {v0, p0}, Lkfd;->a(Lkff;)V

    .line 133
    return-void
.end method

.method public m()V
    .locals 1

    .prologue
    .line 136
    monitor-enter p0

    .line 137
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lkff;->x:Z

    .line 138
    iget-object v0, p0, Lkff;->w:Lorg/chromium/net/HttpUrlRequest;

    .line 139
    if-eqz v0, :cond_0

    .line 140
    invoke-interface {v0}, Lorg/chromium/net/HttpUrlRequest;->h()V

    .line 141
    iget-object v0, p0, Lkff;->p:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    .line 143
    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public n()Z
    .locals 1

    .prologue
    .line 190
    iget-boolean v0, p0, Lkff;->x:Z

    return v0
.end method

.method public o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 210
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final onRequestComplete(Lorg/chromium/net/HttpUrlRequest;)V
    .locals 3

    .prologue
    .line 490
    invoke-interface {p1}, Lorg/chromium/net/HttpUrlRequest;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 491
    const/4 v0, 0x1

    iput-boolean v0, p0, Lkff;->x:Z

    .line 501
    :goto_0
    iget-object v0, p0, Lkff;->p:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    .line 502
    return-void

    .line 493
    :cond_0
    invoke-interface {p1}, Lorg/chromium/net/HttpUrlRequest;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkff;->u:Ljava/lang/String;

    .line 494
    iget-object v0, p0, Lkff;->m:Ljava/nio/channels/WritableByteChannel;

    if-nez v0, :cond_1

    .line 495
    invoke-interface {p1}, Lorg/chromium/net/HttpUrlRequest;->e()[B

    move-result-object v0

    iput-object v0, p0, Lkff;->v:[B

    .line 498
    :cond_1
    invoke-interface {p1}, Lorg/chromium/net/HttpUrlRequest;->b()I

    move-result v0

    const/4 v1, 0x0

    .line 499
    invoke-interface {p1}, Lorg/chromium/net/HttpUrlRequest;->c()Ljava/io/IOException;

    move-result-object v2

    .line 498
    invoke-virtual {p0, v0, v1, v2}, Lkff;->a(ILjava/lang/String;Ljava/io/IOException;)V

    goto :goto_0
.end method

.method public final onResponseStarted(Lorg/chromium/net/HttpUrlRequest;)V
    .locals 2

    .prologue
    .line 480
    invoke-interface {p1}, Lorg/chromium/net/HttpUrlRequest;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkff;->z:Ljava/lang/String;

    .line 481
    iget-object v0, p0, Lkff;->h:Lkfg;

    if-eqz v0, :cond_0

    .line 482
    iget-object v0, p0, Lkff;->h:Lkfg;

    invoke-virtual {v0}, Lkfg;->a()V

    .line 483
    iget-object v0, p0, Lkff;->o:Lkfk;

    iget-object v1, p0, Lkff;->z:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lkfk;->a(Ljava/lang/String;)V

    .line 485
    :cond_0
    invoke-virtual {p0, p1}, Lkff;->a(Lorg/chromium/net/HttpUrlRequest;)V

    .line 486
    return-void
.end method

.method public p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lkff;->q:Ljava/lang/String;

    return-object v0
.end method

.method public q()Lkfj;
    .locals 1

    .prologue
    .line 231
    iget-object v0, p0, Lkff;->r:Lkfj;

    return-object v0
.end method

.method public r()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 239
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lkff;->o()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    return-object v0
.end method

.method public s()Ljava/lang/String;
    .locals 1

    .prologue
    .line 286
    const/4 v0, 0x0

    return-object v0
.end method

.method public t()Z
    .locals 2

    .prologue
    .line 335
    iget v0, p0, Lkff;->i:I

    const/16 v1, 0xc8

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lkff;->k:Ljava/lang/Exception;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method u()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 381
    :try_start_0
    invoke-virtual {p0}, Lkff;->N_()V

    .line 383
    iget-object v0, p0, Lkff;->o:Lkfk;

    invoke-virtual {v0}, Lkfk;->f()V

    .line 385
    iget-object v0, p0, Lkff;->r:Lkfj;

    invoke-virtual {p0}, Lkff;->Q_()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lkfj;->a(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v3

    .line 386
    const-string v0, "HttpOperation"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "HTTP headers:\n"

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    const-string v1, "Authorization"

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, "Authorization: <removed>"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_1
    const-string v0, "\n"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 433
    :catch_0
    move-exception v0

    .line 434
    const/4 v1, 0x0

    const/4 v2, 0x0

    :try_start_1
    invoke-virtual {p0, v1, v2, v0}, Lkff;->a(ILjava/lang/String;Ljava/io/IOException;)V

    .line 435
    invoke-virtual {p0}, Lkff;->x()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 437
    iput-object v6, p0, Lkff;->v:[B

    .line 438
    iput-object v6, p0, Lkff;->w:Lorg/chromium/net/HttpUrlRequest;

    .line 439
    :goto_2
    return-void

    .line 386
    :cond_0
    :try_start_2
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, ": "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 437
    :catchall_0
    move-exception v0

    iput-object v6, p0, Lkff;->v:[B

    .line 438
    iput-object v6, p0, Lkff;->w:Lorg/chromium/net/HttpUrlRequest;

    throw v0

    .line 388
    :cond_1
    :try_start_3
    monitor-enter p0
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 389
    :try_start_4
    invoke-virtual {p0}, Lkff;->n()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 390
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 437
    iput-object v6, p0, Lkff;->v:[B

    .line 438
    iput-object v6, p0, Lkff;->w:Lorg/chromium/net/HttpUrlRequest;

    goto :goto_2

    .line 393
    :cond_2
    :try_start_5
    iget-object v0, p0, Lkff;->m:Ljava/nio/channels/WritableByteChannel;

    if-eqz v0, :cond_7

    .line 394
    iget-object v0, p0, Lkff;->f:Landroid/content/Context;

    invoke-virtual {p0}, Lkff;->Q_()Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lkff;->l:I

    iget-object v4, p0, Lkff;->m:Ljava/nio/channels/WritableByteChannel;

    move-object v5, p0

    invoke-static/range {v0 .. v5}, Ljgm;->a(Landroid/content/Context;Ljava/lang/String;ILjava/util/Map;Ljava/nio/channels/WritableByteChannel;Lorg/chromium/net/HttpUrlRequestListener;)Lorg/chromium/net/HttpUrlRequest;

    move-result-object v0

    iput-object v0, p0, Lkff;->w:Lorg/chromium/net/HttpUrlRequest;

    .line 402
    :goto_3
    iget-object v0, p0, Lkff;->p:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->close()V

    .line 403
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 405
    :try_start_6
    iget-object v0, p0, Lkff;->o:Lkfk;

    invoke-virtual {v0}, Lkfk;->d()V

    .line 407
    invoke-virtual {p0}, Lkff;->P_()[B

    move-result-object v0

    .line 408
    if-eqz v0, :cond_8

    .line 409
    iget-object v1, p0, Lkff;->w:Lorg/chromium/net/HttpUrlRequest;

    invoke-virtual {p0}, Lkff;->R_()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Lorg/chromium/net/HttpUrlRequest;->a(Ljava/lang/String;[B)V

    .line 410
    iget-object v1, p0, Lkff;->o:Lkfk;

    array-length v2, v0

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Lkfk;->a(J)V

    .line 412
    invoke-virtual {p0}, Lkff;->z()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 413
    invoke-virtual {p0}, Lkff;->s()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lkff;->c([BLjava/lang/String;)V

    .line 427
    :cond_3
    :goto_4
    iget-object v0, p0, Lkff;->w:Lorg/chromium/net/HttpUrlRequest;

    invoke-interface {v0}, Lorg/chromium/net/HttpUrlRequest;->g()V

    .line 428
    iget-object v0, p0, Lkff;->p:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->block()V

    .line 431
    const/4 v0, 0x0

    iput-object v0, p0, Lkff;->w:Lorg/chromium/net/HttpUrlRequest;

    .line 432
    iget v0, p0, Lkff;->i:I

    const/16 v1, 0xc8

    if-ne v0, v1, :cond_a

    iget-object v0, p0, Lkff;->h:Lkfg;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lkff;->h:Lkfg;

    invoke-virtual {v0}, Lkfg;->c()V

    :cond_4
    iget-object v0, p0, Lkff;->v:[B

    if-eqz v0, :cond_5

    iget-object v0, p0, Lkff;->o:Lkfk;

    iget-object v1, p0, Lkff;->v:[B

    array-length v1, v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lkfk;->b(J)V

    :cond_5
    iget-object v0, p0, Lkff;->v:[B

    iget-object v1, p0, Lkff;->u:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lkff;->a([BLjava/lang/String;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 437
    :cond_6
    :goto_5
    iput-object v6, p0, Lkff;->v:[B

    .line 438
    iput-object v6, p0, Lkff;->w:Lorg/chromium/net/HttpUrlRequest;

    goto/16 :goto_2

    .line 397
    :cond_7
    :try_start_7
    iget-object v0, p0, Lkff;->f:Landroid/content/Context;

    invoke-virtual {p0}, Lkff;->Q_()Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lkff;->l:I

    invoke-static {v0, v1, v2, v3, p0}, Ljgm;->a(Landroid/content/Context;Ljava/lang/String;ILjava/util/Map;Lorg/chromium/net/HttpUrlRequestListener;)Lorg/chromium/net/HttpUrlRequest;

    move-result-object v0

    iput-object v0, p0, Lkff;->w:Lorg/chromium/net/HttpUrlRequest;

    goto :goto_3

    .line 403
    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    throw v0

    .line 416
    :cond_8
    invoke-virtual {p0}, Lkff;->T_()Ljava/nio/channels/ReadableByteChannel;

    move-result-object v0

    .line 417
    if-eqz v0, :cond_3

    .line 418
    invoke-virtual {p0}, Lkff;->U_()J

    move-result-wide v2

    .line 419
    iget-object v1, p0, Lkff;->o:Lkfk;

    invoke-virtual {v1, v2, v3}, Lkfk;->a(J)V

    .line 420
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-gtz v1, :cond_9

    .line 421
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v4, 0x2f

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "length must not be 0, was: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 423
    :cond_9
    iget-object v1, p0, Lkff;->w:Lorg/chromium/net/HttpUrlRequest;

    invoke-virtual {p0}, Lkff;->R_()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4, v0, v2, v3}, Lorg/chromium/net/HttpUrlRequest;->a(Ljava/lang/String;Ljava/nio/channels/ReadableByteChannel;J)V

    goto/16 :goto_4

    .line 432
    :cond_a
    invoke-virtual {p0}, Lkff;->n()Z

    move-result v0

    if-nez v0, :cond_6

    iget v0, p0, Lkff;->i:I

    const/16 v1, 0x191

    if-eq v0, v1, :cond_6

    iget-object v0, p0, Lkff;->v:[B

    iget-object v1, p0, Lkff;->u:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lkff;->b([BLjava/lang/String;)V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_5
.end method

.method v()V
    .locals 3

    .prologue
    .line 458
    iget v0, p0, Lkff;->n:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkff;->n:I

    .line 459
    iget-object v0, p0, Lkff;->k:Ljava/lang/Exception;

    invoke-virtual {p0, v0}, Lkff;->a(Ljava/lang/Exception;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lkff;->n:I

    const/4 v1, 0x2

    if-ge v0, v1, :cond_1

    .line 461
    :try_start_0
    iget-object v0, p0, Lkff;->k:Ljava/lang/Exception;

    invoke-virtual {p0, v0}, Lkff;->b(Ljava/lang/Exception;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 462
    iget-object v0, p0, Lkff;->r:Lkfj;

    invoke-interface {v0}, Lkfj;->a()V

    .line 465
    :cond_0
    invoke-virtual {p0}, Lkff;->u()V

    .line 467
    invoke-virtual {p0}, Lkff;->v()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 476
    :goto_0
    return-void

    .line 469
    :catch_0
    move-exception v0

    .line 470
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2, v0}, Lkff;->a(ILjava/lang/String;Ljava/io/IOException;)V

    .line 471
    :cond_1
    iget v0, p0, Lkff;->i:I

    iget-object v1, p0, Lkff;->j:Ljava/lang/String;

    iget-object v2, p0, Lkff;->k:Ljava/lang/Exception;

    invoke-virtual {p0, v0, v1, v2}, Lkff;->a(ILjava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public w()V
    .locals 7

    .prologue
    .line 621
    invoke-virtual {p0}, Lkff;->t()Z

    move-result v0

    if-nez v0, :cond_1

    .line 631
    :cond_0
    return-void

    .line 625
    :cond_1
    iget-object v0, p0, Lkff;->k:Ljava/lang/Exception;

    if-eqz v0, :cond_2

    .line 626
    new-instance v0, Ljava/io/IOException;

    invoke-virtual {p0}, Lkff;->o()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, " operation failed"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lkff;->k:Ljava/lang/Exception;

    invoke-direct {v0, v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 627
    :cond_2
    invoke-virtual {p0}, Lkff;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 628
    new-instance v0, Ljava/io/IOException;

    invoke-virtual {p0}, Lkff;->o()Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lkff;->i:I

    iget-object v3, p0, Lkff;->j:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x28

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " operation failed, error: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected x()V
    .locals 4

    .prologue
    .line 644
    iget-object v0, p0, Lkff;->k:Ljava/lang/Exception;

    invoke-static {v0}, Lkff;->c(Ljava/lang/Exception;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 645
    const-string v0, "HttpOperation"

    invoke-virtual {p0}, Lkff;->o()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x17

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] Unexpected exception"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lkff;->k:Ljava/lang/Exception;

    invoke-static {v0, v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 647
    :cond_0
    return-void
.end method

.method y()V
    .locals 9

    .prologue
    .line 669
    const/4 v0, 0x0

    iget-object v1, p0, Lkff;->s:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v8

    move v7, v0

    :goto_0
    if-ge v7, v8, :cond_0

    .line 671
    :try_start_0
    iget-object v0, p0, Lkff;->s:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkfe;

    iget-object v1, p0, Lkff;->f:Landroid/content/Context;

    iget-object v2, p0, Lkff;->g:Lkfo;

    invoke-virtual {v2}, Lkfo;->a()Ljava/lang/String;

    move-result-object v2

    .line 672
    invoke-virtual {p0}, Lkff;->o()Ljava/lang/String;

    iget v3, p0, Lkff;->b:I

    iget-object v3, p0, Lkff;->h:Lkfg;

    iget v4, p0, Lkff;->i:I

    iget-object v5, p0, Lkff;->k:Ljava/lang/Exception;

    iget-object v6, p0, Lkff;->z:Ljava/lang/String;

    .line 671
    invoke-interface/range {v0 .. v6}, Lkfe;->a(Landroid/content/Context;Ljava/lang/String;Lkfg;ILjava/lang/Exception;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 669
    :goto_1
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    .line 673
    :catch_0
    move-exception v0

    .line 674
    const-string v1, "HttpOperation"

    const-string v2, "Couldn\'t save network data"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 677
    :cond_0
    return-void
.end method

.method public z()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 708
    iget-object v0, p0, Lkff;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_1

    .line 709
    iget-object v0, p0, Lkff;->s:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkfe;

    invoke-virtual {p0}, Lkff;->o()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4}, Lkfe;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 710
    const/4 v0, 0x1

    .line 713
    :goto_1
    return v0

    .line 708
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 713
    goto :goto_1
.end method
