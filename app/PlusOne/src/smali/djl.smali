.class public final Ldjl;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmao;",
        "Lmap;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final p:Z

.field private final q:Ljava/lang/String;

.field private final r:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 37
    const-string v3, "eventmanageguests"

    new-instance v4, Lmao;

    invoke-direct {v4}, Lmao;-><init>()V

    new-instance v5, Lmap;

    invoke-direct {v5}, Lmap;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 39
    iput-object p3, p0, Ldjl;->a:Ljava/lang/String;

    .line 40
    iput-object p4, p0, Ldjl;->b:Ljava/lang/String;

    .line 41
    iput-boolean p5, p0, Ldjl;->p:Z

    .line 42
    iput-object p6, p0, Ldjl;->q:Ljava/lang/String;

    .line 43
    iput-object p7, p0, Ldjl;->r:Ljava/lang/String;

    .line 44
    return-void
.end method


# virtual methods
.method protected a(Lmao;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 48
    new-instance v0, Lmqa;

    invoke-direct {v0}, Lmqa;-><init>()V

    iput-object v0, p1, Lmao;->a:Lmqa;

    .line 49
    iget-object v2, p1, Lmao;->a:Lmqa;

    .line 51
    iget-object v0, p0, Ldjl;->a:Ljava/lang/String;

    iput-object v0, v2, Lmqa;->a:Ljava/lang/String;

    .line 52
    iget-boolean v0, p0, Ldjl;->p:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput v0, v2, Lmqa;->c:I

    .line 54
    new-instance v0, Lmpm;

    invoke-direct {v0}, Lmpm;-><init>()V

    .line 55
    iget-object v3, p0, Ldjl;->b:Ljava/lang/String;

    iput-object v3, v0, Lmpm;->b:Ljava/lang/String;

    .line 56
    iget-object v3, p0, Ldjl;->a:Ljava/lang/String;

    iput-object v3, v0, Lmpm;->a:Ljava/lang/String;

    .line 57
    iput-object v0, v2, Lmqa;->b:Lmpm;

    .line 59
    new-instance v0, Lpaf;

    invoke-direct {v0}, Lpaf;-><init>()V

    .line 60
    iget-object v3, p0, Ldjl;->q:Ljava/lang/String;

    iput-object v3, v0, Lpaf;->c:Ljava/lang/String;

    .line 61
    iget-object v3, p0, Ldjl;->r:Ljava/lang/String;

    iput-object v3, v0, Lpaf;->e:Ljava/lang/String;

    .line 62
    new-array v1, v1, [Lpaf;

    const/4 v3, 0x0

    aput-object v0, v1, v3

    iput-object v1, v2, Lmqa;->d:[Lpaf;

    .line 63
    return-void

    .line 52
    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method protected a(Lmap;)V
    .locals 6

    .prologue
    .line 67
    iget-object v0, p1, Lmap;->a:Lmqb;

    .line 68
    iget-object v1, v0, Lmqb;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lmqb;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Ldjl;->f:Landroid/content/Context;

    iget v1, p0, Ldjl;->c:I

    iget-object v2, p0, Ldjl;->a:Ljava/lang/String;

    iget-boolean v3, p0, Ldjl;->p:Z

    iget-object v4, p0, Ldjl;->q:Ljava/lang/String;

    iget-object v5, p0, Ldjl;->r:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Ldrm;->a(Landroid/content/Context;ILjava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    .line 72
    :cond_0
    return-void
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 18
    check-cast p1, Lmao;

    invoke-virtual {p0, p1}, Ldjl;->a(Lmao;)V

    return-void
.end method

.method protected synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 18
    check-cast p1, Lmap;

    invoke-virtual {p0, p1}, Ldjl;->a(Lmap;)V

    return-void
.end method
