.class public final Lmjf;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lmyw;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2590
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2593
    iput-object v0, p0, Lmjf;->apiHeader:Llyr;

    .line 2596
    iput-object v0, p0, Lmjf;->a:Lmyw;

    .line 2590
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 2613
    const/4 v0, 0x0

    .line 2614
    iget-object v1, p0, Lmjf;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 2615
    const/4 v0, 0x1

    iget-object v1, p0, Lmjf;->apiHeader:Llyr;

    .line 2616
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2618
    :cond_0
    iget-object v1, p0, Lmjf;->a:Lmyw;

    if-eqz v1, :cond_1

    .line 2619
    const/4 v1, 0x2

    iget-object v2, p0, Lmjf;->a:Lmyw;

    .line 2620
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2622
    :cond_1
    iget-object v1, p0, Lmjf;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2623
    iput v0, p0, Lmjf;->ai:I

    .line 2624
    return v0
.end method

.method public a(Loxn;)Lmjf;
    .locals 2

    .prologue
    .line 2632
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2633
    sparse-switch v0, :sswitch_data_0

    .line 2637
    iget-object v1, p0, Lmjf;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 2638
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmjf;->ah:Ljava/util/List;

    .line 2641
    :cond_1
    iget-object v1, p0, Lmjf;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2643
    :sswitch_0
    return-object p0

    .line 2648
    :sswitch_1
    iget-object v0, p0, Lmjf;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 2649
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmjf;->apiHeader:Llyr;

    .line 2651
    :cond_2
    iget-object v0, p0, Lmjf;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2655
    :sswitch_2
    iget-object v0, p0, Lmjf;->a:Lmyw;

    if-nez v0, :cond_3

    .line 2656
    new-instance v0, Lmyw;

    invoke-direct {v0}, Lmyw;-><init>()V

    iput-object v0, p0, Lmjf;->a:Lmyw;

    .line 2658
    :cond_3
    iget-object v0, p0, Lmjf;->a:Lmyw;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2633
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 2601
    iget-object v0, p0, Lmjf;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 2602
    const/4 v0, 0x1

    iget-object v1, p0, Lmjf;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2604
    :cond_0
    iget-object v0, p0, Lmjf;->a:Lmyw;

    if-eqz v0, :cond_1

    .line 2605
    const/4 v0, 0x2

    iget-object v1, p0, Lmjf;->a:Lmyw;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2607
    :cond_1
    iget-object v0, p0, Lmjf;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2609
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2586
    invoke-virtual {p0, p1}, Lmjf;->a(Loxn;)Lmjf;

    move-result-object v0

    return-object v0
.end method
