.class public final Lmsn;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmsn;


# instance fields
.field public b:Ljava/lang/String;

.field private c:Ljava/lang/Integer;

.field private d:Ljava/lang/Integer;

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 596
    const/4 v0, 0x0

    new-array v0, v0, [Lmsn;

    sput-object v0, Lmsn;->a:[Lmsn;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 597
    invoke-direct {p0}, Loxq;-><init>()V

    .line 606
    const/high16 v0, -0x80000000

    iput v0, p0, Lmsn;->e:I

    .line 597
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 629
    const/4 v0, 0x0

    .line 630
    iget-object v1, p0, Lmsn;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 631
    const/4 v0, 0x1

    iget-object v1, p0, Lmsn;->b:Ljava/lang/String;

    .line 632
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 634
    :cond_0
    iget-object v1, p0, Lmsn;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 635
    const/4 v1, 0x2

    iget-object v2, p0, Lmsn;->c:Ljava/lang/Integer;

    .line 636
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 638
    :cond_1
    iget-object v1, p0, Lmsn;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 639
    const/4 v1, 0x3

    iget-object v2, p0, Lmsn;->d:Ljava/lang/Integer;

    .line 640
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 642
    :cond_2
    iget v1, p0, Lmsn;->e:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_3

    .line 643
    const/4 v1, 0x4

    iget v2, p0, Lmsn;->e:I

    .line 644
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 646
    :cond_3
    iget-object v1, p0, Lmsn;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 647
    iput v0, p0, Lmsn;->ai:I

    .line 648
    return v0
.end method

.method public a(Loxn;)Lmsn;
    .locals 2

    .prologue
    .line 656
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 657
    sparse-switch v0, :sswitch_data_0

    .line 661
    iget-object v1, p0, Lmsn;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 662
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmsn;->ah:Ljava/util/List;

    .line 665
    :cond_1
    iget-object v1, p0, Lmsn;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 667
    :sswitch_0
    return-object p0

    .line 672
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmsn;->b:Ljava/lang/String;

    goto :goto_0

    .line 676
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmsn;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 680
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmsn;->d:Ljava/lang/Integer;

    goto :goto_0

    .line 684
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 685
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    .line 690
    :cond_2
    iput v0, p0, Lmsn;->e:I

    goto :goto_0

    .line 692
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lmsn;->e:I

    goto :goto_0

    .line 657
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 611
    iget-object v0, p0, Lmsn;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 612
    const/4 v0, 0x1

    iget-object v1, p0, Lmsn;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 614
    :cond_0
    iget-object v0, p0, Lmsn;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 615
    const/4 v0, 0x2

    iget-object v1, p0, Lmsn;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 617
    :cond_1
    iget-object v0, p0, Lmsn;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 618
    const/4 v0, 0x3

    iget-object v1, p0, Lmsn;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 620
    :cond_2
    iget v0, p0, Lmsn;->e:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_3

    .line 621
    const/4 v0, 0x4

    iget v1, p0, Lmsn;->e:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 623
    :cond_3
    iget-object v0, p0, Lmsn;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 625
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 593
    invoke-virtual {p0, p1}, Lmsn;->a(Loxn;)Lmsn;

    move-result-object v0

    return-object v0
.end method
