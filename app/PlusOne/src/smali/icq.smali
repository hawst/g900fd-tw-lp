.class public final Licq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llql;
.implements Llqz;
.implements Llrb;
.implements Llrc;
.implements Llrg;


# instance fields
.field private a:Landroid/view/View;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/view/View;

.field private d:Landroid/widget/TextView;

.field private e:Z

.field private f:Z

.field private g:I

.field private h:Ljava/lang/CharSequence;

.field private i:I

.field private j:Z

.field private k:Z

.field private l:Lict;

.field private m:Lico;

.field private final n:Lics;


# direct methods
.method public constructor <init>(Llqr;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-boolean v0, p0, Licq;->e:Z

    .line 61
    iput-boolean v0, p0, Licq;->f:Z

    .line 70
    sget-object v0, Lict;->a:Lict;

    iput-object v0, p0, Licq;->l:Lict;

    .line 73
    new-instance v0, Lics;

    invoke-direct {v0, p0}, Lics;-><init>(Licq;)V

    iput-object v0, p0, Licq;->n:Lics;

    .line 77
    invoke-virtual {p1, p0}, Llqr;->a(Llrg;)Llrg;

    .line 78
    return-void
.end method

.method private static a(Landroid/widget/TextView;ILjava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 348
    if-nez p0, :cond_0

    .line 355
    :goto_0
    return-void

    .line 351
    :cond_0
    if-eqz p1, :cond_1

    .line 352
    invoke-virtual {p0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 354
    :cond_1
    invoke-virtual {p0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private g()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 271
    iget-boolean v0, p0, Licq;->k:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Licq;->a:Landroid/view/View;

    if-nez v0, :cond_1

    .line 292
    :cond_0
    :goto_0
    return-void

    .line 275
    :cond_1
    sget-object v0, Licr;->a:[I

    iget-object v1, p0, Licq;->l:Lict;

    invoke-virtual {v1}, Lict;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 295
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 277
    :pswitch_0
    iget-boolean v0, p0, Licq;->j:Z

    if-eqz v0, :cond_2

    .line 280
    iget-object v0, p0, Licq;->n:Lics;

    invoke-virtual {v0}, Lics;->a()V

    goto :goto_0

    .line 282
    :cond_2
    invoke-virtual {p0}, Licq;->f()V

    goto :goto_0

    .line 287
    :pswitch_1
    iget-object v0, p0, Licq;->n:Lics;

    invoke-virtual {v0}, Lics;->b()V

    iget-boolean v0, p0, Licq;->e:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Licq;->c:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Licq;->a:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Licq;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_1
    iget-object v0, p0, Licq;->m:Lico;

    if-eqz v0, :cond_0

    iget-object v0, p0, Licq;->m:Lico;

    invoke-interface {v0}, Lico;->a()V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Licq;->a:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 291
    :pswitch_2
    iget-object v0, p0, Licq;->n:Lics;

    invoke-virtual {v0}, Lics;->b()V

    iget-object v0, p0, Licq;->a:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Licq;->m:Lico;

    if-eqz v0, :cond_0

    iget-object v0, p0, Licq;->m:Lico;

    invoke-interface {v0}, Lico;->c()V

    goto :goto_0

    .line 275
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private h()V
    .locals 3

    .prologue
    .line 337
    iget-object v0, p0, Licq;->a:Landroid/view/View;

    if-nez v0, :cond_0

    .line 342
    :goto_0
    return-void

    .line 340
    :cond_0
    iget-object v0, p0, Licq;->b:Landroid/widget/TextView;

    iget v1, p0, Licq;->g:I

    iget-object v2, p0, Licq;->h:Ljava/lang/CharSequence;

    invoke-static {v0, v1, v2}, Licq;->a(Landroid/widget/TextView;ILjava/lang/CharSequence;)V

    .line 341
    iget-object v0, p0, Licq;->d:Landroid/widget/TextView;

    iget v1, p0, Licq;->i:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Licq;->a(Landroid/widget/TextView;ILjava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public a(I)Licq;
    .locals 0

    .prologue
    .line 194
    iput p1, p0, Licq;->i:I

    .line 196
    invoke-direct {p0}, Licq;->h()V

    .line 197
    return-object p0
.end method

.method public a(Lico;)Licq;
    .locals 0

    .prologue
    .line 142
    iput-object p1, p0, Licq;->m:Lico;

    .line 143
    return-object p0
.end method

.method public a(Ljava/lang/CharSequence;)Licq;
    .locals 1

    .prologue
    .line 211
    iput-object p1, p0, Licq;->h:Ljava/lang/CharSequence;

    .line 212
    const/4 v0, 0x0

    iput v0, p0, Licq;->g:I

    .line 213
    invoke-direct {p0}, Licq;->h()V

    .line 214
    return-object p0
.end method

.method public a(Z)Licq;
    .locals 0

    .prologue
    .line 152
    iput-boolean p1, p0, Licq;->e:Z

    .line 153
    return-object p0
.end method

.method public a()V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 98
    sget-object v0, Lict;->a:Lict;

    invoke-virtual {p0, v0}, Licq;->a(Lict;)V

    .line 99
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 236
    if-eqz p1, :cond_0

    .line 237
    const/4 v0, 0x1

    iput-boolean v0, p0, Licq;->j:Z

    .line 239
    :cond_0
    return-void
.end method

.method public a(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 244
    const v0, 0x1020004

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Licq;->a:Landroid/view/View;

    .line 245
    iget-object v0, p0, Licq;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Licq;->a:Landroid/view/View;

    const v1, 0x7f10025f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Licq;->b:Landroid/widget/TextView;

    iget-object v0, p0, Licq;->a:Landroid/view/View;

    const v1, 0x7f100260

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Licq;->c:Landroid/view/View;

    iget-object v0, p0, Licq;->a:Landroid/view/View;

    const v1, 0x7f100262

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Licq;->d:Landroid/widget/TextView;

    invoke-direct {p0}, Licq;->h()V

    .line 246
    :cond_0
    return-void
.end method

.method public a(Lict;)V
    .locals 1

    .prologue
    .line 88
    invoke-static {p1}, Llsk;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lict;

    iput-object v0, p0, Licq;->l:Lict;

    .line 89
    invoke-direct {p0}, Licq;->g()V

    .line 90
    return-void
.end method

.method public b(I)Licq;
    .locals 1

    .prologue
    .line 228
    const/4 v0, 0x0

    iput-object v0, p0, Licq;->h:Ljava/lang/CharSequence;

    .line 229
    iput p1, p0, Licq;->g:I

    .line 230
    invoke-direct {p0}, Licq;->h()V

    .line 231
    return-object p0
.end method

.method public b(Z)Licq;
    .locals 0

    .prologue
    .line 162
    iput-boolean p1, p0, Licq;->f:Z

    .line 163
    return-object p0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 261
    const/4 v0, 0x1

    iput-boolean v0, p0, Licq;->k:Z

    .line 262
    invoke-direct {p0}, Licq;->g()V

    .line 263
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 267
    const/4 v0, 0x0

    iput-boolean v0, p0, Licq;->k:Z

    .line 268
    return-void
.end method

.method public d()V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 107
    sget-object v0, Lict;->c:Lict;

    invoke-virtual {p0, v0}, Licq;->a(Lict;)V

    .line 108
    return-void
.end method

.method public e()V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 116
    sget-object v0, Lict;->b:Lict;

    invoke-virtual {p0, v0}, Licq;->a(Lict;)V

    .line 117
    return-void
.end method

.method f()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 314
    iget-boolean v0, p0, Licq;->k:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Licq;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Licq;->l:Lict;

    sget-object v1, Lict;->a:Lict;

    if-ne v0, v1, :cond_0

    .line 315
    iget-boolean v0, p0, Licq;->f:Z

    if-eqz v0, :cond_1

    .line 316
    iget-object v0, p0, Licq;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 317
    iget-object v0, p0, Licq;->a:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 318
    iget-object v0, p0, Licq;->c:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 322
    :goto_0
    iget-object v0, p0, Licq;->m:Lico;

    if-eqz v0, :cond_0

    .line 323
    iget-object v0, p0, Licq;->m:Lico;

    invoke-interface {v0}, Lico;->b()V

    .line 326
    :cond_0
    return-void

    .line 320
    :cond_1
    iget-object v0, p0, Licq;->a:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
