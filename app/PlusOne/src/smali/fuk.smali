.class public final Lfuk;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Lhgw;)I
    .locals 4

    .prologue
    const v0, 0x7f0a0898

    .line 165
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lhgw;->k()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 189
    :cond_0
    :goto_0
    :sswitch_0
    return v0

    .line 169
    :cond_1
    invoke-virtual {p0}, Lhgw;->b()[Lhxc;

    move-result-object v1

    .line 170
    if-eqz v1, :cond_2

    array-length v2, v1

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    .line 171
    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v1}, Lhxc;->c()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 189
    :cond_2
    const v0, 0x7f0a0899

    goto :goto_0

    .line 173
    :sswitch_1
    const v0, 0x7f0a0894

    goto :goto_0

    .line 176
    :sswitch_2
    const v0, 0x7f0a0895

    goto :goto_0

    .line 179
    :sswitch_3
    const v0, 0x7f0a0896

    goto :goto_0

    .line 182
    :sswitch_4
    const v0, 0x7f0a0897

    goto :goto_0

    .line 171
    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_1
        0x7 -> :sswitch_2
        0x8 -> :sswitch_3
        0x9 -> :sswitch_4
        0x65 -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(Landroid/app/Activity;IILhgw;)Landroid/content/Intent;
    .locals 9

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 241
    invoke-virtual {p3}, Lhgw;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 242
    const/4 v3, 0x0

    .line 248
    :goto_0
    invoke-virtual {p0, p2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x5

    move-object v0, p0

    move v1, p1

    move v7, v6

    move v8, v5

    .line 247
    invoke-static/range {v0 .. v8}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;Lhgw;IZZZZ)Landroid/content/Intent;

    move-result-object v0

    .line 254
    return-object v0

    :cond_0
    move-object v3, p3

    .line 244
    goto :goto_0
.end method

.method public static a(ILjava/lang/String;Ljava/lang/String;Lhgw;)Lekr;
    .locals 6

    .prologue
    .line 137
    const/4 v0, 0x3

    invoke-static {p1, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 138
    invoke-virtual {p3}, Lhgw;->f()I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x19

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Hidden count: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 140
    invoke-virtual {p3}, Lhgw;->g()I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x1b

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Audience users: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 141
    invoke-virtual {p3}, Lhgw;->a()[Ljqs;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 142
    const-string v4, "Users: "

    invoke-virtual {v3}, Ljqs;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v4, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 141
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 142
    :cond_0
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 146
    :cond_1
    new-instance v0, Lekr;

    invoke-direct {v0}, Lekr;-><init>()V

    .line 147
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 148
    const-string v2, "account_id"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 149
    const-string v2, "audience"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 150
    const-string v2, "people_list_title"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    invoke-virtual {v0, v1}, Lekr;->f(Landroid/os/Bundle;)V

    .line 152
    return-object v0
.end method

.method public static a(Landroid/content/Context;Lhgw;Lhgw;)Z
    .locals 2

    .prologue
    .line 262
    .line 263
    invoke-virtual {p1, p0}, Lhgw;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, p0}, Lhgw;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 262
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method
