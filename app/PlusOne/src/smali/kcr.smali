.class public final Lkcr;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Landroid/content/res/ColorStateList;

.field private static final b:Landroid/content/res/ColorStateList;

.field private static final c:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lkcs;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const v0, -0xe0e0f

    invoke-static {v0}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    sput-object v0, Lkcr;->a:Landroid/content/res/ColorStateList;

    .line 41
    const/high16 v0, 0x20000000

    .line 42
    invoke-static {v0}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    sput-object v0, Lkcr;->b:Landroid/content/res/ColorStateList;

    .line 54
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lkcr;->c:Landroid/util/SparseArray;

    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/util/AttributeSet;IIII)Landroid/widget/Button;
    .locals 7

    .prologue
    .line 203
    new-instance v1, Landroid/widget/Button;

    invoke-direct {v1, p0, p1, p2}, Landroid/widget/Button;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    move-object v0, p0

    move v2, p3

    move v3, p4

    move v4, p5

    move v5, p4

    move v6, p5

    invoke-static/range {v0 .. v6}, Lkcr;->a(Landroid/content/Context;Landroid/widget/Button;IIIII)V

    return-object v1
.end method

.method private static a(I)Lkcs;
    .locals 6

    .prologue
    const/16 v5, 0x19

    const/16 v4, 0x10

    const/16 v3, 0xb

    const v2, 0x7f0205c9

    const v1, 0x7f0205d7

    .line 57
    sget-object v0, Lkcr;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkcs;

    .line 59
    if-nez v0, :cond_0

    .line 60
    new-instance v0, Lkcs;

    invoke-direct {v0}, Lkcs;-><init>()V

    packed-switch p0, :pswitch_data_0

    const/4 v0, 0x0

    .line 61
    :goto_0
    sget-object v1, Lkcr;->c:Landroid/util/SparseArray;

    invoke-virtual {v1, p0, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 64
    :cond_0
    return-object v0

    .line 60
    :pswitch_0
    const v1, 0x7f02008c

    iput v1, v0, Lkcs;->a:I

    const/16 v1, 0x1f

    iput v1, v0, Lkcs;->b:I

    sget-object v1, Lkcr;->b:Landroid/content/res/ColorStateList;

    iput-object v1, v0, Lkcs;->c:Landroid/content/res/ColorStateList;

    goto :goto_0

    :pswitch_1
    const v1, 0x7f0205db

    iput v1, v0, Lkcs;->a:I

    const/16 v1, 0xa

    iput v1, v0, Lkcs;->b:I

    sget-object v1, Lkcr;->b:Landroid/content/res/ColorStateList;

    iput-object v1, v0, Lkcs;->c:Landroid/content/res/ColorStateList;

    goto :goto_0

    :pswitch_2
    const v1, 0x7f02055c

    iput v1, v0, Lkcs;->a:I

    const/16 v1, 0x20

    iput v1, v0, Lkcs;->b:I

    sget-object v1, Lkcr;->b:Landroid/content/res/ColorStateList;

    iput-object v1, v0, Lkcs;->c:Landroid/content/res/ColorStateList;

    goto :goto_0

    :pswitch_3
    iput v2, v0, Lkcs;->a:I

    iput v4, v0, Lkcs;->b:I

    sget-object v1, Lkcr;->a:Landroid/content/res/ColorStateList;

    iput-object v1, v0, Lkcs;->c:Landroid/content/res/ColorStateList;

    goto :goto_0

    :pswitch_4
    iput v2, v0, Lkcs;->a:I

    iput v5, v0, Lkcs;->b:I

    sget-object v1, Lkcr;->a:Landroid/content/res/ColorStateList;

    iput-object v1, v0, Lkcs;->c:Landroid/content/res/ColorStateList;

    goto :goto_0

    :pswitch_5
    iput v2, v0, Lkcs;->a:I

    const/4 v1, 0x3

    iput v1, v0, Lkcs;->b:I

    sget-object v1, Lkcr;->a:Landroid/content/res/ColorStateList;

    iput-object v1, v0, Lkcs;->c:Landroid/content/res/ColorStateList;

    goto :goto_0

    :pswitch_6
    iput v2, v0, Lkcs;->a:I

    iput v3, v0, Lkcs;->b:I

    sget-object v1, Lkcr;->a:Landroid/content/res/ColorStateList;

    iput-object v1, v0, Lkcs;->c:Landroid/content/res/ColorStateList;

    goto :goto_0

    :pswitch_7
    iput v2, v0, Lkcs;->a:I

    const/16 v1, 0x1d

    iput v1, v0, Lkcs;->b:I

    sget-object v1, Lkcr;->a:Landroid/content/res/ColorStateList;

    iput-object v1, v0, Lkcs;->c:Landroid/content/res/ColorStateList;

    goto :goto_0

    :pswitch_8
    iput v1, v0, Lkcs;->a:I

    const/16 v1, 0xf

    iput v1, v0, Lkcs;->b:I

    sget-object v1, Lkcr;->a:Landroid/content/res/ColorStateList;

    iput-object v1, v0, Lkcs;->c:Landroid/content/res/ColorStateList;

    goto :goto_0

    :pswitch_9
    iput v1, v0, Lkcs;->a:I

    const/16 v1, 0x17

    iput v1, v0, Lkcs;->b:I

    sget-object v1, Lkcr;->a:Landroid/content/res/ColorStateList;

    iput-object v1, v0, Lkcs;->c:Landroid/content/res/ColorStateList;

    goto :goto_0

    :pswitch_a
    iput v1, v0, Lkcs;->a:I

    iput v5, v0, Lkcs;->b:I

    sget-object v1, Lkcr;->a:Landroid/content/res/ColorStateList;

    iput-object v1, v0, Lkcs;->c:Landroid/content/res/ColorStateList;

    goto :goto_0

    :pswitch_b
    iput v1, v0, Lkcs;->a:I

    iput v4, v0, Lkcs;->b:I

    sget-object v1, Lkcr;->a:Landroid/content/res/ColorStateList;

    iput-object v1, v0, Lkcs;->c:Landroid/content/res/ColorStateList;

    goto/16 :goto_0

    :pswitch_c
    iput v1, v0, Lkcs;->a:I

    iput v3, v0, Lkcs;->b:I

    sget-object v1, Lkcr;->a:Landroid/content/res/ColorStateList;

    iput-object v1, v0, Lkcs;->c:Landroid/content/res/ColorStateList;

    goto/16 :goto_0

    :pswitch_d
    iput v1, v0, Lkcs;->a:I

    const/16 v1, 0x1d

    iput v1, v0, Lkcs;->b:I

    sget-object v1, Lkcr;->a:Landroid/content/res/ColorStateList;

    iput-object v1, v0, Lkcs;->c:Landroid/content/res/ColorStateList;

    goto/16 :goto_0

    :pswitch_e
    iput v1, v0, Lkcs;->a:I

    const/16 v1, 0x2d

    iput v1, v0, Lkcs;->b:I

    sget-object v1, Lkcr;->a:Landroid/content/res/ColorStateList;

    iput-object v1, v0, Lkcs;->c:Landroid/content/res/ColorStateList;

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method

.method public static a(Landroid/content/Context;Landroid/widget/Button;III)V
    .locals 7

    .prologue
    .line 238
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p3

    move v6, p4

    invoke-static/range {v0 .. v6}, Lkcr;->a(Landroid/content/Context;Landroid/widget/Button;IIIII)V

    .line 240
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/widget/Button;IIIII)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 256
    if-nez p1, :cond_0

    .line 273
    :goto_0
    return-void

    .line 260
    :cond_0
    invoke-static {p2}, Lkcr;->a(I)Lkcs;

    move-result-object v0

    .line 261
    if-eqz v0, :cond_1

    .line 262
    iget v1, v0, Lkcs;->b:I

    invoke-static {p0, p1, v1}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 263
    invoke-static {}, Llsj;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 266
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v2, v0, Lkcs;->a:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iget-object v0, v0, Lkcs;->c:Landroid/content/res/ColorStateList;

    .line 265
    invoke-static {v1, v0, v1}, Lkct;->a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 271
    :cond_1
    :goto_1
    const/16 v0, 0x10

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setGravity(I)V

    .line 272
    invoke-virtual {p1, p3, p4, p5, p6}, Landroid/widget/Button;->setPadding(IIII)V

    goto :goto_0

    .line 268
    :cond_2
    iget v0, v0, Lkcs;->a:I

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setBackgroundResource(I)V

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;Landroid/widget/ImageButton;III)V
    .locals 7

    .prologue
    .line 324
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p3

    move v6, p4

    invoke-static/range {v0 .. v6}, Lkcr;->a(Landroid/content/Context;Landroid/widget/ImageButton;IIIII)V

    .line 326
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/widget/ImageButton;IIIII)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 342
    if-nez p1, :cond_0

    .line 357
    :goto_0
    return-void

    .line 346
    :cond_0
    invoke-static {p2}, Lkcr;->a(I)Lkcs;

    move-result-object v0

    .line 347
    if-eqz v0, :cond_1

    .line 348
    invoke-static {}, Llsj;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 351
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v2, v0, Lkcs;->a:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iget-object v0, v0, Lkcs;->c:Landroid/content/res/ColorStateList;

    .line 350
    invoke-static {v1, v0, v1}, Lkct;->a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 356
    :cond_1
    :goto_1
    invoke-virtual {p1, p3, p4, p5, p6}, Landroid/widget/ImageButton;->setPadding(IIII)V

    goto :goto_0

    .line 353
    :cond_2
    iget v0, v0, Lkcs;->a:I

    invoke-virtual {p1, v0}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    goto :goto_1
.end method

.method public static b(Landroid/content/Context;Landroid/util/AttributeSet;IIII)Landroid/widget/ImageButton;
    .locals 7

    .prologue
    .line 288
    new-instance v1, Landroid/widget/ImageButton;

    invoke-direct {v1, p0, p1, p2}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    move-object v0, p0

    move v2, p3

    move v3, p4

    move v4, p5

    move v5, p4

    move v6, p5

    invoke-static/range {v0 .. v6}, Lkcr;->a(Landroid/content/Context;Landroid/widget/ImageButton;IIIII)V

    return-object v1
.end method
