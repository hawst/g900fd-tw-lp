.class public final Lniu;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lnit;

.field private b:Lnja;

.field private c:Lnja;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3532
    invoke-direct {p0}, Loxq;-><init>()V

    .line 3535
    sget-object v0, Lnit;->a:[Lnit;

    iput-object v0, p0, Lniu;->a:[Lnit;

    .line 3538
    iput-object v1, p0, Lniu;->b:Lnja;

    .line 3541
    iput-object v1, p0, Lniu;->c:Lnja;

    .line 3532
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 3565
    .line 3566
    iget-object v1, p0, Lniu;->a:[Lnit;

    if-eqz v1, :cond_1

    .line 3567
    iget-object v2, p0, Lniu;->a:[Lnit;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 3568
    if-eqz v4, :cond_0

    .line 3569
    const/4 v5, 0x1

    .line 3570
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 3567
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3574
    :cond_1
    iget-object v1, p0, Lniu;->b:Lnja;

    if-eqz v1, :cond_2

    .line 3575
    const/4 v1, 0x2

    iget-object v2, p0, Lniu;->b:Lnja;

    .line 3576
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3578
    :cond_2
    iget-object v1, p0, Lniu;->c:Lnja;

    if-eqz v1, :cond_3

    .line 3579
    const/4 v1, 0x3

    iget-object v2, p0, Lniu;->c:Lnja;

    .line 3580
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3582
    :cond_3
    iget-object v1, p0, Lniu;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3583
    iput v0, p0, Lniu;->ai:I

    .line 3584
    return v0
.end method

.method public a(Loxn;)Lniu;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3592
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3593
    sparse-switch v0, :sswitch_data_0

    .line 3597
    iget-object v2, p0, Lniu;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 3598
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lniu;->ah:Ljava/util/List;

    .line 3601
    :cond_1
    iget-object v2, p0, Lniu;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3603
    :sswitch_0
    return-object p0

    .line 3608
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 3609
    iget-object v0, p0, Lniu;->a:[Lnit;

    if-nez v0, :cond_3

    move v0, v1

    .line 3610
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnit;

    .line 3611
    iget-object v3, p0, Lniu;->a:[Lnit;

    if-eqz v3, :cond_2

    .line 3612
    iget-object v3, p0, Lniu;->a:[Lnit;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3614
    :cond_2
    iput-object v2, p0, Lniu;->a:[Lnit;

    .line 3615
    :goto_2
    iget-object v2, p0, Lniu;->a:[Lnit;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 3616
    iget-object v2, p0, Lniu;->a:[Lnit;

    new-instance v3, Lnit;

    invoke-direct {v3}, Lnit;-><init>()V

    aput-object v3, v2, v0

    .line 3617
    iget-object v2, p0, Lniu;->a:[Lnit;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 3618
    invoke-virtual {p1}, Loxn;->a()I

    .line 3615
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 3609
    :cond_3
    iget-object v0, p0, Lniu;->a:[Lnit;

    array-length v0, v0

    goto :goto_1

    .line 3621
    :cond_4
    iget-object v2, p0, Lniu;->a:[Lnit;

    new-instance v3, Lnit;

    invoke-direct {v3}, Lnit;-><init>()V

    aput-object v3, v2, v0

    .line 3622
    iget-object v2, p0, Lniu;->a:[Lnit;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3626
    :sswitch_2
    iget-object v0, p0, Lniu;->b:Lnja;

    if-nez v0, :cond_5

    .line 3627
    new-instance v0, Lnja;

    invoke-direct {v0}, Lnja;-><init>()V

    iput-object v0, p0, Lniu;->b:Lnja;

    .line 3629
    :cond_5
    iget-object v0, p0, Lniu;->b:Lnja;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3633
    :sswitch_3
    iget-object v0, p0, Lniu;->c:Lnja;

    if-nez v0, :cond_6

    .line 3634
    new-instance v0, Lnja;

    invoke-direct {v0}, Lnja;-><init>()V

    iput-object v0, p0, Lniu;->c:Lnja;

    .line 3636
    :cond_6
    iget-object v0, p0, Lniu;->c:Lnja;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 3593
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 3546
    iget-object v0, p0, Lniu;->a:[Lnit;

    if-eqz v0, :cond_1

    .line 3547
    iget-object v1, p0, Lniu;->a:[Lnit;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 3548
    if-eqz v3, :cond_0

    .line 3549
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 3547
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3553
    :cond_1
    iget-object v0, p0, Lniu;->b:Lnja;

    if-eqz v0, :cond_2

    .line 3554
    const/4 v0, 0x2

    iget-object v1, p0, Lniu;->b:Lnja;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 3556
    :cond_2
    iget-object v0, p0, Lniu;->c:Lnja;

    if-eqz v0, :cond_3

    .line 3557
    const/4 v0, 0x3

    iget-object v1, p0, Lniu;->c:Lnja;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 3559
    :cond_3
    iget-object v0, p0, Lniu;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3561
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3528
    invoke-virtual {p0, p1}, Lniu;->a(Loxn;)Lniu;

    move-result-object v0

    return-object v0
.end method
