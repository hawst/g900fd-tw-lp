.class public final enum Llkt;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Llkt;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Llkt;

.field public static final enum b:Llkt;

.field public static final enum c:Llkt;

.field public static final enum d:Llkt;

.field private static final synthetic h:[Llkt;


# instance fields
.field e:Llkt;

.field f:Llkt;

.field public final g:F


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 188
    new-instance v0, Llkt;

    const-string v1, "HIDDEN"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v3, v2}, Llkt;-><init>(Ljava/lang/String;IF)V

    sput-object v0, Llkt;->a:Llkt;

    .line 189
    new-instance v0, Llkt;

    const-string v1, "COLLAPSED"

    const/high16 v2, 0x41c80000    # 25.0f

    invoke-direct {v0, v1, v4, v2}, Llkt;-><init>(Ljava/lang/String;IF)V

    sput-object v0, Llkt;->b:Llkt;

    .line 190
    new-instance v0, Llkt;

    const-string v1, "EXPANDED"

    const/high16 v2, 0x42960000    # 75.0f

    invoke-direct {v0, v1, v5, v2}, Llkt;-><init>(Ljava/lang/String;IF)V

    sput-object v0, Llkt;->c:Llkt;

    .line 191
    new-instance v0, Llkt;

    const-string v1, "FULLY_EXPANDED"

    const/high16 v2, 0x42c80000    # 100.0f

    invoke-direct {v0, v1, v6, v2}, Llkt;-><init>(Ljava/lang/String;IF)V

    sput-object v0, Llkt;->d:Llkt;

    .line 187
    const/4 v0, 0x4

    new-array v0, v0, [Llkt;

    sget-object v1, Llkt;->a:Llkt;

    aput-object v1, v0, v3

    sget-object v1, Llkt;->b:Llkt;

    aput-object v1, v0, v4

    sget-object v1, Llkt;->c:Llkt;

    aput-object v1, v0, v5

    sget-object v1, Llkt;->d:Llkt;

    aput-object v1, v0, v6

    sput-object v0, Llkt;->h:[Llkt;

    .line 194
    sget-object v0, Llkt;->a:Llkt;

    sget-object v1, Llkt;->a:Llkt;

    iput-object v1, v0, Llkt;->e:Llkt;

    .line 195
    sget-object v0, Llkt;->a:Llkt;

    sget-object v1, Llkt;->a:Llkt;

    iput-object v1, v0, Llkt;->f:Llkt;

    .line 196
    sget-object v0, Llkt;->b:Llkt;

    sget-object v1, Llkt;->b:Llkt;

    iput-object v1, v0, Llkt;->e:Llkt;

    .line 197
    sget-object v0, Llkt;->b:Llkt;

    sget-object v1, Llkt;->c:Llkt;

    iput-object v1, v0, Llkt;->f:Llkt;

    .line 198
    sget-object v0, Llkt;->c:Llkt;

    sget-object v1, Llkt;->b:Llkt;

    iput-object v1, v0, Llkt;->e:Llkt;

    .line 199
    sget-object v0, Llkt;->c:Llkt;

    sget-object v1, Llkt;->d:Llkt;

    iput-object v1, v0, Llkt;->f:Llkt;

    .line 200
    sget-object v0, Llkt;->d:Llkt;

    sget-object v1, Llkt;->c:Llkt;

    iput-object v1, v0, Llkt;->e:Llkt;

    .line 201
    sget-object v0, Llkt;->d:Llkt;

    sget-object v1, Llkt;->d:Llkt;

    iput-object v1, v0, Llkt;->f:Llkt;

    .line 202
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IF)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F)V"
        }
    .end annotation

    .prologue
    .line 208
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 209
    iput p3, p0, Llkt;->g:F

    .line 210
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Llkt;
    .locals 1

    .prologue
    .line 187
    const-class v0, Llkt;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Llkt;

    return-object v0
.end method

.method public static values()[Llkt;
    .locals 1

    .prologue
    .line 187
    sget-object v0, Llkt;->h:[Llkt;

    invoke-virtual {v0}, [Llkt;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Llkt;

    return-object v0
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 216
    sget-object v0, Llkt;->a:Llkt;

    if-eq p0, v0, :cond_0

    sget-object v0, Llkt;->b:Llkt;

    if-eq p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
