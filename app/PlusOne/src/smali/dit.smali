.class public final Ldit;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Llze;",
        "Llzf;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lduj;

.field private final b:Landroid/content/Context;

.field private final p:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lizu;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private q:Lizu;

.field private r:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILduj;Ljava/util/Map;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Lduj;",
            "Ljava/util/Map",
            "<",
            "Lizu;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 47
    const-string v3, "createmediabundle"

    new-instance v4, Llze;

    invoke-direct {v4}, Llze;-><init>()V

    new-instance v5, Llzf;

    invoke-direct {v5}, Llzf;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 49
    iput-object p3, p0, Ldit;->a:Lduj;

    .line 50
    iput-object p4, p0, Ldit;->p:Ljava/util/Map;

    .line 51
    iput-object p1, p0, Ldit;->b:Landroid/content/Context;

    .line 52
    return-void
.end method


# virtual methods
.method protected a(Llze;)V
    .locals 7

    .prologue
    .line 68
    new-instance v0, Lnct;

    invoke-direct {v0}, Lnct;-><init>()V

    iput-object v0, p1, Llze;->a:Lnct;

    .line 69
    iget-object v2, p1, Llze;->a:Lnct;

    iget-object v0, p0, Ldit;->a:Lduj;

    iget-object v0, v0, Lduj;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    new-instance v4, Lncz;

    invoke-direct {v4}, Lncz;-><init>()V

    new-array v0, v3, [Lncy;

    iput-object v0, v4, Lncz;->a:[Lncy;

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v0, v4, Lncz;->a:[Lncy;

    new-instance v5, Lncy;

    invoke-direct {v5}, Lncy;-><init>()V

    aput-object v5, v0, v1

    iget-object v0, v4, Lncz;->a:[Lncy;

    aget-object v0, v0, v1

    new-instance v5, Lncx;

    invoke-direct {v5}, Lncx;-><init>()V

    iput-object v5, v0, Lncy;->b:Lncx;

    iget-object v0, v4, Lncz;->a:[Lncy;

    aget-object v0, v0, v1

    iget-object v5, v0, Lncy;->b:Lncx;

    iget-object v0, p0, Ldit;->p:Ljava/util/Map;

    iget-object v6, p0, Ldit;->a:Lduj;

    iget-object v6, v6, Lduj;->a:Ljava/util/List;

    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v5, Lncx;->a:Ljava/lang/String;

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iput-object v4, v2, Lnct;->a:Lncz;

    .line 70
    iget-object v0, p1, Llze;->a:Lnct;

    iget-object v0, v0, Lnct;->a:Lncz;

    new-instance v1, Lnbi;

    invoke-direct {v1}, Lnbi;-><init>()V

    iput-object v1, v0, Lncz;->b:Lnbi;

    .line 71
    iget-object v0, p1, Llze;->a:Lnct;

    iget-object v0, v0, Lnct;->a:Lncz;

    iget-object v0, v0, Lncz;->b:Lnbi;

    new-instance v1, Lnbu;

    invoke-direct {v1}, Lnbu;-><init>()V

    iput-object v1, v0, Lnbi;->b:Lnbu;

    .line 72
    iget-object v0, p1, Llze;->a:Lnct;

    iget-object v0, v0, Lnct;->a:Lncz;

    iget-object v0, v0, Lncz;->b:Lnbi;

    iget-object v0, v0, Lnbi;->b:Lnbu;

    new-instance v1, Lopf;

    invoke-direct {v1}, Lopf;-><init>()V

    iput-object v1, v0, Lnbu;->a:Lopf;

    .line 73
    iget-object v0, p1, Llze;->a:Lnct;

    iget-object v0, v0, Lnct;->a:Lncz;

    iget-object v0, v0, Lncz;->b:Lnbi;

    iget-object v0, v0, Lnbi;->b:Lnbu;

    iget-object v0, v0, Lnbu;->a:Lopf;

    iget-object v1, p0, Ldit;->a:Lduj;

    iget v1, v1, Lduj;->b:I

    iput v1, v0, Lopf;->b:I

    .line 74
    iget-object v0, p1, Llze;->a:Lnct;

    new-instance v1, Lncw;

    invoke-direct {v1}, Lncw;-><init>()V

    iput-object v1, v0, Lnct;->b:Lncw;

    .line 75
    iget-object v0, p1, Llze;->a:Lnct;

    iget-object v0, v0, Lnct;->b:Lncw;

    const/4 v1, 0x1

    iput v1, v0, Lncw;->a:I

    .line 76
    return-void
.end method

.method protected a(Llzf;)V
    .locals 8

    .prologue
    const/16 v5, 0x9

    const/4 v4, 0x0

    .line 80
    iget-object v0, p1, Llzf;->a:Lncu;

    if-nez v0, :cond_0

    .line 101
    :goto_0
    return-void

    .line 83
    :cond_0
    iget-object v0, p1, Llzf;->a:Lncu;

    iget-object v0, v0, Lncu;->a:Lncv;

    iget-object v1, v0, Lncv;->b:Lnww;

    .line 84
    iget-object v0, p1, Llzf;->a:Lncu;

    iget-object v0, v0, Lncu;->a:Lncv;

    iget-object v0, v0, Lncv;->d:Ljava/lang/Boolean;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 85
    iget-object v0, p1, Llzf;->a:Lncu;

    iget-object v0, v0, Lncu;->a:Lncv;

    iget-object v0, v0, Lncv;->c:Lnzx;

    sget-object v2, Lnzu;->a:Loxr;

    .line 86
    invoke-virtual {v0, v2}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnzu;

    .line 87
    iget-object v0, v0, Lnzu;->b:Lnym;

    .line 88
    invoke-static {v0}, Ljvd;->b(Lnym;)Ljac;

    move-result-object v0

    .line 89
    iget-object v2, p0, Ldit;->b:Landroid/content/Context;

    iget-object v3, p1, Llzf;->a:Lncu;

    iget-object v3, v3, Lncu;->a:Lncv;

    iget-object v3, v3, Lncv;->c:Lnzx;

    iget-object v3, v3, Lnzx;->b:Ljava/lang/String;

    iget-object v1, v1, Lnww;->a:Ljava/lang/String;

    invoke-static {v2, v3, v1, v0}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v0

    iput-object v0, p0, Ldit;->q:Lizu;

    .line 91
    const/4 v0, 0x1

    new-array v3, v0, [Lnzx;

    .line 92
    iget-object v0, p1, Llzf;->a:Lncu;

    iget-object v0, v0, Lncu;->a:Lncv;

    iget-object v0, v0, Lncv;->c:Lnzx;

    aput-object v0, v3, v4

    .line 95
    iget-object v0, p0, Ldit;->b:Landroid/content/Context;

    iget v1, p0, Ldit;->c:I

    new-array v2, v4, [Ljava/lang/String;

    .line 96
    invoke-static {v5, v2}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 95
    invoke-static {v0, v1, v2}, Ljvj;->a(Landroid/content/Context;ILjava/lang/String;)V

    .line 97
    iget-object v0, p0, Ldit;->b:Landroid/content/Context;

    iget v1, p0, Ldit;->c:I

    new-array v2, v4, [Ljava/lang/String;

    .line 98
    invoke-static {v5, v2}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v6, 0x0

    move v5, v4

    move v7, v4

    .line 97
    invoke-static/range {v0 .. v7}, Ljvj;->a(Landroid/content/Context;ILjava/lang/String;[Lnzx;ZZLjava/lang/String;Z)V

    .line 100
    :cond_1
    iget-object v0, p1, Llzf;->a:Lncu;

    iget-object v0, v0, Lncu;->a:Lncv;

    iget-object v0, v0, Lncv;->e:Ljava/lang/String;

    iput-object v0, p0, Ldit;->r:Ljava/lang/String;

    goto :goto_0
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 33
    check-cast p1, Llze;

    invoke-virtual {p0, p1}, Ldit;->a(Llze;)V

    return-void
.end method

.method public b()Lizu;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Ldit;->q:Lizu;

    return-object v0
.end method

.method protected synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 33
    check-cast p1, Llzf;

    invoke-virtual {p0, p1}, Ldit;->a(Llzf;)V

    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Ldit;->r:Ljava/lang/String;

    return-object v0
.end method
