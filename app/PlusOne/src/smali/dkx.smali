.class public final Ldkx;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmga;",
        "Lmgb;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private b:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Lkfo;I)V
    .locals 7

    .prologue
    .line 28
    const-string v4, "loadpeopleviewnotification"

    new-instance v5, Lmga;

    invoke-direct {v5}, Lmga;-><init>()V

    new-instance v6, Lmgb;

    invoke-direct {v6}, Lmgb;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v6}, Leuh;-><init>(Landroid/content/Context;Lkfo;ILjava/lang/String;Loxu;Loxu;)V

    .line 30
    return-void
.end method


# virtual methods
.method protected a(Lmga;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 34
    new-instance v0, Lnqy;

    invoke-direct {v0}, Lnqy;-><init>()V

    .line 35
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lnqy;->a:Ljava/lang/Boolean;

    .line 36
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lnqy;->b:Ljava/lang/Boolean;

    .line 38
    new-instance v1, Lnqx;

    invoke-direct {v1}, Lnqx;-><init>()V

    .line 39
    const/16 v2, 0xf

    iput v2, v1, Lnqx;->b:I

    .line 40
    iput-object v0, v1, Lnqx;->g:Lnqy;

    .line 42
    new-instance v0, Lnra;

    invoke-direct {v0}, Lnra;-><init>()V

    iput-object v0, p1, Lmga;->a:Lnra;

    .line 43
    iget-object v0, p1, Lmga;->a:Lnra;

    .line 44
    new-array v2, v3, [Lnqx;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    iput-object v2, v0, Lnra;->a:[Lnqx;

    .line 45
    return-void
.end method

.method protected a(Lmgb;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 49
    iget-object v1, p1, Lmgb;->a:Lnrk;

    if-eqz v1, :cond_2

    iget-object v1, p1, Lmgb;->a:Lnrk;

    iget-object v1, v1, Lnrk;->a:[Lnri;

    if-eqz v1, :cond_2

    iget-object v1, p1, Lmgb;->a:Lnrk;

    iget-object v1, v1, Lnrk;->a:[Lnri;

    array-length v1, v1

    if-eqz v1, :cond_2

    iget-object v1, p1, Lmgb;->a:Lnrk;

    iget-object v1, v1, Lnrk;->a:[Lnri;

    aget-object v1, v1, v0

    iget-object v1, v1, Lnri;->e:Lnrm;

    if-eqz v1, :cond_2

    .line 53
    iget-object v1, p1, Lmgb;->a:Lnrk;

    iget-object v1, v1, Lnrk;->a:[Lnri;

    aget-object v1, v1, v0

    iget-object v1, v1, Lnri;->e:Lnrm;

    .line 56
    iget-object v2, v1, Lnrm;->b:[Lnrn;

    if-eqz v2, :cond_1

    .line 57
    iget-object v2, v1, Lnrm;->b:[Lnrn;

    array-length v2, v2

    .line 58
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v3, p0, Ldkx;->a:Ljava/util/ArrayList;

    .line 59
    :goto_0
    if-ge v0, v2, :cond_1

    .line 60
    iget-object v3, v1, Lnrm;->b:[Lnrn;

    aget-object v3, v3, v0

    .line 61
    if-eqz v3, :cond_0

    .line 62
    iget-object v4, p0, Ldkx;->a:Ljava/util/ArrayList;

    iget-object v3, v3, Lnrn;->b:Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 59
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 68
    :cond_1
    iget-object v0, v1, Lnrm;->a:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    iput-wide v0, p0, Ldkx;->b:J

    .line 70
    :cond_2
    return-void
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 21
    check-cast p1, Lmga;

    invoke-virtual {p0, p1}, Ldkx;->a(Lmga;)V

    return-void
.end method

.method public b()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 72
    iget-object v0, p0, Ldkx;->a:Ljava/util/ArrayList;

    return-object v0
.end method

.method protected synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 21
    check-cast p1, Lmgb;

    invoke-virtual {p0, p1}, Ldkx;->a(Lmgb;)V

    return-void
.end method

.method public c()J
    .locals 2

    .prologue
    .line 75
    iget-wide v0, p0, Ldkx;->b:J

    return-wide v0
.end method
