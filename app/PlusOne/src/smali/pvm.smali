.class public final Lpvm;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Integer;

.field private b:Ljava/lang/Integer;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1286
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1317
    const/4 v0, 0x0

    .line 1318
    iget-object v1, p0, Lpvm;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 1319
    const/4 v0, 0x1

    iget-object v1, p0, Lpvm;->b:Ljava/lang/Integer;

    .line 1320
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1322
    :cond_0
    iget-object v1, p0, Lpvm;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1323
    const/4 v1, 0x2

    iget-object v2, p0, Lpvm;->c:Ljava/lang/String;

    .line 1324
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1326
    :cond_1
    iget-object v1, p0, Lpvm;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1327
    const/4 v1, 0x3

    iget-object v2, p0, Lpvm;->d:Ljava/lang/String;

    .line 1328
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1330
    :cond_2
    iget-object v1, p0, Lpvm;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 1331
    const/4 v1, 0x4

    iget-object v2, p0, Lpvm;->a:Ljava/lang/Integer;

    .line 1332
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1334
    :cond_3
    iget-object v1, p0, Lpvm;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1335
    iput v0, p0, Lpvm;->ai:I

    .line 1336
    return v0
.end method

.method public a(Loxn;)Lpvm;
    .locals 2

    .prologue
    .line 1344
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1345
    sparse-switch v0, :sswitch_data_0

    .line 1349
    iget-object v1, p0, Lpvm;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1350
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpvm;->ah:Ljava/util/List;

    .line 1353
    :cond_1
    iget-object v1, p0, Lpvm;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1355
    :sswitch_0
    return-object p0

    .line 1360
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lpvm;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 1364
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpvm;->c:Ljava/lang/String;

    goto :goto_0

    .line 1368
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpvm;->d:Ljava/lang/String;

    goto :goto_0

    .line 1372
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lpvm;->a:Ljava/lang/Integer;

    goto :goto_0

    .line 1345
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1299
    iget-object v0, p0, Lpvm;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 1300
    const/4 v0, 0x1

    iget-object v1, p0, Lpvm;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1302
    :cond_0
    iget-object v0, p0, Lpvm;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1303
    const/4 v0, 0x2

    iget-object v1, p0, Lpvm;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1305
    :cond_1
    iget-object v0, p0, Lpvm;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1306
    const/4 v0, 0x3

    iget-object v1, p0, Lpvm;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1308
    :cond_2
    iget-object v0, p0, Lpvm;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 1309
    const/4 v0, 0x4

    iget-object v1, p0, Lpvm;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1311
    :cond_3
    iget-object v0, p0, Lpvm;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1313
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1282
    invoke-virtual {p0, p1}, Lpvm;->a(Loxn;)Lpvm;

    move-result-object v0

    return-object v0
.end method
