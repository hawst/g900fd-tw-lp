.class public final Lmbv;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmpy;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 18952
    invoke-direct {p0}, Loxq;-><init>()V

    .line 18955
    iput-object v0, p0, Lmbv;->apiHeader:Llyr;

    .line 18958
    iput-object v0, p0, Lmbv;->a:Lmpy;

    .line 18952
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 18975
    const/4 v0, 0x0

    .line 18976
    iget-object v1, p0, Lmbv;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 18977
    const/4 v0, 0x1

    iget-object v1, p0, Lmbv;->apiHeader:Llyr;

    .line 18978
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 18980
    :cond_0
    iget-object v1, p0, Lmbv;->a:Lmpy;

    if-eqz v1, :cond_1

    .line 18981
    const/4 v1, 0x2

    iget-object v2, p0, Lmbv;->a:Lmpy;

    .line 18982
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 18984
    :cond_1
    iget-object v1, p0, Lmbv;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 18985
    iput v0, p0, Lmbv;->ai:I

    .line 18986
    return v0
.end method

.method public a(Loxn;)Lmbv;
    .locals 2

    .prologue
    .line 18994
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 18995
    sparse-switch v0, :sswitch_data_0

    .line 18999
    iget-object v1, p0, Lmbv;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 19000
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmbv;->ah:Ljava/util/List;

    .line 19003
    :cond_1
    iget-object v1, p0, Lmbv;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 19005
    :sswitch_0
    return-object p0

    .line 19010
    :sswitch_1
    iget-object v0, p0, Lmbv;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 19011
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmbv;->apiHeader:Llyr;

    .line 19013
    :cond_2
    iget-object v0, p0, Lmbv;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 19017
    :sswitch_2
    iget-object v0, p0, Lmbv;->a:Lmpy;

    if-nez v0, :cond_3

    .line 19018
    new-instance v0, Lmpy;

    invoke-direct {v0}, Lmpy;-><init>()V

    iput-object v0, p0, Lmbv;->a:Lmpy;

    .line 19020
    :cond_3
    iget-object v0, p0, Lmbv;->a:Lmpy;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 18995
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 18963
    iget-object v0, p0, Lmbv;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 18964
    const/4 v0, 0x1

    iget-object v1, p0, Lmbv;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 18966
    :cond_0
    iget-object v0, p0, Lmbv;->a:Lmpy;

    if-eqz v0, :cond_1

    .line 18967
    const/4 v0, 0x2

    iget-object v1, p0, Lmbv;->a:Lmpy;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 18969
    :cond_1
    iget-object v0, p0, Lmbv;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 18971
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 18948
    invoke-virtual {p0, p1}, Lmbv;->a(Loxn;)Lmbv;

    move-result-object v0

    return-object v0
.end method
