.class public Lcxu;
.super Llol;
.source "PG"


# instance fields
.field private N:Lhov;

.field private O:Lfyp;

.field private P:Ldgi;

.field private Q:Ldgr;

.field private final R:Lhkd;

.field private final S:Lhkd;

.field private final T:Lhke;

.field private final U:Ldgv;

.field private V:Ldeo;

.field private W:Z


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 52
    invoke-direct {p0}, Llol;-><init>()V

    .line 55
    new-instance v0, Lhov;

    iget-object v1, p0, Lcxu;->av:Llqm;

    invoke-direct {v0, v1}, Lhov;-><init>(Llqr;)V

    iput-object v0, p0, Lcxu;->N:Lhov;

    .line 61
    new-instance v0, Lcxv;

    invoke-direct {v0, p0}, Lcxv;-><init>(Lcxu;)V

    iput-object v0, p0, Lcxu;->R:Lhkd;

    .line 83
    new-instance v0, Lcxw;

    invoke-direct {v0, p0}, Lcxw;-><init>(Lcxu;)V

    iput-object v0, p0, Lcxu;->S:Lhkd;

    .line 97
    new-instance v0, Lhke;

    iget-object v1, p0, Lcxu;->av:Llqm;

    invoke-direct {v0, v1}, Lhke;-><init>(Llqr;)V

    iget-object v1, p0, Lcxu;->au:Llnh;

    .line 99
    invoke-virtual {v0, v1}, Lhke;->a(Llnh;)Lhke;

    move-result-object v0

    const v1, 0x7f1000bb

    iget-object v2, p0, Lcxu;->R:Lhkd;

    .line 100
    invoke-virtual {v0, v1, v2}, Lhke;->a(ILhkd;)Lhke;

    move-result-object v0

    const v1, 0x7f1000bc

    iget-object v2, p0, Lcxu;->S:Lhkd;

    .line 101
    invoke-virtual {v0, v1, v2}, Lhke;->a(ILhkd;)Lhke;

    move-result-object v0

    iput-object v0, p0, Lcxu;->T:Lhke;

    .line 104
    new-instance v0, Lcxx;

    invoke-direct {v0, p0}, Lcxx;-><init>(Lcxu;)V

    iput-object v0, p0, Lcxu;->U:Ldgv;

    .line 188
    new-instance v0, Ldgn;

    iget-object v1, p0, Lcxu;->av:Llqm;

    sget-object v2, Ldgx;->T:Ldgo;

    iget-object v3, p0, Lcxu;->U:Ldgv;

    invoke-direct {v0, v1, v2, v3}, Ldgn;-><init>(Llqr;Ldgu;Ldgv;)V

    .line 189
    new-instance v0, Ldep;

    iget-object v1, p0, Lcxu;->av:Llqm;

    new-instance v2, Lcya;

    invoke-direct {v2, p0}, Lcya;-><init>(Lcxu;)V

    invoke-direct {v0, v1, v2}, Ldep;-><init>(Llqr;Ldeq;)V

    .line 195
    return-void
.end method

.method static synthetic a(Lcxu;)Llnl;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcxu;->at:Llnl;

    return-object v0
.end method

.method static synthetic a(Lcxu;Landroid/content/Intent;)Lnzi;
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcxu;->b(Landroid/content/Intent;)Lnzi;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcxu;Z)Z
    .locals 0

    .prologue
    .line 52
    iput-boolean p1, p0, Lcxu;->W:Z

    return p1
.end method

.method static synthetic b(Lcxu;)Ldeo;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcxu;->V:Ldeo;

    return-object v0
.end method

.method private b(Landroid/content/Intent;)Lnzi;
    .locals 3

    .prologue
    .line 231
    const-string v0, "edit_info"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    .line 232
    if-eqz v0, :cond_0

    .line 234
    :try_start_0
    new-instance v1, Lnzi;

    invoke-direct {v1}, Lnzi;-><init>()V

    invoke-static {v1, v0}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lnzi;
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    .line 239
    :goto_0
    return-object v0

    .line 235
    :catch_0
    move-exception v0

    .line 236
    const-string v1, "PhotoEditingFragment"

    const-string v2, "Failed to deserialize EditInfo."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 239
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcxu;Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 52
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    const-string v0, "bucket_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "bucket_id"

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljvj;->k(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p0}, Lcxu;->n()Lz;

    move-result-object v2

    invoke-virtual {v2}, Lz;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Ljac;->a:Ljac;

    invoke-static {v2, v1, v3}, Lizu;->a(Landroid/content/Context;Landroid/net/Uri;Ljac;)Lizu;

    move-result-object v1

    iget-object v2, p0, Lcxu;->P:Ldgi;

    invoke-interface {v2, v1, v0}, Ldgi;->a(Lizu;Ljava/lang/String;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lcxu;)Lfyp;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcxu;->O:Lfyp;

    return-object v0
.end method

.method static synthetic d(Lcxu;)Llnl;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcxu;->at:Llnl;

    return-object v0
.end method

.method static synthetic e(Lcxu;)Lhke;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcxu;->T:Lhke;

    return-object v0
.end method

.method static synthetic f(Lcxu;)V
    .locals 4

    .prologue
    .line 52
    iget-object v1, p0, Lcxu;->Q:Ldgr;

    sget-object v2, Ldgx;->T:Ldgo;

    iget-object v0, p0, Lcxu;->V:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    iget-object v3, p0, Lcxu;->V:Ldeo;

    invoke-virtual {v3}, Ldeo;->d()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Lddl;->Q()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Lddl;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v2, v0}, Ldgr;->a(Ldgu;Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic g(Lcxu;)Ldgr;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcxu;->Q:Ldgr;

    return-object v0
.end method


# virtual methods
.method public a(IILandroid/content/Intent;)V
    .locals 0

    .prologue
    .line 208
    invoke-super {p0, p1, p2, p3}, Llol;->a(IILandroid/content/Intent;)V

    .line 214
    return-void
.end method

.method public aO_()V
    .locals 2

    .prologue
    .line 218
    invoke-super {p0}, Llol;->aO_()V

    .line 219
    iget-boolean v0, p0, Lcxu;->W:Z

    if-eqz v0, :cond_0

    .line 220
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcxu;->W:Z

    .line 221
    iget-object v0, p0, Lcxu;->N:Lhov;

    new-instance v1, Lcyb;

    invoke-direct {v1, p0}, Lcyb;-><init>(Lcxu;)V

    invoke-virtual {v0, v1}, Lhov;->a(Ljava/lang/Runnable;)Lhox;

    .line 228
    :cond_0
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 199
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 200
    iget-object v0, p0, Lcxu;->au:Llnh;

    const-class v1, Lfyp;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfyp;

    iput-object v0, p0, Lcxu;->O:Lfyp;

    .line 201
    iget-object v0, p0, Lcxu;->au:Llnh;

    const-class v1, Ldgi;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldgi;

    iput-object v0, p0, Lcxu;->P:Ldgi;

    .line 202
    iget-object v0, p0, Lcxu;->au:Llnh;

    const-class v1, Ldgr;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldgr;

    iput-object v0, p0, Lcxu;->Q:Ldgr;

    .line 203
    iget-object v0, p0, Lcxu;->au:Llnh;

    const-class v1, Ldeo;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldeo;

    iput-object v0, p0, Lcxu;->V:Ldeo;

    .line 204
    return-void
.end method
