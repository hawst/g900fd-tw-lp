.class public final Lktd;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Licv;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Exception;Licu;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 25
    instance-of v1, p1, Lkgf;

    if-eqz v1, :cond_0

    .line 27
    check-cast p1, Lkgf;

    .line 28
    invoke-interface {p2}, Licu;->b()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {p1}, Lkgf;->a()Ljava/lang/String;

    move-result-object v1

    const-string v3, "MODERATOR_TOO_NEW_FOR_OWNER"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const v1, 0x7f0a04dc

    const v0, 0x7f0a04dd

    :goto_0
    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v1, v0}, Licu;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 30
    :cond_0
    return v0

    .line 28
    :cond_1
    const-string v3, "SQUARE_INVITE_TOO_MANY_INVITEES"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const v1, 0x7f0a04de

    const v0, 0x7f0a04df

    goto :goto_0

    :cond_2
    const-string v3, "SOLE_OWNER_LEAVING_SQUARE"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    const v1, 0x7f0a04e0

    const v0, 0x7f0a04e1

    goto :goto_0

    :cond_3
    const-string v3, "SQUARE_INVITE_EMPTY_CIRCLES"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    const v1, 0x7f0a04e2

    const v0, 0x7f0a04e3

    goto :goto_0

    :cond_4
    const-string v3, "SQUARE_INVITE_NOBODY_INVITED"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f0a04e4

    const v0, 0x7f0a04e5

    goto :goto_0
.end method
