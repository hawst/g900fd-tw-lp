.class public final Lljk;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Lljl;

.field private b:[Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private c:I

.field private d:I

.field private synthetic e:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;)V
    .locals 0

    .prologue
    .line 1251
    iput-object p1, p0, Lljk;->e:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 1278
    iget v1, p0, Lljk;->c:I

    .line 1283
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 1284
    iget-object v2, p0, Lljk;->b:[Ljava/util/ArrayList;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 1283
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1286
    :cond_0
    return-void
.end method

.method public a(I)V
    .locals 3

    .prologue
    .line 1260
    if-gtz p1, :cond_0

    .line 1261
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x3d

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Must have at least one view type ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " types reported)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1264
    :cond_0
    iget v0, p0, Lljk;->c:I

    if-ne p1, v0, :cond_1

    .line 1275
    :goto_0
    return-void

    .line 1269
    :cond_1
    new-array v1, p1, [Ljava/util/ArrayList;

    .line 1270
    const/4 v0, 0x0

    :goto_1
    if-ge v0, p1, :cond_2

    .line 1271
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    aput-object v2, v1, v0

    .line 1270
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1273
    :cond_2
    iput p1, p0, Lljk;->c:I

    .line 1274
    iput-object v1, p0, Lljk;->b:[Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 1289
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Llji;

    .line 1290
    iget v1, v0, Llji;->b:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    .line 1307
    :cond_0
    :goto_0
    return-void

    .line 1294
    :cond_1
    iget-object v1, p0, Lljk;->e:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getChildCount()I

    move-result v1

    .line 1295
    iget v2, p0, Lljk;->d:I

    if-le v1, v2, :cond_2

    .line 1296
    iput v1, p0, Lljk;->d:I

    .line 1299
    :cond_2
    iget-object v1, p0, Lljk;->b:[Ljava/util/ArrayList;

    iget v0, v0, Llji;->b:I

    aget-object v0, v1, v0

    .line 1300
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget v2, p0, Lljk;->d:I

    if-ge v1, v2, :cond_3

    .line 1301
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1304
    :cond_3
    iget-object v0, p0, Lljk;->a:Lljl;

    if-eqz v0, :cond_0

    .line 1305
    iget-object v0, p0, Lljk;->a:Lljl;

    invoke-interface {v0, p1}, Lljl;->a(Landroid/view/View;)V

    goto :goto_0
.end method

.method public b(I)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1310
    const/4 v1, -0x1

    if-ne p1, v1, :cond_1

    .line 1322
    :cond_0
    :goto_0
    return-object v0

    .line 1314
    :cond_1
    iget-object v1, p0, Lljk;->b:[Ljava/util/ArrayList;

    aget-object v1, v1, p1

    .line 1315
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1319
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v2, v0, -0x1

    .line 1320
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1321
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_0
.end method
