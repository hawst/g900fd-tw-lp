.class public final Lfoh;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Lmml;

.field private final b:Lmmc;

.field private final c:I

.field private final d:I

.field private final e:I


# direct methods
.method public constructor <init>(Lmmc;II)V
    .locals 2

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    if-nez p1, :cond_0

    .line 35
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot create a story element from a null enrichment"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 37
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lfoh;->a:Lmml;

    .line 38
    iput-object p1, p0, Lfoh;->b:Lmmc;

    .line 39
    iput p2, p0, Lfoh;->c:I

    .line 40
    iput p3, p0, Lfoh;->d:I

    .line 41
    const/4 v0, 0x2

    iput v0, p0, Lfoh;->e:I

    .line 42
    return-void
.end method

.method public constructor <init>(Lmml;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    if-nez p1, :cond_0

    .line 24
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot create a story element from a null moment"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 26
    :cond_0
    iput-object p1, p0, Lfoh;->a:Lmml;

    .line 27
    const/4 v0, 0x0

    iput-object v0, p0, Lfoh;->b:Lmmc;

    .line 28
    iput v1, p0, Lfoh;->c:I

    .line 29
    iput v1, p0, Lfoh;->d:I

    .line 30
    const/4 v0, 0x1

    iput v0, p0, Lfoh;->e:I

    .line 31
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 45
    iget v0, p0, Lfoh;->e:I

    return v0
.end method

.method public b()Lmml;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lfoh;->a:Lmml;

    return-object v0
.end method

.method public c()Lmmc;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lfoh;->b:Lmmc;

    return-object v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 60
    iget v0, p0, Lfoh;->c:I

    return v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lfoh;->d:I

    return v0
.end method

.method public f()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 69
    invoke-virtual {p0}, Lfoh;->a()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 81
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unknown element type"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 71
    :pswitch_0
    iget-object v1, p0, Lfoh;->a:Lmml;

    iget-object v1, v1, Lmml;->f:Ljava/lang/Boolean;

    if-nez v1, :cond_1

    .line 79
    :cond_0
    :goto_0
    return v0

    .line 74
    :cond_1
    iget-object v0, p0, Lfoh;->a:Lmml;

    iget-object v0, v0, Lmml;->f:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0

    .line 76
    :pswitch_1
    iget-object v1, p0, Lfoh;->b:Lmmc;

    iget-object v1, v1, Lmmc;->f:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 79
    iget-object v0, p0, Lfoh;->b:Lmmc;

    iget-object v0, v0, Lmmc;->f:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0

    .line 69
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 86
    invoke-virtual {p0}, Lfoh;->f()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 90
    invoke-virtual {p0}, Lfoh;->a()I

    move-result v1

    if-ne v1, v0, :cond_0

    invoke-virtual {p0}, Lfoh;->b()Lmml;

    move-result-object v1

    iget v1, v1, Lmml;->c:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()Lmms;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 98
    invoke-virtual {p0}, Lfoh;->a()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 126
    :cond_0
    :goto_0
    return-object v0

    .line 100
    :pswitch_0
    invoke-virtual {p0}, Lfoh;->b()Lmml;

    move-result-object v1

    .line 101
    if-eqz v1, :cond_0

    iget-object v2, v1, Lmml;->b:Lmmm;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lmml;->b:Lmmm;

    iget-object v2, v2, Lmmm;->a:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 105
    new-instance v0, Lmms;

    invoke-direct {v0}, Lmms;-><init>()V

    .line 106
    const/4 v2, 0x3

    iput v2, v0, Lmms;->a:I

    .line 107
    iget-object v1, v1, Lmml;->b:Lmmm;

    iput-object v1, v0, Lmms;->d:Lmmm;

    goto :goto_0

    .line 112
    :pswitch_1
    invoke-virtual {p0}, Lfoh;->c()Lmmc;

    move-result-object v1

    .line 113
    if-eqz v1, :cond_0

    iget-object v2, v1, Lmmc;->c:Lmmd;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lmmc;->c:Lmmd;

    iget-object v2, v2, Lmmd;->a:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 117
    new-instance v0, Lmms;

    invoke-direct {v0}, Lmms;-><init>()V

    .line 118
    const/4 v2, 0x4

    iput v2, v0, Lmms;->a:I

    .line 119
    iget-object v1, v1, Lmmc;->c:Lmmd;

    iput-object v1, v0, Lmms;->e:Lmmd;

    .line 120
    invoke-virtual {p0}, Lfoh;->d()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lmms;->b:Ljava/lang/Integer;

    .line 121
    invoke-virtual {p0}, Lfoh;->e()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lmms;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 98
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
