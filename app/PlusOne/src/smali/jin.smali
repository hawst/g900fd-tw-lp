.class public final enum Ljin;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Ljin;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Ljin;

.field public static final enum b:Ljin;

.field private static final synthetic c:[Ljin;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 9
    new-instance v0, Ljin;

    const-string v1, "IMPORTANT"

    invoke-direct {v0, v1, v2}, Ljin;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ljin;->a:Ljin;

    .line 12
    new-instance v0, Ljin;

    const-string v1, "LOW"

    invoke-direct {v0, v1, v3}, Ljin;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ljin;->b:Ljin;

    .line 8
    const/4 v0, 0x2

    new-array v0, v0, [Ljin;

    sget-object v1, Ljin;->a:Ljin;

    aput-object v1, v0, v2

    sget-object v1, Ljin;->b:Ljin;

    aput-object v1, v0, v3

    sput-object v0, Ljin;->c:[Ljin;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 23
    return-void
.end method

.method public static a(I)Ljin;
    .locals 3

    .prologue
    .line 27
    packed-switch p0, :pswitch_data_0

    .line 33
    const-string v0, "FetchCategory"

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x27

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unsupported fetch category: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 34
    sget-object v0, Ljin;->a:Ljin;

    :goto_0
    return-object v0

    .line 29
    :pswitch_0
    sget-object v0, Ljin;->a:Ljin;

    goto :goto_0

    .line 31
    :pswitch_1
    sget-object v0, Ljin;->b:Ljin;

    goto :goto_0

    .line 27
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Ljin;
    .locals 1

    .prologue
    .line 8
    const-class v0, Ljin;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ljin;

    return-object v0
.end method

.method public static values()[Ljin;
    .locals 1

    .prologue
    .line 8
    sget-object v0, Ljin;->c:[Ljin;

    invoke-virtual {v0}, [Ljin;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljin;

    return-object v0
.end method
