.class public final Lnae;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnae;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/Boolean;

.field private d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 757
    const/4 v0, 0x0

    new-array v0, v0, [Lnae;

    sput-object v0, Lnae;->a:[Lnae;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 758
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 784
    const/4 v0, 0x0

    .line 785
    iget-object v1, p0, Lnae;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 786
    const/4 v0, 0x1

    iget-object v1, p0, Lnae;->b:Ljava/lang/String;

    .line 787
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 789
    :cond_0
    iget-object v1, p0, Lnae;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 790
    const/4 v1, 0x2

    iget-object v2, p0, Lnae;->c:Ljava/lang/Boolean;

    .line 791
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 793
    :cond_1
    iget-object v1, p0, Lnae;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 794
    const/4 v1, 0x3

    iget-object v2, p0, Lnae;->d:Ljava/lang/String;

    .line 795
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 797
    :cond_2
    iget-object v1, p0, Lnae;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 798
    iput v0, p0, Lnae;->ai:I

    .line 799
    return v0
.end method

.method public a(Loxn;)Lnae;
    .locals 2

    .prologue
    .line 807
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 808
    sparse-switch v0, :sswitch_data_0

    .line 812
    iget-object v1, p0, Lnae;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 813
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnae;->ah:Ljava/util/List;

    .line 816
    :cond_1
    iget-object v1, p0, Lnae;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 818
    :sswitch_0
    return-object p0

    .line 823
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnae;->b:Ljava/lang/String;

    goto :goto_0

    .line 827
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnae;->c:Ljava/lang/Boolean;

    goto :goto_0

    .line 831
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnae;->d:Ljava/lang/String;

    goto :goto_0

    .line 808
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 769
    iget-object v0, p0, Lnae;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 770
    const/4 v0, 0x1

    iget-object v1, p0, Lnae;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 772
    :cond_0
    iget-object v0, p0, Lnae;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 773
    const/4 v0, 0x2

    iget-object v1, p0, Lnae;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 775
    :cond_1
    iget-object v0, p0, Lnae;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 776
    const/4 v0, 0x3

    iget-object v1, p0, Lnae;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 778
    :cond_2
    iget-object v0, p0, Lnae;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 780
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 754
    invoke-virtual {p0, p1}, Lnae;->a(Loxn;)Lnae;

    move-result-object v0

    return-object v0
.end method
