.class public final Lesu;
.super Leak;
.source "PG"


# instance fields
.field private N:[Lnhm;

.field private O:Litn;

.field private P:Litr;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Leak;-><init>()V

    return-void
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 99
    sget-object v0, Lhmw;->T:Lhmw;

    return-object v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 52
    invoke-virtual {p0}, Lesu;->k()Landroid/os/Bundle;

    move-result-object v1

    .line 53
    const-string v0, "user_device_locations"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lhyv;

    .line 54
    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 55
    :goto_0
    iput-object v0, p0, Lesu;->N:[Lnhm;

    .line 57
    const v0, 0x7f0400ad

    invoke-virtual {p1, v0, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v7

    .line 60
    const-string v0, "show_actions"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    .line 62
    new-instance v0, Litr;

    invoke-virtual {p0}, Lesu;->n()Lz;

    move-result-object v1

    iget-object v2, p0, Lesu;->N:[Lnhm;

    iget-object v3, p0, Lesu;->O:Litn;

    .line 63
    invoke-virtual {p0}, Lesu;->j()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Litr;-><init>(Landroid/content/Context;[Lnhm;Litn;ZLjava/lang/String;)V

    iput-object v0, p0, Lesu;->P:Litr;

    .line 65
    const v0, 0x102000a

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 66
    iget-object v1, p0, Lesu;->P:Litr;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 68
    iget-object v0, p0, Lesu;->N:[Lnhm;

    if-nez v0, :cond_1

    move v0, v6

    .line 69
    :goto_1
    const v1, 0x7f1001e6

    invoke-virtual {v7, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 70
    if-le v0, v4, :cond_2

    .line 71
    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    .line 72
    invoke-virtual {p0}, Lesu;->o()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f11005e

    new-array v4, v4, [Ljava/lang/Object;

    .line 73
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    .line 72
    invoke-virtual {v2, v3, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 74
    const v0, 0x7f100118

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 75
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 80
    :goto_2
    return-object v7

    .line 54
    :cond_0
    new-array v2, v6, [Lnhm;

    .line 55
    invoke-virtual {v0, v2}, Lhyv;->a([Loxu;)[Loxu;

    move-result-object v0

    check-cast v0, [Lnhm;

    goto :goto_0

    .line 68
    :cond_1
    iget-object v0, p0, Lesu;->N:[Lnhm;

    array-length v0, v0

    goto :goto_1

    .line 77
    :cond_2
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2
.end method

.method public a(Litn;)V
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lesu;->O:Litn;

    .line 47
    return-void
.end method

.method public a([Lnhm;)V
    .locals 3

    .prologue
    .line 84
    invoke-virtual {p0}, Lesu;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "user_device_locations"

    new-instance v2, Lhyv;

    invoke-direct {v2, p1}, Lhyv;-><init>([Loxu;)V

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 87
    iget-object v0, p0, Lesu;->P:Litr;

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lesu;->P:Litr;

    invoke-virtual {v0, p1}, Litr;->a([Lnhm;)V

    .line 90
    :cond_0
    return-void
.end method

.method protected d()Z
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x0

    return v0
.end method
