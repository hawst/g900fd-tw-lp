.class public final Lmea;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lpfk;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3481
    invoke-direct {p0}, Loxq;-><init>()V

    .line 3484
    iput-object v0, p0, Lmea;->apiHeader:Llyq;

    .line 3487
    iput-object v0, p0, Lmea;->a:Lpfk;

    .line 3481
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 3504
    const/4 v0, 0x0

    .line 3505
    iget-object v1, p0, Lmea;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 3506
    const/4 v0, 0x1

    iget-object v1, p0, Lmea;->apiHeader:Llyq;

    .line 3507
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3509
    :cond_0
    iget-object v1, p0, Lmea;->a:Lpfk;

    if-eqz v1, :cond_1

    .line 3510
    const/4 v1, 0x2

    iget-object v2, p0, Lmea;->a:Lpfk;

    .line 3511
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3513
    :cond_1
    iget-object v1, p0, Lmea;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3514
    iput v0, p0, Lmea;->ai:I

    .line 3515
    return v0
.end method

.method public a(Loxn;)Lmea;
    .locals 2

    .prologue
    .line 3523
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3524
    sparse-switch v0, :sswitch_data_0

    .line 3528
    iget-object v1, p0, Lmea;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 3529
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmea;->ah:Ljava/util/List;

    .line 3532
    :cond_1
    iget-object v1, p0, Lmea;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3534
    :sswitch_0
    return-object p0

    .line 3539
    :sswitch_1
    iget-object v0, p0, Lmea;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 3540
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmea;->apiHeader:Llyq;

    .line 3542
    :cond_2
    iget-object v0, p0, Lmea;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3546
    :sswitch_2
    iget-object v0, p0, Lmea;->a:Lpfk;

    if-nez v0, :cond_3

    .line 3547
    new-instance v0, Lpfk;

    invoke-direct {v0}, Lpfk;-><init>()V

    iput-object v0, p0, Lmea;->a:Lpfk;

    .line 3549
    :cond_3
    iget-object v0, p0, Lmea;->a:Lpfk;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3524
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 3492
    iget-object v0, p0, Lmea;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 3493
    const/4 v0, 0x1

    iget-object v1, p0, Lmea;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 3495
    :cond_0
    iget-object v0, p0, Lmea;->a:Lpfk;

    if-eqz v0, :cond_1

    .line 3496
    const/4 v0, 0x2

    iget-object v1, p0, Lmea;->a:Lpfk;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 3498
    :cond_1
    iget-object v0, p0, Lmea;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3500
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3477
    invoke-virtual {p0, p1}, Lmea;->a(Loxn;)Lmea;

    move-result-object v0

    return-object v0
.end method
