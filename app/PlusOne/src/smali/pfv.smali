.class public final Lpfv;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lpfy;

.field private b:[Lpfx;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 13
    const/4 v0, 0x0

    iput-object v0, p0, Lpfv;->a:Lpfy;

    .line 16
    sget-object v0, Lpfx;->a:[Lpfx;

    iput-object v0, p0, Lpfv;->b:[Lpfx;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 37
    .line 38
    iget-object v0, p0, Lpfv;->a:Lpfy;

    if-eqz v0, :cond_2

    .line 39
    const/4 v0, 0x1

    iget-object v2, p0, Lpfv;->a:Lpfy;

    .line 40
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 42
    :goto_0
    iget-object v2, p0, Lpfv;->b:[Lpfx;

    if-eqz v2, :cond_1

    .line 43
    iget-object v2, p0, Lpfv;->b:[Lpfx;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 44
    if-eqz v4, :cond_0

    .line 45
    const/4 v5, 0x2

    .line 46
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 43
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 50
    :cond_1
    iget-object v1, p0, Lpfv;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 51
    iput v0, p0, Lpfv;->ai:I

    .line 52
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lpfv;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 60
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 61
    sparse-switch v0, :sswitch_data_0

    .line 65
    iget-object v2, p0, Lpfv;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 66
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lpfv;->ah:Ljava/util/List;

    .line 69
    :cond_1
    iget-object v2, p0, Lpfv;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 71
    :sswitch_0
    return-object p0

    .line 76
    :sswitch_1
    iget-object v0, p0, Lpfv;->a:Lpfy;

    if-nez v0, :cond_2

    .line 77
    new-instance v0, Lpfy;

    invoke-direct {v0}, Lpfy;-><init>()V

    iput-object v0, p0, Lpfv;->a:Lpfy;

    .line 79
    :cond_2
    iget-object v0, p0, Lpfv;->a:Lpfy;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 83
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 84
    iget-object v0, p0, Lpfv;->b:[Lpfx;

    if-nez v0, :cond_4

    move v0, v1

    .line 85
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lpfx;

    .line 86
    iget-object v3, p0, Lpfv;->b:[Lpfx;

    if-eqz v3, :cond_3

    .line 87
    iget-object v3, p0, Lpfv;->b:[Lpfx;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 89
    :cond_3
    iput-object v2, p0, Lpfv;->b:[Lpfx;

    .line 90
    :goto_2
    iget-object v2, p0, Lpfv;->b:[Lpfx;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 91
    iget-object v2, p0, Lpfv;->b:[Lpfx;

    new-instance v3, Lpfx;

    invoke-direct {v3}, Lpfx;-><init>()V

    aput-object v3, v2, v0

    .line 92
    iget-object v2, p0, Lpfv;->b:[Lpfx;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 93
    invoke-virtual {p1}, Loxn;->a()I

    .line 90
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 84
    :cond_4
    iget-object v0, p0, Lpfv;->b:[Lpfx;

    array-length v0, v0

    goto :goto_1

    .line 96
    :cond_5
    iget-object v2, p0, Lpfv;->b:[Lpfx;

    new-instance v3, Lpfx;

    invoke-direct {v3}, Lpfx;-><init>()V

    aput-object v3, v2, v0

    .line 97
    iget-object v2, p0, Lpfv;->b:[Lpfx;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 61
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 21
    iget-object v0, p0, Lpfv;->a:Lpfy;

    if-eqz v0, :cond_0

    .line 22
    const/4 v0, 0x1

    iget-object v1, p0, Lpfv;->a:Lpfy;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 24
    :cond_0
    iget-object v0, p0, Lpfv;->b:[Lpfx;

    if-eqz v0, :cond_2

    .line 25
    iget-object v1, p0, Lpfv;->b:[Lpfx;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 26
    if-eqz v3, :cond_1

    .line 27
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 25
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 31
    :cond_2
    iget-object v0, p0, Lpfv;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 33
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lpfv;->a(Loxn;)Lpfv;

    move-result-object v0

    return-object v0
.end method
