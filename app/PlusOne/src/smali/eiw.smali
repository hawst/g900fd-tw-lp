.class public final Leiw;
.super Legy;
.source "PG"

# interfaces
.implements Lgby;


# static fields
.field private static final aw:Ljava/lang/String;


# instance fields
.field private aA:Lbc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbc",
            "<",
            "Lkxl;",
            ">;"
        }
    .end annotation
.end field

.field private aB:Lbc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbc",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private final aC:Lfhh;

.field private aD:Lfdy;

.field private ax:Ljava/lang/Integer;

.field private ay:Lbc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbc",
            "<[",
            "Lnmv;",
            ">;"
        }
    .end annotation
.end field

.field private az:Lbc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbc",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 57
    const/4 v0, 0x5

    const/4 v1, 0x1

    .line 58
    invoke-static {v0, v1}, Ljvj;->a(II)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Leiw;->aw:Ljava/lang/String;

    .line 57
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Legy;-><init>()V

    .line 61
    new-instance v0, Leix;

    invoke-direct {v0, p0}, Leix;-><init>(Leiw;)V

    iput-object v0, p0, Leiw;->ay:Lbc;

    .line 82
    new-instance v0, Leiy;

    invoke-direct {v0, p0}, Leiy;-><init>(Leiw;)V

    iput-object v0, p0, Leiw;->az:Lbc;

    .line 106
    new-instance v0, Leiz;

    invoke-direct {v0, p0}, Leiz;-><init>(Leiw;)V

    iput-object v0, p0, Leiw;->aA:Lbc;

    .line 131
    new-instance v0, Leja;

    invoke-direct {v0, p0}, Leja;-><init>(Leiw;)V

    iput-object v0, p0, Leiw;->aB:Lbc;

    .line 154
    new-instance v0, Lejb;

    invoke-direct {v0, p0}, Lejb;-><init>(Leiw;)V

    iput-object v0, p0, Leiw;->aC:Lfhh;

    return-void
.end method

.method static synthetic a(Leiw;)Lfdy;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Leiw;->aD:Lfdy;

    return-object v0
.end method

.method private a(ILfib;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 247
    iget-object v0, p0, Leiw;->ax:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Leiw;->ax:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_1

    .line 262
    :cond_0
    :goto_0
    return-void

    .line 251
    :cond_1
    iput-object v3, p0, Leiw;->ax:Ljava/lang/Integer;

    .line 253
    instance-of v0, p2, Lfhx;

    if-eqz v0, :cond_2

    .line 254
    check-cast p2, Lfhx;

    .line 255
    invoke-virtual {p2}, Lfhx;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 261
    :cond_2
    invoke-virtual {p0}, Leiw;->w()Lbb;

    move-result-object v0

    const/16 v1, 0x8

    iget-object v2, p0, Leiw;->aB:Lbc;

    invoke-virtual {v0, v1, v3, v2}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    goto :goto_0
.end method

.method static synthetic a(Leiw;ILfib;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Leiw;->a(ILfib;)V

    return-void
.end method

.method static synthetic ai()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Leiw;->aw:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Leiw;)Llnl;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Leiw;->at:Llnl;

    return-object v0
.end method


# virtual methods
.method protected U()Z
    .locals 1

    .prologue
    .line 300
    const/4 v0, 0x0

    return v0
.end method

.method protected X()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 225
    invoke-super {p0}, Legy;->X()V

    .line 227
    invoke-virtual {p0}, Leiw;->w()Lbb;

    move-result-object v0

    .line 228
    const/4 v1, 0x5

    iget-object v2, p0, Leiw;->ay:Lbc;

    invoke-virtual {v0, v1, v4, v2}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 229
    const/4 v1, 0x6

    iget-object v2, p0, Leiw;->az:Lbc;

    invoke-virtual {v0, v1, v4, v2}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 230
    const/4 v1, 0x7

    iget-object v2, p0, Leiw;->aA:Lbc;

    invoke-virtual {v0, v1, v4, v2}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 231
    iget-object v0, p0, Leiw;->at:Llnl;

    iget-object v1, p0, Leiw;->R:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    iget-object v2, p0, Leiw;->O:Ljava/lang/String;

    const/4 v3, 0x1

    sget-object v5, Leiw;->aw:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leiw;->ax:Ljava/lang/Integer;

    .line 233
    iget-object v0, p0, Leiw;->aD:Lfdy;

    invoke-virtual {v0, v4}, Lfdy;->b(Landroid/database/Cursor;)V

    .line 234
    iget-object v0, p0, Leiw;->aD:Lfdy;

    invoke-virtual {v0, v4}, Lfdy;->c(Landroid/database/Cursor;)V

    .line 235
    iget-object v0, p0, Leiw;->aD:Lfdy;

    invoke-virtual {v0, v4}, Lfdy;->d(Landroid/database/Cursor;)V

    .line 236
    return-void
.end method

.method protected a(Landroid/content/Context;Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;Llcr;ILfdn;Lfdp;Levp;Llci;)Lfdj;
    .locals 8

    .prologue
    .line 203
    new-instance v0, Lfdy;

    iget-object v1, p0, Leiw;->R:Lhee;

    .line 204
    invoke-interface {v1}, Lhee;->d()I

    move-result v4

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p8

    invoke-direct/range {v0 .. v7}, Lfdy;-><init>(Landroid/content/Context;Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;Llcr;ILfdn;Lfdp;Llci;)V

    iput-object v0, p0, Leiw;->aD:Lfdy;

    .line 207
    iget-object v0, p0, Leiw;->aD:Lfdy;

    invoke-virtual {v0, p0}, Lfdy;->a(Lgby;)V

    .line 208
    iget-object v0, p0, Leiw;->aD:Lfdy;

    iget v1, p0, Leiw;->N:I

    invoke-virtual {v0, v1}, Lfdy;->b(I)V

    .line 209
    iget-object v0, p0, Leiw;->aD:Lfdy;

    return-object v0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 166
    invoke-super {p0, p1}, Legy;->a(Landroid/os/Bundle;)V

    .line 168
    invoke-virtual {p0}, Leiw;->w()Lbb;

    move-result-object v0

    .line 169
    const/4 v1, 0x5

    iget-object v2, p0, Leiw;->ay:Lbc;

    invoke-virtual {v0, v1, v3, v2}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 170
    const/4 v1, 0x6

    iget-object v2, p0, Leiw;->az:Lbc;

    invoke-virtual {v0, v1, v3, v2}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 171
    const/4 v1, 0x7

    iget-object v2, p0, Leiw;->aA:Lbc;

    invoke-virtual {v0, v1, v3, v2}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 173
    iget-object v1, p0, Leiw;->O:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 174
    const/16 v1, 0x8

    iget-object v2, p0, Leiw;->aB:Lbc;

    invoke-virtual {v0, v1, v3, v2}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 176
    :cond_0
    return-void
.end method

.method public aO_()V
    .locals 2

    .prologue
    .line 186
    invoke-super {p0}, Legy;->aO_()V

    .line 187
    iget-object v0, p0, Leiw;->aC:Lfhh;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(Lfhh;)V

    .line 189
    iget-object v0, p0, Leiw;->ax:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 190
    iget-object v0, p0, Leiw;->ax:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 191
    iget-object v0, p0, Leiw;->ax:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(I)Lfib;

    move-result-object v0

    .line 192
    iget-object v1, p0, Leiw;->ax:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, v0}, Leiw;->a(ILfib;)V

    .line 195
    :cond_0
    return-void
.end method

.method public ad()V
    .locals 3

    .prologue
    .line 266
    iget-object v0, p0, Leiw;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    .line 267
    invoke-virtual {p0}, Leiw;->n()Lz;

    move-result-object v1

    iget-object v2, p0, Leiw;->O:Ljava/lang/String;

    invoke-static {v1, v0, v2}, Leyq;->d(Landroid/content/Context;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 268
    invoke-virtual {p0, v0}, Leiw;->a(Landroid/content/Intent;)V

    .line 269
    return-void
.end method

.method public ae()V
    .locals 3

    .prologue
    .line 273
    iget-object v0, p0, Leiw;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    .line 275
    invoke-virtual {p0}, Leiw;->n()Lz;

    move-result-object v1

    iget-object v2, p0, Leiw;->O:Ljava/lang/String;

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/plus/squares/legacyimpl/HostSquareSearchActivity;->a(Landroid/content/Context;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 276
    invoke-virtual {p0, v0}, Leiw;->a(Landroid/content/Intent;)V

    .line 277
    return-void
.end method

.method public af()V
    .locals 3

    .prologue
    .line 281
    iget-object v0, p0, Leiw;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    .line 282
    invoke-virtual {p0}, Leiw;->n()Lz;

    move-result-object v1

    iget-object v2, p0, Leiw;->O:Ljava/lang/String;

    invoke-static {v1, v0, v2}, Leyq;->j(Landroid/content/Context;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 283
    invoke-virtual {p0, v0}, Leiw;->a(Landroid/content/Intent;)V

    .line 284
    return-void
.end method

.method public ag()V
    .locals 2

    .prologue
    .line 288
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Leiw;->c(I)V

    .line 289
    iget-object v0, p0, Leiw;->aD:Lfdy;

    iget v1, p0, Leiw;->N:I

    invoke-virtual {v0, v1}, Lfdy;->b(I)V

    .line 290
    return-void
.end method

.method public ah()V
    .locals 2

    .prologue
    .line 294
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Leiw;->c(I)V

    .line 295
    iget-object v0, p0, Leiw;->aD:Lfdy;

    iget v1, p0, Leiw;->N:I

    invoke-virtual {v0, v1}, Lfdy;->b(I)V

    .line 296
    return-void
.end method

.method public b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 219
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Leiw;->O:Ljava/lang/String;

    .line 220
    const/4 v0, 0x1

    return v0

    .line 219
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public z()V
    .locals 1

    .prologue
    .line 180
    invoke-super {p0}, Legy;->z()V

    .line 181
    iget-object v0, p0, Leiw;->aC:Lfhh;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(Lfhh;)V

    .line 182
    return-void
.end method
