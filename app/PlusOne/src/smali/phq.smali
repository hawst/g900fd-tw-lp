.class public final Lphq;
.super Loxq;
.source "PG"


# static fields
.field public static final a:Loxr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Loxr",
            "<",
            "Lphq;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:[Lphs;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 9
    const v0, 0x4a8a442

    new-instance v1, Lphr;

    invoke-direct {v1}, Lphr;-><init>()V

    .line 14
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    move-result-object v0

    sput-object v0, Lphq;->a:Loxr;

    .line 13
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 155
    sget-object v0, Lphs;->a:[Lphs;

    iput-object v0, p0, Lphq;->b:[Lphs;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 173
    .line 174
    iget-object v1, p0, Lphq;->b:[Lphs;

    if-eqz v1, :cond_1

    .line 175
    iget-object v2, p0, Lphq;->b:[Lphs;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 176
    if-eqz v4, :cond_0

    .line 177
    const/4 v5, 0x1

    .line 178
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 175
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 182
    :cond_1
    iget-object v1, p0, Lphq;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 183
    iput v0, p0, Lphq;->ai:I

    .line 184
    return v0
.end method

.method public a(Loxn;)Lphq;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 192
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 193
    sparse-switch v0, :sswitch_data_0

    .line 197
    iget-object v2, p0, Lphq;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 198
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lphq;->ah:Ljava/util/List;

    .line 201
    :cond_1
    iget-object v2, p0, Lphq;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 203
    :sswitch_0
    return-object p0

    .line 208
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 209
    iget-object v0, p0, Lphq;->b:[Lphs;

    if-nez v0, :cond_3

    move v0, v1

    .line 210
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lphs;

    .line 211
    iget-object v3, p0, Lphq;->b:[Lphs;

    if-eqz v3, :cond_2

    .line 212
    iget-object v3, p0, Lphq;->b:[Lphs;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 214
    :cond_2
    iput-object v2, p0, Lphq;->b:[Lphs;

    .line 215
    :goto_2
    iget-object v2, p0, Lphq;->b:[Lphs;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 216
    iget-object v2, p0, Lphq;->b:[Lphs;

    new-instance v3, Lphs;

    invoke-direct {v3}, Lphs;-><init>()V

    aput-object v3, v2, v0

    .line 217
    iget-object v2, p0, Lphq;->b:[Lphs;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 218
    invoke-virtual {p1}, Loxn;->a()I

    .line 215
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 209
    :cond_3
    iget-object v0, p0, Lphq;->b:[Lphs;

    array-length v0, v0

    goto :goto_1

    .line 221
    :cond_4
    iget-object v2, p0, Lphq;->b:[Lphs;

    new-instance v3, Lphs;

    invoke-direct {v3}, Lphs;-><init>()V

    aput-object v3, v2, v0

    .line 222
    iget-object v2, p0, Lphq;->b:[Lphs;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 193
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 160
    iget-object v0, p0, Lphq;->b:[Lphs;

    if-eqz v0, :cond_1

    .line 161
    iget-object v1, p0, Lphq;->b:[Lphs;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 162
    if-eqz v3, :cond_0

    .line 163
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 161
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 167
    :cond_1
    iget-object v0, p0, Lphq;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 169
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lphq;->a(Loxn;)Lphq;

    move-result-object v0

    return-object v0
.end method
