.class public final Liug;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Liiy;
.implements Liiz;
.implements Lilo;


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Lilo;

.field private c:Landroid/location/Location;

.field private d:Lilj;

.field private final e:J


# direct methods
.method public constructor <init>(Landroid/content/Context;JLandroid/location/Location;Lilo;)V
    .locals 2

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Liug;->a:Landroid/content/Context;

    .line 45
    iput-wide p2, p0, Liug;->e:J

    .line 46
    iput-object p5, p0, Liug;->b:Lilo;

    .line 47
    iput-object p4, p0, Liug;->c:Landroid/location/Location;

    .line 49
    const-class v0, Lilk;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lilk;

    .line 50
    invoke-interface {v0, p1, p0, p0}, Lilk;->a(Landroid/content/Context;Liiy;Liiz;)Lilj;

    move-result-object v0

    iput-object v0, p0, Liug;->d:Lilj;

    .line 51
    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 95
    iget-object v0, p0, Liug;->b:Lilo;

    if-nez v0, :cond_1

    .line 114
    :cond_0
    :goto_0
    return-void

    .line 102
    :cond_1
    iget-object v0, p0, Liug;->d:Lilj;

    invoke-interface {v0}, Lilj;->c()Landroid/location/Location;

    move-result-object v0

    invoke-virtual {p0, v0}, Liug;->a(Landroid/location/Location;)V

    .line 106
    iget-object v0, p0, Liug;->d:Lilj;

    invoke-interface {v0}, Lilj;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Liug;->a:Landroid/content/Context;

    const-class v1, Lilq;

    .line 108
    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lilq;

    invoke-interface {v0}, Lilq;->a()Lilp;

    move-result-object v0

    iget-wide v2, p0, Liug;->e:J

    .line 109
    invoke-interface {v0, v2, v3}, Lilp;->a(J)Lilp;

    move-result-object v0

    const/16 v1, 0x64

    .line 110
    invoke-interface {v0, v1}, Lilp;->a(I)Lilp;

    move-result-object v0

    .line 112
    iget-object v1, p0, Liug;->d:Lilj;

    invoke-interface {v1, v0, p0}, Lilj;->a(Lilp;Lilo;)V

    goto :goto_0
.end method

.method public a(Landroid/location/Location;)V
    .locals 6

    .prologue
    .line 55
    iget-object v0, p0, Liug;->a:Landroid/content/Context;

    invoke-static {v0}, Liuo;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 81
    :cond_0
    :goto_0
    return-void

    .line 60
    :cond_1
    if-eqz p1, :cond_0

    .line 61
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 66
    const-wide/32 v2, 0x493e0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    .line 73
    iget-object v0, p0, Liug;->c:Landroid/location/Location;

    if-eqz v0, :cond_2

    iget-object v0, p0, Liug;->c:Landroid/location/Location;

    .line 74
    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    cmpl-double v1, v2, v4

    if-nez v1, :cond_4

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    cmpl-double v1, v2, v4

    if-nez v1, :cond_4

    invoke-virtual {v0}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    float-to-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v1

    sub-float/2addr v0, v1

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_4

    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_0

    .line 75
    :cond_2
    iget-object v0, p0, Liug;->b:Lilo;

    if-eqz v0, :cond_3

    .line 76
    iget-object v0, p0, Liug;->b:Lilo;

    invoke-interface {v0, p1}, Lilo;->a(Landroid/location/Location;)V

    .line 79
    :cond_3
    iput-object p1, p0, Liug;->c:Landroid/location/Location;

    goto :goto_0

    .line 74
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a(Liiw;)V
    .locals 0

    .prologue
    .line 118
    invoke-interface {p1}, Liiw;->b()I

    .line 120
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 126
    const/4 v0, 0x0

    iput-object v0, p0, Liug;->c:Landroid/location/Location;

    .line 127
    iget-object v0, p0, Liug;->d:Lilj;

    invoke-interface {v0}, Lilj;->d()V

    .line 128
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 150
    iput-object v1, p0, Liug;->b:Lilo;

    .line 151
    iget-object v0, p0, Liug;->d:Lilj;

    invoke-interface {v0}, Lilj;->e()V

    .line 152
    iput-object v1, p0, Liug;->b:Lilo;

    .line 153
    return-void
.end method
