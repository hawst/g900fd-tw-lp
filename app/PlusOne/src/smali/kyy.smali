.class final Lkyy;
.super Llde;
.source "PG"


# instance fields
.field private synthetic e:Lkyx;


# direct methods
.method public constructor <init>(Lkyx;Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lkyy;->e:Lkyx;

    .line 70
    invoke-direct {p0, p2, p3, p4}, Llde;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 71
    return-void
.end method


# virtual methods
.method protected a(Landroid/content/Context;II)I
    .locals 12

    .prologue
    const/4 v0, 0x0

    .line 85
    sget-object v1, Lkyy;->a:Llct;

    iget v1, v1, Llct;->m:I

    .line 86
    sget-object v1, Lkyy;->a:Llct;

    iget v1, v1, Llct;->m:I

    mul-int/lit8 v1, v1, 0x2

    sub-int v2, p3, v1

    .line 88
    iget-object v1, p0, Lkyy;->e:Lkyx;

    iget-object v1, v1, Lkyx;->y:Lkzz;

    invoke-virtual {v1}, Lkzz;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f0a0457

    .line 90
    :goto_0
    iget-object v11, p0, Lkyy;->e:Lkyx;

    sget-object v3, Lkyy;->a:Llct;

    iget-object v4, v3, Llct;->aa:Landroid/graphics/Bitmap;

    iget-object v3, p0, Lkyy;->e:Lkyx;

    .line 91
    invoke-static {v3}, Lkyx;->a(Lkyx;)Landroid/graphics/Rect;

    move-result-object v5

    sget-object v3, Lkyy;->a:Llct;

    iget v6, v3, Llct;->n:I

    .line 92
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    iget-object v1, p0, Lkyy;->e:Lkyx;

    .line 93
    invoke-static {v1}, Lkyx;->b(Lkyx;)Landroid/graphics/Point;

    move-result-object v8

    const/16 v1, 0xb

    .line 94
    invoke-static {p1, v1}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v9

    const/4 v10, 0x1

    move v1, v0

    move v3, v0

    .line 90
    invoke-static/range {v0 .. v10}, Llib;->a(IIIILandroid/graphics/Bitmap;Landroid/graphics/Rect;ILjava/lang/CharSequence;Landroid/graphics/Point;Landroid/text/TextPaint;Z)Landroid/text/StaticLayout;

    move-result-object v0

    invoke-static {v11, v0}, Lkyx;->a(Lkyx;Landroid/text/StaticLayout;)Landroid/text/StaticLayout;

    .line 96
    sget-object v0, Lkyy;->a:Llct;

    iget-object v0, v0, Llct;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iget-object v1, p0, Lkyy;->e:Lkyx;

    .line 97
    invoke-static {v1}, Lkyx;->c(Lkyx;)Landroid/text/StaticLayout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    .line 96
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/2addr v0, p2

    return v0

    .line 88
    :cond_0
    const v1, 0x7f0a0a6d

    goto :goto_0
.end method

.method protected a(Landroid/graphics/Canvas;II)I
    .locals 5

    .prologue
    .line 102
    sget-object v0, Lkyy;->a:Llct;

    iget v0, v0, Llct;->m:I

    add-int/2addr v0, p2

    .line 103
    sget-object v1, Lkyy;->a:Llct;

    iget v1, v1, Llct;->m:I

    .line 105
    sget-object v1, Lkyy;->a:Llct;

    iget-object v1, v1, Llct;->H:Landroid/graphics/Rect;

    iget-object v2, p0, Lkyy;->e:Lkyx;

    invoke-static {v2}, Lkyx;->a(Lkyx;)Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 106
    sget-object v1, Lkyy;->a:Llct;

    iget-object v1, v1, Llct;->H:Landroid/graphics/Rect;

    invoke-virtual {v1, v0, p3}, Landroid/graphics/Rect;->offset(II)V

    .line 107
    sget-object v1, Lkyy;->a:Llct;

    iget-object v1, v1, Llct;->aa:Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    sget-object v3, Lkyy;->a:Llct;

    iget-object v3, v3, Llct;->H:Landroid/graphics/Rect;

    sget-object v4, Lkyy;->a:Llct;

    iget-object v4, v4, Llct;->I:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 110
    iget-object v1, p0, Lkyy;->e:Lkyx;

    invoke-static {v1}, Lkyx;->b(Lkyx;)Landroid/graphics/Point;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Point;->x:I

    add-int/2addr v1, v0

    int-to-float v1, v1

    iget-object v2, p0, Lkyy;->e:Lkyx;

    invoke-static {v2}, Lkyx;->b(Lkyx;)Landroid/graphics/Point;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Point;->y:I

    add-int/2addr v2, p3

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 111
    iget-object v1, p0, Lkyy;->e:Lkyx;

    invoke-static {v1}, Lkyx;->c(Lkyx;)Landroid/text/StaticLayout;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 112
    neg-int v0, v0

    iget-object v1, p0, Lkyy;->e:Lkyx;

    invoke-static {v1}, Lkyx;->b(Lkyx;)Landroid/graphics/Point;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Point;->x:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    neg-int v1, p3

    iget-object v2, p0, Lkyy;->e:Lkyx;

    invoke-static {v2}, Lkyx;->b(Lkyx;)Landroid/graphics/Point;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Point;->y:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 114
    sget-object v0, Lkyy;->a:Llct;

    iget-object v0, v0, Llct;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iget-object v1, p0, Lkyy;->e:Lkyx;

    .line 115
    invoke-static {v1}, Lkyx;->c(Lkyx;)Landroid/text/StaticLayout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    .line 114
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/2addr v0, p3

    return v0
.end method

.method protected d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lkyy;->e:Lkyx;

    iget-object v0, v0, Lkyx;->y:Lkzz;

    invoke-virtual {v0}, Lkzz;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 120
    const/4 v0, 0x0

    return-object v0
.end method

.method protected f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x0

    return-object v0
.end method
