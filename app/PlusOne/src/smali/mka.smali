.class public final Lmka;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lntk;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 16117
    invoke-direct {p0}, Loxq;-><init>()V

    .line 16120
    iput-object v0, p0, Lmka;->apiHeader:Llyq;

    .line 16123
    iput-object v0, p0, Lmka;->a:Lntk;

    .line 16117
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 16140
    const/4 v0, 0x0

    .line 16141
    iget-object v1, p0, Lmka;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 16142
    const/4 v0, 0x1

    iget-object v1, p0, Lmka;->apiHeader:Llyq;

    .line 16143
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 16145
    :cond_0
    iget-object v1, p0, Lmka;->a:Lntk;

    if-eqz v1, :cond_1

    .line 16146
    const/4 v1, 0x2

    iget-object v2, p0, Lmka;->a:Lntk;

    .line 16147
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 16149
    :cond_1
    iget-object v1, p0, Lmka;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 16150
    iput v0, p0, Lmka;->ai:I

    .line 16151
    return v0
.end method

.method public a(Loxn;)Lmka;
    .locals 2

    .prologue
    .line 16159
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 16160
    sparse-switch v0, :sswitch_data_0

    .line 16164
    iget-object v1, p0, Lmka;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 16165
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmka;->ah:Ljava/util/List;

    .line 16168
    :cond_1
    iget-object v1, p0, Lmka;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 16170
    :sswitch_0
    return-object p0

    .line 16175
    :sswitch_1
    iget-object v0, p0, Lmka;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 16176
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmka;->apiHeader:Llyq;

    .line 16178
    :cond_2
    iget-object v0, p0, Lmka;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 16182
    :sswitch_2
    iget-object v0, p0, Lmka;->a:Lntk;

    if-nez v0, :cond_3

    .line 16183
    new-instance v0, Lntk;

    invoke-direct {v0}, Lntk;-><init>()V

    iput-object v0, p0, Lmka;->a:Lntk;

    .line 16185
    :cond_3
    iget-object v0, p0, Lmka;->a:Lntk;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 16160
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 16128
    iget-object v0, p0, Lmka;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 16129
    const/4 v0, 0x1

    iget-object v1, p0, Lmka;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 16131
    :cond_0
    iget-object v0, p0, Lmka;->a:Lntk;

    if-eqz v0, :cond_1

    .line 16132
    const/4 v0, 0x2

    iget-object v1, p0, Lmka;->a:Lntk;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 16134
    :cond_1
    iget-object v0, p0, Lmka;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 16136
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 16113
    invoke-virtual {p0, p1}, Lmka;->a(Loxn;)Lmka;

    move-result-object v0

    return-object v0
.end method
