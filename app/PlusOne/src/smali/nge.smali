.class public final Lnge;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lngg;

.field public b:Ljava/lang/Long;

.field private c:Ljava/lang/String;

.field private d:Lngf;

.field private e:Ljava/lang/String;

.field private f:Lngl;

.field private g:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 581
    invoke-direct {p0}, Loxq;-><init>()V

    .line 586
    iput-object v1, p0, Lnge;->d:Lngf;

    .line 589
    sget-object v0, Lngg;->a:[Lngg;

    iput-object v0, p0, Lnge;->a:[Lngg;

    .line 594
    iput-object v1, p0, Lnge;->f:Lngl;

    .line 581
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 634
    .line 635
    iget-object v0, p0, Lnge;->d:Lngf;

    if-eqz v0, :cond_7

    .line 636
    const/4 v0, 0x2

    iget-object v2, p0, Lnge;->d:Lngf;

    .line 637
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 639
    :goto_0
    iget-object v2, p0, Lnge;->a:[Lngg;

    if-eqz v2, :cond_1

    .line 640
    iget-object v2, p0, Lnge;->a:[Lngg;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 641
    if-eqz v4, :cond_0

    .line 642
    const/4 v5, 0x3

    .line 643
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 640
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 647
    :cond_1
    iget-object v1, p0, Lnge;->e:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 648
    const/4 v1, 0x4

    iget-object v2, p0, Lnge;->e:Ljava/lang/String;

    .line 649
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 651
    :cond_2
    iget-object v1, p0, Lnge;->f:Lngl;

    if-eqz v1, :cond_3

    .line 652
    const/4 v1, 0x5

    iget-object v2, p0, Lnge;->f:Lngl;

    .line 653
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 655
    :cond_3
    iget-object v1, p0, Lnge;->b:Ljava/lang/Long;

    if-eqz v1, :cond_4

    .line 656
    const/4 v1, 0x6

    iget-object v2, p0, Lnge;->b:Ljava/lang/Long;

    .line 657
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 659
    :cond_4
    iget-object v1, p0, Lnge;->c:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 660
    const/4 v1, 0x7

    iget-object v2, p0, Lnge;->c:Ljava/lang/String;

    .line 661
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 663
    :cond_5
    iget-object v1, p0, Lnge;->g:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    .line 664
    const/16 v1, 0x8

    iget-object v2, p0, Lnge;->g:Ljava/lang/Boolean;

    .line 665
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 667
    :cond_6
    iget-object v1, p0, Lnge;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 668
    iput v0, p0, Lnge;->ai:I

    .line 669
    return v0

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lnge;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 677
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 678
    sparse-switch v0, :sswitch_data_0

    .line 682
    iget-object v2, p0, Lnge;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 683
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnge;->ah:Ljava/util/List;

    .line 686
    :cond_1
    iget-object v2, p0, Lnge;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 688
    :sswitch_0
    return-object p0

    .line 693
    :sswitch_1
    iget-object v0, p0, Lnge;->d:Lngf;

    if-nez v0, :cond_2

    .line 694
    new-instance v0, Lngf;

    invoke-direct {v0}, Lngf;-><init>()V

    iput-object v0, p0, Lnge;->d:Lngf;

    .line 696
    :cond_2
    iget-object v0, p0, Lnge;->d:Lngf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 700
    :sswitch_2
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 701
    iget-object v0, p0, Lnge;->a:[Lngg;

    if-nez v0, :cond_4

    move v0, v1

    .line 702
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lngg;

    .line 703
    iget-object v3, p0, Lnge;->a:[Lngg;

    if-eqz v3, :cond_3

    .line 704
    iget-object v3, p0, Lnge;->a:[Lngg;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 706
    :cond_3
    iput-object v2, p0, Lnge;->a:[Lngg;

    .line 707
    :goto_2
    iget-object v2, p0, Lnge;->a:[Lngg;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 708
    iget-object v2, p0, Lnge;->a:[Lngg;

    new-instance v3, Lngg;

    invoke-direct {v3}, Lngg;-><init>()V

    aput-object v3, v2, v0

    .line 709
    iget-object v2, p0, Lnge;->a:[Lngg;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 710
    invoke-virtual {p1}, Loxn;->a()I

    .line 707
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 701
    :cond_4
    iget-object v0, p0, Lnge;->a:[Lngg;

    array-length v0, v0

    goto :goto_1

    .line 713
    :cond_5
    iget-object v2, p0, Lnge;->a:[Lngg;

    new-instance v3, Lngg;

    invoke-direct {v3}, Lngg;-><init>()V

    aput-object v3, v2, v0

    .line 714
    iget-object v2, p0, Lnge;->a:[Lngg;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 718
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnge;->e:Ljava/lang/String;

    goto :goto_0

    .line 722
    :sswitch_4
    iget-object v0, p0, Lnge;->f:Lngl;

    if-nez v0, :cond_6

    .line 723
    new-instance v0, Lngl;

    invoke-direct {v0}, Lngl;-><init>()V

    iput-object v0, p0, Lnge;->f:Lngl;

    .line 725
    :cond_6
    iget-object v0, p0, Lnge;->f:Lngl;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 729
    :sswitch_5
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lnge;->b:Ljava/lang/Long;

    goto/16 :goto_0

    .line 733
    :sswitch_6
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnge;->c:Ljava/lang/String;

    goto/16 :goto_0

    .line 737
    :sswitch_7
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnge;->g:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 678
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x30 -> :sswitch_5
        0x3a -> :sswitch_6
        0x40 -> :sswitch_7
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 603
    iget-object v0, p0, Lnge;->d:Lngf;

    if-eqz v0, :cond_0

    .line 604
    const/4 v0, 0x2

    iget-object v1, p0, Lnge;->d:Lngf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 606
    :cond_0
    iget-object v0, p0, Lnge;->a:[Lngg;

    if-eqz v0, :cond_2

    .line 607
    iget-object v1, p0, Lnge;->a:[Lngg;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 608
    if-eqz v3, :cond_1

    .line 609
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 607
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 613
    :cond_2
    iget-object v0, p0, Lnge;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 614
    const/4 v0, 0x4

    iget-object v1, p0, Lnge;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 616
    :cond_3
    iget-object v0, p0, Lnge;->f:Lngl;

    if-eqz v0, :cond_4

    .line 617
    const/4 v0, 0x5

    iget-object v1, p0, Lnge;->f:Lngl;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 619
    :cond_4
    iget-object v0, p0, Lnge;->b:Ljava/lang/Long;

    if-eqz v0, :cond_5

    .line 620
    const/4 v0, 0x6

    iget-object v1, p0, Lnge;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 622
    :cond_5
    iget-object v0, p0, Lnge;->c:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 623
    const/4 v0, 0x7

    iget-object v1, p0, Lnge;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 625
    :cond_6
    iget-object v0, p0, Lnge;->g:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    .line 626
    const/16 v0, 0x8

    iget-object v1, p0, Lnge;->g:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 628
    :cond_7
    iget-object v0, p0, Lnge;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 630
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 577
    invoke-virtual {p0, p1}, Lnge;->a(Loxn;)Lnge;

    move-result-object v0

    return-object v0
.end method
