.class public final Lobs;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Boolean;

.field private b:Lodo;

.field private c:Lodo;

.field private d:Lodo;

.field private e:Lobl;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 347
    invoke-direct {p0}, Loxq;-><init>()V

    .line 352
    iput-object v0, p0, Lobs;->b:Lodo;

    .line 355
    iput-object v0, p0, Lobs;->c:Lodo;

    .line 358
    iput-object v0, p0, Lobs;->d:Lodo;

    .line 361
    iput-object v0, p0, Lobs;->e:Lobl;

    .line 347
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 387
    const/4 v0, 0x0

    .line 388
    iget-object v1, p0, Lobs;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 389
    const/4 v0, 0x5

    iget-object v1, p0, Lobs;->a:Ljava/lang/Boolean;

    .line 390
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 392
    :cond_0
    iget-object v1, p0, Lobs;->b:Lodo;

    if-eqz v1, :cond_1

    .line 393
    const/4 v1, 0x6

    iget-object v2, p0, Lobs;->b:Lodo;

    .line 394
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 396
    :cond_1
    iget-object v1, p0, Lobs;->c:Lodo;

    if-eqz v1, :cond_2

    .line 397
    const/4 v1, 0x7

    iget-object v2, p0, Lobs;->c:Lodo;

    .line 398
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 400
    :cond_2
    iget-object v1, p0, Lobs;->d:Lodo;

    if-eqz v1, :cond_3

    .line 401
    const/16 v1, 0x8

    iget-object v2, p0, Lobs;->d:Lodo;

    .line 402
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 404
    :cond_3
    iget-object v1, p0, Lobs;->e:Lobl;

    if-eqz v1, :cond_4

    .line 405
    const/16 v1, 0x9

    iget-object v2, p0, Lobs;->e:Lobl;

    .line 406
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 408
    :cond_4
    iget-object v1, p0, Lobs;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 409
    iput v0, p0, Lobs;->ai:I

    .line 410
    return v0
.end method

.method public a(Loxn;)Lobs;
    .locals 2

    .prologue
    .line 418
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 419
    sparse-switch v0, :sswitch_data_0

    .line 423
    iget-object v1, p0, Lobs;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 424
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lobs;->ah:Ljava/util/List;

    .line 427
    :cond_1
    iget-object v1, p0, Lobs;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 429
    :sswitch_0
    return-object p0

    .line 434
    :sswitch_1
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lobs;->a:Ljava/lang/Boolean;

    goto :goto_0

    .line 438
    :sswitch_2
    iget-object v0, p0, Lobs;->b:Lodo;

    if-nez v0, :cond_2

    .line 439
    new-instance v0, Lodo;

    invoke-direct {v0}, Lodo;-><init>()V

    iput-object v0, p0, Lobs;->b:Lodo;

    .line 441
    :cond_2
    iget-object v0, p0, Lobs;->b:Lodo;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 445
    :sswitch_3
    iget-object v0, p0, Lobs;->c:Lodo;

    if-nez v0, :cond_3

    .line 446
    new-instance v0, Lodo;

    invoke-direct {v0}, Lodo;-><init>()V

    iput-object v0, p0, Lobs;->c:Lodo;

    .line 448
    :cond_3
    iget-object v0, p0, Lobs;->c:Lodo;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 452
    :sswitch_4
    iget-object v0, p0, Lobs;->d:Lodo;

    if-nez v0, :cond_4

    .line 453
    new-instance v0, Lodo;

    invoke-direct {v0}, Lodo;-><init>()V

    iput-object v0, p0, Lobs;->d:Lodo;

    .line 455
    :cond_4
    iget-object v0, p0, Lobs;->d:Lodo;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 459
    :sswitch_5
    iget-object v0, p0, Lobs;->e:Lobl;

    if-nez v0, :cond_5

    .line 460
    new-instance v0, Lobl;

    invoke-direct {v0}, Lobl;-><init>()V

    iput-object v0, p0, Lobs;->e:Lobl;

    .line 462
    :cond_5
    iget-object v0, p0, Lobs;->e:Lobl;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 419
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x28 -> :sswitch_1
        0x32 -> :sswitch_2
        0x3a -> :sswitch_3
        0x42 -> :sswitch_4
        0x4a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 366
    iget-object v0, p0, Lobs;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 367
    const/4 v0, 0x5

    iget-object v1, p0, Lobs;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 369
    :cond_0
    iget-object v0, p0, Lobs;->b:Lodo;

    if-eqz v0, :cond_1

    .line 370
    const/4 v0, 0x6

    iget-object v1, p0, Lobs;->b:Lodo;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 372
    :cond_1
    iget-object v0, p0, Lobs;->c:Lodo;

    if-eqz v0, :cond_2

    .line 373
    const/4 v0, 0x7

    iget-object v1, p0, Lobs;->c:Lodo;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 375
    :cond_2
    iget-object v0, p0, Lobs;->d:Lodo;

    if-eqz v0, :cond_3

    .line 376
    const/16 v0, 0x8

    iget-object v1, p0, Lobs;->d:Lodo;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 378
    :cond_3
    iget-object v0, p0, Lobs;->e:Lobl;

    if-eqz v0, :cond_4

    .line 379
    const/16 v0, 0x9

    iget-object v1, p0, Lobs;->e:Lobl;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 381
    :cond_4
    iget-object v0, p0, Lobs;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 383
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 343
    invoke-virtual {p0, p1}, Lobs;->a(Loxn;)Lobs;

    move-result-object v0

    return-object v0
.end method
