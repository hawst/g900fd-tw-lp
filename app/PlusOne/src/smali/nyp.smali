.class public final Lnyp;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Double;

.field public b:Ljava/lang/Double;

.field public c:Ljava/lang/Double;

.field public d:Ljava/lang/Double;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3528
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 3551
    const/4 v0, 0x1

    iget-object v1, p0, Lnyp;->a:Ljava/lang/Double;

    .line 3553
    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x8

    add-int/lit8 v0, v0, 0x0

    .line 3554
    const/4 v1, 0x2

    iget-object v2, p0, Lnyp;->b:Ljava/lang/Double;

    .line 3555
    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 3556
    const/4 v1, 0x3

    iget-object v2, p0, Lnyp;->c:Ljava/lang/Double;

    .line 3557
    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 3558
    const/4 v1, 0x4

    iget-object v2, p0, Lnyp;->d:Ljava/lang/Double;

    .line 3559
    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 3560
    iget-object v1, p0, Lnyp;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3561
    iput v0, p0, Lnyp;->ai:I

    .line 3562
    return v0
.end method

.method public a(Loxn;)Lnyp;
    .locals 2

    .prologue
    .line 3570
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3571
    sparse-switch v0, :sswitch_data_0

    .line 3575
    iget-object v1, p0, Lnyp;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 3576
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnyp;->ah:Ljava/util/List;

    .line 3579
    :cond_1
    iget-object v1, p0, Lnyp;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3581
    :sswitch_0
    return-object p0

    .line 3586
    :sswitch_1
    invoke-virtual {p1}, Loxn;->c()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lnyp;->a:Ljava/lang/Double;

    goto :goto_0

    .line 3590
    :sswitch_2
    invoke-virtual {p1}, Loxn;->c()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lnyp;->b:Ljava/lang/Double;

    goto :goto_0

    .line 3594
    :sswitch_3
    invoke-virtual {p1}, Loxn;->c()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lnyp;->c:Ljava/lang/Double;

    goto :goto_0

    .line 3598
    :sswitch_4
    invoke-virtual {p1}, Loxn;->c()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lnyp;->d:Ljava/lang/Double;

    goto :goto_0

    .line 3571
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x11 -> :sswitch_2
        0x19 -> :sswitch_3
        0x21 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 3541
    const/4 v0, 0x1

    iget-object v1, p0, Lnyp;->a:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(ID)V

    .line 3542
    const/4 v0, 0x2

    iget-object v1, p0, Lnyp;->b:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(ID)V

    .line 3543
    const/4 v0, 0x3

    iget-object v1, p0, Lnyp;->c:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(ID)V

    .line 3544
    const/4 v0, 0x4

    iget-object v1, p0, Lnyp;->d:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(ID)V

    .line 3545
    iget-object v0, p0, Lnyp;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3547
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3524
    invoke-virtual {p0, p1}, Lnyp;->a(Loxn;)Lnyp;

    move-result-object v0

    return-object v0
.end method
