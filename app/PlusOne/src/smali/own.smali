.class public final Lown;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:I

.field private c:Ljava/lang/String;

.field private d:Lowi;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 216
    invoke-direct {p0}, Loxq;-><init>()V

    .line 221
    const/high16 v0, -0x80000000

    iput v0, p0, Lown;->b:I

    .line 226
    const/4 v0, 0x0

    iput-object v0, p0, Lown;->d:Lowi;

    .line 216
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 249
    const/4 v0, 0x0

    .line 250
    iget-object v1, p0, Lown;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 251
    const/4 v0, 0x1

    iget-object v1, p0, Lown;->a:Ljava/lang/String;

    .line 252
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 254
    :cond_0
    iget v1, p0, Lown;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_1

    .line 255
    const/4 v1, 0x2

    iget v2, p0, Lown;->b:I

    .line 256
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 258
    :cond_1
    iget-object v1, p0, Lown;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 259
    const/4 v1, 0x3

    iget-object v2, p0, Lown;->c:Ljava/lang/String;

    .line 260
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 262
    :cond_2
    iget-object v1, p0, Lown;->d:Lowi;

    if-eqz v1, :cond_3

    .line 263
    const/4 v1, 0x4

    iget-object v2, p0, Lown;->d:Lowi;

    .line 264
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 266
    :cond_3
    iget-object v1, p0, Lown;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 267
    iput v0, p0, Lown;->ai:I

    .line 268
    return v0
.end method

.method public a(Loxn;)Lown;
    .locals 2

    .prologue
    .line 276
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 277
    sparse-switch v0, :sswitch_data_0

    .line 281
    iget-object v1, p0, Lown;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 282
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lown;->ah:Ljava/util/List;

    .line 285
    :cond_1
    iget-object v1, p0, Lown;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 287
    :sswitch_0
    return-object p0

    .line 292
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lown;->a:Ljava/lang/String;

    goto :goto_0

    .line 296
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 297
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 301
    :cond_2
    iput v0, p0, Lown;->b:I

    goto :goto_0

    .line 303
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lown;->b:I

    goto :goto_0

    .line 308
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lown;->c:Ljava/lang/String;

    goto :goto_0

    .line 312
    :sswitch_4
    iget-object v0, p0, Lown;->d:Lowi;

    if-nez v0, :cond_4

    .line 313
    new-instance v0, Lowi;

    invoke-direct {v0}, Lowi;-><init>()V

    iput-object v0, p0, Lown;->d:Lowi;

    .line 315
    :cond_4
    iget-object v0, p0, Lown;->d:Lowi;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 277
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 231
    iget-object v0, p0, Lown;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 232
    const/4 v0, 0x1

    iget-object v1, p0, Lown;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 234
    :cond_0
    iget v0, p0, Lown;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 235
    const/4 v0, 0x2

    iget v1, p0, Lown;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 237
    :cond_1
    iget-object v0, p0, Lown;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 238
    const/4 v0, 0x3

    iget-object v1, p0, Lown;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 240
    :cond_2
    iget-object v0, p0, Lown;->d:Lowi;

    if-eqz v0, :cond_3

    .line 241
    const/4 v0, 0x4

    iget-object v1, p0, Lown;->d:Lowi;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 243
    :cond_3
    iget-object v0, p0, Lown;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 245
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 212
    invoke-virtual {p0, p1}, Lown;->a(Loxn;)Lown;

    move-result-object v0

    return-object v0
.end method
