.class public final Lpem;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lpei;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 390
    invoke-direct {p0}, Loxq;-><init>()V

    .line 397
    const/4 v0, 0x0

    iput-object v0, p0, Lpem;->b:Lpei;

    .line 390
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 422
    const/4 v0, 0x0

    .line 423
    iget-object v1, p0, Lpem;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 424
    const/4 v0, 0x1

    iget-object v1, p0, Lpem;->a:Ljava/lang/String;

    .line 425
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 427
    :cond_0
    iget-object v1, p0, Lpem;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 428
    const/4 v1, 0x2

    iget-object v2, p0, Lpem;->c:Ljava/lang/String;

    .line 429
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 431
    :cond_1
    iget-object v1, p0, Lpem;->b:Lpei;

    if-eqz v1, :cond_2

    .line 432
    const/4 v1, 0x3

    iget-object v2, p0, Lpem;->b:Lpei;

    .line 433
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 435
    :cond_2
    iget-object v1, p0, Lpem;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 436
    const/4 v1, 0x4

    iget-object v2, p0, Lpem;->d:Ljava/lang/String;

    .line 437
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 439
    :cond_3
    iget-object v1, p0, Lpem;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 440
    iput v0, p0, Lpem;->ai:I

    .line 441
    return v0
.end method

.method public a(Loxn;)Lpem;
    .locals 2

    .prologue
    .line 449
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 450
    sparse-switch v0, :sswitch_data_0

    .line 454
    iget-object v1, p0, Lpem;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 455
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpem;->ah:Ljava/util/List;

    .line 458
    :cond_1
    iget-object v1, p0, Lpem;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 460
    :sswitch_0
    return-object p0

    .line 465
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpem;->a:Ljava/lang/String;

    goto :goto_0

    .line 469
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpem;->c:Ljava/lang/String;

    goto :goto_0

    .line 473
    :sswitch_3
    iget-object v0, p0, Lpem;->b:Lpei;

    if-nez v0, :cond_2

    .line 474
    new-instance v0, Lpei;

    invoke-direct {v0}, Lpei;-><init>()V

    iput-object v0, p0, Lpem;->b:Lpei;

    .line 476
    :cond_2
    iget-object v0, p0, Lpem;->b:Lpei;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 480
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpem;->d:Ljava/lang/String;

    goto :goto_0

    .line 450
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 404
    iget-object v0, p0, Lpem;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 405
    const/4 v0, 0x1

    iget-object v1, p0, Lpem;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 407
    :cond_0
    iget-object v0, p0, Lpem;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 408
    const/4 v0, 0x2

    iget-object v1, p0, Lpem;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 410
    :cond_1
    iget-object v0, p0, Lpem;->b:Lpei;

    if-eqz v0, :cond_2

    .line 411
    const/4 v0, 0x3

    iget-object v1, p0, Lpem;->b:Lpei;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 413
    :cond_2
    iget-object v0, p0, Lpem;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 414
    const/4 v0, 0x4

    iget-object v1, p0, Lpem;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 416
    :cond_3
    iget-object v0, p0, Lpem;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 418
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 386
    invoke-virtual {p0, p1}, Lpem;->a(Loxn;)Lpem;

    move-result-object v0

    return-object v0
.end method
