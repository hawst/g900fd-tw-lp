.class public final Lmaj;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lntm;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 15388
    invoke-direct {p0}, Loxq;-><init>()V

    .line 15391
    iput-object v0, p0, Lmaj;->apiHeader:Llyr;

    .line 15394
    iput-object v0, p0, Lmaj;->a:Lntm;

    .line 15388
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 15411
    const/4 v0, 0x0

    .line 15412
    iget-object v1, p0, Lmaj;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 15413
    const/4 v0, 0x1

    iget-object v1, p0, Lmaj;->apiHeader:Llyr;

    .line 15414
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 15416
    :cond_0
    iget-object v1, p0, Lmaj;->a:Lntm;

    if-eqz v1, :cond_1

    .line 15417
    const/4 v1, 0x2

    iget-object v2, p0, Lmaj;->a:Lntm;

    .line 15418
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15420
    :cond_1
    iget-object v1, p0, Lmaj;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15421
    iput v0, p0, Lmaj;->ai:I

    .line 15422
    return v0
.end method

.method public a(Loxn;)Lmaj;
    .locals 2

    .prologue
    .line 15430
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 15431
    sparse-switch v0, :sswitch_data_0

    .line 15435
    iget-object v1, p0, Lmaj;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 15436
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmaj;->ah:Ljava/util/List;

    .line 15439
    :cond_1
    iget-object v1, p0, Lmaj;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 15441
    :sswitch_0
    return-object p0

    .line 15446
    :sswitch_1
    iget-object v0, p0, Lmaj;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 15447
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmaj;->apiHeader:Llyr;

    .line 15449
    :cond_2
    iget-object v0, p0, Lmaj;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 15453
    :sswitch_2
    iget-object v0, p0, Lmaj;->a:Lntm;

    if-nez v0, :cond_3

    .line 15454
    new-instance v0, Lntm;

    invoke-direct {v0}, Lntm;-><init>()V

    iput-object v0, p0, Lmaj;->a:Lntm;

    .line 15456
    :cond_3
    iget-object v0, p0, Lmaj;->a:Lntm;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 15431
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 15399
    iget-object v0, p0, Lmaj;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 15400
    const/4 v0, 0x1

    iget-object v1, p0, Lmaj;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 15402
    :cond_0
    iget-object v0, p0, Lmaj;->a:Lntm;

    if-eqz v0, :cond_1

    .line 15403
    const/4 v0, 0x2

    iget-object v1, p0, Lmaj;->a:Lntm;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 15405
    :cond_1
    iget-object v0, p0, Lmaj;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 15407
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 15384
    invoke-virtual {p0, p1}, Lmaj;->a(Loxn;)Lmaj;

    move-result-object v0

    return-object v0
.end method
