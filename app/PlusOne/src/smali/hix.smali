.class public Lhix;
.super Llol;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lhiw;
.implements Lhzn;


# instance fields
.field private N:Landroid/widget/ListView;

.field private O:Lhiv;

.field private P:Landroid/widget/BaseAdapter;

.field private Q:Landroid/widget/BaseAdapter;

.field private R:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lhzd;",
            ">;"
        }
    .end annotation
.end field

.field private S:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lhzd;",
            ">;"
        }
    .end annotation
.end field

.field private T:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private U:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private V:Ljava/lang/String;

.field private W:Lhja;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Llol;-><init>()V

    .line 46
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lhix;->T:Ljava/util/List;

    .line 55
    return-void
.end method

.method private U()V
    .locals 4

    .prologue
    .line 229
    iget-object v0, p0, Lhix;->S:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 230
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 231
    iget-object v0, p0, Lhix;->S:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhzd;

    invoke-interface {v0}, Lhzd;->a()Ljava/lang/String;

    move-result-object v0

    .line 232
    iget-object v3, p0, Lhix;->T:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 230
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 234
    :cond_0
    return-void
.end method

.method static synthetic a(Lhix;Landroid/widget/BaseAdapter;)Landroid/widget/BaseAdapter;
    .locals 0

    .prologue
    .line 30
    iput-object p1, p0, Lhix;->P:Landroid/widget/BaseAdapter;

    return-object p1
.end method

.method static synthetic a(Lhix;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lhix;->N:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic b(Lhix;)Landroid/widget/BaseAdapter;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lhix;->P:Landroid/widget/BaseAdapter;

    return-object v0
.end method

.method static synthetic b(Lhix;Landroid/widget/BaseAdapter;)Landroid/widget/BaseAdapter;
    .locals 0

    .prologue
    .line 30
    iput-object p1, p0, Lhix;->Q:Landroid/widget/BaseAdapter;

    return-object p1
.end method

.method static synthetic c(Lhix;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lhix;->e()V

    return-void
.end method

.method static synthetic d(Lhix;)Landroid/widget/BaseAdapter;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lhix;->Q:Landroid/widget/BaseAdapter;

    return-object v0
.end method

.method private e()V
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v7, 0x0

    .line 185
    iget-object v0, p0, Lhix;->U:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 226
    :cond_0
    return-void

    :cond_1
    move v2, v3

    .line 188
    :goto_0
    iget-object v0, p0, Lhix;->U:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 189
    iget-object v0, p0, Lhix;->U:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 190
    iget-object v1, p0, Lhix;->V:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 191
    iget-object v1, p0, Lhix;->T:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 192
    if-ltz v0, :cond_0

    iget-object v1, p0, Lhix;->S:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 193
    iget-object v1, p0, Lhix;->S:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhzd;

    .line 196
    if-eqz v0, :cond_0

    .line 197
    invoke-virtual {p0}, Lhix;->d()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 201
    iget-object v1, p0, Lhix;->N:Landroid/widget/ListView;

    invoke-interface {v0}, Lhzd;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    .line 202
    iget-object v1, p0, Lhix;->N:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->removeFooterView(Landroid/view/View;)Z

    .line 188
    :cond_2
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 205
    :cond_3
    iget-object v1, p0, Lhix;->N:Landroid/widget/ListView;

    invoke-interface {v0}, Lhzd;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/ListView;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_2

    .line 206
    iget-object v1, p0, Lhix;->at:Llnl;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 207
    const v4, 0x7f04002b

    invoke-virtual {v1, v4, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 208
    invoke-interface {v0}, Lhzd;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 209
    invoke-virtual {v4, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 210
    const v1, 0x7f100118

    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 211
    invoke-interface {v0}, Lhzd;->b()I

    move-result v5

    if-eqz v5, :cond_4

    .line 212
    iget-object v5, p0, Lhix;->at:Llnl;

    invoke-virtual {v5}, Llnl;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-interface {v0}, Lhzd;->b()I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 214
    :cond_4
    invoke-interface {v0}, Lhzd;->c()I

    move-result v5

    if-eqz v5, :cond_5

    .line 215
    iget-object v5, p0, Lhix;->at:Llnl;

    .line 216
    invoke-virtual {v5}, Llnl;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-interface {v0}, Lhzd;->c()I

    move-result v0

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 215
    invoke-virtual {v1, v0, v7, v7, v7}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 220
    :cond_5
    iget-object v0, p0, Lhix;->N:Landroid/widget/ListView;

    invoke-virtual {v0, v4, v7, v3}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 221
    const/4 v0, 0x1

    invoke-virtual {v4, v0}, Landroid/view/View;->setClickable(Z)V

    goto :goto_1
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 141
    const v0, 0x7f04002c

    return v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 131
    invoke-virtual {p0}, Lhix;->a()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 133
    if-eqz v1, :cond_0

    .line 134
    const v0, 0x102000a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lhix;->N:Landroid/widget/ListView;

    .line 137
    :cond_0
    return-object v1
.end method

.method public a(ILandroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lhix;->P:Landroid/widget/BaseAdapter;

    if-eqz v0, :cond_0

    .line 163
    iget-object v0, p0, Lhix;->P:Landroid/widget/BaseAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 165
    :cond_0
    return-void
.end method

.method public a(ILjava/util/Collection;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Collection",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 169
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lhix;->a(ILandroid/os/Parcelable;)V

    .line 170
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 11

    .prologue
    const/4 v0, 0x0

    .line 79
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 81
    invoke-virtual {p0}, Lhix;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "ShareouselSlide.ARG_ENSEMBLE_NAME"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lhix;->V:Ljava/lang/String;

    .line 82
    invoke-virtual {p0}, Lhix;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "ShareouselSlide.ARG_SHAREOUSEL_ORDER"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lhix;->U:Ljava/util/ArrayList;

    .line 84
    iget-object v1, p0, Lhix;->V:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 85
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must specify an ensemble name for this slide."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 90
    :cond_0
    const-string v1, "%s.search"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lhix;->V:Ljava/lang/String;

    aput-object v3, v2, v0

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 92
    iget-object v1, p0, Lhix;->R:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v9

    move v7, v0

    .line 93
    :goto_0
    if-ge v7, v9, :cond_3

    .line 94
    iget-object v0, p0, Lhix;->R:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lhzd;

    .line 97
    iget-object v0, p0, Lhix;->V:Ljava/lang/String;

    invoke-interface {v2}, Lhzd;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 98
    new-instance v0, Lhze;

    invoke-interface {v2}, Lhzd;->d()Lhzc;

    move-result-object v1

    invoke-interface {v2}, Lhzd;->a()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lhiy;

    invoke-direct {v3, p0}, Lhiy;-><init>(Lhix;)V

    .line 108
    invoke-virtual {p0}, Lhix;->q()Lae;

    move-result-object v4

    iget-object v5, p0, Lhix;->av:Llqm;

    new-instance v6, Lhil;

    iget-object v10, p0, Lhix;->at:Llnl;

    invoke-direct {v6, v10}, Lhil;-><init>(Landroid/content/Context;)V

    invoke-direct/range {v0 .. v6}, Lhze;-><init>(Lhzc;Ljava/lang/String;Lhzg;Lae;Llqr;Lhyp;)V

    .line 93
    :cond_1
    :goto_1
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    .line 112
    :cond_2
    invoke-interface {v2}, Lhzd;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 113
    new-instance v0, Lhze;

    invoke-interface {v2}, Lhzd;->d()Lhzc;

    move-result-object v1

    invoke-interface {v2}, Lhzd;->a()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lhiz;

    invoke-direct {v3, p0}, Lhiz;-><init>(Lhix;)V

    .line 123
    invoke-virtual {p0}, Lhix;->q()Lae;

    move-result-object v4

    iget-object v5, p0, Lhix;->av:Llqm;

    invoke-direct/range {v0 .. v5}, Lhze;-><init>(Lhzc;Ljava/lang/String;Lhzg;Lae;Llqr;)V

    goto :goto_1

    .line 126
    :cond_3
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 174
    iget-object v0, p0, Lhix;->N:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 175
    iget-object v1, p0, Lhix;->N:Landroid/widget/ListView;

    if-eqz p1, :cond_1

    iget-object v0, p0, Lhix;->Q:Landroid/widget/BaseAdapter;

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 176
    invoke-direct {p0}, Lhix;->e()V

    .line 178
    :cond_0
    return-void

    .line 175
    :cond_1
    iget-object v0, p0, Lhix;->P:Landroid/widget/BaseAdapter;

    goto :goto_0
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 61
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 62
    iget-object v0, p0, Lhix;->au:Llnh;

    const-class v1, Lhzl;

    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhzl;

    .line 63
    instance-of v1, v0, Lhzp;

    if-eqz v1, :cond_0

    .line 64
    check-cast v0, Lhzp;

    invoke-interface {v0, p0}, Lhzp;->a(Lhzn;)V

    .line 67
    :cond_0
    iget-object v0, p0, Lhix;->au:Llnh;

    const-class v1, Lhiv;

    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhiv;

    iput-object v0, p0, Lhix;->O:Lhiv;

    .line 68
    iget-object v0, p0, Lhix;->O:Lhiv;

    if-eqz v0, :cond_1

    .line 69
    iget-object v0, p0, Lhix;->O:Lhiv;

    invoke-virtual {v0, p0}, Lhiv;->a(Lhiw;)V

    .line 72
    :cond_1
    iget-object v0, p0, Lhix;->au:Llnh;

    const-class v1, Lhzd;

    invoke-virtual {v0, v1}, Llnh;->c(Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lhix;->R:Ljava/util/List;

    .line 73
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lhix;->R:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_3

    iget-object v0, p0, Lhix;->R:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhzd;

    invoke-interface {v0}, Lhzd;->a()Ljava/lang/String;

    move-result-object v0

    const-string v4, ".search"

    invoke-virtual {v0, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lhix;->R:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_3
    iput-object v2, p0, Lhix;->S:Ljava/util/List;

    .line 74
    invoke-direct {p0}, Lhix;->U()V

    .line 75
    return-void
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lhix;->O:Lhiv;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhix;->O:Lhiv;

    invoke-virtual {v0}, Lhiv;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 146
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 147
    iget-object v1, p0, Lhix;->U:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 149
    if-ltz v0, :cond_0

    iget-object v1, p0, Lhix;->U:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 150
    invoke-virtual {p0}, Lhix;->r()Lu;

    move-result-object v0

    .line 151
    instance-of v1, v0, Lhja;

    if-eqz v1, :cond_0

    .line 152
    check-cast v0, Lhja;

    iput-object v0, p0, Lhix;->W:Lhja;

    .line 153
    iget-object v0, p0, Lhix;->W:Lhja;

    if-eqz v0, :cond_0

    .line 154
    iget-object v1, p0, Lhix;->W:Lhja;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v1, v0}, Lhja;->a(Ljava/lang/String;)V

    .line 158
    :cond_0
    return-void
.end method
