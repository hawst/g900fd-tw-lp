.class public final Lnmx;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnmx;


# instance fields
.field public b:Lohp;

.field public c:Lohq;

.field public d:Ljava/lang/String;

.field private e:Ljava/lang/Boolean;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 515
    const/4 v0, 0x0

    new-array v0, v0, [Lnmx;

    sput-object v0, Lnmx;->a:[Lnmx;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 516
    invoke-direct {p0}, Loxq;-><init>()V

    .line 519
    iput-object v0, p0, Lnmx;->b:Lohp;

    .line 522
    iput-object v0, p0, Lnmx;->c:Lohq;

    .line 516
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 559
    const/4 v0, 0x0

    .line 560
    iget-object v1, p0, Lnmx;->b:Lohp;

    if-eqz v1, :cond_0

    .line 561
    const/4 v0, 0x1

    iget-object v1, p0, Lnmx;->b:Lohp;

    .line 562
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 564
    :cond_0
    iget-object v1, p0, Lnmx;->c:Lohq;

    if-eqz v1, :cond_1

    .line 565
    const/4 v1, 0x2

    iget-object v2, p0, Lnmx;->c:Lohq;

    .line 566
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 568
    :cond_1
    iget-object v1, p0, Lnmx;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 569
    const/4 v1, 0x3

    iget-object v2, p0, Lnmx;->d:Ljava/lang/String;

    .line 570
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 572
    :cond_2
    iget-object v1, p0, Lnmx;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 573
    const/4 v1, 0x4

    iget-object v2, p0, Lnmx;->e:Ljava/lang/Boolean;

    .line 574
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 576
    :cond_3
    iget-object v1, p0, Lnmx;->f:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 577
    const/4 v1, 0x5

    iget-object v2, p0, Lnmx;->f:Ljava/lang/String;

    .line 578
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 580
    :cond_4
    iget-object v1, p0, Lnmx;->g:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 581
    const/4 v1, 0x6

    iget-object v2, p0, Lnmx;->g:Ljava/lang/String;

    .line 582
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 584
    :cond_5
    iget-object v1, p0, Lnmx;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 585
    iput v0, p0, Lnmx;->ai:I

    .line 586
    return v0
.end method

.method public a(Loxn;)Lnmx;
    .locals 2

    .prologue
    .line 594
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 595
    sparse-switch v0, :sswitch_data_0

    .line 599
    iget-object v1, p0, Lnmx;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 600
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnmx;->ah:Ljava/util/List;

    .line 603
    :cond_1
    iget-object v1, p0, Lnmx;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 605
    :sswitch_0
    return-object p0

    .line 610
    :sswitch_1
    iget-object v0, p0, Lnmx;->b:Lohp;

    if-nez v0, :cond_2

    .line 611
    new-instance v0, Lohp;

    invoke-direct {v0}, Lohp;-><init>()V

    iput-object v0, p0, Lnmx;->b:Lohp;

    .line 613
    :cond_2
    iget-object v0, p0, Lnmx;->b:Lohp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 617
    :sswitch_2
    iget-object v0, p0, Lnmx;->c:Lohq;

    if-nez v0, :cond_3

    .line 618
    new-instance v0, Lohq;

    invoke-direct {v0}, Lohq;-><init>()V

    iput-object v0, p0, Lnmx;->c:Lohq;

    .line 620
    :cond_3
    iget-object v0, p0, Lnmx;->c:Lohq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 624
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnmx;->d:Ljava/lang/String;

    goto :goto_0

    .line 628
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnmx;->e:Ljava/lang/Boolean;

    goto :goto_0

    .line 632
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnmx;->f:Ljava/lang/String;

    goto :goto_0

    .line 636
    :sswitch_6
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnmx;->g:Ljava/lang/String;

    goto :goto_0

    .line 595
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 535
    iget-object v0, p0, Lnmx;->b:Lohp;

    if-eqz v0, :cond_0

    .line 536
    const/4 v0, 0x1

    iget-object v1, p0, Lnmx;->b:Lohp;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 538
    :cond_0
    iget-object v0, p0, Lnmx;->c:Lohq;

    if-eqz v0, :cond_1

    .line 539
    const/4 v0, 0x2

    iget-object v1, p0, Lnmx;->c:Lohq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 541
    :cond_1
    iget-object v0, p0, Lnmx;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 542
    const/4 v0, 0x3

    iget-object v1, p0, Lnmx;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 544
    :cond_2
    iget-object v0, p0, Lnmx;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 545
    const/4 v0, 0x4

    iget-object v1, p0, Lnmx;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 547
    :cond_3
    iget-object v0, p0, Lnmx;->f:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 548
    const/4 v0, 0x5

    iget-object v1, p0, Lnmx;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 550
    :cond_4
    iget-object v0, p0, Lnmx;->g:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 551
    const/4 v0, 0x6

    iget-object v1, p0, Lnmx;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 553
    :cond_5
    iget-object v0, p0, Lnmx;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 555
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 512
    invoke-virtual {p0, p1}, Lnmx;->a(Loxn;)Lnmx;

    move-result-object v0

    return-object v0
.end method
