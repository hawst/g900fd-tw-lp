.class public final Ldub;
.super Lhxz;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lhxz",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private b:Ljava/lang/String;

.field private c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lhxz;-><init>(Landroid/content/Context;)V

    .line 23
    iput-object p3, p0, Ldub;->b:Ljava/lang/String;

    .line 24
    iput p2, p0, Ldub;->c:I

    .line 25
    return-void
.end method


# virtual methods
.method public f()Ljava/lang/Boolean;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 33
    invoke-virtual {p0}, Ldub;->n()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Ldub;->c:I

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->c(Landroid/content/Context;I)Ljava/util/List;

    move-result-object v4

    .line 34
    const/4 v3, 0x0

    move v1, v2

    .line 35
    :goto_0
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 36
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljeb;

    iget-object v0, v0, Ljeb;->a:Ljed;

    iget-object v0, v0, Ljed;->b:Ljava/lang/String;

    iget-object v5, p0, Ldub;->b:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 37
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljeb;

    .line 41
    :goto_1
    if-nez v0, :cond_1

    .line 44
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 49
    :goto_2
    return-object v0

    .line 35
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 48
    :cond_1
    invoke-virtual {p0}, Ldub;->n()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Ldub;->c:I

    iget-object v3, p0, Ldub;->b:Ljava/lang/String;

    .line 47
    invoke-static {v0, v1, v3, v2}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->a(Landroid/content/Context;ILjava/lang/String;Z)Ljava/util/List;

    move-result-object v0

    .line 49
    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v2, 0x1

    :cond_2
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_2

    :cond_3
    move-object v0, v3

    goto :goto_1
.end method

.method public synthetic j()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 16
    invoke-virtual {p0}, Ldub;->f()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
