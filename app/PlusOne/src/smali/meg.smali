.class public final Lmeg;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lntg;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 15631
    invoke-direct {p0}, Loxq;-><init>()V

    .line 15634
    iput-object v0, p0, Lmeg;->apiHeader:Llyq;

    .line 15637
    iput-object v0, p0, Lmeg;->a:Lntg;

    .line 15631
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 15654
    const/4 v0, 0x0

    .line 15655
    iget-object v1, p0, Lmeg;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 15656
    const/4 v0, 0x1

    iget-object v1, p0, Lmeg;->apiHeader:Llyq;

    .line 15657
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 15659
    :cond_0
    iget-object v1, p0, Lmeg;->a:Lntg;

    if-eqz v1, :cond_1

    .line 15660
    const/4 v1, 0x2

    iget-object v2, p0, Lmeg;->a:Lntg;

    .line 15661
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15663
    :cond_1
    iget-object v1, p0, Lmeg;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15664
    iput v0, p0, Lmeg;->ai:I

    .line 15665
    return v0
.end method

.method public a(Loxn;)Lmeg;
    .locals 2

    .prologue
    .line 15673
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 15674
    sparse-switch v0, :sswitch_data_0

    .line 15678
    iget-object v1, p0, Lmeg;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 15679
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmeg;->ah:Ljava/util/List;

    .line 15682
    :cond_1
    iget-object v1, p0, Lmeg;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 15684
    :sswitch_0
    return-object p0

    .line 15689
    :sswitch_1
    iget-object v0, p0, Lmeg;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 15690
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmeg;->apiHeader:Llyq;

    .line 15692
    :cond_2
    iget-object v0, p0, Lmeg;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 15696
    :sswitch_2
    iget-object v0, p0, Lmeg;->a:Lntg;

    if-nez v0, :cond_3

    .line 15697
    new-instance v0, Lntg;

    invoke-direct {v0}, Lntg;-><init>()V

    iput-object v0, p0, Lmeg;->a:Lntg;

    .line 15699
    :cond_3
    iget-object v0, p0, Lmeg;->a:Lntg;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 15674
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 15642
    iget-object v0, p0, Lmeg;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 15643
    const/4 v0, 0x1

    iget-object v1, p0, Lmeg;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 15645
    :cond_0
    iget-object v0, p0, Lmeg;->a:Lntg;

    if-eqz v0, :cond_1

    .line 15646
    const/4 v0, 0x2

    iget-object v1, p0, Lmeg;->a:Lntg;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 15648
    :cond_1
    iget-object v0, p0, Lmeg;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 15650
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 15627
    invoke-virtual {p0, p1}, Lmeg;->a(Loxn;)Lmeg;

    move-result-object v0

    return-object v0
.end method
