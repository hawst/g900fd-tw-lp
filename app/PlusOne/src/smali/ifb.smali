.class public final enum Lifb;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lifb;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lifb;

.field public static final enum b:Lifb;

.field private static final synthetic d:[Lifb;


# instance fields
.field private final c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 52
    new-instance v0, Lifb;

    const-string v1, "NONE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v3, v2}, Lifb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lifb;->a:Lifb;

    .line 53
    new-instance v0, Lifb;

    const-string v1, "PHOTOS"

    const-string v2, "Photos"

    invoke-direct {v0, v1, v4, v2}, Lifb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lifb;->b:Lifb;

    .line 51
    const/4 v0, 0x2

    new-array v0, v0, [Lifb;

    sget-object v1, Lifb;->a:Lifb;

    aput-object v1, v0, v3

    sget-object v1, Lifb;->b:Lifb;

    aput-object v1, v0, v4

    sput-object v0, Lifb;->d:[Lifb;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 58
    iput-object p3, p0, Lifb;->c:Ljava/lang/String;

    .line 59
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lifb;
    .locals 1

    .prologue
    .line 51
    const-class v0, Lifb;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lifb;

    return-object v0
.end method

.method public static values()[Lifb;
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lifb;->d:[Lifb;

    invoke-virtual {v0}, [Lifb;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lifb;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lifb;->c:Ljava/lang/String;

    return-object v0
.end method
