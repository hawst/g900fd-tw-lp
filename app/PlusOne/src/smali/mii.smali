.class public final Lmii;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmze;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1861
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1864
    iput-object v0, p0, Lmii;->apiHeader:Llyq;

    .line 1867
    iput-object v0, p0, Lmii;->a:Lmze;

    .line 1861
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1884
    const/4 v0, 0x0

    .line 1885
    iget-object v1, p0, Lmii;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 1886
    const/4 v0, 0x1

    iget-object v1, p0, Lmii;->apiHeader:Llyq;

    .line 1887
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1889
    :cond_0
    iget-object v1, p0, Lmii;->a:Lmze;

    if-eqz v1, :cond_1

    .line 1890
    const/4 v1, 0x2

    iget-object v2, p0, Lmii;->a:Lmze;

    .line 1891
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1893
    :cond_1
    iget-object v1, p0, Lmii;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1894
    iput v0, p0, Lmii;->ai:I

    .line 1895
    return v0
.end method

.method public a(Loxn;)Lmii;
    .locals 2

    .prologue
    .line 1903
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1904
    sparse-switch v0, :sswitch_data_0

    .line 1908
    iget-object v1, p0, Lmii;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1909
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmii;->ah:Ljava/util/List;

    .line 1912
    :cond_1
    iget-object v1, p0, Lmii;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1914
    :sswitch_0
    return-object p0

    .line 1919
    :sswitch_1
    iget-object v0, p0, Lmii;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 1920
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmii;->apiHeader:Llyq;

    .line 1922
    :cond_2
    iget-object v0, p0, Lmii;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1926
    :sswitch_2
    iget-object v0, p0, Lmii;->a:Lmze;

    if-nez v0, :cond_3

    .line 1927
    new-instance v0, Lmze;

    invoke-direct {v0}, Lmze;-><init>()V

    iput-object v0, p0, Lmii;->a:Lmze;

    .line 1929
    :cond_3
    iget-object v0, p0, Lmii;->a:Lmze;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1904
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1872
    iget-object v0, p0, Lmii;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 1873
    const/4 v0, 0x1

    iget-object v1, p0, Lmii;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1875
    :cond_0
    iget-object v0, p0, Lmii;->a:Lmze;

    if-eqz v0, :cond_1

    .line 1876
    const/4 v0, 0x2

    iget-object v1, p0, Lmii;->a:Lmze;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1878
    :cond_1
    iget-object v0, p0, Lmii;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1880
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1857
    invoke-virtual {p0, p1}, Lmii;->a(Loxn;)Lmii;

    move-result-object v0

    return-object v0
.end method
