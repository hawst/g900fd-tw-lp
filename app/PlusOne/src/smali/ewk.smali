.class public final Lewk;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhtp;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lu;

.field private final c:Liwk;

.field private d:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lu;)V
    .locals 2

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lewk;->a:Landroid/content/Context;

    .line 45
    iput-object p2, p0, Lewk;->b:Lu;

    .line 46
    const-class v0, Lhee;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    iput v0, p0, Lewk;->d:I

    .line 47
    new-instance v0, Liwk;

    iget v1, p0, Lewk;->d:I

    invoke-direct {v0, p1, v1}, Liwk;-><init>(Landroid/content/Context;I)V

    const-class v1, Lixj;

    .line 48
    invoke-virtual {v0, v1}, Liwk;->a(Ljava/lang/Class;)Liwk;

    move-result-object v0

    iput-object v0, p0, Lewk;->c:Liwk;

    .line 49
    const-class v0, Lhke;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    .line 50
    return-void
.end method

.method private a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;ZLandroid/os/Bundle;I)V
    .locals 4

    .prologue
    .line 136
    iget-object v0, p0, Lewk;->a:Landroid/content/Context;

    const-class v1, Lhei;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 137
    const/4 v1, -0x1

    if-eq p6, v1, :cond_0

    .line 138
    invoke-interface {v0, p6}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 141
    :goto_0
    if-eqz p4, :cond_1

    if-nez v0, :cond_1

    .line 142
    const-string v0, "extra_activity_id"

    invoke-static {v0, p2}, Lhmt;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 143
    iget-object v0, p0, Lewk;->a:Landroid/content/Context;

    const-class v2, Lhms;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    iget-object v3, p0, Lewk;->a:Landroid/content/Context;

    invoke-direct {v2, v3, p6}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v3, Lhmv;->Y:Lhmv;

    .line 145
    invoke-virtual {v2, v3}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v2

    .line 146
    invoke-virtual {v2, v1}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    .line 143
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 147
    iget-object v0, p0, Lewk;->a:Landroid/content/Context;

    const v1, 0x7f0a07e6

    .line 148
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lewk;->a:Landroid/content/Context;

    const v2, 0x7f0a07e7

    .line 149
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lewk;->a:Landroid/content/Context;

    const v3, 0x7f0a07e8

    .line 150
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 147
    invoke-static {v0, v1, v2, p1, p5}, Ldzo;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/content/Intent;Landroid/os/Bundle;)Lt;

    move-result-object v0

    .line 151
    iget-object v1, p0, Lewk;->b:Lu;

    invoke-virtual {v1}, Lu;->p()Lae;

    move-result-object v1

    const-string v2, "reshare_activity"

    invoke-virtual {v0, v1, v2}, Lt;->a(Lae;Ljava/lang/String;)V

    .line 161
    :goto_1
    return-void

    .line 138
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 153
    :cond_1
    iget-object v0, p0, Lewk;->c:Liwk;

    invoke-virtual {v0}, Liwk;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 154
    iget-object v0, p0, Lewk;->a:Landroid/content/Context;

    iget-object v1, p0, Lewk;->c:Liwk;

    invoke-virtual {v1}, Liwk;->b()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 155
    :cond_2
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_3

    .line 156
    iget-object v0, p0, Lewk;->a:Landroid/content/Context;

    invoke-virtual {v0, p1, p5}, Landroid/content/Context;->startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V

    goto :goto_1

    .line 158
    :cond_3
    iget-object v0, p0, Lewk;->a:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_1
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;ZLandroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 55
    iget-object v0, p0, Lewk;->a:Landroid/content/Context;

    const-class v1, Lhxp;

    invoke-static {v0, v1}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhxp;

    .line 56
    iget-object v1, p0, Lewk;->a:Landroid/content/Context;

    const-class v2, Lgcs;

    .line 57
    invoke-static {v1, v2}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lgcs;

    .line 58
    if-eqz v1, :cond_1

    iget v2, p0, Lewk;->d:I

    .line 59
    invoke-interface {v1}, Lgcs;->g()Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    .line 61
    :goto_0
    if-eqz v1, :cond_0

    .line 62
    iget v1, p0, Lewk;->d:I

    invoke-interface {v0}, Lhxp;->b()Landroid/content/Intent;

    move-result-object v1

    .line 64
    iget v6, p0, Lewk;->d:I

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v6}, Lewk;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;ZLandroid/os/Bundle;I)V

    .line 67
    :cond_0
    return-void

    .line 59
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;ZZ[BLandroid/os/Bundle;)V
    .locals 9

    .prologue
    const/4 v5, 0x0

    const/4 v7, 0x0

    .line 74
    iget-object v0, p0, Lewk;->a:Landroid/content/Context;

    const-class v1, Lhxp;

    invoke-static {v0, v1}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhxp;

    .line 75
    iget-object v1, p0, Lewk;->a:Landroid/content/Context;

    const-class v2, Lgcs;

    .line 76
    invoke-static {v1, v2}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lgcs;

    .line 77
    if-eqz v1, :cond_0

    iget v2, p0, Lewk;->d:I

    .line 78
    invoke-interface {v1}, Lgcs;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    .line 80
    :goto_0
    if-eqz v1, :cond_1

    .line 81
    iget v1, p0, Lewk;->d:I

    invoke-interface {v0}, Lhxp;->a()Landroid/content/Intent;

    move-result-object v1

    .line 95
    :goto_1
    iget v6, p0, Lewk;->d:I

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p6

    invoke-direct/range {v0 .. v6}, Lewk;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;ZLandroid/os/Bundle;I)V

    .line 96
    return-void

    :cond_0
    move v1, v7

    .line 78
    goto :goto_0

    .line 84
    :cond_1
    iget-object v0, p0, Lewk;->a:Landroid/content/Context;

    iget v1, p0, Lewk;->d:I

    move-object v2, p1

    move v3, p3

    move v4, p4

    move-object v6, p5

    move-object v8, v5

    invoke-static/range {v0 .. v8}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;ZZLhgw;[BZLjava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    goto :goto_1
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;ZZ[BLandroid/os/Bundle;IZLjava/lang/String;)V
    .locals 10

    .prologue
    .line 103
    iget-object v1, p0, Lewk;->a:Landroid/content/Context;

    const-class v2, Lhee;

    invoke-static {v1, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhee;

    .line 105
    invoke-interface {v1}, Lhee;->g()Lhej;

    move-result-object v2

    const-string v3, "domain_name"

    invoke-interface {v2, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 104
    const/16 v3, 0x9

    move/from16 v0, p7

    if-ne v0, v3, :cond_0

    new-instance v6, Lhgw;

    new-instance v2, Lhxc;

    const-string v3, "0"

    const/16 v4, 0x9

    iget-object v5, p0, Lewk;->a:Landroid/content/Context;

    const v7, 0x7f0a04a3

    invoke-virtual {v5, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x0

    invoke-direct {v2, v3, v4, v5, v7}, Lhxc;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    invoke-direct {v6, v2}, Lhgw;-><init>(Lhxc;)V

    .line 106
    :goto_0
    invoke-interface {v1}, Lhee;->d()I

    move-result v2

    .line 107
    iget-object v1, p0, Lewk;->a:Landroid/content/Context;

    move-object v3, p1

    move v4, p3

    move v5, p4

    move-object v7, p5

    move/from16 v8, p8

    move-object/from16 v9, p9

    invoke-static/range {v1 .. v9}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;ZZLhgw;[BZLjava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    move-object v3, p0

    move-object v5, p1

    move-object v6, p2

    move v7, p3

    move-object/from16 v8, p6

    move v9, v2

    .line 117
    invoke-direct/range {v3 .. v9}, Lewk;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;ZLandroid/os/Bundle;I)V

    .line 118
    return-void

    .line 104
    :cond_0
    const/16 v3, 0x8

    move/from16 v0, p7

    if-ne v0, v3, :cond_1

    new-instance v6, Lhgw;

    new-instance v3, Lhxc;

    const-string v4, "v.domain"

    const/16 v5, 0x8

    const/4 v7, 0x0

    invoke-direct {v3, v4, v5, v2, v7}, Lhxc;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    invoke-direct {v6, v3}, Lhgw;-><init>(Lhxc;)V

    goto :goto_0

    :cond_1
    const/4 v6, 0x0

    goto :goto_0
.end method
