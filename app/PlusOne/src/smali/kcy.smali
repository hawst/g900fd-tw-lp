.class public abstract Lkcy;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lkdd;


# static fields
.field private static a:Landroid/os/Handler;


# instance fields
.field private b:Lkdc;

.field private c:I

.field private d:I

.field private e:Ljava/lang/Object;

.field private f:Lkda;

.field private volatile g:Z

.field private h:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 34
    new-instance v0, Landroid/os/Handler;

    .line 35
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    new-instance v2, Lkcz;

    invoke-direct {v2}, Lkcz;-><init>()V

    invoke-direct {v0, v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    sput-object v0, Lkcy;->a:Landroid/os/Handler;

    .line 34
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    .line 95
    sget-object v0, Lkcy;->a:Landroid/os/Handler;

    sget-object v1, Lkcy;->a:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 96
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 107
    invoke-static {}, Llsx;->c()V

    .line 110
    sget-object v0, Lkcy;->a:Landroid/os/Handler;

    sget-object v1, Lkcy;->a:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 113
    monitor-enter p0

    .line 114
    :try_start_0
    iget-boolean v0, p0, Lkcy;->h:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lkcy;->g:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 116
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 123
    :cond_0
    :goto_0
    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 125
    iget-boolean v0, p0, Lkcy;->g:Z

    if-eqz v0, :cond_1

    .line 126
    new-instance v0, Lkde;

    iget-object v1, p0, Lkcy;->b:Lkdc;

    invoke-direct {v0, v1}, Lkde;-><init>(Lkdc;)V

    throw v0

    .line 118
    :catch_0
    move-exception v0

    const/4 v0, 0x1

    :try_start_3
    iput-boolean v0, p0, Lkcy;->g:Z

    .line 119
    invoke-direct {p0}, Lkcy;->d()V

    .line 120
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 123
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 129
    :cond_1
    iget v0, p0, Lkcy;->c:I

    if-eq v0, v3, :cond_2

    .line 130
    new-instance v0, Lkdn;

    iget-object v1, p0, Lkcy;->b:Lkdc;

    iget v2, p0, Lkcy;->c:I

    iget v3, p0, Lkcy;->d:I

    invoke-direct {v0, v1, v2, v3}, Lkdn;-><init>(Lkdc;II)V

    throw v0

    .line 133
    :cond_2
    iget-object v0, p0, Lkcy;->e:Ljava/lang/Object;

    return-object v0
.end method

.method public a(Lkda;)V
    .locals 2

    .prologue
    .line 70
    iput-object p1, p0, Lkcy;->f:Lkda;

    .line 71
    invoke-virtual {p1}, Lkda;->getStatus()I

    move-result v0

    iput v0, p0, Lkcy;->c:I

    .line 73
    iget v0, p0, Lkcy;->c:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 74
    invoke-virtual {p1}, Lkda;->getIdentifier()Lkdc;

    move-result-object v0

    iput-object v0, p0, Lkcy;->b:Lkdc;

    .line 75
    invoke-virtual {p1}, Lkda;->getHttpStatusCode()I

    move-result v0

    iput v0, p0, Lkcy;->d:I

    .line 76
    invoke-virtual {p1}, Lkda;->getResource()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lkcy;->e:Ljava/lang/Object;

    .line 82
    invoke-direct {p0}, Lkcy;->d()V

    .line 83
    monitor-enter p0

    .line 84
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lkcy;->h:Z

    .line 85
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    .line 86
    monitor-exit p0

    .line 88
    :cond_0
    return-void

    .line 86
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lkcy;->f:Lkda;

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Lkcy;->f:Lkda;

    invoke-virtual {v0, p0}, Lkda;->unregister(Lkdd;)V

    .line 140
    const/4 v0, 0x0

    iput-object v0, p0, Lkcy;->f:Lkda;

    .line 142
    :cond_0
    return-void
.end method
