.class public final Lnqa;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnqa;


# instance fields
.field public b:Lnqf;

.field public c:Lnqe;

.field public d:Lnpz;

.field public e:Lnpx;

.field public f:Lnqd;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 420
    const/4 v0, 0x0

    new-array v0, v0, [Lnqa;

    sput-object v0, Lnqa;->a:[Lnqa;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 421
    invoke-direct {p0}, Loxq;-><init>()V

    .line 424
    iput-object v0, p0, Lnqa;->b:Lnqf;

    .line 427
    iput-object v0, p0, Lnqa;->c:Lnqe;

    .line 430
    iput-object v0, p0, Lnqa;->d:Lnpz;

    .line 433
    iput-object v0, p0, Lnqa;->e:Lnpx;

    .line 436
    iput-object v0, p0, Lnqa;->f:Lnqd;

    .line 421
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 462
    const/4 v0, 0x0

    .line 463
    iget-object v1, p0, Lnqa;->b:Lnqf;

    if-eqz v1, :cond_0

    .line 464
    const/4 v0, 0x1

    iget-object v1, p0, Lnqa;->b:Lnqf;

    .line 465
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 467
    :cond_0
    iget-object v1, p0, Lnqa;->c:Lnqe;

    if-eqz v1, :cond_1

    .line 468
    const/4 v1, 0x2

    iget-object v2, p0, Lnqa;->c:Lnqe;

    .line 469
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 471
    :cond_1
    iget-object v1, p0, Lnqa;->d:Lnpz;

    if-eqz v1, :cond_2

    .line 472
    const/4 v1, 0x3

    iget-object v2, p0, Lnqa;->d:Lnpz;

    .line 473
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 475
    :cond_2
    iget-object v1, p0, Lnqa;->e:Lnpx;

    if-eqz v1, :cond_3

    .line 476
    const/4 v1, 0x4

    iget-object v2, p0, Lnqa;->e:Lnpx;

    .line 477
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 479
    :cond_3
    iget-object v1, p0, Lnqa;->f:Lnqd;

    if-eqz v1, :cond_4

    .line 480
    const/4 v1, 0x5

    iget-object v2, p0, Lnqa;->f:Lnqd;

    .line 481
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 483
    :cond_4
    iget-object v1, p0, Lnqa;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 484
    iput v0, p0, Lnqa;->ai:I

    .line 485
    return v0
.end method

.method public a(Loxn;)Lnqa;
    .locals 2

    .prologue
    .line 493
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 494
    sparse-switch v0, :sswitch_data_0

    .line 498
    iget-object v1, p0, Lnqa;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 499
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnqa;->ah:Ljava/util/List;

    .line 502
    :cond_1
    iget-object v1, p0, Lnqa;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 504
    :sswitch_0
    return-object p0

    .line 509
    :sswitch_1
    iget-object v0, p0, Lnqa;->b:Lnqf;

    if-nez v0, :cond_2

    .line 510
    new-instance v0, Lnqf;

    invoke-direct {v0}, Lnqf;-><init>()V

    iput-object v0, p0, Lnqa;->b:Lnqf;

    .line 512
    :cond_2
    iget-object v0, p0, Lnqa;->b:Lnqf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 516
    :sswitch_2
    iget-object v0, p0, Lnqa;->c:Lnqe;

    if-nez v0, :cond_3

    .line 517
    new-instance v0, Lnqe;

    invoke-direct {v0}, Lnqe;-><init>()V

    iput-object v0, p0, Lnqa;->c:Lnqe;

    .line 519
    :cond_3
    iget-object v0, p0, Lnqa;->c:Lnqe;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 523
    :sswitch_3
    iget-object v0, p0, Lnqa;->d:Lnpz;

    if-nez v0, :cond_4

    .line 524
    new-instance v0, Lnpz;

    invoke-direct {v0}, Lnpz;-><init>()V

    iput-object v0, p0, Lnqa;->d:Lnpz;

    .line 526
    :cond_4
    iget-object v0, p0, Lnqa;->d:Lnpz;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 530
    :sswitch_4
    iget-object v0, p0, Lnqa;->e:Lnpx;

    if-nez v0, :cond_5

    .line 531
    new-instance v0, Lnpx;

    invoke-direct {v0}, Lnpx;-><init>()V

    iput-object v0, p0, Lnqa;->e:Lnpx;

    .line 533
    :cond_5
    iget-object v0, p0, Lnqa;->e:Lnpx;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 537
    :sswitch_5
    iget-object v0, p0, Lnqa;->f:Lnqd;

    if-nez v0, :cond_6

    .line 538
    new-instance v0, Lnqd;

    invoke-direct {v0}, Lnqd;-><init>()V

    iput-object v0, p0, Lnqa;->f:Lnqd;

    .line 540
    :cond_6
    iget-object v0, p0, Lnqa;->f:Lnqd;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 494
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 441
    iget-object v0, p0, Lnqa;->b:Lnqf;

    if-eqz v0, :cond_0

    .line 442
    const/4 v0, 0x1

    iget-object v1, p0, Lnqa;->b:Lnqf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 444
    :cond_0
    iget-object v0, p0, Lnqa;->c:Lnqe;

    if-eqz v0, :cond_1

    .line 445
    const/4 v0, 0x2

    iget-object v1, p0, Lnqa;->c:Lnqe;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 447
    :cond_1
    iget-object v0, p0, Lnqa;->d:Lnpz;

    if-eqz v0, :cond_2

    .line 448
    const/4 v0, 0x3

    iget-object v1, p0, Lnqa;->d:Lnpz;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 450
    :cond_2
    iget-object v0, p0, Lnqa;->e:Lnpx;

    if-eqz v0, :cond_3

    .line 451
    const/4 v0, 0x4

    iget-object v1, p0, Lnqa;->e:Lnpx;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 453
    :cond_3
    iget-object v0, p0, Lnqa;->f:Lnqd;

    if-eqz v0, :cond_4

    .line 454
    const/4 v0, 0x5

    iget-object v1, p0, Lnqa;->f:Lnqd;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 456
    :cond_4
    iget-object v0, p0, Lnqa;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 458
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 417
    invoke-virtual {p0, p1}, Lnqa;->a(Loxn;)Lnqa;

    move-result-object v0

    return-object v0
.end method
