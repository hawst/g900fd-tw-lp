.class public final Lpny;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lpny;


# instance fields
.field private b:Ljava/lang/Float;

.field private c:Ljava/lang/Float;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const/4 v0, 0x0

    new-array v0, v0, [Lpny;

    sput-object v0, Lpny;->a:[Lpny;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 76
    const/4 v0, 0x0

    .line 77
    iget-object v1, p0, Lpny;->b:Ljava/lang/Float;

    if-eqz v1, :cond_0

    .line 78
    const/4 v0, 0x1

    iget-object v1, p0, Lpny;->b:Ljava/lang/Float;

    .line 79
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x0

    .line 81
    :cond_0
    iget-object v1, p0, Lpny;->c:Ljava/lang/Float;

    if-eqz v1, :cond_1

    .line 82
    const/4 v1, 0x2

    iget-object v2, p0, Lpny;->c:Ljava/lang/Float;

    .line 83
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 85
    :cond_1
    iget-object v1, p0, Lpny;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 86
    const/4 v1, 0x3

    iget-object v2, p0, Lpny;->d:Ljava/lang/String;

    .line 87
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 89
    :cond_2
    iget-object v1, p0, Lpny;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 90
    const/4 v1, 0x4

    iget-object v2, p0, Lpny;->e:Ljava/lang/String;

    .line 91
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 93
    :cond_3
    iget-object v1, p0, Lpny;->f:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 94
    const/4 v1, 0x5

    iget-object v2, p0, Lpny;->f:Ljava/lang/String;

    .line 95
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 97
    :cond_4
    iget-object v1, p0, Lpny;->g:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 98
    const/4 v1, 0x7

    iget-object v2, p0, Lpny;->g:Ljava/lang/String;

    .line 99
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 101
    :cond_5
    iget-object v1, p0, Lpny;->h:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 102
    const/16 v1, 0x8

    iget-object v2, p0, Lpny;->h:Ljava/lang/String;

    .line 103
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 105
    :cond_6
    iget-object v1, p0, Lpny;->i:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 106
    const/16 v1, 0x9

    iget-object v2, p0, Lpny;->i:Ljava/lang/String;

    .line 107
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 109
    :cond_7
    iget-object v1, p0, Lpny;->j:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    .line 110
    const/16 v1, 0xa

    iget-object v2, p0, Lpny;->j:Ljava/lang/Boolean;

    .line 111
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 113
    :cond_8
    iget-object v1, p0, Lpny;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 114
    iput v0, p0, Lpny;->ai:I

    .line 115
    return v0
.end method

.method public a(Loxn;)Lpny;
    .locals 2

    .prologue
    .line 123
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 124
    sparse-switch v0, :sswitch_data_0

    .line 128
    iget-object v1, p0, Lpny;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 129
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpny;->ah:Ljava/util/List;

    .line 132
    :cond_1
    iget-object v1, p0, Lpny;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 134
    :sswitch_0
    return-object p0

    .line 139
    :sswitch_1
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpny;->b:Ljava/lang/Float;

    goto :goto_0

    .line 143
    :sswitch_2
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpny;->c:Ljava/lang/Float;

    goto :goto_0

    .line 147
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpny;->d:Ljava/lang/String;

    goto :goto_0

    .line 151
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpny;->e:Ljava/lang/String;

    goto :goto_0

    .line 155
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpny;->f:Ljava/lang/String;

    goto :goto_0

    .line 159
    :sswitch_6
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpny;->g:Ljava/lang/String;

    goto :goto_0

    .line 163
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpny;->h:Ljava/lang/String;

    goto :goto_0

    .line 167
    :sswitch_8
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpny;->i:Ljava/lang/String;

    goto :goto_0

    .line 171
    :sswitch_9
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lpny;->j:Ljava/lang/Boolean;

    goto :goto_0

    .line 124
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x15 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x3a -> :sswitch_6
        0x42 -> :sswitch_7
        0x4a -> :sswitch_8
        0x50 -> :sswitch_9
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 43
    iget-object v0, p0, Lpny;->b:Ljava/lang/Float;

    if-eqz v0, :cond_0

    .line 44
    const/4 v0, 0x1

    iget-object v1, p0, Lpny;->b:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 46
    :cond_0
    iget-object v0, p0, Lpny;->c:Ljava/lang/Float;

    if-eqz v0, :cond_1

    .line 47
    const/4 v0, 0x2

    iget-object v1, p0, Lpny;->c:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 49
    :cond_1
    iget-object v0, p0, Lpny;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 50
    const/4 v0, 0x3

    iget-object v1, p0, Lpny;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 52
    :cond_2
    iget-object v0, p0, Lpny;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 53
    const/4 v0, 0x4

    iget-object v1, p0, Lpny;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 55
    :cond_3
    iget-object v0, p0, Lpny;->f:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 56
    const/4 v0, 0x5

    iget-object v1, p0, Lpny;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 58
    :cond_4
    iget-object v0, p0, Lpny;->g:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 59
    const/4 v0, 0x7

    iget-object v1, p0, Lpny;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 61
    :cond_5
    iget-object v0, p0, Lpny;->h:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 62
    const/16 v0, 0x8

    iget-object v1, p0, Lpny;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 64
    :cond_6
    iget-object v0, p0, Lpny;->i:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 65
    const/16 v0, 0x9

    iget-object v1, p0, Lpny;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 67
    :cond_7
    iget-object v0, p0, Lpny;->j:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    .line 68
    const/16 v0, 0xa

    iget-object v1, p0, Lpny;->j:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 70
    :cond_8
    iget-object v0, p0, Lpny;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 72
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 16
    invoke-virtual {p0, p1}, Lpny;->a(Loxn;)Lpny;

    move-result-object v0

    return-object v0
.end method
