.class public final Leyx;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Landroid/content/Intent;

.field private final b:I

.field private c:Lizu;

.field private d:I

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/Integer;

.field private h:Ljava/lang/Integer;


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/lang/Class;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<*>;I)V"
        }
    .end annotation

    .prologue
    .line 3656
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3657
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1, p2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v0, p0, Leyx;->a:Landroid/content/Intent;

    .line 3658
    iput p3, p0, Leyx;->b:I

    .line 3659
    const/4 v0, 0x0

    iput v0, p0, Leyx;->d:I

    .line 3660
    return-void
.end method


# virtual methods
.method public a()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 3704
    iget v0, p0, Leyx;->d:I

    if-nez v0, :cond_0

    .line 3705
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot create crop activity with no crop mode."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3708
    :cond_0
    iget-object v0, p0, Leyx;->f:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 3709
    iget-object v0, p0, Leyx;->a:Landroid/content/Intent;

    const-string v1, "tile_id"

    iget-object v2, p0, Leyx;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3712
    :cond_1
    iget-object v0, p0, Leyx;->e:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 3713
    iget-object v0, p0, Leyx;->a:Landroid/content/Intent;

    const-string v1, "view_id"

    iget-object v2, p0, Leyx;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3716
    :cond_2
    iget-object v0, p0, Leyx;->c:Lizu;

    if-eqz v0, :cond_3

    .line 3717
    iget-object v0, p0, Leyx;->a:Landroid/content/Intent;

    const-string v1, "photo_ref"

    iget-object v2, p0, Leyx;->c:Lizu;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 3720
    :cond_3
    iget-object v0, p0, Leyx;->g:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 3729
    iget-object v0, p0, Leyx;->a:Landroid/content/Intent;

    const-string v1, "photo_min_width"

    iget-object v2, p0, Leyx;->g:Ljava/lang/Integer;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 3732
    :cond_4
    iget-object v0, p0, Leyx;->h:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 3733
    iget-object v0, p0, Leyx;->a:Landroid/content/Intent;

    const-string v1, "photo_min_height"

    iget-object v2, p0, Leyx;->h:Ljava/lang/Integer;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 3736
    :cond_5
    iget-object v0, p0, Leyx;->a:Landroid/content/Intent;

    const-string v1, "photo_picker_crop_mode"

    iget v2, p0, Leyx;->d:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3737
    iget-object v0, p0, Leyx;->a:Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 3738
    iget-object v0, p0, Leyx;->a:Landroid/content/Intent;

    const-string v1, "account_id"

    iget v2, p0, Leyx;->b:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3740
    iget-object v0, p0, Leyx;->a:Landroid/content/Intent;

    return-object v0
.end method

.method public a(I)Leyx;
    .locals 0

    .prologue
    .line 3678
    iput p1, p0, Leyx;->d:I

    .line 3679
    return-object p0
.end method

.method public a(Lizu;)Leyx;
    .locals 0

    .prologue
    .line 3663
    iput-object p1, p0, Leyx;->c:Lizu;

    .line 3664
    return-object p0
.end method

.method public a(Ljava/lang/String;)Leyx;
    .locals 0

    .prologue
    .line 3668
    iput-object p1, p0, Leyx;->e:Ljava/lang/String;

    .line 3669
    return-object p0
.end method

.method public b(I)Leyx;
    .locals 1

    .prologue
    .line 3693
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leyx;->g:Ljava/lang/Integer;

    .line 3694
    return-object p0
.end method

.method public b(Ljava/lang/String;)Leyx;
    .locals 0

    .prologue
    .line 3673
    iput-object p1, p0, Leyx;->f:Ljava/lang/String;

    .line 3674
    return-object p0
.end method

.method public c(I)Leyx;
    .locals 1

    .prologue
    .line 3698
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leyx;->h:Ljava/lang/Integer;

    .line 3699
    return-object p0
.end method
