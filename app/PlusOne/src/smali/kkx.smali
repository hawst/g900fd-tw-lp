.class public final Lkkx;
.super Landroid/widget/ArrayAdapter;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lizr;",
        ">;"
    }
.end annotation


# instance fields
.field private synthetic a:Lcom/google/android/libraries/social/sharekit/impl/MediaPreviewFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/social/sharekit/impl/MediaPreviewFragment;Landroid/content/Context;ILjava/util/List;)V
    .locals 0

    .prologue
    .line 120
    iput-object p1, p0, Lkkx;->a:Lcom/google/android/libraries/social/sharekit/impl/MediaPreviewFragment;

    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 122
    if-nez p2, :cond_0

    .line 123
    iget-object v2, p0, Lkkx;->a:Lcom/google/android/libraries/social/sharekit/impl/MediaPreviewFragment;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/sharekit/impl/MediaPreviewFragment;->n()Lz;

    move-result-object v2

    invoke-virtual {v2}, Lz;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f0401e0

    .line 124
    invoke-virtual {v2, v3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 126
    :cond_0
    iget-object v2, p0, Lkkx;->a:Lcom/google/android/libraries/social/sharekit/impl/MediaPreviewFragment;

    .line 127
    invoke-virtual {v2}, Lcom/google/android/libraries/social/sharekit/impl/MediaPreviewFragment;->n()Lz;

    move-result-object v2

    invoke-virtual {v2}, Lz;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    iget-object v3, p0, Lkkx;->a:Lcom/google/android/libraries/social/sharekit/impl/MediaPreviewFragment;

    invoke-virtual {v3}, Lcom/google/android/libraries/social/sharekit/impl/MediaPreviewFragment;->o()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {p0}, Lkkx;->getCount()I

    move-result v4

    if-le v4, v0, :cond_1

    .line 126
    :goto_0
    invoke-static {v2, v3, v0}, Lcom/google/android/libraries/social/sharekit/impl/MediaPreviewFragment;->a(Landroid/view/WindowManager;Landroid/content/res/Resources;Z)I

    move-result v0

    .line 129
    new-instance v2, Llji;

    invoke-direct {v2, v0, v0}, Llji;-><init>(II)V

    .line 131
    iput-boolean v1, v2, Llji;->a:Z

    .line 132
    invoke-virtual {p2, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 134
    invoke-virtual {p0, p1}, Lkkx;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizr;

    .line 135
    const v1, 0x7f100589

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 136
    iget-object v2, p0, Lkkx;->a:Lcom/google/android/libraries/social/sharekit/impl/MediaPreviewFragment;

    .line 137
    invoke-virtual {v2}, Lcom/google/android/libraries/social/sharekit/impl/MediaPreviewFragment;->o()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2}, Llhq;->a(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 136
    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->d(Landroid/graphics/drawable/Drawable;)V

    .line 138
    invoke-interface {v0}, Lizr;->f()Lizu;

    move-result-object v2

    invoke-virtual {v1, v2, v5}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;Lizo;)V

    .line 140
    const v1, 0x7f10058a

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 141
    iget-object v2, p0, Lkkx;->a:Lcom/google/android/libraries/social/sharekit/impl/MediaPreviewFragment;

    invoke-static {v2}, Lcom/google/android/libraries/social/sharekit/impl/MediaPreviewFragment;->c(Lcom/google/android/libraries/social/sharekit/impl/MediaPreviewFragment;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 142
    invoke-virtual {v1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 144
    return-object p2

    :cond_1
    move v0, v1

    .line 127
    goto :goto_0
.end method
