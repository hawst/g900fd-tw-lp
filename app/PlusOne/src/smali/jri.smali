.class public final Ljri;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lixl;


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Ljri;->a:Landroid/content/Context;

    .line 26
    return-void
.end method


# virtual methods
.method public a(Lhem;Lmcb;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 47
    iget-object v3, p2, Lmcb;->a:Lnog;

    .line 48
    iget-object v0, v3, Lnog;->b:Lnoj;

    .line 50
    iget-object v4, v0, Lnoj;->b:Ljava/lang/Boolean;

    .line 51
    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v0, v0, Lnoj;->e:Ljava/lang/Boolean;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    move v0, v2

    .line 52
    :goto_0
    iget v4, v3, Lnog;->a:I

    if-ne v4, v2, :cond_3

    .line 55
    :goto_1
    if-eqz v0, :cond_1

    if-nez v2, :cond_1

    .line 56
    iget-object v0, p0, Ljri;->a:Landroid/content/Context;

    const-class v1, Ljpb;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljpb;

    .line 57
    iget-object v1, v3, Lnog;->e:Loif;

    invoke-interface {v0, p1, v1}, Ljpb;->a(Lhem;Loif;)V

    .line 59
    :cond_1
    return-void

    :cond_2
    move v0, v1

    .line 51
    goto :goto_0

    :cond_3
    move v2, v1

    .line 52
    goto :goto_1
.end method

.method public a(Lhej;Lmca;)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 30
    iget-object v0, p0, Ljri;->a:Landroid/content/Context;

    const-class v1, Ljpb;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljpb;

    .line 31
    invoke-interface {v0, p1}, Ljpb;->a(Lhej;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 32
    invoke-interface {v0, p1}, Ljpb;->b(Lhej;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 33
    :cond_0
    iget-object v0, p2, Lmca;->a:Lnoa;

    if-nez v0, :cond_1

    .line 34
    new-instance v0, Lnoa;

    invoke-direct {v0}, Lnoa;-><init>()V

    iput-object v0, p2, Lmca;->a:Lnoa;

    .line 36
    :cond_1
    iget-object v0, p2, Lmca;->a:Lnoa;

    iget-object v0, v0, Lnoa;->c:Lnob;

    if-nez v0, :cond_2

    .line 37
    iget-object v0, p2, Lmca;->a:Lnoa;

    new-instance v1, Lnob;

    invoke-direct {v1}, Lnob;-><init>()V

    iput-object v1, v0, Lnoa;->c:Lnob;

    .line 39
    :cond_2
    iget-object v0, p2, Lmca;->a:Lnoa;

    iget-object v0, v0, Lnoa;->c:Lnob;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lnob;->a:Ljava/lang/Boolean;

    .line 41
    :cond_3
    return v2
.end method
