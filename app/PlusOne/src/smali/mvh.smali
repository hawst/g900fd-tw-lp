.class public final Lmvh;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Integer;

.field private b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10533
    invoke-direct {p0}, Loxq;-><init>()V

    .line 10544
    const/high16 v0, -0x80000000

    iput v0, p0, Lmvh;->b:I

    .line 10533
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 10561
    const/4 v0, 0x0

    .line 10562
    iget-object v1, p0, Lmvh;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 10563
    const/4 v0, 0x1

    iget-object v1, p0, Lmvh;->a:Ljava/lang/Integer;

    .line 10564
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 10566
    :cond_0
    iget v1, p0, Lmvh;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_1

    .line 10567
    const/4 v1, 0x2

    iget v2, p0, Lmvh;->b:I

    .line 10568
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 10570
    :cond_1
    iget-object v1, p0, Lmvh;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10571
    iput v0, p0, Lmvh;->ai:I

    .line 10572
    return v0
.end method

.method public a(Loxn;)Lmvh;
    .locals 2

    .prologue
    .line 10580
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 10581
    sparse-switch v0, :sswitch_data_0

    .line 10585
    iget-object v1, p0, Lmvh;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 10586
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmvh;->ah:Ljava/util/List;

    .line 10589
    :cond_1
    iget-object v1, p0, Lmvh;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 10591
    :sswitch_0
    return-object p0

    .line 10596
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmvh;->a:Ljava/lang/Integer;

    goto :goto_0

    .line 10600
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 10601
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 10604
    :cond_2
    iput v0, p0, Lmvh;->b:I

    goto :goto_0

    .line 10606
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lmvh;->b:I

    goto :goto_0

    .line 10581
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 10549
    iget-object v0, p0, Lmvh;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 10550
    const/4 v0, 0x1

    iget-object v1, p0, Lmvh;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 10552
    :cond_0
    iget v0, p0, Lmvh;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 10553
    const/4 v0, 0x2

    iget v1, p0, Lmvh;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 10555
    :cond_1
    iget-object v0, p0, Lmvh;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 10557
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 10529
    invoke-virtual {p0, p1}, Lmvh;->a(Loxn;)Lmvh;

    move-result-object v0

    return-object v0
.end method
