.class public final Lohu;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Loht;

.field public b:I

.field private c:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2779
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2782
    sget-object v0, Loht;->a:[Loht;

    iput-object v0, p0, Lohu;->a:[Loht;

    .line 2787
    const/high16 v0, -0x80000000

    iput v0, p0, Lohu;->b:I

    .line 2779
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2811
    .line 2812
    iget-object v1, p0, Lohu;->a:[Loht;

    if-eqz v1, :cond_1

    .line 2813
    iget-object v2, p0, Lohu;->a:[Loht;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 2814
    if-eqz v4, :cond_0

    .line 2815
    const/4 v5, 0x1

    .line 2816
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 2813
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2820
    :cond_1
    iget-object v1, p0, Lohu;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 2821
    const/4 v1, 0x2

    iget-object v2, p0, Lohu;->c:Ljava/lang/Boolean;

    .line 2822
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2824
    :cond_2
    iget v1, p0, Lohu;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_3

    .line 2825
    const/4 v1, 0x3

    iget v2, p0, Lohu;->b:I

    .line 2826
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2828
    :cond_3
    iget-object v1, p0, Lohu;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2829
    iput v0, p0, Lohu;->ai:I

    .line 2830
    return v0
.end method

.method public a(Loxn;)Lohu;
    .locals 5

    .prologue
    const/16 v4, 0xa

    const/4 v1, 0x0

    .line 2838
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2839
    sparse-switch v0, :sswitch_data_0

    .line 2843
    iget-object v2, p0, Lohu;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 2844
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lohu;->ah:Ljava/util/List;

    .line 2847
    :cond_1
    iget-object v2, p0, Lohu;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2849
    :sswitch_0
    return-object p0

    .line 2854
    :sswitch_1
    invoke-static {p1, v4}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 2855
    iget-object v0, p0, Lohu;->a:[Loht;

    if-nez v0, :cond_3

    move v0, v1

    .line 2856
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Loht;

    .line 2857
    iget-object v3, p0, Lohu;->a:[Loht;

    if-eqz v3, :cond_2

    .line 2858
    iget-object v3, p0, Lohu;->a:[Loht;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2860
    :cond_2
    iput-object v2, p0, Lohu;->a:[Loht;

    .line 2861
    :goto_2
    iget-object v2, p0, Lohu;->a:[Loht;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 2862
    iget-object v2, p0, Lohu;->a:[Loht;

    new-instance v3, Loht;

    invoke-direct {v3}, Loht;-><init>()V

    aput-object v3, v2, v0

    .line 2863
    iget-object v2, p0, Lohu;->a:[Loht;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 2864
    invoke-virtual {p1}, Loxn;->a()I

    .line 2861
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2855
    :cond_3
    iget-object v0, p0, Lohu;->a:[Loht;

    array-length v0, v0

    goto :goto_1

    .line 2867
    :cond_4
    iget-object v2, p0, Lohu;->a:[Loht;

    new-instance v3, Loht;

    invoke-direct {v3}, Loht;-><init>()V

    aput-object v3, v2, v0

    .line 2868
    iget-object v2, p0, Lohu;->a:[Loht;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2872
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lohu;->c:Ljava/lang/Boolean;

    goto :goto_0

    .line 2876
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 2877
    if-eqz v0, :cond_5

    const/4 v2, 0x1

    if-eq v0, v2, :cond_5

    const/4 v2, 0x2

    if-eq v0, v2, :cond_5

    const/16 v2, 0xa6

    if-eq v0, v2, :cond_5

    const/16 v2, 0xa7

    if-eq v0, v2, :cond_5

    const/16 v2, 0x1b

    if-eq v0, v2, :cond_5

    const/16 v2, 0x1c

    if-eq v0, v2, :cond_5

    const/16 v2, 0x1d

    if-eq v0, v2, :cond_5

    const/16 v2, 0x1e

    if-eq v0, v2, :cond_5

    const/16 v2, 0x1f

    if-eq v0, v2, :cond_5

    const/16 v2, 0x20

    if-eq v0, v2, :cond_5

    const/16 v2, 0x21

    if-eq v0, v2, :cond_5

    const/16 v2, 0x22

    if-eq v0, v2, :cond_5

    const/4 v2, 0x5

    if-eq v0, v2, :cond_5

    const/4 v2, 0x6

    if-eq v0, v2, :cond_5

    const/16 v2, 0x23

    if-eq v0, v2, :cond_5

    const/16 v2, 0x10

    if-eq v0, v2, :cond_5

    const/4 v2, 0x3

    if-eq v0, v2, :cond_5

    const/4 v2, 0x4

    if-eq v0, v2, :cond_5

    const/4 v2, 0x7

    if-eq v0, v2, :cond_5

    const/16 v2, 0xd2

    if-eq v0, v2, :cond_5

    const/16 v2, 0x8

    if-eq v0, v2, :cond_5

    const/16 v2, 0x9

    if-eq v0, v2, :cond_5

    if-eq v0, v4, :cond_5

    const/16 v2, 0x24

    if-eq v0, v2, :cond_5

    const/16 v2, 0x25

    if-eq v0, v2, :cond_5

    const/16 v2, 0x26

    if-eq v0, v2, :cond_5

    const/16 v2, 0x27

    if-eq v0, v2, :cond_5

    const/16 v2, 0x28

    if-eq v0, v2, :cond_5

    const/16 v2, 0x29

    if-eq v0, v2, :cond_5

    const/16 v2, 0x2a

    if-eq v0, v2, :cond_5

    const/16 v2, 0x11

    if-eq v0, v2, :cond_5

    const/16 v2, 0xe1

    if-eq v0, v2, :cond_5

    const/16 v2, 0x2b

    if-eq v0, v2, :cond_5

    const/16 v2, 0x2c

    if-eq v0, v2, :cond_5

    const/16 v2, 0x2d

    if-eq v0, v2, :cond_5

    const/16 v2, 0x2e

    if-eq v0, v2, :cond_5

    const/16 v2, 0x2f

    if-eq v0, v2, :cond_5

    const/16 v2, 0x30

    if-eq v0, v2, :cond_5

    const/16 v2, 0x31

    if-eq v0, v2, :cond_5

    const/16 v2, 0x32

    if-eq v0, v2, :cond_5

    const/16 v2, 0x72

    if-eq v0, v2, :cond_5

    const/16 v2, 0x93

    if-eq v0, v2, :cond_5

    const/16 v2, 0x5d

    if-eq v0, v2, :cond_5

    const/16 v2, 0xe8

    if-eq v0, v2, :cond_5

    const/16 v2, 0x95

    if-eq v0, v2, :cond_5

    const/16 v2, 0x9f

    if-eq v0, v2, :cond_5

    const/16 v2, 0xa0

    if-eq v0, v2, :cond_5

    const/16 v2, 0x33

    if-eq v0, v2, :cond_5

    const/16 v2, 0x34

    if-eq v0, v2, :cond_5

    const/16 v2, 0xa9

    if-eq v0, v2, :cond_5

    const/16 v2, 0xaa

    if-eq v0, v2, :cond_5

    const/16 v2, 0xb6

    if-eq v0, v2, :cond_5

    const/16 v2, 0xab

    if-eq v0, v2, :cond_5

    const/16 v2, 0xb7

    if-eq v0, v2, :cond_5

    const/16 v2, 0xd8

    if-eq v0, v2, :cond_5

    const/16 v2, 0xac

    if-eq v0, v2, :cond_5

    const/16 v2, 0xb5

    if-eq v0, v2, :cond_5

    const/16 v2, 0x35

    if-eq v0, v2, :cond_5

    const/16 v2, 0x13

    if-eq v0, v2, :cond_5

    const/16 v2, 0x36

    if-eq v0, v2, :cond_5

    const/16 v2, 0xf

    if-eq v0, v2, :cond_5

    const/16 v2, 0x37

    if-eq v0, v2, :cond_5

    const/16 v2, 0x38

    if-eq v0, v2, :cond_5

    const/16 v2, 0xb8

    if-eq v0, v2, :cond_5

    const/16 v2, 0xb

    if-eq v0, v2, :cond_5

    const/16 v2, 0x39

    if-eq v0, v2, :cond_5

    const/16 v2, 0x3a

    if-eq v0, v2, :cond_5

    const/16 v2, 0xc

    if-eq v0, v2, :cond_5

    const/16 v2, 0x3b

    if-eq v0, v2, :cond_5

    const/16 v2, 0xcf

    if-eq v0, v2, :cond_5

    const/16 v2, 0xb9

    if-eq v0, v2, :cond_5

    const/16 v2, 0x3c

    if-eq v0, v2, :cond_5

    const/16 v2, 0x3d

    if-eq v0, v2, :cond_5

    const/16 v2, 0xad

    if-eq v0, v2, :cond_5

    const/16 v2, 0x3e

    if-eq v0, v2, :cond_5

    const/16 v2, 0xa2

    if-eq v0, v2, :cond_5

    const/16 v2, 0xa3

    if-eq v0, v2, :cond_5

    const/16 v2, 0xa4

    if-eq v0, v2, :cond_5

    const/16 v2, 0xa5

    if-eq v0, v2, :cond_5

    const/16 v2, 0x3f

    if-eq v0, v2, :cond_5

    const/16 v2, 0x40

    if-eq v0, v2, :cond_5

    const/16 v2, 0x41

    if-eq v0, v2, :cond_5

    const/16 v2, 0x42

    if-eq v0, v2, :cond_5

    const/16 v2, 0x43

    if-eq v0, v2, :cond_5

    const/16 v2, 0x44

    if-eq v0, v2, :cond_5

    const/16 v2, 0x45

    if-eq v0, v2, :cond_5

    const/16 v2, 0x46

    if-eq v0, v2, :cond_5

    const/16 v2, 0xae

    if-eq v0, v2, :cond_5

    const/16 v2, 0x47

    if-eq v0, v2, :cond_5

    const/16 v2, 0x48

    if-eq v0, v2, :cond_5

    const/16 v2, 0xc2

    if-eq v0, v2, :cond_5

    const/16 v2, 0xe7

    if-eq v0, v2, :cond_5

    const/16 v2, 0xc3

    if-eq v0, v2, :cond_5

    const/16 v2, 0xd3

    if-eq v0, v2, :cond_5

    const/16 v2, 0xd4

    if-eq v0, v2, :cond_5

    const/16 v2, 0x4e

    if-eq v0, v2, :cond_5

    const/16 v2, 0x4f

    if-eq v0, v2, :cond_5

    const/16 v2, 0x50

    if-eq v0, v2, :cond_5

    const/16 v2, 0x51

    if-eq v0, v2, :cond_5

    const/16 v2, 0x52

    if-eq v0, v2, :cond_5

    const/16 v2, 0x53

    if-eq v0, v2, :cond_5

    const/16 v2, 0x54

    if-eq v0, v2, :cond_5

    const/16 v2, 0x55

    if-eq v0, v2, :cond_5

    const/16 v2, 0x56

    if-eq v0, v2, :cond_5

    const/16 v2, 0x57

    if-eq v0, v2, :cond_5

    const/16 v2, 0x58

    if-eq v0, v2, :cond_5

    const/16 v2, 0xba

    if-eq v0, v2, :cond_5

    const/16 v2, 0xbb

    if-eq v0, v2, :cond_5

    const/16 v2, 0xbc

    if-eq v0, v2, :cond_5

    const/16 v2, 0x59

    if-eq v0, v2, :cond_5

    const/16 v2, 0xaf

    if-eq v0, v2, :cond_5

    const/16 v2, 0x5a

    if-eq v0, v2, :cond_5

    const/16 v2, 0x5b

    if-eq v0, v2, :cond_5

    const/16 v2, 0x5c

    if-eq v0, v2, :cond_5

    const/16 v2, 0x5e

    if-eq v0, v2, :cond_5

    const/16 v2, 0x5f

    if-eq v0, v2, :cond_5

    const/16 v2, 0x60

    if-eq v0, v2, :cond_5

    const/16 v2, 0x61

    if-eq v0, v2, :cond_5

    const/16 v2, 0x62

    if-eq v0, v2, :cond_5

    const/16 v2, 0x12

    if-eq v0, v2, :cond_5

    const/16 v2, 0x15

    if-eq v0, v2, :cond_5

    const/16 v2, 0x63

    if-eq v0, v2, :cond_5

    const/16 v2, 0x64

    if-eq v0, v2, :cond_5

    const/16 v2, 0x65

    if-eq v0, v2, :cond_5

    const/16 v2, 0x66

    if-eq v0, v2, :cond_5

    const/16 v2, 0x67

    if-eq v0, v2, :cond_5

    const/16 v2, 0xbd

    if-eq v0, v2, :cond_5

    const/16 v2, 0xb0

    if-eq v0, v2, :cond_5

    const/16 v2, 0x68

    if-eq v0, v2, :cond_5

    const/16 v2, 0x69

    if-eq v0, v2, :cond_5

    const/16 v2, 0xbf

    if-eq v0, v2, :cond_5

    const/16 v2, 0x6a

    if-eq v0, v2, :cond_5

    const/16 v2, 0x6b

    if-eq v0, v2, :cond_5

    const/16 v2, 0x6c

    if-eq v0, v2, :cond_5

    const/16 v2, 0xb1

    if-eq v0, v2, :cond_5

    const/16 v2, 0x6d

    if-eq v0, v2, :cond_5

    const/16 v2, 0x6e

    if-eq v0, v2, :cond_5

    const/16 v2, 0x6f

    if-eq v0, v2, :cond_5

    const/16 v2, 0x70

    if-eq v0, v2, :cond_5

    const/16 v2, 0x71

    if-eq v0, v2, :cond_5

    const/16 v2, 0x9b

    if-eq v0, v2, :cond_5

    const/16 v2, 0xc0

    if-eq v0, v2, :cond_5

    const/16 v2, 0xe6

    if-eq v0, v2, :cond_5

    const/16 v2, 0x73

    if-eq v0, v2, :cond_5

    const/16 v2, 0x74

    if-eq v0, v2, :cond_5

    const/16 v2, 0x75

    if-eq v0, v2, :cond_5

    const/16 v2, 0x76

    if-eq v0, v2, :cond_5

    const/16 v2, 0x77

    if-eq v0, v2, :cond_5

    const/16 v2, 0x78

    if-eq v0, v2, :cond_5

    const/16 v2, 0x79

    if-eq v0, v2, :cond_5

    const/16 v2, 0x7a

    if-eq v0, v2, :cond_5

    const/16 v2, 0x7b

    if-eq v0, v2, :cond_5

    const/16 v2, 0x7c

    if-eq v0, v2, :cond_5

    const/16 v2, 0x7d

    if-eq v0, v2, :cond_5

    const/16 v2, 0x7e

    if-eq v0, v2, :cond_5

    const/16 v2, 0x49

    if-eq v0, v2, :cond_5

    const/16 v2, 0x4a

    if-eq v0, v2, :cond_5

    const/16 v2, 0x4b

    if-eq v0, v2, :cond_5

    const/16 v2, 0x4c

    if-eq v0, v2, :cond_5

    const/16 v2, 0x4d

    if-eq v0, v2, :cond_5

    const/16 v2, 0x91

    if-eq v0, v2, :cond_5

    const/16 v2, 0xa1

    if-eq v0, v2, :cond_5

    const/16 v2, 0xd

    if-eq v0, v2, :cond_5

    const/16 v2, 0xe

    if-eq v0, v2, :cond_5

    const/16 v2, 0x7f

    if-eq v0, v2, :cond_5

    const/16 v2, 0x14

    if-eq v0, v2, :cond_5

    const/16 v2, 0x80

    if-eq v0, v2, :cond_5

    const/16 v2, 0x81

    if-eq v0, v2, :cond_5

    const/16 v2, 0xd0

    if-eq v0, v2, :cond_5

    const/16 v2, 0x82

    if-eq v0, v2, :cond_5

    const/16 v2, 0x83

    if-eq v0, v2, :cond_5

    const/16 v2, 0x84

    if-eq v0, v2, :cond_5

    const/16 v2, 0x85

    if-eq v0, v2, :cond_5

    const/16 v2, 0x86

    if-eq v0, v2, :cond_5

    const/16 v2, 0x87

    if-eq v0, v2, :cond_5

    const/16 v2, 0x88

    if-eq v0, v2, :cond_5

    const/16 v2, 0x89

    if-eq v0, v2, :cond_5

    const/16 v2, 0x8a

    if-eq v0, v2, :cond_5

    const/16 v2, 0x8b

    if-eq v0, v2, :cond_5

    const/16 v2, 0x8c

    if-eq v0, v2, :cond_5

    const/16 v2, 0x8d

    if-eq v0, v2, :cond_5

    const/16 v2, 0x8e

    if-eq v0, v2, :cond_5

    const/16 v2, 0x8f

    if-eq v0, v2, :cond_5

    const/16 v2, 0x90

    if-eq v0, v2, :cond_5

    const/16 v2, 0xc1

    if-eq v0, v2, :cond_5

    const/16 v2, 0x19

    if-eq v0, v2, :cond_5

    const/16 v2, 0x1a

    if-eq v0, v2, :cond_5

    const/16 v2, 0x92

    if-eq v0, v2, :cond_5

    const/16 v2, 0x94

    if-eq v0, v2, :cond_5

    const/16 v2, 0x97

    if-eq v0, v2, :cond_5

    const/16 v2, 0x98

    if-eq v0, v2, :cond_5

    const/16 v2, 0x99

    if-eq v0, v2, :cond_5

    const/16 v2, 0xb2

    if-eq v0, v2, :cond_5

    const/16 v2, 0xb3

    if-eq v0, v2, :cond_5

    const/16 v2, 0x9a

    if-eq v0, v2, :cond_5

    const/16 v2, 0x9c

    if-eq v0, v2, :cond_5

    const/16 v2, 0x9d

    if-eq v0, v2, :cond_5

    const/16 v2, 0x9e

    if-eq v0, v2, :cond_5

    const/16 v2, 0xa8

    if-eq v0, v2, :cond_5

    const/16 v2, 0xb4

    if-eq v0, v2, :cond_5

    const/16 v2, 0xc4

    if-eq v0, v2, :cond_5

    const/16 v2, 0xc5

    if-eq v0, v2, :cond_5

    const/16 v2, 0xc6

    if-eq v0, v2, :cond_5

    const/16 v2, 0xc7

    if-eq v0, v2, :cond_5

    const/16 v2, 0xc8

    if-eq v0, v2, :cond_5

    const/16 v2, 0xc9

    if-eq v0, v2, :cond_5

    const/16 v2, 0xca

    if-eq v0, v2, :cond_5

    const/16 v2, 0xe4

    if-eq v0, v2, :cond_5

    const/16 v2, 0xcb

    if-eq v0, v2, :cond_5

    const/16 v2, 0xcc

    if-eq v0, v2, :cond_5

    const/16 v2, 0xcd

    if-eq v0, v2, :cond_5

    const/16 v2, 0xce

    if-eq v0, v2, :cond_5

    const/16 v2, 0xd1

    if-eq v0, v2, :cond_5

    const/16 v2, 0xd5

    if-eq v0, v2, :cond_5

    const/16 v2, 0xd6

    if-eq v0, v2, :cond_5

    const/16 v2, 0xd7

    if-eq v0, v2, :cond_5

    const/16 v2, 0xd9

    if-eq v0, v2, :cond_5

    const/16 v2, 0xda

    if-eq v0, v2, :cond_5

    const/16 v2, 0xdb

    if-eq v0, v2, :cond_5

    const/16 v2, 0xdc

    if-eq v0, v2, :cond_5

    const/16 v2, 0xdd

    if-eq v0, v2, :cond_5

    const/16 v2, 0xde

    if-eq v0, v2, :cond_5

    const/16 v2, 0xdf

    if-eq v0, v2, :cond_5

    const/16 v2, 0xe0

    if-eq v0, v2, :cond_5

    const/16 v2, 0xe2

    if-eq v0, v2, :cond_5

    const/16 v2, 0xe3

    if-eq v0, v2, :cond_5

    const/16 v2, 0xe5

    if-eq v0, v2, :cond_5

    const/16 v2, 0xe9

    if-eq v0, v2, :cond_5

    const/16 v2, 0xea

    if-eq v0, v2, :cond_5

    const/16 v2, 0xeb

    if-eq v0, v2, :cond_5

    const/16 v2, 0x16

    if-eq v0, v2, :cond_5

    const/16 v2, 0x17

    if-eq v0, v2, :cond_5

    const/16 v2, 0x18

    if-ne v0, v2, :cond_6

    .line 3111
    :cond_5
    iput v0, p0, Lohu;->b:I

    goto/16 :goto_0

    .line 3113
    :cond_6
    iput v1, p0, Lohu;->b:I

    goto/16 :goto_0

    .line 2839
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 2792
    iget-object v0, p0, Lohu;->a:[Loht;

    if-eqz v0, :cond_1

    .line 2793
    iget-object v1, p0, Lohu;->a:[Loht;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 2794
    if-eqz v3, :cond_0

    .line 2795
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 2793
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2799
    :cond_1
    iget-object v0, p0, Lohu;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 2800
    const/4 v0, 0x2

    iget-object v1, p0, Lohu;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 2802
    :cond_2
    iget v0, p0, Lohu;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_3

    .line 2803
    const/4 v0, 0x3

    iget v1, p0, Lohu;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2805
    :cond_3
    iget-object v0, p0, Lohu;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2807
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2775
    invoke-virtual {p0, p1}, Lohu;->a(Loxn;)Lohu;

    move-result-object v0

    return-object v0
.end method
