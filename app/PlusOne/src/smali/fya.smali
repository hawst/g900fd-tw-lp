.class public final Lfya;
.super Lfye;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# static fields
.field private static a:Z

.field private static b:I

.field private static c:Landroid/graphics/drawable/Drawable;

.field private static d:Ljava/lang/String;

.field private static e:Landroid/graphics/drawable/Drawable;

.field private static f:Ljava/lang/String;


# instance fields
.field private g:Z

.field private h:Z

.field private i:Lfxe;

.field private j:Landroid/widget/Spinner;

.field private k:Lfyb;

.field private l:Lfxz;

.field private m:Lfxf;

.field private n:I

.field private o:Lidh;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 61
    invoke-direct {p0, p1, p2, p3}, Lfye;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 47
    iput v3, p0, Lfya;->n:I

    .line 62
    sget-boolean v0, Lfya;->a:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d02be

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lfya;->b:I

    const v1, 0x7f0203d0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lfya;->c:Landroid/graphics/drawable/Drawable;

    const v1, 0x7f0a0746

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lfya;->d:Ljava/lang/String;

    const v1, 0x7f0203cf

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lfya;->e:Landroid/graphics/drawable/Drawable;

    const v1, 0x7f0a0747

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lfya;->f:Ljava/lang/String;

    const/4 v0, 0x1

    sput-boolean v0, Lfya;->a:Z

    :cond_0
    new-instance v0, Landroid/widget/Spinner;

    invoke-direct {v0, p1}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lfya;->j:Landroid/widget/Spinner;

    iget-object v0, p0, Lfya;->j:Landroid/widget/Spinner;

    new-instance v1, Lfyf;

    const/4 v2, -0x2

    invoke-direct {v1, v3, v2}, Lfyf;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lfya;->j:Landroid/widget/Spinner;

    invoke-virtual {p0, v0}, Lfya;->addView(Landroid/view/View;)V

    new-instance v0, Lfxe;

    invoke-direct {v0, p1, p2, p3}, Lfxe;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lfya;->i:Lfxe;

    iget-object v0, p0, Lfya;->i:Lfxe;

    invoke-virtual {v0, p0}, Lfxe;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lfya;->i:Lfxe;

    invoke-virtual {p0, v0}, Lfya;->addView(Landroid/view/View;)V

    sget v0, Lfya;->b:I

    sget v1, Lfya;->b:I

    sget v2, Lfya;->b:I

    sget v3, Lfya;->b:I

    invoke-virtual {p0, v0, v1, v2, v3}, Lfya;->setPadding(IIII)V

    .line 63
    return-void
.end method


# virtual methods
.method public a(Lidh;Leaq;Lfxz;Lfxf;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 97
    iput-object p3, p0, Lfya;->l:Lfxz;

    .line 98
    iput-object p4, p0, Lfya;->m:Lfxf;

    .line 99
    iput-object p1, p0, Lfya;->o:Lidh;

    .line 101
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 102
    iget-object v0, p0, Lfya;->o:Lidh;

    invoke-static {v0, v2, v3}, Ldrm;->a(Lidh;J)Z

    move-result v0

    iput-boolean v0, p0, Lfya;->h:Z

    .line 104
    new-instance v0, Lfyb;

    invoke-virtual {p0}, Lfya;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-boolean v3, p0, Lfya;->h:Z

    invoke-direct {v0, p0, v2, v3}, Lfyb;-><init>(Lfya;Landroid/content/Context;Z)V

    iput-object v0, p0, Lfya;->k:Lfyb;

    .line 105
    iget-object v0, p0, Lfya;->j:Landroid/widget/Spinner;

    iget-object v2, p0, Lfya;->k:Lfyb;

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 108
    iget v0, p2, Leaq;->b:I

    const/high16 v2, -0x80000000

    if-eq v0, v2, :cond_2

    iget v0, p2, Leaq;->b:I

    .line 110
    :goto_0
    iget-object v2, p0, Lfya;->k:Lfyb;

    invoke-static {v2, v0}, Lfyb;->a(Lfyb;I)I

    move-result v0

    iput v0, p0, Lfya;->n:I

    .line 112
    iget-object v0, p0, Lfya;->j:Landroid/widget/Spinner;

    iget v2, p0, Lfya;->n:I

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setSelection(I)V

    .line 113
    iget-object v0, p0, Lfya;->k:Lfyb;

    invoke-virtual {v0}, Lfyb;->notifyDataSetChanged()V

    .line 114
    iget-object v0, p0, Lfya;->j:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 115
    iget-object v0, p0, Lfya;->j:Landroid/widget/Spinner;

    iget-boolean v2, p2, Leaq;->c:Z

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 117
    iget v0, p0, Lfya;->n:I

    iget-object v2, p0, Lfya;->k:Lfyb;

    .line 118
    invoke-static {v2, v1}, Lfyb;->a(Lfyb;I)I

    move-result v2

    if-ne v0, v2, :cond_3

    iget-boolean v0, p0, Lfya;->h:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfya;->o:Lidh;

    .line 119
    invoke-static {v0}, Ldrm;->d(Lidh;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lfya;->h:Z

    if-nez v0, :cond_3

    iget-boolean v0, p2, Leaq;->j:Z

    if-eqz v0, :cond_3

    :cond_1
    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lfya;->g:Z

    .line 121
    return-void

    .line 109
    :cond_2
    invoke-static {p1}, Ldrm;->a(Lidh;)I

    move-result v0

    goto :goto_0

    .line 119
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected measureChildren(II)V
    .locals 5

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    const/4 v3, 0x0

    .line 134
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 135
    iget-boolean v1, p0, Lfya;->g:Z

    if-nez v1, :cond_0

    .line 140
    :goto_0
    iget-object v1, p0, Lfya;->j:Landroid/widget/Spinner;

    invoke-static {v1, v0, v4, v3, v3}, Lfya;->a(Landroid/view/View;IIII)V

    .line 141
    iget-object v1, p0, Lfya;->j:Landroid/widget/Spinner;

    invoke-static {v1, v3, v3}, Lfya;->a(Landroid/view/View;II)V

    .line 143
    iget-boolean v1, p0, Lfya;->g:Z

    if-eqz v1, :cond_2

    .line 144
    iget-object v1, p0, Lfya;->i:Lfxe;

    iget-object v2, p0, Lfya;->j:Landroid/widget/Spinner;

    .line 145
    invoke-virtual {v2}, Landroid/widget/Spinner;->getMeasuredHeight()I

    move-result v2

    .line 144
    invoke-static {v1, v0, v4, v2, v4}, Lfya;->a(Landroid/view/View;IIII)V

    .line 146
    iget-object v1, p0, Lfya;->i:Lfxe;

    sget v2, Lfya;->b:I

    add-int/2addr v0, v2

    invoke-static {v1, v0, v3}, Lfya;->a(Landroid/view/View;II)V

    .line 147
    iget-object v0, p0, Lfya;->i:Lfxe;

    invoke-virtual {v0, v3}, Lfxe;->setVisibility(I)V

    .line 149
    iget-object v0, p0, Lfya;->o:Lidh;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfya;->o:Lidh;

    invoke-virtual {v0}, Lidh;->a()Lpbl;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lfya;->h:Z

    if-eqz v0, :cond_1

    .line 150
    iget-object v0, p0, Lfya;->i:Lfxe;

    sget-object v1, Lfya;->f:Ljava/lang/String;

    sget-object v2, Lfya;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1, v2}, Lfxe;->a(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V

    .line 157
    :goto_1
    return-void

    .line 135
    :cond_0
    sget v1, Lfya;->b:I

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    .line 139
    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_0

    .line 152
    :cond_1
    iget-object v0, p0, Lfya;->i:Lfxe;

    sget-object v1, Lfya;->d:Ljava/lang/String;

    sget-object v2, Lfya;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1, v2}, Lfxe;->a(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 155
    :cond_2
    iget-object v0, p0, Lfya;->i:Lfxe;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lfxe;->setVisibility(I)V

    goto :goto_1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 313
    iget-object v0, p0, Lfya;->m:Lfxf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfya;->i:Lfxe;

    if-ne p1, v0, :cond_0

    .line 314
    iget-boolean v0, p0, Lfya;->h:Z

    if-eqz v0, :cond_1

    .line 315
    iget-object v0, p0, Lfya;->m:Lfxf;

    invoke-interface {v0}, Lfxf;->ab()V

    .line 320
    :cond_0
    :goto_0
    return-void

    .line 317
    :cond_1
    iget-object v0, p0, Lfya;->m:Lfxf;

    invoke-interface {v0}, Lfxf;->aa()V

    goto :goto_0
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 281
    iget v3, p0, Lfya;->n:I

    if-eq v3, p3, :cond_1

    .line 282
    iget v3, p0, Lfya;->n:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_2

    move v3, v1

    .line 284
    :goto_0
    if-nez v3, :cond_0

    .line 285
    packed-switch p3, :pswitch_data_0

    .line 299
    :cond_0
    :goto_1
    iput p3, p0, Lfya;->n:I

    .line 300
    iget v0, p0, Lfya;->n:I

    iget-object v3, p0, Lfya;->k:Lfyb;

    .line 302
    invoke-static {v3, v1}, Lfyb;->a(Lfyb;I)I

    move-result v3

    if-ne v0, v3, :cond_4

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lfya;->g:Z

    .line 303
    invoke-virtual {p0}, Lfya;->requestLayout()V

    .line 305
    :cond_1
    return-void

    :cond_2
    move v3, v2

    .line 282
    goto :goto_0

    .line 287
    :pswitch_0
    iget-object v0, p0, Lfya;->l:Lfxz;

    invoke-interface {v0, v1}, Lfxz;->b(I)V

    goto :goto_1

    .line 290
    :pswitch_1
    iget-object v3, p0, Lfya;->l:Lfxz;

    iget-boolean v4, p0, Lfya;->h:Z

    if-eqz v4, :cond_3

    :goto_3
    invoke-interface {v3, v0}, Lfxz;->b(I)V

    goto :goto_1

    :cond_3
    const/4 v0, 0x6

    goto :goto_3

    .line 294
    :pswitch_2
    iget-object v3, p0, Lfya;->l:Lfxz;

    invoke-interface {v3, v0}, Lfxz;->b(I)V

    goto :goto_1

    :cond_4
    move v0, v2

    .line 302
    goto :goto_2

    .line 285
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 309
    return-void
.end method
