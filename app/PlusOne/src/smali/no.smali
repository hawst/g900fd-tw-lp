.class final Lno;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# instance fields
.field private synthetic a:Lnr;

.field private synthetic b:Lnm;


# direct methods
.method constructor <init>(Lnm;Lnr;)V
    .locals 0

    .prologue
    .line 350
    iput-object p1, p0, Lno;->b:Lnm;

    iput-object p2, p0, Lno;->a:Lnr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 360
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 364
    iget-object v0, p0, Lno;->a:Lnr;

    invoke-virtual {v0}, Lnr;->j()V

    .line 365
    iget-object v0, p0, Lno;->a:Lnr;

    invoke-virtual {v0}, Lnr;->a()V

    .line 366
    iget-object v0, p0, Lno;->a:Lnr;

    iget-object v1, p0, Lno;->a:Lnr;

    invoke-virtual {v1}, Lnr;->g()F

    move-result v1

    invoke-virtual {v0, v1}, Lnr;->b(F)V

    .line 367
    iget-object v0, p0, Lno;->b:Lnm;

    iget-boolean v0, v0, Lnm;->a:Z

    if-eqz v0, :cond_0

    .line 370
    iget-object v0, p0, Lno;->b:Lnm;

    iput-boolean v2, v0, Lnm;->a:Z

    .line 371
    const-wide/16 v0, 0x535

    invoke-virtual {p1, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 372
    iget-object v0, p0, Lno;->a:Lnr;

    invoke-virtual {v0, v2}, Lnr;->a(Z)V

    .line 376
    :goto_0
    return-void

    .line 374
    :cond_0
    iget-object v0, p0, Lno;->b:Lnm;

    iget-object v1, p0, Lno;->b:Lnm;

    invoke-static {v1}, Lnm;->a(Lnm;)F

    move-result v1

    const/high16 v2, 0x3f800000    # 1.0f

    add-float/2addr v1, v2

    const/high16 v2, 0x40a00000    # 5.0f

    rem-float/2addr v1, v2

    invoke-static {v0, v1}, Lnm;->a(Lnm;F)F

    goto :goto_0
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2

    .prologue
    .line 354
    iget-object v0, p0, Lno;->b:Lnm;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lnm;->a(Lnm;F)F

    .line 355
    return-void
.end method
