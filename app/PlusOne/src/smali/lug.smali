.class public final Llug;
.super Loxq;
.source "PG"


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field private c:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/high16 v0, -0x80000000

    .line 9
    invoke-direct {p0}, Loxq;-><init>()V

    .line 35
    iput v0, p0, Llug;->a:I

    .line 40
    iput v0, p0, Llug;->c:I

    .line 9
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    const/high16 v3, -0x80000000

    .line 60
    const/4 v0, 0x0

    .line 61
    iget v1, p0, Llug;->a:I

    if-eq v1, v3, :cond_0

    .line 62
    const/4 v0, 0x1

    iget v1, p0, Llug;->a:I

    .line 63
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 65
    :cond_0
    iget-object v1, p0, Llug;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 66
    const/4 v1, 0x2

    iget-object v2, p0, Llug;->b:Ljava/lang/String;

    .line 67
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 69
    :cond_1
    iget v1, p0, Llug;->c:I

    if-eq v1, v3, :cond_2

    .line 70
    const/4 v1, 0x3

    iget v2, p0, Llug;->c:I

    .line 71
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 73
    :cond_2
    iget-object v1, p0, Llug;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 74
    iput v0, p0, Llug;->ai:I

    .line 75
    return v0
.end method

.method public a(Loxn;)Llug;
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 83
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 84
    sparse-switch v0, :sswitch_data_0

    .line 88
    iget-object v1, p0, Llug;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 89
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llug;->ah:Ljava/util/List;

    .line 92
    :cond_1
    iget-object v1, p0, Llug;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 94
    :sswitch_0
    return-object p0

    .line 99
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 100
    if-eqz v0, :cond_2

    if-eq v0, v2, :cond_2

    if-eq v0, v3, :cond_2

    if-eq v0, v4, :cond_2

    if-eq v0, v5, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa

    if-eq v0, v1, :cond_2

    const/16 v1, 0xb

    if-eq v0, v1, :cond_2

    const/16 v1, 0xc

    if-ne v0, v1, :cond_3

    .line 113
    :cond_2
    iput v0, p0, Llug;->a:I

    goto :goto_0

    .line 115
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Llug;->a:I

    goto :goto_0

    .line 120
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llug;->b:Ljava/lang/String;

    goto :goto_0

    .line 124
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 125
    if-eq v0, v2, :cond_4

    if-eq v0, v3, :cond_4

    if-eq v0, v4, :cond_4

    if-ne v0, v5, :cond_5

    .line 129
    :cond_4
    iput v0, p0, Llug;->c:I

    goto :goto_0

    .line 131
    :cond_5
    iput v2, p0, Llug;->c:I

    goto :goto_0

    .line 84
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 45
    iget v0, p0, Llug;->a:I

    if-eq v0, v2, :cond_0

    .line 46
    const/4 v0, 0x1

    iget v1, p0, Llug;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 48
    :cond_0
    iget-object v0, p0, Llug;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 49
    const/4 v0, 0x2

    iget-object v1, p0, Llug;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 51
    :cond_1
    iget v0, p0, Llug;->c:I

    if-eq v0, v2, :cond_2

    .line 52
    const/4 v0, 0x3

    iget v1, p0, Llug;->c:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 54
    :cond_2
    iget-object v0, p0, Llug;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 56
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Llug;->a(Loxn;)Llug;

    move-result-object v0

    return-object v0
.end method
