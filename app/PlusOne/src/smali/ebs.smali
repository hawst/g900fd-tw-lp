.class public final Lebs;
.super Legi;
.source "PG"

# interfaces
.implements Lbc;
.implements Lfuf;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Legi;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lfuf;"
    }
.end annotation


# instance fields
.field private N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

.field private ah:Leuy;

.field private ai:Z

.field private aj:Z

.field private ak:Ljava/lang/Integer;

.field private final al:Licq;

.field private final am:Lfhh;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 49
    invoke-direct {p0}, Legi;-><init>()V

    .line 62
    new-instance v0, Licq;

    iget-object v1, p0, Lebs;->av:Llqm;

    invoke-direct {v0, v1}, Licq;-><init>(Llqr;)V

    const v1, 0x7f0a064c

    .line 63
    invoke-virtual {v0, v1}, Licq;->b(I)Licq;

    move-result-object v0

    iput-object v0, p0, Lebs;->al:Licq;

    .line 65
    new-instance v0, Lebt;

    invoke-direct {v0, p0}, Lebt;-><init>(Lebs;)V

    iput-object v0, p0, Lebs;->am:Lfhh;

    .line 372
    return-void
.end method

.method private a(ILfib;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 346
    iget-object v0, p0, Lebs;->ak:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lebs;->ak:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_1

    .line 360
    :cond_0
    :goto_0
    return-void

    .line 349
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lebs;->ak:Ljava/lang/Integer;

    .line 351
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lfib;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lebs;->Z:Z

    .line 352
    iget-boolean v0, p0, Lebs;->Z:Z

    if-eqz v0, :cond_3

    .line 353
    invoke-virtual {p0}, Lebs;->o()Landroid/content/res/Resources;

    move-result-object v0

    .line 354
    const v2, 0x7f0a07ac

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 355
    invoke-virtual {p0}, Lebs;->n()Lz;

    move-result-object v2

    invoke-static {v2, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 359
    :goto_2
    invoke-virtual {p0}, Lebs;->x()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lebs;->d(Landroid/view/View;)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 351
    goto :goto_1

    .line 357
    :cond_3
    iget-object v0, p0, Lebs;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->h()V

    goto :goto_2
.end method

.method static synthetic a(Lebs;ILfib;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Lebs;->a(ILfib;)V

    return-void
.end method

.method private d(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 328
    if-nez p1, :cond_0

    .line 343
    :goto_0
    return-void

    .line 332
    :cond_0
    invoke-virtual {p0}, Lebs;->U()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 333
    iget-boolean v0, p0, Lebs;->ai:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lebs;->aj:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lebs;->ak:Ljava/lang/Integer;

    if-nez v0, :cond_1

    .line 334
    iget-object v0, p0, Lebs;->al:Licq;

    sget-object v1, Lict;->c:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    .line 341
    :goto_1
    invoke-virtual {p0}, Lebs;->Z_()V

    .line 342
    invoke-virtual {p0}, Lebs;->ag()V

    goto :goto_0

    .line 336
    :cond_1
    iget-object v0, p0, Lebs;->al:Licq;

    sget-object v1, Lict;->a:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    goto :goto_1

    .line 339
    :cond_2
    iget-object v0, p0, Lebs;->al:Licq;

    sget-object v1, Lict;->b:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    goto :goto_1
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 229
    sget-object v0, Lhmw;->ar:Lhmw;

    return-object v0
.end method

.method public J_()Z
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Lebs;->ak:Ljava/lang/Integer;

    if-nez v0, :cond_0

    invoke-super {p0}, Legi;->J_()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public L_()V
    .locals 4

    .prologue
    .line 234
    invoke-super {p0}, Legi;->L_()V

    .line 236
    iget-object v0, p0, Lebs;->ak:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 252
    :goto_0
    return-void

    .line 240
    :cond_0
    invoke-virtual {p0}, Lebs;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "gaia_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 242
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 243
    iget-object v0, p0, Lebs;->P:Lhee;

    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v0

    const-string v1, "gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 246
    :cond_1
    const/4 v1, 0x0

    iput-boolean v1, p0, Lebs;->Z:Z

    .line 247
    iget-object v1, p0, Lebs;->at:Llnl;

    iget-object v2, p0, Lebs;->P:Lhee;

    .line 248
    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    const/4 v3, 0x0

    .line 247
    invoke-static {v1, v2, v0, v3}, Lcom/google/android/apps/plus/service/EsService;->c(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lebs;->ak:Ljava/lang/Integer;

    .line 249
    invoke-virtual {p0}, Lebs;->x()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lebs;->d(Landroid/view/View;)V

    .line 250
    iget-object v0, p0, Lebs;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Lebs;->at:Llnl;

    invoke-direct {v1, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->ek:Lhmv;

    .line 251
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 250
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    goto :goto_0
.end method

.method protected U()Z
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lebs;->ah:Leuy;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 219
    :goto_0
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_1
    return v0

    .line 218
    :cond_1
    iget-object v0, p0, Lebs;->ah:Leuy;

    invoke-virtual {v0}, Leuy;->a()Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    .line 219
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public V()Z
    .locals 3

    .prologue
    .line 178
    invoke-virtual {p0}, Lebs;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "finish_on_back"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 179
    invoke-super {p0}, Legi;->a()Z

    move-result v0

    .line 181
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Legi;->V()Z

    move-result v0

    goto :goto_0
.end method

.method public Y()Z
    .locals 1

    .prologue
    .line 381
    iget-object v0, p0, Lebs;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->l()Z

    move-result v0

    return v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 88
    const v0, 0x7f0400d2

    invoke-super {p0, p1, p2, p3, v0}, Legi;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;

    move-result-object v6

    .line 91
    invoke-virtual {p0}, Lebs;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "gaia_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 93
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lebs;->P:Lhee;

    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v0

    const-string v1, "gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 98
    :cond_0
    new-instance v0, Ljvl;

    iget-object v1, p0, Lebs;->at:Llnl;

    invoke-direct {v0, v1}, Ljvl;-><init>(Landroid/content/Context;)V

    iget v5, v0, Ljvl;->b:I

    .line 100
    new-instance v0, Leuy;

    iget-object v1, p0, Lebs;->at:Llnl;

    iget-object v4, p0, Lebs;->P:Lhee;

    .line 101
    invoke-interface {v4}, Lhee;->d()I

    move-result v4

    invoke-direct/range {v0 .. v5}, Leuy;-><init>(Landroid/content/Context;Landroid/database/Cursor;Ljava/lang/String;II)V

    iput-object v0, p0, Lebs;->ah:Leuy;

    .line 102
    iget-object v0, p0, Lebs;->ah:Leuy;

    invoke-virtual {v0, p0}, Leuy;->a(Landroid/view/View$OnClickListener;)V

    .line 104
    const v0, 0x7f100304

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    iput-object v0, p0, Lebs;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    .line 106
    iget-object v0, p0, Lebs;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->c(I)V

    .line 107
    iget-object v0, p0, Lebs;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-virtual {v0, v5}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(I)V

    .line 109
    iget-object v0, p0, Lebs;->at:Llnl;

    invoke-static {v0}, Llsc;->a(Landroid/content/Context;)Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0d01d1

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-static {v1}, Llsc;->a(Landroid/util/DisplayMetrics;)F

    move-result v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 110
    iget-object v1, p0, Lebs;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->b(I)V

    .line 111
    iget-object v1, p0, Lebs;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-virtual {v1, v0, v0, v0, v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->setPadding(IIII)V

    .line 113
    iget-object v0, p0, Lebs;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    new-instance v1, Lebu;

    invoke-direct {v1}, Lebu;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(Llkg;)V

    .line 114
    iget-object v0, p0, Lebs;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    iget-object v1, p0, Lebs;->ah:Leuy;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(Landroid/widget/ListAdapter;)V

    .line 115
    iget-object v0, p0, Lebs;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    const v1, 0x7f020415

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->g(I)V

    .line 117
    invoke-virtual {p0}, Lebs;->U()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 118
    iget-object v0, p0, Lebs;->al:Licq;

    sget-object v1, Lict;->a:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    .line 121
    :cond_1
    invoke-virtual {p0}, Lebs;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 123
    return-object v6
.end method

.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 291
    invoke-virtual {p0}, Lebs;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "gaia_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 293
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 294
    iget-object v0, p0, Lebs;->P:Lhee;

    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v0

    const-string v1, "gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 297
    :cond_0
    new-instance v1, Leva;

    iget-object v2, p0, Lebs;->at:Llnl;

    iget-object v3, p0, Lebs;->P:Lhee;

    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    const/4 v4, 0x2

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    .line 298
    invoke-static {v4, v5}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v3, v0}, Leva;-><init>(Landroid/content/Context;ILjava/lang/String;)V

    return-object v1
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 303
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    iget-object v0, p0, Lebs;->ak:Ljava/lang/Integer;

    if-nez v0, :cond_2

    .line 305
    invoke-virtual {p0}, Lebs;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "gaia_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 307
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 308
    iget-object v0, p0, Lebs;->P:Lhee;

    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v0

    const-string v1, "gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 311
    :cond_1
    const/4 v1, 0x2

    new-array v2, v8, [Ljava/lang/String;

    aput-object v0, v2, v9

    invoke-static {v1, v2}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 312
    iget-boolean v0, p0, Lebs;->ai:Z

    if-nez v0, :cond_2

    .line 313
    iput-boolean v8, p0, Lebs;->aj:Z

    .line 314
    new-instance v1, Lfue;

    iget-object v2, p0, Lebs;->at:Llnl;

    iget-object v0, p0, Lebs;->P:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v3

    const-wide/16 v6, -0x1

    move-object v4, p0

    invoke-direct/range {v1 .. v7}, Lfue;-><init>(Landroid/content/Context;ILfuf;Ljava/lang/String;J)V

    new-array v0, v9, [Ljava/lang/Void;

    .line 315
    invoke-virtual {v1, v0}, Lfue;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 318
    :cond_2
    iput-boolean v8, p0, Lebs;->ai:Z

    .line 319
    iget-object v0, p0, Lebs;->ah:Leuy;

    invoke-virtual {v0, p1}, Leuy;->b(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 320
    invoke-virtual {p0}, Lebs;->x()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lebs;->d(Landroid/view/View;)V

    .line 321
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 75
    invoke-super {p0, p1}, Legi;->a(Landroid/os/Bundle;)V

    .line 77
    if-eqz p1, :cond_1

    .line 78
    const-string v0, "refresh_request"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    const-string v0, "refresh_request"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lebs;->ak:Ljava/lang/Integer;

    .line 81
    :cond_0
    const-string v0, "first_load_finished"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lebs;->ai:Z

    .line 83
    :cond_1
    return-void
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 325
    return-void
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 49
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p2}, Lebs;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method public a(Loo;)V
    .locals 1

    .prologue
    .line 191
    invoke-super {p0, p1}, Legi;->a(Loo;)V

    .line 193
    iget-object v0, p0, Lebs;->Y:Lctq;

    invoke-virtual {v0}, Lctq;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 194
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Loo;->c(Z)V

    .line 196
    :cond_0
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 186
    invoke-super {p0, p1}, Legi;->a(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public aO_()V
    .locals 2

    .prologue
    .line 134
    invoke-super {p0}, Legi;->aO_()V

    .line 136
    iget-object v0, p0, Lebs;->am:Lfhh;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(Lfhh;)V

    .line 138
    iget-object v0, p0, Lebs;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Lebs;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a()V

    .line 142
    :cond_0
    iget-object v0, p0, Lebs;->ak:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 143
    iget-object v0, p0, Lebs;->ak:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 144
    invoke-virtual {p0}, Lebs;->U()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 145
    iget-object v0, p0, Lebs;->al:Licq;

    sget-object v1, Lict;->a:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    .line 152
    :cond_1
    :goto_0
    return-void

    .line 148
    :cond_2
    iget-object v0, p0, Lebs;->ak:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(I)Lfib;

    move-result-object v0

    .line 149
    iget-object v1, p0, Lebs;->ak:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, v0}, Lebs;->a(ILfib;)V

    goto :goto_0
.end method

.method protected b(Lhjk;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 200
    invoke-super {p0, p1}, Legi;->b(Lhjk;)V

    .line 201
    iget-object v0, p0, Lebs;->Y:Lctq;

    invoke-virtual {v0}, Lctq;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 202
    const v0, 0x7f0a0ab4

    invoke-interface {p1, v0}, Lhjk;->d(I)V

    .line 214
    :goto_0
    return-void

    .line 204
    :cond_0
    const v0, 0x7f0a0a78

    invoke-interface {p1, v0}, Lhjk;->d(I)V

    .line 206
    const v0, 0x7f10067b

    .line 207
    invoke-interface {p1, v0}, Lhjk;->a(I)Lhjc;

    move-result-object v0

    check-cast v0, Lhjv;

    .line 208
    invoke-virtual {v0, v1}, Lhjv;->a(I)V

    .line 210
    invoke-virtual {p0, p1, v1}, Lebs;->a(Lhjk;I)V

    .line 212
    const v0, 0x7f100696

    invoke-interface {p1, v0}, Lhjk;->c(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public c(Landroid/view/View;)Z
    .locals 12

    .prologue
    const/4 v10, 0x1

    const/4 v7, 0x0

    .line 256
    const v0, 0x7f10007f

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 257
    invoke-virtual {p0}, Lebs;->n()Lz;

    move-result-object v0

    .line 258
    if-eqz v0, :cond_0

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 286
    :cond_0
    :goto_0
    return v7

    .line 262
    :cond_1
    iget-object v1, p0, Lebs;->P:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    .line 263
    iget-object v3, p0, Lebs;->Y:Lctq;

    invoke-virtual {v3}, Lctq;->c()I

    move-result v3

    if-ne v3, v10, :cond_2

    .line 267
    invoke-virtual {p0}, Lebs;->k()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "photo_picker_crop_mode"

    invoke-virtual {v3, v4, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 268
    invoke-virtual {p0}, Lebs;->k()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "external"

    invoke-virtual {v4, v5, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    iget v5, p0, Lebs;->aa:I

    .line 270
    invoke-virtual {p0}, Lebs;->k()Landroid/os/Bundle;

    move-result-object v6

    const-string v8, "destination"

    invoke-virtual {v6, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 272
    invoke-virtual {p0}, Lebs;->k()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "photo_min_width"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v8

    .line 273
    invoke-virtual {p0}, Lebs;->k()Landroid/os/Bundle;

    move-result-object v9

    const-string v11, "photo_min_height"

    invoke-virtual {v9, v11}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v9

    .line 264
    invoke-static/range {v0 .. v9}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;IZILjava/lang/Integer;ZII)Landroid/content/Intent;

    move-result-object v0

    .line 274
    iget-object v1, p0, Lebs;->S:Lhke;

    const v2, 0x7f1000c3

    invoke-virtual {v1, v2, v0}, Lhke;->a(ILandroid/content/Intent;)V

    :goto_1
    move v7, v10

    .line 286
    goto :goto_0

    .line 277
    :cond_2
    invoke-static {v0, v1}, Leyq;->g(Landroid/content/Context;I)Ljuj;

    move-result-object v0

    .line 278
    invoke-virtual {v0, v2}, Ljuj;->a(Ljava/lang/String;)Ljuj;

    move-result-object v0

    iget-object v1, p0, Lebs;->Y:Lctq;

    .line 279
    invoke-virtual {v1}, Lctq;->c()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljuj;->a(Ljava/lang/Integer;)Ljuj;

    move-result-object v0

    iget-object v1, p0, Lebs;->X:Lctz;

    .line 280
    invoke-virtual {v1}, Lctz;->a()Ljcn;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljuj;->a(Ljcn;)Ljuj;

    move-result-object v0

    .line 281
    invoke-virtual {v0}, Ljuj;->a()Landroid/content/Intent;

    move-result-object v1

    .line 282
    iget-object v0, p0, Lebs;->au:Llnh;

    const-class v2, Lhms;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    iget-object v3, p0, Lebs;->at:Llnl;

    invoke-direct {v2, v3}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v3, Lhmv;->eb:Lhmv;

    .line 283
    invoke-virtual {v2, v3}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v2

    .line 282
    invoke-interface {v0, v2}, Lhms;->a(Lhmr;)V

    .line 284
    invoke-virtual {p0, v1}, Lebs;->b(Landroid/content/Intent;)V

    goto :goto_1
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 169
    invoke-super {p0, p1}, Legi;->e(Landroid/os/Bundle;)V

    .line 170
    iget-object v0, p0, Lebs;->ak:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 171
    const-string v0, "refresh_request"

    iget-object v1, p0, Lebs;->ak:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 173
    :cond_0
    const-string v0, "first_load_finished"

    iget-boolean v1, p0, Lebs;->ai:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 174
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 128
    invoke-super {p0}, Legi;->g()V

    .line 129
    iget-object v0, p0, Lebs;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-static {v0}, Llii;->c(Landroid/view/View;)V

    .line 130
    return-void
.end method

.method public h()V
    .locals 1

    .prologue
    .line 163
    invoke-super {p0}, Legi;->h()V

    .line 164
    iget-object v0, p0, Lebs;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-static {v0}, Llii;->d(Landroid/view/View;)V

    .line 165
    return-void
.end method

.method public l_(Z)V
    .locals 1

    .prologue
    .line 364
    const/4 v0, 0x0

    iput-boolean v0, p0, Lebs;->aj:Z

    .line 365
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lebs;->n()Lz;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 366
    invoke-virtual {p0}, Lebs;->L_()V

    .line 368
    :cond_0
    invoke-virtual {p0}, Lebs;->x()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lebs;->d(Landroid/view/View;)V

    .line 369
    return-void
.end method

.method public z()V
    .locals 1

    .prologue
    .line 156
    invoke-super {p0}, Legi;->z()V

    .line 158
    iget-object v0, p0, Lebs;->am:Lfhh;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(Lfhh;)V

    .line 159
    return-void
.end method
